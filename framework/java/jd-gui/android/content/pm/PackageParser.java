package android.content.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.os.UserId;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Slog;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import com.android.internal.util.XmlUtils;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import miui.content.pm.ExtraPackageManager;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class PackageParser
{
    private static final String ANDROID_MANIFEST_FILENAME = "AndroidManifest.xml";
    private static final String ANDROID_RESOURCES = "http://schemas.android.com/apk/res/android";
    private static final boolean DEBUG_BACKUP = false;
    private static final boolean DEBUG_JAR = false;
    private static final boolean DEBUG_PARSER = false;
    public static final NewPermissionInfo[] NEW_PERMISSIONS;
    public static final int PARSE_CHATTY = 2;
    private static final int PARSE_DEFAULT_INSTALL_LOCATION = -1;
    public static final int PARSE_FORWARD_LOCK = 16;
    public static final int PARSE_IGNORE_PROCESSES = 8;
    public static final int PARSE_IS_SYSTEM = 1;
    public static final int PARSE_IS_SYSTEM_DIR = 64;
    public static final int PARSE_MUST_BE_APK = 4;
    public static final int PARSE_ON_SDCARD = 32;
    private static final boolean RIGID_PARSER = false;
    private static final String SDK_CODENAME;
    private static final int SDK_VERSION = 0;
    public static final SplitPermissionInfo[] SPLIT_PERMISSIONS;
    private static final String TAG = "PackageParser";
    private static WeakReference<byte[]> mReadBuffer;
    private static final Object mSync;
    private static boolean sCompatibilityModeEnabled;
    private String mArchiveSourcePath;
    private boolean mOnlyCoreApps;
    private ParseComponentArgs mParseActivityAliasArgs;
    private ParseComponentArgs mParseActivityArgs;
    private int mParseError = 1;
    private ParsePackageItemArgs mParseInstrumentationArgs;
    private ParseComponentArgs mParseProviderArgs;
    private ParseComponentArgs mParseServiceArgs;
    private String[] mSeparateProcesses;

    static
    {
        NewPermissionInfo[] arrayOfNewPermissionInfo = new NewPermissionInfo[2];
        arrayOfNewPermissionInfo[0] = new NewPermissionInfo("android.permission.WRITE_EXTERNAL_STORAGE", 4, 0);
        arrayOfNewPermissionInfo[1] = new NewPermissionInfo("android.permission.READ_PHONE_STATE", 4, 0);
        NEW_PERMISSIONS = arrayOfNewPermissionInfo;
        SplitPermissionInfo[] arrayOfSplitPermissionInfo = new SplitPermissionInfo[3];
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "android.permission.READ_EXTERNAL_STORAGE";
        arrayOfSplitPermissionInfo[0] = new SplitPermissionInfo("android.permission.WRITE_EXTERNAL_STORAGE", arrayOfString1, 10001);
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = "android.permission.READ_CALL_LOG";
        arrayOfSplitPermissionInfo[1] = new SplitPermissionInfo("android.permission.READ_CONTACTS", arrayOfString2, 16);
        String[] arrayOfString3 = new String[1];
        arrayOfString3[0] = "android.permission.WRITE_CALL_LOG";
        arrayOfSplitPermissionInfo[2] = new SplitPermissionInfo("android.permission.WRITE_CONTACTS", arrayOfString3, 16);
        SPLIT_PERMISSIONS = arrayOfSplitPermissionInfo;
        SDK_VERSION = Build.VERSION.SDK_INT;
        if ("REL".equals(Build.VERSION.CODENAME));
        for (String str = null; ; str = Build.VERSION.CODENAME)
        {
            SDK_CODENAME = str;
            mSync = new Object();
            sCompatibilityModeEnabled = true;
            return;
        }
    }

    public PackageParser(String paramString)
    {
        this.mArchiveSourcePath = paramString;
    }

    private static String buildClassName(String paramString, CharSequence paramCharSequence, String[] paramArrayOfString)
    {
        String str1 = null;
        if ((paramCharSequence == null) || (paramCharSequence.length() <= 0))
            paramArrayOfString[0] = ("Empty class name in package " + paramString);
        while (true)
        {
            return str1;
            String str2 = paramCharSequence.toString();
            int i = str2.charAt(0);
            if (i == 46)
            {
                str1 = (paramString + str2).intern();
            }
            else if (str2.indexOf('.') < 0)
            {
                StringBuilder localStringBuilder = new StringBuilder(paramString);
                localStringBuilder.append('.');
                localStringBuilder.append(str2);
                str1 = localStringBuilder.toString().intern();
            }
            else if ((i >= 97) && (i <= 122))
            {
                str1 = str2.intern();
            }
            else
            {
                paramArrayOfString[0] = ("Bad class name " + str2 + " in package " + paramString);
            }
        }
    }

    private static String buildCompoundName(String paramString1, CharSequence paramCharSequence, String paramString2, String[] paramArrayOfString)
    {
        String str1 = null;
        String str2 = paramCharSequence.toString();
        int i = str2.charAt(0);
        if ((paramString1 != null) && (i == 58))
            if (str2.length() < 2)
                paramArrayOfString[0] = ("Bad " + paramString2 + " name " + str2 + " in package " + paramString1 + ": must be at least two characters");
        while (true)
        {
            return str1;
            String str4 = validateName(str2.substring(1), false);
            if (str4 != null)
            {
                paramArrayOfString[0] = ("Invalid " + paramString2 + " name " + str2 + " in package " + paramString1 + ": " + str4);
            }
            else
            {
                str1 = (paramString1 + str2).intern();
                continue;
                String str3 = validateName(str2, true);
                if ((str3 != null) && (!"system".equals(str2)))
                    paramArrayOfString[0] = ("Invalid " + paramString2 + " name " + str2 + " in package " + paramString1 + ": " + str3);
                else
                    str1 = str2.intern();
            }
        }
    }

    private static String buildProcessName(String paramString1, String paramString2, CharSequence paramCharSequence, int paramInt, String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        if (((paramInt & 0x8) != 0) && (!"system".equals(paramCharSequence)))
            if (paramString2 == null);
        while (true)
        {
            return paramString2;
            paramString2 = paramString1;
            continue;
            if (paramArrayOfString1 != null)
                for (int i = -1 + paramArrayOfString1.length; ; i--)
                {
                    if (i < 0)
                        break label90;
                    String str = paramArrayOfString1[i];
                    if ((str.equals(paramString1)) || (str.equals(paramString2)) || (str.equals(paramCharSequence)))
                    {
                        paramString2 = paramString1;
                        break;
                    }
                }
            label90: if ((paramCharSequence != null) && (paramCharSequence.length() > 0))
                paramString2 = buildCompoundName(paramString1, paramCharSequence, "process", paramArrayOfString2);
        }
    }

    private static String buildTaskAffinityName(String paramString1, String paramString2, CharSequence paramCharSequence, String[] paramArrayOfString)
    {
        if (paramCharSequence == null);
        while (true)
        {
            return paramString2;
            if (paramCharSequence.length() <= 0)
                paramString2 = null;
            else
                paramString2 = buildCompoundName(paramString1, paramCharSequence, "taskAffinity", paramArrayOfString);
        }
    }

    private static boolean copyNeeded(int paramInt1, Package paramPackage, int paramInt2, Bundle paramBundle)
    {
        int i = 1;
        int j;
        if (paramInt2 != 0)
            if (paramInt2 == i)
            {
                j = i;
                if (paramPackage.applicationInfo.enabled == j)
                    break label38;
            }
        while (true)
        {
            return i;
            j = 0;
            break;
            label38: if ((((paramInt1 & 0x80) == 0) || ((paramBundle == null) && (paramPackage.mAppMetaData == null))) && (((paramInt1 & 0x400) == 0) || (paramPackage.usesLibraryFiles == null)))
                i = 0;
        }
    }

    public static final ActivityInfo generateActivityInfo(Activity paramActivity, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
    {
        ActivityInfo localActivityInfo;
        if (paramActivity == null)
            localActivityInfo = null;
        while (true)
        {
            return localActivityInfo;
            if ((!copyNeeded(paramInt1, paramActivity.owner, paramInt2, paramActivity.metaData)) && (paramInt3 == 0))
            {
                localActivityInfo = paramActivity.info;
            }
            else
            {
                localActivityInfo = new ActivityInfo(paramActivity.info);
                localActivityInfo.metaData = paramActivity.metaData;
                localActivityInfo.applicationInfo = generateApplicationInfo(paramActivity.owner, paramInt1, paramBoolean, paramInt2, paramInt3);
            }
        }
    }

    public static ApplicationInfo generateApplicationInfo(Package paramPackage, int paramInt1, boolean paramBoolean, int paramInt2)
    {
        return generateApplicationInfo(paramPackage, paramInt1, paramBoolean, paramInt2, UserId.getCallingUserId());
    }

    public static ApplicationInfo generateApplicationInfo(Package paramPackage, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
    {
        if (paramPackage == null)
        {
            localApplicationInfo1 = null;
            return localApplicationInfo1;
        }
        if ((!copyNeeded(paramInt1, paramPackage, paramInt2, null)) && (paramInt3 == 0))
        {
            if (!sCompatibilityModeEnabled)
                paramPackage.applicationInfo.disableCompatibilityMode();
            if (paramBoolean)
            {
                ApplicationInfo localApplicationInfo3 = paramPackage.applicationInfo;
                localApplicationInfo3.flags = (0x200000 | localApplicationInfo3.flags);
                label62: if (paramInt2 != 1)
                    break label115;
            }
            for (paramPackage.applicationInfo.enabled = true; ; paramPackage.applicationInfo.enabled = false)
                label115: 
                do
                {
                    paramPackage.applicationInfo.enabledSetting = paramInt2;
                    localApplicationInfo1 = paramPackage.applicationInfo;
                    break;
                    ApplicationInfo localApplicationInfo2 = paramPackage.applicationInfo;
                    localApplicationInfo2.flags = (0xFFDFFFFF & localApplicationInfo2.flags);
                    break label62;
                }
                while ((paramInt2 != 2) && (paramInt2 != 3));
        }
        ApplicationInfo localApplicationInfo1 = new ApplicationInfo(paramPackage.applicationInfo);
        if (paramInt3 != 0)
        {
            localApplicationInfo1.uid = UserId.getUid(paramInt3, localApplicationInfo1.uid);
            localApplicationInfo1.dataDir = PackageManager.getDataDirForUser(paramInt3, localApplicationInfo1.packageName);
        }
        if ((paramInt1 & 0x80) != 0)
            localApplicationInfo1.metaData = paramPackage.mAppMetaData;
        if ((paramInt1 & 0x400) != 0)
            localApplicationInfo1.sharedLibraryFiles = paramPackage.usesLibraryFiles;
        if (!sCompatibilityModeEnabled)
            localApplicationInfo1.disableCompatibilityMode();
        if (paramBoolean)
        {
            localApplicationInfo1.flags = (0x200000 | localApplicationInfo1.flags);
            label247: if (paramInt2 != 1)
                break label284;
        }
        for (localApplicationInfo1.enabled = true; ; localApplicationInfo1.enabled = false)
            label284: 
            do
            {
                localApplicationInfo1.enabledSetting = paramInt2;
                break;
                localApplicationInfo1.flags = (0xFFDFFFFF & localApplicationInfo1.flags);
                break label247;
            }
            while ((paramInt2 != 2) && (paramInt2 != 3));
    }

    public static final InstrumentationInfo generateInstrumentationInfo(Instrumentation paramInstrumentation, int paramInt)
    {
        InstrumentationInfo localInstrumentationInfo;
        if (paramInstrumentation == null)
            localInstrumentationInfo = null;
        while (true)
        {
            return localInstrumentationInfo;
            if ((paramInt & 0x80) == 0)
            {
                localInstrumentationInfo = paramInstrumentation.info;
            }
            else
            {
                localInstrumentationInfo = new InstrumentationInfo(paramInstrumentation.info);
                localInstrumentationInfo.metaData = paramInstrumentation.metaData;
            }
        }
    }

    public static PackageInfo generatePackageInfo(Package paramPackage, int[] paramArrayOfInt, int paramInt, long paramLong1, long paramLong2, HashSet<String> paramHashSet)
    {
        return generatePackageInfo(paramPackage, paramArrayOfInt, paramInt, paramLong1, paramLong2, paramHashSet, false, 0, UserId.getCallingUserId());
    }

    public static PackageInfo generatePackageInfo(Package paramPackage, int[] paramArrayOfInt, int paramInt1, long paramLong1, long paramLong2, HashSet<String> paramHashSet, boolean paramBoolean, int paramInt2)
    {
        return generatePackageInfo(paramPackage, paramArrayOfInt, paramInt1, paramLong1, paramLong2, paramHashSet, paramBoolean, paramInt2, UserId.getCallingUserId());
    }

    public static PackageInfo generatePackageInfo(Package paramPackage, int[] paramArrayOfInt, int paramInt1, long paramLong1, long paramLong2, HashSet<String> paramHashSet, boolean paramBoolean, int paramInt2, int paramInt3)
    {
        PackageInfo localPackageInfo = new PackageInfo();
        localPackageInfo.packageName = paramPackage.packageName;
        localPackageInfo.versionCode = paramPackage.mVersionCode;
        localPackageInfo.versionName = paramPackage.mVersionName;
        localPackageInfo.sharedUserId = paramPackage.mSharedUserId;
        localPackageInfo.sharedUserLabel = paramPackage.mSharedUserLabel;
        localPackageInfo.applicationInfo = generateApplicationInfo(paramPackage, paramInt1, paramBoolean, paramInt2, paramInt3);
        localPackageInfo.installLocation = paramPackage.installLocation;
        localPackageInfo.firstInstallTime = paramLong1;
        localPackageInfo.lastUpdateTime = paramLong2;
        if ((paramInt1 & 0x100) != 0)
            localPackageInfo.gids = paramArrayOfInt;
        int i28;
        if ((paramInt1 & 0x4000) != 0)
        {
            int i27 = paramPackage.configPreferences.size();
            if (i27 > 0)
            {
                localPackageInfo.configPreferences = new ConfigurationInfo[i27];
                paramPackage.configPreferences.toArray(localPackageInfo.configPreferences);
            }
            if (paramPackage.reqFeatures == null)
                break label325;
            i28 = paramPackage.reqFeatures.size();
            if (i28 > 0)
            {
                localPackageInfo.reqFeatures = new FeatureInfo[i28];
                paramPackage.reqFeatures.toArray(localPackageInfo.reqFeatures);
            }
        }
        int i21;
        label233: int i24;
        int i25;
        label239: int i26;
        if ((paramInt1 & 0x1) != 0)
        {
            i21 = paramPackage.activities.size();
            if (i21 > 0)
                if ((paramInt1 & 0x200) != 0)
                {
                    localPackageInfo.activities = new ActivityInfo[i21];
                    i24 = 0;
                    i25 = 0;
                    if (i24 >= i21)
                        break label387;
                    if ((!((Activity)paramPackage.activities.get(i24)).info.enabled) && ((paramInt1 & 0x200) == 0))
                        break label1315;
                    ActivityInfo[] arrayOfActivityInfo2 = localPackageInfo.activities;
                    i26 = i25 + 1;
                    arrayOfActivityInfo2[i25] = generateActivityInfo((Activity)paramPackage.activities.get(i24), paramInt1, paramBoolean, paramInt2, paramInt3);
                }
        }
        while (true)
        {
            i24++;
            i25 = i26;
            break label239;
            label325: i28 = 0;
            break;
            int i22 = 0;
            for (int i23 = 0; i23 < i21; i23++)
                if (((Activity)paramPackage.activities.get(i23)).info.enabled)
                    i22++;
            localPackageInfo.activities = new ActivityInfo[i22];
            break label233;
            label387: int i15;
            int i18;
            int i19;
            label431: int i20;
            if ((paramInt1 & 0x2) != 0)
            {
                i15 = paramPackage.receivers.size();
                if (i15 > 0)
                    if ((paramInt1 & 0x200) != 0)
                    {
                        localPackageInfo.receivers = new ActivityInfo[i15];
                        i18 = 0;
                        i19 = 0;
                        if (i18 >= i15)
                            break label573;
                        if ((!((Activity)paramPackage.receivers.get(i18)).info.enabled) && ((paramInt1 & 0x200) == 0))
                            break label1308;
                        ActivityInfo[] arrayOfActivityInfo1 = localPackageInfo.receivers;
                        i20 = i19 + 1;
                        arrayOfActivityInfo1[i19] = generateActivityInfo((Activity)paramPackage.receivers.get(i18), paramInt1, paramBoolean, paramInt2, paramInt3);
                    }
            }
            while (true)
            {
                i18++;
                i19 = i20;
                break label431;
                int i16 = 0;
                for (int i17 = 0; i17 < i15; i17++)
                    if (((Activity)paramPackage.receivers.get(i17)).info.enabled)
                        i16++;
                localPackageInfo.receivers = new ActivityInfo[i16];
                break;
                label573: int i9;
                int i12;
                int i13;
                label617: int i14;
                if ((paramInt1 & 0x4) != 0)
                {
                    i9 = paramPackage.services.size();
                    if (i9 > 0)
                        if ((paramInt1 & 0x200) != 0)
                        {
                            localPackageInfo.services = new ServiceInfo[i9];
                            i12 = 0;
                            i13 = 0;
                            if (i12 >= i9)
                                break label759;
                            if ((!((Service)paramPackage.services.get(i12)).info.enabled) && ((paramInt1 & 0x200) == 0))
                                break label1301;
                            ServiceInfo[] arrayOfServiceInfo = localPackageInfo.services;
                            i14 = i13 + 1;
                            arrayOfServiceInfo[i13] = generateServiceInfo((Service)paramPackage.services.get(i12), paramInt1, paramBoolean, paramInt2, paramInt3);
                        }
                }
                while (true)
                {
                    i12++;
                    i13 = i14;
                    break label617;
                    int i10 = 0;
                    for (int i11 = 0; i11 < i9; i11++)
                        if (((Service)paramPackage.services.get(i11)).info.enabled)
                            i10++;
                    localPackageInfo.services = new ServiceInfo[i10];
                    break;
                    label759: int i3;
                    int i6;
                    int i7;
                    label804: int i8;
                    if ((paramInt1 & 0x8) != 0)
                    {
                        i3 = paramPackage.providers.size();
                        if (i3 > 0)
                            if ((paramInt1 & 0x200) != 0)
                            {
                                localPackageInfo.providers = new ProviderInfo[i3];
                                i6 = 0;
                                i7 = 0;
                                if (i6 >= i3)
                                    break label946;
                                if ((!((Provider)paramPackage.providers.get(i6)).info.enabled) && ((paramInt1 & 0x200) == 0))
                                    break label1294;
                                ProviderInfo[] arrayOfProviderInfo = localPackageInfo.providers;
                                i8 = i7 + 1;
                                arrayOfProviderInfo[i7] = generateProviderInfo((Provider)paramPackage.providers.get(i6), paramInt1, paramBoolean, paramInt2, paramInt3);
                            }
                    }
                    while (true)
                    {
                        i6++;
                        i7 = i8;
                        break label804;
                        int i4 = 0;
                        for (int i5 = 0; i5 < i3; i5++)
                            if (((Provider)paramPackage.providers.get(i5)).info.enabled)
                                i4++;
                        localPackageInfo.providers = new ProviderInfo[i4];
                        break;
                        label946: if ((paramInt1 & 0x10) != 0)
                        {
                            int i1 = paramPackage.instrumentation.size();
                            if (i1 > 0)
                            {
                                localPackageInfo.instrumentation = new InstrumentationInfo[i1];
                                for (int i2 = 0; i2 < i1; i2++)
                                    localPackageInfo.instrumentation[i2] = generateInstrumentationInfo((Instrumentation)paramPackage.instrumentation.get(i2), paramInt1);
                            }
                        }
                        if ((paramInt1 & 0x1000) != 0)
                        {
                            int j = paramPackage.permissions.size();
                            if (j > 0)
                            {
                                localPackageInfo.permissions = new PermissionInfo[j];
                                for (int n = 0; n < j; n++)
                                    localPackageInfo.permissions[n] = generatePermissionInfo((Permission)paramPackage.permissions.get(n), paramInt1);
                            }
                            int k = paramPackage.requestedPermissions.size();
                            if (k > 0)
                            {
                                localPackageInfo.requestedPermissions = new String[k];
                                localPackageInfo.requestedPermissionsFlags = new int[k];
                                for (int m = 0; m < k; m++)
                                {
                                    String str = (String)paramPackage.requestedPermissions.get(m);
                                    localPackageInfo.requestedPermissions[m] = str;
                                    if (((Boolean)paramPackage.requestedPermissionsRequired.get(m)).booleanValue())
                                    {
                                        int[] arrayOfInt2 = localPackageInfo.requestedPermissionsFlags;
                                        arrayOfInt2[m] = (0x1 | arrayOfInt2[m]);
                                    }
                                    if ((paramHashSet != null) && (paramHashSet.contains(str)))
                                    {
                                        int[] arrayOfInt1 = localPackageInfo.requestedPermissionsFlags;
                                        arrayOfInt1[m] = (0x2 | arrayOfInt1[m]);
                                    }
                                }
                            }
                        }
                        if ((paramInt1 & 0x40) != 0)
                            if (paramPackage.mSignatures == null)
                                break label1288;
                        label1288: for (int i = paramPackage.mSignatures.length; ; i = 0)
                        {
                            if (i > 0)
                            {
                                localPackageInfo.signatures = new Signature[i];
                                System.arraycopy(paramPackage.mSignatures, 0, localPackageInfo.signatures, 0, i);
                            }
                            return localPackageInfo;
                        }
                        label1294: i8 = i7;
                    }
                    label1301: i14 = i13;
                }
                label1308: i20 = i19;
            }
            label1315: i26 = i25;
        }
    }

    public static final PermissionGroupInfo generatePermissionGroupInfo(PermissionGroup paramPermissionGroup, int paramInt)
    {
        PermissionGroupInfo localPermissionGroupInfo;
        if (paramPermissionGroup == null)
            localPermissionGroupInfo = null;
        while (true)
        {
            return localPermissionGroupInfo;
            if ((paramInt & 0x80) == 0)
            {
                localPermissionGroupInfo = paramPermissionGroup.info;
            }
            else
            {
                localPermissionGroupInfo = new PermissionGroupInfo(paramPermissionGroup.info);
                localPermissionGroupInfo.metaData = paramPermissionGroup.metaData;
            }
        }
    }

    public static final PermissionInfo generatePermissionInfo(Permission paramPermission, int paramInt)
    {
        PermissionInfo localPermissionInfo;
        if (paramPermission == null)
            localPermissionInfo = null;
        while (true)
        {
            return localPermissionInfo;
            if ((paramInt & 0x80) == 0)
            {
                localPermissionInfo = paramPermission.info;
            }
            else
            {
                localPermissionInfo = new PermissionInfo(paramPermission.info);
                localPermissionInfo.metaData = paramPermission.metaData;
            }
        }
    }

    public static final ProviderInfo generateProviderInfo(Provider paramProvider, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
    {
        ProviderInfo localProviderInfo;
        if (paramProvider == null)
            localProviderInfo = null;
        while (true)
        {
            return localProviderInfo;
            if ((!copyNeeded(paramInt1, paramProvider.owner, paramInt2, paramProvider.metaData)) && (((paramInt1 & 0x800) != 0) || (paramProvider.info.uriPermissionPatterns == null)) && (paramInt3 == 0))
            {
                localProviderInfo = paramProvider.info;
            }
            else
            {
                localProviderInfo = new ProviderInfo(paramProvider.info);
                localProviderInfo.metaData = paramProvider.metaData;
                if ((paramInt1 & 0x800) == 0)
                    localProviderInfo.uriPermissionPatterns = null;
                localProviderInfo.applicationInfo = generateApplicationInfo(paramProvider.owner, paramInt1, paramBoolean, paramInt2, paramInt3);
            }
        }
    }

    public static final ServiceInfo generateServiceInfo(Service paramService, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
    {
        ServiceInfo localServiceInfo;
        if (paramService == null)
            localServiceInfo = null;
        while (true)
        {
            return localServiceInfo;
            if ((!copyNeeded(paramInt1, paramService.owner, paramInt2, paramService.metaData)) && (paramInt3 == UserId.getUserId(paramService.info.applicationInfo.uid)))
            {
                localServiceInfo = paramService.info;
            }
            else
            {
                localServiceInfo = new ServiceInfo(paramService.info);
                localServiceInfo.metaData = paramService.metaData;
                localServiceInfo.applicationInfo = generateApplicationInfo(paramService.owner, paramInt1, paramBoolean, paramInt2, paramInt3);
            }
        }
    }

    private static final boolean isPackageFilename(String paramString)
    {
        return paramString.endsWith(".apk");
    }

    private Certificate[] loadCertificates(JarFile paramJarFile, JarEntry paramJarEntry, byte[] paramArrayOfByte)
    {
        Object localObject = null;
        try
        {
            BufferedInputStream localBufferedInputStream = new BufferedInputStream(paramJarFile.getInputStream(paramJarEntry));
            while (localBufferedInputStream.read(paramArrayOfByte, 0, paramArrayOfByte.length) != -1);
            localBufferedInputStream.close();
            if (paramJarEntry != null)
            {
                Certificate[] arrayOfCertificate = paramJarEntry.getCertificates();
                localObject = arrayOfCertificate;
            }
            return localObject;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.w("PackageParser", "Exception reading " + paramJarEntry.getName() + " in " + paramJarFile.getName(), localIOException);
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Slog.w("PackageParser", "Exception reading " + paramJarEntry.getName() + " in " + paramJarFile.getName(), localRuntimeException);
        }
    }

    private Activity parseActivity(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString, boolean paramBoolean1, boolean paramBoolean2)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestActivity);
        if (this.mParseActivityArgs == null)
            this.mParseActivityArgs = new ParseComponentArgs(paramPackage, paramArrayOfString, 3, 1, 2, 23, this.mSeparateProcesses, 7, 17, 5);
        ParseComponentArgs localParseComponentArgs = this.mParseActivityArgs;
        String str1;
        Activity localActivity;
        if (paramBoolean1)
        {
            str1 = "<receiver>";
            localParseComponentArgs.tag = str1;
            this.mParseActivityArgs.sa = localTypedArray;
            this.mParseActivityArgs.flags = paramInt;
            localActivity = new Activity(this.mParseActivityArgs, new ActivityInfo());
            if (paramArrayOfString[0] == null)
                break label133;
            localTypedArray.recycle();
            localActivity = null;
        }
        label133: boolean bool1;
        label253: 
        do
        {
            while (true)
            {
                return localActivity;
                str1 = "<activity>";
                break;
                bool1 = localTypedArray.hasValue(6);
                if (bool1)
                    localActivity.info.exported = localTypedArray.getBoolean(6, false);
                localActivity.info.theme = localTypedArray.getResourceId(0, 0);
                localActivity.info.uiOptions = localTypedArray.getInt(26, localActivity.info.applicationInfo.uiOptions);
                String str2 = localTypedArray.getNonConfigurationString(27, 0);
                String str3;
                boolean bool2;
                if (str2 != null)
                {
                    String str6 = buildClassName(localActivity.info.packageName, str2, paramArrayOfString);
                    if (paramArrayOfString[0] == null)
                        localActivity.info.parentActivityName = str6;
                }
                else
                {
                    str3 = localTypedArray.getNonConfigurationString(4, 0);
                    if (str3 != null)
                        break label871;
                    localActivity.info.permission = paramPackage.applicationInfo.permission;
                    String str5 = localTypedArray.getNonConfigurationString(8, 0);
                    localActivity.info.taskAffinity = buildTaskAffinityName(paramPackage.applicationInfo.packageName, paramPackage.applicationInfo.taskAffinity, str5, paramArrayOfString);
                    localActivity.info.flags = 0;
                    if (localTypedArray.getBoolean(9, false))
                    {
                        ActivityInfo localActivityInfo13 = localActivity.info;
                        localActivityInfo13.flags = (0x1 | localActivityInfo13.flags);
                    }
                    if (localTypedArray.getBoolean(10, false))
                    {
                        ActivityInfo localActivityInfo12 = localActivity.info;
                        localActivityInfo12.flags = (0x2 | localActivityInfo12.flags);
                    }
                    if (localTypedArray.getBoolean(11, false))
                    {
                        ActivityInfo localActivityInfo11 = localActivity.info;
                        localActivityInfo11.flags = (0x4 | localActivityInfo11.flags);
                    }
                    if (localTypedArray.getBoolean(21, false))
                    {
                        ActivityInfo localActivityInfo10 = localActivity.info;
                        localActivityInfo10.flags = (0x80 | localActivityInfo10.flags);
                    }
                    if (localTypedArray.getBoolean(18, false))
                    {
                        ActivityInfo localActivityInfo9 = localActivity.info;
                        localActivityInfo9.flags = (0x8 | localActivityInfo9.flags);
                    }
                    if (localTypedArray.getBoolean(12, false))
                    {
                        ActivityInfo localActivityInfo8 = localActivity.info;
                        localActivityInfo8.flags = (0x10 | localActivityInfo8.flags);
                    }
                    if (localTypedArray.getBoolean(13, false))
                    {
                        ActivityInfo localActivityInfo7 = localActivity.info;
                        localActivityInfo7.flags = (0x20 | localActivityInfo7.flags);
                    }
                    if ((0x20 & paramPackage.applicationInfo.flags) == 0)
                        break label912;
                    bool2 = true;
                    if (localTypedArray.getBoolean(19, bool2))
                    {
                        ActivityInfo localActivityInfo6 = localActivity.info;
                        localActivityInfo6.flags = (0x40 | localActivityInfo6.flags);
                    }
                    if (localTypedArray.getBoolean(22, false))
                    {
                        ActivityInfo localActivityInfo5 = localActivity.info;
                        localActivityInfo5.flags = (0x100 | localActivityInfo5.flags);
                    }
                    if (localTypedArray.getBoolean(24, false))
                    {
                        ActivityInfo localActivityInfo4 = localActivity.info;
                        localActivityInfo4.flags = (0x400 | localActivityInfo4.flags);
                    }
                    if (paramBoolean1)
                        break label918;
                    if (localTypedArray.getBoolean(25, paramBoolean2))
                    {
                        ActivityInfo localActivityInfo3 = localActivity.info;
                        localActivityInfo3.flags = (0x200 | localActivityInfo3.flags);
                    }
                    localActivity.info.launchMode = localTypedArray.getInt(14, 0);
                    localActivity.info.screenOrientation = localTypedArray.getInt(15, -1);
                    localActivity.info.configChanges = localTypedArray.getInt(16, 0);
                    localActivity.info.softInputMode = localTypedArray.getInt(20, 0);
                }
                while (true)
                {
                    localTypedArray.recycle();
                    if ((paramBoolean1) && ((0x10000000 & paramPackage.applicationInfo.flags) != 0) && (localActivity.info.processName == paramPackage.packageName))
                        paramArrayOfString[0] = "Heavy-weight applications can not have receivers in main process";
                    if (paramArrayOfString[0] == null)
                        break label939;
                    localActivity = null;
                    break;
                    Log.e("PackageParser", "Activity " + localActivity.info.name + " specified invalid parentActivityName " + str2);
                    paramArrayOfString[0] = null;
                    break label253;
                    ActivityInfo localActivityInfo1 = localActivity.info;
                    if (str3.length() > 0);
                    for (String str4 = str3.toString().intern(); ; str4 = null)
                    {
                        localActivityInfo1.permission = str4;
                        break;
                    }
                    bool2 = false;
                    break label561;
                    localActivity.info.launchMode = 0;
                    localActivity.info.configChanges = 0;
                }
                int i = paramXmlPullParser.getDepth();
                Bundle localBundle;
                do
                {
                    while (true)
                    {
                        int j = paramXmlPullParser.next();
                        if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                            break label1342;
                        if ((j != 3) && (j != 4))
                        {
                            if (!paramXmlPullParser.getName().equals("intent-filter"))
                                break label1124;
                            ActivityIntentInfo localActivityIntentInfo = new ActivityIntentInfo(localActivity);
                            if (!paramBoolean1);
                            for (boolean bool4 = true; ; bool4 = false)
                            {
                                if (parseIntent(paramResources, paramXmlPullParser, paramAttributeSet, paramInt, localActivityIntentInfo, paramArrayOfString, bool4))
                                    break label1055;
                                localActivity = null;
                                break;
                            }
                            if (localActivityIntentInfo.countActions() == 0)
                                Slog.w("PackageParser", "No actions in intent filter at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                            else
                                localActivity.intents.add(localActivityIntentInfo);
                        }
                    }
                    if (!paramXmlPullParser.getName().equals("meta-data"))
                        break label1174;
                    localBundle = parseMetaData(paramResources, paramXmlPullParser, paramAttributeSet, localActivity.metaData, paramArrayOfString);
                    localActivity.metaData = localBundle;
                }
                while (localBundle != null);
                localActivity = null;
            }
            Slog.w("PackageParser", "Problem in package " + this.mArchiveSourcePath + ":");
            if (paramBoolean1)
                Slog.w("PackageParser", "Unknown element under <receiver>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
            while (true)
            {
                XmlUtils.skipCurrentTag(paramXmlPullParser);
                break;
                Slog.w("PackageParser", "Unknown element under <activity>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
            }
        }
        while (bool1);
        label561: label871: ActivityInfo localActivityInfo2 = localActivity.info;
        label912: label918: label939: label1124: if (localActivity.intents.size() > 0);
        label1055: label1342: for (boolean bool3 = true; ; bool3 = false)
        {
            localActivityInfo2.exported = bool3;
            break;
        }
    }

    private Activity parseActivityAlias(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestActivityAlias);
        String str1 = localTypedArray.getNonConfigurationString(7, 0);
        Activity localActivity1;
        if (str1 == null)
        {
            paramArrayOfString[0] = "<activity-alias> does not specify android:targetActivity";
            localTypedArray.recycle();
            localActivity1 = null;
        }
        label247: boolean bool1;
        label566: label698: label984: 
        do
        {
            while (true)
            {
                return localActivity1;
                String str2 = buildClassName(paramPackage.applicationInfo.packageName, str1, paramArrayOfString);
                if (str2 == null)
                {
                    localTypedArray.recycle();
                    localActivity1 = null;
                }
                else
                {
                    if (this.mParseActivityAliasArgs == null)
                    {
                        this.mParseActivityAliasArgs = new ParseComponentArgs(paramPackage, paramArrayOfString, 2, 0, 1, 8, this.mSeparateProcesses, 0, 6, 4);
                        this.mParseActivityAliasArgs.tag = "<activity-alias>";
                    }
                    this.mParseActivityAliasArgs.sa = localTypedArray;
                    this.mParseActivityAliasArgs.flags = paramInt;
                    Object localObject = null;
                    int i = paramPackage.activities.size();
                    for (int j = 0; ; j++)
                        if (j < i)
                        {
                            Activity localActivity2 = (Activity)paramPackage.activities.get(j);
                            if (str2.equals(localActivity2.info.name))
                                localObject = localActivity2;
                        }
                        else
                        {
                            if (localObject != null)
                                break label247;
                            paramArrayOfString[0] = ("<activity-alias> target activity " + str2 + " not found in manifest");
                            localTypedArray.recycle();
                            localActivity1 = null;
                            break;
                        }
                    ActivityInfo localActivityInfo1 = new ActivityInfo();
                    localActivityInfo1.targetActivity = str2;
                    localActivityInfo1.configChanges = localObject.info.configChanges;
                    localActivityInfo1.flags = localObject.info.flags;
                    localActivityInfo1.icon = localObject.info.icon;
                    localActivityInfo1.logo = localObject.info.logo;
                    localActivityInfo1.labelRes = localObject.info.labelRes;
                    localActivityInfo1.nonLocalizedLabel = localObject.info.nonLocalizedLabel;
                    localActivityInfo1.launchMode = localObject.info.launchMode;
                    localActivityInfo1.processName = localObject.info.processName;
                    if (localActivityInfo1.descriptionRes == 0)
                        localActivityInfo1.descriptionRes = localObject.info.descriptionRes;
                    localActivityInfo1.screenOrientation = localObject.info.screenOrientation;
                    localActivityInfo1.taskAffinity = localObject.info.taskAffinity;
                    localActivityInfo1.theme = localObject.info.theme;
                    localActivityInfo1.softInputMode = localObject.info.softInputMode;
                    localActivityInfo1.uiOptions = localObject.info.uiOptions;
                    localActivityInfo1.parentActivityName = localObject.info.parentActivityName;
                    localActivity1 = new Activity(this.mParseActivityAliasArgs, localActivityInfo1);
                    if (paramArrayOfString[0] == null)
                        break;
                    localTypedArray.recycle();
                    localActivity1 = null;
                }
            }
            bool1 = localTypedArray.hasValue(5);
            if (bool1)
                localActivity1.info.exported = localTypedArray.getBoolean(5, false);
            String str3 = localTypedArray.getNonConfigurationString(3, 0);
            String str6;
            String str4;
            if (str3 != null)
            {
                ActivityInfo localActivityInfo3 = localActivity1.info;
                if (str3.length() > 0)
                {
                    str6 = str3.toString().intern();
                    localActivityInfo3.permission = str6;
                }
            }
            else
            {
                str4 = localTypedArray.getNonConfigurationString(9, 0);
                if (str4 != null)
                {
                    String str5 = buildClassName(localActivity1.info.packageName, str4, paramArrayOfString);
                    if (paramArrayOfString[0] != null)
                        break label646;
                    localActivity1.info.parentActivityName = str5;
                }
            }
            while (true)
            {
                localTypedArray.recycle();
                if (paramArrayOfString[0] == null)
                    break label698;
                localActivity1 = null;
                break;
                str6 = null;
                break label566;
                Log.e("PackageParser", "Activity alias " + localActivity1.info.name + " specified invalid parentActivityName " + str4);
                paramArrayOfString[0] = null;
            }
            int k = paramXmlPullParser.getDepth();
            while (true)
            {
                int m = paramXmlPullParser.next();
                if ((m == 1) || ((m == 3) && (paramXmlPullParser.getDepth() <= k)))
                    break label984;
                if ((m != 3) && (m != 4))
                {
                    if (paramXmlPullParser.getName().equals("intent-filter"))
                    {
                        ActivityIntentInfo localActivityIntentInfo = new ActivityIntentInfo(localActivity1);
                        if (!parseIntent(paramResources, paramXmlPullParser, paramAttributeSet, paramInt, localActivityIntentInfo, paramArrayOfString, true))
                        {
                            localActivity1 = null;
                            break;
                        }
                        if (localActivityIntentInfo.countActions() == 0)
                        {
                            Slog.w("PackageParser", "No actions in intent filter at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                            continue;
                        }
                        localActivity1.intents.add(localActivityIntentInfo);
                        continue;
                    }
                    if (paramXmlPullParser.getName().equals("meta-data"))
                    {
                        Bundle localBundle = parseMetaData(paramResources, paramXmlPullParser, paramAttributeSet, localActivity1.metaData, paramArrayOfString);
                        localActivity1.metaData = localBundle;
                        if (localBundle != null)
                            continue;
                        localActivity1 = null;
                        break;
                    }
                    Slog.w("PackageParser", "Unknown element under <activity-alias>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
            }
        }
        while (bool1);
        label646: ActivityInfo localActivityInfo2 = localActivity1.info;
        if (localActivity1.intents.size() > 0);
        for (boolean bool2 = true; ; bool2 = false)
        {
            localActivityInfo2.exported = bool2;
            break;
        }
    }

    private boolean parseAllMetaData(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, String paramString, Component paramComponent, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        Bundle localBundle;
        do
        {
            int j;
            do
            {
                j = paramXmlPullParser.next();
                if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                    break;
            }
            while ((j == 3) || (j == 4));
            if (!paramXmlPullParser.getName().equals("meta-data"))
                break;
            localBundle = parseMetaData(paramResources, paramXmlPullParser, paramAttributeSet, paramComponent.metaData, paramArrayOfString);
            paramComponent.metaData = localBundle;
        }
        while (localBundle != null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            Slog.w("PackageParser", "Unknown element under " + paramString + ": " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
        }
    }

    private boolean parseApplication(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        ApplicationInfo localApplicationInfo = paramPackage.applicationInfo;
        String str1 = paramPackage.applicationInfo.packageName;
        TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestApplication);
        String str2 = localTypedArray1.getNonConfigurationString(3, 0);
        boolean bool3;
        if (str2 != null)
        {
            localApplicationInfo.className = buildClassName(str1, str2, paramArrayOfString);
            if (localApplicationInfo.className == null)
            {
                localTypedArray1.recycle();
                this.mParseError = -108;
                bool3 = false;
            }
        }
        while (true)
        {
            return bool3;
            String str3 = localTypedArray1.getNonConfigurationString(4, 0);
            if (str3 != null)
                localApplicationInfo.manageSpaceActivityName = buildClassName(str1, str3, paramArrayOfString);
            if (localTypedArray1.getBoolean(17, true))
            {
                localApplicationInfo.flags = (0x8000 | localApplicationInfo.flags);
                String str10 = localTypedArray1.getNonConfigurationString(16, 0);
                if (str10 != null)
                {
                    localApplicationInfo.backupAgentName = buildClassName(str1, str10, paramArrayOfString);
                    if (localTypedArray1.getBoolean(18, true))
                        localApplicationInfo.flags = (0x10000 | localApplicationInfo.flags);
                    if (localTypedArray1.getBoolean(21, false))
                        localApplicationInfo.flags = (0x20000 | localApplicationInfo.flags);
                }
            }
            TypedValue localTypedValue = localTypedArray1.peekValue(1);
            if (localTypedValue != null)
            {
                int k = localTypedValue.resourceId;
                localApplicationInfo.labelRes = k;
                if (k == 0)
                    localApplicationInfo.nonLocalizedLabel = localTypedValue.coerceToString();
            }
            localApplicationInfo.icon = localTypedArray1.getResourceId(2, 0);
            localApplicationInfo.logo = localTypedArray1.getResourceId(22, 0);
            localApplicationInfo.theme = localTypedArray1.getResourceId(0, 0);
            localApplicationInfo.descriptionRes = localTypedArray1.getResourceId(13, 0);
            if (((paramInt & 0x1) != 0) && (localTypedArray1.getBoolean(8, false)))
                localApplicationInfo.flags = (0x8 | localApplicationInfo.flags);
            if (localTypedArray1.getBoolean(10, false))
                localApplicationInfo.flags = (0x2 | localApplicationInfo.flags);
            if (localTypedArray1.getBoolean(20, false))
                localApplicationInfo.flags = (0x4000 | localApplicationInfo.flags);
            boolean bool1;
            label397: boolean bool2;
            String str5;
            label583: String str6;
            if (paramPackage.applicationInfo.targetSdkVersion >= 14)
            {
                bool1 = true;
                bool2 = localTypedArray1.getBoolean(23, bool1);
                if (localTypedArray1.getBoolean(7, true))
                    localApplicationInfo.flags = (0x4 | localApplicationInfo.flags);
                if (localTypedArray1.getBoolean(14, false))
                    localApplicationInfo.flags = (0x20 | localApplicationInfo.flags);
                if (localTypedArray1.getBoolean(5, true))
                    localApplicationInfo.flags = (0x40 | localApplicationInfo.flags);
                if (localTypedArray1.getBoolean(15, false))
                    localApplicationInfo.flags = (0x100 | localApplicationInfo.flags);
                if (localTypedArray1.getBoolean(24, false))
                    localApplicationInfo.flags = (0x100000 | localApplicationInfo.flags);
                if (localTypedArray1.getBoolean(28, false))
                    localApplicationInfo.flags = (0x400000 | localApplicationInfo.flags);
                String str4 = localTypedArray1.getNonConfigurationString(6, 0);
                if ((str4 == null) || (str4.length() <= 0))
                    break label743;
                str5 = str4.intern();
                localApplicationInfo.permission = str5;
                if (paramPackage.applicationInfo.targetSdkVersion < 8)
                    break label749;
                str6 = localTypedArray1.getNonConfigurationString(12, 0);
                label612: localApplicationInfo.taskAffinity = buildTaskAffinityName(localApplicationInfo.packageName, localApplicationInfo.packageName, str6, paramArrayOfString);
                if (paramArrayOfString[0] == null)
                    if (paramPackage.applicationInfo.targetSdkVersion < 8)
                        break label761;
            }
            label743: label749: label761: for (String str9 = localTypedArray1.getNonConfigurationString(11, 0); ; str9 = localTypedArray1.getNonResourceString(11))
            {
                localApplicationInfo.processName = buildProcessName(localApplicationInfo.packageName, null, str9, paramInt, this.mSeparateProcesses, paramArrayOfString);
                localApplicationInfo.enabled = localTypedArray1.getBoolean(9, true);
                localApplicationInfo.uiOptions = localTypedArray1.getInt(25, 0);
                localTypedArray1.recycle();
                if (paramArrayOfString[0] == null)
                    break label773;
                this.mParseError = -108;
                bool3 = false;
                break;
                bool1 = false;
                break label397;
                str5 = null;
                break label583;
                str6 = localTypedArray1.getNonResourceString(12);
                break label612;
            }
            label773: int i = paramXmlPullParser.getDepth();
            while (true)
            {
                int j = paramXmlPullParser.next();
                if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                    break label1401;
                if ((j != 3) && (j != 4))
                {
                    String str7 = paramXmlPullParser.getName();
                    if (str7.equals("activity"))
                    {
                        Activity localActivity3 = parseActivity(paramPackage, paramResources, paramXmlPullParser, paramAttributeSet, paramInt, paramArrayOfString, false, bool2);
                        if (localActivity3 == null)
                        {
                            this.mParseError = -108;
                            bool3 = false;
                            break;
                        }
                        paramPackage.activities.add(localActivity3);
                        continue;
                    }
                    if (str7.equals("receiver"))
                    {
                        Activity localActivity2 = parseActivity(paramPackage, paramResources, paramXmlPullParser, paramAttributeSet, paramInt, paramArrayOfString, true, false);
                        if (localActivity2 == null)
                        {
                            this.mParseError = -108;
                            bool3 = false;
                            break;
                        }
                        paramPackage.receivers.add(localActivity2);
                        continue;
                    }
                    if (str7.equals("service"))
                    {
                        Service localService = parseService(paramPackage, paramResources, paramXmlPullParser, paramAttributeSet, paramInt, paramArrayOfString);
                        if (localService == null)
                        {
                            this.mParseError = -108;
                            bool3 = false;
                            break;
                        }
                        paramPackage.services.add(localService);
                        continue;
                    }
                    if (str7.equals("provider"))
                    {
                        Provider localProvider = parseProvider(paramPackage, paramResources, paramXmlPullParser, paramAttributeSet, paramInt, paramArrayOfString);
                        if (localProvider == null)
                        {
                            this.mParseError = -108;
                            bool3 = false;
                            break;
                        }
                        paramPackage.providers.add(localProvider);
                        continue;
                    }
                    if (str7.equals("activity-alias"))
                    {
                        Activity localActivity1 = parseActivityAlias(paramPackage, paramResources, paramXmlPullParser, paramAttributeSet, paramInt, paramArrayOfString);
                        if (localActivity1 == null)
                        {
                            this.mParseError = -108;
                            bool3 = false;
                            break;
                        }
                        paramPackage.activities.add(localActivity1);
                        continue;
                    }
                    if (paramXmlPullParser.getName().equals("meta-data"))
                    {
                        Bundle localBundle = parseMetaData(paramResources, paramXmlPullParser, paramAttributeSet, paramPackage.mAppMetaData, paramArrayOfString);
                        paramPackage.mAppMetaData = localBundle;
                        if (localBundle != null)
                            continue;
                        this.mParseError = -108;
                        bool3 = false;
                        break;
                    }
                    if (str7.equals("uses-library"))
                    {
                        TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestUsesLibrary);
                        String str8 = localTypedArray2.getNonResourceString(0);
                        boolean bool4 = localTypedArray2.getBoolean(1, true);
                        localTypedArray2.recycle();
                        if (str8 != null)
                        {
                            if (!bool4)
                                break label1275;
                            if (paramPackage.usesLibraries == null)
                                paramPackage.usesLibraries = new ArrayList();
                            if (!paramPackage.usesLibraries.contains(str8))
                                paramPackage.usesLibraries.add(str8.intern());
                        }
                        while (true)
                        {
                            XmlUtils.skipCurrentTag(paramXmlPullParser);
                            break;
                            label1275: if (paramPackage.usesOptionalLibraries == null)
                                paramPackage.usesOptionalLibraries = new ArrayList();
                            if (!paramPackage.usesOptionalLibraries.contains(str8))
                                paramPackage.usesOptionalLibraries.add(str8.intern());
                        }
                    }
                    if (str7.equals("uses-package"))
                    {
                        XmlUtils.skipCurrentTag(paramXmlPullParser);
                    }
                    else
                    {
                        Slog.w("PackageParser", "Unknown element under <application>: " + str7 + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                        XmlUtils.skipCurrentTag(paramXmlPullParser);
                    }
                }
            }
            label1401: bool3 = true;
        }
    }

    private Instrumentation parseInstrumentation(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestInstrumentation);
        if (this.mParseInstrumentationArgs == null)
        {
            this.mParseInstrumentationArgs = new ParsePackageItemArgs(paramPackage, paramArrayOfString, 2, 0, 1, 6);
            this.mParseInstrumentationArgs.tag = "<instrumentation>";
        }
        this.mParseInstrumentationArgs.sa = localTypedArray;
        Instrumentation localInstrumentation = new Instrumentation(this.mParseInstrumentationArgs, new InstrumentationInfo());
        if (paramArrayOfString[0] != null)
        {
            localTypedArray.recycle();
            this.mParseError = -108;
            localInstrumentation = null;
        }
        while (true)
        {
            return localInstrumentation;
            String str1 = localTypedArray.getNonResourceString(3);
            InstrumentationInfo localInstrumentationInfo = localInstrumentation.info;
            if (str1 != null);
            for (String str2 = str1.intern(); ; str2 = null)
            {
                localInstrumentationInfo.targetPackage = str2;
                localInstrumentation.info.handleProfiling = localTypedArray.getBoolean(4, false);
                localInstrumentation.info.functionalTest = localTypedArray.getBoolean(5, false);
                localTypedArray.recycle();
                if (localInstrumentation.info.targetPackage != null)
                    break label205;
                paramArrayOfString[0] = "<instrumentation> does not specify targetPackage";
                this.mParseError = -108;
                localInstrumentation = null;
                break;
            }
            label205: if (!parseAllMetaData(paramResources, paramXmlPullParser, paramAttributeSet, "<instrumentation>", localInstrumentation, paramArrayOfString))
            {
                this.mParseError = -108;
                localInstrumentation = null;
            }
            else
            {
                paramPackage.instrumentation.add(localInstrumentation);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private boolean parseIntent(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, IntentInfo paramIntentInfo, String[] paramArrayOfString, boolean paramBoolean)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestIntentFilter);
        paramIntentInfo.setPriority(Injector.checkPriority(paramInt, localTypedArray1.getInt(2, 0)));
        TypedValue localTypedValue = localTypedArray1.peekValue(0);
        if (localTypedValue != null)
        {
            int k = localTypedValue.resourceId;
            paramIntentInfo.labelRes = k;
            if (k == 0)
                paramIntentInfo.nonLocalizedLabel = localTypedValue.coerceToString();
        }
        paramIntentInfo.icon = localTypedArray1.getResourceId(1, 0);
        paramIntentInfo.logo = localTypedArray1.getResourceId(3, 0);
        localTypedArray1.recycle();
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        String str1 = paramXmlPullParser.getName();
        String str10;
        boolean bool;
        if (str1.equals("action"))
        {
            str10 = paramAttributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
            if ((str10 == null) || (str10 == ""))
            {
                paramArrayOfString[0] = "No value supplied for <android:name>";
                bool = false;
            }
        }
        while (true)
        {
            while (true)
            {
                return bool;
                XmlUtils.skipCurrentTag(paramXmlPullParser);
                paramIntentInfo.addAction(str10);
                break;
                if (str1.equals("category"))
                {
                    String str9 = paramAttributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
                    if ((str9 == null) || (str9 == ""))
                    {
                        paramArrayOfString[0] = "No value supplied for <android:name>";
                        bool = false;
                        continue;
                    }
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                    paramIntentInfo.addCategory(str9);
                    break;
                }
                if (!str1.equals("data"))
                    break label481;
                TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestData);
                String str2 = localTypedArray2.getNonConfigurationString(0, 0);
                if (str2 != null);
                try
                {
                    paramIntentInfo.addDataType(str2);
                    String str3 = localTypedArray2.getNonConfigurationString(1, 0);
                    if (str3 != null)
                        paramIntentInfo.addDataScheme(str3);
                    String str4 = localTypedArray2.getNonConfigurationString(2, 0);
                    String str5 = localTypedArray2.getNonConfigurationString(3, 0);
                    if (str4 != null)
                        paramIntentInfo.addDataAuthority(str4, str5);
                    String str6 = localTypedArray2.getNonConfigurationString(4, 0);
                    if (str6 != null)
                        paramIntentInfo.addDataPath(str6, 0);
                    String str7 = localTypedArray2.getNonConfigurationString(5, 0);
                    if (str7 != null)
                        paramIntentInfo.addDataPath(str7, 1);
                    String str8 = localTypedArray2.getNonConfigurationString(6, 0);
                    if (str8 != null)
                        paramIntentInfo.addDataPath(str8, 2);
                    localTypedArray2.recycle();
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
                catch (IntentFilter.MalformedMimeTypeException localMalformedMimeTypeException)
                {
                    paramArrayOfString[0] = localMalformedMimeTypeException.toString();
                    localTypedArray2.recycle();
                    bool = false;
                }
            }
            continue;
            label481: Slog.w("PackageParser", "Unknown element under <intent-filter>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            paramIntentInfo.hasDefault = paramIntentInfo.hasCategory("android.intent.category.DEFAULT");
            bool = true;
        }
    }

    private Bundle parseMetaData(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Bundle paramBundle, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        Object localObject = null;
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestMetaData);
        if (paramBundle == null)
            paramBundle = new Bundle();
        String str1 = localTypedArray.getNonConfigurationString(0, 0);
        if (str1 == null)
        {
            paramArrayOfString[0] = "<meta-data> requires an android:name attribute";
            localTypedArray.recycle();
            return localObject;
        }
        String str2 = str1.intern();
        TypedValue localTypedValue1 = localTypedArray.peekValue(2);
        if ((localTypedValue1 != null) && (localTypedValue1.resourceId != 0))
            paramBundle.putInt(str2, localTypedValue1.resourceId);
        while (true)
        {
            localTypedArray.recycle();
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            localObject = paramBundle;
            break;
            TypedValue localTypedValue2 = localTypedArray.peekValue(1);
            if (localTypedValue2 != null)
            {
                if (localTypedValue2.type == 3)
                {
                    CharSequence localCharSequence = localTypedValue2.coerceToString();
                    if (localCharSequence != null)
                        localObject = localCharSequence.toString().intern();
                    paramBundle.putString(str2, (String)localObject);
                }
                else
                {
                    if (localTypedValue2.type == 18)
                    {
                        if (localTypedValue2.data != 0);
                        for (boolean bool = true; ; bool = false)
                        {
                            paramBundle.putBoolean(str2, bool);
                            break;
                        }
                    }
                    if ((localTypedValue2.type >= 16) && (localTypedValue2.type <= 31))
                        paramBundle.putInt(str2, localTypedValue2.data);
                    else if (localTypedValue2.type == 4)
                        paramBundle.putFloat(str2, localTypedValue2.getFloat());
                    else
                        Slog.w("PackageParser", "<meta-data> only supports string, integer, float, color, boolean, and resource reference types: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                }
            }
            else
            {
                paramArrayOfString[0] = "<meta-data> requires an android:value or android:resource attribute";
                paramBundle = null;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private Package parsePackage(Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        this.mParseInstrumentationArgs = null;
        this.mParseActivityArgs = null;
        this.mParseServiceArgs = null;
        this.mParseProviderArgs = null;
        String str1 = parsePackageName(paramXmlResourceParser, paramXmlResourceParser, paramInt, paramArrayOfString);
        Package localPackage;
        if (str1 == null)
        {
            this.mParseError = -106;
            localPackage = null;
        }
        while (true)
        {
            return localPackage;
            if ((this.mOnlyCoreApps) && (!paramXmlResourceParser.getAttributeBooleanValue(null, "coreApp", false)))
            {
                this.mParseError = 1;
                localPackage = null;
            }
            else
            {
                localPackage = new Package(str1);
                int i = 0;
                TypedArray localTypedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifest);
                localPackage.mVersionCode = localTypedArray1.getInteger(1, 0);
                localPackage.mVersionName = localTypedArray1.getNonConfigurationString(2, 0);
                if (localPackage.mVersionName != null)
                    localPackage.mVersionName = localPackage.mVersionName.intern();
                String str2 = localTypedArray1.getNonConfigurationString(0, 0);
                if ((str2 != null) && (str2.length() > 0))
                {
                    String str15 = Injector.filterNameError(str1, validateName(str2, true));
                    if ((str15 != null) && (!"android".equals(str1)))
                    {
                        paramArrayOfString[0] = ("<manifest> specifies bad sharedUserId name \"" + str2 + "\": " + str15);
                        this.mParseError = -107;
                        localPackage = null;
                    }
                    else
                    {
                        localPackage.mSharedUserId = str2.intern();
                        localPackage.mSharedUserLabel = localTypedArray1.getResourceId(3, 0);
                    }
                }
                else
                {
                    localTypedArray1.recycle();
                    localPackage.installLocation = localTypedArray1.getInteger(4, -1);
                    localPackage.applicationInfo.installLocation = localPackage.installLocation;
                    if ((paramInt & 0x10) != 0)
                    {
                        ApplicationInfo localApplicationInfo8 = localPackage.applicationInfo;
                        localApplicationInfo8.flags = (0x20000000 | localApplicationInfo8.flags);
                    }
                    if ((paramInt & 0x20) != 0)
                    {
                        ApplicationInfo localApplicationInfo7 = localPackage.applicationInfo;
                        localApplicationInfo7.flags = (0x40000 | localApplicationInfo7.flags);
                    }
                    int j = 1;
                    int k = 1;
                    int m = 1;
                    int n = 1;
                    int i1 = 1;
                    int i2 = 1;
                    int i3 = paramXmlResourceParser.getDepth();
                    while (true)
                    {
                        int i4 = paramXmlResourceParser.next();
                        if ((i4 == 1) || ((i4 == 3) && (paramXmlResourceParser.getDepth() <= i3)))
                            break label2014;
                        if ((i4 != 3) && (i4 != 4))
                        {
                            String str6 = paramXmlResourceParser.getName();
                            if (str6.equals("application"))
                            {
                                if (i != 0)
                                {
                                    Slog.w("PackageParser", "<manifest> has more than one <application>");
                                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    continue;
                                }
                                i = 1;
                                if (parseApplication(localPackage, paramResources, paramXmlResourceParser, paramXmlResourceParser, paramInt, paramArrayOfString))
                                    continue;
                                localPackage = null;
                                break;
                            }
                            if (str6.equals("permission-group"))
                            {
                                if (parsePermissionGroup(localPackage, paramInt, paramResources, paramXmlResourceParser, paramXmlResourceParser, paramArrayOfString) != null)
                                    continue;
                                localPackage = null;
                                break;
                            }
                            if (str6.equals("permission"))
                            {
                                if (parsePermission(localPackage, paramResources, paramXmlResourceParser, paramXmlResourceParser, paramArrayOfString) != null)
                                    continue;
                                localPackage = null;
                                break;
                            }
                            if (str6.equals("permission-tree"))
                            {
                                if (parsePermissionTree(localPackage, paramResources, paramXmlResourceParser, paramXmlResourceParser, paramArrayOfString) != null)
                                    continue;
                                localPackage = null;
                                break;
                            }
                            if (str6.equals("uses-permission"))
                            {
                                TypedArray localTypedArray9 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesPermission);
                                String str14 = localTypedArray9.getNonResourceString(0);
                                localTypedArray9.recycle();
                                if ((str14 != null) && (!localPackage.requestedPermissions.contains(str14)))
                                {
                                    localPackage.requestedPermissions.add(str14.intern());
                                    localPackage.requestedPermissionsRequired.add(Boolean.TRUE);
                                }
                                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                            }
                            else if (str6.equals("uses-configuration"))
                            {
                                ConfigurationInfo localConfigurationInfo1 = new ConfigurationInfo();
                                TypedArray localTypedArray2 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesConfiguration);
                                localConfigurationInfo1.reqTouchScreen = localTypedArray2.getInt(0, 0);
                                localConfigurationInfo1.reqKeyboardType = localTypedArray2.getInt(1, 0);
                                if (localTypedArray2.getBoolean(2, false))
                                    localConfigurationInfo1.reqInputFeatures = (0x1 | localConfigurationInfo1.reqInputFeatures);
                                localConfigurationInfo1.reqNavigation = localTypedArray2.getInt(3, 0);
                                if (localTypedArray2.getBoolean(4, false))
                                    localConfigurationInfo1.reqInputFeatures = (0x2 | localConfigurationInfo1.reqInputFeatures);
                                localTypedArray2.recycle();
                                localPackage.configPreferences.add(localConfigurationInfo1);
                                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                            }
                            else if (str6.equals("uses-feature"))
                            {
                                FeatureInfo localFeatureInfo = new FeatureInfo();
                                TypedArray localTypedArray3 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesFeature);
                                localFeatureInfo.name = localTypedArray3.getNonResourceString(0);
                                if (localFeatureInfo.name == null)
                                    localFeatureInfo.reqGlEsVersion = localTypedArray3.getInt(1, 0);
                                if (localTypedArray3.getBoolean(2, true))
                                    localFeatureInfo.flags = (0x1 | localFeatureInfo.flags);
                                localTypedArray3.recycle();
                                if (localPackage.reqFeatures == null)
                                    localPackage.reqFeatures = new ArrayList();
                                localPackage.reqFeatures.add(localFeatureInfo);
                                if (localFeatureInfo.name == null)
                                {
                                    ConfigurationInfo localConfigurationInfo2 = new ConfigurationInfo();
                                    localConfigurationInfo2.reqGlEsVersion = localFeatureInfo.reqGlEsVersion;
                                    localPackage.configPreferences.add(localConfigurationInfo2);
                                }
                                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                            }
                            else
                            {
                                if (str6.equals("uses-sdk"))
                                {
                                    int i12;
                                    if (SDK_VERSION > 0)
                                    {
                                        TypedArray localTypedArray8 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesSdk);
                                        int i11 = 0;
                                        String str10 = null;
                                        i12 = 0;
                                        String str11 = null;
                                        TypedValue localTypedValue1 = localTypedArray8.peekValue(0);
                                        label1048: TypedValue localTypedValue2;
                                        if (localTypedValue1 != null)
                                        {
                                            if ((localTypedValue1.type == 3) && (localTypedValue1.string != null))
                                            {
                                                str10 = localTypedValue1.string.toString();
                                                str11 = str10;
                                            }
                                        }
                                        else
                                        {
                                            localTypedValue2 = localTypedArray8.peekValue(1);
                                            if (localTypedValue2 != null)
                                            {
                                                if ((localTypedValue2.type != 3) || (localTypedValue2.string == null))
                                                    break label1192;
                                                str10 = localTypedValue2.string.toString();
                                                str11 = str10;
                                            }
                                            label1092: localTypedArray8.recycle();
                                            if (str10 == null)
                                                break label1236;
                                            String str13 = SDK_CODENAME;
                                            if (str10.equals(str13))
                                                break label1303;
                                            if (SDK_CODENAME == null)
                                                break label1202;
                                            paramArrayOfString[0] = ("Requires development platform " + str10 + " (current platform is " + SDK_CODENAME + ")");
                                        }
                                        while (true)
                                        {
                                            this.mParseError = -12;
                                            localPackage = null;
                                            break;
                                            i11 = localTypedValue1.data;
                                            i12 = i11;
                                            break label1048;
                                            label1192: i12 = localTypedValue2.data;
                                            break label1092;
                                            label1202: paramArrayOfString[0] = ("Requires development platform " + str10 + " but this is a release platform.");
                                        }
                                        label1236: int i13 = SDK_VERSION;
                                        if (i11 > i13)
                                        {
                                            paramArrayOfString[0] = ("Requires newer sdk version #" + i11 + " (current version is #" + SDK_VERSION + ")");
                                            this.mParseError = -12;
                                            localPackage = null;
                                            break;
                                        }
                                        label1303: if (str11 == null)
                                            break label1436;
                                        String str12 = SDK_CODENAME;
                                        if (!str11.equals(str12))
                                        {
                                            if (SDK_CODENAME != null)
                                                paramArrayOfString[0] = ("Requires development platform " + str11 + " (current platform is " + SDK_CODENAME + ")");
                                            while (true)
                                            {
                                                this.mParseError = -12;
                                                localPackage = null;
                                                break;
                                                paramArrayOfString[0] = ("Requires development platform " + str11 + " but this is a release platform.");
                                            }
                                        }
                                    }
                                    label1436: for (localPackage.applicationInfo.targetSdkVersion = 10000; ; localPackage.applicationInfo.targetSdkVersion = i12)
                                    {
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                        break;
                                    }
                                }
                                if (str6.equals("supports-screens"))
                                {
                                    TypedArray localTypedArray7 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestSupportsScreens);
                                    localPackage.applicationInfo.requiresSmallestWidthDp = localTypedArray7.getInteger(6, 0);
                                    localPackage.applicationInfo.compatibleWidthLimitDp = localTypedArray7.getInteger(7, 0);
                                    localPackage.applicationInfo.largestWidthLimitDp = localTypedArray7.getInteger(8, 0);
                                    j = localTypedArray7.getInteger(1, j);
                                    k = localTypedArray7.getInteger(2, k);
                                    m = localTypedArray7.getInteger(3, m);
                                    n = localTypedArray7.getInteger(5, n);
                                    i1 = localTypedArray7.getInteger(4, i1);
                                    i2 = localTypedArray7.getInteger(0, i2);
                                    localTypedArray7.recycle();
                                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                }
                                else if (str6.equals("protected-broadcast"))
                                {
                                    TypedArray localTypedArray6 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestProtectedBroadcast);
                                    String str9 = localTypedArray6.getNonResourceString(0);
                                    localTypedArray6.recycle();
                                    if ((str9 != null) && ((paramInt & 0x1) != 0))
                                    {
                                        if (localPackage.protectedBroadcasts == null)
                                            localPackage.protectedBroadcasts = new ArrayList();
                                        if (!localPackage.protectedBroadcasts.contains(str9))
                                            localPackage.protectedBroadcasts.add(str9.intern());
                                    }
                                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                }
                                else
                                {
                                    if (str6.equals("instrumentation"))
                                    {
                                        if (parseInstrumentation(localPackage, paramResources, paramXmlResourceParser, paramXmlResourceParser, paramArrayOfString) != null)
                                            continue;
                                        localPackage = null;
                                        break;
                                    }
                                    if (str6.equals("original-package"))
                                    {
                                        TypedArray localTypedArray5 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestOriginalPackage);
                                        String str8 = localTypedArray5.getNonConfigurationString(0, 0);
                                        if (!localPackage.packageName.equals(str8))
                                        {
                                            if (localPackage.mOriginalPackages == null)
                                            {
                                                localPackage.mOriginalPackages = new ArrayList();
                                                localPackage.mRealPackage = localPackage.packageName;
                                            }
                                            localPackage.mOriginalPackages.add(str8);
                                        }
                                        localTypedArray5.recycle();
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    }
                                    else if (str6.equals("adopt-permissions"))
                                    {
                                        TypedArray localTypedArray4 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestOriginalPackage);
                                        String str7 = localTypedArray4.getNonConfigurationString(0, 0);
                                        localTypedArray4.recycle();
                                        if (str7 != null)
                                        {
                                            if (localPackage.mAdoptPermissions == null)
                                                localPackage.mAdoptPermissions = new ArrayList();
                                            localPackage.mAdoptPermissions.add(str7);
                                        }
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    }
                                    else if (str6.equals("uses-gl-texture"))
                                    {
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    }
                                    else if (str6.equals("compatible-screens"))
                                    {
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    }
                                    else if (str6.equals("eat-comment"))
                                    {
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    }
                                    else
                                    {
                                        Slog.w("PackageParser", "Unknown element under <manifest>: " + paramXmlResourceParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlResourceParser.getPositionDescription());
                                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    }
                                }
                            }
                        }
                    }
                    label2014: if ((i == 0) && (localPackage.instrumentation.size() == 0))
                    {
                        paramArrayOfString[0] = "<manifest> does not contain an <application> or <instrumentation>";
                        this.mParseError = -109;
                    }
                    int i5 = NEW_PERMISSIONS.length;
                    StringBuilder localStringBuilder = null;
                    int i6 = 0;
                    NewPermissionInfo localNewPermissionInfo;
                    SplitPermissionInfo localSplitPermissionInfo;
                    if (i6 < i5)
                    {
                        localNewPermissionInfo = NEW_PERMISSIONS[i6];
                        if (localPackage.applicationInfo.targetSdkVersion < localNewPermissionInfo.sdkVersion);
                    }
                    else
                    {
                        if (localStringBuilder != null)
                            Slog.i("PackageParser", localStringBuilder.toString());
                        int i7 = SPLIT_PERMISSIONS.length;
                        for (int i8 = 0; ; i8++)
                        {
                            if (i8 >= i7)
                                break label2349;
                            localSplitPermissionInfo = SPLIT_PERMISSIONS[i8];
                            if ((localPackage.applicationInfo.targetSdkVersion < localSplitPermissionInfo.targetSdk) && (localPackage.requestedPermissions.contains(localSplitPermissionInfo.rootPerm)))
                                break;
                        }
                    }
                    if (!localPackage.requestedPermissions.contains(localNewPermissionInfo.name))
                    {
                        if (localStringBuilder != null)
                            break label2268;
                        localStringBuilder = new StringBuilder(128);
                        String str4 = localPackage.packageName;
                        localStringBuilder.append(str4);
                        localStringBuilder.append(": compat added ");
                    }
                    while (true)
                    {
                        String str5 = localNewPermissionInfo.name;
                        localStringBuilder.append(str5);
                        localPackage.requestedPermissions.add(localNewPermissionInfo.name);
                        localPackage.requestedPermissionsRequired.add(Boolean.TRUE);
                        i6++;
                        break;
                        label2268: localStringBuilder.append(' ');
                    }
                    for (int i9 = 0; ; i9++)
                    {
                        int i10 = localSplitPermissionInfo.newPerms.length;
                        if (i9 >= i10)
                            break;
                        String str3 = localSplitPermissionInfo.newPerms[i9];
                        if (!localPackage.requestedPermissions.contains(str3))
                        {
                            localPackage.requestedPermissions.add(str3);
                            localPackage.requestedPermissionsRequired.add(Boolean.TRUE);
                        }
                    }
                    label2349: if ((j < 0) || ((j > 0) && (localPackage.applicationInfo.targetSdkVersion >= 4)))
                    {
                        ApplicationInfo localApplicationInfo1 = localPackage.applicationInfo;
                        localApplicationInfo1.flags = (0x200 | localApplicationInfo1.flags);
                    }
                    if (k != 0)
                    {
                        ApplicationInfo localApplicationInfo6 = localPackage.applicationInfo;
                        localApplicationInfo6.flags = (0x400 | localApplicationInfo6.flags);
                    }
                    if ((m < 0) || ((m > 0) && (localPackage.applicationInfo.targetSdkVersion >= 4)))
                    {
                        ApplicationInfo localApplicationInfo2 = localPackage.applicationInfo;
                        localApplicationInfo2.flags = (0x800 | localApplicationInfo2.flags);
                    }
                    if ((n < 0) || ((n > 0) && (localPackage.applicationInfo.targetSdkVersion >= 9)))
                    {
                        ApplicationInfo localApplicationInfo3 = localPackage.applicationInfo;
                        localApplicationInfo3.flags = (0x80000 | localApplicationInfo3.flags);
                    }
                    if ((i1 < 0) || ((i1 > 0) && (localPackage.applicationInfo.targetSdkVersion >= 4)))
                    {
                        ApplicationInfo localApplicationInfo4 = localPackage.applicationInfo;
                        localApplicationInfo4.flags = (0x1000 | localApplicationInfo4.flags);
                    }
                    if ((i2 < 0) || ((i2 > 0) && (localPackage.applicationInfo.targetSdkVersion >= 4)))
                    {
                        ApplicationInfo localApplicationInfo5 = localPackage.applicationInfo;
                        localApplicationInfo5.flags = (0x2000 | localApplicationInfo5.flags);
                    }
                }
            }
        }
    }

    private boolean parsePackageItemInfo(Package paramPackage, PackageItemInfo paramPackageItemInfo, String[] paramArrayOfString, String paramString, TypedArray paramTypedArray, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        String str = paramTypedArray.getNonConfigurationString(paramInt1, 0);
        boolean bool;
        if (str == null)
        {
            paramArrayOfString[0] = (paramString + " does not specify android:name");
            bool = false;
        }
        while (true)
        {
            return bool;
            paramPackageItemInfo.name = buildClassName(paramPackage.applicationInfo.packageName, str, paramArrayOfString);
            if (paramPackageItemInfo.name == null)
            {
                bool = false;
            }
            else
            {
                int i = paramTypedArray.getResourceId(paramInt3, 0);
                if (i != 0)
                {
                    paramPackageItemInfo.icon = i;
                    paramPackageItemInfo.nonLocalizedLabel = null;
                }
                int j = paramTypedArray.getResourceId(paramInt4, 0);
                if (j != 0)
                    paramPackageItemInfo.logo = j;
                TypedValue localTypedValue = paramTypedArray.peekValue(paramInt2);
                if (localTypedValue != null)
                {
                    int k = localTypedValue.resourceId;
                    paramPackageItemInfo.labelRes = k;
                    if (k == 0)
                        paramPackageItemInfo.nonLocalizedLabel = localTypedValue.coerceToString();
                }
                paramPackageItemInfo.packageName = paramPackage.packageName;
                bool = true;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static PackageLite parsePackageLite(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws IOException, XmlPullParserException
    {
        int i;
        do
            i = paramXmlPullParser.next();
        while ((i != 2) && (i != 1));
        PackageLite localPackageLite;
        if (i != 2)
        {
            paramArrayOfString[0] = "No start tag found";
            localPackageLite = null;
        }
        while (true)
        {
            return localPackageLite;
            if (!paramXmlPullParser.getName().equals("manifest"))
            {
                paramArrayOfString[0] = "No <manifest> tag";
                localPackageLite = null;
            }
            else
            {
                String str1 = paramAttributeSet.getAttributeValue(null, "package");
                if ((str1 == null) || (str1.length() == 0))
                {
                    paramArrayOfString[0] = "<manifest> does not specify package";
                    localPackageLite = null;
                }
                else
                {
                    String str2 = Injector.filterNameError(str1, validateName(str1, true));
                    if ((str2 != null) && (!"android".equals(str1)))
                    {
                        paramArrayOfString[0] = ("<manifest> specifies bad package name \"" + str1 + "\": " + str2);
                        localPackageLite = null;
                    }
                    else
                    {
                        int j = -1;
                        ArrayList localArrayList;
                        for (int k = 0; ; k++)
                            if (k < paramAttributeSet.getAttributeCount())
                            {
                                if (paramAttributeSet.getAttributeName(k).equals("installLocation"))
                                    j = paramAttributeSet.getAttributeIntValue(k, -1);
                            }
                            else
                            {
                                int m = 1 + paramXmlPullParser.getDepth();
                                localArrayList = new ArrayList();
                                while (true)
                                {
                                    int n = paramXmlPullParser.next();
                                    if ((n == 1) || ((n == 3) && (paramXmlPullParser.getDepth() < m)))
                                        break;
                                    if ((n != 3) && (n != 4) && (paramXmlPullParser.getDepth() == m) && ("package-verifier".equals(paramXmlPullParser.getName())))
                                    {
                                        VerifierInfo localVerifierInfo = parseVerifier(paramResources, paramXmlPullParser, paramAttributeSet, paramInt, paramArrayOfString);
                                        if (localVerifierInfo != null)
                                            localArrayList.add(localVerifierInfo);
                                    }
                                }
                            }
                        localPackageLite = new PackageLite(str1.intern(), j, localArrayList);
                    }
                }
            }
        }
    }

    // ERROR //
    public static PackageLite parsePackageLite(String paramString, int paramInt)
    {
        // Byte code:
        //     0: new 1319	android/content/res/AssetManager
        //     3: dup
        //     4: invokespecial 1320	android/content/res/AssetManager:<init>	()V
        //     7: astore_2
        //     8: aload_2
        //     9: iconst_0
        //     10: iconst_0
        //     11: aconst_null
        //     12: iconst_0
        //     13: iconst_0
        //     14: iconst_0
        //     15: iconst_0
        //     16: iconst_0
        //     17: iconst_0
        //     18: iconst_0
        //     19: iconst_0
        //     20: iconst_0
        //     21: iconst_0
        //     22: iconst_0
        //     23: iconst_0
        //     24: iconst_0
        //     25: getstatic 1323	android/os/Build$VERSION:RESOURCES_SDK_INT	I
        //     28: invokevirtual 1327	android/content/res/AssetManager:setConfiguration	(IILjava/lang/String;IIIIIIIIIIIIII)V
        //     31: aload_2
        //     32: aload_0
        //     33: invokevirtual 1331	android/content/res/AssetManager:addAssetPath	(Ljava/lang/String;)I
        //     36: istore 6
        //     38: iload 6
        //     40: ifne +9 -> 49
        //     43: aconst_null
        //     44: astore 5
        //     46: goto +268 -> 314
        //     49: new 1333	android/util/DisplayMetrics
        //     52: dup
        //     53: invokespecial 1334	android/util/DisplayMetrics:<init>	()V
        //     56: astore 7
        //     58: aload 7
        //     60: invokevirtual 1337	android/util/DisplayMetrics:setToDefaults	()V
        //     63: new 639	android/content/res/Resources
        //     66: dup
        //     67: aload_2
        //     68: aload 7
        //     70: aconst_null
        //     71: invokespecial 1340	android/content/res/Resources:<init>	(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
        //     74: astore 8
        //     76: aload_2
        //     77: iload 6
        //     79: ldc 59
        //     81: invokevirtual 1344	android/content/res/AssetManager:openXmlResourceParser	(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
        //     84: astore 9
        //     86: iconst_1
        //     87: anewarray 126	java/lang/String
        //     90: astore 10
        //     92: aconst_null
        //     93: astore 5
        //     95: aload 8
        //     97: aload 9
        //     99: aload 9
        //     101: iload_1
        //     102: aload 10
        //     104: invokestatic 1346	android/content/pm/PackageParser:parsePackageLite	(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$PackageLite;
        //     107: astore 17
        //     109: aload 17
        //     111: astore 5
        //     113: aload 9
        //     115: ifnull +10 -> 125
        //     118: aload 9
        //     120: invokeinterface 1347 1 0
        //     125: aload_2
        //     126: ifnull +7 -> 133
        //     129: aload_2
        //     130: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     133: aload 5
        //     135: ifnonnull +179 -> 314
        //     138: ldc 94
        //     140: new 188	java/lang/StringBuilder
        //     143: dup
        //     144: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     147: ldc_w 1350
        //     150: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     153: aload 10
        //     155: iconst_0
        //     156: aaload
        //     157: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     160: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     163: invokestatic 1351	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     166: pop
        //     167: aconst_null
        //     168: astore 5
        //     170: goto +144 -> 314
        //     173: astore_3
        //     174: aconst_null
        //     175: astore_2
        //     176: aload_2
        //     177: ifnull +7 -> 184
        //     180: aload_2
        //     181: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     184: ldc 94
        //     186: new 188	java/lang/StringBuilder
        //     189: dup
        //     190: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     193: ldc_w 1353
        //     196: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     199: aload_0
        //     200: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     203: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     206: aload_3
        //     207: invokestatic 628	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     210: pop
        //     211: aconst_null
        //     212: astore 5
        //     214: goto +100 -> 314
        //     217: astore 15
        //     219: ldc 94
        //     221: aload_0
        //     222: aload 15
        //     224: invokestatic 628	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     227: pop
        //     228: aload 9
        //     230: ifnull +10 -> 240
        //     233: aload 9
        //     235: invokeinterface 1347 1 0
        //     240: aload_2
        //     241: ifnull -108 -> 133
        //     244: aload_2
        //     245: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     248: goto -115 -> 133
        //     251: astore 12
        //     253: ldc 94
        //     255: aload_0
        //     256: aload 12
        //     258: invokestatic 628	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     261: pop
        //     262: aload 9
        //     264: ifnull +10 -> 274
        //     267: aload 9
        //     269: invokeinterface 1347 1 0
        //     274: aload_2
        //     275: ifnull -142 -> 133
        //     278: aload_2
        //     279: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     282: goto -149 -> 133
        //     285: astore 11
        //     287: aload 9
        //     289: ifnull +10 -> 299
        //     292: aload 9
        //     294: invokeinterface 1347 1 0
        //     299: aload_2
        //     300: ifnull +7 -> 307
        //     303: aload_2
        //     304: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     307: aload 11
        //     309: athrow
        //     310: astore_3
        //     311: goto -135 -> 176
        //     314: aload 5
        //     316: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     0	8	173	java/lang/Exception
        //     95	109	217	java/io/IOException
        //     95	109	251	org/xmlpull/v1/XmlPullParserException
        //     95	109	285	finally
        //     219	228	285	finally
        //     253	262	285	finally
        //     8	86	310	java/lang/Exception
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static String parsePackageName(XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws IOException, XmlPullParserException
    {
        String str1 = null;
        int i;
        do
            i = paramXmlPullParser.next();
        while ((i != 2) && (i != 1));
        if (i != 2)
            paramArrayOfString[0] = "No start tag found";
        while (true)
        {
            return str1;
            if (!paramXmlPullParser.getName().equals("manifest"))
            {
                paramArrayOfString[0] = "No <manifest> tag";
            }
            else
            {
                String str2 = paramAttributeSet.getAttributeValue(null, "package");
                if ((str2 == null) || (str2.length() == 0))
                {
                    paramArrayOfString[0] = "<manifest> does not specify package";
                }
                else
                {
                    String str3 = Injector.filterNameError(str2, validateName(str2, true));
                    if ((str3 != null) && (!"android".equals(str2)))
                        paramArrayOfString[0] = ("<manifest> specifies bad package name \"" + str2 + "\": " + str3);
                    else
                        str1 = str2.intern();
                }
            }
        }
    }

    private Permission parsePermission(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        Permission localPermission = new Permission(paramPackage);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestPermission);
        if (!parsePackageItemInfo(paramPackage, localPermission.info, paramArrayOfString, "<permission>", localTypedArray, 2, 0, 1, 6))
        {
            localTypedArray.recycle();
            this.mParseError = -108;
            localPermission = null;
        }
        while (true)
        {
            return localPermission;
            localPermission.info.group = localTypedArray.getNonResourceString(4);
            if (localPermission.info.group != null)
                localPermission.info.group = localPermission.info.group.intern();
            localPermission.info.descriptionRes = localTypedArray.getResourceId(5, 0);
            localPermission.info.protectionLevel = localTypedArray.getInt(3, 0);
            localTypedArray.recycle();
            if (localPermission.info.protectionLevel == -1)
            {
                paramArrayOfString[0] = "<permission> does not specify protectionLevel";
                this.mParseError = -108;
                localPermission = null;
            }
            else
            {
                localPermission.info.protectionLevel = PermissionInfo.fixProtectionLevel(localPermission.info.protectionLevel);
                if (((0xF0 & localPermission.info.protectionLevel) != 0) && ((0xF & localPermission.info.protectionLevel) != 2))
                {
                    paramArrayOfString[0] = "<permission>    protectionLevel specifies a flag but is not based on signature type";
                    this.mParseError = -108;
                    localPermission = null;
                }
                else if (!parseAllMetaData(paramResources, paramXmlPullParser, paramAttributeSet, "<permission>", localPermission, paramArrayOfString))
                {
                    this.mParseError = -108;
                    localPermission = null;
                }
                else
                {
                    paramPackage.permissions.add(localPermission);
                }
            }
        }
    }

    private PermissionGroup parsePermissionGroup(Package paramPackage, int paramInt, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        PermissionGroup localPermissionGroup = new PermissionGroup(paramPackage);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestPermissionGroup);
        if (!parsePackageItemInfo(paramPackage, localPermissionGroup.info, paramArrayOfString, "<permission-group>", localTypedArray, 2, 0, 1, 5))
        {
            localTypedArray.recycle();
            this.mParseError = -108;
            localPermissionGroup = null;
        }
        while (true)
        {
            return localPermissionGroup;
            localPermissionGroup.info.descriptionRes = localTypedArray.getResourceId(4, 0);
            localPermissionGroup.info.flags = 0;
            localPermissionGroup.info.priority = localTypedArray.getInt(3, 0);
            if ((localPermissionGroup.info.priority > 0) && ((paramInt & 0x1) == 0))
                localPermissionGroup.info.priority = 0;
            localTypedArray.recycle();
            if (!parseAllMetaData(paramResources, paramXmlPullParser, paramAttributeSet, "<permission-group>", localPermissionGroup, paramArrayOfString))
            {
                this.mParseError = -108;
                localPermissionGroup = null;
            }
            else
            {
                paramPackage.permissionGroups.add(localPermissionGroup);
            }
        }
    }

    private Permission parsePermissionTree(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        Permission localPermission = new Permission(paramPackage);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestPermissionTree);
        if (!parsePackageItemInfo(paramPackage, localPermission.info, paramArrayOfString, "<permission-tree>", localTypedArray, 2, 0, 1, 3))
        {
            localTypedArray.recycle();
            this.mParseError = -108;
            localPermission = null;
        }
        while (true)
        {
            return localPermission;
            localTypedArray.recycle();
            int i = localPermission.info.name.indexOf('.');
            if (i > 0)
                i = localPermission.info.name.indexOf('.', i + 1);
            if (i < 0)
            {
                paramArrayOfString[0] = ("<permission-tree> name has less than three segments: " + localPermission.info.name);
                this.mParseError = -108;
                localPermission = null;
            }
            else
            {
                localPermission.info.descriptionRes = 0;
                localPermission.info.protectionLevel = 0;
                localPermission.tree = true;
                if (!parseAllMetaData(paramResources, paramXmlPullParser, paramAttributeSet, "<permission-tree>", localPermission, paramArrayOfString))
                {
                    this.mParseError = -108;
                    localPermission = null;
                }
                else
                {
                    paramPackage.permissions.add(localPermission);
                }
            }
        }
    }

    private Provider parseProvider(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestProvider);
        if (this.mParseProviderArgs == null)
        {
            this.mParseProviderArgs = new ParseComponentArgs(paramPackage, paramArrayOfString, 2, 0, 1, 15, this.mSeparateProcesses, 8, 14, 6);
            this.mParseProviderArgs.tag = "<provider>";
        }
        this.mParseProviderArgs.sa = localTypedArray;
        this.mParseProviderArgs.flags = paramInt;
        Provider localProvider = new Provider(this.mParseProviderArgs, new ProviderInfo());
        if (paramArrayOfString[0] != null)
        {
            localTypedArray.recycle();
            localProvider = null;
        }
        while (true)
        {
            return localProvider;
            localProvider.info.exported = localTypedArray.getBoolean(7, true);
            String str1 = localTypedArray.getNonConfigurationString(10, 0);
            localProvider.info.isSyncable = localTypedArray.getBoolean(11, false);
            String str2 = localTypedArray.getNonConfigurationString(3, 0);
            String str3 = localTypedArray.getNonConfigurationString(4, 0);
            if (str3 == null)
                str3 = str2;
            String str5;
            if (str3 == null)
            {
                localProvider.info.readPermission = paramPackage.applicationInfo.permission;
                str5 = localTypedArray.getNonConfigurationString(5, 0);
                if (str5 == null)
                    str5 = str2;
                if (str5 == null)
                {
                    localProvider.info.writePermission = paramPackage.applicationInfo.permission;
                    localProvider.info.grantUriPermissions = localTypedArray.getBoolean(13, false);
                    localProvider.info.multiprocess = localTypedArray.getBoolean(9, false);
                    localProvider.info.initOrder = localTypedArray.getInt(12, 0);
                    localTypedArray.recycle();
                    if (((0x10000000 & paramPackage.applicationInfo.flags) == 0) || (localProvider.info.processName != paramPackage.packageName))
                        break label417;
                    paramArrayOfString[0] = "Heavy-weight applications can not have providers in main process";
                    localProvider = null;
                }
            }
            else
            {
                ProviderInfo localProviderInfo1 = localProvider.info;
                if (str3.length() > 0);
                for (String str4 = str3.toString().intern(); ; str4 = null)
                {
                    localProviderInfo1.readPermission = str4;
                    break;
                }
                ProviderInfo localProviderInfo2 = localProvider.info;
                if (str5.length() > 0);
                for (String str6 = str5.toString().intern(); ; str6 = null)
                {
                    localProviderInfo2.writePermission = str6;
                    break;
                }
                label417: if (str1 == null)
                {
                    paramArrayOfString[0] = "<provider> does not incude authorities attribute";
                    localProvider = null;
                }
                else
                {
                    localProvider.info.authority = str1.intern();
                    if (!parseProviderTags(paramResources, paramXmlPullParser, paramAttributeSet, localProvider, paramArrayOfString))
                        localProvider = null;
                }
            }
        }
    }

    private boolean parseProviderTags(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Provider paramProvider, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        Bundle localBundle;
        do
        {
            int j;
            do
            {
                j = paramXmlPullParser.next();
                if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                    break;
            }
            while ((j == 3) || (j == 4));
            if (!paramXmlPullParser.getName().equals("meta-data"))
                break;
            localBundle = parseMetaData(paramResources, paramXmlPullParser, paramAttributeSet, paramProvider.metaData, paramArrayOfString);
            paramProvider.metaData = localBundle;
        }
        while (localBundle != null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            if (paramXmlPullParser.getName().equals("grant-uri-permission"))
            {
                TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestGrantUriPermission);
                PatternMatcher localPatternMatcher = null;
                String str7 = localTypedArray2.getNonConfigurationString(0, 0);
                if (str7 != null)
                    localPatternMatcher = new PatternMatcher(str7, 0);
                String str8 = localTypedArray2.getNonConfigurationString(1, 0);
                if (str8 != null)
                    localPatternMatcher = new PatternMatcher(str8, 1);
                String str9 = localTypedArray2.getNonConfigurationString(2, 0);
                if (str9 != null)
                    localPatternMatcher = new PatternMatcher(str9, 2);
                localTypedArray2.recycle();
                if (localPatternMatcher != null)
                {
                    if (paramProvider.info.uriPermissionPatterns == null)
                    {
                        paramProvider.info.uriPermissionPatterns = new PatternMatcher[1];
                        paramProvider.info.uriPermissionPatterns[0] = localPatternMatcher;
                    }
                    while (true)
                    {
                        paramProvider.info.grantUriPermissions = true;
                        XmlUtils.skipCurrentTag(paramXmlPullParser);
                        break;
                        int n = paramProvider.info.uriPermissionPatterns.length;
                        PatternMatcher[] arrayOfPatternMatcher = new PatternMatcher[n + 1];
                        System.arraycopy(paramProvider.info.uriPermissionPatterns, 0, arrayOfPatternMatcher, 0, n);
                        arrayOfPatternMatcher[n] = localPatternMatcher;
                        paramProvider.info.uriPermissionPatterns = arrayOfPatternMatcher;
                    }
                }
                Slog.w("PackageParser", "Unknown element under <path-permission>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                XmlUtils.skipCurrentTag(paramXmlPullParser);
                break;
            }
            if (paramXmlPullParser.getName().equals("path-permission"))
            {
                TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestPathPermission);
                PathPermission localPathPermission = null;
                String str1 = localTypedArray1.getNonConfigurationString(0, 0);
                String str2 = localTypedArray1.getNonConfigurationString(1, 0);
                if (str2 == null)
                    str2 = str1;
                String str3 = localTypedArray1.getNonConfigurationString(2, 0);
                if (str3 == null)
                    str3 = str1;
                int k = 0;
                if (str2 != null)
                {
                    str2 = str2.intern();
                    k = 1;
                }
                if (str3 != null)
                {
                    str3 = str3.intern();
                    k = 1;
                }
                if (k == 0)
                {
                    Slog.w("PackageParser", "No readPermission or writePermssion for <path-permission>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                    break;
                }
                String str4 = localTypedArray1.getNonConfigurationString(3, 0);
                if (str4 != null)
                    localPathPermission = new PathPermission(str4, 0, str2, str3);
                String str5 = localTypedArray1.getNonConfigurationString(4, 0);
                if (str5 != null)
                    localPathPermission = new PathPermission(str5, 1, str2, str3);
                String str6 = localTypedArray1.getNonConfigurationString(5, 0);
                if (str6 != null)
                    localPathPermission = new PathPermission(str6, 2, str2, str3);
                localTypedArray1.recycle();
                if (localPathPermission != null)
                {
                    if (paramProvider.info.pathPermissions == null)
                    {
                        paramProvider.info.pathPermissions = new PathPermission[1];
                        paramProvider.info.pathPermissions[0] = localPathPermission;
                    }
                    while (true)
                    {
                        XmlUtils.skipCurrentTag(paramXmlPullParser);
                        break;
                        int m = paramProvider.info.pathPermissions.length;
                        PathPermission[] arrayOfPathPermission = new PathPermission[m + 1];
                        System.arraycopy(paramProvider.info.pathPermissions, 0, arrayOfPathPermission, 0, m);
                        arrayOfPathPermission[m] = localPathPermission;
                        paramProvider.info.pathPermissions = arrayOfPathPermission;
                    }
                }
                Slog.w("PackageParser", "No path, pathPrefix, or pathPattern for <path-permission>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                XmlUtils.skipCurrentTag(paramXmlPullParser);
                break;
            }
            Slog.w("PackageParser", "Unknown element under <provider>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
        }
    }

    private Service parseService(Package paramPackage, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestService);
        if (this.mParseServiceArgs == null)
        {
            this.mParseServiceArgs = new ParseComponentArgs(paramPackage, paramArrayOfString, 2, 0, 1, 8, this.mSeparateProcesses, 6, 7, 4);
            this.mParseServiceArgs.tag = "<service>";
        }
        this.mParseServiceArgs.sa = localTypedArray;
        this.mParseServiceArgs.flags = paramInt;
        Service localService = new Service(this.mParseServiceArgs, new ServiceInfo());
        if (paramArrayOfString[0] != null)
        {
            localTypedArray.recycle();
            localService = null;
        }
        boolean bool1;
        label326: 
        do
        {
            String str1;
            while (true)
            {
                return localService;
                bool1 = localTypedArray.hasValue(5);
                if (bool1)
                    localService.info.exported = localTypedArray.getBoolean(5, false);
                str1 = localTypedArray.getNonConfigurationString(3, 0);
                if (str1 != null)
                    break;
                localService.info.permission = paramPackage.applicationInfo.permission;
                localService.info.flags = 0;
                if (localTypedArray.getBoolean(9, false))
                {
                    ServiceInfo localServiceInfo4 = localService.info;
                    localServiceInfo4.flags = (0x1 | localServiceInfo4.flags);
                }
                if (localTypedArray.getBoolean(10, false))
                {
                    ServiceInfo localServiceInfo3 = localService.info;
                    localServiceInfo3.flags = (0x2 | localServiceInfo3.flags);
                }
                localTypedArray.recycle();
                if (((0x10000000 & paramPackage.applicationInfo.flags) == 0) || (localService.info.processName != paramPackage.packageName))
                    break label326;
                paramArrayOfString[0] = "Heavy-weight applications can not have services in main process";
                localService = null;
            }
            ServiceInfo localServiceInfo1 = localService.info;
            if (str1.length() > 0);
            for (String str2 = str1.toString().intern(); ; str2 = null)
            {
                localServiceInfo1.permission = str2;
                break;
            }
            int i = paramXmlPullParser.getDepth();
            while (true)
            {
                int j = paramXmlPullParser.next();
                if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                    break label557;
                if ((j != 3) && (j != 4))
                {
                    if (paramXmlPullParser.getName().equals("intent-filter"))
                    {
                        ServiceIntentInfo localServiceIntentInfo = new ServiceIntentInfo(localService);
                        if (!parseIntent(paramResources, paramXmlPullParser, paramAttributeSet, paramInt, localServiceIntentInfo, paramArrayOfString, false))
                        {
                            localService = null;
                            break;
                        }
                        localService.intents.add(localServiceIntentInfo);
                        continue;
                    }
                    if (paramXmlPullParser.getName().equals("meta-data"))
                    {
                        Bundle localBundle = parseMetaData(paramResources, paramXmlPullParser, paramAttributeSet, localService.metaData, paramArrayOfString);
                        localService.metaData = localBundle;
                        if (localBundle != null)
                            continue;
                        localService = null;
                        break;
                    }
                    Slog.w("PackageParser", "Unknown element under <service>: " + paramXmlPullParser.getName() + " at " + this.mArchiveSourcePath + " " + paramXmlPullParser.getPositionDescription());
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
            }
        }
        while (bool1);
        label557: ServiceInfo localServiceInfo2 = localService.info;
        if (localService.intents.size() > 0);
        for (boolean bool2 = true; ; bool2 = false)
        {
            localServiceInfo2.exported = bool2;
            break;
        }
    }

    // ERROR //
    private static VerifierInfo parseVerifier(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, int paramInt, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_2
        //     2: getstatic 1505	com/android/internal/R$styleable:AndroidManifestPackageVerifier	[I
        //     5: invokevirtual 643	android/content/res/Resources:obtainAttributes	(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
        //     8: astore 5
        //     10: aload 5
        //     12: iconst_0
        //     13: invokevirtual 891	android/content/res/TypedArray:getNonResourceString	(I)Ljava/lang/String;
        //     16: astore 6
        //     18: aload 5
        //     20: iconst_1
        //     21: invokevirtual 891	android/content/res/TypedArray:getNonResourceString	(I)Ljava/lang/String;
        //     24: astore 7
        //     26: aload 5
        //     28: invokevirtual 669	android/content/res/TypedArray:recycle	()V
        //     31: aload 6
        //     33: ifnull +11 -> 44
        //     36: aload 6
        //     38: invokevirtual 223	java/lang/String:length	()I
        //     41: ifne +18 -> 59
        //     44: ldc 94
        //     46: ldc_w 1507
        //     49: invokestatic 1251	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     52: pop
        //     53: aconst_null
        //     54: astore 9
        //     56: aload 9
        //     58: areturn
        //     59: aload 7
        //     61: ifnonnull +36 -> 97
        //     64: ldc 94
        //     66: new 188	java/lang/StringBuilder
        //     69: dup
        //     70: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     73: ldc_w 1509
        //     76: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     79: aload 6
        //     81: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     84: ldc_w 1511
        //     87: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     90: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     93: invokestatic 1251	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     96: pop
        //     97: new 1513	java/security/spec/X509EncodedKeySpec
        //     100: dup
        //     101: aload 7
        //     103: iconst_0
        //     104: invokestatic 1519	android/util/Base64:decode	(Ljava/lang/String;I)[B
        //     107: invokespecial 1522	java/security/spec/X509EncodedKeySpec:<init>	([B)V
        //     110: astore 10
        //     112: new 1524	android/content/pm/VerifierInfo
        //     115: dup
        //     116: aload 6
        //     118: ldc_w 1526
        //     121: invokestatic 1532	java/security/KeyFactory:getInstance	(Ljava/lang/String;)Ljava/security/KeyFactory;
        //     124: aload 10
        //     126: invokevirtual 1536	java/security/KeyFactory:generatePublic	(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
        //     129: invokespecial 1539	android/content/pm/VerifierInfo:<init>	(Ljava/lang/String;Ljava/security/PublicKey;)V
        //     132: astore 9
        //     134: goto -78 -> 56
        //     137: astore 15
        //     139: ldc 94
        //     141: ldc_w 1541
        //     144: invokestatic 1544	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;)I
        //     147: pop
        //     148: aconst_null
        //     149: astore 9
        //     151: goto -95 -> 56
        //     154: astore 17
        //     156: ldc 94
        //     158: new 188	java/lang/StringBuilder
        //     161: dup
        //     162: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     165: ldc_w 1546
        //     168: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     171: aload 6
        //     173: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     176: ldc_w 1548
        //     179: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     182: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     185: invokestatic 1251	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     188: pop
        //     189: aconst_null
        //     190: astore 9
        //     192: goto -136 -> 56
        //     195: astore 11
        //     197: new 1524	android/content/pm/VerifierInfo
        //     200: dup
        //     201: aload 6
        //     203: ldc_w 1550
        //     206: invokestatic 1532	java/security/KeyFactory:getInstance	(Ljava/lang/String;)Ljava/security/KeyFactory;
        //     209: aload 10
        //     211: invokevirtual 1536	java/security/KeyFactory:generatePublic	(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
        //     214: invokespecial 1539	android/content/pm/VerifierInfo:<init>	(Ljava/lang/String;Ljava/security/PublicKey;)V
        //     217: astore 9
        //     219: goto -163 -> 56
        //     222: astore 13
        //     224: ldc 94
        //     226: ldc_w 1552
        //     229: invokestatic 1544	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;)I
        //     232: pop
        //     233: aconst_null
        //     234: astore 9
        //     236: goto -180 -> 56
        //     239: astore 12
        //     241: aconst_null
        //     242: astore 9
        //     244: goto -188 -> 56
        //
        // Exception table:
        //     from	to	target	type
        //     112	134	137	java/security/NoSuchAlgorithmException
        //     97	112	154	java/lang/IllegalArgumentException
        //     112	134	195	java/security/spec/InvalidKeySpecException
        //     197	219	222	java/security/NoSuchAlgorithmException
        //     197	219	239	java/security/spec/InvalidKeySpecException
    }

    public static void setCompatibilityModeEnabled(boolean paramBoolean)
    {
        sCompatibilityModeEnabled = paramBoolean;
    }

    public static Signature stringToSignature(String paramString)
    {
        int i = paramString.length();
        byte[] arrayOfByte = new byte[i];
        for (int j = 0; j < i; j++)
            arrayOfByte[j] = ((byte)paramString.charAt(j));
        return new Signature(arrayOfByte);
    }

    private static String validateName(String paramString, boolean paramBoolean)
    {
        int i = paramString.length();
        int j = 0;
        int k = 1;
        int m = 0;
        label105: String str;
        if (m < i)
        {
            char c = paramString.charAt(m);
            if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')));
            for (k = 0; ; k = 1)
            {
                do
                {
                    m++;
                    break;
                }
                while ((k == 0) && (((c >= '0') && (c <= '9')) || (c == '_')));
                if (c != '.')
                    break label105;
                j = 1;
            }
            str = "bad character '" + c + "'";
        }
        while (true)
        {
            return str;
            if ((j != 0) || (!paramBoolean))
                str = null;
            else
                str = "must have at least one '.' separator";
        }
    }

    public boolean collectCertificates(Package paramPackage, int paramInt)
    {
        paramPackage.mSignatures = null;
        byte[] arrayOfByte = null;
        WeakReference localWeakReference;
        synchronized (mSync)
        {
            localWeakReference = mReadBuffer;
            if (localWeakReference != null)
            {
                mReadBuffer = null;
                arrayOfByte = (byte[])localWeakReference.get();
            }
            if (arrayOfByte == null)
            {
                arrayOfByte = new byte[8192];
                localWeakReference = new WeakReference(arrayOfByte);
            }
        }
        try
        {
            JarFile localJarFile = new JarFile(this.mArchiveSourcePath);
            localObject3 = null;
            if ((paramInt & 0x1) != 0)
            {
                JarEntry localJarEntry2 = localJarFile.getJarEntry("AndroidManifest.xml");
                localObject3 = loadCertificates(localJarFile, localJarEntry2, arrayOfByte);
                if (localObject3 == null)
                {
                    Slog.e("PackageParser", "Package " + paramPackage.packageName + " has no certificates at entry " + localJarEntry2.getName() + "; ignoring!");
                    localJarFile.close();
                    this.mParseError = -103;
                    bool = false;
                }
            }
            else
            {
                while (true)
                {
                    return bool;
                    localObject2 = finally;
                    throw localObject2;
                    Enumeration localEnumeration = localJarFile.entries();
                    Manifest localManifest = localJarFile.getManifest();
                    JarEntry localJarEntry1;
                    do
                    {
                        do
                        {
                            String str;
                            do
                            {
                                do
                                {
                                    if (!localEnumeration.hasMoreElements())
                                        break;
                                    localJarEntry1 = (JarEntry)localEnumeration.nextElement();
                                }
                                while (localJarEntry1.isDirectory());
                                str = localJarEntry1.getName();
                            }
                            while (str.startsWith("META-INF/"));
                            if ("AndroidManifest.xml".equals(str))
                                paramPackage.manifestDigest = ManifestDigest.fromAttributes(localManifest.getAttributes(str));
                            arrayOfCertificate = loadCertificates(localJarFile, localJarEntry1, arrayOfByte);
                            if (arrayOfCertificate != null)
                                break label772;
                        }
                        while (ExtraPackageManager.isTrustedAppEntry(localJarEntry1, paramPackage.packageName));
                        Slog.e("PackageParser", "Package " + paramPackage.packageName + " has no certificates at entry " + localJarEntry1.getName() + "; ignoring!");
                        localJarFile.close();
                        this.mParseError = -103;
                        bool = false;
                        break;
                    }
                    while (k >= localObject3.length);
                    int m = 0;
                    n = 0;
                    if (n < arrayOfCertificate.length)
                    {
                        if ((localObject3[k] == null) || (!localObject3[k].equals(arrayOfCertificate[n])))
                            break label790;
                        m = 1;
                    }
                    if ((m != 0) && (localObject3.length == arrayOfCertificate.length))
                        break label796;
                    Slog.e("PackageParser", "Package " + paramPackage.packageName + " has mismatched certificates at entry " + localJarEntry1.getName() + "; ignoring!");
                    localJarFile.close();
                    this.mParseError = -104;
                    bool = false;
                }
            }
            localJarFile.close();
            synchronized (mSync)
            {
                mReadBuffer = localWeakReference;
                if ((localObject3 != null) && (localObject3.length > 0))
                {
                    int i = localObject3.length;
                    paramPackage.mSignatures = new Signature[localObject3.length];
                    int j = 0;
                    if (j >= i)
                        break label766;
                    paramPackage.mSignatures[j] = new Signature(localObject3[j].getEncoded());
                    j++;
                }
            }
        }
        catch (CertificateEncodingException localCertificateEncodingException)
        {
            while (true)
            {
                Slog.w("PackageParser", "Exception reading " + this.mArchiveSourcePath, localCertificateEncodingException);
                this.mParseError = -105;
                bool = false;
                continue;
                Slog.e("PackageParser", "Package " + paramPackage.packageName + " has no certificates; ignoring!");
                this.mParseError = -103;
                bool = false;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Slog.w("PackageParser", "Exception reading " + this.mArchiveSourcePath, localIOException);
                this.mParseError = -105;
                bool = false;
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Object localObject3;
                Certificate[] arrayOfCertificate;
                int k;
                int n;
                Slog.w("PackageParser", "Exception reading " + this.mArchiveSourcePath, localRuntimeException);
                this.mParseError = -102;
                boolean bool = false;
                continue;
                label766: bool = true;
                continue;
                label772: if (localObject3 == null)
                {
                    localObject3 = arrayOfCertificate;
                }
                else
                {
                    k = 0;
                    continue;
                    label790: n++;
                    continue;
                    label796: k++;
                }
            }
        }
    }

    public int getParseError()
    {
        return this.mParseError;
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Package parsePackage(java.io.File paramFile, String paramString, android.util.DisplayMetrics paramDisplayMetrics, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: iconst_1
        //     2: putfield 168	android/content/pm/PackageParser:mParseError	I
        //     5: aload_0
        //     6: aload_1
        //     7: invokevirtual 1657	java/io/File:getPath	()Ljava/lang/String;
        //     10: putfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     13: aload_1
        //     14: invokevirtual 1660	java/io/File:isFile	()Z
        //     17: ifne +44 -> 61
        //     20: ldc 94
        //     22: new 188	java/lang/StringBuilder
        //     25: dup
        //     26: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     29: ldc_w 1662
        //     32: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     35: aload_0
        //     36: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     39: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     42: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     45: invokestatic 775	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     48: pop
        //     49: aload_0
        //     50: bipush 156
        //     52: putfield 168	android/content/pm/PackageParser:mParseError	I
        //     55: aconst_null
        //     56: astore 12
        //     58: aload 12
        //     60: areturn
        //     61: aload_1
        //     62: invokevirtual 1663	java/io/File:getName	()Ljava/lang/String;
        //     65: invokestatic 1665	android/content/pm/PackageParser:isPackageFilename	(Ljava/lang/String;)Z
        //     68: ifne +58 -> 126
        //     71: iload 4
        //     73: iconst_4
        //     74: iand
        //     75: ifeq +51 -> 126
        //     78: iload 4
        //     80: iconst_1
        //     81: iand
        //     82: ifne +32 -> 114
        //     85: ldc 94
        //     87: new 188	java/lang/StringBuilder
        //     90: dup
        //     91: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     94: ldc_w 1667
        //     97: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: aload_0
        //     101: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     104: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     110: invokestatic 775	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     113: pop
        //     114: aload_0
        //     115: bipush 156
        //     117: putfield 168	android/content/pm/PackageParser:mParseError	I
        //     120: aconst_null
        //     121: astore 12
        //     123: goto -65 -> 58
        //     126: aconst_null
        //     127: astore 5
        //     129: aconst_null
        //     130: astore 6
        //     132: iconst_1
        //     133: istore 7
        //     135: new 1319	android/content/res/AssetManager
        //     138: dup
        //     139: invokespecial 1320	android/content/res/AssetManager:<init>	()V
        //     142: astore 8
        //     144: aload 8
        //     146: aload_0
        //     147: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     150: invokevirtual 1331	android/content/res/AssetManager:addAssetPath	(Ljava/lang/String;)I
        //     153: istore 19
        //     155: iload 19
        //     157: ifeq +89 -> 246
        //     160: new 1669	android/content/res/MiuiResources
        //     163: dup
        //     164: aload 8
        //     166: aload_3
        //     167: aconst_null
        //     168: invokespecial 1670	android/content/res/MiuiResources:<init>	(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
        //     171: astore 20
        //     173: aload 8
        //     175: iconst_0
        //     176: iconst_0
        //     177: aconst_null
        //     178: iconst_0
        //     179: iconst_0
        //     180: iconst_0
        //     181: iconst_0
        //     182: iconst_0
        //     183: iconst_0
        //     184: iconst_0
        //     185: iconst_0
        //     186: iconst_0
        //     187: iconst_0
        //     188: iconst_0
        //     189: iconst_0
        //     190: iconst_0
        //     191: getstatic 1323	android/os/Build$VERSION:RESOURCES_SDK_INT	I
        //     194: invokevirtual 1327	android/content/res/AssetManager:setConfiguration	(IILjava/lang/String;IIIIIIIIIIIIII)V
        //     197: aload 8
        //     199: iload 19
        //     201: ldc 59
        //     203: invokevirtual 1344	android/content/res/AssetManager:openXmlResourceParser	(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
        //     206: astore 21
        //     208: aload 21
        //     210: astore 5
        //     212: iconst_0
        //     213: istore 7
        //     215: aload 20
        //     217: astore 6
        //     219: iload 7
        //     221: ifeq +93 -> 314
        //     224: aload 8
        //     226: ifnull +8 -> 234
        //     229: aload 8
        //     231: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     234: aload_0
        //     235: bipush 155
        //     237: putfield 168	android/content/pm/PackageParser:mParseError	I
        //     240: aconst_null
        //     241: astore 12
        //     243: goto -185 -> 58
        //     246: ldc 94
        //     248: new 188	java/lang/StringBuilder
        //     251: dup
        //     252: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     255: ldc_w 1672
        //     258: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     261: aload_0
        //     262: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     265: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     268: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     271: invokestatic 775	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     274: pop
        //     275: goto -56 -> 219
        //     278: astore 9
        //     280: ldc 94
        //     282: new 188	java/lang/StringBuilder
        //     285: dup
        //     286: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     289: ldc_w 1353
        //     292: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     295: aload_0
        //     296: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     299: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     302: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     305: aload 9
        //     307: invokestatic 628	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     310: pop
        //     311: goto -92 -> 219
        //     314: iconst_1
        //     315: anewarray 126	java/lang/String
        //     318: astore 11
        //     320: aconst_null
        //     321: astore 12
        //     323: aconst_null
        //     324: astore 13
        //     326: aload_0
        //     327: aload 6
        //     329: aload 5
        //     331: iload 4
        //     333: aload 11
        //     335: invokespecial 1674	android/content/pm/PackageParser:parsePackage	(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Package;
        //     338: astore 18
        //     340: aload 18
        //     342: astore 12
        //     344: aload 12
        //     346: ifnonnull +137 -> 483
        //     349: aload_0
        //     350: getfield 1079	android/content/pm/PackageParser:mOnlyCoreApps	Z
        //     353: ifeq +11 -> 364
        //     356: aload_0
        //     357: getfield 168	android/content/pm/PackageParser:mParseError	I
        //     360: iconst_1
        //     361: if_icmpeq +34 -> 395
        //     364: aload 13
        //     366: ifnull +62 -> 428
        //     369: ldc 94
        //     371: aload_0
        //     372: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     375: aload 13
        //     377: invokestatic 628	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     380: pop
        //     381: aload_0
        //     382: getfield 168	android/content/pm/PackageParser:mParseError	I
        //     385: iconst_1
        //     386: if_icmpne +9 -> 395
        //     389: aload_0
        //     390: bipush 148
        //     392: putfield 168	android/content/pm/PackageParser:mParseError	I
        //     395: aload 5
        //     397: invokeinterface 1347 1 0
        //     402: aload 8
        //     404: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     407: aconst_null
        //     408: astore 12
        //     410: goto -352 -> 58
        //     413: astore 14
        //     415: aload 14
        //     417: astore 13
        //     419: aload_0
        //     420: bipush 154
        //     422: putfield 168	android/content/pm/PackageParser:mParseError	I
        //     425: goto -81 -> 344
        //     428: ldc 94
        //     430: new 188	java/lang/StringBuilder
        //     433: dup
        //     434: invokespecial 189	java/lang/StringBuilder:<init>	()V
        //     437: aload_0
        //     438: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     441: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     444: ldc_w 1676
        //     447: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     450: aload 5
        //     452: invokeinterface 1243 1 0
        //     457: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     460: ldc_w 1678
        //     463: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     466: aload 11
        //     468: iconst_0
        //     469: aaload
        //     470: invokevirtual 195	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     473: invokevirtual 199	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     476: invokestatic 775	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     479: pop
        //     480: goto -99 -> 381
        //     483: aload 5
        //     485: invokeinterface 1347 1 0
        //     490: aload 8
        //     492: invokevirtual 1348	android/content/res/AssetManager:close	()V
        //     495: aload 12
        //     497: aload_2
        //     498: putfield 1681	android/content/pm/PackageParser$Package:mPath	Ljava/lang/String;
        //     501: aload_0
        //     502: getfield 170	android/content/pm/PackageParser:mArchiveSourcePath	Ljava/lang/String;
        //     505: astore 15
        //     507: aload 12
        //     509: aload 15
        //     511: putfield 1684	android/content/pm/PackageParser$Package:mScanPath	Ljava/lang/String;
        //     514: aload 12
        //     516: aconst_null
        //     517: putfield 523	android/content/pm/PackageParser$Package:mSignatures	[Landroid/content/pm/Signature;
        //     520: goto -462 -> 58
        //     523: astore 9
        //     525: aconst_null
        //     526: astore 8
        //     528: goto -248 -> 280
        //     531: astore 9
        //     533: aload 20
        //     535: astore 6
        //     537: goto -257 -> 280
        //
        // Exception table:
        //     from	to	target	type
        //     144	173	278	java/lang/Exception
        //     246	275	278	java/lang/Exception
        //     326	340	413	java/lang/Exception
        //     135	144	523	java/lang/Exception
        //     173	208	531	java/lang/Exception
    }

    public void setOnlyCoreApps(boolean paramBoolean)
    {
        this.mOnlyCoreApps = paramBoolean;
    }

    public void setSeparateProcesses(String[] paramArrayOfString)
    {
        this.mSeparateProcesses = paramArrayOfString;
    }

    public static final class ServiceIntentInfo extends PackageParser.IntentInfo
    {
        public final PackageParser.Service service;

        public ServiceIntentInfo(PackageParser.Service paramService)
        {
            this.service = paramService;
        }

        public String toString()
        {
            return "ServiceIntentInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.service.info.name + "}";
        }
    }

    public static final class ActivityIntentInfo extends PackageParser.IntentInfo
    {
        public final PackageParser.Activity activity;

        public ActivityIntentInfo(PackageParser.Activity paramActivity)
        {
            this.activity = paramActivity;
        }

        public String toString()
        {
            return "ActivityIntentInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.activity.info.name + "}";
        }
    }

    public static class IntentInfo extends IntentFilter
    {
        public boolean hasDefault;
        public int icon;
        public int labelRes;
        public int logo;
        public CharSequence nonLocalizedLabel;
    }

    public static final class Instrumentation extends PackageParser.Component
    {
        public final InstrumentationInfo info;

        public Instrumentation(PackageParser.ParsePackageItemArgs paramParsePackageItemArgs, InstrumentationInfo paramInstrumentationInfo)
        {
            super(paramInstrumentationInfo);
            this.info = paramInstrumentationInfo;
        }

        public void setPackageName(String paramString)
        {
            super.setPackageName(paramString);
            this.info.packageName = paramString;
        }

        public String toString()
        {
            return "Instrumentation{" + Integer.toHexString(System.identityHashCode(this)) + " " + getComponentShortName() + "}";
        }
    }

    public static final class Provider extends PackageParser.Component
    {
        public final ProviderInfo info;
        public boolean syncable;

        public Provider(PackageParser.ParseComponentArgs paramParseComponentArgs, ProviderInfo paramProviderInfo)
        {
            super(paramProviderInfo);
            this.info = paramProviderInfo;
            this.info.applicationInfo = paramParseComponentArgs.owner.applicationInfo;
            this.syncable = false;
        }

        public Provider(Provider paramProvider)
        {
            super();
            this.info = paramProvider.info;
            this.syncable = paramProvider.syncable;
        }

        public void setPackageName(String paramString)
        {
            super.setPackageName(paramString);
            this.info.packageName = paramString;
        }

        public String toString()
        {
            return "Provider{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.info.name + "}";
        }
    }

    public static final class Service extends PackageParser.Component<PackageParser.ServiceIntentInfo>
    {
        public final ServiceInfo info;

        public Service(PackageParser.ParseComponentArgs paramParseComponentArgs, ServiceInfo paramServiceInfo)
        {
            super(paramServiceInfo);
            this.info = paramServiceInfo;
            this.info.applicationInfo = paramParseComponentArgs.owner.applicationInfo;
        }

        public void setPackageName(String paramString)
        {
            super.setPackageName(paramString);
            this.info.packageName = paramString;
        }

        public String toString()
        {
            return "Service{" + Integer.toHexString(System.identityHashCode(this)) + " " + getComponentShortName() + "}";
        }
    }

    public static final class Activity extends PackageParser.Component<PackageParser.ActivityIntentInfo>
    {
        public final ActivityInfo info;

        public Activity(PackageParser.ParseComponentArgs paramParseComponentArgs, ActivityInfo paramActivityInfo)
        {
            super(paramActivityInfo);
            this.info = paramActivityInfo;
            this.info.applicationInfo = paramParseComponentArgs.owner.applicationInfo;
        }

        public void setPackageName(String paramString)
        {
            super.setPackageName(paramString);
            this.info.packageName = paramString;
        }

        public String toString()
        {
            return "Activity{" + Integer.toHexString(System.identityHashCode(this)) + " " + getComponentShortName() + "}";
        }
    }

    public static final class PermissionGroup extends PackageParser.Component<PackageParser.IntentInfo>
    {
        public final PermissionGroupInfo info;

        public PermissionGroup(PackageParser.Package paramPackage)
        {
            super();
            this.info = new PermissionGroupInfo();
        }

        public PermissionGroup(PackageParser.Package paramPackage, PermissionGroupInfo paramPermissionGroupInfo)
        {
            super();
            this.info = paramPermissionGroupInfo;
        }

        public void setPackageName(String paramString)
        {
            super.setPackageName(paramString);
            this.info.packageName = paramString;
        }

        public String toString()
        {
            return "PermissionGroup{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.info.name + "}";
        }
    }

    public static final class Permission extends PackageParser.Component<PackageParser.IntentInfo>
    {
        public PackageParser.PermissionGroup group;
        public final PermissionInfo info;
        public boolean tree;

        public Permission(PackageParser.Package paramPackage)
        {
            super();
            this.info = new PermissionInfo();
        }

        public Permission(PackageParser.Package paramPackage, PermissionInfo paramPermissionInfo)
        {
            super();
            this.info = paramPermissionInfo;
        }

        public void setPackageName(String paramString)
        {
            super.setPackageName(paramString);
            this.info.packageName = paramString;
        }

        public String toString()
        {
            return "Permission{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.info.name + "}";
        }
    }

    public static class Component<II extends PackageParser.IntentInfo>
    {
        public final String className;
        ComponentName componentName;
        String componentShortName;
        public final ArrayList<II> intents;
        public Bundle metaData;
        public final PackageParser.Package owner;

        public Component(Component<II> paramComponent)
        {
            this.owner = paramComponent.owner;
            this.intents = paramComponent.intents;
            this.className = paramComponent.className;
            this.componentName = paramComponent.componentName;
            this.componentShortName = paramComponent.componentShortName;
        }

        public Component(PackageParser.Package paramPackage)
        {
            this.owner = paramPackage;
            this.intents = null;
            this.className = null;
        }

        public Component(PackageParser.ParseComponentArgs paramParseComponentArgs, ComponentInfo paramComponentInfo)
        {
            this(paramParseComponentArgs, paramComponentInfo);
            if (paramParseComponentArgs.outError[0] != null)
                return;
            if (paramParseComponentArgs.processRes != 0)
                if (this.owner.applicationInfo.targetSdkVersion < 8)
                    break label133;
            label133: for (String str = paramParseComponentArgs.sa.getNonConfigurationString(paramParseComponentArgs.processRes, 0); ; str = paramParseComponentArgs.sa.getNonResourceString(paramParseComponentArgs.processRes))
            {
                paramComponentInfo.processName = PackageParser.buildProcessName(this.owner.applicationInfo.packageName, this.owner.applicationInfo.processName, str, paramParseComponentArgs.flags, paramParseComponentArgs.sepProcesses, paramParseComponentArgs.outError);
                if (paramParseComponentArgs.descriptionRes != 0)
                    paramComponentInfo.descriptionRes = paramParseComponentArgs.sa.getResourceId(paramParseComponentArgs.descriptionRes, 0);
                paramComponentInfo.enabled = paramParseComponentArgs.sa.getBoolean(paramParseComponentArgs.enabledRes, true);
                break;
            }
        }

        public Component(PackageParser.ParsePackageItemArgs paramParsePackageItemArgs, PackageItemInfo paramPackageItemInfo)
        {
            this.owner = paramParsePackageItemArgs.owner;
            this.intents = new ArrayList(0);
            String str = paramParsePackageItemArgs.sa.getNonConfigurationString(paramParsePackageItemArgs.nameRes, 0);
            if (str == null)
            {
                this.className = null;
                paramParsePackageItemArgs.outError[0] = (paramParsePackageItemArgs.tag + " does not specify android:name");
            }
            while (true)
            {
                return;
                paramPackageItemInfo.name = PackageParser.buildClassName(this.owner.applicationInfo.packageName, str, paramParsePackageItemArgs.outError);
                if (paramPackageItemInfo.name == null)
                {
                    this.className = null;
                    paramParsePackageItemArgs.outError[0] = (paramParsePackageItemArgs.tag + " does not have valid android:name");
                }
                else
                {
                    this.className = paramPackageItemInfo.name;
                    int i = paramParsePackageItemArgs.sa.getResourceId(paramParsePackageItemArgs.iconRes, 0);
                    if (i != 0)
                    {
                        paramPackageItemInfo.icon = i;
                        paramPackageItemInfo.nonLocalizedLabel = null;
                    }
                    int j = paramParsePackageItemArgs.sa.getResourceId(paramParsePackageItemArgs.logoRes, 0);
                    if (j != 0)
                        paramPackageItemInfo.logo = j;
                    TypedValue localTypedValue = paramParsePackageItemArgs.sa.peekValue(paramParsePackageItemArgs.labelRes);
                    if (localTypedValue != null)
                    {
                        int k = localTypedValue.resourceId;
                        paramPackageItemInfo.labelRes = k;
                        if (k == 0)
                            paramPackageItemInfo.nonLocalizedLabel = localTypedValue.coerceToString();
                    }
                    paramPackageItemInfo.packageName = this.owner.packageName;
                }
            }
        }

        public ComponentName getComponentName()
        {
            if (this.componentName != null);
            for (ComponentName localComponentName = this.componentName; ; localComponentName = this.componentName)
            {
                return localComponentName;
                if (this.className != null)
                    this.componentName = new ComponentName(this.owner.applicationInfo.packageName, this.className);
            }
        }

        public String getComponentShortName()
        {
            if (this.componentShortName != null);
            for (String str = this.componentShortName; ; str = this.componentShortName)
            {
                return str;
                ComponentName localComponentName = getComponentName();
                if (localComponentName != null)
                    this.componentShortName = localComponentName.flattenToShortString();
            }
        }

        public void setPackageName(String paramString)
        {
            this.componentName = null;
            this.componentShortName = null;
        }
    }

    public static final class Package
    {
        public final ArrayList<PackageParser.Activity> activities = new ArrayList(0);
        public final ApplicationInfo applicationInfo = new ApplicationInfo();
        public final ArrayList<ConfigurationInfo> configPreferences = new ArrayList();
        public int installLocation;
        public final ArrayList<PackageParser.Instrumentation> instrumentation = new ArrayList(0);
        public ArrayList<String> mAdoptPermissions = null;
        public Bundle mAppMetaData = null;
        public boolean mDidDexOpt;
        public Object mExtras;
        public boolean mOperationPending;
        public ArrayList<String> mOriginalPackages = null;
        public String mPath;
        public int mPreferredOrder = 0;
        public String mRealPackage = null;
        public String mScanPath;
        public String mSharedUserId;
        public int mSharedUserLabel;
        public Signature[] mSignatures;
        public int mVersionCode;
        public String mVersionName;
        public ManifestDigest manifestDigest;
        public String packageName;
        public final ArrayList<PackageParser.PermissionGroup> permissionGroups = new ArrayList(0);
        public final ArrayList<PackageParser.Permission> permissions = new ArrayList(0);
        public ArrayList<String> protectedBroadcasts;
        public final ArrayList<PackageParser.Provider> providers = new ArrayList(0);
        public final ArrayList<PackageParser.Activity> receivers = new ArrayList(0);
        public ArrayList<FeatureInfo> reqFeatures = null;
        public final ArrayList<String> requestedPermissions = new ArrayList();
        public final ArrayList<Boolean> requestedPermissionsRequired = new ArrayList();
        public final ArrayList<PackageParser.Service> services = new ArrayList(0);
        public ArrayList<String> usesLibraries = null;
        public String[] usesLibraryFiles = null;
        public ArrayList<String> usesOptionalLibraries = null;

        public Package(String paramString)
        {
            this.packageName = paramString;
            this.applicationInfo.packageName = paramString;
            this.applicationInfo.uid = -1;
        }

        public boolean hasComponentClassName(String paramString)
        {
            int i = -1 + this.activities.size();
            boolean bool;
            if (i >= 0)
                if (paramString.equals(((PackageParser.Activity)this.activities.get(i)).className))
                    bool = true;
            while (true)
            {
                return bool;
                i--;
                break;
                for (int j = -1 + this.receivers.size(); ; j--)
                {
                    if (j < 0)
                        break label96;
                    if (paramString.equals(((PackageParser.Activity)this.receivers.get(j)).className))
                    {
                        bool = true;
                        break;
                    }
                }
                label96: for (int k = -1 + this.providers.size(); ; k--)
                {
                    if (k < 0)
                        break label147;
                    if (paramString.equals(((PackageParser.Provider)this.providers.get(k)).className))
                    {
                        bool = true;
                        break;
                    }
                }
                label147: for (int m = -1 + this.services.size(); ; m--)
                {
                    if (m < 0)
                        break label198;
                    if (paramString.equals(((PackageParser.Service)this.services.get(m)).className))
                    {
                        bool = true;
                        break;
                    }
                }
                label198: for (int n = -1 + this.instrumentation.size(); ; n--)
                {
                    if (n < 0)
                        break label249;
                    if (paramString.equals(((PackageParser.Instrumentation)this.instrumentation.get(n)).className))
                    {
                        bool = true;
                        break;
                    }
                }
                label249: bool = false;
            }
        }

        public void setPackageName(String paramString)
        {
            this.packageName = paramString;
            this.applicationInfo.packageName = paramString;
            for (int i = -1 + this.permissions.size(); i >= 0; i--)
                ((PackageParser.Permission)this.permissions.get(i)).setPackageName(paramString);
            for (int j = -1 + this.permissionGroups.size(); j >= 0; j--)
                ((PackageParser.PermissionGroup)this.permissionGroups.get(j)).setPackageName(paramString);
            for (int k = -1 + this.activities.size(); k >= 0; k--)
                ((PackageParser.Activity)this.activities.get(k)).setPackageName(paramString);
            for (int m = -1 + this.receivers.size(); m >= 0; m--)
                ((PackageParser.Activity)this.receivers.get(m)).setPackageName(paramString);
            for (int n = -1 + this.providers.size(); n >= 0; n--)
                ((PackageParser.Provider)this.providers.get(n)).setPackageName(paramString);
            for (int i1 = -1 + this.services.size(); i1 >= 0; i1--)
                ((PackageParser.Service)this.services.get(i1)).setPackageName(paramString);
            for (int i2 = -1 + this.instrumentation.size(); i2 >= 0; i2--)
                ((PackageParser.Instrumentation)this.instrumentation.get(i2)).setPackageName(paramString);
        }

        public String toString()
        {
            return "Package{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
        }
    }

    public static class PackageLite
    {
        public final int installLocation;
        public final String packageName;
        public final VerifierInfo[] verifiers;

        public PackageLite(String paramString, int paramInt, List<VerifierInfo> paramList)
        {
            this.packageName = paramString;
            this.installLocation = paramInt;
            this.verifiers = ((VerifierInfo[])paramList.toArray(new VerifierInfo[paramList.size()]));
        }
    }

    static class ParseComponentArgs extends PackageParser.ParsePackageItemArgs
    {
        final int descriptionRes;
        final int enabledRes;
        int flags;
        final int processRes;
        final String[] sepProcesses;

        ParseComponentArgs(PackageParser.Package paramPackage, String[] paramArrayOfString1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String[] paramArrayOfString2, int paramInt5, int paramInt6, int paramInt7)
        {
            super(paramArrayOfString1, paramInt1, paramInt2, paramInt3, paramInt4);
            this.sepProcesses = paramArrayOfString2;
            this.processRes = paramInt5;
            this.descriptionRes = paramInt6;
            this.enabledRes = paramInt7;
        }
    }

    static class ParsePackageItemArgs
    {
        final int iconRes;
        final int labelRes;
        final int logoRes;
        final int nameRes;
        final String[] outError;
        final PackageParser.Package owner;
        TypedArray sa;
        String tag;

        ParsePackageItemArgs(PackageParser.Package paramPackage, String[] paramArrayOfString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.owner = paramPackage;
            this.outError = paramArrayOfString;
            this.nameRes = paramInt1;
            this.labelRes = paramInt2;
            this.iconRes = paramInt3;
            this.logoRes = paramInt4;
        }
    }

    public static class SplitPermissionInfo
    {
        public final String[] newPerms;
        public final String rootPerm;
        public final int targetSdk;

        public SplitPermissionInfo(String paramString, String[] paramArrayOfString, int paramInt)
        {
            this.rootPerm = paramString;
            this.newPerms = paramArrayOfString;
            this.targetSdk = paramInt;
        }
    }

    public static class NewPermissionInfo
    {
        public final int fileVersion;
        public final String name;
        public final int sdkVersion;

        public NewPermissionInfo(String paramString, int paramInt1, int paramInt2)
        {
            this.name = paramString;
            this.sdkVersion = paramInt1;
            this.fileVersion = paramInt2;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static int checkPriority(int paramInt1, int paramInt2)
        {
            if ((paramInt1 & 0x1) == 0)
            {
                if (paramInt2 < 1000)
                    break label19;
                paramInt2 = 999;
            }
            while (true)
            {
                return paramInt2;
                label19: if (paramInt2 <= -1000)
                    paramInt2 = -999;
            }
        }

        static String filterNameError(String paramString1, String paramString2)
        {
            if ("miui".equals(paramString1))
                paramString2 = null;
            return paramString2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PackageParser
 * JD-Core Version:        0.6.2
 */