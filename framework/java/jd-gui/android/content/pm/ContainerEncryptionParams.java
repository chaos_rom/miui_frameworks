package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Slog;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class ContainerEncryptionParams
    implements Parcelable
{
    public static final Parcelable.Creator<ContainerEncryptionParams> CREATOR = new Parcelable.Creator()
    {
        public ContainerEncryptionParams createFromParcel(Parcel paramAnonymousParcel)
        {
            try
            {
                localContainerEncryptionParams = new ContainerEncryptionParams(paramAnonymousParcel, null);
                return localContainerEncryptionParams;
            }
            catch (InvalidAlgorithmParameterException localInvalidAlgorithmParameterException)
            {
                while (true)
                {
                    Slog.e("ContainerEncryptionParams", "Invalid algorithm parameters specified", localInvalidAlgorithmParameterException);
                    ContainerEncryptionParams localContainerEncryptionParams = null;
                }
            }
        }

        public ContainerEncryptionParams[] newArray(int paramAnonymousInt)
        {
            return new ContainerEncryptionParams[paramAnonymousInt];
        }
    };
    private static final int ENC_PARAMS_IV_PARAMETERS = 1;
    private static final int MAC_PARAMS_NONE = 1;
    protected static final String TAG = "ContainerEncryptionParams";
    private static final String TO_STRING_PREFIX = "ContainerEncryptionParams{";
    private final long mAuthenticatedDataStart;
    private final long mDataEnd;
    private final long mEncryptedDataStart;
    private final String mEncryptionAlgorithm;
    private final SecretKey mEncryptionKey;
    private final IvParameterSpec mEncryptionSpec;
    private final String mMacAlgorithm;
    private final SecretKey mMacKey;
    private final AlgorithmParameterSpec mMacSpec;
    private final byte[] mMacTag;

    private ContainerEncryptionParams(Parcel paramParcel)
        throws InvalidAlgorithmParameterException
    {
        this.mEncryptionAlgorithm = paramParcel.readString();
        int i = paramParcel.readInt();
        byte[] arrayOfByte = paramParcel.createByteArray();
        this.mEncryptionKey = ((SecretKey)paramParcel.readSerializable());
        this.mMacAlgorithm = paramParcel.readString();
        int j = paramParcel.readInt();
        paramParcel.createByteArray();
        this.mMacKey = ((SecretKey)paramParcel.readSerializable());
        this.mMacTag = paramParcel.createByteArray();
        this.mAuthenticatedDataStart = paramParcel.readLong();
        this.mEncryptedDataStart = paramParcel.readLong();
        this.mDataEnd = paramParcel.readLong();
        switch (i)
        {
        default:
            throw new InvalidAlgorithmParameterException("Unknown parameter type " + i);
        case 1:
        }
        this.mEncryptionSpec = new IvParameterSpec(arrayOfByte);
        switch (j)
        {
        default:
            throw new InvalidAlgorithmParameterException("Unknown parameter type " + j);
        case 1:
        }
        this.mMacSpec = null;
        if (this.mEncryptionKey == null)
            throw new NullPointerException("encryptionKey == null");
    }

    public ContainerEncryptionParams(String paramString, AlgorithmParameterSpec paramAlgorithmParameterSpec, SecretKey paramSecretKey)
        throws InvalidAlgorithmParameterException
    {
        this(paramString, paramAlgorithmParameterSpec, paramSecretKey, null, null, null, null, -1L, -1L, -1L);
    }

    public ContainerEncryptionParams(String paramString1, AlgorithmParameterSpec paramAlgorithmParameterSpec1, SecretKey paramSecretKey1, String paramString2, AlgorithmParameterSpec paramAlgorithmParameterSpec2, SecretKey paramSecretKey2, byte[] paramArrayOfByte, long paramLong1, long paramLong2, long paramLong3)
        throws InvalidAlgorithmParameterException
    {
        if (TextUtils.isEmpty(paramString1))
            throw new NullPointerException("algorithm == null");
        if (paramAlgorithmParameterSpec1 == null)
            throw new NullPointerException("encryptionSpec == null");
        if (paramSecretKey1 == null)
            throw new NullPointerException("encryptionKey == null");
        if ((!TextUtils.isEmpty(paramString2)) && (paramSecretKey2 == null))
            throw new NullPointerException("macKey == null");
        if (!(paramAlgorithmParameterSpec1 instanceof IvParameterSpec))
            throw new InvalidAlgorithmParameterException("Unknown parameter spec class; must be IvParameters");
        this.mEncryptionAlgorithm = paramString1;
        this.mEncryptionSpec = ((IvParameterSpec)paramAlgorithmParameterSpec1);
        this.mEncryptionKey = paramSecretKey1;
        this.mMacAlgorithm = paramString2;
        this.mMacSpec = paramAlgorithmParameterSpec2;
        this.mMacKey = paramSecretKey2;
        this.mMacTag = paramArrayOfByte;
        this.mAuthenticatedDataStart = paramLong1;
        this.mEncryptedDataStart = paramLong2;
        this.mDataEnd = paramLong3;
    }

    private static final boolean isSecretKeyEqual(SecretKey paramSecretKey1, SecretKey paramSecretKey2)
    {
        boolean bool = false;
        String str1 = paramSecretKey1.getFormat();
        String str2 = paramSecretKey2.getFormat();
        if (str1 == null)
            if (str1 == str2);
        while (true)
        {
            return bool;
            if (paramSecretKey1.getEncoded() == paramSecretKey2.getEncoded())
                do
                {
                    bool = true;
                    break;
                    if (!str1.equals(paramSecretKey2.getFormat()))
                        break;
                }
                while (Arrays.equals(paramSecretKey1.getEncoded(), paramSecretKey2.getEncoded()));
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof ContainerEncryptionParams))
            {
                bool = false;
            }
            else
            {
                ContainerEncryptionParams localContainerEncryptionParams = (ContainerEncryptionParams)paramObject;
                if ((this.mAuthenticatedDataStart != localContainerEncryptionParams.mAuthenticatedDataStart) || (this.mEncryptedDataStart != localContainerEncryptionParams.mEncryptedDataStart) || (this.mDataEnd != localContainerEncryptionParams.mDataEnd))
                    bool = false;
                else if ((!this.mEncryptionAlgorithm.equals(localContainerEncryptionParams.mEncryptionAlgorithm)) || (!this.mMacAlgorithm.equals(localContainerEncryptionParams.mMacAlgorithm)))
                    bool = false;
                else if ((!isSecretKeyEqual(this.mEncryptionKey, localContainerEncryptionParams.mEncryptionKey)) || (!isSecretKeyEqual(this.mMacKey, localContainerEncryptionParams.mMacKey)))
                    bool = false;
                else if ((!Arrays.equals(this.mEncryptionSpec.getIV(), localContainerEncryptionParams.mEncryptionSpec.getIV())) || (!Arrays.equals(this.mMacTag, localContainerEncryptionParams.mMacTag)) || (this.mMacSpec != localContainerEncryptionParams.mMacSpec))
                    bool = false;
            }
        }
    }

    public long getAuthenticatedDataStart()
    {
        return this.mAuthenticatedDataStart;
    }

    public long getDataEnd()
    {
        return this.mDataEnd;
    }

    public long getEncryptedDataStart()
    {
        return this.mEncryptedDataStart;
    }

    public String getEncryptionAlgorithm()
    {
        return this.mEncryptionAlgorithm;
    }

    public SecretKey getEncryptionKey()
    {
        return this.mEncryptionKey;
    }

    public AlgorithmParameterSpec getEncryptionSpec()
    {
        return this.mEncryptionSpec;
    }

    public String getMacAlgorithm()
    {
        return this.mMacAlgorithm;
    }

    public SecretKey getMacKey()
    {
        return this.mMacKey;
    }

    public AlgorithmParameterSpec getMacSpec()
    {
        return this.mMacSpec;
    }

    public byte[] getMacTag()
    {
        return this.mMacTag;
    }

    public int hashCode()
    {
        return (int)((int)((int)(3 + 5 * this.mEncryptionAlgorithm.hashCode() + 7 * Arrays.hashCode(this.mEncryptionSpec.getIV()) + 11 * this.mEncryptionKey.hashCode() + 13 * this.mMacAlgorithm.hashCode() + 17 * this.mMacKey.hashCode() + 19 * Arrays.hashCode(this.mMacTag) + 23L * this.mAuthenticatedDataStart) + 29L * this.mEncryptedDataStart) + 31L * this.mDataEnd);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("ContainerEncryptionParams{");
        localStringBuilder.append("mEncryptionAlgorithm=\"");
        localStringBuilder.append(this.mEncryptionAlgorithm);
        localStringBuilder.append("\",");
        localStringBuilder.append("mEncryptionSpec=");
        localStringBuilder.append(this.mEncryptionSpec.toString());
        localStringBuilder.append("mEncryptionKey=");
        localStringBuilder.append(this.mEncryptionKey.toString());
        localStringBuilder.append("mMacAlgorithm=\"");
        localStringBuilder.append(this.mMacAlgorithm);
        localStringBuilder.append("\",");
        localStringBuilder.append("mMacSpec=");
        localStringBuilder.append(this.mMacSpec.toString());
        localStringBuilder.append("mMacKey=");
        localStringBuilder.append(this.mMacKey.toString());
        localStringBuilder.append(",mAuthenticatedDataStart=");
        localStringBuilder.append(this.mAuthenticatedDataStart);
        localStringBuilder.append(",mEncryptedDataStart=");
        localStringBuilder.append(this.mEncryptedDataStart);
        localStringBuilder.append(",mDataEnd=");
        localStringBuilder.append(this.mDataEnd);
        localStringBuilder.append('}');
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mEncryptionAlgorithm);
        paramParcel.writeInt(1);
        paramParcel.writeByteArray(this.mEncryptionSpec.getIV());
        paramParcel.writeSerializable(this.mEncryptionKey);
        paramParcel.writeString(this.mMacAlgorithm);
        paramParcel.writeInt(1);
        paramParcel.writeByteArray(new byte[0]);
        paramParcel.writeSerializable(this.mMacKey);
        paramParcel.writeByteArray(this.mMacTag);
        paramParcel.writeLong(this.mAuthenticatedDataStart);
        paramParcel.writeLong(this.mEncryptedDataStart);
        paramParcel.writeLong(this.mDataEnd);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ContainerEncryptionParams
 * JD-Core Version:        0.6.2
 */