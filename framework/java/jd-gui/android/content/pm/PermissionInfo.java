package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public class PermissionInfo extends PackageItemInfo
    implements Parcelable
{
    public static final Parcelable.Creator<PermissionInfo> CREATOR = new Parcelable.Creator()
    {
        public PermissionInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PermissionInfo(paramAnonymousParcel, null);
        }

        public PermissionInfo[] newArray(int paramAnonymousInt)
        {
            return new PermissionInfo[paramAnonymousInt];
        }
    };
    public static final int PROTECTION_DANGEROUS = 1;
    public static final int PROTECTION_FLAG_DEVELOPMENT = 32;
    public static final int PROTECTION_FLAG_SYSTEM = 16;
    public static final int PROTECTION_MASK_BASE = 15;
    public static final int PROTECTION_MASK_FLAGS = 240;
    public static final int PROTECTION_NORMAL = 0;
    public static final int PROTECTION_SIGNATURE = 2;
    public static final int PROTECTION_SIGNATURE_OR_SYSTEM = 3;
    public int descriptionRes;
    public String group;
    public CharSequence nonLocalizedDescription;
    public int protectionLevel;

    public PermissionInfo()
    {
    }

    public PermissionInfo(PermissionInfo paramPermissionInfo)
    {
        super(paramPermissionInfo);
        this.group = paramPermissionInfo.group;
        this.descriptionRes = paramPermissionInfo.descriptionRes;
        this.protectionLevel = paramPermissionInfo.protectionLevel;
        this.nonLocalizedDescription = paramPermissionInfo.nonLocalizedDescription;
    }

    private PermissionInfo(Parcel paramParcel)
    {
        super(paramParcel);
        this.group = paramParcel.readString();
        this.descriptionRes = paramParcel.readInt();
        this.protectionLevel = paramParcel.readInt();
        this.nonLocalizedDescription = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    }

    public static int fixProtectionLevel(int paramInt)
    {
        if (paramInt == 3)
            paramInt = 18;
        return paramInt;
    }

    public static String protectionToString(int paramInt)
    {
        String str = "????";
        switch (paramInt & 0xF)
        {
        default:
        case 1:
        case 0:
        case 2:
        case 3:
        }
        while (true)
        {
            if ((paramInt & 0x10) != 0)
                str = str + "|system";
            if ((paramInt & 0x20) != 0)
                str = str + "|development";
            return str;
            str = "dangerous";
            continue;
            str = "normal";
            continue;
            str = "signature";
            continue;
            str = "signatureOrSystem";
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public CharSequence loadDescription(PackageManager paramPackageManager)
    {
        CharSequence localCharSequence;
        if (this.nonLocalizedDescription != null)
            localCharSequence = this.nonLocalizedDescription;
        while (true)
        {
            return localCharSequence;
            if (this.descriptionRes != 0)
            {
                localCharSequence = paramPackageManager.getText(this.packageName, this.descriptionRes, null);
                if (localCharSequence != null);
            }
            else
            {
                localCharSequence = null;
            }
        }
    }

    public String toString()
    {
        return "PermissionInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.name + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.group);
        paramParcel.writeInt(this.descriptionRes);
        paramParcel.writeInt(this.protectionLevel);
        TextUtils.writeToParcel(this.nonLocalizedDescription, paramParcel, paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PermissionInfo
 * JD-Core Version:        0.6.2
 */