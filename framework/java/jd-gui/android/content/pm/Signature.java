package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.ByteArrayInputStream;
import java.lang.ref.SoftReference;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Arrays;

public class Signature
    implements Parcelable
{
    public static final Parcelable.Creator<Signature> CREATOR = new Parcelable.Creator()
    {
        public Signature createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Signature(paramAnonymousParcel, null);
        }

        public Signature[] newArray(int paramAnonymousInt)
        {
            return new Signature[paramAnonymousInt];
        }
    };
    private int mHashCode;
    private boolean mHaveHashCode;
    private final byte[] mSignature;
    private SoftReference<String> mStringRef;

    private Signature(Parcel paramParcel)
    {
        this.mSignature = paramParcel.createByteArray();
    }

    public Signature(String paramString)
    {
        byte[] arrayOfByte1 = paramString.getBytes();
        int i = arrayOfByte1.length;
        if (i % 2 != 0)
            throw new IllegalArgumentException("text size " + i + " is not even");
        byte[] arrayOfByte2 = new byte[i / 2];
        int j = 0;
        int i2;
        for (int k = 0; j < i; k = i2)
        {
            int m = j + 1;
            int n = parseHexDigit(arrayOfByte1[j]);
            j = m + 1;
            int i1 = parseHexDigit(arrayOfByte1[m]);
            i2 = k + 1;
            arrayOfByte2[k] = ((byte)(i1 | n << 4));
        }
        this.mSignature = arrayOfByte2;
    }

    public Signature(byte[] paramArrayOfByte)
    {
        this.mSignature = ((byte[])paramArrayOfByte.clone());
    }

    private static final int parseHexDigit(int paramInt)
    {
        int i;
        if ((48 <= paramInt) && (paramInt <= 57))
            i = paramInt - 48;
        while (true)
        {
            return i;
            if ((97 <= paramInt) && (paramInt <= 102))
            {
                i = 10 + (paramInt - 97);
            }
            else
            {
                if ((65 > paramInt) || (paramInt > 70))
                    break;
                i = 10 + (paramInt - 65);
            }
        }
        throw new IllegalArgumentException("Invalid character " + paramInt + " in hex string");
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool1 = false;
        if (paramObject != null);
        try
        {
            Signature localSignature = (Signature)paramObject;
            if (this != localSignature)
            {
                boolean bool2 = Arrays.equals(this.mSignature, localSignature.mSignature);
                if (!bool2);
            }
            else
            {
                bool1 = true;
            }
            label39: return bool1;
        }
        catch (ClassCastException localClassCastException)
        {
            break label39;
        }
    }

    public PublicKey getPublicKey()
        throws CertificateException
    {
        return CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(this.mSignature)).getPublicKey();
    }

    public int hashCode()
    {
        if (this.mHaveHashCode);
        for (int i = this.mHashCode; ; i = this.mHashCode)
        {
            return i;
            this.mHashCode = Arrays.hashCode(this.mSignature);
            this.mHaveHashCode = true;
        }
    }

    public byte[] toByteArray()
    {
        byte[] arrayOfByte = new byte[this.mSignature.length];
        System.arraycopy(this.mSignature, 0, arrayOfByte, 0, this.mSignature.length);
        return arrayOfByte;
    }

    public char[] toChars()
    {
        return toChars(null, null);
    }

    public char[] toChars(char[] paramArrayOfChar, int[] paramArrayOfInt)
    {
        byte[] arrayOfByte = this.mSignature;
        int i = arrayOfByte.length;
        int j = i * 2;
        char[] arrayOfChar;
        int k;
        label35: int n;
        int i2;
        label80: int i3;
        int i4;
        if ((paramArrayOfChar == null) || (j > paramArrayOfChar.length))
        {
            arrayOfChar = new char[j];
            k = 0;
            if (k >= i)
                break label160;
            int m = arrayOfByte[k];
            n = 0xF & m >> 4;
            int i1 = k * 2;
            if (n < 10)
                break label140;
            i2 = -10 + (n + 97);
            arrayOfChar[i1] = ((char)i2);
            i3 = m & 0xF;
            i4 = 1 + k * 2;
            if (i3 < 10)
                break label150;
        }
        label140: label150: for (int i5 = -10 + (i3 + 97); ; i5 = i3 + 48)
        {
            arrayOfChar[i4] = ((char)i5);
            k++;
            break label35;
            arrayOfChar = paramArrayOfChar;
            break;
            i2 = n + 48;
            break label80;
        }
        label160: if (paramArrayOfInt != null)
            paramArrayOfInt[0] = i;
        return arrayOfChar;
    }

    public String toCharsString()
    {
        String str1;
        if (this.mStringRef == null)
        {
            str1 = null;
            if (str1 == null)
                break label31;
        }
        label31: String str2;
        for (Object localObject = str1; ; localObject = str2)
        {
            return localObject;
            str1 = (String)this.mStringRef.get();
            break;
            str2 = new String(toChars());
            this.mStringRef = new SoftReference(str2);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeByteArray(this.mSignature);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.Signature
 * JD-Core Version:        0.6.2
 */