package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class InstrumentationInfo extends PackageItemInfo
    implements Parcelable
{
    public static final Parcelable.Creator<InstrumentationInfo> CREATOR = new Parcelable.Creator()
    {
        public InstrumentationInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new InstrumentationInfo(paramAnonymousParcel, null);
        }

        public InstrumentationInfo[] newArray(int paramAnonymousInt)
        {
            return new InstrumentationInfo[paramAnonymousInt];
        }
    };
    public String dataDir;
    public boolean functionalTest;
    public boolean handleProfiling;
    public String nativeLibraryDir;
    public String publicSourceDir;
    public String sourceDir;
    public String targetPackage;

    public InstrumentationInfo()
    {
    }

    public InstrumentationInfo(InstrumentationInfo paramInstrumentationInfo)
    {
        super(paramInstrumentationInfo);
        this.targetPackage = paramInstrumentationInfo.targetPackage;
        this.sourceDir = paramInstrumentationInfo.sourceDir;
        this.publicSourceDir = paramInstrumentationInfo.publicSourceDir;
        this.dataDir = paramInstrumentationInfo.dataDir;
        this.nativeLibraryDir = paramInstrumentationInfo.nativeLibraryDir;
        this.handleProfiling = paramInstrumentationInfo.handleProfiling;
        this.functionalTest = paramInstrumentationInfo.functionalTest;
    }

    private InstrumentationInfo(Parcel paramParcel)
    {
        super(paramParcel);
        this.targetPackage = paramParcel.readString();
        this.sourceDir = paramParcel.readString();
        this.publicSourceDir = paramParcel.readString();
        this.dataDir = paramParcel.readString();
        this.nativeLibraryDir = paramParcel.readString();
        boolean bool2;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.handleProfiling = bool2;
            if (paramParcel.readInt() == 0)
                break label79;
        }
        while (true)
        {
            this.functionalTest = bool1;
            return;
            bool2 = false;
            break;
            label79: bool1 = false;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "InstrumentationInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 0;
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.targetPackage);
        paramParcel.writeString(this.sourceDir);
        paramParcel.writeString(this.publicSourceDir);
        paramParcel.writeString(this.dataDir);
        paramParcel.writeString(this.nativeLibraryDir);
        int j;
        if (!this.handleProfiling)
        {
            j = 0;
            paramParcel.writeInt(j);
            if (this.functionalTest)
                break label83;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 1;
            break;
            label83: i = 1;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.InstrumentationInfo
 * JD-Core Version:        0.6.2
 */