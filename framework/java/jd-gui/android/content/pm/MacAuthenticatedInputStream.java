package android.content.pm;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.crypto.Mac;

public class MacAuthenticatedInputStream extends FilterInputStream
{
    private final Mac mMac;

    public MacAuthenticatedInputStream(InputStream paramInputStream, Mac paramMac)
    {
        super(paramInputStream);
        this.mMac = paramMac;
    }

    public boolean isTagEqual(byte[] paramArrayOfByte)
    {
        boolean bool = false;
        byte[] arrayOfByte = this.mMac.doFinal();
        if ((paramArrayOfByte == null) || (arrayOfByte == null) || (paramArrayOfByte.length != arrayOfByte.length));
        while (true)
        {
            return bool;
            int i = 0;
            for (int j = 0; j < paramArrayOfByte.length; j++)
                i |= paramArrayOfByte[j] ^ arrayOfByte[j];
            if (i == 0)
                bool = true;
        }
    }

    public int read()
        throws IOException
    {
        int i = super.read();
        if (i >= 0)
            this.mMac.update((byte)i);
        return i;
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        int i = super.read(paramArrayOfByte, paramInt1, paramInt2);
        if (i > 0)
            this.mMac.update(paramArrayOfByte, paramInt1, i);
        return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.MacAuthenticatedInputStream
 * JD-Core Version:        0.6.2
 */