package android.content;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISyncContext extends IInterface
{
    public abstract void onFinished(SyncResult paramSyncResult)
        throws RemoteException;

    public abstract void sendHeartbeat()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISyncContext
    {
        private static final String DESCRIPTOR = "android.content.ISyncContext";
        static final int TRANSACTION_onFinished = 2;
        static final int TRANSACTION_sendHeartbeat = 1;

        public Stub()
        {
            attachInterface(this, "android.content.ISyncContext");
        }

        public static ISyncContext asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.ISyncContext");
                if ((localIInterface != null) && ((localIInterface instanceof ISyncContext)))
                    localObject = (ISyncContext)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.content.ISyncContext");
                    continue;
                    paramParcel1.enforceInterface("android.content.ISyncContext");
                    sendHeartbeat();
                    paramParcel2.writeNoException();
                }
            case 2:
            }
            paramParcel1.enforceInterface("android.content.ISyncContext");
            if (paramParcel1.readInt() != 0);
            for (SyncResult localSyncResult = (SyncResult)SyncResult.CREATOR.createFromParcel(paramParcel1); ; localSyncResult = null)
            {
                onFinished(localSyncResult);
                paramParcel2.writeNoException();
                break;
            }
        }

        private static class Proxy
            implements ISyncContext
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.ISyncContext";
            }

            public void onFinished(SyncResult paramSyncResult)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.ISyncContext");
                    if (paramSyncResult != null)
                    {
                        localParcel1.writeInt(1);
                        paramSyncResult.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void sendHeartbeat()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.ISyncContext");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ISyncContext
 * JD-Core Version:        0.6.2
 */