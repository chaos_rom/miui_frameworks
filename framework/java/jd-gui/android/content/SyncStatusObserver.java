package android.content;

public abstract interface SyncStatusObserver
{
    public abstract void onStatusChanged(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncStatusObserver
 * JD-Core Version:        0.6.2
 */