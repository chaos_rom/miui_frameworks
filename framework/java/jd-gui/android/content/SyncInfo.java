package android.content;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SyncInfo
    implements Parcelable
{
    public static final Parcelable.Creator<SyncInfo> CREATOR = new Parcelable.Creator()
    {
        public SyncInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SyncInfo(paramAnonymousParcel);
        }

        public SyncInfo[] newArray(int paramAnonymousInt)
        {
            return new SyncInfo[paramAnonymousInt];
        }
    };
    public final Account account;
    public final String authority;
    public final int authorityId;
    public final long startTime;

    SyncInfo(int paramInt, Account paramAccount, String paramString, long paramLong)
    {
        this.authorityId = paramInt;
        this.account = paramAccount;
        this.authority = paramString;
        this.startTime = paramLong;
    }

    SyncInfo(Parcel paramParcel)
    {
        this.authorityId = paramParcel.readInt();
        this.account = new Account(paramParcel);
        this.authority = paramParcel.readString();
        this.startTime = paramParcel.readLong();
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.authorityId);
        this.account.writeToParcel(paramParcel, 0);
        paramParcel.writeString(this.authority);
        paramParcel.writeLong(this.startTime);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncInfo
 * JD-Core Version:        0.6.2
 */