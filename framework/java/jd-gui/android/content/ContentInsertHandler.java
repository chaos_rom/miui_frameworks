package android.content;

import java.io.IOException;
import java.io.InputStream;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public abstract interface ContentInsertHandler extends ContentHandler
{
    public abstract void insert(ContentResolver paramContentResolver, InputStream paramInputStream)
        throws IOException, SAXException;

    public abstract void insert(ContentResolver paramContentResolver, String paramString)
        throws SAXException;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentInsertHandler
 * JD-Core Version:        0.6.2
 */