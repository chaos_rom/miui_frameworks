package android.content;

import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ContentProviderOperation
    implements Parcelable
{
    public static final Parcelable.Creator<ContentProviderOperation> CREATOR = new Parcelable.Creator()
    {
        public ContentProviderOperation createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ContentProviderOperation(paramAnonymousParcel, null);
        }

        public ContentProviderOperation[] newArray(int paramAnonymousInt)
        {
            return new ContentProviderOperation[paramAnonymousInt];
        }
    };
    private static final String TAG = "ContentProviderOperation";
    public static final int TYPE_ASSERT = 4;
    public static final int TYPE_DELETE = 3;
    public static final int TYPE_INSERT = 1;
    public static final int TYPE_UPDATE = 2;
    private final Integer mExpectedCount;
    private final String mSelection;
    private final String[] mSelectionArgs;
    private final Map<Integer, Integer> mSelectionArgsBackReferences;
    private final int mType;
    private final Uri mUri;
    private final ContentValues mValues;
    private final ContentValues mValuesBackReferences;
    private final boolean mYieldAllowed;

    private ContentProviderOperation(Builder paramBuilder)
    {
        this.mType = paramBuilder.mType;
        this.mUri = paramBuilder.mUri;
        this.mValues = paramBuilder.mValues;
        this.mSelection = paramBuilder.mSelection;
        this.mSelectionArgs = paramBuilder.mSelectionArgs;
        this.mExpectedCount = paramBuilder.mExpectedCount;
        this.mSelectionArgsBackReferences = paramBuilder.mSelectionArgsBackReferences;
        this.mValuesBackReferences = paramBuilder.mValuesBackReferences;
        this.mYieldAllowed = paramBuilder.mYieldAllowed;
    }

    private ContentProviderOperation(Parcel paramParcel)
    {
        this.mType = paramParcel.readInt();
        this.mUri = ((Uri)Uri.CREATOR.createFromParcel(paramParcel));
        ContentValues localContentValues1;
        String str;
        label68: String[] arrayOfString;
        label87: Integer localInteger;
        if (paramParcel.readInt() != 0)
        {
            localContentValues1 = (ContentValues)ContentValues.CREATOR.createFromParcel(paramParcel);
            this.mValues = localContentValues1;
            if (paramParcel.readInt() == 0)
                break label220;
            str = paramParcel.readString();
            this.mSelection = str;
            if (paramParcel.readInt() == 0)
                break label226;
            arrayOfString = paramParcel.readStringArray();
            this.mSelectionArgs = arrayOfString;
            if (paramParcel.readInt() == 0)
                break label232;
            localInteger = Integer.valueOf(paramParcel.readInt());
            label109: this.mExpectedCount = localInteger;
            if (paramParcel.readInt() == 0)
                break label238;
        }
        label220: label226: label232: label238: for (ContentValues localContentValues2 = (ContentValues)ContentValues.CREATOR.createFromParcel(paramParcel); ; localContentValues2 = null)
        {
            this.mValuesBackReferences = localContentValues2;
            if (paramParcel.readInt() != 0)
                localHashMap = new HashMap();
            this.mSelectionArgsBackReferences = localHashMap;
            if (this.mSelectionArgsBackReferences == null)
                break label244;
            int i = paramParcel.readInt();
            for (int j = 0; j < i; j++)
                this.mSelectionArgsBackReferences.put(Integer.valueOf(paramParcel.readInt()), Integer.valueOf(paramParcel.readInt()));
            localContentValues1 = null;
            break;
            str = null;
            break label68;
            arrayOfString = null;
            break label87;
            localInteger = null;
            break label109;
        }
        label244: if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mYieldAllowed = bool;
            return;
        }
    }

    private long backRefToValue(ContentProviderResult[] paramArrayOfContentProviderResult, int paramInt, Integer paramInteger)
    {
        if (paramInteger.intValue() >= paramInt)
        {
            Log.e("ContentProviderOperation", toString());
            throw new ArrayIndexOutOfBoundsException("asked for back ref " + paramInteger + " but there are only " + paramInt + " back refs");
        }
        ContentProviderResult localContentProviderResult = paramArrayOfContentProviderResult[paramInteger.intValue()];
        if (localContentProviderResult.uri != null);
        for (long l = ContentUris.parseId(localContentProviderResult.uri); ; l = localContentProviderResult.count.intValue())
            return l;
    }

    public static Builder newAssertQuery(Uri paramUri)
    {
        return new Builder(4, paramUri, null);
    }

    public static Builder newDelete(Uri paramUri)
    {
        return new Builder(3, paramUri, null);
    }

    public static Builder newInsert(Uri paramUri)
    {
        return new Builder(1, paramUri, null);
    }

    public static Builder newUpdate(Uri paramUri)
    {
        return new Builder(2, paramUri, null);
    }

    public ContentProviderResult apply(ContentProvider paramContentProvider, ContentProviderResult[] paramArrayOfContentProviderResult, int paramInt)
        throws OperationApplicationException
    {
        ContentValues localContentValues = resolveValueBackReferences(paramArrayOfContentProviderResult, paramInt);
        String[] arrayOfString1 = resolveSelectionArgsBackReferences(paramArrayOfContentProviderResult, paramInt);
        Uri localUri;
        if (this.mType == 1)
        {
            localUri = paramContentProvider.insert(this.mUri, localContentValues);
            if (localUri == null)
                throw new OperationApplicationException("insert failed");
        }
        int i;
        for (ContentProviderResult localContentProviderResult = new ContentProviderResult(localUri); ; localContentProviderResult = new ContentProviderResult(i))
        {
            return localContentProviderResult;
            if (this.mType == 3)
                i = paramContentProvider.delete(this.mUri, this.mSelection, arrayOfString1);
            while ((this.mExpectedCount != null) && (this.mExpectedCount.intValue() != i))
            {
                Log.e("ContentProviderOperation", toString());
                throw new OperationApplicationException("wrong number of rows: " + i);
                if (this.mType == 2)
                {
                    i = paramContentProvider.update(this.mUri, localContentValues, this.mSelection, arrayOfString1);
                }
                else if (this.mType == 4)
                {
                    String[] arrayOfString2 = null;
                    if (localContentValues != null)
                    {
                        ArrayList localArrayList = new ArrayList();
                        Iterator localIterator = localContentValues.valueSet().iterator();
                        while (localIterator.hasNext())
                            localArrayList.add(((Map.Entry)localIterator.next()).getKey());
                        arrayOfString2 = (String[])localArrayList.toArray(new String[localArrayList.size()]);
                    }
                    Cursor localCursor = paramContentProvider.query(this.mUri, arrayOfString2, this.mSelection, arrayOfString1, null);
                    while (true)
                    {
                        int j;
                        try
                        {
                            i = localCursor.getCount();
                            if (arrayOfString2 == null)
                                break;
                            if (!localCursor.moveToNext())
                                break;
                            j = 0;
                            if (j >= arrayOfString2.length)
                                continue;
                            String str1 = localCursor.getString(j);
                            String str2 = localContentValues.getAsString(arrayOfString2[j]);
                            if (!TextUtils.equals(str1, str2))
                            {
                                Log.e("ContentProviderOperation", toString());
                                throw new OperationApplicationException("Found value " + str1 + " when expected " + str2 + " for column " + arrayOfString2[j]);
                            }
                        }
                        finally
                        {
                            localCursor.close();
                        }
                        j++;
                    }
                    localCursor.close();
                }
                else
                {
                    Log.e("ContentProviderOperation", toString());
                    throw new IllegalStateException("bad type, " + this.mType);
                }
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int getType()
    {
        return this.mType;
    }

    public Uri getUri()
    {
        return this.mUri;
    }

    public boolean isReadOperation()
    {
        if (this.mType == 4);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isWriteOperation()
    {
        int i = 1;
        if ((this.mType == 3) || (this.mType == i) || (this.mType == 2));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean isYieldAllowed()
    {
        return this.mYieldAllowed;
    }

    public String[] resolveSelectionArgsBackReferences(ContentProviderResult[] paramArrayOfContentProviderResult, int paramInt)
    {
        String[] arrayOfString;
        if (this.mSelectionArgsBackReferences == null)
            arrayOfString = this.mSelectionArgs;
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[this.mSelectionArgs.length];
            System.arraycopy(this.mSelectionArgs, 0, arrayOfString, 0, this.mSelectionArgs.length);
            Iterator localIterator = this.mSelectionArgsBackReferences.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                Integer localInteger = (Integer)localEntry.getKey();
                int i = ((Integer)localEntry.getValue()).intValue();
                arrayOfString[localInteger.intValue()] = String.valueOf(backRefToValue(paramArrayOfContentProviderResult, paramInt, Integer.valueOf(i)));
            }
        }
    }

    public ContentValues resolveValueBackReferences(ContentProviderResult[] paramArrayOfContentProviderResult, int paramInt)
    {
        ContentValues localContentValues;
        if (this.mValuesBackReferences == null)
            localContentValues = this.mValues;
        label167: 
        while (true)
        {
            return localContentValues;
            Iterator localIterator;
            if (this.mValues == null)
            {
                localContentValues = new ContentValues();
                localIterator = this.mValuesBackReferences.valueSet().iterator();
            }
            while (true)
            {
                if (!localIterator.hasNext())
                    break label167;
                String str = (String)((Map.Entry)localIterator.next()).getKey();
                Integer localInteger = this.mValuesBackReferences.getAsInteger(str);
                if (localInteger == null)
                {
                    Log.e("ContentProviderOperation", toString());
                    throw new IllegalArgumentException("values backref " + str + " is not an integer");
                    localContentValues = new ContentValues(this.mValues);
                    break;
                }
                localContentValues.put(str, Long.valueOf(backRefToValue(paramArrayOfContentProviderResult, paramInt, localInteger)));
            }
        }
    }

    public String toString()
    {
        return "mType: " + this.mType + ", mUri: " + this.mUri + ", mSelection: " + this.mSelection + ", mExpectedCount: " + this.mExpectedCount + ", mYieldAllowed: " + this.mYieldAllowed + ", mValues: " + this.mValues + ", mValuesBackReferences: " + this.mValuesBackReferences + ", mSelectionArgsBackReferences: " + this.mSelectionArgsBackReferences;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mType);
        Uri.writeToParcel(paramParcel, this.mUri);
        if (this.mValues != null)
        {
            paramParcel.writeInt(1);
            this.mValues.writeToParcel(paramParcel, 0);
            if (this.mSelection == null)
                break label229;
            paramParcel.writeInt(1);
            paramParcel.writeString(this.mSelection);
            label57: if (this.mSelectionArgs == null)
                break label237;
            paramParcel.writeInt(1);
            paramParcel.writeStringArray(this.mSelectionArgs);
            label77: if (this.mExpectedCount == null)
                break label245;
            paramParcel.writeInt(1);
            paramParcel.writeInt(this.mExpectedCount.intValue());
            label100: if (this.mValuesBackReferences == null)
                break label253;
            paramParcel.writeInt(1);
            this.mValuesBackReferences.writeToParcel(paramParcel, 0);
        }
        while (true)
        {
            if (this.mSelectionArgsBackReferences == null)
                break label261;
            paramParcel.writeInt(1);
            paramParcel.writeInt(this.mSelectionArgsBackReferences.size());
            Iterator localIterator = this.mSelectionArgsBackReferences.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                paramParcel.writeInt(((Integer)localEntry.getKey()).intValue());
                paramParcel.writeInt(((Integer)localEntry.getValue()).intValue());
            }
            paramParcel.writeInt(0);
            break;
            label229: paramParcel.writeInt(0);
            break label57;
            label237: paramParcel.writeInt(0);
            break label77;
            label245: paramParcel.writeInt(0);
            break label100;
            label253: paramParcel.writeInt(0);
        }
        label261: paramParcel.writeInt(0);
        if (this.mYieldAllowed);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }

    public static class Builder
    {
        private Integer mExpectedCount;
        private String mSelection;
        private String[] mSelectionArgs;
        private Map<Integer, Integer> mSelectionArgsBackReferences;
        private final int mType;
        private final Uri mUri;
        private ContentValues mValues;
        private ContentValues mValuesBackReferences;
        private boolean mYieldAllowed;

        private Builder(int paramInt, Uri paramUri)
        {
            if (paramUri == null)
                throw new IllegalArgumentException("uri must not be null");
            this.mType = paramInt;
            this.mUri = paramUri;
        }

        public ContentProviderOperation build()
        {
            if ((this.mType == 2) && ((this.mValues == null) || (this.mValues.size() == 0)) && ((this.mValuesBackReferences == null) || (this.mValuesBackReferences.size() == 0)))
                throw new IllegalArgumentException("Empty values");
            if ((this.mType == 4) && ((this.mValues == null) || (this.mValues.size() == 0)) && ((this.mValuesBackReferences == null) || (this.mValuesBackReferences.size() == 0)) && (this.mExpectedCount == null))
                throw new IllegalArgumentException("Empty values");
            return new ContentProviderOperation(this, null);
        }

        public Builder withExpectedCount(int paramInt)
        {
            if ((this.mType != 2) && (this.mType != 3) && (this.mType != 4))
                throw new IllegalArgumentException("only updates, deletes, and asserts can have expected counts");
            this.mExpectedCount = Integer.valueOf(paramInt);
            return this;
        }

        public Builder withSelection(String paramString, String[] paramArrayOfString)
        {
            if ((this.mType != 2) && (this.mType != 3) && (this.mType != 4))
                throw new IllegalArgumentException("only updates, deletes, and asserts can have selections");
            this.mSelection = paramString;
            if (paramArrayOfString == null)
                this.mSelectionArgs = null;
            while (true)
            {
                return this;
                this.mSelectionArgs = new String[paramArrayOfString.length];
                System.arraycopy(paramArrayOfString, 0, this.mSelectionArgs, 0, paramArrayOfString.length);
            }
        }

        public Builder withSelectionBackReference(int paramInt1, int paramInt2)
        {
            if ((this.mType != 2) && (this.mType != 3) && (this.mType != 4))
                throw new IllegalArgumentException("only updates, deletes, and asserts can have selection back-references");
            if (this.mSelectionArgsBackReferences == null)
                this.mSelectionArgsBackReferences = new HashMap();
            this.mSelectionArgsBackReferences.put(Integer.valueOf(paramInt1), Integer.valueOf(paramInt2));
            return this;
        }

        public Builder withValue(String paramString, Object paramObject)
        {
            if ((this.mType != 1) && (this.mType != 2) && (this.mType != 4))
                throw new IllegalArgumentException("only inserts and updates can have values");
            if (this.mValues == null)
                this.mValues = new ContentValues();
            if (paramObject == null)
                this.mValues.putNull(paramString);
            while (true)
            {
                return this;
                if ((paramObject instanceof String))
                {
                    this.mValues.put(paramString, (String)paramObject);
                }
                else if ((paramObject instanceof Byte))
                {
                    this.mValues.put(paramString, (Byte)paramObject);
                }
                else if ((paramObject instanceof Short))
                {
                    this.mValues.put(paramString, (Short)paramObject);
                }
                else if ((paramObject instanceof Integer))
                {
                    this.mValues.put(paramString, (Integer)paramObject);
                }
                else if ((paramObject instanceof Long))
                {
                    this.mValues.put(paramString, (Long)paramObject);
                }
                else if ((paramObject instanceof Float))
                {
                    this.mValues.put(paramString, (Float)paramObject);
                }
                else if ((paramObject instanceof Double))
                {
                    this.mValues.put(paramString, (Double)paramObject);
                }
                else if ((paramObject instanceof Boolean))
                {
                    this.mValues.put(paramString, (Boolean)paramObject);
                }
                else
                {
                    if (!(paramObject instanceof byte[]))
                        break;
                    this.mValues.put(paramString, (byte[])paramObject);
                }
            }
            throw new IllegalArgumentException("bad value type: " + paramObject.getClass().getName());
        }

        public Builder withValueBackReference(String paramString, int paramInt)
        {
            if ((this.mType != 1) && (this.mType != 2) && (this.mType != 4))
                throw new IllegalArgumentException("only inserts, updates, and asserts can have value back-references");
            if (this.mValuesBackReferences == null)
                this.mValuesBackReferences = new ContentValues();
            this.mValuesBackReferences.put(paramString, Integer.valueOf(paramInt));
            return this;
        }

        public Builder withValueBackReferences(ContentValues paramContentValues)
        {
            if ((this.mType != 1) && (this.mType != 2) && (this.mType != 4))
                throw new IllegalArgumentException("only inserts, updates, and asserts can have value back-references");
            this.mValuesBackReferences = paramContentValues;
            return this;
        }

        public Builder withValues(ContentValues paramContentValues)
        {
            if ((this.mType != 1) && (this.mType != 2) && (this.mType != 4))
                throw new IllegalArgumentException("only inserts, updates, and asserts can have values");
            if (this.mValues == null)
                this.mValues = new ContentValues();
            this.mValues.putAll(paramContentValues);
            return this;
        }

        public Builder withYieldAllowed(boolean paramBoolean)
        {
            this.mYieldAllowed = paramBoolean;
            return this;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentProviderOperation
 * JD-Core Version:        0.6.2
 */