package android.content;

import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R.styleable;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Intent
    implements Parcelable, Cloneable
{
    public static final String ACTION_ADVANCED_SETTINGS_CHANGED = "android.intent.action.ADVANCED_SETTINGS";
    public static final String ACTION_AIRPLANE_MODE_CHANGED = "android.intent.action.AIRPLANE_MODE";
    public static final String ACTION_ALARM_CHANGED = "android.intent.action.ALARM_CHANGED";
    public static final String ACTION_ALL_APPS = "android.intent.action.ALL_APPS";
    public static final String ACTION_ANALOG_AUDIO_DOCK_PLUG = "android.intent.action.ANALOG_AUDIO_DOCK_PLUG";
    public static final String ACTION_ANSWER = "android.intent.action.ANSWER";
    public static final String ACTION_APP_ERROR = "android.intent.action.APP_ERROR";
    public static final String ACTION_ASSIST = "android.intent.action.ASSIST";
    public static final String ACTION_ATTACH_DATA = "android.intent.action.ATTACH_DATA";
    public static final String ACTION_BATTERY_CHANGED = "android.intent.action.BATTERY_CHANGED";
    public static final String ACTION_BATTERY_LOW = "android.intent.action.BATTERY_LOW";
    public static final String ACTION_BATTERY_OKAY = "android.intent.action.BATTERY_OKAY";
    public static final String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    public static final String ACTION_BUG_REPORT = "android.intent.action.BUG_REPORT";
    public static final String ACTION_CALL = "android.intent.action.CALL";
    public static final String ACTION_CALL_BUTTON = "android.intent.action.CALL_BUTTON";
    public static final String ACTION_CALL_EMERGENCY = "android.intent.action.CALL_EMERGENCY";
    public static final String ACTION_CALL_PRIVILEGED = "android.intent.action.CALL_PRIVILEGED";
    public static final String ACTION_CAMERA_BUTTON = "android.intent.action.CAMERA_BUTTON";
    public static final String ACTION_CHOOSER = "android.intent.action.CHOOSER";
    public static final String ACTION_CLEAR_DNS_CACHE = "android.intent.action.CLEAR_DNS_CACHE";
    public static final String ACTION_CLOSE_SYSTEM_DIALOGS = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
    public static final String ACTION_CONFIGURATION_CHANGED = "android.intent.action.CONFIGURATION_CHANGED";
    public static final String ACTION_CREATE_SHORTCUT = "android.intent.action.CREATE_SHORTCUT";
    public static final String ACTION_DATE_CHANGED = "android.intent.action.DATE_CHANGED";
    public static final String ACTION_DEFAULT = "android.intent.action.VIEW";
    public static final String ACTION_DELETE = "android.intent.action.DELETE";
    public static final String ACTION_DEVICE_STORAGE_FULL = "android.intent.action.DEVICE_STORAGE_FULL";
    public static final String ACTION_DEVICE_STORAGE_LOW = "android.intent.action.DEVICE_STORAGE_LOW";
    public static final String ACTION_DEVICE_STORAGE_NOT_FULL = "android.intent.action.DEVICE_STORAGE_NOT_FULL";
    public static final String ACTION_DEVICE_STORAGE_OK = "android.intent.action.DEVICE_STORAGE_OK";
    public static final String ACTION_DIAL = "android.intent.action.DIAL";
    public static final String ACTION_DIGITAL_AUDIO_DOCK_PLUG = "android.intent.action.DIGITAL_AUDIO_DOCK_PLUG";
    public static final String ACTION_DOCK_EVENT = "android.intent.action.DOCK_EVENT";
    public static final String ACTION_EDIT = "android.intent.action.EDIT";
    public static final String ACTION_EXTERNAL_APPLICATIONS_AVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE";
    public static final String ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE";
    public static final String ACTION_FACTORY_TEST = "android.intent.action.FACTORY_TEST";
    public static final String ACTION_GET_CONTENT = "android.intent.action.GET_CONTENT";
    public static final String ACTION_GTALK_SERVICE_CONNECTED = "android.intent.action.GTALK_CONNECTED";
    public static final String ACTION_GTALK_SERVICE_DISCONNECTED = "android.intent.action.GTALK_DISCONNECTED";
    public static final String ACTION_HDMI_AUDIO_PLUG = "android.intent.action.HDMI_AUDIO_PLUG";
    public static final String ACTION_HEADSET_PLUG = "android.intent.action.HEADSET_PLUG";
    public static final String ACTION_INPUT_METHOD_CHANGED = "android.intent.action.INPUT_METHOD_CHANGED";
    public static final String ACTION_INSERT = "android.intent.action.INSERT";
    public static final String ACTION_INSERT_OR_EDIT = "android.intent.action.INSERT_OR_EDIT";
    public static final String ACTION_INSTALL_PACKAGE = "android.intent.action.INSTALL_PACKAGE";
    public static final String ACTION_LOCALE_CHANGED = "android.intent.action.LOCALE_CHANGED";
    public static final String ACTION_MAIN = "android.intent.action.MAIN";
    public static final String ACTION_MANAGE_NETWORK_USAGE = "android.intent.action.MANAGE_NETWORK_USAGE";
    public static final String ACTION_MANAGE_PACKAGE_STORAGE = "android.intent.action.MANAGE_PACKAGE_STORAGE";
    public static final String ACTION_MEDIA_BAD_REMOVAL = "android.intent.action.MEDIA_BAD_REMOVAL";
    public static final String ACTION_MEDIA_BUTTON = "android.intent.action.MEDIA_BUTTON";
    public static final String ACTION_MEDIA_CHECKING = "android.intent.action.MEDIA_CHECKING";
    public static final String ACTION_MEDIA_EJECT = "android.intent.action.MEDIA_EJECT";
    public static final String ACTION_MEDIA_MOUNTED = "android.intent.action.MEDIA_MOUNTED";
    public static final String ACTION_MEDIA_NOFS = "android.intent.action.MEDIA_NOFS";
    public static final String ACTION_MEDIA_REMOVED = "android.intent.action.MEDIA_REMOVED";
    public static final String ACTION_MEDIA_SCANNER_FINISHED = "android.intent.action.MEDIA_SCANNER_FINISHED";
    public static final String ACTION_MEDIA_SCANNER_SCAN_FILE = "android.intent.action.MEDIA_SCANNER_SCAN_FILE";
    public static final String ACTION_MEDIA_SCANNER_STARTED = "android.intent.action.MEDIA_SCANNER_STARTED";
    public static final String ACTION_MEDIA_SHARED = "android.intent.action.MEDIA_SHARED";
    public static final String ACTION_MEDIA_UNMOUNTABLE = "android.intent.action.MEDIA_UNMOUNTABLE";
    public static final String ACTION_MEDIA_UNMOUNTED = "android.intent.action.MEDIA_UNMOUNTED";
    public static final String ACTION_MEDIA_UNSHARED = "android.intent.action.MEDIA_UNSHARED";
    public static final String ACTION_MY_PACKAGE_REPLACED = "android.intent.action.MY_PACKAGE_REPLACED";
    public static final String ACTION_NEW_OUTGOING_CALL = "android.intent.action.NEW_OUTGOING_CALL";
    public static final String ACTION_PACKAGE_ADDED = "android.intent.action.PACKAGE_ADDED";
    public static final String ACTION_PACKAGE_CHANGED = "android.intent.action.PACKAGE_CHANGED";
    public static final String ACTION_PACKAGE_DATA_CLEARED = "android.intent.action.PACKAGE_DATA_CLEARED";
    public static final String ACTION_PACKAGE_FIRST_LAUNCH = "android.intent.action.PACKAGE_FIRST_LAUNCH";
    public static final String ACTION_PACKAGE_FULLY_REMOVED = "android.intent.action.PACKAGE_FULLY_REMOVED";

    @Deprecated
    public static final String ACTION_PACKAGE_INSTALL = "android.intent.action.PACKAGE_INSTALL";
    public static final String ACTION_PACKAGE_NEEDS_VERIFICATION = "android.intent.action.PACKAGE_NEEDS_VERIFICATION";
    public static final String ACTION_PACKAGE_REMOVED = "android.intent.action.PACKAGE_REMOVED";
    public static final String ACTION_PACKAGE_REPLACED = "android.intent.action.PACKAGE_REPLACED";
    public static final String ACTION_PACKAGE_RESTARTED = "android.intent.action.PACKAGE_RESTARTED";
    public static final String ACTION_PASTE = "android.intent.action.PASTE";
    public static final String ACTION_PICK = "android.intent.action.PICK";
    public static final String ACTION_PICK_ACTIVITY = "android.intent.action.PICK_ACTIVITY";
    public static final String ACTION_POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
    public static final String ACTION_POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
    public static final String ACTION_POWER_USAGE_SUMMARY = "android.intent.action.POWER_USAGE_SUMMARY";
    public static final String ACTION_PRE_BOOT_COMPLETED = "android.intent.action.PRE_BOOT_COMPLETED";
    public static final String ACTION_PROVIDER_CHANGED = "android.intent.action.PROVIDER_CHANGED";
    public static final String ACTION_QUERY_PACKAGE_RESTART = "android.intent.action.QUERY_PACKAGE_RESTART";
    public static final String ACTION_REBOOT = "android.intent.action.REBOOT";
    public static final String ACTION_REMOTE_INTENT = "com.google.android.c2dm.intent.RECEIVE";
    public static final String ACTION_REQUEST_SHUTDOWN = "android.intent.action.ACTION_REQUEST_SHUTDOWN";
    public static final String ACTION_RUN = "android.intent.action.RUN";
    public static final String ACTION_SCREEN_OFF = "android.intent.action.SCREEN_OFF";
    public static final String ACTION_SCREEN_ON = "android.intent.action.SCREEN_ON";
    public static final String ACTION_SEARCH = "android.intent.action.SEARCH";
    public static final String ACTION_SEARCH_LONG_PRESS = "android.intent.action.SEARCH_LONG_PRESS";
    public static final String ACTION_SEND = "android.intent.action.SEND";
    public static final String ACTION_SENDTO = "android.intent.action.SENDTO";
    public static final String ACTION_SEND_MULTIPLE = "android.intent.action.SEND_MULTIPLE";
    public static final String ACTION_SET_WALLPAPER = "android.intent.action.SET_WALLPAPER";
    public static final String ACTION_SHUTDOWN = "android.intent.action.ACTION_SHUTDOWN";
    public static final String ACTION_SYNC = "android.intent.action.SYNC";
    public static final String ACTION_SYNC_STATE_CHANGED = "android.intent.action.SYNC_STATE_CHANGED";
    public static final String ACTION_SYSTEM_TUTORIAL = "android.intent.action.SYSTEM_TUTORIAL";
    public static final String ACTION_TIMEZONE_CHANGED = "android.intent.action.TIMEZONE_CHANGED";
    public static final String ACTION_TIME_CHANGED = "android.intent.action.TIME_SET";
    public static final String ACTION_TIME_TICK = "android.intent.action.TIME_TICK";
    public static final String ACTION_UID_REMOVED = "android.intent.action.UID_REMOVED";

    @Deprecated
    public static final String ACTION_UMS_CONNECTED = "android.intent.action.UMS_CONNECTED";

    @Deprecated
    public static final String ACTION_UMS_DISCONNECTED = "android.intent.action.UMS_DISCONNECTED";
    public static final String ACTION_UNINSTALL_PACKAGE = "android.intent.action.UNINSTALL_PACKAGE";
    public static final String ACTION_UPGRADE_SETUP = "android.intent.action.UPGRADE_SETUP";
    public static final String ACTION_USB_AUDIO_ACCESSORY_PLUG = "android.intent.action.USB_AUDIO_ACCESSORY_PLUG";
    public static final String ACTION_USB_AUDIO_DEVICE_PLUG = "android.intent.action.USB_AUDIO_DEVICE_PLUG";
    public static final String ACTION_USER_ADDED = "android.intent.action.USER_ADDED";
    public static final String ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT";
    public static final String ACTION_USER_REMOVED = "android.intent.action.USER_REMOVED";
    public static final String ACTION_USER_SWITCHED = "android.intent.action.USER_SWITCHED";
    public static final String ACTION_VIEW = "android.intent.action.VIEW";
    public static final String ACTION_VOICE_COMMAND = "android.intent.action.VOICE_COMMAND";

    @Deprecated
    public static final String ACTION_WALLPAPER_CHANGED = "android.intent.action.WALLPAPER_CHANGED";
    public static final String ACTION_WEB_SEARCH = "android.intent.action.WEB_SEARCH";
    public static final String CATEGORY_ALTERNATIVE = "android.intent.category.ALTERNATIVE";
    public static final String CATEGORY_APP_BROWSER = "android.intent.category.APP_BROWSER";
    public static final String CATEGORY_APP_CALCULATOR = "android.intent.category.APP_CALCULATOR";
    public static final String CATEGORY_APP_CALENDAR = "android.intent.category.APP_CALENDAR";
    public static final String CATEGORY_APP_CONTACTS = "android.intent.category.APP_CONTACTS";
    public static final String CATEGORY_APP_EMAIL = "android.intent.category.APP_EMAIL";
    public static final String CATEGORY_APP_GALLERY = "android.intent.category.APP_GALLERY";
    public static final String CATEGORY_APP_MAPS = "android.intent.category.APP_MAPS";
    public static final String CATEGORY_APP_MARKET = "android.intent.category.APP_MARKET";
    public static final String CATEGORY_APP_MESSAGING = "android.intent.category.APP_MESSAGING";
    public static final String CATEGORY_APP_MUSIC = "android.intent.category.APP_MUSIC";
    public static final String CATEGORY_BROWSABLE = "android.intent.category.BROWSABLE";
    public static final String CATEGORY_CAR_DOCK = "android.intent.category.CAR_DOCK";
    public static final String CATEGORY_CAR_MODE = "android.intent.category.CAR_MODE";
    public static final String CATEGORY_DEFAULT = "android.intent.category.DEFAULT";
    public static final String CATEGORY_DESK_DOCK = "android.intent.category.DESK_DOCK";
    public static final String CATEGORY_DEVELOPMENT_PREFERENCE = "android.intent.category.DEVELOPMENT_PREFERENCE";
    public static final String CATEGORY_EMBED = "android.intent.category.EMBED";
    public static final String CATEGORY_FRAMEWORK_INSTRUMENTATION_TEST = "android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST";
    public static final String CATEGORY_HE_DESK_DOCK = "android.intent.category.HE_DESK_DOCK";
    public static final String CATEGORY_HOME = "android.intent.category.HOME";
    public static final String CATEGORY_INFO = "android.intent.category.INFO";
    public static final String CATEGORY_LAUNCHER = "android.intent.category.LAUNCHER";
    public static final String CATEGORY_LE_DESK_DOCK = "android.intent.category.LE_DESK_DOCK";
    public static final String CATEGORY_MONKEY = "android.intent.category.MONKEY";
    public static final String CATEGORY_OPENABLE = "android.intent.category.OPENABLE";
    public static final String CATEGORY_PREFERENCE = "android.intent.category.PREFERENCE";
    public static final String CATEGORY_SAMPLE_CODE = "android.intent.category.SAMPLE_CODE";
    public static final String CATEGORY_SELECTED_ALTERNATIVE = "android.intent.category.SELECTED_ALTERNATIVE";
    public static final String CATEGORY_TAB = "android.intent.category.TAB";
    public static final String CATEGORY_TEST = "android.intent.category.TEST";
    public static final String CATEGORY_UNIT_TEST = "android.intent.category.UNIT_TEST";
    public static final Parcelable.Creator<Intent> CREATOR = new Parcelable.Creator()
    {
        public Intent createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Intent(paramAnonymousParcel);
        }

        public Intent[] newArray(int paramAnonymousInt)
        {
            return new Intent[paramAnonymousInt];
        }
    };
    public static final String EXTRA_ALARM_COUNT = "android.intent.extra.ALARM_COUNT";

    @Deprecated
    public static final String EXTRA_ALLOW_REPLACE = "android.intent.extra.ALLOW_REPLACE";
    public static final String EXTRA_BCC = "android.intent.extra.BCC";
    public static final String EXTRA_BUG_REPORT = "android.intent.extra.BUG_REPORT";
    public static final String EXTRA_CC = "android.intent.extra.CC";

    @Deprecated
    public static final String EXTRA_CHANGED_COMPONENT_NAME = "android.intent.extra.changed_component_name";
    public static final String EXTRA_CHANGED_COMPONENT_NAME_LIST = "android.intent.extra.changed_component_name_list";
    public static final String EXTRA_CHANGED_PACKAGE_LIST = "android.intent.extra.changed_package_list";
    public static final String EXTRA_CHANGED_UID_LIST = "android.intent.extra.changed_uid_list";
    public static final String EXTRA_CLIENT_INTENT = "android.intent.extra.client_intent";
    public static final String EXTRA_CLIENT_LABEL = "android.intent.extra.client_label";
    public static final String EXTRA_DATA_REMOVED = "android.intent.extra.DATA_REMOVED";
    public static final String EXTRA_DOCK_STATE = "android.intent.extra.DOCK_STATE";
    public static final int EXTRA_DOCK_STATE_CAR = 2;
    public static final int EXTRA_DOCK_STATE_DESK = 1;
    public static final int EXTRA_DOCK_STATE_HE_DESK = 4;
    public static final int EXTRA_DOCK_STATE_LE_DESK = 3;
    public static final int EXTRA_DOCK_STATE_UNDOCKED = 0;
    public static final String EXTRA_DONT_KILL_APP = "android.intent.extra.DONT_KILL_APP";
    public static final String EXTRA_EMAIL = "android.intent.extra.EMAIL";
    public static final String EXTRA_HTML_TEXT = "android.intent.extra.HTML_TEXT";
    public static final String EXTRA_INITIAL_INTENTS = "android.intent.extra.INITIAL_INTENTS";
    public static final String EXTRA_INSTALLER_PACKAGE_NAME = "android.intent.extra.INSTALLER_PACKAGE_NAME";
    public static final String EXTRA_INSTALL_RESULT = "android.intent.extra.INSTALL_RESULT";
    public static final String EXTRA_INTENT = "android.intent.extra.INTENT";
    public static final String EXTRA_KEY_CONFIRM = "android.intent.extra.KEY_CONFIRM";
    public static final String EXTRA_KEY_EVENT = "android.intent.extra.KEY_EVENT";
    public static final String EXTRA_LOCAL_ONLY = "android.intent.extra.LOCAL_ONLY";
    public static final String EXTRA_NOT_UNKNOWN_SOURCE = "android.intent.extra.NOT_UNKNOWN_SOURCE";
    public static final String EXTRA_PACKAGES = "android.intent.extra.PACKAGES";
    public static final String EXTRA_PHONE_NUMBER = "android.intent.extra.PHONE_NUMBER";
    public static final String EXTRA_REMOTE_INTENT_TOKEN = "android.intent.extra.remote_intent_token";
    public static final String EXTRA_REPLACING = "android.intent.extra.REPLACING";
    public static final String EXTRA_RETURN_RESULT = "android.intent.extra.RETURN_RESULT";
    public static final String EXTRA_SHORTCUT_ICON = "android.intent.extra.shortcut.ICON";
    public static final String EXTRA_SHORTCUT_ICON_RESOURCE = "android.intent.extra.shortcut.ICON_RESOURCE";
    public static final String EXTRA_SHORTCUT_INTENT = "android.intent.extra.shortcut.INTENT";
    public static final String EXTRA_SHORTCUT_NAME = "android.intent.extra.shortcut.NAME";
    public static final String EXTRA_STREAM = "android.intent.extra.STREAM";
    public static final String EXTRA_SUBJECT = "android.intent.extra.SUBJECT";
    public static final String EXTRA_TEMPLATE = "android.intent.extra.TEMPLATE";
    public static final String EXTRA_TEXT = "android.intent.extra.TEXT";
    public static final String EXTRA_TITLE = "android.intent.extra.TITLE";
    public static final String EXTRA_UID = "android.intent.extra.UID";
    public static final String EXTRA_USERID = "android.intent.extra.user_id";
    public static final int FILL_IN_ACTION = 1;
    public static final int FILL_IN_CATEGORIES = 4;
    public static final int FILL_IN_CLIP_DATA = 128;
    public static final int FILL_IN_COMPONENT = 8;
    public static final int FILL_IN_DATA = 2;
    public static final int FILL_IN_PACKAGE = 16;
    public static final int FILL_IN_SELECTOR = 64;
    public static final int FILL_IN_SOURCE_BOUNDS = 32;
    public static final int FLAG_ACTIVITY_BROUGHT_TO_FRONT = 4194304;
    public static final int FLAG_ACTIVITY_CLEAR_TASK = 32768;
    public static final int FLAG_ACTIVITY_CLEAR_TOP = 67108864;
    public static final int FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET = 524288;
    public static final int FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS = 8388608;
    public static final int FLAG_ACTIVITY_FORWARD_RESULT = 33554432;
    public static final int FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY = 1048576;
    public static final int FLAG_ACTIVITY_MULTIPLE_TASK = 134217728;
    public static final int FLAG_ACTIVITY_NEW_TASK = 268435456;
    public static final int FLAG_ACTIVITY_NO_ANIMATION = 65536;
    public static final int FLAG_ACTIVITY_NO_HISTORY = 1073741824;
    public static final int FLAG_ACTIVITY_NO_USER_ACTION = 262144;
    public static final int FLAG_ACTIVITY_PREVIOUS_IS_TOP = 16777216;
    public static final int FLAG_ACTIVITY_REORDER_TO_FRONT = 131072;
    public static final int FLAG_ACTIVITY_RESET_TASK_IF_NEEDED = 2097152;
    public static final int FLAG_ACTIVITY_SINGLE_TOP = 536870912;
    public static final int FLAG_ACTIVITY_TASK_ON_HOME = 16384;
    public static final int FLAG_DEBUG_LOG_RESOLUTION = 8;
    public static final int FLAG_EXCLUDE_STOPPED_PACKAGES = 16;
    public static final int FLAG_FROM_BACKGROUND = 4;
    public static final int FLAG_GRANT_READ_URI_PERMISSION = 1;
    public static final int FLAG_GRANT_WRITE_URI_PERMISSION = 2;
    public static final int FLAG_INCLUDE_STOPPED_PACKAGES = 32;
    public static final int FLAG_RECEIVER_BOOT_UPGRADE = 67108864;
    public static final int FLAG_RECEIVER_FOREGROUND = 268435456;
    public static final int FLAG_RECEIVER_REGISTERED_ONLY = 1073741824;
    public static final int FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT = 134217728;
    public static final int FLAG_RECEIVER_REPLACE_PENDING = 536870912;
    public static final int IMMUTABLE_FLAGS = 3;
    public static final String METADATA_DOCK_HOME = "android.dock_home";
    public static final String METADATA_SETUP_VERSION = "android.SETUP_VERSION";
    public static final int URI_INTENT_SCHEME = 1;
    private String mAction;
    private HashSet<String> mCategories;
    private ClipData mClipData;
    private ComponentName mComponent;
    private Uri mData;
    private Bundle mExtras;
    private int mFlags;
    private String mPackage;
    private Intent mSelector;
    private Rect mSourceBounds;
    private String mType;

    public Intent()
    {
    }

    public Intent(Context paramContext, Class<?> paramClass)
    {
        this.mComponent = new ComponentName(paramContext, paramClass);
    }

    public Intent(Intent paramIntent)
    {
        this.mAction = paramIntent.mAction;
        this.mData = paramIntent.mData;
        this.mType = paramIntent.mType;
        this.mPackage = paramIntent.mPackage;
        this.mComponent = paramIntent.mComponent;
        this.mFlags = paramIntent.mFlags;
        if (paramIntent.mCategories != null)
            this.mCategories = new HashSet(paramIntent.mCategories);
        if (paramIntent.mExtras != null)
            this.mExtras = new Bundle(paramIntent.mExtras);
        if (paramIntent.mSourceBounds != null)
            this.mSourceBounds = new Rect(paramIntent.mSourceBounds);
        if (paramIntent.mSelector != null)
            this.mSelector = new Intent(paramIntent.mSelector);
        if (paramIntent.mClipData != null)
            this.mClipData = new ClipData(paramIntent.mClipData);
    }

    private Intent(Intent paramIntent, boolean paramBoolean)
    {
        this.mAction = paramIntent.mAction;
        this.mData = paramIntent.mData;
        this.mType = paramIntent.mType;
        this.mPackage = paramIntent.mPackage;
        this.mComponent = paramIntent.mComponent;
        if (paramIntent.mCategories != null)
            this.mCategories = new HashSet(paramIntent.mCategories);
    }

    protected Intent(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    public Intent(String paramString)
    {
        setAction(paramString);
    }

    public Intent(String paramString, Uri paramUri)
    {
        setAction(paramString);
        this.mData = paramUri;
    }

    public Intent(String paramString, Uri paramUri, Context paramContext, Class<?> paramClass)
    {
        setAction(paramString);
        this.mData = paramUri;
        this.mComponent = new ComponentName(paramContext, paramClass);
    }

    public static Intent createChooser(Intent paramIntent, CharSequence paramCharSequence)
    {
        Intent localIntent = new Intent("android.intent.action.CHOOSER");
        localIntent.putExtra("android.intent.extra.INTENT", paramIntent);
        if (paramCharSequence != null)
            localIntent.putExtra("android.intent.extra.TITLE", paramCharSequence);
        int i = 0x3 & paramIntent.getFlags();
        ClipData localClipData;
        ClipData.Item localItem;
        String[] arrayOfString;
        if (i != 0)
        {
            localClipData = paramIntent.getClipData();
            if ((localClipData == null) && (paramIntent.getData() != null))
            {
                localItem = new ClipData.Item(paramIntent.getData());
                if (paramIntent.getType() == null)
                    break label131;
                arrayOfString = new String[1];
                arrayOfString[0] = paramIntent.getType();
            }
        }
        while (true)
        {
            localClipData = new ClipData(null, arrayOfString, localItem);
            if (localClipData != null)
            {
                localIntent.setClipData(localClipData);
                localIntent.addFlags(i);
            }
            return localIntent;
            label131: arrayOfString = new String[0];
        }
    }

    @Deprecated
    public static Intent getIntent(String paramString)
        throws URISyntaxException
    {
        return parseUri(paramString, 0);
    }

    public static Intent getIntentOld(String paramString)
        throws URISyntaxException
    {
        int i = paramString.lastIndexOf('#');
        Intent localIntent;
        int i5;
        String str2;
        String str3;
        label756: int i6;
        if (i >= 0)
        {
            String str1 = null;
            int j = 0;
            int k = i + 1;
            if (paramString.regionMatches(k, "action(", 0, 7))
            {
                j = 1;
                int i17 = k + 7;
                int i18 = paramString.indexOf(')', i17);
                str1 = paramString.substring(i17, i18);
                k = i18 + 1;
            }
            localIntent = new Intent(str1);
            if (paramString.regionMatches(k, "categories(", 0, 11))
            {
                j = 1;
                int i14 = k + 11;
                int i15 = paramString.indexOf(')', i14);
                while (i14 < i15)
                {
                    int i16 = paramString.indexOf('!', i14);
                    if (i16 < 0)
                        i16 = i15;
                    if (i14 < i16)
                        localIntent.addCategory(paramString.substring(i14, i16));
                    i14 = i16 + 1;
                }
                k = i15 + 1;
            }
            if (paramString.regionMatches(k, "type(", 0, 5))
            {
                j = 1;
                int i12 = k + 5;
                int i13 = paramString.indexOf(')', i12);
                localIntent.mType = paramString.substring(i12, i13);
                k = i13 + 1;
            }
            if (paramString.regionMatches(k, "launchFlags(", 0, 12))
            {
                j = 1;
                int i10 = k + 12;
                int i11 = paramString.indexOf(')', i10);
                localIntent.mFlags = Integer.decode(paramString.substring(i10, i11)).intValue();
                k = i11 + 1;
            }
            if (paramString.regionMatches(k, "component(", 0, 10))
            {
                j = 1;
                int i7 = k + 10;
                int i8 = paramString.indexOf(')', i7);
                int i9 = paramString.indexOf('!', i7);
                if ((i9 >= 0) && (i9 < i8))
                {
                    String str4 = paramString.substring(i7, i9);
                    String str5 = paramString.substring(i9 + 1, i8);
                    ComponentName localComponentName = new ComponentName(str4, str5);
                    localIntent.mComponent = localComponentName;
                }
                k = i8 + 1;
            }
            int i2;
            if (paramString.regionMatches(k, "extras(", 0, 7))
            {
                j = 1;
                int m = k + 7;
                int n = paramString.indexOf(')', m);
                if (n == -1)
                {
                    URISyntaxException localURISyntaxException1 = new URISyntaxException(paramString, "EXTRA missing trailing ')'", m);
                    throw localURISyntaxException1;
                    m = i5 + 1;
                }
                if (m < n)
                {
                    int i1 = paramString.indexOf('=', m);
                    if ((i1 <= m + 1) || (m >= n))
                    {
                        URISyntaxException localURISyntaxException2 = new URISyntaxException(paramString, "EXTRA missing '='", m);
                        throw localURISyntaxException2;
                    }
                    i2 = paramString.charAt(m);
                    str2 = paramString.substring(m + 1, i1);
                    int i3 = i1 + 1;
                    int i4 = paramString.indexOf('!', i3);
                    if ((i4 == -1) || (i4 >= n))
                        i4 = n;
                    if (i3 >= i4)
                    {
                        URISyntaxException localURISyntaxException3 = new URISyntaxException(paramString, "EXTRA missing '!'", i3);
                        throw localURISyntaxException3;
                    }
                    str3 = paramString.substring(i3, i4);
                    i5 = i4;
                    if (localIntent.mExtras == null)
                        localIntent.mExtras = new Bundle();
                }
            }
            switch (i2)
            {
            default:
                try
                {
                    URISyntaxException localURISyntaxException6 = new URISyntaxException(paramString, "EXTRA has unknown type", i5);
                    throw localURISyntaxException6;
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    URISyntaxException localURISyntaxException5 = new URISyntaxException(paramString, "EXTRA value can't be parsed", i5);
                    throw localURISyntaxException5;
                }
            case 83:
                localIntent.mExtras.putString(str2, Uri.decode(str3));
                i6 = paramString.charAt(i5);
                if (i6 == 41)
                {
                    if (j == 0)
                        break label969;
                    localIntent.mData = Uri.parse(paramString.substring(0, i));
                    label789: if (localIntent.mAction == null)
                        localIntent.mAction = "android.intent.action.VIEW";
                }
                break;
            case 66:
            case 98:
            case 99:
            case 100:
            case 102:
            case 105:
            case 108:
            case 115:
            }
        }
        while (true)
        {
            return localIntent;
            localIntent.mExtras.putBoolean(str2, Boolean.parseBoolean(str3));
            break label756;
            localIntent.mExtras.putByte(str2, Byte.parseByte(str3));
            break label756;
            localIntent.mExtras.putChar(str2, Uri.decode(str3).charAt(0));
            break label756;
            localIntent.mExtras.putDouble(str2, Double.parseDouble(str3));
            break label756;
            localIntent.mExtras.putFloat(str2, Float.parseFloat(str3));
            break label756;
            localIntent.mExtras.putInt(str2, Integer.parseInt(str3));
            break label756;
            localIntent.mExtras.putLong(str2, Long.parseLong(str3));
            break label756;
            localIntent.mExtras.putShort(str2, Short.parseShort(str3));
            break label756;
            if (i6 == 33)
                break;
            URISyntaxException localURISyntaxException4 = new URISyntaxException(paramString, "EXTRA missing '!'", i5);
            throw localURISyntaxException4;
            label969: localIntent.mData = Uri.parse(paramString);
            break label789;
            localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
        }
    }

    private static ClipData.Item makeClipItem(ArrayList<Uri> paramArrayList, ArrayList<CharSequence> paramArrayList1, ArrayList<String> paramArrayList2, int paramInt)
    {
        Uri localUri;
        CharSequence localCharSequence;
        if (paramArrayList != null)
        {
            localUri = (Uri)paramArrayList.get(paramInt);
            if (paramArrayList1 == null)
                break label63;
            localCharSequence = (CharSequence)paramArrayList1.get(paramInt);
            label28: if (paramArrayList2 == null)
                break label69;
        }
        label63: label69: for (String str = (String)paramArrayList2.get(paramInt); ; str = null)
        {
            return new ClipData.Item(localCharSequence, str, null, localUri);
            localUri = null;
            break;
            localCharSequence = null;
            break label28;
        }
    }

    public static Intent makeMainActivity(ComponentName paramComponentName)
    {
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.setComponent(paramComponentName);
        localIntent.addCategory("android.intent.category.LAUNCHER");
        return localIntent;
    }

    public static Intent makeMainSelectorActivity(String paramString1, String paramString2)
    {
        Intent localIntent1 = new Intent("android.intent.action.MAIN");
        localIntent1.addCategory("android.intent.category.LAUNCHER");
        Intent localIntent2 = new Intent();
        localIntent2.setAction(paramString1);
        localIntent2.addCategory(paramString2);
        localIntent1.setSelector(localIntent2);
        return localIntent1;
    }

    public static Intent makeRestartActivityTask(ComponentName paramComponentName)
    {
        Intent localIntent = makeMainActivity(paramComponentName);
        localIntent.addFlags(268468224);
        return localIntent;
    }

    public static String normalizeMimeType(String paramString)
    {
        String str;
        if (paramString == null)
            str = null;
        while (true)
        {
            return str;
            str = paramString.trim().toLowerCase(Locale.US);
            int i = str.indexOf(';');
            if (i != -1)
                str = str.substring(0, i);
        }
    }

    public static Intent parseIntent(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        Intent localIntent = new Intent();
        TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.Intent);
        localIntent.setAction(localTypedArray1.getString(2));
        String str1 = localTypedArray1.getString(3);
        String str2 = localTypedArray1.getString(1);
        Uri localUri;
        int i;
        if (str1 != null)
        {
            localUri = Uri.parse(str1);
            localIntent.setDataAndType(localUri, str2);
            String str3 = localTypedArray1.getString(0);
            String str4 = localTypedArray1.getString(4);
            if ((str3 != null) && (str4 != null))
                localIntent.setComponent(new ComponentName(str3, str4));
            localTypedArray1.recycle();
            i = paramXmlPullParser.getDepth();
        }
        while (true)
        {
            int j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break label286;
            if ((j != 3) && (j != 4))
            {
                String str5 = paramXmlPullParser.getName();
                if (str5.equals("category"))
                {
                    TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.IntentCategory);
                    String str6 = localTypedArray2.getString(0);
                    localTypedArray2.recycle();
                    if (str6 != null)
                        localIntent.addCategory(str6);
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                    continue;
                    localUri = null;
                    break;
                }
                if (str5.equals("extra"))
                {
                    if (localIntent.mExtras == null)
                        localIntent.mExtras = new Bundle();
                    paramResources.parseBundleExtra("extra", paramAttributeSet, localIntent.mExtras);
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
                else
                {
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
            }
        }
        label286: return localIntent;
    }

    public static Intent parseUri(String paramString, int paramInt)
        throws URISyntaxException
    {
        int i = 0;
        if ((paramInt & 0x1) != 0);
        while (true)
        {
            try
            {
                if (!paramString.startsWith("intent:"))
                {
                    localObject1 = new Intent("android.intent.action.VIEW");
                    try
                    {
                        ((Intent)localObject1).setData(Uri.parse(paramString));
                        return localObject1;
                    }
                    catch (IllegalArgumentException localIllegalArgumentException2)
                    {
                        throw new URISyntaxException(paramString, localIllegalArgumentException2.getMessage());
                    }
                }
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new URISyntaxException(paramString, "illegal Intent URI format", i);
            }
            i = paramString.lastIndexOf("#");
            if (i == -1)
            {
                localObject1 = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
            }
            else
            {
                if (paramString.startsWith("#Intent;", i))
                    break;
                localObject1 = getIntentOld(paramString);
            }
        }
        Object localObject1 = new Intent("android.intent.action.VIEW");
        Object localObject2 = localObject1;
        String str1;
        label145: Object localObject3;
        label157: int k;
        int m;
        if (i >= 0)
        {
            str1 = paramString.substring(0, i);
            localObject3 = null;
            i += "#Intent;".length();
            if (!paramString.startsWith("end", i))
            {
                k = paramString.indexOf('=', i);
                if (k < 0)
                    k = i - 1;
                m = paramString.indexOf(';', i);
                if (k >= m)
                    break label829;
            }
        }
        label829: for (String str2 = Uri.decode(paramString.substring(k + 1, m)); ; str2 = "")
        {
            while (true)
                if (paramString.startsWith("action=", i))
                {
                    ((Intent)localObject1).setAction(str2);
                }
                else if (paramString.startsWith("category=", i))
                {
                    ((Intent)localObject1).addCategory(str2);
                }
                else if (paramString.startsWith("type=", i))
                {
                    ((Intent)localObject1).mType = str2;
                }
                else if (paramString.startsWith("launchFlags=", i))
                {
                    ((Intent)localObject1).mFlags = Integer.decode(str2).intValue();
                }
                else if (paramString.startsWith("package=", i))
                {
                    ((Intent)localObject1).mPackage = str2;
                }
                else if (paramString.startsWith("component=", i))
                {
                    ((Intent)localObject1).mComponent = ComponentName.unflattenFromString(str2);
                }
                else if (paramString.startsWith("scheme=", i))
                {
                    localObject3 = str2;
                }
                else if (paramString.startsWith("sourceBounds=", i))
                {
                    ((Intent)localObject1).mSourceBounds = Rect.unflattenFromString(str2);
                }
                else if ((m == i + 3) && (paramString.startsWith("SEL", i)))
                {
                    localObject1 = new Intent();
                }
                else
                {
                    String str3 = Uri.decode(paramString.substring(i + 2, k));
                    if (((Intent)localObject1).mExtras == null)
                        ((Intent)localObject1).mExtras = new Bundle();
                    Bundle localBundle = ((Intent)localObject1).mExtras;
                    if (paramString.startsWith("S.", i))
                    {
                        localBundle.putString(str3, str2);
                    }
                    else if (paramString.startsWith("B.", i))
                    {
                        localBundle.putBoolean(str3, Boolean.parseBoolean(str2));
                    }
                    else if (paramString.startsWith("b.", i))
                    {
                        localBundle.putByte(str3, Byte.parseByte(str2));
                    }
                    else if (paramString.startsWith("c.", i))
                    {
                        localBundle.putChar(str3, str2.charAt(0));
                    }
                    else if (paramString.startsWith("d.", i))
                    {
                        localBundle.putDouble(str3, Double.parseDouble(str2));
                    }
                    else if (paramString.startsWith("f.", i))
                    {
                        localBundle.putFloat(str3, Float.parseFloat(str2));
                    }
                    else if (paramString.startsWith("i.", i))
                    {
                        localBundle.putInt(str3, Integer.parseInt(str2));
                    }
                    else if (paramString.startsWith("l.", i))
                    {
                        localBundle.putLong(str3, Long.parseLong(str2));
                    }
                    else if (paramString.startsWith("s.", i))
                    {
                        localBundle.putShort(str3, Short.parseShort(str2));
                    }
                    else
                    {
                        throw new URISyntaxException(paramString, "unknown EXTRA type", i);
                        if (localObject1 != localObject2)
                        {
                            localObject2.setSelector((Intent)localObject1);
                            localObject1 = localObject2;
                        }
                        if (str1 == null)
                            break;
                        if (str1.startsWith("intent:"))
                        {
                            str1 = str1.substring(7);
                            if (localObject3 != null)
                                str1 = localObject3 + ':' + str1;
                        }
                        int j = str1.length();
                        if (j <= 0)
                            break;
                        try
                        {
                            ((Intent)localObject1).mData = Uri.parse(str1);
                        }
                        catch (IllegalArgumentException localIllegalArgumentException1)
                        {
                            throw new URISyntaxException(paramString, localIllegalArgumentException1.getMessage());
                        }
                    }
                }
            i = m + 1;
            break label157;
            str1 = null;
            break label145;
        }
    }

    private void toUriInner(StringBuilder paramStringBuilder, String paramString, int paramInt)
    {
        if (paramString != null)
            paramStringBuilder.append("scheme=").append(paramString).append(';');
        if (this.mAction != null)
            paramStringBuilder.append("action=").append(Uri.encode(this.mAction)).append(';');
        if (this.mCategories != null)
        {
            Iterator localIterator2 = this.mCategories.iterator();
            while (localIterator2.hasNext())
            {
                String str2 = (String)localIterator2.next();
                paramStringBuilder.append("category=").append(Uri.encode(str2)).append(';');
            }
        }
        if (this.mType != null)
            paramStringBuilder.append("type=").append(Uri.encode(this.mType, "/")).append(';');
        if (this.mFlags != 0)
            paramStringBuilder.append("launchFlags=0x").append(Integer.toHexString(this.mFlags)).append(';');
        if (this.mPackage != null)
            paramStringBuilder.append("package=").append(Uri.encode(this.mPackage)).append(';');
        if (this.mComponent != null)
            paramStringBuilder.append("component=").append(Uri.encode(this.mComponent.flattenToShortString(), "/")).append(';');
        if (this.mSourceBounds != null)
            paramStringBuilder.append("sourceBounds=").append(Uri.encode(this.mSourceBounds.flattenToString())).append(';');
        if (this.mExtras != null)
        {
            Iterator localIterator1 = this.mExtras.keySet().iterator();
            label524: 
            while (localIterator1.hasNext())
            {
                String str1 = (String)localIterator1.next();
                Object localObject = this.mExtras.get(str1);
                char c;
                if ((localObject instanceof String))
                    c = 'S';
                while (true)
                {
                    if (c == 0)
                        break label524;
                    paramStringBuilder.append(c);
                    paramStringBuilder.append('.');
                    paramStringBuilder.append(Uri.encode(str1));
                    paramStringBuilder.append('=');
                    paramStringBuilder.append(Uri.encode(localObject.toString()));
                    paramStringBuilder.append(';');
                    break;
                    if ((localObject instanceof Boolean))
                        c = 'B';
                    else if ((localObject instanceof Byte))
                        c = 'b';
                    else if ((localObject instanceof Character))
                        c = 'c';
                    else if ((localObject instanceof Double))
                        c = 'd';
                    else if ((localObject instanceof Float))
                        c = 'f';
                    else if ((localObject instanceof Integer))
                        c = 'i';
                    else if ((localObject instanceof Long))
                        c = 'l';
                    else if ((localObject instanceof Short))
                        c = 's';
                    else
                        c = '\000';
                }
            }
        }
    }

    public Intent addCategory(String paramString)
    {
        if (this.mCategories == null)
            this.mCategories = new HashSet();
        this.mCategories.add(paramString.intern());
        return this;
    }

    public Intent addFlags(int paramInt)
    {
        this.mFlags = (paramInt | this.mFlags);
        return this;
    }

    public Object clone()
    {
        return new Intent(this);
    }

    public Intent cloneFilter()
    {
        return new Intent(this, false);
    }

    public int describeContents()
    {
        if (this.mExtras != null);
        for (int i = this.mExtras.describeContents(); ; i = 0)
            return i;
    }

    public int fillIn(Intent paramIntent, int paramInt)
    {
        int i = 0;
        if ((paramIntent.mAction != null) && ((this.mAction == null) || ((paramInt & 0x1) != 0)))
        {
            this.mAction = paramIntent.mAction;
            i = 0x0 | 0x1;
        }
        if (((paramIntent.mData != null) || (paramIntent.mType != null)) && (((this.mData == null) && (this.mType == null)) || ((paramInt & 0x2) != 0)))
        {
            this.mData = paramIntent.mData;
            this.mType = paramIntent.mType;
            i |= 2;
        }
        if ((paramIntent.mCategories != null) && ((this.mCategories == null) || ((paramInt & 0x4) != 0)))
        {
            if (paramIntent.mCategories != null)
                this.mCategories = new HashSet(paramIntent.mCategories);
            i |= 4;
        }
        if ((paramIntent.mPackage != null) && ((this.mPackage == null) || ((paramInt & 0x10) != 0)) && (this.mSelector == null))
        {
            this.mPackage = paramIntent.mPackage;
            i |= 16;
        }
        if ((paramIntent.mSelector != null) && ((paramInt & 0x40) != 0) && (this.mPackage == null))
        {
            this.mSelector = new Intent(paramIntent.mSelector);
            this.mPackage = null;
            i |= 64;
        }
        if ((paramIntent.mClipData != null) && ((this.mClipData == null) || ((paramInt & 0x80) != 0)))
        {
            this.mClipData = paramIntent.mClipData;
            i |= 128;
        }
        if ((paramIntent.mComponent != null) && ((paramInt & 0x8) != 0))
        {
            this.mComponent = paramIntent.mComponent;
            i |= 8;
        }
        this.mFlags |= paramIntent.mFlags;
        if ((paramIntent.mSourceBounds != null) && ((this.mSourceBounds == null) || ((paramInt & 0x20) != 0)))
        {
            this.mSourceBounds = new Rect(paramIntent.mSourceBounds);
            i |= 32;
        }
        if (this.mExtras == null)
            if (paramIntent.mExtras != null)
                this.mExtras = new Bundle(paramIntent.mExtras);
        while (true)
        {
            return i;
            if (paramIntent.mExtras != null)
                try
                {
                    Bundle localBundle = new Bundle(paramIntent.mExtras);
                    localBundle.putAll(this.mExtras);
                    this.mExtras = localBundle;
                }
                catch (RuntimeException localRuntimeException)
                {
                    Log.w("Intent", "Failure filling in extras", localRuntimeException);
                }
        }
    }

    public boolean filterEquals(Intent paramIntent)
    {
        boolean bool = false;
        if (paramIntent == null);
        while (true)
        {
            return bool;
            label136: label168: if (this.mAction != paramIntent.mAction)
            {
                if (this.mAction != null)
                    if (!this.mAction.equals(paramIntent.mAction))
                        continue;
            }
            else
                label40: if (this.mData != paramIntent.mData)
                {
                    if (this.mData != null)
                        if (!this.mData.equals(paramIntent.mData))
                            continue;
                }
                else
                    label72: if (this.mType != paramIntent.mType)
                    {
                        if (this.mType != null)
                            if (!this.mType.equals(paramIntent.mType))
                                continue;
                    }
                    else
                        label104: if (this.mPackage != paramIntent.mPackage)
                        {
                            if (this.mPackage != null)
                                if (!this.mPackage.equals(paramIntent.mPackage))
                                    continue;
                        }
                        else if (this.mComponent != paramIntent.mComponent)
                        {
                            if (this.mComponent != null)
                                if (!this.mComponent.equals(paramIntent.mComponent))
                                    continue;
                        }
                        else if (this.mCategories != paramIntent.mCategories)
                        {
                            if (this.mCategories != null)
                                if (!this.mCategories.equals(paramIntent.mCategories))
                                    continue;
                        }
                        else
                            while (paramIntent.mCategories.equals(this.mCategories))
                            {
                                bool = true;
                                break;
                                if (paramIntent.mAction.equals(this.mAction))
                                    break label40;
                                break;
                                if (paramIntent.mData.equals(this.mData))
                                    break label72;
                                break;
                                if (paramIntent.mType.equals(this.mType))
                                    break label104;
                                break;
                                if (paramIntent.mPackage.equals(this.mPackage))
                                    break label136;
                                break;
                                if (paramIntent.mComponent.equals(this.mComponent))
                                    break label168;
                                break;
                            }
        }
    }

    public int filterHashCode()
    {
        int i = 0;
        if (this.mAction != null)
            i = 0 + this.mAction.hashCode();
        if (this.mData != null)
            i += this.mData.hashCode();
        if (this.mType != null)
            i += this.mType.hashCode();
        if (this.mPackage != null)
            i += this.mPackage.hashCode();
        if (this.mComponent != null)
            i += this.mComponent.hashCode();
        if (this.mCategories != null)
            i += this.mCategories.hashCode();
        return i;
    }

    public String getAction()
    {
        return this.mAction;
    }

    public boolean[] getBooleanArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (boolean[] arrayOfBoolean = null; ; arrayOfBoolean = this.mExtras.getBooleanArray(paramString))
            return arrayOfBoolean;
    }

    public boolean getBooleanExtra(String paramString, boolean paramBoolean)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramBoolean;
            paramBoolean = this.mExtras.getBoolean(paramString, paramBoolean);
        }
    }

    public Bundle getBundleExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Bundle localBundle = null; ; localBundle = this.mExtras.getBundle(paramString))
            return localBundle;
    }

    public byte[] getByteArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (byte[] arrayOfByte = null; ; arrayOfByte = this.mExtras.getByteArray(paramString))
            return arrayOfByte;
    }

    public byte getByteExtra(String paramString, byte paramByte)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramByte;
            paramByte = this.mExtras.getByte(paramString, paramByte).byteValue();
        }
    }

    public Set<String> getCategories()
    {
        return this.mCategories;
    }

    public char[] getCharArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (char[] arrayOfChar = null; ; arrayOfChar = this.mExtras.getCharArray(paramString))
            return arrayOfChar;
    }

    public char getCharExtra(String paramString, char paramChar)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramChar;
            paramChar = this.mExtras.getChar(paramString, paramChar);
        }
    }

    public CharSequence[] getCharSequenceArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (CharSequence[] arrayOfCharSequence = null; ; arrayOfCharSequence = this.mExtras.getCharSequenceArray(paramString))
            return arrayOfCharSequence;
    }

    public ArrayList<CharSequence> getCharSequenceArrayListExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Object localObject = null; ; localObject = this.mExtras.getCharSequenceArrayList(paramString))
            return localObject;
    }

    public CharSequence getCharSequenceExtra(String paramString)
    {
        if (this.mExtras == null);
        for (CharSequence localCharSequence = null; ; localCharSequence = this.mExtras.getCharSequence(paramString))
            return localCharSequence;
    }

    public ClipData getClipData()
    {
        return this.mClipData;
    }

    public ComponentName getComponent()
    {
        return this.mComponent;
    }

    public Uri getData()
    {
        return this.mData;
    }

    public String getDataString()
    {
        if (this.mData != null);
        for (String str = this.mData.toString(); ; str = null)
            return str;
    }

    public double[] getDoubleArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (double[] arrayOfDouble = null; ; arrayOfDouble = this.mExtras.getDoubleArray(paramString))
            return arrayOfDouble;
    }

    public double getDoubleExtra(String paramString, double paramDouble)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramDouble;
            paramDouble = this.mExtras.getDouble(paramString, paramDouble);
        }
    }

    @Deprecated
    public Object getExtra(String paramString)
    {
        return getExtra(paramString, null);
    }

    @Deprecated
    public Object getExtra(String paramString, Object paramObject)
    {
        Object localObject1 = paramObject;
        if (this.mExtras != null)
        {
            Object localObject2 = this.mExtras.get(paramString);
            if (localObject2 != null)
                localObject1 = localObject2;
        }
        return localObject1;
    }

    public Bundle getExtras()
    {
        if (this.mExtras != null);
        for (Bundle localBundle = new Bundle(this.mExtras); ; localBundle = null)
            return localBundle;
    }

    public int getFlags()
    {
        return this.mFlags;
    }

    public float[] getFloatArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (float[] arrayOfFloat = null; ; arrayOfFloat = this.mExtras.getFloatArray(paramString))
            return arrayOfFloat;
    }

    public float getFloatExtra(String paramString, float paramFloat)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramFloat;
            paramFloat = this.mExtras.getFloat(paramString, paramFloat);
        }
    }

    @Deprecated
    public IBinder getIBinderExtra(String paramString)
    {
        if (this.mExtras == null);
        for (IBinder localIBinder = null; ; localIBinder = this.mExtras.getIBinder(paramString))
            return localIBinder;
    }

    public int[] getIntArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (int[] arrayOfInt = null; ; arrayOfInt = this.mExtras.getIntArray(paramString))
            return arrayOfInt;
    }

    public int getIntExtra(String paramString, int paramInt)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramInt;
            paramInt = this.mExtras.getInt(paramString, paramInt);
        }
    }

    public ArrayList<Integer> getIntegerArrayListExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Object localObject = null; ; localObject = this.mExtras.getIntegerArrayList(paramString))
            return localObject;
    }

    public long[] getLongArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (long[] arrayOfLong = null; ; arrayOfLong = this.mExtras.getLongArray(paramString))
            return arrayOfLong;
    }

    public long getLongExtra(String paramString, long paramLong)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramLong;
            paramLong = this.mExtras.getLong(paramString, paramLong);
        }
    }

    public String getPackage()
    {
        return this.mPackage;
    }

    public Parcelable[] getParcelableArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Parcelable[] arrayOfParcelable = null; ; arrayOfParcelable = this.mExtras.getParcelableArray(paramString))
            return arrayOfParcelable;
    }

    public <T extends Parcelable> ArrayList<T> getParcelableArrayListExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Object localObject = null; ; localObject = this.mExtras.getParcelableArrayList(paramString))
            return localObject;
    }

    public <T extends Parcelable> T getParcelableExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Object localObject = null; ; localObject = this.mExtras.getParcelable(paramString))
            return localObject;
    }

    public String getScheme()
    {
        if (this.mData != null);
        for (String str = this.mData.getScheme(); ; str = null)
            return str;
    }

    public Intent getSelector()
    {
        return this.mSelector;
    }

    public Serializable getSerializableExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Serializable localSerializable = null; ; localSerializable = this.mExtras.getSerializable(paramString))
            return localSerializable;
    }

    public short[] getShortArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (short[] arrayOfShort = null; ; arrayOfShort = this.mExtras.getShortArray(paramString))
            return arrayOfShort;
    }

    public short getShortExtra(String paramString, short paramShort)
    {
        if (this.mExtras == null);
        while (true)
        {
            return paramShort;
            paramShort = this.mExtras.getShort(paramString, paramShort);
        }
    }

    public Rect getSourceBounds()
    {
        return this.mSourceBounds;
    }

    public String[] getStringArrayExtra(String paramString)
    {
        if (this.mExtras == null);
        for (String[] arrayOfString = null; ; arrayOfString = this.mExtras.getStringArray(paramString))
            return arrayOfString;
    }

    public ArrayList<String> getStringArrayListExtra(String paramString)
    {
        if (this.mExtras == null);
        for (Object localObject = null; ; localObject = this.mExtras.getStringArrayList(paramString))
            return localObject;
    }

    public String getStringExtra(String paramString)
    {
        if (this.mExtras == null);
        for (String str = null; ; str = this.mExtras.getString(paramString))
            return str;
    }

    public String getType()
    {
        return this.mType;
    }

    public boolean hasCategory(String paramString)
    {
        if ((this.mCategories != null) && (this.mCategories.contains(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasExtra(String paramString)
    {
        if ((this.mExtras != null) && (this.mExtras.containsKey(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasFileDescriptors()
    {
        if ((this.mExtras != null) && (this.mExtras.hasFileDescriptors()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isExcludingStopped()
    {
        if ((0x30 & this.mFlags) == 16);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean migrateExtraStreamToClipData()
    {
        boolean bool;
        if ((this.mExtras != null) && (this.mExtras.isParcelled()))
            bool = false;
        while (true)
        {
            return bool;
            if (getClipData() != null)
            {
                bool = false;
                continue;
            }
            String str1 = getAction();
            if ("android.intent.action.CHOOSER".equals(str1));
            try
            {
                Intent localIntent = (Intent)getParcelableExtra("android.intent.extra.INTENT");
                if ((localIntent != null) && (localIntent.migrateExtraStreamToClipData()))
                {
                    setClipData(localIntent.getClipData());
                    addFlags(0x3 & localIntent.getFlags());
                    bool = true;
                    continue;
                }
                bool = false;
                continue;
                if ("android.intent.action.SEND".equals(str1));
                try
                {
                    Uri localUri = (Uri)getParcelableExtra("android.intent.extra.STREAM");
                    CharSequence localCharSequence = getCharSequenceExtra("android.intent.extra.TEXT");
                    String str2 = getStringExtra("android.intent.extra.HTML_TEXT");
                    if ((localUri != null) || (localCharSequence != null) || (str2 != null))
                    {
                        String[] arrayOfString2 = new String[1];
                        arrayOfString2[0] = getType();
                        setClipData(new ClipData(null, arrayOfString2, new ClipData.Item(localCharSequence, str2, null, localUri)));
                        addFlags(1);
                        bool = true;
                        continue;
                        if ("android.intent.action.SEND_MULTIPLE".equals(str1))
                            try
                            {
                                ArrayList localArrayList1 = getParcelableArrayListExtra("android.intent.extra.STREAM");
                                ArrayList localArrayList2 = getCharSequenceArrayListExtra("android.intent.extra.TEXT");
                                ArrayList localArrayList3 = getStringArrayListExtra("android.intent.extra.HTML_TEXT");
                                int i = -1;
                                if (localArrayList1 != null)
                                    i = localArrayList1.size();
                                if (localArrayList2 != null)
                                {
                                    if ((i >= 0) && (i != localArrayList2.size()))
                                    {
                                        bool = false;
                                        continue;
                                    }
                                    i = localArrayList2.size();
                                }
                                if (localArrayList3 != null)
                                {
                                    if ((i >= 0) && (i != localArrayList3.size()))
                                    {
                                        bool = false;
                                        continue;
                                    }
                                    i = localArrayList3.size();
                                }
                                if (i > 0)
                                {
                                    String[] arrayOfString1 = new String[1];
                                    arrayOfString1[0] = getType();
                                    ClipData localClipData = new ClipData(null, arrayOfString1, makeClipItem(localArrayList1, localArrayList2, localArrayList3, 0));
                                    for (int j = 1; j < i; j++)
                                        localClipData.addItem(makeClipItem(localArrayList1, localArrayList2, localArrayList3, j));
                                    setClipData(localClipData);
                                    addFlags(1);
                                    bool = true;
                                }
                            }
                            catch (ClassCastException localClassCastException1)
                            {
                            }
                    }
                    label419: bool = false;
                }
                catch (ClassCastException localClassCastException2)
                {
                    break label419;
                }
            }
            catch (ClassCastException localClassCastException3)
            {
                break label419;
            }
        }
    }

    public Intent putCharSequenceArrayListExtra(String paramString, ArrayList<CharSequence> paramArrayList)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putCharSequenceArrayList(paramString, paramArrayList);
        return this;
    }

    public Intent putExtra(String paramString, byte paramByte)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putByte(paramString, paramByte);
        return this;
    }

    public Intent putExtra(String paramString, char paramChar)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putChar(paramString, paramChar);
        return this;
    }

    public Intent putExtra(String paramString, double paramDouble)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putDouble(paramString, paramDouble);
        return this;
    }

    public Intent putExtra(String paramString, float paramFloat)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putFloat(paramString, paramFloat);
        return this;
    }

    public Intent putExtra(String paramString, int paramInt)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putInt(paramString, paramInt);
        return this;
    }

    public Intent putExtra(String paramString, long paramLong)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putLong(paramString, paramLong);
        return this;
    }

    public Intent putExtra(String paramString, Bundle paramBundle)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putBundle(paramString, paramBundle);
        return this;
    }

    @Deprecated
    public Intent putExtra(String paramString, IBinder paramIBinder)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putIBinder(paramString, paramIBinder);
        return this;
    }

    public Intent putExtra(String paramString, Parcelable paramParcelable)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putParcelable(paramString, paramParcelable);
        return this;
    }

    public Intent putExtra(String paramString, Serializable paramSerializable)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putSerializable(paramString, paramSerializable);
        return this;
    }

    public Intent putExtra(String paramString, CharSequence paramCharSequence)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putCharSequence(paramString, paramCharSequence);
        return this;
    }

    public Intent putExtra(String paramString1, String paramString2)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putString(paramString1, paramString2);
        return this;
    }

    public Intent putExtra(String paramString, short paramShort)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putShort(paramString, paramShort);
        return this;
    }

    public Intent putExtra(String paramString, boolean paramBoolean)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putBoolean(paramString, paramBoolean);
        return this;
    }

    public Intent putExtra(String paramString, byte[] paramArrayOfByte)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putByteArray(paramString, paramArrayOfByte);
        return this;
    }

    public Intent putExtra(String paramString, char[] paramArrayOfChar)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putCharArray(paramString, paramArrayOfChar);
        return this;
    }

    public Intent putExtra(String paramString, double[] paramArrayOfDouble)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putDoubleArray(paramString, paramArrayOfDouble);
        return this;
    }

    public Intent putExtra(String paramString, float[] paramArrayOfFloat)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putFloatArray(paramString, paramArrayOfFloat);
        return this;
    }

    public Intent putExtra(String paramString, int[] paramArrayOfInt)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putIntArray(paramString, paramArrayOfInt);
        return this;
    }

    public Intent putExtra(String paramString, long[] paramArrayOfLong)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putLongArray(paramString, paramArrayOfLong);
        return this;
    }

    public Intent putExtra(String paramString, Parcelable[] paramArrayOfParcelable)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putParcelableArray(paramString, paramArrayOfParcelable);
        return this;
    }

    public Intent putExtra(String paramString, CharSequence[] paramArrayOfCharSequence)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putCharSequenceArray(paramString, paramArrayOfCharSequence);
        return this;
    }

    public Intent putExtra(String paramString, String[] paramArrayOfString)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putStringArray(paramString, paramArrayOfString);
        return this;
    }

    public Intent putExtra(String paramString, short[] paramArrayOfShort)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putShortArray(paramString, paramArrayOfShort);
        return this;
    }

    public Intent putExtra(String paramString, boolean[] paramArrayOfBoolean)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putBooleanArray(paramString, paramArrayOfBoolean);
        return this;
    }

    public Intent putExtras(Intent paramIntent)
    {
        if (paramIntent.mExtras != null)
        {
            if (this.mExtras != null)
                break label31;
            this.mExtras = new Bundle(paramIntent.mExtras);
        }
        while (true)
        {
            return this;
            label31: this.mExtras.putAll(paramIntent.mExtras);
        }
    }

    public Intent putExtras(Bundle paramBundle)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putAll(paramBundle);
        return this;
    }

    public Intent putIntegerArrayListExtra(String paramString, ArrayList<Integer> paramArrayList)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putIntegerArrayList(paramString, paramArrayList);
        return this;
    }

    public Intent putParcelableArrayListExtra(String paramString, ArrayList<? extends Parcelable> paramArrayList)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putParcelableArrayList(paramString, paramArrayList);
        return this;
    }

    public Intent putStringArrayListExtra(String paramString, ArrayList<String> paramArrayList)
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        this.mExtras.putStringArrayList(paramString, paramArrayList);
        return this;
    }

    public void readFromParcel(Parcel paramParcel)
    {
        setAction(paramParcel.readString());
        this.mData = ((Uri)Uri.CREATOR.createFromParcel(paramParcel));
        this.mType = paramParcel.readString();
        this.mFlags = paramParcel.readInt();
        this.mPackage = paramParcel.readString();
        this.mComponent = ComponentName.readFromParcel(paramParcel);
        if (paramParcel.readInt() != 0)
            this.mSourceBounds = ((Rect)Rect.CREATOR.createFromParcel(paramParcel));
        int i = paramParcel.readInt();
        if (i > 0)
        {
            this.mCategories = new HashSet();
            for (int j = 0; j < i; j++)
                this.mCategories.add(paramParcel.readString().intern());
        }
        this.mCategories = null;
        if (paramParcel.readInt() != 0)
            this.mSelector = new Intent(paramParcel);
        if (paramParcel.readInt() != 0)
            this.mClipData = new ClipData(paramParcel);
        this.mExtras = paramParcel.readBundle();
    }

    public void removeCategory(String paramString)
    {
        if (this.mCategories != null)
        {
            this.mCategories.remove(paramString);
            if (this.mCategories.size() == 0)
                this.mCategories = null;
        }
    }

    public void removeExtra(String paramString)
    {
        if (this.mExtras != null)
        {
            this.mExtras.remove(paramString);
            if (this.mExtras.size() == 0)
                this.mExtras = null;
        }
    }

    public Intent replaceExtras(Intent paramIntent)
    {
        if (paramIntent.mExtras != null);
        for (Bundle localBundle = new Bundle(paramIntent.mExtras); ; localBundle = null)
        {
            this.mExtras = localBundle;
            return this;
        }
    }

    public Intent replaceExtras(Bundle paramBundle)
    {
        if (paramBundle != null);
        for (Bundle localBundle = new Bundle(paramBundle); ; localBundle = null)
        {
            this.mExtras = localBundle;
            return this;
        }
    }

    public ComponentName resolveActivity(PackageManager paramPackageManager)
    {
        ComponentName localComponentName;
        if (this.mComponent != null)
            localComponentName = this.mComponent;
        while (true)
        {
            return localComponentName;
            ResolveInfo localResolveInfo = paramPackageManager.resolveActivity(this, 65536);
            if (localResolveInfo != null)
                localComponentName = new ComponentName(localResolveInfo.activityInfo.applicationInfo.packageName, localResolveInfo.activityInfo.name);
            else
                localComponentName = null;
        }
    }

    public ActivityInfo resolveActivityInfo(PackageManager paramPackageManager, int paramInt)
    {
        Object localObject = null;
        if (this.mComponent != null);
        try
        {
            ActivityInfo localActivityInfo = paramPackageManager.getActivityInfo(this.mComponent, paramInt);
            localObject = localActivityInfo;
            while (true)
            {
                label23: return localObject;
                ResolveInfo localResolveInfo = paramPackageManager.resolveActivity(this, 0x10000 | paramInt);
                if (localResolveInfo != null)
                    localObject = localResolveInfo.activityInfo;
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            break label23;
        }
    }

    public String resolveType(ContentResolver paramContentResolver)
    {
        String str;
        if (this.mType != null)
            str = this.mType;
        while (true)
        {
            return str;
            if ((this.mData != null) && ("content".equals(this.mData.getScheme())))
                str = paramContentResolver.getType(this.mData);
            else
                str = null;
        }
    }

    public String resolveType(Context paramContext)
    {
        return resolveType(paramContext.getContentResolver());
    }

    public String resolveTypeIfNeeded(ContentResolver paramContentResolver)
    {
        if (this.mComponent != null);
        for (String str = this.mType; ; str = resolveType(paramContentResolver))
            return str;
    }

    public Intent setAction(String paramString)
    {
        if (paramString != null);
        for (String str = paramString.intern(); ; str = null)
        {
            this.mAction = str;
            return this;
        }
    }

    public void setAllowFds(boolean paramBoolean)
    {
        if (this.mExtras != null)
            this.mExtras.setAllowFds(paramBoolean);
    }

    public Intent setClass(Context paramContext, Class<?> paramClass)
    {
        this.mComponent = new ComponentName(paramContext, paramClass);
        return this;
    }

    public Intent setClassName(Context paramContext, String paramString)
    {
        this.mComponent = new ComponentName(paramContext, paramString);
        return this;
    }

    public Intent setClassName(String paramString1, String paramString2)
    {
        this.mComponent = new ComponentName(paramString1, paramString2);
        return this;
    }

    public void setClipData(ClipData paramClipData)
    {
        this.mClipData = paramClipData;
    }

    public Intent setComponent(ComponentName paramComponentName)
    {
        this.mComponent = paramComponentName;
        return this;
    }

    public Intent setData(Uri paramUri)
    {
        this.mData = paramUri;
        this.mType = null;
        return this;
    }

    public Intent setDataAndNormalize(Uri paramUri)
    {
        return setData(paramUri.normalizeScheme());
    }

    public Intent setDataAndType(Uri paramUri, String paramString)
    {
        this.mData = paramUri;
        this.mType = paramString;
        return this;
    }

    public Intent setDataAndTypeAndNormalize(Uri paramUri, String paramString)
    {
        return setDataAndType(paramUri.normalizeScheme(), normalizeMimeType(paramString));
    }

    public void setExtrasClassLoader(ClassLoader paramClassLoader)
    {
        if (this.mExtras != null)
            this.mExtras.setClassLoader(paramClassLoader);
    }

    public Intent setFlags(int paramInt)
    {
        this.mFlags = paramInt;
        return this;
    }

    public Intent setPackage(String paramString)
    {
        if ((paramString != null) && (this.mSelector != null))
            throw new IllegalArgumentException("Can't set package name when selector is already set");
        this.mPackage = paramString;
        return this;
    }

    public void setSelector(Intent paramIntent)
    {
        if (paramIntent == this)
            throw new IllegalArgumentException("Intent being set as a selector of itself");
        if ((paramIntent != null) && (this.mPackage != null))
            throw new IllegalArgumentException("Can't set selector when package name is already set");
        this.mSelector = paramIntent;
    }

    public void setSourceBounds(Rect paramRect)
    {
        if (paramRect != null);
        for (this.mSourceBounds = new Rect(paramRect); ; this.mSourceBounds = null)
            return;
    }

    public Intent setType(String paramString)
    {
        this.mData = null;
        this.mType = paramString;
        return this;
    }

    public Intent setTypeAndNormalize(String paramString)
    {
        return setType(normalizeMimeType(paramString));
    }

    public String toInsecureString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("Intent { ");
        toShortString(localStringBuilder, false, true, true, false);
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    public String toInsecureStringWithClip()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("Intent { ");
        toShortString(localStringBuilder, false, true, true, true);
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    public String toShortString(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        toShortString(localStringBuilder, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4);
        return localStringBuilder.toString();
    }

    public void toShortString(StringBuilder paramStringBuilder, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
    {
        int i = 1;
        if (this.mAction != null)
        {
            paramStringBuilder.append("act=").append(this.mAction);
            i = 0;
        }
        if (this.mCategories != null)
        {
            if (i == 0)
                paramStringBuilder.append(' ');
            i = 0;
            paramStringBuilder.append("cat=[");
            Iterator localIterator = this.mCategories.iterator();
            int j = 0;
            while (localIterator.hasNext())
            {
                if (j != 0)
                    paramStringBuilder.append(",");
                j = 1;
                paramStringBuilder.append((String)localIterator.next());
            }
            paramStringBuilder.append("]");
        }
        if (this.mData != null)
        {
            if (i == 0)
                paramStringBuilder.append(' ');
            i = 0;
            paramStringBuilder.append("dat=");
            if (paramBoolean1)
                paramStringBuilder.append(this.mData.toSafeString());
        }
        else
        {
            if (this.mType != null)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append("typ=").append(this.mType);
            }
            if (this.mFlags != 0)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append("flg=0x").append(Integer.toHexString(this.mFlags));
            }
            if (this.mPackage != null)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append("pkg=").append(this.mPackage);
            }
            if ((paramBoolean2) && (this.mComponent != null))
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append("cmp=").append(this.mComponent.flattenToShortString());
            }
            if (this.mSourceBounds != null)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append("bnds=").append(this.mSourceBounds.toShortString());
            }
            if (this.mClipData != null)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                if (!paramBoolean4)
                    break label498;
                paramStringBuilder.append("clip={");
                this.mClipData.toShortString(paramStringBuilder);
                paramStringBuilder.append('}');
            }
        }
        while (true)
        {
            if ((paramBoolean3) && (this.mExtras != null))
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                paramStringBuilder.append("(has extras)");
            }
            if (this.mSelector != null)
            {
                paramStringBuilder.append(" sel={");
                this.mSelector.toShortString(paramStringBuilder, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4);
                paramStringBuilder.append("}");
            }
            return;
            paramStringBuilder.append(this.mData);
            break;
            label498: paramStringBuilder.append("(has clip)");
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("Intent { ");
        toShortString(localStringBuilder, true, true, true, false);
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    @Deprecated
    public String toURI()
    {
        return toUri(0);
    }

    public String toUri(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        String str1 = null;
        if (this.mData != null)
        {
            String str2 = this.mData.toString();
            if ((paramInt & 0x1) != 0)
            {
                int i = str2.length();
                int j = 0;
                while (j < i)
                {
                    int k = str2.charAt(j);
                    if (((k >= 97) && (k <= 122)) || ((k >= 65) && (k <= 90)) || (k == 46) || (k == 45))
                    {
                        j++;
                    }
                    else if ((k == 58) && (j > 0))
                    {
                        str1 = str2.substring(0, j);
                        localStringBuilder.append("intent:");
                        str2 = str2.substring(j + 1);
                    }
                }
            }
            localStringBuilder.append(str2);
        }
        while (true)
        {
            localStringBuilder.append("#Intent;");
            toUriInner(localStringBuilder, str1, paramInt);
            if (this.mSelector != null)
            {
                localStringBuilder.append("SEL;");
                this.mSelector.toUriInner(localStringBuilder, null, paramInt);
            }
            localStringBuilder.append("end");
            return localStringBuilder.toString();
            if ((paramInt & 0x1) != 0)
                localStringBuilder.append("intent:");
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mAction);
        Uri.writeToParcel(paramParcel, this.mData);
        paramParcel.writeString(this.mType);
        paramParcel.writeInt(this.mFlags);
        paramParcel.writeString(this.mPackage);
        ComponentName.writeToParcel(this.mComponent, paramParcel);
        if (this.mSourceBounds != null)
        {
            paramParcel.writeInt(1);
            this.mSourceBounds.writeToParcel(paramParcel, paramInt);
        }
        while (this.mCategories != null)
        {
            paramParcel.writeInt(this.mCategories.size());
            Iterator localIterator = this.mCategories.iterator();
            while (localIterator.hasNext())
                paramParcel.writeString((String)localIterator.next());
            paramParcel.writeInt(0);
        }
        paramParcel.writeInt(0);
        if (this.mSelector != null)
        {
            paramParcel.writeInt(1);
            this.mSelector.writeToParcel(paramParcel, paramInt);
            if (this.mClipData == null)
                break label192;
            paramParcel.writeInt(1);
            this.mClipData.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            paramParcel.writeBundle(this.mExtras);
            return;
            paramParcel.writeInt(0);
            break;
            label192: paramParcel.writeInt(0);
        }
    }

    public static final class FilterComparison
    {
        private final int mHashCode;
        private final Intent mIntent;

        public FilterComparison(Intent paramIntent)
        {
            this.mIntent = paramIntent;
            this.mHashCode = paramIntent.filterHashCode();
        }

        public boolean equals(Object paramObject)
        {
            Intent localIntent;
            if ((paramObject instanceof FilterComparison))
                localIntent = ((FilterComparison)paramObject).mIntent;
            for (boolean bool = this.mIntent.filterEquals(localIntent); ; bool = false)
                return bool;
        }

        public Intent getIntent()
        {
            return this.mIntent;
        }

        public int hashCode()
        {
            return this.mHashCode;
        }
    }

    public static class ShortcutIconResource
        implements Parcelable
    {
        public static final Parcelable.Creator<ShortcutIconResource> CREATOR = new Parcelable.Creator()
        {
            public Intent.ShortcutIconResource createFromParcel(Parcel paramAnonymousParcel)
            {
                Intent.ShortcutIconResource localShortcutIconResource = new Intent.ShortcutIconResource();
                localShortcutIconResource.packageName = paramAnonymousParcel.readString();
                localShortcutIconResource.resourceName = paramAnonymousParcel.readString();
                return localShortcutIconResource;
            }

            public Intent.ShortcutIconResource[] newArray(int paramAnonymousInt)
            {
                return new Intent.ShortcutIconResource[paramAnonymousInt];
            }
        };
        public String packageName;
        public String resourceName;

        public static ShortcutIconResource fromContext(Context paramContext, int paramInt)
        {
            ShortcutIconResource localShortcutIconResource = new ShortcutIconResource();
            localShortcutIconResource.packageName = paramContext.getPackageName();
            localShortcutIconResource.resourceName = paramContext.getResources().getResourceName(paramInt);
            return localShortcutIconResource;
        }

        public int describeContents()
        {
            return 0;
        }

        public String toString()
        {
            return this.resourceName;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeString(this.packageName);
            paramParcel.writeString(this.resourceName);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.Intent
 * JD-Core Version:        0.6.2
 */