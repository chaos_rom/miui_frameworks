package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class SyncResult
    implements Parcelable
{
    public static final SyncResult ALREADY_IN_PROGRESS = new SyncResult(true);
    public static final Parcelable.Creator<SyncResult> CREATOR = new Parcelable.Creator()
    {
        public SyncResult createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SyncResult(paramAnonymousParcel, null);
        }

        public SyncResult[] newArray(int paramAnonymousInt)
        {
            return new SyncResult[paramAnonymousInt];
        }
    };
    public boolean databaseError;
    public long delayUntil;
    public boolean fullSyncRequested;
    public boolean moreRecordsToGet;
    public boolean partialSyncUnavailable;
    public final SyncStats stats;
    public final boolean syncAlreadyInProgress;
    public boolean tooManyDeletions;
    public boolean tooManyRetries;

    public SyncResult()
    {
        this(false);
    }

    private SyncResult(Parcel paramParcel)
    {
        boolean bool2;
        boolean bool3;
        label30: boolean bool4;
        label46: boolean bool5;
        label62: boolean bool6;
        label78: boolean bool7;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.syncAlreadyInProgress = bool2;
            if (paramParcel.readInt() == 0)
                break label138;
            bool3 = bool1;
            this.tooManyDeletions = bool3;
            if (paramParcel.readInt() == 0)
                break label144;
            bool4 = bool1;
            this.tooManyRetries = bool4;
            if (paramParcel.readInt() == 0)
                break label150;
            bool5 = bool1;
            this.databaseError = bool5;
            if (paramParcel.readInt() == 0)
                break label156;
            bool6 = bool1;
            this.fullSyncRequested = bool6;
            if (paramParcel.readInt() == 0)
                break label162;
            bool7 = bool1;
            label94: this.partialSyncUnavailable = bool7;
            if (paramParcel.readInt() == 0)
                break label168;
        }
        while (true)
        {
            this.moreRecordsToGet = bool1;
            this.delayUntil = paramParcel.readLong();
            this.stats = new SyncStats(paramParcel);
            return;
            bool2 = false;
            break;
            label138: bool3 = false;
            break label30;
            label144: bool4 = false;
            break label46;
            label150: bool5 = false;
            break label62;
            label156: bool6 = false;
            break label78;
            label162: bool7 = false;
            break label94;
            label168: bool1 = false;
        }
    }

    private SyncResult(boolean paramBoolean)
    {
        this.syncAlreadyInProgress = paramBoolean;
        this.tooManyDeletions = false;
        this.tooManyRetries = false;
        this.fullSyncRequested = false;
        this.partialSyncUnavailable = false;
        this.moreRecordsToGet = false;
        this.delayUntil = 0L;
        this.stats = new SyncStats();
    }

    public void clear()
    {
        if (this.syncAlreadyInProgress)
            throw new UnsupportedOperationException("you are not allowed to clear the ALREADY_IN_PROGRESS SyncStats");
        this.tooManyDeletions = false;
        this.tooManyRetries = false;
        this.databaseError = false;
        this.fullSyncRequested = false;
        this.partialSyncUnavailable = false;
        this.moreRecordsToGet = false;
        this.delayUntil = 0L;
        this.stats.clear();
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean hasError()
    {
        if ((hasSoftError()) || (hasHardError()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasHardError()
    {
        if ((this.stats.numParseExceptions > 0L) || (this.stats.numConflictDetectedExceptions > 0L) || (this.stats.numAuthExceptions > 0L) || (this.tooManyDeletions) || (this.tooManyRetries) || (this.databaseError));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasSoftError()
    {
        if ((this.syncAlreadyInProgress) || (this.stats.numIoExceptions > 0L));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean madeSomeProgress()
    {
        if (((this.stats.numDeletes > 0L) && (!this.tooManyDeletions)) || (this.stats.numInserts > 0L) || (this.stats.numUpdates > 0L));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toDebugString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        if (this.fullSyncRequested)
            localStringBuffer.append("f1");
        if (this.partialSyncUnavailable)
            localStringBuffer.append("r1");
        if (hasHardError())
            localStringBuffer.append("X1");
        if (this.stats.numParseExceptions > 0L)
            localStringBuffer.append("e").append(this.stats.numParseExceptions);
        if (this.stats.numConflictDetectedExceptions > 0L)
            localStringBuffer.append("c").append(this.stats.numConflictDetectedExceptions);
        if (this.stats.numAuthExceptions > 0L)
            localStringBuffer.append("a").append(this.stats.numAuthExceptions);
        if (this.tooManyDeletions)
            localStringBuffer.append("D1");
        if (this.tooManyRetries)
            localStringBuffer.append("R1");
        if (this.databaseError)
            localStringBuffer.append("b1");
        if (hasSoftError())
            localStringBuffer.append("x1");
        if (this.syncAlreadyInProgress)
            localStringBuffer.append("l1");
        if (this.stats.numIoExceptions > 0L)
            localStringBuffer.append("I").append(this.stats.numIoExceptions);
        return localStringBuffer.toString();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("SyncResult:");
        if (this.syncAlreadyInProgress)
            localStringBuilder.append(" syncAlreadyInProgress: ").append(this.syncAlreadyInProgress);
        if (this.tooManyDeletions)
            localStringBuilder.append(" tooManyDeletions: ").append(this.tooManyDeletions);
        if (this.tooManyRetries)
            localStringBuilder.append(" tooManyRetries: ").append(this.tooManyRetries);
        if (this.databaseError)
            localStringBuilder.append(" databaseError: ").append(this.databaseError);
        if (this.fullSyncRequested)
            localStringBuilder.append(" fullSyncRequested: ").append(this.fullSyncRequested);
        if (this.partialSyncUnavailable)
            localStringBuilder.append(" partialSyncUnavailable: ").append(this.partialSyncUnavailable);
        if (this.moreRecordsToGet)
            localStringBuilder.append(" moreRecordsToGet: ").append(this.moreRecordsToGet);
        if (this.delayUntil > 0L)
            localStringBuilder.append(" delayUntil: ").append(this.delayUntil);
        localStringBuilder.append(this.stats);
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        int j;
        int k;
        label28: int m;
        label44: int n;
        label60: int i1;
        label76: int i2;
        if (this.syncAlreadyInProgress)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.tooManyDeletions)
                break label134;
            k = i;
            paramParcel.writeInt(k);
            if (!this.tooManyRetries)
                break label140;
            m = i;
            paramParcel.writeInt(m);
            if (!this.databaseError)
                break label146;
            n = i;
            paramParcel.writeInt(n);
            if (!this.fullSyncRequested)
                break label152;
            i1 = i;
            paramParcel.writeInt(i1);
            if (!this.partialSyncUnavailable)
                break label158;
            i2 = i;
            label92: paramParcel.writeInt(i2);
            if (!this.moreRecordsToGet)
                break label164;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeLong(this.delayUntil);
            this.stats.writeToParcel(paramParcel, paramInt);
            return;
            j = 0;
            break;
            label134: k = 0;
            break label28;
            label140: m = 0;
            break label44;
            label146: n = 0;
            break label60;
            label152: i1 = 0;
            break label76;
            label158: i2 = 0;
            break label92;
            label164: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncResult
 * JD-Core Version:        0.6.2
 */