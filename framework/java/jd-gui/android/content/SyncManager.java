package android.content;

import android.accounts.Account;
import android.accounts.AccountAndUser;
import android.accounts.AccountManager;
import android.accounts.AccountManagerService;
import android.accounts.OnAccountsUpdateListener;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppGlobals;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.RegisteredServicesCache.ServiceInfo;
import android.content.pm.RegisteredServicesCacheListener;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.WorkSource;
import android.provider.Settings.Secure;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.EventLog;
import android.util.Log;
import android.util.Pair;
import com.google.android.collect.Lists;
import com.google.android.collect.Maps;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class SyncManager
    implements OnAccountsUpdateListener
{
    private static final String ACTION_SYNC_ALARM = "android.content.syncmanager.SYNC_ALARM";
    private static final long DEFAULT_MAX_SYNC_RETRY_TIME_IN_SECONDS = 3600L;
    private static final int DELAY_RETRY_SYNC_IN_PROGRESS_IN_SECONDS = 10;
    private static final String HANDLE_SYNC_ALARM_WAKE_LOCK = "SyncManagerHandleSyncAlarm";
    private static final int INITIALIZATION_UNBIND_DELAY_MS = 5000;
    private static final AccountAndUser[] INITIAL_ACCOUNTS_ARRAY;
    private static final long INITIAL_SYNC_RETRY_TIME_IN_MS = 30000L;
    private static final long LOCAL_SYNC_DELAY = 0L;
    private static final int MAX_SIMULTANEOUS_INITIALIZATION_SYNCS = 0;
    private static final int MAX_SIMULTANEOUS_REGULAR_SYNCS = 0;
    private static final long MAX_TIME_PER_SYNC = 0L;
    private static final long SYNC_ALARM_TIMEOUT_MAX = 7200000L;
    private static final long SYNC_ALARM_TIMEOUT_MIN = 30000L;
    private static final String SYNC_LOOP_WAKE_LOCK = "SyncLoopWakeLock";
    private static final long SYNC_NOTIFICATION_DELAY = 0L;
    private static final String SYNC_WAKE_LOCK_PREFIX = "*sync*";
    private static final String TAG = "SyncManager";
    private volatile AccountAndUser[] mAccounts = INITIAL_ACCOUNTS_ARRAY;
    protected final ArrayList<ActiveSyncContext> mActiveSyncContexts = Lists.newArrayList();
    private AlarmManager mAlarmService = null;
    private BroadcastReceiver mBackgroundDataSettingChanged = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (SyncManager.this.getConnectivityManager().getBackgroundDataSetting())
                SyncManager.this.scheduleSync(null, -1, null, new Bundle(), 0L, false);
        }
    };
    private volatile boolean mBootCompleted = false;
    private BroadcastReceiver mBootCompletedReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            SyncManager.this.mSyncHandler.onBootCompleted();
        }
    };
    private ConnectivityManager mConnManagerDoNotUseDirectly;
    private BroadcastReceiver mConnectivityIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            boolean bool = SyncManager.this.mDataConnectionIsConnected;
            SyncManager.access$402(SyncManager.this, SyncManager.this.readDataConnectionState());
            if (SyncManager.this.mDataConnectionIsConnected)
            {
                if (!bool)
                {
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "Reconnection detected: clearing all backoffs");
                    SyncManager.this.mSyncStorageEngine.clearAllBackoffs(SyncManager.this.mSyncQueue);
                }
                SyncManager.this.sendCheckAlarmsMessage();
            }
        }
    };
    private Context mContext;
    private volatile boolean mDataConnectionIsConnected = false;
    private volatile PowerManager.WakeLock mHandleAlarmWakeLock;
    private boolean mNeedSyncActiveNotification = false;
    private final NotificationManager mNotificationMgr;
    private final PowerManager mPowerManager;
    private BroadcastReceiver mShutdownIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            Log.w("SyncManager", "Writing sync state before shutdown...");
            SyncManager.this.getSyncStorageEngine().writeAllState();
        }
    };
    private BroadcastReceiver mStorageIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if ("android.intent.action.DEVICE_STORAGE_LOW".equals(str))
            {
                if (Log.isLoggable("SyncManager", 2))
                    Log.v("SyncManager", "Internal storage is low.");
                SyncManager.access$002(SyncManager.this, true);
                SyncManager.this.cancelActiveSync(null, -1, null);
            }
            while (true)
            {
                return;
                if ("android.intent.action.DEVICE_STORAGE_OK".equals(str))
                {
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "Internal storage is ok.");
                    SyncManager.access$002(SyncManager.this, false);
                    SyncManager.this.sendCheckAlarmsMessage();
                }
            }
        }
    };
    private volatile boolean mStorageIsLow = false;
    protected SyncAdaptersCache mSyncAdapters;
    private final PendingIntent mSyncAlarmIntent;
    private final SyncHandler mSyncHandler;
    private volatile PowerManager.WakeLock mSyncManagerWakeLock;
    public SyncQueue mSyncQueue;
    private int mSyncRandomOffsetMillis;
    private SyncStorageEngine mSyncStorageEngine;
    private BroadcastReceiver mUserIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            SyncManager.this.onUserRemoved(paramAnonymousIntent);
        }
    };

    static
    {
        int i = 2;
        boolean bool = ActivityManager.isLargeRAM();
        int j;
        if (bool)
        {
            j = 5;
            if (!bool)
                break label80;
        }
        while (true)
        {
            MAX_SIMULTANEOUS_INITIALIZATION_SYNCS = SystemProperties.getInt("sync.max_init_syncs", j);
            MAX_SIMULTANEOUS_REGULAR_SYNCS = SystemProperties.getInt("sync.max_regular_syncs", i);
            LOCAL_SYNC_DELAY = SystemProperties.getLong("sync.local_sync_delay", 30000L);
            MAX_TIME_PER_SYNC = SystemProperties.getLong("sync.max_time_per_sync", 300000L);
            SYNC_NOTIFICATION_DELAY = SystemProperties.getLong("sync.notification_delay", 30000L);
            INITIAL_ACCOUNTS_ARRAY = new AccountAndUser[0];
            return;
            j = i;
            break;
            label80: i = 1;
        }
    }

    public SyncManager(Context paramContext, boolean paramBoolean)
    {
        this.mContext = paramContext;
        SyncStorageEngine.init(paramContext);
        this.mSyncStorageEngine = SyncStorageEngine.getSingleton();
        this.mSyncStorageEngine.setOnSyncRequestListener(new SyncStorageEngine.OnSyncRequestListener()
        {
            public void onSyncRequest(Account paramAnonymousAccount, int paramAnonymousInt, String paramAnonymousString, Bundle paramAnonymousBundle)
            {
                SyncManager.this.scheduleSync(paramAnonymousAccount, paramAnonymousInt, paramAnonymousString, paramAnonymousBundle, 0L, false);
            }
        });
        this.mSyncAdapters = new SyncAdaptersCache(this.mContext);
        this.mSyncQueue = new SyncQueue(this.mSyncStorageEngine, this.mSyncAdapters);
        HandlerThread localHandlerThread = new HandlerThread("SyncHandlerThread", 10);
        localHandlerThread.start();
        this.mSyncHandler = new SyncHandler(localHandlerThread.getLooper());
        this.mSyncAdapters.setListener(new RegisteredServicesCacheListener()
        {
            public void onServiceChanged(SyncAdapterType paramAnonymousSyncAdapterType, boolean paramAnonymousBoolean)
            {
                if (!paramAnonymousBoolean)
                    SyncManager.this.scheduleSync(null, -1, paramAnonymousSyncAdapterType.authority, null, 0L, false);
            }
        }
        , this.mSyncHandler);
        this.mSyncAlarmIntent = PendingIntent.getBroadcast(this.mContext, 0, new Intent("android.content.syncmanager.SYNC_ALARM"), 0);
        IntentFilter localIntentFilter1 = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        paramContext.registerReceiver(this.mConnectivityIntentReceiver, localIntentFilter1);
        if (!paramBoolean)
        {
            IntentFilter localIntentFilter2 = new IntentFilter("android.intent.action.BOOT_COMPLETED");
            paramContext.registerReceiver(this.mBootCompletedReceiver, localIntentFilter2);
        }
        IntentFilter localIntentFilter3 = new IntentFilter("android.net.conn.BACKGROUND_DATA_SETTING_CHANGED");
        paramContext.registerReceiver(this.mBackgroundDataSettingChanged, localIntentFilter3);
        IntentFilter localIntentFilter4 = new IntentFilter("android.intent.action.DEVICE_STORAGE_LOW");
        localIntentFilter4.addAction("android.intent.action.DEVICE_STORAGE_OK");
        paramContext.registerReceiver(this.mStorageIntentReceiver, localIntentFilter4);
        IntentFilter localIntentFilter5 = new IntentFilter("android.intent.action.ACTION_SHUTDOWN");
        localIntentFilter5.setPriority(100);
        paramContext.registerReceiver(this.mShutdownIntentReceiver, localIntentFilter5);
        IntentFilter localIntentFilter6 = new IntentFilter();
        localIntentFilter6.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiver(this.mUserIntentReceiver, localIntentFilter6);
        if (!paramBoolean)
        {
            this.mNotificationMgr = ((NotificationManager)paramContext.getSystemService("notification"));
            paramContext.registerReceiver(new SyncAlarmIntentReceiver(), new IntentFilter("android.content.syncmanager.SYNC_ALARM"));
        }
        while (true)
        {
            this.mPowerManager = ((PowerManager)paramContext.getSystemService("power"));
            this.mHandleAlarmWakeLock = this.mPowerManager.newWakeLock(1, "SyncManagerHandleSyncAlarm");
            this.mHandleAlarmWakeLock.setReferenceCounted(false);
            this.mSyncManagerWakeLock = this.mPowerManager.newWakeLock(1, "SyncLoopWakeLock");
            this.mSyncManagerWakeLock.setReferenceCounted(false);
            this.mSyncStorageEngine.addStatusChangeListener(1, new ISyncStatusObserver.Stub()
            {
                public void onStatusChanged(int paramAnonymousInt)
                {
                    SyncManager.this.sendCheckAlarmsMessage();
                }
            });
            if (!paramBoolean)
            {
                AccountManager.get(this.mContext).addOnAccountsUpdatedListener(this, this.mSyncHandler, false);
                onAccountsUpdated(null);
            }
            this.mSyncRandomOffsetMillis = (1000 * this.mSyncStorageEngine.getSyncRandomOffset());
            return;
            this.mNotificationMgr = null;
        }
    }

    private void clearBackoffSetting(SyncOperation paramSyncOperation)
    {
        this.mSyncStorageEngine.setBackoff(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority, -1L, -1L);
        synchronized (this.mSyncQueue)
        {
            this.mSyncQueue.onBackoffChanged(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority, 0L);
            return;
        }
    }

    private boolean containsAccountAndUser(AccountAndUser[] paramArrayOfAccountAndUser, Account paramAccount, int paramInt)
    {
        boolean bool = false;
        for (int i = 0; ; i++)
            if (i < paramArrayOfAccountAndUser.length)
            {
                if ((paramArrayOfAccountAndUser[i].userId == paramInt) && (paramArrayOfAccountAndUser[i].account.equals(paramAccount)))
                    bool = true;
            }
            else
                return bool;
    }

    private void dumpDayStatistic(PrintWriter paramPrintWriter, SyncStorageEngine.DayStats paramDayStats)
    {
        paramPrintWriter.print("Success (");
        paramPrintWriter.print(paramDayStats.successCount);
        if (paramDayStats.successCount > 0)
        {
            paramPrintWriter.print(" for ");
            dumpTimeSec(paramPrintWriter, paramDayStats.successTime);
            paramPrintWriter.print(" avg=");
            dumpTimeSec(paramPrintWriter, paramDayStats.successTime / paramDayStats.successCount);
        }
        paramPrintWriter.print(") Failure (");
        paramPrintWriter.print(paramDayStats.failureCount);
        if (paramDayStats.failureCount > 0)
        {
            paramPrintWriter.print(" for ");
            dumpTimeSec(paramPrintWriter, paramDayStats.failureTime);
            paramPrintWriter.print(" avg=");
            dumpTimeSec(paramPrintWriter, paramDayStats.failureTime / paramDayStats.failureCount);
        }
        paramPrintWriter.println(")");
    }

    private void dumpDayStatistics(PrintWriter paramPrintWriter)
    {
        SyncStorageEngine.DayStats[] arrayOfDayStats = this.mSyncStorageEngine.getDayStatistics();
        if ((arrayOfDayStats != null) && (arrayOfDayStats[0] != null))
        {
            paramPrintWriter.println();
            paramPrintWriter.println("Sync Statistics");
            paramPrintWriter.print("    Today:    ");
            dumpDayStatistic(paramPrintWriter, arrayOfDayStats[0]);
            int i = arrayOfDayStats[0].day;
            int j = 1;
            SyncStorageEngine.DayStats localDayStats3;
            label79: int k;
            label81: label82: SyncStorageEngine.DayStats localDayStats1;
            if ((j <= 6) && (j < arrayOfDayStats.length))
            {
                localDayStats3 = arrayOfDayStats[j];
                if (localDayStats3 != null);
            }
            else
            {
                k = i;
                break label117;
                if (j >= arrayOfDayStats.length)
                    return;
                localDayStats1 = null;
                k -= 7;
            }
            while (true)
            {
                SyncStorageEngine.DayStats localDayStats2;
                if (j < arrayOfDayStats.length)
                {
                    localDayStats2 = arrayOfDayStats[j];
                    if (localDayStats2 == null)
                        j = arrayOfDayStats.length;
                }
                else
                {
                    label117: if (localDayStats1 == null)
                        break label82;
                    paramPrintWriter.print("    Week-");
                    paramPrintWriter.print((i - k) / 7);
                    paramPrintWriter.print(": ");
                    dumpDayStatistic(paramPrintWriter, localDayStats1);
                    break label82;
                    int m = i - localDayStats3.day;
                    if (m > 6)
                        break label79;
                    paramPrintWriter.print("    Day-");
                    paramPrintWriter.print(m);
                    paramPrintWriter.print(":    ");
                    dumpDayStatistic(paramPrintWriter, localDayStats3);
                    j++;
                    break;
                }
                if (k - localDayStats2.day > 6)
                    break label81;
                j++;
                if (localDayStats1 == null)
                    localDayStats1 = new SyncStorageEngine.DayStats(k);
                localDayStats1.successCount += localDayStats2.successCount;
                localDayStats1.successTime += localDayStats2.successTime;
                localDayStats1.failureCount += localDayStats2.failureCount;
                localDayStats1.failureTime += localDayStats2.failureTime;
            }
        }
    }

    private void dumpRecentHistory(PrintWriter paramPrintWriter)
    {
        ArrayList localArrayList1 = this.mSyncStorageEngine.getSyncHistory();
        if ((localArrayList1 != null) && (localArrayList1.size() > 0))
        {
            HashMap localHashMap1 = Maps.newHashMap();
            long l1 = 0L;
            long l2 = 0L;
            int i = localArrayList1.size();
            int j = 0;
            int k = 0;
            Iterator localIterator1 = localArrayList1.iterator();
            if (localIterator1.hasNext())
            {
                SyncStorageEngine.SyncHistoryItem localSyncHistoryItem2 = (SyncStorageEngine.SyncHistoryItem)localIterator1.next();
                SyncStorageEngine.AuthorityInfo localAuthorityInfo2 = this.mSyncStorageEngine.getAuthority(localSyncHistoryItem2.authorityId);
                String str14;
                if (localAuthorityInfo2 != null)
                    str14 = localAuthorityInfo2.authority;
                for (String str15 = localAuthorityInfo2.account.name + "/" + localAuthorityInfo2.account.type; ; str15 = "Unknown")
                {
                    int i4 = str14.length();
                    if (i4 > j)
                        j = i4;
                    int i5 = str15.length();
                    if (i5 > k)
                        k = i5;
                    long l9 = localSyncHistoryItem2.elapsedTime;
                    l1 += l9;
                    l2 += 1L;
                    AuthoritySyncStats localAuthoritySyncStats2 = (AuthoritySyncStats)localHashMap1.get(str14);
                    if (localAuthoritySyncStats2 == null)
                    {
                        localAuthoritySyncStats2 = new AuthoritySyncStats(str14, null);
                        localHashMap1.put(str14, localAuthoritySyncStats2);
                    }
                    localAuthoritySyncStats2.elapsedTime = (l9 + localAuthoritySyncStats2.elapsedTime);
                    localAuthoritySyncStats2.times = (1 + localAuthoritySyncStats2.times);
                    Map localMap = localAuthoritySyncStats2.accountMap;
                    AccountSyncStats localAccountSyncStats2 = (AccountSyncStats)localMap.get(str15);
                    if (localAccountSyncStats2 == null)
                    {
                        localAccountSyncStats2 = new AccountSyncStats(str15, null);
                        localMap.put(str15, localAccountSyncStats2);
                    }
                    localAccountSyncStats2.elapsedTime = (l9 + localAccountSyncStats2.elapsedTime);
                    localAccountSyncStats2.times = (1 + localAccountSyncStats2.times);
                    break;
                    str14 = "Unknown";
                }
            }
            if (l1 > 0L)
            {
                paramPrintWriter.println();
                Object[] arrayOfObject7 = new Object[2];
                arrayOfObject7[0] = Long.valueOf(l2);
                arrayOfObject7[1] = Long.valueOf(l1 / 1000L);
                paramPrintWriter.printf("Detailed Statistics (Recent history):    %d (# of times) %ds (sync time)\n", arrayOfObject7);
                ArrayList localArrayList2 = new ArrayList(localHashMap1.values());
                Comparator local10 = new Comparator()
                {
                    public int compare(SyncManager.AuthoritySyncStats paramAnonymousAuthoritySyncStats1, SyncManager.AuthoritySyncStats paramAnonymousAuthoritySyncStats2)
                    {
                        int i = Integer.compare(paramAnonymousAuthoritySyncStats2.times, paramAnonymousAuthoritySyncStats1.times);
                        if (i == 0)
                            i = Long.compare(paramAnonymousAuthoritySyncStats2.elapsedTime, paramAnonymousAuthoritySyncStats1.elapsedTime);
                        return i;
                    }
                };
                Collections.sort(localArrayList2, local10);
                int n = k + 3;
                int i1 = Math.max(j, n);
                char[] arrayOfChar = new char[11 + (10 + (2 + (i1 + 4)))];
                Arrays.fill(arrayOfChar, '-');
                String str6 = new String(arrayOfChar);
                Object[] arrayOfObject8 = new Object[1];
                arrayOfObject8[0] = Integer.valueOf(i1 + 2);
                String str7 = String.format("    %%-%ds: %%-9s    %%-11s\n", arrayOfObject8);
                Object[] arrayOfObject9 = new Object[1];
                arrayOfObject9[0] = Integer.valueOf(i1);
                String str8 = String.format("        %%-%ds:     %%-9s    %%-11s\n", arrayOfObject9);
                paramPrintWriter.println(str6);
                Iterator localIterator2 = localArrayList2.iterator();
                while (localIterator2.hasNext())
                {
                    AuthoritySyncStats localAuthoritySyncStats1 = (AuthoritySyncStats)localIterator2.next();
                    String str9 = localAuthoritySyncStats1.name;
                    long l7 = localAuthoritySyncStats1.elapsedTime;
                    int i2 = localAuthoritySyncStats1.times;
                    Object[] arrayOfObject10 = new Object[2];
                    arrayOfObject10[0] = Long.valueOf(l7 / 1000L);
                    arrayOfObject10[1] = Long.valueOf(100L * l7 / l1);
                    String str10 = String.format("%ds/%d%%", arrayOfObject10);
                    Object[] arrayOfObject11 = new Object[2];
                    arrayOfObject11[0] = Integer.valueOf(i2);
                    arrayOfObject11[1] = Long.valueOf(i2 * 100 / l2);
                    String str11 = String.format("%d/%d%%", arrayOfObject11);
                    Object[] arrayOfObject12 = new Object[3];
                    arrayOfObject12[0] = str9;
                    arrayOfObject12[1] = str11;
                    arrayOfObject12[2] = str10;
                    paramPrintWriter.printf(str7, arrayOfObject12);
                    ArrayList localArrayList3 = new ArrayList(localAuthoritySyncStats1.accountMap.values());
                    Comparator local11 = new Comparator()
                    {
                        public int compare(SyncManager.AccountSyncStats paramAnonymousAccountSyncStats1, SyncManager.AccountSyncStats paramAnonymousAccountSyncStats2)
                        {
                            int i = Integer.compare(paramAnonymousAccountSyncStats2.times, paramAnonymousAccountSyncStats1.times);
                            if (i == 0)
                                i = Long.compare(paramAnonymousAccountSyncStats2.elapsedTime, paramAnonymousAccountSyncStats1.elapsedTime);
                            return i;
                        }
                    };
                    Collections.sort(localArrayList3, local11);
                    Iterator localIterator3 = localArrayList3.iterator();
                    while (localIterator3.hasNext())
                    {
                        AccountSyncStats localAccountSyncStats1 = (AccountSyncStats)localIterator3.next();
                        long l8 = localAccountSyncStats1.elapsedTime;
                        int i3 = localAccountSyncStats1.times;
                        Object[] arrayOfObject13 = new Object[2];
                        arrayOfObject13[0] = Long.valueOf(l8 / 1000L);
                        arrayOfObject13[1] = Long.valueOf(100L * l8 / l1);
                        String str12 = String.format("%ds/%d%%", arrayOfObject13);
                        Object[] arrayOfObject14 = new Object[2];
                        arrayOfObject14[0] = Integer.valueOf(i3);
                        arrayOfObject14[1] = Long.valueOf(i3 * 100 / l2);
                        String str13 = String.format("%d/%d%%", arrayOfObject14);
                        Object[] arrayOfObject15 = new Object[3];
                        arrayOfObject15[0] = localAccountSyncStats1.name;
                        arrayOfObject15[1] = str13;
                        arrayOfObject15[2] = str12;
                        paramPrintWriter.printf(str8, arrayOfObject15);
                    }
                    paramPrintWriter.println(str6);
                }
            }
            paramPrintWriter.println();
            paramPrintWriter.println("Recent Sync History");
            String str1 = "    %-" + k + "s    %s\n";
            HashMap localHashMap2 = Maps.newHashMap();
            int m = 0;
            if (m < i)
            {
                SyncStorageEngine.SyncHistoryItem localSyncHistoryItem1 = (SyncStorageEngine.SyncHistoryItem)localArrayList1.get(m);
                SyncStorageEngine.AuthorityInfo localAuthorityInfo1 = this.mSyncStorageEngine.getAuthority(localSyncHistoryItem1.authorityId);
                String str2;
                String str3;
                label1054: long l3;
                long l4;
                String str4;
                Long localLong;
                String str5;
                if (localAuthorityInfo1 != null)
                {
                    str2 = localAuthorityInfo1.authority;
                    str3 = localAuthorityInfo1.account.name + "/" + localAuthorityInfo1.account.type;
                    l3 = localSyncHistoryItem1.elapsedTime;
                    Time localTime = new Time();
                    l4 = localSyncHistoryItem1.eventTime;
                    localTime.set(l4);
                    str4 = str2 + "/" + str3;
                    localLong = (Long)localHashMap2.get(str4);
                    if (localLong != null)
                        break label1394;
                    str5 = "";
                }
                while (true)
                {
                    localHashMap2.put(str4, Long.valueOf(l4));
                    Object[] arrayOfObject2 = new Object[5];
                    arrayOfObject2[0] = Integer.valueOf(m + 1);
                    arrayOfObject2[1] = formatTime(l4);
                    arrayOfObject2[2] = SyncStorageEngine.SOURCES[localSyncHistoryItem1.source];
                    arrayOfObject2[3] = Float.valueOf((float)l3 / 1000.0F);
                    arrayOfObject2[4] = str5;
                    paramPrintWriter.printf("    #%-3d: %s %8s    %5.1fs    %8s", arrayOfObject2);
                    Object[] arrayOfObject3 = new Object[2];
                    arrayOfObject3[0] = str3;
                    arrayOfObject3[1] = str2;
                    paramPrintWriter.printf(str1, arrayOfObject3);
                    if ((localSyncHistoryItem1.event != 1) || (localSyncHistoryItem1.upstreamActivity != 0L) || (localSyncHistoryItem1.downstreamActivity != 0L))
                    {
                        Object[] arrayOfObject4 = new Object[3];
                        arrayOfObject4[0] = Integer.valueOf(localSyncHistoryItem1.event);
                        arrayOfObject4[1] = Long.valueOf(localSyncHistoryItem1.upstreamActivity);
                        arrayOfObject4[2] = Long.valueOf(localSyncHistoryItem1.downstreamActivity);
                        paramPrintWriter.printf("        event=%d upstreamActivity=%d downstreamActivity=%d\n", arrayOfObject4);
                    }
                    if ((localSyncHistoryItem1.mesg != null) && (!"success".equals(localSyncHistoryItem1.mesg)))
                    {
                        Object[] arrayOfObject5 = new Object[1];
                        arrayOfObject5[0] = localSyncHistoryItem1.mesg;
                        paramPrintWriter.printf("        mesg=%s\n", arrayOfObject5);
                    }
                    m++;
                    break;
                    str2 = "Unknown";
                    str3 = "Unknown";
                    break label1054;
                    label1394: long l5 = (localLong.longValue() - l4) / 1000L;
                    if (l5 < 60L)
                    {
                        str5 = String.valueOf(l5);
                    }
                    else if (l5 < 3600L)
                    {
                        Object[] arrayOfObject6 = new Object[2];
                        arrayOfObject6[0] = Long.valueOf(l5 / 60L);
                        arrayOfObject6[1] = Long.valueOf(l5 % 60L);
                        str5 = String.format("%02d:%02d", arrayOfObject6);
                    }
                    else
                    {
                        long l6 = l5 % 3600L;
                        Object[] arrayOfObject1 = new Object[3];
                        arrayOfObject1[0] = Long.valueOf(l5 / 3600L);
                        arrayOfObject1[1] = Long.valueOf(l6 / 60L);
                        arrayOfObject1[2] = Long.valueOf(l6 % 60L);
                        str5 = String.format("%02d:%02d:%02d", arrayOfObject1);
                    }
                }
            }
        }
    }

    private void dumpTimeSec(PrintWriter paramPrintWriter, long paramLong)
    {
        paramPrintWriter.print(paramLong / 1000L);
        paramPrintWriter.print('.');
        paramPrintWriter.print(paramLong / 100L % 10L);
        paramPrintWriter.print('s');
    }

    private void ensureAlarmService()
    {
        if (this.mAlarmService == null)
            this.mAlarmService = ((AlarmManager)this.mContext.getSystemService("alarm"));
    }

    static String formatTime(long paramLong)
    {
        Time localTime = new Time();
        localTime.set(paramLong);
        return localTime.format("%Y-%m-%d %H:%M:%S");
    }

    private List<UserInfo> getAllUsers()
    {
        try
        {
            List localList2 = AppGlobals.getPackageManager().getUsers();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    private ConnectivityManager getConnectivityManager()
    {
        try
        {
            if (this.mConnManagerDoNotUseDirectly == null)
                this.mConnManagerDoNotUseDirectly = ((ConnectivityManager)this.mContext.getSystemService("connectivity"));
            ConnectivityManager localConnectivityManager = this.mConnManagerDoNotUseDirectly;
            return localConnectivityManager;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private String getLastFailureMessage(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "unknown";
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        }
        while (true)
        {
            return str;
            str = "sync already in progress";
            continue;
            str = "authentication error";
            continue;
            str = "I/O error";
            continue;
            str = "parse error";
            continue;
            str = "conflict error";
            continue;
            str = "too many deletions error";
            continue;
            str = "too many retries error";
            continue;
            str = "internal error";
        }
    }

    private void increaseBackoffSetting(SyncOperation paramSyncOperation)
    {
        long l1 = SystemClock.elapsedRealtime();
        Pair localPair = this.mSyncStorageEngine.getBackoff(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority);
        long l2 = -1L;
        if (localPair != null)
            if (l1 < ((Long)localPair.first).longValue())
                if (Log.isLoggable("SyncManager", 2))
                    Log.v("SyncManager", "Still in backoff, do not increase it. Remaining: " + (((Long)localPair.first).longValue() - l1) / 1000L + " seconds.");
        while (true)
        {
            return;
            l2 = 2L * ((Long)localPair.second).longValue();
            if (l2 <= 0L)
                l2 = jitterize(30000L, 33000L);
            long l3 = Settings.Secure.getLong(this.mContext.getContentResolver(), "sync_max_retry_delay_in_seconds", 3600L);
            if (l2 > 1000L * l3)
                l2 = l3 * 1000L;
            long l4 = l1 + l2;
            this.mSyncStorageEngine.setBackoff(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority, l4, l2);
            paramSyncOperation.backoff = Long.valueOf(l4);
            paramSyncOperation.updateEffectiveRunTime();
            synchronized (this.mSyncQueue)
            {
                this.mSyncQueue.onBackoffChanged(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority, l4);
            }
        }
    }

    private boolean isSyncStillActive(ActiveSyncContext paramActiveSyncContext)
    {
        Iterator localIterator = this.mActiveSyncContexts.iterator();
        do
            if (!localIterator.hasNext())
                break;
        while ((ActiveSyncContext)localIterator.next() != paramActiveSyncContext);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private long jitterize(long paramLong1, long paramLong2)
    {
        Random localRandom = new Random(SystemClock.elapsedRealtime());
        long l = paramLong2 - paramLong1;
        if (l > 2147483647L)
            throw new IllegalArgumentException("the difference between the maxValue and the minValue must be less than 2147483647");
        return paramLong1 + localRandom.nextInt((int)l);
    }

    private void onUserRemoved(Intent paramIntent)
    {
        int i = paramIntent.getIntExtra("android.intent.extra.user_id", -1);
        if (i == -1);
        while (true)
        {
            return;
            this.mSyncStorageEngine.doDatabaseCleanup(new Account[0], i);
            onAccountsUpdated(null);
            synchronized (this.mSyncQueue)
            {
                this.mSyncQueue.removeUser(i);
            }
        }
    }

    private boolean readDataConnectionState()
    {
        NetworkInfo localNetworkInfo = getConnectivityManager().getActiveNetworkInfo();
        if ((localNetworkInfo != null) && (localNetworkInfo.isConnected()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void sendCancelSyncsMessage(Account paramAccount, int paramInt, String paramString)
    {
        if (Log.isLoggable("SyncManager", 2))
            Log.v("SyncManager", "sending MESSAGE_CANCEL");
        Message localMessage = this.mSyncHandler.obtainMessage();
        localMessage.what = 6;
        localMessage.obj = Pair.create(paramAccount, paramString);
        localMessage.arg1 = paramInt;
        this.mSyncHandler.sendMessage(localMessage);
    }

    private void sendCheckAlarmsMessage()
    {
        if (Log.isLoggable("SyncManager", 2))
            Log.v("SyncManager", "sending MESSAGE_CHECK_ALARMS");
        this.mSyncHandler.removeMessages(3);
        this.mSyncHandler.sendEmptyMessage(3);
    }

    private void sendSyncAlarmMessage()
    {
        if (Log.isLoggable("SyncManager", 2))
            Log.v("SyncManager", "sending MESSAGE_SYNC_ALARM");
        this.mSyncHandler.sendEmptyMessage(2);
    }

    private void sendSyncFinishedOrCanceledMessage(ActiveSyncContext paramActiveSyncContext, SyncResult paramSyncResult)
    {
        if (Log.isLoggable("SyncManager", 2))
            Log.v("SyncManager", "sending MESSAGE_SYNC_FINISHED");
        Message localMessage = this.mSyncHandler.obtainMessage();
        localMessage.what = 1;
        localMessage.obj = new SyncHandlerMessagePayload(paramActiveSyncContext, paramSyncResult);
        this.mSyncHandler.sendMessage(localMessage);
    }

    private void setDelayUntilTime(SyncOperation paramSyncOperation, long paramLong)
    {
        long l1 = paramLong * 1000L;
        long l2 = System.currentTimeMillis();
        if (l1 > l2);
        for (long l3 = SystemClock.elapsedRealtime() + (l1 - l2); ; l3 = 0L)
        {
            this.mSyncStorageEngine.setDelayUntilTime(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority, l3);
            synchronized (this.mSyncQueue)
            {
                this.mSyncQueue.onDelayUntilTimeChanged(paramSyncOperation.account, paramSyncOperation.authority, l3);
                return;
            }
        }
    }

    public void cancelActiveSync(Account paramAccount, int paramInt, String paramString)
    {
        sendCancelSyncsMessage(paramAccount, paramInt, paramString);
    }

    public void clearScheduledSyncOperations(Account paramAccount, int paramInt, String paramString)
    {
        synchronized (this.mSyncQueue)
        {
            this.mSyncQueue.remove(paramAccount, paramInt, paramString);
            this.mSyncStorageEngine.setBackoff(paramAccount, paramInt, paramString, -1L, -1L);
            return;
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter)
    {
        dumpSyncState(paramPrintWriter);
        dumpSyncHistory(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.println("SyncAdapters:");
        Iterator localIterator = this.mSyncAdapters.getAllServices().iterator();
        while (localIterator.hasNext())
        {
            RegisteredServicesCache.ServiceInfo localServiceInfo = (RegisteredServicesCache.ServiceInfo)localIterator.next();
            paramPrintWriter.println("    " + localServiceInfo);
        }
    }

    protected void dumpSyncHistory(PrintWriter paramPrintWriter)
    {
        dumpRecentHistory(paramPrintWriter);
        dumpDayStatistics(paramPrintWriter);
    }

    protected void dumpSyncState(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print("data connected: ");
        paramPrintWriter.println(this.mDataConnectionIsConnected);
        paramPrintWriter.print("auto sync: ");
        List localList = getAllUsers();
        if (localList != null)
        {
            Iterator localIterator3 = localList.iterator();
            while (localIterator3.hasNext())
            {
                UserInfo localUserInfo = (UserInfo)localIterator3.next();
                paramPrintWriter.print("u" + localUserInfo.id + "=" + this.mSyncStorageEngine.getMasterSyncAutomatically(localUserInfo.id));
            }
            paramPrintWriter.println();
        }
        paramPrintWriter.print("memory low: ");
        paramPrintWriter.println(this.mStorageIsLow);
        AccountAndUser[] arrayOfAccountAndUser = this.mAccounts;
        paramPrintWriter.print("accounts: ");
        long l1;
        String str1;
        if (arrayOfAccountAndUser != INITIAL_ACCOUNTS_ARRAY)
        {
            paramPrintWriter.println(arrayOfAccountAndUser.length);
            l1 = SystemClock.elapsedRealtime();
            paramPrintWriter.print("now: ");
            paramPrintWriter.print(l1);
            paramPrintWriter.println(" (" + formatTime(System.currentTimeMillis()) + ")");
            paramPrintWriter.print("offset: ");
            paramPrintWriter.print(DateUtils.formatElapsedTime(this.mSyncRandomOffsetMillis / 1000));
            paramPrintWriter.println(" (HH:MM:SS)");
            paramPrintWriter.print("uptime: ");
            paramPrintWriter.print(DateUtils.formatElapsedTime(l1 / 1000L));
            paramPrintWriter.println(" (HH:MM:SS)");
            paramPrintWriter.print("time spent syncing: ");
            paramPrintWriter.print(DateUtils.formatElapsedTime(this.mSyncHandler.mSyncTimeTracker.timeSpentSyncing() / 1000L));
            paramPrintWriter.print(" (HH:MM:SS), sync ");
            if (!this.mSyncHandler.mSyncTimeTracker.mLastWasSyncing)
                break label568;
            str1 = "";
            label320: paramPrintWriter.print(str1);
            paramPrintWriter.println("in progress");
            if (this.mSyncHandler.mAlarmScheduleTime == null)
                break label576;
            paramPrintWriter.print("next alarm time: ");
            paramPrintWriter.print(this.mSyncHandler.mAlarmScheduleTime);
            paramPrintWriter.print(" (");
            paramPrintWriter.print(DateUtils.formatElapsedTime((this.mSyncHandler.mAlarmScheduleTime.longValue() - l1) / 1000L));
            paramPrintWriter.println(" (HH:MM:SS) from now)");
        }
        StringBuilder localStringBuilder1;
        while (true)
        {
            paramPrintWriter.print("notification info: ");
            localStringBuilder1 = new StringBuilder();
            this.mSyncHandler.mSyncNotificationInfo.toString(localStringBuilder1);
            paramPrintWriter.println(localStringBuilder1.toString());
            paramPrintWriter.println();
            paramPrintWriter.println("Active Syncs: " + this.mActiveSyncContexts.size());
            Iterator localIterator1 = this.mActiveSyncContexts.iterator();
            while (localIterator1.hasNext())
            {
                ActiveSyncContext localActiveSyncContext = (ActiveSyncContext)localIterator1.next();
                long l3 = (l1 - localActiveSyncContext.mStartTime) / 1000L;
                paramPrintWriter.print("    ");
                paramPrintWriter.print(DateUtils.formatElapsedTime(l3));
                paramPrintWriter.print(" - ");
                paramPrintWriter.print(localActiveSyncContext.mSyncOperation.dump(false));
                paramPrintWriter.println();
            }
            paramPrintWriter.println("not known yet");
            break;
            label568: str1 = "not ";
            break label320;
            label576: paramPrintWriter.println("no alarm is scheduled (there had better not be any pending syncs)");
        }
        while (true)
        {
            int j;
            SyncStorageEngine.AuthorityInfo localAuthorityInfo;
            SyncStatusInfo localSyncStatusInfo;
            String str2;
            String str3;
            synchronized (this.mSyncQueue)
            {
                localStringBuilder1.setLength(0);
                this.mSyncQueue.dump(localStringBuilder1);
                paramPrintWriter.println();
                paramPrintWriter.print(localStringBuilder1.toString());
                paramPrintWriter.println();
                paramPrintWriter.println("Sync Status");
                int i = arrayOfAccountAndUser.length;
                j = 0;
                if (j >= i)
                    break;
                AccountAndUser localAccountAndUser = arrayOfAccountAndUser[j];
                paramPrintWriter.print("    Account ");
                paramPrintWriter.print(localAccountAndUser.account.name);
                paramPrintWriter.print(" u");
                paramPrintWriter.print(localAccountAndUser.userId);
                paramPrintWriter.print(" ");
                paramPrintWriter.print(localAccountAndUser.account.type);
                paramPrintWriter.println(":");
                Iterator localIterator2 = this.mSyncAdapters.getAllServices().iterator();
                if (!localIterator2.hasNext())
                    break label1550;
                RegisteredServicesCache.ServiceInfo localServiceInfo = (RegisteredServicesCache.ServiceInfo)localIterator2.next();
                if (!((SyncAdapterType)localServiceInfo.type).accountType.equals(localAccountAndUser.account.type))
                    continue;
                localAuthorityInfo = this.mSyncStorageEngine.getOrCreateAuthority(localAccountAndUser.account, localAccountAndUser.userId, ((SyncAdapterType)localServiceInfo.type).authority);
                localSyncStatusInfo = this.mSyncStorageEngine.getOrCreateSyncStatus(localAuthorityInfo);
                paramPrintWriter.print("        ");
                paramPrintWriter.print(localAuthorityInfo.authority);
                paramPrintWriter.println(":");
                paramPrintWriter.print("            settings:");
                StringBuilder localStringBuilder2 = new StringBuilder().append(" ");
                if (localAuthorityInfo.syncable > 0)
                {
                    str2 = "syncable";
                    paramPrintWriter.print(str2);
                    StringBuilder localStringBuilder3 = new StringBuilder().append(", ");
                    if (!localAuthorityInfo.enabled)
                        break label1244;
                    str3 = "enabled";
                    paramPrintWriter.print(str3);
                    if (localAuthorityInfo.delayUntil > l1)
                        paramPrintWriter.print(", delay for " + (localAuthorityInfo.delayUntil - l1) / 1000L + " sec");
                    if (localAuthorityInfo.backoffTime > l1)
                        paramPrintWriter.print(", backoff for " + (localAuthorityInfo.backoffTime - l1) / 1000L + " sec");
                    if (localAuthorityInfo.backoffDelay > 0L)
                        paramPrintWriter.print(", the backoff increment is " + localAuthorityInfo.backoffDelay / 1000L + " sec");
                    paramPrintWriter.println();
                    int k = 0;
                    int m = localAuthorityInfo.periodicSyncs.size();
                    if (k >= m)
                        break label1252;
                    Pair localPair = (Pair)localAuthorityInfo.periodicSyncs.get(k);
                    long l2 = localSyncStatusInfo.getPeriodicSyncTime(k) + 1000L * ((Long)localPair.second).longValue();
                    paramPrintWriter.println("            periodic period=" + localPair.second + ", extras=" + localPair.first + ", next=" + formatTime(l2));
                    k++;
                }
            }
            if (localAuthorityInfo.syncable == 0)
            {
                str2 = "not syncable";
            }
            else
            {
                str2 = "not initialized";
                continue;
                label1244: str3 = "disabled";
                continue;
                label1252: paramPrintWriter.print("            count: local=");
                paramPrintWriter.print(localSyncStatusInfo.numSourceLocal);
                paramPrintWriter.print(" poll=");
                paramPrintWriter.print(localSyncStatusInfo.numSourcePoll);
                paramPrintWriter.print(" periodic=");
                paramPrintWriter.print(localSyncStatusInfo.numSourcePeriodic);
                paramPrintWriter.print(" server=");
                paramPrintWriter.print(localSyncStatusInfo.numSourceServer);
                paramPrintWriter.print(" user=");
                paramPrintWriter.print(localSyncStatusInfo.numSourceUser);
                paramPrintWriter.print(" total=");
                paramPrintWriter.print(localSyncStatusInfo.numSyncs);
                paramPrintWriter.println();
                paramPrintWriter.print("            total duration: ");
                paramPrintWriter.println(DateUtils.formatElapsedTime(localSyncStatusInfo.totalElapsedTime / 1000L));
                if (localSyncStatusInfo.lastSuccessTime != 0L)
                {
                    paramPrintWriter.print("            SUCCESS: source=");
                    paramPrintWriter.print(SyncStorageEngine.SOURCES[localSyncStatusInfo.lastSuccessSource]);
                    paramPrintWriter.print(" time=");
                    paramPrintWriter.println(formatTime(localSyncStatusInfo.lastSuccessTime));
                }
                if (localSyncStatusInfo.lastFailureTime != 0L)
                {
                    paramPrintWriter.print("            FAILURE: source=");
                    paramPrintWriter.print(SyncStorageEngine.SOURCES[localSyncStatusInfo.lastFailureSource]);
                    paramPrintWriter.print(" initialTime=");
                    paramPrintWriter.print(formatTime(localSyncStatusInfo.initialFailureTime));
                    paramPrintWriter.print(" lastTime=");
                    paramPrintWriter.println(formatTime(localSyncStatusInfo.lastFailureTime));
                    int n = localSyncStatusInfo.getLastFailureMesgAsInt(0);
                    paramPrintWriter.print("            message: ");
                    paramPrintWriter.println(getLastFailureMessage(n) + " (" + n + ")");
                    continue;
                    label1550: j++;
                }
            }
        }
    }

    public SyncAdapterType[] getSyncAdapterTypes()
    {
        Collection localCollection = this.mSyncAdapters.getAllServices();
        SyncAdapterType[] arrayOfSyncAdapterType = new SyncAdapterType[localCollection.size()];
        int i = 0;
        Iterator localIterator = localCollection.iterator();
        while (localIterator.hasNext())
        {
            arrayOfSyncAdapterType[i] = ((SyncAdapterType)((RegisteredServicesCache.ServiceInfo)localIterator.next()).type);
            i++;
        }
        return arrayOfSyncAdapterType;
    }

    public SyncStorageEngine getSyncStorageEngine()
    {
        return this.mSyncStorageEngine;
    }

    void maybeRescheduleSync(SyncResult paramSyncResult, SyncOperation paramSyncOperation)
    {
        boolean bool = Log.isLoggable("SyncManager", 3);
        if (bool)
            Log.d("SyncManager", "encountered error(s) during the sync: " + paramSyncResult + ", " + paramSyncOperation);
        SyncOperation localSyncOperation = new SyncOperation(paramSyncOperation);
        if (localSyncOperation.extras.getBoolean("ignore_backoff", false))
            localSyncOperation.extras.remove("ignore_backoff");
        if (localSyncOperation.extras.getBoolean("do_not_retry", false))
            Log.d("SyncManager", "not retrying sync operation because SYNC_EXTRAS_DO_NOT_RETRY was specified " + localSyncOperation);
        while (true)
        {
            return;
            if ((localSyncOperation.extras.getBoolean("upload", false)) && (!paramSyncResult.syncAlreadyInProgress))
            {
                localSyncOperation.extras.remove("upload");
                Log.d("SyncManager", "retrying sync operation as a two-way sync because an upload-only sync encountered an error: " + localSyncOperation);
                scheduleSyncOperation(localSyncOperation);
            }
            else if (paramSyncResult.tooManyRetries)
            {
                Log.d("SyncManager", "not retrying sync operation because it retried too many times: " + localSyncOperation);
            }
            else if (paramSyncResult.madeSomeProgress())
            {
                if (bool)
                    Log.d("SyncManager", "retrying sync operation because even though it had an error it achieved some success");
                scheduleSyncOperation(localSyncOperation);
            }
            else if (paramSyncResult.syncAlreadyInProgress)
            {
                if (bool)
                    Log.d("SyncManager", "retrying sync operation that failed because there was already a sync in progress: " + localSyncOperation);
                scheduleSyncOperation(new SyncOperation(localSyncOperation.account, localSyncOperation.userId, localSyncOperation.syncSource, localSyncOperation.authority, localSyncOperation.extras, 10000L, localSyncOperation.backoff.longValue(), localSyncOperation.delayUntil, localSyncOperation.allowParallelSyncs));
            }
            else if (paramSyncResult.hasSoftError())
            {
                if (bool)
                    Log.d("SyncManager", "retrying sync operation because it encountered a soft error: " + localSyncOperation);
                scheduleSyncOperation(localSyncOperation);
            }
            else
            {
                Log.d("SyncManager", "not retrying sync operation because the error is a hard error: " + localSyncOperation);
            }
        }
    }

    public void onAccountsUpdated(Account[] paramArrayOfAccount)
    {
        boolean bool;
        List localList;
        if (this.mAccounts == INITIAL_ACCOUNTS_ARRAY)
        {
            bool = true;
            localList = getAllUsers();
            if (localList != null)
                break label27;
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label27: AccountAndUser[] arrayOfAccountAndUser = AccountManagerService.getSingleton().getAllAccounts();
            Iterator localIterator1 = localList.iterator();
            while (localIterator1.hasNext())
            {
                UserInfo localUserInfo = (UserInfo)localIterator1.next();
                if (this.mBootCompleted)
                {
                    Account[] arrayOfAccount = AccountManagerService.getSingleton().getAccounts(localUserInfo.id);
                    this.mSyncStorageEngine.doDatabaseCleanup(arrayOfAccount, localUserInfo.id);
                }
            }
            this.mAccounts = arrayOfAccountAndUser;
            Iterator localIterator2 = this.mActiveSyncContexts.iterator();
            while (localIterator2.hasNext())
            {
                ActiveSyncContext localActiveSyncContext = (ActiveSyncContext)localIterator2.next();
                if (!containsAccountAndUser(arrayOfAccountAndUser, localActiveSyncContext.mSyncOperation.account, localActiveSyncContext.mSyncOperation.userId))
                {
                    Log.d("SyncManager", "canceling sync since the account has been removed");
                    sendSyncFinishedOrCanceledMessage(localActiveSyncContext, null);
                }
            }
            sendCheckAlarmsMessage();
            if (arrayOfAccountAndUser.length > 0)
                scheduleSync(null, -1, null, null, 0L, bool);
        }
    }

    public void scheduleLocalSync(Account paramAccount, int paramInt, String paramString)
    {
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("upload", true);
        scheduleSync(paramAccount, paramInt, paramString, localBundle, LOCAL_SYNC_DELAY, false);
    }

    public void scheduleSync(Account paramAccount, int paramInt, String paramString, Bundle paramBundle, long paramLong, boolean paramBoolean)
    {
        boolean bool1 = Log.isLoggable("SyncManager", 2);
        int i;
        AccountAndUser[] arrayOfAccountAndUser1;
        if ((!this.mBootCompleted) || (getConnectivityManager().getBackgroundDataSetting()))
        {
            i = 1;
            if (paramBundle == null)
                paramBundle = new Bundle();
            if (Boolean.valueOf(paramBundle.getBoolean("expedited", false)).booleanValue())
                paramLong = -1L;
            if ((paramAccount == null) || (paramInt == -1))
                break label225;
            arrayOfAccountAndUser1 = new AccountAndUser[1];
            arrayOfAccountAndUser1[0] = new AccountAndUser(paramAccount, paramInt);
        }
        boolean bool2;
        boolean bool3;
        boolean bool4;
        int j;
        HashSet localHashSet;
        label225: 
        do
        {
            bool2 = paramBundle.getBoolean("upload", false);
            bool3 = paramBundle.getBoolean("force", false);
            if (bool3)
            {
                paramBundle.putBoolean("ignore_backoff", true);
                paramBundle.putBoolean("ignore_settings", true);
            }
            bool4 = paramBundle.getBoolean("ignore_settings", false);
            if (!bool2)
                break label252;
            j = 1;
            localHashSet = new HashSet();
            Iterator localIterator1 = this.mSyncAdapters.getAllServices().iterator();
            while (localIterator1.hasNext())
                localHashSet.add(((SyncAdapterType)((RegisteredServicesCache.ServiceInfo)localIterator1.next()).type).authority);
            i = 0;
            break;
            arrayOfAccountAndUser1 = this.mAccounts;
        }
        while (arrayOfAccountAndUser1.length != 0);
        if (bool1)
            Log.v("SyncManager", "scheduleSync: no accounts configured, dropping");
        label252: Iterator localIterator2;
        do
        {
            return;
            if (bool3)
            {
                j = 3;
                break;
            }
            if (paramString == null)
            {
                j = 2;
                break;
            }
            j = 0;
            break;
            if (paramString != null)
            {
                boolean bool7 = localHashSet.contains(paramString);
                localHashSet.clear();
                if (bool7)
                    localHashSet.add(paramString);
            }
            localIterator2 = localHashSet.iterator();
        }
        while (!localIterator2.hasNext());
        String str = (String)localIterator2.next();
        AccountAndUser[] arrayOfAccountAndUser2 = arrayOfAccountAndUser1;
        int k = arrayOfAccountAndUser2.length;
        int m = 0;
        label349: AccountAndUser localAccountAndUser;
        int n;
        if (m < k)
        {
            localAccountAndUser = arrayOfAccountAndUser2[m];
            n = this.mSyncStorageEngine.getIsSyncable(localAccountAndUser.account, localAccountAndUser.userId, str);
            if (n != 0)
                break label395;
        }
        label395: RegisteredServicesCache.ServiceInfo localServiceInfo;
        boolean bool5;
        do
        {
            do
            {
                m++;
                break label349;
                break;
                localServiceInfo = this.mSyncAdapters.getServiceInfo(SyncAdapterType.newKey(str, localAccountAndUser.account.type));
            }
            while (localServiceInfo == null);
            bool5 = ((SyncAdapterType)localServiceInfo.type).allowParallelSyncs();
            boolean bool6 = ((SyncAdapterType)localServiceInfo.type).isAlwaysSyncable();
            if ((n < 0) && (bool6))
            {
                this.mSyncStorageEngine.setIsSyncable(localAccountAndUser.account, localAccountAndUser.userId, str, 1);
                n = 1;
            }
        }
        while (((paramBoolean) && (n >= 0)) || ((!((SyncAdapterType)localServiceInfo.type).supportsUploading()) && (bool2)));
        if ((n < 0) || (bool4) || ((i != 0) && (this.mSyncStorageEngine.getMasterSyncAutomatically(localAccountAndUser.userId)) && (this.mSyncStorageEngine.getSyncAutomatically(localAccountAndUser.account, localAccountAndUser.userId, str))));
        for (int i1 = 1; ; i1 = 0)
        {
            if (i1 != 0)
                break label628;
            if (!bool1)
                break;
            Log.d("SyncManager", "scheduleSync: sync of " + localAccountAndUser + ", " + str + " is not allowed, dropping request");
            break;
        }
        label628: Pair localPair = this.mSyncStorageEngine.getBackoff(localAccountAndUser.account, localAccountAndUser.userId, str);
        long l1 = this.mSyncStorageEngine.getDelayUntilTime(localAccountAndUser.account, localAccountAndUser.userId, str);
        if (localPair != null);
        for (long l2 = ((Long)localPair.first).longValue(); ; l2 = 0L)
        {
            if (n < 0)
            {
                Bundle localBundle = new Bundle();
                localBundle.putBoolean("initialize", true);
                if (bool1)
                    Log.v("SyncManager", "scheduleSync: delay " + paramLong + ", source " + j + ", account " + localAccountAndUser + ", authority " + str + ", extras " + localBundle);
                scheduleSyncOperation(new SyncOperation(localAccountAndUser.account, localAccountAndUser.userId, j, str, localBundle, 0L, l2, l1, bool5));
            }
            if (paramBoolean)
                break;
            if (bool1)
                Log.v("SyncManager", "scheduleSync: delay " + paramLong + ", source " + j + ", account " + localAccountAndUser + ", authority " + str + ", extras " + paramBundle);
            scheduleSyncOperation(new SyncOperation(localAccountAndUser.account, localAccountAndUser.userId, j, str, paramBundle, paramLong, l2, l1, bool5));
            break;
        }
    }

    public void scheduleSyncOperation(SyncOperation paramSyncOperation)
    {
        while (true)
        {
            synchronized (this.mSyncQueue)
            {
                boolean bool = this.mSyncQueue.add(paramSyncOperation);
                if (bool)
                {
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "scheduleSyncOperation: enqueued " + paramSyncOperation);
                    sendCheckAlarmsMessage();
                    return;
                }
            }
            if (Log.isLoggable("SyncManager", 2))
                Log.v("SyncManager", "scheduleSyncOperation: dropping duplicate sync operation " + paramSyncOperation);
        }
    }

    class SyncHandler extends Handler
    {
        private static final int MESSAGE_CANCEL = 6;
        private static final int MESSAGE_CHECK_ALARMS = 3;
        private static final int MESSAGE_SERVICE_CONNECTED = 4;
        private static final int MESSAGE_SERVICE_DISCONNECTED = 5;
        private static final int MESSAGE_SYNC_ALARM = 2;
        private static final int MESSAGE_SYNC_FINISHED = 1;
        private Long mAlarmScheduleTime = null;
        private volatile CountDownLatch mReadyToRunLatch = new CountDownLatch(1);
        public final SyncNotificationInfo mSyncNotificationInfo = new SyncNotificationInfo();
        public final SyncManager.SyncTimeTracker mSyncTimeTracker = new SyncManager.SyncTimeTracker(SyncManager.this, null);
        private final HashMap<Pair<Account, String>, PowerManager.WakeLock> mWakeLocks = Maps.newHashMap();

        public SyncHandler(Looper arg2)
        {
            super();
        }

        private void cancelActiveSyncLocked(Account paramAccount, int paramInt, String paramString)
        {
            Iterator localIterator = new ArrayList(SyncManager.this.mActiveSyncContexts).iterator();
            while (localIterator.hasNext())
            {
                SyncManager.ActiveSyncContext localActiveSyncContext = (SyncManager.ActiveSyncContext)localIterator.next();
                if ((localActiveSyncContext != null) && ((paramAccount == null) || (paramAccount.equals(localActiveSyncContext.mSyncOperation.account))) && ((paramString == null) || (paramString.equals(localActiveSyncContext.mSyncOperation.authority))) && ((paramInt == -1) || (paramInt == localActiveSyncContext.mSyncOperation.userId)))
                    runSyncFinishedOrCanceledLocked(null, localActiveSyncContext);
            }
        }

        private void closeActiveSyncContext(SyncManager.ActiveSyncContext paramActiveSyncContext)
        {
            paramActiveSyncContext.close();
            SyncManager.this.mActiveSyncContexts.remove(paramActiveSyncContext);
            SyncManager.this.mSyncStorageEngine.removeActiveSync(paramActiveSyncContext.mSyncInfo, paramActiveSyncContext.mSyncOperation.userId);
        }

        private boolean dispatchSyncOperation(SyncOperation paramSyncOperation)
        {
            if (Log.isLoggable("SyncManager", 2))
            {
                Log.v("SyncManager", "dispatchSyncOperation: we are going to sync " + paramSyncOperation);
                Log.v("SyncManager", "num active syncs: " + SyncManager.this.mActiveSyncContexts.size());
                Iterator localIterator = SyncManager.this.mActiveSyncContexts.iterator();
                while (localIterator.hasNext())
                    Log.v("SyncManager", ((SyncManager.ActiveSyncContext)localIterator.next()).toString());
            }
            SyncAdapterType localSyncAdapterType = SyncAdapterType.newKey(paramSyncOperation.authority, paramSyncOperation.account.type);
            RegisteredServicesCache.ServiceInfo localServiceInfo = SyncManager.this.mSyncAdapters.getServiceInfo(localSyncAdapterType);
            boolean bool;
            if (localServiceInfo == null)
            {
                Log.d("SyncManager", "can't find a sync adapter for " + localSyncAdapterType + ", removing settings for it");
                SyncManager.this.mSyncStorageEngine.removeAuthority(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority);
                bool = false;
            }
            while (true)
            {
                return bool;
                SyncManager.ActiveSyncContext localActiveSyncContext = new SyncManager.ActiveSyncContext(SyncManager.this, paramSyncOperation, insertStartSyncEvent(paramSyncOperation), localServiceInfo.uid);
                localActiveSyncContext.mSyncInfo = SyncManager.this.mSyncStorageEngine.addActiveSync(localActiveSyncContext);
                SyncManager.this.mActiveSyncContexts.add(localActiveSyncContext);
                if (Log.isLoggable("SyncManager", 2))
                    Log.v("SyncManager", "dispatchSyncOperation: starting " + localActiveSyncContext);
                if (!localActiveSyncContext.bindToSyncAdapter(localServiceInfo))
                {
                    Log.e("SyncManager", "Bind attempt failed to " + localServiceInfo);
                    closeActiveSyncContext(localActiveSyncContext);
                    bool = false;
                }
                else
                {
                    bool = true;
                }
            }
        }

        private PowerManager.WakeLock getSyncWakeLock(Account paramAccount, String paramString)
        {
            Pair localPair = Pair.create(paramAccount, paramString);
            PowerManager.WakeLock localWakeLock = (PowerManager.WakeLock)this.mWakeLocks.get(localPair);
            if (localWakeLock == null)
            {
                String str = "*sync*_" + paramString + "_" + paramAccount;
                localWakeLock = SyncManager.this.mPowerManager.newWakeLock(1, str);
                localWakeLock.setReferenceCounted(false);
                this.mWakeLocks.put(localPair, localWakeLock);
            }
            return localWakeLock;
        }

        private void installHandleTooManyDeletesNotification(Account paramAccount, String paramString, long paramLong)
        {
            if (SyncManager.this.mNotificationMgr == null);
            while (true)
            {
                return;
                ProviderInfo localProviderInfo = SyncManager.this.mContext.getPackageManager().resolveContentProvider(paramString, 0);
                if (localProviderInfo != null)
                {
                    CharSequence localCharSequence1 = localProviderInfo.loadLabel(SyncManager.this.mContext.getPackageManager());
                    Intent localIntent = new Intent(SyncManager.this.mContext, SyncActivityTooManyDeletes.class);
                    localIntent.putExtra("account", paramAccount);
                    localIntent.putExtra("authority", paramString);
                    localIntent.putExtra("provider", localCharSequence1.toString());
                    localIntent.putExtra("numDeletes", paramLong);
                    if (!isActivityAvailable(localIntent))
                    {
                        Log.w("SyncManager", "No activity found to handle too many deletes.");
                    }
                    else
                    {
                        PendingIntent localPendingIntent = PendingIntent.getActivity(SyncManager.this.mContext, 0, localIntent, 268435456);
                        CharSequence localCharSequence2 = SyncManager.this.mContext.getResources().getText(17039650);
                        Notification localNotification = new Notification(17302806, SyncManager.this.mContext.getString(17039648), System.currentTimeMillis());
                        Context localContext = SyncManager.this.mContext;
                        String str1 = SyncManager.this.mContext.getString(17039649);
                        String str2 = localCharSequence2.toString();
                        Object[] arrayOfObject = new Object[1];
                        arrayOfObject[0] = localCharSequence1;
                        localNotification.setLatestEventInfo(localContext, str1, String.format(str2, arrayOfObject), localPendingIntent);
                        localNotification.flags = (0x2 | localNotification.flags);
                        SyncManager.this.mNotificationMgr.notify(paramAccount.hashCode() ^ paramString.hashCode(), localNotification);
                    }
                }
            }
        }

        private boolean isActivityAvailable(Intent paramIntent)
        {
            boolean bool = false;
            List localList = SyncManager.this.mContext.getPackageManager().queryIntentActivities(paramIntent, 0);
            int i = localList.size();
            for (int j = 0; ; j++)
                if (j < i)
                {
                    if ((0x1 & ((ResolveInfo)localList.get(j)).activityInfo.applicationInfo.flags) != 0)
                        bool = true;
                }
                else
                    return bool;
        }

        private void manageSyncAlarmLocked(long paramLong1, long paramLong2)
        {
            if (!SyncManager.this.mDataConnectionIsConnected);
            label415: label431: label443: 
            while (true)
            {
                return;
                if (!SyncManager.this.mStorageIsLow)
                {
                    if ((!SyncManager.this.mSyncHandler.mSyncNotificationInfo.isActive) && (SyncManager.this.mSyncHandler.mSyncNotificationInfo.startTime != null));
                    long l2;
                    for (long l1 = SyncManager.this.mSyncHandler.mSyncNotificationInfo.startTime.longValue() + SyncManager.SYNC_NOTIFICATION_DELAY; ; l1 = 9223372036854775807L)
                    {
                        l2 = 9223372036854775807L;
                        Iterator localIterator = SyncManager.this.mActiveSyncContexts.iterator();
                        while (localIterator.hasNext())
                        {
                            long l5 = ((SyncManager.ActiveSyncContext)localIterator.next()).mTimeoutStartTime + SyncManager.MAX_TIME_PER_SYNC;
                            if (Log.isLoggable("SyncManager", 2))
                                Log.v("SyncManager", "manageSyncAlarm: active sync, mTimeoutStartTime + MAX is " + l5);
                            if (l2 > l5)
                                l2 = l5;
                        }
                    }
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "manageSyncAlarm: notificationTime is " + l1);
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "manageSyncAlarm: earliestTimeoutTime is " + l2);
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "manageSyncAlarm: nextPeriodicEventElapsedTime is " + paramLong1);
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "manageSyncAlarm: nextPendingEventElapsedTime is " + paramLong2);
                    long l3 = Math.min(Math.min(Math.min(l1, l2), paramLong1), paramLong2);
                    long l4 = SystemClock.elapsedRealtime();
                    int i;
                    int j;
                    int k;
                    int m;
                    if (l3 < 30000L + l4)
                    {
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "manageSyncAlarm: the alarmTime is too small, " + l3 + ", setting to " + (30000L + l4));
                        l3 = l4 + 30000L;
                        i = 0;
                        j = 0;
                        if (this.mAlarmScheduleTime == null)
                            break label658;
                        k = 1;
                        if (l3 == 9223372036854775807L)
                            break label664;
                        m = 1;
                        if (m == 0)
                            break label670;
                        if ((k == 0) || (l3 < this.mAlarmScheduleTime.longValue()))
                            i = 1;
                    }
                    while (true)
                    {
                        SyncManager.this.ensureAlarmService();
                        if (i == 0)
                            break label677;
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "requesting that the alarm manager wake us up at elapsed time " + l3 + ", now is " + l4 + ", " + (l3 - l4) / 1000L + " secs from now");
                        this.mAlarmScheduleTime = Long.valueOf(l3);
                        SyncManager.this.mAlarmService.set(2, l3, SyncManager.this.mSyncAlarmIntent);
                        break;
                        if (l3 <= 7200000L + l4)
                            break label415;
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "manageSyncAlarm: the alarmTime is too large, " + l3 + ", setting to " + (30000L + l4));
                        l3 = l4 + 7200000L;
                        break label415;
                        k = 0;
                        break label431;
                        m = 0;
                        break label443;
                        j = k;
                    }
                    if (j != 0)
                    {
                        this.mAlarmScheduleTime = null;
                        SyncManager.this.mAlarmService.cancel(SyncManager.this.mSyncAlarmIntent);
                    }
                }
            }
        }

        private void manageSyncNotificationLocked()
        {
            boolean bool;
            int j;
            if (SyncManager.this.mActiveSyncContexts.isEmpty())
            {
                this.mSyncNotificationInfo.startTime = null;
                bool = this.mSyncNotificationInfo.isActive;
                j = 0;
            }
            while (true)
            {
                if ((bool) && (j == 0))
                {
                    SyncManager.access$3402(SyncManager.this, false);
                    sendSyncStateIntent();
                    this.mSyncNotificationInfo.isActive = false;
                }
                if (j != 0)
                {
                    SyncManager.access$3402(SyncManager.this, true);
                    sendSyncStateIntent();
                    this.mSyncNotificationInfo.isActive = true;
                }
                return;
                long l = SystemClock.elapsedRealtime();
                if (this.mSyncNotificationInfo.startTime == null)
                    this.mSyncNotificationInfo.startTime = Long.valueOf(l);
                if (this.mSyncNotificationInfo.isActive)
                {
                    bool = false;
                    j = 0;
                }
                else
                {
                    bool = false;
                    if (l > this.mSyncNotificationInfo.startTime.longValue() + SyncManager.SYNC_NOTIFICATION_DELAY);
                    for (int i = 1; ; i = 0)
                    {
                        if (i == 0)
                            break label173;
                        j = 1;
                        break;
                    }
                    label173: j = 0;
                    Iterator localIterator = SyncManager.this.mActiveSyncContexts.iterator();
                    if (localIterator.hasNext())
                    {
                        if (!((SyncManager.ActiveSyncContext)localIterator.next()).mSyncOperation.extras.getBoolean("force", false))
                            break;
                        j = 1;
                    }
                }
            }
        }

        // ERROR //
        private long maybeStartNextSyncLocked()
        {
            // Byte code:
            //     0: ldc 164
            //     2: iconst_2
            //     3: invokestatic 170	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
            //     6: istore_1
            //     7: iload_1
            //     8: ifeq +12 -> 20
            //     11: ldc 164
            //     13: ldc_w 576
            //     16: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     19: pop
            //     20: aload_0
            //     21: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     24: invokestatic 449	android/content/SyncManager:access$400	(Landroid/content/SyncManager;)Z
            //     27: ifne +24 -> 51
            //     30: iload_1
            //     31: ifeq +12 -> 43
            //     34: ldc 164
            //     36: ldc_w 578
            //     39: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     42: pop
            //     43: ldc2_w 472
            //     46: lstore 6
            //     48: lload 6
            //     50: lreturn
            //     51: aload_0
            //     52: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     55: invokestatic 452	android/content/SyncManager:access$000	(Landroid/content/SyncManager;)Z
            //     58: ifeq +24 -> 82
            //     61: iload_1
            //     62: ifeq +12 -> 74
            //     65: ldc 164
            //     67: ldc_w 580
            //     70: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     73: pop
            //     74: ldc2_w 472
            //     77: lstore 6
            //     79: goto -31 -> 48
            //     82: aload_0
            //     83: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     86: invokestatic 584	android/content/SyncManager:access$2200	(Landroid/content/SyncManager;)[Landroid/accounts/AccountAndUser;
            //     89: astore_2
            //     90: aload_2
            //     91: invokestatic 588	android/content/SyncManager:access$2500	()[Landroid/accounts/AccountAndUser;
            //     94: if_acmpne +24 -> 118
            //     97: iload_1
            //     98: ifeq +12 -> 110
            //     101: ldc 164
            //     103: ldc_w 590
            //     106: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     109: pop
            //     110: ldc2_w 472
            //     113: lstore 6
            //     115: goto -67 -> 48
            //     118: aload_0
            //     119: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     122: invokestatic 594	android/content/SyncManager:access$300	(Landroid/content/SyncManager;)Landroid/net/ConnectivityManager;
            //     125: invokevirtual 599	android/net/ConnectivityManager:getBackgroundDataSetting	()Z
            //     128: istore_3
            //     129: invokestatic 504	android/os/SystemClock:elapsedRealtime	()J
            //     132: lstore 4
            //     134: ldc2_w 472
            //     137: lstore 6
            //     139: new 84	java/util/ArrayList
            //     142: dup
            //     143: invokespecial 600	java/util/ArrayList:<init>	()V
            //     146: astore 8
            //     148: aload_0
            //     149: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     152: getfield 604	android/content/SyncManager:mSyncQueue	Landroid/content/SyncQueue;
            //     155: astore 9
            //     157: aload 9
            //     159: monitorenter
            //     160: iload_1
            //     161: ifeq +41 -> 202
            //     164: ldc 164
            //     166: new 172	java/lang/StringBuilder
            //     169: dup
            //     170: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     173: ldc_w 606
            //     176: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     179: aload_0
            //     180: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     183: getfield 604	android/content/SyncManager:mSyncQueue	Landroid/content/SyncQueue;
            //     186: getfield 611	android/content/SyncQueue:mOperationsMap	Ljava/util/HashMap;
            //     189: invokevirtual 612	java/util/HashMap:size	()I
            //     192: invokevirtual 200	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     195: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     198: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     201: pop
            //     202: aload_0
            //     203: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     206: getfield 604	android/content/SyncManager:mSyncQueue	Landroid/content/SyncQueue;
            //     209: getfield 611	android/content/SyncQueue:mOperationsMap	Ljava/util/HashMap;
            //     212: invokevirtual 616	java/util/HashMap:values	()Ljava/util/Collection;
            //     215: invokeinterface 619 1 0
            //     220: astore 11
            //     222: aload 11
            //     224: invokeinterface 101 1 0
            //     229: ifeq +338 -> 567
            //     232: aload 11
            //     234: invokeinterface 105 1 0
            //     239: checkcast 113	android/content/SyncOperation
            //     242: astore 43
            //     244: aload_0
            //     245: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     248: aload_2
            //     249: aload 43
            //     251: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     254: aload 43
            //     256: getfield 133	android/content/SyncOperation:userId	I
            //     259: invokestatic 623	android/content/SyncManager:access$2400	(Landroid/content/SyncManager;[Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z
            //     262: ifne +37 -> 299
            //     265: aload 11
            //     267: invokeinterface 625 1 0
            //     272: aload_0
            //     273: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     276: invokestatic 150	android/content/SyncManager:access$600	(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
            //     279: aload 43
            //     281: getfield 629	android/content/SyncOperation:pendingOperation	Landroid/content/SyncStorageEngine$PendingOperation;
            //     284: invokevirtual 633	android/content/SyncStorageEngine:deleteFromPending	(Landroid/content/SyncStorageEngine$PendingOperation;)Z
            //     287: pop
            //     288: goto -66 -> 222
            //     291: astore 10
            //     293: aload 9
            //     295: monitorexit
            //     296: aload 10
            //     298: athrow
            //     299: aload_0
            //     300: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     303: invokestatic 150	android/content/SyncManager:access$600	(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
            //     306: aload 43
            //     308: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     311: aload 43
            //     313: getfield 133	android/content/SyncOperation:userId	I
            //     316: aload 43
            //     318: getfield 127	android/content/SyncOperation:authority	Ljava/lang/String;
            //     321: invokevirtual 637	android/content/SyncStorageEngine:getIsSyncable	(Landroid/accounts/Account;ILjava/lang/String;)I
            //     324: istore 44
            //     326: iload 44
            //     328: ifne +29 -> 357
            //     331: aload 11
            //     333: invokeinterface 625 1 0
            //     338: aload_0
            //     339: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     342: invokestatic 150	android/content/SyncManager:access$600	(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
            //     345: aload 43
            //     347: getfield 629	android/content/SyncOperation:pendingOperation	Landroid/content/SyncStorageEngine$PendingOperation;
            //     350: invokevirtual 633	android/content/SyncStorageEngine:deleteFromPending	(Landroid/content/SyncStorageEngine$PendingOperation;)Z
            //     353: pop
            //     354: goto -132 -> 222
            //     357: aload 43
            //     359: getfield 640	android/content/SyncOperation:effectiveRunTime	J
            //     362: lload 4
            //     364: lcmp
            //     365: ifle +24 -> 389
            //     368: lload 6
            //     370: aload 43
            //     372: getfield 640	android/content/SyncOperation:effectiveRunTime	J
            //     375: lcmp
            //     376: ifle -154 -> 222
            //     379: aload 43
            //     381: getfield 640	android/content/SyncOperation:effectiveRunTime	J
            //     384: lstore 6
            //     386: goto -164 -> 222
            //     389: aload_0
            //     390: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     393: getfield 214	android/content/SyncManager:mSyncAdapters	Landroid/content/SyncAdaptersCache;
            //     396: aload 43
            //     398: getfield 127	android/content/SyncOperation:authority	Ljava/lang/String;
            //     401: aload 43
            //     403: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     406: getfield 204	android/accounts/Account:type	Ljava/lang/String;
            //     409: invokestatic 210	android/content/SyncAdapterType:newKey	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;
            //     412: invokevirtual 220	android/content/SyncAdaptersCache:getServiceInfo	(Ljava/lang/Object;)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;
            //     415: astore 45
            //     417: aload 45
            //     419: ifnull +1043 -> 1462
            //     422: aload_0
            //     423: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     426: invokestatic 594	android/content/SyncManager:access$300	(Landroid/content/SyncManager;)Landroid/net/ConnectivityManager;
            //     429: aload 45
            //     431: getfield 239	android/content/pm/RegisteredServicesCache$ServiceInfo:uid	I
            //     434: invokevirtual 644	android/net/ConnectivityManager:getActiveNetworkInfoForUid	(I)Landroid/net/NetworkInfo;
            //     437: astore 49
            //     439: aload 49
            //     441: ifnull +1015 -> 1456
            //     444: aload 49
            //     446: invokevirtual 649	android/net/NetworkInfo:isConnected	()Z
            //     449: ifeq +1007 -> 1456
            //     452: iconst_1
            //     453: istore 46
            //     455: aload 43
            //     457: getfield 565	android/content/SyncOperation:extras	Landroid/os/Bundle;
            //     460: ldc_w 651
            //     463: iconst_0
            //     464: invokevirtual 573	android/os/Bundle:getBoolean	(Ljava/lang/String;Z)Z
            //     467: ifne +89 -> 556
            //     470: iload 44
            //     472: ifle +84 -> 556
            //     475: aload_0
            //     476: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     479: invokestatic 150	android/content/SyncManager:access$600	(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
            //     482: aload 43
            //     484: getfield 133	android/content/SyncOperation:userId	I
            //     487: invokevirtual 655	android/content/SyncStorageEngine:getMasterSyncAutomatically	(I)Z
            //     490: ifeq +40 -> 530
            //     493: iload_3
            //     494: ifeq +36 -> 530
            //     497: iload 46
            //     499: ifeq +31 -> 530
            //     502: aload_0
            //     503: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     506: invokestatic 150	android/content/SyncManager:access$600	(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
            //     509: aload 43
            //     511: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     514: aload 43
            //     516: getfield 133	android/content/SyncOperation:userId	I
            //     519: aload 43
            //     521: getfield 127	android/content/SyncOperation:authority	Ljava/lang/String;
            //     524: invokevirtual 659	android/content/SyncStorageEngine:getSyncAutomatically	(Landroid/accounts/Account;ILjava/lang/String;)Z
            //     527: ifne +29 -> 556
            //     530: aload 11
            //     532: invokeinterface 625 1 0
            //     537: aload_0
            //     538: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     541: invokestatic 150	android/content/SyncManager:access$600	(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
            //     544: aload 43
            //     546: getfield 629	android/content/SyncOperation:pendingOperation	Landroid/content/SyncStorageEngine$PendingOperation;
            //     549: invokevirtual 633	android/content/SyncStorageEngine:deleteFromPending	(Landroid/content/SyncStorageEngine$PendingOperation;)Z
            //     552: pop
            //     553: goto -331 -> 222
            //     556: aload 8
            //     558: aload 43
            //     560: invokevirtual 249	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     563: pop
            //     564: goto -342 -> 222
            //     567: aload 9
            //     569: monitorexit
            //     570: iload_1
            //     571: ifeq +33 -> 604
            //     574: ldc 164
            //     576: new 172	java/lang/StringBuilder
            //     579: dup
            //     580: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     583: ldc_w 661
            //     586: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     589: aload 8
            //     591: invokevirtual 197	java/util/ArrayList:size	()I
            //     594: invokevirtual 200	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     597: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     600: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     603: pop
            //     604: aload 8
            //     606: invokestatic 667	java/util/Collections:sort	(Ljava/util/List;)V
            //     609: iload_1
            //     610: ifeq +12 -> 622
            //     613: ldc 164
            //     615: ldc_w 669
            //     618: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     621: pop
            //     622: iconst_0
            //     623: istore 12
            //     625: aload 8
            //     627: invokevirtual 197	java/util/ArrayList:size	()I
            //     630: istore 13
            //     632: iload 12
            //     634: iload 13
            //     636: if_icmpge -588 -> 48
            //     639: aload 8
            //     641: iload 12
            //     643: invokevirtual 670	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     646: checkcast 113	android/content/SyncOperation
            //     649: astore 14
            //     651: aload 14
            //     653: invokevirtual 673	android/content/SyncOperation:isInitialization	()Z
            //     656: istore 15
            //     658: iconst_0
            //     659: istore 16
            //     661: iconst_0
            //     662: istore 17
            //     664: aconst_null
            //     665: astore 18
            //     667: aconst_null
            //     668: astore 19
            //     670: aconst_null
            //     671: astore 20
            //     673: aconst_null
            //     674: astore 21
            //     676: aload_0
            //     677: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     680: getfield 88	android/content/SyncManager:mActiveSyncContexts	Ljava/util/ArrayList;
            //     683: invokevirtual 95	java/util/ArrayList:iterator	()Ljava/util/Iterator;
            //     686: astore 22
            //     688: aload 22
            //     690: invokeinterface 101 1 0
            //     695: ifeq +190 -> 885
            //     698: aload 22
            //     700: invokeinterface 105 1 0
            //     705: checkcast 107	android/content/SyncManager$ActiveSyncContext
            //     708: astore 39
            //     710: aload 39
            //     712: getfield 111	android/content/SyncManager$ActiveSyncContext:mSyncOperation	Landroid/content/SyncOperation;
            //     715: astore 40
            //     717: aload 40
            //     719: invokevirtual 673	android/content/SyncOperation:isInitialization	()Z
            //     722: ifeq +94 -> 816
            //     725: iinc 16 1
            //     728: aload 40
            //     730: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     733: getfield 204	android/accounts/Account:type	Ljava/lang/String;
            //     736: aload 14
            //     738: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     741: getfield 204	android/accounts/Account:type	Ljava/lang/String;
            //     744: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     747: ifeq +106 -> 853
            //     750: aload 40
            //     752: getfield 127	android/content/SyncOperation:authority	Ljava/lang/String;
            //     755: aload 14
            //     757: getfield 127	android/content/SyncOperation:authority	Ljava/lang/String;
            //     760: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     763: ifeq +90 -> 853
            //     766: aload 40
            //     768: getfield 133	android/content/SyncOperation:userId	I
            //     771: aload 14
            //     773: getfield 133	android/content/SyncOperation:userId	I
            //     776: if_icmpne +77 -> 853
            //     779: aload 40
            //     781: getfield 676	android/content/SyncOperation:allowParallelSyncs	Z
            //     784: ifeq +25 -> 809
            //     787: aload 40
            //     789: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     792: getfield 679	android/accounts/Account:name	Ljava/lang/String;
            //     795: aload 14
            //     797: getfield 117	android/content/SyncOperation:account	Landroid/accounts/Account;
            //     800: getfield 679	android/accounts/Account:name	Ljava/lang/String;
            //     803: invokevirtual 130	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     806: ifeq +47 -> 853
            //     809: aload 39
            //     811: astore 18
            //     813: goto -125 -> 688
            //     816: iinc 17 1
            //     819: aload 40
            //     821: invokevirtual 682	android/content/SyncOperation:isExpedited	()Z
            //     824: ifne -96 -> 728
            //     827: aload 21
            //     829: ifnull +17 -> 846
            //     832: aload 21
            //     834: getfield 685	android/content/SyncManager$ActiveSyncContext:mStartTime	J
            //     837: aload 39
            //     839: getfield 685	android/content/SyncManager$ActiveSyncContext:mStartTime	J
            //     842: lcmp
            //     843: ifle -115 -> 728
            //     846: aload 39
            //     848: astore 21
            //     850: goto -122 -> 728
            //     853: iload 15
            //     855: aload 40
            //     857: invokevirtual 673	android/content/SyncOperation:isInitialization	()Z
            //     860: if_icmpne -172 -> 688
            //     863: aload 39
            //     865: getfield 685	android/content/SyncManager$ActiveSyncContext:mStartTime	J
            //     868: invokestatic 480	android/content/SyncManager:access$2600	()J
            //     871: ladd
            //     872: lload 4
            //     874: lcmp
            //     875: ifge -187 -> 688
            //     878: aload 39
            //     880: astore 19
            //     882: goto -194 -> 688
            //     885: iload_1
            //     886: ifeq +173 -> 1059
            //     889: ldc 164
            //     891: new 172	java/lang/StringBuilder
            //     894: dup
            //     895: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     898: ldc_w 687
            //     901: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     904: iload 12
            //     906: iconst_1
            //     907: iadd
            //     908: invokevirtual 200	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     911: ldc_w 689
            //     914: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     917: iload 13
            //     919: invokevirtual 200	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     922: ldc_w 691
            //     925: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     928: aload 14
            //     930: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     933: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     936: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     939: pop
            //     940: ldc 164
            //     942: new 172	java/lang/StringBuilder
            //     945: dup
            //     946: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     949: ldc_w 693
            //     952: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     955: iload 16
            //     957: invokevirtual 200	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     960: ldc_w 695
            //     963: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     966: iload 17
            //     968: invokevirtual 200	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     971: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     974: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     977: pop
            //     978: ldc 164
            //     980: new 172	java/lang/StringBuilder
            //     983: dup
            //     984: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     987: ldc_w 697
            //     990: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     993: aload 19
            //     995: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     998: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1001: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1004: pop
            //     1005: ldc 164
            //     1007: new 172	java/lang/StringBuilder
            //     1010: dup
            //     1011: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     1014: ldc_w 699
            //     1017: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1020: aload 18
            //     1022: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1025: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1028: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1031: pop
            //     1032: ldc 164
            //     1034: new 172	java/lang/StringBuilder
            //     1037: dup
            //     1038: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     1041: ldc_w 701
            //     1044: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1047: aload 21
            //     1049: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1052: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1055: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1058: pop
            //     1059: iload 15
            //     1061: ifeq +161 -> 1222
            //     1064: invokestatic 704	android/content/SyncManager:access$2700	()I
            //     1067: istore 33
            //     1069: iload 16
            //     1071: iload 33
            //     1073: if_icmpge +143 -> 1216
            //     1076: iconst_1
            //     1077: istore 24
            //     1079: aload 18
            //     1081: ifnull +240 -> 1321
            //     1084: iload 15
            //     1086: ifeq +160 -> 1246
            //     1089: aload 18
            //     1091: getfield 111	android/content/SyncManager$ActiveSyncContext:mSyncOperation	Landroid/content/SyncOperation;
            //     1094: invokevirtual 673	android/content/SyncOperation:isInitialization	()Z
            //     1097: ifne +149 -> 1246
            //     1100: invokestatic 704	android/content/SyncManager:access$2700	()I
            //     1103: istore 31
            //     1105: iload 16
            //     1107: iload 31
            //     1109: if_icmpge +137 -> 1246
            //     1112: aload 18
            //     1114: astore 20
            //     1116: ldc 164
            //     1118: iconst_2
            //     1119: invokestatic 170	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
            //     1122: ifeq +30 -> 1152
            //     1125: ldc 164
            //     1127: new 172	java/lang/StringBuilder
            //     1130: dup
            //     1131: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     1134: ldc_w 706
            //     1137: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1140: aload 18
            //     1142: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1145: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1148: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1151: pop
            //     1152: aload 20
            //     1154: ifnull +22 -> 1176
            //     1157: aload_0
            //     1158: aconst_null
            //     1159: aload 20
            //     1161: invokespecial 137	android/content/SyncManager$SyncHandler:runSyncFinishedOrCanceledLocked	(Landroid/content/SyncResult;Landroid/content/SyncManager$ActiveSyncContext;)V
            //     1164: aload_0
            //     1165: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     1168: aload 20
            //     1170: getfield 111	android/content/SyncManager$ActiveSyncContext:mSyncOperation	Landroid/content/SyncOperation;
            //     1173: invokevirtual 710	android/content/SyncManager:scheduleSyncOperation	(Landroid/content/SyncOperation;)V
            //     1176: aload_0
            //     1177: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     1180: getfield 604	android/content/SyncManager:mSyncQueue	Landroid/content/SyncQueue;
            //     1183: astore 26
            //     1185: aload 26
            //     1187: monitorenter
            //     1188: aload_0
            //     1189: getfield 40	android/content/SyncManager$SyncHandler:this$0	Landroid/content/SyncManager;
            //     1192: getfield 604	android/content/SyncManager:mSyncQueue	Landroid/content/SyncQueue;
            //     1195: aload 14
            //     1197: invokevirtual 712	android/content/SyncQueue:remove	(Landroid/content/SyncOperation;)V
            //     1200: aload 26
            //     1202: monitorexit
            //     1203: aload_0
            //     1204: aload 14
            //     1206: invokespecial 714	android/content/SyncManager$SyncHandler:dispatchSyncOperation	(Landroid/content/SyncOperation;)Z
            //     1209: pop
            //     1210: iinc 12 1
            //     1213: goto -581 -> 632
            //     1216: iconst_0
            //     1217: istore 24
            //     1219: goto -140 -> 1079
            //     1222: invokestatic 717	android/content/SyncManager:access$2800	()I
            //     1225: istore 23
            //     1227: iload 17
            //     1229: iload 23
            //     1231: if_icmpge +9 -> 1240
            //     1234: iconst_1
            //     1235: istore 24
            //     1237: goto -158 -> 1079
            //     1240: iconst_0
            //     1241: istore 24
            //     1243: goto -164 -> 1079
            //     1246: aload 14
            //     1248: getfield 720	android/content/SyncOperation:expedited	Z
            //     1251: ifeq -41 -> 1210
            //     1254: aload 18
            //     1256: getfield 111	android/content/SyncManager$ActiveSyncContext:mSyncOperation	Landroid/content/SyncOperation;
            //     1259: getfield 720	android/content/SyncOperation:expedited	Z
            //     1262: ifne -52 -> 1210
            //     1265: iload 15
            //     1267: aload 18
            //     1269: getfield 111	android/content/SyncManager$ActiveSyncContext:mSyncOperation	Landroid/content/SyncOperation;
            //     1272: invokevirtual 673	android/content/SyncOperation:isInitialization	()Z
            //     1275: if_icmpne -65 -> 1210
            //     1278: aload 18
            //     1280: astore 20
            //     1282: ldc 164
            //     1284: iconst_2
            //     1285: invokestatic 170	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
            //     1288: ifeq -136 -> 1152
            //     1291: ldc 164
            //     1293: new 172	java/lang/StringBuilder
            //     1296: dup
            //     1297: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     1300: ldc_w 722
            //     1303: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1306: aload 18
            //     1308: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1311: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1314: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1317: pop
            //     1318: goto -166 -> 1152
            //     1321: iload 24
            //     1323: ifne -171 -> 1152
            //     1326: aload 14
            //     1328: invokevirtual 682	android/content/SyncOperation:isExpedited	()Z
            //     1331: ifeq +56 -> 1387
            //     1334: aload 21
            //     1336: ifnull +51 -> 1387
            //     1339: iload 15
            //     1341: ifne +46 -> 1387
            //     1344: aload 21
            //     1346: astore 20
            //     1348: ldc 164
            //     1350: iconst_2
            //     1351: invokestatic 170	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
            //     1354: ifeq -202 -> 1152
            //     1357: ldc 164
            //     1359: new 172	java/lang/StringBuilder
            //     1362: dup
            //     1363: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     1366: ldc_w 724
            //     1369: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1372: aload 21
            //     1374: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1377: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1380: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1383: pop
            //     1384: goto -232 -> 1152
            //     1387: aload 19
            //     1389: ifnull -179 -> 1210
            //     1392: iload 15
            //     1394: aload 19
            //     1396: getfield 111	android/content/SyncManager$ActiveSyncContext:mSyncOperation	Landroid/content/SyncOperation;
            //     1399: invokevirtual 673	android/content/SyncOperation:isInitialization	()Z
            //     1402: if_icmpne -192 -> 1210
            //     1405: aload 19
            //     1407: astore 20
            //     1409: ldc 164
            //     1411: iconst_2
            //     1412: invokestatic 170	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
            //     1415: ifeq -263 -> 1152
            //     1418: ldc 164
            //     1420: new 172	java/lang/StringBuilder
            //     1423: dup
            //     1424: invokespecial 174	java/lang/StringBuilder:<init>	()V
            //     1427: ldc_w 726
            //     1430: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1433: aload 19
            //     1435: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1438: invokevirtual 187	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1441: invokestatic 191	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     1444: pop
            //     1445: goto -293 -> 1152
            //     1448: astore 27
            //     1450: aload 26
            //     1452: monitorexit
            //     1453: aload 27
            //     1455: athrow
            //     1456: iconst_0
            //     1457: istore 46
            //     1459: goto -1004 -> 455
            //     1462: iconst_0
            //     1463: istore 46
            //     1465: goto -1010 -> 455
            //
            // Exception table:
            //     from	to	target	type
            //     164	296	291	finally
            //     299	570	291	finally
            //     1188	1203	1448	finally
            //     1450	1453	1448	finally
        }

        private void runBoundToSyncAdapter(SyncManager.ActiveSyncContext paramActiveSyncContext, ISyncAdapter paramISyncAdapter)
        {
            paramActiveSyncContext.mSyncAdapter = paramISyncAdapter;
            SyncOperation localSyncOperation = paramActiveSyncContext.mSyncOperation;
            try
            {
                paramActiveSyncContext.mIsLinkedToDeath = true;
                paramISyncAdapter.asBinder().linkToDeath(paramActiveSyncContext, 0);
                paramISyncAdapter.startSync(paramActiveSyncContext, localSyncOperation.authority, localSyncOperation.account, localSyncOperation.extras);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Log.d("SyncManager", "maybeStartNextSync: caught a RemoteException, rescheduling", localRemoteException);
                    closeActiveSyncContext(paramActiveSyncContext);
                    SyncManager.this.increaseBackoffSetting(localSyncOperation);
                    SyncManager.this.scheduleSyncOperation(new SyncOperation(localSyncOperation));
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                {
                    closeActiveSyncContext(paramActiveSyncContext);
                    Log.e("SyncManager", "Caught RuntimeException while starting the sync " + localSyncOperation, localRuntimeException);
                }
            }
        }

        private void runSyncFinishedOrCanceledLocked(SyncResult paramSyncResult, SyncManager.ActiveSyncContext paramActiveSyncContext)
        {
            boolean bool = Log.isLoggable("SyncManager", 2);
            if (paramActiveSyncContext.mIsLinkedToDeath)
            {
                paramActiveSyncContext.mSyncAdapter.asBinder().unlinkToDeath(paramActiveSyncContext, 0);
                paramActiveSyncContext.mIsLinkedToDeath = false;
            }
            closeActiveSyncContext(paramActiveSyncContext);
            SyncOperation localSyncOperation = paramActiveSyncContext.mSyncOperation;
            long l = SystemClock.elapsedRealtime() - paramActiveSyncContext.mStartTime;
            String str;
            if (paramSyncResult != null)
            {
                if (bool)
                    Log.v("SyncManager", "runSyncFinishedOrCanceled [finished]: " + localSyncOperation + ", result " + paramSyncResult);
                if (!paramSyncResult.hasError())
                {
                    str = "success";
                    SyncManager.this.clearBackoffSetting(localSyncOperation);
                    SyncManager.this.setDelayUntilTime(localSyncOperation, paramSyncResult.delayUntil);
                }
            }
            while (true)
            {
                stopSyncEvent(paramActiveSyncContext.mHistoryRowId, localSyncOperation, str, 0, 0, l);
                if ((paramSyncResult != null) && (paramSyncResult.tooManyDeletions))
                {
                    installHandleTooManyDeletesNotification(localSyncOperation.account, localSyncOperation.authority, paramSyncResult.stats.numDeletes);
                    if ((paramSyncResult != null) && (paramSyncResult.fullSyncRequested))
                        SyncManager.this.scheduleSyncOperation(new SyncOperation(localSyncOperation.account, localSyncOperation.userId, localSyncOperation.syncSource, localSyncOperation.authority, new Bundle(), 0L, localSyncOperation.backoff.longValue(), localSyncOperation.delayUntil, localSyncOperation.allowParallelSyncs));
                    return;
                    Log.d("SyncManager", "failed sync operation " + localSyncOperation + ", " + paramSyncResult);
                    if (!paramSyncResult.syncAlreadyInProgress)
                        SyncManager.this.increaseBackoffSetting(localSyncOperation);
                    SyncManager.this.maybeRescheduleSync(paramSyncResult, localSyncOperation);
                    str = Integer.toString(syncResultToErrorNumber(paramSyncResult));
                    break;
                    if (bool)
                        Log.v("SyncManager", "runSyncFinishedOrCanceled [canceled]: " + localSyncOperation);
                    if (paramActiveSyncContext.mSyncAdapter == null);
                }
                try
                {
                    paramActiveSyncContext.mSyncAdapter.cancelSync(paramActiveSyncContext);
                    label380: str = "canceled";
                    continue;
                    SyncManager.this.mNotificationMgr.cancel(localSyncOperation.account.hashCode() ^ localSyncOperation.authority.hashCode());
                }
                catch (RemoteException localRemoteException)
                {
                    break label380;
                }
            }
        }

        private long scheduleReadyPeriodicSyncs()
        {
            boolean bool = SyncManager.this.getConnectivityManager().getBackgroundDataSetting();
            long l1 = 9223372036854775807L;
            if (!bool);
            long l2;
            for (long l6 = l1; ; l6 = 9223372036854775807L)
            {
                return l6;
                AccountAndUser[] arrayOfAccountAndUser = SyncManager.this.mAccounts;
                l2 = System.currentTimeMillis();
                if (0L < l2 - SyncManager.this.mSyncRandomOffsetMillis);
                SyncStorageEngine.AuthorityInfo localAuthorityInfo;
                SyncStatusInfo localSyncStatusInfo;
                int i;
                Bundle localBundle;
                long l8;
                Pair localPair;
                RegisteredServicesCache.ServiceInfo localServiceInfo;
                for (long l3 = l2 - SyncManager.this.mSyncRandomOffsetMillis; ; l3 = 0L)
                {
                    Iterator localIterator = SyncManager.this.mSyncStorageEngine.getAuthorities().iterator();
                    while (true)
                    {
                        if (!localIterator.hasNext())
                            break label557;
                        localAuthorityInfo = (SyncStorageEngine.AuthorityInfo)localIterator.next();
                        if ((SyncManager.this.containsAccountAndUser(arrayOfAccountAndUser, localAuthorityInfo.account, localAuthorityInfo.userId)) && (SyncManager.this.mSyncStorageEngine.getMasterSyncAutomatically(localAuthorityInfo.userId)) && (SyncManager.this.mSyncStorageEngine.getSyncAutomatically(localAuthorityInfo.account, localAuthorityInfo.userId, localAuthorityInfo.authority)) && (SyncManager.this.mSyncStorageEngine.getIsSyncable(localAuthorityInfo.account, localAuthorityInfo.userId, localAuthorityInfo.authority) != 0))
                        {
                            localSyncStatusInfo = SyncManager.this.mSyncStorageEngine.getOrCreateSyncStatus(localAuthorityInfo);
                            i = 0;
                            int j = localAuthorityInfo.periodicSyncs.size();
                            while (i < j)
                            {
                                localBundle = (Bundle)((Pair)localAuthorityInfo.periodicSyncs.get(i)).first;
                                Long localLong = Long.valueOf(1000L * ((Long)((Pair)localAuthorityInfo.periodicSyncs.get(i)).second).longValue());
                                long l7 = localSyncStatusInfo.getPeriodicSyncTime(i);
                                l8 = localLong.longValue() - l3 % localLong.longValue();
                                if ((l8 != localLong.longValue()) && (l7 <= l2) && (l2 - l7 < localLong.longValue()))
                                    break label531;
                                localPair = SyncManager.this.mSyncStorageEngine.getBackoff(localAuthorityInfo.account, localAuthorityInfo.userId, localAuthorityInfo.authority);
                                localServiceInfo = SyncManager.this.mSyncAdapters.getServiceInfo(SyncAdapterType.newKey(localAuthorityInfo.authority, localAuthorityInfo.account.type));
                                if (localServiceInfo != null)
                                    break label417;
                                i++;
                            }
                        }
                    }
                }
                label417: SyncManager localSyncManager = SyncManager.this;
                Account localAccount = localAuthorityInfo.account;
                int k = localAuthorityInfo.userId;
                String str = localAuthorityInfo.authority;
                if (localPair != null);
                for (long l9 = ((Long)localPair.first).longValue(); ; l9 = 0L)
                {
                    localSyncManager.scheduleSyncOperation(new SyncOperation(localAccount, k, 4, str, localBundle, 0L, l9, SyncManager.this.mSyncStorageEngine.getDelayUntilTime(localAuthorityInfo.account, localAuthorityInfo.userId, localAuthorityInfo.authority), ((SyncAdapterType)localServiceInfo.type).allowParallelSyncs()));
                    localSyncStatusInfo.setPeriodicSyncTime(i, l2);
                    label531: long l10 = l2 + l8;
                    if (l10 >= l1)
                        break;
                    l1 = l10;
                    break;
                }
                label557: if (l1 != 9223372036854775807L)
                    break;
            }
            long l4 = SystemClock.elapsedRealtime();
            if (l1 < l2);
            for (long l5 = 0L; ; l5 = l1 - l2)
            {
                l6 = l5 + l4;
                break;
            }
        }

        private void sendSyncStateIntent()
        {
            Intent localIntent = new Intent("android.intent.action.SYNC_STATE_CHANGED");
            localIntent.addFlags(134217728);
            localIntent.putExtra("active", SyncManager.this.mNeedSyncActiveNotification);
            localIntent.putExtra("failing", false);
            SyncManager.this.mContext.sendBroadcast(localIntent);
        }

        private int syncResultToErrorNumber(SyncResult paramSyncResult)
        {
            int i;
            if (paramSyncResult.syncAlreadyInProgress)
                i = 1;
            while (true)
            {
                return i;
                if (paramSyncResult.stats.numAuthExceptions > 0L)
                {
                    i = 2;
                }
                else if (paramSyncResult.stats.numIoExceptions > 0L)
                {
                    i = 3;
                }
                else if (paramSyncResult.stats.numParseExceptions > 0L)
                {
                    i = 4;
                }
                else if (paramSyncResult.stats.numConflictDetectedExceptions > 0L)
                {
                    i = 5;
                }
                else if (paramSyncResult.tooManyDeletions)
                {
                    i = 6;
                }
                else if (paramSyncResult.tooManyRetries)
                {
                    i = 7;
                }
                else
                {
                    if (!paramSyncResult.databaseError)
                        break;
                    i = 8;
                }
            }
            throw new IllegalStateException("we are not in an error state, " + paramSyncResult);
        }

        private void waitUntilReadyToRun()
        {
            CountDownLatch localCountDownLatch = this.mReadyToRunLatch;
            if (localCountDownLatch != null);
            while (true)
                try
                {
                    localCountDownLatch.await();
                    this.mReadyToRunLatch = null;
                    return;
                }
                catch (InterruptedException localInterruptedException)
                {
                    Thread.currentThread().interrupt();
                }
        }

        public void handleMessage(Message paramMessage)
        {
            long l1 = 9223372036854775807L;
            long l2 = 9223372036854775807L;
            while (true)
            {
                SyncManager.SyncHandlerMessagePayload localSyncHandlerMessagePayload;
                try
                {
                    waitUntilReadyToRun();
                    SyncManager.access$402(SyncManager.this, SyncManager.this.readDataConnectionState());
                    SyncManager.this.mSyncManagerWakeLock.acquire();
                    l1 = scheduleReadyPeriodicSyncs();
                    int i = paramMessage.what;
                    switch (i)
                    {
                    default:
                        return;
                    case 6:
                        Pair localPair = (Pair)paramMessage.obj;
                        if (Log.isLoggable("SyncManager", 2))
                            Log.d("SyncManager", "handleSyncHandlerMessage: MESSAGE_SERVICE_CANCEL: " + localPair.first + ", " + (String)localPair.second);
                        cancelActiveSyncLocked((Account)localPair.first, paramMessage.arg1, (String)localPair.second);
                        l2 = maybeStartNextSyncLocked();
                        break;
                    case 1:
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "handleSyncHandlerMessage: MESSAGE_SYNC_FINISHED");
                        localSyncHandlerMessagePayload = (SyncManager.SyncHandlerMessagePayload)paramMessage.obj;
                        if (!SyncManager.this.isSyncStillActive(localSyncHandlerMessagePayload.activeSyncContext))
                        {
                            Log.d("SyncManager", "handleSyncHandlerMessage: dropping since the sync is no longer active: " + localSyncHandlerMessagePayload.activeSyncContext);
                            continue;
                        }
                        break;
                    case 4:
                    case 5:
                    case 2:
                    case 3:
                    }
                }
                finally
                {
                    manageSyncNotificationLocked();
                    manageSyncAlarmLocked(l1, l2);
                    this.mSyncTimeTracker.update();
                    SyncManager.this.mSyncManagerWakeLock.release();
                }
                runSyncFinishedOrCanceledLocked(localSyncHandlerMessagePayload.syncResult, localSyncHandlerMessagePayload.activeSyncContext);
                l2 = maybeStartNextSyncLocked();
                continue;
                SyncManager.ServiceConnectionData localServiceConnectionData = (SyncManager.ServiceConnectionData)paramMessage.obj;
                if (Log.isLoggable("SyncManager", 2))
                    Log.d("SyncManager", "handleSyncHandlerMessage: MESSAGE_SERVICE_CONNECTED: " + localServiceConnectionData.activeSyncContext);
                if (!SyncManager.this.isSyncStillActive(localServiceConnectionData.activeSyncContext))
                    continue;
                runBoundToSyncAdapter(localServiceConnectionData.activeSyncContext, localServiceConnectionData.syncAdapter);
                continue;
                SyncManager.ActiveSyncContext localActiveSyncContext = ((SyncManager.ServiceConnectionData)paramMessage.obj).activeSyncContext;
                if (Log.isLoggable("SyncManager", 2))
                    Log.d("SyncManager", "handleSyncHandlerMessage: MESSAGE_SERVICE_DISCONNECTED: " + localActiveSyncContext);
                if (!SyncManager.this.isSyncStillActive(localActiveSyncContext))
                    continue;
                ISyncAdapter localISyncAdapter = localActiveSyncContext.mSyncAdapter;
                if (localISyncAdapter != null);
                try
                {
                    localActiveSyncContext.mSyncAdapter.cancelSync(localActiveSyncContext);
                    label510: SyncResult localSyncResult = new SyncResult();
                    SyncStats localSyncStats = localSyncResult.stats;
                    localSyncStats.numIoExceptions = (1L + localSyncStats.numIoExceptions);
                    runSyncFinishedOrCanceledLocked(localSyncResult, localActiveSyncContext);
                    l2 = maybeStartNextSyncLocked();
                    continue;
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "handleSyncHandlerMessage: MESSAGE_SYNC_ALARM");
                    this.mAlarmScheduleTime = null;
                    try
                    {
                        long l4 = maybeStartNextSyncLocked();
                        l2 = l4;
                        SyncManager.this.mHandleAlarmWakeLock.release();
                    }
                    finally
                    {
                        SyncManager.this.mHandleAlarmWakeLock.release();
                    }
                    Log.v("SyncManager", "handleSyncHandlerMessage: MESSAGE_CHECK_ALARMS");
                    long l3 = maybeStartNextSyncLocked();
                    l2 = l3;
                }
                catch (RemoteException localRemoteException)
                {
                    break label510;
                }
            }
        }

        public long insertStartSyncEvent(SyncOperation paramSyncOperation)
        {
            int i = paramSyncOperation.syncSource;
            long l = System.currentTimeMillis();
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = paramSyncOperation.authority;
            arrayOfObject[1] = Integer.valueOf(0);
            arrayOfObject[2] = Integer.valueOf(i);
            arrayOfObject[3] = Integer.valueOf(paramSyncOperation.account.name.hashCode());
            EventLog.writeEvent(2720, arrayOfObject);
            return SyncManager.this.mSyncStorageEngine.insertStartSyncEvent(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.authority, l, i, paramSyncOperation.isInitialization());
        }

        public void onBootCompleted()
        {
            SyncManager.access$1702(SyncManager.this, true);
            List localList = SyncManager.this.getAllUsers();
            if (localList != null)
            {
                Iterator localIterator = localList.iterator();
                while (localIterator.hasNext())
                {
                    UserInfo localUserInfo = (UserInfo)localIterator.next();
                    SyncManager.this.mSyncStorageEngine.doDatabaseCleanup(AccountManagerService.getSingleton().getAccounts(localUserInfo.id), localUserInfo.id);
                }
            }
            if (this.mReadyToRunLatch != null)
                this.mReadyToRunLatch.countDown();
        }

        public void stopSyncEvent(long paramLong1, SyncOperation paramSyncOperation, String paramString, int paramInt1, int paramInt2, long paramLong2)
        {
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = paramSyncOperation.authority;
            arrayOfObject[1] = Integer.valueOf(1);
            arrayOfObject[2] = Integer.valueOf(paramSyncOperation.syncSource);
            arrayOfObject[3] = Integer.valueOf(paramSyncOperation.account.name.hashCode());
            EventLog.writeEvent(2720, arrayOfObject);
            SyncManager.this.mSyncStorageEngine.stopSyncEvent(paramLong1, paramLong2, paramString, paramInt2, paramInt1);
        }

        class SyncNotificationInfo
        {
            public boolean isActive = false;
            public Long startTime = null;

            SyncNotificationInfo()
            {
            }

            public String toString()
            {
                StringBuilder localStringBuilder = new StringBuilder();
                toString(localStringBuilder);
                return localStringBuilder.toString();
            }

            public void toString(StringBuilder paramStringBuilder)
            {
                paramStringBuilder.append("isActive ").append(this.isActive).append(", startTime ").append(this.startTime);
            }
        }
    }

    class ServiceConnectionData
    {
        public final SyncManager.ActiveSyncContext activeSyncContext;
        public final ISyncAdapter syncAdapter;

        ServiceConnectionData(SyncManager.ActiveSyncContext paramISyncAdapter, ISyncAdapter arg3)
        {
            this.activeSyncContext = paramISyncAdapter;
            Object localObject;
            this.syncAdapter = localObject;
        }
    }

    private class SyncTimeTracker
    {
        boolean mLastWasSyncing = false;
        private long mTimeSpentSyncing;
        long mWhenSyncStarted = 0L;

        private SyncTimeTracker()
        {
        }

        /** @deprecated */
        public long timeSpentSyncing()
        {
            try
            {
                if (!this.mLastWasSyncing);
                long l1;
                long l2;
                long l3;
                for (long l4 = this.mTimeSpentSyncing; ; l4 = l2 + (l1 - l3))
                {
                    return l4;
                    l1 = SystemClock.elapsedRealtime();
                    l2 = this.mTimeSpentSyncing;
                    l3 = this.mWhenSyncStarted;
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public void update()
        {
            while (true)
            {
                long l;
                try
                {
                    boolean bool1;
                    if (!SyncManager.this.mActiveSyncContexts.isEmpty())
                    {
                        bool1 = true;
                        boolean bool2 = this.mLastWasSyncing;
                        if (bool1 != bool2);
                    }
                    else
                    {
                        bool1 = false;
                        continue;
                    }
                    l = SystemClock.elapsedRealtime();
                    if (bool1)
                    {
                        this.mWhenSyncStarted = l;
                        this.mLastWasSyncing = bool1;
                        continue;
                    }
                }
                finally
                {
                }
                this.mTimeSpentSyncing += l - this.mWhenSyncStarted;
            }
        }
    }

    private static class AccountSyncStats
    {
        long elapsedTime;
        String name;
        int times;

        private AccountSyncStats(String paramString)
        {
            this.name = paramString;
        }
    }

    private static class AuthoritySyncStats
    {
        Map<String, SyncManager.AccountSyncStats> accountMap = Maps.newHashMap();
        long elapsedTime;
        String name;
        int times;

        private AuthoritySyncStats(String paramString)
        {
            this.name = paramString;
        }
    }

    class ActiveSyncContext extends ISyncContext.Stub
        implements ServiceConnection, IBinder.DeathRecipient
    {
        boolean mBound;
        final long mHistoryRowId;
        boolean mIsLinkedToDeath = false;
        final long mStartTime;
        ISyncAdapter mSyncAdapter;
        final int mSyncAdapterUid;
        SyncInfo mSyncInfo;
        final SyncOperation mSyncOperation;
        final PowerManager.WakeLock mSyncWakeLock;
        long mTimeoutStartTime;

        public ActiveSyncContext(SyncOperation paramLong, long arg3, int arg5)
        {
            int i;
            this.mSyncAdapterUid = i;
            this.mSyncOperation = paramLong;
            this.mHistoryRowId = ???;
            this.mSyncAdapter = null;
            this.mStartTime = SystemClock.elapsedRealtime();
            this.mTimeoutStartTime = this.mStartTime;
            this.mSyncWakeLock = SyncManager.this.mSyncHandler.getSyncWakeLock(this.mSyncOperation.account, this.mSyncOperation.authority);
            this.mSyncWakeLock.setWorkSource(new WorkSource(i));
            this.mSyncWakeLock.acquire();
        }

        boolean bindToSyncAdapter(RegisteredServicesCache.ServiceInfo paramServiceInfo)
        {
            if (Log.isLoggable("SyncManager", 2))
                Log.d("SyncManager", "bindToSyncAdapter: " + paramServiceInfo.componentName + ", connection " + this);
            Intent localIntent = new Intent();
            localIntent.setAction("android.content.SyncAdapter");
            localIntent.setComponent(paramServiceInfo.componentName);
            localIntent.putExtra("android.intent.extra.client_label", 17040502);
            localIntent.putExtra("android.intent.extra.client_intent", PendingIntent.getActivity(SyncManager.this.mContext, 0, new Intent("android.settings.SYNC_SETTINGS"), 0));
            this.mBound = true;
            boolean bool = SyncManager.this.mContext.bindService(localIntent, this, 21, this.mSyncOperation.userId);
            if (!bool)
                this.mBound = false;
            return bool;
        }

        public void binderDied()
        {
            SyncManager.this.sendSyncFinishedOrCanceledMessage(this, null);
        }

        protected void close()
        {
            if (Log.isLoggable("SyncManager", 2))
                Log.d("SyncManager", "unBindFromSyncAdapter: connection " + this);
            if (this.mBound)
            {
                this.mBound = false;
                SyncManager.this.mContext.unbindService(this);
            }
            this.mSyncWakeLock.release();
            this.mSyncWakeLock.setWorkSource(null);
        }

        public void onFinished(SyncResult paramSyncResult)
        {
            if (Log.isLoggable("SyncManager", 2))
                Log.v("SyncManager", "onFinished: " + this);
            SyncManager.this.sendSyncFinishedOrCanceledMessage(this, paramSyncResult);
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            Message localMessage = SyncManager.this.mSyncHandler.obtainMessage();
            localMessage.what = 4;
            localMessage.obj = new SyncManager.ServiceConnectionData(SyncManager.this, this, ISyncAdapter.Stub.asInterface(paramIBinder));
            SyncManager.this.mSyncHandler.sendMessage(localMessage);
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            Message localMessage = SyncManager.this.mSyncHandler.obtainMessage();
            localMessage.what = 5;
            localMessage.obj = new SyncManager.ServiceConnectionData(SyncManager.this, this, null);
            SyncManager.this.mSyncHandler.sendMessage(localMessage);
        }

        public void sendHeartbeat()
        {
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            toString(localStringBuilder);
            return localStringBuilder.toString();
        }

        public void toString(StringBuilder paramStringBuilder)
        {
            paramStringBuilder.append("startTime ").append(this.mStartTime).append(", mTimeoutStartTime ").append(this.mTimeoutStartTime).append(", mHistoryRowId ").append(this.mHistoryRowId).append(", syncOperation ").append(this.mSyncOperation);
        }
    }

    class SyncAlarmIntentReceiver extends BroadcastReceiver
    {
        SyncAlarmIntentReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            SyncManager.this.mHandleAlarmWakeLock.acquire();
            SyncManager.this.sendSyncAlarmMessage();
        }
    }

    class SyncHandlerMessagePayload
    {
        public final SyncManager.ActiveSyncContext activeSyncContext;
        public final SyncResult syncResult;

        SyncHandlerMessagePayload(SyncManager.ActiveSyncContext paramSyncResult, SyncResult arg3)
        {
            this.activeSyncContext = paramSyncResult;
            Object localObject;
            this.syncResult = localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncManager
 * JD-Core Version:        0.6.2
 */