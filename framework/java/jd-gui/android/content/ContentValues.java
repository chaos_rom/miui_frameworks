package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public final class ContentValues
    implements Parcelable
{
    public static final Parcelable.Creator<ContentValues> CREATOR = new Parcelable.Creator()
    {
        public ContentValues createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ContentValues(paramAnonymousParcel.readHashMap(null), null);
        }

        public ContentValues[] newArray(int paramAnonymousInt)
        {
            return new ContentValues[paramAnonymousInt];
        }
    };
    public static final String TAG = "ContentValues";
    private HashMap<String, Object> mValues;

    public ContentValues()
    {
        this.mValues = new HashMap(8);
    }

    public ContentValues(int paramInt)
    {
        this.mValues = new HashMap(paramInt, 1.0F);
    }

    public ContentValues(ContentValues paramContentValues)
    {
        this.mValues = new HashMap(paramContentValues.mValues);
    }

    private ContentValues(HashMap<String, Object> paramHashMap)
    {
        this.mValues = paramHashMap;
    }

    public void clear()
    {
        this.mValues.clear();
    }

    public boolean containsKey(String paramString)
    {
        return this.mValues.containsKey(paramString);
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof ContentValues));
        for (boolean bool = false; ; bool = this.mValues.equals(((ContentValues)paramObject).mValues))
            return bool;
    }

    public Object get(String paramString)
    {
        return this.mValues.get(paramString);
    }

    public Boolean getAsBoolean(String paramString)
    {
        Object localObject = this.mValues.get(paramString);
        try
        {
            localBoolean = (Boolean)localObject;
            return localBoolean;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                Boolean localBoolean;
                if ((localObject instanceof CharSequence))
                {
                    localBoolean = Boolean.valueOf(localObject.toString());
                }
                else
                {
                    if ((localObject instanceof Number))
                    {
                        if (((Number)localObject).intValue() != 0);
                        for (boolean bool = true; ; bool = false)
                        {
                            localBoolean = Boolean.valueOf(bool);
                            break;
                        }
                    }
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Boolean: " + localObject, localClassCastException);
                    localBoolean = null;
                }
            }
        }
    }

    public Byte getAsByte(String paramString)
    {
        Object localObject1 = null;
        Object localObject2 = this.mValues.get(paramString);
        if (localObject2 != null);
        try
        {
            Byte localByte3 = Byte.valueOf(((Number)localObject2).byteValue());
            for (Byte localByte1 = localByte3; ; localByte1 = null)
            {
                localObject1 = localByte1;
                return localObject1;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                if ((localObject2 instanceof CharSequence))
                    try
                    {
                        Byte localByte2 = Byte.valueOf(localObject2.toString());
                        localObject1 = localByte2;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.e("ContentValues", "Cannot parse Byte value for " + localObject2 + " at key " + paramString);
                    }
                else
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Byte: " + localObject2, localClassCastException);
        }
    }

    public byte[] getAsByteArray(String paramString)
    {
        Object localObject = this.mValues.get(paramString);
        if ((localObject instanceof byte[]));
        for (byte[] arrayOfByte = (byte[])localObject; ; arrayOfByte = null)
            return arrayOfByte;
    }

    public Double getAsDouble(String paramString)
    {
        Object localObject1 = null;
        Object localObject2 = this.mValues.get(paramString);
        if (localObject2 != null);
        try
        {
            Double localDouble3 = Double.valueOf(((Number)localObject2).doubleValue());
            for (Double localDouble1 = localDouble3; ; localDouble1 = null)
            {
                localObject1 = localDouble1;
                return localObject1;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                if ((localObject2 instanceof CharSequence))
                    try
                    {
                        Double localDouble2 = Double.valueOf(localObject2.toString());
                        localObject1 = localDouble2;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.e("ContentValues", "Cannot parse Double value for " + localObject2 + " at key " + paramString);
                    }
                else
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Double: " + localObject2, localClassCastException);
        }
    }

    public Float getAsFloat(String paramString)
    {
        Object localObject1 = null;
        Object localObject2 = this.mValues.get(paramString);
        if (localObject2 != null);
        try
        {
            Float localFloat3 = Float.valueOf(((Number)localObject2).floatValue());
            for (Float localFloat1 = localFloat3; ; localFloat1 = null)
            {
                localObject1 = localFloat1;
                return localObject1;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                if ((localObject2 instanceof CharSequence))
                    try
                    {
                        Float localFloat2 = Float.valueOf(localObject2.toString());
                        localObject1 = localFloat2;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.e("ContentValues", "Cannot parse Float value for " + localObject2 + " at key " + paramString);
                    }
                else
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Float: " + localObject2, localClassCastException);
        }
    }

    public Integer getAsInteger(String paramString)
    {
        Object localObject1 = null;
        Object localObject2 = this.mValues.get(paramString);
        if (localObject2 != null);
        try
        {
            Integer localInteger3 = Integer.valueOf(((Number)localObject2).intValue());
            for (Integer localInteger1 = localInteger3; ; localInteger1 = null)
            {
                localObject1 = localInteger1;
                return localObject1;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                if ((localObject2 instanceof CharSequence))
                    try
                    {
                        Integer localInteger2 = Integer.valueOf(localObject2.toString());
                        localObject1 = localInteger2;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.e("ContentValues", "Cannot parse Integer value for " + localObject2 + " at key " + paramString);
                    }
                else
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Integer: " + localObject2, localClassCastException);
        }
    }

    public Long getAsLong(String paramString)
    {
        Object localObject1 = null;
        Object localObject2 = this.mValues.get(paramString);
        if (localObject2 != null);
        try
        {
            Long localLong3 = Long.valueOf(((Number)localObject2).longValue());
            for (Long localLong1 = localLong3; ; localLong1 = null)
            {
                localObject1 = localLong1;
                return localObject1;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                if ((localObject2 instanceof CharSequence))
                    try
                    {
                        Long localLong2 = Long.valueOf(localObject2.toString());
                        localObject1 = localLong2;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.e("ContentValues", "Cannot parse Long value for " + localObject2 + " at key " + paramString);
                    }
                else
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Long: " + localObject2, localClassCastException);
        }
    }

    public Short getAsShort(String paramString)
    {
        Object localObject1 = null;
        Object localObject2 = this.mValues.get(paramString);
        if (localObject2 != null);
        try
        {
            Short localShort3 = Short.valueOf(((Number)localObject2).shortValue());
            for (Short localShort1 = localShort3; ; localShort1 = null)
            {
                localObject1 = localShort1;
                return localObject1;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                if ((localObject2 instanceof CharSequence))
                    try
                    {
                        Short localShort2 = Short.valueOf(localObject2.toString());
                        localObject1 = localShort2;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.e("ContentValues", "Cannot parse Short value for " + localObject2 + " at key " + paramString);
                    }
                else
                    Log.e("ContentValues", "Cannot cast value for " + paramString + " to a Short: " + localObject2, localClassCastException);
        }
    }

    public String getAsString(String paramString)
    {
        Object localObject = this.mValues.get(paramString);
        if (localObject != null);
        for (String str = localObject.toString(); ; str = null)
            return str;
    }

    @Deprecated
    public ArrayList<String> getStringArrayList(String paramString)
    {
        return (ArrayList)this.mValues.get(paramString);
    }

    public int hashCode()
    {
        return this.mValues.hashCode();
    }

    public Set<String> keySet()
    {
        return this.mValues.keySet();
    }

    public void put(String paramString, Boolean paramBoolean)
    {
        this.mValues.put(paramString, paramBoolean);
    }

    public void put(String paramString, Byte paramByte)
    {
        this.mValues.put(paramString, paramByte);
    }

    public void put(String paramString, Double paramDouble)
    {
        this.mValues.put(paramString, paramDouble);
    }

    public void put(String paramString, Float paramFloat)
    {
        this.mValues.put(paramString, paramFloat);
    }

    public void put(String paramString, Integer paramInteger)
    {
        this.mValues.put(paramString, paramInteger);
    }

    public void put(String paramString, Long paramLong)
    {
        this.mValues.put(paramString, paramLong);
    }

    public void put(String paramString, Short paramShort)
    {
        this.mValues.put(paramString, paramShort);
    }

    public void put(String paramString1, String paramString2)
    {
        this.mValues.put(paramString1, paramString2);
    }

    public void put(String paramString, byte[] paramArrayOfByte)
    {
        this.mValues.put(paramString, paramArrayOfByte);
    }

    public void putAll(ContentValues paramContentValues)
    {
        this.mValues.putAll(paramContentValues.mValues);
    }

    public void putNull(String paramString)
    {
        this.mValues.put(paramString, null);
    }

    @Deprecated
    public void putStringArrayList(String paramString, ArrayList<String> paramArrayList)
    {
        this.mValues.put(paramString, paramArrayList);
    }

    public void remove(String paramString)
    {
        this.mValues.remove(paramString);
    }

    public int size()
    {
        return this.mValues.size();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator = this.mValues.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str1 = (String)localIterator.next();
            String str2 = getAsString(str1);
            if (localStringBuilder.length() > 0)
                localStringBuilder.append(" ");
            localStringBuilder.append(str1 + "=" + str2);
        }
        return localStringBuilder.toString();
    }

    public Set<Map.Entry<String, Object>> valueSet()
    {
        return this.mValues.entrySet();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeMap(this.mValues);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentValues
 * JD-Core Version:        0.6.2
 */