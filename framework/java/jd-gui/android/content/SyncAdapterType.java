package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public class SyncAdapterType
    implements Parcelable
{
    public static final Parcelable.Creator<SyncAdapterType> CREATOR = new Parcelable.Creator()
    {
        public SyncAdapterType createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SyncAdapterType(paramAnonymousParcel);
        }

        public SyncAdapterType[] newArray(int paramAnonymousInt)
        {
            return new SyncAdapterType[paramAnonymousInt];
        }
    };
    public final String accountType;
    private final boolean allowParallelSyncs;
    public final String authority;
    private final boolean isAlwaysSyncable;
    public final boolean isKey;
    private final String settingsActivity;
    private final boolean supportsUploading;
    private final boolean userVisible;

    public SyncAdapterType(Parcel paramParcel)
    {
    }

    private SyncAdapterType(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("the authority must not be empty: " + paramString1);
        if (TextUtils.isEmpty(paramString2))
            throw new IllegalArgumentException("the accountType must not be empty: " + paramString2);
        this.authority = paramString1;
        this.accountType = paramString2;
        this.userVisible = true;
        this.supportsUploading = true;
        this.isAlwaysSyncable = false;
        this.allowParallelSyncs = false;
        this.settingsActivity = null;
        this.isKey = true;
    }

    public SyncAdapterType(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("the authority must not be empty: " + paramString1);
        if (TextUtils.isEmpty(paramString2))
            throw new IllegalArgumentException("the accountType must not be empty: " + paramString2);
        this.authority = paramString1;
        this.accountType = paramString2;
        this.userVisible = paramBoolean1;
        this.supportsUploading = paramBoolean2;
        this.isAlwaysSyncable = false;
        this.allowParallelSyncs = false;
        this.settingsActivity = null;
        this.isKey = false;
    }

    public SyncAdapterType(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString3)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("the authority must not be empty: " + paramString1);
        if (TextUtils.isEmpty(paramString2))
            throw new IllegalArgumentException("the accountType must not be empty: " + paramString2);
        this.authority = paramString1;
        this.accountType = paramString2;
        this.userVisible = paramBoolean1;
        this.supportsUploading = paramBoolean2;
        this.isAlwaysSyncable = paramBoolean3;
        this.allowParallelSyncs = paramBoolean4;
        this.settingsActivity = paramString3;
        this.isKey = false;
    }

    public static SyncAdapterType newKey(String paramString1, String paramString2)
    {
        return new SyncAdapterType(paramString1, paramString2);
    }

    public boolean allowParallelSyncs()
    {
        if (this.isKey)
            throw new IllegalStateException("this method is not allowed to be called when this is a key");
        return this.allowParallelSyncs;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof SyncAdapterType))
            {
                bool = false;
            }
            else
            {
                SyncAdapterType localSyncAdapterType = (SyncAdapterType)paramObject;
                if ((!this.authority.equals(localSyncAdapterType.authority)) || (!this.accountType.equals(localSyncAdapterType.accountType)))
                    bool = false;
            }
        }
    }

    public String getSettingsActivity()
    {
        if (this.isKey)
            throw new IllegalStateException("this method is not allowed to be called when this is a key");
        return this.settingsActivity;
    }

    public int hashCode()
    {
        return 31 * (527 + this.authority.hashCode()) + this.accountType.hashCode();
    }

    public boolean isAlwaysSyncable()
    {
        if (this.isKey)
            throw new IllegalStateException("this method is not allowed to be called when this is a key");
        return this.isAlwaysSyncable;
    }

    public boolean isUserVisible()
    {
        if (this.isKey)
            throw new IllegalStateException("this method is not allowed to be called when this is a key");
        return this.userVisible;
    }

    public boolean supportsUploading()
    {
        if (this.isKey)
            throw new IllegalStateException("this method is not allowed to be called when this is a key");
        return this.supportsUploading;
    }

    public String toString()
    {
        if (this.isKey);
        for (String str = "SyncAdapterType Key {name=" + this.authority + ", type=" + this.accountType + "}"; ; str = "SyncAdapterType {name=" + this.authority + ", type=" + this.accountType + ", userVisible=" + this.userVisible + ", supportsUploading=" + this.supportsUploading + ", isAlwaysSyncable=" + this.isAlwaysSyncable + ", allowParallelSyncs=" + this.allowParallelSyncs + ", settingsActivity=" + this.settingsActivity + "}")
            return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        if (this.isKey)
            throw new IllegalStateException("keys aren't parcelable");
        paramParcel.writeString(this.authority);
        paramParcel.writeString(this.accountType);
        int j;
        int k;
        label61: int m;
        if (this.userVisible)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.supportsUploading)
                break label110;
            k = i;
            paramParcel.writeInt(k);
            if (!this.isAlwaysSyncable)
                break label116;
            m = i;
            label77: paramParcel.writeInt(m);
            if (!this.allowParallelSyncs)
                break label122;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeString(this.settingsActivity);
            return;
            j = 0;
            break;
            label110: k = 0;
            break label61;
            label116: m = 0;
            break label77;
            label122: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncAdapterType
 * JD-Core Version:        0.6.2
 */