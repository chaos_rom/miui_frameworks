package android.content;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ContentProviderResult
    implements Parcelable
{
    public static final Parcelable.Creator<ContentProviderResult> CREATOR = new Parcelable.Creator()
    {
        public ContentProviderResult createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ContentProviderResult(paramAnonymousParcel);
        }

        public ContentProviderResult[] newArray(int paramAnonymousInt)
        {
            return new ContentProviderResult[paramAnonymousInt];
        }
    };
    public final Integer count;
    public final Uri uri;

    public ContentProviderResult(int paramInt)
    {
        this.count = Integer.valueOf(paramInt);
        this.uri = null;
    }

    public ContentProviderResult(Uri paramUri)
    {
        if (paramUri == null)
            throw new IllegalArgumentException("uri must not be null");
        this.uri = paramUri;
        this.count = null;
    }

    public ContentProviderResult(Parcel paramParcel)
    {
        if (paramParcel.readInt() == 1)
            this.count = Integer.valueOf(paramParcel.readInt());
        for (this.uri = null; ; this.uri = ((Uri)Uri.CREATOR.createFromParcel(paramParcel)))
        {
            return;
            this.count = null;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        if (this.uri != null);
        for (String str = "ContentProviderResult(uri=" + this.uri.toString() + ")"; ; str = "ContentProviderResult(count=" + this.count + ")")
            return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.uri == null)
        {
            paramParcel.writeInt(1);
            paramParcel.writeInt(this.count.intValue());
        }
        while (true)
        {
            return;
            paramParcel.writeInt(2);
            this.uri.writeToParcel(paramParcel, 0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentProviderResult
 * JD-Core Version:        0.6.2
 */