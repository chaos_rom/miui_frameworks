package android.content;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ISyncStatusObserver extends IInterface
{
    public abstract void onStatusChanged(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISyncStatusObserver
    {
        private static final String DESCRIPTOR = "android.content.ISyncStatusObserver";
        static final int TRANSACTION_onStatusChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.content.ISyncStatusObserver");
        }

        public static ISyncStatusObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.ISyncStatusObserver");
                if ((localIInterface != null) && ((localIInterface instanceof ISyncStatusObserver)))
                    localObject = (ISyncStatusObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.content.ISyncStatusObserver");
                continue;
                paramParcel1.enforceInterface("android.content.ISyncStatusObserver");
                onStatusChanged(paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements ISyncStatusObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.ISyncStatusObserver";
            }

            public void onStatusChanged(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.content.ISyncStatusObserver");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ISyncStatusObserver
 * JD-Core Version:        0.6.2
 */