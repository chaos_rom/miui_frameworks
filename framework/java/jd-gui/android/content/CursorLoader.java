package android.content;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

public class CursorLoader extends AsyncTaskLoader<Cursor>
{
    CancellationSignal mCancellationSignal;
    Cursor mCursor;
    final Loader<Cursor>.ForceLoadContentObserver mObserver = new Loader.ForceLoadContentObserver(this);
    String[] mProjection;
    String mSelection;
    String[] mSelectionArgs;
    String mSortOrder;
    Uri mUri;

    public CursorLoader(Context paramContext)
    {
        super(paramContext);
    }

    public CursorLoader(Context paramContext, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        super(paramContext);
        this.mUri = paramUri;
        this.mProjection = paramArrayOfString1;
        this.mSelection = paramString1;
        this.mSelectionArgs = paramArrayOfString2;
        this.mSortOrder = paramString2;
    }

    public void cancelLoadInBackground()
    {
        super.cancelLoadInBackground();
        try
        {
            if (this.mCancellationSignal != null)
                this.mCancellationSignal.cancel();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void deliverResult(Cursor paramCursor)
    {
        if (isReset())
            if (paramCursor != null)
                paramCursor.close();
        while (true)
        {
            return;
            Cursor localCursor = this.mCursor;
            this.mCursor = paramCursor;
            if (isStarted())
                super.deliverResult(paramCursor);
            if ((localCursor != null) && (localCursor != paramCursor) && (!localCursor.isClosed()))
                localCursor.close();
        }
    }

    public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        super.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mUri=");
        paramPrintWriter.println(this.mUri);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mProjection=");
        paramPrintWriter.println(Arrays.toString(this.mProjection));
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSelection=");
        paramPrintWriter.println(this.mSelection);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSelectionArgs=");
        paramPrintWriter.println(Arrays.toString(this.mSelectionArgs));
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSortOrder=");
        paramPrintWriter.println(this.mSortOrder);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mCursor=");
        paramPrintWriter.println(this.mCursor);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mContentChanged=");
        paramPrintWriter.println(this.mContentChanged);
    }

    public String[] getProjection()
    {
        return this.mProjection;
    }

    public String getSelection()
    {
        return this.mSelection;
    }

    public String[] getSelectionArgs()
    {
        return this.mSelectionArgs;
    }

    public String getSortOrder()
    {
        return this.mSortOrder;
    }

    public Uri getUri()
    {
        return this.mUri;
    }

    // ERROR //
    public Cursor loadInBackground()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokevirtual 134	android/content/CursorLoader:isLoadInBackgroundCanceled	()Z
        //     6: ifeq +16 -> 22
        //     9: new 136	android/os/OperationCanceledException
        //     12: dup
        //     13: invokespecial 138	android/os/OperationCanceledException:<init>	()V
        //     16: athrow
        //     17: astore_1
        //     18: aload_0
        //     19: monitorexit
        //     20: aload_1
        //     21: athrow
        //     22: aload_0
        //     23: new 50	android/os/CancellationSignal
        //     26: dup
        //     27: invokespecial 139	android/os/CancellationSignal:<init>	()V
        //     30: putfield 48	android/content/CursorLoader:mCancellationSignal	Landroid/os/CancellationSignal;
        //     33: aload_0
        //     34: monitorexit
        //     35: aload_0
        //     36: invokevirtual 143	android/content/CursorLoader:getContext	()Landroid/content/Context;
        //     39: invokevirtual 149	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     42: aload_0
        //     43: getfield 34	android/content/CursorLoader:mUri	Landroid/net/Uri;
        //     46: aload_0
        //     47: getfield 36	android/content/CursorLoader:mProjection	[Ljava/lang/String;
        //     50: aload_0
        //     51: getfield 38	android/content/CursorLoader:mSelection	Ljava/lang/String;
        //     54: aload_0
        //     55: getfield 40	android/content/CursorLoader:mSelectionArgs	[Ljava/lang/String;
        //     58: aload_0
        //     59: getfield 42	android/content/CursorLoader:mSortOrder	Ljava/lang/String;
        //     62: aload_0
        //     63: getfield 48	android/content/CursorLoader:mCancellationSignal	Landroid/os/CancellationSignal;
        //     66: invokevirtual 155	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
        //     69: astore 4
        //     71: aload 4
        //     73: ifnull +21 -> 94
        //     76: aload 4
        //     78: invokeinterface 159 1 0
        //     83: pop
        //     84: aload_0
        //     85: aload 4
        //     87: aload_0
        //     88: getfield 31	android/content/CursorLoader:mObserver	Landroid/content/Loader$ForceLoadContentObserver;
        //     91: invokevirtual 163	android/content/CursorLoader:registerContentObserver	(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
        //     94: aload_0
        //     95: monitorenter
        //     96: aload_0
        //     97: aconst_null
        //     98: putfield 48	android/content/CursorLoader:mCancellationSignal	Landroid/os/CancellationSignal;
        //     101: aload_0
        //     102: monitorexit
        //     103: aload 4
        //     105: areturn
        //     106: astore_2
        //     107: aload_0
        //     108: monitorenter
        //     109: aload_0
        //     110: aconst_null
        //     111: putfield 48	android/content/CursorLoader:mCancellationSignal	Landroid/os/CancellationSignal;
        //     114: aload_0
        //     115: monitorexit
        //     116: aload_2
        //     117: athrow
        //     118: astore_3
        //     119: aload_0
        //     120: monitorexit
        //     121: aload_3
        //     122: athrow
        //     123: astore 5
        //     125: aload_0
        //     126: monitorexit
        //     127: aload 5
        //     129: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	20	17	finally
        //     22	35	17	finally
        //     35	94	106	finally
        //     109	116	118	finally
        //     119	121	118	finally
        //     96	103	123	finally
        //     125	127	123	finally
    }

    public void onCanceled(Cursor paramCursor)
    {
        if ((paramCursor != null) && (!paramCursor.isClosed()))
            paramCursor.close();
    }

    protected void onReset()
    {
        super.onReset();
        onStopLoading();
        if ((this.mCursor != null) && (!this.mCursor.isClosed()))
            this.mCursor.close();
        this.mCursor = null;
    }

    protected void onStartLoading()
    {
        if (this.mCursor != null)
            deliverResult(this.mCursor);
        if ((takeContentChanged()) || (this.mCursor == null))
            forceLoad();
    }

    protected void onStopLoading()
    {
        cancelLoad();
    }

    void registerContentObserver(Cursor paramCursor, ContentObserver paramContentObserver)
    {
        paramCursor.registerContentObserver(this.mObserver);
    }

    public void setProjection(String[] paramArrayOfString)
    {
        this.mProjection = paramArrayOfString;
    }

    public void setSelection(String paramString)
    {
        this.mSelection = paramString;
    }

    public void setSelectionArgs(String[] paramArrayOfString)
    {
        this.mSelectionArgs = paramArrayOfString;
    }

    public void setSortOrder(String paramString)
    {
        this.mSortOrder = paramString;
    }

    public void setUri(Uri paramUri)
    {
        this.mUri = paramUri;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.CursorLoader
 * JD-Core Version:        0.6.2
 */