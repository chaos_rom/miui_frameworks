package android.content;

import android.util.EventLog;

public class EventLogTags
{
    public static final int BINDER_SAMPLE = 52004;
    public static final int CONTENT_QUERY_SAMPLE = 52002;
    public static final int CONTENT_UPDATE_SAMPLE = 52003;

    public static void writeBinderSample(String paramString1, int paramInt1, int paramInt2, String paramString2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = paramString2;
        arrayOfObject[4] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(52004, arrayOfObject);
    }

    public static void writeContentQuerySample(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, String paramString5, int paramInt2)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        arrayOfObject[2] = paramString3;
        arrayOfObject[3] = paramString4;
        arrayOfObject[4] = Integer.valueOf(paramInt1);
        arrayOfObject[5] = paramString5;
        arrayOfObject[6] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(52002, arrayOfObject);
    }

    public static void writeContentUpdateSample(String paramString1, String paramString2, String paramString3, int paramInt1, String paramString4, int paramInt2)
    {
        Object[] arrayOfObject = new Object[6];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        arrayOfObject[2] = paramString3;
        arrayOfObject[3] = Integer.valueOf(paramInt1);
        arrayOfObject[4] = paramString4;
        arrayOfObject[5] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(52003, arrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.EventLogTags
 * JD-Core Version:        0.6.2
 */