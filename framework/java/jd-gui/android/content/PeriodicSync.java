package android.content;

import android.accounts.Account;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PeriodicSync
    implements Parcelable
{
    public static final Parcelable.Creator<PeriodicSync> CREATOR = new Parcelable.Creator()
    {
        public PeriodicSync createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PeriodicSync((Account)Account.CREATOR.createFromParcel(paramAnonymousParcel), paramAnonymousParcel.readString(), paramAnonymousParcel.readBundle(), paramAnonymousParcel.readLong());
        }

        public PeriodicSync[] newArray(int paramAnonymousInt)
        {
            return new PeriodicSync[paramAnonymousInt];
        }
    };
    public final Account account;
    public final String authority;
    public final Bundle extras;
    public final long period;

    public PeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong)
    {
        this.account = paramAccount;
        this.authority = paramString;
        this.extras = new Bundle(paramBundle);
        this.period = paramLong;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof PeriodicSync))
            {
                bool = false;
            }
            else
            {
                PeriodicSync localPeriodicSync = (PeriodicSync)paramObject;
                if ((!this.account.equals(localPeriodicSync.account)) || (!this.authority.equals(localPeriodicSync.authority)) || (this.period != localPeriodicSync.period) || (!SyncStorageEngine.equals(this.extras, localPeriodicSync.extras)))
                    bool = false;
            }
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        this.account.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.authority);
        paramParcel.writeBundle(this.extras);
        paramParcel.writeLong(this.period);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.PeriodicSync
 * JD-Core Version:        0.6.2
 */