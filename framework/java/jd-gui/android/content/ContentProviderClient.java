package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.DeadObjectException;
import android.os.ICancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class ContentProviderClient
{
    private final IContentProvider mContentProvider;
    private final ContentResolver mContentResolver;
    private boolean mReleased;
    private final boolean mStable;

    ContentProviderClient(ContentResolver paramContentResolver, IContentProvider paramIContentProvider, boolean paramBoolean)
    {
        this.mContentProvider = paramIContentProvider;
        this.mContentResolver = paramContentResolver;
        this.mStable = paramBoolean;
    }

    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> paramArrayList)
        throws RemoteException, OperationApplicationException
    {
        try
        {
            ContentProviderResult[] arrayOfContentProviderResult = this.mContentProvider.applyBatch(paramArrayList);
            return arrayOfContentProviderResult;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
        throws RemoteException
    {
        try
        {
            int i = this.mContentProvider.bulkInsert(paramUri, paramArrayOfContentValues);
            return i;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
        throws RemoteException
    {
        try
        {
            int i = this.mContentProvider.delete(paramUri, paramString, paramArrayOfString);
            return i;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public ContentProvider getLocalContentProvider()
    {
        return ContentProvider.coerceToLocalContentProvider(this.mContentProvider);
    }

    public String[] getStreamTypes(Uri paramUri, String paramString)
        throws RemoteException
    {
        try
        {
            String[] arrayOfString = this.mContentProvider.getStreamTypes(paramUri, paramString);
            return arrayOfString;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public String getType(Uri paramUri)
        throws RemoteException
    {
        try
        {
            String str = this.mContentProvider.getType(paramUri);
            return str;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public Uri insert(Uri paramUri, ContentValues paramContentValues)
        throws RemoteException
    {
        try
        {
            Uri localUri = this.mContentProvider.insert(paramUri, paramContentValues);
            return localUri;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public AssetFileDescriptor openAssetFile(Uri paramUri, String paramString)
        throws RemoteException, FileNotFoundException
    {
        try
        {
            AssetFileDescriptor localAssetFileDescriptor = this.mContentProvider.openAssetFile(paramUri, paramString);
            return localAssetFileDescriptor;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
        throws RemoteException, FileNotFoundException
    {
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor = this.mContentProvider.openFile(paramUri, paramString);
            return localParcelFileDescriptor;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public final AssetFileDescriptor openTypedAssetFileDescriptor(Uri paramUri, String paramString, Bundle paramBundle)
        throws RemoteException, FileNotFoundException
    {
        try
        {
            AssetFileDescriptor localAssetFileDescriptor = this.mContentProvider.openTypedAssetFile(paramUri, paramString, paramBundle);
            return localAssetFileDescriptor;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
        throws RemoteException
    {
        try
        {
            Cursor localCursor = query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, null);
            return localCursor;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
        throws RemoteException
    {
        ICancellationSignal localICancellationSignal = null;
        if (paramCancellationSignal != null)
        {
            localICancellationSignal = this.mContentProvider.createCancellationSignal();
            paramCancellationSignal.setRemote(localICancellationSignal);
        }
        try
        {
            Cursor localCursor = this.mContentProvider.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, localICancellationSignal);
            return localCursor;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }

    // ERROR //
    public boolean release()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 105	android/content/ContentProviderClient:mReleased	Z
        //     6: ifeq +18 -> 24
        //     9: new 107	java/lang/IllegalStateException
        //     12: dup
        //     13: ldc 109
        //     15: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_1
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_1
        //     23: athrow
        //     24: aload_0
        //     25: iconst_1
        //     26: putfield 105	android/content/ContentProviderClient:mReleased	Z
        //     29: aload_0
        //     30: getfield 22	android/content/ContentProviderClient:mStable	Z
        //     33: ifeq +20 -> 53
        //     36: aload_0
        //     37: getfield 20	android/content/ContentProviderClient:mContentResolver	Landroid/content/ContentResolver;
        //     40: aload_0
        //     41: getfield 18	android/content/ContentProviderClient:mContentProvider	Landroid/content/IContentProvider;
        //     44: invokevirtual 116	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     47: istore_2
        //     48: aload_0
        //     49: monitorexit
        //     50: goto +17 -> 67
        //     53: aload_0
        //     54: getfield 20	android/content/ContentProviderClient:mContentResolver	Landroid/content/ContentResolver;
        //     57: aload_0
        //     58: getfield 18	android/content/ContentProviderClient:mContentProvider	Landroid/content/IContentProvider;
        //     61: invokevirtual 119	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     64: istore_2
        //     65: aload_0
        //     66: monitorexit
        //     67: iload_2
        //     68: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	67	19	finally
    }

    public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
        throws RemoteException
    {
        try
        {
            int i = this.mContentProvider.update(paramUri, paramContentValues, paramString, paramArrayOfString);
            return i;
        }
        catch (DeadObjectException localDeadObjectException)
        {
            if (!this.mStable)
                this.mContentResolver.unstableProviderDied(this.mContentProvider);
            throw localDeadObjectException;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentProviderClient
 * JD-Core Version:        0.6.2
 */