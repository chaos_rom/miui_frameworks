package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class SyncStatusInfo
    implements Parcelable
{
    public static final Parcelable.Creator<SyncStatusInfo> CREATOR = new Parcelable.Creator()
    {
        public SyncStatusInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SyncStatusInfo(paramAnonymousParcel);
        }

        public SyncStatusInfo[] newArray(int paramAnonymousInt)
        {
            return new SyncStatusInfo[paramAnonymousInt];
        }
    };
    private static final String TAG = "Sync";
    static final int VERSION = 2;
    public final int authorityId;
    public long initialFailureTime;
    public boolean initialize;
    public String lastFailureMesg;
    public int lastFailureSource;
    public long lastFailureTime;
    public int lastSuccessSource;
    public long lastSuccessTime;
    public int numSourceLocal;
    public int numSourcePeriodic;
    public int numSourcePoll;
    public int numSourceServer;
    public int numSourceUser;
    public int numSyncs;
    public boolean pending;
    public ArrayList<Long> periodicSyncTimes;
    public long totalElapsedTime;

    SyncStatusInfo(int paramInt)
    {
        this.authorityId = paramInt;
    }

    SyncStatusInfo(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        if ((i != 2) && (i != 1))
            Log.w("SyncStatusInfo", "Unknown version: " + i);
        this.authorityId = paramParcel.readInt();
        this.totalElapsedTime = paramParcel.readLong();
        this.numSyncs = paramParcel.readInt();
        this.numSourcePoll = paramParcel.readInt();
        this.numSourceServer = paramParcel.readInt();
        this.numSourceLocal = paramParcel.readInt();
        this.numSourceUser = paramParcel.readInt();
        this.lastSuccessTime = paramParcel.readLong();
        this.lastSuccessSource = paramParcel.readInt();
        this.lastFailureTime = paramParcel.readLong();
        this.lastFailureSource = paramParcel.readInt();
        this.lastFailureMesg = paramParcel.readString();
        this.initialFailureTime = paramParcel.readLong();
        boolean bool2;
        if (paramParcel.readInt() != 0)
        {
            bool2 = true;
            this.pending = bool2;
            if (paramParcel.readInt() != 0)
                bool1 = true;
            this.initialize = bool1;
            if (i != 1)
                break label197;
            this.periodicSyncTimes = null;
        }
        while (true)
        {
            return;
            bool2 = false;
            break;
            label197: int j = paramParcel.readInt();
            if (j < 0)
            {
                this.periodicSyncTimes = null;
            }
            else
            {
                this.periodicSyncTimes = new ArrayList();
                for (int k = 0; k < j; k++)
                    this.periodicSyncTimes.add(Long.valueOf(paramParcel.readLong()));
            }
        }
    }

    private void ensurePeriodicSyncTimeSize(int paramInt)
    {
        if (this.periodicSyncTimes == null)
            this.periodicSyncTimes = new ArrayList(0);
        int i = paramInt + 1;
        if (this.periodicSyncTimes.size() < i)
            for (int j = this.periodicSyncTimes.size(); j < i; j++)
                this.periodicSyncTimes.add(Long.valueOf(0L));
    }

    public int describeContents()
    {
        return 0;
    }

    public int getLastFailureMesgAsInt(int paramInt)
    {
        try
        {
            if (this.lastFailureMesg != null)
            {
                int i = Integer.parseInt(this.lastFailureMesg);
                paramInt = i;
            }
            return paramInt;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.d("Sync", "error parsing lastFailureMesg of " + this.lastFailureMesg, localNumberFormatException);
        }
    }

    public long getPeriodicSyncTime(int paramInt)
    {
        if ((this.periodicSyncTimes == null) || (this.periodicSyncTimes.size() < paramInt + 1));
        for (long l = 0L; ; l = ((Long)this.periodicSyncTimes.get(paramInt)).longValue())
            return l;
    }

    public void removePeriodicSyncTime(int paramInt)
    {
        ensurePeriodicSyncTimeSize(paramInt);
        this.periodicSyncTimes.remove(paramInt);
    }

    public void setPeriodicSyncTime(int paramInt, long paramLong)
    {
        ensurePeriodicSyncTimeSize(paramInt);
        this.periodicSyncTimes.set(paramInt, Long.valueOf(paramLong));
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(2);
        paramParcel.writeInt(this.authorityId);
        paramParcel.writeLong(this.totalElapsedTime);
        paramParcel.writeInt(this.numSyncs);
        paramParcel.writeInt(this.numSourcePoll);
        paramParcel.writeInt(this.numSourceServer);
        paramParcel.writeInt(this.numSourceLocal);
        paramParcel.writeInt(this.numSourceUser);
        paramParcel.writeLong(this.lastSuccessTime);
        paramParcel.writeInt(this.lastSuccessSource);
        paramParcel.writeLong(this.lastFailureTime);
        paramParcel.writeInt(this.lastFailureSource);
        paramParcel.writeString(this.lastFailureMesg);
        paramParcel.writeLong(this.initialFailureTime);
        int j;
        if (this.pending)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.initialize)
                break label202;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            if (this.periodicSyncTimes == null)
                break label207;
            paramParcel.writeInt(this.periodicSyncTimes.size());
            Iterator localIterator = this.periodicSyncTimes.iterator();
            while (localIterator.hasNext())
                paramParcel.writeLong(((Long)localIterator.next()).longValue());
            j = 0;
            break;
            label202: i = 0;
        }
        label207: paramParcel.writeInt(-1);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncStatusInfo
 * JD-Core Version:        0.6.2
 */