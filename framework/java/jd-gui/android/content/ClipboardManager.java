package android.content;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import java.util.ArrayList;

public class ClipboardManager extends android.text.ClipboardManager
{
    static final int MSG_REPORT_PRIMARY_CLIP_CHANGED = 1;
    private static IClipboard sService;
    private static final Object sStaticLock = new Object();
    private final Context mContext;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            }
            while (true)
            {
                return;
                ClipboardManager.this.reportPrimaryClipChanged();
            }
        }
    };
    private final ArrayList<OnPrimaryClipChangedListener> mPrimaryClipChangedListeners = new ArrayList();
    private final IOnPrimaryClipChangedListener.Stub mPrimaryClipChangedServiceListener = new IOnPrimaryClipChangedListener.Stub()
    {
        public void dispatchPrimaryClipChanged()
        {
            ClipboardManager.this.mHandler.sendEmptyMessage(1);
        }
    };

    public ClipboardManager(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
    }

    private static IClipboard getService()
    {
        IClipboard localIClipboard;
        synchronized (sStaticLock)
        {
            if (sService != null)
            {
                localIClipboard = sService;
            }
            else
            {
                sService = IClipboard.Stub.asInterface(ServiceManager.getService("clipboard"));
                localIClipboard = sService;
            }
        }
        return localIClipboard;
    }

    public void addPrimaryClipChangedListener(OnPrimaryClipChangedListener paramOnPrimaryClipChangedListener)
    {
        synchronized (this.mPrimaryClipChangedListeners)
        {
            int i = this.mPrimaryClipChangedListeners.size();
            if (i != 0);
        }
        try
        {
            getService().addPrimaryClipChangedListener(this.mPrimaryClipChangedServiceListener);
            label33: this.mPrimaryClipChangedListeners.add(paramOnPrimaryClipChangedListener);
            return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label33;
        }
    }

    public ClipData getPrimaryClip()
    {
        try
        {
            ClipData localClipData2 = getService().getPrimaryClip(this.mContext.getPackageName());
            localClipData1 = localClipData2;
            return localClipData1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ClipData localClipData1 = null;
        }
    }

    public ClipDescription getPrimaryClipDescription()
    {
        try
        {
            ClipDescription localClipDescription2 = getService().getPrimaryClipDescription();
            localClipDescription1 = localClipDescription2;
            return localClipDescription1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ClipDescription localClipDescription1 = null;
        }
    }

    public CharSequence getText()
    {
        ClipData localClipData = getPrimaryClip();
        if ((localClipData != null) && (localClipData.getItemCount() > 0));
        for (CharSequence localCharSequence = localClipData.getItemAt(0).coerceToText(this.mContext); ; localCharSequence = null)
            return localCharSequence;
    }

    public boolean hasPrimaryClip()
    {
        try
        {
            boolean bool2 = getService().hasPrimaryClip();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean hasText()
    {
        try
        {
            boolean bool2 = getService().hasClipboardText();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void removePrimaryClipChangedListener(OnPrimaryClipChangedListener paramOnPrimaryClipChangedListener)
    {
        synchronized (this.mPrimaryClipChangedListeners)
        {
            this.mPrimaryClipChangedListeners.remove(paramOnPrimaryClipChangedListener);
            int i = this.mPrimaryClipChangedListeners.size();
            if (i != 0);
        }
        try
        {
            getService().removePrimaryClipChangedListener(this.mPrimaryClipChangedServiceListener);
            label42: return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label42;
        }
    }

    void reportPrimaryClipChanged()
    {
        synchronized (this.mPrimaryClipChangedListeners)
        {
            if (this.mPrimaryClipChangedListeners.size() > 0)
            {
                Object[] arrayOfObject = this.mPrimaryClipChangedListeners.toArray();
                int i = 0;
                if (i < arrayOfObject.length)
                {
                    ((OnPrimaryClipChangedListener)arrayOfObject[i]).onPrimaryClipChanged();
                    i++;
                }
            }
        }
    }

    public void setPrimaryClip(ClipData paramClipData)
    {
        try
        {
            getService().setPrimaryClip(paramClipData);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    public void setText(CharSequence paramCharSequence)
    {
        setPrimaryClip(ClipData.newPlainText(null, paramCharSequence));
    }

    public static abstract interface OnPrimaryClipChangedListener
    {
        public abstract void onPrimaryClipChanged();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ClipboardManager
 * JD-Core Version:        0.6.2
 */