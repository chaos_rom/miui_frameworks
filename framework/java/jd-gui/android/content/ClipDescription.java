package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.util.ArrayList;

public class ClipDescription
    implements Parcelable
{
    public static final Parcelable.Creator<ClipDescription> CREATOR = new Parcelable.Creator()
    {
        public ClipDescription createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ClipDescription(paramAnonymousParcel);
        }

        public ClipDescription[] newArray(int paramAnonymousInt)
        {
            return new ClipDescription[paramAnonymousInt];
        }
    };
    public static final String MIMETYPE_TEXT_HTML = "text/html";
    public static final String MIMETYPE_TEXT_INTENT = "text/vnd.android.intent";
    public static final String MIMETYPE_TEXT_PLAIN = "text/plain";
    public static final String MIMETYPE_TEXT_URILIST = "text/uri-list";
    final CharSequence mLabel;
    final String[] mMimeTypes;

    public ClipDescription(ClipDescription paramClipDescription)
    {
        this.mLabel = paramClipDescription.mLabel;
        this.mMimeTypes = paramClipDescription.mMimeTypes;
    }

    ClipDescription(Parcel paramParcel)
    {
        this.mLabel = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        this.mMimeTypes = paramParcel.createStringArray();
    }

    public ClipDescription(CharSequence paramCharSequence, String[] paramArrayOfString)
    {
        if (paramArrayOfString == null)
            throw new NullPointerException("mimeTypes is null");
        this.mLabel = paramCharSequence;
        this.mMimeTypes = paramArrayOfString;
    }

    public static boolean compareMimeTypes(String paramString1, String paramString2)
    {
        boolean bool = true;
        int i = paramString2.length();
        if ((i == 3) && (paramString2.equals("*/*")));
        while (true)
        {
            return bool;
            int j = paramString2.indexOf('/');
            if (j > 0)
            {
                if ((i == j + 2) && (paramString2.charAt(j + 1) == '*'))
                    if (paramString2.regionMatches(0, paramString1, 0, j + 1))
                        continue;
            }
            else
                while (!paramString2.equals(paramString1))
                {
                    bool = false;
                    break;
                }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String[] filterMimeTypes(String paramString)
    {
        ArrayList localArrayList = null;
        for (int i = 0; i < this.mMimeTypes.length; i++)
            if (compareMimeTypes(this.mMimeTypes[i], paramString))
            {
                if (localArrayList == null)
                    localArrayList = new ArrayList();
                localArrayList.add(this.mMimeTypes[i]);
            }
        String[] arrayOfString;
        if (localArrayList == null)
            arrayOfString = null;
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[localArrayList.size()];
            localArrayList.toArray(arrayOfString);
        }
    }

    public CharSequence getLabel()
    {
        return this.mLabel;
    }

    public String getMimeType(int paramInt)
    {
        return this.mMimeTypes[paramInt];
    }

    public int getMimeTypeCount()
    {
        return this.mMimeTypes.length;
    }

    public boolean hasMimeType(String paramString)
    {
        int i = 0;
        if (i < this.mMimeTypes.length)
            if (!compareMimeTypes(this.mMimeTypes[i], paramString));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    public boolean toShortString(StringBuilder paramStringBuilder)
    {
        int i = 1;
        for (int j = 0; j < this.mMimeTypes.length; j++)
        {
            if (i == 0)
                paramStringBuilder.append(' ');
            i = 0;
            paramStringBuilder.append(this.mMimeTypes[j]);
        }
        if (this.mLabel != null)
        {
            if (i == 0)
                paramStringBuilder.append(' ');
            i = 0;
            paramStringBuilder.append('"');
            paramStringBuilder.append(this.mLabel);
            paramStringBuilder.append('"');
        }
        if (i == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("ClipDescription { ");
        toShortString(localStringBuilder);
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    public void validate()
    {
        if (this.mMimeTypes == null)
            throw new NullPointerException("null mime types");
        if (this.mMimeTypes.length <= 0)
            throw new IllegalArgumentException("must have at least 1 mime type");
        for (int i = 0; i < this.mMimeTypes.length; i++)
            if (this.mMimeTypes[i] == null)
                throw new NullPointerException("mime type at " + i + " is null");
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        TextUtils.writeToParcel(this.mLabel, paramParcel, paramInt);
        paramParcel.writeStringArray(this.mMimeTypes);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ClipDescription
 * JD-Core Version:        0.6.2
 */