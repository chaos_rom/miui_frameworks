package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class ComponentName
    implements Parcelable, Cloneable, Comparable<ComponentName>
{
    public static final Parcelable.Creator<ComponentName> CREATOR = new Parcelable.Creator()
    {
        public ComponentName createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ComponentName(paramAnonymousParcel);
        }

        public ComponentName[] newArray(int paramAnonymousInt)
        {
            return new ComponentName[paramAnonymousInt];
        }
    };
    private final String mClass;
    private final String mPackage;

    public ComponentName(Context paramContext, Class<?> paramClass)
    {
        this.mPackage = paramContext.getPackageName();
        this.mClass = paramClass.getName();
    }

    public ComponentName(Context paramContext, String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("class name is null");
        this.mPackage = paramContext.getPackageName();
        this.mClass = paramString;
    }

    public ComponentName(Parcel paramParcel)
    {
        this.mPackage = paramParcel.readString();
        if (this.mPackage == null)
            throw new NullPointerException("package name is null");
        this.mClass = paramParcel.readString();
        if (this.mClass == null)
            throw new NullPointerException("class name is null");
    }

    private ComponentName(String paramString, Parcel paramParcel)
    {
        this.mPackage = paramString;
        this.mClass = paramParcel.readString();
    }

    public ComponentName(String paramString1, String paramString2)
    {
        if (paramString1 == null)
            throw new NullPointerException("package name is null");
        if (paramString2 == null)
            throw new NullPointerException("class name is null");
        this.mPackage = paramString1;
        this.mClass = paramString2;
    }

    public static ComponentName readFromParcel(Parcel paramParcel)
    {
        String str = paramParcel.readString();
        if (str != null);
        for (ComponentName localComponentName = new ComponentName(str, paramParcel); ; localComponentName = null)
            return localComponentName;
    }

    public static ComponentName unflattenFromString(String paramString)
    {
        int i = paramString.indexOf('/');
        if ((i < 0) || (i + 1 >= paramString.length()));
        String str1;
        String str2;
        for (ComponentName localComponentName = null; ; localComponentName = new ComponentName(str1, str2))
        {
            return localComponentName;
            str1 = paramString.substring(0, i);
            str2 = paramString.substring(i + 1);
            if ((str2.length() > 0) && (str2.charAt(0) == '.'))
                str2 = str1 + str2;
        }
    }

    public static void writeToParcel(ComponentName paramComponentName, Parcel paramParcel)
    {
        if (paramComponentName != null)
            paramComponentName.writeToParcel(paramParcel, 0);
        while (true)
        {
            return;
            paramParcel.writeString(null);
        }
    }

    public ComponentName clone()
    {
        return new ComponentName(this.mPackage, this.mClass);
    }

    public int compareTo(ComponentName paramComponentName)
    {
        int i = this.mPackage.compareTo(paramComponentName.mPackage);
        if (i != 0);
        while (true)
        {
            return i;
            i = this.mClass.compareTo(paramComponentName.mClass);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool1 = false;
        if (paramObject != null);
        try
        {
            ComponentName localComponentName = (ComponentName)paramObject;
            if (this.mPackage.equals(localComponentName.mPackage))
            {
                boolean bool2 = this.mClass.equals(localComponentName.mClass);
                if (bool2)
                    bool1 = true;
            }
            label48: return bool1;
        }
        catch (ClassCastException localClassCastException)
        {
            break label48;
        }
    }

    public String flattenToShortString()
    {
        return this.mPackage + "/" + getShortClassName();
    }

    public String flattenToString()
    {
        return this.mPackage + "/" + this.mClass;
    }

    public String getClassName()
    {
        return this.mClass;
    }

    public String getPackageName()
    {
        return this.mPackage;
    }

    public String getShortClassName()
    {
        int i;
        int j;
        if (this.mClass.startsWith(this.mPackage))
        {
            i = this.mPackage.length();
            j = this.mClass.length();
            if ((j <= i) || (this.mClass.charAt(i) != '.'));
        }
        for (String str = this.mClass.substring(i, j); ; str = this.mClass)
            return str;
    }

    public int hashCode()
    {
        return this.mPackage.hashCode() + this.mClass.hashCode();
    }

    public String toShortString()
    {
        return "{" + this.mPackage + "/" + this.mClass + "}";
    }

    public String toString()
    {
        return "ComponentInfo{" + this.mPackage + "/" + this.mClass + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mPackage);
        paramParcel.writeString(this.mClass);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ComponentName
 * JD-Core Version:        0.6.2
 */