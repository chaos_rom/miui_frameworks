package android.content;

import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.lang.ref.WeakReference;

public abstract class AsyncQueryHandler extends Handler
{
    private static final int EVENT_ARG_DELETE = 4;
    private static final int EVENT_ARG_INSERT = 2;
    private static final int EVENT_ARG_QUERY = 1;
    private static final int EVENT_ARG_UPDATE = 3;
    private static final String TAG = "AsyncQuery";
    private static final boolean localLOGV;
    private static Looper sLooper = null;
    final WeakReference<ContentResolver> mResolver;
    private Handler mWorkerThreadHandler;

    public AsyncQueryHandler(ContentResolver paramContentResolver)
    {
        this.mResolver = new WeakReference(paramContentResolver);
        try
        {
            if (sLooper == null)
            {
                HandlerThread localHandlerThread = new HandlerThread("AsyncQueryWorker");
                localHandlerThread.start();
                sLooper = localHandlerThread.getLooper();
            }
            this.mWorkerThreadHandler = createHandler(sLooper);
            return;
        }
        finally
        {
        }
    }

    public final void cancelOperation(int paramInt)
    {
        this.mWorkerThreadHandler.removeMessages(paramInt);
    }

    protected Handler createHandler(Looper paramLooper)
    {
        return new WorkerHandler(paramLooper);
    }

    public void handleMessage(Message paramMessage)
    {
        WorkerArgs localWorkerArgs = (WorkerArgs)paramMessage.obj;
        int i = paramMessage.what;
        switch (paramMessage.arg1)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return;
            onQueryComplete(i, localWorkerArgs.cookie, (Cursor)localWorkerArgs.result);
            continue;
            onInsertComplete(i, localWorkerArgs.cookie, (Uri)localWorkerArgs.result);
            continue;
            onUpdateComplete(i, localWorkerArgs.cookie, ((Integer)localWorkerArgs.result).intValue());
            continue;
            onDeleteComplete(i, localWorkerArgs.cookie, ((Integer)localWorkerArgs.result).intValue());
        }
    }

    protected void onDeleteComplete(int paramInt1, Object paramObject, int paramInt2)
    {
    }

    protected void onInsertComplete(int paramInt, Object paramObject, Uri paramUri)
    {
    }

    protected void onQueryComplete(int paramInt, Object paramObject, Cursor paramCursor)
    {
    }

    protected void onUpdateComplete(int paramInt1, Object paramObject, int paramInt2)
    {
    }

    public final void startDelete(int paramInt, Object paramObject, Uri paramUri, String paramString, String[] paramArrayOfString)
    {
        Message localMessage = this.mWorkerThreadHandler.obtainMessage(paramInt);
        localMessage.arg1 = 4;
        WorkerArgs localWorkerArgs = new WorkerArgs();
        localWorkerArgs.handler = this;
        localWorkerArgs.uri = paramUri;
        localWorkerArgs.cookie = paramObject;
        localWorkerArgs.selection = paramString;
        localWorkerArgs.selectionArgs = paramArrayOfString;
        localMessage.obj = localWorkerArgs;
        this.mWorkerThreadHandler.sendMessage(localMessage);
    }

    public final void startInsert(int paramInt, Object paramObject, Uri paramUri, ContentValues paramContentValues)
    {
        Message localMessage = this.mWorkerThreadHandler.obtainMessage(paramInt);
        localMessage.arg1 = 2;
        WorkerArgs localWorkerArgs = new WorkerArgs();
        localWorkerArgs.handler = this;
        localWorkerArgs.uri = paramUri;
        localWorkerArgs.cookie = paramObject;
        localWorkerArgs.values = paramContentValues;
        localMessage.obj = localWorkerArgs;
        this.mWorkerThreadHandler.sendMessage(localMessage);
    }

    public void startQuery(int paramInt, Object paramObject, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        Message localMessage = this.mWorkerThreadHandler.obtainMessage(paramInt);
        localMessage.arg1 = 1;
        WorkerArgs localWorkerArgs = new WorkerArgs();
        localWorkerArgs.handler = this;
        localWorkerArgs.uri = paramUri;
        localWorkerArgs.projection = paramArrayOfString1;
        localWorkerArgs.selection = paramString1;
        localWorkerArgs.selectionArgs = paramArrayOfString2;
        localWorkerArgs.orderBy = paramString2;
        localWorkerArgs.cookie = paramObject;
        localMessage.obj = localWorkerArgs;
        this.mWorkerThreadHandler.sendMessage(localMessage);
    }

    public final void startUpdate(int paramInt, Object paramObject, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
    {
        Message localMessage = this.mWorkerThreadHandler.obtainMessage(paramInt);
        localMessage.arg1 = 3;
        WorkerArgs localWorkerArgs = new WorkerArgs();
        localWorkerArgs.handler = this;
        localWorkerArgs.uri = paramUri;
        localWorkerArgs.cookie = paramObject;
        localWorkerArgs.values = paramContentValues;
        localWorkerArgs.selection = paramString;
        localWorkerArgs.selectionArgs = paramArrayOfString;
        localMessage.obj = localWorkerArgs;
        this.mWorkerThreadHandler.sendMessage(localMessage);
    }

    protected class WorkerHandler extends Handler
    {
        public WorkerHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            ContentResolver localContentResolver = (ContentResolver)AsyncQueryHandler.this.mResolver.get();
            if (localContentResolver == null)
                return;
            AsyncQueryHandler.WorkerArgs localWorkerArgs = (AsyncQueryHandler.WorkerArgs)paramMessage.obj;
            int i = paramMessage.what;
            switch (paramMessage.arg1)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                Message localMessage = localWorkerArgs.handler.obtainMessage(i);
                localMessage.obj = localWorkerArgs;
                localMessage.arg1 = paramMessage.arg1;
                localMessage.sendToTarget();
                break;
                try
                {
                    localCursor = localContentResolver.query(localWorkerArgs.uri, localWorkerArgs.projection, localWorkerArgs.selection, localWorkerArgs.selectionArgs, localWorkerArgs.orderBy);
                    if (localCursor != null)
                        localCursor.getCount();
                    localWorkerArgs.result = localCursor;
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        Log.w("AsyncQuery", "Exception thrown during handling EVENT_ARG_QUERY", localException);
                        Cursor localCursor = null;
                    }
                }
                localWorkerArgs.result = localContentResolver.insert(localWorkerArgs.uri, localWorkerArgs.values);
                continue;
                localWorkerArgs.result = Integer.valueOf(localContentResolver.update(localWorkerArgs.uri, localWorkerArgs.values, localWorkerArgs.selection, localWorkerArgs.selectionArgs));
                continue;
                localWorkerArgs.result = Integer.valueOf(localContentResolver.delete(localWorkerArgs.uri, localWorkerArgs.selection, localWorkerArgs.selectionArgs));
            }
        }
    }

    protected static final class WorkerArgs
    {
        public Object cookie;
        public Handler handler;
        public String orderBy;
        public String[] projection;
        public Object result;
        public String selection;
        public String[] selectionArgs;
        public Uri uri;
        public ContentValues values;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.AsyncQueryHandler
 * JD-Core Version:        0.6.2
 */