package android.content;

public class MutableContextWrapper extends ContextWrapper
{
    public MutableContextWrapper(Context paramContext)
    {
        super(paramContext);
    }

    public void setBaseContext(Context paramContext)
    {
        this.mBase = paramContext;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.MutableContextWrapper
 * JD-Core Version:        0.6.2
 */