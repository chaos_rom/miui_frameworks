package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.ICancellationSignal.Stub;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

final class ContentProviderProxy
    implements IContentProvider
{
    private IBinder mRemote;

    public ContentProviderProxy(IBinder paramIBinder)
    {
        this.mRemote = paramIBinder;
    }

    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> paramArrayList)
        throws RemoteException, OperationApplicationException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            localParcel1.writeInt(paramArrayList.size());
            Iterator localIterator = paramArrayList.iterator();
            while (localIterator.hasNext())
                ((ContentProviderOperation)localIterator.next()).writeToParcel(localParcel1, 0);
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
        this.mRemote.transact(20, localParcel1, localParcel2, 0);
        DatabaseUtils.readExceptionWithOperationApplicationExceptionFromParcel(localParcel2);
        ContentProviderResult[] arrayOfContentProviderResult = (ContentProviderResult[])localParcel2.createTypedArray(ContentProviderResult.CREATOR);
        localParcel1.recycle();
        localParcel2.recycle();
        return arrayOfContentProviderResult;
    }

    public IBinder asBinder()
    {
        return this.mRemote;
    }

    public int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            localParcel1.writeTypedArray(paramArrayOfContentValues, 0);
            this.mRemote.transact(13, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            int i = localParcel2.readInt();
            return i;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public Bundle call(String paramString1, String paramString2, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            localParcel1.writeString(paramString1);
            localParcel1.writeString(paramString2);
            localParcel1.writeBundle(paramBundle);
            this.mRemote.transact(21, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            Bundle localBundle = localParcel2.readBundle();
            return localBundle;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public ICancellationSignal createCancellationSignal()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            this.mRemote.transact(24, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            ICancellationSignal localICancellationSignal = ICancellationSignal.Stub.asInterface(localParcel2.readStrongBinder());
            return localICancellationSignal;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            localParcel1.writeStringArray(paramArrayOfString);
            this.mRemote.transact(4, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            int i = localParcel2.readInt();
            return i;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public String[] getStreamTypes(Uri paramUri, String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            this.mRemote.transact(22, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            String[] arrayOfString = localParcel2.createStringArray();
            return arrayOfString;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public String getType(Uri paramUri)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            this.mRemote.transact(2, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            String str = localParcel2.readString();
            return str;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public Uri insert(Uri paramUri, ContentValues paramContentValues)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            paramContentValues.writeToParcel(localParcel1, 0);
            this.mRemote.transact(3, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            Uri localUri = (Uri)Uri.CREATOR.createFromParcel(localParcel2);
            return localUri;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public AssetFileDescriptor openAssetFile(Uri paramUri, String paramString)
        throws RemoteException, FileNotFoundException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            this.mRemote.transact(15, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionWithFileNotFoundExceptionFromParcel(localParcel2);
            if (localParcel2.readInt() != 0)
            {
                AssetFileDescriptor localAssetFileDescriptor2 = (AssetFileDescriptor)AssetFileDescriptor.CREATOR.createFromParcel(localParcel2);
                localAssetFileDescriptor1 = localAssetFileDescriptor2;
                return localAssetFileDescriptor1;
            }
            AssetFileDescriptor localAssetFileDescriptor1 = null;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
        throws RemoteException, FileNotFoundException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            this.mRemote.transact(14, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionWithFileNotFoundExceptionFromParcel(localParcel2);
            if (localParcel2.readInt() != 0)
            {
                ParcelFileDescriptor localParcelFileDescriptor2 = localParcel2.readFileDescriptor();
                localParcelFileDescriptor1 = localParcelFileDescriptor2;
                return localParcelFileDescriptor1;
            }
            ParcelFileDescriptor localParcelFileDescriptor1 = null;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public AssetFileDescriptor openTypedAssetFile(Uri paramUri, String paramString, Bundle paramBundle)
        throws RemoteException, FileNotFoundException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            localParcel1.writeBundle(paramBundle);
            this.mRemote.transact(23, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionWithFileNotFoundExceptionFromParcel(localParcel2);
            if (localParcel2.readInt() != 0)
            {
                AssetFileDescriptor localAssetFileDescriptor2 = (AssetFileDescriptor)AssetFileDescriptor.CREATOR.createFromParcel(localParcel2);
                localAssetFileDescriptor1 = localAssetFileDescriptor2;
                return localAssetFileDescriptor1;
            }
            AssetFileDescriptor localAssetFileDescriptor1 = null;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    // ERROR //
    public android.database.Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, ICancellationSignal paramICancellationSignal)
        throws RemoteException
    {
        // Byte code:
        //     0: new 185	android/database/BulkCursorToCursorAdaptor
        //     3: dup
        //     4: invokespecial 186	android/database/BulkCursorToCursorAdaptor:<init>	()V
        //     7: astore 7
        //     9: invokestatic 27	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     12: astore 8
        //     14: invokestatic 27	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     17: astore 9
        //     19: aload 8
        //     21: ldc 29
        //     23: invokevirtual 33	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     26: aload_1
        //     27: aload 8
        //     29: iconst_0
        //     30: invokevirtual 97	android/net/Uri:writeToParcel	(Landroid/os/Parcel;I)V
        //     33: iconst_0
        //     34: istore 13
        //     36: aload_2
        //     37: ifnull +7 -> 44
        //     40: aload_2
        //     41: arraylength
        //     42: istore 13
        //     44: aload 8
        //     46: iload 13
        //     48: invokevirtual 43	android/os/Parcel:writeInt	(I)V
        //     51: iconst_0
        //     52: istore 14
        //     54: iload 14
        //     56: iload 13
        //     58: if_icmpge +18 -> 76
        //     61: aload 8
        //     63: aload_2
        //     64: iload 14
        //     66: aaload
        //     67: invokevirtual 112	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     70: iinc 14 1
        //     73: goto -19 -> 54
        //     76: aload 8
        //     78: aload_3
        //     79: invokevirtual 112	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     82: aload 4
        //     84: ifnull +196 -> 280
        //     87: aload 4
        //     89: arraylength
        //     90: istore 15
        //     92: aload 8
        //     94: iload 15
        //     96: invokevirtual 43	android/os/Parcel:writeInt	(I)V
        //     99: iconst_0
        //     100: istore 16
        //     102: iload 16
        //     104: iload 15
        //     106: if_icmpge +19 -> 125
        //     109: aload 8
        //     111: aload 4
        //     113: iload 16
        //     115: aaload
        //     116: invokevirtual 112	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     119: iinc 16 1
        //     122: goto -20 -> 102
        //     125: aload 8
        //     127: aload 5
        //     129: invokevirtual 112	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     132: aload 8
        //     134: aload 7
        //     136: invokevirtual 190	android/database/BulkCursorToCursorAdaptor:getObserver	()Landroid/database/IContentObserver;
        //     139: invokeinterface 194 1 0
        //     144: invokevirtual 197	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     147: aload 6
        //     149: ifnull +79 -> 228
        //     152: aload 6
        //     154: invokeinterface 200 1 0
        //     159: astore 17
        //     161: aload 8
        //     163: aload 17
        //     165: invokevirtual 197	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     168: aload_0
        //     169: getfield 15	android/content/ContentProviderProxy:mRemote	Landroid/os/IBinder;
        //     172: iconst_1
        //     173: aload 8
        //     175: aload 9
        //     177: iconst_0
        //     178: invokeinterface 72 5 0
        //     183: pop
        //     184: aload 9
        //     186: invokestatic 104	android/database/DatabaseUtils:readExceptionFromParcel	(Landroid/os/Parcel;)V
        //     189: aload 9
        //     191: invokevirtual 107	android/os/Parcel:readInt	()I
        //     194: ifeq +40 -> 234
        //     197: aload 7
        //     199: getstatic 203	android/database/BulkCursorDescriptor:CREATOR	Landroid/os/Parcelable$Creator;
        //     202: aload 9
        //     204: invokeinterface 161 2 0
        //     209: checkcast 202	android/database/BulkCursorDescriptor
        //     212: invokevirtual 207	android/database/BulkCursorToCursorAdaptor:initialize	(Landroid/database/BulkCursorDescriptor;)V
        //     215: aload 8
        //     217: invokevirtual 66	android/os/Parcel:recycle	()V
        //     220: aload 9
        //     222: invokevirtual 66	android/os/Parcel:recycle	()V
        //     225: aload 7
        //     227: areturn
        //     228: aconst_null
        //     229: astore 17
        //     231: goto -70 -> 161
        //     234: aload 7
        //     236: invokevirtual 210	android/database/BulkCursorToCursorAdaptor:close	()V
        //     239: aconst_null
        //     240: astore 7
        //     242: goto -27 -> 215
        //     245: astore 12
        //     247: aload 7
        //     249: invokevirtual 210	android/database/BulkCursorToCursorAdaptor:close	()V
        //     252: aload 12
        //     254: athrow
        //     255: astore 11
        //     257: aload 8
        //     259: invokevirtual 66	android/os/Parcel:recycle	()V
        //     262: aload 9
        //     264: invokevirtual 66	android/os/Parcel:recycle	()V
        //     267: aload 11
        //     269: athrow
        //     270: astore 10
        //     272: aload 7
        //     274: invokevirtual 210	android/database/BulkCursorToCursorAdaptor:close	()V
        //     277: aload 10
        //     279: athrow
        //     280: iconst_0
        //     281: istore 15
        //     283: goto -191 -> 92
        //
        // Exception table:
        //     from	to	target	type
        //     19	215	245	android/os/RemoteException
        //     234	239	245	android/os/RemoteException
        //     19	215	255	finally
        //     234	239	255	finally
        //     247	255	255	finally
        //     272	280	255	finally
        //     19	215	270	java/lang/RuntimeException
        //     234	239	270	java/lang/RuntimeException
    }

    public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.content.IContentProvider");
            paramUri.writeToParcel(localParcel1, 0);
            paramContentValues.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            localParcel1.writeStringArray(paramArrayOfString);
            this.mRemote.transact(10, localParcel1, localParcel2, 0);
            DatabaseUtils.readExceptionFromParcel(localParcel2);
            int i = localParcel2.readInt();
            return i;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentProviderProxy
 * JD-Core Version:        0.6.2
 */