package android.content;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IIntentSender extends IInterface
{
    public abstract int send(int paramInt, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, String paramString2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IIntentSender
    {
        private static final String DESCRIPTOR = "android.content.IIntentSender";
        static final int TRANSACTION_send = 1;

        public Stub()
        {
            attachInterface(this, "android.content.IIntentSender");
        }

        public static IIntentSender asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.IIntentSender");
                if ((localIInterface != null) && ((localIInterface instanceof IIntentSender)))
                    localObject = (IIntentSender)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
            case 1598968902:
                for (bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2); ; bool = true)
                {
                    return bool;
                    paramParcel2.writeString("android.content.IIntentSender");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.content.IIntentSender");
            int i = paramParcel1.readInt();
            if (paramParcel1.readInt() != 0);
            for (Intent localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent = null)
            {
                int j = send(i, localIntent, paramParcel1.readString(), IIntentReceiver.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                bool = true;
                break;
            }
        }

        private static class Proxy
            implements IIntentSender
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.IIntentSender";
            }

            public int send(int paramInt, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IIntentSender");
                        localParcel1.writeInt(paramInt);
                        if (paramIntent != null)
                        {
                            localParcel1.writeInt(1);
                            paramIntent.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString1);
                            if (paramIIntentReceiver != null)
                            {
                                localIBinder = paramIIntentReceiver.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                localParcel1.writeString(paramString2);
                                this.mRemote.transact(1, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                return i;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IIntentSender
 * JD-Core Version:        0.6.2
 */