package android.content;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;

public class SyncContext
{
    private static final long HEARTBEAT_SEND_INTERVAL_IN_MS = 1000L;
    private long mLastHeartbeatSendTime;
    private ISyncContext mSyncContext;

    public SyncContext(ISyncContext paramISyncContext)
    {
        this.mSyncContext = paramISyncContext;
        this.mLastHeartbeatSendTime = 0L;
    }

    private void updateHeartbeat()
    {
        long l = SystemClock.elapsedRealtime();
        if (l < 1000L + this.mLastHeartbeatSendTime);
        while (true)
        {
            return;
            try
            {
                this.mLastHeartbeatSendTime = l;
                if (this.mSyncContext != null)
                    this.mSyncContext.sendHeartbeat();
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public IBinder getSyncContextBinder()
    {
        if (this.mSyncContext == null);
        for (IBinder localIBinder = null; ; localIBinder = this.mSyncContext.asBinder())
            return localIBinder;
    }

    public void onFinished(SyncResult paramSyncResult)
    {
        try
        {
            if (this.mSyncContext != null)
                this.mSyncContext.onFinished(paramSyncResult);
            label17: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label17;
        }
    }

    public void setStatusText(String paramString)
    {
        updateHeartbeat();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncContext
 * JD-Core Version:        0.6.2
 */