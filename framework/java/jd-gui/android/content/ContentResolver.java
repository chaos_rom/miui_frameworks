package android.content;

import android.accounts.Account;
import android.app.ActivityManagerNative;
import android.app.AppGlobals;
import android.app.IActivityManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.ContentObserver;
import android.database.CrossProcessCursorWrapper;
import android.database.Cursor;
import android.database.IContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.DeadObjectException;
import android.os.ICancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import dalvik.system.CloseGuard;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public abstract class ContentResolver
{
    public static final String CONTENT_SERVICE_NAME = "content";
    public static final String CURSOR_DIR_BASE_TYPE = "vnd.android.cursor.dir";
    public static final String CURSOR_ITEM_BASE_TYPE = "vnd.android.cursor.item";
    public static final String SCHEME_ANDROID_RESOURCE = "android.resource";
    public static final String SCHEME_CONTENT = "content";
    public static final String SCHEME_FILE = "file";
    private static final int SLOW_THRESHOLD_MILLIS = 500;
    public static final int SYNC_ERROR_AUTHENTICATION = 2;
    public static final int SYNC_ERROR_CONFLICT = 5;
    public static final int SYNC_ERROR_INTERNAL = 8;
    public static final int SYNC_ERROR_IO = 3;
    public static final int SYNC_ERROR_PARSE = 4;
    public static final int SYNC_ERROR_SYNC_ALREADY_IN_PROGRESS = 1;
    public static final int SYNC_ERROR_TOO_MANY_DELETIONS = 6;
    public static final int SYNC_ERROR_TOO_MANY_RETRIES = 7;

    @Deprecated
    public static final String SYNC_EXTRAS_ACCOUNT = "account";
    public static final String SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS = "discard_deletions";
    public static final String SYNC_EXTRAS_DO_NOT_RETRY = "do_not_retry";
    public static final String SYNC_EXTRAS_EXPEDITED = "expedited";

    @Deprecated
    public static final String SYNC_EXTRAS_FORCE = "force";
    public static final String SYNC_EXTRAS_IGNORE_BACKOFF = "ignore_backoff";
    public static final String SYNC_EXTRAS_IGNORE_SETTINGS = "ignore_settings";
    public static final String SYNC_EXTRAS_INITIALIZE = "initialize";
    public static final String SYNC_EXTRAS_MANUAL = "force";
    public static final String SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS = "deletions_override";
    public static final String SYNC_EXTRAS_UPLOAD = "upload";
    public static final int SYNC_OBSERVER_TYPE_ACTIVE = 4;
    public static final int SYNC_OBSERVER_TYPE_ALL = 2147483647;
    public static final int SYNC_OBSERVER_TYPE_PENDING = 2;
    public static final int SYNC_OBSERVER_TYPE_SETTINGS = 1;
    public static final int SYNC_OBSERVER_TYPE_STATUS = 8;
    private static final String TAG = "ContentResolver";
    private static IContentService sContentService;
    private final Context mContext;
    private final Random mRandom = new Random();

    public ContentResolver(Context paramContext)
    {
        this.mContext = paramContext;
    }

    public static void addPeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong)
    {
        validateSyncExtrasBundle(paramBundle);
        if (paramAccount == null)
            throw new IllegalArgumentException("account must not be null");
        if (paramString == null)
            throw new IllegalArgumentException("authority must not be null");
        if ((paramBundle.getBoolean("force", false)) || (paramBundle.getBoolean("do_not_retry", false)) || (paramBundle.getBoolean("ignore_backoff", false)) || (paramBundle.getBoolean("ignore_settings", false)) || (paramBundle.getBoolean("initialize", false)) || (paramBundle.getBoolean("force", false)) || (paramBundle.getBoolean("expedited", false)))
            throw new IllegalArgumentException("illegal extras were set");
        try
        {
            getContentService().addPeriodicSync(paramAccount, paramString, paramBundle, paramLong);
            label124: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label124;
        }
    }

    public static Object addStatusChangeListener(int paramInt, SyncStatusObserver paramSyncStatusObserver)
    {
        if (paramSyncStatusObserver == null)
            throw new IllegalArgumentException("you passed in a null callback");
        try
        {
            ISyncStatusObserver.Stub local1 = new ISyncStatusObserver.Stub()
            {
                public void onStatusChanged(int paramAnonymousInt)
                    throws RemoteException
                {
                    ContentResolver.this.onStatusChanged(paramAnonymousInt);
                }
            };
            getContentService().addStatusChangeListener(paramInt, local1);
            return local1;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static void cancelSync(Account paramAccount, String paramString)
    {
        try
        {
            getContentService().cancelSync(paramAccount, paramString);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public static IContentService getContentService()
    {
        if (sContentService != null);
        for (IContentService localIContentService = sContentService; ; localIContentService = sContentService)
        {
            return localIContentService;
            sContentService = IContentService.Stub.asInterface(ServiceManager.getService("content"));
        }
    }

    @Deprecated
    public static SyncInfo getCurrentSync()
    {
        SyncInfo localSyncInfo;
        try
        {
            List localList = getContentService().getCurrentSyncs();
            if (localList.isEmpty())
                localSyncInfo = null;
            else
                localSyncInfo = (SyncInfo)localList.get(0);
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
        return localSyncInfo;
    }

    public static List<SyncInfo> getCurrentSyncs()
    {
        try
        {
            List localList = getContentService().getCurrentSyncs();
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static int getIsSyncable(Account paramAccount, String paramString)
    {
        try
        {
            int i = getContentService().getIsSyncable(paramAccount, paramString);
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static boolean getMasterSyncAutomatically()
    {
        try
        {
            boolean bool = getContentService().getMasterSyncAutomatically();
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static List<PeriodicSync> getPeriodicSyncs(Account paramAccount, String paramString)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account must not be null");
        if (paramString == null)
            throw new IllegalArgumentException("authority must not be null");
        try
        {
            List localList = getContentService().getPeriodicSyncs(paramAccount, paramString);
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static SyncAdapterType[] getSyncAdapterTypes()
    {
        try
        {
            SyncAdapterType[] arrayOfSyncAdapterType = getContentService().getSyncAdapterTypes();
            return arrayOfSyncAdapterType;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static boolean getSyncAutomatically(Account paramAccount, String paramString)
    {
        try
        {
            boolean bool = getContentService().getSyncAutomatically(paramAccount, paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static SyncStatusInfo getSyncStatus(Account paramAccount, String paramString)
    {
        try
        {
            SyncStatusInfo localSyncStatusInfo = getContentService().getSyncStatus(paramAccount, paramString);
            return localSyncStatusInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static boolean isSyncActive(Account paramAccount, String paramString)
    {
        try
        {
            boolean bool = getContentService().isSyncActive(paramAccount, paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static boolean isSyncPending(Account paramAccount, String paramString)
    {
        try
        {
            boolean bool = getContentService().isSyncPending(paramAccount, paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    private void maybeLogQueryToEventLog(long paramLong, Uri paramUri, String[] paramArrayOfString, String paramString1, String paramString2)
    {
        int i = samplePercentForDuration(paramLong);
        if (i < 100);
        StringBuilder localStringBuilder;
        synchronized (this.mRandom)
        {
            if (this.mRandom.nextInt(100) >= i)
                return;
            localStringBuilder = new StringBuilder(100);
            if (paramArrayOfString != null)
            {
                int j = 0;
                if (j < paramArrayOfString.length)
                {
                    if (j != 0)
                        localStringBuilder.append('/');
                    localStringBuilder.append(paramArrayOfString[j]);
                    j++;
                }
            }
        }
        String str = AppGlobals.getInitialPackage();
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = paramUri.toString();
        arrayOfObject[1] = localStringBuilder.toString();
        if (paramString1 != null)
        {
            arrayOfObject[2] = paramString1;
            if (paramString2 == null)
                break label211;
            label156: arrayOfObject[3] = paramString2;
            arrayOfObject[4] = Long.valueOf(paramLong);
            if (str == null)
                break label219;
        }
        while (true)
        {
            arrayOfObject[5] = str;
            arrayOfObject[6] = Integer.valueOf(i);
            EventLog.writeEvent(52002, arrayOfObject);
            return;
            paramString1 = "";
            break;
            label211: paramString2 = "";
            break label156;
            label219: str = "";
        }
    }

    private void maybeLogUpdateToEventLog(long paramLong, Uri paramUri, String paramString1, String paramString2)
    {
        int i = samplePercentForDuration(paramLong);
        if (i < 100);
        while (true)
        {
            synchronized (this.mRandom)
            {
                if (this.mRandom.nextInt(100) >= i)
                    break;
                str = AppGlobals.getInitialPackage();
                Object[] arrayOfObject = new Object[6];
                arrayOfObject[0] = paramUri.toString();
                arrayOfObject[1] = paramString1;
                if (paramString2 != null)
                {
                    arrayOfObject[2] = paramString2;
                    arrayOfObject[3] = Long.valueOf(paramLong);
                    if (str == null)
                        break label139;
                    arrayOfObject[4] = str;
                    arrayOfObject[5] = Integer.valueOf(i);
                    EventLog.writeEvent(52003, arrayOfObject);
                }
            }
            paramString2 = "";
            continue;
            label139: String str = "";
        }
    }

    public static int modeToMode(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        int i;
        if ("r".equals(paramString))
            i = 268435456;
        while (true)
        {
            return i;
            if (("w".equals(paramString)) || ("wt".equals(paramString)))
            {
                i = 738197504;
            }
            else if ("wa".equals(paramString))
            {
                i = 704643072;
            }
            else if ("rw".equals(paramString))
            {
                i = 939524096;
            }
            else
            {
                if (!"rwt".equals(paramString))
                    break;
                i = 1006632960;
            }
        }
        throw new FileNotFoundException("Bad mode for " + paramUri + ": " + paramString);
    }

    public static void removePeriodicSync(Account paramAccount, String paramString, Bundle paramBundle)
    {
        validateSyncExtrasBundle(paramBundle);
        if (paramAccount == null)
            throw new IllegalArgumentException("account must not be null");
        if (paramString == null)
            throw new IllegalArgumentException("authority must not be null");
        try
        {
            getContentService().removePeriodicSync(paramAccount, paramString, paramBundle);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("the ContentService should always be reachable", localRemoteException);
        }
    }

    public static void removeStatusChangeListener(Object paramObject)
    {
        if (paramObject == null)
            throw new IllegalArgumentException("you passed in a null handle");
        try
        {
            getContentService().removeStatusChangeListener((ISyncStatusObserver.Stub)paramObject);
            label27: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label27;
        }
    }

    public static void requestSync(Account paramAccount, String paramString, Bundle paramBundle)
    {
        validateSyncExtrasBundle(paramBundle);
        try
        {
            getContentService().requestSync(paramAccount, paramString, paramBundle);
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    private int samplePercentForDuration(long paramLong)
    {
        if (paramLong >= 500L);
        for (int i = 100; ; i = 1 + (int)(100L * paramLong / 500L))
            return i;
    }

    public static void setIsSyncable(Account paramAccount, String paramString, int paramInt)
    {
        try
        {
            getContentService().setIsSyncable(paramAccount, paramString, paramInt);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public static void setMasterSyncAutomatically(boolean paramBoolean)
    {
        try
        {
            getContentService().setMasterSyncAutomatically(paramBoolean);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    public static void setSyncAutomatically(Account paramAccount, String paramString, boolean paramBoolean)
    {
        try
        {
            getContentService().setSyncAutomatically(paramAccount, paramString, paramBoolean);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public static void validateSyncExtrasBundle(Bundle paramBundle)
    {
        try
        {
            Iterator localIterator = paramBundle.keySet().iterator();
            while (localIterator.hasNext())
            {
                Object localObject = paramBundle.get((String)localIterator.next());
                if ((localObject != null) && (!(localObject instanceof Long)) && (!(localObject instanceof Integer)) && (!(localObject instanceof Boolean)) && (!(localObject instanceof Float)) && (!(localObject instanceof Double)) && (!(localObject instanceof String)) && (!(localObject instanceof Account)))
                    throw new IllegalArgumentException("unexpected value type: " + localObject.getClass().getName());
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (RuntimeException localRuntimeException)
        {
            throw new IllegalArgumentException("error unparceling Bundle", localRuntimeException);
        }
    }

    public final ContentProviderClient acquireContentProviderClient(Uri paramUri)
    {
        IContentProvider localIContentProvider = acquireProvider(paramUri);
        if (localIContentProvider != null);
        for (ContentProviderClient localContentProviderClient = new ContentProviderClient(this, localIContentProvider, true); ; localContentProviderClient = null)
            return localContentProviderClient;
    }

    public final ContentProviderClient acquireContentProviderClient(String paramString)
    {
        IContentProvider localIContentProvider = acquireProvider(paramString);
        if (localIContentProvider != null);
        for (ContentProviderClient localContentProviderClient = new ContentProviderClient(this, localIContentProvider, true); ; localContentProviderClient = null)
            return localContentProviderClient;
    }

    protected IContentProvider acquireExistingProvider(Context paramContext, String paramString)
    {
        return acquireProvider(paramContext, paramString);
    }

    public final IContentProvider acquireExistingProvider(Uri paramUri)
    {
        IContentProvider localIContentProvider = null;
        if (!"content".equals(paramUri.getScheme()));
        while (true)
        {
            return localIContentProvider;
            if (paramUri.getAuthority() != null)
                localIContentProvider = acquireExistingProvider(this.mContext, paramUri.getAuthority());
        }
    }

    protected abstract IContentProvider acquireProvider(Context paramContext, String paramString);

    public final IContentProvider acquireProvider(Uri paramUri)
    {
        IContentProvider localIContentProvider = null;
        if (!"content".equals(paramUri.getScheme()));
        while (true)
        {
            return localIContentProvider;
            if (paramUri.getAuthority() != null)
                localIContentProvider = acquireProvider(this.mContext, paramUri.getAuthority());
        }
    }

    public final IContentProvider acquireProvider(String paramString)
    {
        if (paramString == null);
        for (IContentProvider localIContentProvider = null; ; localIContentProvider = acquireProvider(this.mContext, paramString))
            return localIContentProvider;
    }

    public final ContentProviderClient acquireUnstableContentProviderClient(Uri paramUri)
    {
        IContentProvider localIContentProvider = acquireUnstableProvider(paramUri);
        if (localIContentProvider != null);
        for (ContentProviderClient localContentProviderClient = new ContentProviderClient(this, localIContentProvider, false); ; localContentProviderClient = null)
            return localContentProviderClient;
    }

    public final ContentProviderClient acquireUnstableContentProviderClient(String paramString)
    {
        IContentProvider localIContentProvider = acquireUnstableProvider(paramString);
        if (localIContentProvider != null);
        for (ContentProviderClient localContentProviderClient = new ContentProviderClient(this, localIContentProvider, false); ; localContentProviderClient = null)
            return localContentProviderClient;
    }

    protected abstract IContentProvider acquireUnstableProvider(Context paramContext, String paramString);

    public final IContentProvider acquireUnstableProvider(Uri paramUri)
    {
        IContentProvider localIContentProvider = null;
        if (!"content".equals(paramUri.getScheme()));
        while (true)
        {
            return localIContentProvider;
            if (paramUri.getAuthority() != null)
                localIContentProvider = acquireUnstableProvider(this.mContext, paramUri.getAuthority());
        }
    }

    public final IContentProvider acquireUnstableProvider(String paramString)
    {
        if (paramString == null);
        for (IContentProvider localIContentProvider = null; ; localIContentProvider = acquireUnstableProvider(this.mContext, paramString))
            return localIContentProvider;
    }

    public ContentProviderResult[] applyBatch(String paramString, ArrayList<ContentProviderOperation> paramArrayList)
        throws RemoteException, OperationApplicationException
    {
        ContentProviderClient localContentProviderClient = acquireContentProviderClient(paramString);
        if (localContentProviderClient == null)
            throw new IllegalArgumentException("Unknown authority " + paramString);
        try
        {
            ContentProviderResult[] arrayOfContentProviderResult = localContentProviderClient.applyBatch(paramArrayList);
            return arrayOfContentProviderResult;
        }
        finally
        {
            localContentProviderClient.release();
        }
    }

    // ERROR //
    public final int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     5: astore_3
        //     6: aload_3
        //     7: ifnonnull +31 -> 38
        //     10: new 120	java/lang/IllegalArgumentException
        //     13: dup
        //     14: new 237	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 447
        //     24: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: aload_1
        //     28: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     31: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     34: invokespecial 125	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     37: athrow
        //     38: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     41: lstore 9
        //     43: aload_3
        //     44: aload_1
        //     45: aload_2
        //     46: invokeinterface 457 3 0
        //     51: istore 7
        //     53: aload_0
        //     54: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     57: lload 9
        //     59: lsub
        //     60: aload_1
        //     61: ldc_w 459
        //     64: aconst_null
        //     65: invokespecial 461	android/content/ContentResolver:maybeLogUpdateToEventLog	(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
        //     68: aload_0
        //     69: aload_3
        //     70: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     73: pop
        //     74: iload 7
        //     76: ireturn
        //     77: astore 6
        //     79: iconst_0
        //     80: istore 7
        //     82: goto -14 -> 68
        //     85: astore 4
        //     87: aload_0
        //     88: aload_3
        //     89: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     92: pop
        //     93: aload 4
        //     95: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     38	68	77	android/os/RemoteException
        //     38	68	85	finally
    }

    // ERROR //
    public final Bundle call(Uri paramUri, String paramString1, String paramString2, Bundle paramBundle)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +14 -> 15
        //     4: new 469	java/lang/NullPointerException
        //     7: dup
        //     8: ldc_w 471
        //     11: invokespecial 472	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     14: athrow
        //     15: aload_2
        //     16: ifnonnull +14 -> 30
        //     19: new 469	java/lang/NullPointerException
        //     22: dup
        //     23: ldc_w 474
        //     26: invokespecial 472	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     29: athrow
        //     30: aload_0
        //     31: aload_1
        //     32: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     35: astore 5
        //     37: aload 5
        //     39: ifnonnull +31 -> 70
        //     42: new 120	java/lang/IllegalArgumentException
        //     45: dup
        //     46: new 237	java/lang/StringBuilder
        //     49: dup
        //     50: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     53: ldc_w 476
        //     56: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     59: aload_1
        //     60: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     63: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     66: invokespecial 125	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     69: athrow
        //     70: aload 5
        //     72: aload_2
        //     73: aload_3
        //     74: aload 4
        //     76: invokeinterface 479 4 0
        //     81: astore 11
        //     83: aload 11
        //     85: astore 9
        //     87: aload_0
        //     88: aload 5
        //     90: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     93: pop
        //     94: aload 9
        //     96: areturn
        //     97: astore 8
        //     99: aconst_null
        //     100: astore 9
        //     102: goto -15 -> 87
        //     105: astore 6
        //     107: aload_0
        //     108: aload 5
        //     110: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     113: pop
        //     114: aload 6
        //     116: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     70	83	97	android/os/RemoteException
        //     70	83	105	finally
    }

    @Deprecated
    public void cancelSync(Uri paramUri)
    {
        if (paramUri != null);
        for (String str = paramUri.getAuthority(); ; str = null)
        {
            cancelSync(null, str);
            return;
        }
    }

    // ERROR //
    public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     5: astore 4
        //     7: aload 4
        //     9: ifnonnull +31 -> 40
        //     12: new 120	java/lang/IllegalArgumentException
        //     15: dup
        //     16: new 237	java/lang/StringBuilder
        //     19: dup
        //     20: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     23: ldc_w 447
        //     26: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: aload_1
        //     30: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     33: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     36: invokespecial 125	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     39: athrow
        //     40: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     43: lstore 10
        //     45: aload 4
        //     47: aload_1
        //     48: aload_2
        //     49: aload_3
        //     50: invokeinterface 485 4 0
        //     55: istore 8
        //     57: aload_0
        //     58: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     61: lload 10
        //     63: lsub
        //     64: aload_1
        //     65: ldc_w 486
        //     68: aload_2
        //     69: invokespecial 461	android/content/ContentResolver:maybeLogUpdateToEventLog	(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
        //     72: aload_0
        //     73: aload 4
        //     75: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     78: pop
        //     79: iload 8
        //     81: ireturn
        //     82: astore 7
        //     84: bipush 255
        //     86: istore 8
        //     88: goto -16 -> 72
        //     91: astore 5
        //     93: aload_0
        //     94: aload 4
        //     96: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     99: pop
        //     100: aload 5
        //     102: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     40	72	82	android/os/RemoteException
        //     40	72	91	finally
    }

    public OpenResourceIdResult getResourceId(Uri paramUri)
        throws FileNotFoundException
    {
        String str = paramUri.getAuthority();
        if (TextUtils.isEmpty(str))
            throw new FileNotFoundException("No authority: " + paramUri);
        Resources localResources;
        List localList;
        try
        {
            localResources = this.mContext.getPackageManager().getResourcesForApplication(str);
            localList = paramUri.getPathSegments();
            if (localList == null)
                throw new FileNotFoundException("No path: " + paramUri);
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new FileNotFoundException("No package found for authority: " + paramUri);
        }
        int i = localList.size();
        if (i == 1);
        int j;
        while (true)
        {
            try
            {
                int k = Integer.parseInt((String)localList.get(0));
                j = k;
                if (j != 0)
                    break;
                throw new FileNotFoundException("No resource found for: " + paramUri);
            }
            catch (NumberFormatException localNumberFormatException)
            {
                throw new FileNotFoundException("Single path segment is not a resource ID: " + paramUri);
            }
            if (i == 2)
                j = localResources.getIdentifier((String)localList.get(1), (String)localList.get(0), str);
            else
                throw new FileNotFoundException("More than two path segments: " + paramUri);
        }
        OpenResourceIdResult localOpenResourceIdResult = new OpenResourceIdResult();
        localOpenResourceIdResult.r = localResources;
        localOpenResourceIdResult.id = j;
        return localOpenResourceIdResult;
    }

    // ERROR //
    public String[] getStreamTypes(Uri paramUri, String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: aload_1
        //     4: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     7: astore 4
        //     9: aload 4
        //     11: ifnonnull +5 -> 16
        //     14: aload_3
        //     15: areturn
        //     16: aload 4
        //     18: aload_1
        //     19: aload_2
        //     20: invokeinterface 551 3 0
        //     25: astore 9
        //     27: aload 9
        //     29: astore_3
        //     30: aload_0
        //     31: aload 4
        //     33: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     36: pop
        //     37: goto -23 -> 14
        //     40: astore 7
        //     42: goto -12 -> 30
        //     45: astore 5
        //     47: aload_0
        //     48: aload 4
        //     50: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     53: pop
        //     54: aload 5
        //     56: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     16	27	40	android/os/RemoteException
        //     16	27	45	finally
    }

    public final String getType(Uri paramUri)
    {
        Object localObject1 = null;
        IContentProvider localIContentProvider = acquireExistingProvider(paramUri);
        if (localIContentProvider != null);
        while (true)
        {
            try
            {
                String str2 = localIContentProvider.getType(paramUri);
                localObject1 = str2;
                return localObject1;
            }
            catch (RemoteException localRemoteException2)
            {
                continue;
            }
            catch (Exception localException2)
            {
                Log.w("ContentResolver", "Failed to get type for: " + paramUri + " (" + localException2.getMessage() + ")");
                continue;
            }
            finally
            {
                releaseProvider(localIContentProvider);
            }
            if ("content".equals(paramUri.getScheme()))
                try
                {
                    String str1 = ActivityManagerNative.getDefault().getProviderMimeType(paramUri);
                    localObject1 = str1;
                }
                catch (RemoteException localRemoteException1)
                {
                }
                catch (Exception localException1)
                {
                    Log.w("ContentResolver", "Failed to get type for: " + paramUri + " (" + localException1.getMessage() + ")");
                }
        }
    }

    // ERROR //
    public final Uri insert(Uri paramUri, ContentValues paramContentValues)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     5: astore_3
        //     6: aload_3
        //     7: ifnonnull +31 -> 38
        //     10: new 120	java/lang/IllegalArgumentException
        //     13: dup
        //     14: new 237	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 447
        //     24: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: aload_1
        //     28: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     31: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     34: invokespecial 125	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     37: athrow
        //     38: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     41: lstore 9
        //     43: aload_3
        //     44: aload_1
        //     45: aload_2
        //     46: invokeinterface 588 3 0
        //     51: astore 8
        //     53: aload_0
        //     54: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     57: lload 9
        //     59: lsub
        //     60: aload_1
        //     61: ldc_w 589
        //     64: aconst_null
        //     65: invokespecial 461	android/content/ContentResolver:maybeLogUpdateToEventLog	(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
        //     68: aload_0
        //     69: aload_3
        //     70: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     73: pop
        //     74: aload 8
        //     76: areturn
        //     77: astore 6
        //     79: aload_0
        //     80: aload_3
        //     81: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     84: pop
        //     85: aconst_null
        //     86: astore 8
        //     88: goto -14 -> 74
        //     91: astore 4
        //     93: aload_0
        //     94: aload_3
        //     95: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     98: pop
        //     99: aload 4
        //     101: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     38	68	77	android/os/RemoteException
        //     38	68	91	finally
    }

    public void notifyChange(Uri paramUri, ContentObserver paramContentObserver)
    {
        notifyChange(paramUri, paramContentObserver, true);
    }

    public void notifyChange(Uri paramUri, ContentObserver paramContentObserver, boolean paramBoolean)
    {
        try
        {
            IContentService localIContentService = getContentService();
            Object localObject;
            if (paramContentObserver == null)
            {
                localObject = null;
                if ((paramContentObserver == null) || (!paramContentObserver.deliverSelfNotifications()))
                    break label55;
            }
            label55: for (boolean bool = true; ; bool = false)
            {
                localIContentService.notifyChange(paramUri, (IContentObserver)localObject, bool, paramBoolean);
                return;
                IContentObserver localIContentObserver = paramContentObserver.getContentObserver();
                localObject = localIContentObserver;
                break;
            }
        }
        catch (RemoteException localRemoteException)
        {
        }
    }

    // ERROR //
    public final AssetFileDescriptor openAssetFileDescriptor(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_1
        //     3: invokevirtual 416	android/net/Uri:getScheme	()Ljava/lang/String;
        //     6: astore 4
        //     8: ldc 28
        //     10: aload 4
        //     12: invokevirtual 294	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     15: ifeq +98 -> 113
        //     18: ldc_w 288
        //     21: aload_2
        //     22: invokevirtual 294	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     25: ifne +31 -> 56
        //     28: new 286	java/io/FileNotFoundException
        //     31: dup
        //     32: new 237	java/lang/StringBuilder
        //     35: dup
        //     36: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     39: ldc_w 614
        //     42: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     45: aload_1
        //     46: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     49: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     52: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     55: athrow
        //     56: aload_0
        //     57: aload_1
        //     58: invokevirtual 616	android/content/ContentResolver:getResourceId	(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;
        //     61: astore 22
        //     63: aload 22
        //     65: getfield 544	android/content/ContentResolver$OpenResourceIdResult:r	Landroid/content/res/Resources;
        //     68: aload 22
        //     70: getfield 547	android/content/ContentResolver$OpenResourceIdResult:id	I
        //     73: invokevirtual 620	android/content/res/Resources:openRawResourceFd	(I)Landroid/content/res/AssetFileDescriptor;
        //     76: astore 24
        //     78: aload 24
        //     80: astore_3
        //     81: aload_3
        //     82: areturn
        //     83: astore 23
        //     85: new 286	java/io/FileNotFoundException
        //     88: dup
        //     89: new 237	java/lang/StringBuilder
        //     92: dup
        //     93: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     96: ldc_w 622
        //     99: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     102: aload_1
        //     103: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     106: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     109: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     112: athrow
        //     113: ldc 32
        //     115: aload 4
        //     117: invokevirtual 294	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     120: ifeq +37 -> 157
        //     123: new 624	android/content/res/AssetFileDescriptor
        //     126: dup
        //     127: new 626	java/io/File
        //     130: dup
        //     131: aload_1
        //     132: invokevirtual 629	android/net/Uri:getPath	()Ljava/lang/String;
        //     135: invokespecial 630	java/io/File:<init>	(Ljava/lang/String;)V
        //     138: aload_1
        //     139: aload_2
        //     140: invokestatic 632	android/content/ContentResolver:modeToMode	(Landroid/net/Uri;Ljava/lang/String;)I
        //     143: invokestatic 638	android/os/ParcelFileDescriptor:open	(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
        //     146: lconst_0
        //     147: ldc2_w 639
        //     150: invokespecial 643	android/content/res/AssetFileDescriptor:<init>	(Landroid/os/ParcelFileDescriptor;JJ)V
        //     153: astore_3
        //     154: goto -73 -> 81
        //     157: ldc_w 288
        //     160: aload_2
        //     161: invokevirtual 294	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     164: ifeq +16 -> 180
        //     167: aload_0
        //     168: aload_1
        //     169: ldc_w 645
        //     172: aconst_null
        //     173: invokevirtual 649	android/content/ContentResolver:openTypedAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
        //     176: astore_3
        //     177: goto -96 -> 81
        //     180: aload_0
        //     181: aload_1
        //     182: invokevirtual 425	android/content/ContentResolver:acquireUnstableProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     185: astore 5
        //     187: aload 5
        //     189: ifnonnull +31 -> 220
        //     192: new 286	java/io/FileNotFoundException
        //     195: dup
        //     196: new 237	java/lang/StringBuilder
        //     199: dup
        //     200: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     203: ldc_w 651
        //     206: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     209: aload_1
        //     210: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     213: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     216: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     219: athrow
        //     220: aconst_null
        //     221: astore 6
        //     223: aload 5
        //     225: aload_1
        //     226: aload_2
        //     227: invokeinterface 654 3 0
        //     232: astore 20
        //     234: aload 20
        //     236: astore 14
        //     238: aload 14
        //     240: ifnonnull +175 -> 415
        //     243: iconst_0
        //     244: ifeq +9 -> 253
        //     247: aload_0
        //     248: aconst_null
        //     249: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     252: pop
        //     253: aload 5
        //     255: ifnull -174 -> 81
        //     258: aload_0
        //     259: aload 5
        //     261: invokevirtual 657	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     264: pop
        //     265: goto -184 -> 81
        //     268: astore 12
        //     270: aload_0
        //     271: aload 5
        //     273: invokevirtual 661	android/content/ContentResolver:unstableProviderDied	(Landroid/content/IContentProvider;)V
        //     276: aload_0
        //     277: aload_1
        //     278: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     281: astore 6
        //     283: aload 6
        //     285: ifnonnull +90 -> 375
        //     288: new 286	java/io/FileNotFoundException
        //     291: dup
        //     292: new 237	java/lang/StringBuilder
        //     295: dup
        //     296: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     299: ldc_w 651
        //     302: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     305: aload_1
        //     306: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     309: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     312: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     315: athrow
        //     316: astore 11
        //     318: new 286	java/io/FileNotFoundException
        //     321: dup
        //     322: new 237	java/lang/StringBuilder
        //     325: dup
        //     326: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     329: ldc_w 663
        //     332: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     335: aload_1
        //     336: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     339: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     342: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     345: athrow
        //     346: astore 8
        //     348: aload 6
        //     350: ifnull +10 -> 360
        //     353: aload_0
        //     354: aload 6
        //     356: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     359: pop
        //     360: aload 5
        //     362: ifnull +10 -> 372
        //     365: aload_0
        //     366: aload 5
        //     368: invokevirtual 657	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     371: pop
        //     372: aload 8
        //     374: athrow
        //     375: aload 6
        //     377: aload_1
        //     378: aload_2
        //     379: invokeinterface 654 3 0
        //     384: astore 13
        //     386: aload 13
        //     388: astore 14
        //     390: aload 14
        //     392: ifnonnull +23 -> 415
        //     395: aload 6
        //     397: ifnull +10 -> 407
        //     400: aload_0
        //     401: aload 6
        //     403: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     406: pop
        //     407: aload 5
        //     409: ifnull -328 -> 81
        //     412: goto -154 -> 258
        //     415: aload 6
        //     417: ifnonnull +10 -> 427
        //     420: aload_0
        //     421: aload_1
        //     422: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     425: astore 6
        //     427: aload_0
        //     428: aload 5
        //     430: invokevirtual 657	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     433: pop
        //     434: new 8	android/content/ContentResolver$ParcelFileDescriptorInner
        //     437: dup
        //     438: aload_0
        //     439: aload 14
        //     441: invokevirtual 667	android/content/res/AssetFileDescriptor:getParcelFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     444: aload 6
        //     446: invokespecial 670	android/content/ContentResolver$ParcelFileDescriptorInner:<init>	(Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/content/IContentProvider;)V
        //     449: astore 16
        //     451: aconst_null
        //     452: astore 6
        //     454: new 624	android/content/res/AssetFileDescriptor
        //     457: dup
        //     458: aload 16
        //     460: aload 14
        //     462: invokevirtual 673	android/content/res/AssetFileDescriptor:getStartOffset	()J
        //     465: aload 14
        //     467: invokevirtual 676	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     470: invokespecial 643	android/content/res/AssetFileDescriptor:<init>	(Landroid/os/ParcelFileDescriptor;JJ)V
        //     473: astore_3
        //     474: iconst_0
        //     475: ifeq +9 -> 484
        //     478: aload_0
        //     479: aconst_null
        //     480: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     483: pop
        //     484: aload 5
        //     486: ifnull -405 -> 81
        //     489: goto -231 -> 258
        //     492: astore 7
        //     494: aload 7
        //     496: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     63	78	83	android/content/res/Resources$NotFoundException
        //     223	234	268	android/os/DeadObjectException
        //     223	234	316	android/os/RemoteException
        //     270	316	316	android/os/RemoteException
        //     375	386	316	android/os/RemoteException
        //     420	474	316	android/os/RemoteException
        //     223	234	346	finally
        //     270	316	346	finally
        //     318	346	346	finally
        //     375	386	346	finally
        //     420	474	346	finally
        //     494	497	346	finally
        //     223	234	492	java/io/FileNotFoundException
        //     270	316	492	java/io/FileNotFoundException
        //     375	386	492	java/io/FileNotFoundException
        //     420	474	492	java/io/FileNotFoundException
    }

    public final ParcelFileDescriptor openFileDescriptor(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        AssetFileDescriptor localAssetFileDescriptor = openAssetFileDescriptor(paramUri, paramString);
        if (localAssetFileDescriptor == null);
        for (ParcelFileDescriptor localParcelFileDescriptor = null; ; localParcelFileDescriptor = localAssetFileDescriptor.getParcelFileDescriptor())
        {
            return localParcelFileDescriptor;
            if (localAssetFileDescriptor.getDeclaredLength() >= 0L)
                break;
        }
        try
        {
            localAssetFileDescriptor.close();
            label39: throw new FileNotFoundException("Not a whole file");
        }
        catch (IOException localIOException)
        {
            break label39;
        }
    }

    public final InputStream openInputStream(Uri paramUri)
        throws FileNotFoundException
    {
        String str = paramUri.getScheme();
        OpenResourceIdResult localOpenResourceIdResult;
        if ("android.resource".equals(str))
            localOpenResourceIdResult = getResourceId(paramUri);
        while (true)
        {
            Object localObject;
            try
            {
                InputStream localInputStream = localOpenResourceIdResult.r.openRawResource(localOpenResourceIdResult.id);
                localObject = localInputStream;
                return localObject;
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                throw new FileNotFoundException("Resource does not exist: " + paramUri);
            }
            if ("file".equals(str))
            {
                localObject = new FileInputStream(paramUri.getPath());
            }
            else
            {
                AssetFileDescriptor localAssetFileDescriptor = openAssetFileDescriptor(paramUri, "r");
                if (localAssetFileDescriptor != null);
                try
                {
                    FileInputStream localFileInputStream2 = localAssetFileDescriptor.createInputStream();
                    for (FileInputStream localFileInputStream1 = localFileInputStream2; ; localFileInputStream1 = null)
                    {
                        localObject = localFileInputStream1;
                        break;
                    }
                }
                catch (IOException localIOException)
                {
                }
            }
        }
        throw new FileNotFoundException("Unable to create stream");
    }

    public final OutputStream openOutputStream(Uri paramUri)
        throws FileNotFoundException
    {
        return openOutputStream(paramUri, "w");
    }

    public final OutputStream openOutputStream(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        AssetFileDescriptor localAssetFileDescriptor = openAssetFileDescriptor(paramUri, paramString);
        if (localAssetFileDescriptor != null);
        try
        {
            FileOutputStream localFileOutputStream2 = localAssetFileDescriptor.createOutputStream();
            for (FileOutputStream localFileOutputStream1 = localFileOutputStream2; ; localFileOutputStream1 = null)
                return localFileOutputStream1;
        }
        catch (IOException localIOException)
        {
        }
        throw new FileNotFoundException("Unable to create stream");
    }

    // ERROR //
    public final AssetFileDescriptor openTypedAssetFileDescriptor(Uri paramUri, String paramString, Bundle paramBundle)
        throws FileNotFoundException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: aload_0
        //     4: aload_1
        //     5: invokevirtual 425	android/content/ContentResolver:acquireUnstableProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     8: astore 5
        //     10: aload 5
        //     12: ifnonnull +31 -> 43
        //     15: new 286	java/io/FileNotFoundException
        //     18: dup
        //     19: new 237	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     26: ldc_w 651
        //     29: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     32: aload_1
        //     33: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     36: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     39: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     42: athrow
        //     43: aconst_null
        //     44: astore 6
        //     46: aload 5
        //     48: aload_1
        //     49: aload_2
        //     50: aload_3
        //     51: invokeinterface 714 4 0
        //     56: astore 20
        //     58: aload 20
        //     60: astore 14
        //     62: aload 14
        //     64: ifnonnull +176 -> 240
        //     67: iconst_0
        //     68: ifeq +9 -> 77
        //     71: aload_0
        //     72: aconst_null
        //     73: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     76: pop
        //     77: aload 5
        //     79: ifnull +10 -> 89
        //     82: aload_0
        //     83: aload 5
        //     85: invokevirtual 657	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     88: pop
        //     89: aload 4
        //     91: areturn
        //     92: astore 12
        //     94: aload_0
        //     95: aload 5
        //     97: invokevirtual 661	android/content/ContentResolver:unstableProviderDied	(Landroid/content/IContentProvider;)V
        //     100: aload_0
        //     101: aload_1
        //     102: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     105: astore 6
        //     107: aload 6
        //     109: ifnonnull +90 -> 199
        //     112: new 286	java/io/FileNotFoundException
        //     115: dup
        //     116: new 237	java/lang/StringBuilder
        //     119: dup
        //     120: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     123: ldc_w 651
        //     126: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     129: aload_1
        //     130: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     133: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     136: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     139: athrow
        //     140: astore 11
        //     142: new 286	java/io/FileNotFoundException
        //     145: dup
        //     146: new 237	java/lang/StringBuilder
        //     149: dup
        //     150: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     153: ldc_w 663
        //     156: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: aload_1
        //     160: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     163: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     166: invokespecial 318	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     169: athrow
        //     170: astore 8
        //     172: aload 6
        //     174: ifnull +10 -> 184
        //     177: aload_0
        //     178: aload 6
        //     180: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     183: pop
        //     184: aload 5
        //     186: ifnull +10 -> 196
        //     189: aload_0
        //     190: aload 5
        //     192: invokevirtual 657	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     195: pop
        //     196: aload 8
        //     198: athrow
        //     199: aload 6
        //     201: aload_1
        //     202: aload_2
        //     203: aload_3
        //     204: invokeinterface 714 4 0
        //     209: astore 13
        //     211: aload 13
        //     213: astore 14
        //     215: aload 14
        //     217: ifnonnull +23 -> 240
        //     220: aload 6
        //     222: ifnull +10 -> 232
        //     225: aload_0
        //     226: aload 6
        //     228: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     231: pop
        //     232: aload 5
        //     234: ifnull -145 -> 89
        //     237: goto -155 -> 82
        //     240: aload 6
        //     242: ifnonnull +10 -> 252
        //     245: aload_0
        //     246: aload_1
        //     247: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     250: astore 6
        //     252: aload_0
        //     253: aload 5
        //     255: invokevirtual 657	android/content/ContentResolver:releaseUnstableProvider	(Landroid/content/IContentProvider;)Z
        //     258: pop
        //     259: new 8	android/content/ContentResolver$ParcelFileDescriptorInner
        //     262: dup
        //     263: aload_0
        //     264: aload 14
        //     266: invokevirtual 667	android/content/res/AssetFileDescriptor:getParcelFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     269: aload 6
        //     271: invokespecial 670	android/content/ContentResolver$ParcelFileDescriptorInner:<init>	(Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/content/IContentProvider;)V
        //     274: astore 16
        //     276: aconst_null
        //     277: astore 6
        //     279: new 624	android/content/res/AssetFileDescriptor
        //     282: dup
        //     283: aload 16
        //     285: aload 14
        //     287: invokevirtual 673	android/content/res/AssetFileDescriptor:getStartOffset	()J
        //     290: aload 14
        //     292: invokevirtual 676	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     295: invokespecial 643	android/content/res/AssetFileDescriptor:<init>	(Landroid/os/ParcelFileDescriptor;JJ)V
        //     298: astore 4
        //     300: iconst_0
        //     301: ifeq +9 -> 310
        //     304: aload_0
        //     305: aconst_null
        //     306: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     309: pop
        //     310: aload 5
        //     312: ifnull -223 -> 89
        //     315: goto -233 -> 82
        //     318: astore 7
        //     320: aload 7
        //     322: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     46	58	92	android/os/DeadObjectException
        //     46	58	140	android/os/RemoteException
        //     94	140	140	android/os/RemoteException
        //     199	211	140	android/os/RemoteException
        //     245	300	140	android/os/RemoteException
        //     46	58	170	finally
        //     94	140	170	finally
        //     142	170	170	finally
        //     199	211	170	finally
        //     245	300	170	finally
        //     320	323	170	finally
        //     46	58	318	java/io/FileNotFoundException
        //     94	140	318	java/io/FileNotFoundException
        //     199	211	318	java/io/FileNotFoundException
        //     245	300	318	java/io/FileNotFoundException
    }

    public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, null);
    }

    public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
    {
        IContentProvider localIContentProvider1 = acquireUnstableProvider(paramUri);
        Object localObject3;
        if (localIContentProvider1 == null)
            localObject3 = null;
        while (true)
        {
            return localObject3;
            Object localObject1 = null;
            try
            {
                long l = SystemClock.uptimeMillis();
                ICancellationSignal localICancellationSignal1 = null;
                if (paramCancellationSignal != null)
                {
                    paramCancellationSignal.throwIfCanceled();
                    localICancellationSignal1 = localIContentProvider1.createCancellationSignal();
                    paramCancellationSignal.setRemote(localICancellationSignal1);
                }
                try
                {
                    Cursor localCursor2 = localIContentProvider1.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, localICancellationSignal1);
                    localCursor1 = localCursor2;
                    if (localCursor1 == null)
                        localObject3 = null;
                }
                catch (DeadObjectException localDeadObjectException)
                {
                    Cursor localCursor1;
                    while (true)
                    {
                        label102: unstableProviderDied(localIContentProvider1);
                        IContentProvider localIContentProvider2 = acquireProvider(paramUri);
                        localObject1 = localIContentProvider2;
                        if (localObject1 == null)
                        {
                            localObject3 = null;
                            if (localIContentProvider1 != null)
                                releaseUnstableProvider(localIContentProvider1);
                            if (localObject1 == null)
                                break;
                            continue;
                        }
                        ICancellationSignal localICancellationSignal2 = localICancellationSignal1;
                        localCursor1 = ((IContentProvider)localObject1).query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, localICancellationSignal2);
                    }
                    localCursor1.getCount();
                    maybeLogQueryToEventLog(SystemClock.uptimeMillis() - l, paramUri, paramArrayOfString1, paramString1, paramString2);
                    if (localObject1 != null);
                    IContentProvider localIContentProvider3;
                    for (Object localObject4 = localObject1; ; localObject4 = localIContentProvider3)
                    {
                        localObject3 = new CursorWrapperInner(localCursor1, localObject4);
                        localObject1 = null;
                        if (localIContentProvider1 != null)
                            releaseUnstableProvider(localIContentProvider1);
                        if (0 == 0)
                            break;
                        break label102;
                        localIContentProvider3 = acquireProvider(paramUri);
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    localObject3 = null;
                    if (localIContentProvider1 != null)
                        releaseUnstableProvider(localIContentProvider1);
                    if (localObject1 == null)
                        break;
                }
            }
            finally
            {
                if (localIContentProvider1 != null)
                    releaseUnstableProvider(localIContentProvider1);
                if (localObject1 != null)
                    releaseProvider((IContentProvider)localObject1);
            }
        }
    }

    public final void registerContentObserver(Uri paramUri, boolean paramBoolean, ContentObserver paramContentObserver)
    {
        try
        {
            getContentService().registerContentObserver(paramUri, paramBoolean, paramContentObserver.getContentObserver());
            label14: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public abstract boolean releaseProvider(IContentProvider paramIContentProvider);

    public abstract boolean releaseUnstableProvider(IContentProvider paramIContentProvider);

    @Deprecated
    public void startSync(Uri paramUri, Bundle paramBundle)
    {
        Account localAccount = null;
        if (paramBundle != null)
        {
            String str2 = paramBundle.getString("account");
            if (!TextUtils.isEmpty(str2))
                localAccount = new Account(str2, "com.google");
            paramBundle.remove("account");
        }
        if (paramUri != null);
        for (String str1 = paramUri.getAuthority(); ; str1 = null)
        {
            requestSync(localAccount, str1, paramBundle);
            return;
        }
    }

    public final void unregisterContentObserver(ContentObserver paramContentObserver)
    {
        try
        {
            IContentObserver localIContentObserver = paramContentObserver.releaseContentObserver();
            if (localIContentObserver != null)
                getContentService().unregisterContentObserver(localIContentObserver);
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    public abstract void unstableProviderDied(IContentProvider paramIContentProvider);

    // ERROR //
    public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokevirtual 400	android/content/ContentResolver:acquireProvider	(Landroid/net/Uri;)Landroid/content/IContentProvider;
        //     5: astore 5
        //     7: aload 5
        //     9: ifnonnull +31 -> 40
        //     12: new 120	java/lang/IllegalArgumentException
        //     15: dup
        //     16: new 237	java/lang/StringBuilder
        //     19: dup
        //     20: invokespecial 310	java/lang/StringBuilder:<init>	()V
        //     23: ldc_w 476
        //     26: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: aload_1
        //     30: invokevirtual 315	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     33: invokevirtual 259	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     36: invokespecial 125	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     39: athrow
        //     40: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     43: lstore 11
        //     45: aload 5
        //     47: aload_1
        //     48: aload_2
        //     49: aload_3
        //     50: aload 4
        //     52: invokeinterface 777 5 0
        //     57: istore 9
        //     59: aload_0
        //     60: invokestatic 453	android/os/SystemClock:uptimeMillis	()J
        //     63: lload 11
        //     65: lsub
        //     66: aload_1
        //     67: ldc_w 778
        //     70: aload_3
        //     71: invokespecial 461	android/content/ContentResolver:maybeLogUpdateToEventLog	(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
        //     74: aload_0
        //     75: aload 5
        //     77: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     80: pop
        //     81: iload 9
        //     83: ireturn
        //     84: astore 8
        //     86: bipush 255
        //     88: istore 9
        //     90: goto -16 -> 74
        //     93: astore 6
        //     95: aload_0
        //     96: aload 5
        //     98: invokevirtual 465	android/content/ContentResolver:releaseProvider	(Landroid/content/IContentProvider;)Z
        //     101: pop
        //     102: aload 6
        //     104: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     40	74	84	android/os/RemoteException
        //     40	74	93	finally
    }

    private final class ParcelFileDescriptorInner extends ParcelFileDescriptor
    {
        private final IContentProvider mContentProvider;
        private boolean mReleaseProviderFlag = false;

        ParcelFileDescriptorInner(ParcelFileDescriptor paramIContentProvider, IContentProvider arg3)
        {
            super();
            Object localObject;
            this.mContentProvider = localObject;
        }

        public void close()
            throws IOException
        {
            if (!this.mReleaseProviderFlag)
            {
                super.close();
                ContentResolver.this.releaseProvider(this.mContentProvider);
                this.mReleaseProviderFlag = true;
            }
        }

        protected void finalize()
            throws Throwable
        {
            if (!this.mReleaseProviderFlag)
                close();
        }
    }

    private final class CursorWrapperInner extends CrossProcessCursorWrapper
    {
        public static final String TAG = "CursorWrapperInner";
        private final CloseGuard mCloseGuard = CloseGuard.get();
        private final IContentProvider mContentProvider;
        private boolean mProviderReleased;

        CursorWrapperInner(Cursor paramIContentProvider, IContentProvider arg3)
        {
            super();
            Object localObject;
            this.mContentProvider = localObject;
            this.mCloseGuard.open("close");
        }

        public void close()
        {
            super.close();
            ContentResolver.this.releaseProvider(this.mContentProvider);
            this.mProviderReleased = true;
            if (this.mCloseGuard != null)
                this.mCloseGuard.close();
        }

        protected void finalize()
            throws Throwable
        {
            try
            {
                if (this.mCloseGuard != null)
                    this.mCloseGuard.warnIfOpen();
                if ((!this.mProviderReleased) && (this.mContentProvider != null))
                {
                    Log.w("CursorWrapperInner", "Cursor finalized without prior close()");
                    ContentResolver.this.releaseProvider(this.mContentProvider);
                }
                return;
            }
            finally
            {
                super.finalize();
            }
        }
    }

    public class OpenResourceIdResult
    {
        public int id;
        public Resources r;

        public OpenResourceIdResult()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentResolver
 * JD-Core Version:        0.6.2
 */