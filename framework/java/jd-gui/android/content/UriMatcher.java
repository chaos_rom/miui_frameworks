package android.content;

import android.net.Uri;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UriMatcher
{
    private static final int EXACT = 0;
    public static final int NO_MATCH = -1;
    private static final int NUMBER = 1;
    static final Pattern PATH_SPLIT_PATTERN = Pattern.compile("/");
    private static final int TEXT = 2;
    private ArrayList<UriMatcher> mChildren;
    private int mCode;
    private String mText;
    private int mWhich;

    private UriMatcher()
    {
        this.mCode = -1;
        this.mWhich = -1;
        this.mChildren = new ArrayList();
        this.mText = null;
    }

    public UriMatcher(int paramInt)
    {
        this.mCode = paramInt;
        this.mWhich = -1;
        this.mChildren = new ArrayList();
        this.mText = null;
    }

    public void addURI(String paramString1, String paramString2, int paramInt)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException("code " + paramInt + " is invalid: it must be positive");
        String[] arrayOfString;
        int i;
        label59: Object localObject;
        int j;
        label66: String str;
        label81: int m;
        label98: UriMatcher localUriMatcher1;
        if (paramString2 != null)
        {
            arrayOfString = PATH_SPLIT_PATTERN.split(paramString2);
            if (arrayOfString == null)
                break label200;
            i = arrayOfString.length;
            localObject = this;
            j = -1;
            if (j >= i)
                break label250;
            if (j >= 0)
                break label206;
            str = paramString1;
            ArrayList localArrayList = ((UriMatcher)localObject).mChildren;
            int k = localArrayList.size();
            m = 0;
            if (m < k)
            {
                UriMatcher localUriMatcher2 = (UriMatcher)localArrayList.get(m);
                if (!str.equals(localUriMatcher2.mText))
                    break label216;
                localObject = localUriMatcher2;
            }
            if (m == k)
            {
                localUriMatcher1 = new UriMatcher();
                if (!str.equals("#"))
                    break label222;
                localUriMatcher1.mWhich = 1;
            }
        }
        while (true)
        {
            localUriMatcher1.mText = str;
            ((UriMatcher)localObject).mChildren.add(localUriMatcher1);
            localObject = localUriMatcher1;
            j++;
            break label66;
            arrayOfString = null;
            break;
            label200: i = 0;
            break label59;
            label206: str = arrayOfString[j];
            break label81;
            label216: m++;
            break label98;
            label222: if (str.equals("*"))
                localUriMatcher1.mWhich = 2;
            else
                localUriMatcher1.mWhich = 0;
        }
        label250: ((UriMatcher)localObject).mCode = paramInt;
    }

    public int match(Uri paramUri)
    {
        List localList = paramUri.getPathSegments();
        int i = localList.size();
        Object localObject = this;
        int k;
        if ((i == 0) && (paramUri.getAuthority() == null))
        {
            k = this.mCode;
            return k;
        }
        label259: for (int j = -1; ; j++)
        {
            if (j < i)
                if (j >= 0)
                    break label78;
            ArrayList localArrayList;
            label78: for (String str = paramUri.getAuthority(); ; str = (String)localList.get(j))
            {
                localArrayList = ((UriMatcher)localObject).mChildren;
                if (localArrayList != null)
                    break label94;
                k = ((UriMatcher)localObject).mCode;
                break;
            }
            label94: localObject = null;
            int m = localArrayList.size();
            label239: label253: for (int n = 0; ; n++)
            {
                UriMatcher localUriMatcher;
                if (n < m)
                {
                    localUriMatcher = (UriMatcher)localArrayList.get(n);
                    switch (localUriMatcher.mWhich)
                    {
                    default:
                    case 0:
                    case 1:
                    case 2:
                    }
                }
                while (true)
                {
                    if (localObject == null)
                        break label253;
                    if (localObject != null)
                        break label259;
                    k = -1;
                    break;
                    if (localUriMatcher.mText.equals(str))
                    {
                        localObject = localUriMatcher;
                        continue;
                        int i1 = str.length();
                        for (int i2 = 0; ; i2++)
                        {
                            if (i2 >= i1)
                                break label239;
                            int i3 = str.charAt(i2);
                            if ((i3 < 48) || (i3 > 57))
                                break;
                        }
                        localObject = localUriMatcher;
                        continue;
                        localObject = localUriMatcher;
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.UriMatcher
 * JD-Core Version:        0.6.2
 */