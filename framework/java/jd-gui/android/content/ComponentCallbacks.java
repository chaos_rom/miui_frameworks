package android.content;

import android.content.res.Configuration;

public abstract interface ComponentCallbacks
{
    public abstract void onConfigurationChanged(Configuration paramConfiguration);

    public abstract void onLowMemory();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ComponentCallbacks
 * JD-Core Version:        0.6.2
 */