package android.content;

import android.content.pm.ComponentInfo;
import android.content.pm.PathPermission;
import android.content.pm.ProviderInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ICancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class ContentProvider
    implements ComponentCallbacks2
{
    private static final String TAG = "ContentProvider";
    private Context mContext = null;
    private boolean mExported;
    private int mMyUid;
    private PathPermission[] mPathPermissions;
    private String mReadPermission;
    private Transport mTransport = new Transport();
    private String mWritePermission;

    public ContentProvider()
    {
    }

    public ContentProvider(Context paramContext, String paramString1, String paramString2, PathPermission[] paramArrayOfPathPermission)
    {
        this.mContext = paramContext;
        this.mReadPermission = paramString1;
        this.mWritePermission = paramString2;
        this.mPathPermissions = paramArrayOfPathPermission;
    }

    public static ContentProvider coerceToLocalContentProvider(IContentProvider paramIContentProvider)
    {
        if ((paramIContentProvider instanceof Transport));
        for (ContentProvider localContentProvider = ((Transport)paramIContentProvider).getContentProvider(); ; localContentProvider = null)
            return localContentProvider;
    }

    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> paramArrayList)
        throws OperationApplicationException
    {
        int i = paramArrayList.size();
        ContentProviderResult[] arrayOfContentProviderResult = new ContentProviderResult[i];
        for (int j = 0; j < i; j++)
            arrayOfContentProviderResult[j] = ((ContentProviderOperation)paramArrayList.get(j)).apply(this, arrayOfContentProviderResult, j);
        return arrayOfContentProviderResult;
    }

    public void attachInfo(Context paramContext, ProviderInfo paramProviderInfo)
    {
        AsyncTask.init();
        if (this.mContext == null)
        {
            this.mContext = paramContext;
            this.mMyUid = Process.myUid();
            if (paramProviderInfo != null)
            {
                setReadPermission(paramProviderInfo.readPermission);
                setWritePermission(paramProviderInfo.writePermission);
                setPathPermissions(paramProviderInfo.pathPermissions);
                this.mExported = paramProviderInfo.exported;
            }
            onCreate();
        }
    }

    public int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
    {
        int i = paramArrayOfContentValues.length;
        for (int j = 0; j < i; j++)
            insert(paramUri, paramArrayOfContentValues[j]);
        return i;
    }

    public Bundle call(String paramString1, String paramString2, Bundle paramBundle)
    {
        return null;
    }

    public abstract int delete(Uri paramUri, String paramString, String[] paramArrayOfString);

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("nothing to dump");
    }

    public final Context getContext()
    {
        return this.mContext;
    }

    public IContentProvider getIContentProvider()
    {
        return this.mTransport;
    }

    public final PathPermission[] getPathPermissions()
    {
        return this.mPathPermissions;
    }

    public final String getReadPermission()
    {
        return this.mReadPermission;
    }

    public String[] getStreamTypes(Uri paramUri, String paramString)
    {
        return null;
    }

    public abstract String getType(Uri paramUri);

    public final String getWritePermission()
    {
        return this.mWritePermission;
    }

    public abstract Uri insert(Uri paramUri, ContentValues paramContentValues);

    protected boolean isTemporary()
    {
        return false;
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
    }

    public abstract boolean onCreate();

    public void onLowMemory()
    {
    }

    public void onTrimMemory(int paramInt)
    {
    }

    public AssetFileDescriptor openAssetFile(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        ParcelFileDescriptor localParcelFileDescriptor = openFile(paramUri, paramString);
        if (localParcelFileDescriptor != null);
        for (AssetFileDescriptor localAssetFileDescriptor = new AssetFileDescriptor(localParcelFileDescriptor, 0L, -1L); ; localAssetFileDescriptor = null)
            return localAssetFileDescriptor;
    }

    public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        throw new FileNotFoundException("No files supported by provider at " + paramUri);
    }

    protected final ParcelFileDescriptor openFileHelper(Uri paramUri, String paramString)
        throws FileNotFoundException
    {
        int i = 0;
        String[] arrayOfString = new String[1];
        arrayOfString[i] = "_data";
        Cursor localCursor = query(paramUri, arrayOfString, null, null, null);
        if (localCursor != null)
            i = localCursor.getCount();
        if (i != 1)
        {
            if (localCursor != null)
                localCursor.close();
            if (i == 0)
                throw new FileNotFoundException("No entry for " + paramUri);
            throw new FileNotFoundException("Multiple items at " + paramUri);
        }
        localCursor.moveToFirst();
        int j = localCursor.getColumnIndex("_data");
        if (j >= 0);
        for (String str = localCursor.getString(j); ; str = null)
        {
            localCursor.close();
            if (str != null)
                break;
            throw new FileNotFoundException("Column _data not found.");
        }
        int k = ContentResolver.modeToMode(paramUri, paramString);
        return ParcelFileDescriptor.open(new File(str), k);
    }

    public <T> ParcelFileDescriptor openPipeHelper(final Uri paramUri, final String paramString, final Bundle paramBundle, final T paramT, final PipeDataWriter<T> paramPipeDataWriter)
        throws FileNotFoundException
    {
        try
        {
            final ParcelFileDescriptor[] arrayOfParcelFileDescriptor = ParcelFileDescriptor.createPipe();
            new AsyncTask()
            {
                protected Object doInBackground(Object[] paramAnonymousArrayOfObject)
                {
                    paramPipeDataWriter.writeDataToPipe(arrayOfParcelFileDescriptor[1], paramUri, paramString, paramBundle, paramT);
                    try
                    {
                        arrayOfParcelFileDescriptor[1].close();
                        return null;
                    }
                    catch (IOException localIOException)
                    {
                        while (true)
                            Log.w("ContentProvider", "Failure closing pipe", localIOException);
                    }
                }
            }
            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])null);
            ParcelFileDescriptor localParcelFileDescriptor = arrayOfParcelFileDescriptor[0];
            return localParcelFileDescriptor;
        }
        catch (IOException localIOException)
        {
        }
        throw new FileNotFoundException("failure making pipe");
    }

    public AssetFileDescriptor openTypedAssetFile(Uri paramUri, String paramString, Bundle paramBundle)
        throws FileNotFoundException
    {
        if ("*/*".equals(paramString));
        for (AssetFileDescriptor localAssetFileDescriptor = openAssetFile(paramUri, "r"); ; localAssetFileDescriptor = openAssetFile(paramUri, "r"))
        {
            return localAssetFileDescriptor;
            String str = getType(paramUri);
            if ((str == null) || (!ClipDescription.compareMimeTypes(str, paramString)))
                break;
        }
        throw new FileNotFoundException("Can't open " + paramUri + " as type " + paramString);
    }

    public abstract Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2);

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
    {
        return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2);
    }

    protected final void setPathPermissions(PathPermission[] paramArrayOfPathPermission)
    {
        this.mPathPermissions = paramArrayOfPathPermission;
    }

    protected final void setReadPermission(String paramString)
    {
        this.mReadPermission = paramString;
    }

    protected final void setWritePermission(String paramString)
    {
        this.mWritePermission = paramString;
    }

    public void shutdown()
    {
        Log.w("ContentProvider", "implement ContentProvider shutdown() to make sure all database connections are gracefully shutdown");
    }

    public abstract int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString);

    public static abstract interface PipeDataWriter<T>
    {
        public abstract void writeDataToPipe(ParcelFileDescriptor paramParcelFileDescriptor, Uri paramUri, String paramString, Bundle paramBundle, T paramT);
    }

    class Transport extends ContentProviderNative
    {
        Transport()
        {
        }

        private void enforceReadPermission(Uri paramUri)
            throws SecurityException
        {
            Context localContext = ContentProvider.this.getContext();
            int i = Binder.getCallingPid();
            int j = Binder.getCallingUid();
            Object localObject = null;
            if (j == ContentProvider.this.mMyUid);
            label176: label181: 
            do
            {
                int k;
                do
                {
                    String str2;
                    do
                    {
                        return;
                        if (!ContentProvider.this.mExported)
                            break label181;
                        str2 = ContentProvider.this.getReadPermission();
                        if (str2 == null)
                            break;
                    }
                    while (localContext.checkPermission(str2, i, j) == 0);
                    localObject = str2;
                    if (str2 == null);
                    for (k = 1; ; k = 0)
                    {
                        PathPermission[] arrayOfPathPermission = ContentProvider.this.getPathPermissions();
                        if (arrayOfPathPermission == null)
                            break label176;
                        String str3 = paramUri.getPath();
                        int m = arrayOfPathPermission.length;
                        for (int n = 0; ; n++)
                        {
                            if (n >= m)
                                break label176;
                            PathPermission localPathPermission = arrayOfPathPermission[n];
                            String str4 = localPathPermission.getReadPermission();
                            if ((str4 != null) && (localPathPermission.match(str3)))
                            {
                                if (localContext.checkPermission(str4, i, j) == 0)
                                    break;
                                k = 0;
                                localObject = str4;
                            }
                        }
                    }
                }
                while (k != 0);
            }
            while (localContext.checkUriPermission(paramUri, i, j, 1) == 0);
            if (ContentProvider.this.mExported);
            for (String str1 = " requires " + localObject + ", or grantUriPermission()"; ; str1 = " requires the provider be exported, or grantUriPermission()")
                throw new SecurityException("Permission Denial: reading " + ContentProvider.this.getClass().getName() + " uri " + paramUri + " from pid=" + i + ", uid=" + j + str1);
        }

        private void enforceWritePermission(Uri paramUri)
            throws SecurityException
        {
            Context localContext = ContentProvider.this.getContext();
            int i = Binder.getCallingPid();
            int j = Binder.getCallingUid();
            Object localObject = null;
            if (j == ContentProvider.this.mMyUid);
            label176: label181: 
            do
            {
                int k;
                do
                {
                    String str2;
                    do
                    {
                        return;
                        if (!ContentProvider.this.mExported)
                            break label181;
                        str2 = ContentProvider.this.getWritePermission();
                        if (str2 == null)
                            break;
                    }
                    while (localContext.checkPermission(str2, i, j) == 0);
                    localObject = str2;
                    if (str2 == null);
                    for (k = 1; ; k = 0)
                    {
                        PathPermission[] arrayOfPathPermission = ContentProvider.this.getPathPermissions();
                        if (arrayOfPathPermission == null)
                            break label176;
                        String str3 = paramUri.getPath();
                        int m = arrayOfPathPermission.length;
                        for (int n = 0; ; n++)
                        {
                            if (n >= m)
                                break label176;
                            PathPermission localPathPermission = arrayOfPathPermission[n];
                            String str4 = localPathPermission.getWritePermission();
                            if ((str4 != null) && (localPathPermission.match(str3)))
                            {
                                if (localContext.checkPermission(str4, i, j) == 0)
                                    break;
                                k = 0;
                                localObject = str4;
                            }
                        }
                    }
                }
                while (k != 0);
            }
            while (localContext.checkUriPermission(paramUri, i, j, 2) == 0);
            if (ContentProvider.this.mExported);
            for (String str1 = " requires " + localObject + ", or grantUriPermission()"; ; str1 = " requires the provider be exported, or grantUriPermission()")
                throw new SecurityException("Permission Denial: writing " + ContentProvider.this.getClass().getName() + " uri " + paramUri + " from pid=" + i + ", uid=" + j + str1);
        }

        public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> paramArrayList)
            throws OperationApplicationException
        {
            Iterator localIterator = paramArrayList.iterator();
            while (localIterator.hasNext())
            {
                ContentProviderOperation localContentProviderOperation = (ContentProviderOperation)localIterator.next();
                if (localContentProviderOperation.isReadOperation())
                    enforceReadPermission(localContentProviderOperation.getUri());
                if (localContentProviderOperation.isWriteOperation())
                    enforceWritePermission(localContentProviderOperation.getUri());
            }
            return ContentProvider.this.applyBatch(paramArrayList);
        }

        public int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
        {
            enforceWritePermission(paramUri);
            return ContentProvider.this.bulkInsert(paramUri, paramArrayOfContentValues);
        }

        public Bundle call(String paramString1, String paramString2, Bundle paramBundle)
        {
            return ContentProvider.this.call(paramString1, paramString2, paramBundle);
        }

        public ICancellationSignal createCancellationSignal()
            throws RemoteException
        {
            return CancellationSignal.createTransport();
        }

        public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
        {
            enforceWritePermission(paramUri);
            return ContentProvider.this.delete(paramUri, paramString, paramArrayOfString);
        }

        ContentProvider getContentProvider()
        {
            return ContentProvider.this;
        }

        public String getProviderName()
        {
            return getContentProvider().getClass().getName();
        }

        public String[] getStreamTypes(Uri paramUri, String paramString)
        {
            return ContentProvider.this.getStreamTypes(paramUri, paramString);
        }

        public String getType(Uri paramUri)
        {
            return ContentProvider.this.getType(paramUri);
        }

        public Uri insert(Uri paramUri, ContentValues paramContentValues)
        {
            enforceWritePermission(paramUri);
            return ContentProvider.this.insert(paramUri, paramContentValues);
        }

        public AssetFileDescriptor openAssetFile(Uri paramUri, String paramString)
            throws FileNotFoundException
        {
            if ((paramString != null) && (paramString.startsWith("rw")))
                enforceWritePermission(paramUri);
            while (true)
            {
                return ContentProvider.this.openAssetFile(paramUri, paramString);
                enforceReadPermission(paramUri);
            }
        }

        public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
            throws FileNotFoundException
        {
            if ((paramString != null) && (paramString.startsWith("rw")))
                enforceWritePermission(paramUri);
            while (true)
            {
                return ContentProvider.this.openFile(paramUri, paramString);
                enforceReadPermission(paramUri);
            }
        }

        public AssetFileDescriptor openTypedAssetFile(Uri paramUri, String paramString, Bundle paramBundle)
            throws FileNotFoundException
        {
            enforceReadPermission(paramUri);
            return ContentProvider.this.openTypedAssetFile(paramUri, paramString, paramBundle);
        }

        public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, ICancellationSignal paramICancellationSignal)
        {
            enforceReadPermission(paramUri);
            return ContentProvider.this.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, CancellationSignal.fromTransport(paramICancellationSignal));
        }

        public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
        {
            enforceWritePermission(paramUri);
            return ContentProvider.this.update(paramUri, paramContentValues, paramString, paramArrayOfString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentProvider
 * JD-Core Version:        0.6.2
 */