package android.content;

import android.accounts.Account;
import android.os.Bundle;
import android.os.SystemClock;
import java.util.Iterator;
import java.util.Set;

public class SyncOperation
    implements Comparable
{
    public final Account account;
    public final boolean allowParallelSyncs;
    public String authority;
    public Long backoff;
    public long delayUntil;
    public long earliestRunTime;
    public long effectiveRunTime;
    public boolean expedited;
    public Bundle extras;
    public final String key;
    public SyncStorageEngine.PendingOperation pendingOperation;
    public int syncSource;
    public final int userId;

    public SyncOperation(Account paramAccount, int paramInt1, int paramInt2, String paramString, Bundle paramBundle, long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean)
    {
        this.account = paramAccount;
        this.userId = paramInt1;
        this.syncSource = paramInt2;
        this.authority = paramString;
        this.allowParallelSyncs = paramBoolean;
        this.extras = new Bundle(paramBundle);
        removeFalseExtra("upload");
        removeFalseExtra("force");
        removeFalseExtra("ignore_settings");
        removeFalseExtra("ignore_backoff");
        removeFalseExtra("do_not_retry");
        removeFalseExtra("discard_deletions");
        removeFalseExtra("expedited");
        removeFalseExtra("deletions_override");
        this.delayUntil = paramLong3;
        this.backoff = Long.valueOf(paramLong2);
        long l = SystemClock.elapsedRealtime();
        if (paramLong1 < 0L)
            this.expedited = true;
        for (this.earliestRunTime = l; ; this.earliestRunTime = (l + paramLong1))
        {
            updateEffectiveRunTime();
            this.key = toKey();
            return;
            this.expedited = false;
        }
    }

    SyncOperation(SyncOperation paramSyncOperation)
    {
        this.account = paramSyncOperation.account;
        this.userId = paramSyncOperation.userId;
        this.syncSource = paramSyncOperation.syncSource;
        this.authority = paramSyncOperation.authority;
        this.extras = new Bundle(paramSyncOperation.extras);
        this.expedited = paramSyncOperation.expedited;
        this.earliestRunTime = SystemClock.elapsedRealtime();
        this.backoff = paramSyncOperation.backoff;
        this.delayUntil = paramSyncOperation.delayUntil;
        this.allowParallelSyncs = paramSyncOperation.allowParallelSyncs;
        updateEffectiveRunTime();
        this.key = toKey();
    }

    public static void extrasToStringBuilder(Bundle paramBundle, StringBuilder paramStringBuilder)
    {
        paramStringBuilder.append("[");
        Iterator localIterator = paramBundle.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            paramStringBuilder.append(str).append("=").append(paramBundle.get(str)).append(" ");
        }
        paramStringBuilder.append("]");
    }

    private void removeFalseExtra(String paramString)
    {
        if (!this.extras.getBoolean(paramString, false))
            this.extras.remove(paramString);
    }

    private String toKey()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("authority: ").append(this.authority);
        localStringBuilder.append(" account {name=" + this.account.name + ", user=" + this.userId + ", type=" + this.account.type + "}");
        localStringBuilder.append(" extras: ");
        extrasToStringBuilder(this.extras, localStringBuilder);
        return localStringBuilder.toString();
    }

    public int compareTo(Object paramObject)
    {
        int i = -1;
        SyncOperation localSyncOperation = (SyncOperation)paramObject;
        if (this.expedited != localSyncOperation.expedited)
            if (!this.expedited);
        while (true)
        {
            return i;
            i = 1;
            continue;
            if (this.effectiveRunTime == localSyncOperation.effectiveRunTime)
                i = 0;
            else if (this.effectiveRunTime >= localSyncOperation.effectiveRunTime)
                i = 1;
        }
    }

    public String dump(boolean paramBoolean)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(this.account.name);
        localStringBuilder.append(" (" + this.account.type + ")");
        localStringBuilder.append(", " + this.authority);
        localStringBuilder.append(", ");
        localStringBuilder.append(SyncStorageEngine.SOURCES[this.syncSource]);
        localStringBuilder.append(", earliestRunTime " + this.earliestRunTime);
        if (this.expedited)
            localStringBuilder.append(", EXPEDITED");
        if ((!paramBoolean) && (!this.extras.keySet().isEmpty()))
        {
            localStringBuilder.append("\n        ");
            extrasToStringBuilder(this.extras, localStringBuilder);
        }
        return localStringBuilder.toString();
    }

    public boolean ignoreBackoff()
    {
        return this.extras.getBoolean("ignore_backoff", false);
    }

    public boolean isExpedited()
    {
        return this.extras.getBoolean("expedited", false);
    }

    public boolean isInitialization()
    {
        return this.extras.getBoolean("initialize", false);
    }

    public String toString()
    {
        return dump(true);
    }

    public void updateEffectiveRunTime()
    {
        if (ignoreBackoff());
        for (long l = this.earliestRunTime; ; l = Math.max(Math.max(this.earliestRunTime, this.delayUntil), this.backoff.longValue()))
        {
            this.effectiveRunTime = l;
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncOperation
 * JD-Core Version:        0.6.2
 */