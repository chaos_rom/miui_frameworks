package android.content;

import android.accounts.Account;
import android.accounts.AccountAndUser;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import com.android.internal.os.AtomicFile;
import com.android.internal.util.FastXmlSerializer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class SyncStorageEngine extends Handler
{
    private static final int ACCOUNTS_VERSION = 2;
    private static final boolean DEBUG_FILE = false;
    private static final long DEFAULT_POLL_FREQUENCY_SECONDS = 86400L;
    public static final String[] EVENTS;
    public static final int EVENT_START = 0;
    public static final int EVENT_STOP = 1;
    public static final int MAX_HISTORY = 100;
    public static final String MESG_CANCELED = "canceled";
    public static final String MESG_SUCCESS = "success";
    static final long MILLIS_IN_4WEEKS = 2419200000L;
    private static final int MSG_WRITE_STATISTICS = 2;
    private static final int MSG_WRITE_STATUS = 1;
    public static final long NOT_IN_BACKOFF_MODE = -1L;
    private static final int PENDING_FINISH_TO_WRITE = 4;
    public static final int PENDING_OPERATION_VERSION = 2;
    public static final String[] SOURCES;
    public static final int SOURCE_LOCAL = 1;
    public static final int SOURCE_PERIODIC = 4;
    public static final int SOURCE_POLL = 2;
    public static final int SOURCE_SERVER = 0;
    public static final int SOURCE_USER = 3;
    public static final int STATISTICS_FILE_END = 0;
    public static final int STATISTICS_FILE_ITEM = 101;
    public static final int STATISTICS_FILE_ITEM_OLD = 100;
    public static final int STATUS_FILE_END = 0;
    public static final int STATUS_FILE_ITEM = 100;
    public static final Intent SYNC_CONNECTION_SETTING_CHANGED_INTENT;
    private static final boolean SYNC_ENABLED_DEFAULT = false;
    private static final String TAG = "SyncManager";
    private static final long WRITE_STATISTICS_DELAY = 1800000L;
    private static final long WRITE_STATUS_DELAY = 600000L;
    private static final String XML_ATTR_ENABLED = "enabled";
    private static final String XML_ATTR_LISTEN_FOR_TICKLES = "listen-for-tickles";
    private static final String XML_ATTR_NEXT_AUTHORITY_ID = "nextAuthorityId";
    private static final String XML_ATTR_SYNC_RANDOM_OFFSET = "offsetInSeconds";
    private static final String XML_ATTR_USER = "user";
    private static final String XML_TAG_LISTEN_FOR_TICKLES = "listenForTickles";
    private static HashMap<String, String> sAuthorityRenames;
    private static volatile SyncStorageEngine sSyncStorageEngine = null;
    private final AtomicFile mAccountInfoFile;
    private final HashMap<AccountAndUser, AccountInfo> mAccounts = new HashMap();
    private final SparseArray<AuthorityInfo> mAuthorities = new SparseArray();
    private final Calendar mCal;
    private final RemoteCallbackList<ISyncStatusObserver> mChangeListeners = new RemoteCallbackList();
    private final Context mContext;
    private final SparseArray<ArrayList<SyncInfo>> mCurrentSyncs = new SparseArray();
    private final DayStats[] mDayStats = new DayStats[28];
    private SparseArray<Boolean> mMasterSyncAutomatically = new SparseArray();
    private int mNextAuthorityId = 0;
    private int mNextHistoryId = 0;
    private int mNumPendingFinished = 0;
    private final AtomicFile mPendingFile;
    private final ArrayList<PendingOperation> mPendingOperations = new ArrayList();
    private final AtomicFile mStatisticsFile;
    private final AtomicFile mStatusFile;
    private final ArrayList<SyncHistoryItem> mSyncHistory = new ArrayList();
    private int mSyncRandomOffset;
    private OnSyncRequestListener mSyncRequestListener;
    private final SparseArray<SyncStatusInfo> mSyncStatus = new SparseArray();
    private int mYear;
    private int mYearInDays;

    static
    {
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "START";
        arrayOfString1[1] = "STOP";
        EVENTS = arrayOfString1;
        SYNC_CONNECTION_SETTING_CHANGED_INTENT = new Intent("com.android.sync.SYNC_CONN_STATUS_CHANGED");
        String[] arrayOfString2 = new String[5];
        arrayOfString2[0] = "SERVER";
        arrayOfString2[1] = "LOCAL";
        arrayOfString2[2] = "POLL";
        arrayOfString2[3] = "USER";
        arrayOfString2[4] = "PERIODIC";
        SOURCES = arrayOfString2;
        sAuthorityRenames = new HashMap();
        sAuthorityRenames.put("contacts", "com.android.contacts");
        sAuthorityRenames.put("calendar", "com.android.calendar");
    }

    private SyncStorageEngine(Context paramContext, File paramFile)
    {
        this.mContext = paramContext;
        sSyncStorageEngine = this;
        this.mCal = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
        File localFile = new File(new File(paramFile, "system"), "sync");
        localFile.mkdirs();
        this.mAccountInfoFile = new AtomicFile(new File(localFile, "accounts.xml"));
        this.mStatusFile = new AtomicFile(new File(localFile, "status.bin"));
        this.mPendingFile = new AtomicFile(new File(localFile, "pending.bin"));
        this.mStatisticsFile = new AtomicFile(new File(localFile, "stats.bin"));
        readAccountInfoLocked();
        readStatusLocked();
        readPendingOperationsLocked();
        readStatisticsLocked();
        readAndDeleteLegacyAccountInfoLocked();
        writeAccountInfoLocked();
        writeStatusLocked();
        writePendingOperationsLocked();
        writeStatisticsLocked();
    }

    // ERROR //
    private void appendPendingOperationLocked(PendingOperation paramPendingOperation)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 278	android/content/SyncStorageEngine:mPendingFile	Lcom/android/internal/os/AtomicFile;
        //     4: invokevirtual 317	com/android/internal/os/AtomicFile:openAppend	()Ljava/io/FileOutputStream;
        //     7: astore_3
        //     8: invokestatic 323	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     11: astore 9
        //     13: aload_0
        //     14: aload_1
        //     15: aload 9
        //     17: invokespecial 327	android/content/SyncStorageEngine:writePendingOperationLocked	(Landroid/content/SyncStorageEngine$PendingOperation;Landroid/os/Parcel;)V
        //     20: aload_3
        //     21: aload 9
        //     23: invokevirtual 331	android/os/Parcel:marshall	()[B
        //     26: invokevirtual 337	java/io/FileOutputStream:write	([B)V
        //     29: aload 9
        //     31: invokevirtual 340	android/os/Parcel:recycle	()V
        //     34: aload_3
        //     35: invokevirtual 343	java/io/FileOutputStream:close	()V
        //     38: return
        //     39: astore_2
        //     40: aload_0
        //     41: invokespecial 306	android/content/SyncStorageEngine:writePendingOperationsLocked	()V
        //     44: goto -6 -> 38
        //     47: astore 6
        //     49: ldc 76
        //     51: ldc_w 345
        //     54: aload 6
        //     56: invokestatic 351	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     59: pop
        //     60: aload_3
        //     61: invokevirtual 343	java/io/FileOutputStream:close	()V
        //     64: goto -26 -> 38
        //     67: astore 8
        //     69: goto -31 -> 38
        //     72: astore 4
        //     74: aload_3
        //     75: invokevirtual 343	java/io/FileOutputStream:close	()V
        //     78: aload 4
        //     80: athrow
        //     81: astore 5
        //     83: goto -5 -> 78
        //
        // Exception table:
        //     from	to	target	type
        //     0	8	39	java/io/IOException
        //     8	34	47	java/io/IOException
        //     34	38	67	java/io/IOException
        //     60	64	67	java/io/IOException
        //     8	34	72	finally
        //     49	60	72	finally
        //     74	78	81	java/io/IOException
    }

    public static boolean equals(Bundle paramBundle1, Bundle paramBundle2)
    {
        boolean bool = false;
        if (paramBundle1.size() != paramBundle2.size());
        while (true)
        {
            return bool;
            if (paramBundle1.isEmpty())
            {
                bool = true;
            }
            else
            {
                Iterator localIterator = paramBundle1.keySet().iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        String str = (String)localIterator.next();
                        if (!paramBundle2.containsKey(str))
                            break;
                        if (!paramBundle1.get(str).equals(paramBundle2.get(str)))
                            break;
                    }
                bool = true;
            }
        }
    }

    private static byte[] flattenBundle(Bundle paramBundle)
    {
        Parcel localParcel = Parcel.obtain();
        try
        {
            paramBundle.writeToParcel(localParcel, 0);
            byte[] arrayOfByte = localParcel.marshall();
            return arrayOfByte;
        }
        finally
        {
            localParcel.recycle();
        }
    }

    private AuthorityInfo getAuthorityLocked(Account paramAccount, int paramInt, String paramString1, String paramString2)
    {
        AccountAndUser localAccountAndUser = new AccountAndUser(paramAccount, paramInt);
        AccountInfo localAccountInfo = (AccountInfo)this.mAccounts.get(localAccountAndUser);
        AuthorityInfo localAuthorityInfo;
        if (localAccountInfo == null)
        {
            if ((paramString2 != null) && (Log.isLoggable("SyncManager", 2)))
                Log.v("SyncManager", paramString2 + ": unknown account " + localAccountAndUser);
            localAuthorityInfo = null;
        }
        while (true)
        {
            return localAuthorityInfo;
            localAuthorityInfo = (AuthorityInfo)localAccountInfo.authorities.get(paramString1);
            if (localAuthorityInfo == null)
            {
                if ((paramString2 != null) && (Log.isLoggable("SyncManager", 2)))
                    Log.v("SyncManager", paramString2 + ": unknown authority " + paramString1);
                localAuthorityInfo = null;
            }
        }
    }

    private int getCurrentDayLocked()
    {
        this.mCal.setTimeInMillis(System.currentTimeMillis());
        int i = this.mCal.get(6);
        if (this.mYear != this.mCal.get(1))
        {
            this.mYear = this.mCal.get(1);
            this.mCal.clear();
            this.mCal.set(1, this.mYear);
            this.mYearInDays = ((int)(this.mCal.getTimeInMillis() / 86400000L));
        }
        return i + this.mYearInDays;
    }

    static int getIntColumn(Cursor paramCursor, String paramString)
    {
        return paramCursor.getInt(paramCursor.getColumnIndex(paramString));
    }

    static long getLongColumn(Cursor paramCursor, String paramString)
    {
        return paramCursor.getLong(paramCursor.getColumnIndex(paramString));
    }

    private AuthorityInfo getOrCreateAuthorityLocked(Account paramAccount, int paramInt1, String paramString, int paramInt2, boolean paramBoolean)
    {
        AccountAndUser localAccountAndUser = new AccountAndUser(paramAccount, paramInt1);
        AccountInfo localAccountInfo = (AccountInfo)this.mAccounts.get(localAccountAndUser);
        if (localAccountInfo == null)
        {
            localAccountInfo = new AccountInfo(localAccountAndUser);
            this.mAccounts.put(localAccountAndUser, localAccountInfo);
        }
        AuthorityInfo localAuthorityInfo = (AuthorityInfo)localAccountInfo.authorities.get(paramString);
        if (localAuthorityInfo == null)
        {
            if (paramInt2 < 0)
            {
                paramInt2 = this.mNextAuthorityId;
                this.mNextAuthorityId = (1 + this.mNextAuthorityId);
                paramBoolean = true;
            }
            if (Log.isLoggable("SyncManager", 2))
                Log.v("SyncManager", "created a new AuthorityInfo for " + paramAccount + ", user " + paramInt1 + ", provider " + paramString);
            localAuthorityInfo = new AuthorityInfo(paramAccount, paramInt1, paramString, paramInt2);
            localAccountInfo.authorities.put(paramString, localAuthorityInfo);
            this.mAuthorities.put(paramInt2, localAuthorityInfo);
            if (paramBoolean)
                writeAccountInfoLocked();
        }
        return localAuthorityInfo;
    }

    private SyncStatusInfo getOrCreateSyncStatusLocked(int paramInt)
    {
        SyncStatusInfo localSyncStatusInfo = (SyncStatusInfo)this.mSyncStatus.get(paramInt);
        if (localSyncStatusInfo == null)
        {
            localSyncStatusInfo = new SyncStatusInfo(paramInt);
            this.mSyncStatus.put(paramInt, localSyncStatusInfo);
        }
        return localSyncStatusInfo;
    }

    public static SyncStorageEngine getSingleton()
    {
        if (sSyncStorageEngine == null)
            throw new IllegalStateException("not initialized");
        return sSyncStorageEngine;
    }

    public static void init(Context paramContext)
    {
        if (sSyncStorageEngine != null);
        while (true)
        {
            return;
            sSyncStorageEngine = new SyncStorageEngine(paramContext, Environment.getSecureDataDirectory());
        }
    }

    private boolean maybeMigrateSettingsForRenamedAuthorities()
    {
        boolean bool = false;
        ArrayList localArrayList = new ArrayList();
        int i = this.mAuthorities.size();
        int j = 0;
        if (j < i)
        {
            AuthorityInfo localAuthorityInfo2 = (AuthorityInfo)this.mAuthorities.valueAt(j);
            String str = (String)sAuthorityRenames.get(localAuthorityInfo2.authority);
            if (str == null);
            while (true)
            {
                j++;
                break;
                localArrayList.add(localAuthorityInfo2);
                if ((localAuthorityInfo2.enabled) && (getAuthorityLocked(localAuthorityInfo2.account, localAuthorityInfo2.userId, str, "cleanup") == null))
                {
                    getOrCreateAuthorityLocked(localAuthorityInfo2.account, localAuthorityInfo2.userId, str, -1, false).enabled = true;
                    bool = true;
                }
            }
        }
        Iterator localIterator = localArrayList.iterator();
        while (localIterator.hasNext())
        {
            AuthorityInfo localAuthorityInfo1 = (AuthorityInfo)localIterator.next();
            removeAuthorityLocked(localAuthorityInfo1.account, localAuthorityInfo1.userId, localAuthorityInfo1.authority, false);
            bool = true;
        }
        return bool;
    }

    public static SyncStorageEngine newTestInstance(Context paramContext)
    {
        return new SyncStorageEngine(paramContext, paramContext.getFilesDir());
    }

    private AuthorityInfo parseAuthority(XmlPullParser paramXmlPullParser, int paramInt)
    {
        AuthorityInfo localAuthorityInfo = null;
        int i = -1;
        try
        {
            int m = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "id"));
            i = m;
            if (i >= 0)
            {
                str1 = paramXmlPullParser.getAttributeValue(null, "authority");
                str2 = paramXmlPullParser.getAttributeValue(null, "enabled");
                str3 = paramXmlPullParser.getAttributeValue(null, "syncable");
                str4 = paramXmlPullParser.getAttributeValue(null, "account");
                String str5 = paramXmlPullParser.getAttributeValue(null, "type");
                str6 = paramXmlPullParser.getAttributeValue(null, "user");
                if (str6 == null)
                {
                    j = 0;
                    if (str5 == null)
                    {
                        str5 = "com.google";
                        str3 = "unknown";
                    }
                    localAuthorityInfo = (AuthorityInfo)this.mAuthorities.get(i);
                    if (localAuthorityInfo == null)
                    {
                        localAuthorityInfo = getOrCreateAuthorityLocked(new Account(str4, str5), j, str1, i, false);
                        if (paramInt > 0)
                            localAuthorityInfo.periodicSyncs.clear();
                    }
                    if (localAuthorityInfo == null)
                        break label298;
                    if ((str2 != null) && (!Boolean.parseBoolean(str2)))
                        break label261;
                    bool = true;
                    localAuthorityInfo.enabled = bool;
                    if (!"unknown".equals(str3))
                        break label267;
                    localAuthorityInfo.syncable = -1;
                }
            }
            else
            {
                return localAuthorityInfo;
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.e("SyncManager", "error parsing the id of the authority", localNumberFormatException);
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
            {
                String str1;
                String str2;
                String str3;
                String str4;
                String str6;
                Log.e("SyncManager", "the id of the authority is null", localNullPointerException);
                continue;
                int j = Integer.parseInt(str6);
                continue;
                label261: boolean bool = false;
                continue;
                label267: if ((str3 == null) || (Boolean.parseBoolean(str3)));
                for (int k = 1; ; k = 0)
                {
                    localAuthorityInfo.syncable = k;
                    break;
                }
                label298: Log.w("SyncManager", "Failure adding authority: account=" + str4 + " auth=" + str1 + " enabled=" + str2 + " syncable=" + str3);
            }
        }
    }

    private void parseExtra(XmlPullParser paramXmlPullParser, Pair<Bundle, Long> paramPair)
    {
        Bundle localBundle = (Bundle)paramPair.first;
        String str1 = paramXmlPullParser.getAttributeValue(null, "name");
        String str2 = paramXmlPullParser.getAttributeValue(null, "type");
        String str3 = paramXmlPullParser.getAttributeValue(null, "value1");
        String str4 = paramXmlPullParser.getAttributeValue(null, "value2");
        try
        {
            if ("long".equals(str2))
                localBundle.putLong(str1, Long.parseLong(str3));
            else if ("integer".equals(str2))
                localBundle.putInt(str1, Integer.parseInt(str3));
        }
        catch (NumberFormatException localNumberFormatException)
        {
            Log.e("SyncManager", "error parsing bundle value", localNumberFormatException);
            return;
            if ("double".equals(str2))
                localBundle.putDouble(str1, Double.parseDouble(str3));
        }
        catch (NullPointerException localNullPointerException)
        {
            Log.e("SyncManager", "error parsing bundle value", localNullPointerException);
        }
        if ("float".equals(str2))
            localBundle.putFloat(str1, Float.parseFloat(str3));
        else if ("boolean".equals(str2))
            localBundle.putBoolean(str1, Boolean.parseBoolean(str3));
        else if ("string".equals(str2))
            localBundle.putString(str1, str3);
        else if ("account".equals(str2))
            localBundle.putParcelable(str1, new Account(str3, str4));
    }

    private void parseListenForTickles(XmlPullParser paramXmlPullParser)
    {
        String str1 = paramXmlPullParser.getAttributeValue(null, "user");
        int i = 0;
        try
        {
            int j = Integer.parseInt(str1);
            i = j;
            String str2 = paramXmlPullParser.getAttributeValue(null, "enabled");
            if ((str2 == null) || (Boolean.parseBoolean(str2)))
            {
                bool = true;
                this.mMasterSyncAutomatically.put(i, Boolean.valueOf(bool));
                return;
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.e("SyncManager", "error parsing the user for listen-for-tickles", localNumberFormatException);
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
            {
                Log.e("SyncManager", "the user in listen-for-tickles is null", localNullPointerException);
                continue;
                boolean bool = false;
            }
        }
    }

    private Pair<Bundle, Long> parsePeriodicSync(XmlPullParser paramXmlPullParser, AuthorityInfo paramAuthorityInfo)
    {
        Pair localPair = null;
        Bundle localBundle = new Bundle();
        String str = paramXmlPullParser.getAttributeValue(null, "period");
        try
        {
            long l = Long.parseLong(str);
            localPair = Pair.create(localBundle, Long.valueOf(l));
            paramAuthorityInfo.periodicSyncs.add(localPair);
            return localPair;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.e("SyncManager", "error parsing the period of a periodic sync", localNumberFormatException);
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
                Log.e("SyncManager", "the period of a periodic sync is null", localNullPointerException);
        }
    }

    // ERROR //
    private void readAccountInfoLocked()
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_1
        //     3: aconst_null
        //     4: astore_2
        //     5: aload_0
        //     6: getfield 270	android/content/SyncStorageEngine:mAccountInfoFile	Lcom/android/internal/os/AtomicFile;
        //     9: invokevirtual 739	com/android/internal/os/AtomicFile:openRead	()Ljava/io/FileInputStream;
        //     12: astore_2
        //     13: invokestatic 745	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     16: astore 11
        //     18: aload 11
        //     20: aload_2
        //     21: aconst_null
        //     22: invokeinterface 749 3 0
        //     27: aload 11
        //     29: invokeinterface 752 1 0
        //     34: istore 12
        //     36: iload 12
        //     38: iconst_2
        //     39: if_icmpeq +15 -> 54
        //     42: aload 11
        //     44: invokeinterface 754 1 0
        //     49: istore 12
        //     51: goto -15 -> 36
        //     54: ldc_w 756
        //     57: aload 11
        //     59: invokeinterface 759 1 0
        //     64: invokevirtual 615	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     67: ifeq +254 -> 321
        //     70: aload 11
        //     72: aconst_null
        //     73: ldc 88
        //     75: invokeinterface 585 3 0
        //     80: astore 15
        //     82: aload 11
        //     84: aconst_null
        //     85: ldc_w 761
        //     88: invokeinterface 585 3 0
        //     93: astore 16
        //     95: aload 16
        //     97: ifnonnull +252 -> 349
        //     100: iconst_0
        //     101: istore 18
        //     103: aload 11
        //     105: aconst_null
        //     106: ldc 91
        //     108: invokeinterface 585 3 0
        //     113: astore 19
        //     115: aload 19
        //     117: ifnonnull +254 -> 371
        //     120: iconst_0
        //     121: istore 33
        //     123: aload_0
        //     124: aload_0
        //     125: getfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     128: iload 33
        //     130: invokestatic 767	java/lang/Math:max	(II)I
        //     133: putfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     136: aload 11
        //     138: aconst_null
        //     139: ldc 94
        //     141: invokeinterface 585 3 0
        //     146: astore 21
        //     148: aload 21
        //     150: ifnonnull +235 -> 385
        //     153: iconst_0
        //     154: istore 31
        //     156: aload_0
        //     157: iload 31
        //     159: putfield 769	android/content/SyncStorageEngine:mSyncRandomOffset	I
        //     162: aload_0
        //     163: getfield 769	android/content/SyncStorageEngine:mSyncRandomOffset	I
        //     166: ifne +23 -> 189
        //     169: aload_0
        //     170: new 771	java/util/Random
        //     173: dup
        //     174: invokestatic 446	java/lang/System:currentTimeMillis	()J
        //     177: invokespecial 773	java/util/Random:<init>	(J)V
        //     180: ldc_w 774
        //     183: invokevirtual 777	java/util/Random:nextInt	(I)I
        //     186: putfield 769	android/content/SyncStorageEngine:mSyncRandomOffset	I
        //     189: aload_0
        //     190: getfield 230	android/content/SyncStorageEngine:mMasterSyncAutomatically	Landroid/util/SparseArray;
        //     193: astore 23
        //     195: aload 15
        //     197: ifnull +452 -> 649
        //     200: aload 15
        //     202: invokestatic 614	java/lang/Boolean:parseBoolean	(Ljava/lang/String;)Z
        //     205: ifeq +247 -> 452
        //     208: goto +441 -> 649
        //     211: aload 23
        //     213: iconst_0
        //     214: iload 24
        //     216: invokestatic 713	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     219: invokevirtual 506	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     222: aload 11
        //     224: invokeinterface 754 1 0
        //     229: istore 25
        //     231: aconst_null
        //     232: astore 26
        //     234: aconst_null
        //     235: astore 27
        //     237: iload 25
        //     239: iconst_2
        //     240: if_icmpne +62 -> 302
        //     243: aload 11
        //     245: invokeinterface 759 1 0
        //     250: astore 29
        //     252: aload 11
        //     254: invokeinterface 780 1 0
        //     259: iconst_2
        //     260: if_icmpne +257 -> 517
        //     263: ldc_w 591
        //     266: aload 29
        //     268: invokevirtual 615	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     271: ifeq +187 -> 458
        //     274: aload_0
        //     275: aload 11
        //     277: iload 18
        //     279: invokespecial 782	android/content/SyncStorageEngine:parseAuthority	(Lorg/xmlpull/v1/XmlPullParser;I)Landroid/content/SyncStorageEngine$AuthorityInfo;
        //     282: astore 26
        //     284: aconst_null
        //     285: astore 27
        //     287: aload 26
        //     289: getfield 785	android/content/SyncStorageEngine$AuthorityInfo:ident	I
        //     292: iload_1
        //     293: if_icmple +9 -> 302
        //     296: aload 26
        //     298: getfield 785	android/content/SyncStorageEngine$AuthorityInfo:ident	I
        //     301: istore_1
        //     302: aload 11
        //     304: invokeinterface 754 1 0
        //     309: istore 28
        //     311: iload 28
        //     313: istore 25
        //     315: iload 25
        //     317: iconst_1
        //     318: if_icmpne -81 -> 237
        //     321: aload_0
        //     322: iload_1
        //     323: iconst_1
        //     324: iadd
        //     325: aload_0
        //     326: getfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     329: invokestatic 767	java/lang/Math:max	(II)I
        //     332: putfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     335: aload_2
        //     336: ifnull +7 -> 343
        //     339: aload_2
        //     340: invokevirtual 788	java/io/FileInputStream:close	()V
        //     343: aload_0
        //     344: invokespecial 790	android/content/SyncStorageEngine:maybeMigrateSettingsForRenamedAuthorities	()Z
        //     347: pop
        //     348: return
        //     349: aload 16
        //     351: invokestatic 590	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     354: istore 34
        //     356: iload 34
        //     358: istore 18
        //     360: goto -257 -> 103
        //     363: astore 17
        //     365: iconst_0
        //     366: istore 18
        //     368: goto -265 -> 103
        //     371: aload 19
        //     373: invokestatic 590	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     376: istore 32
        //     378: iload 32
        //     380: istore 33
        //     382: goto -259 -> 123
        //     385: aload 21
        //     387: invokestatic 590	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     390: istore 30
        //     392: iload 30
        //     394: istore 31
        //     396: goto -240 -> 156
        //     399: astore 22
        //     401: aload_0
        //     402: iconst_0
        //     403: putfield 769	android/content/SyncStorageEngine:mSyncRandomOffset	I
        //     406: goto -244 -> 162
        //     409: astore 9
        //     411: ldc 76
        //     413: ldc_w 792
        //     416: aload 9
        //     418: invokestatic 351	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     421: pop
        //     422: aload_0
        //     423: iload_1
        //     424: iconst_1
        //     425: iadd
        //     426: aload_0
        //     427: getfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     430: invokestatic 767	java/lang/Math:max	(II)I
        //     433: putfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     436: aload_2
        //     437: ifnull -89 -> 348
        //     440: aload_2
        //     441: invokevirtual 788	java/io/FileInputStream:close	()V
        //     444: goto -96 -> 348
        //     447: astore 7
        //     449: goto -101 -> 348
        //     452: iconst_0
        //     453: istore 24
        //     455: goto -244 -> 211
        //     458: ldc 100
        //     460: aload 29
        //     462: invokevirtual 615	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     465: ifeq -163 -> 302
        //     468: aload_0
        //     469: aload 11
        //     471: invokespecial 794	android/content/SyncStorageEngine:parseListenForTickles	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     474: goto -172 -> 302
        //     477: astore 5
        //     479: aload_2
        //     480: ifnonnull +140 -> 620
        //     483: ldc 76
        //     485: ldc_w 796
        //     488: invokestatic 799	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     491: pop
        //     492: aload_0
        //     493: iload_1
        //     494: iconst_1
        //     495: iadd
        //     496: aload_0
        //     497: getfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     500: invokestatic 767	java/lang/Math:max	(II)I
        //     503: putfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     506: aload_2
        //     507: ifnull -159 -> 348
        //     510: aload_2
        //     511: invokevirtual 788	java/io/FileInputStream:close	()V
        //     514: goto -166 -> 348
        //     517: aload 11
        //     519: invokeinterface 780 1 0
        //     524: iconst_3
        //     525: if_icmpne +32 -> 557
        //     528: ldc_w 801
        //     531: aload 29
        //     533: invokevirtual 615	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     536: ifeq -234 -> 302
        //     539: aload 26
        //     541: ifnull -239 -> 302
        //     544: aload_0
        //     545: aload 11
        //     547: aload 26
        //     549: invokespecial 803	android/content/SyncStorageEngine:parsePeriodicSync	(Lorg/xmlpull/v1/XmlPullParser;Landroid/content/SyncStorageEngine$AuthorityInfo;)Landroid/util/Pair;
        //     552: astore 27
        //     554: goto -252 -> 302
        //     557: aload 11
        //     559: invokeinterface 780 1 0
        //     564: iconst_4
        //     565: if_icmpne -263 -> 302
        //     568: aload 27
        //     570: ifnull -268 -> 302
        //     573: ldc_w 805
        //     576: aload 29
        //     578: invokevirtual 615	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     581: ifeq -279 -> 302
        //     584: aload_0
        //     585: aload 11
        //     587: aload 27
        //     589: invokespecial 807	android/content/SyncStorageEngine:parseExtra	(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/Pair;)V
        //     592: goto -290 -> 302
        //     595: astore_3
        //     596: aload_0
        //     597: iload_1
        //     598: iconst_1
        //     599: iadd
        //     600: aload_0
        //     601: getfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     604: invokestatic 767	java/lang/Math:max	(II)I
        //     607: putfield 222	android/content/SyncStorageEngine:mNextAuthorityId	I
        //     610: aload_2
        //     611: ifnull +7 -> 618
        //     614: aload_2
        //     615: invokevirtual 788	java/io/FileInputStream:close	()V
        //     618: aload_3
        //     619: athrow
        //     620: ldc 76
        //     622: ldc_w 792
        //     625: aload 5
        //     627: invokestatic 351	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     630: pop
        //     631: goto -139 -> 492
        //     634: astore 4
        //     636: goto -18 -> 618
        //     639: astore 14
        //     641: goto -298 -> 343
        //     644: astore 20
        //     646: goto -510 -> 136
        //     649: iconst_1
        //     650: istore 24
        //     652: goto -441 -> 211
        //
        // Exception table:
        //     from	to	target	type
        //     349	356	363	java/lang/NumberFormatException
        //     156	162	399	java/lang/NumberFormatException
        //     385	392	399	java/lang/NumberFormatException
        //     5	115	409	org/xmlpull/v1/XmlPullParserException
        //     123	136	409	org/xmlpull/v1/XmlPullParserException
        //     136	148	409	org/xmlpull/v1/XmlPullParserException
        //     156	162	409	org/xmlpull/v1/XmlPullParserException
        //     162	311	409	org/xmlpull/v1/XmlPullParserException
        //     349	356	409	org/xmlpull/v1/XmlPullParserException
        //     371	378	409	org/xmlpull/v1/XmlPullParserException
        //     385	392	409	org/xmlpull/v1/XmlPullParserException
        //     401	406	409	org/xmlpull/v1/XmlPullParserException
        //     458	474	409	org/xmlpull/v1/XmlPullParserException
        //     517	592	409	org/xmlpull/v1/XmlPullParserException
        //     440	444	447	java/io/IOException
        //     510	514	447	java/io/IOException
        //     5	115	477	java/io/IOException
        //     123	136	477	java/io/IOException
        //     136	148	477	java/io/IOException
        //     156	162	477	java/io/IOException
        //     162	311	477	java/io/IOException
        //     349	356	477	java/io/IOException
        //     371	378	477	java/io/IOException
        //     385	392	477	java/io/IOException
        //     401	406	477	java/io/IOException
        //     458	474	477	java/io/IOException
        //     517	592	477	java/io/IOException
        //     5	115	595	finally
        //     123	136	595	finally
        //     136	148	595	finally
        //     156	162	595	finally
        //     162	311	595	finally
        //     349	356	595	finally
        //     371	378	595	finally
        //     385	392	595	finally
        //     401	406	595	finally
        //     411	422	595	finally
        //     458	474	595	finally
        //     483	492	595	finally
        //     517	592	595	finally
        //     620	631	595	finally
        //     614	618	634	java/io/IOException
        //     339	343	639	java/io/IOException
        //     123	136	644	java/lang/NumberFormatException
        //     371	378	644	java/lang/NumberFormatException
    }

    private void readAndDeleteLegacyAccountInfoLocked()
    {
        File localFile = this.mContext.getDatabasePath("syncmanager.db");
        if (!localFile.exists());
        while (true)
        {
            return;
            String str1 = localFile.getPath();
            Object localObject = null;
            try
            {
                SQLiteDatabase localSQLiteDatabase = SQLiteDatabase.openDatabase(str1, null, 1);
                localObject = localSQLiteDatabase;
                label37: if (localObject == null)
                    continue;
                int i;
                Cursor localCursor1;
                label305: String str6;
                label358: SyncStatusInfo localSyncStatusInfo;
                if (localObject.getVersion() >= 11)
                {
                    i = 1;
                    SQLiteQueryBuilder localSQLiteQueryBuilder1 = new SQLiteQueryBuilder();
                    localSQLiteQueryBuilder1.setTables("stats, status");
                    HashMap localHashMap = new HashMap();
                    localHashMap.put("_id", "status._id as _id");
                    localHashMap.put("account", "stats.account as account");
                    if (i != 0)
                        localHashMap.put("account_type", "stats.account_type as account_type");
                    localHashMap.put("authority", "stats.authority as authority");
                    localHashMap.put("totalElapsedTime", "totalElapsedTime");
                    localHashMap.put("numSyncs", "numSyncs");
                    localHashMap.put("numSourceLocal", "numSourceLocal");
                    localHashMap.put("numSourcePoll", "numSourcePoll");
                    localHashMap.put("numSourceServer", "numSourceServer");
                    localHashMap.put("numSourceUser", "numSourceUser");
                    localHashMap.put("lastSuccessSource", "lastSuccessSource");
                    localHashMap.put("lastSuccessTime", "lastSuccessTime");
                    localHashMap.put("lastFailureSource", "lastFailureSource");
                    localHashMap.put("lastFailureTime", "lastFailureTime");
                    localHashMap.put("lastFailureMesg", "lastFailureMesg");
                    localHashMap.put("pending", "pending");
                    localSQLiteQueryBuilder1.setProjectionMap(localHashMap);
                    localSQLiteQueryBuilder1.appendWhere("stats._id = status.stats_id");
                    localCursor1 = localSQLiteQueryBuilder1.query(localObject, null, null, null, null, null, null);
                    AuthorityInfo localAuthorityInfo2;
                    do
                    {
                        if (!localCursor1.moveToNext())
                            break label749;
                        String str5 = localCursor1.getString(localCursor1.getColumnIndex("account"));
                        if (i == 0)
                            break;
                        str6 = localCursor1.getString(localCursor1.getColumnIndex("account_type"));
                        if (str6 == null)
                            str6 = "com.google";
                        String str7 = localCursor1.getString(localCursor1.getColumnIndex("authority"));
                        localAuthorityInfo2 = getOrCreateAuthorityLocked(new Account(str5, str6), 0, str7, -1, false);
                    }
                    while (localAuthorityInfo2 == null);
                    int k = this.mSyncStatus.size();
                    int m = 0;
                    localSyncStatusInfo = null;
                    while (k > 0)
                    {
                        k--;
                        localSyncStatusInfo = (SyncStatusInfo)this.mSyncStatus.valueAt(k);
                        if (localSyncStatusInfo.authorityId == localAuthorityInfo2.ident)
                            m = 1;
                    }
                    if (m == 0)
                    {
                        int i7 = localAuthorityInfo2.ident;
                        localSyncStatusInfo = new SyncStatusInfo(i7);
                        this.mSyncStatus.put(localAuthorityInfo2.ident, localSyncStatusInfo);
                    }
                    long l1 = getLongColumn(localCursor1, "totalElapsedTime");
                    localSyncStatusInfo.totalElapsedTime = l1;
                    int n = getIntColumn(localCursor1, "numSyncs");
                    localSyncStatusInfo.numSyncs = n;
                    int i1 = getIntColumn(localCursor1, "numSourceLocal");
                    localSyncStatusInfo.numSourceLocal = i1;
                    int i2 = getIntColumn(localCursor1, "numSourcePoll");
                    localSyncStatusInfo.numSourcePoll = i2;
                    int i3 = getIntColumn(localCursor1, "numSourceServer");
                    localSyncStatusInfo.numSourceServer = i3;
                    int i4 = getIntColumn(localCursor1, "numSourceUser");
                    localSyncStatusInfo.numSourceUser = i4;
                    localSyncStatusInfo.numSourcePeriodic = 0;
                    int i5 = getIntColumn(localCursor1, "lastSuccessSource");
                    localSyncStatusInfo.lastSuccessSource = i5;
                    long l2 = getLongColumn(localCursor1, "lastSuccessTime");
                    localSyncStatusInfo.lastSuccessTime = l2;
                    int i6 = getIntColumn(localCursor1, "lastFailureSource");
                    localSyncStatusInfo.lastFailureSource = i6;
                    long l3 = getLongColumn(localCursor1, "lastFailureTime");
                    localSyncStatusInfo.lastFailureTime = l3;
                    String str8 = localCursor1.getString(localCursor1.getColumnIndex("lastFailureMesg"));
                    localSyncStatusInfo.lastFailureMesg = str8;
                    if (getIntColumn(localCursor1, "pending") == 0)
                        break label743;
                }
                label743: for (boolean bool3 = true; ; bool3 = false)
                {
                    localSyncStatusInfo.pending = bool3;
                    break label305;
                    i = 0;
                    break;
                    str6 = null;
                    break label358;
                }
                label749: localCursor1.close();
                SQLiteQueryBuilder localSQLiteQueryBuilder2 = new SQLiteQueryBuilder();
                localSQLiteQueryBuilder2.setTables("settings");
                Cursor localCursor2 = localSQLiteQueryBuilder2.query(localObject, null, null, null, null, null, null);
                while (localCursor2.moveToNext())
                {
                    String str2 = localCursor2.getString(localCursor2.getColumnIndex("name"));
                    String str3 = localCursor2.getString(localCursor2.getColumnIndex("value"));
                    if (str2 != null)
                    {
                        if (str2.equals("listen_for_tickles"))
                        {
                            if ((str3 == null) || (Boolean.parseBoolean(str3)));
                            for (boolean bool2 = true; ; bool2 = false)
                            {
                                setMasterSyncAutomatically(bool2, 0);
                                break;
                            }
                        }
                        if (str2.startsWith("sync_provider_"))
                        {
                            String str4 = str2.substring("sync_provider_".length(), str2.length());
                            int j = this.mAuthorities.size();
                            label921: AuthorityInfo localAuthorityInfo1;
                            while (j > 0)
                            {
                                j--;
                                localAuthorityInfo1 = (AuthorityInfo)this.mAuthorities.valueAt(j);
                                if (localAuthorityInfo1.authority.equals(str4))
                                    if ((str3 != null) && (!Boolean.parseBoolean(str3)))
                                        break label988;
                            }
                            label988: for (boolean bool1 = true; ; bool1 = false)
                            {
                                localAuthorityInfo1.enabled = bool1;
                                localAuthorityInfo1.syncable = 1;
                                break label921;
                                break;
                            }
                        }
                    }
                }
                localCursor2.close();
                localObject.close();
                new File(str1).delete();
            }
            catch (SQLiteException localSQLiteException)
            {
                break label37;
            }
        }
    }

    private void readPendingOperationsLocked()
    {
        try
        {
            byte[] arrayOfByte1 = this.mPendingFile.readFully();
            Parcel localParcel = Parcel.obtain();
            localParcel.unmarshall(arrayOfByte1, 0, arrayOfByte1.length);
            localParcel.setDataPosition(0);
            int i = localParcel.dataSize();
            while (true)
            {
                if (localParcel.dataPosition() >= i)
                    return;
                int j = localParcel.readInt();
                if ((j != 2) && (j != 1))
                {
                    Log.w("SyncManager", "Unknown pending operation version " + j + "; dropping all ops");
                    return;
                }
                int k = localParcel.readInt();
                int m = localParcel.readInt();
                byte[] arrayOfByte2 = localParcel.createByteArray();
                if (j != 2)
                    break label245;
                if (localParcel.readInt() == 0)
                    break;
                bool = true;
                AuthorityInfo localAuthorityInfo = (AuthorityInfo)this.mAuthorities.get(k);
                if (localAuthorityInfo != null)
                {
                    if (arrayOfByte2 == null)
                        break label251;
                    localBundle = unflattenBundle(arrayOfByte2);
                    PendingOperation localPendingOperation = new PendingOperation(localAuthorityInfo.account, localAuthorityInfo.userId, m, localAuthorityInfo.authority, localBundle, bool);
                    localPendingOperation.authorityId = k;
                    localPendingOperation.flatExtras = arrayOfByte2;
                    this.mPendingOperations.add(localPendingOperation);
                }
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.i("SyncManager", "No initial pending operations");
                break;
                boolean bool = false;
                continue;
                label245: bool = false;
                continue;
                label251: Bundle localBundle = new Bundle();
            }
        }
    }

    private void readStatisticsLocked()
    {
        try
        {
            byte[] arrayOfByte = this.mStatisticsFile.readFully();
            Parcel localParcel = Parcel.obtain();
            localParcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
            localParcel.setDataPosition(0);
            int i = 0;
            int j;
            while (true)
            {
                j = localParcel.readInt();
                if (j == 0)
                    break label186;
                if ((j != 101) && (j != 100))
                    break;
                int k = localParcel.readInt();
                if (j == 100)
                    k = 14245 + (k - 2009);
                DayStats localDayStats = new DayStats(k);
                localDayStats.successCount = localParcel.readInt();
                localDayStats.successTime = localParcel.readLong();
                localDayStats.failureCount = localParcel.readInt();
                localDayStats.failureTime = localParcel.readLong();
                if (i < this.mDayStats.length)
                {
                    this.mDayStats[i] = localDayStats;
                    i++;
                }
            }
            Log.w("SyncManager", "Unknown stats token: " + j);
            label186: return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.i("SyncManager", "No initial statistics");
        }
    }

    private void readStatusLocked()
    {
        int i;
        try
        {
            byte[] arrayOfByte = this.mStatusFile.readFully();
            Parcel localParcel = Parcel.obtain();
            localParcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
            localParcel.setDataPosition(0);
            while (true)
            {
                i = localParcel.readInt();
                if (i == 0)
                    break;
                if (i != 100)
                    break label107;
                SyncStatusInfo localSyncStatusInfo = new SyncStatusInfo(localParcel);
                if (this.mAuthorities.indexOfKey(localSyncStatusInfo.authorityId) >= 0)
                {
                    localSyncStatusInfo.pending = false;
                    this.mSyncStatus.put(localSyncStatusInfo.authorityId, localSyncStatusInfo);
                }
            }
        }
        catch (IOException localIOException)
        {
            Log.i("SyncManager", "No initial status");
        }
        while (true)
        {
            return;
            label107: Log.w("SyncManager", "Unknown status token: " + i);
        }
    }

    private void removeAuthorityLocked(Account paramAccount, int paramInt, String paramString, boolean paramBoolean)
    {
        AccountInfo localAccountInfo = (AccountInfo)this.mAccounts.get(new AccountAndUser(paramAccount, paramInt));
        if (localAccountInfo != null)
        {
            AuthorityInfo localAuthorityInfo = (AuthorityInfo)localAccountInfo.authorities.remove(paramString);
            if (localAuthorityInfo != null)
            {
                this.mAuthorities.remove(localAuthorityInfo.ident);
                if (paramBoolean)
                    writeAccountInfoLocked();
            }
        }
    }

    private void reportChange(int paramInt)
    {
        while (true)
        {
            int j;
            Object localObject3;
            synchronized (this.mAuthorities)
            {
                int i = this.mChangeListeners.beginBroadcast();
                j = i;
                localObject3 = null;
                if (j > 0)
                    j--;
            }
            try
            {
                if ((paramInt & ((Integer)this.mChangeListeners.getBroadcastCookie(j)).intValue()) == 0)
                    continue;
                if (localObject3 == null)
                {
                    localObject4 = new ArrayList(j);
                    ((ArrayList)localObject4).add(this.mChangeListeners.getBroadcastItem(j));
                    localObject3 = localObject4;
                    continue;
                    this.mChangeListeners.finishBroadcast();
                    if (Log.isLoggable("SyncManager", 2))
                        Log.v("SyncManager", "reportChange " + paramInt + " to: " + localObject3);
                    if (localObject3 != null)
                    {
                        int k = localObject3.size();
                        while (k > 0)
                        {
                            k--;
                            try
                            {
                                ((ISyncStatusObserver)localObject3.get(k)).onStatusChanged(paramInt);
                            }
                            catch (RemoteException localRemoteException)
                            {
                            }
                            continue;
                            localObject1 = finally;
                            throw localObject1;
                        }
                    }
                    return;
                }
            }
            finally
            {
                while (true)
                {
                    continue;
                    Object localObject4 = localObject3;
                }
            }
        }
    }

    private void requestSync(Account paramAccount, int paramInt, String paramString, Bundle paramBundle)
    {
        if ((Process.myUid() == 1000) && (this.mSyncRequestListener != null))
            this.mSyncRequestListener.onSyncRequest(paramAccount, paramInt, paramString, paramBundle);
        while (true)
        {
            return;
            ContentResolver.requestSync(paramAccount, paramString, paramBundle);
        }
    }

    private static Bundle unflattenBundle(byte[] paramArrayOfByte)
    {
        Parcel localParcel = Parcel.obtain();
        try
        {
            localParcel.unmarshall(paramArrayOfByte, 0, paramArrayOfByte.length);
            localParcel.setDataPosition(0);
            Bundle localBundle2 = localParcel.readBundle();
            localBundle1 = localBundle2;
            return localBundle1;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Bundle localBundle1 = new Bundle();
        }
        finally
        {
            localParcel.recycle();
        }
    }

    // ERROR //
    private void updateOrRemovePeriodicSync(Account paramAccount, int paramInt, String paramString, Bundle paramBundle, long paramLong, boolean paramBoolean)
    {
        // Byte code:
        //     0: lload 5
        //     2: lconst_0
        //     3: lcmp
        //     4: ifgt +6 -> 10
        //     7: lconst_0
        //     8: lstore 5
        //     10: aload 4
        //     12: ifnonnull +12 -> 24
        //     15: new 355	android/os/Bundle
        //     18: dup
        //     19: invokespecial 720	android/os/Bundle:<init>	()V
        //     22: astore 4
        //     24: ldc 76
        //     26: iconst_2
        //     27: invokestatic 414	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     30: ifeq +71 -> 101
        //     33: ldc 76
        //     35: new 416	java/lang/StringBuilder
        //     38: dup
        //     39: invokespecial 417	java/lang/StringBuilder:<init>	()V
        //     42: ldc_w 1090
        //     45: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload_1
        //     49: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     52: ldc_w 495
        //     55: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     58: iload_2
        //     59: invokevirtual 498	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     62: ldc_w 500
        //     65: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: aload_3
        //     69: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     72: ldc_w 1092
        //     75: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     78: lload 5
        //     80: invokevirtual 1095	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     83: ldc_w 1097
        //     86: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     89: aload 4
        //     91: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     94: invokevirtual 430	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     97: invokestatic 434	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     100: pop
        //     101: aload_0
        //     102: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     105: astore 8
        //     107: aload 8
        //     109: monitorenter
        //     110: aload_0
        //     111: aload_1
        //     112: iload_2
        //     113: aload_3
        //     114: bipush 255
        //     116: iconst_0
        //     117: invokespecial 559	android/content/SyncStorageEngine:getOrCreateAuthorityLocked	(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;
        //     120: astore 11
        //     122: iload 7
        //     124: ifeq +203 -> 327
        //     127: iconst_0
        //     128: istore 16
        //     130: iconst_0
        //     131: istore 17
        //     133: aload 11
        //     135: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     138: invokevirtual 1057	java/util/ArrayList:size	()I
        //     141: istore 18
        //     143: iload 17
        //     145: iload 18
        //     147: if_icmpge +99 -> 246
        //     150: aload 11
        //     152: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     155: iload 17
        //     157: invokevirtual 1058	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     160: checkcast 638	android/util/Pair
        //     163: astore 22
        //     165: aload 22
        //     167: getfield 642	android/util/Pair:first	Ljava/lang/Object;
        //     170: checkcast 355	android/os/Bundle
        //     173: aload 4
        //     175: invokestatic 1099	android/content/SyncStorageEngine:equals	(Landroid/os/Bundle;Landroid/os/Bundle;)Z
        //     178: ifeq +143 -> 321
        //     181: aload 22
        //     183: getfield 1102	android/util/Pair:second	Ljava/lang/Object;
        //     186: checkcast 652	java/lang/Long
        //     189: invokevirtual 1105	java/lang/Long:longValue	()J
        //     192: lstore 23
        //     194: lload 23
        //     196: lload 5
        //     198: lcmp
        //     199: ifne +15 -> 214
        //     202: aload_0
        //     203: invokespecial 300	android/content/SyncStorageEngine:writeAccountInfoLocked	()V
        //     206: aload_0
        //     207: invokespecial 303	android/content/SyncStorageEngine:writeStatusLocked	()V
        //     210: aload 8
        //     212: monitorexit
        //     213: return
        //     214: aload 11
        //     216: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     219: astore 25
        //     221: lload 5
        //     223: invokestatic 725	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     226: astore 26
        //     228: aload 25
        //     230: iload 17
        //     232: aload 4
        //     234: aload 26
        //     236: invokestatic 729	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
        //     239: invokevirtual 1108	java/util/ArrayList:set	(ILjava/lang/Object;)Ljava/lang/Object;
        //     242: pop
        //     243: iconst_1
        //     244: istore 16
        //     246: iload 16
        //     248: ifne +54 -> 302
        //     251: aload 11
        //     253: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     256: astore 19
        //     258: lload 5
        //     260: invokestatic 725	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     263: astore 20
        //     265: aload 19
        //     267: aload 4
        //     269: aload 20
        //     271: invokestatic 729	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
        //     274: invokevirtual 544	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     277: pop
        //     278: aload_0
        //     279: aload 11
        //     281: getfield 785	android/content/SyncStorageEngine$AuthorityInfo:ident	I
        //     284: invokespecial 1110	android/content/SyncStorageEngine:getOrCreateSyncStatusLocked	(I)Landroid/content/SyncStatusInfo;
        //     287: bipush 255
        //     289: aload 11
        //     291: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     294: invokevirtual 1057	java/util/ArrayList:size	()I
        //     297: iadd
        //     298: lconst_0
        //     299: invokevirtual 1114	android/content/SyncStatusInfo:setPeriodicSyncTime	(IJ)V
        //     302: aload_0
        //     303: invokespecial 300	android/content/SyncStorageEngine:writeAccountInfoLocked	()V
        //     306: aload_0
        //     307: invokespecial 303	android/content/SyncStorageEngine:writeStatusLocked	()V
        //     310: aload 8
        //     312: monitorexit
        //     313: aload_0
        //     314: iconst_1
        //     315: invokespecial 1116	android/content/SyncStorageEngine:reportChange	(I)V
        //     318: goto -105 -> 213
        //     321: iinc 17 1
        //     324: goto -181 -> 143
        //     327: aload_0
        //     328: getfield 213	android/content/SyncStorageEngine:mSyncStatus	Landroid/util/SparseArray;
        //     331: aload 11
        //     333: getfield 785	android/content/SyncStorageEngine$AuthorityInfo:ident	I
        //     336: invokevirtual 511	android/util/SparseArray:get	(I)Ljava/lang/Object;
        //     339: checkcast 513	android/content/SyncStatusInfo
        //     342: astore 12
        //     344: iconst_0
        //     345: istore 13
        //     347: aload 11
        //     349: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     352: invokevirtual 560	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     355: astore 14
        //     357: iconst_0
        //     358: istore 15
        //     360: aload 14
        //     362: invokeinterface 377 1 0
        //     367: ifeq +79 -> 446
        //     370: aload 14
        //     372: invokeinterface 381 1 0
        //     377: checkcast 638	android/util/Pair
        //     380: getfield 642	android/util/Pair:first	Ljava/lang/Object;
        //     383: checkcast 355	android/os/Bundle
        //     386: aload 4
        //     388: invokestatic 1099	android/content/SyncStorageEngine:equals	(Landroid/os/Bundle;Landroid/os/Bundle;)Z
        //     391: ifeq +49 -> 440
        //     394: aload 14
        //     396: invokeinterface 1118 1 0
        //     401: iconst_1
        //     402: istore 13
        //     404: aload 12
        //     406: ifnull -46 -> 360
        //     409: aload 12
        //     411: iload 15
        //     413: invokevirtual 1121	android/content/SyncStatusInfo:removePeriodicSyncTime	(I)V
        //     416: goto -56 -> 360
        //     419: astore 9
        //     421: aload_0
        //     422: invokespecial 300	android/content/SyncStorageEngine:writeAccountInfoLocked	()V
        //     425: aload_0
        //     426: invokespecial 303	android/content/SyncStorageEngine:writeStatusLocked	()V
        //     429: aload 9
        //     431: athrow
        //     432: astore 10
        //     434: aload 8
        //     436: monitorexit
        //     437: aload 10
        //     439: athrow
        //     440: iinc 15 1
        //     443: goto -83 -> 360
        //     446: iload 13
        //     448: ifne -146 -> 302
        //     451: aload_0
        //     452: invokespecial 300	android/content/SyncStorageEngine:writeAccountInfoLocked	()V
        //     455: aload_0
        //     456: invokespecial 303	android/content/SyncStorageEngine:writeStatusLocked	()V
        //     459: aload 8
        //     461: monitorexit
        //     462: goto -249 -> 213
        //
        // Exception table:
        //     from	to	target	type
        //     110	194	419	finally
        //     214	302	419	finally
        //     327	416	419	finally
        //     202	213	432	finally
        //     302	313	432	finally
        //     421	437	432	finally
        //     451	462	432	finally
    }

    private void writeAccountInfoLocked()
    {
        FileOutputStream localFileOutputStream = null;
        FastXmlSerializer localFastXmlSerializer;
        int m;
        AuthorityInfo localAuthorityInfo;
        label407: label417: Object localObject;
        try
        {
            localFileOutputStream = this.mAccountInfoFile.startWrite();
            localFastXmlSerializer = new FastXmlSerializer();
            localFastXmlSerializer.setOutput(localFileOutputStream, "utf-8");
            localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
            localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            localFastXmlSerializer.startTag(null, "accounts");
            localFastXmlSerializer.attribute(null, "version", Integer.toString(2));
            localFastXmlSerializer.attribute(null, "nextAuthorityId", Integer.toString(this.mNextAuthorityId));
            localFastXmlSerializer.attribute(null, "offsetInSeconds", Integer.toString(this.mSyncRandomOffset));
            int i = this.mMasterSyncAutomatically.size();
            for (int j = 0; j < i; j++)
            {
                int n = this.mMasterSyncAutomatically.keyAt(j);
                Boolean localBoolean = (Boolean)this.mMasterSyncAutomatically.valueAt(j);
                localFastXmlSerializer.startTag(null, "listenForTickles");
                localFastXmlSerializer.attribute(null, "user", Integer.toString(n));
                localFastXmlSerializer.attribute(null, "enabled", Boolean.toString(localBoolean.booleanValue()));
                localFastXmlSerializer.endTag(null, "listenForTickles");
            }
            int k = this.mAuthorities.size();
            m = 0;
            if (m >= k)
                break label980;
            localAuthorityInfo = (AuthorityInfo)this.mAuthorities.valueAt(m);
            localFastXmlSerializer.startTag(null, "authority");
            localFastXmlSerializer.attribute(null, "id", Integer.toString(localAuthorityInfo.ident));
            localFastXmlSerializer.attribute(null, "account", localAuthorityInfo.account.name);
            localFastXmlSerializer.attribute(null, "user", Integer.toString(localAuthorityInfo.userId));
            localFastXmlSerializer.attribute(null, "type", localAuthorityInfo.account.type);
            localFastXmlSerializer.attribute(null, "authority", localAuthorityInfo.authority);
            localFastXmlSerializer.attribute(null, "enabled", Boolean.toString(localAuthorityInfo.enabled));
            if (localAuthorityInfo.syncable >= 0)
                break label635;
            localFastXmlSerializer.attribute(null, "syncable", "unknown");
            Iterator localIterator1 = localAuthorityInfo.periodicSyncs.iterator();
            if (!localIterator1.hasNext())
                break label962;
            Pair localPair = (Pair)localIterator1.next();
            localFastXmlSerializer.startTag(null, "periodicSync");
            localFastXmlSerializer.attribute(null, "period", Long.toString(((Long)localPair.second).longValue()));
            Bundle localBundle = (Bundle)localPair.first;
            Iterator localIterator2 = localBundle.keySet().iterator();
            while (true)
            {
                if (!localIterator2.hasNext())
                    break label947;
                String str = (String)localIterator2.next();
                localFastXmlSerializer.startTag(null, "extra");
                localFastXmlSerializer.attribute(null, "name", str);
                localObject = localBundle.get(str);
                if (!(localObject instanceof Long))
                    break;
                localFastXmlSerializer.attribute(null, "type", "long");
                localFastXmlSerializer.attribute(null, "value1", localObject.toString());
                label596: localFastXmlSerializer.endTag(null, "extra");
            }
        }
        catch (IOException localIOException)
        {
            Log.w("SyncManager", "Error writing accounts", localIOException);
            if (localFileOutputStream != null)
                this.mAccountInfoFile.failWrite(localFileOutputStream);
        }
        label634: return;
        label635: if (localAuthorityInfo.syncable != 0);
        for (boolean bool = true; ; bool = false)
        {
            localFastXmlSerializer.attribute(null, "syncable", Boolean.toString(bool));
            break label407;
            if ((localObject instanceof Integer))
            {
                localFastXmlSerializer.attribute(null, "type", "integer");
                localFastXmlSerializer.attribute(null, "value1", localObject.toString());
                break label596;
            }
            if ((localObject instanceof Boolean))
            {
                localFastXmlSerializer.attribute(null, "type", "boolean");
                localFastXmlSerializer.attribute(null, "value1", localObject.toString());
                break label596;
            }
            if ((localObject instanceof Float))
            {
                localFastXmlSerializer.attribute(null, "type", "float");
                localFastXmlSerializer.attribute(null, "value1", localObject.toString());
                break label596;
            }
            if ((localObject instanceof Double))
            {
                localFastXmlSerializer.attribute(null, "type", "double");
                localFastXmlSerializer.attribute(null, "value1", localObject.toString());
                break label596;
            }
            if ((localObject instanceof String))
            {
                localFastXmlSerializer.attribute(null, "type", "string");
                localFastXmlSerializer.attribute(null, "value1", localObject.toString());
                break label596;
            }
            if (!(localObject instanceof Account))
                break label596;
            localFastXmlSerializer.attribute(null, "type", "account");
            localFastXmlSerializer.attribute(null, "value1", ((Account)localObject).name);
            localFastXmlSerializer.attribute(null, "value2", ((Account)localObject).type);
            break label596;
            label947: localFastXmlSerializer.endTag(null, "periodicSync");
            break label417;
            label962: localFastXmlSerializer.endTag(null, "authority");
            m++;
            break;
            label980: localFastXmlSerializer.endTag(null, "accounts");
            localFastXmlSerializer.endDocument();
            this.mAccountInfoFile.finishWrite(localFileOutputStream);
            break label634;
        }
    }

    private void writePendingOperationLocked(PendingOperation paramPendingOperation, Parcel paramParcel)
    {
        paramParcel.writeInt(2);
        paramParcel.writeInt(paramPendingOperation.authorityId);
        paramParcel.writeInt(paramPendingOperation.syncSource);
        if ((paramPendingOperation.flatExtras == null) && (paramPendingOperation.extras != null))
            paramPendingOperation.flatExtras = flattenBundle(paramPendingOperation.extras);
        paramParcel.writeByteArray(paramPendingOperation.flatExtras);
        if (paramPendingOperation.expedited);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }

    private void writePendingOperationsLocked()
    {
        int i = this.mPendingOperations.size();
        FileOutputStream localFileOutputStream = null;
        if (i == 0);
        try
        {
            this.mPendingFile.truncate();
            return;
            localFileOutputStream = this.mPendingFile.startWrite();
            Parcel localParcel = Parcel.obtain();
            for (int j = 0; j < i; j++)
                writePendingOperationLocked((PendingOperation)this.mPendingOperations.get(j), localParcel);
            localFileOutputStream.write(localParcel.marshall());
            localParcel.recycle();
            this.mPendingFile.finishWrite(localFileOutputStream);
        }
        catch (IOException localIOException)
        {
            Log.w("SyncManager", "Error writing pending operations", localIOException);
            if (localFileOutputStream != null)
                this.mPendingFile.failWrite(localFileOutputStream);
        }
    }

    private void writeStatisticsLocked()
    {
        removeMessages(2);
        FileOutputStream localFileOutputStream = null;
        try
        {
            localFileOutputStream = this.mStatisticsFile.startWrite();
            Parcel localParcel = Parcel.obtain();
            int i = this.mDayStats.length;
            for (int j = 0; ; j++)
            {
                DayStats localDayStats;
                if (j < i)
                {
                    localDayStats = this.mDayStats[j];
                    if (localDayStats != null);
                }
                else
                {
                    localParcel.writeInt(0);
                    localFileOutputStream.write(localParcel.marshall());
                    localParcel.recycle();
                    this.mStatisticsFile.finishWrite(localFileOutputStream);
                    break;
                }
                localParcel.writeInt(101);
                localParcel.writeInt(localDayStats.day);
                localParcel.writeInt(localDayStats.successCount);
                localParcel.writeLong(localDayStats.successTime);
                localParcel.writeInt(localDayStats.failureCount);
                localParcel.writeLong(localDayStats.failureTime);
            }
        }
        catch (IOException localIOException)
        {
            Log.w("SyncManager", "Error writing stats", localIOException);
            if (localFileOutputStream != null)
                this.mStatisticsFile.failWrite(localFileOutputStream);
        }
    }

    private void writeStatusLocked()
    {
        removeMessages(1);
        FileOutputStream localFileOutputStream = null;
        try
        {
            localFileOutputStream = this.mStatusFile.startWrite();
            Parcel localParcel = Parcel.obtain();
            int i = this.mSyncStatus.size();
            for (int j = 0; j < i; j++)
            {
                SyncStatusInfo localSyncStatusInfo = (SyncStatusInfo)this.mSyncStatus.valueAt(j);
                localParcel.writeInt(100);
                localSyncStatusInfo.writeToParcel(localParcel, 0);
            }
            localParcel.writeInt(0);
            localFileOutputStream.write(localParcel.marshall());
            localParcel.recycle();
            this.mStatusFile.finishWrite(localFileOutputStream);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.w("SyncManager", "Error writing status", localIOException);
                if (localFileOutputStream != null)
                    this.mStatusFile.failWrite(localFileOutputStream);
            }
        }
    }

    public SyncInfo addActiveSync(SyncManager.ActiveSyncContext paramActiveSyncContext)
    {
        synchronized (this.mAuthorities)
        {
            if (Log.isLoggable("SyncManager", 2))
                Log.v("SyncManager", "setActiveSync: account=" + paramActiveSyncContext.mSyncOperation.account + " auth=" + paramActiveSyncContext.mSyncOperation.authority + " src=" + paramActiveSyncContext.mSyncOperation.syncSource + " extras=" + paramActiveSyncContext.mSyncOperation.extras);
            AuthorityInfo localAuthorityInfo = getOrCreateAuthorityLocked(paramActiveSyncContext.mSyncOperation.account, paramActiveSyncContext.mSyncOperation.userId, paramActiveSyncContext.mSyncOperation.authority, -1, true);
            SyncInfo localSyncInfo = new SyncInfo(localAuthorityInfo.ident, localAuthorityInfo.account, localAuthorityInfo.authority, paramActiveSyncContext.mStartTime);
            getCurrentSyncs(localAuthorityInfo.userId).add(localSyncInfo);
            reportActiveChange();
            return localSyncInfo;
        }
    }

    public void addPeriodicSync(Account paramAccount, int paramInt, String paramString, Bundle paramBundle, long paramLong)
    {
        updateOrRemovePeriodicSync(paramAccount, paramInt, paramString, paramBundle, paramLong, true);
    }

    public void addStatusChangeListener(int paramInt, ISyncStatusObserver paramISyncStatusObserver)
    {
        synchronized (this.mAuthorities)
        {
            this.mChangeListeners.register(paramISyncStatusObserver, Integer.valueOf(paramInt));
            return;
        }
    }

    public void clearAllBackoffs(SyncQueue paramSyncQueue)
    {
        int i = 0;
        synchronized (this.mAuthorities)
        {
            Iterator localIterator1 = this.mAccounts.values().iterator();
            while (localIterator1.hasNext())
            {
                AccountInfo localAccountInfo = (AccountInfo)localIterator1.next();
                Iterator localIterator2 = localAccountInfo.authorities.values().iterator();
                while (localIterator2.hasNext())
                {
                    AuthorityInfo localAuthorityInfo = (AuthorityInfo)localIterator2.next();
                    if ((localAuthorityInfo.backoffTime != -1L) || (localAuthorityInfo.backoffDelay != -1L))
                    {
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "clearAllBackoffs: authority:" + localAuthorityInfo.authority + " account:" + localAccountInfo.accountAndUser.account.name + " user:" + localAccountInfo.accountAndUser.userId + " backoffTime was: " + localAuthorityInfo.backoffTime + " backoffDelay was: " + localAuthorityInfo.backoffDelay);
                        localAuthorityInfo.backoffTime = -1L;
                        localAuthorityInfo.backoffDelay = -1L;
                        paramSyncQueue.onBackoffChanged(localAccountInfo.accountAndUser.account, localAccountInfo.accountAndUser.userId, localAuthorityInfo.authority, 0L);
                        i = 1;
                    }
                }
            }
            if (i != 0)
                reportChange(1);
            return;
        }
    }

    public void clearAndReadState()
    {
        synchronized (this.mAuthorities)
        {
            this.mAuthorities.clear();
            this.mAccounts.clear();
            this.mPendingOperations.clear();
            this.mSyncStatus.clear();
            this.mSyncHistory.clear();
            readAccountInfoLocked();
            readStatusLocked();
            readPendingOperationsLocked();
            readStatisticsLocked();
            readAndDeleteLegacyAccountInfoLocked();
            writeAccountInfoLocked();
            writeStatusLocked();
            writePendingOperationsLocked();
            writeStatisticsLocked();
            return;
        }
    }

    public boolean deleteFromPending(PendingOperation paramPendingOperation)
    {
        boolean bool = false;
        while (true)
        {
            int k;
            synchronized (this.mAuthorities)
            {
                if (Log.isLoggable("SyncManager", 2))
                    Log.v("SyncManager", "deleteFromPending: account=" + paramPendingOperation.account + " user=" + paramPendingOperation.userId + " auth=" + paramPendingOperation.authority + " src=" + paramPendingOperation.syncSource + " extras=" + paramPendingOperation.extras);
                if (this.mPendingOperations.remove(paramPendingOperation))
                {
                    if ((this.mPendingOperations.size() == 0) || (this.mNumPendingFinished >= 4))
                    {
                        writePendingOperationsLocked();
                        this.mNumPendingFinished = 0;
                        AuthorityInfo localAuthorityInfo = getAuthorityLocked(paramPendingOperation.account, paramPendingOperation.userId, paramPendingOperation.authority, "deleteFromPending");
                        if (localAuthorityInfo == null)
                            break label354;
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "removing - " + localAuthorityInfo);
                        int i = this.mPendingOperations.size();
                        int j = 0;
                        k = 0;
                        if (k < i)
                        {
                            PendingOperation localPendingOperation = (PendingOperation)this.mPendingOperations.get(k);
                            if ((!localPendingOperation.account.equals(paramPendingOperation.account)) || (!localPendingOperation.authority.equals(paramPendingOperation.authority)) || (localPendingOperation.userId != paramPendingOperation.userId))
                                break label348;
                            j = 1;
                        }
                        if (j != 0)
                            break label354;
                        if (Log.isLoggable("SyncManager", 2))
                            Log.v("SyncManager", "no more pending!");
                        getOrCreateSyncStatusLocked(localAuthorityInfo.ident).pending = false;
                        break label354;
                    }
                }
                else
                {
                    reportChange(2);
                    return bool;
                }
                this.mNumPendingFinished = (1 + this.mNumPendingFinished);
            }
            label348: k++;
            continue;
            label354: bool = true;
        }
    }

    // ERROR //
    public void doDatabaseCleanup(Account[] paramArrayOfAccount, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: ldc 76
        //     9: iconst_2
        //     10: invokestatic 414	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     13: ifeq +12 -> 25
        //     16: ldc 76
        //     18: ldc_w 1334
        //     21: invokestatic 634	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     24: pop
        //     25: new 199	android/util/SparseArray
        //     28: dup
        //     29: invokespecial 200	android/util/SparseArray:<init>	()V
        //     32: astore 5
        //     34: aload_0
        //     35: getfield 204	android/content/SyncStorageEngine:mAccounts	Ljava/util/HashMap;
        //     38: invokevirtual 1279	java/util/HashMap:values	()Ljava/util/Collection;
        //     41: invokeinterface 1282 1 0
        //     46: astore 6
        //     48: aload 6
        //     50: invokeinterface 377 1 0
        //     55: ifeq +150 -> 205
        //     58: aload 6
        //     60: invokeinterface 381 1 0
        //     65: checkcast 18	android/content/SyncStorageEngine$AccountInfo
        //     68: astore 12
        //     70: aload_1
        //     71: aload 12
        //     73: getfield 1296	android/content/SyncStorageEngine$AccountInfo:accountAndUser	Landroid/accounts/AccountAndUser;
        //     76: getfield 1297	android/accounts/AccountAndUser:account	Landroid/accounts/Account;
        //     79: invokestatic 1340	com/android/internal/util/ArrayUtils:contains	([Ljava/lang/Object;Ljava/lang/Object;)Z
        //     82: ifne -34 -> 48
        //     85: aload 12
        //     87: getfield 1296	android/content/SyncStorageEngine$AccountInfo:accountAndUser	Landroid/accounts/AccountAndUser;
        //     90: getfield 1300	android/accounts/AccountAndUser:userId	I
        //     93: iload_2
        //     94: if_icmpne -46 -> 48
        //     97: ldc 76
        //     99: iconst_2
        //     100: invokestatic 414	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     103: ifeq +33 -> 136
        //     106: ldc 76
        //     108: new 416	java/lang/StringBuilder
        //     111: dup
        //     112: invokespecial 417	java/lang/StringBuilder:<init>	()V
        //     115: ldc_w 1342
        //     118: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     121: aload 12
        //     123: getfield 1296	android/content/SyncStorageEngine$AccountInfo:accountAndUser	Landroid/accounts/AccountAndUser;
        //     126: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     129: invokevirtual 430	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     132: invokestatic 634	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     135: pop
        //     136: aload 12
        //     138: getfield 437	android/content/SyncStorageEngine$AccountInfo:authorities	Ljava/util/HashMap;
        //     141: invokevirtual 1279	java/util/HashMap:values	()Ljava/util/Collection;
        //     144: invokeinterface 1282 1 0
        //     149: astore 13
        //     151: aload 13
        //     153: invokeinterface 377 1 0
        //     158: ifeq +37 -> 195
        //     161: aload 13
        //     163: invokeinterface 381 1 0
        //     168: checkcast 15	android/content/SyncStorageEngine$AuthorityInfo
        //     171: astore 14
        //     173: aload 5
        //     175: aload 14
        //     177: getfield 785	android/content/SyncStorageEngine$AuthorityInfo:ident	I
        //     180: aload 14
        //     182: invokevirtual 506	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     185: goto -34 -> 151
        //     188: astore 4
        //     190: aload_3
        //     191: monitorexit
        //     192: aload 4
        //     194: athrow
        //     195: aload 6
        //     197: invokeinterface 1118 1 0
        //     202: goto -154 -> 48
        //     205: aload 5
        //     207: invokevirtual 535	android/util/SparseArray:size	()I
        //     210: istore 7
        //     212: iload 7
        //     214: ifle +145 -> 359
        //     217: iload 7
        //     219: ifle +124 -> 343
        //     222: iinc 7 255
        //     225: aload 5
        //     227: iload 7
        //     229: invokevirtual 1157	android/util/SparseArray:keyAt	(I)I
        //     232: istore 8
        //     234: aload_0
        //     235: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     238: iload 8
        //     240: invokevirtual 1032	android/util/SparseArray:remove	(I)V
        //     243: aload_0
        //     244: getfield 213	android/content/SyncStorageEngine:mSyncStatus	Landroid/util/SparseArray;
        //     247: invokevirtual 535	android/util/SparseArray:size	()I
        //     250: istore 9
        //     252: iload 9
        //     254: ifle +39 -> 293
        //     257: iinc 9 255
        //     260: aload_0
        //     261: getfield 213	android/content/SyncStorageEngine:mSyncStatus	Landroid/util/SparseArray;
        //     264: iload 9
        //     266: invokevirtual 1157	android/util/SparseArray:keyAt	(I)I
        //     269: iload 8
        //     271: if_icmpne -19 -> 252
        //     274: aload_0
        //     275: getfield 213	android/content/SyncStorageEngine:mSyncStatus	Landroid/util/SparseArray;
        //     278: aload_0
        //     279: getfield 213	android/content/SyncStorageEngine:mSyncStatus	Landroid/util/SparseArray;
        //     282: iload 9
        //     284: invokevirtual 1157	android/util/SparseArray:keyAt	(I)I
        //     287: invokevirtual 1032	android/util/SparseArray:remove	(I)V
        //     290: goto -38 -> 252
        //     293: aload_0
        //     294: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     297: invokevirtual 1057	java/util/ArrayList:size	()I
        //     300: istore 10
        //     302: iload 10
        //     304: ifle -87 -> 217
        //     307: iinc 10 255
        //     310: aload_0
        //     311: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     314: iload 10
        //     316: invokevirtual 1058	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     319: checkcast 12	android/content/SyncStorageEngine$SyncHistoryItem
        //     322: getfield 1343	android/content/SyncStorageEngine$SyncHistoryItem:authorityId	I
        //     325: iload 8
        //     327: if_icmpne -25 -> 302
        //     330: aload_0
        //     331: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     334: iload 10
        //     336: invokevirtual 1345	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     339: pop
        //     340: goto -38 -> 302
        //     343: aload_0
        //     344: invokespecial 300	android/content/SyncStorageEngine:writeAccountInfoLocked	()V
        //     347: aload_0
        //     348: invokespecial 303	android/content/SyncStorageEngine:writeStatusLocked	()V
        //     351: aload_0
        //     352: invokespecial 306	android/content/SyncStorageEngine:writePendingOperationsLocked	()V
        //     355: aload_0
        //     356: invokespecial 309	android/content/SyncStorageEngine:writeStatisticsLocked	()V
        //     359: aload_3
        //     360: monitorexit
        //     361: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	192	188	finally
        //     195	361	188	finally
    }

    public ArrayList<AuthorityInfo> getAuthorities()
    {
        synchronized (this.mAuthorities)
        {
            int i = this.mAuthorities.size();
            ArrayList localArrayList = new ArrayList(i);
            for (int j = 0; j < i; j++)
                localArrayList.add(new AuthorityInfo((AuthorityInfo)this.mAuthorities.valueAt(j)));
            return localArrayList;
        }
    }

    public AuthorityInfo getAuthority(int paramInt)
    {
        synchronized (this.mAuthorities)
        {
            AuthorityInfo localAuthorityInfo = (AuthorityInfo)this.mAuthorities.get(paramInt);
            return localAuthorityInfo;
        }
    }

    public Pair<Long, Long> getBackoff(Account paramAccount, int paramInt, String paramString)
    {
        Pair localPair;
        synchronized (this.mAuthorities)
        {
            AuthorityInfo localAuthorityInfo = getAuthorityLocked(paramAccount, paramInt, paramString, "getBackoff");
            if ((localAuthorityInfo == null) || (localAuthorityInfo.backoffTime < 0L))
                localPair = null;
            else
                localPair = Pair.create(Long.valueOf(localAuthorityInfo.backoffTime), Long.valueOf(localAuthorityInfo.backoffDelay));
        }
        return localPair;
    }

    public List<SyncInfo> getCurrentSyncs(int paramInt)
    {
        synchronized (this.mAuthorities)
        {
            ArrayList localArrayList = (ArrayList)this.mCurrentSyncs.get(paramInt);
            if (localArrayList == null)
            {
                localArrayList = new ArrayList();
                this.mCurrentSyncs.put(paramInt, localArrayList);
            }
            return localArrayList;
        }
    }

    public DayStats[] getDayStatistics()
    {
        synchronized (this.mAuthorities)
        {
            DayStats[] arrayOfDayStats = new DayStats[this.mDayStats.length];
            System.arraycopy(this.mDayStats, 0, arrayOfDayStats, 0, arrayOfDayStats.length);
            return arrayOfDayStats;
        }
    }

    public long getDelayUntilTime(Account paramAccount, int paramInt, String paramString)
    {
        long l;
        synchronized (this.mAuthorities)
        {
            AuthorityInfo localAuthorityInfo = getAuthorityLocked(paramAccount, paramInt, paramString, "getDelayUntil");
            if (localAuthorityInfo == null)
                l = 0L;
            else
                l = localAuthorityInfo.delayUntil;
        }
        return l;
    }

    // ERROR //
    public int getIsSyncable(Account paramAccount, int paramInt, String paramString)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore 4
        //     4: aload_0
        //     5: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     8: astore 5
        //     10: aload 5
        //     12: monitorenter
        //     13: aload_1
        //     14: ifnull +47 -> 61
        //     17: aload_0
        //     18: aload_1
        //     19: iload_2
        //     20: aload_3
        //     21: ldc_w 1371
        //     24: invokespecial 557	android/content/SyncStorageEngine:getAuthorityLocked	(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;
        //     27: astore 9
        //     29: aload 9
        //     31: ifnonnull +9 -> 40
        //     34: aload 5
        //     36: monitorexit
        //     37: goto +83 -> 120
        //     40: aload 9
        //     42: getfield 617	android/content/SyncStorageEngine$AuthorityInfo:syncable	I
        //     45: istore 4
        //     47: aload 5
        //     49: monitorexit
        //     50: goto +70 -> 120
        //     53: astore 7
        //     55: aload 5
        //     57: monitorexit
        //     58: aload 7
        //     60: athrow
        //     61: aload_0
        //     62: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     65: invokevirtual 535	android/util/SparseArray:size	()I
        //     68: istore 6
        //     70: iload 6
        //     72: ifle +45 -> 117
        //     75: iinc 6 255
        //     78: aload_0
        //     79: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     82: iload 6
        //     84: invokevirtual 538	android/util/SparseArray:valueAt	(I)Ljava/lang/Object;
        //     87: checkcast 15	android/content/SyncStorageEngine$AuthorityInfo
        //     90: astore 8
        //     92: aload 8
        //     94: getfield 541	android/content/SyncStorageEngine$AuthorityInfo:authority	Ljava/lang/String;
        //     97: aload_3
        //     98: invokevirtual 615	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     101: ifeq -31 -> 70
        //     104: aload 8
        //     106: getfield 617	android/content/SyncStorageEngine$AuthorityInfo:syncable	I
        //     109: istore 4
        //     111: aload 5
        //     113: monitorexit
        //     114: goto +6 -> 120
        //     117: aload 5
        //     119: monitorexit
        //     120: iload 4
        //     122: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     17	58	53	finally
        //     61	120	53	finally
    }

    public boolean getMasterSyncAutomatically(int paramInt)
    {
        synchronized (this.mAuthorities)
        {
            Boolean localBoolean = (Boolean)this.mMasterSyncAutomatically.get(paramInt);
            if (localBoolean == null)
            {
                bool = true;
                return bool;
            }
            boolean bool = localBoolean.booleanValue();
        }
    }

    public AuthorityInfo getOrCreateAuthority(Account paramAccount, int paramInt, String paramString)
    {
        synchronized (this.mAuthorities)
        {
            AuthorityInfo localAuthorityInfo = getOrCreateAuthorityLocked(paramAccount, paramInt, paramString, -1, true);
            return localAuthorityInfo;
        }
    }

    public SyncStatusInfo getOrCreateSyncStatus(AuthorityInfo paramAuthorityInfo)
    {
        synchronized (this.mAuthorities)
        {
            SyncStatusInfo localSyncStatusInfo = getOrCreateSyncStatusLocked(paramAuthorityInfo.ident);
            return localSyncStatusInfo;
        }
    }

    public int getPendingOperationCount()
    {
        synchronized (this.mAuthorities)
        {
            int i = this.mPendingOperations.size();
            return i;
        }
    }

    public ArrayList<PendingOperation> getPendingOperations()
    {
        synchronized (this.mAuthorities)
        {
            ArrayList localArrayList = new ArrayList(this.mPendingOperations);
            return localArrayList;
        }
    }

    // ERROR //
    public List<PeriodicSync> getPeriodicSyncs(Account paramAccount, int paramInt, String paramString)
    {
        // Byte code:
        //     0: new 206	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 207	java/util/ArrayList:<init>	()V
        //     7: astore 4
        //     9: aload_0
        //     10: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     13: astore 5
        //     15: aload 5
        //     17: monitorenter
        //     18: aload_0
        //     19: aload_1
        //     20: iload_2
        //     21: aload_3
        //     22: ldc_w 1385
        //     25: invokespecial 557	android/content/SyncStorageEngine:getAuthorityLocked	(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;
        //     28: astore 7
        //     30: aload 7
        //     32: ifnull +80 -> 112
        //     35: aload 7
        //     37: getfield 608	android/content/SyncStorageEngine$AuthorityInfo:periodicSyncs	Ljava/util/ArrayList;
        //     40: invokevirtual 560	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     43: astore 8
        //     45: aload 8
        //     47: invokeinterface 377 1 0
        //     52: ifeq +60 -> 112
        //     55: aload 8
        //     57: invokeinterface 381 1 0
        //     62: checkcast 638	android/util/Pair
        //     65: astore 9
        //     67: aload 4
        //     69: new 1387	android/content/PeriodicSync
        //     72: dup
        //     73: aload_1
        //     74: aload_3
        //     75: aload 9
        //     77: getfield 642	android/util/Pair:first	Ljava/lang/Object;
        //     80: checkcast 355	android/os/Bundle
        //     83: aload 9
        //     85: getfield 1102	android/util/Pair:second	Ljava/lang/Object;
        //     88: checkcast 652	java/lang/Long
        //     91: invokevirtual 1105	java/lang/Long:longValue	()J
        //     94: invokespecial 1390	android/content/PeriodicSync:<init>	(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
        //     97: invokevirtual 544	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     100: pop
        //     101: goto -56 -> 45
        //     104: astore 6
        //     106: aload 5
        //     108: monitorexit
        //     109: aload 6
        //     111: athrow
        //     112: aload 5
        //     114: monitorexit
        //     115: aload 4
        //     117: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     18	109	104	finally
        //     112	115	104	finally
    }

    public SyncStatusInfo getStatusByAccountAndAuthority(Account paramAccount, int paramInt, String paramString)
    {
        if ((paramAccount == null) || (paramString == null))
            throw new IllegalArgumentException();
        while (true)
        {
            int j;
            SyncStatusInfo localSyncStatusInfo;
            synchronized (this.mAuthorities)
            {
                int i = this.mSyncStatus.size();
                j = 0;
                if (j < i)
                {
                    localSyncStatusInfo = (SyncStatusInfo)this.mSyncStatus.valueAt(j);
                    AuthorityInfo localAuthorityInfo = (AuthorityInfo)this.mAuthorities.get(localSyncStatusInfo.authorityId);
                    if ((localAuthorityInfo == null) || (!localAuthorityInfo.authority.equals(paramString)) || (localAuthorityInfo.userId != paramInt) || (!paramAccount.equals(localAuthorityInfo.account)))
                        break label139;
                }
                else
                {
                    localSyncStatusInfo = null;
                }
            }
            return localSyncStatusInfo;
            label139: j++;
        }
    }

    public boolean getSyncAutomatically(Account paramAccount, int paramInt, String paramString)
    {
        boolean bool = true;
        SparseArray localSparseArray = this.mAuthorities;
        if (paramAccount != null);
        while (true)
        {
            try
            {
                AuthorityInfo localAuthorityInfo2 = getAuthorityLocked(paramAccount, paramInt, paramString, "getSyncAutomatically");
                if ((localAuthorityInfo2 != null) && (localAuthorityInfo2.enabled))
                {
                    continue;
                    int i = this.mAuthorities.size();
                    if (i > 0)
                    {
                        i--;
                        AuthorityInfo localAuthorityInfo1 = (AuthorityInfo)this.mAuthorities.valueAt(i);
                        if ((!localAuthorityInfo1.authority.equals(paramString)) || (localAuthorityInfo1.userId != paramInt) || (!localAuthorityInfo1.enabled))
                            continue;
                    }
                }
            }
            finally
            {
                throw localObject;
            }
            bool = false;
        }
    }

    public ArrayList<SyncHistoryItem> getSyncHistory()
    {
        synchronized (this.mAuthorities)
        {
            int i = this.mSyncHistory.size();
            ArrayList localArrayList = new ArrayList(i);
            for (int j = 0; j < i; j++)
                localArrayList.add(this.mSyncHistory.get(j));
            return localArrayList;
        }
    }

    public int getSyncRandomOffset()
    {
        return this.mSyncRandomOffset;
    }

    public ArrayList<SyncStatusInfo> getSyncStatus()
    {
        synchronized (this.mAuthorities)
        {
            int i = this.mSyncStatus.size();
            ArrayList localArrayList = new ArrayList(i);
            for (int j = 0; j < i; j++)
                localArrayList.add(this.mSyncStatus.valueAt(j));
            return localArrayList;
        }
    }

    public void handleMessage(Message paramMessage)
    {
        if (paramMessage.what == 1)
            synchronized (this.mAuthorities)
            {
                writeStatusLocked();
            }
        if (paramMessage.what == 2)
            synchronized (this.mAuthorities)
            {
                writeStatisticsLocked();
            }
    }

    public PendingOperation insertIntoPending(PendingOperation paramPendingOperation)
    {
        AuthorityInfo localAuthorityInfo;
        PendingOperation localPendingOperation;
        synchronized (this.mAuthorities)
        {
            if (Log.isLoggable("SyncManager", 2))
                Log.v("SyncManager", "insertIntoPending: account=" + paramPendingOperation.account + " user=" + paramPendingOperation.userId + " auth=" + paramPendingOperation.authority + " src=" + paramPendingOperation.syncSource + " extras=" + paramPendingOperation.extras);
            localAuthorityInfo = getOrCreateAuthorityLocked(paramPendingOperation.account, paramPendingOperation.userId, paramPendingOperation.authority, -1, true);
            if (localAuthorityInfo == null)
            {
                localPendingOperation = null;
                break label205;
            }
            localPendingOperation = new PendingOperation(paramPendingOperation);
        }
        try
        {
            localPendingOperation.authorityId = localAuthorityInfo.ident;
            this.mPendingOperations.add(localPendingOperation);
            appendPendingOperationLocked(localPendingOperation);
            getOrCreateSyncStatusLocked(localAuthorityInfo.ident).pending = true;
            reportChange(2);
            break label205;
            localObject1 = finally;
            throw localObject1;
        }
        finally
        {
        }
        label205: return localPendingOperation;
    }

    // ERROR //
    public long insertStartSyncEvent(Account paramAccount, int paramInt1, String paramString, long paramLong, int paramInt2, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 202	android/content/SyncStorageEngine:mAuthorities	Landroid/util/SparseArray;
        //     4: astore 8
        //     6: aload 8
        //     8: monitorenter
        //     9: ldc 76
        //     11: iconst_2
        //     12: invokestatic 414	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     15: ifeq +60 -> 75
        //     18: ldc 76
        //     20: new 416	java/lang/StringBuilder
        //     23: dup
        //     24: invokespecial 417	java/lang/StringBuilder:<init>	()V
        //     27: ldc_w 1420
        //     30: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     33: aload_1
        //     34: invokevirtual 426	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     37: ldc_w 1422
        //     40: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     43: iload_2
        //     44: invokevirtual 498	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     47: ldc_w 628
        //     50: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     53: aload_3
        //     54: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: ldc_w 1424
        //     60: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     63: iload 6
        //     65: invokevirtual 498	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     68: invokevirtual 430	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     71: invokestatic 434	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     74: pop
        //     75: aload_0
        //     76: aload_1
        //     77: iload_2
        //     78: aload_3
        //     79: ldc_w 1425
        //     82: invokespecial 557	android/content/SyncStorageEngine:getAuthorityLocked	(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;
        //     85: astore 10
        //     87: aload 10
        //     89: ifnonnull +14 -> 103
        //     92: ldc2_w 53
        //     95: lstore 13
        //     97: aload 8
        //     99: monitorexit
        //     100: goto +186 -> 286
        //     103: new 12	android/content/SyncStorageEngine$SyncHistoryItem
        //     106: dup
        //     107: invokespecial 1426	android/content/SyncStorageEngine$SyncHistoryItem:<init>	()V
        //     110: astore 11
        //     112: aload 11
        //     114: iload 7
        //     116: putfield 1429	android/content/SyncStorageEngine$SyncHistoryItem:initialization	Z
        //     119: aload 11
        //     121: aload 10
        //     123: getfield 785	android/content/SyncStorageEngine$AuthorityInfo:ident	I
        //     126: putfield 1343	android/content/SyncStorageEngine$SyncHistoryItem:authorityId	I
        //     129: aload_0
        //     130: getfield 228	android/content/SyncStorageEngine:mNextHistoryId	I
        //     133: istore 12
        //     135: aload_0
        //     136: iload 12
        //     138: iconst_1
        //     139: iadd
        //     140: putfield 228	android/content/SyncStorageEngine:mNextHistoryId	I
        //     143: aload 11
        //     145: iload 12
        //     147: putfield 1432	android/content/SyncStorageEngine$SyncHistoryItem:historyId	I
        //     150: aload_0
        //     151: getfield 228	android/content/SyncStorageEngine:mNextHistoryId	I
        //     154: ifge +8 -> 162
        //     157: aload_0
        //     158: iconst_0
        //     159: putfield 228	android/content/SyncStorageEngine:mNextHistoryId	I
        //     162: aload 11
        //     164: lload 4
        //     166: putfield 1435	android/content/SyncStorageEngine$SyncHistoryItem:eventTime	J
        //     169: aload 11
        //     171: iload 6
        //     173: putfield 1438	android/content/SyncStorageEngine$SyncHistoryItem:source	I
        //     176: aload 11
        //     178: iconst_0
        //     179: putfield 1441	android/content/SyncStorageEngine$SyncHistoryItem:event	I
        //     182: aload_0
        //     183: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     186: iconst_0
        //     187: aload 11
        //     189: invokevirtual 1443	java/util/ArrayList:add	(ILjava/lang/Object;)V
        //     192: aload_0
        //     193: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     196: invokevirtual 1057	java/util/ArrayList:size	()I
        //     199: bipush 100
        //     201: if_icmple +32 -> 233
        //     204: aload_0
        //     205: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     208: bipush 255
        //     210: aload_0
        //     211: getfield 215	android/content/SyncStorageEngine:mSyncHistory	Ljava/util/ArrayList;
        //     214: invokevirtual 1057	java/util/ArrayList:size	()I
        //     217: iadd
        //     218: invokevirtual 1345	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     221: pop
        //     222: goto -30 -> 192
        //     225: astore 9
        //     227: aload 8
        //     229: monitorexit
        //     230: aload 9
        //     232: athrow
        //     233: aload 11
        //     235: getfield 1432	android/content/SyncStorageEngine$SyncHistoryItem:historyId	I
        //     238: i2l
        //     239: lstore 13
        //     241: ldc 76
        //     243: iconst_2
        //     244: invokestatic 414	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     247: ifeq +30 -> 277
        //     250: ldc 76
        //     252: new 416	java/lang/StringBuilder
        //     255: dup
        //     256: invokespecial 417	java/lang/StringBuilder:<init>	()V
        //     259: ldc_w 1445
        //     262: invokevirtual 421	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     265: lload 13
        //     267: invokevirtual 1095	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     270: invokevirtual 430	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     273: invokestatic 434	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     276: pop
        //     277: aload 8
        //     279: monitorexit
        //     280: aload_0
        //     281: bipush 8
        //     283: invokespecial 1116	android/content/SyncStorageEngine:reportChange	(I)V
        //     286: lload 13
        //     288: lreturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	230	225	finally
        //     233	280	225	finally
    }

    public boolean isSyncActive(Account paramAccount, int paramInt, String paramString)
    {
        boolean bool;
        synchronized (this.mAuthorities)
        {
            Iterator localIterator = getCurrentSyncs(paramInt).iterator();
            while (localIterator.hasNext())
            {
                AuthorityInfo localAuthorityInfo = getAuthority(((SyncInfo)localIterator.next()).authorityId);
                if ((localAuthorityInfo != null) && (localAuthorityInfo.account.equals(paramAccount)) && (localAuthorityInfo.authority.equals(paramString)) && (localAuthorityInfo.userId == paramInt))
                {
                    bool = true;
                    break label114;
                }
            }
            bool = false;
        }
        label114: return bool;
    }

    public boolean isSyncPending(Account paramAccount, int paramInt, String paramString)
    {
        boolean bool;
        while (true)
        {
            int j;
            synchronized (this.mAuthorities)
            {
                int i = this.mSyncStatus.size();
                j = 0;
                if (j < i)
                {
                    SyncStatusInfo localSyncStatusInfo = (SyncStatusInfo)this.mSyncStatus.valueAt(j);
                    AuthorityInfo localAuthorityInfo = (AuthorityInfo)this.mAuthorities.get(localSyncStatusInfo.authorityId);
                    if ((localAuthorityInfo != null) && (paramInt == localAuthorityInfo.userId) && ((paramAccount == null) || (localAuthorityInfo.account.equals(paramAccount))) && (localAuthorityInfo.authority.equals(paramString)) && (localSyncStatusInfo.pending))
                    {
                        bool = true;
                        break;
                    }
                }
                else
                {
                    bool = false;
                }
            }
            j++;
        }
        return bool;
    }

    public void removeActiveSync(SyncInfo paramSyncInfo, int paramInt)
    {
        synchronized (this.mAuthorities)
        {
            if (Log.isLoggable("SyncManager", 2))
                Log.v("SyncManager", "removeActiveSync: account=" + paramSyncInfo.account + " user=" + paramInt + " auth=" + paramSyncInfo.authority);
            getCurrentSyncs(paramInt).remove(paramSyncInfo);
            reportActiveChange();
            return;
        }
    }

    public void removeAuthority(Account paramAccount, int paramInt, String paramString)
    {
        synchronized (this.mAuthorities)
        {
            removeAuthorityLocked(paramAccount, paramInt, paramString, true);
            return;
        }
    }

    public void removePeriodicSync(Account paramAccount, int paramInt, String paramString, Bundle paramBundle)
    {
        updateOrRemovePeriodicSync(paramAccount, paramInt, paramString, paramBundle, 0L, false);
    }

    public void removeStatusChangeListener(ISyncStatusObserver paramISyncStatusObserver)
    {
        synchronized (this.mAuthorities)
        {
            this.mChangeListeners.unregister(paramISyncStatusObserver);
            return;
        }
    }

    public void reportActiveChange()
    {
        reportChange(4);
    }

    public void setBackoff(Account paramAccount, int paramInt, String paramString, long paramLong1, long paramLong2)
    {
        if (Log.isLoggable("SyncManager", 2))
            Log.v("SyncManager", "setBackoff: " + paramAccount + ", provider " + paramString + ", user " + paramInt + " -> nextSyncTime " + paramLong1 + ", nextDelay " + paramLong2);
        int i = 0;
        SparseArray localSparseArray = this.mAuthorities;
        if ((paramAccount == null) || (paramString == null));
        try
        {
            Iterator localIterator1 = this.mAccounts.values().iterator();
            while (localIterator1.hasNext())
            {
                AccountInfo localAccountInfo = (AccountInfo)localIterator1.next();
                if ((paramAccount == null) || (paramAccount.equals(localAccountInfo.accountAndUser.account)) || (paramInt == localAccountInfo.accountAndUser.userId))
                {
                    Iterator localIterator2 = localAccountInfo.authorities.values().iterator();
                    while (localIterator2.hasNext())
                    {
                        AuthorityInfo localAuthorityInfo1 = (AuthorityInfo)localIterator2.next();
                        if (((paramString == null) || (paramString.equals(localAuthorityInfo1.authority))) && ((localAuthorityInfo1.backoffTime != paramLong1) || (localAuthorityInfo1.backoffDelay != paramLong2)))
                        {
                            localAuthorityInfo1.backoffTime = paramLong1;
                            localAuthorityInfo1.backoffDelay = paramLong2;
                            i = 1;
                        }
                    }
                    continue;
                    AuthorityInfo localAuthorityInfo2 = getOrCreateAuthorityLocked(paramAccount, paramInt, paramString, -1, true);
                    if ((localAuthorityInfo2.backoffTime == paramLong1) && (localAuthorityInfo2.backoffDelay == paramLong2))
                        return;
                    localAuthorityInfo2.backoffTime = paramLong1;
                    localAuthorityInfo2.backoffDelay = paramLong2;
                    i = 1;
                }
            }
            if (i != 0)
                reportChange(1);
        }
        finally
        {
        }
    }

    public void setDelayUntilTime(Account paramAccount, int paramInt, String paramString, long paramLong)
    {
        if (Log.isLoggable("SyncManager", 2))
            Log.v("SyncManager", "setDelayUntil: " + paramAccount + ", provider " + paramString + ", user " + paramInt + " -> delayUntil " + paramLong);
        synchronized (this.mAuthorities)
        {
            AuthorityInfo localAuthorityInfo = getOrCreateAuthorityLocked(paramAccount, paramInt, paramString, -1, true);
            if (localAuthorityInfo.delayUntil != paramLong)
            {
                localAuthorityInfo.delayUntil = paramLong;
                reportChange(1);
            }
        }
    }

    public void setIsSyncable(Account paramAccount, int paramInt1, String paramString, int paramInt2)
    {
        if (paramInt2 > 1)
            paramInt2 = 1;
        while (true)
        {
            Log.d("SyncManager", "setIsSyncable: " + paramAccount + ", provider " + paramString + ", user " + paramInt1 + " -> " + paramInt2);
            synchronized (this.mAuthorities)
            {
                AuthorityInfo localAuthorityInfo = getOrCreateAuthorityLocked(paramAccount, paramInt1, paramString, -1, false);
                if (localAuthorityInfo.syncable == paramInt2)
                {
                    Log.d("SyncManager", "setIsSyncable: already set to " + paramInt2 + ", doing nothing");
                }
                else
                {
                    localAuthorityInfo.syncable = paramInt2;
                    writeAccountInfoLocked();
                    if (paramInt2 > 0)
                        requestSync(paramAccount, paramInt1, paramString, new Bundle());
                    reportChange(1);
                }
            }
            return;
            if (paramInt2 < -1)
                paramInt2 = -1;
        }
    }

    public void setMasterSyncAutomatically(boolean paramBoolean, int paramInt)
    {
        synchronized (this.mAuthorities)
        {
            Boolean localBoolean = (Boolean)this.mMasterSyncAutomatically.get(paramInt);
            if ((localBoolean == null) || (localBoolean.booleanValue() != paramBoolean))
            {
                this.mMasterSyncAutomatically.put(paramInt, Boolean.valueOf(paramBoolean));
                writeAccountInfoLocked();
                if (paramBoolean)
                    requestSync(null, paramInt, null, new Bundle());
                reportChange(1);
                this.mContext.sendBroadcast(SYNC_CONNECTION_SETTING_CHANGED_INTENT);
            }
        }
    }

    protected void setOnSyncRequestListener(OnSyncRequestListener paramOnSyncRequestListener)
    {
        if (this.mSyncRequestListener == null)
            this.mSyncRequestListener = paramOnSyncRequestListener;
    }

    public void setSyncAutomatically(Account paramAccount, int paramInt, String paramString, boolean paramBoolean)
    {
        Log.d("SyncManager", "setSyncAutomatically:    provider " + paramString + ", user " + paramInt + " -> " + paramBoolean);
        synchronized (this.mAuthorities)
        {
            AuthorityInfo localAuthorityInfo = getOrCreateAuthorityLocked(paramAccount, paramInt, paramString, -1, false);
            if (localAuthorityInfo.enabled == paramBoolean)
            {
                Log.d("SyncManager", "setSyncAutomatically: already set to " + paramBoolean + ", doing nothing");
            }
            else
            {
                localAuthorityInfo.enabled = paramBoolean;
                writeAccountInfoLocked();
                if (paramBoolean)
                    requestSync(paramAccount, paramInt, paramString, new Bundle());
                reportChange(1);
            }
        }
    }

    public void stopSyncEvent(long paramLong1, long paramLong2, String paramString, long paramLong3, long paramLong4)
    {
        while (true)
        {
            SyncHistoryItem localSyncHistoryItem;
            SyncStatusInfo localSyncStatusInfo;
            int j;
            int k;
            DayStats localDayStats;
            long l;
            int m;
            synchronized (this.mAuthorities)
            {
                if (Log.isLoggable("SyncManager", 2))
                    Log.v("SyncManager", "stopSyncEvent: historyId=" + paramLong1);
                localSyncHistoryItem = null;
                int i = this.mSyncHistory.size();
                if (i > 0)
                {
                    i--;
                    localSyncHistoryItem = (SyncHistoryItem)this.mSyncHistory.get(i);
                    if (localSyncHistoryItem.historyId != paramLong1)
                        break label705;
                }
                if (localSyncHistoryItem == null)
                {
                    Log.w("SyncManager", "stopSyncEvent: no history for id " + paramLong1);
                    break label704;
                }
                localSyncHistoryItem.elapsedTime = paramLong2;
                localSyncHistoryItem.event = 1;
                localSyncHistoryItem.mesg = paramString;
                localSyncHistoryItem.downstreamActivity = paramLong3;
                localSyncHistoryItem.upstreamActivity = paramLong4;
                localSyncStatusInfo = getOrCreateSyncStatusLocked(localSyncHistoryItem.authorityId);
                localSyncStatusInfo.numSyncs = (1 + localSyncStatusInfo.numSyncs);
                localSyncStatusInfo.totalElapsedTime = (paramLong2 + localSyncStatusInfo.totalElapsedTime);
                switch (localSyncHistoryItem.source)
                {
                default:
                    j = 0;
                    k = getCurrentDayLocked();
                    if (this.mDayStats[0] != null)
                        break;
                    this.mDayStats[0] = new DayStats(k);
                    localDayStats = this.mDayStats[0];
                    l = paramLong2 + localSyncHistoryItem.eventTime;
                    m = 0;
                    if (!"success".equals(paramString))
                        break label565;
                    if ((localSyncStatusInfo.lastSuccessTime == 0L) || (localSyncStatusInfo.lastFailureTime != 0L))
                        break label711;
                    localSyncStatusInfo.lastSuccessTime = l;
                    localSyncStatusInfo.lastSuccessSource = localSyncHistoryItem.source;
                    localSyncStatusInfo.lastFailureTime = 0L;
                    localSyncStatusInfo.lastFailureSource = -1;
                    localSyncStatusInfo.lastFailureMesg = null;
                    localSyncStatusInfo.initialFailureTime = 0L;
                    localDayStats.successCount = (1 + localDayStats.successCount);
                    localDayStats.successTime = (paramLong2 + localDayStats.successTime);
                    if (m == 0)
                        break label656;
                    writeStatusLocked();
                    if (j == 0)
                        break label680;
                    writeStatisticsLocked();
                    reportChange(8);
                    break;
                case 1:
                    localSyncStatusInfo.numSourceLocal = (1 + localSyncStatusInfo.numSourceLocal);
                case 2:
                case 3:
                case 0:
                case 4:
                }
            }
            localSyncStatusInfo.numSourcePoll = (1 + localSyncStatusInfo.numSourcePoll);
            continue;
            localSyncStatusInfo.numSourceUser = (1 + localSyncStatusInfo.numSourceUser);
            continue;
            localSyncStatusInfo.numSourceServer = (1 + localSyncStatusInfo.numSourceServer);
            continue;
            localSyncStatusInfo.numSourcePeriodic = (1 + localSyncStatusInfo.numSourcePeriodic);
            continue;
            if (k != this.mDayStats[0].day)
            {
                System.arraycopy(this.mDayStats, 0, this.mDayStats, 1, -1 + this.mDayStats.length);
                this.mDayStats[0] = new DayStats(k);
                j = 1;
            }
            else
            {
                label656: label680: if (this.mDayStats[0] == null)
                {
                    continue;
                    label565: if (!"canceled".equals(paramString))
                    {
                        if (localSyncStatusInfo.lastFailureTime == 0L)
                            m = 1;
                        localSyncStatusInfo.lastFailureTime = l;
                        localSyncStatusInfo.lastFailureSource = localSyncHistoryItem.source;
                        localSyncStatusInfo.lastFailureMesg = paramString;
                        if (localSyncStatusInfo.initialFailureTime == 0L)
                            localSyncStatusInfo.initialFailureTime = l;
                        localDayStats.failureCount = (1 + localDayStats.failureCount);
                        localDayStats.failureTime = (paramLong2 + localDayStats.failureTime);
                        continue;
                        if (!hasMessages(1))
                        {
                            sendMessageDelayed(obtainMessage(1), 600000L);
                            continue;
                            if (!hasMessages(2))
                            {
                                sendMessageDelayed(obtainMessage(2), 1800000L);
                                continue;
                                label704: return;
                                label705: localSyncHistoryItem = null;
                                continue;
                                label711: m = 1;
                            }
                        }
                    }
                }
            }
        }
    }

    public void writeAllState()
    {
        synchronized (this.mAuthorities)
        {
            if (this.mNumPendingFinished > 0)
                writePendingOperationsLocked();
            writeStatusLocked();
            writeStatisticsLocked();
            return;
        }
    }

    static abstract interface OnSyncRequestListener
    {
        public abstract void onSyncRequest(Account paramAccount, int paramInt, String paramString, Bundle paramBundle);
    }

    public static class DayStats
    {
        public final int day;
        public int failureCount;
        public long failureTime;
        public int successCount;
        public long successTime;

        public DayStats(int paramInt)
        {
            this.day = paramInt;
        }
    }

    public static class SyncHistoryItem
    {
        int authorityId;
        long downstreamActivity;
        long elapsedTime;
        int event;
        long eventTime;
        int historyId;
        boolean initialization;
        String mesg;
        int source;
        long upstreamActivity;
    }

    public static class AuthorityInfo
    {
        final Account account;
        final String authority;
        long backoffDelay;
        long backoffTime;
        long delayUntil;
        boolean enabled;
        final int ident;
        final ArrayList<Pair<Bundle, Long>> periodicSyncs;
        int syncable;
        final int userId;

        AuthorityInfo(Account paramAccount, int paramInt1, String paramString, int paramInt2)
        {
            this.account = paramAccount;
            this.userId = paramInt1;
            this.authority = paramString;
            this.ident = paramInt2;
            this.enabled = false;
            this.syncable = -1;
            this.backoffTime = -1L;
            this.backoffDelay = -1L;
            this.periodicSyncs = new ArrayList();
            this.periodicSyncs.add(Pair.create(new Bundle(), Long.valueOf(86400L)));
        }

        AuthorityInfo(AuthorityInfo paramAuthorityInfo)
        {
            this.account = paramAuthorityInfo.account;
            this.userId = paramAuthorityInfo.userId;
            this.authority = paramAuthorityInfo.authority;
            this.ident = paramAuthorityInfo.ident;
            this.enabled = paramAuthorityInfo.enabled;
            this.syncable = paramAuthorityInfo.syncable;
            this.backoffTime = paramAuthorityInfo.backoffTime;
            this.backoffDelay = paramAuthorityInfo.backoffDelay;
            this.delayUntil = paramAuthorityInfo.delayUntil;
            this.periodicSyncs = new ArrayList();
            Iterator localIterator = paramAuthorityInfo.periodicSyncs.iterator();
            while (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                this.periodicSyncs.add(Pair.create(new Bundle((Bundle)localPair.first), localPair.second));
            }
        }
    }

    static class AccountInfo
    {
        final AccountAndUser accountAndUser;
        final HashMap<String, SyncStorageEngine.AuthorityInfo> authorities = new HashMap();

        AccountInfo(AccountAndUser paramAccountAndUser)
        {
            this.accountAndUser = paramAccountAndUser;
        }
    }

    public static class PendingOperation
    {
        final Account account;
        final String authority;
        int authorityId;
        final boolean expedited;
        final Bundle extras;
        byte[] flatExtras;
        final int syncSource;
        final int userId;

        PendingOperation(Account paramAccount, int paramInt1, int paramInt2, String paramString, Bundle paramBundle, boolean paramBoolean)
        {
            this.account = paramAccount;
            this.userId = paramInt1;
            this.syncSource = paramInt2;
            this.authority = paramString;
            if (paramBundle != null)
                paramBundle = new Bundle(paramBundle);
            this.extras = paramBundle;
            this.expedited = paramBoolean;
            this.authorityId = -1;
        }

        PendingOperation(PendingOperation paramPendingOperation)
        {
            this.account = paramPendingOperation.account;
            this.userId = paramPendingOperation.userId;
            this.syncSource = paramPendingOperation.syncSource;
            this.authority = paramPendingOperation.authority;
            this.extras = paramPendingOperation.extras;
            this.authorityId = paramPendingOperation.authorityId;
            this.expedited = paramPendingOperation.expedited;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncStorageEngine
 * JD-Core Version:        0.6.2
 */