package android.content;

import android.accounts.Account;
import android.database.IContentObserver;
import android.database.IContentObserver.Stub;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IContentService extends IInterface
{
    public abstract void addPeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong)
        throws RemoteException;

    public abstract void addStatusChangeListener(int paramInt, ISyncStatusObserver paramISyncStatusObserver)
        throws RemoteException;

    public abstract void cancelSync(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract List<SyncInfo> getCurrentSyncs()
        throws RemoteException;

    public abstract int getIsSyncable(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract boolean getMasterSyncAutomatically()
        throws RemoteException;

    public abstract List<PeriodicSync> getPeriodicSyncs(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract SyncAdapterType[] getSyncAdapterTypes()
        throws RemoteException;

    public abstract boolean getSyncAutomatically(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract SyncStatusInfo getSyncStatus(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract boolean isSyncActive(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract boolean isSyncPending(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract void notifyChange(Uri paramUri, IContentObserver paramIContentObserver, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract void registerContentObserver(Uri paramUri, boolean paramBoolean, IContentObserver paramIContentObserver)
        throws RemoteException;

    public abstract void removePeriodicSync(Account paramAccount, String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void removeStatusChangeListener(ISyncStatusObserver paramISyncStatusObserver)
        throws RemoteException;

    public abstract void requestSync(Account paramAccount, String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void setIsSyncable(Account paramAccount, String paramString, int paramInt)
        throws RemoteException;

    public abstract void setMasterSyncAutomatically(boolean paramBoolean)
        throws RemoteException;

    public abstract void setSyncAutomatically(Account paramAccount, String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void unregisterContentObserver(IContentObserver paramIContentObserver)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IContentService
    {
        private static final String DESCRIPTOR = "android.content.IContentService";
        static final int TRANSACTION_addPeriodicSync = 9;
        static final int TRANSACTION_addStatusChangeListener = 20;
        static final int TRANSACTION_cancelSync = 5;
        static final int TRANSACTION_getCurrentSyncs = 16;
        static final int TRANSACTION_getIsSyncable = 11;
        static final int TRANSACTION_getMasterSyncAutomatically = 14;
        static final int TRANSACTION_getPeriodicSyncs = 8;
        static final int TRANSACTION_getSyncAdapterTypes = 17;
        static final int TRANSACTION_getSyncAutomatically = 6;
        static final int TRANSACTION_getSyncStatus = 18;
        static final int TRANSACTION_isSyncActive = 15;
        static final int TRANSACTION_isSyncPending = 19;
        static final int TRANSACTION_notifyChange = 3;
        static final int TRANSACTION_registerContentObserver = 1;
        static final int TRANSACTION_removePeriodicSync = 10;
        static final int TRANSACTION_removeStatusChangeListener = 21;
        static final int TRANSACTION_requestSync = 4;
        static final int TRANSACTION_setIsSyncable = 12;
        static final int TRANSACTION_setMasterSyncAutomatically = 13;
        static final int TRANSACTION_setSyncAutomatically = 7;
        static final int TRANSACTION_unregisterContentObserver = 2;

        public Stub()
        {
            attachInterface(this, "android.content.IContentService");
        }

        public static IContentService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.IContentService");
                if ((localIInterface != null) && ((localIInterface instanceof IContentService)))
                    localObject = (IContentService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.content.IContentService");
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                Uri localUri2;
                if (paramParcel1.readInt() != 0)
                {
                    localUri2 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    label242: if (paramParcel1.readInt() == 0)
                        break label281;
                }
                label281: int i8;
                for (int i7 = j; ; i8 = 0)
                {
                    registerContentObserver(localUri2, i7, IContentObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    break;
                    localUri2 = null;
                    break label242;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                unregisterContentObserver(IContentObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                Uri localUri1;
                label338: IContentObserver localIContentObserver;
                int i3;
                if (paramParcel1.readInt() != 0)
                {
                    localUri1 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    localIContentObserver = IContentObserver.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() == 0)
                        break label394;
                    i3 = j;
                    label358: if (paramParcel1.readInt() == 0)
                        break label400;
                }
                label394: label400: int i6;
                for (int i5 = j; ; i6 = 0)
                {
                    notifyChange(localUri1, localIContentObserver, i3, i5);
                    paramParcel2.writeNoException();
                    break;
                    localUri1 = null;
                    break label338;
                    int i4 = 0;
                    break label358;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                Account localAccount12;
                label433: String str4;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount12 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str4 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label483;
                }
                label483: for (Bundle localBundle3 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle3 = null)
                {
                    requestSync(localAccount12, str4, localBundle3);
                    paramParcel2.writeNoException();
                    break;
                    localAccount12 = null;
                    break label433;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount11 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount11 = null)
                {
                    cancelSync(localAccount11, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount10 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount10 = null)
                {
                    boolean bool4 = getSyncAutomatically(localAccount10, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool4)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                Account localAccount9;
                label633: String str3;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount9 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str3 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label673;
                }
                label673: int i2;
                for (int i1 = j; ; i2 = 0)
                {
                    setSyncAutomatically(localAccount9, str3, i1);
                    paramParcel2.writeNoException();
                    break;
                    localAccount9 = null;
                    break label633;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount8 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount8 = null)
                {
                    List localList2 = getPeriodicSyncs(localAccount8, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList2);
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                Account localAccount7;
                label764: String str2;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount7 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str2 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label818;
                }
                label818: for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                {
                    addPeriodicSync(localAccount7, str2, localBundle2, paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    break;
                    localAccount7 = null;
                    break label764;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                Account localAccount6;
                label851: String str1;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount6 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str1 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label901;
                }
                label901: for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
                {
                    removePeriodicSync(localAccount6, str1, localBundle1);
                    paramParcel2.writeNoException();
                    break;
                    localAccount6 = null;
                    break label851;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount5 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount5 = null)
                {
                    int n = getIsSyncable(localAccount5, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(n);
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount4 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount4 = null)
                {
                    setIsSyncable(localAccount4, paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                int m;
                for (int k = j; ; m = 0)
                {
                    setMasterSyncAutomatically(k);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                boolean bool3 = getMasterSyncAutomatically();
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount3 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount3 = null)
                {
                    boolean bool2 = isSyncActive(localAccount3, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool2)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                List localList1 = getCurrentSyncs();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList1);
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                SyncAdapterType[] arrayOfSyncAdapterType = getSyncAdapterTypes();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedArray(arrayOfSyncAdapterType, j);
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount2 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount2 = null)
                {
                    SyncStatusInfo localSyncStatusInfo = getSyncStatus(localAccount2, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localSyncStatusInfo == null)
                        break label1279;
                    paramParcel2.writeInt(j);
                    localSyncStatusInfo.writeToParcel(paramParcel2, j);
                    break;
                }
                label1279: paramParcel2.writeInt(0);
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount1 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount1 = null)
                {
                    boolean bool1 = isSyncPending(localAccount1, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool1)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("android.content.IContentService");
                addStatusChangeListener(paramParcel1.readInt(), ISyncStatusObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.content.IContentService");
                removeStatusChangeListener(ISyncStatusObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IContentService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addPeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                localParcel1.writeLong(paramLong);
                                this.mRemote.transact(9, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void addStatusChangeListener(int paramInt, ISyncStatusObserver paramISyncStatusObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    localParcel1.writeInt(paramInt);
                    if (paramISyncStatusObserver != null)
                    {
                        localIBinder = paramISyncStatusObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelSync(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<SyncInfo> getCurrentSyncs()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(SyncInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.IContentService";
            }

            public int getIsSyncable(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getMasterSyncAutomatically()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<PeriodicSync> getPeriodicSyncs(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        ArrayList localArrayList = localParcel2.createTypedArrayList(PeriodicSync.CREATOR);
                        return localArrayList;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public SyncAdapterType[] getSyncAdapterTypes()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    SyncAdapterType[] arrayOfSyncAdapterType = (SyncAdapterType[])localParcel2.createTypedArray(SyncAdapterType.CREATOR);
                    return arrayOfSyncAdapterType;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getSyncAutomatically(Account paramAccount, String paramString)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            this.mRemote.transact(6, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public SyncStatusInfo getSyncStatus(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            this.mRemote.transact(18, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localSyncStatusInfo = (SyncStatusInfo)SyncStatusInfo.CREATOR.createFromParcel(localParcel2);
                                return localSyncStatusInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    SyncStatusInfo localSyncStatusInfo = null;
                }
            }

            public boolean isSyncActive(Account paramAccount, String paramString)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            this.mRemote.transact(15, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean isSyncPending(Account paramAccount, String paramString)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            this.mRemote.transact(19, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void notifyChange(Uri paramUri, IContentObserver paramIContentObserver, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            if (paramIContentObserver != null)
                            {
                                localIBinder = paramIContentObserver.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                if (!paramBoolean1)
                                    break label145;
                                j = i;
                                localParcel1.writeInt(j);
                                if (!paramBoolean2)
                                    break label151;
                                localParcel1.writeInt(i);
                                this.mRemote.transact(3, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                    continue;
                    label145: int j = 0;
                    continue;
                    label151: i = 0;
                }
            }

            public void registerContentObserver(Uri paramUri, boolean paramBoolean, IContentObserver paramIContentObserver)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    IBinder localIBinder;
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            break label134;
                            localParcel1.writeInt(i);
                            if (paramIContentObserver != null)
                            {
                                localIBinder = paramIContentObserver.asBinder();
                                label59: localParcel1.writeStrongBinder(localIBinder);
                                this.mRemote.transact(1, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label134: 
                    do
                    {
                        i = 0;
                        break;
                        localIBinder = null;
                        break label59;
                    }
                    while (!paramBoolean);
                }
            }

            public void removePeriodicSync(Account paramAccount, String paramString, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(10, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void removeStatusChangeListener(ISyncStatusObserver paramISyncStatusObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramISyncStatusObserver != null)
                    {
                        localIBinder = paramISyncStatusObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(21, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void requestSync(Account paramAccount, String paramString, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(4, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void setIsSyncable(Account paramAccount, String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(12, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMasterSyncAutomatically(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setSyncAutomatically(Account paramAccount, String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.IContentService");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramBoolean)
                            {
                                localParcel1.writeInt(i);
                                this.mRemote.transact(7, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    i = 0;
                }
            }

            public void unregisterContentObserver(IContentObserver paramIContentObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IContentService");
                    if (paramIContentObserver != null)
                    {
                        localIBinder = paramIContentObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IContentService
 * JD-Core Version:        0.6.2
 */