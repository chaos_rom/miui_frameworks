package android.content;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class ContentQueryMap extends Observable
{
    private String[] mColumnNames;
    private ContentObserver mContentObserver;
    private volatile Cursor mCursor;
    private boolean mDirty = false;
    private Handler mHandlerForUpdateNotifications = null;
    private boolean mKeepUpdated = false;
    private int mKeyColumn;
    private Map<String, ContentValues> mValues = null;

    public ContentQueryMap(Cursor paramCursor, String paramString, boolean paramBoolean, Handler paramHandler)
    {
        this.mCursor = paramCursor;
        this.mColumnNames = this.mCursor.getColumnNames();
        this.mKeyColumn = this.mCursor.getColumnIndexOrThrow(paramString);
        this.mHandlerForUpdateNotifications = paramHandler;
        setKeepUpdated(paramBoolean);
        if (!paramBoolean)
            readCursorIntoCache(paramCursor);
    }

    /** @deprecated */
    private void readCursorIntoCache(Cursor paramCursor)
    {
        while (true)
        {
            int j;
            try
            {
                if (this.mValues == null)
                    break label133;
                i = this.mValues.size();
                this.mValues = new HashMap(i);
                if (paramCursor.moveToNext())
                {
                    ContentValues localContentValues = new ContentValues();
                    j = 0;
                    if (j < this.mColumnNames.length)
                    {
                        if (j == this.mKeyColumn)
                            break label127;
                        localContentValues.put(this.mColumnNames[j], paramCursor.getString(j));
                        break label127;
                    }
                    this.mValues.put(paramCursor.getString(this.mKeyColumn), localContentValues);
                }
            }
            finally
            {
            }
            return;
            label127: j++;
            continue;
            label133: int i = 0;
        }
    }

    /** @deprecated */
    public void close()
    {
        try
        {
            if (this.mContentObserver != null)
            {
                this.mCursor.unregisterContentObserver(this.mContentObserver);
                this.mContentObserver = null;
            }
            this.mCursor.close();
            this.mCursor = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mCursor != null)
            close();
        super.finalize();
    }

    /** @deprecated */
    public Map<String, ContentValues> getRows()
    {
        try
        {
            if (this.mDirty)
                requery();
            Map localMap = this.mValues;
            return localMap;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public ContentValues getValues(String paramString)
    {
        try
        {
            if (this.mDirty)
                requery();
            ContentValues localContentValues = (ContentValues)this.mValues.get(paramString);
            return localContentValues;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void requery()
    {
        Cursor localCursor = this.mCursor;
        if (localCursor == null);
        while (true)
        {
            return;
            this.mDirty = false;
            if (localCursor.requery())
            {
                readCursorIntoCache(localCursor);
                setChanged();
                notifyObservers();
            }
        }
    }

    public void setKeepUpdated(boolean paramBoolean)
    {
        if (paramBoolean == this.mKeepUpdated);
        while (true)
        {
            return;
            this.mKeepUpdated = paramBoolean;
            if (!this.mKeepUpdated)
            {
                this.mCursor.unregisterContentObserver(this.mContentObserver);
                this.mContentObserver = null;
            }
            else
            {
                if (this.mHandlerForUpdateNotifications == null)
                    this.mHandlerForUpdateNotifications = new Handler();
                if (this.mContentObserver == null)
                    this.mContentObserver = new ContentObserver(this.mHandlerForUpdateNotifications)
                    {
                        public void onChange(boolean paramAnonymousBoolean)
                        {
                            if (ContentQueryMap.this.countObservers() != 0)
                                ContentQueryMap.this.requery();
                            while (true)
                            {
                                return;
                                ContentQueryMap.access$002(ContentQueryMap.this, true);
                            }
                        }
                    };
                this.mCursor.registerContentObserver(this.mContentObserver);
                this.mDirty = true;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentQueryMap
 * JD-Core Version:        0.6.2
 */