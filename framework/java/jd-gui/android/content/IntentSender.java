package android.content;

import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.util.AndroidException;

public class IntentSender
    implements Parcelable
{
    public static final Parcelable.Creator<IntentSender> CREATOR = new Parcelable.Creator()
    {
        public IntentSender createFromParcel(Parcel paramAnonymousParcel)
        {
            IBinder localIBinder = paramAnonymousParcel.readStrongBinder();
            if (localIBinder != null);
            for (IntentSender localIntentSender = new IntentSender(localIBinder); ; localIntentSender = null)
                return localIntentSender;
        }

        public IntentSender[] newArray(int paramAnonymousInt)
        {
            return new IntentSender[paramAnonymousInt];
        }
    };
    private final IIntentSender mTarget;

    public IntentSender(IIntentSender paramIIntentSender)
    {
        this.mTarget = paramIIntentSender;
    }

    public IntentSender(IBinder paramIBinder)
    {
        this.mTarget = IIntentSender.Stub.asInterface(paramIBinder);
    }

    public static IntentSender readIntentSenderOrNullFromParcel(Parcel paramParcel)
    {
        IBinder localIBinder = paramParcel.readStrongBinder();
        if (localIBinder != null);
        for (IntentSender localIntentSender = new IntentSender(localIBinder); ; localIntentSender = null)
            return localIntentSender;
    }

    public static void writeIntentSenderOrNullToParcel(IntentSender paramIntentSender, Parcel paramParcel)
    {
        if (paramIntentSender != null);
        for (IBinder localIBinder = paramIntentSender.mTarget.asBinder(); ; localIBinder = null)
        {
            paramParcel.writeStrongBinder(localIBinder);
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        if ((paramObject instanceof IntentSender));
        for (boolean bool = this.mTarget.asBinder().equals(((IntentSender)paramObject).mTarget.asBinder()); ; bool = false)
            return bool;
    }

    public IIntentSender getTarget()
    {
        return this.mTarget;
    }

    public String getTargetPackage()
    {
        try
        {
            String str2 = ActivityManagerNative.getDefault().getPackageForIntentSender(this.mTarget);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String str1 = null;
        }
    }

    public int hashCode()
    {
        return this.mTarget.asBinder().hashCode();
    }

    public void sendIntent(Context paramContext, int paramInt, Intent paramIntent, OnFinished paramOnFinished, Handler paramHandler)
        throws IntentSender.SendIntentException
    {
        sendIntent(paramContext, paramInt, paramIntent, paramOnFinished, paramHandler, null);
    }

    public void sendIntent(Context paramContext, int paramInt, Intent paramIntent, OnFinished paramOnFinished, Handler paramHandler, String paramString)
        throws IntentSender.SendIntentException
    {
        FinishedDispatcher localFinishedDispatcher = null;
        if (paramIntent != null);
        while (true)
        {
            try
            {
                str = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
                IIntentSender localIIntentSender = this.mTarget;
                if (paramOnFinished != null)
                    localFinishedDispatcher = new FinishedDispatcher(this, paramOnFinished, paramHandler);
                if (localIIntentSender.send(paramInt, paramIntent, str, localFinishedDispatcher, paramString) >= 0)
                    break;
                throw new SendIntentException();
            }
            catch (RemoteException localRemoteException)
            {
                throw new SendIntentException();
            }
            String str = null;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("IntentSender{");
        localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuilder.append(": ");
        if (this.mTarget != null);
        for (IBinder localIBinder = this.mTarget.asBinder(); ; localIBinder = null)
        {
            localStringBuilder.append(localIBinder);
            localStringBuilder.append('}');
            return localStringBuilder.toString();
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongBinder(this.mTarget.asBinder());
    }

    private static class FinishedDispatcher extends IIntentReceiver.Stub
        implements Runnable
    {
        private final Handler mHandler;
        private Intent mIntent;
        private final IntentSender mIntentSender;
        private int mResultCode;
        private String mResultData;
        private Bundle mResultExtras;
        private final IntentSender.OnFinished mWho;

        FinishedDispatcher(IntentSender paramIntentSender, IntentSender.OnFinished paramOnFinished, Handler paramHandler)
        {
            this.mIntentSender = paramIntentSender;
            this.mWho = paramOnFinished;
            this.mHandler = paramHandler;
        }

        public void performReceive(Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
        {
            this.mIntent = paramIntent;
            this.mResultCode = paramInt;
            this.mResultData = paramString;
            this.mResultExtras = paramBundle;
            if (this.mHandler == null)
                run();
            while (true)
            {
                return;
                this.mHandler.post(this);
            }
        }

        public void run()
        {
            this.mWho.onSendFinished(this.mIntentSender, this.mIntent, this.mResultCode, this.mResultData, this.mResultExtras);
        }
    }

    public static abstract interface OnFinished
    {
        public abstract void onSendFinished(IntentSender paramIntentSender, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle);
    }

    public static class SendIntentException extends AndroidException
    {
        public SendIntentException()
        {
        }

        public SendIntentException(Exception paramException)
        {
            super();
        }

        public SendIntentException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IntentSender
 * JD-Core Version:        0.6.2
 */