package android.content;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IOnPrimaryClipChangedListener extends IInterface
{
    public abstract void dispatchPrimaryClipChanged()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IOnPrimaryClipChangedListener
    {
        private static final String DESCRIPTOR = "android.content.IOnPrimaryClipChangedListener";
        static final int TRANSACTION_dispatchPrimaryClipChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.content.IOnPrimaryClipChangedListener");
        }

        public static IOnPrimaryClipChangedListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.IOnPrimaryClipChangedListener");
                if ((localIInterface != null) && ((localIInterface instanceof IOnPrimaryClipChangedListener)))
                    localObject = (IOnPrimaryClipChangedListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.content.IOnPrimaryClipChangedListener");
                continue;
                paramParcel1.enforceInterface("android.content.IOnPrimaryClipChangedListener");
                dispatchPrimaryClipChanged();
            }
        }

        private static class Proxy
            implements IOnPrimaryClipChangedListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dispatchPrimaryClipChanged()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.content.IOnPrimaryClipChangedListener");
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.IOnPrimaryClipChangedListener";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IOnPrimaryClipChangedListener
 * JD-Core Version:        0.6.2
 */