package android.content;

import android.os.IBinder;

public abstract interface ServiceConnection
{
    public abstract void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder);

    public abstract void onServiceDisconnected(ComponentName paramComponentName);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ServiceConnection
 * JD-Core Version:        0.6.2
 */