package android.content;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractThreadedSyncAdapter
{

    @Deprecated
    public static final int LOG_SYNC_DETAILS = 2743;
    private boolean mAllowParallelSyncs;
    private final boolean mAutoInitialize;
    private final Context mContext;
    private final ISyncAdapterImpl mISyncAdapterImpl;
    private final AtomicInteger mNumSyncStarts;
    private final Object mSyncThreadLock = new Object();
    private final HashMap<Account, SyncThread> mSyncThreads = new HashMap();

    public AbstractThreadedSyncAdapter(Context paramContext, boolean paramBoolean)
    {
        this(paramContext, paramBoolean, false);
    }

    public AbstractThreadedSyncAdapter(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mContext = paramContext;
        this.mISyncAdapterImpl = new ISyncAdapterImpl(null);
        this.mNumSyncStarts = new AtomicInteger(0);
        this.mAutoInitialize = paramBoolean1;
        this.mAllowParallelSyncs = paramBoolean2;
    }

    private Account toSyncKey(Account paramAccount)
    {
        if (this.mAllowParallelSyncs);
        while (true)
        {
            return paramAccount;
            paramAccount = null;
        }
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public final IBinder getSyncAdapterBinder()
    {
        return this.mISyncAdapterImpl.asBinder();
    }

    public abstract void onPerformSync(Account paramAccount, Bundle paramBundle, String paramString, ContentProviderClient paramContentProviderClient, SyncResult paramSyncResult);

    public void onSyncCanceled()
    {
        synchronized (this.mSyncThreadLock)
        {
            SyncThread localSyncThread = (SyncThread)this.mSyncThreads.get(null);
            if (localSyncThread != null)
                localSyncThread.interrupt();
            return;
        }
    }

    public void onSyncCanceled(Thread paramThread)
    {
        paramThread.interrupt();
    }

    private class SyncThread extends Thread
    {
        private final Account mAccount;
        private final String mAuthority;
        private final Bundle mExtras;
        private final SyncContext mSyncContext;
        private final Account mThreadsKey;

        private SyncThread(String paramSyncContext, SyncContext paramString1, String paramAccount, Account paramBundle, Bundle arg6)
        {
            super();
            this.mSyncContext = paramString1;
            this.mAuthority = paramAccount;
            this.mAccount = paramBundle;
            Object localObject;
            this.mExtras = localObject;
            this.mThreadsKey = AbstractThreadedSyncAdapter.this.toSyncKey(paramBundle);
        }

        private boolean isCanceled()
        {
            return Thread.currentThread().isInterrupted();
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: bipush 10
            //     2: invokestatic 61	android/os/Process:setThreadPriority	(I)V
            //     5: ldc2_w 62
            //     8: aload_0
            //     9: getfield 29	android/content/AbstractThreadedSyncAdapter$SyncThread:mAuthority	Ljava/lang/String;
            //     12: invokestatic 69	android/os/Trace:traceBegin	(JLjava/lang/String;)V
            //     15: new 71	android/content/SyncResult
            //     18: dup
            //     19: invokespecial 73	android/content/SyncResult:<init>	()V
            //     22: astore_1
            //     23: aconst_null
            //     24: astore_2
            //     25: aload_0
            //     26: invokespecial 75	android/content/AbstractThreadedSyncAdapter$SyncThread:isCanceled	()Z
            //     29: istore 8
            //     31: iload 8
            //     33: ifeq +61 -> 94
            //     36: ldc2_w 62
            //     39: invokestatic 79	android/os/Trace:traceEnd	(J)V
            //     42: iconst_0
            //     43: ifeq +5 -> 48
            //     46: aconst_null
            //     47: athrow
            //     48: aload_0
            //     49: invokespecial 75	android/content/AbstractThreadedSyncAdapter$SyncThread:isCanceled	()Z
            //     52: ifne +11 -> 63
            //     55: aload_0
            //     56: getfield 27	android/content/AbstractThreadedSyncAdapter$SyncThread:mSyncContext	Landroid/content/SyncContext;
            //     59: aload_1
            //     60: invokevirtual 85	android/content/SyncContext:onFinished	(Landroid/content/SyncResult;)V
            //     63: aload_0
            //     64: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     67: invokestatic 89	android/content/AbstractThreadedSyncAdapter:access$200	(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;
            //     70: astore 13
            //     72: aload 13
            //     74: monitorenter
            //     75: aload_0
            //     76: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     79: invokestatic 93	android/content/AbstractThreadedSyncAdapter:access$300	(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;
            //     82: aload_0
            //     83: getfield 39	android/content/AbstractThreadedSyncAdapter$SyncThread:mThreadsKey	Landroid/accounts/Account;
            //     86: invokevirtual 99	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
            //     89: pop
            //     90: aload 13
            //     92: monitorexit
            //     93: return
            //     94: aload_0
            //     95: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     98: invokestatic 103	android/content/AbstractThreadedSyncAdapter:access$900	(Landroid/content/AbstractThreadedSyncAdapter;)Landroid/content/Context;
            //     101: invokevirtual 109	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     104: aload_0
            //     105: getfield 29	android/content/AbstractThreadedSyncAdapter$SyncThread:mAuthority	Ljava/lang/String;
            //     108: invokevirtual 115	android/content/ContentResolver:acquireContentProviderClient	(Ljava/lang/String;)Landroid/content/ContentProviderClient;
            //     111: astore_2
            //     112: aload_2
            //     113: ifnull +95 -> 208
            //     116: aload_0
            //     117: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     120: aload_0
            //     121: getfield 31	android/content/AbstractThreadedSyncAdapter$SyncThread:mAccount	Landroid/accounts/Account;
            //     124: aload_0
            //     125: getfield 33	android/content/AbstractThreadedSyncAdapter$SyncThread:mExtras	Landroid/os/Bundle;
            //     128: aload_0
            //     129: getfield 29	android/content/AbstractThreadedSyncAdapter$SyncThread:mAuthority	Ljava/lang/String;
            //     132: aload_2
            //     133: aload_1
            //     134: invokevirtual 119	android/content/AbstractThreadedSyncAdapter:onPerformSync	(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
            //     137: ldc2_w 62
            //     140: invokestatic 79	android/os/Trace:traceEnd	(J)V
            //     143: aload_2
            //     144: ifnull +8 -> 152
            //     147: aload_2
            //     148: invokevirtual 124	android/content/ContentProviderClient:release	()Z
            //     151: pop
            //     152: aload_0
            //     153: invokespecial 75	android/content/AbstractThreadedSyncAdapter$SyncThread:isCanceled	()Z
            //     156: ifne +11 -> 167
            //     159: aload_0
            //     160: getfield 27	android/content/AbstractThreadedSyncAdapter$SyncThread:mSyncContext	Landroid/content/SyncContext;
            //     163: aload_1
            //     164: invokevirtual 85	android/content/SyncContext:onFinished	(Landroid/content/SyncResult;)V
            //     167: aload_0
            //     168: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     171: invokestatic 89	android/content/AbstractThreadedSyncAdapter:access$200	(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;
            //     174: astore 9
            //     176: aload 9
            //     178: monitorenter
            //     179: aload_0
            //     180: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     183: invokestatic 93	android/content/AbstractThreadedSyncAdapter:access$300	(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;
            //     186: aload_0
            //     187: getfield 39	android/content/AbstractThreadedSyncAdapter$SyncThread:mThreadsKey	Landroid/accounts/Account;
            //     190: invokevirtual 99	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
            //     193: pop
            //     194: aload 9
            //     196: monitorexit
            //     197: goto -104 -> 93
            //     200: astore 10
            //     202: aload 9
            //     204: monitorexit
            //     205: aload 10
            //     207: athrow
            //     208: aload_1
            //     209: iconst_1
            //     210: putfield 128	android/content/SyncResult:databaseError	Z
            //     213: goto -76 -> 137
            //     216: astore_3
            //     217: ldc2_w 62
            //     220: invokestatic 79	android/os/Trace:traceEnd	(J)V
            //     223: aload_2
            //     224: ifnull +8 -> 232
            //     227: aload_2
            //     228: invokevirtual 124	android/content/ContentProviderClient:release	()Z
            //     231: pop
            //     232: aload_0
            //     233: invokespecial 75	android/content/AbstractThreadedSyncAdapter$SyncThread:isCanceled	()Z
            //     236: ifne +11 -> 247
            //     239: aload_0
            //     240: getfield 27	android/content/AbstractThreadedSyncAdapter$SyncThread:mSyncContext	Landroid/content/SyncContext;
            //     243: aload_1
            //     244: invokevirtual 85	android/content/SyncContext:onFinished	(Landroid/content/SyncResult;)V
            //     247: aload_0
            //     248: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     251: invokestatic 89	android/content/AbstractThreadedSyncAdapter:access$200	(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;
            //     254: astore 4
            //     256: aload 4
            //     258: monitorenter
            //     259: aload_0
            //     260: getfield 22	android/content/AbstractThreadedSyncAdapter$SyncThread:this$0	Landroid/content/AbstractThreadedSyncAdapter;
            //     263: invokestatic 93	android/content/AbstractThreadedSyncAdapter:access$300	(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;
            //     266: aload_0
            //     267: getfield 39	android/content/AbstractThreadedSyncAdapter$SyncThread:mThreadsKey	Landroid/accounts/Account;
            //     270: invokevirtual 99	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
            //     273: pop
            //     274: aload 4
            //     276: monitorexit
            //     277: aload_3
            //     278: athrow
            //     279: astore 5
            //     281: aload 4
            //     283: monitorexit
            //     284: aload 5
            //     286: athrow
            //     287: astore 14
            //     289: aload 13
            //     291: monitorexit
            //     292: aload 14
            //     294: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     179	205	200	finally
            //     25	31	216	finally
            //     94	137	216	finally
            //     208	213	216	finally
            //     259	277	279	finally
            //     281	284	279	finally
            //     75	93	287	finally
            //     289	292	287	finally
        }
    }

    private class ISyncAdapterImpl extends ISyncAdapter.Stub
    {
        private ISyncAdapterImpl()
        {
        }

        public void cancelSync(ISyncContext paramISyncContext)
        {
            Object localObject1 = null;
            while (true)
            {
                synchronized (AbstractThreadedSyncAdapter.this.mSyncThreadLock)
                {
                    Iterator localIterator = AbstractThreadedSyncAdapter.this.mSyncThreads.values().iterator();
                    if (localIterator.hasNext())
                    {
                        AbstractThreadedSyncAdapter.SyncThread localSyncThread = (AbstractThreadedSyncAdapter.SyncThread)localIterator.next();
                        if (localSyncThread.mSyncContext.getSyncContextBinder() != paramISyncContext.asBinder())
                            continue;
                        localObject1 = localSyncThread;
                    }
                    if (localObject1 != null)
                    {
                        if (AbstractThreadedSyncAdapter.this.mAllowParallelSyncs)
                            AbstractThreadedSyncAdapter.this.onSyncCanceled(localObject1);
                    }
                    else
                        return;
                }
                AbstractThreadedSyncAdapter.this.onSyncCanceled();
            }
        }

        public void initialize(Account paramAccount, String paramString)
            throws RemoteException
        {
            Bundle localBundle = new Bundle();
            localBundle.putBoolean("initialize", true);
            startSync(null, paramString, paramAccount, localBundle);
        }

        public void startSync(ISyncContext paramISyncContext, String paramString, Account paramAccount, Bundle paramBundle)
        {
            SyncContext localSyncContext = new SyncContext(paramISyncContext);
            Account localAccount = AbstractThreadedSyncAdapter.this.toSyncKey(paramAccount);
            synchronized (AbstractThreadedSyncAdapter.this.mSyncThreadLock)
            {
                int i;
                if (!AbstractThreadedSyncAdapter.this.mSyncThreads.containsKey(localAccount))
                {
                    if ((AbstractThreadedSyncAdapter.this.mAutoInitialize) && (paramBundle != null) && (paramBundle.getBoolean("initialize", false)))
                    {
                        if (ContentResolver.getIsSyncable(paramAccount, paramString) < 0)
                            ContentResolver.setIsSyncable(paramAccount, paramString, 1);
                        localSyncContext.onFinished(new SyncResult());
                    }
                    else
                    {
                        AbstractThreadedSyncAdapter.SyncThread localSyncThread = new AbstractThreadedSyncAdapter.SyncThread(AbstractThreadedSyncAdapter.this, "SyncAdapterThread-" + AbstractThreadedSyncAdapter.this.mNumSyncStarts.incrementAndGet(), localSyncContext, paramString, paramAccount, paramBundle, null);
                        AbstractThreadedSyncAdapter.this.mSyncThreads.put(localAccount, localSyncThread);
                        localSyncThread.start();
                        i = 0;
                        if (i != 0)
                            localSyncContext.onFinished(SyncResult.ALREADY_IN_PROGRESS);
                    }
                }
                else
                    i = 1;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.AbstractThreadedSyncAdapter
 * JD-Core Version:        0.6.2
 */