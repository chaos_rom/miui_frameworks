package android.content.res;

import android.util.TypedValue;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParserException;

final class XmlBlock
{
    private static final boolean DEBUG;
    private final AssetManager mAssets;
    private final int mNative;
    private boolean mOpen = true;
    private int mOpenCount = 1;
    final StringBlock mStrings;

    XmlBlock(AssetManager paramAssetManager, int paramInt)
    {
        this.mAssets = paramAssetManager;
        this.mNative = paramInt;
        this.mStrings = new StringBlock(nativeGetStringBlock(paramInt), false);
    }

    public XmlBlock(byte[] paramArrayOfByte)
    {
        this.mAssets = null;
        this.mNative = nativeCreate(paramArrayOfByte, 0, paramArrayOfByte.length);
        this.mStrings = new StringBlock(nativeGetStringBlock(this.mNative), false);
    }

    public XmlBlock(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        this.mAssets = null;
        this.mNative = nativeCreate(paramArrayOfByte, paramInt1, paramInt2);
        this.mStrings = new StringBlock(nativeGetStringBlock(this.mNative), false);
    }

    private void decOpenCountLocked()
    {
        this.mOpenCount = (-1 + this.mOpenCount);
        if (this.mOpenCount == 0)
        {
            nativeDestroy(this.mNative);
            if (this.mAssets != null)
                this.mAssets.xmlBlockGone(hashCode());
        }
    }

    private static final native int nativeCreate(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    private static final native int nativeCreateParseState(int paramInt);

    private static final native void nativeDestroy(int paramInt);

    private static final native void nativeDestroyParseState(int paramInt);

    private static final native int nativeGetAttributeCount(int paramInt);

    private static final native int nativeGetAttributeData(int paramInt1, int paramInt2);

    private static final native int nativeGetAttributeDataType(int paramInt1, int paramInt2);

    private static final native int nativeGetAttributeIndex(int paramInt, String paramString1, String paramString2);

    private static final native int nativeGetAttributeName(int paramInt1, int paramInt2);

    private static final native int nativeGetAttributeNamespace(int paramInt1, int paramInt2);

    private static final native int nativeGetAttributeResource(int paramInt1, int paramInt2);

    private static final native int nativeGetAttributeStringValue(int paramInt1, int paramInt2);

    private static final native int nativeGetClassAttribute(int paramInt);

    private static final native int nativeGetIdAttribute(int paramInt);

    private static final native int nativeGetLineNumber(int paramInt);

    static final native int nativeGetName(int paramInt);

    private static final native int nativeGetNamespace(int paramInt);

    private static final native int nativeGetStringBlock(int paramInt);

    private static final native int nativeGetStyleAttribute(int paramInt);

    private static final native int nativeGetText(int paramInt);

    static final native int nativeNext(int paramInt);

    public void close()
    {
        try
        {
            if (this.mOpen)
            {
                this.mOpen = false;
                decOpenCountLocked();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void finalize()
        throws Throwable
    {
        close();
    }

    public XmlResourceParser newParser()
    {
        Parser localParser;
        try
        {
            if (this.mNative != 0)
                localParser = new Parser(nativeCreateParseState(this.mNative), this);
            else
                localParser = null;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
        return localParser;
    }

    final class Parser
        implements XmlResourceParser
    {
        private final XmlBlock mBlock;
        private boolean mDecNextDepth = false;
        private int mDepth = 0;
        private int mEventType = 0;
        int mParseState;
        private boolean mStarted = false;

        Parser(int paramXmlBlock, XmlBlock arg3)
        {
            this.mParseState = paramXmlBlock;
            XmlBlock localXmlBlock;
            this.mBlock = localXmlBlock;
            XmlBlock.access$008(localXmlBlock);
        }

        public void close()
        {
            synchronized (this.mBlock)
            {
                if (this.mParseState != 0)
                {
                    XmlBlock.nativeDestroyParseState(this.mParseState);
                    this.mParseState = 0;
                    this.mBlock.decOpenCountLocked();
                }
                return;
            }
        }

        public void defineEntityReplacementText(String paramString1, String paramString2)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("defineEntityReplacementText() not supported");
        }

        protected void finalize()
            throws Throwable
        {
            close();
        }

        public boolean getAttributeBooleanValue(int paramInt, boolean paramBoolean)
        {
            int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt);
            boolean bool;
            if ((i >= 16) && (i <= 31))
                if (XmlBlock.nativeGetAttributeData(this.mParseState, paramInt) != 0)
                    bool = true;
            while (true)
            {
                return bool;
                bool = false;
                continue;
                bool = paramBoolean;
            }
        }

        public boolean getAttributeBooleanValue(String paramString1, String paramString2, boolean paramBoolean)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0)
                paramBoolean = getAttributeBooleanValue(i, paramBoolean);
            return paramBoolean;
        }

        public int getAttributeCount()
        {
            if (this.mEventType == 2);
            for (int i = XmlBlock.nativeGetAttributeCount(this.mParseState); ; i = -1)
                return i;
        }

        public float getAttributeFloatValue(int paramInt, float paramFloat)
        {
            if (XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt) == 4)
                return Float.intBitsToFloat(XmlBlock.nativeGetAttributeData(this.mParseState, paramInt));
            throw new RuntimeException("not a float!");
        }

        public float getAttributeFloatValue(String paramString1, String paramString2, float paramFloat)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0)
                paramFloat = getAttributeFloatValue(i, paramFloat);
            return paramFloat;
        }

        public int getAttributeIntValue(int paramInt1, int paramInt2)
        {
            int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt1);
            if ((i >= 16) && (i <= 31))
                paramInt2 = XmlBlock.nativeGetAttributeData(this.mParseState, paramInt1);
            return paramInt2;
        }

        public int getAttributeIntValue(String paramString1, String paramString2, int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0)
                paramInt = getAttributeIntValue(i, paramInt);
            return paramInt;
        }

        public int getAttributeListValue(int paramInt1, String[] paramArrayOfString, int paramInt2)
        {
            int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt1);
            int j = XmlBlock.nativeGetAttributeData(this.mParseState, paramInt1);
            if (i == 3)
                j = XmlUtils.convertValueToList(XmlBlock.this.mStrings.get(j), paramArrayOfString, paramInt2);
            return j;
        }

        public int getAttributeListValue(String paramString1, String paramString2, String[] paramArrayOfString, int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0)
                paramInt = getAttributeListValue(i, paramArrayOfString, paramInt);
            return paramInt;
        }

        public String getAttributeName(int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeName(this.mParseState, paramInt);
            if (i >= 0)
                return XmlBlock.this.mStrings.get(i).toString();
            throw new IndexOutOfBoundsException(String.valueOf(paramInt));
        }

        public int getAttributeNameResource(int paramInt)
        {
            return XmlBlock.nativeGetAttributeResource(this.mParseState, paramInt);
        }

        public String getAttributeNamespace(int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeNamespace(this.mParseState, paramInt);
            if (i >= 0);
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = "")
            {
                return str;
                if (i != -1)
                    break;
            }
            throw new IndexOutOfBoundsException(String.valueOf(paramInt));
        }

        public String getAttributePrefix(int paramInt)
        {
            throw new RuntimeException("getAttributePrefix not supported");
        }

        public int getAttributeResourceValue(int paramInt1, int paramInt2)
        {
            if (XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt1) == 1)
                paramInt2 = XmlBlock.nativeGetAttributeData(this.mParseState, paramInt1);
            return paramInt2;
        }

        public int getAttributeResourceValue(String paramString1, String paramString2, int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0)
                paramInt = getAttributeResourceValue(i, paramInt);
            return paramInt;
        }

        public String getAttributeType(int paramInt)
        {
            return "CDATA";
        }

        public int getAttributeUnsignedIntValue(int paramInt1, int paramInt2)
        {
            int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt1);
            if ((i >= 16) && (i <= 31))
                paramInt2 = XmlBlock.nativeGetAttributeData(this.mParseState, paramInt1);
            return paramInt2;
        }

        public int getAttributeUnsignedIntValue(String paramString1, String paramString2, int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0)
                paramInt = getAttributeUnsignedIntValue(i, paramInt);
            return paramInt;
        }

        public String getAttributeValue(int paramInt)
        {
            int i = XmlBlock.nativeGetAttributeStringValue(this.mParseState, paramInt);
            if (i >= 0);
            int j;
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = TypedValue.coerceToString(j, XmlBlock.nativeGetAttributeData(this.mParseState, paramInt)))
            {
                return str;
                j = XmlBlock.nativeGetAttributeDataType(this.mParseState, paramInt);
                if (j == 0)
                    throw new IndexOutOfBoundsException(String.valueOf(paramInt));
            }
        }

        public String getAttributeValue(String paramString1, String paramString2)
        {
            int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, paramString1, paramString2);
            if (i >= 0);
            for (String str = getAttributeValue(i); ; str = null)
                return str;
        }

        public String getClassAttribute()
        {
            int i = XmlBlock.nativeGetClassAttribute(this.mParseState);
            if (i >= 0);
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = null)
                return str;
        }

        public int getColumnNumber()
        {
            return -1;
        }

        public int getDepth()
        {
            return this.mDepth;
        }

        public int getEventType()
            throws XmlPullParserException
        {
            return this.mEventType;
        }

        public boolean getFeature(String paramString)
        {
            boolean bool = true;
            if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(paramString));
            while (true)
            {
                return bool;
                if (!"http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes".equals(paramString))
                    bool = false;
            }
        }

        public String getIdAttribute()
        {
            int i = XmlBlock.nativeGetIdAttribute(this.mParseState);
            if (i >= 0);
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = null)
                return str;
        }

        public int getIdAttributeResourceValue(int paramInt)
        {
            return getAttributeResourceValue(null, "id", paramInt);
        }

        public String getInputEncoding()
        {
            return null;
        }

        public int getLineNumber()
        {
            return XmlBlock.nativeGetLineNumber(this.mParseState);
        }

        public String getName()
        {
            int i = XmlBlock.nativeGetName(this.mParseState);
            if (i >= 0);
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = null)
                return str;
        }

        public String getNamespace()
        {
            int i = XmlBlock.nativeGetNamespace(this.mParseState);
            if (i >= 0);
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = "")
                return str;
        }

        public String getNamespace(String paramString)
        {
            throw new RuntimeException("getNamespace() not supported");
        }

        public int getNamespaceCount(int paramInt)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("getNamespaceCount() not supported");
        }

        public String getNamespacePrefix(int paramInt)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("getNamespacePrefix() not supported");
        }

        public String getNamespaceUri(int paramInt)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("getNamespaceUri() not supported");
        }

        final CharSequence getPooledString(int paramInt)
        {
            return XmlBlock.this.mStrings.get(paramInt);
        }

        public String getPositionDescription()
        {
            return "Binary XML file line #" + getLineNumber();
        }

        public String getPrefix()
        {
            throw new RuntimeException("getPrefix not supported");
        }

        public Object getProperty(String paramString)
        {
            return null;
        }

        public int getStyleAttribute()
        {
            return XmlBlock.nativeGetStyleAttribute(this.mParseState);
        }

        public String getText()
        {
            int i = XmlBlock.nativeGetText(this.mParseState);
            if (i >= 0);
            for (String str = XmlBlock.this.mStrings.get(i).toString(); ; str = null)
                return str;
        }

        public char[] getTextCharacters(int[] paramArrayOfInt)
        {
            String str = getText();
            char[] arrayOfChar = null;
            if (str != null)
            {
                paramArrayOfInt[0] = 0;
                paramArrayOfInt[1] = str.length();
                arrayOfChar = new char[str.length()];
                str.getChars(0, str.length(), arrayOfChar, 0);
            }
            return arrayOfChar;
        }

        public boolean isAttributeDefault(int paramInt)
        {
            return false;
        }

        public boolean isEmptyElementTag()
            throws XmlPullParserException
        {
            return false;
        }

        public boolean isWhitespace()
            throws XmlPullParserException
        {
            return false;
        }

        public int next()
            throws XmlPullParserException, IOException
        {
            if (!this.mStarted)
                this.mStarted = true;
            for (int i = 0; ; i = 1)
            {
                return i;
                if (this.mParseState != 0)
                    break;
            }
            i = XmlBlock.nativeNext(this.mParseState);
            if (this.mDecNextDepth)
            {
                this.mDepth = (-1 + this.mDepth);
                this.mDecNextDepth = false;
            }
            switch (i)
            {
            default:
            case 2:
            case 3:
            }
            while (true)
            {
                this.mEventType = i;
                if (i != 1)
                    break;
                close();
                break;
                this.mDepth = (1 + this.mDepth);
                continue;
                this.mDecNextDepth = true;
            }
        }

        public int nextTag()
            throws XmlPullParserException, IOException
        {
            int i = next();
            if ((i == 4) && (isWhitespace()))
                i = next();
            if ((i != 2) && (i != 3))
                throw new XmlPullParserException(getPositionDescription() + ": expected start or end tag", this, null);
            return i;
        }

        public String nextText()
            throws XmlPullParserException, IOException
        {
            if (getEventType() != 2)
                throw new XmlPullParserException(getPositionDescription() + ": parser must be on START_TAG to read next text", this, null);
            int i = next();
            String str;
            if (i == 4)
            {
                str = getText();
                if (next() != 3)
                    throw new XmlPullParserException(getPositionDescription() + ": event TEXT it must be immediately followed by END_TAG", this, null);
            }
            else
            {
                if (i != 3)
                    break label107;
                str = "";
            }
            return str;
            label107: throw new XmlPullParserException(getPositionDescription() + ": parser must be on START_TAG or TEXT to read text", this, null);
        }

        public int nextToken()
            throws XmlPullParserException, IOException
        {
            return next();
        }

        public void require(int paramInt, String paramString1, String paramString2)
            throws XmlPullParserException, IOException
        {
            if ((paramInt != getEventType()) || ((paramString1 != null) && (!paramString1.equals(getNamespace()))) || ((paramString2 != null) && (!paramString2.equals(getName()))))
                throw new XmlPullParserException("expected " + TYPES[paramInt] + getPositionDescription());
        }

        public void setFeature(String paramString, boolean paramBoolean)
            throws XmlPullParserException
        {
            if (("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(paramString)) && (paramBoolean));
            while (("http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes".equals(paramString)) && (paramBoolean))
                return;
            throw new XmlPullParserException("Unsupported feature: " + paramString);
        }

        public void setInput(InputStream paramInputStream, String paramString)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("setInput() not supported");
        }

        public void setInput(Reader paramReader)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("setInput() not supported");
        }

        public void setProperty(String paramString, Object paramObject)
            throws XmlPullParserException
        {
            throw new XmlPullParserException("setProperty() not supported");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.XmlBlock
 * JD-Core Version:        0.6.2
 */