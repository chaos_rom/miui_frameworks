package android.content.res;

import java.io.File;
import java.io.IOException;

public class ObbScanner
{
    public static ObbInfo getObbInfo(String paramString)
        throws IOException
    {
        if (paramString == null)
            throw new IllegalArgumentException("file path cannot be null");
        File localFile = new File(paramString);
        if (!localFile.exists())
            throw new IllegalArgumentException("OBB file does not exist: " + paramString);
        String str = localFile.getCanonicalPath();
        ObbInfo localObbInfo = new ObbInfo();
        localObbInfo.filename = str;
        getObbInfo_native(str, localObbInfo);
        return localObbInfo;
    }

    private static native void getObbInfo_native(String paramString, ObbInfo paramObbInfo)
        throws IOException;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.ObbScanner
 * JD-Core Version:        0.6.2
 */