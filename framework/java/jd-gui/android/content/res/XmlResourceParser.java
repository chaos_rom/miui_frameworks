package android.content.res;

import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;

public abstract interface XmlResourceParser extends XmlPullParser, AttributeSet
{
    public abstract void close();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.XmlResourceParser
 * JD-Core Version:        0.6.2
 */