package android.content.res;

import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Rect;
import android.text.Annotation;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextPaint;
import android.text.TextUtils.TruncateAt;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.LineHeightSpan.WithDensity;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.SparseArray;
import com.android.internal.util.XmlUtils;

final class StringBlock
{
    private static final String TAG = "AssetManager";
    private static final boolean localLOGV;
    private final int mNative;
    private final boolean mOwnsNative;
    private SparseArray<CharSequence> mSparseStrings;
    private CharSequence[] mStrings;
    StyleIDs mStyleIDs = null;
    private final boolean mUseSparse;

    StringBlock(int paramInt, boolean paramBoolean)
    {
        this.mNative = paramInt;
        this.mUseSparse = paramBoolean;
        this.mOwnsNative = false;
    }

    public StringBlock(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        this.mNative = nativeCreate(paramArrayOfByte, paramInt1, paramInt2);
        this.mUseSparse = paramBoolean;
        this.mOwnsNative = true;
    }

    public StringBlock(byte[] paramArrayOfByte, boolean paramBoolean)
    {
        this.mNative = nativeCreate(paramArrayOfByte, 0, paramArrayOfByte.length);
        this.mUseSparse = paramBoolean;
        this.mOwnsNative = true;
    }

    private static void addParagraphSpan(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
    {
        int i = paramSpannable.length();
        if ((paramInt1 != 0) && (paramInt1 != i) && (paramSpannable.charAt(paramInt1 - 1) != '\n'))
        {
            paramInt1--;
            if ((paramInt1 > 0) && (paramSpannable.charAt(paramInt1 - 1) != '\n'));
        }
        else if ((paramInt2 != 0) && (paramInt2 != i) && (paramSpannable.charAt(paramInt2 - 1) != '\n'))
        {
            paramInt2++;
        }
        while (true)
        {
            if ((paramInt2 >= i) || (paramSpannable.charAt(paramInt2 - 1) == '\n'))
            {
                paramSpannable.setSpan(paramObject, paramInt1, paramInt2, 51);
                return;
                paramInt1--;
                break;
            }
            paramInt2++;
        }
    }

    private CharSequence applyStyles(String paramString, int[] paramArrayOfInt, StyleIDs paramStyleIDs)
    {
        if (paramArrayOfInt.length == 0);
        while (true)
        {
            return paramString;
            SpannableString localSpannableString = new SpannableString(paramString);
            int i = 0;
            if (i < paramArrayOfInt.length)
            {
                int j = paramArrayOfInt[i];
                if (j == paramStyleIDs.boldId)
                    localSpannableString.setSpan(new StyleSpan(1), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                label924: 
                while (true)
                {
                    i += 3;
                    break;
                    if (j == paramStyleIDs.italicId)
                    {
                        localSpannableString.setSpan(new StyleSpan(2), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.underlineId)
                    {
                        localSpannableString.setSpan(new UnderlineSpan(), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.ttId)
                    {
                        localSpannableString.setSpan(new TypefaceSpan("monospace"), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.bigId)
                    {
                        localSpannableString.setSpan(new RelativeSizeSpan(1.25F), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.smallId)
                    {
                        localSpannableString.setSpan(new RelativeSizeSpan(0.8F), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.subId)
                    {
                        localSpannableString.setSpan(new SubscriptSpan(), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.supId)
                    {
                        localSpannableString.setSpan(new SuperscriptSpan(), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.strikeId)
                    {
                        localSpannableString.setSpan(new StrikethroughSpan(), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                    }
                    else if (j == paramStyleIDs.listItemId)
                    {
                        addParagraphSpan(localSpannableString, new BulletSpan(10), paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)]);
                    }
                    else if (j == paramStyleIDs.marqueeId)
                    {
                        localSpannableString.setSpan(TextUtils.TruncateAt.MARQUEE, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 18);
                    }
                    else
                    {
                        String str1 = nativeGetString(this.mNative, j);
                        if (str1.startsWith("font;"))
                        {
                            String str5 = subtag(str1, ";height=");
                            if (str5 != null)
                            {
                                int i5 = Integer.parseInt(str5);
                                Height localHeight = new Height(i5);
                                addParagraphSpan(localSpannableString, localHeight, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)]);
                            }
                            String str6 = subtag(str1, ";size=");
                            if (str6 != null)
                            {
                                int i4 = Integer.parseInt(str6);
                                AbsoluteSizeSpan localAbsoluteSizeSpan = new AbsoluteSizeSpan(i4, true);
                                localSpannableString.setSpan(localAbsoluteSizeSpan, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                            }
                            String str7 = subtag(str1, ";fgcolor=");
                            if (str7 != null)
                            {
                                int i3 = XmlUtils.convertValueToUnsignedInt(str7, -1);
                                ForegroundColorSpan localForegroundColorSpan = new ForegroundColorSpan(i3);
                                localSpannableString.setSpan(localForegroundColorSpan, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                            }
                            String str8 = subtag(str1, ";bgcolor=");
                            if (str8 != null)
                            {
                                int i2 = XmlUtils.convertValueToUnsignedInt(str8, -1);
                                BackgroundColorSpan localBackgroundColorSpan = new BackgroundColorSpan(i2);
                                localSpannableString.setSpan(localBackgroundColorSpan, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                            }
                        }
                        else if (str1.startsWith("a;"))
                        {
                            String str4 = subtag(str1, ";href=");
                            if (str4 != null)
                            {
                                URLSpan localURLSpan = new URLSpan(str4);
                                localSpannableString.setSpan(localURLSpan, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                            }
                        }
                        else if (str1.startsWith("annotation;"))
                        {
                            int k = str1.length();
                            int i1;
                            for (int m = str1.indexOf(';'); ; m = i1)
                            {
                                if (m >= k)
                                    break label924;
                                int n = str1.indexOf('=', m);
                                if (n < 0)
                                    break;
                                i1 = str1.indexOf(';', n);
                                if (i1 < 0)
                                    i1 = k;
                                String str2 = str1.substring(m + 1, n);
                                String str3 = str1.substring(n + 1, i1);
                                Annotation localAnnotation = new Annotation(str2, str3);
                                localSpannableString.setSpan(localAnnotation, paramArrayOfInt[(i + 1)], 1 + paramArrayOfInt[(i + 2)], 33);
                            }
                        }
                    }
                }
            }
            paramString = new SpannedString(localSpannableString);
        }
    }

    private static final native int nativeCreate(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    private static final native void nativeDestroy(int paramInt);

    private static final native int nativeGetSize(int paramInt);

    private static final native String nativeGetString(int paramInt1, int paramInt2);

    private static final native int[] nativeGetStyle(int paramInt1, int paramInt2);

    private static String subtag(String paramString1, String paramString2)
    {
        int i = paramString1.indexOf(paramString2);
        String str;
        if (i < 0)
            str = null;
        while (true)
        {
            return str;
            int j = i + paramString2.length();
            int k = paramString1.indexOf(';', j);
            if (k < 0)
                str = paramString1.substring(j);
            else
                str = paramString1.substring(j, k);
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mOwnsNative)
            nativeDestroy(this.mNative);
    }

    public CharSequence get(int paramInt)
    {
        while (true)
        {
            Object localObject2;
            try
            {
                if (this.mStrings != null)
                {
                    localObject2 = this.mStrings[paramInt];
                    if (localObject2 != null)
                        continue;
                }
                else if (this.mSparseStrings != null)
                {
                    localObject2 = (CharSequence)this.mSparseStrings.get(paramInt);
                    if (localObject2 == null)
                        continue;
                }
            }
            finally
            {
                throw localObject1;
                int i = nativeGetSize(this.mNative);
                String str1;
                int[] arrayOfInt;
                int k;
                if ((this.mUseSparse) && (i > 250))
                {
                    this.mSparseStrings = new SparseArray();
                    str1 = nativeGetString(this.mNative, paramInt);
                    localObject2 = str1;
                    arrayOfInt = nativeGetStyle(this.mNative, paramInt);
                    if (arrayOfInt == null)
                        continue;
                    if (this.mStyleIDs != null)
                        break label631;
                    this.mStyleIDs = new StyleIDs();
                    break label631;
                    if (j >= arrayOfInt.length)
                        continue;
                    k = arrayOfInt[j];
                    if ((k == this.mStyleIDs.boldId) || (k == this.mStyleIDs.italicId) || (k == this.mStyleIDs.underlineId) || (k == this.mStyleIDs.ttId) || (k == this.mStyleIDs.bigId) || (k == this.mStyleIDs.smallId) || (k == this.mStyleIDs.subId) || (k == this.mStyleIDs.supId) || (k == this.mStyleIDs.strikeId) || (k == this.mStyleIDs.listItemId))
                        break label637;
                    if (k == this.mStyleIDs.marqueeId)
                        break label637;
                }
                else
                {
                    this.mStrings = new CharSequence[i];
                    continue;
                }
                String str2 = nativeGetString(this.mNative, k);
                if (str2.equals("b"))
                {
                    StyleIDs.access$002(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("i"))
                {
                    StyleIDs.access$102(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("u"))
                {
                    StyleIDs.access$202(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("tt"))
                {
                    StyleIDs.access$302(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("big"))
                {
                    StyleIDs.access$402(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("small"))
                {
                    StyleIDs.access$502(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("sup"))
                {
                    StyleIDs.access$702(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("sub"))
                {
                    StyleIDs.access$602(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("strike"))
                {
                    StyleIDs.access$802(this.mStyleIDs, k);
                    break label637;
                }
                if (str2.equals("li"))
                {
                    StyleIDs.access$902(this.mStyleIDs, k);
                    break label637;
                }
                if (!str2.equals("marquee"))
                    break label637;
                StyleIDs.access$1002(this.mStyleIDs, k);
                break label637;
                localObject2 = applyStyles(str1, arrayOfInt, this.mStyleIDs);
                if (this.mStrings != null)
                    this.mStrings[paramInt] = localObject2;
                else
                    this.mSparseStrings.put(paramInt, localObject2);
            }
            label631: int j = 0;
            continue;
            label637: j += 3;
        }
    }

    private static class Height
        implements LineHeightSpan.WithDensity
    {
        private static float sProportion = 0.0F;
        private int mSize;

        public Height(int paramInt)
        {
            this.mSize = paramInt;
        }

        public void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt)
        {
            chooseHeight(paramCharSequence, paramInt1, paramInt2, paramInt3, paramInt4, paramFontMetricsInt, null);
        }

        public void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt, TextPaint paramTextPaint)
        {
            int i = this.mSize;
            if (paramTextPaint != null)
                i = (int)(i * paramTextPaint.density);
            if (paramFontMetricsInt.bottom - paramFontMetricsInt.top < i)
            {
                paramFontMetricsInt.top = (paramFontMetricsInt.bottom - i);
                paramFontMetricsInt.ascent -= i;
            }
            while (true)
            {
                return;
                if (sProportion == 0.0F)
                {
                    Paint localPaint = new Paint();
                    localPaint.setTextSize(100.0F);
                    Rect localRect = new Rect();
                    localPaint.getTextBounds("ABCDEFG", 0, 7, localRect);
                    sProportion = localRect.top / localPaint.ascent();
                }
                int j = (int)Math.ceil(-paramFontMetricsInt.top * sProportion);
                if (i - paramFontMetricsInt.descent >= j)
                {
                    paramFontMetricsInt.top = (paramFontMetricsInt.bottom - i);
                    paramFontMetricsInt.ascent = (paramFontMetricsInt.descent - i);
                }
                else if (i >= j)
                {
                    int m = -j;
                    paramFontMetricsInt.ascent = m;
                    paramFontMetricsInt.top = m;
                    int n = i + paramFontMetricsInt.top;
                    paramFontMetricsInt.descent = n;
                    paramFontMetricsInt.bottom = n;
                }
                else
                {
                    int k = -i;
                    paramFontMetricsInt.ascent = k;
                    paramFontMetricsInt.top = k;
                    paramFontMetricsInt.descent = 0;
                    paramFontMetricsInt.bottom = 0;
                }
            }
        }
    }

    static final class StyleIDs
    {
        private int bigId = -1;
        private int boldId = -1;
        private int italicId = -1;
        private int listItemId = -1;
        private int marqueeId = -1;
        private int smallId = -1;
        private int strikeId = -1;
        private int subId = -1;
        private int supId = -1;
        private int ttId = -1;
        private int underlineId = -1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.StringBlock
 * JD-Core Version:        0.6.2
 */