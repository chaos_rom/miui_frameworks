package android.content.res;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.LocaleUtil;
import java.util.Locale;
import miui.content.res.ExtraConfiguration;

public final class Configuration
    implements Parcelable, Comparable<Configuration>
{
    public static final Parcelable.Creator<Configuration> CREATOR = new Parcelable.Creator()
    {
        public Configuration createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Configuration(paramAnonymousParcel, null);
        }

        public Configuration[] newArray(int paramAnonymousInt)
        {
            return new Configuration[paramAnonymousInt];
        }
    };
    public static final int HARDKEYBOARDHIDDEN_NO = 1;
    public static final int HARDKEYBOARDHIDDEN_UNDEFINED = 0;
    public static final int HARDKEYBOARDHIDDEN_YES = 2;
    public static final int KEYBOARDHIDDEN_NO = 1;
    public static final int KEYBOARDHIDDEN_SOFT = 3;
    public static final int KEYBOARDHIDDEN_UNDEFINED = 0;
    public static final int KEYBOARDHIDDEN_YES = 2;
    public static final int KEYBOARD_12KEY = 3;
    public static final int KEYBOARD_NOKEYS = 1;
    public static final int KEYBOARD_QWERTY = 2;
    public static final int KEYBOARD_UNDEFINED = 0;
    public static final int NAVIGATIONHIDDEN_NO = 1;
    public static final int NAVIGATIONHIDDEN_UNDEFINED = 0;
    public static final int NAVIGATIONHIDDEN_YES = 2;
    public static final int NAVIGATION_DPAD = 2;
    public static final int NAVIGATION_NONAV = 1;
    public static final int NAVIGATION_TRACKBALL = 3;
    public static final int NAVIGATION_UNDEFINED = 0;
    public static final int NAVIGATION_WHEEL = 4;
    public static final int ORIENTATION_LANDSCAPE = 2;
    public static final int ORIENTATION_PORTRAIT = 1;

    @Deprecated
    public static final int ORIENTATION_SQUARE = 3;
    public static final int ORIENTATION_UNDEFINED = 0;
    public static final int SCREENLAYOUT_COMPAT_NEEDED = 268435456;
    public static final int SCREENLAYOUT_LONG_MASK = 48;
    public static final int SCREENLAYOUT_LONG_NO = 16;
    public static final int SCREENLAYOUT_LONG_UNDEFINED = 0;
    public static final int SCREENLAYOUT_LONG_YES = 32;
    public static final int SCREENLAYOUT_SIZE_LARGE = 3;
    public static final int SCREENLAYOUT_SIZE_MASK = 15;
    public static final int SCREENLAYOUT_SIZE_NORMAL = 2;
    public static final int SCREENLAYOUT_SIZE_SMALL = 1;
    public static final int SCREENLAYOUT_SIZE_UNDEFINED = 0;
    public static final int SCREENLAYOUT_SIZE_XLARGE = 4;
    public static final int SCREEN_HEIGHT_DP_UNDEFINED = 0;
    public static final int SCREEN_WIDTH_DP_UNDEFINED = 0;
    public static final int SMALLEST_SCREEN_WIDTH_DP_UNDEFINED = 0;
    public static final int TOUCHSCREEN_FINGER = 3;
    public static final int TOUCHSCREEN_NOTOUCH = 1;

    @Deprecated
    public static final int TOUCHSCREEN_STYLUS = 2;
    public static final int TOUCHSCREEN_UNDEFINED = 0;
    public static final int UI_MODE_NIGHT_MASK = 48;
    public static final int UI_MODE_NIGHT_NO = 16;
    public static final int UI_MODE_NIGHT_UNDEFINED = 0;
    public static final int UI_MODE_NIGHT_YES = 32;
    public static final int UI_MODE_TYPE_APPLIANCE = 5;
    public static final int UI_MODE_TYPE_CAR = 3;
    public static final int UI_MODE_TYPE_DESK = 2;
    public static final int UI_MODE_TYPE_MASK = 15;
    public static final int UI_MODE_TYPE_NORMAL = 1;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int UI_MODE_TYPE_SCALE_HUGE = 15;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int UI_MODE_TYPE_SCALE_LARGE = 14;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int UI_MODE_TYPE_SCALE_MEDIUM = 13;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int UI_MODE_TYPE_SCALE_SMALL = 12;
    public static final int UI_MODE_TYPE_TELEVISION = 4;
    public static final int UI_MODE_TYPE_UNDEFINED;
    public int compatScreenHeightDp;
    public int compatScreenWidthDp;
    public int compatSmallestScreenWidthDp;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public ExtraConfiguration extraConfig = new ExtraConfiguration();
    public float fontScale;
    public int hardKeyboardHidden;
    public int keyboard;
    public int keyboardHidden;
    public int layoutDirection;
    public Locale locale;
    public int mcc;
    public int mnc;
    public int navigation;
    public int navigationHidden;
    public int orientation;
    public int screenHeightDp;
    public int screenLayout;
    public int screenWidthDp;
    public int seq;
    public int smallestScreenWidthDp;
    public int touchscreen;
    public int uiMode;
    public boolean userSetLocale;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Configuration()
    {
        setToDefaults();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Configuration(Configuration paramConfiguration)
    {
        setTo(paramConfiguration);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private Configuration(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static boolean needNewResources(int paramInt1, int paramInt2)
    {
        if (((paramInt1 & (0x40000000 | paramInt2)) != 0) || (ExtraConfiguration.needNewResources(paramInt1)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public int compareTo(Configuration paramConfiguration)
    {
        float f1 = this.fontScale;
        float f2 = paramConfiguration.fontScale;
        int i;
        if (f1 < f2)
            i = -1;
        while (true)
        {
            return i;
            if (f1 > f2)
            {
                i = 1;
            }
            else
            {
                i = this.mcc - paramConfiguration.mcc;
                if (i == 0)
                {
                    i = this.mnc - paramConfiguration.mnc;
                    if (i == 0)
                        if (this.locale == null)
                        {
                            if (paramConfiguration.locale != null)
                                i = 1;
                        }
                        else if (paramConfiguration.locale == null)
                        {
                            i = -1;
                        }
                        else
                        {
                            i = this.locale.getLanguage().compareTo(paramConfiguration.locale.getLanguage());
                            if (i == 0)
                            {
                                i = this.locale.getCountry().compareTo(paramConfiguration.locale.getCountry());
                                if (i == 0)
                                {
                                    i = this.locale.getVariant().compareTo(paramConfiguration.locale.getVariant());
                                    if (i == 0)
                                    {
                                        i = this.touchscreen - paramConfiguration.touchscreen;
                                        if (i == 0)
                                        {
                                            i = this.keyboard - paramConfiguration.keyboard;
                                            if (i == 0)
                                            {
                                                i = this.keyboardHidden - paramConfiguration.keyboardHidden;
                                                if (i == 0)
                                                {
                                                    i = this.hardKeyboardHidden - paramConfiguration.hardKeyboardHidden;
                                                    if (i == 0)
                                                    {
                                                        i = this.navigation - paramConfiguration.navigation;
                                                        if (i == 0)
                                                        {
                                                            i = this.navigationHidden - paramConfiguration.navigationHidden;
                                                            if (i == 0)
                                                            {
                                                                i = this.orientation - paramConfiguration.orientation;
                                                                if (i == 0)
                                                                {
                                                                    i = this.screenLayout - paramConfiguration.screenLayout;
                                                                    if (i == 0)
                                                                    {
                                                                        i = this.uiMode - paramConfiguration.uiMode;
                                                                        if (i == 0)
                                                                        {
                                                                            i = this.screenWidthDp - paramConfiguration.screenWidthDp;
                                                                            if (i == 0)
                                                                            {
                                                                                i = this.screenHeightDp - paramConfiguration.screenHeightDp;
                                                                                if (i == 0)
                                                                                {
                                                                                    (this.smallestScreenWidthDp - paramConfiguration.smallestScreenWidthDp);
                                                                                    i = this.extraConfig.compareTo(paramConfiguration.extraConfig);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                }
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int diff(Configuration paramConfiguration)
    {
        int i = 0;
        if ((paramConfiguration.fontScale > 0.0F) && (this.fontScale != paramConfiguration.fontScale))
            i = 0x0 | 0x40000000;
        if ((paramConfiguration.mcc != 0) && (this.mcc != paramConfiguration.mcc))
            i |= 1;
        if ((paramConfiguration.mnc != 0) && (this.mnc != paramConfiguration.mnc))
            i |= 2;
        if ((paramConfiguration.locale != null) && ((this.locale == null) || (!this.locale.equals(paramConfiguration.locale))))
            i |= 4;
        if ((paramConfiguration.touchscreen != 0) && (this.touchscreen != paramConfiguration.touchscreen))
            i |= 8;
        if ((paramConfiguration.keyboard != 0) && (this.keyboard != paramConfiguration.keyboard))
            i |= 16;
        if ((paramConfiguration.keyboardHidden != 0) && (this.keyboardHidden != paramConfiguration.keyboardHidden))
            i |= 32;
        if ((paramConfiguration.hardKeyboardHidden != 0) && (this.hardKeyboardHidden != paramConfiguration.hardKeyboardHidden))
            i |= 32;
        if ((paramConfiguration.navigation != 0) && (this.navigation != paramConfiguration.navigation))
            i |= 64;
        if ((paramConfiguration.navigationHidden != 0) && (this.navigationHidden != paramConfiguration.navigationHidden))
            i |= 32;
        if ((paramConfiguration.orientation != 0) && (this.orientation != paramConfiguration.orientation))
            i |= 128;
        if ((paramConfiguration.screenLayout != 0) && (this.screenLayout != paramConfiguration.screenLayout))
            i |= 256;
        if ((paramConfiguration.uiMode != 0) && (this.uiMode != paramConfiguration.uiMode))
            i |= 512;
        if ((paramConfiguration.screenWidthDp != 0) && (this.screenWidthDp != paramConfiguration.screenWidthDp))
            i |= 1024;
        if ((paramConfiguration.screenHeightDp != 0) && (this.screenHeightDp != paramConfiguration.screenHeightDp))
            i |= 1024;
        if ((paramConfiguration.smallestScreenWidthDp != 0) && (this.smallestScreenWidthDp != paramConfiguration.smallestScreenWidthDp))
            i |= 2048;
        return i | this.extraConfig.diff(paramConfiguration.extraConfig);
    }

    public boolean equals(Configuration paramConfiguration)
    {
        boolean bool = true;
        if (paramConfiguration == null);
        for (bool = false; ; bool = false)
            do
                return bool;
            while ((paramConfiguration == this) || (compareTo(paramConfiguration) == 0));
    }

    public boolean equals(Object paramObject)
    {
        try
        {
            boolean bool2 = equals((Configuration)paramObject);
            bool1 = bool2;
            return bool1;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public int hashCode()
    {
        int i = 31 * (31 * (31 * (527 + Float.floatToIntBits(this.fontScale)) + this.mcc) + this.mnc);
        if (this.locale != null);
        for (int j = this.locale.hashCode(); ; j = 0)
            return 31 * (31 * (31 * (31 * (31 * (31 * (31 * (31 * (31 * (31 * (31 * (31 * (i + j) + this.touchscreen) + this.keyboard) + this.keyboardHidden) + this.hardKeyboardHidden) + this.navigation) + this.navigationHidden) + this.orientation) + this.screenLayout) + this.uiMode) + this.screenWidthDp) + this.screenHeightDp) + this.smallestScreenWidthDp + this.extraConfig.hashCode();
    }

    public boolean isLayoutSizeAtLeast(int paramInt)
    {
        boolean bool = false;
        int i = 0xF & this.screenLayout;
        if (i == 0);
        while (true)
        {
            return bool;
            if (i >= paramInt)
                bool = true;
        }
    }

    public boolean isOtherSeqNewer(Configuration paramConfiguration)
    {
        boolean bool = true;
        if (paramConfiguration == null)
            bool = false;
        while (true)
        {
            return bool;
            if ((paramConfiguration.seq != 0) && (this.seq != 0))
            {
                int i = paramConfiguration.seq - this.seq;
                if (i > 65536)
                    bool = false;
                else if (i <= 0)
                    bool = false;
            }
        }
    }

    @Deprecated
    public void makeDefault()
    {
        setToDefaults();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void readFromParcel(Parcel paramParcel)
    {
        int i = 1;
        this.fontScale = paramParcel.readFloat();
        this.mcc = paramParcel.readInt();
        this.mnc = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
            this.locale = new Locale(paramParcel.readString(), paramParcel.readString(), paramParcel.readString());
        if (paramParcel.readInt() == i);
        while (true)
        {
            this.userSetLocale = i;
            this.touchscreen = paramParcel.readInt();
            this.keyboard = paramParcel.readInt();
            this.keyboardHidden = paramParcel.readInt();
            this.hardKeyboardHidden = paramParcel.readInt();
            this.navigation = paramParcel.readInt();
            this.navigationHidden = paramParcel.readInt();
            this.orientation = paramParcel.readInt();
            this.screenLayout = paramParcel.readInt();
            this.uiMode = paramParcel.readInt();
            this.screenWidthDp = paramParcel.readInt();
            this.screenHeightDp = paramParcel.readInt();
            this.smallestScreenWidthDp = paramParcel.readInt();
            this.compatScreenWidthDp = paramParcel.readInt();
            this.compatScreenHeightDp = paramParcel.readInt();
            this.compatSmallestScreenWidthDp = paramParcel.readInt();
            this.layoutDirection = paramParcel.readInt();
            this.seq = paramParcel.readInt();
            this.extraConfig.readFromParcel(paramParcel);
            return;
            i = 0;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void setTo(Configuration paramConfiguration)
    {
        this.fontScale = paramConfiguration.fontScale;
        this.mcc = paramConfiguration.mcc;
        this.mnc = paramConfiguration.mnc;
        if (paramConfiguration.locale != null)
        {
            this.locale = ((Locale)paramConfiguration.locale.clone());
            this.layoutDirection = paramConfiguration.layoutDirection;
        }
        this.userSetLocale = paramConfiguration.userSetLocale;
        this.touchscreen = paramConfiguration.touchscreen;
        this.keyboard = paramConfiguration.keyboard;
        this.keyboardHidden = paramConfiguration.keyboardHidden;
        this.hardKeyboardHidden = paramConfiguration.hardKeyboardHidden;
        this.navigation = paramConfiguration.navigation;
        this.navigationHidden = paramConfiguration.navigationHidden;
        this.orientation = paramConfiguration.orientation;
        this.screenLayout = paramConfiguration.screenLayout;
        this.uiMode = paramConfiguration.uiMode;
        this.screenWidthDp = paramConfiguration.screenWidthDp;
        this.screenHeightDp = paramConfiguration.screenHeightDp;
        this.smallestScreenWidthDp = paramConfiguration.smallestScreenWidthDp;
        this.compatScreenWidthDp = paramConfiguration.compatScreenWidthDp;
        this.compatScreenHeightDp = paramConfiguration.compatScreenHeightDp;
        this.compatSmallestScreenWidthDp = paramConfiguration.compatSmallestScreenWidthDp;
        this.seq = paramConfiguration.seq;
        this.extraConfig.setTo(paramConfiguration.extraConfig);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void setToDefaults()
    {
        this.fontScale = 1.0F;
        this.mnc = 0;
        this.mcc = 0;
        this.locale = null;
        this.userSetLocale = false;
        this.touchscreen = 0;
        this.keyboard = 0;
        this.keyboardHidden = 0;
        this.hardKeyboardHidden = 0;
        this.navigation = 0;
        this.navigationHidden = 0;
        this.orientation = 0;
        this.screenLayout = 0;
        this.uiMode = 0;
        this.compatScreenWidthDp = 0;
        this.screenWidthDp = 0;
        this.compatScreenHeightDp = 0;
        this.screenHeightDp = 0;
        this.compatSmallestScreenWidthDp = 0;
        this.smallestScreenWidthDp = 0;
        this.layoutDirection = 0;
        this.seq = 0;
        this.extraConfig.setToDefaults();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("{");
        localStringBuilder.append(this.fontScale);
        localStringBuilder.append(" ");
        localStringBuilder.append(this.mcc);
        localStringBuilder.append("mcc");
        localStringBuilder.append(this.mnc);
        localStringBuilder.append("mnc");
        label137: label169: label689: if (this.locale != null)
        {
            localStringBuilder.append(" ");
            localStringBuilder.append(this.locale);
            label585: switch (this.layoutDirection)
            {
            default:
                localStringBuilder.append(" layoutDir=");
                localStringBuilder.append(this.layoutDirection);
            case 0:
                label201: label356: if (this.smallestScreenWidthDp != 0)
                {
                    localStringBuilder.append(" sw");
                    localStringBuilder.append(this.smallestScreenWidthDp);
                    localStringBuilder.append("dp");
                    if (this.screenWidthDp == 0)
                        break label922;
                    localStringBuilder.append(" w");
                    localStringBuilder.append(this.screenWidthDp);
                    localStringBuilder.append("dp");
                    if (this.screenHeightDp == 0)
                        break label933;
                    localStringBuilder.append(" h");
                    localStringBuilder.append(this.screenHeightDp);
                    localStringBuilder.append("dp");
                    label233: switch (0xF & this.screenLayout)
                    {
                    default:
                        localStringBuilder.append(" layoutSize=");
                        localStringBuilder.append(0xF & this.screenLayout);
                        switch (0x30 & this.screenLayout)
                        {
                        default:
                            localStringBuilder.append(" layoutLong=");
                            localStringBuilder.append(0x30 & this.screenLayout);
                        case 16:
                            switch (this.orientation)
                            {
                            default:
                                localStringBuilder.append(" orien=");
                                localStringBuilder.append(this.orientation);
                                switch (0xF & this.uiMode)
                                {
                                default:
                                    localStringBuilder.append(" uimode=");
                                    localStringBuilder.append(0xF & this.uiMode);
                                case 1:
                                    switch (0x30 & this.uiMode)
                                    {
                                    default:
                                        localStringBuilder.append(" night=");
                                        localStringBuilder.append(0x30 & this.uiMode);
                                    case 16:
                                        switch (this.touchscreen)
                                        {
                                        default:
                                            localStringBuilder.append(" touch=");
                                            localStringBuilder.append(this.touchscreen);
                                            switch (this.keyboard)
                                            {
                                            default:
                                                localStringBuilder.append(" keys=");
                                                localStringBuilder.append(this.keyboard);
                                                label637: switch (this.keyboardHidden)
                                                {
                                                default:
                                                    localStringBuilder.append("/");
                                                    localStringBuilder.append(this.keyboardHidden);
                                                    switch (this.hardKeyboardHidden)
                                                    {
                                                    default:
                                                        localStringBuilder.append("/");
                                                        localStringBuilder.append(this.hardKeyboardHidden);
                                                        switch (this.navigation)
                                                        {
                                                        default:
                                                            localStringBuilder.append(" nav=");
                                                            localStringBuilder.append(this.navigation);
                                                            switch (this.navigationHidden)
                                                            {
                                                            default:
                                                                localStringBuilder.append("/");
                                                                localStringBuilder.append(this.navigationHidden);
                                                            case 0:
                                                            case 1:
                                                            case 2:
                                                            }
                                                            break;
                                                        case 0:
                                                        case 1:
                                                        case 2:
                                                        case 3:
                                                        case 4:
                                                        }
                                                        break;
                                                    case 0:
                                                    case 1:
                                                    case 2:
                                                    }
                                                    break;
                                                case 0:
                                                case 1:
                                                case 2:
                                                case 3:
                                                }
                                                break;
                                            case 0:
                                            case 1:
                                            case 2:
                                            case 3:
                                            }
                                            break;
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        }
                                        break;
                                    case 0:
                                    case 32:
                                    }
                                    break;
                                case 0:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                }
                                break;
                            case 0:
                            case 2:
                            case 1:
                            }
                            break;
                        case 0:
                        case 32:
                        }
                        break;
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    }
                }
                label472: label737: break;
            case 1:
            }
        }
        while (true)
        {
            label296: if (this.seq != 0)
            {
                localStringBuilder.append(" s.");
                localStringBuilder.append(this.seq);
            }
            label405: label793: localStringBuilder.append(this.extraConfig.toString());
            label532: localStringBuilder.append('}');
            return localStringBuilder.toString();
            localStringBuilder.append(" (no locale)");
            break;
            localStringBuilder.append(" rtl");
            break label137;
            localStringBuilder.append(" ?swdp");
            break label169;
            label922: localStringBuilder.append(" ?wdp");
            break label201;
            label933: localStringBuilder.append(" ?hdp");
            break label233;
            localStringBuilder.append(" ?lsize");
            break label296;
            localStringBuilder.append(" smll");
            break label296;
            localStringBuilder.append(" nrml");
            break label296;
            localStringBuilder.append(" lrg");
            break label296;
            localStringBuilder.append(" xlrg");
            break label296;
            localStringBuilder.append(" ?long");
            break label356;
            localStringBuilder.append(" long");
            break label356;
            localStringBuilder.append(" ?orien");
            break label405;
            localStringBuilder.append(" land");
            break label405;
            localStringBuilder.append(" port");
            break label405;
            localStringBuilder.append(" ?uimode");
            break label472;
            localStringBuilder.append(" desk");
            break label472;
            localStringBuilder.append(" car");
            break label472;
            localStringBuilder.append(" television");
            break label472;
            localStringBuilder.append(" appliance");
            break label472;
            localStringBuilder.append(" ?night");
            break label532;
            localStringBuilder.append(" night");
            break label532;
            localStringBuilder.append(" ?touch");
            break label585;
            localStringBuilder.append(" -touch");
            break label585;
            localStringBuilder.append(" stylus");
            break label585;
            localStringBuilder.append(" finger");
            break label585;
            localStringBuilder.append(" ?keyb");
            break label637;
            localStringBuilder.append(" -keyb");
            break label637;
            localStringBuilder.append(" qwerty");
            break label637;
            localStringBuilder.append(" 12key");
            break label637;
            localStringBuilder.append("/?");
            break label689;
            localStringBuilder.append("/v");
            break label689;
            localStringBuilder.append("/h");
            break label689;
            localStringBuilder.append("/s");
            break label689;
            localStringBuilder.append("/?");
            break label737;
            localStringBuilder.append("/v");
            break label737;
            localStringBuilder.append("/h");
            break label737;
            localStringBuilder.append(" ?nav");
            break label793;
            localStringBuilder.append(" -nav");
            break label793;
            localStringBuilder.append(" dpad");
            break label793;
            localStringBuilder.append(" tball");
            break label793;
            localStringBuilder.append(" wheel");
            break label793;
            localStringBuilder.append("/?");
            continue;
            localStringBuilder.append("/v");
            continue;
            localStringBuilder.append("/h");
        }
    }

    public int updateFrom(Configuration paramConfiguration)
    {
        int i = 0;
        if ((paramConfiguration.fontScale > 0.0F) && (this.fontScale != paramConfiguration.fontScale))
        {
            i = 0x0 | 0x40000000;
            this.fontScale = paramConfiguration.fontScale;
        }
        if ((paramConfiguration.mcc != 0) && (this.mcc != paramConfiguration.mcc))
        {
            i |= 1;
            this.mcc = paramConfiguration.mcc;
        }
        if ((paramConfiguration.mnc != 0) && (this.mnc != paramConfiguration.mnc))
        {
            i |= 2;
            this.mnc = paramConfiguration.mnc;
        }
        if ((paramConfiguration.locale != null) && ((this.locale == null) || (!this.locale.equals(paramConfiguration.locale))))
        {
            i |= 4;
            if (paramConfiguration.locale == null)
                break label676;
        }
        label676: for (Locale localLocale = (Locale)paramConfiguration.locale.clone(); ; localLocale = null)
        {
            this.locale = localLocale;
            this.layoutDirection = LocaleUtil.getLayoutDirectionFromLocale(this.locale);
            if ((paramConfiguration.userSetLocale) && ((!this.userSetLocale) || ((i & 0x4) != 0)))
            {
                this.userSetLocale = true;
                i |= 4;
            }
            if ((paramConfiguration.touchscreen != 0) && (this.touchscreen != paramConfiguration.touchscreen))
            {
                i |= 8;
                this.touchscreen = paramConfiguration.touchscreen;
            }
            if ((paramConfiguration.keyboard != 0) && (this.keyboard != paramConfiguration.keyboard))
            {
                i |= 16;
                this.keyboard = paramConfiguration.keyboard;
            }
            if ((paramConfiguration.keyboardHidden != 0) && (this.keyboardHidden != paramConfiguration.keyboardHidden))
            {
                i |= 32;
                this.keyboardHidden = paramConfiguration.keyboardHidden;
            }
            if ((paramConfiguration.hardKeyboardHidden != 0) && (this.hardKeyboardHidden != paramConfiguration.hardKeyboardHidden))
            {
                i |= 32;
                this.hardKeyboardHidden = paramConfiguration.hardKeyboardHidden;
            }
            if ((paramConfiguration.navigation != 0) && (this.navigation != paramConfiguration.navigation))
            {
                i |= 64;
                this.navigation = paramConfiguration.navigation;
            }
            if ((paramConfiguration.navigationHidden != 0) && (this.navigationHidden != paramConfiguration.navigationHidden))
            {
                i |= 32;
                this.navigationHidden = paramConfiguration.navigationHidden;
            }
            if ((paramConfiguration.orientation != 0) && (this.orientation != paramConfiguration.orientation))
            {
                i |= 128;
                this.orientation = paramConfiguration.orientation;
            }
            if ((paramConfiguration.screenLayout != 0) && (this.screenLayout != paramConfiguration.screenLayout))
            {
                i |= 256;
                this.screenLayout = paramConfiguration.screenLayout;
            }
            if ((paramConfiguration.uiMode != 0) && (this.uiMode != paramConfiguration.uiMode))
            {
                i |= 512;
                if ((0xF & paramConfiguration.uiMode) != 0)
                    this.uiMode = (0xFFFFFFF0 & this.uiMode | 0xF & paramConfiguration.uiMode);
                if ((0x30 & paramConfiguration.uiMode) != 0)
                    this.uiMode = (0xFFFFFFCF & this.uiMode | 0x30 & paramConfiguration.uiMode);
            }
            if ((paramConfiguration.screenWidthDp != 0) && (this.screenWidthDp != paramConfiguration.screenWidthDp))
            {
                i |= 1024;
                this.screenWidthDp = paramConfiguration.screenWidthDp;
            }
            if ((paramConfiguration.screenHeightDp != 0) && (this.screenHeightDp != paramConfiguration.screenHeightDp))
            {
                i |= 1024;
                this.screenHeightDp = paramConfiguration.screenHeightDp;
            }
            if (paramConfiguration.smallestScreenWidthDp != 0)
                this.smallestScreenWidthDp = paramConfiguration.smallestScreenWidthDp;
            if (paramConfiguration.compatScreenWidthDp != 0)
                this.compatScreenWidthDp = paramConfiguration.compatScreenWidthDp;
            if (paramConfiguration.compatScreenHeightDp != 0)
                this.compatScreenHeightDp = paramConfiguration.compatScreenHeightDp;
            if (paramConfiguration.compatSmallestScreenWidthDp != 0)
                this.compatSmallestScreenWidthDp = paramConfiguration.compatSmallestScreenWidthDp;
            if (paramConfiguration.seq != 0)
                this.seq = paramConfiguration.seq;
            return i | this.extraConfig.updateFrom(paramConfiguration.extraConfig);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeFloat(this.fontScale);
        paramParcel.writeInt(this.mcc);
        paramParcel.writeInt(this.mnc);
        if (this.locale == null)
        {
            paramParcel.writeInt(0);
            if (!this.userSetLocale)
                break label235;
            paramParcel.writeInt(1);
        }
        while (true)
        {
            paramParcel.writeInt(this.touchscreen);
            paramParcel.writeInt(this.keyboard);
            paramParcel.writeInt(this.keyboardHidden);
            paramParcel.writeInt(this.hardKeyboardHidden);
            paramParcel.writeInt(this.navigation);
            paramParcel.writeInt(this.navigationHidden);
            paramParcel.writeInt(this.orientation);
            paramParcel.writeInt(this.screenLayout);
            paramParcel.writeInt(this.uiMode);
            paramParcel.writeInt(this.screenWidthDp);
            paramParcel.writeInt(this.screenHeightDp);
            paramParcel.writeInt(this.smallestScreenWidthDp);
            paramParcel.writeInt(this.compatScreenWidthDp);
            paramParcel.writeInt(this.compatScreenHeightDp);
            paramParcel.writeInt(this.compatSmallestScreenWidthDp);
            paramParcel.writeInt(this.layoutDirection);
            paramParcel.writeInt(this.seq);
            this.extraConfig.writeToParcel(paramParcel, paramInt);
            return;
            paramParcel.writeInt(1);
            paramParcel.writeString(this.locale.getLanguage());
            paramParcel.writeString(this.locale.getCountry());
            paramParcel.writeString(this.locale.getVariant());
            break;
            label235: paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.Configuration
 * JD-Core Version:        0.6.2
 */