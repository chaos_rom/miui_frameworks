package android.content.res;

public class MiuiTypedArray extends TypedArray
{
    private boolean mIsMiuiResources = getResources() instanceof MiuiResources;

    MiuiTypedArray(Resources paramResources, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
    {
        super(paramResources, paramArrayOfInt1, paramArrayOfInt2, paramInt);
    }

    private CharSequence loadStringValueAt(int paramInt)
    {
        CharSequence localCharSequence;
        if (!this.mIsMiuiResources)
            localCharSequence = null;
        while (true)
        {
            return localCharSequence;
            if (this.mData[(paramInt + 0)] == 3)
            {
                int i = this.mData[(paramInt + 3)];
                localCharSequence = ((MiuiResources)getResources()).getThemeCharSequence(i);
                if (localCharSequence != null);
            }
            else
            {
                localCharSequence = null;
            }
        }
    }

    public String getString(int paramInt)
    {
        CharSequence localCharSequence = loadStringValueAt(paramInt * 6);
        if (localCharSequence != null);
        for (String str = localCharSequence.toString(); ; str = super.getString(paramInt))
            return str;
    }

    public CharSequence getText(int paramInt)
    {
        CharSequence localCharSequence = loadStringValueAt(paramInt * 6);
        if (localCharSequence != null);
        while (true)
        {
            return localCharSequence;
            localCharSequence = super.getText(paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.MiuiTypedArray
 * JD-Core Version:        0.6.2
 */