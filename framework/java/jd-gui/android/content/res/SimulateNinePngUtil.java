package android.content.res;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.zip.CRC32;

class SimulateNinePngUtil
{
    private static byte[] PNG_HEAD_FORMAT = arrayOfByte;

    static
    {
        byte[] arrayOfByte = new byte[8];
        arrayOfByte[0] = -119;
        arrayOfByte[1] = 80;
        arrayOfByte[2] = 78;
        arrayOfByte[3] = 71;
        arrayOfByte[4] = 13;
        arrayOfByte[5] = 10;
        arrayOfByte[6] = 26;
        arrayOfByte[7] = 10;
    }

    private static int computePatchColor(byte[] paramArrayOfByte)
    {
        return 1;
    }

    private static int convertByteToIntByBigEndian(byte[] paramArrayOfByte, int paramInt)
    {
        return 0 + ((0xFF & paramArrayOfByte[(paramInt + 0)]) << 24) + ((0xFF & paramArrayOfByte[(paramInt + 1)]) << 16) + ((0xFF & paramArrayOfByte[(paramInt + 2)]) << 8) + (0xFF & paramArrayOfByte[(paramInt + 3)]);
    }

    private static void convertBytesFromIntByBigEndian(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        paramArrayOfByte[(paramInt1 + 0)] = ((byte)(0xFF & paramInt2 >>> 24));
        paramArrayOfByte[(paramInt1 + 1)] = ((byte)(0xFF & paramInt2 >> 16));
        paramArrayOfByte[(paramInt1 + 2)] = ((byte)(0xFF & paramInt2 >> 8));
        paramArrayOfByte[(paramInt1 + 3)] = ((byte)(paramInt2 & 0xFF));
    }

    public static byte[] convertIntoNinePngData(byte[] paramArrayOfByte)
    {
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length < 41) || (!isPngFormat(paramArrayOfByte)))
            paramArrayOfByte = null;
        while (true)
        {
            return paramArrayOfByte;
            if (!isNinePngFormat(paramArrayOfByte))
            {
                byte[] arrayOfByte1 = getNinePatchChunk(paramArrayOfByte);
                byte[] arrayOfByte2 = new byte[12 + paramArrayOfByte.length + arrayOfByte1.length];
                for (int i = 0; i < 33; i++)
                    arrayOfByte2[i] = paramArrayOfByte[i];
                convertBytesFromIntByBigEndian(arrayOfByte2, 33, arrayOfByte1.length);
                fillNinePngFormatMark(arrayOfByte2);
                for (int j = 0; j < arrayOfByte1.length; j++)
                    arrayOfByte2[(j + 41)] = arrayOfByte1[j];
                int k = 41 + arrayOfByte1.length;
                CRC32 localCRC32 = new CRC32();
                localCRC32.update(arrayOfByte2, 37, 4 + arrayOfByte1.length);
                convertBytesFromIntByBigEndian(arrayOfByte2, k, (int)localCRC32.getValue());
                for (int m = 0; m < -33 + paramArrayOfByte.length; m++)
                    arrayOfByte2[(m + (k + 4))] = paramArrayOfByte[(m + 33)];
                paramArrayOfByte = arrayOfByte2;
            }
        }
    }

    public static InputStream convertIntoNinePngStream(InputStream paramInputStream)
    {
        NinePathInputStream localNinePathInputStream = null;
        try
        {
            Object localObject = new byte[41];
            int i = paramInputStream.read((byte[])localObject);
            if (i <= 0)
                localObject = null;
            while (true)
            {
                byte[] arrayOfByte2 = convertIntoNinePngData((byte[])localObject);
                if (arrayOfByte2 == null)
                    break;
                localNinePathInputStream = new NinePathInputStream(paramInputStream, arrayOfByte2);
                break;
                if (i < localObject.length)
                {
                    byte[] arrayOfByte1 = Arrays.copyOf((byte[])localObject, i);
                    localObject = arrayOfByte1;
                }
            }
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
        return localNinePathInputStream;
    }

    private static void fillNinePngFormatMark(byte[] paramArrayOfByte)
    {
        paramArrayOfByte[37] = 110;
        paramArrayOfByte[38] = 112;
        paramArrayOfByte[39] = 84;
        paramArrayOfByte[40] = 99;
    }

    private static byte[] getNinePatchChunk(byte[] paramArrayOfByte)
    {
        int i = convertByteToIntByBigEndian(paramArrayOfByte, 16);
        int j = convertByteToIntByBigEndian(paramArrayOfByte, 20);
        byte[] arrayOfByte = new byte[52];
        arrayOfByte[0] = 1;
        arrayOfByte[1] = 2;
        arrayOfByte[2] = 2;
        arrayOfByte[3] = 1;
        convertBytesFromIntByBigEndian(arrayOfByte, 36, i);
        convertBytesFromIntByBigEndian(arrayOfByte, 44, j);
        convertBytesFromIntByBigEndian(arrayOfByte, 48, computePatchColor(paramArrayOfByte));
        return arrayOfByte;
    }

    private static boolean isNinePngFormat(byte[] paramArrayOfByte)
    {
        if ((paramArrayOfByte != null) && (paramArrayOfByte.length > 40) && (paramArrayOfByte[37] == 110) && (paramArrayOfByte[38] == 112) && (paramArrayOfByte[39] == 84) && (paramArrayOfByte[40] == 99));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isPngFormat(byte[] paramArrayOfByte)
    {
        int i = 0;
        if (i < PNG_HEAD_FORMAT.length)
            if (paramArrayOfByte[i] == PNG_HEAD_FORMAT[i]);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    private static class NinePathInputStream extends InputStream
    {
        private int mCount = 0;
        private byte[] mExtraHeaderData;
        private InputStream mInputStream;

        public NinePathInputStream(InputStream paramInputStream, byte[] paramArrayOfByte)
        {
            this.mInputStream = paramInputStream;
            this.mExtraHeaderData = paramArrayOfByte;
            this.mCount = 0;
        }

        public void close()
            throws IOException
        {
            if (this.mInputStream != null)
                this.mInputStream.close();
        }

        public int read()
            throws IOException
        {
            byte[] arrayOfByte;
            int j;
            if (this.mCount < this.mExtraHeaderData.length)
            {
                arrayOfByte = this.mExtraHeaderData;
                j = this.mCount;
                this.mCount = (j + 1);
            }
            for (int i = arrayOfByte[j]; ; i = this.mInputStream.read())
                return i;
        }

        public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            Arrays.checkOffsetAndCount(paramArrayOfByte.length, paramInt1, paramInt2);
            int j;
            for (int i = 0; (this.mCount < this.mExtraHeaderData.length) && (i < paramInt2); i = j)
            {
                j = i + 1;
                int k = paramInt1 + i;
                byte[] arrayOfByte = this.mExtraHeaderData;
                int m = this.mCount;
                this.mCount = (m + 1);
                paramArrayOfByte[k] = arrayOfByte[m];
            }
            if (i < paramInt2)
                i += this.mInputStream.read(paramArrayOfByte, paramInt1 + i, paramInt2 - i);
            return i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.SimulateNinePngUtil
 * JD-Core Version:        0.6.2
 */