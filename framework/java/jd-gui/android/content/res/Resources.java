package android.content.res;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.graphics.BitmapFactory.Options;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.LongSparseArray;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import libcore.icu.NativePluralRules;
import org.xmlpull.v1.XmlPullParserException;

public class Resources
{
    private static final boolean DEBUG_ATTRIBUTES_CACHE = false;
    private static final boolean DEBUG_CONFIG = false;
    private static final boolean DEBUG_LOAD = false;
    private static final int ID_OTHER = 16777220;
    static final String TAG = "Resources";
    private static final boolean TRACE_FOR_MISS_PRELOAD;
    private static final boolean TRACE_FOR_PRELOAD;
    private static boolean mPreloaded;
    private static final Object mSync = new Object();
    static Resources mSystem = null;
    private static final LongSparseArray<Drawable.ConstantState> sPreloadedColorDrawables = new LongSparseArray();
    private static final LongSparseArray<ColorStateList> sPreloadedColorStateLists;
    private static final LongSparseArray<Drawable.ConstantState> sPreloadedDrawables = new LongSparseArray();
    final AssetManager mAssets;
    TypedArray mCachedStyledAttributes = null;
    private final int[] mCachedXmlBlockIds;
    private final XmlBlock[] mCachedXmlBlocks;
    private final LongSparseArray<WeakReference<Drawable.ConstantState>> mColorDrawableCache = new LongSparseArray();
    private final LongSparseArray<WeakReference<ColorStateList>> mColorStateListCache = new LongSparseArray();
    private CompatibilityInfo mCompatibilityInfo;
    private final Configuration mConfiguration;
    private final LongSparseArray<WeakReference<Drawable.ConstantState>> mDrawableCache = new LongSparseArray();
    private int mLastCachedXmlBlockIndex = -1;
    RuntimeException mLastRetrievedAttrs = null;
    final DisplayMetrics mMetrics;
    private NativePluralRules mPluralRule;
    private boolean mPreloading;
    final Configuration mTmpConfig = new Configuration();
    final TypedValue mTmpValue = new TypedValue();

    static
    {
        sPreloadedColorStateLists = new LongSparseArray();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    Resources()
    {
        int[] arrayOfInt = new int[4];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        arrayOfInt[2] = 0;
        arrayOfInt[3] = 0;
        this.mCachedXmlBlockIds = arrayOfInt;
        this.mCachedXmlBlocks = new XmlBlock[4];
        this.mConfiguration = new Configuration();
        this.mMetrics = new DisplayMetrics();
        this.mAssets = AssetManager.getSystem();
        this.mConfiguration.setToDefaults();
        this.mMetrics.setToDefaults();
        updateConfiguration(null, null);
        this.mAssets.ensureStringBlocks();
        this.mCompatibilityInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;
    }

    public Resources(AssetManager paramAssetManager, DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration)
    {
        this(paramAssetManager, paramDisplayMetrics, paramConfiguration, null);
    }

    public Resources(AssetManager paramAssetManager, DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo)
    {
        int[] arrayOfInt = new int[4];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        arrayOfInt[2] = 0;
        arrayOfInt[3] = 0;
        this.mCachedXmlBlockIds = arrayOfInt;
        this.mCachedXmlBlocks = new XmlBlock[4];
        this.mConfiguration = new Configuration();
        this.mMetrics = new DisplayMetrics();
        this.mAssets = paramAssetManager;
        this.mMetrics.setToDefaults();
        this.mCompatibilityInfo = paramCompatibilityInfo;
        updateConfiguration(paramConfiguration, paramDisplayMetrics);
        paramAssetManager.ensureStringBlocks();
    }

    private static int attrForQuantityCode(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 16777220;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return i;
            i = 16777221;
            continue;
            i = 16777222;
            continue;
            i = 16777223;
            continue;
            i = 16777224;
            continue;
            i = 16777225;
        }
    }

    private void clearDrawableCache(LongSparseArray<WeakReference<Drawable.ConstantState>> paramLongSparseArray, int paramInt)
    {
        int i = paramLongSparseArray.size();
        for (int j = 0; j < i; j++)
        {
            WeakReference localWeakReference = (WeakReference)paramLongSparseArray.valueAt(j);
            if (localWeakReference != null)
            {
                Drawable.ConstantState localConstantState = (Drawable.ConstantState)localWeakReference.get();
                if ((localConstantState != null) && (Configuration.needNewResources(paramInt, localConstantState.getChangingConfigurations())))
                    paramLongSparseArray.setValueAt(j, null);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static void clearPreloadedCache()
    {
        sPreloadedDrawables.clear();
        sPreloadedColorStateLists.clear();
        sPreloadedColorDrawables.clear();
    }

    private ColorStateList getCachedColorStateList(long paramLong)
    {
        ColorStateList localColorStateList;
        synchronized (this.mTmpValue)
        {
            WeakReference localWeakReference = (WeakReference)this.mColorStateListCache.get(paramLong);
            if (localWeakReference != null)
            {
                localColorStateList = (ColorStateList)localWeakReference.get();
                if (localColorStateList == null)
                    this.mColorStateListCache.delete(paramLong);
            }
            else
            {
                localColorStateList = null;
            }
        }
        return localColorStateList;
    }

    private Drawable getCachedDrawable(LongSparseArray<WeakReference<Drawable.ConstantState>> paramLongSparseArray, long paramLong)
    {
        Drawable localDrawable;
        synchronized (this.mTmpValue)
        {
            WeakReference localWeakReference = (WeakReference)paramLongSparseArray.get(paramLong);
            if (localWeakReference != null)
            {
                Drawable.ConstantState localConstantState = (Drawable.ConstantState)localWeakReference.get();
                if (localConstantState != null)
                    localDrawable = localConstantState.newDrawable(this);
                else
                    paramLongSparseArray.delete(paramLong);
            }
            else
            {
                localDrawable = null;
            }
        }
        return localDrawable;
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private TypedArray getCachedStyledAttributes(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 93	android/content/res/Resources:mTmpValue	Landroid/util/TypedValue;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 106	android/content/res/Resources:mCachedStyledAttributes	Landroid/content/res/TypedArray;
        //     11: astore 4
        //     13: aload 4
        //     15: ifnull +65 -> 80
        //     18: aload_0
        //     19: aconst_null
        //     20: putfield 106	android/content/res/Resources:mCachedStyledAttributes	Landroid/content/res/TypedArray;
        //     23: aload 4
        //     25: iload_1
        //     26: putfield 223	android/content/res/TypedArray:mLength	I
        //     29: iload_1
        //     30: bipush 6
        //     32: imul
        //     33: istore 5
        //     35: aload 4
        //     37: getfield 226	android/content/res/TypedArray:mData	[I
        //     40: arraylength
        //     41: iload 5
        //     43: if_icmplt +8 -> 51
        //     46: aload_2
        //     47: monitorexit
        //     48: goto +56 -> 104
        //     51: aload 4
        //     53: iload 5
        //     55: newarray int
        //     57: putfield 226	android/content/res/TypedArray:mData	[I
        //     60: aload 4
        //     62: iload_1
        //     63: iconst_1
        //     64: iadd
        //     65: newarray int
        //     67: putfield 229	android/content/res/TypedArray:mIndices	[I
        //     70: aload_2
        //     71: monitorexit
        //     72: goto +32 -> 104
        //     75: astore_3
        //     76: aload_2
        //     77: monitorexit
        //     78: aload_3
        //     79: athrow
        //     80: new 231	android/content/res/MiuiTypedArray
        //     83: dup
        //     84: aload_0
        //     85: iload_1
        //     86: bipush 6
        //     88: imul
        //     89: newarray int
        //     91: iload_1
        //     92: iconst_1
        //     93: iadd
        //     94: newarray int
        //     96: iload_1
        //     97: invokespecial 234	android/content/res/MiuiTypedArray:<init>	(Landroid/content/res/Resources;[I[II)V
        //     100: astore 4
        //     102: aload_2
        //     103: monitorexit
        //     104: aload 4
        //     106: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	78	75	finally
        //     80	104	75	finally
    }

    private NativePluralRules getPluralRule()
    {
        synchronized (mSync)
        {
            if (this.mPluralRule == null)
                this.mPluralRule = NativePluralRules.forLocale(this.mConfiguration.locale);
            NativePluralRules localNativePluralRules = this.mPluralRule;
            return localNativePluralRules;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static Resources getSystem()
    {
        synchronized (mSync)
        {
            Object localObject3 = mSystem;
            if (localObject3 == null)
            {
                localObject3 = new MiuiResources();
                mSystem = (Resources)localObject3;
            }
            return localObject3;
        }
    }

    public static int selectDefaultTheme(int paramInt1, int paramInt2)
    {
        return selectSystemTheme(paramInt1, paramInt2, 16973829, 16973931, 16974120);
    }

    public static int selectSystemTheme(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if (paramInt1 != 0);
        while (true)
        {
            return paramInt1;
            if (paramInt2 < 11)
                paramInt1 = paramInt3;
            else if (paramInt2 < 14)
                paramInt1 = paramInt4;
            else
                paramInt1 = paramInt5;
        }
    }

    private static String stringForQuantityCode(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "other";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return str;
            str = "zero";
            continue;
            str = "one";
            continue;
            str = "two";
            continue;
            str = "few";
            continue;
            str = "many";
        }
    }

    public static void updateSystemConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics)
    {
        updateSystemConfiguration(paramConfiguration, paramDisplayMetrics, null);
    }

    public static void updateSystemConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo)
    {
        if (mSystem != null)
            mSystem.updateConfiguration(paramConfiguration, paramDisplayMetrics, paramCompatibilityInfo);
    }

    public final void finishPreloading()
    {
        if (this.mPreloading)
        {
            this.mPreloading = false;
            flushLayoutCache();
        }
    }

    public final void flushLayoutCache()
    {
        synchronized (this.mCachedXmlBlockIds)
        {
            int i = this.mCachedXmlBlockIds.length;
            for (int j = 0; j < i; j++)
            {
                this.mCachedXmlBlockIds[j] = 0;
                XmlBlock localXmlBlock = this.mCachedXmlBlocks[j];
                if (localXmlBlock != null)
                    localXmlBlock.close();
                this.mCachedXmlBlocks[j] = null;
            }
            return;
        }
    }

    public XmlResourceParser getAnimation(int paramInt)
        throws Resources.NotFoundException
    {
        return loadXmlResourceParser(paramInt, "anim");
    }

    public final AssetManager getAssets()
    {
        return this.mAssets;
    }

    public boolean getBoolean(int paramInt)
        throws Resources.NotFoundException
    {
        for (boolean bool = true; ; bool = false)
            synchronized (this.mTmpValue)
            {
                TypedValue localTypedValue2 = this.mTmpValue;
                getValue(paramInt, localTypedValue2, true);
                if ((localTypedValue2.type >= 16) && (localTypedValue2.type <= 31))
                {
                    if (localTypedValue2.data != 0)
                        return bool;
                }
                else
                    throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
            }
    }

    // ERROR //
    public int getColor(int paramInt)
        throws Resources.NotFoundException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 93	android/content/res/Resources:mTmpValue	Landroid/util/TypedValue;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 93	android/content/res/Resources:mTmpValue	Landroid/util/TypedValue;
        //     11: astore 4
        //     13: aload_0
        //     14: iload_1
        //     15: aload 4
        //     17: iconst_1
        //     18: invokevirtual 305	android/content/res/Resources:getValue	(ILandroid/util/TypedValue;Z)V
        //     21: aload 4
        //     23: getfield 308	android/util/TypedValue:type	I
        //     26: bipush 16
        //     28: if_icmplt +25 -> 53
        //     31: aload 4
        //     33: getfield 308	android/util/TypedValue:type	I
        //     36: bipush 31
        //     38: if_icmpgt +15 -> 53
        //     41: aload 4
        //     43: getfield 311	android/util/TypedValue:data	I
        //     46: istore 5
        //     48: aload_2
        //     49: monitorexit
        //     50: goto +90 -> 140
        //     53: aload 4
        //     55: getfield 308	android/util/TypedValue:type	I
        //     58: iconst_3
        //     59: if_icmpne +27 -> 86
        //     62: aload_0
        //     63: aload_0
        //     64: getfield 93	android/content/res/Resources:mTmpValue	Landroid/util/TypedValue;
        //     67: iload_1
        //     68: invokevirtual 341	android/content/res/Resources:loadColorStateList	(Landroid/util/TypedValue;I)Landroid/content/res/ColorStateList;
        //     71: invokevirtual 344	android/content/res/ColorStateList:getDefaultColor	()I
        //     74: istore 5
        //     76: aload_2
        //     77: monitorexit
        //     78: goto +62 -> 140
        //     81: astore_3
        //     82: aload_2
        //     83: monitorexit
        //     84: aload_3
        //     85: athrow
        //     86: new 9	android/content/res/Resources$NotFoundException
        //     89: dup
        //     90: new 313	java/lang/StringBuilder
        //     93: dup
        //     94: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     97: ldc_w 316
        //     100: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     103: iload_1
        //     104: invokestatic 325	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     107: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     110: ldc_w 327
        //     113: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     116: aload 4
        //     118: getfield 308	android/util/TypedValue:type	I
        //     121: invokestatic 325	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     124: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     127: ldc_w 329
        //     130: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     136: invokespecial 336	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     139: athrow
        //     140: iload 5
        //     142: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	84	81	finally
        //     86	140	81	finally
    }

    public ColorStateList getColorStateList(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            ColorStateList localColorStateList = loadColorStateList(localTypedValue2, paramInt);
            return localColorStateList;
        }
    }

    public CompatibilityInfo getCompatibilityInfo()
    {
        if (this.mCompatibilityInfo != null);
        for (CompatibilityInfo localCompatibilityInfo = this.mCompatibilityInfo; ; localCompatibilityInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO)
            return localCompatibilityInfo;
    }

    public Configuration getConfiguration()
    {
        return this.mConfiguration;
    }

    public float getDimension(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            if (localTypedValue2.type == 5)
            {
                float f = TypedValue.complexToDimension(localTypedValue2.data, this.mMetrics);
                return f;
            }
            throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
        }
    }

    public int getDimensionPixelOffset(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            if (localTypedValue2.type == 5)
            {
                int i = TypedValue.complexToDimensionPixelOffset(localTypedValue2.data, this.mMetrics);
                return i;
            }
            throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
        }
    }

    public int getDimensionPixelSize(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            if (localTypedValue2.type == 5)
            {
                int i = TypedValue.complexToDimensionPixelSize(localTypedValue2.data, this.mMetrics);
                return i;
            }
            throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
        }
    }

    public DisplayMetrics getDisplayMetrics()
    {
        return this.mMetrics;
    }

    public Drawable getDrawable(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            Drawable localDrawable = loadDrawable(localTypedValue2, paramInt);
            return localDrawable;
        }
    }

    public Drawable getDrawableForDensity(int paramInt1, int paramInt2)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValueForDensity(paramInt1, paramInt2, localTypedValue2, true);
            if ((localTypedValue2.density > 0) && (localTypedValue2.density != 65535))
            {
                if (localTypedValue2.density == paramInt2)
                    localTypedValue2.density = DisplayMetrics.DENSITY_DEVICE;
            }
            else
            {
                Drawable localDrawable = loadDrawable(localTypedValue2, paramInt1);
                return localDrawable;
            }
            localTypedValue2.density = (localTypedValue2.density * DisplayMetrics.DENSITY_DEVICE / paramInt2);
        }
    }

    public float getFraction(int paramInt1, int paramInt2, int paramInt3)
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt1, localTypedValue2, true);
            if (localTypedValue2.type == 6)
            {
                float f = TypedValue.complexToFraction(localTypedValue2.data, paramInt2, paramInt3);
                return f;
            }
            throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt1) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
        }
    }

    public int getIdentifier(String paramString1, String paramString2, String paramString3)
    {
        try
        {
            int j = Integer.parseInt(paramString1);
            i = j;
            return i;
        }
        catch (Exception localException)
        {
            while (true)
                int i = this.mAssets.getResourceIdentifier(paramString1, paramString2, paramString3);
        }
    }

    public int[] getIntArray(int paramInt)
        throws Resources.NotFoundException
    {
        int[] arrayOfInt = this.mAssets.getArrayIntResource(paramInt);
        if (arrayOfInt != null)
            return arrayOfInt;
        throw new NotFoundException("Int array resource ID #0x" + Integer.toHexString(paramInt));
    }

    public int getInteger(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            if ((localTypedValue2.type >= 16) && (localTypedValue2.type <= 31))
            {
                int i = localTypedValue2.data;
                return i;
            }
            throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
        }
    }

    public XmlResourceParser getLayout(int paramInt)
        throws Resources.NotFoundException
    {
        return loadXmlResourceParser(paramInt, "layout");
    }

    public Movie getMovie(int paramInt)
        throws Resources.NotFoundException
    {
        InputStream localInputStream = openRawResource(paramInt);
        Movie localMovie = Movie.decodeStream(localInputStream);
        try
        {
            localInputStream.close();
            label15: return localMovie;
        }
        catch (IOException localIOException)
        {
            break label15;
        }
    }

    public String getQuantityString(int paramInt1, int paramInt2)
        throws Resources.NotFoundException
    {
        return getQuantityText(paramInt1, paramInt2).toString();
    }

    public String getQuantityString(int paramInt1, int paramInt2, Object[] paramArrayOfObject)
        throws Resources.NotFoundException
    {
        String str = getQuantityText(paramInt1, paramInt2).toString();
        return String.format(this.mConfiguration.locale, str, paramArrayOfObject);
    }

    public CharSequence getQuantityText(int paramInt1, int paramInt2)
        throws Resources.NotFoundException
    {
        NativePluralRules localNativePluralRules = getPluralRule();
        CharSequence localCharSequence1 = this.mAssets.getResourceBagText(paramInt1, attrForQuantityCode(localNativePluralRules.quantityForInt(paramInt2)));
        if (localCharSequence1 != null);
        CharSequence localCharSequence2;
        for (Object localObject = localCharSequence1; ; localObject = localCharSequence2)
        {
            return localObject;
            localCharSequence2 = this.mAssets.getResourceBagText(paramInt1, 16777220);
            if (localCharSequence2 == null)
                break;
        }
        throw new NotFoundException("Plural resource ID #0x" + Integer.toHexString(paramInt1) + " quantity=" + paramInt2 + " item=" + stringForQuantityCode(localNativePluralRules.quantityForInt(paramInt2)));
    }

    public String getResourceEntryName(int paramInt)
        throws Resources.NotFoundException
    {
        String str = this.mAssets.getResourceEntryName(paramInt);
        if (str != null)
            return str;
        throw new NotFoundException("Unable to find resource ID #0x" + Integer.toHexString(paramInt));
    }

    public String getResourceName(int paramInt)
        throws Resources.NotFoundException
    {
        String str = this.mAssets.getResourceName(paramInt);
        if (str != null)
            return str;
        throw new NotFoundException("Unable to find resource ID #0x" + Integer.toHexString(paramInt));
    }

    public String getResourcePackageName(int paramInt)
        throws Resources.NotFoundException
    {
        String str = this.mAssets.getResourcePackageName(paramInt);
        if (str != null)
            return str;
        throw new NotFoundException("Unable to find resource ID #0x" + Integer.toHexString(paramInt));
    }

    public String getResourceTypeName(int paramInt)
        throws Resources.NotFoundException
    {
        String str = this.mAssets.getResourceTypeName(paramInt);
        if (str != null)
            return str;
        throw new NotFoundException("Unable to find resource ID #0x" + Integer.toHexString(paramInt));
    }

    public String getString(int paramInt)
        throws Resources.NotFoundException
    {
        CharSequence localCharSequence = getText(paramInt);
        if (localCharSequence != null)
            return localCharSequence.toString();
        throw new NotFoundException("String resource ID #0x" + Integer.toHexString(paramInt));
    }

    public String getString(int paramInt, Object[] paramArrayOfObject)
        throws Resources.NotFoundException
    {
        String str = getString(paramInt);
        return String.format(this.mConfiguration.locale, str, paramArrayOfObject);
    }

    public String[] getStringArray(int paramInt)
        throws Resources.NotFoundException
    {
        String[] arrayOfString = this.mAssets.getResourceStringArray(paramInt);
        if (arrayOfString != null)
            return arrayOfString;
        throw new NotFoundException("String array resource ID #0x" + Integer.toHexString(paramInt));
    }

    public CharSequence getText(int paramInt)
        throws Resources.NotFoundException
    {
        CharSequence localCharSequence = this.mAssets.getResourceText(paramInt);
        if (localCharSequence != null)
            return localCharSequence;
        throw new NotFoundException("String resource ID #0x" + Integer.toHexString(paramInt));
    }

    public CharSequence getText(int paramInt, CharSequence paramCharSequence)
    {
        CharSequence localCharSequence;
        if (paramInt != 0)
        {
            localCharSequence = this.mAssets.getResourceText(paramInt);
            if (localCharSequence == null)
                break label24;
        }
        while (true)
        {
            return localCharSequence;
            localCharSequence = null;
            break;
            label24: localCharSequence = paramCharSequence;
        }
    }

    public CharSequence[] getTextArray(int paramInt)
        throws Resources.NotFoundException
    {
        CharSequence[] arrayOfCharSequence = this.mAssets.getResourceTextArray(paramInt);
        if (arrayOfCharSequence != null)
            return arrayOfCharSequence;
        throw new NotFoundException("Text array resource ID #0x" + Integer.toHexString(paramInt));
    }

    public void getValue(int paramInt, TypedValue paramTypedValue, boolean paramBoolean)
        throws Resources.NotFoundException
    {
        if (this.mAssets.getResourceValue(paramInt, 0, paramTypedValue, paramBoolean))
            return;
        throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt));
    }

    public void getValue(String paramString, TypedValue paramTypedValue, boolean paramBoolean)
        throws Resources.NotFoundException
    {
        int i = getIdentifier(paramString, "string", null);
        if (i != 0)
        {
            getValue(i, paramTypedValue, paramBoolean);
            return;
        }
        throw new NotFoundException("String resource name " + paramString);
    }

    public void getValueForDensity(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean)
        throws Resources.NotFoundException
    {
        if (this.mAssets.getResourceValue(paramInt1, paramInt2, paramTypedValue, paramBoolean))
            return;
        throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt1));
    }

    public XmlResourceParser getXml(int paramInt)
        throws Resources.NotFoundException
    {
        return loadXmlResourceParser(paramInt, "xml");
    }

    ColorStateList loadColorStateList(TypedValue paramTypedValue, int paramInt)
        throws Resources.NotFoundException
    {
        long l = paramTypedValue.assetCookie << 32 | paramTypedValue.data;
        Object localObject1;
        if ((paramTypedValue.type >= 28) && (paramTypedValue.type <= 31))
        {
            ColorStateList localColorStateList4 = (ColorStateList)sPreloadedColorStateLists.get(l);
            if (localColorStateList4 != null)
                localObject1 = localColorStateList4;
        }
        while (true)
        {
            return localObject1;
            ColorStateList localColorStateList5 = ColorStateList.valueOf(paramTypedValue.data);
            if (this.mPreloading)
                sPreloadedColorStateLists.put(l, localColorStateList5);
            localObject1 = localColorStateList5;
            continue;
            ColorStateList localColorStateList1 = getCachedColorStateList(l);
            if (localColorStateList1 != null)
            {
                localObject1 = localColorStateList1;
            }
            else
            {
                ColorStateList localColorStateList2 = (ColorStateList)sPreloadedColorStateLists.get(l);
                if (localColorStateList2 == null)
                    break;
                localObject1 = localColorStateList2;
            }
        }
        if (paramTypedValue.string == null)
            throw new NotFoundException("Resource is not a ColorStateList (color or path): " + paramTypedValue);
        String str = paramTypedValue.string.toString();
        if (str.endsWith(".xml"));
        while (true)
        {
            ColorStateList localColorStateList3;
            try
            {
                XmlResourceParser localXmlResourceParser = loadXmlResourceParser(str, paramInt, paramTypedValue.assetCookie, "colorstatelist");
                localColorStateList3 = ColorStateList.createFromXml(this, localXmlResourceParser);
                localXmlResourceParser.close();
                if (localColorStateList3 != null)
                {
                    if (!this.mPreloading)
                        break label350;
                    sPreloadedColorStateLists.put(l, localColorStateList3);
                }
                localObject1 = localColorStateList3;
            }
            catch (Exception localException)
            {
                NotFoundException localNotFoundException = new NotFoundException("File " + str + " from color state list resource ID #0x" + Integer.toHexString(paramInt));
                localNotFoundException.initCause(localException);
                throw localNotFoundException;
            }
            throw new NotFoundException("File " + str + " from drawable resource ID #0x" + Integer.toHexString(paramInt) + ": .xml extension required");
            label350: synchronized (this.mTmpValue)
            {
                this.mColorStateListCache.put(l, new WeakReference(localColorStateList3));
            }
        }
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    Drawable loadDrawable(TypedValue paramTypedValue, int paramInt)
        throws Resources.NotFoundException
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 525	android/util/TypedValue:assetCookie	I
        //     4: i2l
        //     5: bipush 32
        //     7: lshl
        //     8: aload_1
        //     9: getfield 311	android/util/TypedValue:data	I
        //     12: i2l
        //     13: lor
        //     14: lstore_3
        //     15: iconst_0
        //     16: istore 5
        //     18: aload_1
        //     19: getfield 308	android/util/TypedValue:type	I
        //     22: bipush 28
        //     24: if_icmplt +15 -> 39
        //     27: aload_1
        //     28: getfield 308	android/util/TypedValue:type	I
        //     31: bipush 31
        //     33: if_icmpgt +6 -> 39
        //     36: iconst_1
        //     37: istore 5
        //     39: iload 5
        //     41: ifeq +30 -> 71
        //     44: aload_0
        //     45: getfield 104	android/content/res/Resources:mColorDrawableCache	Landroid/util/LongSparseArray;
        //     48: astore 6
        //     50: aload_0
        //     51: aload 6
        //     53: lload_3
        //     54: invokespecial 577	android/content/res/Resources:getCachedDrawable	(Landroid/util/LongSparseArray;J)Landroid/graphics/drawable/Drawable;
        //     57: astore 7
        //     59: aload 7
        //     61: ifnull +19 -> 80
        //     64: aload 7
        //     66: astore 14
        //     68: aload 14
        //     70: areturn
        //     71: aload_0
        //     72: getfield 100	android/content/res/Resources:mDrawableCache	Landroid/util/LongSparseArray;
        //     75: astore 6
        //     77: goto -27 -> 50
        //     80: iload 5
        //     82: ifeq +82 -> 164
        //     85: getstatic 84	android/content/res/Resources:sPreloadedColorDrawables	Landroid/util/LongSparseArray;
        //     88: lload_3
        //     89: invokevirtual 205	android/util/LongSparseArray:get	(J)Ljava/lang/Object;
        //     92: checkcast 184	android/graphics/drawable/Drawable$ConstantState
        //     95: astore 8
        //     97: aload 8
        //     99: ifnull +80 -> 179
        //     102: aload 8
        //     104: aload_0
        //     105: invokevirtual 217	android/graphics/drawable/Drawable$ConstantState:newDrawable	(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
        //     108: astore 7
        //     110: aload 7
        //     112: ifnull +45 -> 157
        //     115: aload 7
        //     117: aload_1
        //     118: getfield 580	android/util/TypedValue:changingConfigurations	I
        //     121: invokevirtual 586	android/graphics/drawable/Drawable:setChangingConfigurations	(I)V
        //     124: aload 7
        //     126: invokevirtual 590	android/graphics/drawable/Drawable:getConstantState	()Landroid/graphics/drawable/Drawable$ConstantState;
        //     129: astore 15
        //     131: aload 15
        //     133: ifnull +24 -> 157
        //     136: aload_0
        //     137: getfield 284	android/content/res/Resources:mPreloading	Z
        //     140: ifeq +315 -> 455
        //     143: iload 5
        //     145: ifeq +298 -> 443
        //     148: getstatic 84	android/content/res/Resources:sPreloadedColorDrawables	Landroid/util/LongSparseArray;
        //     151: lload_3
        //     152: aload 15
        //     154: invokevirtual 532	android/util/LongSparseArray:put	(JLjava/lang/Object;)V
        //     157: aload 7
        //     159: astore 14
        //     161: goto -93 -> 68
        //     164: getstatic 80	android/content/res/Resources:sPreloadedDrawables	Landroid/util/LongSparseArray;
        //     167: lload_3
        //     168: invokevirtual 205	android/util/LongSparseArray:get	(J)Ljava/lang/Object;
        //     171: checkcast 184	android/graphics/drawable/Drawable$ConstantState
        //     174: astore 8
        //     176: goto -79 -> 97
        //     179: iload 5
        //     181: ifeq +16 -> 197
        //     184: new 592	android/graphics/drawable/ColorDrawable
        //     187: dup
        //     188: aload_1
        //     189: getfield 311	android/util/TypedValue:data	I
        //     192: invokespecial 594	android/graphics/drawable/ColorDrawable:<init>	(I)V
        //     195: astore 7
        //     197: aload 7
        //     199: ifnonnull -89 -> 110
        //     202: aload_1
        //     203: getfield 537	android/util/TypedValue:string	Ljava/lang/CharSequence;
        //     206: ifnonnull +31 -> 237
        //     209: new 9	android/content/res/Resources$NotFoundException
        //     212: dup
        //     213: new 313	java/lang/StringBuilder
        //     216: dup
        //     217: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     220: ldc_w 596
        //     223: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: aload_1
        //     227: invokevirtual 542	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     230: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     233: invokespecial 336	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     236: athrow
        //     237: aload_1
        //     238: getfield 537	android/util/TypedValue:string	Ljava/lang/CharSequence;
        //     241: invokevirtual 438	java/lang/Object:toString	()Ljava/lang/String;
        //     244: astore 9
        //     246: aload 9
        //     248: ldc_w 544
        //     251: invokevirtual 548	java/lang/String:endsWith	(Ljava/lang/String;)Z
        //     254: ifeq +93 -> 347
        //     257: aload_0
        //     258: aload 9
        //     260: iload_2
        //     261: aload_1
        //     262: getfield 525	android/util/TypedValue:assetCookie	I
        //     265: ldc_w 598
        //     268: invokevirtual 553	android/content/res/Resources:loadXmlResourceParser	(Ljava/lang/String;IILjava/lang/String;)Landroid/content/res/XmlResourceParser;
        //     271: astore 21
        //     273: aload_0
        //     274: aload 21
        //     276: invokestatic 601	android/graphics/drawable/Drawable:createFromXml	(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;)Landroid/graphics/drawable/Drawable;
        //     279: astore 7
        //     281: aload 21
        //     283: invokeinterface 560 1 0
        //     288: goto -178 -> 110
        //     291: astore 18
        //     293: new 9	android/content/res/Resources$NotFoundException
        //     296: dup
        //     297: new 313	java/lang/StringBuilder
        //     300: dup
        //     301: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     304: ldc_w 562
        //     307: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     310: aload 9
        //     312: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     315: ldc_w 570
        //     318: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     321: iload_2
        //     322: invokestatic 325	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     325: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     328: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     331: invokespecial 336	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     334: astore 19
        //     336: aload 19
        //     338: aload 18
        //     340: invokevirtual 568	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     343: pop
        //     344: aload 19
        //     346: athrow
        //     347: aload_0
        //     348: getfield 131	android/content/res/Resources:mAssets	Landroid/content/res/AssetManager;
        //     351: aload_1
        //     352: getfield 525	android/util/TypedValue:assetCookie	I
        //     355: aload 9
        //     357: iconst_2
        //     358: invokevirtual 605	android/content/res/AssetManager:openNonAsset	(ILjava/lang/String;I)Ljava/io/InputStream;
        //     361: astore 13
        //     363: iload_2
        //     364: invokestatic 608	android/content/res/Resources$Injector:setDrawableId	(I)V
        //     367: aload_0
        //     368: aload_1
        //     369: aload 13
        //     371: aload 9
        //     373: aconst_null
        //     374: invokestatic 612	android/content/res/Resources$Injector:createFromResourceStream	(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
        //     377: astore 7
        //     379: aload 13
        //     381: invokevirtual 431	java/io/InputStream:close	()V
        //     384: goto -274 -> 110
        //     387: astore 10
        //     389: new 9	android/content/res/Resources$NotFoundException
        //     392: dup
        //     393: new 313	java/lang/StringBuilder
        //     396: dup
        //     397: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     400: ldc_w 562
        //     403: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     406: aload 9
        //     408: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     411: ldc_w 570
        //     414: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     417: iload_2
        //     418: invokestatic 325	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     421: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     424: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     427: invokespecial 336	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     430: astore 11
        //     432: aload 11
        //     434: aload 10
        //     436: invokevirtual 568	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     439: pop
        //     440: aload 11
        //     442: athrow
        //     443: getstatic 80	android/content/res/Resources:sPreloadedDrawables	Landroid/util/LongSparseArray;
        //     446: lload_3
        //     447: aload 15
        //     449: invokevirtual 532	android/util/LongSparseArray:put	(JLjava/lang/Object;)V
        //     452: goto -295 -> 157
        //     455: aload_0
        //     456: getfield 93	android/content/res/Resources:mTmpValue	Landroid/util/TypedValue;
        //     459: astore 16
        //     461: aload 16
        //     463: monitorenter
        //     464: iload 5
        //     466: ifeq +34 -> 500
        //     469: aload_0
        //     470: getfield 104	android/content/res/Resources:mColorDrawableCache	Landroid/util/LongSparseArray;
        //     473: lload_3
        //     474: new 178	java/lang/ref/WeakReference
        //     477: dup
        //     478: aload 15
        //     480: invokespecial 575	java/lang/ref/WeakReference:<init>	(Ljava/lang/Object;)V
        //     483: invokevirtual 532	android/util/LongSparseArray:put	(JLjava/lang/Object;)V
        //     486: aload 16
        //     488: monitorexit
        //     489: goto -332 -> 157
        //     492: astore 17
        //     494: aload 16
        //     496: monitorexit
        //     497: aload 17
        //     499: athrow
        //     500: aload_0
        //     501: getfield 100	android/content/res/Resources:mDrawableCache	Landroid/util/LongSparseArray;
        //     504: lload_3
        //     505: new 178	java/lang/ref/WeakReference
        //     508: dup
        //     509: aload 15
        //     511: invokespecial 575	java/lang/ref/WeakReference:<init>	(Ljava/lang/Object;)V
        //     514: invokevirtual 532	android/util/LongSparseArray:put	(JLjava/lang/Object;)V
        //     517: goto -31 -> 486
        //
        // Exception table:
        //     from	to	target	type
        //     257	288	291	java/lang/Exception
        //     347	384	387	java/lang/Exception
        //     469	497	492	finally
        //     500	517	492	finally
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Drawable loadOverlayDrawable(TypedValue paramTypedValue, int paramInt)
    {
        return null;
    }

    XmlResourceParser loadXmlResourceParser(int paramInt, String paramString)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            if (localTypedValue2.type == 3)
            {
                XmlResourceParser localXmlResourceParser = loadXmlResourceParser(localTypedValue2.string.toString(), paramInt, localTypedValue2.assetCookie, paramString);
                return localXmlResourceParser;
            }
            throw new NotFoundException("Resource ID #0x" + Integer.toHexString(paramInt) + " type #0x" + Integer.toHexString(localTypedValue2.type) + " is not valid");
        }
    }

    // ERROR //
    XmlResourceParser loadXmlResourceParser(String paramString1, int paramInt1, int paramInt2, String paramString2)
        throws Resources.NotFoundException
    {
        // Byte code:
        //     0: iload_2
        //     1: ifeq +224 -> 225
        //     4: aload_0
        //     5: getfield 112	android/content/res/Resources:mCachedXmlBlockIds	[I
        //     8: astore 8
        //     10: aload 8
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 112	android/content/res/Resources:mCachedXmlBlockIds	[I
        //     17: arraylength
        //     18: istore 10
        //     20: iconst_0
        //     21: istore 11
        //     23: iload 11
        //     25: iload 10
        //     27: if_icmpge +32 -> 59
        //     30: aload_0
        //     31: getfield 112	android/content/res/Resources:mCachedXmlBlockIds	[I
        //     34: iload 11
        //     36: iaload
        //     37: iload_2
        //     38: if_icmpne +242 -> 280
        //     41: aload_0
        //     42: getfield 116	android/content/res/Resources:mCachedXmlBlocks	[Landroid/content/res/XmlBlock;
        //     45: iload 11
        //     47: aaload
        //     48: invokevirtual 617	android/content/res/XmlBlock:newParser	()Landroid/content/res/XmlResourceParser;
        //     51: astore 15
        //     53: aload 8
        //     55: monitorexit
        //     56: goto +221 -> 277
        //     59: aload_0
        //     60: getfield 131	android/content/res/Resources:mAssets	Landroid/content/res/AssetManager;
        //     63: iload_3
        //     64: aload_1
        //     65: invokevirtual 621	android/content/res/AssetManager:openXmlBlockAsset	(ILjava/lang/String;)Landroid/content/res/XmlBlock;
        //     68: astore 12
        //     70: aload 12
        //     72: ifnull +150 -> 222
        //     75: iconst_1
        //     76: aload_0
        //     77: getfield 110	android/content/res/Resources:mLastCachedXmlBlockIndex	I
        //     80: iadd
        //     81: istore 13
        //     83: iload 13
        //     85: iload 10
        //     87: if_icmplt +6 -> 93
        //     90: iconst_0
        //     91: istore 13
        //     93: aload_0
        //     94: iload 13
        //     96: putfield 110	android/content/res/Resources:mLastCachedXmlBlockIndex	I
        //     99: aload_0
        //     100: getfield 116	android/content/res/Resources:mCachedXmlBlocks	[Landroid/content/res/XmlBlock;
        //     103: iload 13
        //     105: aaload
        //     106: astore 14
        //     108: aload 14
        //     110: ifnull +8 -> 118
        //     113: aload 14
        //     115: invokevirtual 290	android/content/res/XmlBlock:close	()V
        //     118: aload_0
        //     119: getfield 112	android/content/res/Resources:mCachedXmlBlockIds	[I
        //     122: iload 13
        //     124: iload_2
        //     125: iastore
        //     126: aload_0
        //     127: getfield 116	android/content/res/Resources:mCachedXmlBlocks	[Landroid/content/res/XmlBlock;
        //     130: iload 13
        //     132: aload 12
        //     134: aastore
        //     135: aload 12
        //     137: invokevirtual 617	android/content/res/XmlBlock:newParser	()Landroid/content/res/XmlResourceParser;
        //     140: astore 15
        //     142: aload 8
        //     144: monitorexit
        //     145: goto +132 -> 277
        //     148: astore 9
        //     150: aload 8
        //     152: monitorexit
        //     153: aload 9
        //     155: athrow
        //     156: astore 5
        //     158: new 9	android/content/res/Resources$NotFoundException
        //     161: dup
        //     162: new 313	java/lang/StringBuilder
        //     165: dup
        //     166: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     169: ldc_w 562
        //     172: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     175: aload_1
        //     176: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: ldc_w 623
        //     182: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     185: aload 4
        //     187: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     190: ldc_w 625
        //     193: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     196: iload_2
        //     197: invokestatic 325	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     200: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     203: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     206: invokespecial 336	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     209: astore 6
        //     211: aload 6
        //     213: aload 5
        //     215: invokevirtual 568	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     218: pop
        //     219: aload 6
        //     221: athrow
        //     222: aload 8
        //     224: monitorexit
        //     225: new 9	android/content/res/Resources$NotFoundException
        //     228: dup
        //     229: new 313	java/lang/StringBuilder
        //     232: dup
        //     233: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     236: ldc_w 562
        //     239: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     242: aload_1
        //     243: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     246: ldc_w 623
        //     249: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     252: aload 4
        //     254: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     257: ldc_w 625
        //     260: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     263: iload_2
        //     264: invokestatic 325	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     267: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     270: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     273: invokespecial 336	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     276: athrow
        //     277: aload 15
        //     279: areturn
        //     280: iinc 11 1
        //     283: goto -260 -> 23
        //
        // Exception table:
        //     from	to	target	type
        //     13	153	148	finally
        //     222	225	148	finally
        //     4	13	156	java/lang/Exception
        //     153	156	156	java/lang/Exception
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public Theme newTheme()
    {
        return new Theme();
    }

    public TypedArray obtainAttributes(AttributeSet paramAttributeSet, int[] paramArrayOfInt)
    {
        TypedArray localTypedArray = getCachedStyledAttributes(paramArrayOfInt.length);
        XmlBlock.Parser localParser = (XmlBlock.Parser)paramAttributeSet;
        this.mAssets.retrieveAttributes(localParser.mParseState, paramArrayOfInt, localTypedArray.mData, localTypedArray.mIndices);
        localTypedArray.mRsrcs = paramArrayOfInt;
        localTypedArray.mXml = localParser;
        return localTypedArray;
    }

    public TypedArray obtainTypedArray(int paramInt)
        throws Resources.NotFoundException
    {
        int i = this.mAssets.getArraySize(paramInt);
        if (i < 0)
            throw new NotFoundException("Array resource ID #0x" + Integer.toHexString(paramInt));
        TypedArray localTypedArray = getCachedStyledAttributes(i);
        localTypedArray.mLength = this.mAssets.retrieveArray(paramInt, localTypedArray.mData);
        localTypedArray.mIndices[0] = 0;
        return localTypedArray;
    }

    public InputStream openRawResource(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            InputStream localInputStream = openRawResource(paramInt, this.mTmpValue);
            return localInputStream;
        }
    }

    public InputStream openRawResource(int paramInt, TypedValue paramTypedValue)
        throws Resources.NotFoundException
    {
        getValue(paramInt, paramTypedValue, true);
        try
        {
            InputStream localInputStream = this.mAssets.openNonAsset(paramTypedValue.assetCookie, paramTypedValue.string.toString(), 2);
            return localInputStream;
        }
        catch (Exception localException)
        {
            NotFoundException localNotFoundException = new NotFoundException("File " + paramTypedValue.string.toString() + " from drawable resource ID #0x" + Integer.toHexString(paramInt));
            localNotFoundException.initCause(localException);
            throw localNotFoundException;
        }
    }

    public AssetFileDescriptor openRawResourceFd(int paramInt)
        throws Resources.NotFoundException
    {
        synchronized (this.mTmpValue)
        {
            TypedValue localTypedValue2 = this.mTmpValue;
            getValue(paramInt, localTypedValue2, true);
            try
            {
                AssetFileDescriptor localAssetFileDescriptor = this.mAssets.openNonAssetFd(localTypedValue2.assetCookie, localTypedValue2.string.toString());
                return localAssetFileDescriptor;
            }
            catch (Exception localException)
            {
                NotFoundException localNotFoundException = new NotFoundException("File " + localTypedValue2.string.toString() + " from drawable resource ID #0x" + Integer.toHexString(paramInt));
                localNotFoundException.initCause(localException);
                throw localNotFoundException;
            }
        }
    }

    public void parseBundleExtra(String paramString, AttributeSet paramAttributeSet, Bundle paramBundle)
        throws XmlPullParserException
    {
        int i = 1;
        TypedArray localTypedArray = obtainAttributes(paramAttributeSet, R.styleable.Extra);
        String str = localTypedArray.getString(0);
        if (str == null)
        {
            localTypedArray.recycle();
            throw new XmlPullParserException("<" + paramString + "> requires an android:name attribute at " + paramAttributeSet.getPositionDescription());
        }
        TypedValue localTypedValue = localTypedArray.peekValue(i);
        if (localTypedValue != null)
        {
            if (localTypedValue.type == 3)
                paramBundle.putCharSequence(str, localTypedValue.coerceToString());
            while (true)
            {
                localTypedArray.recycle();
                return;
                if (localTypedValue.type == 18)
                {
                    if (localTypedValue.data != 0);
                    while (true)
                    {
                        paramBundle.putBoolean(str, i);
                        break;
                        int j = 0;
                    }
                }
                if ((localTypedValue.type >= 16) && (localTypedValue.type <= 31))
                {
                    paramBundle.putInt(str, localTypedValue.data);
                }
                else
                {
                    if (localTypedValue.type != 4)
                        break;
                    paramBundle.putFloat(str, localTypedValue.getFloat());
                }
            }
            localTypedArray.recycle();
            throw new XmlPullParserException("<" + paramString + "> only supports string, integer, float, color, and boolean at " + paramAttributeSet.getPositionDescription());
        }
        localTypedArray.recycle();
        throw new XmlPullParserException("<" + paramString + "> requires an android:value or android:resource attribute at " + paramAttributeSet.getPositionDescription());
    }

    public void parseBundleExtras(XmlResourceParser paramXmlResourceParser, Bundle paramBundle)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlResourceParser.getDepth();
        while (true)
        {
            int j = paramXmlResourceParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlResourceParser.getDepth() <= i)))
                break;
            if ((j != 3) && (j != 4))
                if (paramXmlResourceParser.getName().equals("extra"))
                {
                    parseBundleExtra("extra", paramXmlResourceParser, paramBundle);
                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                }
                else
                {
                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                }
        }
    }

    public void setCompatibilityInfo(CompatibilityInfo paramCompatibilityInfo)
    {
        this.mCompatibilityInfo = paramCompatibilityInfo;
        updateConfiguration(this.mConfiguration, this.mMetrics);
    }

    // ERROR //
    public final void startPreloading()
    {
        // Byte code:
        //     0: getstatic 73	android/content/res/Resources:mSync	Ljava/lang/Object;
        //     3: astore_1
        //     4: aload_1
        //     5: monitorenter
        //     6: getstatic 756	android/content/res/Resources:mPreloaded	Z
        //     9: ifeq +19 -> 28
        //     12: new 758	java/lang/IllegalStateException
        //     15: dup
        //     16: ldc_w 760
        //     19: invokespecial 761	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     22: athrow
        //     23: astore_2
        //     24: aload_1
        //     25: monitorexit
        //     26: aload_2
        //     27: athrow
        //     28: iconst_1
        //     29: putstatic 756	android/content/res/Resources:mPreloaded	Z
        //     32: aload_0
        //     33: iconst_1
        //     34: putfield 284	android/content/res/Resources:mPreloading	Z
        //     37: aload_1
        //     38: monitorexit
        //     39: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	26	23	finally
        //     28	39	23	finally
    }

    public void updateConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics)
    {
        updateConfiguration(paramConfiguration, paramDisplayMetrics, null);
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void updateConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 93	android/content/res/Resources:mTmpValue	Landroid/util/TypedValue;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_3
        //     10: ifnull +8 -> 18
        //     13: aload_0
        //     14: aload_3
        //     15: putfield 149	android/content/res/Resources:mCompatibilityInfo	Landroid/content/res/CompatibilityInfo;
        //     18: aload_2
        //     19: ifnull +11 -> 30
        //     22: aload_0
        //     23: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     26: aload_2
        //     27: invokevirtual 765	android/util/DisplayMetrics:setTo	(Landroid/util/DisplayMetrics;)V
        //     30: aload_0
        //     31: getfield 149	android/content/res/Resources:mCompatibilityInfo	Landroid/content/res/CompatibilityInfo;
        //     34: ifnull +14 -> 48
        //     37: aload_0
        //     38: getfield 149	android/content/res/Resources:mCompatibilityInfo	Landroid/content/res/CompatibilityInfo;
        //     41: aload_0
        //     42: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     45: invokevirtual 768	android/content/res/CompatibilityInfo:applyToDisplayMetrics	(Landroid/util/DisplayMetrics;)V
        //     48: ldc_w 769
        //     51: istore 6
        //     53: aload_1
        //     54: ifnull +76 -> 130
        //     57: aload_0
        //     58: getfield 98	android/content/res/Resources:mTmpConfig	Landroid/content/res/Configuration;
        //     61: aload_1
        //     62: invokevirtual 772	android/content/res/Configuration:setTo	(Landroid/content/res/Configuration;)V
        //     65: aload_0
        //     66: getfield 149	android/content/res/Resources:mCompatibilityInfo	Landroid/content/res/CompatibilityInfo;
        //     69: ifnull +14 -> 83
        //     72: aload_0
        //     73: getfield 149	android/content/res/Resources:mCompatibilityInfo	Landroid/content/res/CompatibilityInfo;
        //     76: aload_0
        //     77: getfield 98	android/content/res/Resources:mTmpConfig	Landroid/content/res/Configuration;
        //     80: invokevirtual 775	android/content/res/CompatibilityInfo:applyToConfiguration	(Landroid/content/res/Configuration;)V
        //     83: aload_0
        //     84: getfield 98	android/content/res/Resources:mTmpConfig	Landroid/content/res/Configuration;
        //     87: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     90: ifnonnull +13 -> 103
        //     93: aload_0
        //     94: getfield 98	android/content/res/Resources:mTmpConfig	Landroid/content/res/Configuration;
        //     97: invokestatic 781	java/util/Locale:getDefault	()Ljava/util/Locale;
        //     100: putfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     103: aload_0
        //     104: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     107: aload_0
        //     108: getfield 98	android/content/res/Resources:mTmpConfig	Landroid/content/res/Configuration;
        //     111: invokevirtual 785	android/content/res/Configuration:updateFrom	(Landroid/content/res/Configuration;)I
        //     114: istore 13
        //     116: iload 13
        //     118: invokestatic 790	android/content/pm/ActivityInfo:activityInfoConfigToNative	(I)I
        //     121: ldc_w 791
        //     124: iload 13
        //     126: iand
        //     127: ior
        //     128: istore 6
        //     130: aload_0
        //     131: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     134: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     137: ifnonnull +13 -> 150
        //     140: aload_0
        //     141: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     144: invokestatic 781	java/util/Locale:getDefault	()Ljava/util/Locale;
        //     147: putfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     150: aload_0
        //     151: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     154: aload_0
        //     155: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     158: getfield 794	android/util/DisplayMetrics:density	F
        //     161: aload_0
        //     162: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     165: getfield 797	android/content/res/Configuration:fontScale	F
        //     168: fmul
        //     169: putfield 800	android/util/DisplayMetrics:scaledDensity	F
        //     172: aconst_null
        //     173: astore 7
        //     175: aload_0
        //     176: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     179: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     182: ifnull +64 -> 246
        //     185: aload_0
        //     186: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     189: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     192: invokevirtual 803	java/util/Locale:getLanguage	()Ljava/lang/String;
        //     195: astore 7
        //     197: aload_0
        //     198: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     201: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     204: invokevirtual 806	java/util/Locale:getCountry	()Ljava/lang/String;
        //     207: ifnull +39 -> 246
        //     210: new 313	java/lang/StringBuilder
        //     213: dup
        //     214: invokespecial 314	java/lang/StringBuilder:<init>	()V
        //     217: aload 7
        //     219: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: ldc_w 808
        //     225: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     228: aload_0
        //     229: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     232: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     235: invokevirtual 806	java/util/Locale:getCountry	()Ljava/lang/String;
        //     238: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     241: invokevirtual 333	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     244: astore 7
        //     246: aload_0
        //     247: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     250: getfield 811	android/util/DisplayMetrics:widthPixels	I
        //     253: aload_0
        //     254: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     257: getfield 814	android/util/DisplayMetrics:heightPixels	I
        //     260: if_icmplt +221 -> 481
        //     263: aload_0
        //     264: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     267: getfield 811	android/util/DisplayMetrics:widthPixels	I
        //     270: istore 8
        //     272: aload_0
        //     273: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     276: getfield 814	android/util/DisplayMetrics:heightPixels	I
        //     279: istore 9
        //     281: aload_0
        //     282: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     285: getfield 817	android/content/res/Configuration:keyboardHidden	I
        //     288: istore 10
        //     290: iload 10
        //     292: iconst_1
        //     293: if_icmpne +17 -> 310
        //     296: aload_0
        //     297: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     300: getfield 820	android/content/res/Configuration:hardKeyboardHidden	I
        //     303: iconst_2
        //     304: if_icmpne +6 -> 310
        //     307: iconst_3
        //     308: istore 10
        //     310: aload_0
        //     311: getfield 131	android/content/res/Resources:mAssets	Landroid/content/res/AssetManager;
        //     314: aload_0
        //     315: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     318: getfield 823	android/content/res/Configuration:mcc	I
        //     321: aload_0
        //     322: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     325: getfield 826	android/content/res/Configuration:mnc	I
        //     328: aload 7
        //     330: aload_0
        //     331: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     334: getfield 829	android/content/res/Configuration:orientation	I
        //     337: aload_0
        //     338: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     341: getfield 832	android/content/res/Configuration:touchscreen	I
        //     344: ldc_w 833
        //     347: aload_0
        //     348: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     351: getfield 794	android/util/DisplayMetrics:density	F
        //     354: fmul
        //     355: f2i
        //     356: aload_0
        //     357: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     360: getfield 836	android/content/res/Configuration:keyboard	I
        //     363: iload 10
        //     365: aload_0
        //     366: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     369: getfield 839	android/content/res/Configuration:navigation	I
        //     372: iload 8
        //     374: iload 9
        //     376: aload_0
        //     377: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     380: getfield 842	android/content/res/Configuration:smallestScreenWidthDp	I
        //     383: aload_0
        //     384: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     387: getfield 845	android/content/res/Configuration:screenWidthDp	I
        //     390: aload_0
        //     391: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     394: getfield 848	android/content/res/Configuration:screenHeightDp	I
        //     397: aload_0
        //     398: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     401: getfield 851	android/content/res/Configuration:screenLayout	I
        //     404: aload_0
        //     405: getfield 118	android/content/res/Resources:mConfiguration	Landroid/content/res/Configuration;
        //     408: getfield 854	android/content/res/Configuration:uiMode	I
        //     411: getstatic 859	android/os/Build$VERSION:RESOURCES_SDK_INT	I
        //     414: invokevirtual 863	android/content/res/AssetManager:setConfiguration	(IILjava/lang/String;IIIIIIIIIIIIII)V
        //     417: aload_0
        //     418: aload_0
        //     419: getfield 100	android/content/res/Resources:mDrawableCache	Landroid/util/LongSparseArray;
        //     422: iload 6
        //     424: invokespecial 865	android/content/res/Resources:clearDrawableCache	(Landroid/util/LongSparseArray;I)V
        //     427: aload_0
        //     428: aload_0
        //     429: getfield 104	android/content/res/Resources:mColorDrawableCache	Landroid/util/LongSparseArray;
        //     432: iload 6
        //     434: invokespecial 865	android/content/res/Resources:clearDrawableCache	(Landroid/util/LongSparseArray;I)V
        //     437: aload_0
        //     438: getfield 102	android/content/res/Resources:mColorStateListCache	Landroid/util/LongSparseArray;
        //     441: invokevirtual 200	android/util/LongSparseArray:clear	()V
        //     444: aload_0
        //     445: invokevirtual 287	android/content/res/Resources:flushLayoutCache	()V
        //     448: aload 4
        //     450: monitorexit
        //     451: getstatic 73	android/content/res/Resources:mSync	Ljava/lang/Object;
        //     454: astore 11
        //     456: aload 11
        //     458: monitorenter
        //     459: aload_0
        //     460: getfield 238	android/content/res/Resources:mPluralRule	Llibcore/icu/NativePluralRules;
        //     463: ifnull +14 -> 477
        //     466: aload_0
        //     467: aload_1
        //     468: getfield 242	android/content/res/Configuration:locale	Ljava/util/Locale;
        //     471: invokestatic 248	libcore/icu/NativePluralRules:forLocale	(Ljava/util/Locale;)Llibcore/icu/NativePluralRules;
        //     474: putfield 238	android/content/res/Resources:mPluralRule	Llibcore/icu/NativePluralRules;
        //     477: aload 11
        //     479: monitorexit
        //     480: return
        //     481: aload_0
        //     482: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     485: getfield 814	android/util/DisplayMetrics:heightPixels	I
        //     488: istore 8
        //     490: aload_0
        //     491: getfield 123	android/content/res/Resources:mMetrics	Landroid/util/DisplayMetrics;
        //     494: getfield 811	android/util/DisplayMetrics:widthPixels	I
        //     497: istore 9
        //     499: goto -218 -> 281
        //     502: astore 5
        //     504: aload 4
        //     506: monitorexit
        //     507: aload 5
        //     509: athrow
        //     510: astore 12
        //     512: aload 11
        //     514: monitorexit
        //     515: aload 12
        //     517: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     13	451	502	finally
        //     481	507	502	finally
        //     459	480	510	finally
        //     512	515	510	finally
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public class Theme
    {
        private final AssetManager mAssets = Resources.this.mAssets;
        private final int mTheme = this.mAssets.createTheme();

        Theme()
        {
        }

        public void applyStyle(int paramInt, boolean paramBoolean)
        {
            AssetManager.applyThemeStyle(this.mTheme, paramInt, paramBoolean);
        }

        public void dump(int paramInt, String paramString1, String paramString2)
        {
            AssetManager.dumpTheme(this.mTheme, paramInt, paramString1, paramString2);
        }

        protected void finalize()
            throws Throwable
        {
            super.finalize();
            this.mAssets.releaseTheme(this.mTheme);
        }

        public TypedArray obtainStyledAttributes(int paramInt, int[] paramArrayOfInt)
            throws Resources.NotFoundException
        {
            int i = paramArrayOfInt.length;
            TypedArray localTypedArray = Resources.this.getCachedStyledAttributes(i);
            localTypedArray.mRsrcs = paramArrayOfInt;
            AssetManager.applyStyle(this.mTheme, 0, paramInt, 0, paramArrayOfInt, localTypedArray.mData, localTypedArray.mIndices);
            return localTypedArray;
        }

        public TypedArray obtainStyledAttributes(AttributeSet paramAttributeSet, int[] paramArrayOfInt, int paramInt1, int paramInt2)
        {
            int i = paramArrayOfInt.length;
            TypedArray localTypedArray = Resources.this.getCachedStyledAttributes(i);
            XmlBlock.Parser localParser = (XmlBlock.Parser)paramAttributeSet;
            int j = this.mTheme;
            if (localParser != null);
            for (int k = localParser.mParseState; ; k = 0)
            {
                AssetManager.applyStyle(j, paramInt1, paramInt2, k, paramArrayOfInt, localTypedArray.mData, localTypedArray.mIndices);
                localTypedArray.mRsrcs = paramArrayOfInt;
                localTypedArray.mXml = localParser;
                return localTypedArray;
            }
        }

        public TypedArray obtainStyledAttributes(int[] paramArrayOfInt)
        {
            int i = paramArrayOfInt.length;
            TypedArray localTypedArray = Resources.this.getCachedStyledAttributes(i);
            localTypedArray.mRsrcs = paramArrayOfInt;
            AssetManager.applyStyle(this.mTheme, 0, 0, 0, paramArrayOfInt, localTypedArray.mData, localTypedArray.mIndices);
            return localTypedArray;
        }

        public boolean resolveAttribute(int paramInt, TypedValue paramTypedValue, boolean paramBoolean)
        {
            return this.mAssets.getThemeValue(this.mTheme, paramInt, paramTypedValue, paramBoolean);
        }

        public void setTo(Theme paramTheme)
        {
            AssetManager.copyTheme(this.mTheme, paramTheme.mTheme);
        }
    }

    public static class NotFoundException extends RuntimeException
    {
        public NotFoundException()
        {
        }

        public NotFoundException(String paramString)
        {
            super();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        private static int mId;

        static Drawable createFromResourceStream(Resources paramResources, TypedValue paramTypedValue, InputStream paramInputStream, String paramString, BitmapFactory.Options paramOptions)
        {
            Drawable localDrawable = paramResources.loadOverlayDrawable(paramTypedValue, mId);
            if (localDrawable == null)
                localDrawable = Drawable.createFromResourceStream(paramResources, paramTypedValue, paramInputStream, paramString, paramOptions);
            return localDrawable;
        }

        static void setDrawableId(int paramInt)
        {
            mId = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.Resources
 * JD-Core Version:        0.6.2
 */