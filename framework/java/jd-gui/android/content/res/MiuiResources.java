package android.content.res;

import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import java.io.InputStream;
import miui.content.res.ExtraConfiguration;
import miui.content.res.ThemeResources;
import miui.content.res.ThemeResourcesPackage;
import miui.content.res.ThemeResourcesSystem;
import miui.content.res.ThemeZipFile.ThemeFileInfo;

public final class MiuiResources extends Resources
{
    public static final int sCookieTypeFramework = 1;
    public static final int sCookieTypeMiui = 2;
    public static final int sCookieTypeOther = 3;
    private SparseArray<CharSequence> mCharSequences = new SparseArray();
    private SparseIntArray mCookieType = new SparseIntArray();
    private boolean mHasValues;
    private SparseArray<Integer> mIntegers = new SparseArray();
    private SparseArray<Boolean> mSkipFiles = new SparseArray();
    private ThemeResources mThemeResources;

    MiuiResources()
    {
        init(null);
    }

    public MiuiResources(AssetManager paramAssetManager, DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration)
    {
        super(paramAssetManager, paramDisplayMetrics, paramConfiguration);
        init(null);
    }

    public MiuiResources(AssetManager paramAssetManager, DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo)
    {
        super(paramAssetManager, paramDisplayMetrics, paramConfiguration, paramCompatibilityInfo);
        init(null);
    }

    private int getCookieType(int paramInt)
    {
        int i = this.mCookieType.get(paramInt);
        String str;
        if (i == 0)
        {
            str = this.mAssets.getCookieName(paramInt);
            if (!"/system/framework/framework-res.apk".equals(str))
                break label44;
            i = 1;
        }
        while (true)
        {
            this.mCookieType.put(paramInt, i);
            return i;
            label44: if ("/system/framework/framework-miui-res.apk".equals(str))
                i = 2;
            else
                i = 3;
        }
    }

    private TypedArray replaceTypedArray(TypedArray paramTypedArray)
    {
        if ((this.mThemeResources != null) && (!this.mHasValues));
        while (true)
        {
            return paramTypedArray;
            int[] arrayOfInt = paramTypedArray.mData;
            for (int i = 0; i < arrayOfInt.length; i += 6)
            {
                int j = arrayOfInt[(i + 0)];
                int k = arrayOfInt[(i + 3)];
                if (((j >= 16) && (j <= 31)) || (j == 5))
                {
                    Integer localInteger = getThemeInt(k);
                    if (localInteger != null)
                        arrayOfInt[(i + 1)] = localInteger.intValue();
                }
            }
        }
    }

    public CharSequence getText(int paramInt)
        throws Resources.NotFoundException
    {
        CharSequence localCharSequence = getThemeCharSequence(paramInt);
        if (localCharSequence != null);
        while (true)
        {
            return localCharSequence;
            localCharSequence = super.getText(paramInt);
        }
    }

    public CharSequence getText(int paramInt, CharSequence paramCharSequence)
    {
        CharSequence localCharSequence = getThemeCharSequence(paramInt);
        if (localCharSequence != null);
        while (true)
        {
            return localCharSequence;
            localCharSequence = super.getText(paramInt, paramCharSequence);
        }
    }

    CharSequence getThemeCharSequence(int paramInt)
    {
        CharSequence localCharSequence;
        if (!this.mHasValues)
            localCharSequence = null;
        while (true)
        {
            return localCharSequence;
            int i = this.mCharSequences.indexOfKey(paramInt);
            if (i >= 0)
            {
                localCharSequence = (CharSequence)this.mCharSequences.valueAt(i);
            }
            else
            {
                localCharSequence = this.mThemeResources.getThemeCharSequence(paramInt);
                this.mCharSequences.put(paramInt, localCharSequence);
            }
        }
    }

    Integer getThemeInt(int paramInt)
    {
        Integer localInteger;
        if (!this.mHasValues)
            localInteger = null;
        while (true)
        {
            return localInteger;
            int i = this.mIntegers.indexOfKey(paramInt);
            if (i >= 0)
            {
                localInteger = (Integer)this.mIntegers.valueAt(i);
            }
            else
            {
                localInteger = this.mThemeResources.getThemeInt(paramInt);
                this.mIntegers.put(paramInt, localInteger);
            }
        }
    }

    public void getValue(int paramInt, TypedValue paramTypedValue, boolean paramBoolean)
        throws Resources.NotFoundException
    {
        super.getValue(paramInt, paramTypedValue, paramBoolean);
        if (((paramTypedValue.type >= 16) && (paramTypedValue.type <= 31)) || (paramTypedValue.type == 5))
        {
            Integer localInteger = getThemeInt(paramInt);
            if (localInteger != null)
                paramTypedValue.data = localInteger.intValue();
        }
    }

    public void init(String paramString)
    {
        if ((TextUtils.isEmpty(paramString)) || ("android".equals(paramString)) || ("miui".equals(paramString)));
        for (this.mThemeResources = ThemeResources.getSystem(this); ; this.mThemeResources = ThemeResourcesPackage.getThemeResources(this, paramString))
        {
            this.mHasValues = this.mThemeResources.hasValues();
            return;
        }
    }

    Drawable loadOverlayDrawable(TypedValue paramTypedValue, int paramInt)
    {
        Object localObject1;
        if (this.mSkipFiles.get(paramInt) != null)
            localObject1 = null;
        while (true)
        {
            return localObject1;
            localObject1 = null;
            String str = paramTypedValue.string.toString();
            ThemeZipFile.ThemeFileInfo localThemeFileInfo = this.mThemeResources.getThemeFileStream(getCookieType(paramTypedValue.assetCookie), str);
            Object localObject2;
            if (localThemeFileInfo != null)
                localObject2 = null;
            try
            {
                if ((localThemeFileInfo.mDensity > 0) && (paramTypedValue.density != 65535))
                    localOptions = new BitmapFactory.Options();
            }
            catch (OutOfMemoryError localOutOfMemoryError1)
            {
                try
                {
                    BitmapFactory.Options localOptions;
                    localOptions.inDensity = localThemeFileInfo.mDensity;
                    localObject2 = localOptions;
                    InputStream localInputStream = localThemeFileInfo.mInput;
                    if (str.endsWith(".9.png"))
                        localInputStream = SimulateNinePngUtil.convertIntoNinePngStream(localInputStream);
                    Drawable localDrawable = Drawable.createFromResourceStream(this, paramTypedValue, localInputStream, str, localObject2);
                    localObject1 = localDrawable;
                    while (true)
                    {
                        try
                        {
                            label133: localThemeFileInfo.mInput.close();
                        }
                        catch (Exception localException)
                        {
                        }
                        break;
                        this.mSkipFiles.put(paramInt, Boolean.valueOf(true));
                        break;
                        localOutOfMemoryError1 = localOutOfMemoryError1;
                    }
                }
                catch (OutOfMemoryError localOutOfMemoryError2)
                {
                    break label133;
                }
            }
        }
    }

    public final Resources.Theme newTheme()
    {
        return new MIUITheme();
    }

    public TypedArray obtainAttributes(AttributeSet paramAttributeSet, int[] paramArrayOfInt)
    {
        return replaceTypedArray(super.obtainAttributes(paramAttributeSet, paramArrayOfInt));
    }

    public TypedArray obtainTypedArray(int paramInt)
        throws Resources.NotFoundException
    {
        return replaceTypedArray(super.obtainTypedArray(paramInt));
    }

    public InputStream openRawResource(int paramInt, TypedValue paramTypedValue)
        throws Resources.NotFoundException
    {
        ThemeZipFile.ThemeFileInfo localThemeFileInfo;
        if (this.mSkipFiles.get(paramInt) == null)
        {
            getValue(paramInt, paramTypedValue, true);
            String str = paramTypedValue.string.toString();
            localThemeFileInfo = this.mThemeResources.getThemeFileStream(getCookieType(paramTypedValue.assetCookie), str);
            if (localThemeFileInfo == null);
        }
        for (InputStream localInputStream = localThemeFileInfo.mInput; ; localInputStream = super.openRawResource(paramInt, paramTypedValue))
        {
            return localInputStream;
            this.mSkipFiles.put(paramInt, Boolean.valueOf(true));
        }
    }

    public void updateConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo)
    {
        Configuration localConfiguration = getConfiguration();
        if ((localConfiguration != null) && (paramConfiguration != null));
        for (int i = localConfiguration.diff(paramConfiguration); ; i = 0)
        {
            super.updateConfiguration(paramConfiguration, paramDisplayMetrics, paramCompatibilityInfo);
            if ((this.mThemeResources != null) && (((i & 0x200) != 0) || (ExtraConfiguration.needNewResources(i))))
            {
                if (ThemeResources.getSystem().checkUpdate())
                    Resources.clearPreloadedCache();
                this.mIntegers.clear();
                this.mCharSequences.clear();
                this.mSkipFiles.clear();
                this.mThemeResources.checkUpdate();
                this.mHasValues = this.mThemeResources.hasValues();
            }
            return;
        }
    }

    public final class MIUITheme extends Resources.Theme
    {
        public MIUITheme()
        {
            super();
        }

        public TypedArray obtainStyledAttributes(int paramInt, int[] paramArrayOfInt)
            throws Resources.NotFoundException
        {
            return MiuiResources.this.replaceTypedArray(super.obtainStyledAttributes(paramInt, paramArrayOfInt));
        }

        public TypedArray obtainStyledAttributes(AttributeSet paramAttributeSet, int[] paramArrayOfInt, int paramInt1, int paramInt2)
        {
            return MiuiResources.this.replaceTypedArray(super.obtainStyledAttributes(paramAttributeSet, paramArrayOfInt, paramInt1, paramInt2));
        }

        public TypedArray obtainStyledAttributes(int[] paramArrayOfInt)
        {
            return MiuiResources.this.replaceTypedArray(super.obtainStyledAttributes(paramArrayOfInt));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.MiuiResources
 * JD-Core Version:        0.6.2
 */