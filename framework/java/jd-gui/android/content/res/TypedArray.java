package android.content.res;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.TypedValue;
import com.android.internal.util.XmlUtils;
import java.util.Arrays;

public class TypedArray
{
    int[] mData;
    int[] mIndices;
    int mLength;
    private final Resources mResources;
    int[] mRsrcs;
    TypedValue mValue = new TypedValue();
    XmlBlock.Parser mXml;

    TypedArray(Resources paramResources, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
    {
        this.mResources = paramResources;
        this.mData = paramArrayOfInt1;
        this.mIndices = paramArrayOfInt2;
        this.mLength = paramInt;
    }

    private boolean getValueAt(int paramInt, TypedValue paramTypedValue)
    {
        int[] arrayOfInt = this.mData;
        int i = arrayOfInt[(paramInt + 0)];
        boolean bool;
        if (i == 0)
        {
            bool = false;
            return bool;
        }
        paramTypedValue.type = i;
        paramTypedValue.data = arrayOfInt[(paramInt + 1)];
        paramTypedValue.assetCookie = arrayOfInt[(paramInt + 2)];
        paramTypedValue.resourceId = arrayOfInt[(paramInt + 3)];
        paramTypedValue.changingConfigurations = arrayOfInt[(paramInt + 4)];
        paramTypedValue.density = arrayOfInt[(paramInt + 5)];
        if (i == 3);
        for (CharSequence localCharSequence = loadStringValueAt(paramInt); ; localCharSequence = null)
        {
            paramTypedValue.string = localCharSequence;
            bool = true;
            break;
        }
    }

    private CharSequence loadStringValueAt(int paramInt)
    {
        int[] arrayOfInt = this.mData;
        int i = arrayOfInt[(paramInt + 2)];
        CharSequence localCharSequence;
        if (i < 0)
            if (this.mXml != null)
                localCharSequence = this.mXml.getPooledString(arrayOfInt[(paramInt + 1)]);
        while (true)
        {
            return localCharSequence;
            localCharSequence = null;
            continue;
            localCharSequence = this.mResources.mAssets.getPooledString(i, arrayOfInt[(paramInt + 1)]);
        }
    }

    public boolean getBoolean(int paramInt, boolean paramBoolean)
    {
        int i = paramInt * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramBoolean;
            if ((j >= 16) && (j <= 31))
            {
                if (arrayOfInt[(i + 1)] != 0);
                for (boolean bool = true; ; bool = false)
                {
                    paramBoolean = bool;
                    break;
                }
            }
            TypedValue localTypedValue = this.mValue;
            if (getValueAt(i, localTypedValue))
            {
                Log.w("Resources", "Converting to boolean: " + localTypedValue);
                paramBoolean = XmlUtils.convertValueToBoolean(localTypedValue.coerceToString(), paramBoolean);
            }
            else
            {
                Log.w("Resources", "getBoolean of bad type: 0x" + Integer.toHexString(j));
            }
        }
    }

    public int getColor(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramInt2;
            if ((j >= 16) && (j <= 31))
            {
                paramInt2 = arrayOfInt[(i + 1)];
            }
            else
            {
                if (j != 3)
                    break;
                TypedValue localTypedValue = this.mValue;
                if (getValueAt(i, localTypedValue))
                    paramInt2 = this.mResources.loadColorStateList(localTypedValue, localTypedValue.resourceId).getDefaultColor();
            }
        }
        throw new UnsupportedOperationException("Can't convert to color: type=0x" + Integer.toHexString(j));
    }

    public ColorStateList getColorStateList(int paramInt)
    {
        TypedValue localTypedValue = this.mValue;
        if (getValueAt(paramInt * 6, localTypedValue));
        for (ColorStateList localColorStateList = this.mResources.loadColorStateList(localTypedValue, localTypedValue.resourceId); ; localColorStateList = null)
            return localColorStateList;
    }

    public float getDimension(int paramInt, float paramFloat)
    {
        int i = paramInt * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramFloat;
            if (j != 5)
                break;
            paramFloat = TypedValue.complexToDimension(arrayOfInt[(i + 1)], this.mResources.mMetrics);
        }
        throw new UnsupportedOperationException("Can't convert to dimension: type=0x" + Integer.toHexString(j));
    }

    public int getDimensionPixelOffset(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramInt2;
            if (j != 5)
                break;
            paramInt2 = TypedValue.complexToDimensionPixelOffset(arrayOfInt[(i + 1)], this.mResources.mMetrics);
        }
        throw new UnsupportedOperationException("Can't convert to dimension: type=0x" + Integer.toHexString(j));
    }

    public int getDimensionPixelSize(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramInt2;
            if (j != 5)
                break;
            paramInt2 = TypedValue.complexToDimensionPixelSize(arrayOfInt[(i + 1)], this.mResources.mMetrics);
        }
        throw new UnsupportedOperationException("Can't convert to dimension: type=0x" + Integer.toHexString(j));
    }

    public Drawable getDrawable(int paramInt)
    {
        TypedValue localTypedValue = this.mValue;
        if (getValueAt(paramInt * 6, localTypedValue));
        for (Drawable localDrawable = this.mResources.loadDrawable(localTypedValue, localTypedValue.resourceId); ; localDrawable = null)
            return localDrawable;
    }

    public float getFloat(int paramInt, float paramFloat)
    {
        int i = paramInt * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramFloat;
            if (j == 4)
            {
                paramFloat = Float.intBitsToFloat(arrayOfInt[(i + 1)]);
            }
            else if ((j >= 16) && (j <= 31))
            {
                paramFloat = arrayOfInt[(i + 1)];
            }
            else
            {
                TypedValue localTypedValue = this.mValue;
                if (getValueAt(i, localTypedValue))
                {
                    Log.w("Resources", "Converting to float: " + localTypedValue);
                    CharSequence localCharSequence = localTypedValue.coerceToString();
                    if (localCharSequence != null)
                        paramFloat = Float.parseFloat(localCharSequence.toString());
                }
                else
                {
                    Log.w("Resources", "getFloat of bad type: 0x" + Integer.toHexString(j));
                }
            }
        }
    }

    public float getFraction(int paramInt1, int paramInt2, int paramInt3, float paramFloat)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramFloat;
            if (j != 6)
                break;
            paramFloat = TypedValue.complexToFraction(arrayOfInt[(i + 1)], paramInt2, paramInt3);
        }
        throw new UnsupportedOperationException("Can't convert to fraction: type=0x" + Integer.toHexString(j));
    }

    public int getIndex(int paramInt)
    {
        return this.mIndices[(paramInt + 1)];
    }

    public int getIndexCount()
    {
        return this.mIndices[0];
    }

    public int getInt(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramInt2;
            if ((j >= 16) && (j <= 31))
            {
                paramInt2 = arrayOfInt[(i + 1)];
            }
            else
            {
                TypedValue localTypedValue = this.mValue;
                if (getValueAt(i, localTypedValue))
                {
                    Log.w("Resources", "Converting to int: " + localTypedValue);
                    paramInt2 = XmlUtils.convertValueToInt(localTypedValue.coerceToString(), paramInt2);
                }
                else
                {
                    Log.w("Resources", "getInt of bad type: 0x" + Integer.toHexString(j));
                }
            }
        }
    }

    public int getInteger(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if (j == 0);
        while (true)
        {
            return paramInt2;
            if ((j < 16) || (j > 31))
                break;
            paramInt2 = arrayOfInt[(i + 1)];
        }
        throw new UnsupportedOperationException("Can't convert to integer: type=0x" + Integer.toHexString(j));
    }

    public int getLayoutDimension(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if ((j >= 16) && (j <= 31))
            paramInt2 = arrayOfInt[(i + 1)];
        while (true)
        {
            return paramInt2;
            if (j == 5)
                paramInt2 = TypedValue.complexToDimensionPixelSize(arrayOfInt[(i + 1)], this.mResources.mMetrics);
        }
    }

    public int getLayoutDimension(int paramInt, String paramString)
    {
        int i = paramInt * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if ((j >= 16) && (j <= 31));
        for (int k = arrayOfInt[(i + 1)]; ; k = TypedValue.complexToDimensionPixelSize(arrayOfInt[(i + 1)], this.mResources.mMetrics))
        {
            return k;
            if (j != 5)
                break;
        }
        throw new RuntimeException(getPositionDescription() + ": You must supply a " + paramString + " attribute.");
    }

    public String getNonConfigurationString(int paramInt1, int paramInt2)
    {
        String str = null;
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j = arrayOfInt[(i + 0)];
        if ((arrayOfInt[(i + 4)] & (paramInt2 ^ 0xFFFFFFFF)) != 0);
        while (true)
        {
            return str;
            if (j != 0)
                if (j == 3)
                {
                    str = loadStringValueAt(i).toString();
                }
                else
                {
                    TypedValue localTypedValue = this.mValue;
                    if (getValueAt(i, localTypedValue))
                    {
                        Log.w("Resources", "Converting to string: " + localTypedValue);
                        CharSequence localCharSequence = localTypedValue.coerceToString();
                        if (localCharSequence != null)
                            str = localCharSequence.toString();
                    }
                    else
                    {
                        Log.w("Resources", "getString of bad type: 0x" + Integer.toHexString(j));
                    }
                }
        }
    }

    public String getNonResourceString(int paramInt)
    {
        int i = paramInt * 6;
        int[] arrayOfInt = this.mData;
        if ((arrayOfInt[(i + 0)] == 3) && (arrayOfInt[(i + 2)] < 0));
        for (String str = this.mXml.getPooledString(arrayOfInt[(i + 1)]).toString(); ; str = null)
            return str;
    }

    public String getPositionDescription()
    {
        if (this.mXml != null);
        for (String str = this.mXml.getPositionDescription(); ; str = "<internal>")
            return str;
    }

    public int getResourceId(int paramInt1, int paramInt2)
    {
        int i = paramInt1 * 6;
        int[] arrayOfInt = this.mData;
        int j;
        if (arrayOfInt[(i + 0)] != 0)
        {
            j = arrayOfInt[(i + 3)];
            if (j == 0);
        }
        while (true)
        {
            return j;
            j = paramInt2;
        }
    }

    public Resources getResources()
    {
        return this.mResources;
    }

    public String getString(int paramInt)
    {
        String str = null;
        int i = paramInt * 6;
        int j = this.mData[(i + 0)];
        if (j == 0);
        while (true)
        {
            return str;
            if (j == 3)
            {
                str = loadStringValueAt(i).toString();
            }
            else
            {
                TypedValue localTypedValue = this.mValue;
                if (getValueAt(i, localTypedValue))
                {
                    Log.w("Resources", "Converting to string: " + localTypedValue);
                    CharSequence localCharSequence = localTypedValue.coerceToString();
                    if (localCharSequence != null)
                        str = localCharSequence.toString();
                }
                else
                {
                    Log.w("Resources", "getString of bad type: 0x" + Integer.toHexString(j));
                }
            }
        }
    }

    public CharSequence getText(int paramInt)
    {
        CharSequence localCharSequence = null;
        int i = paramInt * 6;
        int j = this.mData[(i + 0)];
        if (j == 0);
        while (true)
        {
            return localCharSequence;
            if (j == 3)
            {
                localCharSequence = loadStringValueAt(i);
            }
            else
            {
                TypedValue localTypedValue = this.mValue;
                if (getValueAt(i, localTypedValue))
                {
                    Log.w("Resources", "Converting to string: " + localTypedValue);
                    localCharSequence = localTypedValue.coerceToString();
                }
                else
                {
                    Log.w("Resources", "getString of bad type: 0x" + Integer.toHexString(j));
                }
            }
        }
    }

    public CharSequence[] getTextArray(int paramInt)
    {
        TypedValue localTypedValue = this.mValue;
        if (getValueAt(paramInt * 6, localTypedValue));
        for (CharSequence[] arrayOfCharSequence = this.mResources.getTextArray(localTypedValue.resourceId); ; arrayOfCharSequence = null)
            return arrayOfCharSequence;
    }

    public boolean getValue(int paramInt, TypedValue paramTypedValue)
    {
        return getValueAt(paramInt * 6, paramTypedValue);
    }

    public boolean hasValue(int paramInt)
    {
        int i = paramInt * 6;
        if (this.mData[(i + 0)] != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int length()
    {
        return this.mLength;
    }

    public TypedValue peekValue(int paramInt)
    {
        TypedValue localTypedValue = this.mValue;
        if (getValueAt(paramInt * 6, localTypedValue));
        while (true)
        {
            return localTypedValue;
            localTypedValue = null;
        }
    }

    public void recycle()
    {
        synchronized (this.mResources.mTmpValue)
        {
            TypedArray localTypedArray = this.mResources.mCachedStyledAttributes;
            if ((localTypedArray == null) || (localTypedArray.mData.length < this.mData.length))
            {
                this.mXml = null;
                this.mResources.mCachedStyledAttributes = this;
            }
            return;
        }
    }

    public String toString()
    {
        return Arrays.toString(this.mData);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.TypedArray
 * JD-Core Version:        0.6.2
 */