package android.content.res;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.MiuiThemeHelper;
import android.os.ParcelFileDescriptor;
import android.util.TypedValue;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public final class AssetManager
{
    public static final int ACCESS_BUFFER = 3;
    public static final int ACCESS_RANDOM = 1;
    public static final int ACCESS_STREAMING = 2;
    public static final int ACCESS_UNKNOWN = 0;
    private static final boolean DEBUG_REFS = false;
    static final int STYLE_ASSET_COOKIE = 2;
    static final int STYLE_CHANGING_CONFIGURATIONS = 4;
    static final int STYLE_DATA = 1;
    static final int STYLE_DENSITY = 5;
    static final int STYLE_NUM_ENTRIES = 6;
    static final int STYLE_RESOURCE_ID = 3;
    static final int STYLE_TYPE = 0;
    private static final String TAG = "AssetManager";
    private static final boolean localLOGV;
    private static final Object sSync = new Object();
    static AssetManager sSystem = null;
    private int mNObject;
    private int mNumRefs = 1;
    private int mObject;
    private final long[] mOffsets = new long[2];
    private boolean mOpen = true;
    private HashMap<Integer, RuntimeException> mRefStacks;
    private StringBlock[] mStringBlocks = null;
    private final TypedValue mValue = new TypedValue();

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public AssetManager()
    {
        try
        {
            init();
            ensureSystemAssets();
            MiuiThemeHelper.addExtraAssetPaths(this);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private AssetManager(boolean paramBoolean)
    {
        init();
        MiuiThemeHelper.addExtraAssetPaths(this);
    }

    static final native boolean applyStyle(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3);

    static final native void applyThemeStyle(int paramInt1, int paramInt2, boolean paramBoolean);

    static final native void copyTheme(int paramInt1, int paramInt2);

    private final void decRefsLocked(int paramInt)
    {
        this.mNumRefs = (-1 + this.mNumRefs);
        if (this.mNumRefs == 0)
            destroy();
    }

    private final native void deleteTheme(int paramInt);

    private final native void destroy();

    private final native void destroyAsset(int paramInt);

    static final native void dumpTheme(int paramInt1, int paramInt2, String paramString1, String paramString2);

    private static void ensureSystemAssets()
    {
        synchronized (sSync)
        {
            if (sSystem == null)
            {
                AssetManager localAssetManager = new AssetManager(true);
                localAssetManager.makeStringBlocks(false);
                sSystem = localAssetManager;
            }
            return;
        }
    }

    private final native int[] getArrayStringInfo(int paramInt);

    private final native String[] getArrayStringResource(int paramInt);

    public static final native String getAssetAllocations();

    private final native long getAssetLength(int paramInt);

    private final native long getAssetRemainingLength(int paramInt);

    public static final native int getGlobalAssetCount();

    public static final native int getGlobalAssetManagerCount();

    private final native int getNativeStringBlock(int paramInt);

    private final native int getStringBlockCount();

    public static AssetManager getSystem()
    {
        ensureSystemAssets();
        return sSystem;
    }

    private final void incRefsLocked(int paramInt)
    {
        this.mNumRefs = (1 + this.mNumRefs);
    }

    private final native void init();

    private final native int loadResourceBagValue(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean);

    private final native int loadResourceValue(int paramInt, short paramShort, TypedValue paramTypedValue, boolean paramBoolean);

    static final native int loadThemeAttributeValue(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean);

    private final native int newTheme();

    private final native int openAsset(String paramString, int paramInt);

    private final native ParcelFileDescriptor openAssetFd(String paramString, long[] paramArrayOfLong)
        throws IOException;

    private native ParcelFileDescriptor openNonAssetFdNative(int paramInt, String paramString, long[] paramArrayOfLong)
        throws IOException;

    private final native int openNonAssetNative(int paramInt1, String paramString, int paramInt2);

    private final native int openXmlAssetNative(int paramInt, String paramString);

    private final native int readAsset(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3);

    private final native int readAssetChar(int paramInt);

    private final native long seekAsset(int paramInt1, long paramLong, int paramInt2);

    public final native int addAssetPath(String paramString);

    public final int[] addAssetPaths(String[] paramArrayOfString)
    {
        int[] arrayOfInt;
        if (paramArrayOfString == null)
            arrayOfInt = null;
        while (true)
        {
            return arrayOfInt;
            arrayOfInt = new int[paramArrayOfString.length];
            for (int i = 0; i < paramArrayOfString.length; i++)
                arrayOfInt[i] = addAssetPath(paramArrayOfString[i]);
        }
    }

    public void close()
    {
        try
        {
            if (this.mOpen)
            {
                this.mOpen = false;
                decRefsLocked(hashCode());
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    final int createTheme()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 78	android/content/res/AssetManager:mOpen	Z
        //     6: ifne +18 -> 24
        //     9: new 191	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 193
        //     15: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_1
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_1
        //     23: athrow
        //     24: aload_0
        //     25: invokespecial 198	android/content/res/AssetManager:newTheme	()I
        //     28: istore_2
        //     29: aload_0
        //     30: iload_2
        //     31: invokespecial 200	android/content/res/AssetManager:incRefsLocked	(I)V
        //     34: aload_0
        //     35: monitorexit
        //     36: iload_2
        //     37: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	36	19	finally
    }

    final void ensureStringBlocks()
    {
        if (this.mStringBlocks == null)
            try
            {
                if (this.mStringBlocks == null)
                    makeStringBlocks(true);
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            destroy();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    final native int[] getArrayIntResource(int paramInt);

    final native int getArraySize(int paramInt);

    public final native String getCookieName(int paramInt);

    public final native String[] getLocales();

    final CharSequence getPooledString(int paramInt1, int paramInt2)
    {
        return this.mStringBlocks[(paramInt1 - 1)].get(paramInt2);
    }

    // ERROR //
    final CharSequence getResourceBagText(int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 70	android/content/res/AssetManager:mValue	Landroid/util/TypedValue;
        //     6: astore 4
        //     8: aload_0
        //     9: iload_1
        //     10: iload_2
        //     11: aload 4
        //     13: iconst_1
        //     14: invokespecial 223	android/content/res/AssetManager:loadResourceBagValue	(IILandroid/util/TypedValue;Z)I
        //     17: istore 5
        //     19: iload 5
        //     21: iflt +51 -> 72
        //     24: aload 4
        //     26: getfield 226	android/util/TypedValue:type	I
        //     29: iconst_3
        //     30: if_icmpne +25 -> 55
        //     33: aload_0
        //     34: getfield 74	android/content/res/AssetManager:mStringBlocks	[Landroid/content/res/StringBlock;
        //     37: iload 5
        //     39: aaload
        //     40: aload 4
        //     42: getfield 229	android/util/TypedValue:data	I
        //     45: invokevirtual 220	android/content/res/StringBlock:get	(I)Ljava/lang/CharSequence;
        //     48: astore 6
        //     50: aload_0
        //     51: monitorexit
        //     52: goto +25 -> 77
        //     55: aload 4
        //     57: invokevirtual 233	android/util/TypedValue:coerceToString	()Ljava/lang/CharSequence;
        //     60: astore 6
        //     62: aload_0
        //     63: monitorexit
        //     64: goto +13 -> 77
        //     67: astore_3
        //     68: aload_0
        //     69: monitorexit
        //     70: aload_3
        //     71: athrow
        //     72: aload_0
        //     73: monitorexit
        //     74: aconst_null
        //     75: astore 6
        //     77: aload 6
        //     79: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	70	67	finally
        //     72	74	67	finally
    }

    final native String getResourceEntryName(int paramInt);

    final native int getResourceIdentifier(String paramString1, String paramString2, String paramString3);

    final native String getResourceName(int paramInt);

    final native String getResourcePackageName(int paramInt);

    final String[] getResourceStringArray(int paramInt)
    {
        return getArrayStringResource(paramInt);
    }

    // ERROR //
    final CharSequence getResourceText(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 70	android/content/res/AssetManager:mValue	Landroid/util/TypedValue;
        //     6: astore_3
        //     7: aload_0
        //     8: iload_1
        //     9: iconst_0
        //     10: aload_3
        //     11: iconst_1
        //     12: invokespecial 244	android/content/res/AssetManager:loadResourceValue	(ISLandroid/util/TypedValue;Z)I
        //     15: istore 4
        //     17: iload 4
        //     19: iflt +48 -> 67
        //     22: aload_3
        //     23: getfield 226	android/util/TypedValue:type	I
        //     26: iconst_3
        //     27: if_icmpne +24 -> 51
        //     30: aload_0
        //     31: getfield 74	android/content/res/AssetManager:mStringBlocks	[Landroid/content/res/StringBlock;
        //     34: iload 4
        //     36: aaload
        //     37: aload_3
        //     38: getfield 229	android/util/TypedValue:data	I
        //     41: invokevirtual 220	android/content/res/StringBlock:get	(I)Ljava/lang/CharSequence;
        //     44: astore 5
        //     46: aload_0
        //     47: monitorexit
        //     48: goto +24 -> 72
        //     51: aload_3
        //     52: invokevirtual 233	android/util/TypedValue:coerceToString	()Ljava/lang/CharSequence;
        //     55: astore 5
        //     57: aload_0
        //     58: monitorexit
        //     59: goto +13 -> 72
        //     62: astore_2
        //     63: aload_0
        //     64: monitorexit
        //     65: aload_2
        //     66: athrow
        //     67: aload_0
        //     68: monitorexit
        //     69: aconst_null
        //     70: astore 5
        //     72: aload 5
        //     74: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	65	62	finally
        //     67	69	62	finally
    }

    final CharSequence[] getResourceTextArray(int paramInt)
    {
        int[] arrayOfInt = getArrayStringInfo(paramInt);
        int i = arrayOfInt.length;
        CharSequence[] arrayOfCharSequence = new CharSequence[i / 2];
        int j = 0;
        int k = 0;
        if (j < i)
        {
            int m = arrayOfInt[j];
            int n = arrayOfInt[(j + 1)];
            if (n >= 0);
            for (CharSequence localCharSequence = this.mStringBlocks[m].get(n); ; localCharSequence = null)
            {
                arrayOfCharSequence[k] = localCharSequence;
                j += 2;
                k++;
                break;
            }
        }
        return arrayOfCharSequence;
    }

    final native String getResourceTypeName(int paramInt);

    final boolean getResourceValue(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean)
    {
        boolean bool = true;
        int i = loadResourceValue(paramInt1, (short)paramInt2, paramTypedValue, paramBoolean);
        if (i >= 0)
            if (paramTypedValue.type == 3);
        while (true)
        {
            return bool;
            paramTypedValue.string = this.mStringBlocks[i].get(paramTypedValue.data);
            continue;
            bool = false;
        }
    }

    final boolean getThemeValue(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean)
    {
        boolean bool = true;
        int i = loadThemeAttributeValue(paramInt1, paramInt2, paramTypedValue, paramBoolean);
        if (i >= 0)
            if (paramTypedValue.type == 3);
        while (true)
        {
            return bool;
            StringBlock[] arrayOfStringBlock = this.mStringBlocks;
            if (arrayOfStringBlock == null)
            {
                ensureStringBlocks();
                arrayOfStringBlock = this.mStringBlocks;
            }
            paramTypedValue.string = arrayOfStringBlock[i].get(paramTypedValue.data);
            continue;
            bool = false;
        }
    }

    public final native boolean isUpToDate();

    public final native String[] list(String paramString)
        throws IOException;

    final void makeStringBlocks(boolean paramBoolean)
    {
        int i;
        int k;
        if (paramBoolean)
        {
            i = sSystem.mStringBlocks.length;
            int j = getStringBlockCount();
            this.mStringBlocks = new StringBlock[j];
            k = 0;
            label28: if (k >= j)
                return;
            if (k >= i)
                break label67;
            this.mStringBlocks[k] = sSystem.mStringBlocks[k];
        }
        while (true)
        {
            k++;
            break label28;
            i = 0;
            break;
            label67: this.mStringBlocks[k] = new StringBlock(getNativeStringBlock(k), true);
        }
    }

    public final InputStream open(String paramString)
        throws IOException
    {
        return open(paramString, 2);
    }

    // ERROR //
    public final InputStream open(String paramString, int paramInt)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 78	android/content/res/AssetManager:mOpen	Z
        //     6: ifne +18 -> 24
        //     9: new 191	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 193
        //     15: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_3
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_3
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iload_2
        //     27: invokespecial 280	android/content/res/AssetManager:openAsset	(Ljava/lang/String;I)I
        //     30: istore 4
        //     32: iload 4
        //     34: ifeq +30 -> 64
        //     37: new 8	android/content/res/AssetManager$AssetInputStream
        //     40: dup
        //     41: aload_0
        //     42: iload 4
        //     44: aconst_null
        //     45: invokespecial 283	android/content/res/AssetManager$AssetInputStream:<init>	(Landroid/content/res/AssetManager;ILandroid/content/res/AssetManager$1;)V
        //     48: astore 5
        //     50: aload_0
        //     51: aload 5
        //     53: invokevirtual 188	java/lang/Object:hashCode	()I
        //     56: invokespecial 200	android/content/res/AssetManager:incRefsLocked	(I)V
        //     59: aload_0
        //     60: monitorexit
        //     61: aload 5
        //     63: areturn
        //     64: aload_0
        //     65: monitorexit
        //     66: new 285	java/io/FileNotFoundException
        //     69: dup
        //     70: new 287	java/lang/StringBuilder
        //     73: dup
        //     74: invokespecial 288	java/lang/StringBuilder:<init>	()V
        //     77: ldc_w 290
        //     80: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     83: aload_1
        //     84: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     87: invokevirtual 297	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     90: invokespecial 298	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     93: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	66	19	finally
    }

    // ERROR //
    public final AssetFileDescriptor openFd(String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 78	android/content/res/AssetManager:mOpen	Z
        //     6: ifne +18 -> 24
        //     9: new 191	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 193
        //     15: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_2
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_2
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: aload_0
        //     27: getfield 72	android/content/res/AssetManager:mOffsets	[J
        //     30: invokespecial 302	android/content/res/AssetManager:openAssetFd	(Ljava/lang/String;[J)Landroid/os/ParcelFileDescriptor;
        //     33: astore_3
        //     34: aload_3
        //     35: ifnull +30 -> 65
        //     38: new 304	android/content/res/AssetFileDescriptor
        //     41: dup
        //     42: aload_3
        //     43: aload_0
        //     44: getfield 72	android/content/res/AssetManager:mOffsets	[J
        //     47: iconst_0
        //     48: laload
        //     49: aload_0
        //     50: getfield 72	android/content/res/AssetManager:mOffsets	[J
        //     53: iconst_1
        //     54: laload
        //     55: invokespecial 307	android/content/res/AssetFileDescriptor:<init>	(Landroid/os/ParcelFileDescriptor;JJ)V
        //     58: astore 4
        //     60: aload_0
        //     61: monitorexit
        //     62: aload 4
        //     64: areturn
        //     65: aload_0
        //     66: monitorexit
        //     67: new 285	java/io/FileNotFoundException
        //     70: dup
        //     71: new 287	java/lang/StringBuilder
        //     74: dup
        //     75: invokespecial 288	java/lang/StringBuilder:<init>	()V
        //     78: ldc_w 290
        //     81: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     84: aload_1
        //     85: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     88: invokevirtual 297	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     91: invokespecial 298	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     94: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	67	19	finally
    }

    public final InputStream openNonAsset(int paramInt, String paramString)
        throws IOException
    {
        return openNonAsset(paramInt, paramString, 2);
    }

    // ERROR //
    public final InputStream openNonAsset(int paramInt1, String paramString, int paramInt2)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 78	android/content/res/AssetManager:mOpen	Z
        //     6: ifne +20 -> 26
        //     9: new 191	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 193
        //     15: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore 4
        //     21: aload_0
        //     22: monitorexit
        //     23: aload 4
        //     25: athrow
        //     26: aload_0
        //     27: iload_1
        //     28: aload_2
        //     29: iload_3
        //     30: invokespecial 314	android/content/res/AssetManager:openNonAssetNative	(ILjava/lang/String;I)I
        //     33: istore 5
        //     35: iload 5
        //     37: ifeq +30 -> 67
        //     40: new 8	android/content/res/AssetManager$AssetInputStream
        //     43: dup
        //     44: aload_0
        //     45: iload 5
        //     47: aconst_null
        //     48: invokespecial 283	android/content/res/AssetManager$AssetInputStream:<init>	(Landroid/content/res/AssetManager;ILandroid/content/res/AssetManager$1;)V
        //     51: astore 6
        //     53: aload_0
        //     54: aload 6
        //     56: invokevirtual 188	java/lang/Object:hashCode	()I
        //     59: invokespecial 200	android/content/res/AssetManager:incRefsLocked	(I)V
        //     62: aload_0
        //     63: monitorexit
        //     64: aload 6
        //     66: areturn
        //     67: aload_0
        //     68: monitorexit
        //     69: new 285	java/io/FileNotFoundException
        //     72: dup
        //     73: new 287	java/lang/StringBuilder
        //     76: dup
        //     77: invokespecial 288	java/lang/StringBuilder:<init>	()V
        //     80: ldc_w 316
        //     83: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     86: aload_2
        //     87: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     90: invokevirtual 297	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     93: invokespecial 298	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     96: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	23	19	finally
        //     26	69	19	finally
    }

    public final InputStream openNonAsset(String paramString)
        throws IOException
    {
        return openNonAsset(0, paramString, 2);
    }

    public final InputStream openNonAsset(String paramString, int paramInt)
        throws IOException
    {
        return openNonAsset(0, paramString, paramInt);
    }

    // ERROR //
    public final AssetFileDescriptor openNonAssetFd(int paramInt, String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 78	android/content/res/AssetManager:mOpen	Z
        //     6: ifne +18 -> 24
        //     9: new 191	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 193
        //     15: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_3
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_3
        //     23: athrow
        //     24: aload_0
        //     25: iload_1
        //     26: aload_2
        //     27: aload_0
        //     28: getfield 72	android/content/res/AssetManager:mOffsets	[J
        //     31: invokespecial 320	android/content/res/AssetManager:openNonAssetFdNative	(ILjava/lang/String;[J)Landroid/os/ParcelFileDescriptor;
        //     34: astore 4
        //     36: aload 4
        //     38: ifnull +31 -> 69
        //     41: new 304	android/content/res/AssetFileDescriptor
        //     44: dup
        //     45: aload 4
        //     47: aload_0
        //     48: getfield 72	android/content/res/AssetManager:mOffsets	[J
        //     51: iconst_0
        //     52: laload
        //     53: aload_0
        //     54: getfield 72	android/content/res/AssetManager:mOffsets	[J
        //     57: iconst_1
        //     58: laload
        //     59: invokespecial 307	android/content/res/AssetFileDescriptor:<init>	(Landroid/os/ParcelFileDescriptor;JJ)V
        //     62: astore 5
        //     64: aload_0
        //     65: monitorexit
        //     66: aload 5
        //     68: areturn
        //     69: aload_0
        //     70: monitorexit
        //     71: new 285	java/io/FileNotFoundException
        //     74: dup
        //     75: new 287	java/lang/StringBuilder
        //     78: dup
        //     79: invokespecial 288	java/lang/StringBuilder:<init>	()V
        //     82: ldc_w 316
        //     85: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     88: aload_2
        //     89: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     92: invokevirtual 297	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     95: invokespecial 298	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     98: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	71	19	finally
    }

    public final AssetFileDescriptor openNonAssetFd(String paramString)
        throws IOException
    {
        return openNonAssetFd(0, paramString);
    }

    // ERROR //
    final XmlBlock openXmlBlockAsset(int paramInt, String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 78	android/content/res/AssetManager:mOpen	Z
        //     6: ifne +18 -> 24
        //     9: new 191	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 193
        //     15: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_3
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_3
        //     23: athrow
        //     24: aload_0
        //     25: iload_1
        //     26: aload_2
        //     27: invokespecial 326	android/content/res/AssetManager:openXmlAssetNative	(ILjava/lang/String;)I
        //     30: istore 4
        //     32: iload 4
        //     34: ifeq +29 -> 63
        //     37: new 328	android/content/res/XmlBlock
        //     40: dup
        //     41: aload_0
        //     42: iload 4
        //     44: invokespecial 330	android/content/res/XmlBlock:<init>	(Landroid/content/res/AssetManager;I)V
        //     47: astore 5
        //     49: aload_0
        //     50: aload 5
        //     52: invokevirtual 188	java/lang/Object:hashCode	()I
        //     55: invokespecial 200	android/content/res/AssetManager:incRefsLocked	(I)V
        //     58: aload_0
        //     59: monitorexit
        //     60: aload 5
        //     62: areturn
        //     63: aload_0
        //     64: monitorexit
        //     65: new 285	java/io/FileNotFoundException
        //     68: dup
        //     69: new 287	java/lang/StringBuilder
        //     72: dup
        //     73: invokespecial 288	java/lang/StringBuilder:<init>	()V
        //     76: ldc_w 332
        //     79: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     82: aload_2
        //     83: invokevirtual 294	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     86: invokevirtual 297	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     89: invokespecial 298	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     92: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	65	19	finally
    }

    final XmlBlock openXmlBlockAsset(String paramString)
        throws IOException
    {
        return openXmlBlockAsset(0, paramString);
    }

    public final XmlResourceParser openXmlResourceParser(int paramInt, String paramString)
        throws IOException
    {
        XmlBlock localXmlBlock = openXmlBlockAsset(paramInt, paramString);
        XmlResourceParser localXmlResourceParser = localXmlBlock.newParser();
        localXmlBlock.close();
        return localXmlResourceParser;
    }

    public final XmlResourceParser openXmlResourceParser(String paramString)
        throws IOException
    {
        return openXmlResourceParser(0, paramString);
    }

    final void releaseTheme(int paramInt)
    {
        try
        {
            deleteTheme(paramInt);
            decRefsLocked(paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    final native int retrieveArray(int paramInt, int[] paramArrayOfInt);

    final native boolean retrieveAttributes(int paramInt, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3);

    public final native void setConfiguration(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, int paramInt15, int paramInt16);

    public final native void setLocale(String paramString);

    void xmlBlockGone(int paramInt)
    {
        try
        {
            decRefsLocked(paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final class AssetInputStream extends InputStream
    {
        private int mAsset;
        private long mLength;
        private long mMarkPos;

        private AssetInputStream(int arg2)
        {
            int i;
            this.mAsset = i;
            this.mLength = AssetManager.this.getAssetLength(i);
        }

        public final int available()
            throws IOException
        {
            long l = AssetManager.this.getAssetRemainingLength(this.mAsset);
            if (l > 2147483647L);
            for (int i = 2147483647; ; i = (int)l)
                return i;
        }

        public final void close()
            throws IOException
        {
            synchronized (AssetManager.this)
            {
                if (this.mAsset != 0)
                {
                    AssetManager.this.destroyAsset(this.mAsset);
                    this.mAsset = 0;
                    AssetManager.this.decRefsLocked(hashCode());
                }
                return;
            }
        }

        protected void finalize()
            throws Throwable
        {
            close();
        }

        public final int getAssetInt()
        {
            return this.mAsset;
        }

        public final void mark(int paramInt)
        {
            this.mMarkPos = AssetManager.this.seekAsset(this.mAsset, 0L, 0);
        }

        public final boolean markSupported()
        {
            return true;
        }

        public final int read()
            throws IOException
        {
            return AssetManager.this.readAssetChar(this.mAsset);
        }

        public final int read(byte[] paramArrayOfByte)
            throws IOException
        {
            return AssetManager.this.readAsset(this.mAsset, paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            return AssetManager.this.readAsset(this.mAsset, paramArrayOfByte, paramInt1, paramInt2);
        }

        public final void reset()
            throws IOException
        {
            AssetManager.this.seekAsset(this.mAsset, this.mMarkPos, -1);
        }

        public final long skip(long paramLong)
            throws IOException
        {
            long l = AssetManager.this.seekAsset(this.mAsset, 0L, 0);
            if (l + paramLong > this.mLength)
                paramLong = this.mLength - l;
            if (paramLong > 0L)
                AssetManager.this.seekAsset(this.mAsset, paramLong, 0);
            return paramLong;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.AssetManager
 * JD-Core Version:        0.6.2
 */