package android.content.res;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class AssetFileDescriptor
    implements Parcelable
{
    public static final Parcelable.Creator<AssetFileDescriptor> CREATOR = new Parcelable.Creator()
    {
        public AssetFileDescriptor createFromParcel(Parcel paramAnonymousParcel)
        {
            return new AssetFileDescriptor(paramAnonymousParcel);
        }

        public AssetFileDescriptor[] newArray(int paramAnonymousInt)
        {
            return new AssetFileDescriptor[paramAnonymousInt];
        }
    };
    public static final long UNKNOWN_LENGTH = -1L;
    private final ParcelFileDescriptor mFd;
    private final long mLength;
    private final long mStartOffset;

    AssetFileDescriptor(Parcel paramParcel)
    {
        this.mFd = ((ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel));
        this.mStartOffset = paramParcel.readLong();
        this.mLength = paramParcel.readLong();
    }

    public AssetFileDescriptor(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, long paramLong2)
    {
        if (paramParcelFileDescriptor == null)
            throw new IllegalArgumentException("fd must not be null");
        if ((paramLong2 < 0L) && (paramLong1 != 0L))
            throw new IllegalArgumentException("startOffset must be 0 when using UNKNOWN_LENGTH");
        this.mFd = paramParcelFileDescriptor;
        this.mStartOffset = paramLong1;
        this.mLength = paramLong2;
    }

    public void close()
        throws IOException
    {
        this.mFd.close();
    }

    public FileInputStream createInputStream()
        throws IOException
    {
        if (this.mLength < 0L);
        for (Object localObject = new ParcelFileDescriptor.AutoCloseInputStream(this.mFd); ; localObject = new AutoCloseInputStream(this))
            return localObject;
    }

    public FileOutputStream createOutputStream()
        throws IOException
    {
        if (this.mLength < 0L);
        for (Object localObject = new ParcelFileDescriptor.AutoCloseOutputStream(this.mFd); ; localObject = new AutoCloseOutputStream(this))
            return localObject;
    }

    public int describeContents()
    {
        return this.mFd.describeContents();
    }

    public long getDeclaredLength()
    {
        return this.mLength;
    }

    public FileDescriptor getFileDescriptor()
    {
        return this.mFd.getFileDescriptor();
    }

    public long getLength()
    {
        long l;
        if (this.mLength >= 0L)
            l = this.mLength;
        while (true)
        {
            return l;
            l = this.mFd.getStatSize();
            if (l < 0L)
                l = -1L;
        }
    }

    public ParcelFileDescriptor getParcelFileDescriptor()
    {
        return this.mFd;
    }

    public long getStartOffset()
    {
        return this.mStartOffset;
    }

    public String toString()
    {
        return "{AssetFileDescriptor: " + this.mFd + " start=" + this.mStartOffset + " len=" + this.mLength + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        this.mFd.writeToParcel(paramParcel, paramInt);
        paramParcel.writeLong(this.mStartOffset);
        paramParcel.writeLong(this.mLength);
    }

    public static class AutoCloseOutputStream extends ParcelFileDescriptor.AutoCloseOutputStream
    {
        private long mRemaining;

        public AutoCloseOutputStream(AssetFileDescriptor paramAssetFileDescriptor)
            throws IOException
        {
            super();
            if (paramAssetFileDescriptor.getParcelFileDescriptor().seekTo(paramAssetFileDescriptor.getStartOffset()) < 0L)
                throw new IOException("Unable to seek");
            this.mRemaining = ((int)paramAssetFileDescriptor.getLength());
        }

        public void write(int paramInt)
            throws IOException
        {
            if (this.mRemaining >= 0L)
                if (this.mRemaining != 0L);
            while (true)
            {
                return;
                super.write(paramInt);
                this.mRemaining -= 1L;
                continue;
                super.write(paramInt);
            }
        }

        public void write(byte[] paramArrayOfByte)
            throws IOException
        {
            if (this.mRemaining >= 0L)
                if (this.mRemaining != 0L);
            while (true)
            {
                return;
                int i = paramArrayOfByte.length;
                if (i > this.mRemaining)
                    i = (int)this.mRemaining;
                super.write(paramArrayOfByte);
                this.mRemaining -= i;
                continue;
                super.write(paramArrayOfByte);
            }
        }

        public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            if (this.mRemaining >= 0L)
                if (this.mRemaining != 0L);
            while (true)
            {
                return;
                if (paramInt2 > this.mRemaining)
                    paramInt2 = (int)this.mRemaining;
                super.write(paramArrayOfByte, paramInt1, paramInt2);
                this.mRemaining -= paramInt2;
                continue;
                super.write(paramArrayOfByte, paramInt1, paramInt2);
            }
        }
    }

    public static class AutoCloseInputStream extends ParcelFileDescriptor.AutoCloseInputStream
    {
        private long mRemaining;

        public AutoCloseInputStream(AssetFileDescriptor paramAssetFileDescriptor)
            throws IOException
        {
            super();
            super.skip(paramAssetFileDescriptor.getStartOffset());
            this.mRemaining = ((int)paramAssetFileDescriptor.getLength());
        }

        public int available()
            throws IOException
        {
            int i;
            if (this.mRemaining >= 0L)
                if (this.mRemaining < 2147483647L)
                    i = (int)this.mRemaining;
            while (true)
            {
                return i;
                i = 2147483647;
                continue;
                i = super.available();
            }
        }

        public void mark(int paramInt)
        {
            if (this.mRemaining >= 0L);
            while (true)
            {
                return;
                super.mark(paramInt);
            }
        }

        public boolean markSupported()
        {
            if (this.mRemaining >= 0L);
            for (boolean bool = false; ; bool = super.markSupported())
                return bool;
        }

        public int read()
            throws IOException
        {
            int i = -1;
            byte[] arrayOfByte = new byte[1];
            if (read(arrayOfByte, 0, 1) == i);
            while (true)
            {
                return i;
                i = 0xFF & arrayOfByte[0];
            }
        }

        public int read(byte[] paramArrayOfByte)
            throws IOException
        {
            return read(paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            int i;
            if (this.mRemaining >= 0L)
                if (this.mRemaining == 0L)
                    i = -1;
            while (true)
            {
                return i;
                if (paramInt2 > this.mRemaining)
                    paramInt2 = (int)this.mRemaining;
                i = super.read(paramArrayOfByte, paramInt1, paramInt2);
                if (i >= 0)
                {
                    this.mRemaining -= i;
                    continue;
                    i = super.read(paramArrayOfByte, paramInt1, paramInt2);
                }
            }
        }

        /** @deprecated */
        public void reset()
            throws IOException
        {
            try
            {
                long l = this.mRemaining;
                if (l >= 0L);
                while (true)
                {
                    return;
                    super.reset();
                }
            }
            finally
            {
            }
        }

        public long skip(long paramLong)
            throws IOException
        {
            long l;
            if (this.mRemaining >= 0L)
                if (this.mRemaining == 0L)
                    l = -1L;
            while (true)
            {
                return l;
                if (paramLong > this.mRemaining)
                    paramLong = this.mRemaining;
                l = super.skip(paramLong);
                if (l >= 0L)
                {
                    this.mRemaining -= l;
                    continue;
                    l = super.skip(paramLong);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.res.AssetFileDescriptor
 * JD-Core Version:        0.6.2
 */