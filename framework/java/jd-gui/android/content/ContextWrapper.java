package android.content;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ContextWrapper extends Context
{
    Context mBase;

    public ContextWrapper(Context paramContext)
    {
        this.mBase = paramContext;
    }

    protected void attachBaseContext(Context paramContext)
    {
        if (this.mBase != null)
            throw new IllegalStateException("Base context already set");
        this.mBase = paramContext;
    }

    public boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt)
    {
        return this.mBase.bindService(paramIntent, paramServiceConnection, paramInt);
    }

    public boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt1, int paramInt2)
    {
        return this.mBase.bindService(paramIntent, paramServiceConnection, paramInt1, paramInt2);
    }

    public int checkCallingOrSelfPermission(String paramString)
    {
        return this.mBase.checkCallingOrSelfPermission(paramString);
    }

    public int checkCallingOrSelfUriPermission(Uri paramUri, int paramInt)
    {
        return this.mBase.checkCallingOrSelfUriPermission(paramUri, paramInt);
    }

    public int checkCallingPermission(String paramString)
    {
        return this.mBase.checkCallingPermission(paramString);
    }

    public int checkCallingUriPermission(Uri paramUri, int paramInt)
    {
        return this.mBase.checkCallingUriPermission(paramUri, paramInt);
    }

    public int checkPermission(String paramString, int paramInt1, int paramInt2)
    {
        return this.mBase.checkPermission(paramString, paramInt1, paramInt2);
    }

    public int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3)
    {
        return this.mBase.checkUriPermission(paramUri, paramInt1, paramInt2, paramInt3);
    }

    public int checkUriPermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3)
    {
        return this.mBase.checkUriPermission(paramUri, paramString1, paramString2, paramInt1, paramInt2, paramInt3);
    }

    public void clearWallpaper()
        throws IOException
    {
        this.mBase.clearWallpaper();
    }

    public Context createPackageContext(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        return this.mBase.createPackageContext(paramString, paramInt);
    }

    public String[] databaseList()
    {
        return this.mBase.databaseList();
    }

    public boolean deleteDatabase(String paramString)
    {
        return this.mBase.deleteDatabase(paramString);
    }

    public boolean deleteFile(String paramString)
    {
        return this.mBase.deleteFile(paramString);
    }

    public void enforceCallingOrSelfPermission(String paramString1, String paramString2)
    {
        this.mBase.enforceCallingOrSelfPermission(paramString1, paramString2);
    }

    public void enforceCallingOrSelfUriPermission(Uri paramUri, int paramInt, String paramString)
    {
        this.mBase.enforceCallingOrSelfUriPermission(paramUri, paramInt, paramString);
    }

    public void enforceCallingPermission(String paramString1, String paramString2)
    {
        this.mBase.enforceCallingPermission(paramString1, paramString2);
    }

    public void enforceCallingUriPermission(Uri paramUri, int paramInt, String paramString)
    {
        this.mBase.enforceCallingUriPermission(paramUri, paramInt, paramString);
    }

    public void enforcePermission(String paramString1, int paramInt1, int paramInt2, String paramString2)
    {
        this.mBase.enforcePermission(paramString1, paramInt1, paramInt2, paramString2);
    }

    public void enforceUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, String paramString)
    {
        this.mBase.enforceUriPermission(paramUri, paramInt1, paramInt2, paramInt3, paramString);
    }

    public void enforceUriPermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
    {
        this.mBase.enforceUriPermission(paramUri, paramString1, paramString2, paramInt1, paramInt2, paramInt3, paramString3);
    }

    public String[] fileList()
    {
        return this.mBase.fileList();
    }

    public Context getApplicationContext()
    {
        return this.mBase.getApplicationContext();
    }

    public ApplicationInfo getApplicationInfo()
    {
        return this.mBase.getApplicationInfo();
    }

    public AssetManager getAssets()
    {
        return this.mBase.getAssets();
    }

    public Context getBaseContext()
    {
        return this.mBase;
    }

    public File getCacheDir()
    {
        return this.mBase.getCacheDir();
    }

    public ClassLoader getClassLoader()
    {
        return this.mBase.getClassLoader();
    }

    public ContentResolver getContentResolver()
    {
        return this.mBase.getContentResolver();
    }

    public File getDatabasePath(String paramString)
    {
        return this.mBase.getDatabasePath(paramString);
    }

    public File getDir(String paramString, int paramInt)
    {
        return this.mBase.getDir(paramString, paramInt);
    }

    public File getExternalCacheDir()
    {
        return this.mBase.getExternalCacheDir();
    }

    public File getExternalFilesDir(String paramString)
    {
        return this.mBase.getExternalFilesDir(paramString);
    }

    public File getFileStreamPath(String paramString)
    {
        return this.mBase.getFileStreamPath(paramString);
    }

    public File getFilesDir()
    {
        return this.mBase.getFilesDir();
    }

    public Looper getMainLooper()
    {
        return this.mBase.getMainLooper();
    }

    public File getObbDir()
    {
        return this.mBase.getObbDir();
    }

    public String getPackageCodePath()
    {
        return this.mBase.getPackageCodePath();
    }

    public PackageManager getPackageManager()
    {
        return this.mBase.getPackageManager();
    }

    public String getPackageName()
    {
        return this.mBase.getPackageName();
    }

    public String getPackageResourcePath()
    {
        return this.mBase.getPackageResourcePath();
    }

    public Resources getResources()
    {
        return this.mBase.getResources();
    }

    public SharedPreferences getSharedPreferences(String paramString, int paramInt)
    {
        return this.mBase.getSharedPreferences(paramString, paramInt);
    }

    public File getSharedPrefsFile(String paramString)
    {
        return this.mBase.getSharedPrefsFile(paramString);
    }

    public Object getSystemService(String paramString)
    {
        return this.mBase.getSystemService(paramString);
    }

    public Resources.Theme getTheme()
    {
        return this.mBase.getTheme();
    }

    public int getThemeResId()
    {
        return this.mBase.getThemeResId();
    }

    public Drawable getWallpaper()
    {
        return this.mBase.getWallpaper();
    }

    public int getWallpaperDesiredMinimumHeight()
    {
        return this.mBase.getWallpaperDesiredMinimumHeight();
    }

    public int getWallpaperDesiredMinimumWidth()
    {
        return this.mBase.getWallpaperDesiredMinimumWidth();
    }

    public void grantUriPermission(String paramString, Uri paramUri, int paramInt)
    {
        this.mBase.grantUriPermission(paramString, paramUri, paramInt);
    }

    public boolean isRestricted()
    {
        return this.mBase.isRestricted();
    }

    public FileInputStream openFileInput(String paramString)
        throws FileNotFoundException
    {
        return this.mBase.openFileInput(paramString);
    }

    public FileOutputStream openFileOutput(String paramString, int paramInt)
        throws FileNotFoundException
    {
        return this.mBase.openFileOutput(paramString, paramInt);
    }

    public SQLiteDatabase openOrCreateDatabase(String paramString, int paramInt, SQLiteDatabase.CursorFactory paramCursorFactory)
    {
        return this.mBase.openOrCreateDatabase(paramString, paramInt, paramCursorFactory);
    }

    public SQLiteDatabase openOrCreateDatabase(String paramString, int paramInt, SQLiteDatabase.CursorFactory paramCursorFactory, DatabaseErrorHandler paramDatabaseErrorHandler)
    {
        return this.mBase.openOrCreateDatabase(paramString, paramInt, paramCursorFactory, paramDatabaseErrorHandler);
    }

    public Drawable peekWallpaper()
    {
        return this.mBase.peekWallpaper();
    }

    public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter)
    {
        return this.mBase.registerReceiver(paramBroadcastReceiver, paramIntentFilter);
    }

    public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler)
    {
        return this.mBase.registerReceiver(paramBroadcastReceiver, paramIntentFilter, paramString, paramHandler);
    }

    public void removeStickyBroadcast(Intent paramIntent)
    {
        this.mBase.removeStickyBroadcast(paramIntent);
    }

    public void revokeUriPermission(Uri paramUri, int paramInt)
    {
        this.mBase.revokeUriPermission(paramUri, paramInt);
    }

    public void sendBroadcast(Intent paramIntent)
    {
        this.mBase.sendBroadcast(paramIntent);
    }

    public void sendBroadcast(Intent paramIntent, int paramInt)
    {
        this.mBase.sendBroadcast(paramIntent, paramInt);
    }

    public void sendBroadcast(Intent paramIntent, String paramString)
    {
        this.mBase.sendBroadcast(paramIntent, paramString);
    }

    public void sendOrderedBroadcast(Intent paramIntent, String paramString)
    {
        this.mBase.sendOrderedBroadcast(paramIntent, paramString);
    }

    public void sendOrderedBroadcast(Intent paramIntent, String paramString1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString2, Bundle paramBundle)
    {
        this.mBase.sendOrderedBroadcast(paramIntent, paramString1, paramBroadcastReceiver, paramHandler, paramInt, paramString2, paramBundle);
    }

    public void sendStickyBroadcast(Intent paramIntent)
    {
        this.mBase.sendStickyBroadcast(paramIntent);
    }

    public void sendStickyOrderedBroadcast(Intent paramIntent, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString, Bundle paramBundle)
    {
        this.mBase.sendStickyOrderedBroadcast(paramIntent, paramBroadcastReceiver, paramHandler, paramInt, paramString, paramBundle);
    }

    public void setTheme(int paramInt)
    {
        this.mBase.setTheme(paramInt);
    }

    public void setWallpaper(Bitmap paramBitmap)
        throws IOException
    {
        this.mBase.setWallpaper(paramBitmap);
    }

    public void setWallpaper(InputStream paramInputStream)
        throws IOException
    {
        this.mBase.setWallpaper(paramInputStream);
    }

    public void startActivities(Intent[] paramArrayOfIntent)
    {
        this.mBase.startActivities(paramArrayOfIntent);
    }

    public void startActivities(Intent[] paramArrayOfIntent, Bundle paramBundle)
    {
        this.mBase.startActivities(paramArrayOfIntent, paramBundle);
    }

    public void startActivity(Intent paramIntent)
    {
        this.mBase.startActivity(paramIntent);
    }

    public void startActivity(Intent paramIntent, Bundle paramBundle)
    {
        this.mBase.startActivity(paramIntent, paramBundle);
    }

    public boolean startInstrumentation(ComponentName paramComponentName, String paramString, Bundle paramBundle)
    {
        return this.mBase.startInstrumentation(paramComponentName, paramString, paramBundle);
    }

    public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3)
        throws IntentSender.SendIntentException
    {
        this.mBase.startIntentSender(paramIntentSender, paramIntent, paramInt1, paramInt2, paramInt3);
    }

    public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
        throws IntentSender.SendIntentException
    {
        this.mBase.startIntentSender(paramIntentSender, paramIntent, paramInt1, paramInt2, paramInt3, paramBundle);
    }

    public ComponentName startService(Intent paramIntent)
    {
        return this.mBase.startService(paramIntent);
    }

    public boolean stopService(Intent paramIntent)
    {
        return this.mBase.stopService(paramIntent);
    }

    public void unbindService(ServiceConnection paramServiceConnection)
    {
        this.mBase.unbindService(paramServiceConnection);
    }

    public void unregisterReceiver(BroadcastReceiver paramBroadcastReceiver)
    {
        this.mBase.unregisterReceiver(paramBroadcastReceiver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContextWrapper
 * JD-Core Version:        0.6.2
 */