package android.content;

import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.QueuedWork;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.concurrent.ExecutorService;

public abstract class BroadcastReceiver
{
    private boolean mDebugUnregister;
    private PendingResult mPendingResult;

    public final void abortBroadcast()
    {
        checkSynchronousHint();
        this.mPendingResult.mAbortBroadcast = true;
    }

    void checkSynchronousHint()
    {
        if (this.mPendingResult == null)
            throw new IllegalStateException("Call while result is not pending");
        if ((this.mPendingResult.mOrderedHint) || (this.mPendingResult.mInitialStickyHint));
        while (true)
        {
            return;
            RuntimeException localRuntimeException = new RuntimeException("BroadcastReceiver trying to return result during a non-ordered broadcast");
            localRuntimeException.fillInStackTrace();
            Log.e("BroadcastReceiver", localRuntimeException.getMessage(), localRuntimeException);
        }
    }

    public final void clearAbortBroadcast()
    {
        if (this.mPendingResult != null)
            this.mPendingResult.mAbortBroadcast = false;
    }

    public final boolean getAbortBroadcast()
    {
        if (this.mPendingResult != null);
        for (boolean bool = this.mPendingResult.mAbortBroadcast; ; bool = false)
            return bool;
    }

    public final boolean getDebugUnregister()
    {
        return this.mDebugUnregister;
    }

    public final PendingResult getPendingResult()
    {
        return this.mPendingResult;
    }

    public final int getResultCode()
    {
        if (this.mPendingResult != null);
        for (int i = this.mPendingResult.mResultCode; ; i = 0)
            return i;
    }

    public final String getResultData()
    {
        if (this.mPendingResult != null);
        for (String str = this.mPendingResult.mResultData; ; str = null)
            return str;
    }

    public final Bundle getResultExtras(boolean paramBoolean)
    {
        Bundle localBundle;
        if (this.mPendingResult == null)
            localBundle = null;
        while (true)
        {
            return localBundle;
            localBundle = this.mPendingResult.mResultExtras;
            if ((paramBoolean) && (localBundle == null))
            {
                PendingResult localPendingResult = this.mPendingResult;
                localBundle = new Bundle();
                localPendingResult.mResultExtras = localBundle;
            }
        }
    }

    public final PendingResult goAsync()
    {
        PendingResult localPendingResult = this.mPendingResult;
        this.mPendingResult = null;
        return localPendingResult;
    }

    public final boolean isInitialStickyBroadcast()
    {
        if (this.mPendingResult != null);
        for (boolean bool = this.mPendingResult.mInitialStickyHint; ; bool = false)
            return bool;
    }

    public final boolean isOrderedBroadcast()
    {
        if (this.mPendingResult != null);
        for (boolean bool = this.mPendingResult.mOrderedHint; ; bool = false)
            return bool;
    }

    public abstract void onReceive(Context paramContext, Intent paramIntent);

    public IBinder peekService(Context paramContext, Intent paramIntent)
    {
        IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
        Object localObject = null;
        try
        {
            paramIntent.setAllowFds(false);
            IBinder localIBinder = localIActivityManager.peekService(paramIntent, paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver()));
            localObject = localIBinder;
            label33: return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label33;
        }
    }

    public final void setDebugUnregister(boolean paramBoolean)
    {
        this.mDebugUnregister = paramBoolean;
    }

    public final void setOrderedHint(boolean paramBoolean)
    {
    }

    public final void setPendingResult(PendingResult paramPendingResult)
    {
        this.mPendingResult = paramPendingResult;
    }

    public final void setResult(int paramInt, String paramString, Bundle paramBundle)
    {
        checkSynchronousHint();
        this.mPendingResult.mResultCode = paramInt;
        this.mPendingResult.mResultData = paramString;
        this.mPendingResult.mResultExtras = paramBundle;
    }

    public final void setResultCode(int paramInt)
    {
        checkSynchronousHint();
        this.mPendingResult.mResultCode = paramInt;
    }

    public final void setResultData(String paramString)
    {
        checkSynchronousHint();
        this.mPendingResult.mResultData = paramString;
    }

    public final void setResultExtras(Bundle paramBundle)
    {
        checkSynchronousHint();
        this.mPendingResult.mResultExtras = paramBundle;
    }

    public static class PendingResult
    {
        public static final int TYPE_COMPONENT = 0;
        public static final int TYPE_REGISTERED = 1;
        public static final int TYPE_UNREGISTERED = 2;
        boolean mAbortBroadcast;
        boolean mFinished;
        final boolean mInitialStickyHint;
        final boolean mOrderedHint;
        int mResultCode;
        String mResultData;
        Bundle mResultExtras;
        final IBinder mToken;
        final int mType;

        public PendingResult(int paramInt1, String paramString, Bundle paramBundle, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, IBinder paramIBinder)
        {
            this.mResultCode = paramInt1;
            this.mResultData = paramString;
            this.mResultExtras = paramBundle;
            this.mType = paramInt2;
            this.mOrderedHint = paramBoolean1;
            this.mInitialStickyHint = paramBoolean2;
            this.mToken = paramIBinder;
        }

        public final void abortBroadcast()
        {
            checkSynchronousHint();
            this.mAbortBroadcast = true;
        }

        void checkSynchronousHint()
        {
            if ((this.mOrderedHint) || (this.mInitialStickyHint));
            while (true)
            {
                return;
                RuntimeException localRuntimeException = new RuntimeException("BroadcastReceiver trying to return result during a non-ordered broadcast");
                localRuntimeException.fillInStackTrace();
                Log.e("BroadcastReceiver", localRuntimeException.getMessage(), localRuntimeException);
            }
        }

        public final void clearAbortBroadcast()
        {
            this.mAbortBroadcast = false;
        }

        public final void finish()
        {
            final IActivityManager localIActivityManager;
            if (this.mType == 0)
            {
                localIActivityManager = ActivityManagerNative.getDefault();
                if (QueuedWork.hasPendingWork())
                    QueuedWork.singleThreadExecutor().execute(new Runnable()
                    {
                        public void run()
                        {
                            BroadcastReceiver.PendingResult.this.sendFinished(localIActivityManager);
                        }
                    });
            }
            while (true)
            {
                return;
                sendFinished(localIActivityManager);
                continue;
                if ((this.mOrderedHint) && (this.mType != 2))
                    sendFinished(ActivityManagerNative.getDefault());
            }
        }

        public final boolean getAbortBroadcast()
        {
            return this.mAbortBroadcast;
        }

        public final int getResultCode()
        {
            return this.mResultCode;
        }

        public final String getResultData()
        {
            return this.mResultData;
        }

        public final Bundle getResultExtras(boolean paramBoolean)
        {
            Bundle localBundle1 = this.mResultExtras;
            if (!paramBoolean);
            for (Bundle localBundle2 = localBundle1; ; localBundle2 = localBundle1)
            {
                return localBundle2;
                if (localBundle1 == null)
                {
                    localBundle1 = new Bundle();
                    this.mResultExtras = localBundle1;
                }
            }
        }

        // ERROR //
        public void sendFinished(IActivityManager paramIActivityManager)
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 121	android/content/BroadcastReceiver$PendingResult:mFinished	Z
            //     6: ifeq +18 -> 24
            //     9: new 123	java/lang/IllegalStateException
            //     12: dup
            //     13: ldc 125
            //     15: invokespecial 126	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     18: athrow
            //     19: astore_2
            //     20: aload_0
            //     21: monitorexit
            //     22: aload_2
            //     23: athrow
            //     24: aload_0
            //     25: iconst_1
            //     26: putfield 121	android/content/BroadcastReceiver$PendingResult:mFinished	Z
            //     29: aload_0
            //     30: getfield 40	android/content/BroadcastReceiver$PendingResult:mResultExtras	Landroid/os/Bundle;
            //     33: ifnull +12 -> 45
            //     36: aload_0
            //     37: getfield 40	android/content/BroadcastReceiver$PendingResult:mResultExtras	Landroid/os/Bundle;
            //     40: iconst_0
            //     41: invokevirtual 130	android/os/Bundle:setAllowFds	(Z)Z
            //     44: pop
            //     45: aload_0
            //     46: getfield 44	android/content/BroadcastReceiver$PendingResult:mOrderedHint	Z
            //     49: ifeq +32 -> 81
            //     52: aload_1
            //     53: aload_0
            //     54: getfield 48	android/content/BroadcastReceiver$PendingResult:mToken	Landroid/os/IBinder;
            //     57: aload_0
            //     58: getfield 36	android/content/BroadcastReceiver$PendingResult:mResultCode	I
            //     61: aload_0
            //     62: getfield 38	android/content/BroadcastReceiver$PendingResult:mResultData	Ljava/lang/String;
            //     65: aload_0
            //     66: getfield 40	android/content/BroadcastReceiver$PendingResult:mResultExtras	Landroid/os/Bundle;
            //     69: aload_0
            //     70: getfield 54	android/content/BroadcastReceiver$PendingResult:mAbortBroadcast	Z
            //     73: invokeinterface 136 6 0
            //     78: aload_0
            //     79: monitorexit
            //     80: return
            //     81: aload_1
            //     82: aload_0
            //     83: getfield 48	android/content/BroadcastReceiver$PendingResult:mToken	Landroid/os/IBinder;
            //     86: iconst_0
            //     87: aconst_null
            //     88: aconst_null
            //     89: iconst_0
            //     90: invokeinterface 136 6 0
            //     95: goto -17 -> 78
            //     98: astore_3
            //     99: goto -21 -> 78
            //
            // Exception table:
            //     from	to	target	type
            //     2	22	19	finally
            //     24	29	19	finally
            //     29	78	19	finally
            //     78	80	19	finally
            //     81	95	19	finally
            //     29	78	98	android/os/RemoteException
            //     81	95	98	android/os/RemoteException
        }

        public void setExtrasClassLoader(ClassLoader paramClassLoader)
        {
            if (this.mResultExtras != null)
                this.mResultExtras.setClassLoader(paramClassLoader);
        }

        public final void setResult(int paramInt, String paramString, Bundle paramBundle)
        {
            checkSynchronousHint();
            this.mResultCode = paramInt;
            this.mResultData = paramString;
            this.mResultExtras = paramBundle;
        }

        public final void setResultCode(int paramInt)
        {
            checkSynchronousHint();
            this.mResultCode = paramInt;
        }

        public final void setResultData(String paramString)
        {
            checkSynchronousHint();
            this.mResultData = paramString;
        }

        public final void setResultExtras(Bundle paramBundle)
        {
            checkSynchronousHint();
            this.mResultExtras = paramBundle;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.BroadcastReceiver
 * JD-Core Version:        0.6.2
 */