package android.content;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.PatternMatcher;
import android.util.AndroidException;
import android.util.Log;
import android.util.Printer;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class IntentFilter
    implements Parcelable
{
    private static final String ACTION_STR = "action";
    private static final String AUTH_STR = "auth";
    private static final String CAT_STR = "cat";
    public static final Parcelable.Creator<IntentFilter> CREATOR = new Parcelable.Creator()
    {
        public IntentFilter createFromParcel(Parcel paramAnonymousParcel)
        {
            return new IntentFilter(paramAnonymousParcel, null);
        }

        public IntentFilter[] newArray(int paramAnonymousInt)
        {
            return new IntentFilter[paramAnonymousInt];
        }
    };
    private static final String HOST_STR = "host";
    private static final String LITERAL_STR = "literal";
    public static final int MATCH_ADJUSTMENT_MASK = 65535;
    public static final int MATCH_ADJUSTMENT_NORMAL = 32768;
    public static final int MATCH_CATEGORY_EMPTY = 1048576;
    public static final int MATCH_CATEGORY_HOST = 3145728;
    public static final int MATCH_CATEGORY_MASK = 268369920;
    public static final int MATCH_CATEGORY_PATH = 5242880;
    public static final int MATCH_CATEGORY_PORT = 4194304;
    public static final int MATCH_CATEGORY_SCHEME = 2097152;
    public static final int MATCH_CATEGORY_TYPE = 6291456;
    private static final String NAME_STR = "name";
    public static final int NO_MATCH_ACTION = -3;
    public static final int NO_MATCH_CATEGORY = -4;
    public static final int NO_MATCH_DATA = -2;
    public static final int NO_MATCH_TYPE = -1;
    private static final String PATH_STR = "path";
    private static final String PORT_STR = "port";
    private static final String PREFIX_STR = "prefix";
    private static final String SCHEME_STR = "scheme";
    private static final String SGLOB_STR = "sglob";
    public static final int SYSTEM_HIGH_PRIORITY = 1000;
    public static final int SYSTEM_LOW_PRIORITY = -1000;
    private static final String TYPE_STR = "type";
    private final ArrayList<String> mActions;
    private ArrayList<String> mCategories = null;
    private ArrayList<AuthorityEntry> mDataAuthorities = null;
    private ArrayList<PatternMatcher> mDataPaths = null;
    private ArrayList<String> mDataSchemes = null;
    private ArrayList<String> mDataTypes = null;
    private boolean mHasPartialTypes = false;
    private int mPriority;

    public IntentFilter()
    {
        this.mPriority = 0;
        this.mActions = new ArrayList();
    }

    public IntentFilter(IntentFilter paramIntentFilter)
    {
        this.mPriority = paramIntentFilter.mPriority;
        this.mActions = new ArrayList(paramIntentFilter.mActions);
        if (paramIntentFilter.mCategories != null)
            this.mCategories = new ArrayList(paramIntentFilter.mCategories);
        if (paramIntentFilter.mDataTypes != null)
            this.mDataTypes = new ArrayList(paramIntentFilter.mDataTypes);
        if (paramIntentFilter.mDataSchemes != null)
            this.mDataSchemes = new ArrayList(paramIntentFilter.mDataSchemes);
        if (paramIntentFilter.mDataAuthorities != null)
            this.mDataAuthorities = new ArrayList(paramIntentFilter.mDataAuthorities);
        if (paramIntentFilter.mDataPaths != null)
            this.mDataPaths = new ArrayList(paramIntentFilter.mDataPaths);
        this.mHasPartialTypes = paramIntentFilter.mHasPartialTypes;
    }

    private IntentFilter(Parcel paramParcel)
    {
        this.mActions = new ArrayList();
        paramParcel.readStringList(this.mActions);
        if (paramParcel.readInt() != 0)
        {
            this.mCategories = new ArrayList();
            paramParcel.readStringList(this.mCategories);
        }
        if (paramParcel.readInt() != 0)
        {
            this.mDataSchemes = new ArrayList();
            paramParcel.readStringList(this.mDataSchemes);
        }
        if (paramParcel.readInt() != 0)
        {
            this.mDataTypes = new ArrayList();
            paramParcel.readStringList(this.mDataTypes);
        }
        int i = paramParcel.readInt();
        if (i > 0)
        {
            this.mDataAuthorities = new ArrayList();
            for (int m = 0; m < i; m++)
                this.mDataAuthorities.add(new AuthorityEntry(paramParcel));
        }
        int j = paramParcel.readInt();
        if (j > 0)
        {
            this.mDataPaths = new ArrayList();
            for (int k = 0; k < j; k++)
                this.mDataPaths.add(new PatternMatcher(paramParcel));
        }
        this.mPriority = paramParcel.readInt();
        if (paramParcel.readInt() > 0)
            bool = true;
        this.mHasPartialTypes = bool;
    }

    public IntentFilter(String paramString)
    {
        this.mPriority = 0;
        this.mActions = new ArrayList();
        addAction(paramString);
    }

    public IntentFilter(String paramString1, String paramString2)
        throws IntentFilter.MalformedMimeTypeException
    {
        this.mPriority = 0;
        this.mActions = new ArrayList();
        addAction(paramString1);
        addDataType(paramString2);
    }

    private static String[] addStringToSet(String[] paramArrayOfString, String paramString, int[] paramArrayOfInt, int paramInt)
    {
        if (findStringInSet(paramArrayOfString, paramString, paramArrayOfInt, paramInt) >= 0);
        while (true)
        {
            return paramArrayOfString;
            if (paramArrayOfString == null)
            {
                paramArrayOfString = new String[2];
                paramArrayOfString[0] = paramString;
                paramArrayOfInt[paramInt] = 1;
            }
            else
            {
                int i = paramArrayOfInt[paramInt];
                if (i < paramArrayOfString.length)
                {
                    paramArrayOfString[i] = paramString;
                    paramArrayOfInt[paramInt] = (i + 1);
                }
                else
                {
                    String[] arrayOfString = new String[2 + i * 3 / 2];
                    System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, i);
                    paramArrayOfString = arrayOfString;
                    paramArrayOfString[i] = paramString;
                    paramArrayOfInt[paramInt] = (i + 1);
                }
            }
        }
    }

    public static IntentFilter create(String paramString1, String paramString2)
    {
        try
        {
            IntentFilter localIntentFilter = new IntentFilter(paramString1, paramString2);
            return localIntentFilter;
        }
        catch (MalformedMimeTypeException localMalformedMimeTypeException)
        {
            throw new RuntimeException("Bad MIME type", localMalformedMimeTypeException);
        }
    }

    private final boolean findMimeType(String paramString)
    {
        boolean bool = true;
        ArrayList localArrayList = this.mDataTypes;
        if (paramString == null)
            bool = false;
        while (true)
        {
            return bool;
            if (!localArrayList.contains(paramString))
            {
                int i = paramString.length();
                if ((i == 3) && (paramString.equals("*/*")))
                {
                    if (localArrayList.isEmpty())
                        bool = false;
                }
                else if ((!this.mHasPartialTypes) || (!localArrayList.contains("*")))
                {
                    int j = paramString.indexOf('/');
                    if (j > 0)
                    {
                        if ((!this.mHasPartialTypes) || (!localArrayList.contains(paramString.substring(0, j))))
                            if ((i == j + 2) && (paramString.charAt(j + 1) == '*'))
                            {
                                int k = localArrayList.size();
                                for (int m = 0; ; m++)
                                {
                                    if (m >= k)
                                        break label172;
                                    if (paramString.regionMatches(0, (String)localArrayList.get(m), 0, j + 1))
                                        break;
                                }
                            }
                    }
                    else
                        label172: bool = false;
                }
            }
        }
    }

    private static int findStringInSet(String[] paramArrayOfString, String paramString, int[] paramArrayOfInt, int paramInt)
    {
        int j;
        if (paramArrayOfString == null)
            j = -1;
        while (true)
        {
            return j;
            int i = paramArrayOfInt[paramInt];
            for (j = 0; ; j++)
            {
                if (j >= i)
                    break label43;
                if (paramArrayOfString[j].equals(paramString))
                    break;
            }
            label43: j = -1;
        }
    }

    private static String[] removeStringFromSet(String[] paramArrayOfString, String paramString, int[] paramArrayOfInt, int paramInt)
    {
        int i = findStringInSet(paramArrayOfString, paramString, paramArrayOfInt, paramInt);
        if (i < 0);
        while (true)
        {
            return paramArrayOfString;
            int j = paramArrayOfInt[paramInt];
            if (j > paramArrayOfString.length / 4)
            {
                int k = j - (i + 1);
                if (k > 0)
                    System.arraycopy(paramArrayOfString, i + 1, paramArrayOfString, i, k);
                paramArrayOfString[(j - 1)] = null;
                paramArrayOfInt[paramInt] = (j - 1);
            }
            else
            {
                String[] arrayOfString = new String[paramArrayOfString.length / 3];
                if (i > 0)
                    System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, i);
                if (i + 1 < j)
                    System.arraycopy(paramArrayOfString, i + 1, arrayOfString, i, j - (i + 1));
                paramArrayOfString = arrayOfString;
            }
        }
    }

    public final Iterator<String> actionsIterator()
    {
        if (this.mActions != null);
        for (Iterator localIterator = this.mActions.iterator(); ; localIterator = null)
            return localIterator;
    }

    public final void addAction(String paramString)
    {
        if (!this.mActions.contains(paramString))
            this.mActions.add(paramString.intern());
    }

    public final void addCategory(String paramString)
    {
        if (this.mCategories == null)
            this.mCategories = new ArrayList();
        if (!this.mCategories.contains(paramString))
            this.mCategories.add(paramString.intern());
    }

    public final void addDataAuthority(String paramString1, String paramString2)
    {
        if (this.mDataAuthorities == null)
            this.mDataAuthorities = new ArrayList();
        if (paramString2 != null)
            paramString2 = paramString2.intern();
        this.mDataAuthorities.add(new AuthorityEntry(paramString1.intern(), paramString2));
    }

    public final void addDataPath(String paramString, int paramInt)
    {
        if (this.mDataPaths == null)
            this.mDataPaths = new ArrayList();
        this.mDataPaths.add(new PatternMatcher(paramString.intern(), paramInt));
    }

    public final void addDataScheme(String paramString)
    {
        if (this.mDataSchemes == null)
            this.mDataSchemes = new ArrayList();
        if (!this.mDataSchemes.contains(paramString))
            this.mDataSchemes.add(paramString.intern());
    }

    public final void addDataType(String paramString)
        throws IntentFilter.MalformedMimeTypeException
    {
        int i = paramString.indexOf('/');
        int j = paramString.length();
        if ((i > 0) && (j >= i + 2))
        {
            if (this.mDataTypes == null)
                this.mDataTypes = new ArrayList();
            if ((j == i + 2) && (paramString.charAt(i + 1) == '*'))
            {
                String str = paramString.substring(0, i);
                if (!this.mDataTypes.contains(str))
                    this.mDataTypes.add(str.intern());
                this.mHasPartialTypes = true;
            }
            while (true)
            {
                return;
                if (!this.mDataTypes.contains(paramString))
                    this.mDataTypes.add(paramString.intern());
            }
        }
        throw new MalformedMimeTypeException(paramString);
    }

    public final Iterator<AuthorityEntry> authoritiesIterator()
    {
        if (this.mDataAuthorities != null);
        for (Iterator localIterator = this.mDataAuthorities.iterator(); ; localIterator = null)
            return localIterator;
    }

    public final Iterator<String> categoriesIterator()
    {
        if (this.mCategories != null);
        for (Iterator localIterator = this.mCategories.iterator(); ; localIterator = null)
            return localIterator;
    }

    public final int countActions()
    {
        return this.mActions.size();
    }

    public final int countCategories()
    {
        if (this.mCategories != null);
        for (int i = this.mCategories.size(); ; i = 0)
            return i;
    }

    public final int countDataAuthorities()
    {
        if (this.mDataAuthorities != null);
        for (int i = this.mDataAuthorities.size(); ; i = 0)
            return i;
    }

    public final int countDataPaths()
    {
        if (this.mDataPaths != null);
        for (int i = this.mDataPaths.size(); ; i = 0)
            return i;
    }

    public final int countDataSchemes()
    {
        if (this.mDataSchemes != null);
        for (int i = this.mDataSchemes.size(); ; i = 0)
            return i;
    }

    public final int countDataTypes()
    {
        if (this.mDataTypes != null);
        for (int i = this.mDataTypes.size(); ; i = 0)
            return i;
    }

    public boolean debugCheck()
    {
        return true;
    }

    public final int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder(256);
        if (this.mActions.size() > 0)
        {
            Iterator localIterator6 = this.mActions.iterator();
            while (localIterator6.hasNext())
            {
                localStringBuilder.setLength(0);
                localStringBuilder.append(paramString);
                localStringBuilder.append("Action: \"");
                localStringBuilder.append((String)localIterator6.next());
                localStringBuilder.append("\"");
                paramPrinter.println(localStringBuilder.toString());
            }
        }
        if (this.mCategories != null)
        {
            Iterator localIterator5 = this.mCategories.iterator();
            while (localIterator5.hasNext())
            {
                localStringBuilder.setLength(0);
                localStringBuilder.append(paramString);
                localStringBuilder.append("Category: \"");
                localStringBuilder.append((String)localIterator5.next());
                localStringBuilder.append("\"");
                paramPrinter.println(localStringBuilder.toString());
            }
        }
        if (this.mDataSchemes != null)
        {
            Iterator localIterator4 = this.mDataSchemes.iterator();
            while (localIterator4.hasNext())
            {
                localStringBuilder.setLength(0);
                localStringBuilder.append(paramString);
                localStringBuilder.append("Scheme: \"");
                localStringBuilder.append((String)localIterator4.next());
                localStringBuilder.append("\"");
                paramPrinter.println(localStringBuilder.toString());
            }
        }
        if (this.mDataAuthorities != null)
        {
            Iterator localIterator3 = this.mDataAuthorities.iterator();
            while (localIterator3.hasNext())
            {
                AuthorityEntry localAuthorityEntry = (AuthorityEntry)localIterator3.next();
                localStringBuilder.setLength(0);
                localStringBuilder.append(paramString);
                localStringBuilder.append("Authority: \"");
                localStringBuilder.append(localAuthorityEntry.mHost);
                localStringBuilder.append("\": ");
                localStringBuilder.append(localAuthorityEntry.mPort);
                if (localAuthorityEntry.mWild)
                    localStringBuilder.append(" WILD");
                paramPrinter.println(localStringBuilder.toString());
            }
        }
        if (this.mDataPaths != null)
        {
            Iterator localIterator2 = this.mDataPaths.iterator();
            while (localIterator2.hasNext())
            {
                PatternMatcher localPatternMatcher = (PatternMatcher)localIterator2.next();
                localStringBuilder.setLength(0);
                localStringBuilder.append(paramString);
                localStringBuilder.append("Path: \"");
                localStringBuilder.append(localPatternMatcher);
                localStringBuilder.append("\"");
                paramPrinter.println(localStringBuilder.toString());
            }
        }
        if (this.mDataTypes != null)
        {
            Iterator localIterator1 = this.mDataTypes.iterator();
            while (localIterator1.hasNext())
            {
                localStringBuilder.setLength(0);
                localStringBuilder.append(paramString);
                localStringBuilder.append("Type: \"");
                localStringBuilder.append((String)localIterator1.next());
                localStringBuilder.append("\"");
                paramPrinter.println(localStringBuilder.toString());
            }
        }
        if ((this.mPriority != 0) || (this.mHasPartialTypes))
        {
            localStringBuilder.setLength(0);
            localStringBuilder.append(paramString);
            localStringBuilder.append("mPriority=");
            localStringBuilder.append(this.mPriority);
            localStringBuilder.append(", mHasPartialTypes=");
            localStringBuilder.append(this.mHasPartialTypes);
            paramPrinter.println(localStringBuilder.toString());
        }
    }

    public final String getAction(int paramInt)
    {
        return (String)this.mActions.get(paramInt);
    }

    public final String getCategory(int paramInt)
    {
        return (String)this.mCategories.get(paramInt);
    }

    public final AuthorityEntry getDataAuthority(int paramInt)
    {
        return (AuthorityEntry)this.mDataAuthorities.get(paramInt);
    }

    public final PatternMatcher getDataPath(int paramInt)
    {
        return (PatternMatcher)this.mDataPaths.get(paramInt);
    }

    public final String getDataScheme(int paramInt)
    {
        return (String)this.mDataSchemes.get(paramInt);
    }

    public final String getDataType(int paramInt)
    {
        return (String)this.mDataTypes.get(paramInt);
    }

    public final int getPriority()
    {
        return this.mPriority;
    }

    public final boolean hasAction(String paramString)
    {
        if ((paramString != null) && (this.mActions.contains(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean hasCategory(String paramString)
    {
        if ((this.mCategories != null) && (this.mCategories.contains(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean hasDataAuthority(Uri paramUri)
    {
        if (matchDataAuthority(paramUri) >= 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean hasDataPath(String paramString)
    {
        boolean bool = false;
        if (this.mDataPaths == null);
        label56: 
        while (true)
        {
            return bool;
            int i = this.mDataPaths.size();
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label56;
                if (((PatternMatcher)this.mDataPaths.get(j)).match(paramString))
                {
                    bool = true;
                    break;
                }
            }
        }
    }

    public final boolean hasDataScheme(String paramString)
    {
        if ((this.mDataSchemes != null) && (this.mDataSchemes.contains(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean hasDataType(String paramString)
    {
        if ((this.mDataTypes != null) && (findMimeType(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final int match(ContentResolver paramContentResolver, Intent paramIntent, boolean paramBoolean, String paramString)
    {
        if (paramBoolean);
        for (String str = paramIntent.resolveType(paramContentResolver); ; str = paramIntent.getType())
            return match(paramIntent.getAction(), str, paramIntent.getScheme(), paramIntent.getData(), paramIntent.getCategories(), paramString);
    }

    public final int match(String paramString1, String paramString2, String paramString3, Uri paramUri, Set<String> paramSet, String paramString4)
    {
        if ((paramString1 != null) && (!matchAction(paramString1)));
        for (int i = -3; ; i = -4)
            do
            {
                return i;
                i = matchData(paramString2, paramString3, paramUri);
            }
            while ((i < 0) || (matchCategories(paramSet) == null));
    }

    public final boolean matchAction(String paramString)
    {
        return hasAction(paramString);
    }

    public final String matchCategories(Set<String> paramSet)
    {
        Object localObject = null;
        if (paramSet == null)
            break label44;
        while (true)
        {
            return localObject;
            Iterator localIterator = paramSet.iterator();
            if (this.mCategories == null)
            {
                if (localIterator.hasNext())
                    localObject = (String)localIterator.next();
            }
            else
                label44: if (localIterator.hasNext())
                {
                    String str = (String)localIterator.next();
                    if (this.mCategories.contains(str))
                        break;
                    localObject = str;
                }
        }
    }

    public final int matchData(String paramString1, String paramString2, Uri paramUri)
    {
        int i = -2;
        ArrayList localArrayList1 = this.mDataTypes;
        ArrayList localArrayList2 = this.mDataSchemes;
        ArrayList localArrayList3 = this.mDataAuthorities;
        ArrayList localArrayList4 = this.mDataPaths;
        int j = 1048576;
        if ((localArrayList1 == null) && (localArrayList2 == null))
            if ((paramString1 == null) && (paramUri == null))
                i = 1081344;
        while (true)
        {
            return i;
            if (localArrayList2 != null)
            {
                if (paramString2 != null)
                {
                    label67: if (!localArrayList2.contains(paramString2))
                        break label138;
                    j = 2097152;
                    if (localArrayList3 != null)
                    {
                        int k = matchDataAuthority(paramUri);
                        if (k < 0)
                            continue;
                        if (localArrayList4 != null)
                            break label140;
                        j = k;
                    }
                    label106: if (localArrayList1 == null)
                        break label202;
                    if (!findMimeType(paramString1))
                        break label195;
                    j = 6291456;
                }
            }
            else
            {
                label138: label140: label195: label202: 
                while (paramString1 == null)
                {
                    i = 32768 + j;
                    break;
                    paramString2 = "";
                    break label67;
                    break;
                    if (!hasDataPath(paramUri.getPath()))
                        break;
                    j = 5242880;
                    break label106;
                    if ((paramString2 == null) || ("".equals(paramString2)) || ("content".equals(paramString2)) || ("file".equals(paramString2)))
                        break label106;
                    break;
                    i = -1;
                    break;
                }
                i = -1;
            }
        }
    }

    public final int matchDataAuthority(Uri paramUri)
    {
        int k;
        if (this.mDataAuthorities == null)
            k = -2;
        while (true)
        {
            return k;
            int i = this.mDataAuthorities.size();
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label57;
                k = ((AuthorityEntry)this.mDataAuthorities.get(j)).match(paramUri);
                if (k >= 0)
                    break;
            }
            label57: k = -2;
        }
    }

    public final Iterator<PatternMatcher> pathsIterator()
    {
        if (this.mDataPaths != null);
        for (Iterator localIterator = this.mDataPaths.iterator(); ; localIterator = null)
            return localIterator;
    }

    public void readFromXml(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        String str1 = paramXmlPullParser.getName();
        if (str1.equals("action"))
        {
            String str10 = paramXmlPullParser.getAttributeValue(null, "name");
            if (str10 != null)
                addAction(str10);
        }
        while (true)
        {
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            if (str1.equals("cat"))
            {
                String str9 = paramXmlPullParser.getAttributeValue(null, "name");
                if (str9 != null)
                    addCategory(str9);
            }
            else if (str1.equals("type"))
            {
                String str8 = paramXmlPullParser.getAttributeValue(null, "name");
                if (str8 != null)
                    try
                    {
                        addDataType(str8);
                    }
                    catch (MalformedMimeTypeException localMalformedMimeTypeException)
                    {
                    }
            }
            else if (str1.equals("scheme"))
            {
                String str7 = paramXmlPullParser.getAttributeValue(null, "name");
                if (str7 != null)
                    addDataScheme(str7);
            }
            else if (str1.equals("auth"))
            {
                String str5 = paramXmlPullParser.getAttributeValue(null, "host");
                String str6 = paramXmlPullParser.getAttributeValue(null, "port");
                if (str5 != null)
                    addDataAuthority(str5, str6);
            }
            else if (str1.equals("path"))
            {
                String str2 = paramXmlPullParser.getAttributeValue(null, "literal");
                if (str2 != null)
                {
                    addDataPath(str2, 0);
                }
                else
                {
                    String str3 = paramXmlPullParser.getAttributeValue(null, "prefix");
                    if (str3 != null)
                    {
                        addDataPath(str3, 1);
                    }
                    else
                    {
                        String str4 = paramXmlPullParser.getAttributeValue(null, "sglob");
                        if (str4 != null)
                            addDataPath(str4, 2);
                    }
                }
            }
            else
            {
                Log.w("IntentFilter", "Unknown tag parsing IntentFilter: " + str1);
            }
        }
    }

    public final Iterator<String> schemesIterator()
    {
        if (this.mDataSchemes != null);
        for (Iterator localIterator = this.mDataSchemes.iterator(); ; localIterator = null)
            return localIterator;
    }

    public final void setPriority(int paramInt)
    {
        this.mPriority = paramInt;
    }

    public final Iterator<String> typesIterator()
    {
        if (this.mDataTypes != null);
        for (Iterator localIterator = this.mDataTypes.iterator(); ; localIterator = null)
            return localIterator;
    }

    public final void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStringList(this.mActions);
        if (this.mCategories != null)
        {
            paramParcel.writeInt(1);
            paramParcel.writeStringList(this.mCategories);
            if (this.mDataSchemes == null)
                break label130;
            paramParcel.writeInt(1);
            paramParcel.writeStringList(this.mDataSchemes);
            label48: if (this.mDataTypes == null)
                break label138;
            paramParcel.writeInt(1);
            paramParcel.writeStringList(this.mDataTypes);
        }
        while (true)
        {
            if (this.mDataAuthorities == null)
                break label146;
            int m = this.mDataAuthorities.size();
            paramParcel.writeInt(m);
            for (int n = 0; n < m; n++)
                ((AuthorityEntry)this.mDataAuthorities.get(n)).writeToParcel(paramParcel);
            paramParcel.writeInt(0);
            break;
            label130: paramParcel.writeInt(0);
            break label48;
            label138: paramParcel.writeInt(0);
        }
        label146: paramParcel.writeInt(0);
        if (this.mDataPaths != null)
        {
            int j = this.mDataPaths.size();
            paramParcel.writeInt(j);
            for (int k = 0; k < j; k++)
                ((PatternMatcher)this.mDataPaths.get(k)).writeToParcel(paramParcel, 0);
        }
        paramParcel.writeInt(0);
        paramParcel.writeInt(this.mPriority);
        if (this.mHasPartialTypes);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }

    public void writeToXml(XmlSerializer paramXmlSerializer)
        throws IOException
    {
        int i = countActions();
        for (int j = 0; j < i; j++)
        {
            paramXmlSerializer.startTag(null, "action");
            paramXmlSerializer.attribute(null, "name", (String)this.mActions.get(j));
            paramXmlSerializer.endTag(null, "action");
        }
        int k = countCategories();
        for (int m = 0; m < k; m++)
        {
            paramXmlSerializer.startTag(null, "cat");
            paramXmlSerializer.attribute(null, "name", (String)this.mCategories.get(m));
            paramXmlSerializer.endTag(null, "cat");
        }
        int n = countDataTypes();
        for (int i1 = 0; i1 < n; i1++)
        {
            paramXmlSerializer.startTag(null, "type");
            String str = (String)this.mDataTypes.get(i1);
            if (str.indexOf('/') < 0)
                str = str + "/*";
            paramXmlSerializer.attribute(null, "name", str);
            paramXmlSerializer.endTag(null, "type");
        }
        int i2 = countDataSchemes();
        for (int i3 = 0; i3 < i2; i3++)
        {
            paramXmlSerializer.startTag(null, "scheme");
            paramXmlSerializer.attribute(null, "name", (String)this.mDataSchemes.get(i3));
            paramXmlSerializer.endTag(null, "scheme");
        }
        int i4 = countDataAuthorities();
        for (int i5 = 0; i5 < i4; i5++)
        {
            paramXmlSerializer.startTag(null, "auth");
            AuthorityEntry localAuthorityEntry = (AuthorityEntry)this.mDataAuthorities.get(i5);
            paramXmlSerializer.attribute(null, "host", localAuthorityEntry.getHost());
            if (localAuthorityEntry.getPort() >= 0)
                paramXmlSerializer.attribute(null, "port", Integer.toString(localAuthorityEntry.getPort()));
            paramXmlSerializer.endTag(null, "auth");
        }
        int i6 = countDataPaths();
        int i7 = 0;
        if (i7 < i6)
        {
            paramXmlSerializer.startTag(null, "path");
            PatternMatcher localPatternMatcher = (PatternMatcher)this.mDataPaths.get(i7);
            switch (localPatternMatcher.getType())
            {
            default:
            case 0:
            case 1:
            case 2:
            }
            while (true)
            {
                paramXmlSerializer.endTag(null, "path");
                i7++;
                break;
                paramXmlSerializer.attribute(null, "literal", localPatternMatcher.getPath());
                continue;
                paramXmlSerializer.attribute(null, "prefix", localPatternMatcher.getPath());
                continue;
                paramXmlSerializer.attribute(null, "sglob", localPatternMatcher.getPath());
            }
        }
    }

    public static final class AuthorityEntry
    {
        private final String mHost;
        private final String mOrigHost;
        private final int mPort;
        private final boolean mWild;

        AuthorityEntry(Parcel paramParcel)
        {
            this.mOrigHost = paramParcel.readString();
            this.mHost = paramParcel.readString();
            if (paramParcel.readInt() != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.mWild = bool;
                this.mPort = paramParcel.readInt();
                return;
            }
        }

        public AuthorityEntry(String paramString1, String paramString2)
        {
            this.mOrigHost = paramString1;
            if ((paramString1.length() > 0) && (paramString1.charAt(0) == '*'))
                bool = true;
            this.mWild = bool;
            if (this.mWild)
                paramString1 = paramString1.substring(1).intern();
            this.mHost = paramString1;
            if (paramString2 != null);
            for (int i = Integer.parseInt(paramString2); ; i = -1)
            {
                this.mPort = i;
                return;
            }
        }

        public String getHost()
        {
            return this.mOrigHost;
        }

        public int getPort()
        {
            return this.mPort;
        }

        public int match(Uri paramUri)
        {
            int i = -2;
            String str = paramUri.getHost();
            if (str == null);
            while (true)
            {
                return i;
                if (this.mWild)
                {
                    if (str.length() >= this.mHost.length())
                        str = str.substring(str.length() - this.mHost.length());
                }
                else if (str.compareToIgnoreCase(this.mHost) == 0)
                    if (this.mPort >= 0)
                    {
                        if (this.mPort == paramUri.getPort())
                            i = 4194304;
                    }
                    else
                        i = 3145728;
            }
        }

        void writeToParcel(Parcel paramParcel)
        {
            paramParcel.writeString(this.mOrigHost);
            paramParcel.writeString(this.mHost);
            if (this.mWild);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                paramParcel.writeInt(this.mPort);
                return;
            }
        }
    }

    public static class MalformedMimeTypeException extends AndroidException
    {
        public MalformedMimeTypeException()
        {
        }

        public MalformedMimeTypeException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IntentFilter
 * JD-Core Version:        0.6.2
 */