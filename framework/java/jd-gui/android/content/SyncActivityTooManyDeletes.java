package android.content;

import android.accounts.Account;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class SyncActivityTooManyDeletes extends Activity
    implements AdapterView.OnItemClickListener
{
    private Account mAccount;
    private String mAuthority;
    private long mNumDeletes;
    private String mProvider;

    private void startSyncReallyDelete()
    {
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("deletions_override", true);
        localBundle.putBoolean("force", true);
        localBundle.putBoolean("expedited", true);
        localBundle.putBoolean("upload", true);
        ContentResolver.requestSync(this.mAccount, this.mAuthority, localBundle);
    }

    private void startSyncUndoDeletes()
    {
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("discard_deletions", true);
        localBundle.putBoolean("force", true);
        localBundle.putBoolean("expedited", true);
        localBundle.putBoolean("upload", true);
        ContentResolver.requestSync(this.mAccount, this.mAuthority, localBundle);
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        Bundle localBundle = getIntent().getExtras();
        if (localBundle == null)
            finish();
        while (true)
        {
            return;
            this.mNumDeletes = localBundle.getLong("numDeletes");
            this.mAccount = ((Account)localBundle.getParcelable("account"));
            this.mAuthority = localBundle.getString("authority");
            this.mProvider = localBundle.getString("provider");
            CharSequence[] arrayOfCharSequence = new CharSequence[3];
            arrayOfCharSequence[0] = getResources().getText(17040547);
            arrayOfCharSequence[1] = getResources().getText(17040548);
            arrayOfCharSequence[2] = getResources().getText(17040549);
            ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367043, 16908308, arrayOfCharSequence);
            ListView localListView = new ListView(this);
            localListView.setAdapter(localArrayAdapter);
            localListView.setItemsCanFocus(true);
            localListView.setOnItemClickListener(this);
            TextView localTextView = new TextView(this);
            String str = getResources().getText(17040546).toString();
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = Long.valueOf(this.mNumDeletes);
            arrayOfObject[1] = this.mProvider;
            arrayOfObject[2] = this.mAccount.name;
            localTextView.setText(String.format(str, arrayOfObject));
            LinearLayout localLinearLayout = new LinearLayout(this);
            localLinearLayout.setOrientation(1);
            LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2, 0.0F);
            localLinearLayout.addView(localTextView, localLayoutParams);
            localLinearLayout.addView(localListView, localLayoutParams);
            setContentView(localLinearLayout);
        }
    }

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        if (paramInt == 0)
            startSyncReallyDelete();
        while (true)
        {
            finish();
            return;
            if (paramInt == 1)
                startSyncUndoDeletes();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncActivityTooManyDeletes
 * JD-Core Version:        0.6.2
 */