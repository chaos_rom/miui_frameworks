package android.content;

import android.net.Uri;
import android.util.Xml;
import android.util.Xml.Encoding;
import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class DefaultDataHandler
    implements ContentInsertHandler
{
    private static final String ARG = "arg";
    private static final String COL = "col";
    private static final String DEL = "del";
    private static final String POSTFIX = "postfix";
    private static final String ROW = "row";
    private static final String SELECT = "select";
    private static final String URI_STR = "uri";
    private ContentResolver mContentResolver;
    private Stack<Uri> mUris = new Stack();
    private ContentValues mValues;

    private Uri insertRow()
    {
        Uri localUri = this.mContentResolver.insert((Uri)this.mUris.lastElement(), this.mValues);
        this.mValues = null;
        return localUri;
    }

    private void parseRow(Attributes paramAttributes)
        throws SAXException
    {
        String str1 = paramAttributes.getValue("uri");
        String str2;
        if (str1 != null)
        {
            localUri = Uri.parse(str1);
            if (localUri == null)
                throw new SAXException("attribute " + paramAttributes.getValue("uri") + " parsing failure");
        }
        else
        {
            if (this.mUris.size() <= 0)
                break label128;
            str2 = paramAttributes.getValue("postfix");
            if (str2 == null)
                break label113;
        }
        label113: for (Uri localUri = Uri.withAppendedPath((Uri)this.mUris.lastElement(), str2); ; localUri = (Uri)this.mUris.lastElement())
        {
            this.mUris.push(localUri);
            return;
        }
        label128: throw new SAXException("attribute parsing failure");
    }

    public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws SAXException
    {
    }

    public void endDocument()
        throws SAXException
    {
    }

    public void endElement(String paramString1, String paramString2, String paramString3)
        throws SAXException
    {
        if ("row".equals(paramString2))
        {
            if (this.mUris.empty())
                throw new SAXException("uri mismatch");
            if (this.mValues != null)
                insertRow();
            this.mUris.pop();
        }
    }

    public void endPrefixMapping(String paramString)
        throws SAXException
    {
    }

    public void ignorableWhitespace(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws SAXException
    {
    }

    public void insert(ContentResolver paramContentResolver, InputStream paramInputStream)
        throws IOException, SAXException
    {
        this.mContentResolver = paramContentResolver;
        Xml.parse(paramInputStream, Xml.Encoding.UTF_8, this);
    }

    public void insert(ContentResolver paramContentResolver, String paramString)
        throws SAXException
    {
        this.mContentResolver = paramContentResolver;
        Xml.parse(paramString, this);
    }

    public void processingInstruction(String paramString1, String paramString2)
        throws SAXException
    {
    }

    public void setDocumentLocator(Locator paramLocator)
    {
    }

    public void skippedEntity(String paramString)
        throws SAXException
    {
    }

    public void startDocument()
        throws SAXException
    {
    }

    public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
        throws SAXException
    {
        if ("row".equals(paramString2))
            if (this.mValues != null)
            {
                if (this.mUris.empty())
                    throw new SAXException("uri is empty");
                Uri localUri2 = insertRow();
                if (localUri2 == null)
                    throw new SAXException("insert to uri " + ((Uri)this.mUris.lastElement()).toString() + " failure");
                this.mUris.pop();
                this.mUris.push(localUri2);
                parseRow(paramAttributes);
            }
        while (true)
        {
            return;
            if (paramAttributes.getLength() == 0)
            {
                this.mUris.push(this.mUris.lastElement());
            }
            else
            {
                parseRow(paramAttributes);
                continue;
                if ("col".equals(paramString2))
                {
                    int k = paramAttributes.getLength();
                    if (k != 2)
                        throw new SAXException("illegal attributes number " + k);
                    String str1 = paramAttributes.getValue(0);
                    String str2 = paramAttributes.getValue(1);
                    if ((str1 != null) && (str1.length() > 0) && (str2 != null) && (str2.length() > 0))
                    {
                        if (this.mValues == null)
                            this.mValues = new ContentValues();
                        this.mValues.put(str1, str2);
                    }
                    else
                    {
                        throw new SAXException("illegal attributes value");
                    }
                }
                else
                {
                    if (!"del".equals(paramString2))
                        break;
                    Uri localUri1 = Uri.parse(paramAttributes.getValue("uri"));
                    if (localUri1 == null)
                        throw new SAXException("attribute " + paramAttributes.getValue("uri") + " parsing failure");
                    int i = -2 + paramAttributes.getLength();
                    if (i > 0)
                    {
                        String[] arrayOfString = new String[i];
                        for (int j = 0; j < i; j++)
                            arrayOfString[j] = paramAttributes.getValue(j + 2);
                        this.mContentResolver.delete(localUri1, paramAttributes.getValue(1), arrayOfString);
                    }
                    else if (i == 0)
                    {
                        this.mContentResolver.delete(localUri1, paramAttributes.getValue(1), null);
                    }
                    else
                    {
                        this.mContentResolver.delete(localUri1, null, null);
                    }
                }
            }
        }
        throw new SAXException("unknown element: " + paramString2);
    }

    public void startPrefixMapping(String paramString1, String paramString2)
        throws SAXException
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.DefaultDataHandler
 * JD-Core Version:        0.6.2
 */