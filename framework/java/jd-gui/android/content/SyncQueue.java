package android.content;

import android.accounts.Account;
import android.content.pm.RegisteredServicesCache.ServiceInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.collect.Maps;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class SyncQueue
{
    private static final String TAG = "SyncManager";
    public final HashMap<String, SyncOperation> mOperationsMap = Maps.newHashMap();
    private SyncStorageEngine mSyncStorageEngine;

    public SyncQueue(SyncStorageEngine paramSyncStorageEngine, SyncAdaptersCache paramSyncAdaptersCache)
    {
        this.mSyncStorageEngine = paramSyncStorageEngine;
        ArrayList localArrayList = this.mSyncStorageEngine.getPendingOperations();
        int i = localArrayList.size();
        int j = 0;
        while (j < i)
        {
            SyncStorageEngine.PendingOperation localPendingOperation = (SyncStorageEngine.PendingOperation)localArrayList.get(j);
            Pair localPair = paramSyncStorageEngine.getBackoff(localPendingOperation.account, localPendingOperation.userId, localPendingOperation.authority);
            RegisteredServicesCache.ServiceInfo localServiceInfo = paramSyncAdaptersCache.getServiceInfo(SyncAdapterType.newKey(localPendingOperation.authority, localPendingOperation.account.type));
            if (localServiceInfo == null)
            {
                j++;
            }
            else
            {
                Account localAccount = localPendingOperation.account;
                int k = localPendingOperation.userId;
                int m = localPendingOperation.syncSource;
                String str = localPendingOperation.authority;
                Bundle localBundle = localPendingOperation.extras;
                if (localPair != null);
                for (long l = ((Long)localPair.first).longValue(); ; l = 0L)
                {
                    SyncOperation localSyncOperation = new SyncOperation(localAccount, k, m, str, localBundle, 0L, l, paramSyncStorageEngine.getDelayUntilTime(localPendingOperation.account, localPendingOperation.userId, localPendingOperation.authority), ((SyncAdapterType)localServiceInfo.type).allowParallelSyncs());
                    localSyncOperation.expedited = localPendingOperation.expedited;
                    localSyncOperation.pendingOperation = localPendingOperation;
                    add(localSyncOperation, localPendingOperation);
                    break;
                }
            }
        }
    }

    private boolean add(SyncOperation paramSyncOperation, SyncStorageEngine.PendingOperation paramPendingOperation)
    {
        String str = paramSyncOperation.key;
        SyncOperation localSyncOperation = (SyncOperation)this.mOperationsMap.get(str);
        boolean bool;
        if (localSyncOperation != null)
        {
            bool = false;
            if (localSyncOperation.expedited == paramSyncOperation.expedited)
            {
                long l = Math.min(localSyncOperation.earliestRunTime, paramSyncOperation.earliestRunTime);
                if (localSyncOperation.earliestRunTime != l)
                {
                    localSyncOperation.earliestRunTime = l;
                    bool = true;
                }
            }
        }
        while (true)
        {
            return bool;
            if (paramSyncOperation.expedited)
            {
                localSyncOperation.expedited = true;
                bool = true;
                continue;
                paramSyncOperation.pendingOperation = paramPendingOperation;
                if (paramSyncOperation.pendingOperation == null)
                {
                    SyncStorageEngine.PendingOperation localPendingOperation1 = new SyncStorageEngine.PendingOperation(paramSyncOperation.account, paramSyncOperation.userId, paramSyncOperation.syncSource, paramSyncOperation.authority, paramSyncOperation.extras, paramSyncOperation.expedited);
                    SyncStorageEngine.PendingOperation localPendingOperation2 = this.mSyncStorageEngine.insertIntoPending(localPendingOperation1);
                    if (localPendingOperation2 == null)
                        throw new IllegalStateException("error adding pending sync operation " + paramSyncOperation);
                    paramSyncOperation.pendingOperation = localPendingOperation2;
                }
                this.mOperationsMap.put(str, paramSyncOperation);
                bool = true;
            }
        }
    }

    public boolean add(SyncOperation paramSyncOperation)
    {
        return add(paramSyncOperation, null);
    }

    public void dump(StringBuilder paramStringBuilder)
    {
        long l = SystemClock.elapsedRealtime();
        paramStringBuilder.append("SyncQueue: ").append(this.mOperationsMap.size()).append(" operation(s)\n");
        Iterator localIterator = this.mOperationsMap.values().iterator();
        if (localIterator.hasNext())
        {
            SyncOperation localSyncOperation = (SyncOperation)localIterator.next();
            paramStringBuilder.append("    ");
            if (localSyncOperation.effectiveRunTime <= l)
                paramStringBuilder.append("READY");
            while (true)
            {
                paramStringBuilder.append(" - ");
                paramStringBuilder.append(localSyncOperation.dump(false)).append("\n");
                break;
                paramStringBuilder.append(DateUtils.formatElapsedTime((localSyncOperation.effectiveRunTime - l) / 1000L));
            }
        }
    }

    public void onBackoffChanged(Account paramAccount, int paramInt, String paramString, long paramLong)
    {
        Iterator localIterator = this.mOperationsMap.values().iterator();
        while (localIterator.hasNext())
        {
            SyncOperation localSyncOperation = (SyncOperation)localIterator.next();
            if ((localSyncOperation.account.equals(paramAccount)) && (localSyncOperation.authority.equals(paramString)) && (localSyncOperation.userId == paramInt))
            {
                localSyncOperation.backoff = Long.valueOf(paramLong);
                localSyncOperation.updateEffectiveRunTime();
            }
        }
    }

    public void onDelayUntilTimeChanged(Account paramAccount, String paramString, long paramLong)
    {
        Iterator localIterator = this.mOperationsMap.values().iterator();
        while (localIterator.hasNext())
        {
            SyncOperation localSyncOperation = (SyncOperation)localIterator.next();
            if ((localSyncOperation.account.equals(paramAccount)) && (localSyncOperation.authority.equals(paramString)))
            {
                localSyncOperation.delayUntil = paramLong;
                localSyncOperation.updateEffectiveRunTime();
            }
        }
    }

    public void remove(Account paramAccount, int paramInt, String paramString)
    {
        Iterator localIterator = this.mOperationsMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            SyncOperation localSyncOperation = (SyncOperation)((Map.Entry)localIterator.next()).getValue();
            if (((paramAccount == null) || (localSyncOperation.account.equals(paramAccount))) && ((paramString == null) || (localSyncOperation.authority.equals(paramString))) && (paramInt == localSyncOperation.userId))
            {
                localIterator.remove();
                if (!this.mSyncStorageEngine.deleteFromPending(localSyncOperation.pendingOperation))
                {
                    String str = "unable to find pending row for " + localSyncOperation;
                    Log.e("SyncManager", str, new IllegalStateException(str));
                }
            }
        }
    }

    public void remove(SyncOperation paramSyncOperation)
    {
        SyncOperation localSyncOperation = (SyncOperation)this.mOperationsMap.remove(paramSyncOperation.key);
        if (localSyncOperation == null);
        while (true)
        {
            return;
            if (!this.mSyncStorageEngine.deleteFromPending(localSyncOperation.pendingOperation))
            {
                String str = "unable to find pending row for " + localSyncOperation;
                Log.e("SyncManager", str, new IllegalStateException(str));
            }
        }
    }

    public void removeUser(int paramInt)
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator1 = this.mOperationsMap.values().iterator();
        while (localIterator1.hasNext())
        {
            SyncOperation localSyncOperation = (SyncOperation)localIterator1.next();
            if (localSyncOperation.userId == paramInt)
                localArrayList.add(localSyncOperation);
        }
        Iterator localIterator2 = localArrayList.iterator();
        while (localIterator2.hasNext())
            remove((SyncOperation)localIterator2.next());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncQueue
 * JD-Core Version:        0.6.2
 */