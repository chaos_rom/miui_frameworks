package android.content;

import java.util.Iterator;

public abstract interface EntityIterator extends Iterator<Entity>
{
    public abstract void close();

    public abstract void reset();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.EntityIterator
 * JD-Core Version:        0.6.2
 */