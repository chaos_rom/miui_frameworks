package android.content;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IIntentReceiver extends IInterface
{
    public abstract void performReceive(Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IIntentReceiver
    {
        private static final String DESCRIPTOR = "android.content.IIntentReceiver";
        static final int TRANSACTION_performReceive = 1;

        public Stub()
        {
            attachInterface(this, "android.content.IIntentReceiver");
        }

        public static IIntentReceiver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.IIntentReceiver");
                if ((localIInterface != null) && ((localIInterface instanceof IIntentReceiver)))
                    localObject = (IIntentReceiver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.content.IIntentReceiver");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.content.IIntentReceiver");
            Intent localIntent;
            label82: int i;
            String str;
            Bundle localBundle;
            label115: boolean bool2;
            if (paramParcel1.readInt() != 0)
            {
                localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel1);
                i = paramParcel1.readInt();
                str = paramParcel1.readString();
                if (paramParcel1.readInt() == 0)
                    break label162;
                localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                if (paramParcel1.readInt() == 0)
                    break label168;
                bool2 = bool1;
                label126: if (paramParcel1.readInt() == 0)
                    break label174;
            }
            label162: label168: label174: for (boolean bool3 = bool1; ; bool3 = false)
            {
                performReceive(localIntent, i, str, localBundle, bool2, bool3);
                break;
                localIntent = null;
                break label82;
                localBundle = null;
                break label115;
                bool2 = false;
                break label126;
            }
        }

        private static class Proxy
            implements IIntentReceiver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.IIntentReceiver";
            }

            public void performReceive(Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.content.IIntentReceiver");
                        if (paramIntent != null)
                        {
                            localParcel.writeInt(1);
                            paramIntent.writeToParcel(localParcel, 0);
                            localParcel.writeInt(paramInt);
                            localParcel.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                break label146;
                                localParcel.writeInt(j);
                                if (!paramBoolean2)
                                    break label140;
                                label78: localParcel.writeInt(i);
                                this.mRemote.transact(1, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                    label140: label146: 
                    while (!paramBoolean1)
                    {
                        j = 0;
                        break;
                        i = 0;
                        break label78;
                    }
                    int j = i;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IIntentReceiver
 * JD-Core Version:        0.6.2
 */