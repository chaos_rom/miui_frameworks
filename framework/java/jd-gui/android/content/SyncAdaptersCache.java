package android.content;

import android.content.pm.RegisteredServicesCache;
import android.content.pm.XmlSerializerAndParser;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class SyncAdaptersCache extends RegisteredServicesCache<SyncAdapterType>
{
    private static final String ATTRIBUTES_NAME = "sync-adapter";
    private static final String SERVICE_INTERFACE = "android.content.SyncAdapter";
    private static final String SERVICE_META_DATA = "android.content.SyncAdapter";
    private static final String TAG = "Account";
    private static final MySerializer sSerializer = new MySerializer();

    SyncAdaptersCache(Context paramContext)
    {
        super(paramContext, "android.content.SyncAdapter", "android.content.SyncAdapter", "sync-adapter", sSerializer);
    }

    public SyncAdapterType parseServiceAttributes(Resources paramResources, String paramString, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.SyncAdapter);
        try
        {
            String str1 = localTypedArray.getString(2);
            String str2 = localTypedArray.getString(1);
            SyncAdapterType localSyncAdapterType;
            if ((str1 == null) || (str2 == null))
                localSyncAdapterType = null;
            while (true)
            {
                return localSyncAdapterType;
                localSyncAdapterType = new SyncAdapterType(str1, str2, localTypedArray.getBoolean(3, true), localTypedArray.getBoolean(4, true), localTypedArray.getBoolean(6, false), localTypedArray.getBoolean(5, false), localTypedArray.getString(0));
                localTypedArray.recycle();
            }
        }
        finally
        {
            localTypedArray.recycle();
        }
    }

    static class MySerializer
        implements XmlSerializerAndParser<SyncAdapterType>
    {
        public SyncAdapterType createFromXml(XmlPullParser paramXmlPullParser)
            throws IOException, XmlPullParserException
        {
            return SyncAdapterType.newKey(paramXmlPullParser.getAttributeValue(null, "authority"), paramXmlPullParser.getAttributeValue(null, "accountType"));
        }

        public void writeAsXml(SyncAdapterType paramSyncAdapterType, XmlSerializer paramXmlSerializer)
            throws IOException
        {
            paramXmlSerializer.attribute(null, "authority", paramSyncAdapterType.authority);
            paramXmlSerializer.attribute(null, "accountType", paramSyncAdapterType.accountType);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SyncAdaptersCache
 * JD-Core Version:        0.6.2
 */