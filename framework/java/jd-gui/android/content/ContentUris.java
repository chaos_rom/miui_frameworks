package android.content;

import android.net.Uri;
import android.net.Uri.Builder;

public class ContentUris
{
    public static Uri.Builder appendId(Uri.Builder paramBuilder, long paramLong)
    {
        return paramBuilder.appendEncodedPath(String.valueOf(paramLong));
    }

    public static long parseId(Uri paramUri)
    {
        String str = paramUri.getLastPathSegment();
        if (str == null);
        for (long l = -1L; ; l = Long.parseLong(str))
            return l;
    }

    public static Uri withAppendedId(Uri paramUri, long paramLong)
    {
        return appendId(paramUri.buildUpon(), paramLong).build();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentUris
 * JD-Core Version:        0.6.2
 */