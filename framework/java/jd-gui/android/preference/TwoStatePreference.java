package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.TextView;

public abstract class TwoStatePreference extends Preference
{
    boolean mChecked;
    private boolean mDisableDependentsState;
    private boolean mSendClickAccessibilityEvent;
    private CharSequence mSummaryOff;
    private CharSequence mSummaryOn;

    public TwoStatePreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public TwoStatePreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public TwoStatePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean getDisableDependentsState()
    {
        return this.mDisableDependentsState;
    }

    public CharSequence getSummaryOff()
    {
        return this.mSummaryOff;
    }

    public CharSequence getSummaryOn()
    {
        return this.mSummaryOn;
    }

    public boolean isChecked()
    {
        return this.mChecked;
    }

    protected void onClick()
    {
        super.onClick();
        boolean bool;
        if (!isChecked())
        {
            bool = true;
            this.mSendClickAccessibilityEvent = true;
            if (callChangeListener(Boolean.valueOf(bool)))
                break label35;
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label35: setChecked(bool);
        }
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return Boolean.valueOf(paramTypedArray.getBoolean(paramInt, false));
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class)))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            setChecked(localSavedState.checked);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.checked = isChecked();
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        if (paramBoolean);
        for (boolean bool = getPersistedBoolean(this.mChecked); ; bool = ((Boolean)paramObject).booleanValue())
        {
            setChecked(bool);
            return;
        }
    }

    void sendAccessibilityEvent(View paramView)
    {
        AccessibilityManager localAccessibilityManager = AccessibilityManager.getInstance(getContext());
        if ((this.mSendClickAccessibilityEvent) && (localAccessibilityManager.isEnabled()))
        {
            AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain();
            localAccessibilityEvent.setEventType(1);
            paramView.onInitializeAccessibilityEvent(localAccessibilityEvent);
            paramView.dispatchPopulateAccessibilityEvent(localAccessibilityEvent);
            localAccessibilityManager.sendAccessibilityEvent(localAccessibilityEvent);
        }
        this.mSendClickAccessibilityEvent = false;
    }

    public void setChecked(boolean paramBoolean)
    {
        if (this.mChecked != paramBoolean)
        {
            this.mChecked = paramBoolean;
            persistBoolean(paramBoolean);
            notifyDependencyChange(shouldDisableDependents());
            notifyChanged();
        }
    }

    public void setDisableDependentsState(boolean paramBoolean)
    {
        this.mDisableDependentsState = paramBoolean;
    }

    public void setSummaryOff(int paramInt)
    {
        setSummaryOff(getContext().getString(paramInt));
    }

    public void setSummaryOff(CharSequence paramCharSequence)
    {
        this.mSummaryOff = paramCharSequence;
        if (!isChecked())
            notifyChanged();
    }

    public void setSummaryOn(int paramInt)
    {
        setSummaryOn(getContext().getString(paramInt));
    }

    public void setSummaryOn(CharSequence paramCharSequence)
    {
        this.mSummaryOn = paramCharSequence;
        if (isChecked())
            notifyChanged();
    }

    public boolean shouldDisableDependents()
    {
        boolean bool1 = false;
        boolean bool2;
        if (this.mDisableDependentsState)
            bool2 = this.mChecked;
        while (true)
        {
            if ((bool2) || (super.shouldDisableDependents()))
                bool1 = true;
            return bool1;
            if (!this.mChecked)
                bool2 = true;
            else
                bool2 = false;
        }
    }

    void syncSummaryView(View paramView)
    {
        TextView localTextView = (TextView)paramView.findViewById(16908304);
        if (localTextView != null)
        {
            i = 1;
            if ((!this.mChecked) || (this.mSummaryOn == null))
                break label90;
            localTextView.setText(this.mSummaryOn);
        }
        for (int i = 0; ; i = 0)
        {
            label90: 
            do
            {
                if (i != 0)
                {
                    CharSequence localCharSequence = getSummary();
                    if (localCharSequence != null)
                    {
                        localTextView.setText(localCharSequence);
                        i = 0;
                    }
                }
                int j = 8;
                if (i == 0)
                    j = 0;
                if (j != localTextView.getVisibility())
                    localTextView.setVisibility(j);
                return;
            }
            while ((this.mChecked) || (this.mSummaryOff == null));
            localTextView.setText(this.mSummaryOff);
        }
    }

    static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public TwoStatePreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new TwoStatePreference.SavedState(paramAnonymousParcel);
            }

            public TwoStatePreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new TwoStatePreference.SavedState[paramAnonymousInt];
            }
        };
        boolean checked;

        public SavedState(Parcel paramParcel)
        {
            super();
            if (paramParcel.readInt() == i);
            while (true)
            {
                this.checked = i;
                return;
                i = 0;
            }
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            if (this.checked);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.TwoStatePreference
 * JD-Core Version:        0.6.2
 */