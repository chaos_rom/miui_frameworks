package android.preference;

abstract interface OnDependencyChangeListener
{
    public abstract void onDependencyChanged(Preference paramPreference, boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.OnDependencyChangeListener
 * JD-Core Version:        0.6.2
 */