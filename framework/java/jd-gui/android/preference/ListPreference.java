package android.preference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class ListPreference extends DialogPreference
{
    private int mClickedDialogEntryIndex;
    private CharSequence[] mEntries;
    private CharSequence[] mEntryValues;
    private String mSummary;
    private String mValue;

    public ListPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public ListPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListPreference, 0, 0);
        this.mEntries = localTypedArray1.getTextArray(0);
        this.mEntryValues = localTypedArray1.getTextArray(1);
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, 0, 0);
        this.mSummary = localTypedArray2.getString(7);
        localTypedArray2.recycle();
    }

    private int getValueIndex()
    {
        return findIndexOfValue(this.mValue);
    }

    public int findIndexOfValue(String paramString)
    {
        int i;
        if ((paramString != null) && (this.mEntryValues != null))
        {
            i = -1 + this.mEntryValues.length;
            if (i >= 0)
                if (!this.mEntryValues[i].equals(paramString));
        }
        while (true)
        {
            return i;
            i--;
            break;
            i = -1;
        }
    }

    public CharSequence[] getEntries()
    {
        return this.mEntries;
    }

    public CharSequence getEntry()
    {
        int i = getValueIndex();
        if ((i >= 0) && (this.mEntries != null));
        for (CharSequence localCharSequence = this.mEntries[i]; ; localCharSequence = null)
            return localCharSequence;
    }

    public CharSequence[] getEntryValues()
    {
        return this.mEntryValues;
    }

    public CharSequence getSummary()
    {
        CharSequence localCharSequence = getEntry();
        if ((this.mSummary == null) || (localCharSequence == null));
        String str;
        Object[] arrayOfObject;
        for (Object localObject = super.getSummary(); ; localObject = String.format(str, arrayOfObject))
        {
            return localObject;
            str = this.mSummary;
            arrayOfObject = new Object[1];
            arrayOfObject[0] = localCharSequence;
        }
    }

    public String getValue()
    {
        return this.mValue;
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
        super.onDialogClosed(paramBoolean);
        if ((paramBoolean) && (this.mClickedDialogEntryIndex >= 0) && (this.mEntryValues != null))
        {
            String str = this.mEntryValues[this.mClickedDialogEntryIndex].toString();
            if (callChangeListener(str))
                setValue(str);
        }
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return paramTypedArray.getString(paramInt);
    }

    protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder)
    {
        super.onPrepareDialogBuilder(paramBuilder);
        if ((this.mEntries == null) || (this.mEntryValues == null))
            throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
        this.mClickedDialogEntryIndex = getValueIndex();
        paramBuilder.setSingleChoiceItems(this.mEntries, this.mClickedDialogEntryIndex, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
                ListPreference.access$002(ListPreference.this, paramAnonymousInt);
                ListPreference.this.onClick(paramAnonymousDialogInterface, -1);
                paramAnonymousDialogInterface.dismiss();
            }
        });
        paramBuilder.setPositiveButton(null, null);
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class)))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            setValue(localSavedState.value);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.value = getValue();
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        if (paramBoolean);
        for (String str = getPersistedString(this.mValue); ; str = (String)paramObject)
        {
            setValue(str);
            return;
        }
    }

    public void setEntries(int paramInt)
    {
        setEntries(getContext().getResources().getTextArray(paramInt));
    }

    public void setEntries(CharSequence[] paramArrayOfCharSequence)
    {
        this.mEntries = paramArrayOfCharSequence;
    }

    public void setEntryValues(int paramInt)
    {
        setEntryValues(getContext().getResources().getTextArray(paramInt));
    }

    public void setEntryValues(CharSequence[] paramArrayOfCharSequence)
    {
        this.mEntryValues = paramArrayOfCharSequence;
    }

    public void setSummary(CharSequence paramCharSequence)
    {
        super.setSummary(paramCharSequence);
        if ((paramCharSequence == null) && (this.mSummary != null))
            this.mSummary = null;
        while (true)
        {
            return;
            if ((paramCharSequence != null) && (!paramCharSequence.equals(this.mSummary)))
                this.mSummary = paramCharSequence.toString();
        }
    }

    public void setValue(String paramString)
    {
        this.mValue = paramString;
        persistString(paramString);
    }

    public void setValueIndex(int paramInt)
    {
        if (this.mEntryValues != null)
            setValue(this.mEntryValues[paramInt].toString());
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public ListPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ListPreference.SavedState(paramAnonymousParcel);
            }

            public ListPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new ListPreference.SavedState[paramAnonymousInt];
            }
        };
        String value;

        public SavedState(Parcel paramParcel)
        {
            super();
            this.value = paramParcel.readString();
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeString(this.value);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.ListPreference
 * JD-Core Version:        0.6.2
 */