package android.preference;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

public class SeekBarDialogPreference extends DialogPreference
{
    private static final String TAG = "SeekBarDialogPreference";
    private Drawable mMyIcon;

    public SeekBarDialogPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        setDialogLayoutResource(17367203);
        createActionButtons();
        this.mMyIcon = getDialogIcon();
        setDialogIcon(null);
    }

    protected static SeekBar getSeekBar(View paramView)
    {
        return (SeekBar)paramView.findViewById(16909068);
    }

    public void createActionButtons()
    {
        setPositiveButtonText(17039370);
        setNegativeButtonText(17039360);
    }

    protected void onBindDialogView(View paramView)
    {
        super.onBindDialogView(paramView);
        ImageView localImageView = (ImageView)paramView.findViewById(16908294);
        if (this.mMyIcon != null)
            localImageView.setImageDrawable(this.mMyIcon);
        while (true)
        {
            return;
            localImageView.setVisibility(8);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.SeekBarDialogPreference
 * JD-Core Version:        0.6.2
 */