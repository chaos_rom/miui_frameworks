package android.preference;

import android.content.Context;
import android.util.AttributeSet;

public class PreferenceCategory extends PreferenceGroup
{
    private static final String TAG = "PreferenceCategory";

    public PreferenceCategory(Context paramContext)
    {
        this(paramContext, null);
    }

    public PreferenceCategory(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842892);
    }

    public PreferenceCategory(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean isEnabled()
    {
        return false;
    }

    protected boolean onPrepareAddPreference(Preference paramPreference)
    {
        if ((paramPreference instanceof PreferenceCategory))
            throw new IllegalArgumentException("Cannot add a PreferenceCategory directly to a PreferenceCategory");
        return super.onPrepareAddPreference(paramPreference);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceCategory
 * JD-Core Version:        0.6.2
 */