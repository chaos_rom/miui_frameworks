package android.preference;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.android.internal.R.styleable;

public class PreferenceFrameLayout extends FrameLayout
{
    private static final int DEFAULT_BORDER_BOTTOM;
    private static final int DEFAULT_BORDER_LEFT;
    private static final int DEFAULT_BORDER_RIGHT;
    private static final int DEFAULT_BORDER_TOP;
    private final int mBorderBottom;
    private final int mBorderLeft;
    private final int mBorderRight;
    private final int mBorderTop;
    private boolean mPaddingApplied;

    public PreferenceFrameLayout(Context paramContext)
    {
        this(paramContext, null);
    }

    public PreferenceFrameLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843737);
    }

    public PreferenceFrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PreferenceFrameLayout, paramInt, 0);
        float f = paramContext.getResources().getDisplayMetrics().density;
        int i = (int)(0.5F + f * 0.0F);
        int j = (int)(0.5F + f * 0.0F);
        int k = (int)(0.5F + f * 0.0F);
        int m = (int)(0.5F + f * 0.0F);
        this.mBorderTop = localTypedArray.getDimensionPixelSize(0, i);
        this.mBorderBottom = localTypedArray.getDimensionPixelSize(1, j);
        this.mBorderLeft = localTypedArray.getDimensionPixelSize(2, k);
        this.mBorderRight = localTypedArray.getDimensionPixelSize(3, m);
        localTypedArray.recycle();
    }

    public void addView(View paramView)
    {
        int i = getPaddingTop();
        int j = getPaddingBottom();
        int k = getPaddingLeft();
        int m = getPaddingRight();
        LayoutParams localLayoutParams;
        if ((paramView.getLayoutParams() instanceof LayoutParams))
        {
            localLayoutParams = (LayoutParams)paramView.getLayoutParams();
            if ((localLayoutParams == null) || (!localLayoutParams.removeBorders))
                break label170;
            if (this.mPaddingApplied)
            {
                i -= this.mBorderTop;
                j -= this.mBorderBottom;
                k -= this.mBorderLeft;
                m -= this.mBorderRight;
                this.mPaddingApplied = false;
            }
        }
        while (true)
        {
            int n = getPaddingTop();
            int i1 = getPaddingBottom();
            int i2 = getPaddingLeft();
            int i3 = getPaddingRight();
            if ((n != i) || (i1 != j) || (i2 != k) || (i3 != m))
                setPadding(k, i, m, j);
            super.addView(paramView);
            return;
            localLayoutParams = null;
            break;
            label170: if (!this.mPaddingApplied)
            {
                i += this.mBorderTop;
                j += this.mBorderBottom;
                k += this.mBorderLeft;
                m += this.mBorderRight;
                this.mPaddingApplied = true;
            }
        }
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    public static class LayoutParams extends FrameLayout.LayoutParams
    {
        public boolean removeBorders = false;

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PreferenceFrameLayout_Layout);
            this.removeBorders = localTypedArray.getBoolean(0, false);
            localTypedArray.recycle();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceFrameLayout
 * JD-Core Version:        0.6.2
 */