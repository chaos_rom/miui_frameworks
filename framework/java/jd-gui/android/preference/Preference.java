package android.preference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.R.styleable;
import com.android.internal.util.CharSequences;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Preference
    implements Comparable<Preference>, OnDependencyChangeListener
{
    public static final int DEFAULT_ORDER = 2147483647;
    private boolean mBaseMethodCalled;
    private Context mContext;
    private Object mDefaultValue;
    private String mDependencyKey;
    private boolean mDependencyMet = true;
    private List<Preference> mDependents;
    private boolean mEnabled = true;
    private Bundle mExtras;
    private String mFragment;
    private boolean mHasSpecifiedLayout = false;
    private Drawable mIcon;
    private int mIconResId;
    private long mId;
    private Intent mIntent;
    private String mKey;
    private int mLayoutResId = 17367166;
    private OnPreferenceChangeInternalListener mListener;
    private OnPreferenceChangeListener mOnChangeListener;
    private OnPreferenceClickListener mOnClickListener;
    private int mOrder = 2147483647;
    private boolean mPersistent = true;
    private PreferenceManager mPreferenceManager;
    private boolean mRequiresKey;
    private boolean mSelectable = true;
    private boolean mShouldDisableView = true;
    private CharSequence mSummary;
    private CharSequence mTitle;
    private int mTitleRes;
    private int mWidgetLayoutResId;

    public Preference(Context paramContext)
    {
        this(paramContext, null);
    }

    public Preference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842894);
    }

    public Preference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        this.mContext = paramContext;
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, paramInt, 0);
        int i = localTypedArray.getIndexCount();
        if (i >= 0)
        {
            int j = localTypedArray.getIndex(i);
            switch (j)
            {
            default:
            case 0:
            case 6:
            case 4:
            case 7:
            case 8:
            case 13:
            case 3:
            case 9:
            case 2:
            case 5:
            case 1:
            case 10:
            case 11:
            case 12:
            }
            while (true)
            {
                i--;
                break;
                this.mIconResId = localTypedArray.getResourceId(j, 0);
                continue;
                this.mKey = localTypedArray.getString(j);
                continue;
                this.mTitleRes = localTypedArray.getResourceId(j, 0);
                this.mTitle = localTypedArray.getString(j);
                continue;
                this.mSummary = localTypedArray.getString(j);
                continue;
                this.mOrder = localTypedArray.getInt(j, this.mOrder);
                continue;
                this.mFragment = localTypedArray.getString(j);
                continue;
                this.mLayoutResId = localTypedArray.getResourceId(j, this.mLayoutResId);
                continue;
                this.mWidgetLayoutResId = localTypedArray.getResourceId(j, this.mWidgetLayoutResId);
                continue;
                this.mEnabled = localTypedArray.getBoolean(j, true);
                continue;
                this.mSelectable = localTypedArray.getBoolean(j, true);
                continue;
                this.mPersistent = localTypedArray.getBoolean(j, this.mPersistent);
                continue;
                this.mDependencyKey = localTypedArray.getString(j);
                continue;
                this.mDefaultValue = onGetDefaultValue(localTypedArray, j);
                continue;
                this.mShouldDisableView = localTypedArray.getBoolean(j, this.mShouldDisableView);
            }
        }
        localTypedArray.recycle();
        if (!getClass().getName().startsWith("android.preference"))
            this.mHasSpecifiedLayout = true;
    }

    private void dispatchSetInitialValue()
    {
        if ((!shouldPersist()) || (!getSharedPreferences().contains(this.mKey)))
            if (this.mDefaultValue != null)
                onSetInitialValue(false, this.mDefaultValue);
        while (true)
        {
            return;
            onSetInitialValue(true, null);
        }
    }

    private void registerDependency()
    {
        if (TextUtils.isEmpty(this.mDependencyKey));
        while (true)
        {
            return;
            Preference localPreference = findPreferenceInHierarchy(this.mDependencyKey);
            if (localPreference == null)
                break;
            localPreference.registerDependent(this);
        }
        throw new IllegalStateException("Dependency \"" + this.mDependencyKey + "\" not found for preference \"" + this.mKey + "\" (title: \"" + this.mTitle + "\"");
    }

    private void registerDependent(Preference paramPreference)
    {
        if (this.mDependents == null)
            this.mDependents = new ArrayList();
        this.mDependents.add(paramPreference);
        paramPreference.onDependencyChanged(this, shouldDisableDependents());
    }

    private void setEnabledStateOnViews(View paramView, boolean paramBoolean)
    {
        paramView.setEnabled(paramBoolean);
        if ((paramView instanceof ViewGroup))
        {
            ViewGroup localViewGroup = (ViewGroup)paramView;
            for (int i = -1 + localViewGroup.getChildCount(); i >= 0; i--)
                setEnabledStateOnViews(localViewGroup.getChildAt(i), paramBoolean);
        }
    }

    private void tryCommit(SharedPreferences.Editor paramEditor)
    {
        if (this.mPreferenceManager.shouldCommit());
        try
        {
            paramEditor.apply();
            return;
        }
        catch (AbstractMethodError localAbstractMethodError)
        {
            while (true)
                paramEditor.commit();
        }
    }

    private void unregisterDependency()
    {
        if (this.mDependencyKey != null)
        {
            Preference localPreference = findPreferenceInHierarchy(this.mDependencyKey);
            if (localPreference != null)
                localPreference.unregisterDependent(this);
        }
    }

    private void unregisterDependent(Preference paramPreference)
    {
        if (this.mDependents != null)
            this.mDependents.remove(paramPreference);
    }

    protected boolean callChangeListener(Object paramObject)
    {
        if (this.mOnChangeListener == null);
        for (boolean bool = true; ; bool = this.mOnChangeListener.onPreferenceChange(this, paramObject))
            return bool;
    }

    public int compareTo(Preference paramPreference)
    {
        int i;
        if ((this.mOrder != 2147483647) || ((this.mOrder == 2147483647) && (paramPreference.mOrder != 2147483647)))
            i = this.mOrder - paramPreference.mOrder;
        while (true)
        {
            return i;
            if (this.mTitle == null)
                i = 1;
            else if (paramPreference.mTitle == null)
                i = -1;
            else
                i = CharSequences.compareToIgnoreCase(this.mTitle, paramPreference.mTitle);
        }
    }

    void dispatchRestoreInstanceState(Bundle paramBundle)
    {
        if (hasKey())
        {
            Parcelable localParcelable = paramBundle.getParcelable(this.mKey);
            if (localParcelable != null)
            {
                this.mBaseMethodCalled = false;
                onRestoreInstanceState(localParcelable);
                if (!this.mBaseMethodCalled)
                    throw new IllegalStateException("Derived class did not call super.onRestoreInstanceState()");
            }
        }
    }

    void dispatchSaveInstanceState(Bundle paramBundle)
    {
        if (hasKey())
        {
            this.mBaseMethodCalled = false;
            Parcelable localParcelable = onSaveInstanceState();
            if (!this.mBaseMethodCalled)
                throw new IllegalStateException("Derived class did not call super.onSaveInstanceState()");
            if (localParcelable != null)
                paramBundle.putParcelable(this.mKey, localParcelable);
        }
    }

    protected Preference findPreferenceInHierarchy(String paramString)
    {
        if ((TextUtils.isEmpty(paramString)) || (this.mPreferenceManager == null));
        for (Preference localPreference = null; ; localPreference = this.mPreferenceManager.findPreference(paramString))
            return localPreference;
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public String getDependency()
    {
        return this.mDependencyKey;
    }

    public SharedPreferences.Editor getEditor()
    {
        if (this.mPreferenceManager == null);
        for (SharedPreferences.Editor localEditor = null; ; localEditor = this.mPreferenceManager.getEditor())
            return localEditor;
    }

    public Bundle getExtras()
    {
        if (this.mExtras == null)
            this.mExtras = new Bundle();
        return this.mExtras;
    }

    StringBuilder getFilterableStringBuilder()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        CharSequence localCharSequence1 = getTitle();
        if (!TextUtils.isEmpty(localCharSequence1))
            localStringBuilder.append(localCharSequence1).append(' ');
        CharSequence localCharSequence2 = getSummary();
        if (!TextUtils.isEmpty(localCharSequence2))
            localStringBuilder.append(localCharSequence2).append(' ');
        if (localStringBuilder.length() > 0)
            localStringBuilder.setLength(-1 + localStringBuilder.length());
        return localStringBuilder;
    }

    public String getFragment()
    {
        return this.mFragment;
    }

    public Drawable getIcon()
    {
        return this.mIcon;
    }

    long getId()
    {
        return this.mId;
    }

    public Intent getIntent()
    {
        return this.mIntent;
    }

    public String getKey()
    {
        return this.mKey;
    }

    public int getLayoutResource()
    {
        return this.mLayoutResId;
    }

    public OnPreferenceChangeListener getOnPreferenceChangeListener()
    {
        return this.mOnChangeListener;
    }

    public OnPreferenceClickListener getOnPreferenceClickListener()
    {
        return this.mOnClickListener;
    }

    public int getOrder()
    {
        return this.mOrder;
    }

    protected boolean getPersistedBoolean(boolean paramBoolean)
    {
        if (!shouldPersist());
        while (true)
        {
            return paramBoolean;
            paramBoolean = this.mPreferenceManager.getSharedPreferences().getBoolean(this.mKey, paramBoolean);
        }
    }

    protected float getPersistedFloat(float paramFloat)
    {
        if (!shouldPersist());
        while (true)
        {
            return paramFloat;
            paramFloat = this.mPreferenceManager.getSharedPreferences().getFloat(this.mKey, paramFloat);
        }
    }

    protected int getPersistedInt(int paramInt)
    {
        if (!shouldPersist());
        while (true)
        {
            return paramInt;
            paramInt = this.mPreferenceManager.getSharedPreferences().getInt(this.mKey, paramInt);
        }
    }

    protected long getPersistedLong(long paramLong)
    {
        if (!shouldPersist());
        while (true)
        {
            return paramLong;
            paramLong = this.mPreferenceManager.getSharedPreferences().getLong(this.mKey, paramLong);
        }
    }

    protected String getPersistedString(String paramString)
    {
        if (!shouldPersist());
        while (true)
        {
            return paramString;
            paramString = this.mPreferenceManager.getSharedPreferences().getString(this.mKey, paramString);
        }
    }

    protected Set<String> getPersistedStringSet(Set<String> paramSet)
    {
        if (!shouldPersist());
        while (true)
        {
            return paramSet;
            paramSet = this.mPreferenceManager.getSharedPreferences().getStringSet(this.mKey, paramSet);
        }
    }

    public PreferenceManager getPreferenceManager()
    {
        return this.mPreferenceManager;
    }

    public SharedPreferences getSharedPreferences()
    {
        if (this.mPreferenceManager == null);
        for (SharedPreferences localSharedPreferences = null; ; localSharedPreferences = this.mPreferenceManager.getSharedPreferences())
            return localSharedPreferences;
    }

    public boolean getShouldDisableView()
    {
        return this.mShouldDisableView;
    }

    public CharSequence getSummary()
    {
        return this.mSummary;
    }

    public CharSequence getTitle()
    {
        return this.mTitle;
    }

    public int getTitleRes()
    {
        return this.mTitleRes;
    }

    public View getView(View paramView, ViewGroup paramViewGroup)
    {
        if (paramView == null)
            paramView = onCreateView(paramViewGroup);
        onBindView(paramView);
        return paramView;
    }

    public int getWidgetLayoutResource()
    {
        return this.mWidgetLayoutResId;
    }

    public boolean hasKey()
    {
        if (!TextUtils.isEmpty(this.mKey));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean hasSpecifiedLayout()
    {
        return this.mHasSpecifiedLayout;
    }

    public boolean isEnabled()
    {
        if ((this.mEnabled) && (this.mDependencyMet));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPersistent()
    {
        return this.mPersistent;
    }

    public boolean isSelectable()
    {
        return this.mSelectable;
    }

    protected void notifyChanged()
    {
        if (this.mListener != null)
            this.mListener.onPreferenceChange(this);
    }

    public void notifyDependencyChange(boolean paramBoolean)
    {
        List localList = this.mDependents;
        if (localList == null);
        while (true)
        {
            return;
            int i = localList.size();
            for (int j = 0; j < i; j++)
                ((Preference)localList.get(j)).onDependencyChanged(this, paramBoolean);
        }
    }

    protected void notifyHierarchyChanged()
    {
        if (this.mListener != null)
            this.mListener.onPreferenceHierarchyChange(this);
    }

    protected void onAttachedToActivity()
    {
        registerDependency();
    }

    protected void onAttachedToHierarchy(PreferenceManager paramPreferenceManager)
    {
        this.mPreferenceManager = paramPreferenceManager;
        this.mId = paramPreferenceManager.getNextId();
        dispatchSetInitialValue();
    }

    protected void onBindView(View paramView)
    {
        int i = 0;
        TextView localTextView1 = (TextView)paramView.findViewById(16908310);
        TextView localTextView2;
        label86: ImageView localImageView;
        if (localTextView1 != null)
        {
            CharSequence localCharSequence2 = getTitle();
            if (!TextUtils.isEmpty(localCharSequence2))
            {
                localTextView1.setText(localCharSequence2);
                localTextView1.setVisibility(0);
            }
        }
        else
        {
            localTextView2 = (TextView)paramView.findViewById(16908304);
            if (localTextView2 != null)
            {
                CharSequence localCharSequence1 = getSummary();
                if (TextUtils.isEmpty(localCharSequence1))
                    break label197;
                localTextView2.setText(localCharSequence1);
                localTextView2.setVisibility(0);
            }
            localImageView = (ImageView)paramView.findViewById(16908294);
            if (localImageView != null)
            {
                if ((this.mIconResId != 0) || (this.mIcon != null))
                {
                    if (this.mIcon == null)
                        this.mIcon = getContext().getResources().getDrawable(this.mIconResId);
                    if (this.mIcon != null)
                        localImageView.setImageDrawable(this.mIcon);
                }
                if (this.mIcon == null)
                    break label207;
            }
        }
        while (true)
        {
            localImageView.setVisibility(i);
            if (this.mShouldDisableView)
                setEnabledStateOnViews(paramView, isEnabled());
            return;
            localTextView1.setVisibility(8);
            break;
            label197: localTextView2.setVisibility(8);
            break label86;
            label207: i = 8;
        }
    }

    protected void onClick()
    {
    }

    protected View onCreateView(ViewGroup paramViewGroup)
    {
        LayoutInflater localLayoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
        View localView = localLayoutInflater.inflate(this.mLayoutResId, paramViewGroup, false);
        ViewGroup localViewGroup = (ViewGroup)localView.findViewById(16908312);
        if (localViewGroup != null)
        {
            if (this.mWidgetLayoutResId == 0)
                break label62;
            localLayoutInflater.inflate(this.mWidgetLayoutResId, localViewGroup);
        }
        while (true)
        {
            return localView;
            label62: localViewGroup.setVisibility(8);
        }
    }

    public void onDependencyChanged(Preference paramPreference, boolean paramBoolean)
    {
        if (this.mDependencyMet == paramBoolean)
            if (paramBoolean)
                break label32;
        label32: for (boolean bool = true; ; bool = false)
        {
            this.mDependencyMet = bool;
            notifyDependencyChange(shouldDisableDependents());
            notifyChanged();
            return;
        }
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return null;
    }

    public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    protected void onPrepareForRemoval()
    {
        unregisterDependency();
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        this.mBaseMethodCalled = true;
        if ((paramParcelable != BaseSavedState.EMPTY_STATE) && (paramParcelable != null))
            throw new IllegalArgumentException("Wrong state class -- expecting Preference State");
    }

    protected Parcelable onSaveInstanceState()
    {
        this.mBaseMethodCalled = true;
        return BaseSavedState.EMPTY_STATE;
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
    }

    public Bundle peekExtras()
    {
        return this.mExtras;
    }

    void performClick(PreferenceScreen paramPreferenceScreen)
    {
        if (!isEnabled());
        while (true)
        {
            return;
            onClick();
            if ((this.mOnClickListener == null) || (!this.mOnClickListener.onPreferenceClick(this)))
            {
                PreferenceManager localPreferenceManager = getPreferenceManager();
                if (localPreferenceManager != null)
                {
                    PreferenceManager.OnPreferenceTreeClickListener localOnPreferenceTreeClickListener = localPreferenceManager.getOnPreferenceTreeClickListener();
                    if ((paramPreferenceScreen != null) && (localOnPreferenceTreeClickListener != null) && (localOnPreferenceTreeClickListener.onPreferenceTreeClick(paramPreferenceScreen, this)));
                }
                else if (this.mIntent != null)
                {
                    getContext().startActivity(this.mIntent);
                }
            }
        }
    }

    protected boolean persistBoolean(boolean paramBoolean)
    {
        boolean bool1 = false;
        boolean bool2 = true;
        if (shouldPersist())
        {
            if (!paramBoolean)
                bool1 = bool2;
            if (paramBoolean != getPersistedBoolean(bool1));
        }
        while (true)
        {
            return bool2;
            SharedPreferences.Editor localEditor = this.mPreferenceManager.getEditor();
            localEditor.putBoolean(this.mKey, paramBoolean);
            tryCommit(localEditor);
            continue;
            bool2 = false;
        }
    }

    protected boolean persistFloat(float paramFloat)
    {
        boolean bool = true;
        if (shouldPersist())
            if (paramFloat != getPersistedFloat((0.0F / 0.0F)));
        while (true)
        {
            return bool;
            SharedPreferences.Editor localEditor = this.mPreferenceManager.getEditor();
            localEditor.putFloat(this.mKey, paramFloat);
            tryCommit(localEditor);
            continue;
            bool = false;
        }
    }

    protected boolean persistInt(int paramInt)
    {
        boolean bool = true;
        if (shouldPersist())
            if (paramInt != getPersistedInt(paramInt ^ 0xFFFFFFFF));
        while (true)
        {
            return bool;
            SharedPreferences.Editor localEditor = this.mPreferenceManager.getEditor();
            localEditor.putInt(this.mKey, paramInt);
            tryCommit(localEditor);
            continue;
            bool = false;
        }
    }

    protected boolean persistLong(long paramLong)
    {
        boolean bool = true;
        if (shouldPersist())
            if (paramLong != getPersistedLong(0xFFFFFFFF ^ paramLong));
        while (true)
        {
            return bool;
            SharedPreferences.Editor localEditor = this.mPreferenceManager.getEditor();
            localEditor.putLong(this.mKey, paramLong);
            tryCommit(localEditor);
            continue;
            bool = false;
        }
    }

    protected boolean persistString(String paramString)
    {
        boolean bool = true;
        if (shouldPersist())
            if (paramString != getPersistedString(null));
        while (true)
        {
            return bool;
            SharedPreferences.Editor localEditor = this.mPreferenceManager.getEditor();
            localEditor.putString(this.mKey, paramString);
            tryCommit(localEditor);
            continue;
            bool = false;
        }
    }

    protected boolean persistStringSet(Set<String> paramSet)
    {
        boolean bool = true;
        if (shouldPersist())
            if (!paramSet.equals(getPersistedStringSet(null)));
        while (true)
        {
            return bool;
            SharedPreferences.Editor localEditor = this.mPreferenceManager.getEditor();
            localEditor.putStringSet(this.mKey, paramSet);
            tryCommit(localEditor);
            continue;
            bool = false;
        }
    }

    void requireKey()
    {
        if (this.mKey == null)
            throw new IllegalStateException("Preference does not have a key assigned.");
        this.mRequiresKey = true;
    }

    public void restoreHierarchyState(Bundle paramBundle)
    {
        dispatchRestoreInstanceState(paramBundle);
    }

    public void saveHierarchyState(Bundle paramBundle)
    {
        dispatchSaveInstanceState(paramBundle);
    }

    public void setDefaultValue(Object paramObject)
    {
        this.mDefaultValue = paramObject;
    }

    public void setDependency(String paramString)
    {
        unregisterDependency();
        this.mDependencyKey = paramString;
        registerDependency();
    }

    public void setEnabled(boolean paramBoolean)
    {
        if (this.mEnabled != paramBoolean)
        {
            this.mEnabled = paramBoolean;
            notifyDependencyChange(shouldDisableDependents());
            notifyChanged();
        }
    }

    public void setFragment(String paramString)
    {
        this.mFragment = paramString;
    }

    public void setIcon(int paramInt)
    {
        this.mIconResId = paramInt;
        setIcon(this.mContext.getResources().getDrawable(paramInt));
    }

    public void setIcon(Drawable paramDrawable)
    {
        if (((paramDrawable == null) && (this.mIcon != null)) || ((paramDrawable != null) && (this.mIcon != paramDrawable)))
        {
            this.mIcon = paramDrawable;
            notifyChanged();
        }
    }

    public void setIntent(Intent paramIntent)
    {
        this.mIntent = paramIntent;
    }

    public void setKey(String paramString)
    {
        this.mKey = paramString;
        if ((this.mRequiresKey) && (!hasKey()))
            requireKey();
    }

    public void setLayoutResource(int paramInt)
    {
        if (paramInt != this.mLayoutResId)
            this.mHasSpecifiedLayout = true;
        this.mLayoutResId = paramInt;
    }

    final void setOnPreferenceChangeInternalListener(OnPreferenceChangeInternalListener paramOnPreferenceChangeInternalListener)
    {
        this.mListener = paramOnPreferenceChangeInternalListener;
    }

    public void setOnPreferenceChangeListener(OnPreferenceChangeListener paramOnPreferenceChangeListener)
    {
        this.mOnChangeListener = paramOnPreferenceChangeListener;
    }

    public void setOnPreferenceClickListener(OnPreferenceClickListener paramOnPreferenceClickListener)
    {
        this.mOnClickListener = paramOnPreferenceClickListener;
    }

    public void setOrder(int paramInt)
    {
        if (paramInt != this.mOrder)
        {
            this.mOrder = paramInt;
            notifyHierarchyChanged();
        }
    }

    public void setPersistent(boolean paramBoolean)
    {
        this.mPersistent = paramBoolean;
    }

    public void setSelectable(boolean paramBoolean)
    {
        if (this.mSelectable != paramBoolean)
        {
            this.mSelectable = paramBoolean;
            notifyChanged();
        }
    }

    public void setShouldDisableView(boolean paramBoolean)
    {
        this.mShouldDisableView = paramBoolean;
        notifyChanged();
    }

    public void setSummary(int paramInt)
    {
        setSummary(this.mContext.getString(paramInt));
    }

    public void setSummary(CharSequence paramCharSequence)
    {
        if (((paramCharSequence == null) && (this.mSummary != null)) || ((paramCharSequence != null) && (!paramCharSequence.equals(this.mSummary))))
        {
            this.mSummary = paramCharSequence;
            notifyChanged();
        }
    }

    public void setTitle(int paramInt)
    {
        setTitle(this.mContext.getString(paramInt));
        this.mTitleRes = paramInt;
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        if (((paramCharSequence == null) && (this.mTitle != null)) || ((paramCharSequence != null) && (!paramCharSequence.equals(this.mTitle))))
        {
            this.mTitleRes = 0;
            this.mTitle = paramCharSequence;
            notifyChanged();
        }
    }

    public void setWidgetLayoutResource(int paramInt)
    {
        if (paramInt != this.mWidgetLayoutResId)
            this.mHasSpecifiedLayout = true;
        this.mWidgetLayoutResId = paramInt;
    }

    public boolean shouldCommit()
    {
        if (this.mPreferenceManager == null);
        for (boolean bool = false; ; bool = this.mPreferenceManager.shouldCommit())
            return bool;
    }

    public boolean shouldDisableDependents()
    {
        if (!isEnabled());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean shouldPersist()
    {
        if ((this.mPreferenceManager != null) && (isPersistent()) && (hasKey()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        return getFilterableStringBuilder().toString();
    }

    public static class BaseSavedState extends AbsSavedState
    {
        public static final Parcelable.Creator<BaseSavedState> CREATOR = new Parcelable.Creator()
        {
            public Preference.BaseSavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new Preference.BaseSavedState(paramAnonymousParcel);
            }

            public Preference.BaseSavedState[] newArray(int paramAnonymousInt)
            {
                return new Preference.BaseSavedState[paramAnonymousInt];
            }
        };

        public BaseSavedState(Parcel paramParcel)
        {
            super();
        }

        public BaseSavedState(Parcelable paramParcelable)
        {
            super();
        }
    }

    static abstract interface OnPreferenceChangeInternalListener
    {
        public abstract void onPreferenceChange(Preference paramPreference);

        public abstract void onPreferenceHierarchyChange(Preference paramPreference);
    }

    public static abstract interface OnPreferenceClickListener
    {
        public abstract boolean onPreferenceClick(Preference paramPreference);
    }

    public static abstract interface OnPreferenceChangeListener
    {
        public abstract boolean onPreferenceChange(Preference paramPreference, Object paramObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.Preference
 * JD-Core Version:        0.6.2
 */