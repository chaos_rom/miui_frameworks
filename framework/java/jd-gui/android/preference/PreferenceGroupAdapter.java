package android.preference;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class PreferenceGroupAdapter extends BaseAdapter
    implements Preference.OnPreferenceChangeInternalListener
{
    private static final String TAG = "PreferenceGroupAdapter";
    private Handler mHandler = new Handler();
    private boolean mHasReturnedViewTypeCount = false;
    private volatile boolean mIsSyncing = false;
    private PreferenceGroup mPreferenceGroup;
    private ArrayList<PreferenceLayout> mPreferenceLayouts;
    private List<Preference> mPreferenceList;
    private Runnable mSyncRunnable = new Runnable()
    {
        public void run()
        {
            PreferenceGroupAdapter.this.syncMyPreferences();
        }
    };
    private PreferenceLayout mTempPreferenceLayout = new PreferenceLayout(null);

    public PreferenceGroupAdapter(PreferenceGroup paramPreferenceGroup)
    {
        this.mPreferenceGroup = paramPreferenceGroup;
        this.mPreferenceGroup.setOnPreferenceChangeInternalListener(this);
        this.mPreferenceList = new ArrayList();
        this.mPreferenceLayouts = new ArrayList();
        syncMyPreferences();
    }

    private void addPreferenceClassName(Preference paramPreference)
    {
        PreferenceLayout localPreferenceLayout = createPreferenceLayout(paramPreference, null);
        int i = Collections.binarySearch(this.mPreferenceLayouts, localPreferenceLayout);
        if (i < 0)
        {
            int j = -1 + i * -1;
            this.mPreferenceLayouts.add(j, localPreferenceLayout);
        }
    }

    private PreferenceLayout createPreferenceLayout(Preference paramPreference, PreferenceLayout paramPreferenceLayout)
    {
        if (paramPreferenceLayout != null);
        for (PreferenceLayout localPreferenceLayout = paramPreferenceLayout; ; localPreferenceLayout = new PreferenceLayout(null))
        {
            PreferenceLayout.access$202(localPreferenceLayout, paramPreference.getClass().getName());
            PreferenceLayout.access$302(localPreferenceLayout, paramPreference.getLayoutResource());
            PreferenceLayout.access$402(localPreferenceLayout, paramPreference.getWidgetLayoutResource());
            return localPreferenceLayout;
        }
    }

    private void flattenPreferenceGroup(List<Preference> paramList, PreferenceGroup paramPreferenceGroup)
    {
        paramPreferenceGroup.sortPreferences();
        int i = paramPreferenceGroup.getPreferenceCount();
        for (int j = 0; j < i; j++)
        {
            Preference localPreference = paramPreferenceGroup.getPreference(j);
            paramList.add(localPreference);
            if ((!this.mHasReturnedViewTypeCount) && (!localPreference.hasSpecifiedLayout()))
                addPreferenceClassName(localPreference);
            if ((localPreference instanceof PreferenceGroup))
            {
                PreferenceGroup localPreferenceGroup = (PreferenceGroup)localPreference;
                if (localPreferenceGroup.isOnSameScreenAsChildren())
                    flattenPreferenceGroup(paramList, localPreferenceGroup);
            }
            localPreference.setOnPreferenceChangeInternalListener(this);
        }
    }

    private void syncMyPreferences()
    {
        try
        {
            if (this.mIsSyncing)
                return;
            this.mIsSyncing = true;
            ArrayList localArrayList = new ArrayList(this.mPreferenceList.size());
            flattenPreferenceGroup(localArrayList, this.mPreferenceGroup);
            this.mPreferenceList = localArrayList;
            notifyDataSetChanged();
            try
            {
                this.mIsSyncing = false;
                notifyAll();
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
        }
        finally
        {
        }
    }

    public boolean areAllItemsEnabled()
    {
        return false;
    }

    public int getCount()
    {
        return this.mPreferenceList.size();
    }

    public Preference getItem(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= getCount()));
        for (Preference localPreference = null; ; localPreference = (Preference)this.mPreferenceList.get(paramInt))
            return localPreference;
    }

    public long getItemId(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= getCount()));
        for (long l = -9223372036854775808L; ; l = getItem(paramInt).getId())
            return l;
    }

    public int getItemViewType(int paramInt)
    {
        if (!this.mHasReturnedViewTypeCount)
            this.mHasReturnedViewTypeCount = true;
        Preference localPreference = getItem(paramInt);
        int i;
        if (localPreference.hasSpecifiedLayout())
            i = -1;
        while (true)
        {
            return i;
            this.mTempPreferenceLayout = createPreferenceLayout(localPreference, this.mTempPreferenceLayout);
            i = Collections.binarySearch(this.mPreferenceLayouts, this.mTempPreferenceLayout);
            if (i < 0)
                i = -1;
        }
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        Preference localPreference = getItem(paramInt);
        this.mTempPreferenceLayout = createPreferenceLayout(localPreference, this.mTempPreferenceLayout);
        if (Collections.binarySearch(this.mPreferenceLayouts, this.mTempPreferenceLayout) < 0)
            paramView = null;
        return localPreference.getView(paramView, paramViewGroup);
    }

    public int getViewTypeCount()
    {
        if (!this.mHasReturnedViewTypeCount)
            this.mHasReturnedViewTypeCount = true;
        return Math.max(1, this.mPreferenceLayouts.size());
    }

    public boolean hasStableIds()
    {
        return true;
    }

    public boolean isEnabled(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= getCount()));
        for (boolean bool = true; ; bool = getItem(paramInt).isSelectable())
            return bool;
    }

    public void onPreferenceChange(Preference paramPreference)
    {
        notifyDataSetChanged();
    }

    public void onPreferenceHierarchyChange(Preference paramPreference)
    {
        this.mHandler.removeCallbacks(this.mSyncRunnable);
        this.mHandler.post(this.mSyncRunnable);
    }

    private static class PreferenceLayout
        implements Comparable<PreferenceLayout>
    {
        private String name;
        private int resId;
        private int widgetResId;

        public int compareTo(PreferenceLayout paramPreferenceLayout)
        {
            int i = this.name.compareTo(paramPreferenceLayout.name);
            if (i == 0)
            {
                if (this.resId != paramPreferenceLayout.resId)
                    break label55;
                if (this.widgetResId != paramPreferenceLayout.widgetResId)
                    break label42;
                i = 0;
            }
            while (true)
            {
                return i;
                label42: i = this.widgetResId - paramPreferenceLayout.widgetResId;
                continue;
                label55: i = this.resId - paramPreferenceLayout.resId;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceGroupAdapter
 * JD-Core Version:        0.6.2
 */