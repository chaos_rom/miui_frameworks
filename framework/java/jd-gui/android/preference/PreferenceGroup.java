package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class PreferenceGroup extends Preference
    implements GenericInflater.Parent<Preference>
{
    private boolean mAttachedToActivity = false;
    private int mCurrentPreferenceOrder = 0;
    private boolean mOrderingAsAdded = true;
    private List<Preference> mPreferenceList = new ArrayList();

    public PreferenceGroup(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public PreferenceGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PreferenceGroup, paramInt, 0);
        this.mOrderingAsAdded = localTypedArray.getBoolean(0, this.mOrderingAsAdded);
        localTypedArray.recycle();
    }

    private boolean removePreferenceInt(Preference paramPreference)
    {
        try
        {
            paramPreference.onPrepareForRemoval();
            boolean bool = this.mPreferenceList.remove(paramPreference);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void addItemFromInflater(Preference paramPreference)
    {
        addPreference(paramPreference);
    }

    public boolean addPreference(Preference paramPreference)
    {
        boolean bool;
        if (this.mPreferenceList.contains(paramPreference))
            bool = true;
        while (true)
        {
            return bool;
            if (paramPreference.getOrder() == 2147483647)
            {
                if (this.mOrderingAsAdded)
                {
                    int j = this.mCurrentPreferenceOrder;
                    this.mCurrentPreferenceOrder = (j + 1);
                    paramPreference.setOrder(j);
                }
                if ((paramPreference instanceof PreferenceGroup))
                    ((PreferenceGroup)paramPreference).setOrderingAsAdded(this.mOrderingAsAdded);
            }
            int i = Collections.binarySearch(this.mPreferenceList, paramPreference);
            if (i < 0)
                i = -1 + i * -1;
            if (!onPrepareAddPreference(paramPreference))
            {
                bool = false;
                continue;
            }
            try
            {
                this.mPreferenceList.add(i, paramPreference);
                paramPreference.onAttachedToHierarchy(getPreferenceManager());
                if (this.mAttachedToActivity)
                    paramPreference.onAttachedToActivity();
                notifyHierarchyChanged();
                bool = true;
            }
            finally
            {
            }
        }
    }

    protected void dispatchRestoreInstanceState(Bundle paramBundle)
    {
        super.dispatchRestoreInstanceState(paramBundle);
        int i = getPreferenceCount();
        for (int j = 0; j < i; j++)
            getPreference(j).dispatchRestoreInstanceState(paramBundle);
    }

    protected void dispatchSaveInstanceState(Bundle paramBundle)
    {
        super.dispatchSaveInstanceState(paramBundle);
        int i = getPreferenceCount();
        for (int j = 0; j < i; j++)
            getPreference(j).dispatchSaveInstanceState(paramBundle);
    }

    public Preference findPreference(CharSequence paramCharSequence)
    {
        if (TextUtils.equals(getKey(), paramCharSequence));
        while (true)
        {
            return this;
            int i = getPreferenceCount();
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label95;
                Preference localPreference1 = getPreference(j);
                String str = localPreference1.getKey();
                if ((str != null) && (str.equals(paramCharSequence)))
                {
                    this = localPreference1;
                    break;
                }
                if ((localPreference1 instanceof PreferenceGroup))
                {
                    Preference localPreference2 = ((PreferenceGroup)localPreference1).findPreference(paramCharSequence);
                    if (localPreference2 != null)
                    {
                        this = localPreference2;
                        break;
                    }
                }
            }
            label95: this = null;
        }
    }

    public Preference getPreference(int paramInt)
    {
        return (Preference)this.mPreferenceList.get(paramInt);
    }

    public int getPreferenceCount()
    {
        return this.mPreferenceList.size();
    }

    protected boolean isOnSameScreenAsChildren()
    {
        return true;
    }

    public boolean isOrderingAsAdded()
    {
        return this.mOrderingAsAdded;
    }

    protected void onAttachedToActivity()
    {
        super.onAttachedToActivity();
        this.mAttachedToActivity = true;
        int i = getPreferenceCount();
        for (int j = 0; j < i; j++)
            getPreference(j).onAttachedToActivity();
    }

    protected boolean onPrepareAddPreference(Preference paramPreference)
    {
        if (!super.isEnabled())
            paramPreference.setEnabled(false);
        return true;
    }

    protected void onPrepareForRemoval()
    {
        super.onPrepareForRemoval();
        this.mAttachedToActivity = false;
    }

    public void removeAll()
    {
        try
        {
            List localList = this.mPreferenceList;
            for (int i = -1 + localList.size(); i >= 0; i--)
                removePreferenceInt((Preference)localList.get(0));
            notifyHierarchyChanged();
            return;
        }
        finally
        {
        }
    }

    public boolean removePreference(Preference paramPreference)
    {
        boolean bool = removePreferenceInt(paramPreference);
        notifyHierarchyChanged();
        return bool;
    }

    public void setEnabled(boolean paramBoolean)
    {
        super.setEnabled(paramBoolean);
        int i = getPreferenceCount();
        for (int j = 0; j < i; j++)
            getPreference(j).setEnabled(paramBoolean);
    }

    public void setOrderingAsAdded(boolean paramBoolean)
    {
        this.mOrderingAsAdded = paramBoolean;
    }

    void sortPreferences()
    {
        try
        {
            Collections.sort(this.mPreferenceList);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceGroup
 * JD-Core Version:        0.6.2
 */