package android.preference;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.AttributeSet;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class PreferenceInflater extends GenericInflater<Preference, PreferenceGroup>
{
    private static final String EXTRA_TAG_NAME = "extra";
    private static final String INTENT_TAG_NAME = "intent";
    private static final String TAG = "PreferenceInflater";
    private PreferenceManager mPreferenceManager;

    public PreferenceInflater(Context paramContext, PreferenceManager paramPreferenceManager)
    {
        super(paramContext);
        init(paramPreferenceManager);
    }

    PreferenceInflater(GenericInflater<Preference, PreferenceGroup> paramGenericInflater, PreferenceManager paramPreferenceManager, Context paramContext)
    {
        super(paramGenericInflater, paramContext);
        init(paramPreferenceManager);
    }

    private void init(PreferenceManager paramPreferenceManager)
    {
        this.mPreferenceManager = paramPreferenceManager;
        setDefaultPackage("android.preference.");
    }

    public GenericInflater<Preference, PreferenceGroup> cloneInContext(Context paramContext)
    {
        return new PreferenceInflater(this, this.mPreferenceManager, paramContext);
    }

    protected boolean onCreateCustomFromTag(XmlPullParser paramXmlPullParser, Preference paramPreference, AttributeSet paramAttributeSet)
        throws XmlPullParserException
    {
        boolean bool = true;
        String str = paramXmlPullParser.getName();
        if (str.equals("intent"));
        while (true)
        {
            try
            {
                Intent localIntent = Intent.parseIntent(getContext().getResources(), paramXmlPullParser, paramAttributeSet);
                if (localIntent != null)
                    paramPreference.setIntent(localIntent);
                return bool;
            }
            catch (IOException localIOException2)
            {
                XmlPullParserException localXmlPullParserException2 = new XmlPullParserException("Error parsing preference");
                localXmlPullParserException2.initCause(localIOException2);
                throw localXmlPullParserException2;
            }
            if (str.equals("extra"))
            {
                getContext().getResources().parseBundleExtra("extra", paramAttributeSet, paramPreference.getExtras());
                try
                {
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
                catch (IOException localIOException1)
                {
                    XmlPullParserException localXmlPullParserException1 = new XmlPullParserException("Error parsing preference");
                    localXmlPullParserException1.initCause(localIOException1);
                    throw localXmlPullParserException1;
                }
            }
            else
            {
                bool = false;
            }
        }
    }

    protected PreferenceGroup onMergeRoots(PreferenceGroup paramPreferenceGroup1, boolean paramBoolean, PreferenceGroup paramPreferenceGroup2)
    {
        if (paramPreferenceGroup1 == null)
            paramPreferenceGroup2.onAttachedToHierarchy(this.mPreferenceManager);
        while (true)
        {
            return paramPreferenceGroup2;
            paramPreferenceGroup2 = paramPreferenceGroup1;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceInflater
 * JD-Core Version:        0.6.2
 */