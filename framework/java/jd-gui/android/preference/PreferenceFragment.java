package android.preference;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.ListView;

public abstract class PreferenceFragment extends Fragment
    implements PreferenceManager.OnPreferenceTreeClickListener
{
    private static final int FIRST_REQUEST_CODE = 100;
    private static final int MSG_BIND_PREFERENCES = 1;
    private static final String PREFERENCES_TAG = "android:preferences";
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            }
            while (true)
            {
                return;
                PreferenceFragment.this.bindPreferences();
            }
        }
    };
    private boolean mHavePrefs;
    private boolean mInitDone;
    private ListView mList;
    private View.OnKeyListener mListOnKeyListener = new View.OnKeyListener()
    {
        public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
        {
            Object localObject = PreferenceFragment.this.mList.getSelectedItem();
            View localView;
            if ((localObject instanceof Preference))
                localView = PreferenceFragment.this.mList.getSelectedView();
            for (boolean bool = ((Preference)localObject).onKey(localView, paramAnonymousInt, paramAnonymousKeyEvent); ; bool = false)
                return bool;
        }
    };
    private PreferenceManager mPreferenceManager;
    private final Runnable mRequestFocus = new Runnable()
    {
        public void run()
        {
            PreferenceFragment.this.mList.focusableViewAvailable(PreferenceFragment.this.mList);
        }
    };

    private void bindPreferences()
    {
        PreferenceScreen localPreferenceScreen = getPreferenceScreen();
        if (localPreferenceScreen != null)
            localPreferenceScreen.bind(getListView());
    }

    private void ensureList()
    {
        if (this.mList != null);
        while (true)
        {
            return;
            View localView1 = getView();
            if (localView1 == null)
                throw new IllegalStateException("Content view not yet created");
            View localView2 = localView1.findViewById(16908298);
            if (!(localView2 instanceof ListView))
                throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
            this.mList = ((ListView)localView2);
            if (this.mList == null)
                throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
            this.mList.setOnKeyListener(this.mListOnKeyListener);
            this.mHandler.post(this.mRequestFocus);
        }
    }

    private void postBindPreferences()
    {
        if (this.mHandler.hasMessages(1));
        while (true)
        {
            return;
            this.mHandler.obtainMessage(1).sendToTarget();
        }
    }

    private void requirePreferenceManager()
    {
        if (this.mPreferenceManager == null)
            throw new RuntimeException("This should be called after super.onCreate.");
    }

    public void addPreferencesFromIntent(Intent paramIntent)
    {
        requirePreferenceManager();
        setPreferenceScreen(this.mPreferenceManager.inflateFromIntent(paramIntent, getPreferenceScreen()));
    }

    public void addPreferencesFromResource(int paramInt)
    {
        requirePreferenceManager();
        setPreferenceScreen(this.mPreferenceManager.inflateFromResource(getActivity(), paramInt, getPreferenceScreen()));
    }

    public Preference findPreference(CharSequence paramCharSequence)
    {
        if (this.mPreferenceManager == null);
        for (Preference localPreference = null; ; localPreference = this.mPreferenceManager.findPreference(paramCharSequence))
            return localPreference;
    }

    public ListView getListView()
    {
        ensureList();
        return this.mList;
    }

    public PreferenceManager getPreferenceManager()
    {
        return this.mPreferenceManager;
    }

    public PreferenceScreen getPreferenceScreen()
    {
        return this.mPreferenceManager.getPreferenceScreen();
    }

    public void onActivityCreated(Bundle paramBundle)
    {
        super.onActivityCreated(paramBundle);
        if (this.mHavePrefs)
            bindPreferences();
        this.mInitDone = true;
        if (paramBundle != null)
        {
            Bundle localBundle = paramBundle.getBundle("android:preferences");
            if (localBundle != null)
            {
                PreferenceScreen localPreferenceScreen = getPreferenceScreen();
                if (localPreferenceScreen != null)
                    localPreferenceScreen.restoreHierarchyState(localBundle);
            }
        }
    }

    public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        this.mPreferenceManager.dispatchActivityResult(paramInt1, paramInt2, paramIntent);
    }

    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        this.mPreferenceManager = new PreferenceManager(getActivity(), 100);
        this.mPreferenceManager.setFragment(this);
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
        return paramLayoutInflater.inflate(17367177, paramViewGroup, false);
    }

    public void onDestroy()
    {
        super.onDestroy();
        this.mPreferenceManager.dispatchActivityDestroy();
    }

    public void onDestroyView()
    {
        this.mList = null;
        this.mHandler.removeCallbacks(this.mRequestFocus);
        this.mHandler.removeMessages(1);
        super.onDestroyView();
    }

    public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference)
    {
        if ((paramPreference.getFragment() != null) && ((getActivity() instanceof OnPreferenceStartFragmentCallback)));
        for (boolean bool = ((OnPreferenceStartFragmentCallback)getActivity()).onPreferenceStartFragment(this, paramPreference); ; bool = false)
            return bool;
    }

    public void onSaveInstanceState(Bundle paramBundle)
    {
        super.onSaveInstanceState(paramBundle);
        PreferenceScreen localPreferenceScreen = getPreferenceScreen();
        if (localPreferenceScreen != null)
        {
            Bundle localBundle = new Bundle();
            localPreferenceScreen.saveHierarchyState(localBundle);
            paramBundle.putBundle("android:preferences", localBundle);
        }
    }

    public void onStart()
    {
        super.onStart();
        this.mPreferenceManager.setOnPreferenceTreeClickListener(this);
    }

    public void onStop()
    {
        super.onStop();
        this.mPreferenceManager.dispatchActivityStop();
        this.mPreferenceManager.setOnPreferenceTreeClickListener(null);
    }

    public void setPreferenceScreen(PreferenceScreen paramPreferenceScreen)
    {
        if ((this.mPreferenceManager.setPreferences(paramPreferenceScreen)) && (paramPreferenceScreen != null))
        {
            this.mHavePrefs = true;
            if (this.mInitDone)
                postBindPreferences();
        }
    }

    public static abstract interface OnPreferenceStartFragmentCallback
    {
        public abstract boolean onPreferenceStartFragment(PreferenceFragment paramPreferenceFragment, Preference paramPreference);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceFragment
 * JD-Core Version:        0.6.2
 */