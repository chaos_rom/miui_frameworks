package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import com.android.internal.R.styleable;

public class SwitchPreference extends TwoStatePreference
{
    private final Listener mListener = new Listener(null);
    private CharSequence mSwitchOff;
    private CharSequence mSwitchOn;

    public SwitchPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public SwitchPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843629);
    }

    public SwitchPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SwitchPreference, paramInt, 0);
        setSummaryOn(localTypedArray.getString(0));
        setSummaryOff(localTypedArray.getString(1));
        setSwitchTextOn(localTypedArray.getString(3));
        setSwitchTextOff(localTypedArray.getString(4));
        setDisableDependentsState(localTypedArray.getBoolean(2, false));
        localTypedArray.recycle();
    }

    public CharSequence getSwitchTextOff()
    {
        return this.mSwitchOff;
    }

    public CharSequence getSwitchTextOn()
    {
        return this.mSwitchOn;
    }

    protected void onBindView(View paramView)
    {
        super.onBindView(paramView);
        View localView = paramView.findViewById(16909069);
        if ((localView != null) && ((localView instanceof Checkable)))
        {
            ((Checkable)localView).setChecked(this.mChecked);
            sendAccessibilityEvent(localView);
            if ((localView instanceof Switch))
            {
                Switch localSwitch = (Switch)localView;
                localSwitch.setTextOn(this.mSwitchOn);
                localSwitch.setTextOff(this.mSwitchOff);
                localSwitch.setOnCheckedChangeListener(this.mListener);
            }
        }
        syncSummaryView(paramView);
    }

    public void setSwitchTextOff(int paramInt)
    {
        setSwitchTextOff(getContext().getString(paramInt));
    }

    public void setSwitchTextOff(CharSequence paramCharSequence)
    {
        this.mSwitchOff = paramCharSequence;
        notifyChanged();
    }

    public void setSwitchTextOn(int paramInt)
    {
        setSwitchTextOn(getContext().getString(paramInt));
    }

    public void setSwitchTextOn(CharSequence paramCharSequence)
    {
        this.mSwitchOn = paramCharSequence;
        notifyChanged();
    }

    private class Listener
        implements CompoundButton.OnCheckedChangeListener
    {
        private Listener()
        {
        }

        public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
        {
            boolean bool;
            if (!SwitchPreference.this.callChangeListener(Boolean.valueOf(paramBoolean)))
                if (!paramBoolean)
                {
                    bool = true;
                    paramCompoundButton.setChecked(bool);
                }
            while (true)
            {
                return;
                bool = false;
                break;
                SwitchPreference.this.setChecked(paramBoolean);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.SwitchPreference
 * JD-Core Version:        0.6.2
 */