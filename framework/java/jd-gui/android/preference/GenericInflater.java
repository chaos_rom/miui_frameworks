package android.preference;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.InflateException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

abstract class GenericInflater<T, P extends Parent>
{
    private static final Class[] mConstructorSignature = arrayOfClass;
    private static final HashMap sConstructorMap = new HashMap();
    private final boolean DEBUG = false;
    private final Object[] mConstructorArgs = new Object[2];
    protected final Context mContext;
    private String mDefaultPackage;
    private Factory<T> mFactory;
    private boolean mFactorySet;

    static
    {
        Class[] arrayOfClass = new Class[2];
        arrayOfClass[0] = Context.class;
        arrayOfClass[1] = AttributeSet.class;
    }

    protected GenericInflater(Context paramContext)
    {
        this.mContext = paramContext;
    }

    protected GenericInflater(GenericInflater<T, P> paramGenericInflater, Context paramContext)
    {
        this.mContext = paramContext;
        this.mFactory = paramGenericInflater.mFactory;
    }

    private final T createItemFromTag(XmlPullParser paramXmlPullParser, String paramString, AttributeSet paramAttributeSet)
    {
        Object localObject1 = null;
        try
        {
            if (this.mFactory == null);
            while (localObject1 == null)
                if (-1 == paramString.indexOf('.'))
                {
                    localObject1 = onCreateItem(paramString, paramAttributeSet);
                    break;
                    localObject1 = this.mFactory.onCreateItem(paramString, this.mContext, paramAttributeSet);
                }
                else
                {
                    Object localObject2 = createItem(paramString, null, paramAttributeSet);
                    localObject1 = localObject2;
                }
        }
        catch (InflateException localInflateException3)
        {
            throw localInflateException3;
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            InflateException localInflateException2 = new InflateException(paramAttributeSet.getPositionDescription() + ": Error inflating class " + paramString);
            localInflateException2.initCause(localClassNotFoundException);
            throw localInflateException2;
        }
        catch (Exception localException)
        {
            InflateException localInflateException1 = new InflateException(paramAttributeSet.getPositionDescription() + ": Error inflating class " + paramString);
            localInflateException1.initCause(localException);
            throw localInflateException1;
        }
        return localObject1;
    }

    private void rInflate(XmlPullParser paramXmlPullParser, T paramT, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        while (true)
        {
            int j = paramXmlPullParser.next();
            if (((j == 3) && (paramXmlPullParser.getDepth() <= i)) || (j == 1))
                break;
            if ((j == 2) && (!onCreateCustomFromTag(paramXmlPullParser, paramT, paramAttributeSet)))
            {
                Object localObject = createItemFromTag(paramXmlPullParser, paramXmlPullParser.getName(), paramAttributeSet);
                ((Parent)paramT).addItemFromInflater(localObject);
                rInflate(paramXmlPullParser, localObject, paramAttributeSet);
            }
        }
    }

    public abstract GenericInflater cloneInContext(Context paramContext);

    public final T createItem(String paramString1, String paramString2, AttributeSet paramAttributeSet)
        throws ClassNotFoundException, InflateException
    {
        Constructor localConstructor = (Constructor)sConstructorMap.get(paramString1);
        if (localConstructor == null);
        try
        {
            ClassLoader localClassLoader = this.mContext.getClassLoader();
            if (paramString2 != null);
            for (String str = paramString2 + paramString1; ; str = paramString1)
            {
                localConstructor = localClassLoader.loadClass(str).getConstructor(mConstructorSignature);
                sConstructorMap.put(paramString1, localConstructor);
                Object[] arrayOfObject = this.mConstructorArgs;
                arrayOfObject[1] = paramAttributeSet;
                Object localObject = localConstructor.newInstance(arrayOfObject);
                return localObject;
            }
        }
        catch (NoSuchMethodException localNoSuchMethodException)
        {
            StringBuilder localStringBuilder = new StringBuilder().append(paramAttributeSet.getPositionDescription()).append(": Error inflating class ");
            if (paramString2 != null)
                paramString1 = paramString2 + paramString1;
            InflateException localInflateException2 = new InflateException(paramString1);
            localInflateException2.initCause(localNoSuchMethodException);
            throw localInflateException2;
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            throw localClassNotFoundException;
        }
        catch (Exception localException)
        {
            InflateException localInflateException1 = new InflateException(paramAttributeSet.getPositionDescription() + ": Error inflating class " + localConstructor.getClass().getName());
            localInflateException1.initCause(localException);
            throw localInflateException1;
        }
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public String getDefaultPackage()
    {
        return this.mDefaultPackage;
    }

    public final Factory<T> getFactory()
    {
        return this.mFactory;
    }

    public T inflate(int paramInt, P paramP)
    {
        if (paramP != null);
        for (boolean bool = true; ; bool = false)
            return inflate(paramInt, paramP, bool);
    }

    public T inflate(int paramInt, P paramP, boolean paramBoolean)
    {
        XmlResourceParser localXmlResourceParser = getContext().getResources().getXml(paramInt);
        try
        {
            Object localObject2 = inflate(localXmlResourceParser, paramP, paramBoolean);
            return localObject2;
        }
        finally
        {
            localXmlResourceParser.close();
        }
    }

    public T inflate(XmlPullParser paramXmlPullParser, P paramP)
    {
        if (paramP != null);
        for (boolean bool = true; ; bool = false)
            return inflate(paramXmlPullParser, paramP, bool);
    }

    public T inflate(XmlPullParser paramXmlPullParser, P paramP, boolean paramBoolean)
    {
        AttributeSet localAttributeSet;
        synchronized (this.mConstructorArgs)
        {
            localAttributeSet = Xml.asAttributeSet(paramXmlPullParser);
            this.mConstructorArgs[0] = this.mContext;
        }
        try
        {
            int i;
            do
                i = paramXmlPullParser.next();
            while ((i != 2) && (i != 1));
            if (i != 2)
                throw new InflateException(paramXmlPullParser.getPositionDescription() + ": No start tag found!");
        }
        catch (InflateException localInflateException3)
        {
            throw localInflateException3;
            localObject = finally;
            throw localObject;
            Parent localParent = onMergeRoots(paramP, paramBoolean, (Parent)createItemFromTag(paramXmlPullParser, paramXmlPullParser.getName(), localAttributeSet));
            rInflate(paramXmlPullParser, localParent, localAttributeSet);
            return localParent;
        }
        catch (XmlPullParserException localXmlPullParserException)
        {
            InflateException localInflateException2 = new InflateException(localXmlPullParserException.getMessage());
            localInflateException2.initCause(localXmlPullParserException);
            throw localInflateException2;
        }
        catch (IOException localIOException)
        {
            InflateException localInflateException1 = new InflateException(paramXmlPullParser.getPositionDescription() + ": " + localIOException.getMessage());
            localInflateException1.initCause(localIOException);
            throw localInflateException1;
        }
    }

    protected boolean onCreateCustomFromTag(XmlPullParser paramXmlPullParser, T paramT, AttributeSet paramAttributeSet)
        throws XmlPullParserException
    {
        return false;
    }

    protected T onCreateItem(String paramString, AttributeSet paramAttributeSet)
        throws ClassNotFoundException
    {
        return createItem(paramString, this.mDefaultPackage, paramAttributeSet);
    }

    protected P onMergeRoots(P paramP1, boolean paramBoolean, P paramP2)
    {
        return paramP2;
    }

    public void setDefaultPackage(String paramString)
    {
        this.mDefaultPackage = paramString;
    }

    public void setFactory(Factory<T> paramFactory)
    {
        if (this.mFactorySet)
            throw new IllegalStateException("A factory has already been set on this inflater");
        if (paramFactory == null)
            throw new NullPointerException("Given factory can not be null");
        this.mFactorySet = true;
        if (this.mFactory == null);
        for (this.mFactory = paramFactory; ; this.mFactory = new FactoryMerger(paramFactory, this.mFactory))
            return;
    }

    private static class FactoryMerger<T>
        implements GenericInflater.Factory<T>
    {
        private final GenericInflater.Factory<T> mF1;
        private final GenericInflater.Factory<T> mF2;

        FactoryMerger(GenericInflater.Factory<T> paramFactory1, GenericInflater.Factory<T> paramFactory2)
        {
            this.mF1 = paramFactory1;
            this.mF2 = paramFactory2;
        }

        public T onCreateItem(String paramString, Context paramContext, AttributeSet paramAttributeSet)
        {
            Object localObject = this.mF1.onCreateItem(paramString, paramContext, paramAttributeSet);
            if (localObject != null);
            while (true)
            {
                return localObject;
                localObject = this.mF2.onCreateItem(paramString, paramContext, paramAttributeSet);
            }
        }
    }

    public static abstract interface Factory<T>
    {
        public abstract T onCreateItem(String paramString, Context paramContext, AttributeSet paramAttributeSet);
    }

    public static abstract interface Parent<T>
    {
        public abstract void addItemFromInflater(T paramT);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.GenericInflater
 * JD-Core Version:        0.6.2
 */