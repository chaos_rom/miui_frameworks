package android.preference;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import com.android.internal.R.styleable;
import miui.widget.SlidingButton;
import miui.widget.SlidingButton.OnCheckedChangedListener;

public class CheckBoxPreference extends TwoStatePreference
{
    public CheckBoxPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public CheckBoxPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842895);
    }

    public CheckBoxPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CheckBoxPreference, paramInt, 0);
        setSummaryOn(localTypedArray.getString(0));
        setSummaryOff(localTypedArray.getString(1));
        setDisableDependentsState(localTypedArray.getBoolean(2, false));
        localTypedArray.recycle();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private void setSlidingButtonListener(View paramView)
    {
        if ((paramView != null) && ((paramView instanceof SlidingButton)))
        {
            final SlidingButton localSlidingButton = (SlidingButton)paramView;
            localSlidingButton.setOnCheckedChangedListener(new SlidingButton.OnCheckedChangedListener()
            {
                public void onCheckedChanged(boolean paramAnonymousBoolean)
                {
                    CheckBoxPreference.this.performClick(CheckBoxPreference.this.getPreferenceManager().getPreferenceScreen());
                    SlidingButton localSlidingButton;
                    if (paramAnonymousBoolean != CheckBoxPreference.this.isChecked())
                    {
                        localSlidingButton.setOnCheckedChangedListener(null);
                        localSlidingButton = localSlidingButton;
                        if (paramAnonymousBoolean)
                            break label61;
                    }
                    label61: for (boolean bool = true; ; bool = false)
                    {
                        localSlidingButton.setChecked(bool);
                        localSlidingButton.setOnCheckedChangedListener(this);
                        return;
                    }
                }
            });
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onBindView(View paramView)
    {
        super.onBindView(paramView);
        View localView = paramView.findViewById(16908289);
        if ((localView != null) && ((localView instanceof Checkable)))
        {
            ((Checkable)localView).setChecked(this.mChecked);
            sendAccessibilityEvent(localView);
        }
        setSlidingButtonListener(localView);
        syncSummaryView(paramView);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.CheckBoxPreference
 * JD-Core Version:        0.6.2
 */