package android.preference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.util.HashSet;
import java.util.Set;

public class MultiSelectListPreference extends DialogPreference
{
    private CharSequence[] mEntries;
    private CharSequence[] mEntryValues;
    private Set<String> mNewValues = new HashSet();
    private boolean mPreferenceChanged;
    private Set<String> mValues = new HashSet();

    public MultiSelectListPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public MultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MultiSelectListPreference, 0, 0);
        this.mEntries = localTypedArray.getTextArray(0);
        this.mEntryValues = localTypedArray.getTextArray(1);
        localTypedArray.recycle();
    }

    private boolean[] getSelectedItems()
    {
        CharSequence[] arrayOfCharSequence = this.mEntryValues;
        int i = arrayOfCharSequence.length;
        Set localSet = this.mValues;
        boolean[] arrayOfBoolean = new boolean[i];
        for (int j = 0; j < i; j++)
            arrayOfBoolean[j] = localSet.contains(arrayOfCharSequence[j].toString());
        return arrayOfBoolean;
    }

    public int findIndexOfValue(String paramString)
    {
        int i;
        if ((paramString != null) && (this.mEntryValues != null))
        {
            i = -1 + this.mEntryValues.length;
            if (i >= 0)
                if (!this.mEntryValues[i].equals(paramString));
        }
        while (true)
        {
            return i;
            i--;
            break;
            i = -1;
        }
    }

    public CharSequence[] getEntries()
    {
        return this.mEntries;
    }

    public CharSequence[] getEntryValues()
    {
        return this.mEntryValues;
    }

    public Set<String> getValues()
    {
        return this.mValues;
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
        super.onDialogClosed(paramBoolean);
        if ((paramBoolean) && (this.mPreferenceChanged))
        {
            Set localSet = this.mNewValues;
            if (callChangeListener(localSet))
                setValues(localSet);
        }
        this.mPreferenceChanged = false;
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        CharSequence[] arrayOfCharSequence = paramTypedArray.getTextArray(paramInt);
        int i = arrayOfCharSequence.length;
        HashSet localHashSet = new HashSet();
        for (int j = 0; j < i; j++)
            localHashSet.add(arrayOfCharSequence[j].toString());
        return localHashSet;
    }

    protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder)
    {
        super.onPrepareDialogBuilder(paramBuilder);
        if ((this.mEntries == null) || (this.mEntryValues == null))
            throw new IllegalStateException("MultiSelectListPreference requires an entries array and an entryValues array.");
        boolean[] arrayOfBoolean = getSelectedItems();
        paramBuilder.setMultiChoiceItems(this.mEntries, arrayOfBoolean, new DialogInterface.OnMultiChoiceClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, boolean paramAnonymousBoolean)
            {
                if (paramAnonymousBoolean)
                    MultiSelectListPreference.access$076(MultiSelectListPreference.this, MultiSelectListPreference.this.mNewValues.add(MultiSelectListPreference.this.mEntryValues[paramAnonymousInt].toString()));
                while (true)
                {
                    return;
                    MultiSelectListPreference.access$076(MultiSelectListPreference.this, MultiSelectListPreference.this.mNewValues.remove(MultiSelectListPreference.this.mEntryValues[paramAnonymousInt].toString()));
                }
            }
        });
        this.mNewValues.clear();
        this.mNewValues.addAll(this.mValues);
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.values = getValues();
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        if (paramBoolean);
        for (Set localSet = getPersistedStringSet(this.mValues); ; localSet = (Set)paramObject)
        {
            setValues(localSet);
            return;
        }
    }

    public void setEntries(int paramInt)
    {
        setEntries(getContext().getResources().getTextArray(paramInt));
    }

    public void setEntries(CharSequence[] paramArrayOfCharSequence)
    {
        this.mEntries = paramArrayOfCharSequence;
    }

    public void setEntryValues(int paramInt)
    {
        setEntryValues(getContext().getResources().getTextArray(paramInt));
    }

    public void setEntryValues(CharSequence[] paramArrayOfCharSequence)
    {
        this.mEntryValues = paramArrayOfCharSequence;
    }

    public void setValues(Set<String> paramSet)
    {
        this.mValues.clear();
        this.mValues.addAll(paramSet);
        persistStringSet(paramSet);
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public MultiSelectListPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new MultiSelectListPreference.SavedState(paramAnonymousParcel);
            }

            public MultiSelectListPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new MultiSelectListPreference.SavedState[paramAnonymousInt];
            }
        };
        Set<String> values;

        public SavedState(Parcel paramParcel)
        {
            super();
            this.values = new HashSet();
            String[] arrayOfString = paramParcel.readStringArray();
            int i = arrayOfString.length;
            for (int j = 0; j < i; j++)
                this.values.add(arrayOfString[j]);
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeStringArray((String[])this.values.toArray(new String[0]));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.MultiSelectListPreference
 * JD-Core Version:        0.6.2
 */