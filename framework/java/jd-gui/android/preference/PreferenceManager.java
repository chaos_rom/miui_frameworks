package android.preference;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class PreferenceManager
{
    public static final String KEY_HAS_SET_DEFAULT_VALUES = "_has_set_default_values";
    public static final String METADATA_KEY_PREFERENCES = "android.preference";
    private static final String TAG = "PreferenceManager";
    private Activity mActivity;
    private List<OnActivityDestroyListener> mActivityDestroyListeners;
    private List<OnActivityResultListener> mActivityResultListeners;
    private List<OnActivityStopListener> mActivityStopListeners;
    private Context mContext;
    private SharedPreferences.Editor mEditor;
    private PreferenceFragment mFragment;
    private long mNextId = 0L;
    private int mNextRequestCode;
    private boolean mNoCommit;
    private OnPreferenceTreeClickListener mOnPreferenceTreeClickListener;
    private PreferenceScreen mPreferenceScreen;
    private List<DialogInterface> mPreferencesScreens;
    private SharedPreferences mSharedPreferences;
    private int mSharedPreferencesMode;
    private String mSharedPreferencesName;

    PreferenceManager(Activity paramActivity, int paramInt)
    {
        this.mActivity = paramActivity;
        this.mNextRequestCode = paramInt;
        init(paramActivity);
    }

    private PreferenceManager(Context paramContext)
    {
        init(paramContext);
    }

    private void dismissAllScreens()
    {
        try
        {
            if (this.mPreferencesScreens == null)
                return;
            ArrayList localArrayList = new ArrayList(this.mPreferencesScreens);
            this.mPreferencesScreens.clear();
            for (int i = -1 + localArrayList.size(); i >= 0; i--)
                ((DialogInterface)localArrayList.get(i)).dismiss();
        }
        finally
        {
        }
    }

    public static SharedPreferences getDefaultSharedPreferences(Context paramContext)
    {
        return paramContext.getSharedPreferences(getDefaultSharedPreferencesName(paramContext), getDefaultSharedPreferencesMode());
    }

    private static int getDefaultSharedPreferencesMode()
    {
        return 0;
    }

    private static String getDefaultSharedPreferencesName(Context paramContext)
    {
        return paramContext.getPackageName() + "_preferences";
    }

    private void init(Context paramContext)
    {
        this.mContext = paramContext;
        setSharedPreferencesName(getDefaultSharedPreferencesName(paramContext));
    }

    private List<ResolveInfo> queryIntentActivities(Intent paramIntent)
    {
        return this.mContext.getPackageManager().queryIntentActivities(paramIntent, 128);
    }

    public static void setDefaultValues(Context paramContext, int paramInt, boolean paramBoolean)
    {
        setDefaultValues(paramContext, getDefaultSharedPreferencesName(paramContext), getDefaultSharedPreferencesMode(), paramInt, paramBoolean);
    }

    public static void setDefaultValues(Context paramContext, String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("_has_set_default_values", 0);
        SharedPreferences.Editor localEditor;
        if ((paramBoolean) || (!localSharedPreferences.getBoolean("_has_set_default_values", false)))
        {
            PreferenceManager localPreferenceManager = new PreferenceManager(paramContext);
            localPreferenceManager.setSharedPreferencesName(paramString);
            localPreferenceManager.setSharedPreferencesMode(paramInt1);
            localPreferenceManager.inflateFromResource(paramContext, paramInt2, null);
            localEditor = localSharedPreferences.edit().putBoolean("_has_set_default_values", true);
        }
        try
        {
            localEditor.apply();
            return;
        }
        catch (AbstractMethodError localAbstractMethodError)
        {
            while (true)
                localEditor.commit();
        }
    }

    private void setNoCommit(boolean paramBoolean)
    {
        if ((!paramBoolean) && (this.mEditor != null));
        try
        {
            this.mEditor.apply();
            this.mNoCommit = paramBoolean;
            return;
        }
        catch (AbstractMethodError localAbstractMethodError)
        {
            while (true)
                this.mEditor.commit();
        }
    }

    void addPreferencesScreen(DialogInterface paramDialogInterface)
    {
        try
        {
            if (this.mPreferencesScreens == null)
                this.mPreferencesScreens = new ArrayList();
            this.mPreferencesScreens.add(paramDialogInterface);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public PreferenceScreen createPreferenceScreen(Context paramContext)
    {
        PreferenceScreen localPreferenceScreen = new PreferenceScreen(paramContext, null);
        localPreferenceScreen.onAttachedToHierarchy(this);
        return localPreferenceScreen;
    }

    void dispatchActivityDestroy()
    {
        ArrayList localArrayList = null;
        try
        {
            if (this.mActivityDestroyListeners != null)
                localArrayList = new ArrayList(this.mActivityDestroyListeners);
            if (localArrayList != null)
            {
                int i = localArrayList.size();
                for (int j = 0; j < i; j++)
                    ((OnActivityDestroyListener)localArrayList.get(j)).onActivityDestroy();
            }
        }
        finally
        {
        }
        dismissAllScreens();
    }

    void dispatchActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
        try
        {
            if (this.mActivityResultListeners == null)
                return;
            ArrayList localArrayList = new ArrayList(this.mActivityResultListeners);
            int i = localArrayList.size();
            for (int j = 0; (j < i) && (!((OnActivityResultListener)localArrayList.get(j)).onActivityResult(paramInt1, paramInt2, paramIntent)); j++);
        }
        finally
        {
        }
    }

    void dispatchActivityStop()
    {
        try
        {
            if (this.mActivityStopListeners == null)
                return;
            ArrayList localArrayList = new ArrayList(this.mActivityStopListeners);
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((OnActivityStopListener)localArrayList.get(j)).onActivityStop();
        }
        finally
        {
        }
    }

    void dispatchNewIntent(Intent paramIntent)
    {
        dismissAllScreens();
    }

    public Preference findPreference(CharSequence paramCharSequence)
    {
        if (this.mPreferenceScreen == null);
        for (Preference localPreference = null; ; localPreference = this.mPreferenceScreen.findPreference(paramCharSequence))
            return localPreference;
    }

    Activity getActivity()
    {
        return this.mActivity;
    }

    Context getContext()
    {
        return this.mContext;
    }

    SharedPreferences.Editor getEditor()
    {
        if (this.mNoCommit)
            if (this.mEditor == null)
                this.mEditor = getSharedPreferences().edit();
        for (SharedPreferences.Editor localEditor = this.mEditor; ; localEditor = getSharedPreferences().edit())
            return localEditor;
    }

    PreferenceFragment getFragment()
    {
        return this.mFragment;
    }

    long getNextId()
    {
        try
        {
            long l = this.mNextId;
            this.mNextId = (1L + l);
            return l;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    int getNextRequestCode()
    {
        try
        {
            int i = this.mNextRequestCode;
            this.mNextRequestCode = (i + 1);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    OnPreferenceTreeClickListener getOnPreferenceTreeClickListener()
    {
        return this.mOnPreferenceTreeClickListener;
    }

    PreferenceScreen getPreferenceScreen()
    {
        return this.mPreferenceScreen;
    }

    public SharedPreferences getSharedPreferences()
    {
        if (this.mSharedPreferences == null)
            this.mSharedPreferences = this.mContext.getSharedPreferences(this.mSharedPreferencesName, this.mSharedPreferencesMode);
        return this.mSharedPreferences;
    }

    public int getSharedPreferencesMode()
    {
        return this.mSharedPreferencesMode;
    }

    public String getSharedPreferencesName()
    {
        return this.mSharedPreferencesName;
    }

    PreferenceScreen inflateFromIntent(Intent paramIntent, PreferenceScreen paramPreferenceScreen)
    {
        List localList = queryIntentActivities(paramIntent);
        HashSet localHashSet = new HashSet();
        int i = -1 + localList.size();
        if (i >= 0)
        {
            ActivityInfo localActivityInfo = ((ResolveInfo)localList.get(i)).activityInfo;
            Bundle localBundle = localActivityInfo.metaData;
            if ((localBundle == null) || (!localBundle.containsKey("android.preference")));
            while (true)
            {
                i--;
                break;
                String str = localActivityInfo.packageName + ":" + localActivityInfo.metaData.getInt("android.preference");
                if (!localHashSet.contains(str))
                {
                    localHashSet.add(str);
                    try
                    {
                        Context localContext = this.mContext.createPackageContext(localActivityInfo.packageName, 0);
                        PreferenceInflater localPreferenceInflater = new PreferenceInflater(localContext, this);
                        XmlResourceParser localXmlResourceParser = localActivityInfo.loadXmlMetaData(localContext.getPackageManager(), "android.preference");
                        paramPreferenceScreen = (PreferenceScreen)localPreferenceInflater.inflate(localXmlResourceParser, paramPreferenceScreen, true);
                        localXmlResourceParser.close();
                    }
                    catch (PackageManager.NameNotFoundException localNameNotFoundException)
                    {
                        Log.w("PreferenceManager", "Could not create context for " + localActivityInfo.packageName + ": " + Log.getStackTraceString(localNameNotFoundException));
                    }
                }
            }
        }
        paramPreferenceScreen.onAttachedToHierarchy(this);
        return paramPreferenceScreen;
    }

    public PreferenceScreen inflateFromResource(Context paramContext, int paramInt, PreferenceScreen paramPreferenceScreen)
    {
        setNoCommit(true);
        PreferenceScreen localPreferenceScreen = (PreferenceScreen)new PreferenceInflater(paramContext, this).inflate(paramInt, paramPreferenceScreen, true);
        localPreferenceScreen.onAttachedToHierarchy(this);
        setNoCommit(false);
        return localPreferenceScreen;
    }

    void registerOnActivityDestroyListener(OnActivityDestroyListener paramOnActivityDestroyListener)
    {
        try
        {
            if (this.mActivityDestroyListeners == null)
                this.mActivityDestroyListeners = new ArrayList();
            if (!this.mActivityDestroyListeners.contains(paramOnActivityDestroyListener))
                this.mActivityDestroyListeners.add(paramOnActivityDestroyListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void registerOnActivityResultListener(OnActivityResultListener paramOnActivityResultListener)
    {
        try
        {
            if (this.mActivityResultListeners == null)
                this.mActivityResultListeners = new ArrayList();
            if (!this.mActivityResultListeners.contains(paramOnActivityResultListener))
                this.mActivityResultListeners.add(paramOnActivityResultListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void registerOnActivityStopListener(OnActivityStopListener paramOnActivityStopListener)
    {
        try
        {
            if (this.mActivityStopListeners == null)
                this.mActivityStopListeners = new ArrayList();
            if (!this.mActivityStopListeners.contains(paramOnActivityStopListener))
                this.mActivityStopListeners.add(paramOnActivityStopListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void removePreferencesScreen(DialogInterface paramDialogInterface)
    {
        try
        {
            if (this.mPreferencesScreens == null)
                return;
            this.mPreferencesScreens.remove(paramDialogInterface);
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setFragment(PreferenceFragment paramPreferenceFragment)
    {
        this.mFragment = paramPreferenceFragment;
    }

    void setOnPreferenceTreeClickListener(OnPreferenceTreeClickListener paramOnPreferenceTreeClickListener)
    {
        this.mOnPreferenceTreeClickListener = paramOnPreferenceTreeClickListener;
    }

    boolean setPreferences(PreferenceScreen paramPreferenceScreen)
    {
        if (paramPreferenceScreen != this.mPreferenceScreen)
            this.mPreferenceScreen = paramPreferenceScreen;
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setSharedPreferencesMode(int paramInt)
    {
        this.mSharedPreferencesMode = paramInt;
        this.mSharedPreferences = null;
    }

    public void setSharedPreferencesName(String paramString)
    {
        this.mSharedPreferencesName = paramString;
        this.mSharedPreferences = null;
    }

    boolean shouldCommit()
    {
        if (!this.mNoCommit);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void unregisterOnActivityDestroyListener(OnActivityDestroyListener paramOnActivityDestroyListener)
    {
        try
        {
            if (this.mActivityDestroyListeners != null)
                this.mActivityDestroyListeners.remove(paramOnActivityDestroyListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void unregisterOnActivityResultListener(OnActivityResultListener paramOnActivityResultListener)
    {
        try
        {
            if (this.mActivityResultListeners != null)
                this.mActivityResultListeners.remove(paramOnActivityResultListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void unregisterOnActivityStopListener(OnActivityStopListener paramOnActivityStopListener)
    {
        try
        {
            if (this.mActivityStopListeners != null)
                this.mActivityStopListeners.remove(paramOnActivityStopListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static abstract interface OnActivityDestroyListener
    {
        public abstract void onActivityDestroy();
    }

    public static abstract interface OnActivityStopListener
    {
        public abstract void onActivityStop();
    }

    public static abstract interface OnActivityResultListener
    {
        public abstract boolean onActivityResult(int paramInt1, int paramInt2, Intent paramIntent);
    }

    static abstract interface OnPreferenceTreeClickListener
    {
        public abstract boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceManager
 * JD-Core Version:        0.6.2
 */