package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.android.internal.R.styleable;

public class SeekBarPreference extends Preference
    implements SeekBar.OnSeekBarChangeListener
{
    private int mMax;
    private int mProgress;
    private boolean mTrackingTouch;

    public SeekBarPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public SeekBarPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public SeekBarPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ProgressBar, paramInt, 0);
        setMax(localTypedArray.getInt(2, this.mMax));
        localTypedArray.recycle();
        setLayoutResource(17367179);
    }

    private void setProgress(int paramInt, boolean paramBoolean)
    {
        if (paramInt > this.mMax)
            paramInt = this.mMax;
        if (paramInt < 0)
            paramInt = 0;
        if (paramInt != this.mProgress)
        {
            this.mProgress = paramInt;
            persistInt(paramInt);
            if (paramBoolean)
                notifyChanged();
        }
    }

    public int getProgress()
    {
        return this.mProgress;
    }

    public CharSequence getSummary()
    {
        return null;
    }

    protected void onBindView(View paramView)
    {
        super.onBindView(paramView);
        SeekBar localSeekBar = (SeekBar)paramView.findViewById(16909068);
        localSeekBar.setOnSeekBarChangeListener(this);
        localSeekBar.setMax(this.mMax);
        localSeekBar.setProgress(this.mProgress);
        localSeekBar.setEnabled(isEnabled());
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return Integer.valueOf(paramTypedArray.getInt(paramInt, 0));
    }

    public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        if (paramKeyEvent.getAction() != i)
            if ((paramInt == 81) || (paramInt == 70))
                setProgress(1 + getProgress());
        while (true)
        {
            return i;
            if (paramInt == 69)
                setProgress(-1 + getProgress());
            else
                int j = 0;
        }
    }

    public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
    {
        if ((paramBoolean) && (!this.mTrackingTouch))
            syncProgress(paramSeekBar);
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if (!paramParcelable.getClass().equals(SavedState.class))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            this.mProgress = localSavedState.progress;
            this.mMax = localSavedState.max;
            notifyChanged();
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.progress = this.mProgress;
            localSavedState.max = this.mMax;
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        if (paramBoolean);
        for (int i = getPersistedInt(this.mProgress); ; i = ((Integer)paramObject).intValue())
        {
            setProgress(i);
            return;
        }
    }

    public void onStartTrackingTouch(SeekBar paramSeekBar)
    {
        this.mTrackingTouch = true;
    }

    public void onStopTrackingTouch(SeekBar paramSeekBar)
    {
        this.mTrackingTouch = false;
        if (paramSeekBar.getProgress() != this.mProgress)
            syncProgress(paramSeekBar);
    }

    public void setMax(int paramInt)
    {
        if (paramInt != this.mMax)
        {
            this.mMax = paramInt;
            notifyChanged();
        }
    }

    public void setProgress(int paramInt)
    {
        setProgress(paramInt, true);
    }

    void syncProgress(SeekBar paramSeekBar)
    {
        int i = paramSeekBar.getProgress();
        if (i != this.mProgress)
        {
            if (!callChangeListener(Integer.valueOf(i)))
                break label31;
            setProgress(i, false);
        }
        while (true)
        {
            return;
            label31: paramSeekBar.setProgress(this.mProgress);
        }
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public SeekBarPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new SeekBarPreference.SavedState(paramAnonymousParcel);
            }

            public SeekBarPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new SeekBarPreference.SavedState[paramAnonymousInt];
            }
        };
        int max;
        int progress;

        public SavedState(Parcel paramParcel)
        {
            super();
            this.progress = paramParcel.readInt();
            this.max = paramParcel.readInt();
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.progress);
            paramParcel.writeInt(this.max);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.SeekBarPreference
 * JD-Core Version:        0.6.2
 */