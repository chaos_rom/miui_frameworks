package android.preference;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.android.internal.R.styleable;

public abstract class DialogPreference extends Preference
    implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, PreferenceManager.OnActivityDestroyListener
{
    private AlertDialog.Builder mBuilder;
    private Dialog mDialog;
    private Drawable mDialogIcon;
    private int mDialogLayoutResId;
    private CharSequence mDialogMessage;
    private CharSequence mDialogTitle;
    private CharSequence mNegativeButtonText;
    private CharSequence mPositiveButtonText;
    private int mWhichButtonClicked;

    public DialogPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842897);
    }

    public DialogPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DialogPreference, paramInt, 0);
        this.mDialogTitle = localTypedArray.getString(0);
        if (this.mDialogTitle == null)
            this.mDialogTitle = getTitle();
        this.mDialogMessage = localTypedArray.getString(1);
        this.mDialogIcon = localTypedArray.getDrawable(2);
        this.mPositiveButtonText = localTypedArray.getString(3);
        this.mNegativeButtonText = localTypedArray.getString(4);
        this.mDialogLayoutResId = localTypedArray.getResourceId(5, this.mDialogLayoutResId);
        localTypedArray.recycle();
    }

    private void requestInputMethod(Dialog paramDialog)
    {
        paramDialog.getWindow().setSoftInputMode(5);
    }

    public Dialog getDialog()
    {
        return this.mDialog;
    }

    public Drawable getDialogIcon()
    {
        return this.mDialogIcon;
    }

    public int getDialogLayoutResource()
    {
        return this.mDialogLayoutResId;
    }

    public CharSequence getDialogMessage()
    {
        return this.mDialogMessage;
    }

    public CharSequence getDialogTitle()
    {
        return this.mDialogTitle;
    }

    public CharSequence getNegativeButtonText()
    {
        return this.mNegativeButtonText;
    }

    public CharSequence getPositiveButtonText()
    {
        return this.mPositiveButtonText;
    }

    protected boolean needInputMethod()
    {
        return false;
    }

    public void onActivityDestroy()
    {
        if ((this.mDialog == null) || (!this.mDialog.isShowing()));
        while (true)
        {
            return;
            this.mDialog.dismiss();
        }
    }

    protected void onBindDialogView(View paramView)
    {
        View localView = paramView.findViewById(16908299);
        if (localView != null)
        {
            CharSequence localCharSequence = getDialogMessage();
            int i = 8;
            if (!TextUtils.isEmpty(localCharSequence))
            {
                if ((localView instanceof TextView))
                    ((TextView)localView).setText(localCharSequence);
                i = 0;
            }
            if (localView.getVisibility() != i)
                localView.setVisibility(i);
        }
    }

    protected void onClick()
    {
        if ((this.mDialog != null) && (this.mDialog.isShowing()));
        while (true)
        {
            return;
            showDialog(null);
        }
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        this.mWhichButtonClicked = paramInt;
    }

    protected View onCreateDialogView()
    {
        View localView = null;
        if (this.mDialogLayoutResId == 0);
        while (true)
        {
            return localView;
            localView = LayoutInflater.from(this.mBuilder.getContext()).inflate(this.mDialogLayoutResId, null);
        }
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
    }

    public void onDismiss(DialogInterface paramDialogInterface)
    {
        getPreferenceManager().unregisterOnActivityDestroyListener(this);
        this.mDialog = null;
        if (this.mWhichButtonClicked == -1);
        for (boolean bool = true; ; bool = false)
        {
            onDialogClosed(bool);
            return;
        }
    }

    protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder)
    {
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class)))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            if (localSavedState.isDialogShowing)
                showDialog(localSavedState.dialogBundle);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Parcelable localParcelable = super.onSaveInstanceState();
        Object localObject;
        if ((this.mDialog == null) || (!this.mDialog.isShowing()))
            localObject = localParcelable;
        while (true)
        {
            return localObject;
            localObject = new SavedState(localParcelable);
            ((SavedState)localObject).isDialogShowing = true;
            ((SavedState)localObject).dialogBundle = this.mDialog.onSaveInstanceState();
        }
    }

    public void setDialogIcon(int paramInt)
    {
        this.mDialogIcon = getContext().getResources().getDrawable(paramInt);
    }

    public void setDialogIcon(Drawable paramDrawable)
    {
        this.mDialogIcon = paramDrawable;
    }

    public void setDialogLayoutResource(int paramInt)
    {
        this.mDialogLayoutResId = paramInt;
    }

    public void setDialogMessage(int paramInt)
    {
        setDialogMessage(getContext().getString(paramInt));
    }

    public void setDialogMessage(CharSequence paramCharSequence)
    {
        this.mDialogMessage = paramCharSequence;
    }

    public void setDialogTitle(int paramInt)
    {
        setDialogTitle(getContext().getString(paramInt));
    }

    public void setDialogTitle(CharSequence paramCharSequence)
    {
        this.mDialogTitle = paramCharSequence;
    }

    public void setNegativeButtonText(int paramInt)
    {
        setNegativeButtonText(getContext().getString(paramInt));
    }

    public void setNegativeButtonText(CharSequence paramCharSequence)
    {
        this.mNegativeButtonText = paramCharSequence;
    }

    public void setPositiveButtonText(int paramInt)
    {
        setPositiveButtonText(getContext().getString(paramInt));
    }

    public void setPositiveButtonText(CharSequence paramCharSequence)
    {
        this.mPositiveButtonText = paramCharSequence;
    }

    protected void showDialog(Bundle paramBundle)
    {
        Context localContext = getContext();
        this.mWhichButtonClicked = -2;
        this.mBuilder = new AlertDialog.Builder(localContext).setTitle(this.mDialogTitle).setIcon(this.mDialogIcon).setPositiveButton(this.mPositiveButtonText, this).setNegativeButton(this.mNegativeButtonText, this);
        View localView = onCreateDialogView();
        if (localView != null)
        {
            onBindDialogView(localView);
            this.mBuilder.setView(localView);
        }
        while (true)
        {
            onPrepareDialogBuilder(this.mBuilder);
            getPreferenceManager().registerOnActivityDestroyListener(this);
            AlertDialog localAlertDialog = this.mBuilder.create();
            this.mDialog = localAlertDialog;
            if (paramBundle != null)
                localAlertDialog.onRestoreInstanceState(paramBundle);
            if (needInputMethod())
                requestInputMethod(localAlertDialog);
            localAlertDialog.setOnDismissListener(this);
            localAlertDialog.show();
            return;
            this.mBuilder.setMessage(this.mDialogMessage);
        }
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public DialogPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new DialogPreference.SavedState(paramAnonymousParcel);
            }

            public DialogPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new DialogPreference.SavedState[paramAnonymousInt];
            }
        };
        Bundle dialogBundle;
        boolean isDialogShowing;

        public SavedState(Parcel paramParcel)
        {
            super();
            if (paramParcel.readInt() == i);
            while (true)
            {
                this.isDialogShowing = i;
                this.dialogBundle = paramParcel.readBundle();
                return;
                i = 0;
            }
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            if (this.isDialogShowing);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                paramParcel.writeBundle(this.dialogBundle);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.DialogPreference
 * JD-Core Version:        0.6.2
 */