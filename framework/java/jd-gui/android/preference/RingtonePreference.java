package android.preference;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class RingtonePreference extends Preference
    implements PreferenceManager.OnActivityResultListener
{
    private static final String TAG = "RingtonePreference";
    private int mRequestCode;
    private int mRingtoneType;
    private boolean mShowDefault;
    private boolean mShowSilent;

    public RingtonePreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public RingtonePreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842899);
    }

    public RingtonePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RingtonePreference, paramInt, 0);
        this.mRingtoneType = localTypedArray.getInt(0, 1);
        this.mShowDefault = localTypedArray.getBoolean(1, true);
        this.mShowSilent = localTypedArray.getBoolean(2, true);
        localTypedArray.recycle();
    }

    public int getRingtoneType()
    {
        return this.mRingtoneType;
    }

    public boolean getShowDefault()
    {
        return this.mShowDefault;
    }

    public boolean getShowSilent()
    {
        return this.mShowSilent;
    }

    public boolean onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
        String str;
        if (paramInt1 == this.mRequestCode)
            if (paramIntent != null)
            {
                Uri localUri = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                if (localUri == null)
                    break label56;
                str = localUri.toString();
                if (callChangeListener(str))
                    onSaveRingtone(localUri);
            }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            label56: str = "";
            break;
        }
    }

    protected void onAttachedToHierarchy(PreferenceManager paramPreferenceManager)
    {
        super.onAttachedToHierarchy(paramPreferenceManager);
        paramPreferenceManager.registerOnActivityResultListener(this);
        this.mRequestCode = paramPreferenceManager.getNextRequestCode();
    }

    protected void onClick()
    {
        Intent localIntent = new Intent("android.intent.action.RINGTONE_PICKER");
        onPrepareRingtonePickerIntent(localIntent);
        PreferenceFragment localPreferenceFragment = getPreferenceManager().getFragment();
        if (localPreferenceFragment != null)
            localPreferenceFragment.startActivityForResult(localIntent, this.mRequestCode);
        while (true)
        {
            return;
            getPreferenceManager().getActivity().startActivityForResult(localIntent, this.mRequestCode);
        }
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return paramTypedArray.getString(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onPrepareRingtonePickerIntent(Intent paramIntent)
    {
        paramIntent.putExtra("android.intent.extra.ringtone.EXISTING_URI", onRestoreRingtone());
        paramIntent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", this.mShowDefault);
        if (this.mShowDefault)
            paramIntent.putExtra("android.intent.extra.ringtone.DEFAULT_URI", RingtoneManager.getDefaultUri(getRingtoneType()));
        paramIntent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", this.mShowSilent);
        paramIntent.putExtra("android.intent.extra.ringtone.TYPE", this.mRingtoneType);
        paramIntent.putExtra("android.intent.extra.ringtone.TITLE", getTitle());
        paramIntent.setClassName("com.android.thememanager", "com.android.thememanager.activity.ThemeTabActivity");
    }

    protected Uri onRestoreRingtone()
    {
        Uri localUri = null;
        String str = getPersistedString(null);
        if (!TextUtils.isEmpty(str))
            localUri = Uri.parse(str);
        return localUri;
    }

    protected void onSaveRingtone(Uri paramUri)
    {
        if (paramUri != null);
        for (String str = paramUri.toString(); ; str = "")
        {
            persistString(str);
            return;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        String str = (String)paramObject;
        if (paramBoolean);
        while (true)
        {
            return;
            if (!TextUtils.isEmpty(str))
                onSaveRingtone(Uri.parse(str));
        }
    }

    public void setRingtoneType(int paramInt)
    {
        this.mRingtoneType = paramInt;
    }

    public void setShowDefault(boolean paramBoolean)
    {
        this.mShowDefault = paramBoolean;
    }

    public void setShowSilent(boolean paramBoolean)
    {
        this.mShowSilent = paramBoolean;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.RingtonePreference
 * JD-Core Version:        0.6.2
 */