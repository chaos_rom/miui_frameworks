package android.preference;

import android.app.Fragment;
import android.app.FragmentBreadCrumbs;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public abstract class PreferenceActivity extends ListActivity
    implements PreferenceManager.OnPreferenceTreeClickListener, PreferenceFragment.OnPreferenceStartFragmentCallback
{
    private static final String BACK_STACK_PREFS = ":android:prefs";
    private static final String CUR_HEADER_TAG = ":android:cur_header";
    public static final String EXTRA_NO_HEADERS = ":android:no_headers";
    private static final String EXTRA_PREFS_SET_BACK_TEXT = "extra_prefs_set_back_text";
    private static final String EXTRA_PREFS_SET_NEXT_TEXT = "extra_prefs_set_next_text";
    private static final String EXTRA_PREFS_SHOW_BUTTON_BAR = "extra_prefs_show_button_bar";
    private static final String EXTRA_PREFS_SHOW_SKIP = "extra_prefs_show_skip";
    public static final String EXTRA_SHOW_FRAGMENT = ":android:show_fragment";
    public static final String EXTRA_SHOW_FRAGMENT_ARGUMENTS = ":android:show_fragment_args";
    public static final String EXTRA_SHOW_FRAGMENT_SHORT_TITLE = ":android:show_fragment_short_title";
    public static final String EXTRA_SHOW_FRAGMENT_TITLE = ":android:show_fragment_title";
    private static final int FIRST_REQUEST_CODE = 100;
    private static final String HEADERS_TAG = ":android:headers";
    public static final long HEADER_ID_UNDEFINED = -1L;
    private static final int MSG_BIND_PREFERENCES = 1;
    private static final int MSG_BUILD_HEADERS = 2;
    private static final String PREFERENCES_TAG = ":android:preferences";
    private Header mCurHeader;
    private FragmentBreadCrumbs mFragmentBreadCrumbs;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                PreferenceActivity.this.bindPreferences();
                continue;
                ArrayList localArrayList = new ArrayList(PreferenceActivity.this.mHeaders);
                PreferenceActivity.this.mHeaders.clear();
                PreferenceActivity.this.onBuildHeaders(PreferenceActivity.this.mHeaders);
                if ((PreferenceActivity.access$200(PreferenceActivity.this) instanceof BaseAdapter))
                    ((BaseAdapter)PreferenceActivity.access$300(PreferenceActivity.this)).notifyDataSetChanged();
                PreferenceActivity.Header localHeader1 = PreferenceActivity.this.onGetNewHeader();
                if ((localHeader1 != null) && (localHeader1.fragment != null))
                {
                    PreferenceActivity.Header localHeader3 = PreferenceActivity.this.findBestMatchingHeader(localHeader1, localArrayList);
                    if ((localHeader3 == null) || (PreferenceActivity.this.mCurHeader != localHeader3))
                        PreferenceActivity.this.switchToHeader(localHeader1);
                }
                else if (PreferenceActivity.this.mCurHeader != null)
                {
                    PreferenceActivity.Header localHeader2 = PreferenceActivity.this.findBestMatchingHeader(PreferenceActivity.this.mCurHeader, PreferenceActivity.this.mHeaders);
                    if (localHeader2 != null)
                        PreferenceActivity.this.setSelectedHeader(localHeader2);
                }
            }
        }
    };
    private final ArrayList<Header> mHeaders = new ArrayList();
    private FrameLayout mListFooter;
    private Button mNextButton;
    private PreferenceManager mPreferenceManager;
    private ViewGroup mPrefsContainer;
    private Bundle mSavedInstanceState;
    private boolean mSinglePane;

    private void bindPreferences()
    {
        PreferenceScreen localPreferenceScreen = getPreferenceScreen();
        if (localPreferenceScreen != null)
        {
            localPreferenceScreen.bind(getListView());
            if (this.mSavedInstanceState != null)
            {
                super.onRestoreInstanceState(this.mSavedInstanceState);
                this.mSavedInstanceState = null;
            }
        }
    }

    private void postBindPreferences()
    {
        if (this.mHandler.hasMessages(1));
        while (true)
        {
            return;
            this.mHandler.obtainMessage(1).sendToTarget();
        }
    }

    private void requirePreferenceManager()
    {
        if (this.mPreferenceManager == null)
        {
            if (this.mAdapter == null)
                throw new RuntimeException("This should be called after super.onCreate.");
            throw new RuntimeException("Modern two-pane PreferenceActivity requires use of a PreferenceFragment");
        }
    }

    private void switchToHeaderInner(String paramString, Bundle paramBundle, int paramInt)
    {
        getFragmentManager().popBackStack(":android:prefs", 1);
        Fragment localFragment = Fragment.instantiate(this, paramString, paramBundle);
        FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
        localFragmentTransaction.setTransition(4099);
        localFragmentTransaction.replace(16909064, localFragment);
        localFragmentTransaction.commitAllowingStateLoss();
    }

    @Deprecated
    public void addPreferencesFromIntent(Intent paramIntent)
    {
        requirePreferenceManager();
        setPreferenceScreen(this.mPreferenceManager.inflateFromIntent(paramIntent, getPreferenceScreen()));
    }

    @Deprecated
    public void addPreferencesFromResource(int paramInt)
    {
        requirePreferenceManager();
        setPreferenceScreen(this.mPreferenceManager.inflateFromResource(this, paramInt, getPreferenceScreen()));
    }

    Header findBestMatchingHeader(Header paramHeader, ArrayList<Header> paramArrayList)
    {
        ArrayList localArrayList = new ArrayList();
        int i = 0;
        Header localHeader2;
        int j;
        if (i < paramArrayList.size())
        {
            localHeader2 = (Header)paramArrayList.get(i);
            if ((paramHeader == localHeader2) || ((paramHeader.id != -1L) && (paramHeader.id == localHeader2.id)))
            {
                localArrayList.clear();
                localArrayList.add(localHeader2);
            }
        }
        else
        {
            j = localArrayList.size();
            if (j != 1)
                break label196;
        }
        label295: for (Header localHeader1 = (Header)localArrayList.get(0); ; localHeader1 = null)
        {
            return localHeader1;
            if (paramHeader.fragment != null)
                if (paramHeader.fragment.equals(localHeader2.fragment))
                    localArrayList.add(localHeader2);
            while (true)
            {
                i++;
                break;
                if (paramHeader.intent != null)
                {
                    if (paramHeader.intent.equals(localHeader2.intent))
                        localArrayList.add(localHeader2);
                }
                else if ((paramHeader.title != null) && (paramHeader.title.equals(localHeader2.title)))
                    localArrayList.add(localHeader2);
            }
            label196: if (j > 1)
                for (int k = 0; ; k++)
                {
                    if (k >= j)
                        break label295;
                    localHeader1 = (Header)localArrayList.get(k);
                    if (((paramHeader.fragmentArguments != null) && (paramHeader.fragmentArguments.equals(localHeader1.fragmentArguments))) || ((paramHeader.extras != null) && (paramHeader.extras.equals(localHeader1.extras))) || ((paramHeader.title != null) && (paramHeader.title.equals(localHeader1.title))))
                        break;
                }
        }
    }

    @Deprecated
    public Preference findPreference(CharSequence paramCharSequence)
    {
        if (this.mPreferenceManager == null);
        for (Preference localPreference = null; ; localPreference = this.mPreferenceManager.findPreference(paramCharSequence))
            return localPreference;
    }

    public void finishPreferencePanel(Fragment paramFragment, int paramInt, Intent paramIntent)
    {
        if (this.mSinglePane)
        {
            setResult(paramInt, paramIntent);
            finish();
        }
        while (true)
        {
            return;
            onBackPressed();
            if ((paramFragment != null) && (paramFragment.getTargetFragment() != null))
                paramFragment.getTargetFragment().onActivityResult(paramFragment.getTargetRequestCode(), paramInt, paramIntent);
        }
    }

    public List<Header> getHeaders()
    {
        return this.mHeaders;
    }

    protected Button getNextButton()
    {
        return this.mNextButton;
    }

    @Deprecated
    public PreferenceManager getPreferenceManager()
    {
        return this.mPreferenceManager;
    }

    @Deprecated
    public PreferenceScreen getPreferenceScreen()
    {
        if (this.mPreferenceManager != null);
        for (PreferenceScreen localPreferenceScreen = this.mPreferenceManager.getPreferenceScreen(); ; localPreferenceScreen = null)
            return localPreferenceScreen;
    }

    public boolean hasHeaders()
    {
        if ((getListView().getVisibility() == 0) && (this.mPreferenceManager == null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean hasNextButton()
    {
        if (this.mNextButton != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void invalidateHeaders()
    {
        if (!this.mHandler.hasMessages(2))
            this.mHandler.sendEmptyMessage(2);
    }

    public boolean isMultiPane()
    {
        if ((hasHeaders()) && (this.mPrefsContainer.getVisibility() == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    public void loadHeadersFromResource(int paramInt, List<Header> paramList)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: invokevirtual 343	android/preference/PreferenceActivity:getResources	()Landroid/content/res/Resources;
        //     6: iload_1
        //     7: invokevirtual 349	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
        //     10: astore_3
        //     11: aload_3
        //     12: invokestatic 355	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     15: astore 7
        //     17: aload_3
        //     18: invokeinterface 360 1 0
        //     23: istore 8
        //     25: iload 8
        //     27: iconst_1
        //     28: if_icmpeq +9 -> 37
        //     31: iload 8
        //     33: iconst_2
        //     34: if_icmpne -17 -> 17
        //     37: aload_3
        //     38: invokeinterface 364 1 0
        //     43: astore 9
        //     45: ldc_w 366
        //     48: aload 9
        //     50: invokevirtual 258	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     53: ifne +77 -> 130
        //     56: new 166	java/lang/RuntimeException
        //     59: dup
        //     60: new 368	java/lang/StringBuilder
        //     63: dup
        //     64: invokespecial 369	java/lang/StringBuilder:<init>	()V
        //     67: ldc_w 371
        //     70: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: aload 9
        //     75: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     78: ldc_w 377
        //     81: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     84: aload_3
        //     85: invokeinterface 380 1 0
        //     90: invokevirtual 375	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     93: invokevirtual 383	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     96: invokespecial 171	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     99: athrow
        //     100: astore 6
        //     102: new 166	java/lang/RuntimeException
        //     105: dup
        //     106: ldc_w 385
        //     109: aload 6
        //     111: invokespecial 388	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     114: athrow
        //     115: astore 5
        //     117: aload_3
        //     118: ifnull +9 -> 127
        //     121: aload_3
        //     122: invokeinterface 391 1 0
        //     127: aload 5
        //     129: athrow
        //     130: aconst_null
        //     131: astore 10
        //     133: aload_3
        //     134: invokeinterface 394 1 0
        //     139: istore 11
        //     141: aload_3
        //     142: invokeinterface 360 1 0
        //     147: istore 12
        //     149: iload 12
        //     151: iconst_1
        //     152: if_icmpeq +518 -> 670
        //     155: iload 12
        //     157: iconst_3
        //     158: if_icmpne +14 -> 172
        //     161: aload_3
        //     162: invokeinterface 394 1 0
        //     167: iload 11
        //     169: if_icmple +501 -> 670
        //     172: iload 12
        //     174: iconst_3
        //     175: if_icmpeq -34 -> 141
        //     178: iload 12
        //     180: iconst_4
        //     181: if_icmpeq -40 -> 141
        //     184: ldc_w 396
        //     187: aload_3
        //     188: invokeinterface 364 1 0
        //     193: invokevirtual 258	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     196: ifeq +467 -> 663
        //     199: new 18	android/preference/PreferenceActivity$Header
        //     202: dup
        //     203: invokespecial 397	android/preference/PreferenceActivity$Header:<init>	()V
        //     206: astore 13
        //     208: aload_0
        //     209: invokevirtual 343	android/preference/PreferenceActivity:getResources	()Landroid/content/res/Resources;
        //     212: aload 7
        //     214: getstatic 403	com/android/internal/R$styleable:PreferenceHeader	[I
        //     217: invokevirtual 407	android/content/res/Resources:obtainAttributes	(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
        //     220: astore 14
        //     222: aload 13
        //     224: aload 14
        //     226: iconst_1
        //     227: bipush 255
        //     229: invokevirtual 413	android/content/res/TypedArray:getResourceId	(II)I
        //     232: i2l
        //     233: putfield 243	android/preference/PreferenceActivity$Header:id	J
        //     236: aload 14
        //     238: iconst_2
        //     239: invokevirtual 417	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     242: astore 15
        //     244: aload 15
        //     246: ifnull +30 -> 276
        //     249: aload 15
        //     251: getfield 422	android/util/TypedValue:type	I
        //     254: iconst_3
        //     255: if_icmpne +21 -> 276
        //     258: aload 15
        //     260: getfield 425	android/util/TypedValue:resourceId	I
        //     263: ifeq +282 -> 545
        //     266: aload 13
        //     268: aload 15
        //     270: getfield 425	android/util/TypedValue:resourceId	I
        //     273: putfield 428	android/preference/PreferenceActivity$Header:titleRes	I
        //     276: aload 14
        //     278: iconst_3
        //     279: invokevirtual 417	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     282: astore 16
        //     284: aload 16
        //     286: ifnull +30 -> 316
        //     289: aload 16
        //     291: getfield 422	android/util/TypedValue:type	I
        //     294: iconst_3
        //     295: if_icmpne +21 -> 316
        //     298: aload 16
        //     300: getfield 425	android/util/TypedValue:resourceId	I
        //     303: ifeq +255 -> 558
        //     306: aload 13
        //     308: aload 16
        //     310: getfield 425	android/util/TypedValue:resourceId	I
        //     313: putfield 431	android/preference/PreferenceActivity$Header:summaryRes	I
        //     316: aload 14
        //     318: iconst_5
        //     319: invokevirtual 417	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     322: astore 17
        //     324: aload 17
        //     326: ifnull +30 -> 356
        //     329: aload 17
        //     331: getfield 422	android/util/TypedValue:type	I
        //     334: iconst_3
        //     335: if_icmpne +21 -> 356
        //     338: aload 17
        //     340: getfield 425	android/util/TypedValue:resourceId	I
        //     343: ifeq +228 -> 571
        //     346: aload 13
        //     348: aload 17
        //     350: getfield 425	android/util/TypedValue:resourceId	I
        //     353: putfield 434	android/preference/PreferenceActivity$Header:breadCrumbTitleRes	I
        //     356: aload 14
        //     358: bipush 6
        //     360: invokevirtual 417	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     363: astore 18
        //     365: aload 18
        //     367: ifnull +30 -> 397
        //     370: aload 18
        //     372: getfield 422	android/util/TypedValue:type	I
        //     375: iconst_3
        //     376: if_icmpne +21 -> 397
        //     379: aload 18
        //     381: getfield 425	android/util/TypedValue:resourceId	I
        //     384: ifeq +200 -> 584
        //     387: aload 13
        //     389: aload 18
        //     391: getfield 425	android/util/TypedValue:resourceId	I
        //     394: putfield 437	android/preference/PreferenceActivity$Header:breadCrumbShortTitleRes	I
        //     397: aload 13
        //     399: aload 14
        //     401: iconst_0
        //     402: iconst_0
        //     403: invokevirtual 413	android/content/res/TypedArray:getResourceId	(II)I
        //     406: putfield 440	android/preference/PreferenceActivity$Header:iconRes	I
        //     409: aload 13
        //     411: aload 14
        //     413: iconst_4
        //     414: invokevirtual 444	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     417: putfield 253	android/preference/PreferenceActivity$Header:fragment	Ljava/lang/String;
        //     420: aload 14
        //     422: invokevirtual 447	android/content/res/TypedArray:recycle	()V
        //     425: aload 10
        //     427: ifnonnull +12 -> 439
        //     430: new 449	android/os/Bundle
        //     433: dup
        //     434: invokespecial 450	android/os/Bundle:<init>	()V
        //     437: astore 10
        //     439: aload_3
        //     440: invokeinterface 394 1 0
        //     445: istore 19
        //     447: aload_3
        //     448: invokeinterface 360 1 0
        //     453: istore 20
        //     455: iload 20
        //     457: iconst_1
        //     458: if_icmpeq +175 -> 633
        //     461: iload 20
        //     463: iconst_3
        //     464: if_icmpne +14 -> 478
        //     467: aload_3
        //     468: invokeinterface 394 1 0
        //     473: iload 19
        //     475: if_icmple +158 -> 633
        //     478: iload 20
        //     480: iconst_3
        //     481: if_icmpeq -34 -> 447
        //     484: iload 20
        //     486: iconst_4
        //     487: if_icmpeq -40 -> 447
        //     490: aload_3
        //     491: invokeinterface 364 1 0
        //     496: astore 22
        //     498: aload 22
        //     500: ldc_w 452
        //     503: invokevirtual 258	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     506: ifeq +91 -> 597
        //     509: aload_0
        //     510: invokevirtual 343	android/preference/PreferenceActivity:getResources	()Landroid/content/res/Resources;
        //     513: ldc_w 452
        //     516: aload 7
        //     518: aload 10
        //     520: invokevirtual 456	android/content/res/Resources:parseBundleExtra	(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
        //     523: aload_3
        //     524: invokestatic 462	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     527: goto -80 -> 447
        //     530: astore 4
        //     532: new 166	java/lang/RuntimeException
        //     535: dup
        //     536: ldc_w 385
        //     539: aload 4
        //     541: invokespecial 388	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     544: athrow
        //     545: aload 13
        //     547: aload 15
        //     549: getfield 465	android/util/TypedValue:string	Ljava/lang/CharSequence;
        //     552: putfield 269	android/preference/PreferenceActivity$Header:title	Ljava/lang/CharSequence;
        //     555: goto -279 -> 276
        //     558: aload 13
        //     560: aload 16
        //     562: getfield 465	android/util/TypedValue:string	Ljava/lang/CharSequence;
        //     565: putfield 468	android/preference/PreferenceActivity$Header:summary	Ljava/lang/CharSequence;
        //     568: goto -252 -> 316
        //     571: aload 13
        //     573: aload 17
        //     575: getfield 465	android/util/TypedValue:string	Ljava/lang/CharSequence;
        //     578: putfield 471	android/preference/PreferenceActivity$Header:breadCrumbTitle	Ljava/lang/CharSequence;
        //     581: goto -225 -> 356
        //     584: aload 13
        //     586: aload 18
        //     588: getfield 465	android/util/TypedValue:string	Ljava/lang/CharSequence;
        //     591: putfield 474	android/preference/PreferenceActivity$Header:breadCrumbShortTitle	Ljava/lang/CharSequence;
        //     594: goto -197 -> 397
        //     597: aload 22
        //     599: ldc_w 475
        //     602: invokevirtual 258	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     605: ifeq +21 -> 626
        //     608: aload 13
        //     610: aload_0
        //     611: invokevirtual 343	android/preference/PreferenceActivity:getResources	()Landroid/content/res/Resources;
        //     614: aload_3
        //     615: aload 7
        //     617: invokestatic 481	android/content/Intent:parseIntent	(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/Intent;
        //     620: putfield 262	android/preference/PreferenceActivity$Header:intent	Landroid/content/Intent;
        //     623: goto -176 -> 447
        //     626: aload_3
        //     627: invokestatic 462	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     630: goto -183 -> 447
        //     633: aload 10
        //     635: invokevirtual 482	android/os/Bundle:size	()I
        //     638: ifle +13 -> 651
        //     641: aload 13
        //     643: aload 10
        //     645: putfield 272	android/preference/PreferenceActivity$Header:fragmentArguments	Landroid/os/Bundle;
        //     648: aconst_null
        //     649: astore 10
        //     651: aload_2
        //     652: aload 13
        //     654: invokeinterface 485 2 0
        //     659: pop
        //     660: goto -519 -> 141
        //     663: aload_3
        //     664: invokestatic 462	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     667: goto -526 -> 141
        //     670: aload_3
        //     671: ifnull +9 -> 680
        //     674: aload_3
        //     675: invokeinterface 391 1 0
        //     680: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	100	100	org/xmlpull/v1/XmlPullParserException
        //     133	527	100	org/xmlpull/v1/XmlPullParserException
        //     545	667	100	org/xmlpull/v1/XmlPullParserException
        //     2	100	115	finally
        //     102	115	115	finally
        //     133	527	115	finally
        //     532	545	115	finally
        //     545	667	115	finally
        //     2	100	530	java/io/IOException
        //     133	527	530	java/io/IOException
        //     545	667	530	java/io/IOException
    }

    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        if (this.mPreferenceManager != null)
            this.mPreferenceManager.dispatchActivityResult(paramInt1, paramInt2, paramIntent);
    }

    public void onBuildHeaders(List<Header> paramList)
    {
    }

    public Intent onBuildStartFragmentIntent(String paramString, Bundle paramBundle, int paramInt1, int paramInt2)
    {
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.setClass(this, getClass());
        localIntent.putExtra(":android:show_fragment", paramString);
        localIntent.putExtra(":android:show_fragment_args", paramBundle);
        localIntent.putExtra(":android:show_fragment_title", paramInt1);
        localIntent.putExtra(":android:show_fragment_short_title", paramInt2);
        localIntent.putExtra(":android:no_headers", true);
        return localIntent;
    }

    public void onContentChanged()
    {
        super.onContentChanged();
        if (this.mPreferenceManager != null)
            postBindPreferences();
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        setContentView(17367175);
        this.mListFooter = ((FrameLayout)findViewById(16909062));
        this.mPrefsContainer = ((ViewGroup)findViewById(16909063));
        boolean bool;
        String str1;
        Bundle localBundle;
        int i;
        int j;
        label176: CharSequence localCharSequence2;
        label233: label241: Intent localIntent;
        Button localButton1;
        Button localButton2;
        String str3;
        label400: String str2;
        if ((onIsHidingHeaders()) || (!onIsMultiPane()))
        {
            bool = true;
            this.mSinglePane = bool;
            str1 = getIntent().getStringExtra(":android:show_fragment");
            localBundle = getIntent().getBundleExtra(":android:show_fragment_args");
            i = getIntent().getIntExtra(":android:show_fragment_title", 0);
            j = getIntent().getIntExtra(":android:show_fragment_short_title", 0);
            if (paramBundle == null)
                break label457;
            ArrayList localArrayList = paramBundle.getParcelableArrayList(":android:headers");
            if (localArrayList != null)
            {
                this.mHeaders.addAll(localArrayList);
                int k = paramBundle.getInt(":android:cur_header", -1);
                if ((k >= 0) && (k < this.mHeaders.size()))
                    setSelectedHeader((Header)this.mHeaders.get(k));
            }
            if ((str1 == null) || (!this.mSinglePane))
                break label574;
            findViewById(16909061).setVisibility(8);
            this.mPrefsContainer.setVisibility(0);
            if (i != 0)
            {
                CharSequence localCharSequence1 = getText(i);
                if (j == 0)
                    break label568;
                localCharSequence2 = getText(j);
                showBreadCrumbs(localCharSequence1, localCharSequence2);
            }
            localIntent = getIntent();
            if (localIntent.getBooleanExtra("extra_prefs_show_button_bar", false))
            {
                findViewById(16908909).setVisibility(0);
                localButton1 = (Button)findViewById(16909065);
                View.OnClickListener local2 = new View.OnClickListener()
                {
                    public void onClick(View paramAnonymousView)
                    {
                        PreferenceActivity.this.setResult(0);
                        PreferenceActivity.this.finish();
                    }
                };
                localButton1.setOnClickListener(local2);
                localButton2 = (Button)findViewById(16909066);
                View.OnClickListener local3 = new View.OnClickListener()
                {
                    public void onClick(View paramAnonymousView)
                    {
                        PreferenceActivity.this.setResult(-1);
                        PreferenceActivity.this.finish();
                    }
                };
                localButton2.setOnClickListener(local3);
                this.mNextButton = ((Button)findViewById(16909067));
                Button localButton3 = this.mNextButton;
                View.OnClickListener local4 = new View.OnClickListener()
                {
                    public void onClick(View paramAnonymousView)
                    {
                        PreferenceActivity.this.setResult(-1);
                        PreferenceActivity.this.finish();
                    }
                };
                localButton3.setOnClickListener(local4);
                if (localIntent.hasExtra("extra_prefs_set_next_text"))
                {
                    str3 = localIntent.getStringExtra("extra_prefs_set_next_text");
                    if (!TextUtils.isEmpty(str3))
                        break label708;
                    this.mNextButton.setVisibility(8);
                }
                if (localIntent.hasExtra("extra_prefs_set_back_text"))
                {
                    str2 = localIntent.getStringExtra("extra_prefs_set_back_text");
                    if (!TextUtils.isEmpty(str2))
                        break label720;
                    localButton1.setVisibility(8);
                }
            }
        }
        while (true)
        {
            if (localIntent.getBooleanExtra("extra_prefs_show_skip", false))
                localButton2.setVisibility(0);
            return;
            bool = false;
            break;
            label457: if ((str1 != null) && (this.mSinglePane))
            {
                switchToHeader(str1, localBundle);
                if (i == 0)
                    break label176;
                CharSequence localCharSequence3 = getText(i);
                if (j != 0);
                for (CharSequence localCharSequence4 = getText(j); ; localCharSequence4 = null)
                {
                    showBreadCrumbs(localCharSequence3, localCharSequence4);
                    break;
                }
            }
            onBuildHeaders(this.mHeaders);
            if ((this.mHeaders.size() <= 0) || (this.mSinglePane))
                break label176;
            if (str1 == null)
            {
                switchToHeader(onGetInitialHeader());
                break label176;
            }
            switchToHeader(str1, localBundle);
            break label176;
            label568: localCharSequence2 = null;
            break label233;
            label574: if (this.mHeaders.size() > 0)
            {
                HeaderAdapter localHeaderAdapter = new HeaderAdapter(this, this.mHeaders);
                setListAdapter(localHeaderAdapter);
                if (this.mSinglePane)
                    break label241;
                getListView().setChoiceMode(1);
                if (this.mCurHeader != null)
                    setSelectedHeader(this.mCurHeader);
                this.mPrefsContainer.setVisibility(0);
                break label241;
            }
            setContentView(17367176);
            this.mListFooter = ((FrameLayout)findViewById(16909062));
            this.mPrefsContainer = ((ViewGroup)findViewById(16909064));
            PreferenceManager localPreferenceManager = new PreferenceManager(this, 100);
            this.mPreferenceManager = localPreferenceManager;
            this.mPreferenceManager.setOnPreferenceTreeClickListener(this);
            break label241;
            label708: this.mNextButton.setText(str3);
            break label400;
            label720: localButton1.setText(str2);
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
        if (this.mPreferenceManager != null)
            this.mPreferenceManager.dispatchActivityDestroy();
    }

    public Header onGetInitialHeader()
    {
        return (Header)this.mHeaders.get(0);
    }

    public Header onGetNewHeader()
    {
        return null;
    }

    public void onHeaderClick(Header paramHeader, int paramInt)
    {
        if (paramHeader.fragment != null)
            if (this.mSinglePane)
            {
                int i = paramHeader.breadCrumbTitleRes;
                int j = paramHeader.breadCrumbShortTitleRes;
                if (i == 0)
                {
                    i = paramHeader.titleRes;
                    j = 0;
                }
                startWithFragment(paramHeader.fragment, paramHeader.fragmentArguments, null, 0, i, j);
            }
        while (true)
        {
            return;
            switchToHeader(paramHeader);
            continue;
            if (paramHeader.intent != null)
                startActivity(paramHeader.intent);
        }
    }

    public boolean onIsHidingHeaders()
    {
        return getIntent().getBooleanExtra(":android:no_headers", false);
    }

    public boolean onIsMultiPane()
    {
        return getResources().getBoolean(17891331);
    }

    protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
    {
        super.onListItemClick(paramListView, paramView, paramInt, paramLong);
        if (this.mAdapter != null)
        {
            Object localObject = this.mAdapter.getItem(paramInt);
            if ((localObject instanceof Header))
                onHeaderClick((Header)localObject, paramInt);
        }
    }

    protected void onNewIntent(Intent paramIntent)
    {
        if (this.mPreferenceManager != null)
            this.mPreferenceManager.dispatchNewIntent(paramIntent);
    }

    public boolean onPreferenceStartFragment(PreferenceFragment paramPreferenceFragment, Preference paramPreference)
    {
        startPreferencePanel(paramPreference.getFragment(), paramPreference.getExtras(), paramPreference.getTitleRes(), paramPreference.getTitle(), null, 0);
        return true;
    }

    @Deprecated
    public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference)
    {
        return false;
    }

    protected void onRestoreInstanceState(Bundle paramBundle)
    {
        if (this.mPreferenceManager != null)
        {
            Bundle localBundle = paramBundle.getBundle(":android:preferences");
            if (localBundle != null)
            {
                PreferenceScreen localPreferenceScreen = getPreferenceScreen();
                if (localPreferenceScreen != null)
                {
                    localPreferenceScreen.restoreHierarchyState(localBundle);
                    this.mSavedInstanceState = paramBundle;
                }
            }
        }
        while (true)
        {
            return;
            super.onRestoreInstanceState(paramBundle);
        }
    }

    protected void onSaveInstanceState(Bundle paramBundle)
    {
        super.onSaveInstanceState(paramBundle);
        if (this.mHeaders.size() > 0)
        {
            paramBundle.putParcelableArrayList(":android:headers", this.mHeaders);
            if (this.mCurHeader != null)
            {
                int i = this.mHeaders.indexOf(this.mCurHeader);
                if (i >= 0)
                    paramBundle.putInt(":android:cur_header", i);
            }
        }
        if (this.mPreferenceManager != null)
        {
            PreferenceScreen localPreferenceScreen = getPreferenceScreen();
            if (localPreferenceScreen != null)
            {
                Bundle localBundle = new Bundle();
                localPreferenceScreen.saveHierarchyState(localBundle);
                paramBundle.putBundle(":android:preferences", localBundle);
            }
        }
    }

    protected void onStop()
    {
        super.onStop();
        if (this.mPreferenceManager != null)
            this.mPreferenceManager.dispatchActivityStop();
    }

    public void setListFooter(View paramView)
    {
        this.mListFooter.removeAllViews();
        this.mListFooter.addView(paramView, new FrameLayout.LayoutParams(-1, -2));
    }

    public void setParentTitle(CharSequence paramCharSequence1, CharSequence paramCharSequence2, View.OnClickListener paramOnClickListener)
    {
        if (this.mFragmentBreadCrumbs != null)
            this.mFragmentBreadCrumbs.setParentTitle(paramCharSequence1, paramCharSequence2, paramOnClickListener);
    }

    @Deprecated
    public void setPreferenceScreen(PreferenceScreen paramPreferenceScreen)
    {
        requirePreferenceManager();
        if ((this.mPreferenceManager.setPreferences(paramPreferenceScreen)) && (paramPreferenceScreen != null))
        {
            postBindPreferences();
            CharSequence localCharSequence = getPreferenceScreen().getTitle();
            if (localCharSequence != null)
                setTitle(localCharSequence);
        }
    }

    void setSelectedHeader(Header paramHeader)
    {
        this.mCurHeader = paramHeader;
        int i = this.mHeaders.indexOf(paramHeader);
        if (i >= 0)
            getListView().setItemChecked(i, true);
        while (true)
        {
            showBreadCrumbs(paramHeader);
            return;
            getListView().clearChoices();
        }
    }

    void showBreadCrumbs(Header paramHeader)
    {
        if (paramHeader != null)
        {
            CharSequence localCharSequence = paramHeader.getBreadCrumbTitle(getResources());
            if (localCharSequence == null)
                localCharSequence = paramHeader.getTitle(getResources());
            if (localCharSequence == null)
                localCharSequence = getTitle();
            showBreadCrumbs(localCharSequence, paramHeader.getBreadCrumbShortTitle(getResources()));
        }
        while (true)
        {
            return;
            showBreadCrumbs(getTitle(), null);
        }
    }

    public void showBreadCrumbs(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        View localView;
        if (this.mFragmentBreadCrumbs == null)
            localView = findViewById(16908310);
        while (true)
        {
            try
            {
                this.mFragmentBreadCrumbs = ((FragmentBreadCrumbs)localView);
                if (this.mFragmentBreadCrumbs == null)
                {
                    if (paramCharSequence1 != null)
                        setTitle(paramCharSequence1);
                    return;
                }
            }
            catch (ClassCastException localClassCastException)
            {
                continue;
                this.mFragmentBreadCrumbs.setMaxVisible(2);
                this.mFragmentBreadCrumbs.setActivity(this);
            }
            this.mFragmentBreadCrumbs.setTitle(paramCharSequence1, paramCharSequence2);
            this.mFragmentBreadCrumbs.setParentTitle(null, null, null);
        }
    }

    public void startPreferenceFragment(Fragment paramFragment, boolean paramBoolean)
    {
        FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
        localFragmentTransaction.replace(16909064, paramFragment);
        if (paramBoolean)
        {
            localFragmentTransaction.setTransition(4097);
            localFragmentTransaction.addToBackStack(":android:prefs");
        }
        while (true)
        {
            localFragmentTransaction.commitAllowingStateLoss();
            return;
            localFragmentTransaction.setTransition(4099);
        }
    }

    public void startPreferencePanel(String paramString, Bundle paramBundle, int paramInt1, CharSequence paramCharSequence, Fragment paramFragment, int paramInt2)
    {
        if (this.mSinglePane)
        {
            startWithFragment(paramString, paramBundle, paramFragment, paramInt2, paramInt1, 0);
            return;
        }
        Fragment localFragment = Fragment.instantiate(this, paramString, paramBundle);
        if (paramFragment != null)
            localFragment.setTargetFragment(paramFragment, paramInt2);
        FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
        localFragmentTransaction.replace(16909064, localFragment);
        if (paramInt1 != 0)
            localFragmentTransaction.setBreadCrumbTitle(paramInt1);
        while (true)
        {
            localFragmentTransaction.setTransition(4097);
            localFragmentTransaction.addToBackStack(":android:prefs");
            localFragmentTransaction.commitAllowingStateLoss();
            break;
            if (paramCharSequence != null)
                localFragmentTransaction.setBreadCrumbTitle(paramCharSequence);
        }
    }

    public void startWithFragment(String paramString, Bundle paramBundle, Fragment paramFragment, int paramInt)
    {
        startWithFragment(paramString, paramBundle, paramFragment, paramInt, 0, 0);
    }

    public void startWithFragment(String paramString, Bundle paramBundle, Fragment paramFragment, int paramInt1, int paramInt2, int paramInt3)
    {
        Intent localIntent = onBuildStartFragmentIntent(paramString, paramBundle, paramInt2, paramInt3);
        if (paramFragment == null)
            startActivity(localIntent);
        while (true)
        {
            return;
            paramFragment.startActivityForResult(localIntent, paramInt1);
        }
    }

    public void switchToHeader(Header paramHeader)
    {
        if (this.mCurHeader == paramHeader)
            getFragmentManager().popBackStack(":android:prefs", 1);
        while (true)
        {
            return;
            int i = this.mHeaders.indexOf(paramHeader) - this.mHeaders.indexOf(this.mCurHeader);
            switchToHeaderInner(paramHeader.fragment, paramHeader.fragmentArguments, i);
            setSelectedHeader(paramHeader);
        }
    }

    public void switchToHeader(String paramString, Bundle paramBundle)
    {
        setSelectedHeader(null);
        switchToHeaderInner(paramString, paramBundle, 0);
    }

    public static final class Header
        implements Parcelable
    {
        public static final Parcelable.Creator<Header> CREATOR = new Parcelable.Creator()
        {
            public PreferenceActivity.Header createFromParcel(Parcel paramAnonymousParcel)
            {
                return new PreferenceActivity.Header(paramAnonymousParcel);
            }

            public PreferenceActivity.Header[] newArray(int paramAnonymousInt)
            {
                return new PreferenceActivity.Header[paramAnonymousInt];
            }
        };
        public CharSequence breadCrumbShortTitle;
        public int breadCrumbShortTitleRes;
        public CharSequence breadCrumbTitle;
        public int breadCrumbTitleRes;
        public Bundle extras;
        public String fragment;
        public Bundle fragmentArguments;
        public int iconRes;
        public long id = -1L;
        public Intent intent;
        public CharSequence summary;
        public int summaryRes;
        public CharSequence title;
        public int titleRes;

        public Header()
        {
        }

        Header(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public CharSequence getBreadCrumbShortTitle(Resources paramResources)
        {
            if (this.breadCrumbShortTitleRes != 0);
            for (CharSequence localCharSequence = paramResources.getText(this.breadCrumbShortTitleRes); ; localCharSequence = this.breadCrumbShortTitle)
                return localCharSequence;
        }

        public CharSequence getBreadCrumbTitle(Resources paramResources)
        {
            if (this.breadCrumbTitleRes != 0);
            for (CharSequence localCharSequence = paramResources.getText(this.breadCrumbTitleRes); ; localCharSequence = this.breadCrumbTitle)
                return localCharSequence;
        }

        public CharSequence getSummary(Resources paramResources)
        {
            if (this.summaryRes != 0);
            for (CharSequence localCharSequence = paramResources.getText(this.summaryRes); ; localCharSequence = this.summary)
                return localCharSequence;
        }

        public CharSequence getTitle(Resources paramResources)
        {
            if (this.titleRes != 0);
            for (CharSequence localCharSequence = paramResources.getText(this.titleRes); ; localCharSequence = this.title)
                return localCharSequence;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.id = paramParcel.readLong();
            this.titleRes = paramParcel.readInt();
            this.title = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.summaryRes = paramParcel.readInt();
            this.summary = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.breadCrumbTitleRes = paramParcel.readInt();
            this.breadCrumbTitle = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.breadCrumbShortTitleRes = paramParcel.readInt();
            this.breadCrumbShortTitle = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.iconRes = paramParcel.readInt();
            this.fragment = paramParcel.readString();
            this.fragmentArguments = paramParcel.readBundle();
            if (paramParcel.readInt() != 0)
                this.intent = ((Intent)Intent.CREATOR.createFromParcel(paramParcel));
            this.extras = paramParcel.readBundle();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeLong(this.id);
            paramParcel.writeInt(this.titleRes);
            TextUtils.writeToParcel(this.title, paramParcel, paramInt);
            paramParcel.writeInt(this.summaryRes);
            TextUtils.writeToParcel(this.summary, paramParcel, paramInt);
            paramParcel.writeInt(this.breadCrumbTitleRes);
            TextUtils.writeToParcel(this.breadCrumbTitle, paramParcel, paramInt);
            paramParcel.writeInt(this.breadCrumbShortTitleRes);
            TextUtils.writeToParcel(this.breadCrumbShortTitle, paramParcel, paramInt);
            paramParcel.writeInt(this.iconRes);
            paramParcel.writeString(this.fragment);
            paramParcel.writeBundle(this.fragmentArguments);
            if (this.intent != null)
            {
                paramParcel.writeInt(1);
                this.intent.writeToParcel(paramParcel, paramInt);
            }
            while (true)
            {
                paramParcel.writeBundle(this.extras);
                return;
                paramParcel.writeInt(0);
            }
        }
    }

    private static class HeaderAdapter extends ArrayAdapter<PreferenceActivity.Header>
    {
        private LayoutInflater mInflater;

        public HeaderAdapter(Context paramContext, List<PreferenceActivity.Header> paramList)
        {
            super(0, paramList);
            this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            View localView;
            HeaderViewHolder localHeaderViewHolder;
            if (paramView == null)
            {
                localView = this.mInflater.inflate(17367171, paramViewGroup, false);
                localHeaderViewHolder = new HeaderViewHolder(null);
                localHeaderViewHolder.icon = ((ImageView)localView.findViewById(16908294));
                localHeaderViewHolder.title = ((TextView)localView.findViewById(16908310));
                localHeaderViewHolder.summary = ((TextView)localView.findViewById(16908304));
                localView.setTag(localHeaderViewHolder);
                PreferenceActivity.Header localHeader = (PreferenceActivity.Header)getItem(paramInt);
                localHeaderViewHolder.icon.setImageResource(localHeader.iconRes);
                localHeaderViewHolder.title.setText(localHeader.getTitle(getContext().getResources()));
                CharSequence localCharSequence = localHeader.getSummary(getContext().getResources());
                if (TextUtils.isEmpty(localCharSequence))
                    break label182;
                localHeaderViewHolder.summary.setVisibility(0);
                localHeaderViewHolder.summary.setText(localCharSequence);
            }
            while (true)
            {
                return localView;
                localView = paramView;
                localHeaderViewHolder = (HeaderViewHolder)localView.getTag();
                break;
                label182: localHeaderViewHolder.summary.setVisibility(8);
            }
        }

        private static class HeaderViewHolder
        {
            ImageView icon;
            TextView summary;
            TextView title;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceActivity
 * JD-Core Version:        0.6.2
 */