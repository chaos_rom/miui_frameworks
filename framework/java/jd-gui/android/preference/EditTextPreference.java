package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;

public class EditTextPreference extends DialogPreference
{
    private EditText mEditText;
    private String mText;

    public EditTextPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public EditTextPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842898);
    }

    public EditTextPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        this.mEditText = new EditText(paramContext, paramAttributeSet);
        this.mEditText.setId(16908291);
        this.mEditText.setEnabled(true);
    }

    public EditText getEditText()
    {
        return this.mEditText;
    }

    public String getText()
    {
        return this.mText;
    }

    protected boolean needInputMethod()
    {
        return true;
    }

    protected void onAddEditTextToDialogView(View paramView, EditText paramEditText)
    {
        ViewGroup localViewGroup = (ViewGroup)paramView.findViewById(16909060);
        if (localViewGroup != null)
            localViewGroup.addView(paramEditText, -1, -2);
    }

    protected void onBindDialogView(View paramView)
    {
        super.onBindDialogView(paramView);
        EditText localEditText = this.mEditText;
        localEditText.setText(getText());
        ViewParent localViewParent = localEditText.getParent();
        if (localViewParent != paramView)
        {
            if (localViewParent != null)
                ((ViewGroup)localViewParent).removeView(localEditText);
            onAddEditTextToDialogView(paramView, localEditText);
        }
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
        super.onDialogClosed(paramBoolean);
        if (paramBoolean)
        {
            String str = this.mEditText.getText().toString();
            if (callChangeListener(str))
                setText(str);
        }
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return paramTypedArray.getString(paramInt);
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class)))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            setText(localSavedState.text);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.text = getText();
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        if (paramBoolean);
        for (String str = getPersistedString(this.mText); ; str = (String)paramObject)
        {
            setText(str);
            return;
        }
    }

    public void setText(String paramString)
    {
        boolean bool1 = shouldDisableDependents();
        this.mText = paramString;
        persistString(paramString);
        boolean bool2 = shouldDisableDependents();
        if (bool2 != bool1)
            notifyDependencyChange(bool2);
    }

    public boolean shouldDisableDependents()
    {
        if ((TextUtils.isEmpty(this.mText)) || (super.shouldDisableDependents()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public EditTextPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new EditTextPreference.SavedState(paramAnonymousParcel);
            }

            public EditTextPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new EditTextPreference.SavedState[paramAnonymousInt];
            }
        };
        String text;

        public SavedState(Parcel paramParcel)
        {
            super();
            this.text = paramParcel.readString();
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeString(this.text);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.EditTextPreference
 * JD-Core Version:        0.6.2
 */