package android.preference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.util.Arrays;

public class MultiCheckPreference extends DialogPreference
{
    private CharSequence[] mEntries;
    private String[] mEntryValues;
    private boolean[] mOrigValues;
    private boolean[] mSetValues;
    private String mSummary;

    public MultiCheckPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public MultiCheckPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListPreference, 0, 0);
        this.mEntries = localTypedArray1.getTextArray(0);
        if (this.mEntries != null)
            setEntries(this.mEntries);
        setEntryValuesCS(localTypedArray1.getTextArray(1));
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, 0, 0);
        this.mSummary = localTypedArray2.getString(7);
        localTypedArray2.recycle();
    }

    private void setEntryValuesCS(CharSequence[] paramArrayOfCharSequence)
    {
        setValues(null);
        if (paramArrayOfCharSequence != null)
        {
            this.mEntryValues = new String[paramArrayOfCharSequence.length];
            for (int i = 0; i < paramArrayOfCharSequence.length; i++)
                this.mEntryValues[i] = paramArrayOfCharSequence[i].toString();
        }
    }

    public int findIndexOfValue(String paramString)
    {
        int i;
        if ((paramString != null) && (this.mEntryValues != null))
        {
            i = -1 + this.mEntryValues.length;
            if (i >= 0)
                if (!this.mEntryValues[i].equals(paramString));
        }
        while (true)
        {
            return i;
            i--;
            break;
            i = -1;
        }
    }

    public CharSequence[] getEntries()
    {
        return this.mEntries;
    }

    public String[] getEntryValues()
    {
        return this.mEntryValues;
    }

    public CharSequence getSummary()
    {
        if (this.mSummary == null);
        for (Object localObject = super.getSummary(); ; localObject = this.mSummary)
            return localObject;
    }

    public boolean getValue(int paramInt)
    {
        return this.mSetValues[paramInt];
    }

    public boolean[] getValues()
    {
        return this.mSetValues;
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
        super.onDialogClosed(paramBoolean);
        if ((paramBoolean) && (callChangeListener(getValues())));
        while (true)
        {
            return;
            System.arraycopy(this.mOrigValues, 0, this.mSetValues, 0, this.mSetValues.length);
        }
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return paramTypedArray.getString(paramInt);
    }

    protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder)
    {
        super.onPrepareDialogBuilder(paramBuilder);
        if ((this.mEntries == null) || (this.mEntryValues == null))
            throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
        this.mOrigValues = Arrays.copyOf(this.mSetValues, this.mSetValues.length);
        paramBuilder.setMultiChoiceItems(this.mEntries, this.mSetValues, new DialogInterface.OnMultiChoiceClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, boolean paramAnonymousBoolean)
            {
                MultiCheckPreference.this.mSetValues[paramAnonymousInt] = paramAnonymousBoolean;
            }
        });
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class)))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            setValues(localSavedState.values);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.values = getValues();
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
    }

    public void setEntries(int paramInt)
    {
        setEntries(getContext().getResources().getTextArray(paramInt));
    }

    public void setEntries(CharSequence[] paramArrayOfCharSequence)
    {
        this.mEntries = paramArrayOfCharSequence;
        this.mSetValues = new boolean[paramArrayOfCharSequence.length];
        this.mOrigValues = new boolean[paramArrayOfCharSequence.length];
    }

    public void setEntryValues(int paramInt)
    {
        setEntryValuesCS(getContext().getResources().getTextArray(paramInt));
    }

    public void setEntryValues(String[] paramArrayOfString)
    {
        this.mEntryValues = paramArrayOfString;
        Arrays.fill(this.mSetValues, false);
        Arrays.fill(this.mOrigValues, false);
    }

    public void setSummary(CharSequence paramCharSequence)
    {
        super.setSummary(paramCharSequence);
        if ((paramCharSequence == null) && (this.mSummary != null))
            this.mSummary = null;
        while (true)
        {
            return;
            if ((paramCharSequence != null) && (!paramCharSequence.equals(this.mSummary)))
                this.mSummary = paramCharSequence.toString();
        }
    }

    public void setValue(int paramInt, boolean paramBoolean)
    {
        this.mSetValues[paramInt] = paramBoolean;
    }

    public void setValues(boolean[] paramArrayOfBoolean)
    {
        boolean[] arrayOfBoolean;
        if (this.mSetValues != null)
        {
            Arrays.fill(this.mSetValues, false);
            Arrays.fill(this.mOrigValues, false);
            if (paramArrayOfBoolean != null)
            {
                arrayOfBoolean = this.mSetValues;
                if (paramArrayOfBoolean.length >= this.mSetValues.length)
                    break label54;
            }
        }
        label54: for (int i = paramArrayOfBoolean.length; ; i = this.mSetValues.length)
        {
            System.arraycopy(paramArrayOfBoolean, 0, arrayOfBoolean, 0, i);
            return;
        }
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public MultiCheckPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new MultiCheckPreference.SavedState(paramAnonymousParcel);
            }

            public MultiCheckPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new MultiCheckPreference.SavedState[paramAnonymousInt];
            }
        };
        boolean[] values;

        public SavedState(Parcel paramParcel)
        {
            super();
            this.values = paramParcel.createBooleanArray();
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeBooleanArray(this.values);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.MultiCheckPreference
 * JD-Core Version:        0.6.2
 */