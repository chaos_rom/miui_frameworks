package android.preference;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.Dialog;
import android.app.PreferenceDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

public final class PreferenceScreen extends PreferenceGroup
    implements AdapterView.OnItemClickListener, DialogInterface.OnDismissListener
{
    private Dialog mDialog;
    private ListView mListView;
    private ListAdapter mRootAdapter;

    public PreferenceScreen(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet, 16842891);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void showDialog(Bundle paramBundle)
    {
        Context localContext = getContext();
        if (this.mListView != null)
            this.mListView.setAdapter(null);
        View localView = ((LayoutInflater)localContext.getSystemService("layout_inflater")).inflate(17367177, null);
        this.mListView = ((ListView)localView.findViewById(16908298));
        bind(this.mListView);
        CharSequence localCharSequence = getTitle();
        PreferenceDialog localPreferenceDialog = new PreferenceDialog(localContext, localContext.getThemeResId());
        this.mDialog = localPreferenceDialog;
        if (TextUtils.isEmpty(localCharSequence))
            localPreferenceDialog.getWindow().requestFeature(1);
        while (true)
        {
            localPreferenceDialog.setContentView(localView);
            localPreferenceDialog.setOnDismissListener(this);
            if (paramBundle != null)
                localPreferenceDialog.onRestoreInstanceState(paramBundle);
            getPreferenceManager().addPreferencesScreen(localPreferenceDialog);
            localPreferenceDialog.show();
            return;
            localPreferenceDialog.setTitle(localCharSequence);
        }
    }

    public void bind(ListView paramListView)
    {
        paramListView.setOnItemClickListener(this);
        paramListView.setAdapter(getRootAdapter());
        onAttachedToActivity();
    }

    public Dialog getDialog()
    {
        return this.mDialog;
    }

    public ListAdapter getRootAdapter()
    {
        if (this.mRootAdapter == null)
            this.mRootAdapter = onCreateRootAdapter();
        return this.mRootAdapter;
    }

    protected boolean isOnSameScreenAsChildren()
    {
        return false;
    }

    protected void onClick()
    {
        if ((getIntent() != null) || (getFragment() != null) || (getPreferenceCount() == 0));
        while (true)
        {
            return;
            showDialog(null);
        }
    }

    protected ListAdapter onCreateRootAdapter()
    {
        return new PreferenceGroupAdapter(this);
    }

    public void onDismiss(DialogInterface paramDialogInterface)
    {
        this.mDialog = null;
        getPreferenceManager().removePreferencesScreen(paramDialogInterface);
    }

    public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        if ((paramAdapterView instanceof ListView))
            paramInt -= ((ListView)paramAdapterView).getHeaderViewsCount();
        Object localObject = getRootAdapter().getItem(paramInt);
        if (!(localObject instanceof Preference));
        while (true)
        {
            return;
            ((Preference)localObject).performClick(this);
        }
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class)))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            if (localSavedState.isDialogShowing)
                showDialog(localSavedState.dialogBundle);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Parcelable localParcelable = super.onSaveInstanceState();
        Dialog localDialog = this.mDialog;
        Object localObject;
        if ((localDialog == null) || (!localDialog.isShowing()))
            localObject = localParcelable;
        while (true)
        {
            return localObject;
            localObject = new SavedState(localParcelable);
            ((SavedState)localObject).isDialogShowing = true;
            ((SavedState)localObject).dialogBundle = localDialog.onSaveInstanceState();
        }
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public PreferenceScreen.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new PreferenceScreen.SavedState(paramAnonymousParcel);
            }

            public PreferenceScreen.SavedState[] newArray(int paramAnonymousInt)
            {
                return new PreferenceScreen.SavedState[paramAnonymousInt];
            }
        };
        Bundle dialogBundle;
        boolean isDialogShowing;

        public SavedState(Parcel paramParcel)
        {
            super();
            if (paramParcel.readInt() == i);
            while (true)
            {
                this.isDialogShowing = i;
                this.dialogBundle = paramParcel.readBundle();
                return;
                i = 0;
            }
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            if (this.isDialogShowing);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                paramParcel.writeBundle(this.dialogBundle);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.preference.PreferenceScreen
 * JD-Core Version:        0.6.2
 */