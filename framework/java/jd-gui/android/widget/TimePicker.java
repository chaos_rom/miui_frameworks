package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R.styleable;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TimePicker extends FrameLayout
{
    private static final boolean DEFAULT_ENABLED_STATE = true;
    private static final int HOURS_IN_HALF_DAY = 12;
    private static final OnTimeChangedListener NO_OP_CHANGE_LISTENER = new OnTimeChangedListener()
    {
        public void onTimeChanged(TimePicker paramAnonymousTimePicker, int paramAnonymousInt1, int paramAnonymousInt2)
        {
        }
    };
    private final Button mAmPmButton;
    private final NumberPicker mAmPmSpinner;
    private final EditText mAmPmSpinnerInput;
    private final String[] mAmPmStrings;
    private Locale mCurrentLocale;
    private final TextView mDivider;
    private final NumberPicker mHourSpinner;
    private final EditText mHourSpinnerInput;
    private boolean mIs24HourView;
    private boolean mIsAm;
    private boolean mIsEnabled = true;
    private final NumberPicker mMinuteSpinner;
    private final EditText mMinuteSpinnerInput;
    private OnTimeChangedListener mOnTimeChangedListener;
    private Calendar mTempCalendar;

    public TimePicker(Context paramContext)
    {
        this(paramContext, null);
    }

    public TimePicker(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843715);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public TimePicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        setCurrentLocale(Locale.getDefault());
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TimePicker, paramInt, 0);
        int i = localTypedArray.getResourceId(0, 17367228);
        localTypedArray.recycle();
        ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(i, this, true);
        this.mHourSpinner = ((NumberPicker)findViewById(16909134));
        this.mHourSpinner.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            public void onValueChange(NumberPicker paramAnonymousNumberPicker, int paramAnonymousInt1, int paramAnonymousInt2)
            {
                TimePicker.this.updateInputState();
                TimePicker localTimePicker;
                if ((!TimePicker.this.is24HourView()) && (((paramAnonymousInt1 == 11) && (paramAnonymousInt2 == 12)) || ((paramAnonymousInt1 == 12) && (paramAnonymousInt2 == 11))))
                {
                    localTimePicker = TimePicker.this;
                    if (TimePicker.this.mIsAm)
                        break label83;
                }
                label83: for (boolean bool = true; ; bool = false)
                {
                    TimePicker.access$102(localTimePicker, bool);
                    TimePicker.this.updateAmPmControl();
                    TimePicker.this.onTimeChanged();
                    return;
                }
            }
        });
        this.mHourSpinnerInput = ((EditText)this.mHourSpinner.findViewById(16909052));
        this.mHourSpinnerInput.setImeOptions(5);
        this.mDivider = ((TextView)findViewById(16909138));
        if (this.mDivider != null)
            this.mDivider.setText(17039548);
        this.mMinuteSpinner = ((NumberPicker)findViewById(16909135));
        this.mMinuteSpinner.setMinValue(0);
        this.mMinuteSpinner.setMaxValue(59);
        this.mMinuteSpinner.setOnLongPressUpdateInterval(100L);
        this.mMinuteSpinner.setFormatter(NumberPicker.TWO_DIGIT_FORMATTER);
        this.mMinuteSpinner.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            public void onValueChange(NumberPicker paramAnonymousNumberPicker, int paramAnonymousInt1, int paramAnonymousInt2)
            {
                boolean bool = true;
                TimePicker.this.updateInputState();
                int i = TimePicker.this.mMinuteSpinner.getMinValue();
                int j = TimePicker.this.mMinuteSpinner.getMaxValue();
                if ((paramAnonymousInt1 == j) && (paramAnonymousInt2 == i))
                {
                    m = 1 + TimePicker.this.mHourSpinner.getValue();
                    if ((!TimePicker.this.is24HourView()) && (m == 12))
                    {
                        localTimePicker2 = TimePicker.this;
                        if (!TimePicker.this.mIsAm)
                        {
                            TimePicker.access$102(localTimePicker2, bool);
                            TimePicker.this.updateAmPmControl();
                        }
                    }
                    else
                    {
                        TimePicker.this.mHourSpinner.setValue(m);
                    }
                }
                while ((paramAnonymousInt1 != i) || (paramAnonymousInt2 != j))
                    while (true)
                    {
                        int m;
                        TimePicker localTimePicker2;
                        TimePicker.this.onTimeChanged();
                        return;
                        bool = false;
                    }
                int k = -1 + TimePicker.this.mHourSpinner.getValue();
                TimePicker localTimePicker1;
                if ((!TimePicker.this.is24HourView()) && (k == 11))
                {
                    localTimePicker1 = TimePicker.this;
                    if (TimePicker.this.mIsAm)
                        break label224;
                }
                while (true)
                {
                    TimePicker.access$102(localTimePicker1, bool);
                    TimePicker.this.updateAmPmControl();
                    TimePicker.this.mHourSpinner.setValue(k);
                    break;
                    label224: bool = false;
                }
            }
        });
        this.mMinuteSpinner.setOnValueChangedListener(new OnMinuteChangeListener());
        this.mMinuteSpinnerInput = ((EditText)this.mMinuteSpinner.findViewById(16909052));
        this.mMinuteSpinnerInput.setImeOptions(5);
        this.mAmPmStrings = new DateFormatSymbols().getAmPmStrings();
        View localView = findViewById(16909136);
        if ((localView instanceof Button))
        {
            this.mAmPmSpinner = null;
            this.mAmPmSpinnerInput = null;
            this.mAmPmButton = ((Button)localView);
            this.mAmPmButton.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    paramAnonymousView.requestFocus();
                    TimePicker localTimePicker = TimePicker.this;
                    if (!TimePicker.this.mIsAm);
                    for (boolean bool = true; ; bool = false)
                    {
                        TimePicker.access$102(localTimePicker, bool);
                        TimePicker.this.updateAmPmControl();
                        TimePicker.this.onTimeChanged();
                        return;
                    }
                }
            });
        }
        while (true)
        {
            updateHourControl();
            updateAmPmControl();
            setOnTimeChangedListener(NO_OP_CHANGE_LISTENER);
            setCurrentHour(Integer.valueOf(this.mTempCalendar.get(11)));
            setCurrentMinute(Integer.valueOf(this.mTempCalendar.get(12)));
            if (!isEnabled())
                setEnabled(false);
            setContentDescriptions();
            if (getImportantForAccessibility() == 0)
                setImportantForAccessibility(1);
            return;
            this.mAmPmButton = null;
            this.mAmPmSpinner = ((NumberPicker)localView);
            this.mAmPmSpinner.setMinValue(0);
            this.mAmPmSpinner.setMaxValue(1);
            this.mAmPmSpinner.setDisplayedValues(this.mAmPmStrings);
            this.mAmPmSpinner.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
            {
                public void onValueChange(NumberPicker paramAnonymousNumberPicker, int paramAnonymousInt1, int paramAnonymousInt2)
                {
                    TimePicker.this.updateInputState();
                    paramAnonymousNumberPicker.requestFocus();
                    TimePicker localTimePicker = TimePicker.this;
                    if (!TimePicker.this.mIsAm);
                    for (boolean bool = true; ; bool = false)
                    {
                        TimePicker.access$102(localTimePicker, bool);
                        TimePicker.this.updateAmPmControl();
                        TimePicker.this.onTimeChanged();
                        return;
                    }
                }
            });
            this.mAmPmSpinnerInput = ((EditText)this.mAmPmSpinner.findViewById(16909052));
            this.mAmPmSpinnerInput.setImeOptions(6);
        }
    }

    private void onTimeChanged()
    {
        sendAccessibilityEvent(4);
        if (this.mOnTimeChangedListener != null)
            this.mOnTimeChangedListener.onTimeChanged(this, getCurrentHour().intValue(), getCurrentMinute().intValue());
    }

    private void setContentDescriptions()
    {
        trySetContentDescription(this.mMinuteSpinner, 16909051, 17040557);
        trySetContentDescription(this.mMinuteSpinner, 16909053, 17040558);
        trySetContentDescription(this.mHourSpinner, 16909051, 17040559);
        trySetContentDescription(this.mHourSpinner, 16909053, 17040560);
        if (this.mAmPmSpinner != null)
        {
            trySetContentDescription(this.mAmPmSpinner, 16909051, 17040561);
            trySetContentDescription(this.mAmPmSpinner, 16909053, 17040562);
        }
    }

    private void setCurrentLocale(Locale paramLocale)
    {
        if (paramLocale.equals(this.mCurrentLocale));
        while (true)
        {
            return;
            this.mCurrentLocale = paramLocale;
            this.mTempCalendar = Calendar.getInstance(paramLocale);
        }
    }

    private void trySetContentDescription(View paramView, int paramInt1, int paramInt2)
    {
        View localView = paramView.findViewById(paramInt1);
        if (localView != null)
            localView.setContentDescription(this.mContext.getString(paramInt2));
    }

    private void updateAmPmControl()
    {
        if (is24HourView())
            if (this.mAmPmSpinner != null)
                this.mAmPmSpinner.setVisibility(8);
        while (true)
        {
            sendAccessibilityEvent(4);
            return;
            this.mAmPmButton.setVisibility(8);
            continue;
            if (this.mIsAm);
            for (int i = 0; ; i = 1)
            {
                if (this.mAmPmSpinner == null)
                    break label81;
                this.mAmPmSpinner.setValue(i);
                this.mAmPmSpinner.setVisibility(0);
                break;
            }
            label81: this.mAmPmButton.setText(this.mAmPmStrings[i]);
            this.mAmPmButton.setVisibility(0);
        }
    }

    private void updateHourControl()
    {
        if (is24HourView())
        {
            this.mHourSpinner.setMinValue(0);
            this.mHourSpinner.setMaxValue(23);
            this.mHourSpinner.setFormatter(NumberPicker.TWO_DIGIT_FORMATTER);
        }
        while (true)
        {
            return;
            this.mHourSpinner.setMinValue(1);
            this.mHourSpinner.setMaxValue(12);
            this.mHourSpinner.setFormatter(null);
        }
    }

    private void updateInputState()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if (localInputMethodManager != null)
        {
            if (!localInputMethodManager.isActive(this.mHourSpinnerInput))
                break label37;
            this.mHourSpinnerInput.clearFocus();
            localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
        while (true)
        {
            return;
            label37: if (localInputMethodManager.isActive(this.mMinuteSpinnerInput))
            {
                this.mMinuteSpinnerInput.clearFocus();
                localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            }
            else if (localInputMethodManager.isActive(this.mAmPmSpinnerInput))
            {
                this.mAmPmSpinnerInput.clearFocus();
                localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    void callOnTimeChanged()
    {
        onTimeChanged();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callUpdateInputState()
    {
        updateInputState();
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        onPopulateAccessibilityEvent(paramAccessibilityEvent);
        return true;
    }

    public int getBaseline()
    {
        return this.mHourSpinner.getBaseline();
    }

    public Integer getCurrentHour()
    {
        int i = this.mHourSpinner.getValue();
        Integer localInteger;
        if (is24HourView())
            localInteger = Integer.valueOf(i);
        while (true)
        {
            return localInteger;
            if (this.mIsAm)
                localInteger = Integer.valueOf(i % 12);
            else
                localInteger = Integer.valueOf(12 + i % 12);
        }
    }

    public Integer getCurrentMinute()
    {
        return Integer.valueOf(this.mMinuteSpinner.getValue());
    }

    public boolean is24HourView()
    {
        return this.mIs24HourView;
    }

    public boolean isEnabled()
    {
        return this.mIsEnabled;
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        setCurrentLocale(paramConfiguration.locale);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TimePicker.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TimePicker.class.getName());
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
        if (this.mIs24HourView);
        for (int i = 0x1 | 0x80; ; i = 0x1 | 0x40)
        {
            this.mTempCalendar.set(11, getCurrentHour().intValue());
            this.mTempCalendar.set(12, getCurrentMinute().intValue());
            String str = DateUtils.formatDateTime(this.mContext, this.mTempCalendar.getTimeInMillis(), i);
            paramAccessibilityEvent.getText().add(str);
            return;
        }
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        setCurrentHour(Integer.valueOf(localSavedState.getHour()));
        setCurrentMinute(Integer.valueOf(localSavedState.getMinute()));
    }

    protected Parcelable onSaveInstanceState()
    {
        return new SavedState(super.onSaveInstanceState(), getCurrentHour().intValue(), getCurrentMinute().intValue(), null);
    }

    public void setCurrentHour(Integer paramInteger)
    {
        if ((paramInteger == null) || (paramInteger == getCurrentHour()))
            return;
        if (!is24HourView())
        {
            if (paramInteger.intValue() < 12)
                break label76;
            this.mIsAm = false;
            if (paramInteger.intValue() > 12)
                paramInteger = Integer.valueOf(-12 + paramInteger.intValue());
        }
        while (true)
        {
            updateAmPmControl();
            this.mHourSpinner.setValue(paramInteger.intValue());
            onTimeChanged();
            break;
            label76: this.mIsAm = true;
            if (paramInteger.intValue() == 0)
                paramInteger = Integer.valueOf(12);
        }
    }

    public void setCurrentMinute(Integer paramInteger)
    {
        if (paramInteger == getCurrentMinute());
        while (true)
        {
            return;
            this.mMinuteSpinner.setValue(paramInteger.intValue());
            onTimeChanged();
        }
    }

    public void setEnabled(boolean paramBoolean)
    {
        if (this.mIsEnabled == paramBoolean)
            return;
        super.setEnabled(paramBoolean);
        this.mMinuteSpinner.setEnabled(paramBoolean);
        if (this.mDivider != null)
            this.mDivider.setEnabled(paramBoolean);
        this.mHourSpinner.setEnabled(paramBoolean);
        if (this.mAmPmSpinner != null)
            this.mAmPmSpinner.setEnabled(paramBoolean);
        while (true)
        {
            this.mIsEnabled = paramBoolean;
            break;
            this.mAmPmButton.setEnabled(paramBoolean);
        }
    }

    public void setIs24HourView(Boolean paramBoolean)
    {
        if (this.mIs24HourView == paramBoolean.booleanValue());
        while (true)
        {
            return;
            this.mIs24HourView = paramBoolean.booleanValue();
            int i = getCurrentHour().intValue();
            updateHourControl();
            setCurrentHour(Integer.valueOf(i));
            updateAmPmControl();
        }
    }

    public void setOnTimeChangedListener(OnTimeChangedListener paramOnTimeChangedListener)
    {
        this.mOnTimeChangedListener = paramOnTimeChangedListener;
    }

    private static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public TimePicker.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new TimePicker.SavedState(paramAnonymousParcel, null);
            }

            public TimePicker.SavedState[] newArray(int paramAnonymousInt)
            {
                return new TimePicker.SavedState[paramAnonymousInt];
            }
        };
        private final int mHour;
        private final int mMinute;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.mHour = paramParcel.readInt();
            this.mMinute = paramParcel.readInt();
        }

        private SavedState(Parcelable paramParcelable, int paramInt1, int paramInt2)
        {
            super();
            this.mHour = paramInt1;
            this.mMinute = paramInt2;
        }

        public int getHour()
        {
            return this.mHour;
        }

        public int getMinute()
        {
            return this.mMinute;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.mHour);
            paramParcel.writeInt(this.mMinute);
        }
    }

    public static abstract interface OnTimeChangedListener
    {
        public abstract void onTimeChanged(TimePicker paramTimePicker, int paramInt1, int paramInt2);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    class OnMinuteChangeListener
        implements NumberPicker.OnValueChangeListener
    {
        OnMinuteChangeListener()
        {
        }

        public void onValueChange(NumberPicker paramNumberPicker, int paramInt1, int paramInt2)
        {
            TimePicker.this.callUpdateInputState();
            TimePicker.this.callOnTimeChanged();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TimePicker
 * JD-Core Version:        0.6.2
 */