package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.regex.Pattern;

public class TableLayout extends LinearLayout
{
    private SparseBooleanArray mCollapsedColumns;
    private boolean mInitialized;
    private int[] mMaxWidths;
    private PassThroughHierarchyChangeListener mPassThroughListener;
    private boolean mShrinkAllColumns;
    private SparseBooleanArray mShrinkableColumns;
    private boolean mStretchAllColumns;
    private SparseBooleanArray mStretchableColumns;

    public TableLayout(Context paramContext)
    {
        super(paramContext);
        initTableLayout();
    }

    public TableLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TableLayout);
        String str1 = localTypedArray.getString(0);
        String str2;
        if (str1 != null)
        {
            if (str1.charAt(0) == '*')
                this.mStretchAllColumns = true;
        }
        else
        {
            str2 = localTypedArray.getString(1);
            if (str2 != null)
            {
                if (str2.charAt(0) != '*')
                    break label113;
                this.mShrinkAllColumns = true;
            }
        }
        while (true)
        {
            String str3 = localTypedArray.getString(2);
            if (str3 != null)
                this.mCollapsedColumns = parseColumns(str3);
            localTypedArray.recycle();
            initTableLayout();
            return;
            this.mStretchableColumns = parseColumns(str1);
            break;
            label113: this.mShrinkableColumns = parseColumns(str2);
        }
    }

    private void findLargestCells(int paramInt)
    {
        int i = 1;
        int j = getChildCount();
        int k = 0;
        if (k < j)
        {
            View localView = getChildAt(k);
            if (localView.getVisibility() == 8);
            while (true)
            {
                k++;
                break;
                if ((localView instanceof TableRow))
                {
                    TableRow localTableRow = (TableRow)localView;
                    localTableRow.getLayoutParams().height = -2;
                    int[] arrayOfInt1 = localTableRow.getColumnsWidths(paramInt);
                    int m = arrayOfInt1.length;
                    if (i != 0)
                    {
                        if ((this.mMaxWidths == null) || (this.mMaxWidths.length != m))
                            this.mMaxWidths = new int[m];
                        System.arraycopy(arrayOfInt1, 0, this.mMaxWidths, 0, m);
                        i = 0;
                    }
                    else
                    {
                        int n = this.mMaxWidths.length;
                        int i1 = m - n;
                        if (i1 > 0)
                        {
                            int[] arrayOfInt3 = this.mMaxWidths;
                            this.mMaxWidths = new int[m];
                            System.arraycopy(arrayOfInt3, 0, this.mMaxWidths, 0, arrayOfInt3.length);
                            System.arraycopy(arrayOfInt1, arrayOfInt3.length, this.mMaxWidths, arrayOfInt3.length, i1);
                        }
                        int[] arrayOfInt2 = this.mMaxWidths;
                        int i2 = Math.min(n, m);
                        for (int i3 = 0; i3 < i2; i3++)
                            arrayOfInt2[i3] = Math.max(arrayOfInt2[i3], arrayOfInt1[i3]);
                    }
                }
            }
        }
    }

    private void initTableLayout()
    {
        if (this.mCollapsedColumns == null)
            this.mCollapsedColumns = new SparseBooleanArray();
        if (this.mStretchableColumns == null)
            this.mStretchableColumns = new SparseBooleanArray();
        if (this.mShrinkableColumns == null)
            this.mShrinkableColumns = new SparseBooleanArray();
        this.mPassThroughListener = new PassThroughHierarchyChangeListener(null);
        super.setOnHierarchyChangeListener(this.mPassThroughListener);
        this.mInitialized = true;
    }

    private void mutateColumnsWidth(SparseBooleanArray paramSparseBooleanArray, boolean paramBoolean, int paramInt1, int paramInt2)
    {
        int i = 0;
        int[] arrayOfInt = this.mMaxWidths;
        int j = arrayOfInt.length;
        if (paramBoolean);
        int m;
        for (int k = j; ; k = paramSparseBooleanArray.size())
        {
            m = (paramInt1 - paramInt2) / k;
            int n = getChildCount();
            for (int i1 = 0; i1 < n; i1++)
            {
                View localView = getChildAt(i1);
                if ((localView instanceof TableRow))
                    localView.forceLayout();
            }
        }
        if (!paramBoolean)
        {
            int i3 = 0;
            if (i3 < k)
            {
                int i7 = paramSparseBooleanArray.keyAt(i3);
                if (paramSparseBooleanArray.valueAt(i3))
                {
                    if (i7 >= j)
                        break label140;
                    arrayOfInt[i7] = (m + arrayOfInt[i7]);
                }
                while (true)
                {
                    i3++;
                    break;
                    label140: i++;
                }
            }
        }
        else
        {
            for (int i2 = 0; i2 < k; i2++)
                arrayOfInt[i2] = (m + arrayOfInt[i2]);
        }
        if ((i > 0) && (i < k))
        {
            int i4 = i * m / (k - i);
            int i5 = 0;
            if (i5 < k)
            {
                int i6 = paramSparseBooleanArray.keyAt(i5);
                if ((paramSparseBooleanArray.valueAt(i5)) && (i6 < j))
                {
                    if (i4 <= arrayOfInt[i6])
                        break label256;
                    arrayOfInt[i6] = 0;
                }
                while (true)
                {
                    i5++;
                    break;
                    label256: arrayOfInt[i6] = (i4 + arrayOfInt[i6]);
                }
            }
        }
    }

    private static SparseBooleanArray parseColumns(String paramString)
    {
        SparseBooleanArray localSparseBooleanArray = new SparseBooleanArray();
        String[] arrayOfString = Pattern.compile("\\s*,\\s*").split(paramString);
        int i = arrayOfString.length;
        int j = 0;
        while (true)
        {
            String str;
            if (j < i)
                str = arrayOfString[j];
            try
            {
                int k = Integer.parseInt(str);
                if (k >= 0)
                    localSparseBooleanArray.put(k, true);
                label55: j++;
                continue;
                return localSparseBooleanArray;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label55;
            }
        }
    }

    private void requestRowsLayout()
    {
        if (this.mInitialized)
        {
            int i = getChildCount();
            for (int j = 0; j < i; j++)
                getChildAt(j).requestLayout();
        }
    }

    private void shrinkAndStretchColumns(int paramInt)
    {
        if (this.mMaxWidths == null);
        while (true)
        {
            return;
            int i = 0;
            int[] arrayOfInt = this.mMaxWidths;
            int j = arrayOfInt.length;
            for (int k = 0; k < j; k++)
                i += arrayOfInt[k];
            int m = View.MeasureSpec.getSize(paramInt) - this.mPaddingLeft - this.mPaddingRight;
            if ((i > m) && ((this.mShrinkAllColumns) || (this.mShrinkableColumns.size() > 0)))
                mutateColumnsWidth(this.mShrinkableColumns, this.mShrinkAllColumns, m, i);
            else if ((i < m) && ((this.mStretchAllColumns) || (this.mStretchableColumns.size() > 0)))
                mutateColumnsWidth(this.mStretchableColumns, this.mStretchAllColumns, m, i);
        }
    }

    private void trackCollapsedColumns(View paramView)
    {
        if ((paramView instanceof TableRow))
        {
            TableRow localTableRow = (TableRow)paramView;
            SparseBooleanArray localSparseBooleanArray = this.mCollapsedColumns;
            int i = localSparseBooleanArray.size();
            for (int j = 0; j < i; j++)
            {
                int k = localSparseBooleanArray.keyAt(j);
                boolean bool = localSparseBooleanArray.valueAt(j);
                if (bool)
                    localTableRow.setColumnCollapsed(k, bool);
            }
        }
    }

    public void addView(View paramView)
    {
        super.addView(paramView);
        requestRowsLayout();
    }

    public void addView(View paramView, int paramInt)
    {
        super.addView(paramView, paramInt);
        requestRowsLayout();
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        super.addView(paramView, paramInt, paramLayoutParams);
        requestRowsLayout();
    }

    public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        super.addView(paramView, paramLayoutParams);
        requestRowsLayout();
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    protected LinearLayout.LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams();
    }

    protected LinearLayout.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    public boolean isColumnCollapsed(int paramInt)
    {
        return this.mCollapsedColumns.get(paramInt);
    }

    public boolean isColumnShrinkable(int paramInt)
    {
        if ((this.mShrinkAllColumns) || (this.mShrinkableColumns.get(paramInt)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isColumnStretchable(int paramInt)
    {
        if ((this.mStretchAllColumns) || (this.mStretchableColumns.get(paramInt)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isShrinkAllColumns()
    {
        return this.mShrinkAllColumns;
    }

    public boolean isStretchAllColumns()
    {
        return this.mStretchAllColumns;
    }

    void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if ((paramView instanceof TableRow))
            ((TableRow)paramView).setColumnsWidthConstraints(this.mMaxWidths);
        super.measureChildBeforeLayout(paramView, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    }

    void measureVertical(int paramInt1, int paramInt2)
    {
        findLargestCells(paramInt1);
        shrinkAndStretchColumns(paramInt1);
        super.measureVertical(paramInt1, paramInt2);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TableLayout.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TableLayout.class.getName());
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        layoutVertical();
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        measureVertical(paramInt1, paramInt2);
    }

    public void requestLayout()
    {
        if (this.mInitialized)
        {
            int i = getChildCount();
            for (int j = 0; j < i; j++)
                getChildAt(j).forceLayout();
        }
        super.requestLayout();
    }

    public void setColumnCollapsed(int paramInt, boolean paramBoolean)
    {
        this.mCollapsedColumns.put(paramInt, paramBoolean);
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            if ((localView instanceof TableRow))
                ((TableRow)localView).setColumnCollapsed(paramInt, paramBoolean);
        }
        requestRowsLayout();
    }

    public void setColumnShrinkable(int paramInt, boolean paramBoolean)
    {
        this.mShrinkableColumns.put(paramInt, paramBoolean);
        requestRowsLayout();
    }

    public void setColumnStretchable(int paramInt, boolean paramBoolean)
    {
        this.mStretchableColumns.put(paramInt, paramBoolean);
        requestRowsLayout();
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener paramOnHierarchyChangeListener)
    {
        PassThroughHierarchyChangeListener.access$102(this.mPassThroughListener, paramOnHierarchyChangeListener);
    }

    public void setShrinkAllColumns(boolean paramBoolean)
    {
        this.mShrinkAllColumns = paramBoolean;
    }

    public void setStretchAllColumns(boolean paramBoolean)
    {
        this.mStretchAllColumns = paramBoolean;
    }

    private class PassThroughHierarchyChangeListener
        implements ViewGroup.OnHierarchyChangeListener
    {
        private ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;

        private PassThroughHierarchyChangeListener()
        {
        }

        public void onChildViewAdded(View paramView1, View paramView2)
        {
            TableLayout.this.trackCollapsedColumns(paramView2);
            if (this.mOnHierarchyChangeListener != null)
                this.mOnHierarchyChangeListener.onChildViewAdded(paramView1, paramView2);
        }

        public void onChildViewRemoved(View paramView1, View paramView2)
        {
            if (this.mOnHierarchyChangeListener != null)
                this.mOnHierarchyChangeListener.onChildViewRemoved(paramView1, paramView2);
        }
    }

    public static class LayoutParams extends LinearLayout.LayoutParams
    {
        public LayoutParams()
        {
            super(-2);
        }

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(int paramInt1, int paramInt2, float paramFloat)
        {
            super(paramInt2, paramFloat);
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
        {
            super();
        }

        protected void setBaseAttributes(TypedArray paramTypedArray, int paramInt1, int paramInt2)
        {
            this.width = -1;
            if (paramTypedArray.hasValue(paramInt2));
            for (this.height = paramTypedArray.getLayoutDimension(paramInt2, "layout_height"); ; this.height = -2)
                return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TableLayout
 * JD-Core Version:        0.6.2
 */