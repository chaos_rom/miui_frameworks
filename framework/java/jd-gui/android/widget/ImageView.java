package android.widget;

import android.content.ContentResolver;
import android.content.ContentResolver.OpenResourceIdResult;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.io.PrintStream;
import java.util.List;

@RemoteViews.RemoteView
public class ImageView extends View
{
    private static final Matrix.ScaleToFit[] sS2FArray = arrayOfScaleToFit;
    private static final ScaleType[] sScaleTypeArray;
    private boolean mAdjustViewBounds = false;
    private int mAlpha = 255;
    private int mBaseline = -1;
    private boolean mBaselineAlignBottom = false;
    private ColorFilter mColorFilter;
    private boolean mColorMod = false;
    private boolean mCropToPadding;
    private Matrix mDrawMatrix = null;
    private Drawable mDrawable = null;
    private int mDrawableHeight;
    private int mDrawableWidth;
    private boolean mHaveFrame = false;
    private int mLevel = 0;
    private Matrix mMatrix;
    private int mMaxHeight = 2147483647;
    private int mMaxWidth = 2147483647;
    private boolean mMergeState = false;
    private int mResource = 0;
    private ScaleType mScaleType;
    private int[] mState = null;
    private RectF mTempDst = new RectF();
    private RectF mTempSrc = new RectF();
    private Uri mUri;
    private int mViewAlphaScale = 256;

    static
    {
        ScaleType[] arrayOfScaleType = new ScaleType[8];
        arrayOfScaleType[0] = ScaleType.MATRIX;
        arrayOfScaleType[1] = ScaleType.FIT_XY;
        arrayOfScaleType[2] = ScaleType.FIT_START;
        arrayOfScaleType[3] = ScaleType.FIT_CENTER;
        arrayOfScaleType[4] = ScaleType.FIT_END;
        arrayOfScaleType[5] = ScaleType.CENTER;
        arrayOfScaleType[6] = ScaleType.CENTER_CROP;
        arrayOfScaleType[7] = ScaleType.CENTER_INSIDE;
        sScaleTypeArray = arrayOfScaleType;
        Matrix.ScaleToFit[] arrayOfScaleToFit = new Matrix.ScaleToFit[4];
        arrayOfScaleToFit[0] = Matrix.ScaleToFit.FILL;
        arrayOfScaleToFit[1] = Matrix.ScaleToFit.START;
        arrayOfScaleToFit[2] = Matrix.ScaleToFit.CENTER;
        arrayOfScaleToFit[3] = Matrix.ScaleToFit.END;
    }

    public ImageView(Context paramContext)
    {
        super(paramContext);
        initImageView();
    }

    public ImageView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public ImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initImageView();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ImageView, paramInt, 0);
        Drawable localDrawable = localTypedArray.getDrawable(0);
        if (localDrawable != null)
            setImageDrawable(localDrawable);
        this.mBaselineAlignBottom = localTypedArray.getBoolean(6, false);
        this.mBaseline = localTypedArray.getDimensionPixelSize(8, -1);
        setAdjustViewBounds(localTypedArray.getBoolean(2, false));
        setMaxWidth(localTypedArray.getDimensionPixelSize(3, 2147483647));
        setMaxHeight(localTypedArray.getDimensionPixelSize(4, 2147483647));
        int i = localTypedArray.getInt(1, -1);
        if (i >= 0)
            setScaleType(sScaleTypeArray[i]);
        int j = localTypedArray.getInt(5, 0);
        if (j != 0)
            setColorFilter(j);
        int k = localTypedArray.getInt(9, 255);
        if (k != 255)
            setAlpha(k);
        this.mCropToPadding = localTypedArray.getBoolean(7, false);
        localTypedArray.recycle();
    }

    private void applyColorMod()
    {
        if ((this.mDrawable != null) && (this.mColorMod))
        {
            this.mDrawable = this.mDrawable.mutate();
            this.mDrawable.setColorFilter(this.mColorFilter);
            this.mDrawable.setAlpha(this.mAlpha * this.mViewAlphaScale >> 8);
        }
    }

    private void configureBounds()
    {
        if ((this.mDrawable == null) || (!this.mHaveFrame));
        while (true)
        {
            return;
            int i = this.mDrawableWidth;
            int j = this.mDrawableHeight;
            int k = getWidth() - this.mPaddingLeft - this.mPaddingRight;
            int m = getHeight() - this.mPaddingTop - this.mPaddingBottom;
            if (((i < 0) || (k == i)) && ((j < 0) || (m == j)));
            for (int n = 1; ; n = 0)
            {
                if ((i > 0) && (j > 0) && (ScaleType.FIT_XY != this.mScaleType))
                    break label122;
                this.mDrawable.setBounds(0, 0, k, m);
                this.mDrawMatrix = null;
                break;
            }
            label122: this.mDrawable.setBounds(0, 0, i, j);
            if (ScaleType.MATRIX == this.mScaleType)
            {
                if (this.mMatrix.isIdentity())
                    this.mDrawMatrix = null;
                else
                    this.mDrawMatrix = this.mMatrix;
            }
            else if (n != 0)
            {
                this.mDrawMatrix = null;
            }
            else if (ScaleType.CENTER == this.mScaleType)
            {
                this.mDrawMatrix = this.mMatrix;
                this.mDrawMatrix.setTranslate((int)(0.5F + 0.5F * (k - i)), (int)(0.5F + 0.5F * (m - j)));
            }
            else
            {
                if (ScaleType.CENTER_CROP == this.mScaleType)
                {
                    this.mDrawMatrix = this.mMatrix;
                    float f4 = 0.0F;
                    float f5 = 0.0F;
                    float f6;
                    if (i * m > k * j)
                    {
                        f6 = m / j;
                        f4 = 0.5F * (k - f6 * i);
                    }
                    while (true)
                    {
                        this.mDrawMatrix.setScale(f6, f6);
                        this.mDrawMatrix.postTranslate((int)(f4 + 0.5F), (int)(f5 + 0.5F));
                        break;
                        f6 = k / i;
                        f5 = 0.5F * (m - f6 * j);
                    }
                }
                if (ScaleType.CENTER_INSIDE == this.mScaleType)
                {
                    this.mDrawMatrix = this.mMatrix;
                    if ((i <= k) && (j <= m));
                    for (float f1 = 1.0F; ; f1 = Math.min(k / i, m / j))
                    {
                        float f2 = (int)(0.5F + 0.5F * (k - f1 * i));
                        float f3 = (int)(0.5F + 0.5F * (m - f1 * j));
                        this.mDrawMatrix.setScale(f1, f1);
                        this.mDrawMatrix.postTranslate(f2, f3);
                        break;
                    }
                }
                this.mTempSrc.set(0.0F, 0.0F, i, j);
                this.mTempDst.set(0.0F, 0.0F, k, m);
                this.mDrawMatrix = this.mMatrix;
                this.mDrawMatrix.setRectToRect(this.mTempSrc, this.mTempDst, scaleTypeToScaleToFit(this.mScaleType));
            }
        }
    }

    private void initImageView()
    {
        this.mMatrix = new Matrix();
        this.mScaleType = ScaleType.FIT_CENTER;
    }

    private void resizeFromDrawable()
    {
        Drawable localDrawable = this.mDrawable;
        if (localDrawable != null)
        {
            int i = localDrawable.getIntrinsicWidth();
            if (i < 0)
                i = this.mDrawableWidth;
            int j = localDrawable.getIntrinsicHeight();
            if (j < 0)
                j = this.mDrawableHeight;
            if ((i != this.mDrawableWidth) || (j != this.mDrawableHeight))
            {
                this.mDrawableWidth = i;
                this.mDrawableHeight = j;
                requestLayout();
            }
        }
    }

    private int resolveAdjustedSize(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1;
        int j = View.MeasureSpec.getMode(paramInt3);
        int k = View.MeasureSpec.getSize(paramInt3);
        switch (j)
        {
        default:
        case 0:
        case -2147483648:
        case 1073741824:
        }
        while (true)
        {
            return i;
            i = Math.min(paramInt1, paramInt2);
            continue;
            i = Math.min(Math.min(paramInt1, k), paramInt2);
            continue;
            i = k;
        }
    }

    private void resolveUri()
    {
        if (this.mDrawable != null);
        Object localObject;
        do
            while (true)
            {
                return;
                Resources localResources = getResources();
                if (localResources != null)
                {
                    localObject = null;
                    if (this.mResource != 0)
                        try
                        {
                            Drawable localDrawable3 = localResources.getDrawable(this.mResource);
                            localObject = localDrawable3;
                            updateDrawable((Drawable)localObject);
                        }
                        catch (Exception localException3)
                        {
                            while (true)
                            {
                                Log.w("ImageView", "Unable to find resource: " + this.mResource, localException3);
                                this.mUri = null;
                            }
                        }
                }
            }
        while (this.mUri == null);
        String str = this.mUri.getScheme();
        if ("android.resource".equals(str));
        while (true)
        {
            try
            {
                ContentResolver.OpenResourceIdResult localOpenResourceIdResult = this.mContext.getContentResolver().getResourceId(this.mUri);
                Drawable localDrawable2 = localOpenResourceIdResult.r.getDrawable(localOpenResourceIdResult.id);
                localObject = localDrawable2;
                if (localObject != null)
                    break;
                System.out.println("resolveUri failed on bad bitmap uri: " + this.mUri);
                this.mUri = null;
            }
            catch (Exception localException2)
            {
                Log.w("ImageView", "Unable to open content: " + this.mUri, localException2);
                continue;
            }
            if (("content".equals(str)) || ("file".equals(str)))
                try
                {
                    Drawable localDrawable1 = Drawable.createFromStream(this.mContext.getContentResolver().openInputStream(this.mUri), null);
                    localObject = localDrawable1;
                }
                catch (Exception localException1)
                {
                    Log.w("ImageView", "Unable to open content: " + this.mUri, localException1);
                }
            else
                localObject = Drawable.createFromPath(this.mUri.toString());
        }
    }

    private static Matrix.ScaleToFit scaleTypeToScaleToFit(ScaleType paramScaleType)
    {
        return sS2FArray[(-1 + paramScaleType.nativeInt)];
    }

    private void updateDrawable(Drawable paramDrawable)
    {
        if (this.mDrawable != null)
        {
            this.mDrawable.setCallback(null);
            unscheduleDrawable(this.mDrawable);
        }
        this.mDrawable = paramDrawable;
        if (paramDrawable != null)
        {
            paramDrawable.setCallback(this);
            if (paramDrawable.isStateful())
                paramDrawable.setState(getDrawableState());
            paramDrawable.setLevel(this.mLevel);
            this.mDrawableWidth = paramDrawable.getIntrinsicWidth();
            this.mDrawableHeight = paramDrawable.getIntrinsicHeight();
            applyColorMod();
            configureBounds();
        }
        while (true)
        {
            return;
            this.mDrawableHeight = -1;
            this.mDrawableWidth = -1;
        }
    }

    public final void clearColorFilter()
    {
        setColorFilter(null);
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        Drawable localDrawable = this.mDrawable;
        if ((localDrawable != null) && (localDrawable.isStateful()))
            localDrawable.setState(getDrawableState());
    }

    public boolean getAdjustViewBounds()
    {
        return this.mAdjustViewBounds;
    }

    @ViewDebug.ExportedProperty(category="layout")
    public int getBaseline()
    {
        if (this.mBaselineAlignBottom);
        for (int i = getMeasuredHeight(); ; i = this.mBaseline)
            return i;
    }

    public boolean getBaselineAlignBottom()
    {
        return this.mBaselineAlignBottom;
    }

    public ColorFilter getColorFilter()
    {
        return this.mColorFilter;
    }

    public boolean getCropToPadding()
    {
        return this.mCropToPadding;
    }

    public Drawable getDrawable()
    {
        return this.mDrawable;
    }

    public int getImageAlpha()
    {
        return this.mAlpha;
    }

    public Matrix getImageMatrix()
    {
        return this.mMatrix;
    }

    public int getMaxHeight()
    {
        return this.mMaxHeight;
    }

    public int getMaxWidth()
    {
        return this.mMaxWidth;
    }

    public int getResolvedLayoutDirection(Drawable paramDrawable)
    {
        if (paramDrawable == this.mDrawable);
        for (int i = getResolvedLayoutDirection(); ; i = super.getResolvedLayoutDirection(paramDrawable))
            return i;
    }

    public ScaleType getScaleType()
    {
        return this.mScaleType;
    }

    public boolean hasOverlappingRendering()
    {
        if (getBackground() != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        if (paramDrawable == this.mDrawable)
            invalidate();
        while (true)
        {
            return;
            super.invalidateDrawable(paramDrawable);
        }
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mDrawable != null)
            this.mDrawable.jumpToCurrentState();
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        Drawable localDrawable;
        if (this.mDrawable != null)
        {
            localDrawable = this.mDrawable;
            if (getVisibility() != 0)
                break label33;
        }
        label33: for (boolean bool = true; ; bool = false)
        {
            localDrawable.setVisible(bool, false);
            return;
        }
    }

    public int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt;
        if (this.mState == null)
            arrayOfInt = super.onCreateDrawableState(paramInt);
        while (true)
        {
            return arrayOfInt;
            if (!this.mMergeState)
                arrayOfInt = this.mState;
            else
                arrayOfInt = mergeDrawableStates(super.onCreateDrawableState(paramInt + this.mState.length), this.mState);
        }
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.mDrawable != null)
            this.mDrawable.setVisible(false, false);
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        if (this.mDrawable == null);
        while (true)
        {
            return;
            if ((this.mDrawableWidth != 0) && (this.mDrawableHeight != 0))
                if ((this.mDrawMatrix == null) && (this.mPaddingTop == 0) && (this.mPaddingLeft == 0))
                {
                    this.mDrawable.draw(paramCanvas);
                }
                else
                {
                    int i = paramCanvas.getSaveCount();
                    paramCanvas.save();
                    if (this.mCropToPadding)
                    {
                        int j = this.mScrollX;
                        int k = this.mScrollY;
                        paramCanvas.clipRect(j + this.mPaddingLeft, k + this.mPaddingTop, j + this.mRight - this.mLeft - this.mPaddingRight, k + this.mBottom - this.mTop - this.mPaddingBottom);
                    }
                    paramCanvas.translate(this.mPaddingLeft, this.mPaddingTop);
                    if (this.mDrawMatrix != null)
                        paramCanvas.concat(this.mDrawMatrix);
                    this.mDrawable.draw(paramCanvas);
                    paramCanvas.restoreToCount(i);
                }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ImageView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ImageView.class.getName());
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        resolveUri();
        float f = 0.0F;
        int i = 0;
        int j = 0;
        int k = View.MeasureSpec.getMode(paramInt1);
        int m = View.MeasureSpec.getMode(paramInt2);
        int i1;
        int n;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i9;
        if (this.mDrawable == null)
        {
            this.mDrawableWidth = -1;
            this.mDrawableHeight = -1;
            i1 = 0;
            n = 0;
            i2 = this.mPaddingLeft;
            i3 = this.mPaddingRight;
            i4 = this.mPaddingTop;
            i5 = this.mPaddingBottom;
            if ((i == 0) && (j == 0))
                break label331;
            i6 = resolveAdjustedSize(i3 + (n + i2), this.mMaxWidth, paramInt1);
            i7 = resolveAdjustedSize(i5 + (i1 + i4), this.mMaxHeight, paramInt2);
            if ((f != 0.0F) && (Math.abs((i6 - i2 - i3) / (i7 - i4 - i5) - f) > 1.0E-07D))
            {
                int i8 = 0;
                if (i != 0)
                {
                    int i10 = i3 + (i2 + (int)(f * (i7 - i4 - i5)));
                    if (i10 <= i6)
                    {
                        i6 = i10;
                        i8 = 1;
                    }
                }
                if ((i8 == 0) && (j != 0))
                {
                    i9 = i5 + (i4 + (int)((i6 - i2 - i3) / f));
                    if (i9 > i7);
                }
            }
        }
        label297: label325: label331: int i14;
        for (int i7 = i9; ; i7 = resolveSizeAndState(i14, paramInt2, 0))
        {
            setMeasuredDimension(i6, i7);
            return;
            n = this.mDrawableWidth;
            i1 = this.mDrawableHeight;
            if (n <= 0)
                n = 1;
            if (i1 <= 0)
                i1 = 1;
            if (!this.mAdjustViewBounds)
                break;
            if (k != 1073741824)
            {
                i = 1;
                if (m == 1073741824)
                    break label325;
            }
            for (j = 1; ; j = 0)
            {
                f = n / i1;
                break;
                i = 0;
                break label297;
            }
            int i11 = n + (i2 + i3);
            int i12 = i1 + (i4 + i5);
            int i13 = Math.max(i11, getSuggestedMinimumWidth());
            i14 = Math.max(i12, getSuggestedMinimumHeight());
            i6 = resolveSizeAndState(i13, paramInt1, 0);
        }
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
        CharSequence localCharSequence = getContentDescription();
        if (!TextUtils.isEmpty(localCharSequence))
            paramAccessibilityEvent.getText().add(localCharSequence);
    }

    @RemotableViewMethod
    public void setAdjustViewBounds(boolean paramBoolean)
    {
        this.mAdjustViewBounds = paramBoolean;
        if (paramBoolean)
            setScaleType(ScaleType.FIT_CENTER);
    }

    @RemotableViewMethod
    @Deprecated
    public void setAlpha(int paramInt)
    {
        int i = paramInt & 0xFF;
        if (this.mAlpha != i)
        {
            this.mAlpha = i;
            this.mColorMod = true;
            applyColorMod();
            invalidate();
        }
    }

    public void setBaseline(int paramInt)
    {
        if (this.mBaseline != paramInt)
        {
            this.mBaseline = paramInt;
            requestLayout();
        }
    }

    public void setBaselineAlignBottom(boolean paramBoolean)
    {
        if (this.mBaselineAlignBottom != paramBoolean)
        {
            this.mBaselineAlignBottom = paramBoolean;
            requestLayout();
        }
    }

    @RemotableViewMethod
    public final void setColorFilter(int paramInt)
    {
        setColorFilter(paramInt, PorterDuff.Mode.SRC_ATOP);
    }

    public final void setColorFilter(int paramInt, PorterDuff.Mode paramMode)
    {
        setColorFilter(new PorterDuffColorFilter(paramInt, paramMode));
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        if (this.mColorFilter != paramColorFilter)
        {
            this.mColorFilter = paramColorFilter;
            this.mColorMod = true;
            applyColorMod();
            invalidate();
        }
    }

    public void setCropToPadding(boolean paramBoolean)
    {
        if (this.mCropToPadding != paramBoolean)
        {
            this.mCropToPadding = paramBoolean;
            requestLayout();
            invalidate();
        }
    }

    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mHaveFrame = true;
        configureBounds();
        return bool;
    }

    @RemotableViewMethod
    public void setImageAlpha(int paramInt)
    {
        setAlpha(paramInt);
    }

    @RemotableViewMethod
    public void setImageBitmap(Bitmap paramBitmap)
    {
        setImageDrawable(new BitmapDrawable(this.mContext.getResources(), paramBitmap));
    }

    public void setImageDrawable(Drawable paramDrawable)
    {
        if (this.mDrawable != paramDrawable)
        {
            this.mResource = 0;
            this.mUri = null;
            int i = this.mDrawableWidth;
            int j = this.mDrawableHeight;
            updateDrawable(paramDrawable);
            if ((i != this.mDrawableWidth) || (j != this.mDrawableHeight))
                requestLayout();
            invalidate();
        }
    }

    @RemotableViewMethod
    public void setImageLevel(int paramInt)
    {
        this.mLevel = paramInt;
        if (this.mDrawable != null)
        {
            this.mDrawable.setLevel(paramInt);
            resizeFromDrawable();
        }
    }

    public void setImageMatrix(Matrix paramMatrix)
    {
        if ((paramMatrix != null) && (paramMatrix.isIdentity()))
            paramMatrix = null;
        if (((paramMatrix == null) && (!this.mMatrix.isIdentity())) || ((paramMatrix != null) && (!this.mMatrix.equals(paramMatrix))))
        {
            this.mMatrix.set(paramMatrix);
            configureBounds();
            invalidate();
        }
    }

    @RemotableViewMethod
    public void setImageResource(int paramInt)
    {
        if ((this.mUri != null) || (this.mResource != paramInt))
        {
            updateDrawable(null);
            this.mResource = paramInt;
            this.mUri = null;
            resolveUri();
            requestLayout();
            invalidate();
        }
    }

    public void setImageState(int[] paramArrayOfInt, boolean paramBoolean)
    {
        this.mState = paramArrayOfInt;
        this.mMergeState = paramBoolean;
        if (this.mDrawable != null)
        {
            refreshDrawableState();
            resizeFromDrawable();
        }
    }

    @RemotableViewMethod
    public void setImageURI(Uri paramUri)
    {
        if ((this.mResource != 0) || ((this.mUri != paramUri) && ((paramUri == null) || (this.mUri == null) || (!paramUri.equals(this.mUri)))))
        {
            updateDrawable(null);
            this.mResource = 0;
            this.mUri = paramUri;
            resolveUri();
            requestLayout();
            invalidate();
        }
    }

    @RemotableViewMethod
    public void setMaxHeight(int paramInt)
    {
        this.mMaxHeight = paramInt;
    }

    @RemotableViewMethod
    public void setMaxWidth(int paramInt)
    {
        this.mMaxWidth = paramInt;
    }

    public void setScaleType(ScaleType paramScaleType)
    {
        if (paramScaleType == null)
            throw new NullPointerException();
        if (this.mScaleType != paramScaleType)
        {
            this.mScaleType = paramScaleType;
            if (this.mScaleType != ScaleType.CENTER)
                break label51;
        }
        label51: for (boolean bool = true; ; bool = false)
        {
            setWillNotCacheDrawing(bool);
            requestLayout();
            invalidate();
            return;
        }
    }

    public void setSelected(boolean paramBoolean)
    {
        super.setSelected(paramBoolean);
        resizeFromDrawable();
    }

    @RemotableViewMethod
    public void setVisibility(int paramInt)
    {
        super.setVisibility(paramInt);
        Drawable localDrawable;
        if (this.mDrawable != null)
        {
            localDrawable = this.mDrawable;
            if (paramInt != 0)
                break label31;
        }
        label31: for (boolean bool = true; ; bool = false)
        {
            localDrawable.setVisible(bool, false);
            return;
        }
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((this.mDrawable == paramDrawable) || (super.verifyDrawable(paramDrawable)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static enum ScaleType
    {
        final int nativeInt;

        static
        {
            FIT_XY = new ScaleType("FIT_XY", 1, 1);
            FIT_START = new ScaleType("FIT_START", 2, 2);
            FIT_CENTER = new ScaleType("FIT_CENTER", 3, 3);
            FIT_END = new ScaleType("FIT_END", 4, 4);
            CENTER = new ScaleType("CENTER", 5, 5);
            CENTER_CROP = new ScaleType("CENTER_CROP", 6, 6);
            CENTER_INSIDE = new ScaleType("CENTER_INSIDE", 7, 7);
            ScaleType[] arrayOfScaleType = new ScaleType[8];
            arrayOfScaleType[0] = MATRIX;
            arrayOfScaleType[1] = FIT_XY;
            arrayOfScaleType[2] = FIT_START;
            arrayOfScaleType[3] = FIT_CENTER;
            arrayOfScaleType[4] = FIT_END;
            arrayOfScaleType[5] = CENTER;
            arrayOfScaleType[6] = CENTER_CROP;
            arrayOfScaleType[7] = CENTER_INSIDE;
        }

        private ScaleType(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ImageView
 * JD-Core Version:        0.6.2
 */