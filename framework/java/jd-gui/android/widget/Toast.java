package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.INotificationManager;
import android.app.INotificationManager.Stub;
import android.app.ITransientNotification.Stub;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerImpl;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

public class Toast
{
    public static final int LENGTH_LONG = 1;
    public static final int LENGTH_SHORT = 0;
    static final String TAG = "Toast";
    static final boolean localLOGV;
    private static INotificationManager sService;
    final Context mContext;
    int mDuration;
    View mNextView;
    final TN mTN;

    public Toast(Context paramContext)
    {
        this.mContext = paramContext;
        this.mTN = new TN();
        this.mTN.mY = paramContext.getResources().getDimensionPixelSize(17104905);
    }

    private static INotificationManager getService()
    {
        if (sService != null);
        for (INotificationManager localINotificationManager = sService; ; localINotificationManager = sService)
        {
            return localINotificationManager;
            sService = INotificationManager.Stub.asInterface(ServiceManager.getService("notification"));
        }
    }

    public static Toast makeText(Context paramContext, int paramInt1, int paramInt2)
        throws Resources.NotFoundException
    {
        return makeText(paramContext, paramContext.getResources().getText(paramInt1), paramInt2);
    }

    public static Toast makeText(Context paramContext, CharSequence paramCharSequence, int paramInt)
    {
        Toast localToast = new Toast(paramContext);
        View localView = ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(17367231, null);
        ((TextView)localView.findViewById(16908299)).setText(paramCharSequence);
        localToast.mNextView = localView;
        localToast.mDuration = paramInt;
        return localToast;
    }

    public void cancel()
    {
        this.mTN.hide();
        try
        {
            getService().cancelToast(this.mContext.getPackageName(), this.mTN);
            label26: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label26;
        }
    }

    public int getDuration()
    {
        return this.mDuration;
    }

    public int getGravity()
    {
        return this.mTN.mGravity;
    }

    public float getHorizontalMargin()
    {
        return this.mTN.mHorizontalMargin;
    }

    public float getVerticalMargin()
    {
        return this.mTN.mVerticalMargin;
    }

    public View getView()
    {
        return this.mNextView;
    }

    public int getXOffset()
    {
        return this.mTN.mX;
    }

    public int getYOffset()
    {
        return this.mTN.mY;
    }

    public void setDuration(int paramInt)
    {
        this.mDuration = paramInt;
    }

    public void setGravity(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mTN.mGravity = paramInt1;
        this.mTN.mX = paramInt2;
        this.mTN.mY = paramInt3;
    }

    public void setMargin(float paramFloat1, float paramFloat2)
    {
        this.mTN.mHorizontalMargin = paramFloat1;
        this.mTN.mVerticalMargin = paramFloat2;
    }

    public void setText(int paramInt)
    {
        setText(this.mContext.getText(paramInt));
    }

    public void setText(CharSequence paramCharSequence)
    {
        if (this.mNextView == null)
            throw new RuntimeException("This Toast was not created with Toast.makeText()");
        TextView localTextView = (TextView)this.mNextView.findViewById(16908299);
        if (localTextView == null)
            throw new RuntimeException("This Toast was not created with Toast.makeText()");
        localTextView.setText(paramCharSequence);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setType(int paramInt)
    {
        this.mTN.getParams().type = paramInt;
    }

    public void setView(View paramView)
    {
        this.mNextView = paramView;
    }

    public void show()
    {
        if (this.mNextView == null)
            throw new RuntimeException("setView must have been called");
        INotificationManager localINotificationManager = getService();
        String str = this.mContext.getPackageName();
        TN localTN = this.mTN;
        localTN.mNextView = this.mNextView;
        try
        {
            localINotificationManager.enqueueToast(str, localTN, this.mDuration);
            label54: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label54;
        }
    }

    private static class TN extends ITransientNotification.Stub
    {
        int mGravity = 81;
        final Handler mHandler = new Handler();
        final Runnable mHide = new Runnable()
        {
            public void run()
            {
                Toast.TN.this.handleHide();
                Toast.TN.this.mNextView = null;
            }
        };
        float mHorizontalMargin;
        View mNextView;
        private final WindowManager.LayoutParams mParams = new WindowManager.LayoutParams();
        final Runnable mShow = new Runnable()
        {
            public void run()
            {
                Toast.TN.this.handleShow();
            }
        };
        float mVerticalMargin;
        View mView;
        WindowManagerImpl mWM;
        int mX;
        int mY;

        TN()
        {
            WindowManager.LayoutParams localLayoutParams = this.mParams;
            localLayoutParams.height = -2;
            localLayoutParams.width = -2;
            localLayoutParams.flags = 152;
            localLayoutParams.format = -3;
            localLayoutParams.windowAnimations = 16973828;
            localLayoutParams.type = 2005;
            localLayoutParams.setTitle("Toast");
        }

        private void trySendAccessibilityEvent()
        {
            AccessibilityManager localAccessibilityManager = AccessibilityManager.getInstance(this.mView.getContext());
            if (!localAccessibilityManager.isEnabled());
            while (true)
            {
                return;
                AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(64);
                localAccessibilityEvent.setClassName(getClass().getName());
                localAccessibilityEvent.setPackageName(this.mView.getContext().getPackageName());
                this.mView.dispatchPopulateAccessibilityEvent(localAccessibilityEvent);
                localAccessibilityManager.sendAccessibilityEvent(localAccessibilityEvent);
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        WindowManager.LayoutParams getParams()
        {
            return this.mParams;
        }

        public void handleHide()
        {
            if (this.mView != null)
            {
                if (this.mView.getParent() != null)
                    this.mWM.removeView(this.mView);
                this.mView = null;
            }
        }

        public void handleShow()
        {
            if (this.mView != this.mNextView)
            {
                handleHide();
                this.mView = this.mNextView;
                this.mWM = WindowManagerImpl.getDefault();
                int i = this.mGravity;
                this.mParams.gravity = i;
                if ((i & 0x7) == 7)
                    this.mParams.horizontalWeight = 1.0F;
                if ((i & 0x70) == 112)
                    this.mParams.verticalWeight = 1.0F;
                this.mParams.x = this.mX;
                this.mParams.y = this.mY;
                this.mParams.verticalMargin = this.mVerticalMargin;
                this.mParams.horizontalMargin = this.mHorizontalMargin;
                if (this.mView.getParent() != null)
                    this.mWM.removeView(this.mView);
                this.mWM.addView(this.mView, this.mParams);
                trySendAccessibilityEvent();
            }
        }

        public void hide()
        {
            this.mHandler.post(this.mHide);
        }

        public void show()
        {
            this.mHandler.post(this.mShow);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Toast
 * JD-Core Version:        0.6.2
 */