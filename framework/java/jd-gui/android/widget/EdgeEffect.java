package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

public class EdgeEffect
{
    private static final float EPSILON = 0.001F;
    private static final float HELD_EDGE_SCALE_Y = 0.5F;
    private static final float MAX_ALPHA = 1.0F;
    private static final float MAX_GLOW_HEIGHT = 4.0F;
    private static final int MIN_VELOCITY = 100;
    private static final int MIN_WIDTH = 300;
    private static final int PULL_DECAY_TIME = 1000;
    private static final float PULL_DISTANCE_ALPHA_GLOW_FACTOR = 1.1F;
    private static final int PULL_DISTANCE_EDGE_FACTOR = 7;
    private static final int PULL_DISTANCE_GLOW_FACTOR = 7;
    private static final float PULL_EDGE_BEGIN = 0.6F;
    private static final float PULL_GLOW_BEGIN = 1.0F;
    private static final int PULL_TIME = 167;
    private static final int RECEDE_TIME = 1000;
    private static final int STATE_ABSORB = 2;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PULL = 1;
    private static final int STATE_PULL_DECAY = 4;
    private static final int STATE_RECEDE = 3;
    private static final String TAG = "EdgeEffect";
    private static final int VELOCITY_EDGE_FACTOR = 8;
    private static final int VELOCITY_GLOW_FACTOR = 16;
    private final Rect mBounds = new Rect();
    private float mDuration;
    private final Drawable mEdge;
    private float mEdgeAlpha;
    private float mEdgeAlphaFinish;
    private float mEdgeAlphaStart;
    private final int mEdgeHeight;
    private float mEdgeScaleY;
    private float mEdgeScaleYFinish;
    private float mEdgeScaleYStart;
    private final Drawable mGlow;
    private float mGlowAlpha;
    private float mGlowAlphaFinish;
    private float mGlowAlphaStart;
    private final int mGlowHeight;
    private float mGlowScaleY;
    private float mGlowScaleYFinish;
    private float mGlowScaleYStart;
    private final int mGlowWidth;
    private int mHeight;
    private final Interpolator mInterpolator;
    private final int mMaxEffectHeight;
    private final int mMinWidth;
    private float mPullDistance;
    private long mStartTime;
    private int mState = 0;
    private int mWidth;
    private int mX;
    private int mY;

    public EdgeEffect(Context paramContext)
    {
        Resources localResources = paramContext.getResources();
        this.mEdge = localResources.getDrawable(17302570);
        this.mGlow = localResources.getDrawable(17302571);
        this.mEdgeHeight = this.mEdge.getIntrinsicHeight();
        this.mGlowHeight = this.mGlow.getIntrinsicHeight();
        this.mGlowWidth = this.mGlow.getIntrinsicWidth();
        this.mMaxEffectHeight = ((int)(0.5F + Math.min(0.6F * (4.0F * this.mGlowHeight * this.mGlowHeight / this.mGlowWidth), 4.0F * this.mGlowHeight)));
        this.mMinWidth = ((int)(0.5F + 300.0F * localResources.getDisplayMetrics().density));
        this.mInterpolator = new DecelerateInterpolator();
    }

    private void update()
    {
        float f1 = Math.min((float)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime) / this.mDuration, 1.0F);
        float f2 = this.mInterpolator.getInterpolation(f1);
        this.mEdgeAlpha = (this.mEdgeAlphaStart + f2 * (this.mEdgeAlphaFinish - this.mEdgeAlphaStart));
        this.mEdgeScaleY = (this.mEdgeScaleYStart + f2 * (this.mEdgeScaleYFinish - this.mEdgeScaleYStart));
        this.mGlowAlpha = (this.mGlowAlphaStart + f2 * (this.mGlowAlphaFinish - this.mGlowAlphaStart));
        this.mGlowScaleY = (this.mGlowScaleYStart + f2 * (this.mGlowScaleYFinish - this.mGlowScaleYStart));
        if (f1 >= 0.999F)
            switch (this.mState)
            {
            default:
            case 2:
            case 1:
            case 4:
            case 3:
            }
        while (true)
        {
            return;
            this.mState = 3;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = 1000.0F;
            this.mEdgeAlphaStart = this.mEdgeAlpha;
            this.mEdgeScaleYStart = this.mEdgeScaleY;
            this.mGlowAlphaStart = this.mGlowAlpha;
            this.mGlowScaleYStart = this.mGlowScaleY;
            this.mEdgeAlphaFinish = 0.0F;
            this.mEdgeScaleYFinish = 0.0F;
            this.mGlowAlphaFinish = 0.0F;
            this.mGlowScaleYFinish = 0.0F;
            continue;
            this.mState = 4;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = 1000.0F;
            this.mEdgeAlphaStart = this.mEdgeAlpha;
            this.mEdgeScaleYStart = this.mEdgeScaleY;
            this.mGlowAlphaStart = this.mGlowAlpha;
            this.mGlowScaleYStart = this.mGlowScaleY;
            this.mEdgeAlphaFinish = 0.0F;
            this.mEdgeScaleYFinish = 0.0F;
            this.mGlowAlphaFinish = 0.0F;
            this.mGlowScaleYFinish = 0.0F;
            continue;
            if (this.mGlowScaleYFinish != 0.0F);
            for (float f3 = 1.0F / (this.mGlowScaleYFinish * this.mGlowScaleYFinish); ; f3 = 3.4028235E+38F)
            {
                this.mEdgeScaleY = (this.mEdgeScaleYStart + f3 * (f2 * (this.mEdgeScaleYFinish - this.mEdgeScaleYStart)));
                this.mState = 3;
                break;
            }
            this.mState = 0;
        }
    }

    public boolean draw(Canvas paramCanvas)
    {
        boolean bool = false;
        update();
        this.mGlow.setAlpha((int)(255.0F * Math.max(0.0F, Math.min(this.mGlowAlpha, 1.0F))));
        int i = (int)Math.min(0.6F * (this.mGlowHeight * this.mGlowScaleY * this.mGlowHeight / this.mGlowWidth), 4.0F * this.mGlowHeight);
        int j;
        if (this.mWidth < this.mMinWidth)
        {
            int m = (this.mWidth - this.mMinWidth) / 2;
            this.mGlow.setBounds(m, 0, this.mWidth - m, i);
            this.mGlow.draw(paramCanvas);
            this.mEdge.setAlpha((int)(255.0F * Math.max(0.0F, Math.min(this.mEdgeAlpha, 1.0F))));
            j = (int)(this.mEdgeHeight * this.mEdgeScaleY);
            if (this.mWidth >= this.mMinWidth)
                break label254;
            int k = (this.mWidth - this.mMinWidth) / 2;
            this.mEdge.setBounds(k, 0, this.mWidth - k, j);
        }
        while (true)
        {
            this.mEdge.draw(paramCanvas);
            if ((this.mState == 3) && (i == 0) && (j == 0))
                this.mState = 0;
            if (this.mState != 0)
                bool = true;
            return bool;
            this.mGlow.setBounds(0, 0, this.mWidth, i);
            break;
            label254: this.mEdge.setBounds(0, 0, this.mWidth, j);
        }
    }

    public void finish()
    {
        this.mState = 0;
    }

    public Rect getBounds(boolean paramBoolean)
    {
        int i = 0;
        this.mBounds.set(0, 0, this.mWidth, this.mMaxEffectHeight);
        Rect localRect = this.mBounds;
        int j = this.mX;
        int k = this.mY;
        if (paramBoolean)
            i = this.mMaxEffectHeight;
        localRect.offset(j, k - i);
        return this.mBounds;
    }

    public boolean isFinished()
    {
        if (this.mState == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onAbsorb(int paramInt)
    {
        this.mState = 2;
        int i = Math.max(100, Math.abs(paramInt));
        this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
        this.mDuration = (0.1F + 0.03F * i);
        this.mEdgeAlphaStart = 0.0F;
        this.mEdgeScaleYStart = 0.0F;
        this.mEdgeScaleY = 0.0F;
        this.mGlowAlphaStart = 0.5F;
        this.mGlowScaleYStart = 0.0F;
        this.mEdgeAlphaFinish = Math.max(0, Math.min(i * 8, 1));
        this.mEdgeScaleYFinish = Math.max(0.5F, Math.min(i * 8, 1.0F));
        this.mGlowScaleYFinish = Math.min(0.025F + 0.00015F * (i * (i / 100)), 1.75F);
        this.mGlowAlphaFinish = Math.max(this.mGlowAlphaStart, Math.min(1.0E-05F * (i * 16), 1.0F));
    }

    public void onPull(float paramFloat)
    {
        long l = AnimationUtils.currentAnimationTimeMillis();
        if ((this.mState == 4) && ((float)(l - this.mStartTime) < this.mDuration));
        while (true)
        {
            return;
            if (this.mState != 1)
                this.mGlowScaleY = 1.0F;
            this.mState = 1;
            this.mStartTime = l;
            this.mDuration = 167.0F;
            this.mPullDistance = (paramFloat + this.mPullDistance);
            float f1 = Math.abs(this.mPullDistance);
            float f2 = Math.max(0.6F, Math.min(f1, 1.0F));
            this.mEdgeAlphaStart = f2;
            this.mEdgeAlpha = f2;
            float f3 = Math.max(0.5F, Math.min(f1 * 7.0F, 1.0F));
            this.mEdgeScaleYStart = f3;
            this.mEdgeScaleY = f3;
            float f4 = Math.min(1.0F, this.mGlowAlpha + 1.1F * Math.abs(paramFloat));
            this.mGlowAlphaStart = f4;
            this.mGlowAlpha = f4;
            float f5 = Math.abs(paramFloat);
            if ((paramFloat > 0.0F) && (this.mPullDistance < 0.0F))
                f5 = -f5;
            if (this.mPullDistance == 0.0F)
                this.mGlowScaleY = 0.0F;
            float f6 = Math.min(4.0F, Math.max(0.0F, this.mGlowScaleY + f5 * 7.0F));
            this.mGlowScaleYStart = f6;
            this.mGlowScaleY = f6;
            this.mEdgeAlphaFinish = this.mEdgeAlpha;
            this.mEdgeScaleYFinish = this.mEdgeScaleY;
            this.mGlowAlphaFinish = this.mGlowAlpha;
            this.mGlowScaleYFinish = this.mGlowScaleY;
        }
    }

    public void onRelease()
    {
        this.mPullDistance = 0.0F;
        if ((this.mState != 1) && (this.mState != 4));
        while (true)
        {
            return;
            this.mState = 3;
            this.mEdgeAlphaStart = this.mEdgeAlpha;
            this.mEdgeScaleYStart = this.mEdgeScaleY;
            this.mGlowAlphaStart = this.mGlowAlpha;
            this.mGlowScaleYStart = this.mGlowScaleY;
            this.mEdgeAlphaFinish = 0.0F;
            this.mEdgeScaleYFinish = 0.0F;
            this.mGlowAlphaFinish = 0.0F;
            this.mGlowScaleYFinish = 0.0F;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = 1000.0F;
        }
    }

    void setPosition(int paramInt1, int paramInt2)
    {
        this.mX = paramInt1;
        this.mY = paramInt2;
    }

    public void setSize(int paramInt1, int paramInt2)
    {
        this.mWidth = paramInt1;
        this.mHeight = paramInt2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.EdgeEffect
 * JD-Core Version:        0.6.2
 */