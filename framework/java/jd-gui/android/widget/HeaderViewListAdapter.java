package android.widget;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public class HeaderViewListAdapter
    implements WrapperListAdapter, Filterable
{
    static final ArrayList<ListView.FixedViewInfo> EMPTY_INFO_LIST = new ArrayList();
    private final ListAdapter mAdapter;
    boolean mAreAllFixedViewsSelectable;
    ArrayList<ListView.FixedViewInfo> mFooterViewInfos;
    ArrayList<ListView.FixedViewInfo> mHeaderViewInfos;
    private final boolean mIsFilterable;

    public HeaderViewListAdapter(ArrayList<ListView.FixedViewInfo> paramArrayList1, ArrayList<ListView.FixedViewInfo> paramArrayList2, ListAdapter paramListAdapter)
    {
        this.mAdapter = paramListAdapter;
        this.mIsFilterable = (paramListAdapter instanceof Filterable);
        if (paramArrayList1 == null)
        {
            this.mHeaderViewInfos = EMPTY_INFO_LIST;
            if (paramArrayList2 != null)
                break label79;
            this.mFooterViewInfos = EMPTY_INFO_LIST;
            label39: if ((!areAllListInfosSelectable(this.mHeaderViewInfos)) || (!areAllListInfosSelectable(this.mFooterViewInfos)))
                break label87;
        }
        label79: label87: for (boolean bool = true; ; bool = false)
        {
            this.mAreAllFixedViewsSelectable = bool;
            return;
            this.mHeaderViewInfos = paramArrayList1;
            break;
            this.mFooterViewInfos = paramArrayList2;
            break label39;
        }
    }

    private boolean areAllListInfosSelectable(ArrayList<ListView.FixedViewInfo> paramArrayList)
    {
        if (paramArrayList != null)
        {
            Iterator localIterator = paramArrayList.iterator();
            do
                if (!localIterator.hasNext())
                    break;
            while (((ListView.FixedViewInfo)localIterator.next()).isSelectable);
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public boolean areAllItemsEnabled()
    {
        boolean bool = true;
        if ((this.mAdapter == null) || ((this.mAreAllFixedViewsSelectable) && (this.mAdapter.areAllItemsEnabled())));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public int getCount()
    {
        if (this.mAdapter != null);
        for (int i = getFootersCount() + getHeadersCount() + this.mAdapter.getCount(); ; i = getFootersCount() + getHeadersCount())
            return i;
    }

    public Filter getFilter()
    {
        if (this.mIsFilterable);
        for (Filter localFilter = ((Filterable)this.mAdapter).getFilter(); ; localFilter = null)
            return localFilter;
    }

    public int getFootersCount()
    {
        return this.mFooterViewInfos.size();
    }

    public int getHeadersCount()
    {
        return this.mHeaderViewInfos.size();
    }

    public Object getItem(int paramInt)
    {
        int i = getHeadersCount();
        Object localObject;
        if (paramInt < i)
            localObject = ((ListView.FixedViewInfo)this.mHeaderViewInfos.get(paramInt)).data;
        while (true)
        {
            return localObject;
            int j = paramInt - i;
            int k = 0;
            if (this.mAdapter != null)
            {
                k = this.mAdapter.getCount();
                if (j < k)
                    localObject = this.mAdapter.getItem(j);
            }
            else
            {
                localObject = ((ListView.FixedViewInfo)this.mFooterViewInfos.get(j - k)).data;
            }
        }
    }

    public long getItemId(int paramInt)
    {
        int i = getHeadersCount();
        int j;
        if ((this.mAdapter != null) && (paramInt >= i))
        {
            j = paramInt - i;
            if (j >= this.mAdapter.getCount());
        }
        for (long l = this.mAdapter.getItemId(j); ; l = -1L)
            return l;
    }

    public int getItemViewType(int paramInt)
    {
        int i = getHeadersCount();
        int k;
        if ((this.mAdapter != null) && (paramInt >= i))
        {
            k = paramInt - i;
            if (k >= this.mAdapter.getCount());
        }
        for (int j = this.mAdapter.getItemViewType(k); ; j = -2)
            return j;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        int i = getHeadersCount();
        View localView;
        if (paramInt < i)
            localView = ((ListView.FixedViewInfo)this.mHeaderViewInfos.get(paramInt)).view;
        while (true)
        {
            return localView;
            int j = paramInt - i;
            int k = 0;
            if (this.mAdapter != null)
            {
                k = this.mAdapter.getCount();
                if (j < k)
                    localView = this.mAdapter.getView(j, paramView, paramViewGroup);
            }
            else
            {
                localView = ((ListView.FixedViewInfo)this.mFooterViewInfos.get(j - k)).view;
            }
        }
    }

    public int getViewTypeCount()
    {
        if (this.mAdapter != null);
        for (int i = this.mAdapter.getViewTypeCount(); ; i = 1)
            return i;
    }

    public ListAdapter getWrappedAdapter()
    {
        return this.mAdapter;
    }

    public boolean hasStableIds()
    {
        if (this.mAdapter != null);
        for (boolean bool = this.mAdapter.hasStableIds(); ; bool = false)
            return bool;
    }

    public boolean isEmpty()
    {
        if ((this.mAdapter == null) || (this.mAdapter.isEmpty()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEnabled(int paramInt)
    {
        int i = getHeadersCount();
        boolean bool;
        if (paramInt < i)
            bool = ((ListView.FixedViewInfo)this.mHeaderViewInfos.get(paramInt)).isSelectable;
        while (true)
        {
            return bool;
            int j = paramInt - i;
            int k = 0;
            if (this.mAdapter != null)
            {
                k = this.mAdapter.getCount();
                if (j < k)
                    bool = this.mAdapter.isEnabled(j);
            }
            else
            {
                bool = ((ListView.FixedViewInfo)this.mFooterViewInfos.get(j - k)).isSelectable;
            }
        }
    }

    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        if (this.mAdapter != null)
            this.mAdapter.registerDataSetObserver(paramDataSetObserver);
    }

    public boolean removeFooter(View paramView)
    {
        boolean bool1 = true;
        boolean bool2 = false;
        int i = 0;
        if (i < this.mFooterViewInfos.size())
            if (((ListView.FixedViewInfo)this.mFooterViewInfos.get(i)).view == paramView)
            {
                this.mFooterViewInfos.remove(i);
                if ((areAllListInfosSelectable(this.mHeaderViewInfos)) && (areAllListInfosSelectable(this.mFooterViewInfos)))
                    bool2 = bool1;
                this.mAreAllFixedViewsSelectable = bool2;
            }
        while (true)
        {
            return bool1;
            i++;
            break;
            bool1 = false;
        }
    }

    public boolean removeHeader(View paramView)
    {
        boolean bool1 = true;
        boolean bool2 = false;
        int i = 0;
        if (i < this.mHeaderViewInfos.size())
            if (((ListView.FixedViewInfo)this.mHeaderViewInfos.get(i)).view == paramView)
            {
                this.mHeaderViewInfos.remove(i);
                if ((areAllListInfosSelectable(this.mHeaderViewInfos)) && (areAllListInfosSelectable(this.mFooterViewInfos)))
                    bool2 = bool1;
                this.mAreAllFixedViewsSelectable = bool2;
            }
        while (true)
        {
            return bool1;
            i++;
            break;
            bool1 = false;
        }
    }

    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        if (this.mAdapter != null)
            this.mAdapter.unregisterDataSetObserver(paramDataSetObserver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.HeaderViewListAdapter
 * JD-Core Version:        0.6.2
 */