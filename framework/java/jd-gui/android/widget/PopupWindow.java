package android.widget;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.IBinder;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import com.android.internal.R.styleable;
import java.lang.ref.WeakReference;

public class PopupWindow
{
    private static final int[] ABOVE_ANCHOR_STATE_SET = arrayOfInt;
    public static final int INPUT_METHOD_FROM_FOCUSABLE = 0;
    public static final int INPUT_METHOD_NEEDED = 1;
    public static final int INPUT_METHOD_NOT_NEEDED = 2;
    private boolean mAboveAnchor;
    private Drawable mAboveAnchorBackgroundDrawable;
    private boolean mAllowScrollingAnchorParent = true;
    private WeakReference<View> mAnchor;
    private int mAnchorXoff;
    private int mAnchorYoff;
    private int mAnimationStyle = -1;
    private Drawable mBackground;
    private Drawable mBelowAnchorBackgroundDrawable;
    private boolean mClipToScreen;
    private boolean mClippingEnabled = true;
    private View mContentView;
    private Context mContext;
    private int[] mDrawingLocation = new int[2];
    private boolean mFocusable;
    private int mHeight;
    private int mHeightMode;
    private boolean mIgnoreCheekPress = false;
    private int mInputMethodMode = 0;
    private boolean mIsDropdown;
    private boolean mIsShowing;
    private int mLastHeight;
    private int mLastWidth;
    private boolean mLayoutInScreen;
    private boolean mLayoutInsetDecor = false;
    private boolean mNotTouchModal;
    private OnDismissListener mOnDismissListener;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener = new ViewTreeObserver.OnScrollChangedListener()
    {
        public void onScrollChanged()
        {
            if (PopupWindow.this.mAnchor != null);
            for (View localView = (View)PopupWindow.this.mAnchor.get(); ; localView = null)
            {
                if ((localView != null) && (PopupWindow.this.mPopupView != null))
                {
                    WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)PopupWindow.this.mPopupView.getLayoutParams();
                    PopupWindow.this.updateAboveAnchor(PopupWindow.access$400(PopupWindow.this, localView, localLayoutParams, PopupWindow.this.mAnchorXoff, PopupWindow.this.mAnchorYoff));
                    PopupWindow.this.update(localLayoutParams.x, localLayoutParams.y, -1, -1, true);
                }
                return;
            }
        }
    };
    private boolean mOutsideTouchable = false;
    private int mPopupHeight;
    private View mPopupView;
    private int mPopupWidth;
    private int[] mScreenLocation = new int[2];
    private int mSoftInputMode = 1;
    private int mSplitTouchEnabled = -1;
    private Rect mTempRect = new Rect();
    private View.OnTouchListener mTouchInterceptor;
    private boolean mTouchable = true;
    private int mWidth;
    private int mWidthMode;
    private int mWindowLayoutType = 1000;
    private WindowManager mWindowManager;

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842922;
    }

    public PopupWindow()
    {
        this(null, 0, 0);
    }

    public PopupWindow(int paramInt1, int paramInt2)
    {
        this(null, paramInt1, paramInt2);
    }

    public PopupWindow(Context paramContext)
    {
        this(paramContext, null);
    }

    public PopupWindow(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842870);
    }

    public PopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        this(paramContext, paramAttributeSet, paramInt, 0);
    }

    public PopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
    {
        this.mContext = paramContext;
        this.mWindowManager = ((WindowManager)paramContext.getSystemService("window"));
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PopupWindow, paramInt1, paramInt2);
        this.mBackground = localTypedArray.getDrawable(0);
        int i = localTypedArray.getResourceId(1, -1);
        if (i == 16974314)
            i = -1;
        this.mAnimationStyle = i;
        int n;
        if ((this.mBackground instanceof StateListDrawable))
        {
            StateListDrawable localStateListDrawable = (StateListDrawable)this.mBackground;
            int j = localStateListDrawable.getStateDrawableIndex(ABOVE_ANCHOR_STATE_SET);
            int k = localStateListDrawable.getStateCount();
            int m = -1;
            n = 0;
            if (n < k)
            {
                if (n == j)
                    break label271;
                m = n;
            }
            if ((j == -1) || (m == -1))
                break label277;
            this.mAboveAnchorBackgroundDrawable = localStateListDrawable.getStateDrawable(j);
            this.mBelowAnchorBackgroundDrawable = localStateListDrawable.getStateDrawable(m);
        }
        while (true)
        {
            localTypedArray.recycle();
            return;
            label271: n++;
            break;
            label277: this.mBelowAnchorBackgroundDrawable = null;
            this.mAboveAnchorBackgroundDrawable = null;
        }
    }

    public PopupWindow(View paramView)
    {
        this(paramView, 0, 0);
    }

    public PopupWindow(View paramView, int paramInt1, int paramInt2)
    {
        this(paramView, paramInt1, paramInt2, false);
    }

    public PopupWindow(View paramView, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (paramView != null)
        {
            this.mContext = paramView.getContext();
            this.mWindowManager = ((WindowManager)this.mContext.getSystemService("window"));
        }
        setContentView(paramView);
        setWidth(paramInt1);
        setHeight(paramInt2);
        setFocusable(paramBoolean);
    }

    private int computeAnimationResource()
    {
        int i;
        if (this.mAnimationStyle == -1)
            if (this.mIsDropdown)
                if (this.mAboveAnchor)
                    i = 16974308;
        while (true)
        {
            return i;
            i = 16974307;
            continue;
            i = 0;
            continue;
            i = this.mAnimationStyle;
        }
    }

    private int computeFlags(int paramInt)
    {
        int i = paramInt & 0xFF797DE7;
        if (this.mIgnoreCheekPress)
            i |= 32768;
        if (!this.mFocusable)
        {
            i |= 8;
            if (this.mInputMethodMode == 1)
                i |= 131072;
        }
        while (true)
        {
            if (!this.mTouchable)
                i |= 16;
            if (this.mOutsideTouchable)
                i |= 262144;
            if (!this.mClippingEnabled)
                i |= 512;
            if (isSplitTouchEnabled())
                i |= 8388608;
            if (this.mLayoutInScreen)
                i |= 256;
            if (this.mLayoutInsetDecor)
                i |= 65536;
            if (this.mNotTouchModal)
                i |= 32;
            return i;
            if (this.mInputMethodMode == 2)
                i |= 131072;
        }
    }

    private WindowManager.LayoutParams createPopupLayout(IBinder paramIBinder)
    {
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.gravity = 51;
        int i = this.mWidth;
        this.mLastWidth = i;
        localLayoutParams.width = i;
        int j = this.mHeight;
        this.mLastHeight = j;
        localLayoutParams.height = j;
        if (this.mBackground != null);
        for (localLayoutParams.format = this.mBackground.getOpacity(); ; localLayoutParams.format = -3)
        {
            localLayoutParams.flags = computeFlags(localLayoutParams.flags);
            localLayoutParams.type = this.mWindowLayoutType;
            localLayoutParams.token = paramIBinder;
            localLayoutParams.softInputMode = this.mSoftInputMode;
            localLayoutParams.setTitle("PopupWindow:" + Integer.toHexString(hashCode()));
            return localLayoutParams;
        }
    }

    private boolean findDropDownPosition(View paramView, WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2)
    {
        int i = paramView.getHeight();
        paramView.getLocationInWindow(this.mDrawingLocation);
        paramLayoutParams.x = (paramInt1 + this.mDrawingLocation[0]);
        paramLayoutParams.y = (paramInt2 + (i + this.mDrawingLocation[1]));
        boolean bool = false;
        paramLayoutParams.gravity = 51;
        paramView.getLocationOnScreen(this.mScreenLocation);
        Rect localRect = new Rect();
        paramView.getWindowVisibleDisplayFrame(localRect);
        int j = paramInt2 + (i + this.mScreenLocation[1]);
        View localView = paramView.getRootView();
        label304: int n;
        if ((j + this.mPopupHeight > localRect.bottom) || (paramLayoutParams.x + this.mPopupWidth - localView.getWidth() > 0))
        {
            if (this.mAllowScrollingAnchorParent)
            {
                int i1 = paramView.getScrollX();
                int i2 = paramView.getScrollY();
                paramView.requestRectangleOnScreen(new Rect(i1, i2, paramInt1 + (i1 + this.mPopupWidth), paramInt2 + (i2 + this.mPopupHeight + paramView.getHeight())), true);
            }
            paramView.getLocationInWindow(this.mDrawingLocation);
            paramLayoutParams.x = (paramInt1 + this.mDrawingLocation[0]);
            paramLayoutParams.y = (paramInt2 + (this.mDrawingLocation[1] + paramView.getHeight()));
            paramView.getLocationOnScreen(this.mScreenLocation);
            if (localRect.bottom - this.mScreenLocation[1] - paramView.getHeight() - paramInt2 < this.mScreenLocation[1] - paramInt2 - localRect.top)
            {
                bool = true;
                if (!bool)
                    break label448;
                paramLayoutParams.gravity = 83;
                paramLayoutParams.y = (paramInt2 + (localView.getHeight() - this.mDrawingLocation[1]));
            }
        }
        else if (this.mClipToScreen)
        {
            int k = localRect.right - localRect.left;
            int m = paramLayoutParams.x + paramLayoutParams.width;
            if (m > k)
                paramLayoutParams.x -= m - k;
            if (paramLayoutParams.x < localRect.left)
            {
                paramLayoutParams.x = localRect.left;
                paramLayoutParams.width = Math.min(paramLayoutParams.width, k);
            }
            if (!bool)
                break label469;
            n = paramInt2 + this.mScreenLocation[1] - this.mPopupHeight;
            if (n >= 0);
        }
        label448: label469: for (paramLayoutParams.y = (n + paramLayoutParams.y); ; paramLayoutParams.y = Math.max(paramLayoutParams.y, localRect.top))
        {
            paramLayoutParams.gravity = (0x10000000 | paramLayoutParams.gravity);
            return bool;
            bool = false;
            break;
            paramLayoutParams.y = (paramInt2 + (this.mDrawingLocation[1] + paramView.getHeight()));
            break label304;
        }
    }

    private void invokePopup(WindowManager.LayoutParams paramLayoutParams)
    {
        if (this.mContext != null)
            paramLayoutParams.packageName = this.mContext.getPackageName();
        this.mPopupView.setFitsSystemWindows(this.mLayoutInsetDecor);
        this.mWindowManager.addView(this.mPopupView, paramLayoutParams);
    }

    private void preparePopup(WindowManager.LayoutParams paramLayoutParams)
    {
        if ((this.mContentView == null) || (this.mContext == null) || (this.mWindowManager == null))
            throw new IllegalStateException("You must specify a valid content view by calling setContentView() before attempting to show the popup.");
        PopupViewContainer localPopupViewContainer;
        if (this.mBackground != null)
        {
            ViewGroup.LayoutParams localLayoutParams = this.mContentView.getLayoutParams();
            int i = -1;
            if ((localLayoutParams != null) && (localLayoutParams.height == -2))
                i = -2;
            localPopupViewContainer = new PopupViewContainer(this.mContext);
            FrameLayout.LayoutParams localLayoutParams1 = new FrameLayout.LayoutParams(-1, i);
            localPopupViewContainer.setBackgroundDrawable(this.mBackground);
            localPopupViewContainer.addView(this.mContentView, localLayoutParams1);
        }
        for (this.mPopupView = localPopupViewContainer; ; this.mPopupView = this.mContentView)
        {
            this.mPopupWidth = paramLayoutParams.width;
            this.mPopupHeight = paramLayoutParams.height;
            return;
        }
    }

    private void registerForScrollChanged(View paramView, int paramInt1, int paramInt2)
    {
        unregisterForScrollChanged();
        this.mAnchor = new WeakReference(paramView);
        ViewTreeObserver localViewTreeObserver = paramView.getViewTreeObserver();
        if (localViewTreeObserver != null)
            localViewTreeObserver.addOnScrollChangedListener(this.mOnScrollChangedListener);
        this.mAnchorXoff = paramInt1;
        this.mAnchorYoff = paramInt2;
    }

    private void unregisterForScrollChanged()
    {
        WeakReference localWeakReference = this.mAnchor;
        View localView = null;
        if (localWeakReference != null)
            localView = (View)localWeakReference.get();
        if (localView != null)
            localView.getViewTreeObserver().removeOnScrollChangedListener(this.mOnScrollChangedListener);
        this.mAnchor = null;
    }

    private void update(View paramView, boolean paramBoolean1, int paramInt1, int paramInt2, boolean paramBoolean2, int paramInt3, int paramInt4)
    {
        if ((!isShowing()) || (this.mContentView == null))
            return;
        WeakReference localWeakReference = this.mAnchor;
        int i;
        label45: label79: WindowManager.LayoutParams localLayoutParams;
        label109: label122: int m;
        label154: int n;
        if ((paramBoolean1) && ((this.mAnchorXoff != paramInt1) || (this.mAnchorYoff != paramInt2)))
        {
            i = 1;
            if ((localWeakReference != null) && (localWeakReference.get() == paramView) && ((i == 0) || (this.mIsDropdown)))
                break label214;
            registerForScrollChanged(paramView, paramInt1, paramInt2);
            localLayoutParams = (WindowManager.LayoutParams)this.mPopupView.getLayoutParams();
            if (paramBoolean2)
            {
                if (paramInt3 != -1)
                    break label233;
                paramInt3 = this.mPopupWidth;
                if (paramInt4 != -1)
                    break label242;
                paramInt4 = this.mPopupHeight;
            }
            int j = localLayoutParams.x;
            int k = localLayoutParams.y;
            if (!paramBoolean1)
                break label251;
            updateAboveAnchor(findDropDownPosition(paramView, localLayoutParams, paramInt1, paramInt2));
            m = localLayoutParams.x;
            n = localLayoutParams.y;
            if ((j == localLayoutParams.x) && (k == localLayoutParams.y))
                break label273;
        }
        label273: for (boolean bool = true; ; bool = false)
        {
            update(m, n, paramInt3, paramInt4, bool);
            break;
            i = 0;
            break label45;
            label214: if (i == 0)
                break label79;
            this.mAnchorXoff = paramInt1;
            this.mAnchorYoff = paramInt2;
            break label79;
            label233: this.mPopupWidth = paramInt3;
            break label109;
            label242: this.mPopupHeight = paramInt4;
            break label122;
            label251: updateAboveAnchor(findDropDownPosition(paramView, localLayoutParams, this.mAnchorXoff, this.mAnchorYoff));
            break label154;
        }
    }

    private void updateAboveAnchor(boolean paramBoolean)
    {
        if (paramBoolean != this.mAboveAnchor)
        {
            this.mAboveAnchor = paramBoolean;
            if (this.mBackground != null)
            {
                if (this.mAboveAnchorBackgroundDrawable == null)
                    break label60;
                if (!this.mAboveAnchor)
                    break label46;
                this.mPopupView.setBackgroundDrawable(this.mAboveAnchorBackgroundDrawable);
            }
        }
        while (true)
        {
            return;
            label46: this.mPopupView.setBackgroundDrawable(this.mBelowAnchorBackgroundDrawable);
            continue;
            label60: this.mPopupView.refreshDrawableState();
        }
    }

    public void dismiss()
    {
        if ((isShowing()) && (this.mPopupView != null))
        {
            this.mIsShowing = false;
            unregisterForScrollChanged();
        }
        try
        {
            this.mWindowManager.removeViewImmediate(this.mPopupView);
            return;
        }
        finally
        {
            if ((this.mPopupView != this.mContentView) && ((this.mPopupView instanceof ViewGroup)))
                ((ViewGroup)this.mPopupView).removeView(this.mContentView);
            this.mPopupView = null;
            if (this.mOnDismissListener != null)
                this.mOnDismissListener.onDismiss();
        }
    }

    public int getAnimationStyle()
    {
        return this.mAnimationStyle;
    }

    public Drawable getBackground()
    {
        return this.mBackground;
    }

    public View getContentView()
    {
        return this.mContentView;
    }

    public int getHeight()
    {
        return this.mHeight;
    }

    public int getInputMethodMode()
    {
        return this.mInputMethodMode;
    }

    public int getMaxAvailableHeight(View paramView)
    {
        return getMaxAvailableHeight(paramView, 0);
    }

    public int getMaxAvailableHeight(View paramView, int paramInt)
    {
        return getMaxAvailableHeight(paramView, paramInt, false);
    }

    public int getMaxAvailableHeight(View paramView, int paramInt, boolean paramBoolean)
    {
        Rect localRect = new Rect();
        paramView.getWindowVisibleDisplayFrame(localRect);
        int[] arrayOfInt = this.mDrawingLocation;
        paramView.getLocationOnScreen(arrayOfInt);
        int i = localRect.bottom;
        if (paramBoolean)
            i = paramView.getContext().getResources().getDisplayMetrics().heightPixels;
        int j = Math.max(i - (arrayOfInt[1] + paramView.getHeight()) - paramInt, paramInt + (arrayOfInt[1] - localRect.top));
        if (this.mBackground != null)
        {
            this.mBackground.getPadding(this.mTempRect);
            j -= this.mTempRect.top + this.mTempRect.bottom;
        }
        return j;
    }

    public int getSoftInputMode()
    {
        return this.mSoftInputMode;
    }

    public int getWidth()
    {
        return this.mWidth;
    }

    public int getWindowLayoutType()
    {
        return this.mWindowLayoutType;
    }

    public boolean isAboveAnchor()
    {
        return this.mAboveAnchor;
    }

    public boolean isClippingEnabled()
    {
        return this.mClippingEnabled;
    }

    public boolean isFocusable()
    {
        return this.mFocusable;
    }

    public boolean isLayoutInScreenEnabled()
    {
        return this.mLayoutInScreen;
    }

    public boolean isOutsideTouchable()
    {
        return this.mOutsideTouchable;
    }

    public boolean isShowing()
    {
        return this.mIsShowing;
    }

    public boolean isSplitTouchEnabled()
    {
        int i = 1;
        if ((this.mSplitTouchEnabled < 0) && (this.mContext != null))
            if (this.mContext.getApplicationInfo().targetSdkVersion < 11);
        while (true)
        {
            return i;
            i = 0;
            continue;
            if (this.mSplitTouchEnabled != i)
                int j = 0;
        }
    }

    public boolean isTouchable()
    {
        return this.mTouchable;
    }

    void setAllowScrollingAnchorParent(boolean paramBoolean)
    {
        this.mAllowScrollingAnchorParent = paramBoolean;
    }

    public void setAnimationStyle(int paramInt)
    {
        this.mAnimationStyle = paramInt;
    }

    public void setBackgroundDrawable(Drawable paramDrawable)
    {
        this.mBackground = paramDrawable;
    }

    public void setClipToScreenEnabled(boolean paramBoolean)
    {
        this.mClipToScreen = paramBoolean;
        if (!paramBoolean);
        for (boolean bool = true; ; bool = false)
        {
            setClippingEnabled(bool);
            return;
        }
    }

    public void setClippingEnabled(boolean paramBoolean)
    {
        this.mClippingEnabled = paramBoolean;
    }

    public void setContentView(View paramView)
    {
        if (isShowing());
        while (true)
        {
            return;
            this.mContentView = paramView;
            if ((this.mContext == null) && (this.mContentView != null))
                this.mContext = this.mContentView.getContext();
            if ((this.mWindowManager == null) && (this.mContentView != null))
                this.mWindowManager = ((WindowManager)this.mContext.getSystemService("window"));
        }
    }

    public void setFocusable(boolean paramBoolean)
    {
        this.mFocusable = paramBoolean;
    }

    public void setHeight(int paramInt)
    {
        this.mHeight = paramInt;
    }

    public void setIgnoreCheekPress()
    {
        this.mIgnoreCheekPress = true;
    }

    public void setInputMethodMode(int paramInt)
    {
        this.mInputMethodMode = paramInt;
    }

    public void setLayoutInScreenEnabled(boolean paramBoolean)
    {
        this.mLayoutInScreen = paramBoolean;
    }

    public void setLayoutInsetDecor(boolean paramBoolean)
    {
        this.mLayoutInsetDecor = paramBoolean;
    }

    public void setOnDismissListener(OnDismissListener paramOnDismissListener)
    {
        this.mOnDismissListener = paramOnDismissListener;
    }

    public void setOutsideTouchable(boolean paramBoolean)
    {
        this.mOutsideTouchable = paramBoolean;
    }

    public void setSoftInputMode(int paramInt)
    {
        this.mSoftInputMode = paramInt;
    }

    public void setSplitTouchEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            this.mSplitTouchEnabled = i;
            return;
        }
    }

    public void setTouchInterceptor(View.OnTouchListener paramOnTouchListener)
    {
        this.mTouchInterceptor = paramOnTouchListener;
    }

    public void setTouchModal(boolean paramBoolean)
    {
        if (!paramBoolean);
        for (boolean bool = true; ; bool = false)
        {
            this.mNotTouchModal = bool;
            return;
        }
    }

    public void setTouchable(boolean paramBoolean)
    {
        this.mTouchable = paramBoolean;
    }

    public void setWidth(int paramInt)
    {
        this.mWidth = paramInt;
    }

    public void setWindowLayoutMode(int paramInt1, int paramInt2)
    {
        this.mWidthMode = paramInt1;
        this.mHeightMode = paramInt2;
    }

    public void setWindowLayoutType(int paramInt)
    {
        this.mWindowLayoutType = paramInt;
    }

    public void showAsDropDown(View paramView)
    {
        showAsDropDown(paramView, 0, 0);
    }

    public void showAsDropDown(View paramView, int paramInt1, int paramInt2)
    {
        if ((isShowing()) || (this.mContentView == null));
        while (true)
        {
            return;
            registerForScrollChanged(paramView, paramInt1, paramInt2);
            this.mIsShowing = true;
            this.mIsDropdown = true;
            WindowManager.LayoutParams localLayoutParams = createPopupLayout(paramView.getWindowToken());
            preparePopup(localLayoutParams);
            updateAboveAnchor(findDropDownPosition(paramView, localLayoutParams, paramInt1, paramInt2));
            if (this.mHeightMode < 0)
            {
                int j = this.mHeightMode;
                this.mLastHeight = j;
                localLayoutParams.height = j;
            }
            if (this.mWidthMode < 0)
            {
                int i = this.mWidthMode;
                this.mLastWidth = i;
                localLayoutParams.width = i;
            }
            localLayoutParams.windowAnimations = computeAnimationResource();
            invokePopup(localLayoutParams);
        }
    }

    public void showAtLocation(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3)
    {
        if ((isShowing()) || (this.mContentView == null));
        while (true)
        {
            return;
            unregisterForScrollChanged();
            this.mIsShowing = true;
            this.mIsDropdown = false;
            WindowManager.LayoutParams localLayoutParams = createPopupLayout(paramIBinder);
            localLayoutParams.windowAnimations = computeAnimationResource();
            preparePopup(localLayoutParams);
            if (paramInt1 == 0)
                paramInt1 = 51;
            localLayoutParams.gravity = paramInt1;
            localLayoutParams.x = paramInt2;
            localLayoutParams.y = paramInt3;
            if (this.mHeightMode < 0)
            {
                int j = this.mHeightMode;
                this.mLastHeight = j;
                localLayoutParams.height = j;
            }
            if (this.mWidthMode < 0)
            {
                int i = this.mWidthMode;
                this.mLastWidth = i;
                localLayoutParams.width = i;
            }
            invokePopup(localLayoutParams);
        }
    }

    public void showAtLocation(View paramView, int paramInt1, int paramInt2, int paramInt3)
    {
        showAtLocation(paramView.getWindowToken(), paramInt1, paramInt2, paramInt3);
    }

    public void update()
    {
        if ((!isShowing()) || (this.mContentView == null));
        while (true)
        {
            return;
            WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)this.mPopupView.getLayoutParams();
            int i = 0;
            int j = computeAnimationResource();
            if (j != localLayoutParams.windowAnimations)
            {
                localLayoutParams.windowAnimations = j;
                i = 1;
            }
            int k = computeFlags(localLayoutParams.flags);
            if (k != localLayoutParams.flags)
            {
                localLayoutParams.flags = k;
                i = 1;
            }
            if (i != 0)
                this.mWindowManager.updateViewLayout(this.mPopupView, localLayoutParams);
        }
    }

    public void update(int paramInt1, int paramInt2)
    {
        WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)this.mPopupView.getLayoutParams();
        update(localLayoutParams.x, localLayoutParams.y, paramInt1, paramInt2, false);
    }

    public void update(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        update(paramInt1, paramInt2, paramInt3, paramInt4, false);
    }

    public void update(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
    {
        if (paramInt3 != -1)
        {
            this.mLastWidth = paramInt3;
            setWidth(paramInt3);
        }
        if (paramInt4 != -1)
        {
            this.mLastHeight = paramInt4;
            setHeight(paramInt4);
        }
        if ((!isShowing()) || (this.mContentView == null))
            return;
        WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)this.mPopupView.getLayoutParams();
        boolean bool = paramBoolean;
        int i;
        if (this.mWidthMode < 0)
        {
            i = this.mWidthMode;
            label79: if ((paramInt3 != -1) && (localLayoutParams.width != i))
            {
                this.mLastWidth = i;
                localLayoutParams.width = i;
                bool = true;
            }
            if (this.mHeightMode >= 0)
                break label282;
        }
        label282: for (int j = this.mHeightMode; ; j = this.mLastHeight)
        {
            if ((paramInt4 != -1) && (localLayoutParams.height != j))
            {
                this.mLastHeight = j;
                localLayoutParams.height = j;
                bool = true;
            }
            if (localLayoutParams.x != paramInt1)
            {
                localLayoutParams.x = paramInt1;
                bool = true;
            }
            if (localLayoutParams.y != paramInt2)
            {
                localLayoutParams.y = paramInt2;
                bool = true;
            }
            int k = computeAnimationResource();
            if (k != localLayoutParams.windowAnimations)
            {
                localLayoutParams.windowAnimations = k;
                bool = true;
            }
            int m = computeFlags(localLayoutParams.flags);
            if (m != localLayoutParams.flags)
            {
                localLayoutParams.flags = m;
                bool = true;
            }
            if (!bool)
                break;
            this.mWindowManager.updateViewLayout(this.mPopupView, localLayoutParams);
            break;
            i = this.mLastWidth;
            break label79;
        }
    }

    public void update(View paramView, int paramInt1, int paramInt2)
    {
        update(paramView, false, 0, 0, true, paramInt1, paramInt2);
    }

    public void update(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        update(paramView, true, paramInt1, paramInt2, true, paramInt3, paramInt4);
    }

    private class PopupViewContainer extends FrameLayout
    {
        private static final String TAG = "PopupWindow.PopupViewContainer";

        public PopupViewContainer(Context arg2)
        {
            super();
        }

        public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
        {
            int i = 1;
            if (paramKeyEvent.getKeyCode() == 4)
                if (getKeyDispatcherState() == null)
                    i = super.dispatchKeyEvent(paramKeyEvent);
            while (true)
            {
                return i;
                if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getRepeatCount() == 0))
                {
                    KeyEvent.DispatcherState localDispatcherState2 = getKeyDispatcherState();
                    if (localDispatcherState2 != null)
                        localDispatcherState2.startTracking(paramKeyEvent, this);
                }
                else if (paramKeyEvent.getAction() == i)
                {
                    KeyEvent.DispatcherState localDispatcherState1 = getKeyDispatcherState();
                    if ((localDispatcherState1 != null) && (localDispatcherState1.isTracking(paramKeyEvent)) && (!paramKeyEvent.isCanceled()))
                        PopupWindow.this.dismiss();
                }
                else
                {
                    boolean bool = super.dispatchKeyEvent(paramKeyEvent);
                    continue;
                    bool = super.dispatchKeyEvent(paramKeyEvent);
                }
            }
        }

        public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
        {
            if ((PopupWindow.this.mTouchInterceptor != null) && (PopupWindow.this.mTouchInterceptor.onTouch(this, paramMotionEvent)));
            for (boolean bool = true; ; bool = super.dispatchTouchEvent(paramMotionEvent))
                return bool;
        }

        protected int[] onCreateDrawableState(int paramInt)
        {
            int[] arrayOfInt;
            if (PopupWindow.this.mAboveAnchor)
            {
                arrayOfInt = super.onCreateDrawableState(paramInt + 1);
                View.mergeDrawableStates(arrayOfInt, PopupWindow.ABOVE_ANCHOR_STATE_SET);
            }
            while (true)
            {
                return arrayOfInt;
                arrayOfInt = super.onCreateDrawableState(paramInt);
            }
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            boolean bool = true;
            int i = (int)paramMotionEvent.getX();
            int j = (int)paramMotionEvent.getY();
            if ((paramMotionEvent.getAction() == 0) && ((i < 0) || (i >= getWidth()) || (j < 0) || (j >= getHeight())))
                PopupWindow.this.dismiss();
            while (true)
            {
                return bool;
                if (paramMotionEvent.getAction() == 4)
                    PopupWindow.this.dismiss();
                else
                    bool = super.onTouchEvent(paramMotionEvent);
            }
        }

        public void sendAccessibilityEvent(int paramInt)
        {
            if (PopupWindow.this.mContentView != null)
                PopupWindow.this.mContentView.sendAccessibilityEvent(paramInt);
            while (true)
            {
                return;
                super.sendAccessibilityEvent(paramInt);
            }
        }
    }

    public static abstract interface OnDismissListener
    {
        public abstract void onDismiss();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.PopupWindow
 * JD-Core Version:        0.6.2
 */