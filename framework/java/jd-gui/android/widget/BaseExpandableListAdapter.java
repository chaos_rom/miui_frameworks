package android.widget;

import android.database.DataSetObservable;
import android.database.DataSetObserver;

public abstract class BaseExpandableListAdapter
    implements ExpandableListAdapter, HeterogeneousExpandableList
{
    private final DataSetObservable mDataSetObservable = new DataSetObservable();

    public boolean areAllItemsEnabled()
    {
        return true;
    }

    public int getChildType(int paramInt1, int paramInt2)
    {
        return 0;
    }

    public int getChildTypeCount()
    {
        return 1;
    }

    public long getCombinedChildId(long paramLong1, long paramLong2)
    {
        return 0x0 | (0x7FFFFFFF & paramLong1) << 32 | 0xFFFFFFFF & paramLong2;
    }

    public long getCombinedGroupId(long paramLong)
    {
        return (0x7FFFFFFF & paramLong) << 32;
    }

    public int getGroupType(int paramInt)
    {
        return 0;
    }

    public int getGroupTypeCount()
    {
        return 1;
    }

    public boolean isEmpty()
    {
        if (getGroupCount() == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void notifyDataSetChanged()
    {
        this.mDataSetObservable.notifyChanged();
    }

    public void notifyDataSetInvalidated()
    {
        this.mDataSetObservable.notifyInvalidated();
    }

    public void onGroupCollapsed(int paramInt)
    {
    }

    public void onGroupExpanded(int paramInt)
    {
    }

    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        this.mDataSetObservable.registerObserver(paramDataSetObserver);
    }

    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        this.mDataSetObservable.unregisterObserver(paramDataSetObserver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.BaseExpandableListAdapter
 * JD-Core Version:        0.6.2
 */