package android.widget;

import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.text.method.WordIterator;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.ViewTreeObserver.OnTouchModeChangeListener;
import android.view.WindowManager;
import java.util.ArrayList;

public abstract class MiuiCursorController
    implements ViewTreeObserver.OnTouchModeChangeListener
{
    public static final int INSERT_CURSOR_TYPE = 1;
    public static final String LOG_TAG = "MiuiCursorController";
    public static final int MAX_CLIP_DATA_NUMBER = 3;
    public static final int SELECT_CURSOR_TYPE = 2;
    protected Context mContext;
    protected Editor mOwner;
    protected TextView mTextView;

    MiuiCursorController(Editor paramEditor, Context paramContext)
    {
        this.mOwner = paramEditor;
        this.mContext = paramContext;
        this.mTextView = this.mOwner.textview();
    }

    static MiuiCursorController create(Editor paramEditor, Context paramContext, int paramInt)
    {
        Object localObject;
        if (paramInt == 1)
            localObject = new InsertionPointCursorController(paramEditor, paramContext);
        while (true)
        {
            return localObject;
            if (paramInt == 2)
                localObject = new SelectionModifierCursorController(paramEditor, paramContext);
            else
                localObject = null;
        }
    }

    private static int extractRangeEndFromLong(long paramLong)
    {
        return (int)(0xFFFFFFFF & paramLong);
    }

    private static int extractRangeStartFromLong(long paramLong)
    {
        return (int)(paramLong >>> 32);
    }

    public abstract void computePanelPosition(int[] paramArrayOfInt);

    public abstract int computePanelPositionOnBottom();

    public FloatPanelViewController createFloatPanelViewController(int paramInt)
    {
        return new FloatPanelViewController(this, paramInt);
    }

    public int getMaxTouchOffset()
    {
        return 0;
    }

    public int getMinTouchOffset()
    {
        return -1;
    }

    public abstract void hide();

    MiuiHandleView initHandleView(Editor paramEditor, int paramInt, MiuiCursorController paramMiuiCursorController)
    {
        Object localObject = null;
        if (paramInt == 0)
            if (paramEditor.hasMagnifierController())
            {
                localObject = new MagnifierInsertionHandleView(paramEditor, this.mOwner.getSelectHandleCenterRes());
                if (localObject != null)
                    break label130;
                Log.e("MiuiCursorController", "Failed to init handle view.");
            }
        while (true)
        {
            return localObject;
            localObject = new InsertionHandleView(paramEditor, this.mOwner.getSelectHandleCenterRes());
            break;
            Drawable localDrawable1 = this.mOwner.getSelectHandleRightRes();
            Drawable localDrawable2 = this.mOwner.getSelectHandleLeftRes();
            if (paramInt == 1)
                localObject = new SelectionStartHandleView(paramEditor, localDrawable2, localDrawable1);
            if (paramInt != 2)
                break;
            localObject = new SelectionEndHandleView(paramEditor, localDrawable1, localDrawable2);
            break;
            label130: ((MiuiHandleView)localObject).setCursorController(paramMiuiCursorController);
            ((MiuiHandleView)localObject).setType(paramInt);
        }
    }

    public boolean isSelectionStartDragged()
    {
        return false;
    }

    public abstract boolean isShowing();

    public void onDetached()
    {
    }

    public abstract boolean onHandleTouchEvent(MiuiHandleView paramMiuiHandleView, MotionEvent paramMotionEvent);

    public abstract boolean onTouchEvent(MotionEvent paramMotionEvent);

    public void onTouchModeChanged(boolean paramBoolean)
    {
        if (!paramBoolean)
            hide();
    }

    public void resetTouchOffsets()
    {
    }

    public boolean selectCurrentRange()
    {
        boolean bool;
        if ((this.mTextView.getTransformationMethod() instanceof PasswordTransformationMethod))
            bool = this.mOwner.selectAllWrap();
        while (true)
        {
            return bool;
            int i = this.mTextView.getInputType();
            int j = i & 0xF;
            int k = i & 0xFF0;
            if ((j == 2) || (j == 3) || (j == 4) || (k == 16) || (k == 32) || (k == 208) || (k == 176))
            {
                bool = this.mOwner.selectAllWrap();
            }
            else
            {
                MiuiCursorController localMiuiCursorController = this.mOwner.getSelectionController();
                int m = localMiuiCursorController.getMinTouchOffset();
                int n = localMiuiCursorController.getMaxTouchOffset();
                if ((m < 0) || (m > this.mTextView.length()))
                {
                    bool = false;
                }
                else if ((n < 0) || (n > this.mTextView.length()))
                {
                    bool = false;
                }
                else
                {
                    CharSequence localCharSequence = this.mTextView.getText();
                    URLSpan[] arrayOfURLSpan = (URLSpan[])((Spanned)localCharSequence).getSpans(m, n, URLSpan.class);
                    int i1;
                    int i2;
                    if (arrayOfURLSpan.length >= 1)
                    {
                        URLSpan localURLSpan = arrayOfURLSpan[0];
                        i1 = ((Spanned)localCharSequence).getSpanStart(localURLSpan);
                        i2 = ((Spanned)localCharSequence).getSpanEnd(localURLSpan);
                    }
                    label371: label382: 
                    while (true)
                    {
                        Selection.setSelection((Spannable)localCharSequence, i1, i2);
                        if (i2 <= i1)
                            break label384;
                        bool = true;
                        break;
                        WordIterator localWordIterator = this.mOwner.getWordIterator();
                        localWordIterator.setCharSequence(localCharSequence, m, n);
                        i1 = localWordIterator.getBeginning(m);
                        i2 = localWordIterator.getEnd(n);
                        if ((i1 == -1) || (i2 == -1))
                        {
                            if (n != localCharSequence.length())
                                break label371;
                            i1 = n - 1;
                        }
                        for (i2 = n; ; i2 = n + 1)
                        {
                            if (i1 != i2)
                                break label382;
                            long l = this.mOwner.getCharRangeWrap(i1);
                            i1 = extractRangeStartFromLong(l);
                            i2 = extractRangeEndFromLong(l);
                            break;
                            i1 = n;
                        }
                    }
                    label384: bool = false;
                }
            }
        }
    }

    public void setMinMaxOffset(int paramInt)
    {
    }

    public abstract void show();

    public abstract void updatePosition();

    class SelectionEndHandleView extends MiuiCursorController.MiuiHandleView
    {
        public SelectionEndHandleView(Editor paramDrawable1, Drawable paramDrawable2, Drawable arg4)
        {
            super(paramDrawable1, paramDrawable2, localDrawable);
        }

        public int getCurrentCursorOffset()
        {
            return this.mTextView.getSelectionEnd();
        }

        protected int getHotspotX(Drawable paramDrawable, boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = 3 * paramDrawable.getIntrinsicWidth() / 4; ; i = paramDrawable.getIntrinsicWidth() / 4)
                return i;
        }

        public void updatePosition(float paramFloat1, float paramFloat2)
        {
            int i = this.mTextView.getOffsetForPosition(paramFloat1, paramFloat2);
            int j = this.mTextView.getSelectionStart();
            if (i <= j)
                i = Math.min(j + 1, this.mTextView.length());
            positionAtCursorOffset(i, false);
        }

        public void updateSelection(int paramInt)
        {
            this.mEditor.setTextSelectionWrap(this.mTextView.getSelectionStart(), paramInt);
            updateDrawable();
        }
    }

    class SelectionStartHandleView extends MiuiCursorController.MiuiHandleView
    {
        public SelectionStartHandleView(Editor paramDrawable1, Drawable paramDrawable2, Drawable arg4)
        {
            super(paramDrawable1, paramDrawable2, localDrawable);
        }

        public int getCurrentCursorOffset()
        {
            return this.mTextView.getSelectionStart();
        }

        protected int getHotspotX(Drawable paramDrawable, boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = paramDrawable.getIntrinsicWidth() / 4; ; i = 3 * paramDrawable.getIntrinsicWidth() / 4)
                return i;
        }

        public void updatePosition(float paramFloat1, float paramFloat2)
        {
            int i = this.mTextView.getOffsetForPosition(paramFloat1, paramFloat2);
            int j = this.mTextView.getSelectionEnd();
            if (i >= j)
                i = Math.max(0, j - 1);
            positionAtCursorOffset(i, false);
        }

        public void updateSelection(int paramInt)
        {
            this.mEditor.setTextSelectionWrap(paramInt, this.mTextView.getSelectionEnd());
            updateDrawable();
        }
    }

    class MagnifierInsertionHandleView extends MiuiCursorController.InsertionHandleView
    {
        boolean canHide = false;

        public MagnifierInsertionHandleView(Editor paramDrawable, Drawable arg3)
        {
            super(paramDrawable, localDrawable);
        }

        public void hide()
        {
            if (this.canHide)
                this.mEditor.removePositionListenerSubscriber(this);
            while (true)
            {
                return;
                Log.i("MiuiHandleView", "To hide the MagnifierInsertionHandleView, just delay");
                this.canHide = true;
                dismiss();
                hideAfterDelay(200);
            }
        }

        protected boolean needShowHandle()
        {
            return false;
        }

        public void show()
        {
            super.show();
            hideAfterDelay(3000);
            updatePositionXY(getCurrentCursorOffset(), true);
        }
    }

    class InsertionHandleView extends MiuiCursorController.MiuiHandleView
    {
        protected static final int DELAY_BEFORE_HANDLE_FADES_OUT = 3000;
        protected static final int DELAY_WHEN_HANDLE_INVISIBLE = 200;
        private Runnable mHider;

        public InsertionHandleView(Editor paramDrawable, Drawable arg3)
        {
            super(paramDrawable, localDrawable, localDrawable);
        }

        private void removeHiderCallback()
        {
            if (this.mHider != null)
                this.mTextView.removeCallbacks(this.mHider);
        }

        public int getCurrentCursorOffset()
        {
            return this.mTextView.getSelectionStart();
        }

        protected int getHotspotX(Drawable paramDrawable, boolean paramBoolean)
        {
            return paramDrawable.getIntrinsicWidth() / 2;
        }

        protected void hideAfterDelay(int paramInt)
        {
            removeHiderCallback();
            if (this.mHider == null)
                this.mHider = new Runnable()
                {
                    public void run()
                    {
                        MiuiCursorController.InsertionHandleView.this.hide();
                    }
                };
            this.mTextView.postDelayed(this.mHider, paramInt);
        }

        public void onDetached()
        {
            super.onDetached();
            removeHiderCallback();
        }

        void onHandleMoved()
        {
            super.onHandleMoved();
            removeHiderCallback();
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            boolean bool = super.onTouchEvent(paramMotionEvent);
            switch (paramMotionEvent.getActionMasked())
            {
            case 0:
            case 1:
            case 2:
            default:
            case 3:
            }
            while (true)
            {
                return bool;
                hideAfterDelay(3000);
            }
        }

        public void show()
        {
            super.show();
            hideAfterDelay(3000);
        }

        public void updatePosition(float paramFloat1, float paramFloat2)
        {
            positionAtCursorOffset(this.mTextView.getOffsetForPosition(paramFloat1, paramFloat2), false);
        }

        public void updateSelection(int paramInt)
        {
            this.mEditor.setTextSelectionWrap(paramInt, paramInt);
        }
    }

    abstract class MiuiHandleView extends View
        implements Editor.TextViewPositionListener
    {
        public static final int HANDLE_END = 2;
        public static final int HANDLE_INSERT = 0;
        public static final int HANDLE_START = 1;
        private static final int HISTORY_SIZE = 5;
        static final String TAG = "MiuiHandleView";
        private static final int TOUCH_UP_FILTER_DELAY_AFTER = 150;
        private static final int TOUCH_UP_FILTER_DELAY_BEFORE = 350;
        private final PopupWindow mContainer;
        private int mContainerX;
        private int mContainerY;
        private MiuiCursorController mCursorController;
        protected Drawable mDrawable;
        protected Drawable mDrawableLtr;
        protected Drawable mDrawableRtl;
        protected Editor mEditor;
        private int mHandleRange;
        private int mHotspotX;
        private float mIdealVerticalOffset;
        private boolean mIsDragging;
        private int mLastParentX;
        private int mLastParentY;
        private int mNumberPreviousOffsets = 0;
        private boolean mPositionHasChanged = true;
        private int mPositionX;
        private int mPositionY;
        private int mPreviousOffset = -1;
        private int mPreviousOffsetIndex = 0;
        private final int[] mPreviousOffsets = new int[5];
        private final long[] mPreviousOffsetsTimes = new long[5];
        protected TextView mTextView;
        private float mTouchOffsetY;
        private float mTouchToWindowOffsetX;
        private float mTouchToWindowOffsetY;
        private int mType;

        public MiuiHandleView(Editor paramDrawable1, Drawable paramDrawable2, Drawable arg4)
        {
            super();
            this.mEditor = paramDrawable1;
            this.mTextView = this.mEditor.textview();
            this.mHandleRange = ((int)this.mTextView.getContext().getResources().getDimension(101318681) / 2);
            this.mContainer = new PopupWindow(this.mTextView.getContext(), null, this.mEditor.getSelectHandleWindowStyle());
            this.mContainer.setSplitTouchEnabled(true);
            this.mContainer.setClippingEnabled(false);
            this.mContainer.setWindowLayoutType(1002);
            this.mContainer.setContentView(this);
            this.mDrawableLtr = paramDrawable2;
            Object localObject;
            this.mDrawableRtl = localObject;
            updateDrawable();
            int i = this.mDrawable.getIntrinsicHeight();
            this.mTouchOffsetY = (-0.3F * i);
            this.mIdealVerticalOffset = (0.7F * i);
        }

        private void addPositionToTouchUpFilter(int paramInt)
        {
            this.mPreviousOffsetIndex = ((1 + this.mPreviousOffsetIndex) % 5);
            this.mPreviousOffsets[this.mPreviousOffsetIndex] = paramInt;
            this.mPreviousOffsetsTimes[this.mPreviousOffsetIndex] = SystemClock.uptimeMillis();
            this.mNumberPreviousOffsets = (1 + this.mNumberPreviousOffsets);
        }

        private void filterOnTouchUp()
        {
            long l = SystemClock.uptimeMillis();
            int i = 0;
            int j = this.mPreviousOffsetIndex;
            int k = Math.min(this.mNumberPreviousOffsets, 5);
            while ((i < k) && (l - this.mPreviousOffsetsTimes[j] < 150L))
            {
                i++;
                j = (5 + (this.mPreviousOffsetIndex - i)) % 5;
            }
            if ((i > 0) && (i < k) && (l - this.mPreviousOffsetsTimes[j] > 350L))
                positionAtCursorOffset(this.mPreviousOffsets[j], false);
        }

        private void startTouchUpFilter(int paramInt)
        {
            this.mNumberPreviousOffsets = 0;
            addPositionToTouchUpFilter(paramInt);
        }

        protected void dismiss()
        {
            this.mIsDragging = false;
            this.mContainer.dismiss();
            onDetached();
        }

        public float distance(float paramFloat1, float paramFloat2)
        {
            return Math.abs((this.mContainerX + this.mHotspotX - paramFloat1) * (this.mContainerY - paramFloat2));
        }

        protected abstract int getCurrentCursorOffset();

        public void getHotspotLocationOnScreen(int[] paramArrayOfInt)
        {
            paramArrayOfInt[0] = (this.mContainerX + this.mHotspotX);
            paramArrayOfInt[1] = this.mContainerY;
        }

        protected abstract int getHotspotX(Drawable paramDrawable, boolean paramBoolean);

        public void hide()
        {
            dismiss();
            this.mEditor.removePositionListenerSubscriber(this);
        }

        public boolean inRecRange(float paramFloat1, float paramFloat2)
        {
            int i = this.mDrawableRtl.getIntrinsicHeight();
            int j = this.mContainerX + this.mHotspotX;
            int k = this.mContainerY + i / 2;
            Log.i("MiuiCursorController", "HandleCenter at x= " + j + " y=" + k + "    and touch at x=" + paramFloat1 + " y=" + paramFloat2);
            if ((Math.abs(j - paramFloat1) < this.mHandleRange) && (Math.abs(k - paramFloat2) < i / 2));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void invisible()
        {
            this.mContainer.dismiss();
        }

        public boolean isDragging()
        {
            return this.mIsDragging;
        }

        public boolean isShowing()
        {
            return this.mContainer.isShowing();
        }

        protected boolean isVisible()
        {
            boolean bool;
            if (this.mIsDragging)
                bool = true;
            while (true)
            {
                return bool;
                if (this.mTextView.isInBatchEditMode())
                    bool = false;
                else
                    bool = this.mEditor.isPositionVisibleWrap(this.mPositionX + this.mHotspotX, this.mPositionY);
            }
        }

        protected boolean needShowHandle()
        {
            return true;
        }

        public void onDetached()
        {
        }

        protected void onDraw(Canvas paramCanvas)
        {
            this.mDrawable.setBounds(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
            this.mDrawable.draw(paramCanvas);
        }

        void onHandleMoved()
        {
        }

        protected void onMeasure(int paramInt1, int paramInt2)
        {
            setMeasuredDimension(this.mDrawable.getIntrinsicWidth(), this.mDrawable.getIntrinsicHeight());
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            default:
            case 0:
            case 2:
            case 1:
            case 3:
            }
            while (true)
            {
                this.mCursorController.onHandleTouchEvent(this, paramMotionEvent);
                return true;
                startTouchUpFilter(getCurrentCursorOffset());
                this.mTouchToWindowOffsetX = (paramMotionEvent.getRawX() - this.mPositionX);
                this.mTouchToWindowOffsetY = (paramMotionEvent.getRawY() - this.mPositionY);
                long l = this.mEditor.getPositionListenerPostion();
                this.mLastParentX = MiuiCursorController.extractRangeStartFromLong(l);
                this.mLastParentY = MiuiCursorController.extractRangeEndFromLong(l);
                this.mIsDragging = true;
                continue;
                float f1 = paramMotionEvent.getRawX();
                float f2 = paramMotionEvent.getRawY();
                float f3 = this.mTouchToWindowOffsetY - this.mLastParentY;
                float f4 = f2 - this.mPositionY - this.mLastParentY;
                if (f3 < this.mIdealVerticalOffset);
                for (float f5 = Math.max(Math.min(f4, this.mIdealVerticalOffset), f3); ; f5 = Math.min(Math.max(f4, this.mIdealVerticalOffset), f3))
                {
                    this.mTouchToWindowOffsetY = (f5 + this.mLastParentY);
                    updatePosition(f1 - this.mTouchToWindowOffsetX + this.mHotspotX, f2 - this.mTouchToWindowOffsetY + this.mTouchOffsetY);
                    break;
                }
                filterOnTouchUp();
                this.mIsDragging = false;
                continue;
                this.mIsDragging = false;
            }
        }

        public void positionAtCursorOffset(int paramInt, boolean paramBoolean)
        {
            if (this.mTextView.getLayout() == null)
                this.mEditor.prepareCursorControllers();
            while (true)
            {
                return;
                if ((paramInt != this.mPreviousOffset) || (paramBoolean))
                {
                    updateSelection(paramInt);
                    addPositionToTouchUpFilter(paramInt);
                    updatePositionXY(paramInt, false);
                    this.mPreviousOffset = paramInt;
                    this.mPositionHasChanged = true;
                }
            }
        }

        public void setCursorController(MiuiCursorController paramMiuiCursorController)
        {
            this.mCursorController = paramMiuiCursorController;
        }

        public void setType(int paramInt)
        {
            this.mType = paramInt;
        }

        public void show()
        {
            Log.i("MiuiHandleView", "HandleView [" + this.mType + "] is shown");
            if (isShowing());
            while (true)
            {
                return;
                this.mEditor.addPositionListenerSubscriber(this, true);
                this.mPreviousOffset = -1;
                positionAtCursorOffset(getCurrentCursorOffset(), false);
            }
        }

        protected void updateDrawable()
        {
            int i = getCurrentCursorOffset();
            boolean bool = this.mTextView.getLayout().isRtlCharAt(i);
            if (bool);
            for (Drawable localDrawable = this.mDrawableRtl; ; localDrawable = this.mDrawableLtr)
            {
                this.mDrawable = localDrawable;
                this.mHotspotX = getHotspotX(this.mDrawable, bool);
                return;
            }
        }

        protected abstract void updatePosition(float paramFloat1, float paramFloat2);

        public void updatePosition(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
        {
            Log.i("MiuiHandleView", "HandleView [" + this.mType + "] handle updatePosition called from PositionListener:" + paramInt1 + "x" + paramInt2);
            positionAtCursorOffset(getCurrentCursorOffset(), paramBoolean2);
            if ((paramBoolean1) || (this.mPositionHasChanged))
            {
                if (this.mIsDragging)
                {
                    if ((paramInt1 != this.mLastParentX) || (paramInt2 != this.mLastParentY))
                    {
                        this.mTouchToWindowOffsetX += paramInt1 - this.mLastParentX;
                        this.mTouchToWindowOffsetY += paramInt2 - this.mLastParentY;
                        this.mLastParentX = paramInt1;
                        this.mLastParentY = paramInt2;
                        if (this.mEditor.hasMagnifierController())
                            this.mEditor.getMagnifierController().onParentChanged();
                    }
                    onHandleMoved();
                }
                if (!isVisible())
                    break label255;
                this.mContainerX = (paramInt1 + this.mPositionX);
                this.mContainerY = (paramInt2 + this.mPositionY);
                if (needShowHandle())
                {
                    if (!isShowing())
                        break label232;
                    this.mContainer.update(this.mContainerX, this.mContainerY, -1, -1);
                }
            }
            while (true)
            {
                this.mPositionHasChanged = false;
                this.mCursorController.updatePosition();
                return;
                label232: this.mContainer.showAtLocation(this.mTextView, 0, this.mContainerX, this.mContainerY);
                continue;
                label255: if (isShowing())
                    dismiss();
            }
        }

        protected void updatePositionXY(int paramInt, boolean paramBoolean)
        {
            int i = this.mTextView.getLayout().getLineForOffset(paramInt);
            this.mPositionX = ((int)(this.mTextView.getLayout().getPrimaryHorizontal(paramInt) - 0.5F - this.mHotspotX));
            this.mPositionY = this.mTextView.getLayout().getLineBottom(i);
            this.mPositionX += this.mTextView.viewportToContentHorizontalOffset();
            this.mPositionY += this.mTextView.viewportToContentVerticalOffset();
            if (paramBoolean)
            {
                int[] arrayOfInt = new int[2];
                this.mTextView.getLocationOnScreen(arrayOfInt);
                this.mContainerX = (arrayOfInt[0] + this.mPositionX);
                this.mContainerY = (arrayOfInt[1] + this.mPositionY);
            }
        }

        protected abstract void updateSelection(int paramInt);
    }

    public static class SelectionModifierCursorController extends MiuiCursorController
    {
        private int[] mCoords = new int[2];
        private int[] mCoords1 = new int[2];
        private int[] mCoords2 = new int[2];
        private MiuiCursorController.MiuiHandleView mEndHandle = initHandleView(this.mOwner, 2, this);
        boolean mHideInvisiblePanel = false;
        private boolean mIsShowing;
        private int mMaxTouchOffset;
        private int mMinTouchOffset;
        private MiuiCursorController.FloatPanelViewController mPanel = createFloatPanelViewController(100859954);
        private float mPreviousTapPositionX;
        private float mPreviousTapPositionY;
        private long mPreviousTapUpTime = 0L;
        private MiuiCursorController.MiuiHandleView mStartHandle = initHandleView(this.mOwner, 1, this);
        private MiuiCursorController.MiuiHandleView mTouchOnHandle;

        SelectionModifierCursorController(Editor paramEditor, Context paramContext)
        {
            super(paramContext);
            View.OnClickListener local1 = new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    MiuiCursorController.SelectionModifierCursorController.this.mOwner.handleFloatPanelClick(paramAnonymousView, MiuiCursorController.SelectionModifierCursorController.this);
                    MiuiCursorController.SelectionModifierCursorController.this.mPanel.hide();
                }
            };
            this.mPanel.setButtonOnClickListener(101384213, local1);
            this.mPanel.setButtonOnClickListener(101384214, local1);
            this.mPanel.setButtonOnClickListener(101384215, local1);
            resetTouchOffsets();
        }

        public void addClipData(ClipboardManager paramClipboardManager, ClipData paramClipData, CharSequence paramCharSequence)
        {
            if (paramClipData == null)
                paramClipboardManager.setPrimaryClip(ClipData.newPlainText(null, paramCharSequence));
            while (true)
            {
                return;
                ClipData localClipData = ClipData.newPlainText(null, paramCharSequence);
                int i = Math.min(paramClipData.getItemCount(), 2);
                for (int j = 0; j < i; j++)
                    localClipData.addItem(paramClipData.getItemAt(j));
                paramClipboardManager.setPrimaryClip(localClipData);
            }
        }

        public void computePanelPosition(int[] paramArrayOfInt)
        {
            if (this.mStartHandle.isShowing())
            {
                this.mStartHandle.getHotspotLocationOnScreen(this.mCoords1);
                this.mEndHandle.getHotspotLocationOnScreen(this.mCoords2);
            }
            paramArrayOfInt[1] = this.mCoords1[1];
            if (this.mCoords1[1] == this.mCoords2[1])
                paramArrayOfInt[0] = ((this.mCoords1[0] + this.mCoords2[0]) / 2);
            while (true)
            {
                return;
                this.mTextView.getLocationOnScreen(this.mCoords);
                paramArrayOfInt[0] = Math.max(this.mCoords[0] + this.mTextView.getWidth() / 2, this.mCoords1[0]);
            }
        }

        public int computePanelPositionOnBottom()
        {
            if (this.mStartHandle.isShowing())
            {
                this.mEndHandle.getHotspotLocationOnScreen(this.mCoords2);
                this.mTextView.getLocationOnScreen(this.mCoords);
            }
            int i = this.mCoords[1] + this.mTextView.getHeight();
            int j = this.mContext.getResources().getDimensionPixelSize(101318660);
            int k = j + this.mCoords2[1];
            if (this.mEndHandle.isShowing())
                i = Math.min(k, i + j);
            return i;
        }

        public int getMaxTouchOffset()
        {
            return this.mMaxTouchOffset;
        }

        public int getMinTouchOffset()
        {
            return this.mMinTouchOffset;
        }

        public void hide()
        {
            this.mStartHandle.hide();
            this.mEndHandle.hide();
            this.mPanel.hide();
            this.mIsShowing = false;
        }

        public boolean isSelectionStartDragged()
        {
            if ((this.mStartHandle != null) && (this.mStartHandle.isDragging()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isShowing()
        {
            return this.mIsShowing;
        }

        public boolean onHandleTouchEvent(MiuiCursorController.MiuiHandleView paramMiuiHandleView, MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return false;
                this.mPanel.showAndUpdatePositionAsync();
                continue;
                if (this.mPanel.isShowing())
                    this.mPanel.hide();
            }
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            boolean bool1 = false;
            int i = paramMotionEvent.getActionMasked();
            boolean bool2;
            if (this.mTouchOnHandle != null)
            {
                this.mTouchOnHandle.onTouchEvent(paramMotionEvent);
                if ((i == 1) || (i == 3))
                {
                    this.mTouchOnHandle = null;
                    if (this.mTextView.getParent() != null)
                        this.mTextView.getParent().requestDisallowInterceptTouchEvent(false);
                    Log.i("MiuiCursorController", "action UP or Cancel to stop moving event to HandleView");
                }
                bool2 = true;
                return bool2;
            }
            if ((isShowing()) && (i == 0))
            {
                float f3 = paramMotionEvent.getRawX();
                float f4 = paramMotionEvent.getRawY();
                boolean bool3 = this.mStartHandle.inRecRange(f3, f4);
                boolean bool4 = this.mEndHandle.inRecRange(f3, f4);
                Log.i("MiuiCursorController", "Touch in handleview: startHandleView=" + bool3 + " endHandleView=" + bool4);
                MiuiCursorController.MiuiHandleView localMiuiHandleView;
                if ((bool3) && (bool4))
                    if (this.mStartHandle.distance(f3, f4) > this.mEndHandle.distance(f3, f4))
                    {
                        localMiuiHandleView = this.mEndHandle;
                        label202: this.mTouchOnHandle = localMiuiHandleView;
                    }
                while (true)
                {
                    if (this.mTouchOnHandle == null)
                        break label302;
                    Log.i("MiuiCursorController", "Touch near handle and move event to HandleView");
                    if (this.mTextView.getParent() != null)
                        this.mTextView.getParent().requestDisallowInterceptTouchEvent(true);
                    this.mTouchOnHandle.onTouchEvent(paramMotionEvent);
                    bool2 = true;
                    break;
                    localMiuiHandleView = this.mStartHandle;
                    break label202;
                    if (bool3)
                        this.mTouchOnHandle = this.mStartHandle;
                    else if (bool4)
                        this.mTouchOnHandle = this.mEndHandle;
                }
            }
            label302: switch (i)
            {
            case 2:
            case 3:
            case 4:
            default:
            case 0:
            case 5:
            case 6:
            case 1:
            }
            while (true)
            {
                if (this.mOwner.hasMagnifierController())
                    bool1 = this.mOwner.getMagnifierController().onTouchEvent(paramMotionEvent);
                if (this.mOwner.hasInsertionController())
                    this.mOwner.getInsertionController().onTouchEvent(paramMotionEvent);
                bool2 = bool1;
                break;
                float f1 = paramMotionEvent.getX();
                float f2 = paramMotionEvent.getY();
                int n = this.mTextView.getOffsetForPosition(f1, f2);
                this.mMaxTouchOffset = n;
                this.mMinTouchOffset = n;
                this.mOwner.startTextSelectionModeIfDouleTap(this.mPreviousTapUpTime, f1, f2, this.mPreviousTapPositionX, this.mPreviousTapPositionY);
                this.mPreviousTapPositionX = f1;
                this.mPreviousTapPositionY = f2;
                continue;
                if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch.distinct"))
                {
                    int j = paramMotionEvent.getPointerCount();
                    for (int k = 0; k < j; k++)
                    {
                        int m = this.mTextView.getOffsetForPosition(paramMotionEvent.getX(k), paramMotionEvent.getY(k));
                        if (m < this.mMinTouchOffset)
                            this.mMinTouchOffset = m;
                        if (m > this.mMaxTouchOffset)
                            this.mMaxTouchOffset = m;
                    }
                    this.mOwner.onTapUpEvent();
                    this.mPreviousTapUpTime = SystemClock.uptimeMillis();
                }
            }
        }

        public void resetTouchOffsets()
        {
            this.mMaxTouchOffset = -1;
            this.mMinTouchOffset = -1;
        }

        public void setMinMaxOffset(int paramInt)
        {
            this.mMaxTouchOffset = paramInt;
            this.mMinTouchOffset = paramInt;
        }

        public void show()
        {
            Log.i("MiuiCursorController", "SelectionModifierCursorController is shown");
            this.mTouchOnHandle = null;
            if (this.mTextView.isInBatchEditMode());
            while (true)
            {
                return;
                Log.i("MiuiCursorController", "SelectionModifierCursorController is shown and hide InsertionPointCursorController");
                this.mIsShowing = true;
                this.mStartHandle.show();
                this.mEndHandle.show();
                updatePosition();
                this.mPanel.showAndUpdatePositionAsync();
                this.mOwner.hideInsertionPointCursorControllerWrap();
                int[] arrayOfInt1 = new int[3];
                arrayOfInt1[0] = 101384213;
                arrayOfInt1[1] = 101384214;
                arrayOfInt1[2] = 101384215;
                int[] arrayOfInt2 = new int[2];
                arrayOfInt2[0] = 101384220;
                arrayOfInt2[1] = 101384221;
                ArrayList localArrayList = this.mOwner.getFloatPanelShowHides(arrayOfInt1);
                this.mPanel.showHideButtons(arrayOfInt1, arrayOfInt2, localArrayList);
            }
        }

        public void updatePosition()
        {
            if (!isShowing());
            while (true)
            {
                return;
                int i = this.mTextView.getSelectionStart();
                int j = this.mTextView.getSelectionEnd();
                if ((i < 0) || (j < 0))
                {
                    Log.e("MiuiCursorController", "Update selection controller position called with no cursor");
                    hide();
                }
                else if (i == j)
                {
                    hide();
                }
                else
                {
                    this.mStartHandle.positionAtCursorOffset(i, true);
                    this.mEndHandle.positionAtCursorOffset(j, true);
                    if ((this.mStartHandle.isShowing()) || (this.mEndHandle.isShowing()))
                    {
                        if ((this.mPanel.isShowing()) || (this.mHideInvisiblePanel))
                        {
                            this.mPanel.showAndUpdatePositionAsync();
                            this.mHideInvisiblePanel = false;
                        }
                    }
                    else if (this.mPanel.isShowing())
                    {
                        this.mPanel.hide();
                        this.mHideInvisiblePanel = true;
                    }
                }
            }
        }
    }

    public static class InsertionPointCursorController extends MiuiCursorController
    {
        private MiuiCursorController.FloatPanelViewController mClipboardPanel = createFloatPanelViewController(100859952);
        private int[] mCoords = new int[2];
        private final MiuiCursorController.MiuiHandleView mHandle = initHandleView(this.mOwner, 0, this);
        private MiuiCursorController.FloatPanelViewController mPanel = createFloatPanelViewController(100859955);
        private long mPreviousDownTime;
        boolean mShowOnOneShot = false;
        private int mTextOffset;

        InsertionPointCursorController(Editor paramEditor, Context paramContext)
        {
            super(paramContext);
            View.OnClickListener local1 = new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    MiuiCursorController.InsertionPointCursorController.this.mOwner.handleFloatPanelClick(paramAnonymousView, MiuiCursorController.InsertionPointCursorController.this);
                    MiuiCursorController.InsertionPointCursorController.this.mPanel.hide();
                }
            };
            this.mPanel.setButtonOnClickListener(101384217, local1);
            this.mPanel.setButtonOnClickListener(101384218, local1);
            this.mPanel.setButtonOnClickListener(101384215, local1);
            this.mPanel.setButtonOnClickListener(101384216, local1);
        }

        private void showInsertionPanel()
        {
            Log.i("MiuiCursorController", ">need InsertPanel, to show Panel and hide handle");
            this.mPanel.showAndUpdatePositionAsync();
            this.mHandle.invisible();
            this.mOwner.getSelectionController().setMinMaxOffset(this.mTextOffset);
        }

        public void computePanelPosition(int[] paramArrayOfInt)
        {
            this.mHandle.getHotspotLocationOnScreen(paramArrayOfInt);
        }

        public int computePanelPositionOnBottom()
        {
            this.mHandle.getHotspotLocationOnScreen(this.mCoords);
            return this.mCoords[1];
        }

        public void hide()
        {
            Log.i("MiuiCursorController", "InsertionPointCursorController is hidden including all panels and handle");
            this.mPanel.hide();
            this.mClipboardPanel.hide();
            this.mHandle.hide();
        }

        public boolean isShowing()
        {
            if ((this.mHandle.isShowing()) || (this.mPanel.isShowing()) || (this.mClipboardPanel.isShowing()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        void onClipBoardPancelClick()
        {
            this.mClipboardPanel.hide();
        }

        public boolean onHandleTouchEvent(MiuiCursorController.MiuiHandleView paramMiuiHandleView, MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            default:
            case 0:
            case 1:
            case 2:
            }
            while (true)
            {
                return false;
                this.mPreviousDownTime = SystemClock.uptimeMillis();
                continue;
                if ((this.mPanel.isShowing()) && (SystemClock.uptimeMillis() - this.mPreviousDownTime <= ViewConfiguration.getTapTimeout()))
                {
                    this.mPanel.hide();
                }
                else
                {
                    Log.i("MiuiCursorController", "InsertionPointCursorController onHandleTouchEvent() for ACTION_UP");
                    if (this.mOwner.needInsertPanel())
                    {
                        showInsertionPanel();
                        continue;
                        if (this.mPanel.isShowing())
                            this.mPanel.hide();
                        this.mHandle.onHandleMoved();
                    }
                }
            }
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            default:
            case 2:
            }
            while (true)
            {
                return false;
                if (this.mPanel.isShowing())
                    hide();
            }
        }

        void setupClipBoardPanel(ClipData paramClipData, View.OnClickListener paramOnClickListener)
        {
            LinearLayout localLinearLayout = (LinearLayout)this.mClipboardPanel.getElement(101384219);
            localLinearLayout.removeAllViews();
            int i = -1 + Math.min(3, paramClipData.getItemCount());
            LayoutInflater localLayoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
            for (int j = 0; j <= i; j++)
            {
                TextView localTextView = (TextView)localLayoutInflater.inflate(100859953, null);
                localTextView.setText(paramClipData.getItemAt(j).coerceToText(this.mContext));
                localTextView.setOnClickListener(paramOnClickListener);
                localLinearLayout.addView(localTextView, new ViewGroup.LayoutParams(-1, -2));
                if (j != i)
                {
                    ImageView localImageView = new ImageView(this.mContext);
                    localImageView.setImageResource(100794636);
                    localImageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    localLinearLayout.addView(localImageView, new ViewGroup.LayoutParams(-1, -2));
                }
            }
            this.mClipboardPanel.showAndUpdatePositionAsync();
        }

        public void show()
        {
            boolean bool = true;
            int[] arrayOfInt1 = new int[4];
            arrayOfInt1[0] = 101384217;
            arrayOfInt1[1] = 101384218;
            arrayOfInt1[2] = 101384215;
            arrayOfInt1[3] = 101384216;
            int[] arrayOfInt2 = new int[3];
            arrayOfInt2[0] = 101384220;
            arrayOfInt2[1] = 101384221;
            arrayOfInt2[2] = 101384222;
            ArrayList localArrayList = this.mOwner.getFloatPanelShowHides(arrayOfInt1);
            Log.i("MiuiCursorController", "InsertionPointCursorController show:" + localArrayList.toString());
            this.mPanel.showHideButtons(arrayOfInt1, arrayOfInt2, localArrayList);
            Log.i("MiuiCursorController", "InsertionPointCursorController is shown with hidden panels and shown handle");
            this.mPanel.hide();
            this.mClipboardPanel.hide();
            int i = this.mTextView.getSelectionStart();
            int j;
            int k;
            if ((i == this.mTextOffset) || (this.mTextView.length() <= 0))
            {
                j = bool;
                this.mTextOffset = i;
                if ((!this.mOwner.hasMagnifierController()) || (!this.mOwner.getMagnifierController().isShowing()))
                    break label247;
                k = bool;
                label181: this.mHandle.show();
                if ((j != 0) && (k == 0))
                {
                    Log.i("MiuiCursorController", "Show InsertionPointCursorController in one shot context");
                    if ((!this.mOwner.needInsertPanel()) || (!this.mShowOnOneShot))
                        break label253;
                    showInsertionPanel();
                    label228: if (this.mShowOnOneShot)
                        break label263;
                }
            }
            while (true)
            {
                this.mShowOnOneShot = bool;
                return;
                j = 0;
                break;
                label247: k = 0;
                break label181;
                label253: this.mHandle.hide();
                break label228;
                label263: bool = false;
            }
        }

        public void updatePosition()
        {
            this.mTextOffset = this.mTextView.getSelectionStart();
            if (this.mTextOffset < 0)
            {
                Log.e("MiuiCursorController", "Update cursor controller position called with no cursor");
                hide();
            }
            while (true)
            {
                return;
                if ((!this.mPanel.isShowing()) && (!this.mClipboardPanel.isShowing()))
                    this.mHandle.positionAtCursorOffset(this.mTextOffset, true);
                if (this.mPanel.isShowing())
                    this.mPanel.showAndUpdatePositionAsync();
                if (this.mClipboardPanel.isShowing())
                    this.mClipboardPanel.showAndUpdatePositionAsync();
            }
        }
    }

    private class FloatPanelViewController
    {
        private PopupWindow mContainer;
        private FloatPanelView mContent;
        private MiuiCursorController mController;
        private Display mDisplay;
        private int[] mPos = new int[2];
        private int mStatusBarHeight;

        static
        {
            if (!MiuiCursorController.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        public FloatPanelViewController(MiuiCursorController paramInt, int arg3)
        {
            this.mController = paramInt;
            this.mContainer = new PopupWindow(MiuiCursorController.this.mContext, null, 16843464);
            this.mContainer.setSplitTouchEnabled(true);
            this.mContainer.setWindowLayoutType(1002);
            int i;
            this.mContent = ((FloatPanelView)((LayoutInflater)MiuiCursorController.this.mContext.getSystemService("layout_inflater")).inflate(i, null));
            this.mContainer.setContentView(this.mContent);
            this.mContainer.setWindowLayoutMode(-2, -2);
            this.mStatusBarHeight = MiuiCursorController.this.mContext.getResources().getDimensionPixelSize(101318656);
            this.mDisplay = ((WindowManager)MiuiCursorController.this.mContext.getSystemService("window")).getDefaultDisplay();
        }

        private void updatePosition()
        {
            this.mController.computePanelPosition(this.mPos);
            int i = this.mPos[0] - this.mContent.getWidth() / 2;
            int j = this.mPos[1] - this.mContent.getHeight() - MiuiCursorController.this.mTextView.getLineHeight();
            int k;
            int m;
            if (j < this.mStatusBarHeight)
            {
                this.mContent.setArrow(true);
                j = this.mController.computePanelPositionOnBottom();
                k = this.mDisplay.getWidth();
                m = this.mContent.getWidth() / 2;
                if (i >= 0)
                    break label151;
                m += i;
            }
            while (true)
            {
                this.mContent.setArrowPos(m);
                this.mContainer.update(i, j, this.mContent.getWidth(), this.mContent.getHeight());
                return;
                this.mContent.setArrow(false);
                break;
                label151: if (i + this.mContent.getWidth() > k)
                    m += i + this.mContent.getWidth() - k;
            }
        }

        public View getElement(int paramInt)
        {
            return this.mContent.findViewById(paramInt);
        }

        public void hide()
        {
            this.mContainer.dismiss();
        }

        public boolean isShowing()
        {
            return this.mContainer.isShowing();
        }

        public void setButtonOnClickListener(int paramInt, View.OnClickListener paramOnClickListener)
        {
            View localView = this.mContent.findViewById(paramInt);
            if (localView != null)
                localView.setOnClickListener(paramOnClickListener);
            while (true)
            {
                return;
                Log.e("MiuiCursorController", "FloatPanelView.setButtonHandler, could not find view, id:" + paramInt);
            }
        }

        public void show()
        {
            this.mController.computePanelPosition(this.mPos);
            this.mContainer.showAtLocation(MiuiCursorController.this.mTextView, 0, this.mPos[0] - this.mContent.getWidth() / 2, this.mPos[1] - this.mContent.getHeight() - MiuiCursorController.this.mTextView.getLineHeight());
            updatePosition();
        }

        public void showAndUpdatePositionAsync()
        {
            Log.i("MiuiCursorController", "FloatPanelViewController to showAndUpdatePositionAsync");
            if (!isShowing())
                show();
            MiuiCursorController.this.mTextView.post(new Runnable()
            {
                public void run()
                {
                    Log.i("MiuiCursorController", ">>FloatPanelViewController to updatePosition for showAndUpdatePositionAsync");
                    MiuiCursorController.FloatPanelViewController.this.updatePosition();
                }
            });
        }

        public void showElement(int paramInt, boolean paramBoolean)
        {
            View localView = this.mContent.findViewById(paramInt);
            int i;
            if (localView != null)
                if (paramBoolean)
                {
                    i = 0;
                    localView.setVisibility(i);
                }
            while (true)
            {
                return;
                i = 8;
                break;
                Log.e("MiuiCursorController", "FloatPanelView.showButton, could not find view, id:" + paramInt);
            }
        }

        public void showHideButtons(int[] paramArrayOfInt1, int[] paramArrayOfInt2, ArrayList<Boolean> paramArrayList)
        {
            assert ((paramArrayOfInt1.length == 1 + paramArrayOfInt2.length) && (paramArrayOfInt1.length == paramArrayList.size()));
            showElement(paramArrayOfInt1[0], ((Boolean)paramArrayList.get(0)).booleanValue());
            int i;
            int j;
            label70: Boolean localBoolean;
            int k;
            if (!((Boolean)paramArrayList.get(0)).booleanValue())
            {
                i = 1;
                j = 1;
                if (j >= paramArrayOfInt1.length)
                    return;
                localBoolean = (Boolean)paramArrayList.get(j);
                showElement(paramArrayOfInt1[j], localBoolean.booleanValue());
                k = paramArrayOfInt2[(j - 1)];
                if ((i != 0) || (!localBoolean.booleanValue()))
                    break label161;
            }
            label161: for (boolean bool = true; ; bool = false)
            {
                showElement(k, bool);
                if ((i != 0) && (localBoolean.booleanValue()))
                    i = 0;
                j++;
                break label70;
                i = 0;
                break;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MiuiCursorController
 * JD-Core Version:        0.6.2
 */