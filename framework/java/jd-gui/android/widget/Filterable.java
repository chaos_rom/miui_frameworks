package android.widget;

public abstract interface Filterable
{
    public abstract Filter getFilter();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Filterable
 * JD-Core Version:        0.6.2
 */