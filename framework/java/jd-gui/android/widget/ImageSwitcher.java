package android.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class ImageSwitcher extends ViewSwitcher
{
    public ImageSwitcher(Context paramContext)
    {
        super(paramContext);
    }

    public ImageSwitcher(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ImageSwitcher.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ImageSwitcher.class.getName());
    }

    public void setImageDrawable(Drawable paramDrawable)
    {
        ((ImageView)getNextView()).setImageDrawable(paramDrawable);
        showNext();
    }

    public void setImageResource(int paramInt)
    {
        ((ImageView)getNextView()).setImageResource(paramInt);
        showNext();
    }

    public void setImageURI(Uri paramUri)
    {
        ((ImageView)getNextView()).setImageURI(paramUri);
        showNext();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ImageSwitcher
 * JD-Core Version:        0.6.2
 */