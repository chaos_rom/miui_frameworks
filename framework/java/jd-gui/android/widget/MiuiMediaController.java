package android.widget;

import android.view.View;

public abstract interface MiuiMediaController
{
    public abstract View getView();

    public abstract void hide();

    public abstract boolean isShowing();

    public abstract void resetTime();

    public abstract void setCanReplay(boolean paramBoolean);

    public abstract void setControlListener(OnControllerChangedListener paramOnControllerChangedListener);

    public abstract void setTimes(int paramInt1, int paramInt2);

    public abstract void show();

    public abstract void showEnded();

    public abstract void showErrorMessage(String paramString);

    public abstract void showLoading();

    public abstract void showPaused();

    public abstract void showPlaying();

    public static abstract interface OnControllerChangedListener
    {
        public abstract void onHidden();

        public abstract void onPlayPause();

        public abstract void onReplay();

        public abstract void onSeekEnd(int paramInt);

        public abstract void onSeekMove(int paramInt);

        public abstract void onSeekStart();

        public abstract void onShown();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MiuiMediaController
 * JD-Core Version:        0.6.2
 */