package android.widget;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewParent;
import miui.provider.ExtraSettings.Secure;

public class MagnifierController
{
    public static final String LOG_TAG = "MiuiMagnifierController";
    private final Editor mEditor;
    private int mLongClickX;
    private int mLongClickY;
    private int mOffset = -1;
    private boolean mShowing;
    private final TextView mTextView;

    public MagnifierController(Context paramContext, Editor paramEditor)
    {
        Log.i("MiuiMagnifierController", "MagnifierController is created");
        this.mEditor = paramEditor;
        this.mTextView = paramEditor.textview();
        this.mShowing = false;
    }

    private void hide()
    {
        this.mShowing = false;
        if (this.mTextView.getParent() != null)
            this.mTextView.getParent().requestDisallowInterceptTouchEvent(false);
        this.mEditor.makeBlink();
    }

    public static boolean isMagnifierEnabled(Context paramContext)
    {
        return ExtraSettings.Secure.showMagnifierWhenInput(paramContext);
    }

    private void showMagnifier()
    {
        this.mEditor.stopBlink();
        this.mTextView.getContext().sendBroadcast(new Intent("android.intent.action.SHOW_MAGNIFIER"));
    }

    private void updatePosition(boolean paramBoolean)
    {
        int i = this.mTextView.getOffsetForPosition(this.mLongClickX, this.mLongClickY);
        if (i != this.mOffset)
        {
            this.mEditor.setTextSelection(i);
            this.mOffset = i;
            if (!paramBoolean)
                showMagnifier();
        }
    }

    public boolean isShowing()
    {
        return this.mShowing;
    }

    public void onParentChanged()
    {
        if (this.mShowing)
            showMagnifier();
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        this.mLongClickX = ((int)paramMotionEvent.getX());
        this.mLongClickY = ((int)paramMotionEvent.getY());
        if (isShowing())
            switch (paramMotionEvent.getActionMasked())
            {
            default:
            case 2:
            case 1:
            case 3:
            }
        while (true)
        {
            this.mEditor.getInsertionController().onHandleTouchEvent(null, paramMotionEvent);
            return bool;
            updatePosition(false);
            bool = true;
            continue;
            hide();
            bool = false;
        }
    }

    public void show()
    {
        this.mShowing = true;
        if (this.mTextView.getParent() != null)
            this.mTextView.getParent().requestDisallowInterceptTouchEvent(true);
        updatePosition(true);
        showMagnifier();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MagnifierController
 * JD-Core Version:        0.6.2
 */