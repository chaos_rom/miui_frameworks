package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.ExtractEditText;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.DynamicLayout;
import android.text.Editable;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.ParcelableSpan;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.text.method.MetaKeyKeyListener;
import android.text.method.MovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.WordIterator;
import android.text.style.EasyEditSpan;
import android.text.style.SuggestionRangeSpan;
import android.text.style.SuggestionSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.DisplayList;
import android.view.DragEvent;
import android.view.HardwareCanvas;
import android.view.HardwareRenderer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.ViewTreeObserver.OnTouchModeChangeListener;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.util.ArrayUtils;
import com.android.internal.widget.EditableInputConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

public class Editor
{
    static final int BLINK = 500;
    private static int DRAG_SHADOW_MAX_TEXT_LENGTH = 0;
    static final int EXTRACT_NOTHING = -2;
    static final int EXTRACT_UNKNOWN = -1;
    private static final String TAG = "Editor";
    private static final float[] TEMP_POSITION = new float[2];
    Blink mBlink;
    private Context mContext;
    CorrectionHighlighter mCorrectionHighlighter;
    boolean mCreatedWithASelection;
    int mCursorCount;
    final Drawable[] mCursorDrawable = new Drawable[2];
    boolean mCursorVisible = true;
    ActionMode.Callback mCustomSelectionActionModeCallback;
    boolean mDiscardNextActionUp;
    private EasyEditSpanController mEasyEditSpanController;
    CharSequence mError;
    ErrorPopup mErrorPopup;
    boolean mErrorWasChanged;
    boolean mFrozenWithFocus;
    boolean mIgnoreActionUpEvent;
    boolean mInBatchEditControllers;
    InputContentType mInputContentType;
    InputMethodState mInputMethodState;
    int mInputType = 0;
    boolean mInsertionControllerEnabled;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    MiuiCursorController mInsertionPointCursorController;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private boolean mIsInTextSelectionMode = false;
    KeyListener mKeyListener;
    float mLastDownPositionX;
    float mLastDownPositionY;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private MagnifierController mMagnifierController;
    private PositionListener mPositionListener;
    boolean mPreserveDetachedSelection;
    boolean mSelectAllOnFocus;
    private Drawable mSelectHandleCenter;
    private Drawable mSelectHandleLeft;
    private Drawable mSelectHandleRight;
    ActionMode mSelectionActionMode;
    boolean mSelectionControllerEnabled;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    MiuiCursorController mSelectionModifierCursorController;
    boolean mSelectionMoved;
    long mShowCursor;
    boolean mShowErrorAfterAttach;
    boolean mShowSoftInputOnFocus = true;
    Runnable mShowSuggestionRunnable;
    SpellChecker mSpellChecker;
    SuggestionRangeSpan mSuggestionRangeSpan;
    SuggestionsPopupWindow mSuggestionsPopupWindow;
    private Rect mTempRect;
    boolean mTemporaryDetach;
    DisplayList[] mTextDisplayLists;
    boolean mTextIsSelectable;
    private TextView mTextView;
    boolean mTouchFocusSelected;
    WordIterator mWordIterator;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    Editor(TextView paramTextView)
    {
        this.mTextView = paramTextView;
        this.mContext = paramTextView.getContext();
    }

    private boolean canSelectText()
    {
        if ((hasSelectionController()) && (this.mTextView.getText().length() != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void chooseSize(PopupWindow paramPopupWindow, CharSequence paramCharSequence, TextView paramTextView)
    {
        int i = paramTextView.getPaddingLeft() + paramTextView.getPaddingRight();
        int j = paramTextView.getPaddingTop() + paramTextView.getPaddingBottom();
        int k = this.mTextView.getResources().getDimensionPixelSize(17104969);
        StaticLayout localStaticLayout = new StaticLayout(paramCharSequence, paramTextView.getPaint(), k, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
        float f = 0.0F;
        for (int m = 0; m < localStaticLayout.getLineCount(); m++)
            f = Math.max(f, localStaticLayout.getLineWidth(m));
        paramPopupWindow.setWidth(i + (int)Math.ceil(f));
        paramPopupWindow.setHeight(j + localStaticLayout.getHeight());
    }

    private void downgradeEasyCorrectionSpans()
    {
        CharSequence localCharSequence = this.mTextView.getText();
        if ((localCharSequence instanceof Spannable))
        {
            Spannable localSpannable = (Spannable)localCharSequence;
            SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])localSpannable.getSpans(0, localSpannable.length(), SuggestionSpan.class);
            for (int i = 0; i < arrayOfSuggestionSpan.length; i++)
            {
                int j = arrayOfSuggestionSpan[i].getFlags();
                if (((j & 0x1) != 0) && ((j & 0x2) == 0))
                {
                    int k = j & 0xFFFFFFFE;
                    arrayOfSuggestionSpan[i].setFlags(k);
                }
            }
        }
    }

    private void drawCursor(Canvas paramCanvas, int paramInt)
    {
        if (paramInt != 0);
        for (int i = 1; ; i = 0)
        {
            if (i != 0)
                paramCanvas.translate(0.0F, paramInt);
            for (int j = 0; j < this.mCursorCount; j++)
                this.mCursorDrawable[j].draw(paramCanvas);
        }
        if (i != 0)
            paramCanvas.translate(0.0F, -paramInt);
    }

    private void drawHardwareAccelerated(Canvas paramCanvas, Layout paramLayout, Path paramPath, Paint paramPaint, int paramInt)
    {
        long l = paramLayout.getLineRangeForDraw(paramCanvas);
        int i = TextUtils.unpackRangeStartFromLong(l);
        int j = TextUtils.unpackRangeEndFromLong(l);
        if (j < 0)
            return;
        paramLayout.drawBackground(paramCanvas, paramPath, paramPaint, paramInt, i, j);
        int[] arrayOfInt1;
        int[] arrayOfInt2;
        int k;
        int m;
        int n;
        int i1;
        if ((paramLayout instanceof DynamicLayout))
        {
            if (this.mTextDisplayLists == null)
                this.mTextDisplayLists = new DisplayList[ArrayUtils.idealObjectArraySize(0)];
            DynamicLayout localDynamicLayout = (DynamicLayout)paramLayout;
            arrayOfInt1 = localDynamicLayout.getBlockEndLines();
            arrayOfInt2 = localDynamicLayout.getBlockIndices();
            k = localDynamicLayout.getNumberOfBlocks();
            m = -1;
            n = 0;
            i1 = 0;
        }
        while (i1 < k)
        {
            int i2 = arrayOfInt1[i1];
            int i3 = arrayOfInt2[i1];
            int i4;
            DisplayList localDisplayList;
            if (i3 == -1)
            {
                i4 = 1;
                if (i4 != 0)
                {
                    i3 = getAvailableDisplayListIndex(arrayOfInt2, k, n);
                    arrayOfInt2[i1] = i3;
                    n = i3 + 1;
                }
                localDisplayList = this.mTextDisplayLists[i3];
                if (localDisplayList != null)
                    break label343;
                DisplayList[] arrayOfDisplayList = this.mTextDisplayLists;
                localDisplayList = this.mTextView.getHardwareRenderer().createDisplayList("Text " + i3);
                arrayOfDisplayList[i3] = localDisplayList;
            }
            label224: int i5;
            int i6;
            int i7;
            int i8;
            int i9;
            label343: HardwareCanvas localHardwareCanvas;
            int i10;
            int i11;
            while (true)
                if (!localDisplayList.isValid())
                {
                    i5 = m + 1;
                    i6 = paramLayout.getLineTop(i5);
                    i7 = paramLayout.getLineBottom(i2);
                    i8 = 0;
                    i9 = this.mTextView.getWidth();
                    if (this.mTextView.getHorizontallyScrolling())
                    {
                        float f1 = 3.4028235E+38F;
                        float f2 = 1.4E-45F;
                        int i12 = i5;
                        while (true)
                            if (i12 <= i2)
                            {
                                float f3 = paramLayout.getLineLeft(i12);
                                f1 = Math.min(f1, f3);
                                float f4 = paramLayout.getLineRight(i12);
                                f2 = Math.max(f2, f4);
                                i12++;
                                continue;
                                i4 = 0;
                                break;
                                if (i4 == 0)
                                    break label224;
                                localDisplayList.invalidate();
                                break label224;
                            }
                        i8 = (int)f1;
                        i9 = (int)(0.5F + f2);
                    }
                    localHardwareCanvas = localDisplayList.start();
                    i10 = i9 - i8;
                    i11 = i7 - i6;
                }
            try
            {
                localHardwareCanvas.setViewport(i10, i11);
                localHardwareCanvas.onPreDraw(null);
                localHardwareCanvas.translate(-i8, -i6);
                paramLayout.drawText(localHardwareCanvas, i5, i2);
                localHardwareCanvas.onPostDraw();
                localDisplayList.end();
                localDisplayList.setLeftTopRightBottom(i8, i6, i9, i7);
                localDisplayList.setClipChildren(false);
                ((HardwareCanvas)paramCanvas).drawDisplayList(localDisplayList, null, 0);
                m = i2;
                i1++;
            }
            finally
            {
                localHardwareCanvas.onPostDraw();
                localDisplayList.end();
                localDisplayList.setLeftTopRightBottom(i8, i6, i9, i7);
                localDisplayList.setClipChildren(false);
            }
        }
    }

    private boolean extractTextInternal(ExtractedTextRequest paramExtractedTextRequest, int paramInt1, int paramInt2, int paramInt3, ExtractedText paramExtractedText)
    {
        boolean bool = false;
        CharSequence localCharSequence = this.mTextView.getText();
        int i;
        int j;
        if (localCharSequence != null)
        {
            if (paramInt1 == -2)
                break label342;
            i = localCharSequence.length();
            if (paramInt1 >= 0)
                break label167;
            paramExtractedText.partialEndOffset = -1;
            paramExtractedText.partialStartOffset = -1;
            paramInt1 = 0;
            j = i;
            if ((0x1 & paramExtractedTextRequest.flags) == 0)
                break label326;
            paramExtractedText.text = localCharSequence.subSequence(paramInt1, j);
        }
        while (true)
        {
            paramExtractedText.flags = 0;
            if (MetaKeyKeyListener.getMetaState(localCharSequence, 2048) != 0)
                paramExtractedText.flags = (0x2 | paramExtractedText.flags);
            if (this.mTextView.isSingleLine())
                paramExtractedText.flags = (0x1 | paramExtractedText.flags);
            paramExtractedText.startOffset = 0;
            paramExtractedText.selectionStart = this.mTextView.getSelectionStart();
            paramExtractedText.selectionEnd = this.mTextView.getSelectionEnd();
            bool = true;
            return bool;
            label167: j = paramInt2 + paramInt3;
            if ((localCharSequence instanceof Spanned))
            {
                Spanned localSpanned = (Spanned)localCharSequence;
                Object[] arrayOfObject = localSpanned.getSpans(paramInt1, j, ParcelableSpan.class);
                int k = arrayOfObject.length;
                while (k > 0)
                {
                    k--;
                    int m = localSpanned.getSpanStart(arrayOfObject[k]);
                    if (m < paramInt1)
                        paramInt1 = m;
                    int n = localSpanned.getSpanEnd(arrayOfObject[k]);
                    if (n > j)
                        j = n;
                }
            }
            paramExtractedText.partialStartOffset = paramInt1;
            paramExtractedText.partialEndOffset = (j - paramInt3);
            if (paramInt1 > i)
                paramInt1 = i;
            while (true)
            {
                if (j <= i)
                    break label315;
                j = i;
                break;
                if (paramInt1 < 0)
                    paramInt1 = 0;
            }
            label315: if (j >= 0)
                break;
            j = 0;
            break;
            label326: paramExtractedText.text = TextUtils.substring(localCharSequence, paramInt1, j);
            continue;
            label342: paramExtractedText.partialStartOffset = 0;
            paramExtractedText.partialEndOffset = 0;
            paramExtractedText.text = "";
        }
    }

    private boolean extractedTextModeWillBeStarted()
    {
        boolean bool = false;
        if (!(this.mTextView instanceof ExtractEditText))
        {
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if ((localInputMethodManager != null) && (localInputMethodManager.isFullscreenMode()))
                bool = true;
        }
        return bool;
    }

    private int getAvailableDisplayListIndex(int[] paramArrayOfInt, int paramInt1, int paramInt2)
    {
        int i = this.mTextDisplayLists.length;
        int j = paramInt2;
        if (j < i)
        {
            int k = 0;
            for (int m = 0; ; m++)
                if (m < paramInt1)
                {
                    if (paramArrayOfInt[m] == j)
                        k = 1;
                }
                else
                {
                    if (k == 0)
                        break label93;
                    j++;
                    break;
                }
        }
        DisplayList[] arrayOfDisplayList = new DisplayList[ArrayUtils.idealIntArraySize(i + 1)];
        System.arraycopy(this.mTextDisplayLists, 0, arrayOfDisplayList, 0, i);
        this.mTextDisplayLists = arrayOfDisplayList;
        j = i;
        label93: return j;
    }

    private long getCharRange(int paramInt)
    {
        int i = this.mTextView.getText().length();
        long l;
        if ((paramInt + 1 < i) && (Character.isSurrogatePair(this.mTextView.getText().charAt(paramInt), this.mTextView.getText().charAt(paramInt + 1))))
            l = TextUtils.packRangeInLong(paramInt, paramInt + 2);
        while (true)
        {
            return l;
            if (paramInt < i)
            {
                l = TextUtils.packRangeInLong(paramInt, paramInt + 1);
            }
            else if (paramInt - 2 >= 0)
            {
                char c = this.mTextView.getText().charAt(paramInt - 1);
                if (Character.isSurrogatePair(this.mTextView.getText().charAt(paramInt - 2), c))
                    l = TextUtils.packRangeInLong(paramInt - 2, paramInt);
            }
            else if (paramInt - 1 >= 0)
            {
                l = TextUtils.packRangeInLong(paramInt - 1, paramInt);
            }
            else
            {
                l = TextUtils.packRangeInLong(paramInt, paramInt);
            }
        }
    }

    private int getErrorX()
    {
        float f = this.mTextView.getResources().getDisplayMetrics().density;
        TextView.Drawables localDrawables = this.mTextView.mDrawables;
        int i = this.mTextView.getWidth() - this.mErrorPopup.getWidth() - this.mTextView.getPaddingRight();
        if (localDrawables != null);
        for (int j = localDrawables.mDrawableSizeRight; ; j = 0)
            return i - j / 2 + (int)(0.5F + 25.0F * f);
    }

    private int getErrorY()
    {
        int i = 0;
        int j = this.mTextView.getCompoundPaddingTop();
        int k = this.mTextView.getBottom() - this.mTextView.getTop() - this.mTextView.getCompoundPaddingBottom() - j;
        TextView.Drawables localDrawables = this.mTextView.mDrawables;
        if (localDrawables != null);
        for (int m = localDrawables.mDrawableHeightRight; ; m = 0)
        {
            int n = j + (k - m) / 2;
            float f = this.mTextView.getResources().getDisplayMetrics().density;
            if (localDrawables != null)
                i = localDrawables.mDrawableHeightRight;
            return n + i - this.mTextView.getHeight() - (int)(0.5F + 2.0F * f);
        }
    }

    private int getLastTapPosition()
    {
        if (this.mSelectionModifierCursorController != null)
        {
            i = this.mSelectionModifierCursorController.getMinTouchOffset();
            if (i >= 0)
                if (i <= this.mTextView.getText().length());
        }
        for (int i = this.mTextView.getText().length(); ; i = -1)
            return i;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private long getLastTouchOffsets()
    {
        MiuiCursorController localMiuiCursorController = getSelectionController();
        return TextUtils.packRangeInLong(localMiuiCursorController.getMinTouchOffset(), localMiuiCursorController.getMaxTouchOffset());
    }

    private PositionListener getPositionListener()
    {
        if (this.mPositionListener == null)
            this.mPositionListener = new PositionListener(null);
        return this.mPositionListener;
    }

    private float getPrimaryHorizontal(Layout paramLayout1, Layout paramLayout2, int paramInt)
    {
        if ((TextUtils.isEmpty(paramLayout1.getText())) && (paramLayout2 != null) && (!TextUtils.isEmpty(paramLayout2.getText())));
        for (float f = paramLayout2.getPrimaryHorizontal(paramInt); ; f = paramLayout1.getPrimaryHorizontal(paramInt))
            return f;
    }

    private View.DragShadowBuilder getTextThumbnailBuilder(CharSequence paramCharSequence)
    {
        TextView localTextView = (TextView)View.inflate(this.mTextView.getContext(), 17367219, null);
        if (localTextView == null)
            throw new IllegalArgumentException("Unable to inflate text drag thumbnail");
        if (paramCharSequence.length() > DRAG_SHADOW_MAX_TEXT_LENGTH)
            paramCharSequence = paramCharSequence.subSequence(0, DRAG_SHADOW_MAX_TEXT_LENGTH);
        localTextView.setText(paramCharSequence);
        localTextView.setTextColor(this.mTextView.getTextColors());
        localTextView.setTextAppearance(this.mTextView.getContext(), 16);
        localTextView.setGravity(17);
        localTextView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        int i = View.MeasureSpec.makeMeasureSpec(0, 0);
        localTextView.measure(i, i);
        localTextView.layout(0, 0, localTextView.getMeasuredWidth(), localTextView.getMeasuredHeight());
        localTextView.invalidate();
        return new View.DragShadowBuilder(localTextView);
    }

    private boolean hasPasswordTransformationMethod()
    {
        return this.mTextView.getTransformationMethod() instanceof PasswordTransformationMethod;
    }

    private void hideCursorControllers()
    {
        if ((this.mSuggestionsPopupWindow != null) && (!this.mSuggestionsPopupWindow.isShowingUp()))
            this.mSuggestionsPopupWindow.hide();
        hideInsertionPointCursorController();
        stopSelectionActionMode();
    }

    private void hideError()
    {
        if ((this.mErrorPopup != null) && (this.mErrorPopup.isShowing()))
            this.mErrorPopup.dismiss();
        this.mShowErrorAfterAttach = false;
    }

    private void hideInsertionPointCursorController()
    {
        if (this.mInsertionPointCursorController != null)
            this.mInsertionPointCursorController.hide();
    }

    private void hideSpanControllers()
    {
        if (this.mEasyEditSpanController != null)
            this.mEasyEditSpanController.hide();
    }

    private boolean isCursorInsideEasyCorrectionSpan()
    {
        SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])((Spannable)this.mTextView.getText()).getSpans(this.mTextView.getSelectionStart(), this.mTextView.getSelectionEnd(), SuggestionSpan.class);
        int i = 0;
        if (i < arrayOfSuggestionSpan.length)
            if ((0x1 & arrayOfSuggestionSpan[i].getFlags()) == 0);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    private boolean isCursorInsideSuggestionSpan()
    {
        boolean bool = false;
        CharSequence localCharSequence = this.mTextView.getText();
        if (!(localCharSequence instanceof Spannable));
        while (true)
        {
            return bool;
            if (((SuggestionSpan[])((Spannable)localCharSequence).getSpans(this.mTextView.getSelectionStart(), this.mTextView.getSelectionEnd(), SuggestionSpan.class)).length > 0)
                bool = true;
        }
    }

    private boolean isOffsetVisible(int paramInt)
    {
        Layout localLayout = this.mTextView.getLayout();
        int i = localLayout.getLineBottom(localLayout.getLineForOffset(paramInt));
        return isPositionVisible((int)localLayout.getPrimaryHorizontal(paramInt) + this.mTextView.viewportToContentHorizontalOffset(), i + this.mTextView.viewportToContentVerticalOffset());
    }

    private boolean isPositionOnText(float paramFloat1, float paramFloat2)
    {
        boolean bool = false;
        Layout localLayout = this.mTextView.getLayout();
        if (localLayout == null);
        while (true)
        {
            return bool;
            int i = this.mTextView.getLineAtCoordinate(paramFloat2);
            float f = this.mTextView.convertToLocalHorizontalCoordinate(paramFloat1);
            if ((f >= localLayout.getLineLeft(i)) && (f <= localLayout.getLineRight(i)))
                bool = true;
        }
    }

    private boolean isPositionVisible(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        while (true)
        {
            synchronized (TEMP_POSITION)
            {
                float[] arrayOfFloat2 = TEMP_POSITION;
                arrayOfFloat2[0] = paramInt1;
                arrayOfFloat2[1] = paramInt2;
                localObject2 = this.mTextView;
                if (localObject2 != null)
                {
                    if (localObject2 != this.mTextView)
                    {
                        arrayOfFloat2[0] -= ((View)localObject2).getScrollX();
                        arrayOfFloat2[1] -= ((View)localObject2).getScrollY();
                    }
                    if ((arrayOfFloat2[0] >= 0.0F) && (arrayOfFloat2[1] >= 0.0F) && (arrayOfFloat2[0] <= ((View)localObject2).getWidth()) && (arrayOfFloat2[1] > ((View)localObject2).getHeight()))
                        continue;
                    if (!((View)localObject2).getMatrix().isIdentity())
                        ((View)localObject2).getMatrix().mapPoints(arrayOfFloat2);
                    arrayOfFloat2[0] += ((View)localObject2).getLeft();
                    arrayOfFloat2[1] += ((View)localObject2).getTop();
                    ViewParent localViewParent = ((View)localObject2).getParent();
                    if (!(localViewParent instanceof View))
                        break label223;
                    localObject2 = (View)localViewParent;
                    continue;
                }
                bool = true;
            }
            label223: Object localObject2 = null;
        }
    }

    private void resumeBlink()
    {
        if (this.mBlink != null)
        {
            this.mBlink.uncancel();
            makeBlink();
        }
    }

    private boolean selectCurrentWord()
    {
        boolean bool;
        if (!canSelectText())
            bool = false;
        while (true)
        {
            return bool;
            if (hasPasswordTransformationMethod())
            {
                bool = this.mTextView.selectAllText();
            }
            else
            {
                int i = this.mTextView.getInputType();
                int j = i & 0xF;
                int k = i & 0xFF0;
                if ((j == 2) || (j == 3) || (j == 4) || (k == 16) || (k == 32) || (k == 208) || (k == 176))
                {
                    bool = this.mTextView.selectAllText();
                }
                else
                {
                    long l1 = getLastTouchOffsets();
                    int m = TextUtils.unpackRangeStartFromLong(l1);
                    int n = TextUtils.unpackRangeEndFromLong(l1);
                    if ((m < 0) || (m >= this.mTextView.getText().length()))
                    {
                        bool = false;
                    }
                    else if ((n < 0) || (n >= this.mTextView.getText().length()))
                    {
                        bool = false;
                    }
                    else
                    {
                        URLSpan[] arrayOfURLSpan = (URLSpan[])((Spanned)this.mTextView.getText()).getSpans(m, n, URLSpan.class);
                        URLSpan localURLSpan;
                        int i1;
                        if (arrayOfURLSpan.length >= 1)
                        {
                            localURLSpan = arrayOfURLSpan[0];
                            i1 = ((Spanned)this.mTextView.getText()).getSpanStart(localURLSpan);
                        }
                        long l2;
                        for (int i2 = ((Spanned)this.mTextView.getText()).getSpanEnd(localURLSpan); ; i2 = TextUtils.unpackRangeEndFromLong(l2))
                        {
                            do
                            {
                                Selection.setSelection((Spannable)this.mTextView.getText(), i1, i2);
                                if (i2 <= i1)
                                    break label374;
                                bool = true;
                                break;
                                WordIterator localWordIterator = getWordIterator();
                                localWordIterator.setCharSequence(this.mTextView.getText(), m, n);
                                i1 = localWordIterator.getBeginning(m);
                                i2 = localWordIterator.getEnd(n);
                            }
                            while ((i1 != -1) && (i2 != -1) && (i1 != i2));
                            l2 = getCharRange(m);
                            i1 = TextUtils.unpackRangeStartFromLong(l2);
                        }
                        label374: bool = false;
                    }
                }
            }
        }
    }

    private boolean shouldBlink()
    {
        boolean bool = false;
        if ((!isCursorVisible()) || (!this.mTextView.isFocused()));
        while (true)
        {
            return bool;
            int i = this.mTextView.getSelectionStart();
            if (i >= 0)
            {
                int j = this.mTextView.getSelectionEnd();
                if ((j >= 0) && (i == j))
                    bool = true;
            }
        }
    }

    private void showError()
    {
        if (this.mTextView.getWindowToken() == null)
            this.mShowErrorAfterAttach = true;
        while (true)
        {
            return;
            if (this.mErrorPopup == null)
            {
                TextView localTextView2 = (TextView)LayoutInflater.from(this.mTextView.getContext()).inflate(17367227, null);
                float f = this.mTextView.getResources().getDisplayMetrics().density;
                this.mErrorPopup = new ErrorPopup(localTextView2, (int)(0.5F + 200.0F * f), (int)(0.5F + 50.0F * f));
                this.mErrorPopup.setFocusable(false);
                this.mErrorPopup.setInputMethodMode(1);
            }
            TextView localTextView1 = (TextView)this.mErrorPopup.getContentView();
            chooseSize(this.mErrorPopup, this.mError, localTextView1);
            localTextView1.setText(this.mError);
            this.mErrorPopup.showAsDropDown(this.mTextView, getErrorX(), getErrorY());
            this.mErrorPopup.fixDirection(this.mErrorPopup.isAboveAnchor());
        }
    }

    private void suspendBlink()
    {
        if (this.mBlink != null)
            this.mBlink.cancel();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private boolean touchPositionIsInSelection()
    {
        boolean bool1 = false;
        int i = this.mTextView.getSelectionStart();
        int j = this.mTextView.getSelectionEnd();
        if (i == j)
            return bool1;
        if (i > j)
        {
            int n = i;
            i = j;
            j = n;
            Selection.setSelection((Spannable)this.mTextView.getText(), i, j);
        }
        MiuiCursorController localMiuiCursorController = getSelectionController();
        int k = localMiuiCursorController.getMinTouchOffset();
        int m = localMiuiCursorController.getMaxTouchOffset();
        if ((k >= i) && (m < j));
        for (boolean bool2 = true; ; bool2 = false)
        {
            bool1 = bool2;
            break;
        }
    }

    private void updateCursorPosition(int paramInt1, int paramInt2, int paramInt3, float paramFloat)
    {
        if (this.mCursorDrawable[paramInt1] == null)
            this.mCursorDrawable[paramInt1] = this.mTextView.getResources().getDrawable(this.mTextView.mCursorDrawableRes);
        if (this.mTempRect == null)
            this.mTempRect = new Rect();
        this.mCursorDrawable[paramInt1].getPadding(this.mTempRect);
        int i = this.mCursorDrawable[paramInt1].getIntrinsicWidth();
        int j = (int)Math.max(0.5F, paramFloat - 0.5F) - this.mTempRect.left;
        this.mCursorDrawable[paramInt1].setBounds(j, paramInt2 - this.mTempRect.top, j + i, paramInt3 + this.mTempRect.bottom);
    }

    private void updateSpellCheckSpans(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if ((this.mTextView.isTextEditable()) && (this.mTextView.isSuggestionsEnabled()) && (!(this.mTextView instanceof ExtractEditText)))
        {
            if ((this.mSpellChecker == null) && (paramBoolean))
                this.mSpellChecker = new SpellChecker(this.mTextView);
            if (this.mSpellChecker != null)
                this.mSpellChecker.spellCheck(paramInt1, paramInt2);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void addPositionListenerSubscriber(TextViewPositionListener paramTextViewPositionListener, boolean paramBoolean)
    {
        getPositionListener().addSubscriber(paramTextViewPositionListener, paramBoolean);
    }

    public void addSpanWatchers(Spannable paramSpannable)
    {
        int i = paramSpannable.length();
        if (this.mKeyListener != null)
            paramSpannable.setSpan(this.mKeyListener, 0, i, 18);
        if (this.mEasyEditSpanController == null)
            this.mEasyEditSpanController = new EasyEditSpanController();
        paramSpannable.setSpan(this.mEasyEditSpanController, 0, i, 18);
    }

    void adjustInputType(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
    {
        if ((0xF & this.mInputType) == 1)
        {
            if ((paramBoolean1) || (paramBoolean2))
                this.mInputType = (0x80 | 0xFFFFF00F & this.mInputType);
            if (paramBoolean3)
                this.mInputType = (0xE0 | 0xFFFFF00F & this.mInputType);
        }
        while (true)
        {
            return;
            if (((0xF & this.mInputType) == 2) && (paramBoolean4))
                this.mInputType = (0x10 | 0xFFFFF00F & this.mInputType);
        }
    }

    boolean areSuggestionsShown()
    {
        if ((this.mSuggestionsPopupWindow != null) && (this.mSuggestionsPopupWindow.isShowing()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void beginBatchEdit()
    {
        this.mInBatchEditControllers = true;
        InputMethodState localInputMethodState = this.mInputMethodState;
        if (localInputMethodState != null)
        {
            int i = 1 + localInputMethodState.mBatchEditNesting;
            localInputMethodState.mBatchEditNesting = i;
            if (i == 1)
            {
                localInputMethodState.mCursorChanged = false;
                localInputMethodState.mChangedDelta = 0;
                if (!localInputMethodState.mContentChanged)
                    break label77;
                localInputMethodState.mChangedStart = 0;
                localInputMethodState.mChangedEnd = this.mTextView.getText().length();
            }
        }
        while (true)
        {
            this.mTextView.onBeginBatchEdit();
            return;
            label77: localInputMethodState.mChangedStart = -1;
            localInputMethodState.mChangedEnd = -1;
            localInputMethodState.mContentChanged = false;
        }
    }

    void createInputContentTypeIfNeeded()
    {
        if (this.mInputContentType == null)
            this.mInputContentType = new InputContentType();
    }

    void createInputMethodStateIfNeeded()
    {
        if (this.mInputMethodState == null)
            this.mInputMethodState = new InputMethodState();
    }

    public void endBatchEdit()
    {
        this.mInBatchEditControllers = false;
        InputMethodState localInputMethodState = this.mInputMethodState;
        if (localInputMethodState != null)
        {
            int i = -1 + localInputMethodState.mBatchEditNesting;
            localInputMethodState.mBatchEditNesting = i;
            if (i == 0)
                finishBatchEdit(localInputMethodState);
        }
    }

    void ensureEndedBatchEdit()
    {
        InputMethodState localInputMethodState = this.mInputMethodState;
        if ((localInputMethodState != null) && (localInputMethodState.mBatchEditNesting != 0))
        {
            localInputMethodState.mBatchEditNesting = 0;
            finishBatchEdit(localInputMethodState);
        }
    }

    boolean extractText(ExtractedTextRequest paramExtractedTextRequest, ExtractedText paramExtractedText)
    {
        return extractTextInternal(paramExtractedTextRequest, -1, -1, -1, paramExtractedText);
    }

    void finishBatchEdit(InputMethodState paramInputMethodState)
    {
        this.mTextView.onEndBatchEdit();
        if ((paramInputMethodState.mContentChanged) || (paramInputMethodState.mSelectionModeChanged))
        {
            this.mTextView.updateAfterEdit();
            reportExtractedText();
        }
        while (true)
        {
            return;
            if (paramInputMethodState.mCursorChanged)
                this.mTextView.invalidateCursor();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getButtonShowHides(boolean paramBoolean, int paramInt)
    {
        boolean bool1 = true;
        switch (paramInt)
        {
        default:
            bool1 = false;
        case 101384217:
        case 101384218:
        case 101384215:
        case 101384216:
        case 101384213:
        case 101384214:
        }
        while (true)
        {
            return bool1;
            try
            {
                bool1 = canSelectText();
                continue;
                if (!paramBoolean)
                    break label166;
                if (this.mTextView.canPaste())
                    continue;
                break label166;
                ClipboardManager localClipboardManager = (ClipboardManager)this.mContext.getSystemService("clipboard");
                if (localClipboardManager.getPrimaryClip() == null)
                    break label176;
                int i = localClipboardManager.getPrimaryClip().getItemCount();
                if (!paramBoolean)
                    break label171;
                if (i > 0)
                    continue;
                break label171;
                bool1 = this.mTextView.canCopy();
                continue;
                if (paramBoolean)
                {
                    boolean bool2 = this.mTextView.canCut();
                    if (bool2)
                        continue;
                }
                bool1 = false;
            }
            catch (SecurityException localSecurityException)
            {
                Log.e("Editor", localSecurityException.toString());
                bool1 = false;
            }
            continue;
            label166: bool1 = false;
            continue;
            label171: bool1 = false;
            continue;
            label176: bool1 = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    long getCharRangeWrap(int paramInt)
    {
        return getCharRange(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    ArrayList<Boolean> getFloatPanelShowHides(int[] paramArrayOfInt)
    {
        ArrayList localArrayList = new ArrayList();
        Boolean localBoolean = Boolean.valueOf(this.mTextView.isTextEditable());
        int i = paramArrayOfInt.length;
        for (int j = 0; j < i; j++)
        {
            int k = paramArrayOfInt[j];
            localArrayList.add(Boolean.valueOf(getButtonShowHides(localBoolean.booleanValue(), k)));
        }
        return localArrayList;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    MiuiCursorController getInsertionController()
    {
        if (!this.mInsertionControllerEnabled);
        for (MiuiCursorController localMiuiCursorController = null; ; localMiuiCursorController = this.mInsertionPointCursorController)
        {
            return localMiuiCursorController;
            if (this.mInsertionPointCursorController == null)
            {
                this.mInsertionPointCursorController = MiuiCursorController.create(this, this.mContext, 1);
                this.mTextView.getViewTreeObserver().addOnTouchModeChangeListener(this.mInsertionPointCursorController);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    long getLastCutOrCopyTime()
    {
        return TextView.LAST_CUT_OR_COPY_TIME;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    MagnifierController getMagnifierController()
    {
        MagnifierController localMagnifierController = null;
        if (hasMagnifierController())
        {
            if (this.mMagnifierController == null)
                this.mMagnifierController = new MagnifierController(this.mContext, this);
            localMagnifierController = this.mMagnifierController;
        }
        return localMagnifierController;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    long getPositionListenerPostion()
    {
        return TextUtils.packRangeInLong(getPositionListener().getPositionX(), getPositionListener().getPositionY());
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Drawable getSelectHandleCenterRes()
    {
        if (this.mSelectHandleCenter == null)
            this.mSelectHandleCenter = this.mContext.getResources().getDrawable(this.mTextView.mTextSelectHandleRes);
        return this.mSelectHandleCenter;
    }

    Drawable getSelectHandleLeftRes()
    {
        if (this.mSelectHandleLeft == null)
            this.mSelectHandleLeft = this.mContext.getResources().getDrawable(this.mTextView.mTextSelectHandleLeftRes);
        return this.mSelectHandleLeft;
    }

    Drawable getSelectHandleRightRes()
    {
        if (this.mSelectHandleRight == null)
            this.mSelectHandleRight = this.mContext.getResources().getDrawable(this.mTextView.mTextSelectHandleRightRes);
        return this.mSelectHandleRight;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    int getSelectHandleWindowStyle()
    {
        return 16843464;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    MiuiCursorController getSelectionController()
    {
        if (!this.mSelectionControllerEnabled);
        for (MiuiCursorController localMiuiCursorController = null; ; localMiuiCursorController = this.mSelectionModifierCursorController)
        {
            return localMiuiCursorController;
            if (this.mSelectionModifierCursorController == null)
            {
                this.mSelectionModifierCursorController = MiuiCursorController.create(this, this.mContext, 2);
                this.mTextView.getViewTreeObserver().addOnTouchModeChangeListener(this.mSelectionModifierCursorController);
            }
        }
    }

    public WordIterator getWordIterator()
    {
        if (this.mWordIterator == null)
            this.mWordIterator = new WordIterator(this.mTextView.getTextServicesLocale());
        return this.mWordIterator;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void handleFloatPanelClick(View paramView, MiuiCursorController paramMiuiCursorController)
    {
        int i = 0;
        int j = this.mTextView.length();
        if (this.mTextView.isFocused())
        {
            int k = this.mTextView.getSelectionStart();
            int m = this.mTextView.getSelectionEnd();
            i = Math.max(0, Math.min(k, m));
            j = Math.max(0, Math.max(k, m));
        }
        ClipboardManager localClipboardManager = (ClipboardManager)this.mContext.getSystemService("clipboard");
        ClipData localClipData = localClipboardManager.getPrimaryClip();
        switch (paramView.getId())
        {
        default:
        case 101384217:
        case 101384218:
        case 101384215:
        case 101384216:
        case 101384213:
        case 101384214:
        }
        while (true)
        {
            return;
            if ((this.mSelectionControllerEnabled) && (startTextSelectionMode()))
            {
                getSelectionController().show();
                continue;
                if (this.mSelectionControllerEnabled)
                {
                    Selection.setSelection((Spannable)this.mTextView.getText(), 0, this.mTextView.length());
                    if (startTextSelectionMode())
                    {
                        getSelectionController().show();
                        continue;
                        CharSequence localCharSequence = null;
                        if (localClipData.getItemCount() > 0)
                            localCharSequence = localClipData.getItemAt(0).coerceToText(this.mContext);
                        if ((localCharSequence != null) && (localCharSequence.length() > 0))
                        {
                            Selection.setSelection((Spannable)this.mTextView.getText(), j);
                            this.mTextView.replaceText_internal(i, j, localCharSequence);
                            stopTextSelectionMode();
                            continue;
                            if (localClipData != null)
                            {
                                PastePanelOnClickListener localPastePanelOnClickListener = new PastePanelOnClickListener(paramMiuiCursorController, i, j);
                                ((MiuiCursorController.InsertionPointCursorController)paramMiuiCursorController).setupClipBoardPanel(localClipData, localPastePanelOnClickListener);
                                continue;
                                ((MiuiCursorController.SelectionModifierCursorController)paramMiuiCursorController).addClipData(localClipboardManager, localClipData, this.mTextView.getTransformedText(i, j));
                                stopTextSelectionMode();
                                Toast.makeText(this.mContext, this.mContext.getResources().getString(17040278), 0).show();
                                continue;
                                ((MiuiCursorController.SelectionModifierCursorController)paramMiuiCursorController).addClipData(localClipboardManager, localClipData, this.mTextView.getTransformedText(i, j));
                                stopTextSelectionMode();
                                this.mTextView.deleteText_internal(i, j);
                                Toast.makeText(this.mContext, this.mContext.getResources().getString(17040278), 0).show();
                            }
                        }
                    }
                }
            }
        }
    }

    boolean hasInsertionController()
    {
        return this.mInsertionControllerEnabled;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean hasMagnifierController()
    {
        if ((MagnifierController.isMagnifierEnabled(this.mContext)) && (this.mInsertionControllerEnabled) && (this.mSelectionControllerEnabled));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean hasSelectionController()
    {
        return this.mSelectionControllerEnabled;
    }

    void hideControllers()
    {
        hideCursorControllers();
        hideSpanControllers();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void hideInsertionPointCursorControllerWrap()
    {
        hideInsertionPointCursorController();
    }

    void invalidateTextDisplayList()
    {
        if (this.mTextDisplayLists != null)
            for (int i = 0; i < this.mTextDisplayLists.length; i++)
                if (this.mTextDisplayLists[i] != null)
                    this.mTextDisplayLists[i].invalidate();
    }

    void invalidateTextDisplayList(Layout paramLayout, int paramInt1, int paramInt2)
    {
        int j;
        int[] arrayOfInt1;
        int[] arrayOfInt2;
        int k;
        int m;
        if ((this.mTextDisplayLists != null) && ((paramLayout instanceof DynamicLayout)))
        {
            int i = paramLayout.getLineForOffset(paramInt1);
            j = paramLayout.getLineForOffset(paramInt2);
            DynamicLayout localDynamicLayout = (DynamicLayout)paramLayout;
            arrayOfInt1 = localDynamicLayout.getBlockEndLines();
            arrayOfInt2 = localDynamicLayout.getBlockIndices();
            k = localDynamicLayout.getNumberOfBlocks();
            m = 0;
            if ((m < k) && (arrayOfInt1[m] < i))
                break label117;
        }
        while (true)
        {
            if (m < k)
            {
                int n = arrayOfInt2[m];
                if (n != -1)
                    this.mTextDisplayLists[n].invalidate();
                if (arrayOfInt1[m] < j);
            }
            else
            {
                return;
                label117: m++;
                break;
            }
            m++;
        }
    }

    boolean isCursorVisible()
    {
        if ((this.mCursorVisible) && (this.mTextView.isTextEditable()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean isOffsetVisibleWrap(int paramInt)
    {
        return isOffsetVisible(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean isPositionVisibleWrap(int paramInt1, int paramInt2)
    {
        return isPositionVisible(paramInt1, paramInt2);
    }

    void makeBlink()
    {
        if (shouldBlink())
        {
            this.mShowCursor = SystemClock.uptimeMillis();
            if (this.mBlink == null)
                this.mBlink = new Blink(null);
            this.mBlink.removeCallbacks(this.mBlink);
            this.mBlink.postAtTime(this.mBlink, 500L + this.mShowCursor);
        }
        while (true)
        {
            return;
            if (this.mBlink != null)
                this.mBlink.removeCallbacks(this.mBlink);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean needInsertPanel()
    {
        if ((!this.mIsInTextSelectionMode) && ((canSelectText()) || (this.mTextView.canPaste())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void onAttachedToWindow()
    {
        if (this.mShowErrorAfterAttach)
        {
            showError();
            this.mShowErrorAfterAttach = false;
        }
        this.mTemporaryDetach = false;
        ViewTreeObserver localViewTreeObserver = this.mTextView.getViewTreeObserver();
        if (this.mInsertionPointCursorController != null)
            localViewTreeObserver.addOnTouchModeChangeListener(this.mInsertionPointCursorController);
        if (this.mSelectionModifierCursorController != null)
        {
            this.mSelectionModifierCursorController.resetTouchOffsets();
            localViewTreeObserver.addOnTouchModeChangeListener(this.mSelectionModifierCursorController);
        }
        updateSpellCheckSpans(0, this.mTextView.getText().length(), true);
        if ((this.mTextView.hasTransientState()) && (this.mTextView.getSelectionStart() != this.mTextView.getSelectionEnd()))
        {
            this.mTextView.setHasTransientState(false);
            startSelectionActionMode();
        }
    }

    public void onCommitCorrection(CorrectionInfo paramCorrectionInfo)
    {
        if (this.mCorrectionHighlighter == null)
            this.mCorrectionHighlighter = new CorrectionHighlighter();
        while (true)
        {
            this.mCorrectionHighlighter.highlight(paramCorrectionInfo);
            return;
            this.mCorrectionHighlighter.invalidate(false);
        }
    }

    void onDetachedFromWindow()
    {
        if (this.mError != null)
            hideError();
        if (this.mBlink != null)
            this.mBlink.removeCallbacks(this.mBlink);
        if (this.mInsertionPointCursorController != null)
            this.mInsertionPointCursorController.onDetached();
        if (this.mSelectionModifierCursorController != null)
            this.mSelectionModifierCursorController.onDetached();
        if (this.mShowSuggestionRunnable != null)
            this.mTextView.removeCallbacks(this.mShowSuggestionRunnable);
        invalidateTextDisplayList();
        if (this.mSpellChecker != null)
        {
            this.mSpellChecker.closeSession();
            this.mSpellChecker = null;
        }
        this.mPreserveDetachedSelection = true;
        hideControllers();
        this.mPreserveDetachedSelection = false;
        this.mTemporaryDetach = false;
    }

    void onDraw(Canvas paramCanvas, Layout paramLayout, Path paramPath, Paint paramPaint, int paramInt)
    {
        int i = this.mTextView.getSelectionStart();
        int j = this.mTextView.getSelectionEnd();
        InputMethodState localInputMethodState = this.mInputMethodState;
        if ((localInputMethodState != null) && (localInputMethodState.mBatchEditNesting == 0))
        {
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if (localInputMethodManager != null)
            {
                if (localInputMethodManager.isActive(this.mTextView))
                {
                    boolean bool = false;
                    if ((localInputMethodState.mContentChanged) || (localInputMethodState.mSelectionModeChanged))
                        bool = reportExtractedText();
                    if ((!bool) && (paramPath != null))
                    {
                        int k = -1;
                        int m = -1;
                        if ((this.mTextView.getText() instanceof Spannable))
                        {
                            Spannable localSpannable = (Spannable)this.mTextView.getText();
                            k = EditableInputConnection.getComposingSpanStart(localSpannable);
                            m = EditableInputConnection.getComposingSpanEnd(localSpannable);
                        }
                        localInputMethodManager.updateSelection(this.mTextView, i, j, k, m);
                    }
                }
                if ((localInputMethodManager.isWatchingCursor(this.mTextView)) && (paramPath != null))
                {
                    RectF localRectF = localInputMethodState.mTmpRectF;
                    paramPath.computeBounds(localRectF, true);
                    float[] arrayOfFloat = localInputMethodState.mTmpOffset;
                    localInputMethodState.mTmpOffset[1] = 0.0F;
                    arrayOfFloat[0] = 0.0F;
                    paramCanvas.getMatrix().mapPoints(localInputMethodState.mTmpOffset);
                    localInputMethodState.mTmpRectF.offset(localInputMethodState.mTmpOffset[0], localInputMethodState.mTmpOffset[1]);
                    localInputMethodState.mTmpRectF.offset(0.0F, paramInt);
                    localInputMethodState.mCursorRectInWindow.set((int)(0.5D + localInputMethodState.mTmpRectF.left), (int)(0.5D + localInputMethodState.mTmpRectF.top), (int)(0.5D + localInputMethodState.mTmpRectF.right), (int)(0.5D + localInputMethodState.mTmpRectF.bottom));
                    localInputMethodManager.updateCursor(this.mTextView, localInputMethodState.mCursorRectInWindow.left, localInputMethodState.mCursorRectInWindow.top, localInputMethodState.mCursorRectInWindow.right, localInputMethodState.mCursorRectInWindow.bottom);
                }
            }
        }
        if (this.mCorrectionHighlighter != null)
            this.mCorrectionHighlighter.draw(paramCanvas, paramInt);
        if ((paramPath != null) && (i == j) && (this.mCursorCount > 0))
        {
            drawCursor(paramCanvas, paramInt);
            paramPath = null;
        }
        if ((this.mTextView.canHaveDisplayList()) && (paramCanvas.isHardwareAccelerated()))
            drawHardwareAccelerated(paramCanvas, paramLayout, paramPath, paramPaint, paramInt);
        while (true)
        {
            return;
            paramLayout.draw(paramCanvas, paramPath, paramPaint, paramInt);
        }
    }

    void onDrop(DragEvent paramDragEvent)
    {
        StringBuilder localStringBuilder = new StringBuilder("");
        ClipData localClipData = paramDragEvent.getClipData();
        int i = localClipData.getItemCount();
        for (int j = 0; j < i; j++)
            localStringBuilder.append(localClipData.getItemAt(j).coerceToStyledText(this.mTextView.getContext()));
        int k = this.mTextView.getOffsetForPosition(paramDragEvent.getX(), paramDragEvent.getY());
        Object localObject = paramDragEvent.getLocalState();
        DragLocalState localDragLocalState = null;
        if ((localObject instanceof DragLocalState))
            localDragLocalState = (DragLocalState)localObject;
        int m;
        if ((localDragLocalState != null) && (localDragLocalState.sourceTextView == this.mTextView))
        {
            m = 1;
            if ((m == 0) || (k < localDragLocalState.start) || (k >= localDragLocalState.end))
                break label152;
        }
        label152: int i3;
        CharSequence localCharSequence;
        do
        {
            int n;
            int i2;
            do
            {
                return;
                m = 0;
                break;
                n = this.mTextView.getText().length();
                long l = this.mTextView.prepareSpacesAroundPaste(k, k, localStringBuilder);
                int i1 = TextUtils.unpackRangeStartFromLong(l);
                i2 = TextUtils.unpackRangeEndFromLong(l);
                Selection.setSelection((Spannable)this.mTextView.getText(), i2);
                this.mTextView.replaceText_internal(i1, i2, localStringBuilder);
            }
            while (m == 0);
            i3 = localDragLocalState.start;
            int i4 = localDragLocalState.end;
            if (i2 <= i3)
            {
                int i7 = this.mTextView.getText().length() - n;
                i3 += i7;
                i4 += i7;
            }
            this.mTextView.deleteText_internal(i3, i4);
            localCharSequence = this.mTextView.getTransformedText(i3 - 1, i3 + 1);
        }
        while (((i3 != 0) && (!Character.isSpaceChar(localCharSequence.charAt(0)))) || ((i3 != this.mTextView.getText().length()) && (!Character.isSpaceChar(localCharSequence.charAt(1)))));
        if (i3 == this.mTextView.getText().length());
        for (int i5 = i3 - 1; ; i5 = i3)
        {
            TextView localTextView = this.mTextView;
            int i6 = i5 + 1;
            localTextView.deleteText_internal(i5, i6);
            break;
        }
    }

    void onFocusChanged(boolean paramBoolean, int paramInt)
    {
        this.mShowCursor = SystemClock.uptimeMillis();
        ensureEndedBatchEdit();
        int n;
        boolean bool;
        if (paramBoolean)
        {
            int k = this.mTextView.getSelectionStart();
            int m = this.mTextView.getSelectionEnd();
            if ((this.mSelectAllOnFocus) && (k == 0) && (m == this.mTextView.getText().length()))
            {
                n = 1;
                if ((!this.mFrozenWithFocus) || (!this.mTextView.hasSelection()) || (n != 0))
                    break label271;
                bool = true;
                label90: this.mCreatedWithASelection = bool;
                if ((!this.mFrozenWithFocus) || (k < 0) || (m < 0))
                {
                    int i1 = getLastTapPosition();
                    if (i1 >= 0)
                        Selection.setSelection((Spannable)this.mTextView.getText(), i1);
                    MovementMethod localMovementMethod = this.mTextView.getMovementMethod();
                    if (localMovementMethod != null)
                        localMovementMethod.onTakeFocus(this.mTextView, (Spannable)this.mTextView.getText(), paramInt);
                    if ((((this.mTextView instanceof ExtractEditText)) || (this.mSelectionMoved)) && (k >= 0) && (m >= 0))
                        Selection.setSelection((Spannable)this.mTextView.getText(), k, m);
                    if (this.mSelectAllOnFocus)
                        this.mTextView.selectAllText();
                    this.mTouchFocusSelected = true;
                }
                this.mFrozenWithFocus = false;
                this.mSelectionMoved = false;
                if (this.mError != null)
                    showError();
                makeBlink();
            }
        }
        label392: 
        while (true)
        {
            return;
            n = 0;
            break;
            label271: bool = false;
            break label90;
            if (this.mError != null)
                hideError();
            this.mTextView.onEndBatchEdit();
            if ((this.mTextView instanceof ExtractEditText))
            {
                int i = this.mTextView.getSelectionStart();
                int j = this.mTextView.getSelectionEnd();
                hideControllers();
                Selection.setSelection((Spannable)this.mTextView.getText(), i, j);
            }
            while (true)
            {
                if (this.mSelectionModifierCursorController == null)
                    break label392;
                this.mSelectionModifierCursorController.resetTouchOffsets();
                break;
                if (this.mTemporaryDetach)
                    this.mPreserveDetachedSelection = true;
                hideControllers();
                if (this.mTemporaryDetach)
                    this.mPreserveDetachedSelection = false;
                downgradeEasyCorrectionSpans();
            }
        }
    }

    void onLocaleChanged()
    {
        this.mWordIterator = null;
    }

    void onScreenStateChanged(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 1:
        case 0:
        }
        while (true)
        {
            return;
            resumeBlink();
            continue;
            suspendBlink();
        }
    }

    void onScrollChanged()
    {
        if (this.mPositionListener != null)
            this.mPositionListener.onScrollChanged();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void onTapUpEvent()
    {
        if (!this.mDiscardNextActionUp)
            stopTextSelectionMode();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if ((hasSelectionController()) && (getSelectionController().onTouchEvent(paramMotionEvent)))
            bool = true;
        while (true)
        {
            return bool;
            if (this.mShowSuggestionRunnable != null)
            {
                this.mTextView.removeCallbacks(this.mShowSuggestionRunnable);
                this.mShowSuggestionRunnable = null;
            }
            if (paramMotionEvent.getActionMasked() == 0)
            {
                this.mLastDownPositionX = paramMotionEvent.getX();
                this.mLastDownPositionY = paramMotionEvent.getY();
                this.mTouchFocusSelected = false;
                this.mIgnoreActionUpEvent = false;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    void onTouchUpEvent(MotionEvent paramMotionEvent)
    {
        int i;
        if ((this.mSelectAllOnFocus) && (this.mTextView.didTouchFocusSelect()))
        {
            i = 1;
            hideControllers();
            CharSequence localCharSequence = this.mTextView.getText();
            if ((i == 0) && (localCharSequence.length() >= 0))
            {
                int j = this.mTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
                Selection.setSelection((Spannable)localCharSequence, j);
                if (this.mSpellChecker != null)
                    this.mSpellChecker.onSelectionChanged();
                if (!extractedTextModeWillBeStarted())
                {
                    if (!isCursorInsideEasyCorrectionSpan())
                        break label132;
                    this.mShowSuggestionRunnable = new Runnable()
                    {
                        public void run()
                        {
                            Editor.this.showSuggestions();
                        }
                    };
                    this.mTextView.postDelayed(this.mShowSuggestionRunnable, ViewConfiguration.getDoubleTapTimeout());
                }
            }
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label132: if (hasInsertionController())
                getInsertionController().show();
        }
    }

    void onWindowFocusChanged(boolean paramBoolean)
    {
        if (paramBoolean)
            if (this.mBlink != null)
            {
                this.mBlink.uncancel();
                makeBlink();
            }
        while (true)
        {
            return;
            if (this.mBlink != null)
                this.mBlink.cancel();
            if (this.mInputContentType != null)
                this.mInputContentType.enterDown = false;
            hideControllers();
            if (this.mSuggestionsPopupWindow != null)
                this.mSuggestionsPopupWindow.onParentLostFocus();
            this.mTextView.onEndBatchEdit();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public boolean performLongClick(boolean paramBoolean)
    {
        boolean bool = false;
        int i = 0;
        if (hasMagnifierController())
        {
            Log.i("MiuiMagnifierController", "performLongClick to show MagnifierController");
            stopTextSelectionMode();
            getSelectionController().hide();
            getInsertionController().hide();
            getMagnifierController().show();
            getInsertionController().show();
            this.mDiscardNextActionUp = true;
            bool = true;
        }
        if (!bool)
        {
            if ((!this.mIsInTextSelectionMode) || (!touchPositionIsInSelection()))
                break label108;
            stopTextSelectionMode();
            bool = true;
        }
        while (true)
        {
            if (i != 0)
                this.mTextView.performHapticFeedback(0);
            if (bool)
                this.mDiscardNextActionUp = true;
            return bool;
            label108: stopTextSelectionMode();
            bool = startTextSelectionMode();
            i = bool;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    void prepareCursorControllers()
    {
        boolean bool1 = true;
        int i = 0;
        ViewGroup.LayoutParams localLayoutParams = this.mTextView.getRootView().getLayoutParams();
        int j;
        label69: boolean bool2;
        if ((localLayoutParams instanceof WindowManager.LayoutParams))
        {
            WindowManager.LayoutParams localLayoutParams1 = (WindowManager.LayoutParams)localLayoutParams;
            if ((localLayoutParams1.type < 1000) || (localLayoutParams1.type > 1999))
                i = bool1;
        }
        else
        {
            if ((i == 0) || (this.mTextView.getLayout() == null))
                break label188;
            j = bool1;
            if ((j == 0) || (!isCursorVisible()))
                break label194;
            bool2 = bool1;
            label84: this.mInsertionControllerEnabled = bool2;
            if ((j == 0) || (!this.mTextView.textCanBeSelected()))
                break label200;
        }
        while (true)
        {
            this.mSelectionControllerEnabled = bool1;
            if (!this.mInsertionControllerEnabled)
            {
                hideInsertionPointCursorController();
                if (this.mInsertionPointCursorController != null)
                {
                    this.mInsertionPointCursorController.onDetached();
                    this.mInsertionPointCursorController = null;
                }
            }
            if (!this.mSelectionControllerEnabled)
            {
                stopSelectionActionMode();
                if (this.mSelectionModifierCursorController != null)
                {
                    this.mSelectionModifierCursorController.onDetached();
                    this.mSelectionModifierCursorController = null;
                }
            }
            if (!hasMagnifierController())
                this.mMagnifierController = null;
            return;
            i = 0;
            break;
            label188: j = 0;
            break label69;
            label194: bool2 = false;
            break label84;
            label200: bool1 = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void removePositionListenerSubscriber(TextViewPositionListener paramTextViewPositionListener)
    {
        getPositionListener().removeSubscriber(paramTextViewPositionListener);
    }

    boolean reportExtractedText()
    {
        InputMethodState localInputMethodState = this.mInputMethodState;
        if (localInputMethodState != null)
        {
            boolean bool2 = localInputMethodState.mContentChanged;
            if ((bool2) || (localInputMethodState.mSelectionModeChanged))
            {
                localInputMethodState.mContentChanged = false;
                localInputMethodState.mSelectionModeChanged = false;
                ExtractedTextRequest localExtractedTextRequest = localInputMethodState.mExtractedTextRequest;
                if (localExtractedTextRequest != null)
                {
                    InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                    if (localInputMethodManager != null)
                    {
                        if ((localInputMethodState.mChangedStart < 0) && (!bool2))
                            localInputMethodState.mChangedStart = -2;
                        if (extractTextInternal(localExtractedTextRequest, localInputMethodState.mChangedStart, localInputMethodState.mChangedEnd, localInputMethodState.mChangedDelta, localInputMethodState.mExtractedText))
                        {
                            localInputMethodManager.updateExtractedText(this.mTextView, localExtractedTextRequest.token, localInputMethodState.mExtractedText);
                            localInputMethodState.mChangedStart = -1;
                            localInputMethodState.mChangedEnd = -1;
                            localInputMethodState.mChangedDelta = 0;
                            localInputMethodState.mContentChanged = false;
                        }
                    }
                }
            }
        }
        for (boolean bool1 = true; ; bool1 = false)
            return bool1;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean selectAllWrap()
    {
        return this.mTextView.selectAllText();
    }

    void sendOnTextChanged(int paramInt1, int paramInt2)
    {
        updateSpellCheckSpans(paramInt1, paramInt1 + paramInt2, false);
        hideCursorControllers();
    }

    public void setError(CharSequence paramCharSequence, Drawable paramDrawable)
    {
        this.mError = TextUtils.stringOrSpannedString(paramCharSequence);
        this.mErrorWasChanged = true;
        TextView.Drawables localDrawables = this.mTextView.mDrawables;
        if (localDrawables != null)
            switch (this.mTextView.getResolvedLayoutDirection())
            {
            default:
                this.mTextView.setCompoundDrawables(localDrawables.mDrawableLeft, localDrawables.mDrawableTop, paramDrawable, localDrawables.mDrawableBottom);
                if (this.mError == null)
                    if (this.mErrorPopup != null)
                    {
                        if (this.mErrorPopup.isShowing())
                            this.mErrorPopup.dismiss();
                        this.mErrorPopup = null;
                    }
                break;
            case 1:
            }
        while (true)
        {
            return;
            this.mTextView.setCompoundDrawables(paramDrawable, localDrawables.mDrawableTop, localDrawables.mDrawableRight, localDrawables.mDrawableBottom);
            break;
            this.mTextView.setCompoundDrawables(null, null, paramDrawable, null);
            break;
            if (this.mTextView.isFocused())
                showError();
        }
    }

    void setFrame()
    {
        if (this.mErrorPopup != null)
        {
            TextView localTextView = (TextView)this.mErrorPopup.getContentView();
            chooseSize(this.mErrorPopup, this.mError, localTextView);
            this.mErrorPopup.update(this.mTextView, getErrorX(), getErrorY(), this.mErrorPopup.getWidth(), this.mErrorPopup.getHeight());
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setTextSelection(int paramInt)
    {
        Spannable localSpannable = (Spannable)this.mTextView.getText();
        int i = paramInt;
        int j = paramInt;
        int k = EditableInputConnection.getComposingSpanStart(localSpannable);
        int m = EditableInputConnection.getComposingSpanEnd(localSpannable);
        if ((k >= 0) && (m >= 0) && (k != m))
        {
            j = Math.max(k, m);
            i = j;
        }
        Selection.setSelection((Spannable)this.mTextView.getText(), i, j);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setTextSelectionWrap(int paramInt1, int paramInt2)
    {
        Selection.setSelection((Spannable)this.mTextView.getText(), paramInt1, paramInt2);
    }

    void showSuggestions()
    {
        if (this.mSuggestionsPopupWindow == null)
            this.mSuggestionsPopupWindow = new SuggestionsPopupWindow();
        hideControllers();
        this.mSuggestionsPopupWindow.show();
    }

    boolean startSelectionActionMode()
    {
        boolean bool1;
        if (this.mSelectionActionMode != null)
            bool1 = false;
        label160: 
        while (true)
        {
            return bool1;
            if ((!canSelectText()) || (!this.mTextView.requestFocus()))
            {
                Log.w("TextView", "TextView does not support text selection. Action mode cancelled.");
                bool1 = false;
            }
            else if ((!this.mTextView.hasSelection()) && (!selectCurrentWord()))
            {
                bool1 = false;
            }
            else
            {
                boolean bool2 = extractedTextModeWillBeStarted();
                if (!bool2)
                {
                    SelectionActionModeCallback localSelectionActionModeCallback = new SelectionActionModeCallback(null);
                    this.mSelectionActionMode = this.mTextView.startActionMode(localSelectionActionModeCallback);
                }
                if ((this.mSelectionActionMode != null) || (bool2));
                for (bool1 = true; ; bool1 = false)
                {
                    if ((!bool1) || (this.mTextView.isTextSelectable()) || (!this.mShowSoftInputOnFocus))
                        break label160;
                    InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                    if (localInputMethodManager == null)
                        break;
                    localInputMethodManager.showSoftInput(this.mTextView, 0, null);
                    break;
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean startTextSelectionMode()
    {
        boolean bool = false;
        if (!this.mIsInTextSelectionMode)
        {
            if (hasSelectionController())
                break label27;
            Log.w("Editor", "TextView has no selection controller. Selection mode cancelled.");
        }
        while (true)
        {
            return bool;
            label27: if ((canSelectText()) && (this.mTextView.requestFocus()))
                if ((!this.mTextView.hasSelection()) && (!getSelectionController().selectCurrentRange()))
                {
                    Log.i("Editor", "can not selection current range");
                }
                else
                {
                    getSelectionController().show();
                    this.mIsInTextSelectionMode = true;
                    bool = true;
                }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void startTextSelectionModeIfDouleTap(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if ((SystemClock.uptimeMillis() - paramLong <= ViewConfiguration.getDoubleTapTimeout()) && (isPositionOnText(paramFloat1, paramFloat2)))
        {
            float f1 = paramFloat1 - paramFloat3;
            float f2 = paramFloat2 - paramFloat4;
            float f3 = f1 * f1 + f2 * f2;
            int i = ViewConfiguration.get(this.mTextView.getContext()).getScaledTouchSlop();
            if ((f3 < i * i) && (startTextSelectionMode()))
                this.mDiscardNextActionUp = true;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void stopBlink()
    {
        if (this.mBlink != null)
            this.mBlink.removeCallbacks(this.mBlink);
    }

    protected void stopSelectionActionMode()
    {
        if (this.mSelectionActionMode != null)
            this.mSelectionActionMode.finish();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void stopTextSelectionMode()
    {
        if (this.mIsInTextSelectionMode)
        {
            Selection.setSelection((Spannable)this.mTextView.getText(), this.mTextView.getSelectionEnd());
            if (this.mSelectionModifierCursorController != null)
                this.mSelectionModifierCursorController.hide();
            this.mIsInTextSelectionMode = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    TextView textview()
    {
        return this.mTextView;
    }

    void updateCursorsPositions()
    {
        if (this.mTextView.mCursorDrawableRes == 0)
        {
            this.mCursorCount = 0;
            return;
        }
        Layout localLayout1 = this.mTextView.getLayout();
        Layout localLayout2 = this.mTextView.getHintLayout();
        int i = this.mTextView.getSelectionStart();
        int j = localLayout1.getLineForOffset(i);
        int k = localLayout1.getLineTop(j);
        int m = localLayout1.getLineTop(j + 1);
        if (localLayout1.isLevelBoundary(i));
        for (int n = 2; ; n = 1)
        {
            this.mCursorCount = n;
            int i1 = m;
            if (this.mCursorCount == 2)
                i1 = k + m >> 1;
            updateCursorPosition(0, k, i1, getPrimaryHorizontal(localLayout1, localLayout2, i));
            if (this.mCursorCount != 2)
                break;
            updateCursorPosition(1, i1, m, localLayout1.getSecondaryHorizontal(i));
            break;
        }
    }

    private class PastePanelOnClickListener
        implements View.OnClickListener
    {
        private MiuiCursorController mController;
        private int mMax;
        private int mMin;

        PastePanelOnClickListener(MiuiCursorController paramInt1, int paramInt2, int arg4)
        {
            this.mController = paramInt1;
            this.mMin = paramInt2;
            int i;
            this.mMax = i;
        }

        public void onClick(View paramView)
        {
            TextView localTextView = (TextView)paramView;
            Editor.this.mTextView.replaceText_internal(this.mMin, this.mMax, localTextView.getText());
            Editor.this.stopTextSelectionMode();
            ((MiuiCursorController.InsertionPointCursorController)this.mController).onClipBoardPancelClick();
        }
    }

    static class InputMethodState
    {
        int mBatchEditNesting;
        int mChangedDelta;
        int mChangedEnd;
        int mChangedStart;
        boolean mContentChanged;
        boolean mCursorChanged;
        Rect mCursorRectInWindow = new Rect();
        final ExtractedText mExtractedText = new ExtractedText();
        ExtractedTextRequest mExtractedTextRequest;
        boolean mSelectionModeChanged;
        float[] mTmpOffset = new float[2];
        RectF mTmpRectF = new RectF();
    }

    static class InputContentType
    {
        boolean enterDown;
        Bundle extras;
        int imeActionId;
        CharSequence imeActionLabel;
        int imeOptions = 0;
        TextView.OnEditorActionListener onEditorActionListener;
        String privateImeOptions;
    }

    private static class ErrorPopup extends PopupWindow
    {
        private boolean mAbove = false;
        private int mPopupInlineErrorAboveBackgroundId = 0;
        private int mPopupInlineErrorBackgroundId = 0;
        private final TextView mView;

        ErrorPopup(TextView paramTextView, int paramInt1, int paramInt2)
        {
            super(paramInt1, paramInt2);
            this.mView = paramTextView;
            this.mPopupInlineErrorBackgroundId = getResourceId(this.mPopupInlineErrorBackgroundId, 218);
            this.mView.setBackgroundResource(this.mPopupInlineErrorBackgroundId);
        }

        private int getResourceId(int paramInt1, int paramInt2)
        {
            if (paramInt1 == 0)
            {
                TypedArray localTypedArray = this.mView.getContext().obtainStyledAttributes(android.R.styleable.Theme);
                paramInt1 = localTypedArray.getResourceId(paramInt2, 0);
                localTypedArray.recycle();
            }
            return paramInt1;
        }

        void fixDirection(boolean paramBoolean)
        {
            this.mAbove = paramBoolean;
            TextView localTextView;
            if (paramBoolean)
            {
                this.mPopupInlineErrorAboveBackgroundId = getResourceId(this.mPopupInlineErrorAboveBackgroundId, 219);
                localTextView = this.mView;
                if (!paramBoolean)
                    break label62;
            }
            label62: for (int i = this.mPopupInlineErrorAboveBackgroundId; ; i = this.mPopupInlineErrorBackgroundId)
            {
                localTextView.setBackgroundResource(i);
                return;
                this.mPopupInlineErrorBackgroundId = getResourceId(this.mPopupInlineErrorBackgroundId, 218);
                break;
            }
        }

        public void update(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
        {
            super.update(paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean);
            boolean bool = isAboveAnchor();
            if (bool != this.mAbove)
                fixDirection(bool);
        }
    }

    private class CorrectionHighlighter
    {
        private static final int FADE_OUT_DURATION = 400;
        private int mEnd;
        private long mFadingStartTime;
        private final Paint mPaint = new Paint(1);
        private final Path mPath = new Path();
        private int mStart;
        private RectF mTempRectF;

        public CorrectionHighlighter()
        {
            this.mPaint.setCompatibilityScaling(Editor.this.mTextView.getResources().getCompatibilityInfo().applicationScale);
            this.mPaint.setStyle(Paint.Style.FILL);
        }

        private void invalidate(boolean paramBoolean)
        {
            if (Editor.this.mTextView.getLayout() == null);
            while (true)
            {
                return;
                if (this.mTempRectF == null)
                    this.mTempRectF = new RectF();
                this.mPath.computeBounds(this.mTempRectF, false);
                int i = Editor.this.mTextView.getCompoundPaddingLeft();
                int j = Editor.this.mTextView.getExtendedPaddingTop() + Editor.this.mTextView.getVerticalOffset(true);
                if (paramBoolean)
                    Editor.this.mTextView.postInvalidateOnAnimation(i + (int)this.mTempRectF.left, j + (int)this.mTempRectF.top, i + (int)this.mTempRectF.right, j + (int)this.mTempRectF.bottom);
                else
                    Editor.this.mTextView.postInvalidate((int)this.mTempRectF.left, (int)this.mTempRectF.top, (int)this.mTempRectF.right, (int)this.mTempRectF.bottom);
            }
        }

        private void stopAnimation()
        {
            Editor.this.mCorrectionHighlighter = null;
        }

        private boolean updatePaint()
        {
            long l = SystemClock.uptimeMillis() - this.mFadingStartTime;
            if (l > 400L);
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                float f = 1.0F - (float)l / 400.0F;
                int i = Color.alpha(Editor.this.mTextView.mHighlightColor);
                int j = (0xFFFFFF & Editor.this.mTextView.mHighlightColor) + ((int)(f * i) << 24);
                this.mPaint.setColor(j);
            }
        }

        private boolean updatePath()
        {
            Layout localLayout = Editor.this.mTextView.getLayout();
            if (localLayout == null);
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                int i = Editor.this.mTextView.getText().length();
                int j = Math.min(i, this.mStart);
                int k = Math.min(i, this.mEnd);
                this.mPath.reset();
                localLayout.getSelectionPath(j, k, this.mPath);
            }
        }

        public void draw(Canvas paramCanvas, int paramInt)
        {
            if ((updatePath()) && (updatePaint()))
            {
                if (paramInt != 0)
                    paramCanvas.translate(0.0F, paramInt);
                paramCanvas.drawPath(this.mPath, this.mPaint);
                if (paramInt != 0)
                    paramCanvas.translate(0.0F, -paramInt);
                invalidate(true);
            }
            while (true)
            {
                return;
                stopAnimation();
                invalidate(false);
            }
        }

        public void highlight(CorrectionInfo paramCorrectionInfo)
        {
            this.mStart = paramCorrectionInfo.getOffset();
            this.mEnd = (this.mStart + paramCorrectionInfo.getNewText().length());
            this.mFadingStartTime = SystemClock.uptimeMillis();
            if ((this.mStart < 0) || (this.mEnd < 0))
                stopAnimation();
        }
    }

    class SelectionModifierCursorController
        implements Editor.CursorController
    {
        private static final int DELAY_BEFORE_REPLACE_ACTION = 200;
        private float mDownPositionX;
        private float mDownPositionY;
        private Editor.SelectionEndHandleView mEndHandle;
        private boolean mGestureStayedInTapRegion;
        private int mMaxTouchOffset;
        private int mMinTouchOffset;
        private long mPreviousTapUpTime = 0L;
        private Editor.SelectionStartHandleView mStartHandle;

        SelectionModifierCursorController()
        {
            resetTouchOffsets();
        }

        private void initDrawables()
        {
            if (Editor.this.mSelectHandleLeft == null)
                Editor.access$2502(Editor.this, Editor.this.mTextView.getContext().getResources().getDrawable(Editor.this.mTextView.mTextSelectHandleLeftRes));
            if (Editor.this.mSelectHandleRight == null)
                Editor.access$2602(Editor.this, Editor.this.mTextView.getContext().getResources().getDrawable(Editor.this.mTextView.mTextSelectHandleRightRes));
        }

        private void initHandles()
        {
            if (this.mStartHandle == null)
                this.mStartHandle = new Editor.SelectionStartHandleView(Editor.this, Editor.this.mSelectHandleLeft, Editor.this.mSelectHandleRight);
            if (this.mEndHandle == null)
                this.mEndHandle = new Editor.SelectionEndHandleView(Editor.this, Editor.this.mSelectHandleRight, Editor.this.mSelectHandleLeft);
            this.mStartHandle.show();
            this.mEndHandle.show();
            this.mStartHandle.showActionPopupWindow(200);
            this.mEndHandle.setActionPopupWindow(this.mStartHandle.getActionPopupWindow());
            Editor.this.hideInsertionPointCursorController();
        }

        private void updateMinAndMaxOffsets(MotionEvent paramMotionEvent)
        {
            int i = paramMotionEvent.getPointerCount();
            for (int j = 0; j < i; j++)
            {
                int k = Editor.this.mTextView.getOffsetForPosition(paramMotionEvent.getX(j), paramMotionEvent.getY(j));
                if (k < this.mMinTouchOffset)
                    this.mMinTouchOffset = k;
                if (k > this.mMaxTouchOffset)
                    this.mMaxTouchOffset = k;
            }
        }

        public int getMaxTouchOffset()
        {
            return this.mMaxTouchOffset;
        }

        public int getMinTouchOffset()
        {
            return this.mMinTouchOffset;
        }

        public void hide()
        {
            if (this.mStartHandle != null)
                this.mStartHandle.hide();
            if (this.mEndHandle != null)
                this.mEndHandle.hide();
        }

        public boolean isSelectionStartDragged()
        {
            if ((this.mStartHandle != null) && (this.mStartHandle.isDragging()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onDetached()
        {
            Editor.this.mTextView.getViewTreeObserver().removeOnTouchModeChangeListener(this);
            if (this.mStartHandle != null)
                this.mStartHandle.onDetached();
            if (this.mEndHandle != null)
                this.mEndHandle.onDetached();
        }

        public void onTouchEvent(MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            case 3:
            case 4:
            default:
            case 0:
            case 5:
            case 6:
            case 2:
            case 1:
            }
            while (true)
            {
                return;
                float f4 = paramMotionEvent.getX();
                float f5 = paramMotionEvent.getY();
                int j = Editor.this.mTextView.getOffsetForPosition(f4, f5);
                this.mMaxTouchOffset = j;
                this.mMinTouchOffset = j;
                if ((this.mGestureStayedInTapRegion) && (SystemClock.uptimeMillis() - this.mPreviousTapUpTime <= ViewConfiguration.getDoubleTapTimeout()))
                {
                    float f6 = f4 - this.mDownPositionX;
                    float f7 = f5 - this.mDownPositionY;
                    float f8 = f6 * f6 + f7 * f7;
                    int k = ViewConfiguration.get(Editor.this.mTextView.getContext()).getScaledDoubleTapSlop();
                    if (f8 >= k * k)
                        break label231;
                }
                label231: for (int m = 1; ; m = 0)
                {
                    if ((m != 0) && (Editor.this.isPositionOnText(f4, f5)))
                    {
                        Editor.this.startSelectionActionMode();
                        Editor.this.mDiscardNextActionUp = true;
                    }
                    this.mDownPositionX = f4;
                    this.mDownPositionY = f5;
                    this.mGestureStayedInTapRegion = true;
                    break;
                }
                if (Editor.this.mTextView.getContext().getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch.distinct"))
                {
                    updateMinAndMaxOffsets(paramMotionEvent);
                    continue;
                    if (this.mGestureStayedInTapRegion)
                    {
                        float f1 = paramMotionEvent.getX() - this.mDownPositionX;
                        float f2 = paramMotionEvent.getY() - this.mDownPositionY;
                        float f3 = f1 * f1 + f2 * f2;
                        int i = ViewConfiguration.get(Editor.this.mTextView.getContext()).getScaledDoubleTapTouchSlop();
                        if (f3 > i * i)
                        {
                            this.mGestureStayedInTapRegion = false;
                            continue;
                            this.mPreviousTapUpTime = SystemClock.uptimeMillis();
                        }
                    }
                }
            }
        }

        public void onTouchModeChanged(boolean paramBoolean)
        {
            if (!paramBoolean)
                hide();
        }

        public void resetTouchOffsets()
        {
            this.mMaxTouchOffset = -1;
            this.mMinTouchOffset = -1;
        }

        public void show()
        {
            if (Editor.this.mTextView.isInBatchEditMode());
            while (true)
            {
                return;
                initDrawables();
                initHandles();
                Editor.this.hideInsertionPointCursorController();
            }
        }
    }

    private class InsertionPointCursorController
        implements Editor.CursorController
    {
        private Editor.InsertionHandleView mHandle;

        private InsertionPointCursorController()
        {
        }

        private Editor.InsertionHandleView getHandle()
        {
            if (Editor.this.mSelectHandleCenter == null)
                Editor.access$2302(Editor.this, Editor.this.mTextView.getResources().getDrawable(Editor.this.mTextView.mTextSelectHandleRes));
            if (this.mHandle == null)
                this.mHandle = new Editor.InsertionHandleView(Editor.this, Editor.this.mSelectHandleCenter);
            return this.mHandle;
        }

        public void hide()
        {
            if (this.mHandle != null)
                this.mHandle.hide();
        }

        public void onDetached()
        {
            Editor.this.mTextView.getViewTreeObserver().removeOnTouchModeChangeListener(this);
            if (this.mHandle != null)
                this.mHandle.onDetached();
        }

        public void onTouchModeChanged(boolean paramBoolean)
        {
            if (!paramBoolean)
                hide();
        }

        public void show()
        {
            getHandle().show();
        }

        public void showWithActionPopup()
        {
            getHandle().showWithActionPopup();
        }
    }

    private static abstract interface CursorController extends ViewTreeObserver.OnTouchModeChangeListener
    {
        public abstract void hide();

        public abstract void onDetached();

        public abstract void show();
    }

    private class SelectionEndHandleView extends Editor.HandleView
    {
        public SelectionEndHandleView(Drawable paramDrawable1, Drawable arg3)
        {
            super(paramDrawable1, localDrawable);
        }

        public int getCurrentCursorOffset()
        {
            return Editor.this.mTextView.getSelectionEnd();
        }

        protected int getHotspotX(Drawable paramDrawable, boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = 3 * paramDrawable.getIntrinsicWidth() / 4; ; i = paramDrawable.getIntrinsicWidth() / 4)
                return i;
        }

        public void setActionPopupWindow(Editor.ActionPopupWindow paramActionPopupWindow)
        {
            this.mActionPopupWindow = paramActionPopupWindow;
        }

        public void updatePosition(float paramFloat1, float paramFloat2)
        {
            int i = Editor.this.mTextView.getOffsetForPosition(paramFloat1, paramFloat2);
            int j = Editor.this.mTextView.getSelectionStart();
            if (i <= j)
                i = Math.min(j + 1, Editor.this.mTextView.getText().length());
            positionAtCursorOffset(i, false);
        }

        public void updateSelection(int paramInt)
        {
            Selection.setSelection((Spannable)Editor.this.mTextView.getText(), Editor.this.mTextView.getSelectionStart(), paramInt);
            updateDrawable();
        }
    }

    private class SelectionStartHandleView extends Editor.HandleView
    {
        public SelectionStartHandleView(Drawable paramDrawable1, Drawable arg3)
        {
            super(paramDrawable1, localDrawable);
        }

        public Editor.ActionPopupWindow getActionPopupWindow()
        {
            return this.mActionPopupWindow;
        }

        public int getCurrentCursorOffset()
        {
            return Editor.this.mTextView.getSelectionStart();
        }

        protected int getHotspotX(Drawable paramDrawable, boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = paramDrawable.getIntrinsicWidth() / 4; ; i = 3 * paramDrawable.getIntrinsicWidth() / 4)
                return i;
        }

        public void updatePosition(float paramFloat1, float paramFloat2)
        {
            int i = Editor.this.mTextView.getOffsetForPosition(paramFloat1, paramFloat2);
            int j = Editor.this.mTextView.getSelectionEnd();
            if (i >= j)
                i = Math.max(0, j - 1);
            positionAtCursorOffset(i, false);
        }

        public void updateSelection(int paramInt)
        {
            Selection.setSelection((Spannable)Editor.this.mTextView.getText(), paramInt, Editor.this.mTextView.getSelectionEnd());
            updateDrawable();
        }
    }

    private class InsertionHandleView extends Editor.HandleView
    {
        private static final int DELAY_BEFORE_HANDLE_FADES_OUT = 4000;
        private static final int RECENT_CUT_COPY_DURATION = 15000;
        private float mDownPositionX;
        private float mDownPositionY;
        private Runnable mHider;

        public InsertionHandleView(Drawable arg2)
        {
            super(localDrawable, localDrawable);
        }

        private void hideAfterDelay()
        {
            if (this.mHider == null)
                this.mHider = new Runnable()
                {
                    public void run()
                    {
                        Editor.InsertionHandleView.this.hide();
                    }
                };
            while (true)
            {
                Editor.this.mTextView.postDelayed(this.mHider, 4000L);
                return;
                removeHiderCallback();
            }
        }

        private void removeHiderCallback()
        {
            if (this.mHider != null)
                Editor.this.mTextView.removeCallbacks(this.mHider);
        }

        public int getCurrentCursorOffset()
        {
            return Editor.this.mTextView.getSelectionStart();
        }

        protected int getHotspotX(Drawable paramDrawable, boolean paramBoolean)
        {
            return paramDrawable.getIntrinsicWidth() / 2;
        }

        public void onDetached()
        {
            super.onDetached();
            removeHiderCallback();
        }

        void onHandleMoved()
        {
            super.onHandleMoved();
            removeHiderCallback();
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            boolean bool = super.onTouchEvent(paramMotionEvent);
            switch (paramMotionEvent.getActionMasked())
            {
            case 2:
            default:
            case 0:
            case 1:
            case 3:
            }
            while (true)
            {
                return bool;
                this.mDownPositionX = paramMotionEvent.getRawX();
                this.mDownPositionY = paramMotionEvent.getRawY();
                continue;
                if (!offsetHasBeenChanged())
                {
                    float f1 = this.mDownPositionX - paramMotionEvent.getRawX();
                    float f2 = this.mDownPositionY - paramMotionEvent.getRawY();
                    float f3 = f1 * f1 + f2 * f2;
                    int i = ViewConfiguration.get(Editor.this.mTextView.getContext()).getScaledTouchSlop();
                    if (f3 < i * i)
                    {
                        if ((this.mActionPopupWindow == null) || (!this.mActionPopupWindow.isShowing()))
                            break label161;
                        this.mActionPopupWindow.hide();
                    }
                }
                while (true)
                {
                    hideAfterDelay();
                    break;
                    label161: showWithActionPopup();
                }
                hideAfterDelay();
            }
        }

        public void show()
        {
            super.show();
            if (SystemClock.uptimeMillis() - TextView.LAST_CUT_OR_COPY_TIME < 15000L)
                showActionPopupWindow(0);
            hideAfterDelay();
        }

        public void showWithActionPopup()
        {
            show();
            showActionPopupWindow(0);
        }

        public void updatePosition(float paramFloat1, float paramFloat2)
        {
            positionAtCursorOffset(Editor.this.mTextView.getOffsetForPosition(paramFloat1, paramFloat2), false);
        }

        public void updateSelection(int paramInt)
        {
            Selection.setSelection((Spannable)Editor.this.mTextView.getText(), paramInt);
        }
    }

    private abstract class HandleView extends View
        implements Editor.TextViewPositionListener
    {
        private static final int HISTORY_SIZE = 5;
        private static final int TOUCH_UP_FILTER_DELAY_AFTER = 150;
        private static final int TOUCH_UP_FILTER_DELAY_BEFORE = 350;
        private Runnable mActionPopupShower;
        protected Editor.ActionPopupWindow mActionPopupWindow;
        private final PopupWindow mContainer = new PopupWindow(Editor.this.mTextView.getContext(), null, 16843464);
        protected Drawable mDrawable;
        protected Drawable mDrawableLtr;
        protected Drawable mDrawableRtl;
        protected int mHotspotX;
        private float mIdealVerticalOffset;
        private boolean mIsDragging;
        private int mLastParentX;
        private int mLastParentY;
        private int mNumberPreviousOffsets = 0;
        private boolean mPositionHasChanged = true;
        private int mPositionX;
        private int mPositionY;
        private int mPreviousOffset = -1;
        private int mPreviousOffsetIndex = 0;
        private final int[] mPreviousOffsets = new int[5];
        private final long[] mPreviousOffsetsTimes = new long[5];
        private float mTouchOffsetY;
        private float mTouchToWindowOffsetX;
        private float mTouchToWindowOffsetY;

        public HandleView(Drawable paramDrawable1, Drawable arg3)
        {
            super();
            this.mContainer.setSplitTouchEnabled(true);
            this.mContainer.setClippingEnabled(false);
            this.mContainer.setWindowLayoutType(1002);
            this.mContainer.setContentView(this);
            this.mDrawableLtr = paramDrawable1;
            Object localObject;
            this.mDrawableRtl = localObject;
            updateDrawable();
            int i = this.mDrawable.getIntrinsicHeight();
            this.mTouchOffsetY = (-0.3F * i);
            this.mIdealVerticalOffset = (0.7F * i);
        }

        private void addPositionToTouchUpFilter(int paramInt)
        {
            this.mPreviousOffsetIndex = ((1 + this.mPreviousOffsetIndex) % 5);
            this.mPreviousOffsets[this.mPreviousOffsetIndex] = paramInt;
            this.mPreviousOffsetsTimes[this.mPreviousOffsetIndex] = SystemClock.uptimeMillis();
            this.mNumberPreviousOffsets = (1 + this.mNumberPreviousOffsets);
        }

        private void filterOnTouchUp()
        {
            long l = SystemClock.uptimeMillis();
            int i = 0;
            int j = this.mPreviousOffsetIndex;
            int k = Math.min(this.mNumberPreviousOffsets, 5);
            while ((i < k) && (l - this.mPreviousOffsetsTimes[j] < 150L))
            {
                i++;
                j = (5 + (this.mPreviousOffsetIndex - i)) % 5;
            }
            if ((i > 0) && (i < k) && (l - this.mPreviousOffsetsTimes[j] > 350L))
                positionAtCursorOffset(this.mPreviousOffsets[j], false);
        }

        private boolean isVisible()
        {
            boolean bool;
            if (this.mIsDragging)
                bool = true;
            while (true)
            {
                return bool;
                if (Editor.this.mTextView.isInBatchEditMode())
                    bool = false;
                else
                    bool = Editor.this.isPositionVisible(this.mPositionX + this.mHotspotX, this.mPositionY);
            }
        }

        private void startTouchUpFilter(int paramInt)
        {
            this.mNumberPreviousOffsets = 0;
            addPositionToTouchUpFilter(paramInt);
        }

        protected void dismiss()
        {
            this.mIsDragging = false;
            this.mContainer.dismiss();
            onDetached();
        }

        public abstract int getCurrentCursorOffset();

        protected abstract int getHotspotX(Drawable paramDrawable, boolean paramBoolean);

        public void hide()
        {
            dismiss();
            Editor.this.getPositionListener().removeSubscriber(this);
        }

        protected void hideActionPopupWindow()
        {
            if (this.mActionPopupShower != null)
                Editor.this.mTextView.removeCallbacks(this.mActionPopupShower);
            if (this.mActionPopupWindow != null)
                this.mActionPopupWindow.hide();
        }

        public boolean isDragging()
        {
            return this.mIsDragging;
        }

        public boolean isShowing()
        {
            return this.mContainer.isShowing();
        }

        public boolean offsetHasBeenChanged()
        {
            int i = 1;
            if (this.mNumberPreviousOffsets > i);
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        public void onDetached()
        {
            hideActionPopupWindow();
        }

        protected void onDraw(Canvas paramCanvas)
        {
            this.mDrawable.setBounds(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
            this.mDrawable.draw(paramCanvas);
        }

        void onHandleMoved()
        {
            hideActionPopupWindow();
        }

        protected void onMeasure(int paramInt1, int paramInt2)
        {
            setMeasuredDimension(this.mDrawable.getIntrinsicWidth(), this.mDrawable.getIntrinsicHeight());
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            default:
            case 0:
            case 2:
            case 1:
            case 3:
            }
            while (true)
            {
                return true;
                startTouchUpFilter(getCurrentCursorOffset());
                this.mTouchToWindowOffsetX = (paramMotionEvent.getRawX() - this.mPositionX);
                this.mTouchToWindowOffsetY = (paramMotionEvent.getRawY() - this.mPositionY);
                Editor.PositionListener localPositionListener = Editor.this.getPositionListener();
                this.mLastParentX = localPositionListener.getPositionX();
                this.mLastParentY = localPositionListener.getPositionY();
                this.mIsDragging = true;
                continue;
                float f1 = paramMotionEvent.getRawX();
                float f2 = paramMotionEvent.getRawY();
                float f3 = this.mTouchToWindowOffsetY - this.mLastParentY;
                float f4 = f2 - this.mPositionY - this.mLastParentY;
                if (f3 < this.mIdealVerticalOffset);
                for (float f5 = Math.max(Math.min(f4, this.mIdealVerticalOffset), f3); ; f5 = Math.min(Math.max(f4, this.mIdealVerticalOffset), f3))
                {
                    this.mTouchToWindowOffsetY = (f5 + this.mLastParentY);
                    updatePosition(f1 - this.mTouchToWindowOffsetX + this.mHotspotX, f2 - this.mTouchToWindowOffsetY + this.mTouchOffsetY);
                    break;
                }
                filterOnTouchUp();
                this.mIsDragging = false;
                continue;
                this.mIsDragging = false;
            }
        }

        protected void positionAtCursorOffset(int paramInt, boolean paramBoolean)
        {
            Layout localLayout = Editor.this.mTextView.getLayout();
            if (localLayout == null)
                Editor.this.prepareCursorControllers();
            label150: 
            while (true)
            {
                return;
                if (paramInt != this.mPreviousOffset);
                for (int i = 1; ; i = 0)
                {
                    if ((i == 0) && (!paramBoolean))
                        break label150;
                    if (i != 0)
                    {
                        updateSelection(paramInt);
                        addPositionToTouchUpFilter(paramInt);
                    }
                    int j = localLayout.getLineForOffset(paramInt);
                    this.mPositionX = ((int)(localLayout.getPrimaryHorizontal(paramInt) - 0.5F - this.mHotspotX));
                    this.mPositionY = localLayout.getLineBottom(j);
                    this.mPositionX += Editor.this.mTextView.viewportToContentHorizontalOffset();
                    this.mPositionY += Editor.this.mTextView.viewportToContentVerticalOffset();
                    this.mPreviousOffset = paramInt;
                    this.mPositionHasChanged = true;
                    break;
                }
            }
        }

        public void show()
        {
            if (isShowing());
            while (true)
            {
                return;
                Editor.this.getPositionListener().addSubscriber(this, true);
                this.mPreviousOffset = -1;
                positionAtCursorOffset(getCurrentCursorOffset(), false);
                hideActionPopupWindow();
            }
        }

        void showActionPopupWindow(int paramInt)
        {
            if (this.mActionPopupWindow == null)
                this.mActionPopupWindow = new Editor.ActionPopupWindow(Editor.this, null);
            if (this.mActionPopupShower == null)
                this.mActionPopupShower = new Runnable()
                {
                    public void run()
                    {
                        Editor.HandleView.this.mActionPopupWindow.show();
                    }
                };
            while (true)
            {
                Editor.this.mTextView.postDelayed(this.mActionPopupShower, paramInt);
                return;
                Editor.this.mTextView.removeCallbacks(this.mActionPopupShower);
            }
        }

        protected void updateDrawable()
        {
            int i = getCurrentCursorOffset();
            boolean bool = Editor.this.mTextView.getLayout().isRtlCharAt(i);
            if (bool);
            for (Drawable localDrawable = this.mDrawableRtl; ; localDrawable = this.mDrawableLtr)
            {
                this.mDrawable = localDrawable;
                this.mHotspotX = getHotspotX(this.mDrawable, bool);
                return;
            }
        }

        public abstract void updatePosition(float paramFloat1, float paramFloat2);

        public void updatePosition(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
        {
            positionAtCursorOffset(getCurrentCursorOffset(), paramBoolean2);
            int i;
            int j;
            if ((paramBoolean1) || (this.mPositionHasChanged))
            {
                if (this.mIsDragging)
                {
                    if ((paramInt1 != this.mLastParentX) || (paramInt2 != this.mLastParentY))
                    {
                        this.mTouchToWindowOffsetX += paramInt1 - this.mLastParentX;
                        this.mTouchToWindowOffsetY += paramInt2 - this.mLastParentY;
                        this.mLastParentX = paramInt1;
                        this.mLastParentY = paramInt2;
                    }
                    onHandleMoved();
                }
                if (!isVisible())
                    break label163;
                i = paramInt1 + this.mPositionX;
                j = paramInt2 + this.mPositionY;
                if (!isShowing())
                    break label141;
                this.mContainer.update(i, j, -1, -1);
            }
            while (true)
            {
                this.mPositionHasChanged = false;
                return;
                label141: this.mContainer.showAtLocation(Editor.this.mTextView, 0, i, j);
                continue;
                label163: if (isShowing())
                    dismiss();
            }
        }

        protected abstract void updateSelection(int paramInt);
    }

    private class ActionPopupWindow extends Editor.PinnedPopupWindow
        implements View.OnClickListener
    {
        private static final int POPUP_TEXT_LAYOUT = 17367220;
        private TextView mPasteTextView;
        private TextView mReplaceTextView;

        private ActionPopupWindow()
        {
            super();
        }

        protected int clipVertically(int paramInt)
        {
            if (paramInt < 0)
            {
                int i = getTextOffset();
                Layout localLayout = Editor.this.mTextView.getLayout();
                int j = localLayout.getLineForOffset(i);
                paramInt = paramInt + (localLayout.getLineBottom(j) - localLayout.getLineTop(j)) + this.mContentView.getMeasuredHeight() + Editor.this.mTextView.getResources().getDrawable(Editor.this.mTextView.mTextSelectHandleRes).getIntrinsicHeight();
            }
            return paramInt;
        }

        protected void createPopupWindow()
        {
            this.mPopupWindow = new PopupWindow(Editor.this.mTextView.getContext(), null, 16843464);
            this.mPopupWindow.setClippingEnabled(true);
        }

        protected int getTextOffset()
        {
            return (Editor.this.mTextView.getSelectionStart() + Editor.this.mTextView.getSelectionEnd()) / 2;
        }

        protected int getVerticalLocalPosition(int paramInt)
        {
            return Editor.this.mTextView.getLayout().getLineTop(paramInt) - this.mContentView.getMeasuredHeight();
        }

        protected void initContentView()
        {
            LinearLayout localLinearLayout = new LinearLayout(Editor.this.mTextView.getContext());
            localLinearLayout.setOrientation(0);
            this.mContentView = localLinearLayout;
            this.mContentView.setBackgroundResource(17302967);
            LayoutInflater localLayoutInflater = (LayoutInflater)Editor.this.mTextView.getContext().getSystemService("layout_inflater");
            ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-2, -2);
            this.mPasteTextView = ((TextView)localLayoutInflater.inflate(17367220, null));
            this.mPasteTextView.setLayoutParams(localLayoutParams);
            this.mContentView.addView(this.mPasteTextView);
            this.mPasteTextView.setText(17039371);
            this.mPasteTextView.setOnClickListener(this);
            this.mReplaceTextView = ((TextView)localLayoutInflater.inflate(17367220, null));
            this.mReplaceTextView.setLayoutParams(localLayoutParams);
            this.mContentView.addView(this.mReplaceTextView);
            this.mReplaceTextView.setText(17040317);
            this.mReplaceTextView.setOnClickListener(this);
        }

        public void onClick(View paramView)
        {
            if ((paramView == this.mPasteTextView) && (Editor.this.mTextView.canPaste()))
            {
                Editor.this.mTextView.onTextContextMenuItem(16908322);
                hide();
            }
            while (true)
            {
                return;
                if (paramView == this.mReplaceTextView)
                {
                    int i = (Editor.this.mTextView.getSelectionStart() + Editor.this.mTextView.getSelectionEnd()) / 2;
                    Editor.this.stopSelectionActionMode();
                    Selection.setSelection((Spannable)Editor.this.mTextView.getText(), i);
                    Editor.this.showSuggestions();
                }
            }
        }

        public void show()
        {
            int i = 0;
            boolean bool = Editor.this.mTextView.canPaste();
            int j;
            int k;
            if ((Editor.this.mTextView.isSuggestionsEnabled()) && (Editor.this.isCursorInsideSuggestionSpan()))
            {
                j = 1;
                TextView localTextView1 = this.mPasteTextView;
                if (!bool)
                    break label88;
                k = 0;
                label51: localTextView1.setVisibility(k);
                TextView localTextView2 = this.mReplaceTextView;
                if (j == 0)
                    break label95;
                label68: localTextView2.setVisibility(i);
                if ((bool) || (j != 0))
                    break label101;
            }
            while (true)
            {
                return;
                j = 0;
                break;
                label88: k = 8;
                break label51;
                label95: i = 8;
                break label68;
                label101: super.show();
            }
        }
    }

    private class SelectionActionModeCallback
        implements ActionMode.Callback
    {
        private SelectionActionModeCallback()
        {
        }

        public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
        {
            if ((Editor.this.mCustomSelectionActionModeCallback != null) && (Editor.this.mCustomSelectionActionModeCallback.onActionItemClicked(paramActionMode, paramMenuItem)));
            for (boolean bool = true; ; bool = Editor.this.mTextView.onTextContextMenuItem(paramMenuItem.getItemId()))
                return bool;
        }

        public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            boolean bool1 = false;
            TypedArray localTypedArray = Editor.this.mTextView.getContext().obtainStyledAttributes(com.android.internal.R.styleable.SelectionModeDrawables);
            boolean bool2 = Editor.this.mTextView.getContext().getResources().getBoolean(17891382);
            paramActionMode.setTitle(Editor.this.mTextView.getContext().getString(17040319));
            paramActionMode.setSubtitle(null);
            paramActionMode.setTitleOptionalHint(true);
            int i = 0;
            if (!bool2)
                i = localTypedArray.getResourceId(3, 0);
            paramMenu.add(0, 16908319, 0, 17039373).setIcon(i).setAlphabeticShortcut('a').setShowAsAction(6);
            if (Editor.this.mTextView.canCut())
                paramMenu.add(0, 16908320, 0, 17039363).setIcon(localTypedArray.getResourceId(0, 0)).setAlphabeticShortcut('x').setShowAsAction(6);
            if (Editor.this.mTextView.canCopy())
                paramMenu.add(0, 16908321, 0, 17039361).setIcon(localTypedArray.getResourceId(1, 0)).setAlphabeticShortcut('c').setShowAsAction(6);
            if (Editor.this.mTextView.canPaste())
                paramMenu.add(0, 16908322, 0, 17039371).setIcon(localTypedArray.getResourceId(2, 0)).setAlphabeticShortcut('v').setShowAsAction(6);
            localTypedArray.recycle();
            if ((Editor.this.mCustomSelectionActionModeCallback != null) && (!Editor.this.mCustomSelectionActionModeCallback.onCreateActionMode(paramActionMode, paramMenu)));
            while (true)
            {
                return bool1;
                if ((paramMenu.hasVisibleItems()) || (paramActionMode.getCustomView() != null))
                {
                    Editor.this.getSelectionController().show();
                    Editor.this.mTextView.setHasTransientState(true);
                    bool1 = true;
                }
            }
        }

        public void onDestroyActionMode(ActionMode paramActionMode)
        {
            if (Editor.this.mCustomSelectionActionModeCallback != null)
                Editor.this.mCustomSelectionActionModeCallback.onDestroyActionMode(paramActionMode);
            if (!Editor.this.mPreserveDetachedSelection)
            {
                Selection.setSelection((Spannable)Editor.this.mTextView.getText(), Editor.this.mTextView.getSelectionEnd());
                Editor.this.mTextView.setHasTransientState(false);
            }
            if (Editor.this.mSelectionModifierCursorController != null)
                Editor.this.mSelectionModifierCursorController.hide();
            Editor.this.mSelectionActionMode = null;
        }

        public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            if (Editor.this.mCustomSelectionActionModeCallback != null);
            for (boolean bool = Editor.this.mCustomSelectionActionModeCallback.onPrepareActionMode(paramActionMode, paramMenu); ; bool = true)
                return bool;
        }
    }

    private class SuggestionsPopupWindow extends Editor.PinnedPopupWindow
        implements AdapterView.OnItemClickListener
    {
        private static final int ADD_TO_DICTIONARY = -1;
        private static final int DELETE_TEXT = -2;
        private static final int MAX_NUMBER_SUGGESTIONS = 5;
        private boolean mCursorWasVisibleBeforeSuggestions = Editor.this.mCursorVisible;
        private boolean mIsShowingUp = false;
        private int mNumberOfSuggestions;
        private final HashMap<SuggestionSpan, Integer> mSpansLengths = new HashMap();
        private SuggestionInfo[] mSuggestionInfos;
        private final Comparator<SuggestionSpan> mSuggestionSpanComparator = new SuggestionSpanComparator(null);
        private SuggestionAdapter mSuggestionsAdapter;

        public SuggestionsPopupWindow()
        {
            super();
        }

        private SuggestionSpan[] getSuggestionSpans()
        {
            int i = Editor.this.mTextView.getSelectionStart();
            Spannable localSpannable = (Spannable)Editor.this.mTextView.getText();
            SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])localSpannable.getSpans(i, i, SuggestionSpan.class);
            this.mSpansLengths.clear();
            int j = arrayOfSuggestionSpan.length;
            for (int k = 0; k < j; k++)
            {
                SuggestionSpan localSuggestionSpan = arrayOfSuggestionSpan[k];
                int m = localSpannable.getSpanStart(localSuggestionSpan);
                int n = localSpannable.getSpanEnd(localSuggestionSpan);
                this.mSpansLengths.put(localSuggestionSpan, Integer.valueOf(n - m));
            }
            Arrays.sort(arrayOfSuggestionSpan, this.mSuggestionSpanComparator);
            return arrayOfSuggestionSpan;
        }

        private void highlightTextDifferences(SuggestionInfo paramSuggestionInfo, int paramInt1, int paramInt2)
        {
            Spannable localSpannable = (Spannable)Editor.this.mTextView.getText();
            int i = localSpannable.getSpanStart(paramSuggestionInfo.suggestionSpan);
            int j = localSpannable.getSpanEnd(paramSuggestionInfo.suggestionSpan);
            paramSuggestionInfo.suggestionStart = (i - paramInt1);
            paramSuggestionInfo.suggestionEnd = (paramSuggestionInfo.suggestionStart + paramSuggestionInfo.text.length());
            paramSuggestionInfo.text.setSpan(paramSuggestionInfo.highlightSpan, 0, paramSuggestionInfo.text.length(), 33);
            String str = localSpannable.toString();
            paramSuggestionInfo.text.insert(0, str.substring(paramInt1, i));
            paramSuggestionInfo.text.append(str.substring(j, paramInt2));
        }

        private boolean updateSuggestions()
        {
            Spannable localSpannable = (Spannable)Editor.this.mTextView.getText();
            SuggestionSpan[] arrayOfSuggestionSpan = getSuggestionSpans();
            int i = arrayOfSuggestionSpan.length;
            boolean bool;
            if (i == 0)
            {
                bool = false;
                return bool;
            }
            this.mNumberOfSuggestions = 0;
            int j = Editor.this.mTextView.getText().length();
            int k = 0;
            Object localObject = null;
            int m = 0;
            int n = 0;
            if (n < i)
            {
                SuggestionSpan localSuggestionSpan1 = arrayOfSuggestionSpan[n];
                int i5 = localSpannable.getSpanStart(localSuggestionSpan1);
                int i6 = localSpannable.getSpanEnd(localSuggestionSpan1);
                j = Math.min(i5, j);
                k = Math.max(i6, k);
                if ((0x2 & localSuggestionSpan1.getFlags()) != 0)
                    localObject = localSuggestionSpan1;
                if (n == 0)
                    m = localSuggestionSpan1.getUnderlineColor();
                String[] arrayOfString = localSuggestionSpan1.getSuggestions();
                int i7 = arrayOfString.length;
                label339: for (int i8 = 0; ; i8++)
                {
                    String str;
                    int i9;
                    if (i8 < i7)
                    {
                        str = arrayOfString[i8];
                        i9 = 0;
                    }
                    for (int i10 = 0; ; i10++)
                        if (i10 < this.mNumberOfSuggestions)
                        {
                            if (this.mSuggestionInfos[i10].text.toString().equals(str))
                            {
                                SuggestionSpan localSuggestionSpan2 = this.mSuggestionInfos[i10].suggestionSpan;
                                int i11 = localSpannable.getSpanStart(localSuggestionSpan2);
                                int i12 = localSpannable.getSpanEnd(localSuggestionSpan2);
                                if ((i5 == i11) && (i6 == i12))
                                    i9 = 1;
                            }
                        }
                        else
                        {
                            if (i9 != 0)
                                break label339;
                            SuggestionInfo localSuggestionInfo3 = this.mSuggestionInfos[this.mNumberOfSuggestions];
                            localSuggestionInfo3.suggestionSpan = localSuggestionSpan1;
                            localSuggestionInfo3.suggestionIndex = i8;
                            localSuggestionInfo3.text.replace(0, localSuggestionInfo3.text.length(), str);
                            this.mNumberOfSuggestions = (1 + this.mNumberOfSuggestions);
                            if (this.mNumberOfSuggestions != 5)
                                break label339;
                            n = i;
                            n++;
                            break;
                        }
                }
            }
            for (int i1 = 0; i1 < this.mNumberOfSuggestions; i1++)
                highlightTextDifferences(this.mSuggestionInfos[i1], j, k);
            if (localObject != null)
            {
                int i3 = localSpannable.getSpanStart(localObject);
                int i4 = localSpannable.getSpanEnd(localObject);
                if ((i3 >= 0) && (i4 > i3))
                {
                    SuggestionInfo localSuggestionInfo2 = this.mSuggestionInfos[this.mNumberOfSuggestions];
                    localSuggestionInfo2.suggestionSpan = localObject;
                    localSuggestionInfo2.suggestionIndex = -1;
                    localSuggestionInfo2.text.replace(0, localSuggestionInfo2.text.length(), Editor.this.mTextView.getContext().getString(17040320));
                    localSuggestionInfo2.text.setSpan(localSuggestionInfo2.highlightSpan, 0, 0, 33);
                    this.mNumberOfSuggestions = (1 + this.mNumberOfSuggestions);
                }
            }
            SuggestionInfo localSuggestionInfo1 = this.mSuggestionInfos[this.mNumberOfSuggestions];
            localSuggestionInfo1.suggestionSpan = null;
            localSuggestionInfo1.suggestionIndex = -2;
            localSuggestionInfo1.text.replace(0, localSuggestionInfo1.text.length(), Editor.this.mTextView.getContext().getString(17040321));
            localSuggestionInfo1.text.setSpan(localSuggestionInfo1.highlightSpan, 0, 0, 33);
            this.mNumberOfSuggestions = (1 + this.mNumberOfSuggestions);
            if (Editor.this.mSuggestionRangeSpan == null)
                Editor.this.mSuggestionRangeSpan = new SuggestionRangeSpan();
            if (m == 0)
                Editor.this.mSuggestionRangeSpan.setBackgroundColor(Editor.this.mTextView.mHighlightColor);
            while (true)
            {
                localSpannable.setSpan(Editor.this.mSuggestionRangeSpan, j, k, 33);
                this.mSuggestionsAdapter.notifyDataSetChanged();
                bool = true;
                break;
                int i2 = (int)(0.4F * Color.alpha(m));
                Editor.this.mSuggestionRangeSpan.setBackgroundColor((0xFFFFFF & m) + (i2 << 24));
            }
        }

        protected int clipVertically(int paramInt)
        {
            int i = this.mContentView.getMeasuredHeight();
            return Math.min(paramInt, Editor.this.mTextView.getResources().getDisplayMetrics().heightPixels - i);
        }

        protected void createPopupWindow()
        {
            this.mPopupWindow = new CustomPopupWindow(Editor.this.mTextView.getContext(), 16843635);
            this.mPopupWindow.setInputMethodMode(2);
            this.mPopupWindow.setFocusable(true);
            this.mPopupWindow.setClippingEnabled(false);
        }

        protected int getTextOffset()
        {
            return Editor.this.mTextView.getSelectionStart();
        }

        protected int getVerticalLocalPosition(int paramInt)
        {
            return Editor.this.mTextView.getLayout().getLineBottom(paramInt);
        }

        public void hide()
        {
            super.hide();
        }

        protected void initContentView()
        {
            ListView localListView = new ListView(Editor.this.mTextView.getContext());
            this.mSuggestionsAdapter = new SuggestionAdapter(null);
            localListView.setAdapter(this.mSuggestionsAdapter);
            localListView.setOnItemClickListener(this);
            this.mContentView = localListView;
            this.mSuggestionInfos = new SuggestionInfo[7];
            for (int i = 0; i < this.mSuggestionInfos.length; i++)
                this.mSuggestionInfos[i] = new SuggestionInfo(null);
        }

        public boolean isShowingUp()
        {
            return this.mIsShowingUp;
        }

        protected void measureContent()
        {
            DisplayMetrics localDisplayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
            int i = View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.widthPixels, -2147483648);
            int j = View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.heightPixels, -2147483648);
            int k = 0;
            View localView = null;
            for (int m = 0; m < this.mNumberOfSuggestions; m++)
            {
                localView = this.mSuggestionsAdapter.getView(m, localView, this.mContentView);
                localView.getLayoutParams().width = -2;
                localView.measure(i, j);
                k = Math.max(k, localView.getMeasuredWidth());
            }
            this.mContentView.measure(View.MeasureSpec.makeMeasureSpec(k, 1073741824), j);
            Drawable localDrawable = this.mPopupWindow.getBackground();
            if (localDrawable != null)
            {
                if (Editor.this.mTempRect == null)
                    Editor.access$1802(Editor.this, new Rect());
                localDrawable.getPadding(Editor.this.mTempRect);
                k += Editor.this.mTempRect.left + Editor.this.mTempRect.right;
            }
            this.mPopupWindow.setWidth(k);
        }

        public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
        {
            Editable localEditable = (Editable)Editor.this.mTextView.getText();
            SuggestionInfo localSuggestionInfo = this.mSuggestionInfos[paramInt];
            if (localSuggestionInfo.suggestionIndex == -2)
            {
                int i6 = localEditable.getSpanStart(Editor.this.mSuggestionRangeSpan);
                int i7 = localEditable.getSpanEnd(Editor.this.mSuggestionRangeSpan);
                if ((i6 >= 0) && (i7 > i6))
                {
                    if ((i7 < localEditable.length()) && (Character.isSpaceChar(localEditable.charAt(i7))) && ((i6 == 0) || (Character.isSpaceChar(localEditable.charAt(i6 - 1)))))
                        i7++;
                    Editor.this.mTextView.deleteText_internal(i6, i7);
                }
                hide();
            }
            int i;
            int j;
            while (true)
            {
                return;
                i = localEditable.getSpanStart(localSuggestionInfo.suggestionSpan);
                j = localEditable.getSpanEnd(localSuggestionInfo.suggestionSpan);
                if ((i >= 0) && (j > i))
                    break;
                hide();
            }
            String str1 = localEditable.toString().substring(i, j);
            if (localSuggestionInfo.suggestionIndex == -1)
            {
                Intent localIntent = new Intent("com.android.settings.USER_DICTIONARY_INSERT");
                localIntent.putExtra("word", str1);
                localIntent.putExtra("locale", Editor.this.mTextView.getTextServicesLocale().toString());
                localIntent.setFlags(0x10000000 | localIntent.getFlags());
                Editor.this.mTextView.getContext().startActivity(localIntent);
                localEditable.removeSpan(localSuggestionInfo.suggestionSpan);
                Selection.setSelection(localEditable, j);
                Editor.this.updateSpellCheckSpans(i, j, false);
            }
            while (true)
            {
                hide();
                break;
                SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])localEditable.getSpans(i, j, SuggestionSpan.class);
                int k = arrayOfSuggestionSpan.length;
                int[] arrayOfInt1 = new int[k];
                int[] arrayOfInt2 = new int[k];
                int[] arrayOfInt3 = new int[k];
                for (int m = 0; m < k; m++)
                {
                    SuggestionSpan localSuggestionSpan = arrayOfSuggestionSpan[m];
                    arrayOfInt1[m] = localEditable.getSpanStart(localSuggestionSpan);
                    arrayOfInt2[m] = localEditable.getSpanEnd(localSuggestionSpan);
                    arrayOfInt3[m] = localEditable.getSpanFlags(localSuggestionSpan);
                    int i5 = localSuggestionSpan.getFlags();
                    if ((i5 & 0x2) > 0)
                        localSuggestionSpan.setFlags(0xFFFFFFFE & (i5 & 0xFFFFFFFD));
                }
                int n = localSuggestionInfo.suggestionStart;
                int i1 = localSuggestionInfo.suggestionEnd;
                String str2 = localSuggestionInfo.text.subSequence(n, i1).toString();
                Editor.this.mTextView.replaceText_internal(i, j, str2);
                if (!TextUtils.isEmpty(localSuggestionInfo.suggestionSpan.getNotificationTargetClassName()))
                {
                    InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                    if (localInputMethodManager != null)
                        localInputMethodManager.notifySuggestionPicked(localSuggestionInfo.suggestionSpan, str1, localSuggestionInfo.suggestionIndex);
                }
                localSuggestionInfo.suggestionSpan.getSuggestions()[localSuggestionInfo.suggestionIndex] = str1;
                int i2 = str2.length() - (j - i);
                for (int i3 = 0; i3 < k; i3++)
                    if ((arrayOfInt1[i3] <= i) && (arrayOfInt2[i3] >= j))
                        Editor.this.mTextView.setSpan_internal(arrayOfSuggestionSpan[i3], arrayOfInt1[i3], i2 + arrayOfInt2[i3], arrayOfInt3[i3]);
                int i4 = j + i2;
                Editor.this.mTextView.setCursorPosition_internal(i4, i4);
            }
        }

        public void onParentLostFocus()
        {
            this.mIsShowingUp = false;
        }

        public void show()
        {
            if (!(Editor.this.mTextView.getText() instanceof Editable));
            while (true)
            {
                return;
                if (updateSuggestions())
                {
                    this.mCursorWasVisibleBeforeSuggestions = Editor.this.mCursorVisible;
                    Editor.this.mTextView.setCursorVisible(false);
                    this.mIsShowingUp = true;
                    super.show();
                }
            }
        }

        private class SuggestionSpanComparator
            implements Comparator<SuggestionSpan>
        {
            private SuggestionSpanComparator()
            {
            }

            public int compare(SuggestionSpan paramSuggestionSpan1, SuggestionSpan paramSuggestionSpan2)
            {
                int i = -1;
                int j = 0;
                int k = paramSuggestionSpan1.getFlags();
                int m = paramSuggestionSpan2.getFlags();
                int n;
                int i1;
                label45: int i2;
                if (k != m)
                    if ((k & 0x1) != 0)
                    {
                        n = 1;
                        if ((m & 0x1) == 0)
                            break label83;
                        i1 = 1;
                        if ((k & 0x2) == 0)
                            break label89;
                        i2 = 1;
                        label55: if ((m & 0x2) != 0)
                            j = 1;
                        if ((n == 0) || (i2 != 0))
                            break label95;
                    }
                while (true)
                {
                    return i;
                    n = 0;
                    break;
                    label83: i1 = 0;
                    break label45;
                    label89: i2 = 0;
                    break label55;
                    label95: if ((i1 != 0) && (j == 0))
                        i = 1;
                    else if (i2 == 0)
                        if (j != 0)
                            i = 1;
                        else
                            i = ((Integer)Editor.SuggestionsPopupWindow.this.mSpansLengths.get(paramSuggestionSpan1)).intValue() - ((Integer)Editor.SuggestionsPopupWindow.this.mSpansLengths.get(paramSuggestionSpan2)).intValue();
                }
            }
        }

        private class SuggestionAdapter extends BaseAdapter
        {
            private LayoutInflater mInflater = (LayoutInflater)Editor.this.mTextView.getContext().getSystemService("layout_inflater");

            private SuggestionAdapter()
            {
            }

            public int getCount()
            {
                return Editor.SuggestionsPopupWindow.this.mNumberOfSuggestions;
            }

            public Object getItem(int paramInt)
            {
                return Editor.SuggestionsPopupWindow.this.mSuggestionInfos[paramInt];
            }

            public long getItemId(int paramInt)
            {
                return paramInt;
            }

            public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
            {
                TextView localTextView = (TextView)paramView;
                if (localTextView == null)
                    localTextView = (TextView)this.mInflater.inflate(Editor.this.mTextView.mTextEditSuggestionItemLayout, paramViewGroup, false);
                Editor.SuggestionsPopupWindow.SuggestionInfo localSuggestionInfo = Editor.SuggestionsPopupWindow.this.mSuggestionInfos[paramInt];
                localTextView.setText(localSuggestionInfo.text);
                if (localSuggestionInfo.suggestionIndex == -1)
                    localTextView.setCompoundDrawablesWithIntrinsicBounds(17302361, 0, 0, 0);
                while (true)
                {
                    return localTextView;
                    if (localSuggestionInfo.suggestionIndex == -2)
                        localTextView.setCompoundDrawablesWithIntrinsicBounds(17302362, 0, 0, 0);
                    else
                        localTextView.setCompoundDrawables(null, null, null, null);
                }
            }
        }

        private class SuggestionInfo
        {
            TextAppearanceSpan highlightSpan = new TextAppearanceSpan(Editor.this.mTextView.getContext(), 16974104);
            int suggestionEnd;
            int suggestionIndex;
            SuggestionSpan suggestionSpan;
            int suggestionStart;
            SpannableStringBuilder text = new SpannableStringBuilder();

            private SuggestionInfo()
            {
            }
        }

        private class CustomPopupWindow extends PopupWindow
        {
            public CustomPopupWindow(Context paramInt, int arg3)
            {
                super(null, i);
            }

            public void dismiss()
            {
                super.dismiss();
                Editor.this.getPositionListener().removeSubscriber(Editor.SuggestionsPopupWindow.this);
                ((Spannable)Editor.this.mTextView.getText()).removeSpan(Editor.this.mSuggestionRangeSpan);
                Editor.this.mTextView.setCursorVisible(Editor.SuggestionsPopupWindow.this.mCursorWasVisibleBeforeSuggestions);
                if (Editor.this.hasInsertionController())
                    Editor.this.getInsertionController().show();
            }
        }
    }

    private abstract class PinnedPopupWindow
        implements Editor.TextViewPositionListener
    {
        protected ViewGroup mContentView;
        protected PopupWindow mPopupWindow;
        int mPositionX;
        int mPositionY;

        public PinnedPopupWindow()
        {
            createPopupWindow();
            this.mPopupWindow.setWindowLayoutType(1002);
            this.mPopupWindow.setWidth(-2);
            this.mPopupWindow.setHeight(-2);
            initContentView();
            ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-2, -2);
            this.mContentView.setLayoutParams(localLayoutParams);
            this.mPopupWindow.setContentView(this.mContentView);
        }

        private void computeLocalPosition()
        {
            measureContent();
            int i = this.mContentView.getMeasuredWidth();
            int j = getTextOffset();
            this.mPositionX = ((int)(Editor.this.mTextView.getLayout().getPrimaryHorizontal(j) - i / 2.0F));
            this.mPositionX += Editor.this.mTextView.viewportToContentHorizontalOffset();
            this.mPositionY = getVerticalLocalPosition(Editor.this.mTextView.getLayout().getLineForOffset(j));
            this.mPositionY += Editor.this.mTextView.viewportToContentVerticalOffset();
        }

        private void updatePosition(int paramInt1, int paramInt2)
        {
            int i = paramInt1 + this.mPositionX;
            int j = clipVertically(paramInt2 + this.mPositionY);
            DisplayMetrics localDisplayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
            int k = this.mContentView.getMeasuredWidth();
            int m = Math.max(0, Math.min(localDisplayMetrics.widthPixels - k, i));
            if (isShowing())
                this.mPopupWindow.update(m, j, -1, -1);
            while (true)
            {
                return;
                this.mPopupWindow.showAtLocation(Editor.this.mTextView, 0, m, j);
            }
        }

        protected abstract int clipVertically(int paramInt);

        protected abstract void createPopupWindow();

        protected abstract int getTextOffset();

        protected abstract int getVerticalLocalPosition(int paramInt);

        public void hide()
        {
            this.mPopupWindow.dismiss();
            Editor.this.getPositionListener().removeSubscriber(this);
        }

        protected abstract void initContentView();

        public boolean isShowing()
        {
            return this.mPopupWindow.isShowing();
        }

        protected void measureContent()
        {
            DisplayMetrics localDisplayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
            this.mContentView.measure(View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.widthPixels, -2147483648), View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.heightPixels, -2147483648));
        }

        public void show()
        {
            Editor.this.getPositionListener().addSubscriber(this, false);
            computeLocalPosition();
            Editor.PositionListener localPositionListener = Editor.this.getPositionListener();
            updatePosition(localPositionListener.getPositionX(), localPositionListener.getPositionY());
        }

        public void updatePosition(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
        {
            if ((isShowing()) && (Editor.this.isOffsetVisible(getTextOffset())))
            {
                if (paramBoolean2)
                    computeLocalPosition();
                updatePosition(paramInt1, paramInt2);
            }
            while (true)
            {
                return;
                hide();
            }
        }
    }

    private class PositionListener
        implements ViewTreeObserver.OnPreDrawListener
    {
        private final int MAXIMUM_NUMBER_OF_LISTENERS = 6;
        private boolean[] mCanMove = new boolean[6];
        private int mNumberOfListeners;
        private boolean mPositionHasChanged = true;
        private Editor.TextViewPositionListener[] mPositionListeners = new Editor.TextViewPositionListener[6];
        private int mPositionX;
        private int mPositionY;
        private boolean mScrollHasChanged;
        final int[] mTempCoords = new int[2];

        private PositionListener()
        {
        }

        private void updatePosition()
        {
            Editor.this.mTextView.getLocationInWindow(this.mTempCoords);
            if ((this.mTempCoords[0] != this.mPositionX) || (this.mTempCoords[1] != this.mPositionY));
            for (boolean bool = true; ; bool = false)
            {
                this.mPositionHasChanged = bool;
                this.mPositionX = this.mTempCoords[0];
                this.mPositionY = this.mTempCoords[1];
                return;
            }
        }

        public void addSubscriber(Editor.TextViewPositionListener paramTextViewPositionListener, boolean paramBoolean)
        {
            if (this.mNumberOfListeners == 0)
            {
                updatePosition();
                Editor.this.mTextView.getViewTreeObserver().addOnPreDrawListener(this);
            }
            int i = -1;
            int j = 0;
            Editor.TextViewPositionListener localTextViewPositionListener;
            if (j < 6)
            {
                localTextViewPositionListener = this.mPositionListeners[j];
                if (localTextViewPositionListener != paramTextViewPositionListener);
            }
            while (true)
            {
                return;
                if ((i < 0) && (localTextViewPositionListener == null))
                    i = j;
                j++;
                break;
                this.mPositionListeners[i] = paramTextViewPositionListener;
                this.mCanMove[i] = paramBoolean;
                this.mNumberOfListeners = (1 + this.mNumberOfListeners);
            }
        }

        public int getPositionX()
        {
            return this.mPositionX;
        }

        public int getPositionY()
        {
            return this.mPositionY;
        }

        public boolean onPreDraw()
        {
            updatePosition();
            for (int i = 0; i < 6; i++)
                if ((this.mPositionHasChanged) || (this.mScrollHasChanged) || (this.mCanMove[i] != 0))
                {
                    Editor.TextViewPositionListener localTextViewPositionListener = this.mPositionListeners[i];
                    if (localTextViewPositionListener != null)
                        localTextViewPositionListener.updatePosition(this.mPositionX, this.mPositionY, this.mPositionHasChanged, this.mScrollHasChanged);
                }
            this.mScrollHasChanged = false;
            return true;
        }

        public void onScrollChanged()
        {
            this.mScrollHasChanged = true;
        }

        public void removeSubscriber(Editor.TextViewPositionListener paramTextViewPositionListener)
        {
            for (int i = 0; ; i++)
                if (i < 6)
                {
                    if (this.mPositionListeners[i] == paramTextViewPositionListener)
                    {
                        this.mPositionListeners[i] = null;
                        this.mNumberOfListeners = (-1 + this.mNumberOfListeners);
                    }
                }
                else
                {
                    if (this.mNumberOfListeners == 0)
                        Editor.this.mTextView.getViewTreeObserver().removeOnPreDrawListener(this);
                    return;
                }
        }
    }

    private class EasyEditPopupWindow extends Editor.PinnedPopupWindow
        implements View.OnClickListener
    {
        private static final int POPUP_TEXT_LAYOUT = 17367220;
        private TextView mDeleteTextView;
        private EasyEditSpan mEasyEditSpan;

        private EasyEditPopupWindow()
        {
            super();
        }

        protected int clipVertically(int paramInt)
        {
            return paramInt;
        }

        protected void createPopupWindow()
        {
            this.mPopupWindow = new PopupWindow(Editor.this.mTextView.getContext(), null, 16843464);
            this.mPopupWindow.setInputMethodMode(2);
            this.mPopupWindow.setClippingEnabled(true);
        }

        protected int getTextOffset()
        {
            return ((Editable)Editor.this.mTextView.getText()).getSpanEnd(this.mEasyEditSpan);
        }

        protected int getVerticalLocalPosition(int paramInt)
        {
            return Editor.this.mTextView.getLayout().getLineBottom(paramInt);
        }

        protected void initContentView()
        {
            LinearLayout localLinearLayout = new LinearLayout(Editor.this.mTextView.getContext());
            localLinearLayout.setOrientation(0);
            this.mContentView = localLinearLayout;
            this.mContentView.setBackgroundResource(17302968);
            LayoutInflater localLayoutInflater = (LayoutInflater)Editor.this.mTextView.getContext().getSystemService("layout_inflater");
            ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-2, -2);
            this.mDeleteTextView = ((TextView)localLayoutInflater.inflate(17367220, null));
            this.mDeleteTextView.setLayoutParams(localLayoutParams);
            this.mDeleteTextView.setText(17040318);
            this.mDeleteTextView.setOnClickListener(this);
            this.mContentView.addView(this.mDeleteTextView);
        }

        public void onClick(View paramView)
        {
            if (paramView == this.mDeleteTextView)
            {
                Editable localEditable = (Editable)Editor.this.mTextView.getText();
                int i = localEditable.getSpanStart(this.mEasyEditSpan);
                int j = localEditable.getSpanEnd(this.mEasyEditSpan);
                if ((i >= 0) && (j >= 0))
                    Editor.this.mTextView.deleteText_internal(i, j);
            }
        }

        public void setEasyEditSpan(EasyEditSpan paramEasyEditSpan)
        {
            this.mEasyEditSpan = paramEasyEditSpan;
        }
    }

    class EasyEditSpanController
        implements SpanWatcher
    {
        private static final int DISPLAY_TIMEOUT_MS = 3000;
        private Runnable mHidePopup;
        private Editor.EasyEditPopupWindow mPopupWindow;

        EasyEditSpanController()
        {
        }

        public void hide()
        {
            if (this.mPopupWindow != null)
            {
                this.mPopupWindow.hide();
                Editor.this.mTextView.removeCallbacks(this.mHidePopup);
            }
        }

        public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
        {
            if ((paramObject instanceof EasyEditSpan))
            {
                if (this.mPopupWindow == null)
                {
                    this.mPopupWindow = new Editor.EasyEditPopupWindow(Editor.this, null);
                    this.mHidePopup = new Runnable()
                    {
                        public void run()
                        {
                            Editor.EasyEditSpanController.this.hide();
                        }
                    };
                }
                if (this.mPopupWindow.mEasyEditSpan != null)
                    paramSpannable.removeSpan(this.mPopupWindow.mEasyEditSpan);
                this.mPopupWindow.setEasyEditSpan((EasyEditSpan)paramObject);
                if (Editor.this.mTextView.getWindowVisibility() == 0)
                    break label90;
            }
            while (true)
            {
                return;
                label90: if ((Editor.this.mTextView.getLayout() != null) && (!Editor.this.extractedTextModeWillBeStarted()))
                {
                    this.mPopupWindow.show();
                    Editor.this.mTextView.removeCallbacks(this.mHidePopup);
                    Editor.this.mTextView.postDelayed(this.mHidePopup, 3000L);
                }
            }
        }

        public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            if ((this.mPopupWindow != null) && (paramObject == this.mPopupWindow.mEasyEditSpan))
                paramSpannable.removeSpan(this.mPopupWindow.mEasyEditSpan);
        }

        public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
        {
            if ((this.mPopupWindow != null) && (paramObject == this.mPopupWindow.mEasyEditSpan))
                hide();
        }
    }

    private static class DragLocalState
    {
        public int end;
        public TextView sourceTextView;
        public int start;

        public DragLocalState(TextView paramTextView, int paramInt1, int paramInt2)
        {
            this.sourceTextView = paramTextView;
            this.start = paramInt1;
            this.end = paramInt2;
        }
    }

    private class Blink extends Handler
        implements Runnable
    {
        private boolean mCancelled;

        private Blink()
        {
        }

        void cancel()
        {
            if (!this.mCancelled)
            {
                removeCallbacks(this);
                this.mCancelled = true;
            }
        }

        public void run()
        {
            if (this.mCancelled);
            while (true)
            {
                return;
                removeCallbacks(this);
                if (Editor.this.shouldBlink())
                {
                    if (Editor.this.mTextView.getLayout() != null)
                        Editor.this.mTextView.invalidateCursorPath();
                    postAtTime(this, 500L + SystemClock.uptimeMillis());
                }
            }
        }

        void uncancel()
        {
            this.mCancelled = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    static abstract interface TextViewPositionListener
    {
        public abstract void updatePosition(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Editor
 * JD-Core Version:        0.6.2
 */