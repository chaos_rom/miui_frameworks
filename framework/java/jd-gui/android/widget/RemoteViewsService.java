package android.widget;

import android.app.Service;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.os.IBinder;
import com.android.internal.widget.IRemoteViewsFactory.Stub;
import java.util.HashMap;

public abstract class RemoteViewsService extends Service
{
    private static final String LOG_TAG = "RemoteViewsService";
    private static final Object sLock = new Object();
    private static final HashMap<Intent.FilterComparison, RemoteViewsFactory> sRemoteViewFactories = new HashMap();

    public IBinder onBind(Intent paramIntent)
    {
        synchronized (sLock)
        {
            Intent.FilterComparison localFilterComparison = new Intent.FilterComparison(paramIntent);
            if (!sRemoteViewFactories.containsKey(localFilterComparison))
            {
                localRemoteViewsFactory = onGetViewFactory(paramIntent);
                sRemoteViewFactories.put(localFilterComparison, localRemoteViewsFactory);
                localRemoteViewsFactory.onCreate();
                bool = false;
                RemoteViewsFactoryAdapter localRemoteViewsFactoryAdapter = new RemoteViewsFactoryAdapter(localRemoteViewsFactory, bool);
                return localRemoteViewsFactoryAdapter;
            }
            RemoteViewsFactory localRemoteViewsFactory = (RemoteViewsFactory)sRemoteViewFactories.get(localFilterComparison);
            boolean bool = true;
        }
    }

    public abstract RemoteViewsFactory onGetViewFactory(Intent paramIntent);

    private static class RemoteViewsFactoryAdapter extends IRemoteViewsFactory.Stub
    {
        private RemoteViewsService.RemoteViewsFactory mFactory;
        private boolean mIsCreated;

        public RemoteViewsFactoryAdapter(RemoteViewsService.RemoteViewsFactory paramRemoteViewsFactory, boolean paramBoolean)
        {
            this.mFactory = paramRemoteViewsFactory;
            this.mIsCreated = paramBoolean;
        }

        /** @deprecated */
        public int getCount()
        {
            int i = 0;
            try
            {
                int j = this.mFactory.getCount();
                i = j;
                return i;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public long getItemId(int paramInt)
        {
            long l1 = 0L;
            try
            {
                long l2 = this.mFactory.getItemId(paramInt);
                l1 = l2;
                return l1;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public RemoteViews getLoadingView()
        {
            Object localObject1 = null;
            try
            {
                RemoteViews localRemoteViews = this.mFactory.getLoadingView();
                localObject1 = localRemoteViews;
                return localObject1;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public RemoteViews getViewAt(int paramInt)
        {
            RemoteViews localRemoteViews = null;
            try
            {
                localRemoteViews = this.mFactory.getViewAt(paramInt);
                if (localRemoteViews != null)
                    localRemoteViews.setIsWidgetCollectionChild(true);
                return localRemoteViews;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public int getViewTypeCount()
        {
            int i = 0;
            try
            {
                int j = this.mFactory.getViewTypeCount();
                i = j;
                return i;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public boolean hasStableIds()
        {
            boolean bool1 = false;
            try
            {
                boolean bool2 = this.mFactory.hasStableIds();
                bool1 = bool2;
                return bool1;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public boolean isCreated()
        {
            try
            {
                boolean bool = this.mIsCreated;
                return bool;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public void onDataSetChanged()
        {
            try
            {
                this.mFactory.onDataSetChanged();
                return;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Thread localThread = Thread.currentThread();
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public void onDataSetChangedAsync()
        {
            try
            {
                onDataSetChanged();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onDestroy(Intent paramIntent)
        {
            synchronized (RemoteViewsService.sLock)
            {
                Intent.FilterComparison localFilterComparison = new Intent.FilterComparison(paramIntent);
                RemoteViewsService.RemoteViewsFactory localRemoteViewsFactory;
                if (RemoteViewsService.sRemoteViewFactories.containsKey(localFilterComparison))
                    localRemoteViewsFactory = (RemoteViewsService.RemoteViewsFactory)RemoteViewsService.sRemoteViewFactories.get(localFilterComparison);
                try
                {
                    localRemoteViewsFactory.onDestroy();
                    RemoteViewsService.sRemoteViewFactories.remove(localFilterComparison);
                    return;
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        Thread localThread = Thread.currentThread();
                        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(localThread, localException);
                    }
                }
            }
        }
    }

    public static abstract interface RemoteViewsFactory
    {
        public abstract int getCount();

        public abstract long getItemId(int paramInt);

        public abstract RemoteViews getLoadingView();

        public abstract RemoteViews getViewAt(int paramInt);

        public abstract int getViewTypeCount();

        public abstract boolean hasStableIds();

        public abstract void onCreate();

        public abstract void onDataSetChanged();

        public abstract void onDestroy();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.RemoteViewsService
 * JD-Core Version:        0.6.2
 */