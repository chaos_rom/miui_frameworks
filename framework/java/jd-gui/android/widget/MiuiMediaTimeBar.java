package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class MiuiMediaTimeBar extends View
{
    private static final int SCRUBBER_PADDING_IN_DP = 10;
    private static final int TEXT_SIZE_IN_DP = 14;
    private static final int V_PADDING_IN_DP = 30;
    private int currentTime;
    private final ScrubbingListener listener;
    private final Rect playedBar;
    private final Paint playedPaint;
    private final Rect progressBar;
    private final Paint progressPaint;
    private final Bitmap scrubber;
    private int scrubberCorrection;
    private int scrubberLeft;
    private final int scrubberPadding;
    private int scrubberTop;
    private boolean scrubbing;
    private boolean showScrubber;
    private boolean showTimes;
    private final Rect timeBounds;
    private final Paint timeTextPaint;
    private int totalTime;
    private int vPaddingInPx;

    public MiuiMediaTimeBar(Context paramContext, ScrubbingListener paramScrubbingListener)
    {
        super(paramContext);
        if (paramScrubbingListener == null)
            throw new NullPointerException();
        this.listener = paramScrubbingListener;
        this.showTimes = true;
        this.showScrubber = true;
        this.progressBar = new Rect();
        this.playedBar = new Rect();
        this.progressPaint = new Paint();
        this.progressPaint.setColor(-8355712);
        this.playedPaint = new Paint();
        this.playedPaint.setColor(-1);
        DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
        float f = 14.0F * localDisplayMetrics.density;
        this.timeTextPaint = new Paint(1);
        this.timeTextPaint.setColor(-3223858);
        this.timeTextPaint.setTextSize(f);
        this.timeTextPaint.setTextAlign(Paint.Align.CENTER);
        this.timeBounds = new Rect();
        this.timeTextPaint.getTextBounds("0:00:00", 0, 7, this.timeBounds);
        this.scrubber = BitmapFactory.decodeResource(getResources(), 100794446);
        this.scrubberPadding = ((int)(10.0F * localDisplayMetrics.density));
        this.vPaddingInPx = ((int)(30.0F * localDisplayMetrics.density));
    }

    private void clampScrubber()
    {
        int i = this.scrubber.getWidth() / 2;
        this.scrubberLeft = Math.min(this.progressBar.right - i, Math.max(this.progressBar.left - i, this.scrubberLeft));
    }

    private int getScrubberTime()
    {
        return (int)((this.scrubberLeft + this.scrubber.getWidth() / 2 - this.progressBar.left) * this.totalTime / this.progressBar.width());
    }

    private boolean inScrubber(float paramFloat1, float paramFloat2)
    {
        int i = this.scrubberLeft + this.scrubber.getWidth();
        int j = this.scrubberTop + this.scrubber.getHeight();
        if ((this.scrubberLeft - this.scrubberPadding < paramFloat1) && (paramFloat1 < i + this.scrubberPadding) && (this.scrubberTop - this.scrubberPadding < paramFloat2) && (paramFloat2 < j + this.scrubberPadding));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private String stringForTime(long paramLong)
    {
        int i = (int)paramLong / 1000;
        int j = i % 60;
        int k = i / 60 % 60;
        int m = i / 3600;
        Object[] arrayOfObject2;
        if (m > 0)
        {
            arrayOfObject2 = new Object[3];
            arrayOfObject2[0] = Integer.valueOf(m);
            arrayOfObject2[1] = Integer.valueOf(k);
            arrayOfObject2[2] = Integer.valueOf(j);
        }
        Object[] arrayOfObject1;
        for (String str = String.format("%d:%02d:%02d", arrayOfObject2).toString(); ; str = String.format("%02d:%02d", arrayOfObject1).toString())
        {
            return str;
            arrayOfObject1 = new Object[2];
            arrayOfObject1[0] = Integer.valueOf(k);
            arrayOfObject1[1] = Integer.valueOf(j);
        }
    }

    private void update()
    {
        this.playedBar.set(this.progressBar);
        if (this.totalTime > 0);
        for (this.playedBar.right = (this.playedBar.left + (int)(this.progressBar.width() * this.currentTime / this.totalTime)); ; this.playedBar.right = this.progressBar.left)
        {
            if (!this.scrubbing)
                this.scrubberLeft = (this.playedBar.right - this.scrubber.getWidth() / 2);
            invalidate();
            return;
        }
    }

    public void draw(Canvas paramCanvas)
    {
        super.draw(paramCanvas);
        paramCanvas.drawRect(this.progressBar, this.progressPaint);
        paramCanvas.drawRect(this.playedBar, this.playedPaint);
        if (this.showScrubber)
            paramCanvas.drawBitmap(this.scrubber, this.scrubberLeft, this.scrubberTop, null);
        if (this.showTimes)
        {
            paramCanvas.drawText(stringForTime(this.currentTime), this.timeBounds.width() / 2 + getPaddingLeft(), 1 + (this.timeBounds.height() + this.vPaddingInPx / 2 + this.scrubberPadding), this.timeTextPaint);
            paramCanvas.drawText(stringForTime(this.totalTime), getWidth() - getPaddingRight() - this.timeBounds.width() / 2, 1 + (this.timeBounds.height() + this.vPaddingInPx / 2 + this.scrubberPadding), this.timeTextPaint);
        }
    }

    public int getBarHeight()
    {
        return this.timeBounds.height() + this.vPaddingInPx;
    }

    public int getPreferredHeight()
    {
        return this.timeBounds.height() + this.vPaddingInPx + this.scrubberPadding;
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = paramInt3 - paramInt1;
        int j = paramInt4 - paramInt2;
        if ((!this.showTimes) && (!this.showScrubber))
            this.progressBar.set(0, 0, i, j);
        while (true)
        {
            update();
            return;
            int k = this.scrubber.getWidth() / 3;
            if (this.showTimes)
                k += this.timeBounds.width();
            int m = (j + this.scrubberPadding) / 2;
            this.scrubberTop = (1 + (m - this.scrubber.getHeight() / 2));
            this.progressBar.set(k + getPaddingLeft(), m, i - getPaddingRight() - k, m + 4);
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        int i;
        int j;
        if (this.showScrubber)
        {
            i = (int)paramMotionEvent.getX();
            j = (int)paramMotionEvent.getY();
        }
        switch (paramMotionEvent.getAction())
        {
        default:
            bool = false;
        case 0:
        case 2:
        case 1:
        }
        while (true)
        {
            return bool;
            if (!inScrubber(i, j))
                break;
            this.scrubbing = bool;
            this.scrubberCorrection = (i - this.scrubberLeft);
            this.listener.onScrubbingStart();
            continue;
            if (!this.scrubbing)
                break;
            this.scrubberLeft = (i - this.scrubberCorrection);
            clampScrubber();
            this.currentTime = getScrubberTime();
            this.listener.onScrubbingMove(this.currentTime);
            invalidate();
            continue;
            if (!this.scrubbing)
                break;
            this.listener.onScrubbingEnd(getScrubberTime());
            this.scrubbing = false;
        }
    }

    public void resetTime()
    {
        setTime(0, 0);
    }

    public void setShowScrubber(boolean paramBoolean)
    {
        this.showScrubber = paramBoolean;
        if ((!paramBoolean) && (this.scrubbing))
        {
            this.listener.onScrubbingEnd(getScrubberTime());
            this.scrubbing = false;
        }
        requestLayout();
    }

    public void setShowTimes(boolean paramBoolean)
    {
        this.showTimes = paramBoolean;
        requestLayout();
    }

    public void setTime(int paramInt1, int paramInt2)
    {
        if ((this.currentTime == paramInt1) && (this.totalTime == paramInt2));
        while (true)
        {
            return;
            this.currentTime = paramInt1;
            this.totalTime = paramInt2;
            update();
        }
    }

    public static abstract interface ScrubbingListener
    {
        public abstract void onScrubbingEnd(int paramInt);

        public abstract void onScrubbingMove(int paramInt);

        public abstract void onScrubbingStart();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MiuiMediaTimeBar
 * JD-Core Version:        0.6.2
 */