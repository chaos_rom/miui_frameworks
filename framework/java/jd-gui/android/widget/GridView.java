package android.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.RemotableViewMethod;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.GridLayoutAnimationController.AnimationParameters;
import com.android.internal.R.styleable;

@RemoteViews.RemoteView
public class GridView extends AbsListView
{
    public static final int AUTO_FIT = -1;
    public static final int NO_STRETCH = 0;
    public static final int STRETCH_COLUMN_WIDTH = 2;
    public static final int STRETCH_SPACING = 1;
    public static final int STRETCH_SPACING_UNIFORM = 3;
    private int mColumnWidth;
    private int mGravity = 3;
    private int mHorizontalSpacing = 0;
    private int mNumColumns = -1;
    private View mReferenceView = null;
    private View mReferenceViewInSelectedRow = null;
    private int mRequestedColumnWidth;
    private int mRequestedHorizontalSpacing;
    private int mRequestedNumColumns;
    private int mStretchMode = 2;
    private final Rect mTempRect = new Rect();
    private int mVerticalSpacing = 0;

    public GridView(Context paramContext)
    {
        this(paramContext, null);
    }

    public GridView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842865);
    }

    public GridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.GridView, paramInt, 0);
        setHorizontalSpacing(localTypedArray.getDimensionPixelOffset(1, 0));
        setVerticalSpacing(localTypedArray.getDimensionPixelOffset(2, 0));
        int i = localTypedArray.getInt(3, 2);
        if (i >= 0)
            setStretchMode(i);
        int j = localTypedArray.getDimensionPixelOffset(4, -1);
        if (j > 0)
            setColumnWidth(j);
        setNumColumns(localTypedArray.getInt(5, 1));
        int k = localTypedArray.getInt(0, -1);
        if (k >= 0)
            setGravity(k);
        localTypedArray.recycle();
    }

    private void adjustForBottomFadingEdge(View paramView, int paramInt1, int paramInt2)
    {
        if (paramView.getBottom() > paramInt2)
            offsetChildrenTopAndBottom(-Math.min(paramView.getTop() - paramInt1, paramView.getBottom() - paramInt2));
    }

    private void adjustForTopFadingEdge(View paramView, int paramInt1, int paramInt2)
    {
        if (paramView.getTop() < paramInt1)
            offsetChildrenTopAndBottom(Math.min(paramInt1 - paramView.getTop(), paramInt2 - paramView.getBottom()));
    }

    private void adjustViewsUpOrDown()
    {
        int i = getChildCount();
        int j;
        if (i > 0)
        {
            if (this.mStackFromBottom)
                break label64;
            j = getChildAt(0).getTop() - this.mListPadding.top;
            if (this.mFirstPosition != 0)
                j -= this.mVerticalSpacing;
            if (j < 0)
                j = 0;
        }
        while (true)
        {
            if (j != 0)
                offsetChildrenTopAndBottom(-j);
            return;
            label64: j = getChildAt(i - 1).getBottom() - (getHeight() - this.mListPadding.bottom);
            if (i + this.mFirstPosition < this.mItemCount)
                j += this.mVerticalSpacing;
            if (j > 0)
                j = 0;
        }
    }

    private boolean commonKey(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        boolean bool1 = false;
        if (this.mAdapter == null);
        while (true)
        {
            return bool1;
            if (this.mDataChanged)
                layoutChildren();
            boolean bool2 = false;
            int i = paramKeyEvent.getAction();
            if (i != 1)
                switch (paramInt1)
                {
                default:
                case 21:
                case 22:
                case 19:
                case 20:
                case 23:
                case 66:
                case 62:
                case 92:
                case 93:
                case 122:
                case 123:
                }
            while (true)
                if (bool2)
                {
                    bool1 = true;
                    break;
                    if (paramKeyEvent.hasNoModifiers())
                    {
                        if ((resurrectSelectionIfNeeded()) || (arrowScroll(17)));
                        for (bool2 = true; ; bool2 = false)
                            break;
                        if (paramKeyEvent.hasNoModifiers())
                        {
                            if ((resurrectSelectionIfNeeded()) || (arrowScroll(66)));
                            for (bool2 = true; ; bool2 = false)
                                break;
                            if (paramKeyEvent.hasNoModifiers())
                            {
                                if ((resurrectSelectionIfNeeded()) || (arrowScroll(33)));
                                for (bool2 = true; ; bool2 = false)
                                    break;
                            }
                            if (paramKeyEvent.hasModifiers(2))
                            {
                                if ((resurrectSelectionIfNeeded()) || (fullScroll(33)));
                                for (bool2 = true; ; bool2 = false)
                                    break;
                                if (paramKeyEvent.hasNoModifiers())
                                {
                                    if ((resurrectSelectionIfNeeded()) || (arrowScroll(130)));
                                    for (bool2 = true; ; bool2 = false)
                                        break;
                                }
                                if (paramKeyEvent.hasModifiers(2))
                                {
                                    if ((resurrectSelectionIfNeeded()) || (fullScroll(130)));
                                    for (bool2 = true; ; bool2 = false)
                                        break;
                                    if (paramKeyEvent.hasNoModifiers())
                                    {
                                        bool2 = resurrectSelectionIfNeeded();
                                        if ((!bool2) && (paramKeyEvent.getRepeatCount() == 0) && (getChildCount() > 0))
                                        {
                                            keyPressed();
                                            bool2 = true;
                                            continue;
                                            if ((this.mPopup == null) || (!this.mPopup.isShowing()))
                                            {
                                                if (paramKeyEvent.hasNoModifiers())
                                                {
                                                    if ((resurrectSelectionIfNeeded()) || (pageScroll(130)));
                                                    for (bool2 = true; ; bool2 = false)
                                                        break;
                                                }
                                                if (paramKeyEvent.hasModifiers(1))
                                                {
                                                    if ((resurrectSelectionIfNeeded()) || (pageScroll(33)));
                                                    for (bool2 = true; ; bool2 = false)
                                                        break;
                                                    if (paramKeyEvent.hasNoModifiers())
                                                    {
                                                        if ((resurrectSelectionIfNeeded()) || (pageScroll(33)));
                                                        for (bool2 = true; ; bool2 = false)
                                                            break;
                                                    }
                                                    if (paramKeyEvent.hasModifiers(2))
                                                    {
                                                        if ((resurrectSelectionIfNeeded()) || (fullScroll(33)));
                                                        for (bool2 = true; ; bool2 = false)
                                                            break;
                                                        if (paramKeyEvent.hasNoModifiers())
                                                        {
                                                            if ((resurrectSelectionIfNeeded()) || (pageScroll(130)));
                                                            for (bool2 = true; ; bool2 = false)
                                                                break;
                                                        }
                                                        if (paramKeyEvent.hasModifiers(2))
                                                        {
                                                            if ((resurrectSelectionIfNeeded()) || (fullScroll(130)));
                                                            for (bool2 = true; ; bool2 = false)
                                                                break;
                                                            if (paramKeyEvent.hasNoModifiers())
                                                            {
                                                                if ((resurrectSelectionIfNeeded()) || (fullScroll(33)));
                                                                for (bool2 = true; ; bool2 = false)
                                                                    break;
                                                                if (paramKeyEvent.hasNoModifiers())
                                                                {
                                                                    if ((resurrectSelectionIfNeeded()) || (fullScroll(130)));
                                                                    for (bool2 = true; ; bool2 = false)
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            if (sendToTextFilter(paramInt1, paramInt2, paramKeyEvent))
                bool1 = true;
            else
                switch (i)
                {
                default:
                    break;
                case 0:
                    bool1 = super.onKeyDown(paramInt1, paramKeyEvent);
                    break;
                case 1:
                    bool1 = super.onKeyUp(paramInt1, paramKeyEvent);
                    break;
                case 2:
                    bool1 = super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
                }
        }
    }

    private void correctTooHigh(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((-1 + (paramInt3 + this.mFirstPosition) == -1 + this.mItemCount) && (paramInt3 > 0))
        {
            int i = getChildAt(paramInt3 - 1).getBottom();
            int j = this.mBottom - this.mTop - this.mListPadding.bottom - i;
            View localView = getChildAt(0);
            int k = localView.getTop();
            if ((j > 0) && ((this.mFirstPosition > 0) || (k < this.mListPadding.top)))
            {
                if (this.mFirstPosition == 0)
                    j = Math.min(j, this.mListPadding.top - k);
                offsetChildrenTopAndBottom(j);
                if (this.mFirstPosition > 0)
                {
                    int m = this.mFirstPosition;
                    if (this.mStackFromBottom)
                        paramInt1 = 1;
                    fillUp(m - paramInt1, localView.getTop() - paramInt2);
                    adjustViewsUpOrDown();
                }
            }
        }
    }

    private void correctTooLow(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((this.mFirstPosition == 0) && (paramInt3 > 0))
        {
            int i = getChildAt(0).getTop();
            int j = this.mListPadding.top;
            int k = this.mBottom - this.mTop - this.mListPadding.bottom;
            int m = i - j;
            View localView = getChildAt(paramInt3 - 1);
            int n = localView.getBottom();
            int i1 = -1 + (paramInt3 + this.mFirstPosition);
            if ((m > 0) && ((i1 < -1 + this.mItemCount) || (n > k)))
            {
                if (i1 == -1 + this.mItemCount)
                    m = Math.min(m, n - k);
                offsetChildrenTopAndBottom(-m);
                if (i1 < -1 + this.mItemCount)
                {
                    if (!this.mStackFromBottom)
                        paramInt1 = 1;
                    fillDown(i1 + paramInt1, paramInt2 + localView.getBottom());
                    adjustViewsUpOrDown();
                }
            }
        }
    }

    private boolean determineColumns(int paramInt)
    {
        int i = this.mRequestedHorizontalSpacing;
        int j = this.mStretchMode;
        int k = this.mRequestedColumnWidth;
        boolean bool = false;
        int m;
        if (this.mRequestedNumColumns == -1)
            if (k > 0)
            {
                this.mNumColumns = ((paramInt + i) / (k + i));
                if (this.mNumColumns <= 0)
                    this.mNumColumns = 1;
                switch (j)
                {
                default:
                    m = paramInt - k * this.mNumColumns - i * (-1 + this.mNumColumns);
                    if (m < 0)
                        bool = true;
                    switch (j)
                    {
                    default:
                    case 2:
                    case 1:
                    case 3:
                    }
                    break;
                case 0:
                }
            }
        while (true)
        {
            return bool;
            this.mNumColumns = 2;
            break;
            this.mNumColumns = this.mRequestedNumColumns;
            break;
            this.mColumnWidth = k;
            this.mHorizontalSpacing = i;
            continue;
            this.mColumnWidth = (k + m / this.mNumColumns);
            this.mHorizontalSpacing = i;
            continue;
            this.mColumnWidth = k;
            if (this.mNumColumns > 1)
            {
                this.mHorizontalSpacing = (i + m / (-1 + this.mNumColumns));
            }
            else
            {
                this.mHorizontalSpacing = (i + m);
                continue;
                this.mColumnWidth = k;
                if (this.mNumColumns > 1)
                    this.mHorizontalSpacing = (i + m / (1 + this.mNumColumns));
                else
                    this.mHorizontalSpacing = (i + m);
            }
        }
    }

    private View fillDown(int paramInt1, int paramInt2)
    {
        Object localObject = null;
        int i = this.mBottom - this.mTop;
        if ((0x22 & this.mGroupFlags) == 34)
            i -= this.mListPadding.bottom;
        while ((paramInt2 < i) && (paramInt1 < this.mItemCount))
        {
            View localView = makeRow(paramInt1, paramInt2, true);
            if (localView != null)
                localObject = localView;
            paramInt2 = this.mReferenceView.getBottom() + this.mVerticalSpacing;
            paramInt1 += this.mNumColumns;
        }
        setVisibleRangeHint(this.mFirstPosition, -1 + (this.mFirstPosition + getChildCount()));
        return localObject;
    }

    private View fillFromBottom(int paramInt1, int paramInt2)
    {
        int i = Math.min(Math.max(paramInt1, this.mSelectedPosition), -1 + this.mItemCount);
        int j = -1 + this.mItemCount - i;
        return fillUp(-1 + this.mItemCount - (j - j % this.mNumColumns), paramInt2);
    }

    private View fillFromSelection(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = getVerticalFadingEdgeLength();
        int j = this.mSelectedPosition;
        int k = this.mNumColumns;
        int m = this.mVerticalSpacing;
        int n = -1;
        int i2;
        int i5;
        label80: View localView1;
        View localView2;
        if (!this.mStackFromBottom)
        {
            i2 = j - j % k;
            int i3 = getTopSelectionPixel(paramInt2, i, i2);
            int i4 = getBottomSelectionPixel(paramInt3, i, k, i2);
            if (!this.mStackFromBottom)
                break label218;
            i5 = n;
            localView1 = makeRow(i5, paramInt1, true);
            this.mFirstPosition = i2;
            localView2 = this.mReferenceView;
            adjustForTopFadingEdge(localView2, i3, i4);
            adjustForBottomFadingEdge(localView2, i3, i4);
            if (this.mStackFromBottom)
                break label225;
            fillUp(i2 - k, localView2.getTop() - m);
            adjustViewsUpOrDown();
            fillDown(i2 + k, m + localView2.getBottom());
        }
        while (true)
        {
            return localView1;
            int i1 = -1 + this.mItemCount - j;
            n = -1 + this.mItemCount - (i1 - i1 % k);
            i2 = Math.max(0, 1 + (n - k));
            break;
            label218: i5 = i2;
            break label80;
            label225: fillDown(n + k, m + localView2.getBottom());
            adjustViewsUpOrDown();
            fillUp(i2 - 1, localView2.getTop() - m);
        }
    }

    private View fillFromTop(int paramInt)
    {
        this.mFirstPosition = Math.min(this.mFirstPosition, this.mSelectedPosition);
        this.mFirstPosition = Math.min(this.mFirstPosition, -1 + this.mItemCount);
        if (this.mFirstPosition < 0)
            this.mFirstPosition = 0;
        this.mFirstPosition -= this.mFirstPosition % this.mNumColumns;
        return fillDown(this.mFirstPosition, paramInt);
    }

    private View fillSelection(int paramInt1, int paramInt2)
    {
        int i = reconcileSelectedPosition();
        int j = this.mNumColumns;
        int k = this.mVerticalSpacing;
        int m = -1;
        int i1;
        int i2;
        int i4;
        label64: View localView1;
        View localView2;
        if (!this.mStackFromBottom)
        {
            i1 = i - i % j;
            i2 = getVerticalFadingEdgeLength();
            int i3 = getTopSelectionPixel(paramInt1, i2, i1);
            if (!this.mStackFromBottom)
                break label187;
            i4 = m;
            localView1 = makeRow(i4, i3, true);
            this.mFirstPosition = i1;
            localView2 = this.mReferenceView;
            if (this.mStackFromBottom)
                break label194;
            fillDown(i1 + j, k + localView2.getBottom());
            pinToBottom(paramInt2);
            fillUp(i1 - j, localView2.getTop() - k);
            adjustViewsUpOrDown();
        }
        while (true)
        {
            return localView1;
            int n = -1 + this.mItemCount - i;
            m = -1 + this.mItemCount - (n - n % j);
            i1 = Math.max(0, 1 + (m - j));
            break;
            label187: i4 = i1;
            break label64;
            label194: offsetChildrenTopAndBottom(getBottomSelectionPixel(paramInt2, i2, j, i1) - localView2.getBottom());
            fillUp(i1 - 1, localView2.getTop() - k);
            pinToTop(paramInt1);
            fillDown(m + j, k + localView2.getBottom());
            adjustViewsUpOrDown();
        }
    }

    private View fillSpecific(int paramInt1, int paramInt2)
    {
        int i = this.mNumColumns;
        int j = -1;
        int m;
        int n;
        label34: Object localObject;
        View localView1;
        if (!this.mStackFromBottom)
        {
            m = paramInt1 - paramInt1 % i;
            if (!this.mStackFromBottom)
                break label110;
            n = j;
            localObject = makeRow(n, paramInt2, true);
            this.mFirstPosition = m;
            localView1 = this.mReferenceView;
            if (localView1 != null)
                break label117;
            localObject = null;
        }
        while (true)
        {
            return localObject;
            int k = -1 + this.mItemCount - paramInt1;
            j = -1 + this.mItemCount - (k - k % i);
            m = Math.max(0, 1 + (j - i));
            break;
            label110: n = m;
            break label34;
            label117: int i1 = this.mVerticalSpacing;
            View localView3;
            View localView2;
            if (!this.mStackFromBottom)
            {
                localView3 = fillUp(m - i, localView1.getTop() - i1);
                adjustViewsUpOrDown();
                localView2 = fillDown(m + i, i1 + localView1.getBottom());
                int i3 = getChildCount();
                if (i3 > 0)
                    correctTooHigh(i, i1, i3);
            }
            while (true)
            {
                if (localObject != null)
                    break label268;
                if (localView3 == null)
                    break label270;
                localObject = localView3;
                break;
                localView2 = fillDown(j + i, i1 + localView1.getBottom());
                adjustViewsUpOrDown();
                localView3 = fillUp(m - 1, localView1.getTop() - i1);
                int i2 = getChildCount();
                if (i2 > 0)
                    correctTooLow(i, i1, i2);
            }
            label268: continue;
            label270: localObject = localView2;
        }
    }

    private View fillUp(int paramInt1, int paramInt2)
    {
        Object localObject = null;
        int i = 0;
        if ((0x22 & this.mGroupFlags) == 34)
            i = this.mListPadding.top;
        while ((paramInt2 > i) && (paramInt1 >= 0))
        {
            View localView = makeRow(paramInt1, paramInt2, false);
            if (localView != null)
                localObject = localView;
            paramInt2 = this.mReferenceView.getTop() - this.mVerticalSpacing;
            this.mFirstPosition = paramInt1;
            paramInt1 -= this.mNumColumns;
        }
        if (this.mStackFromBottom)
            this.mFirstPosition = Math.max(0, paramInt1 + 1);
        setVisibleRangeHint(this.mFirstPosition, -1 + (this.mFirstPosition + getChildCount()));
        return localObject;
    }

    private int getBottomSelectionPixel(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = paramInt1;
        if (-1 + (paramInt4 + paramInt3) < -1 + this.mItemCount)
            i -= paramInt2;
        return i;
    }

    private int getTopSelectionPixel(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1;
        if (paramInt3 > 0)
            i += paramInt2;
        return i;
    }

    private boolean isCandidateSelection(int paramInt1, int paramInt2)
    {
        boolean bool = true;
        int i = getChildCount();
        int j = i - 1 - paramInt1;
        int m;
        int k;
        if (!this.mStackFromBottom)
        {
            m = paramInt1 - paramInt1 % this.mNumColumns;
            k = Math.max(-1 + (m + this.mNumColumns), i);
        }
        while (true)
            switch (paramInt2)
            {
            default:
                throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}.");
                k = i - 1 - (j - j % this.mNumColumns);
                m = Math.max(0, 1 + (k - this.mNumColumns));
            case 66:
            case 130:
            case 17:
            case 33:
            case 2:
            case 1:
            }
        if (paramInt1 == m);
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if (m != 0)
            {
                bool = false;
                continue;
                if (paramInt1 != k)
                {
                    bool = false;
                    continue;
                    if (k != i - 1)
                    {
                        bool = false;
                        continue;
                        if ((paramInt1 != m) || (m != 0))
                        {
                            bool = false;
                            continue;
                            if ((paramInt1 != k) || (k != i - 1))
                                bool = false;
                        }
                    }
                }
            }
        }
    }

    private View makeAndAddView(int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, int paramInt4)
    {
        View localView2;
        if (!this.mDataChanged)
        {
            localView2 = this.mRecycler.getActiveView(paramInt1);
            if (localView2 != null)
                setupChild(localView2, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, true, paramInt4);
        }
        View localView1;
        for (Object localObject = localView2; ; localObject = localView1)
        {
            return localObject;
            localView1 = obtainView(paramInt1, this.mIsScrap);
            setupChild(localView1, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, this.mIsScrap[0], paramInt4);
        }
    }

    private View makeRow(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        int i = this.mColumnWidth;
        int j = this.mHorizontalSpacing;
        int k = this.mListPadding.left;
        int m;
        int n;
        int i1;
        label62: Object localObject;
        boolean bool1;
        boolean bool2;
        View localView;
        int i3;
        label89: boolean bool3;
        if (this.mStretchMode == 3)
        {
            m = j;
            n = k + m;
            if (this.mStackFromBottom)
                break label184;
            i1 = Math.min(paramInt1 + this.mNumColumns, this.mItemCount);
            localObject = null;
            bool1 = shouldShowSelector();
            bool2 = touchModeDrawsInPressedState();
            int i2 = this.mSelectedPosition;
            localView = null;
            i3 = paramInt1;
            if (i3 >= i1)
                break label251;
            if (i3 != i2)
                break label236;
            bool3 = true;
            label106: if (!paramBoolean)
                break label242;
        }
        label184: label236: label242: for (int i4 = -1; ; i4 = i3 - paramInt1)
        {
            localView = makeAndAddView(i3, paramInt2, paramBoolean, n, bool3, i4);
            n += i;
            if (i3 < i1 - 1)
                n += j;
            if ((bool3) && ((bool1) || (bool2)))
                localObject = localView;
            i3++;
            break label89;
            m = 0;
            break;
            i1 = paramInt1 + 1;
            paramInt1 = Math.max(0, 1 + (paramInt1 - this.mNumColumns));
            if (i1 - paramInt1 >= this.mNumColumns)
                break label62;
            n += (this.mNumColumns - (i1 - paramInt1)) * (i + j);
            break label62;
            bool3 = false;
            break label106;
        }
        label251: this.mReferenceView = localView;
        if (localObject != null)
            this.mReferenceViewInSelectedRow = this.mReferenceView;
        return localObject;
    }

    private View moveSelection(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = getVerticalFadingEdgeLength();
        int j = this.mSelectedPosition;
        int k = this.mNumColumns;
        int m = this.mVerticalSpacing;
        int n = -1;
        int i4;
        int i2;
        int i5;
        int i6;
        int i7;
        int i13;
        label111: int i14;
        label122: View localView1;
        View localView2;
        if (!this.mStackFromBottom)
        {
            i4 = j - paramInt1 - (j - paramInt1) % k;
            i2 = j - j % k;
            i5 = i2 - i4;
            i6 = getTopSelectionPixel(paramInt2, i, i2);
            i7 = getBottomSelectionPixel(paramInt3, i, k, i2);
            this.mFirstPosition = i2;
            if (i5 <= 0)
                break label312;
            if (this.mReferenceViewInSelectedRow != null)
                break label293;
            i13 = 0;
            if (!this.mStackFromBottom)
                break label305;
            i14 = n;
            int i15 = i13 + m;
            localView1 = makeRow(i14, i15, true);
            localView2 = this.mReferenceView;
            adjustForBottomFadingEdge(localView2, i6, i7);
            if (this.mStackFromBottom)
                break label454;
            fillUp(i2 - k, localView2.getTop() - m);
            adjustViewsUpOrDown();
            fillDown(i2 + k, m + localView2.getBottom());
        }
        while (true)
        {
            return localView1;
            int i1 = -1 + this.mItemCount - j;
            n = -1 + this.mItemCount - (i1 - i1 % k);
            i2 = Math.max(0, 1 + (n - k));
            int i3 = -1 + this.mItemCount - (j - paramInt1);
            i4 = Math.max(0, 1 + (-1 + this.mItemCount - (i3 - i3 % k) - k));
            break;
            label293: i13 = this.mReferenceViewInSelectedRow.getBottom();
            break label111;
            label305: i14 = i2;
            break label122;
            label312: if (i5 < 0)
            {
                int i10;
                if (this.mReferenceViewInSelectedRow == null)
                {
                    i10 = 0;
                    label327: if (!this.mStackFromBottom)
                        break label387;
                }
                for (int i11 = n; ; i11 = i2)
                {
                    int i12 = i10 - m;
                    localView1 = makeRow(i11, i12, false);
                    localView2 = this.mReferenceView;
                    adjustForTopFadingEdge(localView2, i6, i7);
                    break;
                    i10 = this.mReferenceViewInSelectedRow.getTop();
                    break label327;
                }
            }
            label387: int i8;
            if (this.mReferenceViewInSelectedRow == null)
            {
                i8 = 0;
                label404: if (!this.mStackFromBottom)
                    break label447;
            }
            label447: for (int i9 = n; ; i9 = i2)
            {
                localView1 = makeRow(i9, i8, true);
                localView2 = this.mReferenceView;
                break;
                i8 = this.mReferenceViewInSelectedRow.getTop();
                break label404;
            }
            label454: fillDown(n + k, m + localView2.getBottom());
            adjustViewsUpOrDown();
            fillUp(i2 - 1, localView2.getTop() - m);
        }
    }

    private void pinToBottom(int paramInt)
    {
        int i = getChildCount();
        if (i + this.mFirstPosition == this.mItemCount)
        {
            int j = paramInt - getChildAt(i - 1).getBottom();
            if (j > 0)
                offsetChildrenTopAndBottom(j);
        }
    }

    private void pinToTop(int paramInt)
    {
        if (this.mFirstPosition == 0)
        {
            int i = paramInt - getChildAt(0).getTop();
            if (i < 0)
                offsetChildrenTopAndBottom(i);
        }
    }

    private void setupChild(View paramView, int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, boolean paramBoolean3, int paramInt4)
    {
        boolean bool1;
        int i;
        label27: boolean bool2;
        label55: int k;
        label67: int m;
        label87: AbsListView.LayoutParams localLayoutParams;
        label151: label221: int i1;
        label267: int i2;
        int i3;
        label287: int i5;
        if ((paramBoolean2) && (shouldShowSelector()))
        {
            bool1 = true;
            if (bool1 == paramView.isSelected())
                break label409;
            i = 1;
            int j = this.mTouchMode;
            if ((j <= 0) || (j >= 3) || (this.mMotionPosition != paramInt1))
                break label415;
            bool2 = true;
            if (bool2 == paramView.isPressed())
                break label421;
            k = 1;
            if ((paramBoolean3) && (i == 0) && (!paramView.isLayoutRequested()))
                break label427;
            m = 1;
            localLayoutParams = (AbsListView.LayoutParams)paramView.getLayoutParams();
            if (localLayoutParams == null)
                localLayoutParams = (AbsListView.LayoutParams)generateDefaultLayoutParams();
            int n = this.mAdapter.getItemViewType(paramInt1);
            localLayoutParams.viewType = n;
            if ((!paramBoolean3) || (localLayoutParams.forceAdd))
                break label433;
            attachViewToParent(paramView, paramInt4, localLayoutParams);
            if (i != 0)
            {
                paramView.setSelected(bool1);
                if (bool1)
                    requestFocus();
            }
            if (k != 0)
                paramView.setPressed(bool2);
            if ((this.mChoiceMode != 0) && (this.mCheckStates != null))
            {
                if (!(paramView instanceof Checkable))
                    break label453;
                ((Checkable)paramView).setChecked(this.mCheckStates.get(paramInt1));
            }
            if (m == 0)
                break label483;
            int i6 = ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(0, 0), 0, localLayoutParams.height);
            paramView.measure(ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(this.mColumnWidth, 1073741824), 0, localLayoutParams.width), i6);
            i1 = paramView.getMeasuredWidth();
            i2 = paramView.getMeasuredHeight();
            if (!paramBoolean1)
                break label491;
            i3 = paramInt2;
            int i4 = getResolvedLayoutDirection();
            switch (0x7 & Gravity.getAbsoluteGravity(this.mGravity, i4))
            {
            case 2:
            case 4:
            default:
                i5 = paramInt3;
                label344: if (m != 0)
                    paramView.layout(i5, i3, i5 + i1, i3 + i2);
                break;
            case 3:
            case 1:
            case 5:
            }
        }
        while (true)
        {
            if (this.mCachingStarted)
                paramView.setDrawingCacheEnabled(true);
            if ((paramBoolean3) && (((AbsListView.LayoutParams)paramView.getLayoutParams()).scrappedFromPosition != paramInt1))
                paramView.jumpDrawablesToCurrentState();
            return;
            bool1 = false;
            break;
            label409: i = 0;
            break label27;
            label415: bool2 = false;
            break label55;
            label421: k = 0;
            break label67;
            label427: m = 0;
            break label87;
            label433: localLayoutParams.forceAdd = false;
            addViewInLayout(paramView, paramInt4, localLayoutParams, true);
            break label151;
            label453: if (getContext().getApplicationInfo().targetSdkVersion < 11)
                break label221;
            paramView.setActivated(this.mCheckStates.get(paramInt1));
            break label221;
            label483: cleanupLayoutState(paramView);
            break label267;
            label491: i3 = paramInt2 - i2;
            break label287;
            i5 = paramInt3;
            break label344;
            i5 = paramInt3 + (this.mColumnWidth - i1) / 2;
            break label344;
            i5 = paramInt3 + this.mColumnWidth - i1;
            break label344;
            paramView.offsetLeftAndRight(i5 - paramView.getLeft());
            paramView.offsetTopAndBottom(i3 - paramView.getTop());
        }
    }

    boolean arrowScroll(int paramInt)
    {
        int i = this.mSelectedPosition;
        int j = this.mNumColumns;
        boolean bool = false;
        int n;
        int m;
        if (!this.mStackFromBottom)
        {
            n = j * (i / j);
            m = Math.min(-1 + (n + j), -1 + this.mItemCount);
            switch (paramInt)
            {
            default:
            case 33:
            case 130:
            case 17:
            case 66:
            }
        }
        while (true)
        {
            if (bool)
            {
                playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt));
                invokeOnItemScrollListener();
            }
            if (bool)
                awakenScrollBars();
            return bool;
            int k = -1 + this.mItemCount - i;
            m = -1 + this.mItemCount - j * (k / j);
            n = Math.max(0, 1 + (m - j));
            break;
            if (n > 0)
            {
                this.mLayoutMode = 6;
                setSelectionInt(Math.max(0, i - j));
                bool = true;
                continue;
                if (m < -1 + this.mItemCount)
                {
                    this.mLayoutMode = 6;
                    setSelectionInt(Math.min(i + j, -1 + this.mItemCount));
                    bool = true;
                    continue;
                    if (i > n)
                    {
                        this.mLayoutMode = 6;
                        setSelectionInt(Math.max(0, i - 1));
                        bool = true;
                        continue;
                        if (i < m)
                        {
                            this.mLayoutMode = 6;
                            setSelectionInt(Math.min(i + 1, -1 + this.mItemCount));
                            bool = true;
                        }
                    }
                }
            }
        }
    }

    protected void attachLayoutAnimationParameters(View paramView, ViewGroup.LayoutParams paramLayoutParams, int paramInt1, int paramInt2)
    {
        GridLayoutAnimationController.AnimationParameters localAnimationParameters = (GridLayoutAnimationController.AnimationParameters)paramLayoutParams.layoutAnimationParameters;
        if (localAnimationParameters == null)
        {
            localAnimationParameters = new GridLayoutAnimationController.AnimationParameters();
            paramLayoutParams.layoutAnimationParameters = localAnimationParameters;
        }
        localAnimationParameters.count = paramInt2;
        localAnimationParameters.index = paramInt1;
        localAnimationParameters.columnsCount = this.mNumColumns;
        localAnimationParameters.rowsCount = (paramInt2 / this.mNumColumns);
        if (!this.mStackFromBottom)
            localAnimationParameters.column = (paramInt1 % this.mNumColumns);
        int i;
        for (localAnimationParameters.row = (paramInt1 / this.mNumColumns); ; localAnimationParameters.row = (-1 + localAnimationParameters.rowsCount - i / this.mNumColumns))
        {
            return;
            i = paramInt2 - 1 - paramInt1;
            localAnimationParameters.column = (-1 + this.mNumColumns - i % this.mNumColumns);
        }
    }

    protected int computeVerticalScrollExtent()
    {
        int i = getChildCount();
        int j;
        if (i > 0)
        {
            int k = this.mNumColumns;
            j = 100 * ((-1 + (i + k)) / k);
            View localView1 = getChildAt(0);
            int m = localView1.getTop();
            int n = localView1.getHeight();
            if (n > 0)
                j += m * 100 / n;
            View localView2 = getChildAt(i - 1);
            int i1 = localView2.getBottom();
            int i2 = localView2.getHeight();
            if (i2 > 0)
                j -= 100 * (i1 - getHeight()) / i2;
        }
        while (true)
        {
            return j;
            j = 0;
        }
    }

    protected int computeVerticalScrollOffset()
    {
        int i = 0;
        if ((this.mFirstPosition >= 0) && (getChildCount() > 0))
        {
            View localView = getChildAt(0);
            int j = localView.getTop();
            int k = localView.getHeight();
            if (k > 0)
            {
                int m = this.mNumColumns;
                int n = this.mFirstPosition / m;
                int i1 = (-1 + (m + this.mItemCount)) / m;
                i = Math.max(n * 100 - j * 100 / k + (int)(100.0F * (this.mScrollY / getHeight() * i1)), 0);
            }
        }
        return i;
    }

    protected int computeVerticalScrollRange()
    {
        int i = this.mNumColumns;
        int j = (-1 + (i + this.mItemCount)) / i;
        int k = Math.max(j * 100, 0);
        if (this.mScrollY != 0)
            k += Math.abs((int)(100.0F * (this.mScrollY / getHeight() * j)));
        return k;
    }

    void fillGap(boolean paramBoolean)
    {
        int i = this.mNumColumns;
        int j = this.mVerticalSpacing;
        int k = getChildCount();
        if (paramBoolean)
        {
            int i3 = 0;
            if ((0x22 & this.mGroupFlags) == 34)
                i3 = getListPaddingTop();
            if (k > 0);
            for (int i4 = j + getChildAt(k - 1).getBottom(); ; i4 = i3)
            {
                int i5 = k + this.mFirstPosition;
                if (this.mStackFromBottom)
                    i5 += i - 1;
                fillDown(i5, i4);
                correctTooHigh(i, j, getChildCount());
                return;
            }
        }
        int m = 0;
        if ((0x22 & this.mGroupFlags) == 34)
            m = getListPaddingBottom();
        int n;
        label150: int i1;
        if (k > 0)
        {
            n = getChildAt(0).getTop() - j;
            i1 = this.mFirstPosition;
            if (this.mStackFromBottom)
                break label203;
        }
        label203: for (int i2 = i1 - i; ; i2 = i1 - 1)
        {
            fillUp(i2, n);
            correctTooLow(i, j, getChildCount());
            break;
            n = getHeight() - m;
            break label150;
        }
    }

    int findMotionRow(int paramInt)
    {
        int i = getChildCount();
        int k;
        int n;
        int j;
        if (i > 0)
        {
            k = this.mNumColumns;
            if (!this.mStackFromBottom)
            {
                n = 0;
                if (n >= i)
                    break label108;
                if (paramInt <= getChildAt(n).getBottom())
                    j = n + this.mFirstPosition;
            }
        }
        while (true)
        {
            return j;
            n += k;
            break;
            int m = i - 1;
            while (true)
            {
                if (m < 0)
                    break label108;
                if (paramInt >= getChildAt(m).getTop())
                {
                    j = m + this.mFirstPosition;
                    break;
                }
                m -= k;
            }
            label108: j = -1;
        }
    }

    boolean fullScroll(int paramInt)
    {
        boolean bool = false;
        if (paramInt == 33)
        {
            this.mLayoutMode = 2;
            setSelectionInt(0);
            invokeOnItemScrollListener();
            bool = true;
        }
        while (true)
        {
            if (bool)
                awakenScrollBars();
            return bool;
            if (paramInt == 130)
            {
                this.mLayoutMode = 2;
                setSelectionInt(-1 + this.mItemCount);
                invokeOnItemScrollListener();
                bool = true;
            }
        }
    }

    public ListAdapter getAdapter()
    {
        return this.mAdapter;
    }

    public int getColumnWidth()
    {
        return this.mColumnWidth;
    }

    public int getGravity()
    {
        return this.mGravity;
    }

    public int getHorizontalSpacing()
    {
        return this.mHorizontalSpacing;
    }

    @ViewDebug.ExportedProperty
    public int getNumColumns()
    {
        return this.mNumColumns;
    }

    public int getRequestedColumnWidth()
    {
        return this.mRequestedColumnWidth;
    }

    public int getRequestedHorizontalSpacing()
    {
        return this.mRequestedHorizontalSpacing;
    }

    public int getStretchMode()
    {
        return this.mStretchMode;
    }

    public int getVerticalSpacing()
    {
        return this.mVerticalSpacing;
    }

    protected void layoutChildren()
    {
        boolean bool1 = this.mBlockLayoutRequests;
        if (!bool1)
            this.mBlockLayoutRequests = true;
        int i;
        int j;
        View localView1;
        View localView2;
        int i5;
        View localView4;
        while (true)
        {
            int m;
            try
            {
                super.layoutChildren();
                invalidate();
                if (this.mAdapter == null)
                {
                    resetList();
                    invokeOnItemScrollListener();
                    return;
                }
                i = this.mListPadding.top;
                j = this.mBottom - this.mTop - this.mListPadding.bottom;
                int k = getChildCount();
                m = 0;
                localView1 = null;
                localView2 = null;
                View localView3 = null;
                boolean bool2;
                AbsListView.RecycleBin localRecycleBin;
                switch (this.mLayoutMode)
                {
                default:
                    int i8 = this.mSelectedPosition - this.mFirstPosition;
                    if ((i8 >= 0) && (i8 < k))
                        localView1 = getChildAt(i8);
                    localView2 = getChildAt(0);
                case 1:
                case 3:
                case 4:
                case 5:
                    bool2 = this.mDataChanged;
                    if (bool2)
                        handleDataChanged();
                    if (this.mItemCount == 0)
                    {
                        resetList();
                        invokeOnItemScrollListener();
                        if (bool1)
                            continue;
                    }
                    break;
                case 2:
                    int i7 = this.mNextSelectedPosition - this.mFirstPosition;
                    if ((i7 < 0) || (i7 >= k))
                        continue;
                    localView3 = getChildAt(i7);
                    break;
                case 6:
                    if (this.mNextSelectedPosition < 0)
                        continue;
                    m = this.mNextSelectedPosition - this.mSelectedPosition;
                    continue;
                    setSelectedPositionInt(this.mNextSelectedPosition);
                    int n = this.mFirstPosition;
                    localRecycleBin = this.mRecycler;
                    if (bool2)
                    {
                        int i6 = 0;
                        if (i6 < k)
                        {
                            localRecycleBin.addScrapView(getChildAt(i6), n + i6);
                            i6++;
                            continue;
                        }
                    }
                    else
                    {
                        localRecycleBin.fillActiveViews(k, n);
                    }
                    detachAllViewsFromParent();
                    localRecycleBin.removeSkippedScrap();
                    switch (this.mLayoutMode)
                    {
                    default:
                        if (k != 0)
                            break label703;
                        if (this.mStackFromBottom)
                            break label658;
                        if (this.mAdapter == null)
                            break label870;
                        if (!isInTouchMode())
                            break label877;
                        break label870;
                        setSelectedPositionInt(i5);
                        localView4 = fillFromTop(i);
                    case 2:
                    case 1:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    }
                    break;
                }
                label426: localRecycleBin.scrapActiveViews();
                if (localView4 == null)
                    break label807;
                positionSelector(-1, localView4);
                this.mSelectedTop = localView4.getTop();
                label453: this.mLayoutMode = 0;
                this.mDataChanged = false;
                if (this.mPositionScrollAfterLayout != null)
                {
                    post(this.mPositionScrollAfterLayout);
                    this.mPositionScrollAfterLayout = null;
                }
                this.mNeedSync = false;
                setNextSelectedPositionInt(this.mSelectedPosition);
                updateScrollIndicators();
                if (this.mItemCount > 0)
                    checkSelectionChanged();
                invokeOnItemScrollListener();
                if (bool1)
                    continue;
                continue;
                if (localView3 != null)
                {
                    localView4 = fillFromSelection(localView3.getTop(), i, j);
                    continue;
                }
                localView4 = fillSelection(i, j);
                continue;
                this.mFirstPosition = 0;
                localView4 = fillFromTop(i);
                adjustViewsUpOrDown();
                continue;
            }
            finally
            {
                if (!bool1)
                    this.mBlockLayoutRequests = false;
            }
            localView4 = fillUp(-1 + this.mItemCount, j);
            adjustViewsUpOrDown();
            continue;
            localView4 = fillSpecific(this.mSelectedPosition, this.mSpecificTop);
            continue;
            localView4 = fillSpecific(this.mSyncPosition, this.mSpecificTop);
            continue;
            localView4 = moveSelection(m, i, j);
        }
        label658: int i3 = -1 + this.mItemCount;
        label703: label870: label877: label890: if (this.mAdapter != null)
            if (!isInTouchMode())
                break label890;
        while (true)
        {
            setSelectedPositionInt(i4);
            localView4 = fillFromBottom(i3, j);
            break label426;
            if ((this.mSelectedPosition >= 0) && (this.mSelectedPosition < this.mItemCount))
            {
                int i2 = this.mSelectedPosition;
                if (localView1 == null);
                while (true)
                {
                    localView4 = fillSpecific(i2, i);
                    break;
                    i = localView1.getTop();
                }
            }
            if (this.mFirstPosition < this.mItemCount)
            {
                int i1 = this.mFirstPosition;
                if (localView2 == null);
                while (true)
                {
                    localView4 = fillSpecific(i1, i);
                    break;
                    i = localView2.getTop();
                }
            }
            localView4 = fillSpecific(0, i);
            break label426;
            label807: if ((this.mTouchMode > 0) && (this.mTouchMode < 3))
            {
                View localView5 = getChildAt(this.mMotionPosition - this.mFirstPosition);
                if (localView5 == null)
                    break label453;
                positionSelector(this.mMotionPosition, localView5);
                break label453;
            }
            this.mSelectedTop = 0;
            this.mSelectorRect.setEmpty();
            break label453;
            i5 = -1;
            break;
            i5 = 0;
            break;
            int i4 = -1;
            continue;
            i4 = i3;
        }
    }

    int lookForSelectablePosition(int paramInt, boolean paramBoolean)
    {
        if ((this.mAdapter == null) || (isInTouchMode()));
        for (paramInt = -1; ; paramInt = -1)
            do
                return paramInt;
            while ((paramInt >= 0) && (paramInt < this.mItemCount));
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
        int i = -1;
        if ((paramBoolean) && (paramRect != null))
        {
            paramRect.offset(this.mScrollX, this.mScrollY);
            Rect localRect = this.mTempRect;
            int j = 2147483647;
            int k = getChildCount();
            int m = 0;
            if (m < k)
            {
                if (!isCandidateSelection(m, paramInt));
                while (true)
                {
                    m++;
                    break;
                    View localView = getChildAt(m);
                    localView.getDrawingRect(localRect);
                    offsetDescendantRectToMyCoords(localView, localRect);
                    int n = getDistance(paramRect, localRect, paramInt);
                    if (n < j)
                    {
                        j = n;
                        i = m;
                    }
                }
            }
        }
        if (i >= 0)
            setSelection(i + this.mFirstPosition);
        while (true)
        {
            return;
            requestLayout();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(GridView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(GridView.class.getName());
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        return commonKey(paramInt, 1, paramKeyEvent);
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        return commonKey(paramInt1, paramInt2, paramKeyEvent);
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        return commonKey(paramInt, 1, paramKeyEvent);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt1);
        int m = View.MeasureSpec.getSize(paramInt2);
        int i8;
        boolean bool;
        int n;
        int i1;
        label108: int i2;
        int i3;
        int i4;
        int i5;
        if (i == 0)
        {
            if (this.mColumnWidth > 0)
            {
                i8 = this.mColumnWidth + this.mListPadding.left + this.mListPadding.right;
                k = i8 + getVerticalScrollbarWidth();
            }
        }
        else
        {
            bool = determineColumns(k - this.mListPadding.left - this.mListPadding.right);
            n = 0;
            if (this.mAdapter != null)
                break label497;
            i1 = 0;
            this.mItemCount = i1;
            i2 = this.mItemCount;
            if (i2 > 0)
            {
                View localView = obtainView(0, this.mIsScrap);
                AbsListView.LayoutParams localLayoutParams = (AbsListView.LayoutParams)localView.getLayoutParams();
                if (localLayoutParams == null)
                {
                    localLayoutParams = (AbsListView.LayoutParams)generateDefaultLayoutParams();
                    localView.setLayoutParams(localLayoutParams);
                }
                int i6 = this.mAdapter.getItemViewType(0);
                localLayoutParams.viewType = i6;
                localLayoutParams.forceAdd = true;
                int i7 = getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(0, 0), 0, localLayoutParams.height);
                localView.measure(getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(this.mColumnWidth, 1073741824), 0, localLayoutParams.width), i7);
                n = localView.getMeasuredHeight();
                combineMeasuredStates(0, localView.getMeasuredState());
                if (this.mRecycler.shouldRecycleViewType(localLayoutParams.viewType))
                    this.mRecycler.addScrapView(localView, -1);
            }
            if (j == 0)
                m = n + (this.mListPadding.top + this.mListPadding.bottom) + 2 * getVerticalFadingEdgeLength();
            if (j == -2147483648)
            {
                i3 = this.mListPadding.top + this.mListPadding.bottom;
                i4 = this.mNumColumns;
                i5 = 0;
            }
        }
        while (true)
        {
            if (i5 < i2)
            {
                i3 += n;
                if (i5 + i4 < i2)
                    i3 += this.mVerticalSpacing;
                if (i3 >= m)
                    i3 = m;
            }
            else
            {
                m = i3;
                if ((i == -2147483648) && (this.mRequestedNumColumns != -1) && ((this.mRequestedNumColumns * this.mColumnWidth + (-1 + this.mRequestedNumColumns) * this.mHorizontalSpacing + this.mListPadding.left + this.mListPadding.right > k) || (bool)))
                    k |= 16777216;
                setMeasuredDimension(k, m);
                this.mWidthMeasureSpec = paramInt1;
                return;
                i8 = this.mListPadding.left + this.mListPadding.right;
                break;
                label497: i1 = this.mAdapter.getCount();
                break label108;
            }
            i5 += i4;
        }
    }

    boolean pageScroll(int paramInt)
    {
        boolean bool = false;
        int i = -1;
        if (paramInt == 33)
            i = Math.max(0, this.mSelectedPosition - getChildCount());
        while (true)
        {
            if (i >= 0)
            {
                setSelectionInt(i);
                invokeOnItemScrollListener();
                awakenScrollBars();
                bool = true;
            }
            return bool;
            if (paramInt == 130)
                i = Math.min(-1 + this.mItemCount, this.mSelectedPosition + getChildCount());
        }
    }

    boolean sequenceScroll(int paramInt)
    {
        int i = this.mSelectedPosition;
        int j = this.mNumColumns;
        int k = this.mItemCount;
        int i1;
        int n;
        if (!this.mStackFromBottom)
        {
            i1 = j * (i / j);
            n = Math.min(-1 + (i1 + j), k - 1);
            bool = false;
            i2 = 0;
            switch (paramInt)
            {
            default:
            case 2:
            case 1:
            }
        }
        do
        {
            do
            {
                if (bool)
                {
                    playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt));
                    invokeOnItemScrollListener();
                }
                if (i2 != 0)
                    awakenScrollBars();
                return bool;
                int m = k - 1 - i;
                n = k - 1 - j * (m / j);
                i1 = Math.max(0, 1 + (n - j));
                break;
            }
            while (i >= k - 1);
            this.mLayoutMode = 6;
            setSelectionInt(i + 1);
            bool = true;
            if (i == n);
            for (i2 = 1; ; i2 = 0)
                break;
        }
        while (i <= 0);
        this.mLayoutMode = 6;
        setSelectionInt(i - 1);
        boolean bool = true;
        if (i == i1);
        for (int i2 = 1; ; i2 = 0)
            break;
    }

    public void setAdapter(ListAdapter paramListAdapter)
    {
        if ((this.mAdapter != null) && (this.mDataSetObserver != null))
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
        resetList();
        this.mRecycler.clear();
        this.mAdapter = paramListAdapter;
        this.mOldSelectedPosition = -1;
        this.mOldSelectedRowId = -9223372036854775808L;
        super.setAdapter(paramListAdapter);
        int i;
        if (this.mAdapter != null)
        {
            this.mOldItemCount = this.mItemCount;
            this.mItemCount = this.mAdapter.getCount();
            this.mDataChanged = true;
            checkFocus();
            this.mDataSetObserver = new AbsListView.AdapterDataSetObserver(this);
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            this.mRecycler.setViewTypeCount(this.mAdapter.getViewTypeCount());
            if (this.mStackFromBottom)
            {
                i = lookForSelectablePosition(-1 + this.mItemCount, false);
                setSelectedPositionInt(i);
                setNextSelectedPositionInt(i);
                checkSelectionChanged();
            }
        }
        while (true)
        {
            requestLayout();
            return;
            i = lookForSelectablePosition(0, true);
            break;
            checkFocus();
            checkSelectionChanged();
        }
    }

    public void setColumnWidth(int paramInt)
    {
        if (paramInt != this.mRequestedColumnWidth)
        {
            this.mRequestedColumnWidth = paramInt;
            requestLayoutIfNecessary();
        }
    }

    public void setGravity(int paramInt)
    {
        if (this.mGravity != paramInt)
        {
            this.mGravity = paramInt;
            requestLayoutIfNecessary();
        }
    }

    public void setHorizontalSpacing(int paramInt)
    {
        if (paramInt != this.mRequestedHorizontalSpacing)
        {
            this.mRequestedHorizontalSpacing = paramInt;
            requestLayoutIfNecessary();
        }
    }

    public void setNumColumns(int paramInt)
    {
        if (paramInt != this.mRequestedNumColumns)
        {
            this.mRequestedNumColumns = paramInt;
            requestLayoutIfNecessary();
        }
    }

    @RemotableViewMethod
    public void setRemoteViewsAdapter(Intent paramIntent)
    {
        super.setRemoteViewsAdapter(paramIntent);
    }

    public void setSelection(int paramInt)
    {
        if (!isInTouchMode())
            setNextSelectedPositionInt(paramInt);
        while (true)
        {
            this.mLayoutMode = 2;
            if (this.mPositionScroller != null)
                this.mPositionScroller.stop();
            requestLayout();
            return;
            this.mResurrectToPosition = paramInt;
        }
    }

    void setSelectionInt(int paramInt)
    {
        int i = this.mNextSelectedPosition;
        if (this.mPositionScroller != null)
            this.mPositionScroller.stop();
        setNextSelectedPositionInt(paramInt);
        layoutChildren();
        int j;
        if (this.mStackFromBottom)
        {
            j = -1 + this.mItemCount - this.mNextSelectedPosition;
            if (!this.mStackFromBottom)
                break label96;
        }
        label96: for (int k = -1 + this.mItemCount - i; ; k = i)
        {
            if (j / this.mNumColumns != k / this.mNumColumns)
                awakenScrollBars();
            return;
            j = this.mNextSelectedPosition;
            break;
        }
    }

    public void setStretchMode(int paramInt)
    {
        if (paramInt != this.mStretchMode)
        {
            this.mStretchMode = paramInt;
            requestLayoutIfNecessary();
        }
    }

    public void setVerticalSpacing(int paramInt)
    {
        if (paramInt != this.mVerticalSpacing)
        {
            this.mVerticalSpacing = paramInt;
            requestLayoutIfNecessary();
        }
    }

    @RemotableViewMethod
    public void smoothScrollByOffset(int paramInt)
    {
        super.smoothScrollByOffset(paramInt);
    }

    @RemotableViewMethod
    public void smoothScrollToPosition(int paramInt)
    {
        super.smoothScrollToPosition(paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.GridView
 * JD-Core Version:        0.6.2
 */