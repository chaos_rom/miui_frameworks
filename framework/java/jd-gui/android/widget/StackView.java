package android.widget;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.TableMaskFilter;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.RemotableViewMethod;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.LinearInterpolator;
import com.android.internal.R.styleable;
import java.lang.ref.WeakReference;
import java.util.HashMap;

@RemoteViews.RemoteView
public class StackView extends AdapterViewAnimator
{
    private static final int DEFAULT_ANIMATION_DURATION = 400;
    private static final int FRAME_PADDING = 4;
    private static final int GESTURE_NONE = 0;
    private static final int GESTURE_SLIDE_DOWN = 2;
    private static final int GESTURE_SLIDE_UP = 1;
    private static final int INVALID_POINTER = -1;
    private static final int ITEMS_SLIDE_DOWN = 1;
    private static final int ITEMS_SLIDE_UP = 0;
    private static final int MINIMUM_ANIMATION_DURATION = 50;
    private static final int MIN_TIME_BETWEEN_INTERACTION_AND_AUTOADVANCE = 5000;
    private static final long MIN_TIME_BETWEEN_SCROLLS = 100L;
    private static final int NUM_ACTIVE_VIEWS = 5;
    private static final float PERSPECTIVE_SCALE_FACTOR = 0.0F;
    private static final float PERSPECTIVE_SHIFT_FACTOR_X = 0.1F;
    private static final float PERSPECTIVE_SHIFT_FACTOR_Y = 0.1F;
    private static final float SLIDE_UP_RATIO = 0.7F;
    private static final int STACK_RELAYOUT_DURATION = 100;
    private static final float SWIPE_THRESHOLD_RATIO = 0.2F;
    private static HolographicHelper sHolographicHelper;
    private final String TAG = "StackView";
    private int mActivePointerId;
    private int mClickColor;
    private ImageView mClickFeedback;
    private boolean mClickFeedbackIsValid = false;
    private boolean mFirstLayoutHappened = false;
    private int mFramePadding;
    private ImageView mHighlight;
    private float mInitialX;
    private float mInitialY;
    private long mLastInteractionTime = 0L;
    private long mLastScrollTime;
    private int mMaximumVelocity;
    private float mNewPerspectiveShiftX;
    private float mNewPerspectiveShiftY;
    private float mPerspectiveShiftX;
    private float mPerspectiveShiftY;
    private int mResOutColor;
    private int mSlideAmount;
    private int mStackMode;
    private StackSlider mStackSlider;
    private int mSwipeGestureType = 0;
    private int mSwipeThreshold;
    private final Rect mTouchRect = new Rect();
    private int mTouchSlop;
    private boolean mTransitionIsSetup = false;
    private VelocityTracker mVelocityTracker;
    private int mYVelocity = 0;
    private final Rect stackInvalidateRect = new Rect();

    public StackView(Context paramContext)
    {
        this(paramContext, null);
    }

    public StackView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843713);
    }

    public StackView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.StackView, paramInt, 0);
        this.mResOutColor = localTypedArray.getColor(0, 0);
        this.mClickColor = localTypedArray.getColor(1, 0);
        localTypedArray.recycle();
        initStackView();
    }

    private void beginGestureIfNeeded(float paramFloat)
    {
        int i = 1;
        if (((int)Math.abs(paramFloat) > this.mTouchSlop) && (this.mSwipeGestureType == 0))
            if (paramFloat >= 0.0F)
                break label46;
        label46: int m;
        for (int k = i; ; m = 2)
        {
            cancelLongPress();
            requestDisallowInterceptTouchEvent(i);
            if (this.mAdapter != null)
                break;
            return;
        }
        int n = getCount();
        int i1;
        label72: label113: int i6;
        if (this.mStackMode == 0)
            if (m == 2)
            {
                i1 = 0;
                if ((!this.mLoopViews) || (n != i) || (((this.mStackMode != 0) || (m != i)) && ((this.mStackMode != i) || (m != 2))))
                    break label240;
                int i2 = i;
                if ((!this.mLoopViews) || (n != i) || (((this.mStackMode != i) || (m != i)) && ((this.mStackMode != 0) || (m != 2))))
                    break label246;
                int i4 = i;
                label154: if ((!this.mLoopViews) || (i4 != 0) || (i2 != 0))
                    break label252;
                i6 = 0;
                label174: if (i6 != 0)
                    break label309;
            }
        while (true)
        {
            this.mTransitionIsSetup = i;
            View localView = getViewAtRelativeIndex(i1);
            if (localView == null)
                break;
            setupStackSlider(localView, i6);
            this.mSwipeGestureType = m;
            cancelHandleClick();
            break;
            i1 = i;
            break label72;
            if (m == 2);
            for (i1 = i; ; i1 = 0)
                break;
            label240: int i3 = 0;
            break label113;
            label246: int i5 = 0;
            break label154;
            label252: if ((i1 + this.mCurrentWindowStartUnbounded == -1) || (i5 != 0))
            {
                i1++;
                i6 = 1;
                break label174;
            }
            if ((i1 + this.mCurrentWindowStartUnbounded == n - 1) || (i3 != 0))
            {
                i6 = 2;
                break label174;
            }
            i6 = 0;
            break label174;
            label309: int j = 0;
        }
    }

    private void handlePointerUp(MotionEvent paramMotionEvent)
    {
        int i = (int)(paramMotionEvent.getY(paramMotionEvent.findPointerIndex(this.mActivePointerId)) - this.mInitialY);
        this.mLastInteractionTime = System.currentTimeMillis();
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
            this.mYVelocity = ((int)this.mVelocityTracker.getYVelocity(this.mActivePointerId));
        }
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
        if ((i > this.mSwipeThreshold) && (this.mSwipeGestureType == 2) && (this.mStackSlider.mMode == 0))
        {
            this.mSwipeGestureType = 0;
            if (this.mStackMode == 0)
            {
                showPrevious();
                this.mHighlight.bringToFront();
            }
        }
        label230: label376: 
        do
        {
            this.mActivePointerId = -1;
            this.mSwipeGestureType = 0;
            return;
            showNext();
            break;
            if ((i < -this.mSwipeThreshold) && (this.mSwipeGestureType == 1) && (this.mStackSlider.mMode == 0))
            {
                this.mSwipeGestureType = 0;
                if (this.mStackMode == 0)
                    showNext();
                while (true)
                {
                    this.mHighlight.bringToFront();
                    break;
                    showPrevious();
                }
            }
            if (this.mSwipeGestureType == 1)
            {
                float f2;
                if (this.mStackMode == 1)
                {
                    f2 = 1.0F;
                    if ((this.mStackMode != 0) && (this.mStackSlider.mMode == 0))
                        break label376;
                }
                for (int k = Math.round(this.mStackSlider.getDurationForNeutralPosition()); ; k = Math.round(this.mStackSlider.getDurationForOffscreenPosition()))
                {
                    StackSlider localStackSlider2 = new StackSlider(this.mStackSlider);
                    float[] arrayOfFloat3 = new float[1];
                    arrayOfFloat3[0] = f2;
                    PropertyValuesHolder localPropertyValuesHolder3 = PropertyValuesHolder.ofFloat("YProgress", arrayOfFloat3);
                    float[] arrayOfFloat4 = new float[1];
                    arrayOfFloat4[0] = 0.0F;
                    PropertyValuesHolder localPropertyValuesHolder4 = PropertyValuesHolder.ofFloat("XProgress", arrayOfFloat4);
                    PropertyValuesHolder[] arrayOfPropertyValuesHolder2 = new PropertyValuesHolder[2];
                    arrayOfPropertyValuesHolder2[0] = localPropertyValuesHolder4;
                    arrayOfPropertyValuesHolder2[1] = localPropertyValuesHolder3;
                    ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofPropertyValuesHolder(localStackSlider2, arrayOfPropertyValuesHolder2);
                    localObjectAnimator2.setDuration(k);
                    localObjectAnimator2.setInterpolator(new LinearInterpolator());
                    localObjectAnimator2.start();
                    break;
                    f2 = 0.0F;
                    break label230;
                }
            }
        }
        while (this.mSwipeGestureType != 2);
        float f1;
        if (this.mStackMode == 1)
        {
            f1 = 0.0F;
            label409: if ((this.mStackMode != 1) && (this.mStackSlider.mMode == 0))
                break label542;
        }
        label542: for (int j = Math.round(this.mStackSlider.getDurationForNeutralPosition()); ; j = Math.round(this.mStackSlider.getDurationForOffscreenPosition()))
        {
            StackSlider localStackSlider1 = new StackSlider(this.mStackSlider);
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = f1;
            PropertyValuesHolder localPropertyValuesHolder1 = PropertyValuesHolder.ofFloat("YProgress", arrayOfFloat1);
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = 0.0F;
            PropertyValuesHolder localPropertyValuesHolder2 = PropertyValuesHolder.ofFloat("XProgress", arrayOfFloat2);
            PropertyValuesHolder[] arrayOfPropertyValuesHolder1 = new PropertyValuesHolder[2];
            arrayOfPropertyValuesHolder1[0] = localPropertyValuesHolder2;
            arrayOfPropertyValuesHolder1[1] = localPropertyValuesHolder1;
            ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofPropertyValuesHolder(localStackSlider1, arrayOfPropertyValuesHolder1);
            localObjectAnimator1.setDuration(j);
            localObjectAnimator1.start();
            break;
            f1 = 1.0F;
            break label409;
        }
    }

    private void initStackView()
    {
        configureViewAnimator(5, 1);
        setStaticTransformationsEnabled(true);
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(getContext());
        this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
        this.mMaximumVelocity = localViewConfiguration.getScaledMaximumFlingVelocity();
        this.mActivePointerId = -1;
        this.mHighlight = new ImageView(getContext());
        this.mHighlight.setLayoutParams(new LayoutParams(this.mHighlight));
        addViewInLayout(this.mHighlight, -1, new LayoutParams(this.mHighlight));
        this.mClickFeedback = new ImageView(getContext());
        this.mClickFeedback.setLayoutParams(new LayoutParams(this.mClickFeedback));
        addViewInLayout(this.mClickFeedback, -1, new LayoutParams(this.mClickFeedback));
        this.mClickFeedback.setVisibility(4);
        this.mStackSlider = new StackSlider();
        if (sHolographicHelper == null)
            sHolographicHelper = new HolographicHelper(this.mContext);
        setClipChildren(false);
        setClipToPadding(false);
        this.mStackMode = 1;
        this.mWhichChild = -1;
        this.mFramePadding = ((int)Math.ceil(4.0F * this.mContext.getResources().getDisplayMetrics().density));
    }

    private void measureChildren()
    {
        int i = getChildCount();
        int j = getMeasuredWidth();
        int k = getMeasuredHeight();
        int m = Math.round(0.9F * j) - this.mPaddingLeft - this.mPaddingRight;
        int n = Math.round(0.9F * k) - this.mPaddingTop - this.mPaddingBottom;
        int i1 = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++)
        {
            View localView = getChildAt(i3);
            localView.measure(View.MeasureSpec.makeMeasureSpec(m, -2147483648), View.MeasureSpec.makeMeasureSpec(n, -2147483648));
            if ((localView != this.mHighlight) && (localView != this.mClickFeedback))
            {
                int i4 = localView.getMeasuredWidth();
                int i5 = localView.getMeasuredHeight();
                if (i4 > i1)
                    i1 = i4;
                if (i5 > i2)
                    i2 = i5;
            }
        }
        this.mNewPerspectiveShiftX = (0.1F * j);
        this.mNewPerspectiveShiftY = (0.1F * k);
        if ((i1 > 0) && (i > 0) && (i1 < m))
            this.mNewPerspectiveShiftX = (j - i1);
        if ((i2 > 0) && (i > 0) && (i2 < n))
            this.mNewPerspectiveShiftY = (k - i2);
    }

    private void onLayout()
    {
        if (!this.mFirstLayoutHappened)
        {
            this.mFirstLayoutHappened = true;
            updateChildTransforms();
        }
        int i = Math.round(0.7F * getMeasuredHeight());
        if (this.mSlideAmount != i)
        {
            this.mSlideAmount = i;
            this.mSwipeThreshold = Math.round(0.2F * i);
        }
        if ((Float.compare(this.mPerspectiveShiftY, this.mNewPerspectiveShiftY) != 0) || (Float.compare(this.mPerspectiveShiftX, this.mNewPerspectiveShiftX) != 0))
        {
            this.mPerspectiveShiftY = this.mNewPerspectiveShiftY;
            this.mPerspectiveShiftX = this.mNewPerspectiveShiftX;
            updateChildTransforms();
        }
    }

    private void onSecondaryPointerUp(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getActionIndex();
        int j;
        View localView;
        if (paramMotionEvent.getPointerId(i) == this.mActivePointerId)
        {
            if (this.mSwipeGestureType != 2)
                break label40;
            j = 0;
            localView = getViewAtRelativeIndex(j);
            if (localView != null)
                break label45;
        }
        while (true)
        {
            return;
            label40: j = 1;
            break;
            label45: for (int k = 0; ; k++)
            {
                if (k >= paramMotionEvent.getPointerCount())
                    break label201;
                if (k != i)
                {
                    float f1 = paramMotionEvent.getX(k);
                    float f2 = paramMotionEvent.getY(k);
                    this.mTouchRect.set(localView.getLeft(), localView.getTop(), localView.getRight(), localView.getBottom());
                    if (this.mTouchRect.contains(Math.round(f1), Math.round(f2)))
                    {
                        float f3 = paramMotionEvent.getX(i);
                        float f4 = paramMotionEvent.getY(i);
                        this.mInitialY += f2 - f4;
                        this.mInitialX += f1 - f3;
                        this.mActivePointerId = paramMotionEvent.getPointerId(k);
                        if (this.mVelocityTracker == null)
                            break;
                        this.mVelocityTracker.clear();
                        break;
                    }
                }
            }
            label201: handlePointerUp(paramMotionEvent);
        }
    }

    private void pacedScroll(boolean paramBoolean)
    {
        if (System.currentTimeMillis() - this.mLastScrollTime > 100L)
        {
            if (!paramBoolean)
                break label31;
            showPrevious();
        }
        while (true)
        {
            this.mLastScrollTime = System.currentTimeMillis();
            return;
            label31: showNext();
        }
    }

    private void setupStackSlider(View paramView, int paramInt)
    {
        this.mStackSlider.setMode(paramInt);
        if (paramView != null)
        {
            this.mHighlight.setImageBitmap(sHolographicHelper.createResOutline(paramView, this.mResOutColor));
            this.mHighlight.setRotation(paramView.getRotation());
            this.mHighlight.setTranslationY(paramView.getTranslationY());
            this.mHighlight.setTranslationX(paramView.getTranslationX());
            this.mHighlight.bringToFront();
            paramView.bringToFront();
            this.mStackSlider.setView(paramView);
            paramView.setVisibility(0);
        }
    }

    private void transformViewAtIndex(int paramInt, View paramView, boolean paramBoolean)
    {
        float f1 = this.mPerspectiveShiftY;
        float f2 = this.mPerspectiveShiftX;
        int i;
        float f4;
        float f5;
        float f6;
        if (this.mStackMode == 1)
        {
            i = -1 + (this.mMaxNumActiveViews - paramInt);
            int j = -1 + this.mMaxNumActiveViews;
            if (i == j)
                i--;
            float f3 = 1.0F * i / (-2 + this.mMaxNumActiveViews);
            f4 = 1.0F - 0.0F * (1.0F - f3);
            f5 = f3 * f1 + (f4 - 1.0F) * (0.9F * getMeasuredHeight() / 2.0F);
            f6 = f2 * (1.0F - f3) + (1.0F - f4) * (0.9F * getMeasuredWidth() / 2.0F);
            if ((paramView instanceof StackFrame))
                ((StackFrame)paramView).cancelTransformAnimator();
            if (!paramBoolean)
                break label314;
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = f6;
            PropertyValuesHolder localPropertyValuesHolder1 = PropertyValuesHolder.ofFloat("translationX", arrayOfFloat1);
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = f5;
            PropertyValuesHolder localPropertyValuesHolder2 = PropertyValuesHolder.ofFloat("translationY", arrayOfFloat2);
            float[] arrayOfFloat3 = new float[1];
            arrayOfFloat3[0] = f4;
            PropertyValuesHolder localPropertyValuesHolder3 = PropertyValuesHolder.ofFloat("scaleX", arrayOfFloat3);
            float[] arrayOfFloat4 = new float[1];
            arrayOfFloat4[0] = f4;
            PropertyValuesHolder localPropertyValuesHolder4 = PropertyValuesHolder.ofFloat("scaleY", arrayOfFloat4);
            PropertyValuesHolder[] arrayOfPropertyValuesHolder = new PropertyValuesHolder[4];
            arrayOfPropertyValuesHolder[0] = localPropertyValuesHolder3;
            arrayOfPropertyValuesHolder[1] = localPropertyValuesHolder4;
            arrayOfPropertyValuesHolder[2] = localPropertyValuesHolder2;
            arrayOfPropertyValuesHolder[3] = localPropertyValuesHolder1;
            ObjectAnimator localObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(paramView, arrayOfPropertyValuesHolder);
            localObjectAnimator.setDuration(100L);
            if ((paramView instanceof StackFrame))
                ((StackFrame)paramView).setTransformAnimator(localObjectAnimator);
            localObjectAnimator.start();
        }
        while (true)
        {
            return;
            i = paramInt - 1;
            if (i >= 0)
                break;
            i++;
            break;
            label314: paramView.setTranslationX(f6);
            paramView.setTranslationY(f5);
            paramView.setScaleX(f4);
            paramView.setScaleY(f4);
        }
    }

    private void updateChildTransforms()
    {
        for (int i = 0; i < getNumActiveViews(); i++)
        {
            View localView = getViewAtRelativeIndex(i);
            if (localView != null)
                transformViewAtIndex(i, localView, false);
        }
    }

    public void advance()
    {
        long l = System.currentTimeMillis() - this.mLastInteractionTime;
        if (this.mAdapter == null);
        while (true)
        {
            return;
            if (((getCount() != 1) || (!this.mLoopViews)) && (this.mSwipeGestureType == 0) && (l > 5000L))
                showNext();
        }
    }

    void applyTransformForChildAtIndex(View paramView, int paramInt)
    {
    }

    LayoutParams createOrReuseLayoutParams(View paramView)
    {
        ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
        LayoutParams localLayoutParams1;
        if ((localLayoutParams instanceof LayoutParams))
        {
            localLayoutParams1 = (LayoutParams)localLayoutParams;
            localLayoutParams1.setHorizontalOffset(0);
            localLayoutParams1.setVerticalOffset(0);
            localLayoutParams1.width = 0;
            localLayoutParams1.width = 0;
        }
        while (true)
        {
            return localLayoutParams1;
            localLayoutParams1 = new LayoutParams(paramView);
        }
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        int i = 0;
        paramCanvas.getClipBounds(this.stackInvalidateRect);
        int j = getChildCount();
        for (int k = 0; k < j; k++)
        {
            View localView = getChildAt(k);
            LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
            if (((localLayoutParams.horizontalOffset == 0) && (localLayoutParams.verticalOffset == 0)) || (localView.getAlpha() == 0.0F) || (localView.getVisibility() != 0))
                localLayoutParams.resetInvalidateRect();
            Rect localRect = localLayoutParams.getInvalidateRect();
            if (!localRect.isEmpty())
            {
                i = 1;
                this.stackInvalidateRect.union(localRect);
            }
        }
        if (i != 0)
        {
            paramCanvas.save(2);
            paramCanvas.clipRect(this.stackInvalidateRect, Region.Op.UNION);
            super.dispatchDraw(paramCanvas);
            paramCanvas.restore();
        }
        while (true)
        {
            return;
            super.dispatchDraw(paramCanvas);
        }
    }

    FrameLayout getFrameForChild()
    {
        StackFrame localStackFrame = new StackFrame(this.mContext);
        localStackFrame.setPadding(this.mFramePadding, this.mFramePadding, this.mFramePadding, this.mFramePadding);
        return localStackFrame;
    }

    void hideTapFeedback(View paramView)
    {
        this.mClickFeedback.setVisibility(4);
        invalidate();
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        if ((0x2 & paramMotionEvent.getSource()) != 0);
        switch (paramMotionEvent.getAction())
        {
        default:
            bool = super.onGenericMotionEvent(paramMotionEvent);
        case 8:
        }
        while (true)
        {
            return bool;
            float f = paramMotionEvent.getAxisValue(9);
            if (f < 0.0F)
            {
                pacedScroll(false);
            }
            else
            {
                if (f <= 0.0F)
                    break;
                pacedScroll(bool);
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(StackView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        int i = 1;
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(StackView.class.getName());
        if (getChildCount() > i);
        while (true)
        {
            paramAccessibilityNodeInfo.setScrollable(i);
            if (isEnabled())
            {
                if (getDisplayedChild() < -1 + getChildCount())
                    paramAccessibilityNodeInfo.addAction(4096);
                if (getDisplayedChild() > 0)
                    paramAccessibilityNodeInfo.addAction(8192);
            }
            return;
            int j = 0;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        switch (0xFF & paramMotionEvent.getAction())
        {
        case 4:
        case 5:
        default:
        case 0:
        case 2:
        case 6:
        case 1:
        case 3:
        }
        while (true)
        {
            if (this.mSwipeGestureType != 0)
                bool = true;
            int i;
            while (true)
            {
                return bool;
                if (this.mActivePointerId != -1)
                    break;
                this.mInitialX = paramMotionEvent.getX();
                this.mInitialY = paramMotionEvent.getY();
                this.mActivePointerId = paramMotionEvent.getPointerId(0);
                break;
                i = paramMotionEvent.findPointerIndex(this.mActivePointerId);
                if (i != -1)
                    break label127;
                Log.d("StackView", "Error: No data for our primary pointer.");
            }
            label127: beginGestureIfNeeded(paramMotionEvent.getY(i) - this.mInitialY);
            continue;
            onSecondaryPointerUp(paramMotionEvent);
            continue;
            this.mActivePointerId = -1;
            this.mSwipeGestureType = 0;
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        checkForAndHandleDataChanged();
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            int k = this.mPaddingLeft + localView.getMeasuredWidth();
            int m = this.mPaddingTop + localView.getMeasuredHeight();
            LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
            localView.layout(this.mPaddingLeft + localLayoutParams.horizontalOffset, this.mPaddingTop + localLayoutParams.verticalOffset, k + localLayoutParams.horizontalOffset, m + localLayoutParams.verticalOffset);
        }
        onLayout();
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getSize(paramInt1);
        int j = View.MeasureSpec.getSize(paramInt2);
        int k = View.MeasureSpec.getMode(paramInt1);
        int m = View.MeasureSpec.getMode(paramInt2);
        int n;
        if ((this.mReferenceChildWidth != -1) && (this.mReferenceChildHeight != -1))
        {
            n = 1;
            if (m != 0)
                break label139;
            if (n == 0)
                break label133;
            j = Math.round(this.mReferenceChildHeight * (1.0F + 1.111111F)) + this.mPaddingTop + this.mPaddingBottom;
            label80: if (k != 0)
                break label214;
            if (n == 0)
                break label209;
            i = Math.round(this.mReferenceChildWidth * (1.0F + 1.111111F)) + this.mPaddingLeft + this.mPaddingRight;
        }
        while (true)
        {
            setMeasuredDimension(i, j);
            measureChildren();
            return;
            n = 0;
            break;
            label133: j = 0;
            break label80;
            label139: if (m != -2147483648)
                break label80;
            if (n != 0)
            {
                int i2 = Math.round(this.mReferenceChildHeight * (1.0F + 1.111111F)) + this.mPaddingTop + this.mPaddingBottom;
                if (i2 <= j)
                {
                    j = i2;
                    break label80;
                }
                j |= 16777216;
                break label80;
            }
            j = 0;
            break label80;
            label209: i = 0;
            continue;
            label214: if (m == -2147483648)
                if (n != 0)
                {
                    int i1 = this.mReferenceChildWidth + this.mPaddingLeft + this.mPaddingRight;
                    if (i1 <= i)
                        i = i1;
                    else
                        i |= 16777216;
                }
                else
                {
                    i = 0;
                }
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        super.onTouchEvent(paramMotionEvent);
        int i = paramMotionEvent.getAction();
        int j = paramMotionEvent.findPointerIndex(this.mActivePointerId);
        if (j == -1)
        {
            Log.d("StackView", "Error: No data for our primary pointer.");
            return bool;
        }
        float f1 = paramMotionEvent.getY(j);
        float f2 = paramMotionEvent.getX(j);
        float f3 = f1 - this.mInitialY;
        float f4 = f2 - this.mInitialX;
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        switch (i & 0xFF)
        {
        case 4:
        case 5:
        default:
        case 2:
        case 1:
        case 6:
        case 3:
        }
        while (true)
        {
            bool = true;
            break;
            beginGestureIfNeeded(f3);
            float f5 = f4 / (1.0F * this.mSlideAmount);
            if (this.mSwipeGestureType == 2)
            {
                float f7 = 1.0F * ((f3 - 1.0F * this.mTouchSlop) / this.mSlideAmount);
                if (this.mStackMode == 1)
                    f7 = 1.0F - f7;
                this.mStackSlider.setYProgress(1.0F - f7);
                this.mStackSlider.setXProgress(f5);
                bool = true;
                break;
            }
            if (this.mSwipeGestureType == 1)
            {
                float f6 = 1.0F * (-(f3 + 1.0F * this.mTouchSlop) / this.mSlideAmount);
                if (this.mStackMode == 1)
                    f6 = 1.0F - f6;
                this.mStackSlider.setYProgress(f6);
                this.mStackSlider.setXProgress(f5);
                bool = true;
                break;
                handlePointerUp(paramMotionEvent);
                continue;
                onSecondaryPointerUp(paramMotionEvent);
                continue;
                this.mActivePointerId = -1;
                this.mSwipeGestureType = 0;
            }
        }
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool = true;
        if (super.performAccessibilityAction(paramInt, paramBundle));
        while (true)
        {
            return bool;
            if (!isEnabled())
                bool = false;
            else
                switch (paramInt)
                {
                default:
                    bool = false;
                    break;
                case 4096:
                    if (getDisplayedChild() < -1 + getChildCount())
                        showNext();
                    else
                        bool = false;
                    break;
                case 8192:
                    if (getDisplayedChild() > 0)
                        showPrevious();
                    else
                        bool = false;
                    break;
                }
        }
    }

    @RemotableViewMethod
    public void showNext()
    {
        if (this.mSwipeGestureType != 0);
        while (true)
        {
            return;
            if (!this.mTransitionIsSetup)
            {
                View localView = getViewAtRelativeIndex(1);
                if (localView != null)
                {
                    setupStackSlider(localView, 0);
                    this.mStackSlider.setYProgress(0.0F);
                    this.mStackSlider.setXProgress(0.0F);
                }
            }
            super.showNext();
        }
    }

    void showOnly(int paramInt, boolean paramBoolean)
    {
        super.showOnly(paramInt, paramBoolean);
        for (int i = this.mCurrentWindowEnd; i >= this.mCurrentWindowStart; i--)
        {
            int j = modulo(i, getWindowSize());
            if ((AdapterViewAnimator.ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(j)) != null)
            {
                View localView = ((AdapterViewAnimator.ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(j))).view;
                if (localView != null)
                    localView.bringToFront();
            }
        }
        if (this.mHighlight != null)
            this.mHighlight.bringToFront();
        this.mTransitionIsSetup = false;
        this.mClickFeedbackIsValid = false;
    }

    @RemotableViewMethod
    public void showPrevious()
    {
        if (this.mSwipeGestureType != 0);
        while (true)
        {
            return;
            if (!this.mTransitionIsSetup)
            {
                View localView = getViewAtRelativeIndex(0);
                if (localView != null)
                {
                    setupStackSlider(localView, 0);
                    this.mStackSlider.setYProgress(1.0F);
                    this.mStackSlider.setXProgress(0.0F);
                }
            }
            super.showPrevious();
        }
    }

    void showTapFeedback(View paramView)
    {
        updateClickFeedback();
        this.mClickFeedback.setVisibility(0);
        this.mClickFeedback.bringToFront();
        invalidate();
    }

    void transformViewForTransition(int paramInt1, int paramInt2, final View paramView, boolean paramBoolean)
    {
        if (!paramBoolean)
        {
            ((StackFrame)paramView).cancelSliderAnimator();
            paramView.setRotationX(0.0F);
            LayoutParams localLayoutParams2 = (LayoutParams)paramView.getLayoutParams();
            localLayoutParams2.setVerticalOffset(0);
            localLayoutParams2.setHorizontalOffset(0);
        }
        if ((paramInt1 == -1) && (paramInt2 == -1 + getNumActiveViews()))
        {
            transformViewAtIndex(paramInt2, paramView, false);
            paramView.setVisibility(0);
            paramView.setAlpha(1.0F);
        }
        while (true)
        {
            if (paramInt2 != -1)
                transformViewAtIndex(paramInt2, paramView, paramBoolean);
            return;
            if ((paramInt1 == 0) && (paramInt2 == 1))
            {
                ((StackFrame)paramView).cancelSliderAnimator();
                paramView.setVisibility(0);
                int j = Math.round(this.mStackSlider.getDurationForNeutralPosition(this.mYVelocity));
                StackSlider localStackSlider2 = new StackSlider(this.mStackSlider);
                localStackSlider2.setView(paramView);
                if (paramBoolean)
                {
                    float[] arrayOfFloat3 = new float[1];
                    arrayOfFloat3[0] = 0.0F;
                    PropertyValuesHolder localPropertyValuesHolder3 = PropertyValuesHolder.ofFloat("YProgress", arrayOfFloat3);
                    float[] arrayOfFloat4 = new float[1];
                    arrayOfFloat4[0] = 0.0F;
                    PropertyValuesHolder localPropertyValuesHolder4 = PropertyValuesHolder.ofFloat("XProgress", arrayOfFloat4);
                    PropertyValuesHolder[] arrayOfPropertyValuesHolder2 = new PropertyValuesHolder[2];
                    arrayOfPropertyValuesHolder2[0] = localPropertyValuesHolder4;
                    arrayOfPropertyValuesHolder2[1] = localPropertyValuesHolder3;
                    ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofPropertyValuesHolder(localStackSlider2, arrayOfPropertyValuesHolder2);
                    localObjectAnimator2.setDuration(j);
                    localObjectAnimator2.setInterpolator(new LinearInterpolator());
                    ((StackFrame)paramView).setSliderAnimator(localObjectAnimator2);
                    localObjectAnimator2.start();
                }
                else
                {
                    localStackSlider2.setYProgress(0.0F);
                    localStackSlider2.setXProgress(0.0F);
                }
            }
            else if ((paramInt1 == 1) && (paramInt2 == 0))
            {
                ((StackFrame)paramView).cancelSliderAnimator();
                int i = Math.round(this.mStackSlider.getDurationForOffscreenPosition(this.mYVelocity));
                StackSlider localStackSlider1 = new StackSlider(this.mStackSlider);
                localStackSlider1.setView(paramView);
                if (paramBoolean)
                {
                    float[] arrayOfFloat1 = new float[1];
                    arrayOfFloat1[0] = 1.0F;
                    PropertyValuesHolder localPropertyValuesHolder1 = PropertyValuesHolder.ofFloat("YProgress", arrayOfFloat1);
                    float[] arrayOfFloat2 = new float[1];
                    arrayOfFloat2[0] = 0.0F;
                    PropertyValuesHolder localPropertyValuesHolder2 = PropertyValuesHolder.ofFloat("XProgress", arrayOfFloat2);
                    PropertyValuesHolder[] arrayOfPropertyValuesHolder1 = new PropertyValuesHolder[2];
                    arrayOfPropertyValuesHolder1[0] = localPropertyValuesHolder2;
                    arrayOfPropertyValuesHolder1[1] = localPropertyValuesHolder1;
                    ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofPropertyValuesHolder(localStackSlider1, arrayOfPropertyValuesHolder1);
                    localObjectAnimator1.setDuration(i);
                    localObjectAnimator1.setInterpolator(new LinearInterpolator());
                    ((StackFrame)paramView).setSliderAnimator(localObjectAnimator1);
                    localObjectAnimator1.start();
                }
                else
                {
                    localStackSlider1.setYProgress(1.0F);
                    localStackSlider1.setXProgress(0.0F);
                }
            }
            else if (paramInt2 == 0)
            {
                paramView.setAlpha(0.0F);
                paramView.setVisibility(4);
            }
            else if (((paramInt1 == 0) || (paramInt1 == 1)) && (paramInt2 > 1))
            {
                paramView.setVisibility(0);
                paramView.setAlpha(1.0F);
                paramView.setRotationX(0.0F);
                LayoutParams localLayoutParams1 = (LayoutParams)paramView.getLayoutParams();
                localLayoutParams1.setVerticalOffset(0);
                localLayoutParams1.setHorizontalOffset(0);
            }
            else if (paramInt1 == -1)
            {
                paramView.setAlpha(1.0F);
                paramView.setVisibility(0);
            }
            else if (paramInt2 == -1)
            {
                if (paramBoolean)
                    postDelayed(new Runnable()
                    {
                        public void run()
                        {
                            paramView.setAlpha(0.0F);
                        }
                    }
                    , 100L);
                else
                    paramView.setAlpha(0.0F);
            }
        }
    }

    void updateClickFeedback()
    {
        if (!this.mClickFeedbackIsValid)
        {
            View localView = getViewAtRelativeIndex(1);
            if (localView != null)
            {
                this.mClickFeedback.setImageBitmap(sHolographicHelper.createClickOutline(localView, this.mClickColor));
                this.mClickFeedback.setTranslationX(localView.getTranslationX());
                this.mClickFeedback.setTranslationY(localView.getTranslationY());
            }
            this.mClickFeedbackIsValid = true;
        }
    }

    private static class HolographicHelper
    {
        private static final int CLICK_FEEDBACK = 1;
        private static final int RES_OUT;
        private final Paint mBlurPaint = new Paint();
        private final Canvas mCanvas = new Canvas();
        private float mDensity;
        private final Paint mErasePaint = new Paint();
        private final Paint mHolographicPaint = new Paint();
        private final Matrix mIdentityMatrix = new Matrix();
        private BlurMaskFilter mLargeBlurMaskFilter;
        private final Canvas mMaskCanvas = new Canvas();
        private BlurMaskFilter mSmallBlurMaskFilter;
        private final int[] mTmpXY = new int[2];

        HolographicHelper(Context paramContext)
        {
            this.mDensity = paramContext.getResources().getDisplayMetrics().density;
            this.mHolographicPaint.setFilterBitmap(true);
            this.mHolographicPaint.setMaskFilter(TableMaskFilter.CreateClipTable(0, 30));
            this.mErasePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
            this.mErasePaint.setFilterBitmap(true);
            this.mSmallBlurMaskFilter = new BlurMaskFilter(2.0F * this.mDensity, BlurMaskFilter.Blur.NORMAL);
            this.mLargeBlurMaskFilter = new BlurMaskFilter(4.0F * this.mDensity, BlurMaskFilter.Blur.NORMAL);
        }

        Bitmap createClickOutline(View paramView, int paramInt)
        {
            return createOutline(paramView, 1, paramInt);
        }

        Bitmap createOutline(View paramView, int paramInt1, int paramInt2)
        {
            this.mHolographicPaint.setColor(paramInt2);
            Bitmap localBitmap;
            if (paramInt1 == 0)
            {
                this.mBlurPaint.setMaskFilter(this.mSmallBlurMaskFilter);
                if ((paramView.getMeasuredWidth() != 0) && (paramView.getMeasuredHeight() != 0))
                    break label64;
                localBitmap = null;
            }
            while (true)
            {
                return localBitmap;
                if (paramInt1 != 1)
                    break;
                this.mBlurPaint.setMaskFilter(this.mLargeBlurMaskFilter);
                break;
                label64: localBitmap = Bitmap.createBitmap(paramView.getMeasuredWidth(), paramView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
                this.mCanvas.setBitmap(localBitmap);
                float f1 = paramView.getRotationX();
                float f2 = paramView.getRotation();
                float f3 = paramView.getTranslationY();
                float f4 = paramView.getTranslationX();
                paramView.setRotationX(0.0F);
                paramView.setRotation(0.0F);
                paramView.setTranslationY(0.0F);
                paramView.setTranslationX(0.0F);
                paramView.draw(this.mCanvas);
                paramView.setRotationX(f1);
                paramView.setRotation(f2);
                paramView.setTranslationY(f3);
                paramView.setTranslationX(f4);
                drawOutline(this.mCanvas, localBitmap);
                this.mCanvas.setBitmap(null);
            }
        }

        Bitmap createResOutline(View paramView, int paramInt)
        {
            return createOutline(paramView, 0, paramInt);
        }

        void drawOutline(Canvas paramCanvas, Bitmap paramBitmap)
        {
            int[] arrayOfInt = this.mTmpXY;
            Bitmap localBitmap = paramBitmap.extractAlpha(this.mBlurPaint, arrayOfInt);
            this.mMaskCanvas.setBitmap(localBitmap);
            this.mMaskCanvas.drawBitmap(paramBitmap, -arrayOfInt[0], -arrayOfInt[1], this.mErasePaint);
            paramCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            paramCanvas.setMatrix(this.mIdentityMatrix);
            paramCanvas.drawBitmap(localBitmap, arrayOfInt[0], arrayOfInt[1], this.mHolographicPaint);
            this.mMaskCanvas.setBitmap(null);
            localBitmap.recycle();
        }
    }

    class LayoutParams extends ViewGroup.LayoutParams
    {
        private final Rect globalInvalidateRect = new Rect();
        int horizontalOffset;
        private final Rect invalidateRect = new Rect();
        private final RectF invalidateRectf = new RectF();
        View mView;
        private final Rect parentRect = new Rect();
        int verticalOffset;

        LayoutParams(Context paramAttributeSet, AttributeSet arg3)
        {
            super(localAttributeSet);
            this.horizontalOffset = 0;
            this.verticalOffset = 0;
            this.width = 0;
            this.height = 0;
        }

        LayoutParams(View arg2)
        {
            super(0);
            this.width = 0;
            this.height = 0;
            this.horizontalOffset = 0;
            this.verticalOffset = 0;
            Object localObject;
            this.mView = localObject;
        }

        Rect getInvalidateRect()
        {
            return this.invalidateRect;
        }

        void invalidateGlobalRegion(View paramView, Rect paramRect)
        {
            this.globalInvalidateRect.set(paramRect);
            this.globalInvalidateRect.union(0, 0, StackView.this.getWidth(), StackView.this.getHeight());
            View localView = paramView;
            if ((paramView.getParent() == null) || (!(paramView.getParent() instanceof View)));
            while (true)
            {
                return;
                int i = 1;
                this.parentRect.set(0, 0, 0, 0);
                while ((localView.getParent() != null) && ((localView.getParent() instanceof View)) && (!this.parentRect.contains(this.globalInvalidateRect)))
                {
                    if (i == 0)
                        this.globalInvalidateRect.offset(localView.getLeft() - localView.getScrollX(), localView.getTop() - localView.getScrollY());
                    i = 0;
                    localView = (View)localView.getParent();
                    this.parentRect.set(localView.getScrollX(), localView.getScrollY(), localView.getWidth() + localView.getScrollX(), localView.getHeight() + localView.getScrollY());
                    localView.invalidate(this.globalInvalidateRect.left, this.globalInvalidateRect.top, this.globalInvalidateRect.right, this.globalInvalidateRect.bottom);
                }
                localView.invalidate(this.globalInvalidateRect.left, this.globalInvalidateRect.top, this.globalInvalidateRect.right, this.globalInvalidateRect.bottom);
            }
        }

        void resetInvalidateRect()
        {
            this.invalidateRect.set(0, 0, 0, 0);
        }

        public void setHorizontalOffset(int paramInt)
        {
            setOffsets(paramInt, this.verticalOffset);
        }

        public void setOffsets(int paramInt1, int paramInt2)
        {
            int i = paramInt1 - this.horizontalOffset;
            this.horizontalOffset = paramInt1;
            int j = paramInt2 - this.verticalOffset;
            this.verticalOffset = paramInt2;
            if (this.mView != null)
            {
                this.mView.requestLayout();
                int k = Math.min(i + this.mView.getLeft(), this.mView.getLeft());
                int m = Math.max(i + this.mView.getRight(), this.mView.getRight());
                int n = Math.min(j + this.mView.getTop(), this.mView.getTop());
                int i1 = Math.max(j + this.mView.getBottom(), this.mView.getBottom());
                this.invalidateRectf.set(k, n, m, i1);
                float f1 = -this.invalidateRectf.left;
                float f2 = -this.invalidateRectf.top;
                this.invalidateRectf.offset(f1, f2);
                this.mView.getMatrix().mapRect(this.invalidateRectf);
                this.invalidateRectf.offset(-f1, -f2);
                this.invalidateRect.set((int)Math.floor(this.invalidateRectf.left), (int)Math.floor(this.invalidateRectf.top), (int)Math.ceil(this.invalidateRectf.right), (int)Math.ceil(this.invalidateRectf.bottom));
                invalidateGlobalRegion(this.mView, this.invalidateRect);
            }
        }

        public void setVerticalOffset(int paramInt)
        {
            setOffsets(this.horizontalOffset, paramInt);
        }
    }

    private class StackSlider
    {
        static final int BEGINNING_OF_STACK_MODE = 1;
        static final int END_OF_STACK_MODE = 2;
        static final int NORMAL_MODE;
        int mMode = 0;
        View mView;
        float mXProgress;
        float mYProgress;

        public StackSlider()
        {
        }

        public StackSlider(StackSlider arg2)
        {
            Object localObject;
            this.mView = localObject.mView;
            this.mYProgress = localObject.mYProgress;
            this.mXProgress = localObject.mXProgress;
            this.mMode = localObject.mMode;
        }

        private float cubic(float paramFloat)
        {
            return (float)(1.0D + Math.pow(2.0F * paramFloat - 1.0F, 3.0D)) / 2.0F;
        }

        private float getDuration(boolean paramBoolean, float paramFloat)
        {
            float f2;
            float f3;
            float f4;
            float f1;
            if (this.mView != null)
            {
                StackView.LayoutParams localLayoutParams = (StackView.LayoutParams)this.mView.getLayoutParams();
                f2 = (float)Math.sqrt(Math.pow(localLayoutParams.horizontalOffset, 2.0D) + Math.pow(localLayoutParams.verticalOffset, 2.0D));
                f3 = (float)Math.sqrt(Math.pow(StackView.this.mSlideAmount, 2.0D) + Math.pow(0.4F * StackView.this.mSlideAmount, 2.0D));
                if (paramFloat == 0.0F)
                    if (paramBoolean)
                    {
                        f4 = 1.0F - f2 / f3;
                        f1 = f4 * 400.0F;
                    }
            }
            while (true)
            {
                return f1;
                f4 = f2 / f3;
                break;
                if (paramBoolean);
                for (f1 = f2 / Math.abs(paramFloat); ; f1 = (f3 - f2) / Math.abs(paramFloat))
                {
                    if ((f1 >= 50.0F) && (f1 <= 400.0F))
                        break label174;
                    f1 = getDuration(paramBoolean, 0.0F);
                    break;
                }
                label174: continue;
                f1 = 0.0F;
            }
        }

        private float highlightAlphaInterpolator(float paramFloat)
        {
            if (paramFloat < 0.4F);
            for (float f = 0.85F * cubic(paramFloat / 0.4F); ; f = 0.85F * cubic(1.0F - (paramFloat - 0.4F) / (1.0F - 0.4F)))
                return f;
        }

        private float rotationInterpolator(float paramFloat)
        {
            if (paramFloat < 0.2F);
            for (float f = 0.0F; ; f = (paramFloat - 0.2F) / (1.0F - 0.2F))
                return f;
        }

        private float viewAlphaInterpolator(float paramFloat)
        {
            if (paramFloat > 0.3F);
            for (float f = (paramFloat - 0.3F) / (1.0F - 0.3F); ; f = 0.0F)
                return f;
        }

        float getDurationForNeutralPosition()
        {
            return getDuration(false, 0.0F);
        }

        float getDurationForNeutralPosition(float paramFloat)
        {
            return getDuration(false, paramFloat);
        }

        float getDurationForOffscreenPosition()
        {
            return getDuration(true, 0.0F);
        }

        float getDurationForOffscreenPosition(float paramFloat)
        {
            return getDuration(true, paramFloat);
        }

        public float getXProgress()
        {
            return this.mXProgress;
        }

        public float getYProgress()
        {
            return this.mYProgress;
        }

        void setMode(int paramInt)
        {
            this.mMode = paramInt;
        }

        void setView(View paramView)
        {
            this.mView = paramView;
        }

        public void setXProgress(float paramFloat)
        {
            float f1 = Math.max(-2.0F, Math.min(2.0F, paramFloat));
            this.mXProgress = f1;
            if (this.mView == null);
            while (true)
            {
                return;
                StackView.LayoutParams localLayoutParams1 = (StackView.LayoutParams)this.mView.getLayoutParams();
                StackView.LayoutParams localLayoutParams2 = (StackView.LayoutParams)StackView.this.mHighlight.getLayoutParams();
                float f2 = f1 * 0.2F;
                localLayoutParams1.setHorizontalOffset(Math.round(f2 * StackView.this.mSlideAmount));
                localLayoutParams2.setHorizontalOffset(Math.round(f2 * StackView.this.mSlideAmount));
            }
        }

        public void setYProgress(float paramFloat)
        {
            float f1 = Math.max(0.0F, Math.min(1.0F, paramFloat));
            this.mYProgress = f1;
            if (this.mView == null);
            label301: 
            while (true)
            {
                return;
                StackView.LayoutParams localLayoutParams1 = (StackView.LayoutParams)this.mView.getLayoutParams();
                StackView.LayoutParams localLayoutParams2 = (StackView.LayoutParams)StackView.this.mHighlight.getLayoutParams();
                int i;
                if (StackView.this.mStackMode == 0)
                {
                    i = 1;
                    label62: if ((Float.compare(0.0F, this.mYProgress) == 0) || (Float.compare(1.0F, this.mYProgress) == 0))
                        break label301;
                    if (this.mView.getLayerType() == 0)
                        this.mView.setLayerType(2, null);
                }
                label103: switch (this.mMode)
                {
                default:
                    break;
                case 0:
                    localLayoutParams1.setVerticalOffset(Math.round(-f1 * i * StackView.this.mSlideAmount));
                    localLayoutParams2.setVerticalOffset(Math.round(-f1 * i * StackView.this.mSlideAmount));
                    StackView.this.mHighlight.setAlpha(highlightAlphaInterpolator(f1));
                    float f4 = viewAlphaInterpolator(1.0F - f1);
                    if ((this.mView.getAlpha() == 0.0F) && (f4 != 0.0F) && (this.mView.getVisibility() != 0))
                        this.mView.setVisibility(0);
                    while (true)
                    {
                        this.mView.setAlpha(f4);
                        this.mView.setRotationX(90.0F * i * rotationInterpolator(f1));
                        StackView.this.mHighlight.setRotationX(90.0F * i * rotationInterpolator(f1));
                        break;
                        i = -1;
                        break label62;
                        if (this.mView.getLayerType() == 0)
                            break label103;
                        this.mView.setLayerType(0, null);
                        break label103;
                        if ((f4 == 0.0F) && (this.mView.getAlpha() != 0.0F) && (this.mView.getVisibility() == 0))
                            this.mView.setVisibility(4);
                    }
                case 2:
                    float f3 = f1 * 0.2F;
                    localLayoutParams1.setVerticalOffset(Math.round(f3 * -i * StackView.this.mSlideAmount));
                    localLayoutParams2.setVerticalOffset(Math.round(f3 * -i * StackView.this.mSlideAmount));
                    StackView.this.mHighlight.setAlpha(highlightAlphaInterpolator(f3));
                    break;
                case 1:
                    float f2 = 0.2F * (1.0F - f1);
                    localLayoutParams1.setVerticalOffset(Math.round(f2 * i * StackView.this.mSlideAmount));
                    localLayoutParams2.setVerticalOffset(Math.round(f2 * i * StackView.this.mSlideAmount));
                    StackView.this.mHighlight.setAlpha(highlightAlphaInterpolator(f2));
                }
            }
        }
    }

    private static class StackFrame extends FrameLayout
    {
        WeakReference<ObjectAnimator> sliderAnimator;
        WeakReference<ObjectAnimator> transformAnimator;

        public StackFrame(Context paramContext)
        {
            super();
        }

        boolean cancelSliderAnimator()
        {
            if (this.sliderAnimator != null)
            {
                ObjectAnimator localObjectAnimator = (ObjectAnimator)this.sliderAnimator.get();
                if (localObjectAnimator != null)
                    localObjectAnimator.cancel();
            }
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean cancelTransformAnimator()
        {
            if (this.transformAnimator != null)
            {
                ObjectAnimator localObjectAnimator = (ObjectAnimator)this.transformAnimator.get();
                if (localObjectAnimator != null)
                    localObjectAnimator.cancel();
            }
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        void setSliderAnimator(ObjectAnimator paramObjectAnimator)
        {
            this.sliderAnimator = new WeakReference(paramObjectAnimator);
        }

        void setTransformAnimator(ObjectAnimator paramObjectAnimator)
        {
            this.transformAnimator = new WeakReference(paramObjectAnimator);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.StackView
 * JD-Core Version:        0.6.2
 */