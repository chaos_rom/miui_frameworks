package android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.QwertyKeyListener;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class MultiAutoCompleteTextView extends AutoCompleteTextView
{
    private Tokenizer mTokenizer;

    public MultiAutoCompleteTextView(Context paramContext)
    {
        this(paramContext, null);
    }

    public MultiAutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842859);
    }

    public MultiAutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean enoughToFilter()
    {
        boolean bool = false;
        Editable localEditable = getText();
        int i = getSelectionEnd();
        if ((i < 0) || (this.mTokenizer == null));
        while (true)
        {
            return bool;
            if (i - this.mTokenizer.findTokenStart(localEditable, i) >= getThreshold())
                bool = true;
        }
    }

    void finishInit()
    {
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(MultiAutoCompleteTextView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(MultiAutoCompleteTextView.class.getName());
    }

    protected void performFiltering(CharSequence paramCharSequence, int paramInt)
    {
        if (enoughToFilter())
        {
            int i = getSelectionEnd();
            performFiltering(paramCharSequence, this.mTokenizer.findTokenStart(paramCharSequence, i), i, paramInt);
        }
        while (true)
        {
            return;
            dismissDropDown();
            Filter localFilter = getFilter();
            if (localFilter != null)
                localFilter.filter(null);
        }
    }

    protected void performFiltering(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        getFilter().filter(paramCharSequence.subSequence(paramInt1, paramInt2), this);
    }

    public void performValidation()
    {
        AutoCompleteTextView.Validator localValidator = getValidator();
        if ((localValidator == null) || (this.mTokenizer == null))
            return;
        Editable localEditable = getText();
        int i = getText().length();
        label32: int j;
        CharSequence localCharSequence;
        if (i > 0)
        {
            j = this.mTokenizer.findTokenStart(localEditable, i);
            localCharSequence = localEditable.subSequence(j, this.mTokenizer.findTokenEnd(localEditable, j));
            if (!TextUtils.isEmpty(localCharSequence))
                break label97;
            localEditable.replace(j, i, "");
        }
        while (true)
        {
            i = j;
            break label32;
            break;
            label97: if (!localValidator.isValid(localCharSequence))
                localEditable.replace(j, i, this.mTokenizer.terminateToken(localValidator.fixText(localCharSequence)));
        }
    }

    protected void replaceText(CharSequence paramCharSequence)
    {
        clearComposingText();
        int i = getSelectionEnd();
        int j = this.mTokenizer.findTokenStart(getText(), i);
        Editable localEditable = getText();
        QwertyKeyListener.markAsReplaced(localEditable, j, i, TextUtils.substring(localEditable, j, i));
        localEditable.replace(j, i, this.mTokenizer.terminateToken(paramCharSequence));
    }

    public void setTokenizer(Tokenizer paramTokenizer)
    {
        this.mTokenizer = paramTokenizer;
    }

    public static class CommaTokenizer
        implements MultiAutoCompleteTextView.Tokenizer
    {
        public int findTokenEnd(CharSequence paramCharSequence, int paramInt)
        {
            int i = paramInt;
            int j = paramCharSequence.length();
            if (i < j)
                if (paramCharSequence.charAt(i) != ',');
            while (true)
            {
                return i;
                i++;
                break;
                i = j;
            }
        }

        public int findTokenStart(CharSequence paramCharSequence, int paramInt)
        {
            for (int i = paramInt; (i > 0) && (paramCharSequence.charAt(i - 1) != ','); i--);
            while ((i < paramInt) && (paramCharSequence.charAt(i) == ' '))
                i++;
            return i;
        }

        public CharSequence terminateToken(CharSequence paramCharSequence)
        {
            for (int i = paramCharSequence.length(); (i > 0) && (paramCharSequence.charAt(i - 1) == ' '); i--);
            if ((i > 0) && (paramCharSequence.charAt(i - 1) == ','));
            while (true)
            {
                return paramCharSequence;
                if ((paramCharSequence instanceof Spanned))
                {
                    SpannableString localSpannableString = new SpannableString(paramCharSequence + ", ");
                    TextUtils.copySpansFrom((Spanned)paramCharSequence, 0, paramCharSequence.length(), Object.class, localSpannableString, 0);
                    paramCharSequence = localSpannableString;
                }
                else
                {
                    paramCharSequence = paramCharSequence + ", ";
                }
            }
        }
    }

    public static abstract interface Tokenizer
    {
        public abstract int findTokenEnd(CharSequence paramCharSequence, int paramInt);

        public abstract int findTokenStart(CharSequence paramCharSequence, int paramInt);

        public abstract CharSequence terminateToken(CharSequence paramCharSequence);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MultiAutoCompleteTextView
 * JD-Core Version:        0.6.2
 */