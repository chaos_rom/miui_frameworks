package android.widget;

public abstract interface Checkable
{
    public abstract boolean isChecked();

    public abstract void setChecked(boolean paramBoolean);

    public abstract void toggle();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Checkable
 * JD-Core Version:        0.6.2
 */