package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.CompatibilityInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.AllCapsTransformationMethod;
import android.text.method.TransformationMethod2;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.List;

public class Switch extends CompoundButton
{
    private static final int[] CHECKED_STATE_SET = arrayOfInt;
    private static final int MONOSPACE = 3;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;
    private static final int TOUCH_MODE_IDLE;
    private int mMinFlingVelocity;
    private Layout mOffLayout;
    private Layout mOnLayout;
    private int mSwitchBottom;
    private int mSwitchHeight;
    private int mSwitchLeft;
    private int mSwitchMinWidth;
    private int mSwitchPadding;
    private int mSwitchRight;
    private int mSwitchTop;
    private TransformationMethod2 mSwitchTransformationMethod;
    private int mSwitchWidth;
    private final Rect mTempRect = new Rect();
    private ColorStateList mTextColors;
    private CharSequence mTextOff;
    private CharSequence mTextOn;
    private TextPaint mTextPaint = new TextPaint(1);
    private Drawable mThumbDrawable;
    private float mThumbPosition;
    private int mThumbTextPadding;
    private int mThumbWidth;
    private int mTouchMode;
    private int mTouchSlop;
    private float mTouchX;
    private float mTouchY;
    private Drawable mTrackDrawable;
    private VelocityTracker mVelocityTracker = VelocityTracker.obtain();

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842912;
    }

    public Switch(Context paramContext)
    {
        this(paramContext, null);
    }

    public Switch(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843738);
    }

    public Switch(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        Resources localResources = getResources();
        this.mTextPaint.density = localResources.getDisplayMetrics().density;
        this.mTextPaint.setCompatibilityScaling(localResources.getCompatibilityInfo().applicationScale);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Switch, paramInt, 0);
        this.mThumbDrawable = localTypedArray.getDrawable(2);
        this.mTrackDrawable = localTypedArray.getDrawable(4);
        this.mTextOn = localTypedArray.getText(0);
        this.mTextOff = localTypedArray.getText(1);
        this.mThumbTextPadding = localTypedArray.getDimensionPixelSize(7, 0);
        this.mSwitchMinWidth = localTypedArray.getDimensionPixelSize(5, 0);
        this.mSwitchPadding = localTypedArray.getDimensionPixelSize(6, 0);
        int i = localTypedArray.getResourceId(3, 0);
        if (i != 0)
            setSwitchTextAppearance(paramContext, i);
        localTypedArray.recycle();
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(paramContext);
        this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
        this.mMinFlingVelocity = localViewConfiguration.getScaledMinimumFlingVelocity();
        refreshDrawableState();
        setChecked(isChecked());
    }

    private void animateThumbToCheckedState(boolean paramBoolean)
    {
        setChecked(paramBoolean);
    }

    private void cancelSuperTouch(MotionEvent paramMotionEvent)
    {
        MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
        localMotionEvent.setAction(3);
        super.onTouchEvent(localMotionEvent);
        localMotionEvent.recycle();
    }

    private boolean getTargetCheckedState()
    {
        if (this.mThumbPosition >= getThumbScrollRange() / 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private int getThumbScrollRange()
    {
        if (this.mTrackDrawable == null);
        for (int i = 0; ; i = this.mSwitchWidth - this.mThumbWidth - this.mTempRect.left - this.mTempRect.right)
        {
            return i;
            this.mTrackDrawable.getPadding(this.mTempRect);
        }
    }

    private boolean hitThumb(float paramFloat1, float paramFloat2)
    {
        this.mThumbDrawable.getPadding(this.mTempRect);
        int i = this.mSwitchTop - this.mTouchSlop;
        int j = this.mSwitchLeft + (int)(0.5F + this.mThumbPosition) - this.mTouchSlop;
        int k = j + this.mThumbWidth + this.mTempRect.left + this.mTempRect.right + this.mTouchSlop;
        int m = this.mSwitchBottom + this.mTouchSlop;
        if ((paramFloat1 > j) && (paramFloat1 < k) && (paramFloat2 > i) && (paramFloat2 < m));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private Layout makeLayout(CharSequence paramCharSequence)
    {
        if (this.mSwitchTransformationMethod != null);
        for (CharSequence localCharSequence = this.mSwitchTransformationMethod.getTransformation(paramCharSequence, this); ; localCharSequence = paramCharSequence)
            return new StaticLayout(localCharSequence, this.mTextPaint, (int)Math.ceil(Layout.getDesiredWidth(localCharSequence, this.mTextPaint)), Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
    }

    private void setSwitchTypefaceByIndex(int paramInt1, int paramInt2)
    {
        Typeface localTypeface = null;
        switch (paramInt1)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            setSwitchTypeface(localTypeface, paramInt2);
            return;
            localTypeface = Typeface.SANS_SERIF;
            continue;
            localTypeface = Typeface.SERIF;
            continue;
            localTypeface = Typeface.MONOSPACE;
        }
    }

    private void stopDrag(MotionEvent paramMotionEvent)
    {
        this.mTouchMode = 0;
        int i;
        boolean bool;
        if ((paramMotionEvent.getAction() == 1) && (isEnabled()))
        {
            i = 1;
            cancelSuperTouch(paramMotionEvent);
            if (i == 0)
                break label98;
            this.mVelocityTracker.computeCurrentVelocity(1000);
            float f = this.mVelocityTracker.getXVelocity();
            if (Math.abs(f) <= this.mMinFlingVelocity)
                break label89;
            if (f <= 0.0F)
                break label83;
            bool = true;
            label71: animateThumbToCheckedState(bool);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label83: bool = false;
            break label71;
            label89: bool = getTargetCheckedState();
            break label71;
            label98: animateThumbToCheckedState(isChecked());
        }
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        int[] arrayOfInt = getDrawableState();
        if (this.mThumbDrawable != null)
            this.mThumbDrawable.setState(arrayOfInt);
        if (this.mTrackDrawable != null)
            this.mTrackDrawable.setState(arrayOfInt);
        invalidate();
    }

    public int getCompoundPaddingRight()
    {
        int i = super.getCompoundPaddingRight() + this.mSwitchWidth;
        if (!TextUtils.isEmpty(getText()))
            i += this.mSwitchPadding;
        return i;
    }

    public int getSwitchMinWidth()
    {
        return this.mSwitchMinWidth;
    }

    public int getSwitchPadding()
    {
        return this.mSwitchPadding;
    }

    public CharSequence getTextOff()
    {
        return this.mTextOff;
    }

    public CharSequence getTextOn()
    {
        return this.mTextOn;
    }

    public Drawable getThumbDrawable()
    {
        return this.mThumbDrawable;
    }

    public int getThumbTextPadding()
    {
        return this.mThumbTextPadding;
    }

    public Drawable getTrackDrawable()
    {
        return this.mTrackDrawable;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        this.mThumbDrawable.jumpToCurrentState();
        this.mTrackDrawable.jumpToCurrentState();
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
        if (isChecked())
            mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET);
        return arrayOfInt;
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        int i = this.mSwitchLeft;
        int j = this.mSwitchTop;
        int k = this.mSwitchRight;
        int m = this.mSwitchBottom;
        this.mTrackDrawable.setBounds(i, j, k, m);
        this.mTrackDrawable.draw(paramCanvas);
        paramCanvas.save();
        this.mTrackDrawable.getPadding(this.mTempRect);
        int n = i + this.mTempRect.left;
        int i1 = j + this.mTempRect.top;
        int i2 = k - this.mTempRect.right;
        int i3 = m - this.mTempRect.bottom;
        paramCanvas.clipRect(n, j, i2, m);
        this.mThumbDrawable.getPadding(this.mTempRect);
        int i4 = (int)(0.5F + this.mThumbPosition);
        int i5 = i4 + (n - this.mTempRect.left);
        int i6 = n + i4 + this.mThumbWidth + this.mTempRect.right;
        this.mThumbDrawable.setBounds(i5, j, i6, m);
        this.mThumbDrawable.draw(paramCanvas);
        if (this.mTextColors != null)
            this.mTextPaint.setColor(this.mTextColors.getColorForState(getDrawableState(), this.mTextColors.getDefaultColor()));
        this.mTextPaint.drawableState = getDrawableState();
        if (getTargetCheckedState());
        for (Layout localLayout = this.mOnLayout; ; localLayout = this.mOffLayout)
        {
            paramCanvas.translate((i5 + i6) / 2 - localLayout.getWidth() / 2, (i1 + i3) / 2 - localLayout.getHeight() / 2);
            localLayout.draw(paramCanvas);
            paramCanvas.restore();
            return;
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(Switch.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(Switch.class.getName());
        CharSequence localCharSequence1;
        CharSequence localCharSequence2;
        if (isChecked())
        {
            localCharSequence1 = this.mTextOn;
            if (!TextUtils.isEmpty(localCharSequence1))
            {
                localCharSequence2 = paramAccessibilityNodeInfo.getText();
                if (!TextUtils.isEmpty(localCharSequence2))
                    break label59;
                paramAccessibilityNodeInfo.setText(localCharSequence1);
            }
        }
        while (true)
        {
            return;
            localCharSequence1 = this.mTextOff;
            break;
            label59: StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append(localCharSequence2).append(' ').append(localCharSequence1);
            paramAccessibilityNodeInfo.setText(localStringBuilder);
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        float f;
        int i;
        int j;
        int m;
        int k;
        if (isChecked())
        {
            f = getThumbScrollRange();
            this.mThumbPosition = f;
            i = getWidth() - getPaddingRight();
            j = i - this.mSwitchWidth;
            switch (0x70 & getGravity())
            {
            default:
                m = getPaddingTop();
                k = m + this.mSwitchHeight;
            case 16:
            case 80:
            }
        }
        while (true)
        {
            this.mSwitchLeft = j;
            this.mSwitchTop = m;
            this.mSwitchBottom = k;
            this.mSwitchRight = i;
            return;
            f = 0.0F;
            break;
            m = (getPaddingTop() + getHeight() - getPaddingBottom()) / 2 - this.mSwitchHeight / 2;
            k = m + this.mSwitchHeight;
            continue;
            k = getHeight() - getPaddingBottom();
            m = k - this.mSwitchHeight;
        }
    }

    public void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt1);
        int m = View.MeasureSpec.getSize(paramInt2);
        if (this.mOnLayout == null)
            this.mOnLayout = makeLayout(this.mTextOn);
        if (this.mOffLayout == null)
            this.mOffLayout = makeLayout(this.mTextOff);
        this.mTrackDrawable.getPadding(this.mTempRect);
        int n = Math.max(this.mOnLayout.getWidth(), this.mOffLayout.getWidth());
        int i1 = Math.max(this.mSwitchMinWidth, n * 2 + 4 * this.mThumbTextPadding + this.mTempRect.left + this.mTempRect.right);
        int i2 = this.mTrackDrawable.getIntrinsicHeight();
        this.mThumbWidth = (n + 2 * this.mThumbTextPadding);
        switch (i)
        {
        default:
            switch (j)
            {
            default:
            case -2147483648:
            case 0:
            }
            break;
        case -2147483648:
        case 0:
        }
        while (true)
        {
            this.mSwitchWidth = i1;
            this.mSwitchHeight = i2;
            super.onMeasure(paramInt1, paramInt2);
            if (getMeasuredHeight() < i2)
                setMeasuredDimension(getMeasuredWidthAndState(), i2);
            return;
            Math.min(k, i1);
            break;
            break;
            Math.min(m, i2);
        }
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
        if (isChecked());
        for (CharSequence localCharSequence = this.mOnLayout.getText(); ; localCharSequence = this.mOffLayout.getText())
        {
            if (!TextUtils.isEmpty(localCharSequence))
                paramAccessibilityEvent.getText().add(localCharSequence);
            return;
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = 1;
        this.mVelocityTracker.addMovement(paramMotionEvent);
        switch (paramMotionEvent.getActionMasked())
        {
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            i = super.onTouchEvent(paramMotionEvent);
            while (true)
            {
                return i;
                float f5 = paramMotionEvent.getX();
                float f6 = paramMotionEvent.getY();
                if ((!isEnabled()) || (!hitThumb(f5, f6)))
                    break;
                this.mTouchMode = i;
                this.mTouchX = f5;
                this.mTouchY = f6;
                break;
                switch (this.mTouchMode)
                {
                case 0:
                default:
                    break;
                case 1:
                    float f3 = paramMotionEvent.getX();
                    float f4 = paramMotionEvent.getY();
                    if ((Math.abs(f3 - this.mTouchX) <= this.mTouchSlop) && (Math.abs(f4 - this.mTouchY) <= this.mTouchSlop))
                        break;
                    this.mTouchMode = 2;
                    getParent().requestDisallowInterceptTouchEvent(i);
                    this.mTouchX = f3;
                    this.mTouchY = f4;
                    break;
                case 2:
                    float f1 = paramMotionEvent.getX();
                    float f2 = Math.max(0.0F, Math.min(f1 - this.mTouchX + this.mThumbPosition, getThumbScrollRange()));
                    if (f2 != this.mThumbPosition)
                    {
                        this.mThumbPosition = f2;
                        this.mTouchX = f1;
                        invalidate();
                        continue;
                        if (this.mTouchMode != 2)
                            break label289;
                        stopDrag(paramMotionEvent);
                    }
                    break;
                }
            }
            label289: this.mTouchMode = 0;
            this.mVelocityTracker.clear();
        }
    }

    public void setChecked(boolean paramBoolean)
    {
        super.setChecked(paramBoolean);
        if (paramBoolean);
        for (float f = getThumbScrollRange(); ; f = 0.0F)
        {
            this.mThumbPosition = f;
            invalidate();
            return;
        }
    }

    public void setSwitchMinWidth(int paramInt)
    {
        this.mSwitchMinWidth = paramInt;
        requestLayout();
    }

    public void setSwitchPadding(int paramInt)
    {
        this.mSwitchPadding = paramInt;
        requestLayout();
    }

    public void setSwitchTextAppearance(Context paramContext, int paramInt)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramInt, R.styleable.TextAppearance);
        ColorStateList localColorStateList = localTypedArray.getColorStateList(3);
        if (localColorStateList != null)
        {
            this.mTextColors = localColorStateList;
            int i = localTypedArray.getDimensionPixelSize(0, 0);
            if ((i != 0) && (i != this.mTextPaint.getTextSize()))
            {
                this.mTextPaint.setTextSize(i);
                requestLayout();
            }
            setSwitchTypefaceByIndex(localTypedArray.getInt(1, -1), localTypedArray.getInt(2, -1));
            if (!localTypedArray.getBoolean(7, false))
                break label137;
            this.mSwitchTransformationMethod = new AllCapsTransformationMethod(getContext());
            this.mSwitchTransformationMethod.setLengthChangesAllowed(true);
        }
        while (true)
        {
            localTypedArray.recycle();
            return;
            this.mTextColors = getTextColors();
            break;
            label137: this.mSwitchTransformationMethod = null;
        }
    }

    public void setSwitchTypeface(Typeface paramTypeface)
    {
        if (this.mTextPaint.getTypeface() != paramTypeface)
        {
            this.mTextPaint.setTypeface(paramTypeface);
            requestLayout();
            invalidate();
        }
    }

    public void setSwitchTypeface(Typeface paramTypeface, int paramInt)
    {
        boolean bool = false;
        Typeface localTypeface;
        int i;
        label34: float f;
        if (paramInt > 0)
            if (paramTypeface == null)
            {
                localTypeface = Typeface.defaultFromStyle(paramInt);
                setSwitchTypeface(localTypeface);
                if (localTypeface == null)
                    break label100;
                i = localTypeface.getStyle();
                int j = paramInt & (i ^ 0xFFFFFFFF);
                TextPaint localTextPaint1 = this.mTextPaint;
                if ((j & 0x1) != 0)
                    bool = true;
                localTextPaint1.setFakeBoldText(bool);
                TextPaint localTextPaint2 = this.mTextPaint;
                if ((j & 0x2) == 0)
                    break label106;
                f = -0.25F;
                label82: localTextPaint2.setTextSkewX(f);
            }
        while (true)
        {
            return;
            localTypeface = Typeface.create(paramTypeface, paramInt);
            break;
            label100: i = 0;
            break label34;
            label106: f = 0.0F;
            break label82;
            this.mTextPaint.setFakeBoldText(false);
            this.mTextPaint.setTextSkewX(0.0F);
            setSwitchTypeface(paramTypeface);
        }
    }

    public void setTextOff(CharSequence paramCharSequence)
    {
        this.mTextOff = paramCharSequence;
        requestLayout();
    }

    public void setTextOn(CharSequence paramCharSequence)
    {
        this.mTextOn = paramCharSequence;
        requestLayout();
    }

    public void setThumbDrawable(Drawable paramDrawable)
    {
        this.mThumbDrawable = paramDrawable;
        requestLayout();
    }

    public void setThumbResource(int paramInt)
    {
        setThumbDrawable(getContext().getResources().getDrawable(paramInt));
    }

    public void setThumbTextPadding(int paramInt)
    {
        this.mThumbTextPadding = paramInt;
        requestLayout();
    }

    public void setTrackDrawable(Drawable paramDrawable)
    {
        this.mTrackDrawable = paramDrawable;
        requestLayout();
    }

    public void setTrackResource(int paramInt)
    {
        setTrackDrawable(getContext().getResources().getDrawable(paramInt));
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((super.verifyDrawable(paramDrawable)) || (paramDrawable == this.mThumbDrawable) || (paramDrawable == this.mTrackDrawable));
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Switch
 * JD-Core Version:        0.6.2
 */