package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.RemotableViewMethod;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.Formatter;
import java.util.IllegalFormatException;
import java.util.Locale;

@RemoteViews.RemoteView
public class Chronometer extends TextView
{
    private static final String TAG = "Chronometer";
    private static final int TICK_WHAT = 2;
    private long mBase;
    private String mFormat;
    private StringBuilder mFormatBuilder;
    private Formatter mFormatter;
    private Object[] mFormatterArgs = new Object[1];
    private Locale mFormatterLocale;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            if (Chronometer.this.mRunning)
            {
                Chronometer.this.updateText(SystemClock.elapsedRealtime());
                Chronometer.this.dispatchChronometerTick();
                sendMessageDelayed(Message.obtain(this, 2), 1000L);
            }
        }
    };
    private boolean mLogged;
    private OnChronometerTickListener mOnChronometerTickListener;
    private StringBuilder mRecycle = new StringBuilder(8);
    private boolean mRunning;
    private boolean mStarted;
    private boolean mVisible;

    public Chronometer(Context paramContext)
    {
        this(paramContext, null, 0);
    }

    public Chronometer(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public Chronometer(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Chronometer, paramInt, 0);
        setFormat(localTypedArray.getString(0));
        localTypedArray.recycle();
        init();
    }

    private void init()
    {
        this.mBase = SystemClock.elapsedRealtime();
        updateText(this.mBase);
    }

    private void updateRunning()
    {
        boolean bool;
        if ((this.mVisible) && (this.mStarted))
        {
            bool = true;
            if (bool != this.mRunning)
            {
                if (!bool)
                    break label69;
                updateText(SystemClock.elapsedRealtime());
                dispatchChronometerTick();
                this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 2), 1000L);
            }
        }
        while (true)
        {
            this.mRunning = bool;
            return;
            bool = false;
            break;
            label69: this.mHandler.removeMessages(2);
        }
    }

    /** @deprecated */
    private void updateText(long paramLong)
    {
        try
        {
            long l = (paramLong - this.mBase) / 1000L;
            Object localObject2 = DateUtils.formatElapsedTime(this.mRecycle, l);
            if (this.mFormat != null)
            {
                Locale localLocale = Locale.getDefault();
                if ((this.mFormatter == null) || (!localLocale.equals(this.mFormatterLocale)))
                {
                    this.mFormatterLocale = localLocale;
                    this.mFormatter = new Formatter(this.mFormatBuilder, localLocale);
                }
                this.mFormatBuilder.setLength(0);
                this.mFormatterArgs[0] = localObject2;
            }
            try
            {
                this.mFormatter.format(this.mFormat, this.mFormatterArgs);
                String str = this.mFormatBuilder.toString();
                localObject2 = str;
                setText((CharSequence)localObject2);
                return;
            }
            catch (IllegalFormatException localIllegalFormatException)
            {
                while (true)
                    if (!this.mLogged)
                    {
                        Log.w("Chronometer", "Illegal format string: " + this.mFormat);
                        this.mLogged = true;
                    }
            }
        }
        finally
        {
        }
    }

    void dispatchChronometerTick()
    {
        if (this.mOnChronometerTickListener != null)
            this.mOnChronometerTickListener.onChronometerTick(this);
    }

    public long getBase()
    {
        return this.mBase;
    }

    public String getFormat()
    {
        return this.mFormat;
    }

    public OnChronometerTickListener getOnChronometerTickListener()
    {
        return this.mOnChronometerTickListener;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.mVisible = false;
        updateRunning();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(Chronometer.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(Chronometer.class.getName());
    }

    protected void onWindowVisibilityChanged(int paramInt)
    {
        super.onWindowVisibilityChanged(paramInt);
        if (paramInt == 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mVisible = bool;
            updateRunning();
            return;
        }
    }

    @RemotableViewMethod
    public void setBase(long paramLong)
    {
        this.mBase = paramLong;
        dispatchChronometerTick();
        updateText(SystemClock.elapsedRealtime());
    }

    @RemotableViewMethod
    public void setFormat(String paramString)
    {
        this.mFormat = paramString;
        if ((paramString != null) && (this.mFormatBuilder == null))
            this.mFormatBuilder = new StringBuilder(2 * paramString.length());
    }

    public void setOnChronometerTickListener(OnChronometerTickListener paramOnChronometerTickListener)
    {
        this.mOnChronometerTickListener = paramOnChronometerTickListener;
    }

    @RemotableViewMethod
    public void setStarted(boolean paramBoolean)
    {
        this.mStarted = paramBoolean;
        updateRunning();
    }

    public void start()
    {
        this.mStarted = true;
        updateRunning();
    }

    public void stop()
    {
        this.mStarted = false;
        updateRunning();
    }

    public static abstract interface OnChronometerTickListener
    {
        public abstract void onChronometerTick(Chronometer paramChronometer);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Chronometer
 * JD-Core Version:        0.6.2
 */