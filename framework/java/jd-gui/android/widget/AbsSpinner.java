package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public abstract class AbsSpinner extends AdapterView<SpinnerAdapter>
{
    SpinnerAdapter mAdapter;
    private DataSetObserver mDataSetObserver;
    int mHeightMeasureSpec;
    final RecycleBin mRecycler = new RecycleBin();
    int mSelectionBottomPadding = 0;
    int mSelectionLeftPadding = 0;
    int mSelectionRightPadding = 0;
    int mSelectionTopPadding = 0;
    final Rect mSpinnerPadding = new Rect();
    private Rect mTouchFrame;
    int mWidthMeasureSpec;

    public AbsSpinner(Context paramContext)
    {
        super(paramContext);
        initAbsSpinner();
    }

    public AbsSpinner(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public AbsSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initAbsSpinner();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AbsSpinner, paramInt, 0);
        CharSequence[] arrayOfCharSequence = localTypedArray.getTextArray(0);
        if (arrayOfCharSequence != null)
        {
            ArrayAdapter localArrayAdapter = new ArrayAdapter(paramContext, 17367048, arrayOfCharSequence);
            localArrayAdapter.setDropDownViewResource(17367049);
            setAdapter(localArrayAdapter);
        }
        localTypedArray.recycle();
    }

    private void initAbsSpinner()
    {
        setFocusable(true);
        setWillNotDraw(false);
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
        return new ViewGroup.LayoutParams(-1, -2);
    }

    public SpinnerAdapter getAdapter()
    {
        return this.mAdapter;
    }

    int getChildHeight(View paramView)
    {
        return paramView.getMeasuredHeight();
    }

    int getChildWidth(View paramView)
    {
        return paramView.getMeasuredWidth();
    }

    public int getCount()
    {
        return this.mItemCount;
    }

    public View getSelectedView()
    {
        if ((this.mItemCount > 0) && (this.mSelectedPosition >= 0));
        for (View localView = getChildAt(this.mSelectedPosition - this.mFirstPosition); ; localView = null)
            return localView;
    }

    abstract void layout(int paramInt, boolean paramBoolean);

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(AbsSpinner.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(AbsSpinner.class.getName());
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        Rect localRect1 = this.mSpinnerPadding;
        int j;
        int k;
        label58: int m;
        label88: Rect localRect4;
        if (this.mPaddingLeft > this.mSelectionLeftPadding)
        {
            j = this.mPaddingLeft;
            localRect1.left = j;
            Rect localRect2 = this.mSpinnerPadding;
            if (this.mPaddingTop <= this.mSelectionTopPadding)
                break label436;
            k = this.mPaddingTop;
            localRect2.top = k;
            Rect localRect3 = this.mSpinnerPadding;
            if (this.mPaddingRight <= this.mSelectionRightPadding)
                break label445;
            m = this.mPaddingRight;
            localRect3.right = m;
            localRect4 = this.mSpinnerPadding;
            if (this.mPaddingBottom <= this.mSelectionBottomPadding)
                break label454;
        }
        label436: label445: label454: for (int n = this.mPaddingBottom; ; n = this.mSelectionBottomPadding)
        {
            localRect4.bottom = n;
            if (this.mDataChanged)
                handleDataChanged();
            int i1 = 0;
            int i2 = 0;
            int i3 = 1;
            int i4 = getSelectedItemPosition();
            if ((i4 >= 0) && (this.mAdapter != null) && (i4 < this.mAdapter.getCount()))
            {
                View localView = this.mRecycler.get(i4);
                if (localView == null)
                {
                    localView = this.mAdapter.getView(i4, null, this);
                    if (localView.getImportantForAccessibility() == 0)
                        localView.setImportantForAccessibility(1);
                }
                if (localView != null)
                    this.mRecycler.put(i4, localView);
                if (localView != null)
                {
                    if (localView.getLayoutParams() == null)
                    {
                        this.mBlockLayoutRequests = true;
                        localView.setLayoutParams(generateDefaultLayoutParams());
                        this.mBlockLayoutRequests = false;
                    }
                    measureChild(localView, paramInt1, paramInt2);
                    i1 = getChildHeight(localView) + this.mSpinnerPadding.top + this.mSpinnerPadding.bottom;
                    i2 = getChildWidth(localView) + this.mSpinnerPadding.left + this.mSpinnerPadding.right;
                    i3 = 0;
                }
            }
            if (i3 != 0)
            {
                i1 = this.mSpinnerPadding.top + this.mSpinnerPadding.bottom;
                if (i == 0)
                    i2 = this.mSpinnerPadding.left + this.mSpinnerPadding.right;
            }
            int i5 = Math.max(i1, getSuggestedMinimumHeight());
            int i6 = Math.max(i2, getSuggestedMinimumWidth());
            int i7 = resolveSizeAndState(i5, paramInt2, 0);
            setMeasuredDimension(resolveSizeAndState(i6, paramInt1, 0), i7);
            this.mHeightMeasureSpec = paramInt2;
            this.mWidthMeasureSpec = paramInt1;
            return;
            j = this.mSelectionLeftPadding;
            break;
            k = this.mSelectionTopPadding;
            break label58;
            m = this.mSelectionRightPadding;
            break label88;
        }
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        if (localSavedState.selectedId >= 0L)
        {
            this.mDataChanged = true;
            this.mNeedSync = true;
            this.mSyncRowId = localSavedState.selectedId;
            this.mSyncPosition = localSavedState.position;
            this.mSyncMode = 0;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState()
    {
        SavedState localSavedState = new SavedState(super.onSaveInstanceState());
        localSavedState.selectedId = getSelectedItemId();
        if (localSavedState.selectedId >= 0L);
        for (localSavedState.position = getSelectedItemPosition(); ; localSavedState.position = -1)
            return localSavedState;
    }

    public int pointToPosition(int paramInt1, int paramInt2)
    {
        Rect localRect = this.mTouchFrame;
        if (localRect == null)
        {
            this.mTouchFrame = new Rect();
            localRect = this.mTouchFrame;
        }
        int i = -1 + getChildCount();
        if (i >= 0)
        {
            View localView = getChildAt(i);
            if (localView.getVisibility() == 0)
            {
                localView.getHitRect(localRect);
                if (!localRect.contains(paramInt1, paramInt2));
            }
        }
        for (int j = i + this.mFirstPosition; ; j = -1)
        {
            return j;
            i--;
            break;
        }
    }

    void recycleAllViews()
    {
        int i = getChildCount();
        RecycleBin localRecycleBin = this.mRecycler;
        int j = this.mFirstPosition;
        for (int k = 0; k < i; k++)
        {
            View localView = getChildAt(k);
            localRecycleBin.put(j + k, localView);
        }
    }

    public void requestLayout()
    {
        if (!this.mBlockLayoutRequests)
            super.requestLayout();
    }

    void resetList()
    {
        this.mDataChanged = false;
        this.mNeedSync = false;
        removeAllViewsInLayout();
        this.mOldSelectedPosition = -1;
        this.mOldSelectedRowId = -9223372036854775808L;
        setSelectedPositionInt(-1);
        setNextSelectedPositionInt(-1);
        invalidate();
    }

    public void setAdapter(SpinnerAdapter paramSpinnerAdapter)
    {
        int i = -1;
        if (this.mAdapter != null)
        {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            resetList();
        }
        this.mAdapter = paramSpinnerAdapter;
        this.mOldSelectedPosition = i;
        this.mOldSelectedRowId = -9223372036854775808L;
        if (this.mAdapter != null)
        {
            this.mOldItemCount = this.mItemCount;
            this.mItemCount = this.mAdapter.getCount();
            checkFocus();
            this.mDataSetObserver = new AdapterView.AdapterDataSetObserver(this);
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            if (this.mItemCount > 0)
                i = 0;
            setSelectedPositionInt(i);
            setNextSelectedPositionInt(i);
            if (this.mItemCount == 0)
                checkSelectionChanged();
        }
        while (true)
        {
            requestLayout();
            return;
            checkFocus();
            resetList();
            checkSelectionChanged();
        }
    }

    public void setSelection(int paramInt)
    {
        setNextSelectedPositionInt(paramInt);
        requestLayout();
        invalidate();
    }

    public void setSelection(int paramInt, boolean paramBoolean)
    {
        if ((paramBoolean) && (this.mFirstPosition <= paramInt) && (paramInt <= -1 + (this.mFirstPosition + getChildCount())));
        for (boolean bool = true; ; bool = false)
        {
            setSelectionInt(paramInt, bool);
            return;
        }
    }

    void setSelectionInt(int paramInt, boolean paramBoolean)
    {
        if (paramInt != this.mOldSelectedPosition)
        {
            this.mBlockLayoutRequests = true;
            int i = paramInt - this.mSelectedPosition;
            setNextSelectedPositionInt(paramInt);
            layout(i, paramBoolean);
            this.mBlockLayoutRequests = false;
        }
    }

    class RecycleBin
    {
        private final SparseArray<View> mScrapHeap = new SparseArray();

        RecycleBin()
        {
        }

        void clear()
        {
            SparseArray localSparseArray = this.mScrapHeap;
            int i = localSparseArray.size();
            for (int j = 0; j < i; j++)
            {
                View localView = (View)localSparseArray.valueAt(j);
                if (localView != null)
                    AbsSpinner.this.removeDetachedView(localView, true);
            }
            localSparseArray.clear();
        }

        View get(int paramInt)
        {
            View localView = (View)this.mScrapHeap.get(paramInt);
            if (localView != null)
                this.mScrapHeap.delete(paramInt);
            return localView;
        }

        public void put(int paramInt, View paramView)
        {
            this.mScrapHeap.put(paramInt, paramView);
        }
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public AbsSpinner.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new AbsSpinner.SavedState(paramAnonymousParcel, null);
            }

            public AbsSpinner.SavedState[] newArray(int paramAnonymousInt)
            {
                return new AbsSpinner.SavedState[paramAnonymousInt];
            }
        };
        int position;
        long selectedId;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.selectedId = paramParcel.readLong();
            this.position = paramParcel.readInt();
        }

        SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public String toString()
        {
            return "AbsSpinner.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.selectedId + " position=" + this.position + "}";
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeLong(this.selectedId);
            paramParcel.writeInt(this.position);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AbsSpinner
 * JD-Core Version:        0.6.2
 */