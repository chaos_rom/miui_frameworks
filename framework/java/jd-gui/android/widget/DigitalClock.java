package android.widget;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.Calendar;

public class DigitalClock extends TextView
{
    private static final String m12 = "h:mm:ss aa";
    private static final String m24 = "k:mm:ss";
    Calendar mCalendar;
    String mFormat;
    private FormatChangeObserver mFormatChangeObserver;
    private Handler mHandler;
    private Runnable mTicker;
    private boolean mTickerStopped = false;

    public DigitalClock(Context paramContext)
    {
        super(paramContext);
        initClock(paramContext);
    }

    public DigitalClock(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        initClock(paramContext);
    }

    private boolean get24HourMode()
    {
        return DateFormat.is24HourFormat(getContext());
    }

    private void initClock(Context paramContext)
    {
        this.mContext.getResources();
        if (this.mCalendar == null)
            this.mCalendar = Calendar.getInstance();
        this.mFormatChangeObserver = new FormatChangeObserver();
        getContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.mFormatChangeObserver);
        setFormat();
    }

    private void setFormat()
    {
        if (get24HourMode());
        for (this.mFormat = "k:mm:ss"; ; this.mFormat = "h:mm:ss aa")
            return;
    }

    protected void onAttachedToWindow()
    {
        this.mTickerStopped = false;
        super.onAttachedToWindow();
        this.mHandler = new Handler();
        this.mTicker = new Runnable()
        {
            public void run()
            {
                if (DigitalClock.this.mTickerStopped);
                while (true)
                {
                    return;
                    DigitalClock.this.mCalendar.setTimeInMillis(System.currentTimeMillis());
                    DigitalClock.this.setText(DateFormat.format(DigitalClock.this.mFormat, DigitalClock.this.mCalendar));
                    DigitalClock.this.invalidate();
                    long l1 = SystemClock.uptimeMillis();
                    long l2 = l1 + (1000L - l1 % 1000L);
                    DigitalClock.this.mHandler.postAtTime(DigitalClock.this.mTicker, l2);
                }
            }
        };
        this.mTicker.run();
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.mTickerStopped = true;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(DigitalClock.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(DigitalClock.class.getName());
    }

    private class FormatChangeObserver extends ContentObserver
    {
        public FormatChangeObserver()
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            DigitalClock.this.setFormat();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.DigitalClock
 * JD-Core Version:        0.6.2
 */