package android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils.TruncateAt;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.MovementMethod;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class EditText extends TextView
{
    public EditText(Context paramContext)
    {
        this(paramContext, null);
    }

    public EditText(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842862);
    }

    public EditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public void extendSelection(int paramInt)
    {
        Selection.extendSelection(getText(), paramInt);
    }

    protected boolean getDefaultEditable()
    {
        return true;
    }

    protected MovementMethod getDefaultMovementMethod()
    {
        return ArrowKeyMovementMethod.getInstance();
    }

    public Editable getText()
    {
        return (Editable)super.getText();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(EditText.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(EditText.class.getName());
    }

    public void selectAll()
    {
        Selection.selectAll(getText());
    }

    public void setEllipsize(TextUtils.TruncateAt paramTruncateAt)
    {
        if (paramTruncateAt == TextUtils.TruncateAt.MARQUEE)
            throw new IllegalArgumentException("EditText cannot use the ellipsize mode TextUtils.TruncateAt.MARQUEE");
        super.setEllipsize(paramTruncateAt);
    }

    public void setSelection(int paramInt)
    {
        Selection.setSelection(getText(), paramInt);
    }

    public void setSelection(int paramInt1, int paramInt2)
    {
        Selection.setSelection(getText(), paramInt1, paramInt2);
    }

    public void setText(CharSequence paramCharSequence, TextView.BufferType paramBufferType)
    {
        super.setText(paramCharSequence, TextView.BufferType.EDITABLE);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.EditText
 * JD-Core Version:        0.6.2
 */