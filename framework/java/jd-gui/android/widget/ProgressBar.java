package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Pool;
import android.util.Poolable;
import android.util.PoolableManager;
import android.util.Pools;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.ViewDebug.ExportedProperty;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import com.android.internal.R.styleable;
import java.util.ArrayList;

@RemoteViews.RemoteView
public class ProgressBar extends View
{
    private static final int MAX_LEVEL = 10000;
    private static final int TIMEOUT_SEND_ACCESSIBILITY_EVENT = 200;
    private AccessibilityEventSender mAccessibilityEventSender;
    private AlphaAnimation mAnimation;
    private boolean mAttached;
    private int mBehavior;
    private Drawable mCurrentDrawable;
    private int mDuration;
    private boolean mHasAnimation;
    private boolean mInDrawing;
    private boolean mIndeterminate;
    private Drawable mIndeterminateDrawable;
    private Interpolator mInterpolator;
    private int mMax;
    int mMaxHeight;
    int mMaxWidth;
    int mMinHeight;
    int mMinWidth;
    private boolean mNoInvalidate;
    private boolean mOnlyIndeterminate;
    private int mProgress;
    private Drawable mProgressDrawable;
    private final ArrayList<RefreshData> mRefreshData = new ArrayList();
    private boolean mRefreshIsPosted;
    private RefreshProgressRunnable mRefreshProgressRunnable;
    Bitmap mSampleTile;
    private int mSecondaryProgress;
    private boolean mShouldStartAnimationDrawable;
    private Transformation mTransformation;
    private long mUiThreadId = Thread.currentThread().getId();

    public ProgressBar(Context paramContext)
    {
        this(paramContext, null);
    }

    public ProgressBar(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842871);
    }

    public ProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        this(paramContext, paramAttributeSet, paramInt, 0);
    }

    public ProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
    {
        super(paramContext, paramAttributeSet, paramInt1);
        initProgressBar();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ProgressBar, paramInt1, paramInt2);
        this.mNoInvalidate = true;
        Drawable localDrawable1 = localTypedArray.getDrawable(8);
        if (localDrawable1 != null)
            setProgressDrawable(tileify(localDrawable1, false));
        this.mDuration = localTypedArray.getInt(9, this.mDuration);
        this.mMinWidth = localTypedArray.getDimensionPixelSize(11, this.mMinWidth);
        this.mMaxWidth = localTypedArray.getDimensionPixelSize(0, this.mMaxWidth);
        this.mMinHeight = localTypedArray.getDimensionPixelSize(12, this.mMinHeight);
        this.mMaxHeight = localTypedArray.getDimensionPixelSize(1, this.mMaxHeight);
        this.mBehavior = localTypedArray.getInt(10, this.mBehavior);
        int i = localTypedArray.getResourceId(13, 17432587);
        if (i > 0)
            setInterpolator(paramContext, i);
        setMax(localTypedArray.getInt(2, this.mMax));
        setProgress(localTypedArray.getInt(3, this.mProgress));
        setSecondaryProgress(localTypedArray.getInt(4, this.mSecondaryProgress));
        Drawable localDrawable2 = localTypedArray.getDrawable(7);
        if (localDrawable2 != null)
            setIndeterminateDrawable(tileifyIndeterminate(localDrawable2));
        this.mOnlyIndeterminate = localTypedArray.getBoolean(6, this.mOnlyIndeterminate);
        this.mNoInvalidate = false;
        if ((this.mOnlyIndeterminate) || (localTypedArray.getBoolean(5, this.mIndeterminate)))
            bool = true;
        setIndeterminate(bool);
        localTypedArray.recycle();
    }

    /** @deprecated */
    private void doRefreshProgress(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        while (true)
        {
            float f;
            Drawable localDrawable2;
            try
            {
                Drawable localDrawable1;
                if (this.mMax > 0)
                {
                    f = paramInt2 / this.mMax;
                    localDrawable1 = this.mCurrentDrawable;
                    if (localDrawable1 != null)
                    {
                        localDrawable2 = null;
                        if (!(localDrawable1 instanceof LayerDrawable))
                            break label111;
                        localDrawable2 = ((LayerDrawable)localDrawable1).findDrawableByLayerId(paramInt1);
                        break label111;
                        localDrawable2.setLevel(i);
                        if ((paramBoolean2) && (paramInt1 == 16908301))
                            onProgressRefresh(f, paramBoolean1);
                    }
                }
                else
                {
                    f = 0.0F;
                    continue;
                    localDrawable2 = localDrawable1;
                    continue;
                }
                invalidate();
                continue;
            }
            finally
            {
            }
            label111: int i = (int)(10000.0F * f);
            if (localDrawable2 == null);
        }
    }

    private void initProgressBar()
    {
        this.mMax = 100;
        this.mProgress = 0;
        this.mSecondaryProgress = 0;
        this.mIndeterminate = false;
        this.mOnlyIndeterminate = false;
        this.mDuration = 4000;
        this.mBehavior = 1;
        this.mMinWidth = 24;
        this.mMaxWidth = 48;
        this.mMinHeight = 24;
        this.mMaxHeight = 48;
    }

    /** @deprecated */
    private void refreshProgress(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        try
        {
            if (this.mUiThreadId == Thread.currentThread().getId())
                doRefreshProgress(paramInt1, paramInt2, paramBoolean, true);
            while (true)
            {
                return;
                if (this.mRefreshProgressRunnable == null)
                    this.mRefreshProgressRunnable = new RefreshProgressRunnable(null);
                RefreshData localRefreshData = RefreshData.obtain(paramInt1, paramInt2, paramBoolean);
                this.mRefreshData.add(localRefreshData);
                if ((this.mAttached) && (!this.mRefreshIsPosted))
                {
                    post(this.mRefreshProgressRunnable);
                    this.mRefreshIsPosted = true;
                }
            }
        }
        finally
        {
        }
    }

    private void scheduleAccessibilityEventSender()
    {
        if (this.mAccessibilityEventSender == null)
            this.mAccessibilityEventSender = new AccessibilityEventSender(null);
        while (true)
        {
            postDelayed(this.mAccessibilityEventSender, 200L);
            return;
            removeCallbacks(this.mAccessibilityEventSender);
        }
    }

    private Drawable tileify(Drawable paramDrawable, boolean paramBoolean)
    {
        Object localObject1;
        if ((paramDrawable instanceof LayerDrawable))
        {
            LayerDrawable localLayerDrawable = (LayerDrawable)paramDrawable;
            int k = localLayerDrawable.getNumberOfLayers();
            Drawable[] arrayOfDrawable = new Drawable[k];
            int m = 0;
            if (m < k)
            {
                int i1 = localLayerDrawable.getId(m);
                Drawable localDrawable = localLayerDrawable.getDrawable(m);
                if ((i1 == 16908301) || (i1 == 16908303));
                for (boolean bool = true; ; bool = false)
                {
                    arrayOfDrawable[m] = tileify(localDrawable, bool);
                    m++;
                    break;
                }
            }
            localObject1 = new LayerDrawable(arrayOfDrawable);
            for (int n = 0; n < k; n++)
                ((LayerDrawable)localObject1).setId(n, localLayerDrawable.getId(n));
        }
        if ((paramDrawable instanceof StateListDrawable))
        {
            StateListDrawable localStateListDrawable1 = (StateListDrawable)paramDrawable;
            StateListDrawable localStateListDrawable2 = new StateListDrawable();
            int i = localStateListDrawable1.getStateCount();
            for (int j = 0; j < i; j++)
                localStateListDrawable2.addState(localStateListDrawable1.getStateSet(j), tileify(localStateListDrawable1.getStateDrawable(j), paramBoolean));
            localObject1 = localStateListDrawable2;
        }
        while (true)
        {
            return localObject1;
            if ((paramDrawable instanceof BitmapDrawable))
            {
                Bitmap localBitmap = ((BitmapDrawable)paramDrawable).getBitmap();
                if (this.mSampleTile == null)
                    this.mSampleTile = localBitmap;
                Object localObject2 = new ShapeDrawable(getDrawableShape());
                BitmapShader localBitmapShader = new BitmapShader(localBitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
                ((ShapeDrawable)localObject2).getPaint().setShader(localBitmapShader);
                if (paramBoolean)
                    localObject2 = new ClipDrawable((Drawable)localObject2, 3, 1);
                localObject1 = localObject2;
            }
            else
            {
                localObject1 = paramDrawable;
            }
        }
    }

    private Drawable tileifyIndeterminate(Drawable paramDrawable)
    {
        if ((paramDrawable instanceof AnimationDrawable))
        {
            AnimationDrawable localAnimationDrawable1 = (AnimationDrawable)paramDrawable;
            int i = localAnimationDrawable1.getNumberOfFrames();
            AnimationDrawable localAnimationDrawable2 = new AnimationDrawable();
            localAnimationDrawable2.setOneShot(localAnimationDrawable1.isOneShot());
            for (int j = 0; j < i; j++)
            {
                Drawable localDrawable = tileify(localAnimationDrawable1.getFrame(j), true);
                localDrawable.setLevel(10000);
                localAnimationDrawable2.addFrame(localDrawable, localAnimationDrawable1.getDuration(j));
            }
            localAnimationDrawable2.setLevel(10000);
            paramDrawable = localAnimationDrawable2;
        }
        return paramDrawable;
    }

    private void updateDrawableBounds(int paramInt1, int paramInt2)
    {
        int i = paramInt1 - this.mPaddingRight - this.mPaddingLeft;
        int j = paramInt2 - this.mPaddingBottom - this.mPaddingTop;
        int k = 0;
        int m = 0;
        float f1;
        if (this.mIndeterminateDrawable != null)
            if ((this.mOnlyIndeterminate) && (!(this.mIndeterminateDrawable instanceof AnimationDrawable)))
            {
                int n = this.mIndeterminateDrawable.getIntrinsicWidth();
                int i1 = this.mIndeterminateDrawable.getIntrinsicHeight();
                f1 = n / i1;
                float f2 = paramInt1 / paramInt2;
                if (f1 != f2)
                {
                    if (f2 <= f1)
                        break label161;
                    int i3 = (int)(f1 * paramInt2);
                    m = (paramInt1 - i3) / 2;
                    i = m + i3;
                }
            }
        while (true)
        {
            this.mIndeterminateDrawable.setBounds(m, k, i, j);
            if (this.mProgressDrawable != null)
                this.mProgressDrawable.setBounds(0, 0, i, j);
            return;
            label161: int i2 = (int)(paramInt1 * (1.0F / f1));
            k = (paramInt2 - i2) / 2;
            j = k + i2;
        }
    }

    private void updateDrawableState()
    {
        int[] arrayOfInt = getDrawableState();
        if ((this.mProgressDrawable != null) && (this.mProgressDrawable.isStateful()))
            this.mProgressDrawable.setState(arrayOfInt);
        if ((this.mIndeterminateDrawable != null) && (this.mIndeterminateDrawable.isStateful()))
            this.mIndeterminateDrawable.setState(arrayOfInt);
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        updateDrawableState();
    }

    Drawable getCurrentDrawable()
    {
        return this.mCurrentDrawable;
    }

    Shape getDrawableShape()
    {
        float[] arrayOfFloat = new float[8];
        arrayOfFloat[0] = 5.0F;
        arrayOfFloat[1] = 5.0F;
        arrayOfFloat[2] = 5.0F;
        arrayOfFloat[3] = 5.0F;
        arrayOfFloat[4] = 5.0F;
        arrayOfFloat[5] = 5.0F;
        arrayOfFloat[6] = 5.0F;
        arrayOfFloat[7] = 5.0F;
        return new RoundRectShape(arrayOfFloat, null, null);
    }

    public Drawable getIndeterminateDrawable()
    {
        return this.mIndeterminateDrawable;
    }

    public Interpolator getInterpolator()
    {
        return this.mInterpolator;
    }

    /** @deprecated */
    @ViewDebug.ExportedProperty(category="progress")
    public int getMax()
    {
        try
        {
            int i = this.mMax;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    @ViewDebug.ExportedProperty(category="progress")
    public int getProgress()
    {
        try
        {
            boolean bool = this.mIndeterminate;
            if (bool);
            for (int i = 0; ; i = this.mProgress)
                return i;
        }
        finally
        {
        }
    }

    public Drawable getProgressDrawable()
    {
        return this.mProgressDrawable;
    }

    public int getResolvedLayoutDirection(Drawable paramDrawable)
    {
        if ((paramDrawable == this.mProgressDrawable) || (paramDrawable == this.mIndeterminateDrawable));
        for (int i = getResolvedLayoutDirection(); ; i = super.getResolvedLayoutDirection(paramDrawable))
            return i;
    }

    /** @deprecated */
    @ViewDebug.ExportedProperty(category="progress")
    public int getSecondaryProgress()
    {
        try
        {
            boolean bool = this.mIndeterminate;
            if (bool);
            for (int i = 0; ; i = this.mSecondaryProgress)
                return i;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public final void incrementProgressBy(int paramInt)
    {
        try
        {
            setProgress(paramInt + this.mProgress);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public final void incrementSecondaryProgressBy(int paramInt)
    {
        try
        {
            setSecondaryProgress(paramInt + this.mSecondaryProgress);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        if (!this.mInDrawing)
        {
            if (!verifyDrawable(paramDrawable))
                break label72;
            Rect localRect = paramDrawable.getBounds();
            int i = this.mScrollX + this.mPaddingLeft;
            int j = this.mScrollY + this.mPaddingTop;
            invalidate(i + localRect.left, j + localRect.top, i + localRect.right, j + localRect.bottom);
        }
        while (true)
        {
            return;
            label72: super.invalidateDrawable(paramDrawable);
        }
    }

    /** @deprecated */
    @ViewDebug.ExportedProperty(category="progress")
    public boolean isIndeterminate()
    {
        try
        {
            boolean bool = this.mIndeterminate;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mProgressDrawable != null)
            this.mProgressDrawable.jumpToCurrentState();
        if (this.mIndeterminateDrawable != null)
            this.mIndeterminateDrawable.jumpToCurrentState();
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (this.mIndeterminate)
            startAnimation();
        if (this.mRefreshData != null);
        try
        {
            int i = this.mRefreshData.size();
            for (int j = 0; j < i; j++)
            {
                RefreshData localRefreshData = (RefreshData)this.mRefreshData.get(j);
                doRefreshProgress(localRefreshData.id, localRefreshData.progress, localRefreshData.fromUser, true);
                localRefreshData.recycle();
            }
            this.mRefreshData.clear();
            this.mAttached = true;
            return;
        }
        finally
        {
        }
    }

    protected void onDetachedFromWindow()
    {
        if (this.mIndeterminate)
            stopAnimation();
        if (this.mRefreshProgressRunnable != null)
            removeCallbacks(this.mRefreshProgressRunnable);
        if ((this.mRefreshProgressRunnable != null) && (this.mRefreshIsPosted))
            removeCallbacks(this.mRefreshProgressRunnable);
        if (this.mAccessibilityEventSender != null)
            removeCallbacks(this.mAccessibilityEventSender);
        super.onDetachedFromWindow();
        this.mAttached = false;
    }

    /** @deprecated */
    protected void onDraw(Canvas paramCanvas)
    {
        try
        {
            super.onDraw(paramCanvas);
            Drawable localDrawable = this.mCurrentDrawable;
            float f;
            if (localDrawable != null)
            {
                paramCanvas.save();
                paramCanvas.translate(this.mPaddingLeft, this.mPaddingTop);
                long l = getDrawingTime();
                if (this.mHasAnimation)
                {
                    this.mAnimation.getTransformation(l, this.mTransformation);
                    f = this.mTransformation.getAlpha();
                }
            }
            try
            {
                this.mInDrawing = true;
                localDrawable.setLevel((int)(10000.0F * f));
                this.mInDrawing = false;
                postInvalidateOnAnimation();
                localDrawable.draw(paramCanvas);
                paramCanvas.restore();
                if ((this.mShouldStartAnimationDrawable) && ((localDrawable instanceof Animatable)))
                {
                    ((Animatable)localDrawable).start();
                    this.mShouldStartAnimationDrawable = false;
                }
                return;
            }
            finally
            {
                localObject2 = finally;
                this.mInDrawing = false;
                throw localObject2;
            }
        }
        finally
        {
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ProgressBar.class.getName());
        paramAccessibilityEvent.setItemCount(this.mMax);
        paramAccessibilityEvent.setCurrentItemIndex(this.mProgress);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ProgressBar.class.getName());
    }

    /** @deprecated */
    protected void onMeasure(int paramInt1, int paramInt2)
    {
        try
        {
            Drawable localDrawable = this.mCurrentDrawable;
            int i = 0;
            int j = 0;
            if (localDrawable != null)
            {
                i = Math.max(this.mMinWidth, Math.min(this.mMaxWidth, localDrawable.getIntrinsicWidth()));
                j = Math.max(this.mMinHeight, Math.min(this.mMaxHeight, localDrawable.getIntrinsicHeight()));
            }
            updateDrawableState();
            int k = i + (this.mPaddingLeft + this.mPaddingRight);
            int m = j + (this.mPaddingTop + this.mPaddingBottom);
            setMeasuredDimension(resolveSizeAndState(k, paramInt1, 0), resolveSizeAndState(m, paramInt2, 0));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void onProgressRefresh(float paramFloat, boolean paramBoolean)
    {
        if (AccessibilityManager.getInstance(this.mContext).isEnabled())
            scheduleAccessibilityEventSender();
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        setProgress(localSavedState.progress);
        setSecondaryProgress(localSavedState.secondaryProgress);
    }

    public Parcelable onSaveInstanceState()
    {
        SavedState localSavedState = new SavedState(super.onSaveInstanceState());
        localSavedState.progress = this.mProgress;
        localSavedState.secondaryProgress = this.mSecondaryProgress;
        return localSavedState;
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        updateDrawableBounds(paramInt1, paramInt2);
    }

    protected void onVisibilityChanged(View paramView, int paramInt)
    {
        super.onVisibilityChanged(paramView, paramInt);
        if (this.mIndeterminate)
        {
            if ((paramInt != 8) && (paramInt != 4))
                break label29;
            stopAnimation();
        }
        while (true)
        {
            return;
            label29: startAnimation();
        }
    }

    public void postInvalidate()
    {
        if (!this.mNoInvalidate)
            super.postInvalidate();
    }

    /** @deprecated */
    @RemotableViewMethod
    public void setIndeterminate(boolean paramBoolean)
    {
        try
        {
            if (((!this.mOnlyIndeterminate) || (!this.mIndeterminate)) && (paramBoolean != this.mIndeterminate))
            {
                this.mIndeterminate = paramBoolean;
                if (!paramBoolean)
                    break label48;
                this.mCurrentDrawable = this.mIndeterminateDrawable;
                startAnimation();
            }
            while (true)
            {
                return;
                label48: this.mCurrentDrawable = this.mProgressDrawable;
                stopAnimation();
            }
        }
        finally
        {
        }
    }

    public void setIndeterminateDrawable(Drawable paramDrawable)
    {
        if (paramDrawable != null)
            paramDrawable.setCallback(this);
        this.mIndeterminateDrawable = paramDrawable;
        if (this.mIndeterminate)
        {
            this.mCurrentDrawable = paramDrawable;
            postInvalidate();
        }
    }

    public void setInterpolator(Context paramContext, int paramInt)
    {
        setInterpolator(AnimationUtils.loadInterpolator(paramContext, paramInt));
    }

    public void setInterpolator(Interpolator paramInterpolator)
    {
        this.mInterpolator = paramInterpolator;
    }

    /** @deprecated */
    @RemotableViewMethod
    public void setMax(int paramInt)
    {
        if (paramInt < 0)
            paramInt = 0;
        try
        {
            if (paramInt != this.mMax)
            {
                this.mMax = paramInt;
                postInvalidate();
                if (this.mProgress > paramInt)
                    this.mProgress = paramInt;
                refreshProgress(16908301, this.mProgress, false);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    @RemotableViewMethod
    public void setProgress(int paramInt)
    {
        try
        {
            setProgress(paramInt, false);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    @RemotableViewMethod
    void setProgress(int paramInt, boolean paramBoolean)
    {
        try
        {
            boolean bool = this.mIndeterminate;
            if (bool);
            while (true)
            {
                return;
                if (paramInt < 0)
                    paramInt = 0;
                if (paramInt > this.mMax)
                    paramInt = this.mMax;
                if (paramInt != this.mProgress)
                {
                    this.mProgress = paramInt;
                    refreshProgress(16908301, this.mProgress, paramBoolean);
                }
            }
        }
        finally
        {
        }
    }

    public void setProgressDrawable(Drawable paramDrawable)
    {
        if ((this.mProgressDrawable != null) && (paramDrawable != this.mProgressDrawable))
            this.mProgressDrawable.setCallback(null);
        for (int i = 1; ; i = 0)
        {
            if (paramDrawable != null)
            {
                paramDrawable.setCallback(this);
                int j = paramDrawable.getMinimumHeight();
                if (this.mMaxHeight < j)
                {
                    this.mMaxHeight = j;
                    requestLayout();
                }
            }
            this.mProgressDrawable = paramDrawable;
            if (!this.mIndeterminate)
            {
                this.mCurrentDrawable = paramDrawable;
                postInvalidate();
            }
            if (i != 0)
            {
                updateDrawableBounds(getWidth(), getHeight());
                updateDrawableState();
                doRefreshProgress(16908301, this.mProgress, false, false);
                doRefreshProgress(16908303, this.mSecondaryProgress, false, false);
            }
            return;
        }
    }

    /** @deprecated */
    @RemotableViewMethod
    public void setSecondaryProgress(int paramInt)
    {
        try
        {
            boolean bool = this.mIndeterminate;
            if (bool);
            while (true)
            {
                return;
                if (paramInt < 0)
                    paramInt = 0;
                if (paramInt > this.mMax)
                    paramInt = this.mMax;
                if (paramInt != this.mSecondaryProgress)
                {
                    this.mSecondaryProgress = paramInt;
                    refreshProgress(16908303, this.mSecondaryProgress, false);
                }
            }
        }
        finally
        {
        }
    }

    @RemotableViewMethod
    public void setVisibility(int paramInt)
    {
        if (getVisibility() != paramInt)
        {
            super.setVisibility(paramInt);
            if (this.mIndeterminate)
            {
                if ((paramInt != 8) && (paramInt != 4))
                    break label36;
                stopAnimation();
            }
        }
        while (true)
        {
            return;
            label36: startAnimation();
        }
    }

    void startAnimation()
    {
        if (getVisibility() != 0);
        while (true)
        {
            return;
            if (!(this.mIndeterminateDrawable instanceof Animatable))
                break;
            this.mShouldStartAnimationDrawable = true;
            this.mHasAnimation = false;
            postInvalidate();
        }
        this.mHasAnimation = true;
        if (this.mInterpolator == null)
            this.mInterpolator = new LinearInterpolator();
        if (this.mTransformation == null)
        {
            this.mTransformation = new Transformation();
            label76: if (this.mAnimation != null)
                break label162;
            this.mAnimation = new AlphaAnimation(0.0F, 1.0F);
        }
        while (true)
        {
            this.mAnimation.setRepeatMode(this.mBehavior);
            this.mAnimation.setRepeatCount(-1);
            this.mAnimation.setDuration(this.mDuration);
            this.mAnimation.setInterpolator(this.mInterpolator);
            this.mAnimation.setStartTime(-1L);
            break;
            this.mTransformation.clear();
            break label76;
            label162: this.mAnimation.reset();
        }
    }

    void stopAnimation()
    {
        this.mHasAnimation = false;
        if ((this.mIndeterminateDrawable instanceof Animatable))
        {
            ((Animatable)this.mIndeterminateDrawable).stop();
            this.mShouldStartAnimationDrawable = false;
        }
        postInvalidate();
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((paramDrawable == this.mProgressDrawable) || (paramDrawable == this.mIndeterminateDrawable) || (super.verifyDrawable(paramDrawable)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private class AccessibilityEventSender
        implements Runnable
    {
        private AccessibilityEventSender()
        {
        }

        public void run()
        {
            ProgressBar.this.sendAccessibilityEvent(4);
        }
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public ProgressBar.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ProgressBar.SavedState(paramAnonymousParcel, null);
            }

            public ProgressBar.SavedState[] newArray(int paramAnonymousInt)
            {
                return new ProgressBar.SavedState[paramAnonymousInt];
            }
        };
        int progress;
        int secondaryProgress;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.progress = paramParcel.readInt();
            this.secondaryProgress = paramParcel.readInt();
        }

        SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.progress);
            paramParcel.writeInt(this.secondaryProgress);
        }
    }

    private static class RefreshData
        implements Poolable<RefreshData>
    {
        private static final int POOL_MAX = 24;
        private static final Pool<RefreshData> sPool = Pools.synchronizedPool(Pools.finitePool(new PoolableManager()
        {
            public ProgressBar.RefreshData newInstance()
            {
                return new ProgressBar.RefreshData(null);
            }

            public void onAcquired(ProgressBar.RefreshData paramAnonymousRefreshData)
            {
            }

            public void onReleased(ProgressBar.RefreshData paramAnonymousRefreshData)
            {
            }
        }
        , 24));
        public boolean fromUser;
        public int id;
        private boolean mIsPooled;
        private RefreshData mNext;
        public int progress;

        public static RefreshData obtain(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            RefreshData localRefreshData = (RefreshData)sPool.acquire();
            localRefreshData.id = paramInt1;
            localRefreshData.progress = paramInt2;
            localRefreshData.fromUser = paramBoolean;
            return localRefreshData;
        }

        public RefreshData getNextPoolable()
        {
            return this.mNext;
        }

        public boolean isPooled()
        {
            return this.mIsPooled;
        }

        public void recycle()
        {
            sPool.release(this);
        }

        public void setNextPoolable(RefreshData paramRefreshData)
        {
            this.mNext = paramRefreshData;
        }

        public void setPooled(boolean paramBoolean)
        {
            this.mIsPooled = paramBoolean;
        }
    }

    private class RefreshProgressRunnable
        implements Runnable
    {
        private RefreshProgressRunnable()
        {
        }

        public void run()
        {
            synchronized (ProgressBar.this)
            {
                int i = ProgressBar.this.mRefreshData.size();
                for (int j = 0; j < i; j++)
                {
                    ProgressBar.RefreshData localRefreshData = (ProgressBar.RefreshData)ProgressBar.this.mRefreshData.get(j);
                    ProgressBar.this.doRefreshProgress(localRefreshData.id, localRefreshData.progress, localRefreshData.fromUser, true);
                    localRefreshData.recycle();
                }
                ProgressBar.this.mRefreshData.clear();
                ProgressBar.access$202(ProgressBar.this, false);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ProgressBar
 * JD-Core Version:        0.6.2
 */