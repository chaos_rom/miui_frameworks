package android.widget;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.util.SparseIntArray;
import java.text.Collator;

public class AlphabetIndexer extends DataSetObserver
    implements SectionIndexer
{
    private SparseIntArray mAlphaMap;
    protected CharSequence mAlphabet;
    private String[] mAlphabetArray;
    private int mAlphabetLength;
    private Collator mCollator;
    protected int mColumnIndex;
    protected Cursor mDataCursor;

    public AlphabetIndexer(Cursor paramCursor, int paramInt, CharSequence paramCharSequence)
    {
        this.mDataCursor = paramCursor;
        this.mColumnIndex = paramInt;
        this.mAlphabet = paramCharSequence;
        this.mAlphabetLength = paramCharSequence.length();
        this.mAlphabetArray = new String[this.mAlphabetLength];
        for (int i = 0; i < this.mAlphabetLength; i++)
            this.mAlphabetArray[i] = Character.toString(this.mAlphabet.charAt(i));
        this.mAlphaMap = new SparseIntArray(this.mAlphabetLength);
        if (paramCursor != null)
            paramCursor.registerDataSetObserver(this);
        this.mCollator = Collator.getInstance();
        this.mCollator.setStrength(0);
    }

    protected int compare(String paramString1, String paramString2)
    {
        if (paramString1.length() == 0);
        for (String str = " "; ; str = paramString1.substring(0, 1))
            return this.mCollator.compare(str, paramString2);
    }

    public int getPositionForSection(int paramInt)
    {
        SparseIntArray localSparseIntArray = this.mAlphaMap;
        Cursor localCursor = this.mDataCursor;
        if ((localCursor == null) || (this.mAlphabet == null))
            i = 0;
        int k;
        int m;
        int n;
        char c;
        String str1;
        do
        {
            while (true)
            {
                return i;
                if (paramInt > 0)
                    break;
                i = 0;
            }
            int j = this.mAlphabetLength;
            if (paramInt >= j)
                paramInt = -1 + this.mAlphabetLength;
            k = localCursor.getPosition();
            m = localCursor.getCount();
            n = 0;
            i1 = m;
            c = this.mAlphabet.charAt(paramInt);
            str1 = Character.toString(c);
            i = localSparseIntArray.get(c, -2147483648);
            if (-2147483648 == i)
                break;
        }
        while (i >= 0);
        int i1 = -i;
        if (paramInt > 0)
        {
            int i3 = localSparseIntArray.get(this.mAlphabet.charAt(paramInt - 1), -2147483648);
            if (i3 != -2147483648)
                n = Math.abs(i3);
        }
        int i = (i1 + n) / 2;
        label173: String str2;
        if (i < i1)
        {
            localCursor.moveToPosition(i);
            str2 = localCursor.getString(this.mColumnIndex);
            if (str2 != null)
                break label237;
            if (i != 0)
                break label231;
        }
        while (true)
        {
            label211: localSparseIntArray.put(c, i);
            localCursor.moveToPosition(k);
            break;
            label231: i--;
            break label173;
            label237: int i2 = compare(str2, str1);
            if (i2 == 0)
                break label293;
            if (i2 >= 0)
                break label277;
            n = i + 1;
            if (n < m)
                break label281;
            i = m;
        }
        label277: for (i1 = i; ; i1 = i)
        {
            label281: i = (n + i1) / 2;
            break;
            label293: if (n == i)
                break label211;
        }
    }

    public int getSectionForPosition(int paramInt)
    {
        int i = this.mDataCursor.getPosition();
        this.mDataCursor.moveToPosition(paramInt);
        String str = this.mDataCursor.getString(this.mColumnIndex);
        this.mDataCursor.moveToPosition(i);
        int j = 0;
        if (j < this.mAlphabetLength)
            if (compare(str, Character.toString(this.mAlphabet.charAt(j))) != 0);
        while (true)
        {
            return j;
            j++;
            break;
            j = 0;
        }
    }

    public Object[] getSections()
    {
        return this.mAlphabetArray;
    }

    public void onChanged()
    {
        super.onChanged();
        this.mAlphaMap.clear();
    }

    public void onInvalidated()
    {
        super.onInvalidated();
        this.mAlphaMap.clear();
    }

    public void setCursor(Cursor paramCursor)
    {
        if (this.mDataCursor != null)
            this.mDataCursor.unregisterDataSetObserver(this);
        this.mDataCursor = paramCursor;
        if (paramCursor != null)
            this.mDataCursor.registerDataSetObserver(this);
        this.mAlphaMap.clear();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AlphabetIndexer
 * JD-Core Version:        0.6.2
 */