package android.widget;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageParser.Package;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AppSecurityPermissions
    implements View.OnClickListener
{
    private static final String TAG = "AppSecurityPermissions";
    private boolean localLOGV = false;
    private Context mContext;
    private State mCurrentState;
    private Drawable mDangerousIcon;
    private LinearLayout mDangerousList;
    private Map<String, String> mDangerousMap;
    private String mDefaultGrpLabel;
    private String mDefaultGrpName = "DefaultGrp";
    private boolean mExpanded;
    private HashMap<String, CharSequence> mGroupLabelCache;
    private LayoutInflater mInflater;
    private View mNoPermsView;
    private LinearLayout mNonDangerousList;
    private Drawable mNormalIcon;
    private Map<String, String> mNormalMap;
    private String mPermFormat;
    private List<PermissionInfo> mPermsList;
    private LinearLayout mPermsView;
    private PackageManager mPm;
    private Drawable mShowMaxIcon;
    private Drawable mShowMinIcon;
    private View mShowMore;
    private ImageView mShowMoreIcon;
    private TextView mShowMoreText;

    public AppSecurityPermissions(Context paramContext, PackageParser.Package paramPackage)
    {
        this.mContext = paramContext;
        this.mPm = this.mContext.getPackageManager();
        this.mPermsList = new ArrayList();
        HashSet localHashSet = new HashSet();
        if (paramPackage == null);
        while (true)
        {
            return;
            if (paramPackage.requestedPermissions != null)
            {
                ArrayList localArrayList = paramPackage.requestedPermissions;
                int i = localArrayList.size();
                if (i > 0)
                    extractPerms((String[])localArrayList.toArray(new String[i]), localHashSet);
            }
            if (paramPackage.mSharedUserId != null);
            try
            {
                getAllUsedPermissions(this.mPm.getUidForSharedUser(paramPackage.mSharedUserId), localHashSet);
                Iterator localIterator = localHashSet.iterator();
                while (localIterator.hasNext())
                {
                    PermissionInfo localPermissionInfo = (PermissionInfo)localIterator.next();
                    this.mPermsList.add(localPermissionInfo);
                }
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                while (true)
                    Log.w("AppSecurityPermissions", "Could'nt retrieve shared user id for:" + paramPackage.packageName);
            }
        }
    }

    public AppSecurityPermissions(Context paramContext, String paramString)
    {
        this.mContext = paramContext;
        this.mPm = this.mContext.getPackageManager();
        this.mPermsList = new ArrayList();
        HashSet localHashSet = new HashSet();
        try
        {
            PackageInfo localPackageInfo = this.mPm.getPackageInfo(paramString, 4096);
            if ((localPackageInfo.applicationInfo != null) && (localPackageInfo.applicationInfo.uid != -1))
                getAllUsedPermissions(localPackageInfo.applicationInfo.uid, localHashSet);
            Iterator localIterator = localHashSet.iterator();
            while (localIterator.hasNext())
            {
                PermissionInfo localPermissionInfo = (PermissionInfo)localIterator.next();
                this.mPermsList.add(localPermissionInfo);
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            Log.w("AppSecurityPermissions", "Could'nt retrieve permissions for package:" + paramString);
        }
    }

    public AppSecurityPermissions(Context paramContext, List<PermissionInfo> paramList)
    {
        this.mContext = paramContext;
        this.mPm = this.mContext.getPackageManager();
        this.mPermsList = paramList;
    }

    private void aggregateGroupDescs(Map<String, List<PermissionInfo>> paramMap, Map<String, String> paramMap1)
    {
        if (paramMap == null);
        while (true)
        {
            return;
            if (paramMap1 != null)
            {
                Iterator localIterator1 = paramMap.keySet().iterator();
                while (localIterator1.hasNext())
                {
                    String str1 = null;
                    String str2 = (String)localIterator1.next();
                    List localList = (List)paramMap.get(str2);
                    if (localList != null)
                    {
                        Iterator localIterator2 = localList.iterator();
                        while (localIterator2.hasNext())
                            str1 = formatPermissions(str1, ((PermissionInfo)localIterator2.next()).loadLabel(this.mPm));
                        if (str1 != null)
                        {
                            if (this.localLOGV)
                                Log.i("AppSecurityPermissions", "Group:" + str2 + " description:" + str1.toString());
                            paramMap1.put(str2, str1.toString());
                        }
                    }
                }
            }
        }
    }

    private String canonicalizeGroupDesc(String paramString)
    {
        if ((paramString == null) || (paramString.length() == 0));
        for (String str = null; ; str = paramString)
        {
            return str;
            int i = paramString.length();
            if (paramString.charAt(i - 1) == '.')
                paramString = paramString.substring(0, i - 1);
        }
    }

    private void displayNoPermissions()
    {
        this.mNoPermsView.setVisibility(0);
    }

    private void displayPermissions(boolean paramBoolean)
    {
        Map localMap;
        if (paramBoolean)
        {
            localMap = this.mDangerousMap;
            if (!paramBoolean)
                break label152;
        }
        label152: for (LinearLayout localLinearLayout = this.mDangerousList; ; localLinearLayout = this.mNonDangerousList)
        {
            localLinearLayout.removeAllViews();
            Iterator localIterator = localMap.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                CharSequence localCharSequence = getGroupLabel(str);
                if (this.localLOGV)
                    Log.i("AppSecurityPermissions", "Adding view group:" + localCharSequence + ", desc:" + (String)localMap.get(str));
                localLinearLayout.addView(getPermissionItemView(localCharSequence, (CharSequence)localMap.get(str), paramBoolean));
            }
            localMap = this.mNormalMap;
            break;
        }
    }

    private void extractPerms(String[] paramArrayOfString, Set<PermissionInfo> paramSet)
    {
        if ((paramArrayOfString == null) || (paramArrayOfString.length == 0))
            return;
        int i = paramArrayOfString.length;
        int j = 0;
        while (j < i)
        {
            String str = paramArrayOfString[j];
            try
            {
                PermissionInfo localPermissionInfo = this.mPm.getPermissionInfo(str, 0);
                if (localPermissionInfo != null)
                    paramSet.add(localPermissionInfo);
                j++;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                while (true)
                    Log.i("AppSecurityPermissions", "Ignoring unknown permission:" + str);
            }
        }
    }

    private String formatPermissions(String paramString, CharSequence paramCharSequence)
    {
        Object localObject;
        if (paramString == null)
            if (paramCharSequence == null)
                localObject = null;
        while (true)
        {
            return localObject;
            localObject = paramCharSequence.toString();
            continue;
            String str1 = canonicalizeGroupDesc(paramString);
            if (paramCharSequence == null)
            {
                localObject = str1;
            }
            else
            {
                String str2 = this.mPermFormat;
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = str1;
                arrayOfObject[1] = paramCharSequence.toString();
                localObject = String.format(str2, arrayOfObject);
            }
        }
    }

    private void getAllUsedPermissions(int paramInt, Set<PermissionInfo> paramSet)
    {
        String[] arrayOfString = this.mPm.getPackagesForUid(paramInt);
        if ((arrayOfString == null) || (arrayOfString.length == 0));
        while (true)
        {
            return;
            int i = arrayOfString.length;
            for (int j = 0; j < i; j++)
                getPermissionsForPackage(arrayOfString[j], paramSet);
        }
    }

    private CharSequence getGroupLabel(String paramString)
    {
        Object localObject;
        if (paramString == null)
            localObject = this.mDefaultGrpLabel;
        while (true)
        {
            return localObject;
            localObject = (CharSequence)this.mGroupLabelCache.get(paramString);
            if (localObject == null)
                try
                {
                    PermissionGroupInfo localPermissionGroupInfo = this.mPm.getPermissionGroupInfo(paramString, 0);
                    String str = localPermissionGroupInfo.loadLabel(this.mPm).toString();
                    this.mGroupLabelCache.put(paramString, str);
                    localObject = str;
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    Log.i("AppSecurityPermissions", "Invalid group name:" + paramString);
                    localObject = null;
                }
        }
    }

    private static View getPermissionItemView(Context paramContext, LayoutInflater paramLayoutInflater, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean, Drawable paramDrawable)
    {
        View localView = paramLayoutInflater.inflate(17367081, null);
        TextView localTextView1 = (TextView)localView.findViewById(16908894);
        TextView localTextView2 = (TextView)localView.findViewById(16908895);
        ((ImageView)localView.findViewById(16908893)).setImageDrawable(paramDrawable);
        if (paramCharSequence1 != null)
        {
            localTextView1.setText(paramCharSequence1);
            localTextView2.setText(paramCharSequence2);
        }
        while (true)
        {
            return localView;
            localTextView1.setText(paramCharSequence2);
            localTextView2.setVisibility(8);
        }
    }

    public static View getPermissionItemView(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
    {
        LayoutInflater localLayoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
        Resources localResources = paramContext.getResources();
        if (paramBoolean);
        for (int i = 17302195; ; i = 17302364)
            return getPermissionItemView(paramContext, localLayoutInflater, paramCharSequence1, paramCharSequence2, paramBoolean, localResources.getDrawable(i));
    }

    private View getPermissionItemView(CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
    {
        Context localContext = this.mContext;
        LayoutInflater localLayoutInflater = this.mInflater;
        if (paramBoolean);
        for (Drawable localDrawable = this.mDangerousIcon; ; localDrawable = this.mNormalIcon)
            return getPermissionItemView(localContext, localLayoutInflater, paramCharSequence1, paramCharSequence2, paramBoolean, localDrawable);
    }

    private void getPermissionsForPackage(String paramString, Set<PermissionInfo> paramSet)
    {
        try
        {
            PackageInfo localPackageInfo = this.mPm.getPackageInfo(paramString, 4096);
            if ((localPackageInfo != null) && (localPackageInfo.requestedPermissions != null))
                extractPerms(localPackageInfo.requestedPermissions, paramSet);
            return;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
                Log.w("AppSecurityPermissions", "Could'nt retrieve permissions for package:" + paramString);
        }
    }

    private boolean isDisplayablePermission(PermissionInfo paramPermissionInfo)
    {
        int i = 1;
        if ((paramPermissionInfo.protectionLevel == i) || (paramPermissionInfo.protectionLevel == 0));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void setPermissions(List<PermissionInfo> paramList)
    {
        this.mGroupLabelCache = new HashMap();
        this.mGroupLabelCache.put(this.mDefaultGrpName, this.mDefaultGrpLabel);
        this.mDangerousMap = new HashMap();
        this.mNormalMap = new HashMap();
        HashMap localHashMap1 = new HashMap();
        HashMap localHashMap2 = new HashMap();
        PermissionInfoComparator localPermissionInfoComparator = new PermissionInfoComparator(this.mPm);
        if (paramList != null)
        {
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
                PermissionInfo localPermissionInfo = (PermissionInfo)localIterator.next();
                if (this.localLOGV)
                    Log.i("AppSecurityPermissions", "Processing permission:" + localPermissionInfo.name);
                if (!isDisplayablePermission(localPermissionInfo))
                {
                    if (this.localLOGV)
                        Log.i("AppSecurityPermissions", "Permission:" + localPermissionInfo.name + " is not displayable");
                }
                else
                {
                    HashMap localHashMap3;
                    if (localPermissionInfo.protectionLevel == 1)
                    {
                        localHashMap3 = localHashMap1;
                        label217: if (localPermissionInfo.group != null)
                            break label339;
                    }
                    List localList;
                    label339: for (String str = this.mDefaultGrpName; ; str = localPermissionInfo.group)
                    {
                        if (this.localLOGV)
                            Log.i("AppSecurityPermissions", "Permission:" + localPermissionInfo.name + " belongs to group:" + str);
                        localList = (List)localHashMap3.get(str);
                        if (localList != null)
                            break label349;
                        ArrayList localArrayList = new ArrayList();
                        localHashMap3.put(str, localArrayList);
                        localArrayList.add(localPermissionInfo);
                        break;
                        localHashMap3 = localHashMap2;
                        break label217;
                    }
                    label349: int i = Collections.binarySearch(localList, localPermissionInfo, localPermissionInfoComparator);
                    if (this.localLOGV)
                        Log.i("AppSecurityPermissions", "idx=" + i + ", list.size=" + localList.size());
                    if (i < 0)
                        localList.add(-1 + -i, localPermissionInfo);
                }
            }
            aggregateGroupDescs(localHashMap1, this.mDangerousMap);
            aggregateGroupDescs(localHashMap2, this.mNormalMap);
        }
        this.mCurrentState = State.NO_PERMS;
        State localState;
        if (this.mDangerousMap.size() > 0)
            if (this.mNormalMap.size() > 0)
            {
                localState = State.BOTH;
                this.mCurrentState = localState;
            }
        while (true)
        {
            if (this.localLOGV)
                Log.i("AppSecurityPermissions", "mCurrentState=" + this.mCurrentState);
            showPermissions();
            return;
            localState = State.DANGEROUS_ONLY;
            break;
            if (this.mNormalMap.size() > 0)
                this.mCurrentState = State.NORMAL_ONLY;
        }
    }

    private void showPermissions()
    {
        switch (1.$SwitchMap$android$widget$AppSecurityPermissions$State[this.mCurrentState.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
            while (true)
            {
                return;
                displayNoPermissions();
                continue;
                displayPermissions(true);
                continue;
                displayPermissions(false);
            }
        case 4:
        }
        displayPermissions(true);
        if (this.mExpanded)
        {
            displayPermissions(false);
            this.mShowMoreIcon.setImageDrawable(this.mShowMaxIcon);
            this.mShowMoreText.setText(17040423);
            this.mNonDangerousList.setVisibility(0);
        }
        while (true)
        {
            this.mShowMore.setVisibility(0);
            break;
            this.mShowMoreIcon.setImageDrawable(this.mShowMinIcon);
            this.mShowMoreText.setText(17040424);
            this.mNonDangerousList.setVisibility(8);
        }
    }

    public int getPermissionCount()
    {
        return this.mPermsList.size();
    }

    public View getPermissionsView()
    {
        this.mInflater = ((LayoutInflater)this.mContext.getSystemService("layout_inflater"));
        this.mPermsView = ((LinearLayout)this.mInflater.inflate(17367083, null));
        this.mShowMore = this.mPermsView.findViewById(16908898);
        this.mShowMoreIcon = ((ImageView)this.mShowMore.findViewById(16908900));
        this.mShowMoreText = ((TextView)this.mShowMore.findViewById(16908899));
        this.mDangerousList = ((LinearLayout)this.mPermsView.findViewById(16908897));
        this.mNonDangerousList = ((LinearLayout)this.mPermsView.findViewById(16908901));
        this.mNoPermsView = this.mPermsView.findViewById(16908896);
        this.mShowMore.setClickable(true);
        this.mShowMore.setOnClickListener(this);
        this.mShowMore.setFocusable(true);
        this.mDefaultGrpLabel = this.mContext.getString(17040420);
        this.mPermFormat = this.mContext.getString(17040421);
        this.mNormalIcon = this.mContext.getResources().getDrawable(17302364);
        this.mDangerousIcon = this.mContext.getResources().getDrawable(17302195);
        this.mShowMaxIcon = this.mContext.getResources().getDrawable(17302127);
        this.mShowMinIcon = this.mContext.getResources().getDrawable(17302134);
        setPermissions(this.mPermsList);
        return this.mPermsView;
    }

    public void onClick(View paramView)
    {
        if (this.localLOGV)
            Log.i("AppSecurityPermissions", "mExpanded=" + this.mExpanded);
        if (!this.mExpanded);
        for (boolean bool = true; ; bool = false)
        {
            this.mExpanded = bool;
            showPermissions();
            return;
        }
    }

    private static class PermissionInfoComparator
        implements Comparator<PermissionInfo>
    {
        private PackageManager mPm;
        private final Collator sCollator = Collator.getInstance();

        PermissionInfoComparator(PackageManager paramPackageManager)
        {
            this.mPm = paramPackageManager;
        }

        public final int compare(PermissionInfo paramPermissionInfo1, PermissionInfo paramPermissionInfo2)
        {
            CharSequence localCharSequence1 = paramPermissionInfo1.loadLabel(this.mPm);
            CharSequence localCharSequence2 = paramPermissionInfo2.loadLabel(this.mPm);
            return this.sCollator.compare(localCharSequence1, localCharSequence2);
        }
    }

    private static enum State
    {
        static
        {
            DANGEROUS_ONLY = new State("DANGEROUS_ONLY", 1);
            NORMAL_ONLY = new State("NORMAL_ONLY", 2);
            BOTH = new State("BOTH", 3);
            State[] arrayOfState = new State[4];
            arrayOfState[0] = NO_PERMS;
            arrayOfState[1] = DANGEROUS_ONLY;
            arrayOfState[2] = NORMAL_ONLY;
            arrayOfState[3] = BOTH;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AppSecurityPermissions
 * JD-Core Version:        0.6.2
 */