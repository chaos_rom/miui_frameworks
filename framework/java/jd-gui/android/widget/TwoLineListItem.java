package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class TwoLineListItem extends RelativeLayout
{
    private TextView mText1;
    private TextView mText2;

    public TwoLineListItem(Context paramContext)
    {
        this(paramContext, null, 0);
    }

    public TwoLineListItem(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public TwoLineListItem(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TwoLineListItem, paramInt, 0).recycle();
    }

    public TextView getText1()
    {
        return this.mText1;
    }

    public TextView getText2()
    {
        return this.mText2;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        this.mText1 = ((TextView)findViewById(16908308));
        this.mText2 = ((TextView)findViewById(16908309));
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TwoLineListItem.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TwoLineListItem.class.getName());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TwoLineListItem
 * JD-Core Version:        0.6.2
 */