package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.Resources.Theme;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import java.util.WeakHashMap;

class SuggestionsAdapter extends ResourceCursorAdapter
    implements View.OnClickListener
{
    private static final boolean DBG = false;
    private static final long DELETE_KEY_POST_DELAY = 500L;
    static final int INVALID_INDEX = -1;
    private static final String LOG_TAG = "SuggestionsAdapter";
    private static final int QUERY_LIMIT = 50;
    static final int REFINE_ALL = 2;
    static final int REFINE_BY_ENTRY = 1;
    static final int REFINE_NONE;
    private boolean mClosed = false;
    private int mFlagsCol = -1;
    private int mIconName1Col = -1;
    private int mIconName2Col = -1;
    private WeakHashMap<String, Drawable.ConstantState> mOutsideDrawablesCache;
    private Context mProviderContext;
    private int mQueryRefinement = 1;
    private SearchManager mSearchManager = (SearchManager)this.mContext.getSystemService("search");
    private SearchView mSearchView;
    private SearchableInfo mSearchable;
    private int mText1Col = -1;
    private int mText2Col = -1;
    private int mText2UrlCol = -1;
    private ColorStateList mUrlColor;

    public SuggestionsAdapter(Context paramContext, SearchView paramSearchView, SearchableInfo paramSearchableInfo, WeakHashMap<String, Drawable.ConstantState> paramWeakHashMap)
    {
        super(paramContext, 17367201, null, true);
        this.mSearchView = paramSearchView;
        this.mSearchable = paramSearchableInfo;
        Context localContext = this.mSearchable.getActivityContext(this.mContext);
        this.mProviderContext = this.mSearchable.getProviderContext(this.mContext, localContext);
        this.mOutsideDrawablesCache = paramWeakHashMap;
        getFilter().setDelayer(new Filter.Delayer()
        {
            private int mPreviousLength = 0;

            public long getPostingDelay(CharSequence paramAnonymousCharSequence)
            {
                long l = 0L;
                if (paramAnonymousCharSequence == null);
                while (true)
                {
                    return l;
                    if (paramAnonymousCharSequence.length() < this.mPreviousLength)
                        l = 500L;
                    this.mPreviousLength = paramAnonymousCharSequence.length();
                }
            }
        });
    }

    private Drawable checkIconCache(String paramString)
    {
        Drawable.ConstantState localConstantState = (Drawable.ConstantState)this.mOutsideDrawablesCache.get(paramString);
        if (localConstantState == null);
        for (Drawable localDrawable = null; ; localDrawable = localConstantState.newDrawable())
            return localDrawable;
    }

    private CharSequence formatUrl(CharSequence paramCharSequence)
    {
        if (this.mUrlColor == null)
        {
            TypedValue localTypedValue = new TypedValue();
            this.mContext.getTheme().resolveAttribute(16843367, localTypedValue, true);
            this.mUrlColor = this.mContext.getResources().getColorStateList(localTypedValue.resourceId);
        }
        SpannableString localSpannableString = new SpannableString(paramCharSequence);
        localSpannableString.setSpan(new TextAppearanceSpan(null, 0, 0, this.mUrlColor, null), 0, paramCharSequence.length(), 33);
        return localSpannableString;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private Drawable getActivityIcon(ComponentName paramComponentName)
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        try
        {
            localActivityInfo = localPackageManager.getActivityInfo(paramComponentName, 128);
            i = localActivityInfo.getIconResource();
            if (i == 0)
            {
                localDrawable = null;
                return localDrawable;
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
            {
                ActivityInfo localActivityInfo;
                int i;
                Log.w("SuggestionsAdapter", localNameNotFoundException.toString());
                Drawable localDrawable = null;
                continue;
                localDrawable = Injector.getDrawable(localPackageManager, paramComponentName.getPackageName(), i, localActivityInfo);
                if (localDrawable == null)
                {
                    Log.w("SuggestionsAdapter", "Invalid icon resource " + i + " for " + paramComponentName.flattenToShortString());
                    localDrawable = null;
                }
            }
        }
    }

    private Drawable getActivityIconWithCache(ComponentName paramComponentName)
    {
        Object localObject1 = null;
        String str = paramComponentName.flattenToShortString();
        if (this.mOutsideDrawablesCache.containsKey(str))
        {
            Drawable.ConstantState localConstantState = (Drawable.ConstantState)this.mOutsideDrawablesCache.get(str);
            if (localConstantState == null);
            while (true)
            {
                return localObject1;
                localObject1 = localConstantState.newDrawable(this.mProviderContext.getResources());
            }
        }
        Drawable localDrawable = getActivityIcon(paramComponentName);
        if (localDrawable == null);
        for (Object localObject2 = null; ; localObject2 = localDrawable.getConstantState())
        {
            this.mOutsideDrawablesCache.put(str, localObject2);
            localObject1 = localDrawable;
            break;
        }
    }

    public static String getColumnString(Cursor paramCursor, String paramString)
    {
        return getStringOrNull(paramCursor, paramCursor.getColumnIndex(paramString));
    }

    private Drawable getDefaultIcon1(Cursor paramCursor)
    {
        Drawable localDrawable = getActivityIconWithCache(this.mSearchable.getSearchActivity());
        if (localDrawable != null);
        while (true)
        {
            return localDrawable;
            localDrawable = this.mContext.getPackageManager().getDefaultActivityIcon();
        }
    }

    // ERROR //
    private Drawable getDrawable(Uri paramUri)
    {
        // Byte code:
        //     0: ldc_w 305
        //     3: aload_1
        //     4: invokevirtual 310	android/net/Uri:getScheme	()Ljava/lang/String;
        //     7: invokevirtual 315	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     10: ifeq +114 -> 124
        //     13: aload_0
        //     14: getfield 111	android/widget/SuggestionsAdapter:mProviderContext	Landroid/content/Context;
        //     17: invokevirtual 319	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     20: aload_1
        //     21: invokevirtual 325	android/content/ContentResolver:getResourceId	(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;
        //     24: astore 12
        //     26: aload 12
        //     28: getfield 331	android/content/ContentResolver$OpenResourceIdResult:r	Landroid/content/res/Resources;
        //     31: aload 12
        //     33: getfield 334	android/content/ContentResolver$OpenResourceIdResult:id	I
        //     36: invokevirtual 337	android/content/res/Resources:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     39: astore 14
        //     41: aload 14
        //     43: astore 4
        //     45: aload 4
        //     47: areturn
        //     48: astore 13
        //     50: new 299	java/io/FileNotFoundException
        //     53: dup
        //     54: new 237	java/lang/StringBuilder
        //     57: dup
        //     58: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     61: ldc_w 339
        //     64: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: aload_1
        //     68: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     71: invokevirtual 253	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     74: invokespecial 345	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     77: athrow
        //     78: astore_2
        //     79: ldc 28
        //     81: new 237	java/lang/StringBuilder
        //     84: dup
        //     85: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     88: ldc_w 347
        //     91: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     94: aload_1
        //     95: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     98: ldc_w 349
        //     101: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     104: aload_2
        //     105: invokevirtual 352	java/io/FileNotFoundException:getMessage	()Ljava/lang/String;
        //     108: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     111: invokevirtual 253	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     114: invokestatic 226	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     117: pop
        //     118: aconst_null
        //     119: astore 4
        //     121: goto -76 -> 45
        //     124: aload_0
        //     125: getfield 111	android/widget/SuggestionsAdapter:mProviderContext	Landroid/content/Context;
        //     128: invokevirtual 319	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     131: aload_1
        //     132: invokevirtual 356	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
        //     135: astore 5
        //     137: aload 5
        //     139: ifnonnull +31 -> 170
        //     142: new 299	java/io/FileNotFoundException
        //     145: dup
        //     146: new 237	java/lang/StringBuilder
        //     149: dup
        //     150: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     153: ldc_w 358
        //     156: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: aload_1
        //     160: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     163: invokevirtual 253	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     166: invokespecial 345	java/io/FileNotFoundException:<init>	(Ljava/lang/String;)V
        //     169: athrow
        //     170: aload 5
        //     172: aconst_null
        //     173: invokestatic 362	android/graphics/drawable/Drawable:createFromStream	(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
        //     176: astore 9
        //     178: aload 9
        //     180: astore 4
        //     182: aload 5
        //     184: invokevirtual 367	java/io/InputStream:close	()V
        //     187: goto -142 -> 45
        //     190: astore 10
        //     192: ldc 28
        //     194: new 237	java/lang/StringBuilder
        //     197: dup
        //     198: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     201: ldc_w 369
        //     204: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     207: aload_1
        //     208: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     211: invokevirtual 253	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     214: aload 10
        //     216: invokestatic 373	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     219: pop
        //     220: goto -175 -> 45
        //     223: astore 6
        //     225: aload 5
        //     227: invokevirtual 367	java/io/InputStream:close	()V
        //     230: aload 6
        //     232: athrow
        //     233: astore 7
        //     235: ldc 28
        //     237: new 237	java/lang/StringBuilder
        //     240: dup
        //     241: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     244: ldc_w 369
        //     247: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     250: aload_1
        //     251: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     254: invokevirtual 253	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     257: aload 7
        //     259: invokestatic 373	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     262: pop
        //     263: goto -33 -> 230
        //
        // Exception table:
        //     from	to	target	type
        //     26	41	48	android/content/res/Resources$NotFoundException
        //     0	26	78	java/io/FileNotFoundException
        //     26	41	78	java/io/FileNotFoundException
        //     50	78	78	java/io/FileNotFoundException
        //     124	170	78	java/io/FileNotFoundException
        //     182	187	78	java/io/FileNotFoundException
        //     192	220	78	java/io/FileNotFoundException
        //     225	230	78	java/io/FileNotFoundException
        //     230	263	78	java/io/FileNotFoundException
        //     182	187	190	java/io/IOException
        //     170	178	223	finally
        //     225	230	233	java/io/IOException
    }

    private Drawable getDrawableFromResourceValue(String paramString)
    {
        Drawable localDrawable;
        if ((paramString == null) || (paramString.length() == 0) || ("0".equals(paramString)))
            localDrawable = null;
        while (true)
        {
            return localDrawable;
            try
            {
                int i = Integer.parseInt(paramString);
                String str = "android.resource://" + this.mProviderContext.getPackageName() + "/" + i;
                localDrawable = checkIconCache(str);
                if (localDrawable == null)
                {
                    localDrawable = this.mProviderContext.getResources().getDrawable(i);
                    storeInIconCache(str, localDrawable);
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                localDrawable = checkIconCache(paramString);
                if (localDrawable == null)
                {
                    localDrawable = getDrawable(Uri.parse(paramString));
                    storeInIconCache(paramString, localDrawable);
                }
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                Log.w("SuggestionsAdapter", "Icon resource not found: " + paramString);
                localDrawable = null;
            }
        }
    }

    private Drawable getIcon1(Cursor paramCursor)
    {
        Drawable localDrawable;
        if (this.mIconName1Col == -1)
            localDrawable = null;
        while (true)
        {
            return localDrawable;
            localDrawable = getDrawableFromResourceValue(paramCursor.getString(this.mIconName1Col));
            if (localDrawable == null)
                localDrawable = getDefaultIcon1(paramCursor);
        }
    }

    private Drawable getIcon2(Cursor paramCursor)
    {
        if (this.mIconName2Col == -1);
        for (Drawable localDrawable = null; ; localDrawable = getDrawableFromResourceValue(paramCursor.getString(this.mIconName2Col)))
            return localDrawable;
    }

    private static String getStringOrNull(Cursor paramCursor, int paramInt)
    {
        Object localObject = null;
        if (paramInt == -1);
        while (true)
        {
            return localObject;
            try
            {
                String str = paramCursor.getString(paramInt);
                localObject = str;
            }
            catch (Exception localException)
            {
                Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", localException);
            }
        }
    }

    private void setViewDrawable(ImageView paramImageView, Drawable paramDrawable, int paramInt)
    {
        paramImageView.setImageDrawable(paramDrawable);
        if (paramDrawable == null)
            paramImageView.setVisibility(paramInt);
        while (true)
        {
            return;
            paramImageView.setVisibility(0);
            paramDrawable.setVisible(false, false);
            paramDrawable.setVisible(true, false);
        }
    }

    private void setViewText(TextView paramTextView, CharSequence paramCharSequence)
    {
        paramTextView.setText(paramCharSequence);
        if (TextUtils.isEmpty(paramCharSequence))
            paramTextView.setVisibility(8);
        while (true)
        {
            return;
            paramTextView.setVisibility(0);
        }
    }

    private void storeInIconCache(String paramString, Drawable paramDrawable)
    {
        if (paramDrawable != null)
            this.mOutsideDrawablesCache.put(paramString, paramDrawable.getConstantState());
    }

    private void updateSpinnerState(Cursor paramCursor)
    {
        if (paramCursor != null);
        for (Bundle localBundle = paramCursor.getExtras(); ; localBundle = null)
        {
            if ((localBundle != null) && (localBundle.getBoolean("in_progress")));
            return;
        }
    }

    public void bindView(View paramView, Context paramContext, Cursor paramCursor)
    {
        ChildViewCache localChildViewCache = (ChildViewCache)paramView.getTag();
        int i = 0;
        if (this.mFlagsCol != -1)
            i = paramCursor.getInt(this.mFlagsCol);
        if (localChildViewCache.mText1 != null)
        {
            String str2 = getStringOrNull(paramCursor, this.mText1Col);
            setViewText(localChildViewCache.mText1, str2);
        }
        Object localObject;
        if (localChildViewCache.mText2 != null)
        {
            String str1 = getStringOrNull(paramCursor, this.mText2UrlCol);
            if (str1 != null)
            {
                localObject = formatUrl(str1);
                if (!TextUtils.isEmpty((CharSequence)localObject))
                    break label256;
                if (localChildViewCache.mText1 != null)
                {
                    localChildViewCache.mText1.setSingleLine(false);
                    localChildViewCache.mText1.setMaxLines(2);
                }
                label127: setViewText(localChildViewCache.mText2, (CharSequence)localObject);
            }
        }
        else
        {
            if (localChildViewCache.mIcon1 != null)
                setViewDrawable(localChildViewCache.mIcon1, getIcon1(paramCursor), 4);
            if (localChildViewCache.mIcon2 != null)
                setViewDrawable(localChildViewCache.mIcon2, getIcon2(paramCursor), 8);
            if ((this.mQueryRefinement != 2) && ((this.mQueryRefinement != 1) || ((i & 0x1) == 0)))
                break label285;
            localChildViewCache.mIconRefine.setVisibility(0);
            localChildViewCache.mIconRefine.setTag(localChildViewCache.mText1.getText());
            localChildViewCache.mIconRefine.setOnClickListener(this);
        }
        while (true)
        {
            return;
            localObject = getStringOrNull(paramCursor, this.mText2Col);
            break;
            label256: if (localChildViewCache.mText1 == null)
                break label127;
            localChildViewCache.mText1.setSingleLine(true);
            localChildViewCache.mText1.setMaxLines(1);
            break label127;
            label285: localChildViewCache.mIconRefine.setVisibility(8);
        }
    }

    public void changeCursor(Cursor paramCursor)
    {
        if (this.mClosed)
        {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (paramCursor != null)
                paramCursor.close();
        }
        while (true)
        {
            return;
            try
            {
                super.changeCursor(paramCursor);
                if (paramCursor != null)
                {
                    this.mText1Col = paramCursor.getColumnIndex("suggest_text_1");
                    this.mText2Col = paramCursor.getColumnIndex("suggest_text_2");
                    this.mText2UrlCol = paramCursor.getColumnIndex("suggest_text_2_url");
                    this.mIconName1Col = paramCursor.getColumnIndex("suggest_icon_1");
                    this.mIconName2Col = paramCursor.getColumnIndex("suggest_icon_2");
                    this.mFlagsCol = paramCursor.getColumnIndex("suggest_flags");
                }
            }
            catch (Exception localException)
            {
                Log.e("SuggestionsAdapter", "error changing cursor and caching columns", localException);
            }
        }
    }

    public void close()
    {
        changeCursor(null);
        this.mClosed = true;
    }

    public CharSequence convertToString(Cursor paramCursor)
    {
        Object localObject;
        if (paramCursor == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = getColumnString(paramCursor, "suggest_intent_query");
            if (localObject == null)
                if (this.mSearchable.shouldRewriteQueryFromData())
                {
                    String str2 = getColumnString(paramCursor, "suggest_intent_data");
                    if (str2 != null)
                        localObject = str2;
                }
                else if (this.mSearchable.shouldRewriteQueryFromText())
                {
                    String str1 = getColumnString(paramCursor, "suggest_text_1");
                    if (str1 != null)
                        localObject = str1;
                }
                else
                {
                    localObject = null;
                }
        }
    }

    public int getQueryRefinement()
    {
        return this.mQueryRefinement;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        try
        {
            View localView2 = super.getView(paramInt, paramView, paramViewGroup);
            localView1 = localView2;
            return localView1;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", localRuntimeException);
                View localView1 = newView(this.mContext, this.mCursor, paramViewGroup);
                if (localView1 != null)
                    ((ChildViewCache)localView1.getTag()).mText1.setText(localRuntimeException.toString());
            }
        }
    }

    public boolean hasStableIds()
    {
        return false;
    }

    public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
    {
        View localView = super.newView(paramContext, paramCursor, paramViewGroup);
        localView.setTag(new ChildViewCache(localView));
        return localView;
    }

    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
        updateSpinnerState(getCursor());
    }

    public void notifyDataSetInvalidated()
    {
        super.notifyDataSetInvalidated();
        updateSpinnerState(getCursor());
    }

    public void onClick(View paramView)
    {
        Object localObject = paramView.getTag();
        if ((localObject instanceof CharSequence))
            this.mSearchView.onQueryRefine((CharSequence)localObject);
    }

    public Cursor runQueryOnBackgroundThread(CharSequence paramCharSequence)
    {
        Object localObject = null;
        String str;
        if (paramCharSequence == null)
        {
            str = "";
            if ((this.mSearchView.getVisibility() == 0) && (this.mSearchView.getWindowVisibility() == 0))
                break label40;
        }
        while (true)
        {
            return localObject;
            str = paramCharSequence.toString();
            break;
            try
            {
                label40: Cursor localCursor = this.mSearchManager.getSuggestions(this.mSearchable, str, 50);
                if (localCursor != null)
                {
                    localCursor.getCount();
                    localObject = localCursor;
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", localRuntimeException);
            }
        }
    }

    public void setQueryRefinement(int paramInt)
    {
        this.mQueryRefinement = paramInt;
    }

    private static final class ChildViewCache
    {
        public final ImageView mIcon1;
        public final ImageView mIcon2;
        public final ImageView mIconRefine;
        public final TextView mText1;
        public final TextView mText2;

        public ChildViewCache(View paramView)
        {
            this.mText1 = ((TextView)paramView.findViewById(16908308));
            this.mText2 = ((TextView)paramView.findViewById(16908309));
            this.mIcon1 = ((ImageView)paramView.findViewById(16908295));
            this.mIcon2 = ((ImageView)paramView.findViewById(16908296));
            this.mIconRefine = ((ImageView)paramView.findViewById(16909095));
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static Drawable getDrawable(PackageManager paramPackageManager, String paramString, int paramInt, ActivityInfo paramActivityInfo)
        {
            return paramActivityInfo.loadIcon(paramPackageManager);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SuggestionsAdapter
 * JD-Core Version:        0.6.2
 */