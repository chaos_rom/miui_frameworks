package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class CheckedTextView extends TextView
    implements Checkable
{
    private static final int[] CHECKED_STATE_SET = arrayOfInt;
    private int mBasePadding;
    private Drawable mCheckMarkDrawable;
    private int mCheckMarkResource;
    private int mCheckMarkWidth;
    private boolean mChecked;
    private boolean mNeedRequestlayout;

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842912;
    }

    public CheckedTextView(Context paramContext)
    {
        this(paramContext, null);
    }

    public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CheckedTextView, paramInt, 0);
        Drawable localDrawable = localTypedArray.getDrawable(1);
        if (localDrawable != null)
            setCheckMarkDrawable(localDrawable);
        setChecked(localTypedArray.getBoolean(0, false));
        localTypedArray.recycle();
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.mCheckMarkDrawable != null)
        {
            int[] arrayOfInt = getDrawableState();
            this.mCheckMarkDrawable.setState(arrayOfInt);
            invalidate();
        }
    }

    public Drawable getCheckMarkDrawable()
    {
        return this.mCheckMarkDrawable;
    }

    @ViewDebug.ExportedProperty
    public boolean isChecked()
    {
        return this.mChecked;
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
        if (isChecked())
            mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET);
        return arrayOfInt;
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        Drawable localDrawable = this.mCheckMarkDrawable;
        int j;
        int k;
        if (localDrawable != null)
        {
            int i = 0x70 & getGravity();
            j = localDrawable.getIntrinsicHeight();
            k = 0;
            switch (i)
            {
            default:
            case 80:
            case 16:
            }
        }
        while (true)
        {
            int m = getWidth();
            localDrawable.setBounds(m - this.mPaddingRight, k, m - this.mPaddingRight + this.mCheckMarkWidth, k + j);
            localDrawable.draw(paramCanvas);
            return;
            k = getHeight() - j;
            continue;
            k = (getHeight() - j) / 2;
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(CheckedTextView.class.getName());
        paramAccessibilityEvent.setChecked(this.mChecked);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(CheckedTextView.class.getName());
        paramAccessibilityNodeInfo.setCheckable(true);
        paramAccessibilityNodeInfo.setChecked(this.mChecked);
    }

    public void onPaddingChanged(int paramInt)
    {
        int i;
        boolean bool1;
        if (this.mCheckMarkDrawable != null)
        {
            i = this.mCheckMarkWidth + this.mBasePadding;
            bool1 = this.mNeedRequestlayout;
            if (this.mPaddingRight == i)
                break label71;
        }
        label71: for (boolean bool2 = true; ; bool2 = false)
        {
            this.mNeedRequestlayout = (bool2 | bool1);
            this.mPaddingRight = i;
            if (this.mNeedRequestlayout)
            {
                requestLayout();
                this.mNeedRequestlayout = false;
            }
            return;
            i = this.mBasePadding;
            break;
        }
    }

    public void setCheckMarkDrawable(int paramInt)
    {
        if ((paramInt != 0) && (paramInt == this.mCheckMarkResource));
        while (true)
        {
            return;
            this.mCheckMarkResource = paramInt;
            Drawable localDrawable = null;
            if (this.mCheckMarkResource != 0)
                localDrawable = getResources().getDrawable(this.mCheckMarkResource);
            setCheckMarkDrawable(localDrawable);
        }
    }

    public void setCheckMarkDrawable(Drawable paramDrawable)
    {
        boolean bool1 = true;
        if (this.mCheckMarkDrawable != null)
        {
            this.mCheckMarkDrawable.setCallback(null);
            unscheduleDrawable(this.mCheckMarkDrawable);
        }
        boolean bool2;
        if (paramDrawable != this.mCheckMarkDrawable)
        {
            bool2 = bool1;
            this.mNeedRequestlayout = bool2;
            if (paramDrawable == null)
                break label116;
            paramDrawable.setCallback(this);
            if (getVisibility() != 0)
                break label111;
            label56: paramDrawable.setVisible(bool1, false);
            paramDrawable.setState(CHECKED_STATE_SET);
            setMinHeight(paramDrawable.getIntrinsicHeight());
            this.mCheckMarkWidth = paramDrawable.getIntrinsicWidth();
            paramDrawable.setState(getDrawableState());
        }
        while (true)
        {
            this.mCheckMarkDrawable = paramDrawable;
            resolvePadding();
            return;
            bool2 = false;
            break;
            label111: bool1 = false;
            break label56;
            label116: this.mCheckMarkWidth = 0;
        }
    }

    public void setChecked(boolean paramBoolean)
    {
        if (this.mChecked != paramBoolean)
        {
            this.mChecked = paramBoolean;
            refreshDrawableState();
            notifyAccessibilityStateChanged();
        }
    }

    public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mBasePadding = this.mPaddingRight;
    }

    public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.setPaddingRelative(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mBasePadding = getPaddingEnd();
    }

    public void toggle()
    {
        if (!this.mChecked);
        for (boolean bool = true; ; bool = false)
        {
            setChecked(bool);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.CheckedTextView
 * JD-Core Version:        0.6.2
 */