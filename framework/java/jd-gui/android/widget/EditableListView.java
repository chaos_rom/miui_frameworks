package android.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class EditableListView extends ListView
{
    private static final String TAG = "EditableListView";
    private boolean mIsUpdateBatchChecked;
    private boolean mKeepEditModeIfNoItemChecked;
    private EditableListViewListener mMultiChoiceModeListener;
    private AdapterView.OnItemLongClickListener mOnItemLongClickListener = new AdapterView.OnItemLongClickListener()
    {
        public boolean onItemLongClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
            EditableListView.this.enterEditModeInner();
            return true;
        }
    };
    private boolean mShowCheckBoxInNoneEditMode;

    public EditableListView(Context paramContext)
    {
        super(paramContext);
    }

    public EditableListView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    private void enterEditModeInner()
    {
        if (this.mMultiChoiceModeCallback != null)
            startActionMode(this.mMultiChoiceModeCallback);
    }

    private int getEnabledCount()
    {
        int i = getHeaderViewsCount();
        int j = this.mAdapter.getCount() - getFooterViewsCount();
        int k = 0;
        for (int m = i; m < j; m++)
            if (this.mAdapter.isEnabled(m))
                k++;
        return k;
    }

    private boolean isValidPos(int paramInt)
    {
        int i = getHeaderViewsCount();
        int j = this.mAdapter.getCount() - getFooterViewsCount();
        if ((paramInt >= i) && (paramInt < j));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void updateBatchChecked(boolean paramBoolean)
    {
        boolean bool = true;
        if ((this.mChoiceActionMode == null) || (this.mAdapter == null) || (this.mChoiceMode != 3));
        while (true)
        {
            return;
            this.mIsUpdateBatchChecked = bool;
            int i = getHeaderViewsCount();
            int j = this.mAdapter.getCount() - getFooterViewsCount();
            int k;
            if ((this.mCheckedIdStates != null) && (this.mAdapter.hasStableIds()))
            {
                k = i;
                label73: if (k >= j)
                    break label222;
                if (this.mAdapter.isEnabled(k))
                    break label105;
            }
            label201: 
            while (true)
            {
                k++;
                break label73;
                bool = false;
                break;
                label105: if (paramBoolean)
                {
                    if (this.mCheckStates.get(k));
                }
                else
                {
                    for (this.mCheckedItemCount = (1 + this.mCheckedItemCount); ; this.mCheckedItemCount = (-1 + this.mCheckedItemCount))
                    {
                        this.mCheckStates.put(k, paramBoolean);
                        if (!bool)
                            break;
                        if (!paramBoolean)
                            break label201;
                        this.mCheckedIdStates.put(this.mAdapter.getItemId(k), Integer.valueOf(k));
                        break;
                        if (!this.mCheckStates.get(k))
                            break;
                    }
                    this.mCheckedIdStates.delete(this.mAdapter.getItemId(k));
                }
            }
            label222: ((EditableModeWrapper)this.mMultiChoiceModeCallback).onBatchCheckedStateChanged(this.mChoiceActionMode, paramBoolean);
            updateOnScreenCheckedViews();
            this.mIsUpdateBatchChecked = false;
        }
    }

    private void updateCheckBoxState(CheckBox paramCheckBox, int paramInt)
    {
        if ((paramCheckBox != null) && (isValidPos(paramInt)))
            if ((!isEditMode()) && (!this.mShowCheckBoxInNoneEditMode))
                break label46;
        label46: for (int i = 0; ; i = 8)
        {
            paramCheckBox.setVisibility(i);
            paramCheckBox.setChecked(this.mCheckStates.get(paramInt));
            return;
        }
    }

    private void updateOnScreenCheckedViews()
    {
        int i = this.mFirstPosition;
        int j = getChildCount();
        for (int k = 0; k < j; k++)
        {
            View localView = getChildAt(k);
            int m = i + k;
            updateCheckBoxState((CheckBox)localView.findViewById(16908289), m);
        }
    }

    public void checkAll()
    {
        updateBatchChecked(true);
    }

    public void checkNothing()
    {
        updateBatchChecked(false);
    }

    public void enterEditMode()
    {
        if (this.mMultiChoiceModeCallback == null);
        while (true)
        {
            return;
            this.mChoiceActionMode = startActionMode(this.mMultiChoiceModeCallback);
            this.mMultiChoiceModeListener.onItemCheckedStateChanged(this.mChoiceActionMode, -1, -1L, false);
            if ((!this.mInLayout) && (!this.mBlockLayoutRequests))
            {
                this.mDataChanged = true;
                rememberSyncState();
                requestLayout();
            }
        }
    }

    public void exitEditMode()
    {
        if (this.mChoiceActionMode != null)
            this.mChoiceActionMode.finish();
    }

    public boolean isAllChecked()
    {
        boolean bool = false;
        if ((this.mAdapter != null) && (getEnabledCount() == getCheckedItemCount()))
            bool = true;
        return bool;
    }

    public boolean isEditMode()
    {
        if (this.mChoiceActionMode != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isItemCheckedById(long paramLong)
    {
        Integer localInteger = (Integer)this.mCheckedIdStates.get(paramLong);
        if (localInteger != null);
        for (boolean bool = isItemChecked(localInteger.intValue()); ; bool = false)
            return bool;
    }

    View obtainView(int paramInt, boolean[] paramArrayOfBoolean)
    {
        View localView = super.obtainView(paramInt, paramArrayOfBoolean);
        updateCheckBoxState((CheckBox)localView.findViewById(16908289), paramInt);
        return localView;
    }

    public void setCheckBoxVisiableInNoneEditMode(boolean paramBoolean)
    {
        this.mShowCheckBoxInNoneEditMode = paramBoolean;
    }

    public void setItemChecked(int paramInt, boolean paramBoolean)
    {
        if (isValidPos(paramInt))
            super.setItemChecked(paramInt, paramBoolean);
    }

    public void setKeepExitModeIfNoItemChecked(boolean paramBoolean)
    {
        this.mKeepEditModeIfNoItemChecked = paramBoolean;
    }

    public void setMultiChoiceModeListener(AbsListView.MultiChoiceModeListener paramMultiChoiceModeListener)
    {
        Log.e("EditableListView", "should use setMultiChoiceModeListener(final EditableListViewListener listener)");
    }

    public void setMultiChoiceModeListener(EditableListViewListener paramEditableListViewListener)
    {
        if (this.mMultiChoiceModeCallback == null)
            this.mMultiChoiceModeCallback = new EditableModeWrapper();
        this.mMultiChoiceModeListener = paramEditableListViewListener;
        this.mMultiChoiceModeCallback.setWrapped(paramEditableListViewListener);
        setChoiceMode(3);
        if (paramEditableListViewListener == null);
        for (AdapterView.OnItemLongClickListener localOnItemLongClickListener = null; ; localOnItemLongClickListener = this.mOnItemLongClickListener)
        {
            setOnItemLongClickListener(localOnItemLongClickListener);
            return;
        }
    }

    public static class DropDownMenu
    {
        private Button mButton;
        private Menu mMenu;
        private PopupMenu mPopupMenu;

        public DropDownMenu(Context paramContext, Button paramButton, int paramInt, PopupMenu.OnMenuItemClickListener paramOnMenuItemClickListener)
        {
            this.mButton = paramButton;
            this.mButton.setBackgroundResource(100794731);
            this.mPopupMenu = new PopupMenu(paramContext, this.mButton);
            this.mMenu = this.mPopupMenu.getMenu();
            this.mPopupMenu.getMenuInflater().inflate(paramInt, this.mMenu);
            this.mPopupMenu.setOnMenuItemClickListener(paramOnMenuItemClickListener);
            this.mButton.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    EditableListView.DropDownMenu.this.mPopupMenu.show();
                }
            });
        }

        public MenuItem findItem(int paramInt)
        {
            return this.mMenu.findItem(paramInt);
        }

        public void setTitle(CharSequence paramCharSequence)
        {
            this.mButton.setText(paramCharSequence);
        }
    }

    public static class EditableModeCallback
        implements EditableListView.EditableListViewListener, PopupMenu.OnMenuItemClickListener
    {
        private int mActionBarMenuId = 101580800;
        private Activity mActivity;
        private EditableListView mEditableListView;
        private EditableListView.DropDownMenu mSelectionMenu;
        private int mSplitActionBarMenuId = 101580801;

        public EditableModeCallback(Activity paramActivity, EditableListView paramEditableListView)
        {
            this.mActivity = paramActivity;
            this.mEditableListView = paramEditableListView;
        }

        private void updateSelectionMenu()
        {
            String str = getTitle(this.mEditableListView.getCheckedItemCount());
            this.mSelectionMenu.setTitle(str);
            MenuItem localMenuItem = this.mSelectionMenu.findItem(101384229);
            if (localMenuItem != null)
            {
                if (!this.mEditableListView.isAllChecked())
                    break label54;
                localMenuItem.setTitle(101450104);
            }
            while (true)
            {
                return;
                label54: localMenuItem.setTitle(101450103);
            }
        }

        public String getTitle(int paramInt)
        {
            Activity localActivity = this.mActivity;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(paramInt);
            return localActivity.getString(101450105, arrayOfObject);
        }

        public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
        {
            switch (paramMenuItem.getItemId())
            {
            default:
            case 101384228:
            }
            while (true)
            {
                return true;
                paramActionMode.finish();
            }
        }

        public void onBatchCheckedStateChanged(ActionMode paramActionMode, boolean paramBoolean)
        {
            updateSelectionMenu();
        }

        public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            View localView = LayoutInflater.from(this.mActivity).inflate(100859934, null);
            paramActionMode.setCustomView(localView);
            this.mSelectionMenu = new EditableListView.DropDownMenu(this.mActivity, (Button)localView.findViewById(101384194), this.mActionBarMenuId, this);
            this.mActivity.getMenuInflater().inflate(this.mSplitActionBarMenuId, paramMenu);
            return true;
        }

        public void onDestroyActionMode(ActionMode paramActionMode)
        {
            if (this.mSelectionMenu.mPopupMenu != null)
                this.mSelectionMenu.mPopupMenu.dismiss();
        }

        public void onItemCheckedStateChanged(ActionMode paramActionMode, int paramInt, long paramLong, boolean paramBoolean)
        {
            updateSelectionMenu();
        }

        public boolean onMenuItemClick(MenuItem paramMenuItem)
        {
            switch (paramMenuItem.getItemId())
            {
            default:
            case 101384229:
            }
            while (true)
            {
                return true;
                if (this.mEditableListView.isAllChecked())
                    this.mEditableListView.checkNothing();
                else
                    this.mEditableListView.checkAll();
            }
        }

        public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            return false;
        }

        public void setActionBarMenuId(int paramInt)
        {
            this.mActionBarMenuId = paramInt;
        }

        public void setSplitActionBarMenuId(int paramInt)
        {
            this.mSplitActionBarMenuId = paramInt;
        }
    }

    class EditableModeWrapper extends AbsListView.MultiChoiceModeWrapper
        implements EditableListView.EditableListViewListener
    {
        EditableModeWrapper()
        {
            super();
        }

        private void handleCheckedStateChanged(ActionMode paramActionMode)
        {
            if ((EditableListView.this.mIsUpdateBatchChecked) || (EditableListView.this.mKeepEditModeIfNoItemChecked));
            while (true)
            {
                return;
                if (EditableListView.this.getCheckedItemCount() == 0)
                    paramActionMode.finish();
            }
        }

        public void onBatchCheckedStateChanged(ActionMode paramActionMode, boolean paramBoolean)
        {
            EditableListView.this.mMultiChoiceModeListener.onBatchCheckedStateChanged(paramActionMode, paramBoolean);
            handleCheckedStateChanged(paramActionMode);
        }

        public void onItemCheckedStateChanged(ActionMode paramActionMode, int paramInt, long paramLong, boolean paramBoolean)
        {
            if (!EditableListView.this.isValidPos(paramInt));
            while (true)
            {
                return;
                EditableListView.this.mMultiChoiceModeListener.onItemCheckedStateChanged(paramActionMode, paramInt, paramLong, paramBoolean);
                handleCheckedStateChanged(paramActionMode);
                EditableListView.this.updateOnScreenCheckedViews();
            }
        }
    }

    public static abstract interface EditableListViewListener extends AbsListView.MultiChoiceModeListener
    {
        public abstract void onBatchCheckedStateChanged(ActionMode paramActionMode, boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.EditableListView
 * JD-Core Version:        0.6.2
 */