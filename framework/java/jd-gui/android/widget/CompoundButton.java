package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.View.BaseSavedState;
import android.view.ViewDebug.ExportedProperty;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public abstract class CompoundButton extends Button
    implements Checkable
{
    private static final int[] CHECKED_STATE_SET = arrayOfInt;
    private boolean mBroadcasting;
    private Drawable mButtonDrawable;
    private int mButtonResource;
    private boolean mChecked;
    private OnCheckedChangeListener mOnCheckedChangeListener;
    private OnCheckedChangeListener mOnCheckedChangeWidgetListener;

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842912;
    }

    public CompoundButton(Context paramContext)
    {
        this(paramContext, null);
    }

    public CompoundButton(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public CompoundButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CompoundButton, paramInt, 0);
        Drawable localDrawable = localTypedArray.getDrawable(1);
        if (localDrawable != null)
            setButtonDrawable(localDrawable);
        setChecked(localTypedArray.getBoolean(0, false));
        localTypedArray.recycle();
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.mButtonDrawable != null)
        {
            int[] arrayOfInt = getDrawableState();
            this.mButtonDrawable.setState(arrayOfInt);
            invalidate();
        }
    }

    @ViewDebug.ExportedProperty
    public boolean isChecked()
    {
        return this.mChecked;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mButtonDrawable != null)
            this.mButtonDrawable.jumpToCurrentState();
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
        if (isChecked())
            mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET);
        return arrayOfInt;
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        Drawable localDrawable = this.mButtonDrawable;
        int j;
        int k;
        if (localDrawable != null)
        {
            int i = 0x70 & getGravity();
            j = localDrawable.getIntrinsicHeight();
            k = 0;
            switch (i)
            {
            default:
            case 80:
            case 16:
            }
        }
        while (true)
        {
            localDrawable.setBounds(0, k, localDrawable.getIntrinsicWidth(), k + j);
            localDrawable.draw(paramCanvas);
            return;
            k = getHeight() - j;
            continue;
            k = (getHeight() - j) / 2;
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(CompoundButton.class.getName());
        paramAccessibilityEvent.setChecked(this.mChecked);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(CompoundButton.class.getName());
        paramAccessibilityNodeInfo.setCheckable(true);
        paramAccessibilityNodeInfo.setChecked(this.mChecked);
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        setChecked(localSavedState.checked);
        requestLayout();
    }

    public Parcelable onSaveInstanceState()
    {
        setFreezesText(true);
        SavedState localSavedState = new SavedState(super.onSaveInstanceState());
        localSavedState.checked = isChecked();
        return localSavedState;
    }

    public boolean performClick()
    {
        toggle();
        return super.performClick();
    }

    public void setButtonDrawable(int paramInt)
    {
        if ((paramInt != 0) && (paramInt == this.mButtonResource));
        while (true)
        {
            return;
            this.mButtonResource = paramInt;
            Drawable localDrawable = null;
            if (this.mButtonResource != 0)
                localDrawable = getResources().getDrawable(this.mButtonResource);
            setButtonDrawable(localDrawable);
        }
    }

    public void setButtonDrawable(Drawable paramDrawable)
    {
        if (paramDrawable != null)
        {
            if (this.mButtonDrawable != null)
            {
                this.mButtonDrawable.setCallback(null);
                unscheduleDrawable(this.mButtonDrawable);
            }
            paramDrawable.setCallback(this);
            paramDrawable.setState(getDrawableState());
            if (getVisibility() != 0)
                break label87;
        }
        label87: for (boolean bool = true; ; bool = false)
        {
            paramDrawable.setVisible(bool, false);
            this.mButtonDrawable = paramDrawable;
            this.mButtonDrawable.setState(null);
            setMinHeight(this.mButtonDrawable.getIntrinsicHeight());
            refreshDrawableState();
            return;
        }
    }

    public void setChecked(boolean paramBoolean)
    {
        if (this.mChecked != paramBoolean)
        {
            this.mChecked = paramBoolean;
            refreshDrawableState();
            notifyAccessibilityStateChanged();
            if (!this.mBroadcasting)
                break label29;
        }
        while (true)
        {
            return;
            label29: this.mBroadcasting = true;
            if (this.mOnCheckedChangeListener != null)
                this.mOnCheckedChangeListener.onCheckedChanged(this, this.mChecked);
            if (this.mOnCheckedChangeWidgetListener != null)
                this.mOnCheckedChangeWidgetListener.onCheckedChanged(this, this.mChecked);
            this.mBroadcasting = false;
        }
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener paramOnCheckedChangeListener)
    {
        this.mOnCheckedChangeListener = paramOnCheckedChangeListener;
    }

    void setOnCheckedChangeWidgetListener(OnCheckedChangeListener paramOnCheckedChangeListener)
    {
        this.mOnCheckedChangeWidgetListener = paramOnCheckedChangeListener;
    }

    public void toggle()
    {
        if (!this.mChecked);
        for (boolean bool = true; ; bool = false)
        {
            setChecked(bool);
            return;
        }
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((super.verifyDrawable(paramDrawable)) || (paramDrawable == this.mButtonDrawable));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public CompoundButton.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new CompoundButton.SavedState(paramAnonymousParcel, null);
            }

            public CompoundButton.SavedState[] newArray(int paramAnonymousInt)
            {
                return new CompoundButton.SavedState[paramAnonymousInt];
            }
        };
        boolean checked;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.checked = ((Boolean)paramParcel.readValue(null)).booleanValue();
        }

        SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public String toString()
        {
            return "CompoundButton.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " checked=" + this.checked + "}";
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeValue(Boolean.valueOf(this.checked));
        }
    }

    public static abstract interface OnCheckedChangeListener
    {
        public abstract void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.CompoundButton
 * JD-Core Version:        0.6.2
 */