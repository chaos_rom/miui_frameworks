package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class RadioGroup extends LinearLayout
{
    private int mCheckedId = -1;
    private CompoundButton.OnCheckedChangeListener mChildOnCheckedChangeListener;
    private OnCheckedChangeListener mOnCheckedChangeListener;
    private PassThroughHierarchyChangeListener mPassThroughListener;
    private boolean mProtectFromCheckedChange = false;

    public RadioGroup(Context paramContext)
    {
        super(paramContext);
        setOrientation(1);
        init();
    }

    public RadioGroup(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RadioGroup, 16842878, 0);
        int i = localTypedArray.getResourceId(1, -1);
        if (i != -1)
            this.mCheckedId = i;
        setOrientation(localTypedArray.getInt(0, 1));
        localTypedArray.recycle();
        init();
    }

    private void init()
    {
        this.mChildOnCheckedChangeListener = new CheckedStateTracker(null);
        this.mPassThroughListener = new PassThroughHierarchyChangeListener(null);
        super.setOnHierarchyChangeListener(this.mPassThroughListener);
    }

    private void setCheckedId(int paramInt)
    {
        this.mCheckedId = paramInt;
        if (this.mOnCheckedChangeListener != null)
            this.mOnCheckedChangeListener.onCheckedChanged(this, this.mCheckedId);
    }

    private void setCheckedStateForView(int paramInt, boolean paramBoolean)
    {
        View localView = findViewById(paramInt);
        if ((localView != null) && ((localView instanceof RadioButton)))
            ((RadioButton)localView).setChecked(paramBoolean);
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        if ((paramView instanceof RadioButton))
        {
            RadioButton localRadioButton = (RadioButton)paramView;
            if (localRadioButton.isChecked())
            {
                this.mProtectFromCheckedChange = true;
                if (this.mCheckedId != -1)
                    setCheckedStateForView(this.mCheckedId, false);
                this.mProtectFromCheckedChange = false;
                setCheckedId(localRadioButton.getId());
            }
        }
        super.addView(paramView, paramInt, paramLayoutParams);
    }

    public void check(int paramInt)
    {
        if ((paramInt != -1) && (paramInt == this.mCheckedId));
        while (true)
        {
            return;
            if (this.mCheckedId != -1)
                setCheckedStateForView(this.mCheckedId, false);
            if (paramInt != -1)
                setCheckedStateForView(paramInt, true);
            setCheckedId(paramInt);
        }
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    public void clearCheck()
    {
        check(-1);
    }

    protected LinearLayout.LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-2, -2);
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    public int getCheckedRadioButtonId()
    {
        return this.mCheckedId;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        if (this.mCheckedId != -1)
        {
            this.mProtectFromCheckedChange = true;
            setCheckedStateForView(this.mCheckedId, true);
            this.mProtectFromCheckedChange = false;
            setCheckedId(this.mCheckedId);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(RadioGroup.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(RadioGroup.class.getName());
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener paramOnCheckedChangeListener)
    {
        this.mOnCheckedChangeListener = paramOnCheckedChangeListener;
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener paramOnHierarchyChangeListener)
    {
        PassThroughHierarchyChangeListener.access$202(this.mPassThroughListener, paramOnHierarchyChangeListener);
    }

    private class PassThroughHierarchyChangeListener
        implements ViewGroup.OnHierarchyChangeListener
    {
        private ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;

        private PassThroughHierarchyChangeListener()
        {
        }

        public void onChildViewAdded(View paramView1, View paramView2)
        {
            if ((paramView1 == RadioGroup.this) && ((paramView2 instanceof RadioButton)))
            {
                if (paramView2.getId() == -1)
                    paramView2.setId(paramView2.hashCode());
                ((RadioButton)paramView2).setOnCheckedChangeWidgetListener(RadioGroup.this.mChildOnCheckedChangeListener);
            }
            if (this.mOnHierarchyChangeListener != null)
                this.mOnHierarchyChangeListener.onChildViewAdded(paramView1, paramView2);
        }

        public void onChildViewRemoved(View paramView1, View paramView2)
        {
            if ((paramView1 == RadioGroup.this) && ((paramView2 instanceof RadioButton)))
                ((RadioButton)paramView2).setOnCheckedChangeWidgetListener(null);
            if (this.mOnHierarchyChangeListener != null)
                this.mOnHierarchyChangeListener.onChildViewRemoved(paramView1, paramView2);
        }
    }

    private class CheckedStateTracker
        implements CompoundButton.OnCheckedChangeListener
    {
        private CheckedStateTracker()
        {
        }

        public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
        {
            if (RadioGroup.this.mProtectFromCheckedChange);
            while (true)
            {
                return;
                RadioGroup.access$302(RadioGroup.this, true);
                if (RadioGroup.this.mCheckedId != -1)
                    RadioGroup.this.setCheckedStateForView(RadioGroup.this.mCheckedId, false);
                RadioGroup.access$302(RadioGroup.this, false);
                int i = paramCompoundButton.getId();
                RadioGroup.this.setCheckedId(i);
            }
        }
    }

    public static abstract interface OnCheckedChangeListener
    {
        public abstract void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt);
    }

    public static class LayoutParams extends LinearLayout.LayoutParams
    {
        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(int paramInt1, int paramInt2, float paramFloat)
        {
            super(paramInt2, paramFloat);
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
        {
            super();
        }

        protected void setBaseAttributes(TypedArray paramTypedArray, int paramInt1, int paramInt2)
        {
            if (paramTypedArray.hasValue(paramInt1))
            {
                this.width = paramTypedArray.getLayoutDimension(paramInt1, "layout_width");
                if (!paramTypedArray.hasValue(paramInt2))
                    break label48;
            }
            label48: for (this.height = paramTypedArray.getLayoutDimension(paramInt2, "layout_height"); ; this.height = -2)
            {
                return;
                this.width = -2;
                break;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.RadioGroup
 * JD-Core Version:        0.6.2
 */