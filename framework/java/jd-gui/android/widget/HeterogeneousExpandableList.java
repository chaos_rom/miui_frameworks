package android.widget;

public abstract interface HeterogeneousExpandableList
{
    public abstract int getChildType(int paramInt1, int paramInt2);

    public abstract int getChildTypeCount();

    public abstract int getGroupType(int paramInt);

    public abstract int getGroupTypeCount();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.HeterogeneousExpandableList
 * JD-Core Version:        0.6.2
 */