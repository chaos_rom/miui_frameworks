package android.widget;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class ResourceCursorTreeAdapter extends CursorTreeAdapter
{
    private int mChildLayout;
    private int mCollapsedGroupLayout;
    private int mExpandedGroupLayout;
    private LayoutInflater mInflater;
    private int mLastChildLayout;

    public ResourceCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2)
    {
        this(paramContext, paramCursor, paramInt1, paramInt1, paramInt2, paramInt2);
    }

    public ResourceCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2, int paramInt3)
    {
        this(paramContext, paramCursor, paramInt1, paramInt2, paramInt3, paramInt3);
    }

    public ResourceCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super(paramCursor, paramContext);
        this.mCollapsedGroupLayout = paramInt1;
        this.mExpandedGroupLayout = paramInt2;
        this.mChildLayout = paramInt3;
        this.mLastChildLayout = paramInt4;
        this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    }

    public View newChildView(Context paramContext, Cursor paramCursor, boolean paramBoolean, ViewGroup paramViewGroup)
    {
        LayoutInflater localLayoutInflater = this.mInflater;
        if (paramBoolean);
        for (int i = this.mLastChildLayout; ; i = this.mChildLayout)
            return localLayoutInflater.inflate(i, paramViewGroup, false);
    }

    public View newGroupView(Context paramContext, Cursor paramCursor, boolean paramBoolean, ViewGroup paramViewGroup)
    {
        LayoutInflater localLayoutInflater = this.mInflater;
        if (paramBoolean);
        for (int i = this.mExpandedGroupLayout; ; i = this.mCollapsedGroupLayout)
            return localLayoutInflater.inflate(i, paramViewGroup, false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ResourceCursorTreeAdapter
 * JD-Core Version:        0.6.2
 */