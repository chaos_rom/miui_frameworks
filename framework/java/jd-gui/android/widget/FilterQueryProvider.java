package android.widget;

import android.database.Cursor;

public abstract interface FilterQueryProvider
{
    public abstract Cursor runQuery(CharSequence paramCharSequence);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.FilterQueryProvider
 * JD-Core Version:        0.6.2
 */