package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.android.internal.R.styleable;

public class ViewAnimator extends FrameLayout
{
    boolean mAnimateFirstTime = true;
    boolean mFirstTime = true;
    Animation mInAnimation;
    Animation mOutAnimation;
    int mWhichChild = 0;

    public ViewAnimator(Context paramContext)
    {
        super(paramContext);
        initViewAnimator(paramContext, null);
    }

    public ViewAnimator(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewAnimator);
        int i = localTypedArray.getResourceId(0, 0);
        if (i > 0)
            setInAnimation(paramContext, i);
        int j = localTypedArray.getResourceId(1, 0);
        if (j > 0)
            setOutAnimation(paramContext, j);
        setAnimateFirstView(localTypedArray.getBoolean(2, true));
        localTypedArray.recycle();
        initViewAnimator(paramContext, paramAttributeSet);
    }

    private void initViewAnimator(Context paramContext, AttributeSet paramAttributeSet)
    {
        if (paramAttributeSet == null)
            this.mMeasureAllChildren = true;
        while (true)
        {
            return;
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FrameLayout);
            setMeasureAllChildren(localTypedArray.getBoolean(1, true));
            localTypedArray.recycle();
        }
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        super.addView(paramView, paramInt, paramLayoutParams);
        if (getChildCount() == 1)
            paramView.setVisibility(0);
        while (true)
        {
            if ((paramInt >= 0) && (this.mWhichChild >= paramInt))
                setDisplayedChild(1 + this.mWhichChild);
            return;
            paramView.setVisibility(8);
        }
    }

    public int getBaseline()
    {
        if (getCurrentView() != null);
        for (int i = getCurrentView().getBaseline(); ; i = super.getBaseline())
            return i;
    }

    public View getCurrentView()
    {
        return getChildAt(this.mWhichChild);
    }

    public int getDisplayedChild()
    {
        return this.mWhichChild;
    }

    public Animation getInAnimation()
    {
        return this.mInAnimation;
    }

    public Animation getOutAnimation()
    {
        return this.mOutAnimation;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ViewAnimator.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ViewAnimator.class.getName());
    }

    public void removeAllViews()
    {
        super.removeAllViews();
        this.mWhichChild = 0;
        this.mFirstTime = true;
    }

    public void removeView(View paramView)
    {
        int i = indexOfChild(paramView);
        if (i >= 0)
            removeViewAt(i);
    }

    public void removeViewAt(int paramInt)
    {
        super.removeViewAt(paramInt);
        int i = getChildCount();
        if (i == 0)
        {
            this.mWhichChild = 0;
            this.mFirstTime = true;
        }
        while (true)
        {
            return;
            if (this.mWhichChild >= i)
                setDisplayedChild(i - 1);
            else if (this.mWhichChild == paramInt)
                setDisplayedChild(this.mWhichChild);
        }
    }

    public void removeViewInLayout(View paramView)
    {
        removeView(paramView);
    }

    public void removeViews(int paramInt1, int paramInt2)
    {
        super.removeViews(paramInt1, paramInt2);
        if (getChildCount() == 0)
        {
            this.mWhichChild = 0;
            this.mFirstTime = true;
        }
        while (true)
        {
            return;
            if ((this.mWhichChild >= paramInt1) && (this.mWhichChild < paramInt1 + paramInt2))
                setDisplayedChild(this.mWhichChild);
        }
    }

    public void removeViewsInLayout(int paramInt1, int paramInt2)
    {
        removeViews(paramInt1, paramInt2);
    }

    public void setAnimateFirstView(boolean paramBoolean)
    {
        this.mAnimateFirstTime = paramBoolean;
    }

    @RemotableViewMethod
    public void setDisplayedChild(int paramInt)
    {
        int i = 0;
        this.mWhichChild = paramInt;
        if (paramInt >= getChildCount())
            this.mWhichChild = 0;
        while (true)
        {
            if (getFocusedChild() != null)
                i = 1;
            showOnly(this.mWhichChild);
            if (i != 0)
                requestFocus(2);
            return;
            if (paramInt < 0)
                this.mWhichChild = (-1 + getChildCount());
        }
    }

    public void setInAnimation(Context paramContext, int paramInt)
    {
        setInAnimation(AnimationUtils.loadAnimation(paramContext, paramInt));
    }

    public void setInAnimation(Animation paramAnimation)
    {
        this.mInAnimation = paramAnimation;
    }

    public void setOutAnimation(Context paramContext, int paramInt)
    {
        setOutAnimation(AnimationUtils.loadAnimation(paramContext, paramInt));
    }

    public void setOutAnimation(Animation paramAnimation)
    {
        this.mOutAnimation = paramAnimation;
    }

    @RemotableViewMethod
    public void showNext()
    {
        setDisplayedChild(1 + this.mWhichChild);
    }

    void showOnly(int paramInt)
    {
        if ((!this.mFirstTime) || (this.mAnimateFirstTime));
        for (boolean bool = true; ; bool = false)
        {
            showOnly(paramInt, bool);
            return;
        }
    }

    void showOnly(int paramInt, boolean paramBoolean)
    {
        int i = getChildCount();
        int j = 0;
        while (j < i)
        {
            View localView = getChildAt(j);
            if (j == paramInt)
            {
                if ((paramBoolean) && (this.mInAnimation != null))
                    localView.startAnimation(this.mInAnimation);
                localView.setVisibility(0);
                this.mFirstTime = false;
                j++;
            }
            else
            {
                if ((paramBoolean) && (this.mOutAnimation != null) && (localView.getVisibility() == 0))
                    localView.startAnimation(this.mOutAnimation);
                while (true)
                {
                    localView.setVisibility(8);
                    break;
                    if (localView.getAnimation() == this.mInAnimation)
                        localView.clearAnimation();
                }
            }
        }
    }

    @RemotableViewMethod
    public void showPrevious()
    {
        setDisplayedChild(-1 + this.mWhichChild);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ViewAnimator
 * JD-Core Version:        0.6.2
 */