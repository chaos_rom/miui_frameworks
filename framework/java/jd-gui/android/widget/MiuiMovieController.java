package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

public class MiuiMovieController extends FrameLayout
    implements MiuiMediaController, View.OnClickListener, Animation.AnimationListener, MiuiMediaTimeBar.ScrubbingListener
{
    private static final float ERROR_MESSAGE_RELATIVE_PADDING = 0.1666667F;
    private final View background;
    private boolean canReplay = true;
    private final TextView errorView;
    private final Handler handler;
    private boolean hidden;
    private final Animation hideAnimation;
    private final LinearLayout loadingView;
    private MiuiMediaController.OnControllerChangedListener mControlListener;
    private View mainView;
    private final ImageView playPauseReplayView;
    private final Runnable startHidingRunnable;
    private State state = State.LOADING;
    private final MiuiMediaTimeBar timeBar;

    public MiuiMovieController(Context paramContext)
    {
        super(paramContext);
        FrameLayout.LayoutParams localLayoutParams1 = new FrameLayout.LayoutParams(-2, -2);
        FrameLayout.LayoutParams localLayoutParams2 = new FrameLayout.LayoutParams(-1, -1);
        LayoutInflater.from(paramContext);
        this.background = new View(paramContext);
        this.background.setBackgroundColor(paramContext.getResources().getColor(101122081));
        addView(this.background, localLayoutParams2);
        this.timeBar = new MiuiMediaTimeBar(paramContext, this);
        addView(this.timeBar, localLayoutParams1);
        this.loadingView = new LinearLayout(paramContext);
        this.loadingView.setOrientation(1);
        this.loadingView.setGravity(1);
        ProgressBar localProgressBar = new ProgressBar(paramContext);
        localProgressBar.setIndeterminate(true);
        this.loadingView.addView(localProgressBar, localLayoutParams1);
        addView(this.loadingView, localLayoutParams1);
        this.playPauseReplayView = new ImageView(paramContext);
        this.playPauseReplayView.setImageResource(100794447);
        this.playPauseReplayView.setBackgroundResource(100794450);
        this.playPauseReplayView.setScaleType(ImageView.ScaleType.CENTER);
        this.playPauseReplayView.setFocusable(true);
        this.playPauseReplayView.setClickable(true);
        this.playPauseReplayView.setOnClickListener(this);
        addView(this.playPauseReplayView, localLayoutParams1);
        this.errorView = new TextView(paramContext);
        this.errorView.setGravity(17);
        this.errorView.setBackgroundColor(-872415232);
        this.errorView.setTextColor(-1);
        addView(this.errorView, localLayoutParams2);
        this.handler = new Handler();
        this.startHidingRunnable = new Runnable()
        {
            public void run()
            {
                MiuiMovieController.this.startHiding();
            }
        };
        this.hideAnimation = AnimationUtils.loadAnimation(paramContext, 100925455);
        this.hideAnimation.setAnimationListener(this);
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        hide();
    }

    private void cancelHiding()
    {
        this.handler.removeCallbacks(this.startHidingRunnable);
        this.background.setAnimation(null);
        this.timeBar.setAnimation(null);
        this.playPauseReplayView.setAnimation(null);
    }

    private void layoutCenteredView(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = paramView.getMeasuredWidth();
        int j = paramView.getMeasuredHeight();
        int k = (paramInt3 - paramInt1 - i) / 2;
        int m = (paramInt4 - paramInt2 - j) / 2;
        paramView.layout(k, m, k + i, m + j);
    }

    private void maybeStartHiding()
    {
        cancelHiding();
        if (this.state == State.PLAYING)
            this.handler.postDelayed(this.startHidingRunnable, 1500L);
    }

    private void showMainView(View paramView)
    {
        int i = 0;
        this.mainView = paramView;
        TextView localTextView = this.errorView;
        int j;
        int k;
        label52: ImageView localImageView;
        if (this.mainView == this.errorView)
        {
            j = 0;
            localTextView.setVisibility(j);
            LinearLayout localLinearLayout = this.loadingView;
            if (this.mainView != this.loadingView)
                break label93;
            k = 0;
            localLinearLayout.setVisibility(k);
            localImageView = this.playPauseReplayView;
            if (this.mainView != this.playPauseReplayView)
                break label99;
        }
        while (true)
        {
            localImageView.setVisibility(i);
            show();
            return;
            j = 4;
            break;
            label93: k = 4;
            break label52;
            label99: i = 4;
        }
    }

    private void startHideAnimation(View paramView)
    {
        if (paramView.getVisibility() == 0)
            paramView.startAnimation(this.hideAnimation);
    }

    private void startHiding()
    {
        startHideAnimation(this.timeBar);
        startHideAnimation(this.playPauseReplayView);
    }

    private void updateViews()
    {
        if (this.hidden)
            return;
        this.background.setVisibility(0);
        this.timeBar.setVisibility(0);
        ImageView localImageView1 = this.playPauseReplayView;
        int i;
        label42: ImageView localImageView2;
        if (this.state == State.PAUSED)
        {
            i = 100794447;
            localImageView1.setImageResource(i);
            localImageView2 = this.playPauseReplayView;
            if ((this.state == State.LOADING) || (this.state == State.ERROR) || ((this.state == State.ENDED) && (!this.canReplay)))
                break label129;
        }
        label129: for (int j = 0; ; j = 8)
        {
            localImageView2.setVisibility(j);
            requestLayout();
            break;
            if (this.state == State.PLAYING)
            {
                i = 100794448;
                break label42;
            }
            i = 100794449;
            break label42;
        }
    }

    public View getView()
    {
        return this;
    }

    public void hide()
    {
        boolean bool = this.hidden;
        this.hidden = true;
        this.playPauseReplayView.setVisibility(4);
        this.loadingView.setVisibility(4);
        this.background.setVisibility(4);
        this.timeBar.setVisibility(4);
        setVisibility(4);
        setFocusable(true);
        requestFocus();
        if ((this.mControlListener != null) && (bool != this.hidden))
            this.mControlListener.onHidden();
    }

    public boolean isShowing()
    {
        if (!this.hidden);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onAnimationEnd(Animation paramAnimation)
    {
        hide();
    }

    public void onAnimationRepeat(Animation paramAnimation)
    {
    }

    public void onAnimationStart(Animation paramAnimation)
    {
    }

    public void onClick(View paramView)
    {
        if ((this.mControlListener != null) && (paramView == this.playPauseReplayView))
        {
            if (this.state != State.ENDED)
                break label42;
            if (this.canReplay)
                this.mControlListener.onReplay();
        }
        while (true)
        {
            return;
            label42: if ((this.state == State.PAUSED) || (this.state == State.PLAYING))
                this.mControlListener.onPlayPause();
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if (this.hidden)
            show();
        return super.onKeyDown(paramInt, paramKeyEvent);
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = paramInt4 - paramInt2;
        int j = paramInt3 - paramInt1;
        if (this.errorView.getVisibility() == 0);
        while (true)
        {
            int k = paramInt4 - this.timeBar.getBarHeight();
            this.background.layout(paramInt1, k, paramInt3, paramInt4);
            this.timeBar.layout(paramInt1, paramInt4 - this.timeBar.getPreferredHeight(), paramInt3, paramInt4);
            this.timeBar.requestLayout();
            int m = paramInt1 + j / 2;
            int n = paramInt2 + i / 2;
            int i1 = this.playPauseReplayView.getMeasuredWidth();
            int i2 = this.playPauseReplayView.getMeasuredHeight();
            this.playPauseReplayView.layout(m - i1 / 2, n - i2 / 2, m + i1 / 2, n + i2 / 2);
            ((int)(0.1666667F * j));
            if (this.mainView != null)
                layoutCenteredView(this.mainView, paramInt1, paramInt2, paramInt3, paramInt4);
            return;
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        measureChildren(paramInt1, paramInt2);
    }

    public void onScrubbingEnd(int paramInt)
    {
        maybeStartHiding();
        if (this.mControlListener != null)
            this.mControlListener.onSeekEnd(paramInt);
    }

    public void onScrubbingMove(int paramInt)
    {
        cancelHiding();
        if (this.mControlListener != null)
            this.mControlListener.onSeekMove(paramInt);
    }

    public void onScrubbingStart()
    {
        cancelHiding();
        if (this.mControlListener != null)
            this.mControlListener.onSeekStart();
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if (super.onTouchEvent(paramMotionEvent));
        while (true)
        {
            return true;
            if (this.hidden)
                show();
            else
                switch (paramMotionEvent.getAction())
                {
                default:
                    break;
                case 0:
                    cancelHiding();
                    if (((this.state == State.PLAYING) || (this.state == State.PAUSED)) && (this.mControlListener != null))
                        this.mControlListener.onPlayPause();
                    break;
                case 1:
                    maybeStartHiding();
                }
        }
    }

    public void resetTime()
    {
        this.timeBar.resetTime();
    }

    public void setCanReplay(boolean paramBoolean)
    {
        this.canReplay = paramBoolean;
    }

    public void setControlListener(MiuiMediaController.OnControllerChangedListener paramOnControllerChangedListener)
    {
        this.mControlListener = paramOnControllerChangedListener;
    }

    public void setTimes(int paramInt1, int paramInt2)
    {
        this.timeBar.setTime(paramInt1, paramInt2);
    }

    public void show()
    {
        boolean bool = this.hidden;
        this.hidden = false;
        updateViews();
        setVisibility(0);
        setFocusable(false);
        if ((this.mControlListener != null) && (bool != this.hidden))
            this.mControlListener.onShown();
        maybeStartHiding();
    }

    public void showEnded()
    {
        this.state = State.ENDED;
        showMainView(this.playPauseReplayView);
    }

    public void showErrorMessage(String paramString)
    {
        this.state = State.ERROR;
        int i = (int)(0.1666667F * getMeasuredWidth());
        this.errorView.setPadding(i, 10, i, 10);
        this.errorView.setText(paramString);
        showMainView(this.errorView);
    }

    public void showLoading()
    {
        this.state = State.LOADING;
        showMainView(this.loadingView);
    }

    public void showPaused()
    {
        this.state = State.PAUSED;
        showMainView(this.playPauseReplayView);
    }

    public void showPlaying()
    {
        this.state = State.PLAYING;
        showMainView(this.playPauseReplayView);
    }

    private static enum State
    {
        static
        {
            PAUSED = new State("PAUSED", 1);
            ENDED = new State("ENDED", 2);
            ERROR = new State("ERROR", 3);
            LOADING = new State("LOADING", 4);
            State[] arrayOfState = new State[5];
            arrayOfState[0] = PLAYING;
            arrayOfState[1] = PAUSED;
            arrayOfState[2] = ENDED;
            arrayOfState[3] = ERROR;
            arrayOfState[4] = LOADING;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MiuiMovieController
 * JD-Core Version:        0.6.2
 */