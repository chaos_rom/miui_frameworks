package android.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;

public final class Space extends View
{
    public Space(Context paramContext)
    {
        this(paramContext, null);
    }

    public Space(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public Space(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        if (getVisibility() == 0)
            setVisibility(4);
    }

    private static int getDefaultSize2(int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt2);
        switch (j)
        {
        default:
        case 0:
        case -2147483648:
        case 1073741824:
        }
        while (true)
        {
            return i;
            i = paramInt1;
            continue;
            i = Math.min(paramInt1, k);
            continue;
            i = k;
        }
    }

    public void draw(Canvas paramCanvas)
    {
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        setMeasuredDimension(getDefaultSize2(getSuggestedMinimumWidth(), paramInt1), getDefaultSize2(getSuggestedMinimumHeight(), paramInt2));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Space
 * JD-Core Version:        0.6.2
 */