package android.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.RemotableViewMethod;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import com.android.internal.util.Predicate;
import com.google.android.collect.Lists;
import java.util.ArrayList;

@RemoteViews.RemoteView
public class ListView extends AbsListView
{
    private static final float MAX_SCROLL_FACTOR = 0.33F;
    private static final int MIN_SCROLL_PREVIEW_PIXELS = 2;
    static final int NO_POSITION = -1;
    private boolean mAreAllItemsSelectable = true;
    private final ArrowScrollFocusResult mArrowScrollFocusResult = new ArrowScrollFocusResult(null);
    Drawable mDivider;
    int mDividerHeight;
    private boolean mDividerIsOpaque;
    private Paint mDividerPaint;
    private FocusSelector mFocusSelector;
    private boolean mFooterDividersEnabled;
    private ArrayList<FixedViewInfo> mFooterViewInfos = Lists.newArrayList();
    private boolean mHeaderDividersEnabled;
    private ArrayList<FixedViewInfo> mHeaderViewInfos = Lists.newArrayList();
    private boolean mIsCacheColorOpaque;
    private boolean mItemsCanFocus = false;
    Drawable mOverScrollFooter;
    Drawable mOverScrollHeader;
    private final Rect mTempRect = new Rect();

    public ListView(Context paramContext)
    {
        this(paramContext, null);
    }

    public ListView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842868);
    }

    public ListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListView, paramInt, 0);
        CharSequence[] arrayOfCharSequence = localTypedArray.getTextArray(0);
        if (arrayOfCharSequence != null)
            setAdapter(new ArrayAdapter(paramContext, 17367043, arrayOfCharSequence));
        Drawable localDrawable1 = localTypedArray.getDrawable(1);
        if (localDrawable1 != null)
            setDivider(localDrawable1);
        Drawable localDrawable2 = localTypedArray.getDrawable(5);
        if (localDrawable2 != null)
            setOverscrollHeader(localDrawable2);
        Drawable localDrawable3 = localTypedArray.getDrawable(6);
        if (localDrawable3 != null)
            setOverscrollFooter(localDrawable3);
        int i = localTypedArray.getDimensionPixelSize(2, 0);
        if (i != 0)
            setDividerHeight(i);
        this.mHeaderDividersEnabled = localTypedArray.getBoolean(3, true);
        this.mFooterDividersEnabled = localTypedArray.getBoolean(4, true);
        localTypedArray.recycle();
    }

    private View addViewAbove(View paramView, int paramInt)
    {
        int i = paramInt - 1;
        View localView = obtainView(i, this.mIsScrap);
        setupChild(localView, i, paramView.getTop() - this.mDividerHeight, false, this.mListPadding.left, false, this.mIsScrap[0]);
        return localView;
    }

    private View addViewBelow(View paramView, int paramInt)
    {
        int i = paramInt + 1;
        View localView = obtainView(i, this.mIsScrap);
        setupChild(localView, i, paramView.getBottom() + this.mDividerHeight, true, this.mListPadding.left, false, this.mIsScrap[0]);
        return localView;
    }

    private void adjustViewsUpOrDown()
    {
        int i = getChildCount();
        int j;
        if (i > 0)
        {
            if (this.mStackFromBottom)
                break label64;
            j = getChildAt(0).getTop() - this.mListPadding.top;
            if (this.mFirstPosition != 0)
                j -= this.mDividerHeight;
            if (j < 0)
                j = 0;
        }
        while (true)
        {
            if (j != 0)
                offsetChildrenTopAndBottom(-j);
            return;
            label64: j = getChildAt(i - 1).getBottom() - (getHeight() - this.mListPadding.bottom);
            if (i + this.mFirstPosition < this.mItemCount)
                j += this.mDividerHeight;
            if (j > 0)
                j = 0;
        }
    }

    private int amountToScroll(int paramInt1, int paramInt2)
    {
        int i = getHeight() - this.mListPadding.bottom;
        int j = this.mListPadding.top;
        int k = getChildCount();
        View localView2;
        int i6;
        int i3;
        if (paramInt1 == 130)
        {
            int i4 = k - 1;
            if (paramInt2 != -1)
                i4 = paramInt2 - this.mFirstPosition;
            int i5 = i4 + this.mFirstPosition;
            localView2 = getChildAt(i4);
            i6 = i;
            if (i5 < -1 + this.mItemCount)
                i6 -= getArrowScrollPreviewLength();
            if (localView2.getBottom() <= i6)
                i3 = 0;
        }
        while (true)
        {
            return i3;
            if ((paramInt2 != -1) && (i6 - localView2.getTop() >= getMaxScrollAmount()))
            {
                i3 = 0;
            }
            else
            {
                int i7 = localView2.getBottom() - i6;
                if (k + this.mFirstPosition == this.mItemCount)
                    i7 = Math.min(i7, getChildAt(k - 1).getBottom() - i);
                i3 = Math.min(i7, getMaxScrollAmount());
                continue;
                int m = 0;
                if (paramInt2 != -1)
                    m = paramInt2 - this.mFirstPosition;
                int n = m + this.mFirstPosition;
                View localView1 = getChildAt(m);
                int i1 = j;
                if (n > 0)
                    i1 += getArrowScrollPreviewLength();
                if (localView1.getTop() >= i1)
                {
                    i3 = 0;
                }
                else if ((paramInt2 != -1) && (localView1.getBottom() - i1 >= getMaxScrollAmount()))
                {
                    i3 = 0;
                }
                else
                {
                    int i2 = i1 - localView1.getTop();
                    if (this.mFirstPosition == 0)
                        i2 = Math.min(i2, j - getChildAt(0).getTop());
                    i3 = Math.min(i2, getMaxScrollAmount());
                }
            }
        }
    }

    private int amountToScrollToNewFocus(int paramInt1, View paramView, int paramInt2)
    {
        int i = 0;
        paramView.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(paramView, this.mTempRect);
        if (paramInt1 == 33)
            if (this.mTempRect.top < this.mListPadding.top)
            {
                i = this.mListPadding.top - this.mTempRect.top;
                if (paramInt2 > 0)
                    i += getArrowScrollPreviewLength();
            }
        while (true)
        {
            return i;
            int j = getHeight() - this.mListPadding.bottom;
            if (this.mTempRect.bottom > j)
            {
                i = this.mTempRect.bottom - j;
                if (paramInt2 < -1 + this.mItemCount)
                    i += getArrowScrollPreviewLength();
            }
        }
    }

    private ArrowScrollFocusResult arrowScrollFocused(int paramInt)
    {
        View localView1 = getSelectedView();
        View localView2;
        int i1;
        ArrowScrollFocusResult localArrowScrollFocusResult;
        if ((localView1 != null) && (localView1.hasFocus()))
        {
            View localView3 = localView1.findFocus();
            localView2 = FocusFinder.getInstance().findNextFocus(this, localView3, paramInt);
            if (localView2 == null)
                break label418;
            i1 = positionOfNewFocus(localView2);
            if ((this.mSelectedPosition == -1) || (i1 == this.mSelectedPosition))
                break label329;
            int i4 = lookForSelectablePositionOnScreen(paramInt);
            if ((i4 == -1) || (((paramInt != 130) || (i4 >= i1)) && ((paramInt != 33) || (i4 <= i1))))
                break label329;
            localArrowScrollFocusResult = null;
        }
        while (true)
        {
            return localArrowScrollFocusResult;
            if (paramInt == 130)
            {
                int i5;
                label129: int i7;
                label149: int i8;
                if (this.mFirstPosition > 0)
                {
                    i5 = 1;
                    int i6 = this.mListPadding.top;
                    if (i5 == 0)
                        break label211;
                    i7 = getArrowScrollPreviewLength();
                    i8 = i6 + i7;
                    if ((localView1 == null) || (localView1.getTop() <= i8))
                        break label217;
                }
                label211: label217: for (int i9 = localView1.getTop(); ; i9 = i8)
                {
                    this.mTempRect.set(0, i9, 0, i9);
                    localView2 = FocusFinder.getInstance().findNextFocusFromRect(this, this.mTempRect, paramInt);
                    break;
                    i5 = 0;
                    break label129;
                    i7 = 0;
                    break label149;
                }
            }
            int i;
            label245: int k;
            label269: int m;
            if (-1 + (this.mFirstPosition + getChildCount()) < this.mItemCount)
            {
                i = 1;
                int j = getHeight() - this.mListPadding.bottom;
                if (i == 0)
                    break label316;
                k = getArrowScrollPreviewLength();
                m = j - k;
                if ((localView1 == null) || (localView1.getBottom() >= m))
                    break label322;
            }
            label316: label322: for (int n = localView1.getBottom(); ; n = m)
            {
                this.mTempRect.set(0, n, 0, n);
                break;
                i = 0;
                break label245;
                k = 0;
                break label269;
            }
            label329: int i2 = amountToScrollToNewFocus(paramInt, localView2, i1);
            int i3 = getMaxScrollAmount();
            if (i2 < i3)
            {
                localView2.requestFocus(paramInt);
                this.mArrowScrollFocusResult.populate(i1, i2);
                localArrowScrollFocusResult = this.mArrowScrollFocusResult;
            }
            else if (distanceToView(localView2) < i3)
            {
                localView2.requestFocus(paramInt);
                this.mArrowScrollFocusResult.populate(i1, i3);
                localArrowScrollFocusResult = this.mArrowScrollFocusResult;
            }
            else
            {
                label418: localArrowScrollFocusResult = null;
            }
        }
    }

    private boolean arrowScrollImpl(int paramInt)
    {
        boolean bool1 = false;
        if (getChildCount() <= 0)
            return bool1;
        View localView1 = getSelectedView();
        int i = this.mSelectedPosition;
        int j = lookForSelectablePositionOnScreen(paramInt);
        int k = amountToScroll(paramInt, j);
        ArrowScrollFocusResult localArrowScrollFocusResult;
        label52: int m;
        label79: boolean bool2;
        if (this.mItemsCanFocus)
        {
            localArrowScrollFocusResult = arrowScrollFocused(paramInt);
            if (localArrowScrollFocusResult != null)
            {
                j = localArrowScrollFocusResult.getSelectedPosition();
                k = localArrowScrollFocusResult.getAmountToScroll();
            }
            if (localArrowScrollFocusResult == null)
                break label315;
            m = 1;
            if (j != -1)
            {
                if (localArrowScrollFocusResult == null)
                    break label321;
                bool2 = true;
                label94: handleNewSelectionChange(localView1, paramInt, j, bool2);
                setSelectedPositionInt(j);
                setNextSelectedPositionInt(j);
                localView1 = getSelectedView();
                i = j;
                if ((this.mItemsCanFocus) && (localArrowScrollFocusResult == null))
                {
                    View localView3 = getFocusedChild();
                    if (localView3 != null)
                        localView3.clearFocus();
                }
                m = 1;
                checkSelectionChanged();
            }
            if (k > 0)
                if (paramInt != 33)
                    break label327;
        }
        while (true)
        {
            scrollListItemsBy(k);
            m = 1;
            if ((this.mItemsCanFocus) && (localArrowScrollFocusResult == null) && (localView1 != null) && (localView1.hasFocus()))
            {
                View localView2 = localView1.findFocus();
                if ((!isViewAncestorOf(localView2, this)) || (distanceToView(localView2) > 0))
                    localView2.clearFocus();
            }
            if ((j == -1) && (localView1 != null) && (!isViewAncestorOf(localView1, this)))
            {
                localView1 = null;
                hideSelector();
                this.mResurrectToPosition = -1;
            }
            if (m == 0)
                break;
            if (localView1 != null)
            {
                positionSelector(i, localView1);
                this.mSelectedTop = localView1.getTop();
            }
            if (!awakenScrollBars())
                invalidate();
            invokeOnItemScrollListener();
            bool1 = true;
            break;
            localArrowScrollFocusResult = null;
            break label52;
            label315: m = 0;
            break label79;
            label321: bool2 = false;
            break label94;
            label327: k = -k;
        }
    }

    private void clearRecycledState(ArrayList<FixedViewInfo> paramArrayList)
    {
        if (paramArrayList != null)
        {
            int i = paramArrayList.size();
            for (int j = 0; j < i; j++)
            {
                AbsListView.LayoutParams localLayoutParams = (AbsListView.LayoutParams)((FixedViewInfo)paramArrayList.get(j)).view.getLayoutParams();
                if (localLayoutParams != null)
                    localLayoutParams.recycledHeaderFooter = false;
            }
        }
    }

    private boolean commonKey(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        int i = 1;
        if ((this.mAdapter == null) || (!this.mIsAttached))
            i = 0;
        while (true)
        {
            label20: return i;
            if (this.mDataChanged)
                layoutChildren();
            boolean bool2 = false;
            int j = paramKeyEvent.getAction();
            if (j != i)
                switch (paramInt1)
                {
                default:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 66:
                case 62:
                case 92:
                case 93:
                case 122:
                case 123:
                }
            do
            {
                do
                {
                    do
                    {
                        do
                        {
                            do
                                while ((!bool2) && (!sendToTextFilter(paramInt1, paramInt2, paramKeyEvent)))
                                    switch (j)
                                    {
                                    default:
                                        i = 0;
                                        break label20;
                                        if (paramKeyEvent.hasNoModifiers())
                                        {
                                            bool2 = resurrectSelectionIfNeeded();
                                            if (!bool2)
                                                for (int m = paramInt2; ; m = paramInt2)
                                                {
                                                    paramInt2 = m - 1;
                                                    if ((m <= 0) || (!arrowScroll(33)))
                                                        break;
                                                    bool2 = true;
                                                }
                                        }
                                        else if (paramKeyEvent.hasModifiers(2))
                                        {
                                            if ((resurrectSelectionIfNeeded()) || (fullScroll(33)));
                                            for (bool2 = i; ; bool2 = false)
                                                break;
                                            if (paramKeyEvent.hasNoModifiers())
                                            {
                                                bool2 = resurrectSelectionIfNeeded();
                                                if (!bool2)
                                                    for (int k = paramInt2; ; k = paramInt2)
                                                    {
                                                        paramInt2 = k - 1;
                                                        if ((k <= 0) || (!arrowScroll(130)))
                                                            break;
                                                        bool2 = true;
                                                    }
                                            }
                                            else if (paramKeyEvent.hasModifiers(2))
                                            {
                                                if ((resurrectSelectionIfNeeded()) || (fullScroll(130)));
                                                for (bool2 = i; ; bool2 = false)
                                                    break;
                                                if (paramKeyEvent.hasNoModifiers())
                                                {
                                                    bool2 = handleHorizontalFocusWithinListItem(17);
                                                    continue;
                                                    if (paramKeyEvent.hasNoModifiers())
                                                    {
                                                        bool2 = handleHorizontalFocusWithinListItem(66);
                                                        continue;
                                                        if (paramKeyEvent.hasNoModifiers())
                                                        {
                                                            bool2 = resurrectSelectionIfNeeded();
                                                            if ((!bool2) && (paramKeyEvent.getRepeatCount() == 0) && (getChildCount() > 0))
                                                            {
                                                                keyPressed();
                                                                bool2 = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 0:
                                    case 1:
                                    case 2:
                                    }
                            while ((this.mPopup != null) && (this.mPopup.isShowing()));
                            if (paramKeyEvent.hasNoModifiers())
                                if ((!resurrectSelectionIfNeeded()) && (!pageScroll(130)));
                            while ((!paramKeyEvent.hasModifiers(i)) || ((!resurrectSelectionIfNeeded()) && (pageScroll(33))))
                                while (true)
                                {
                                    bool2 = true;
                                    break;
                                }
                            while (true);
                            if (paramKeyEvent.hasNoModifiers())
                            {
                                if ((resurrectSelectionIfNeeded()) || (pageScroll(33)));
                                for (bool2 = i; ; bool2 = false)
                                    break;
                            }
                        }
                        while (!paramKeyEvent.hasModifiers(2));
                        if ((resurrectSelectionIfNeeded()) || (fullScroll(33)));
                        for (bool2 = i; ; bool2 = false)
                            break;
                        if (paramKeyEvent.hasNoModifiers())
                        {
                            if ((resurrectSelectionIfNeeded()) || (pageScroll(130)));
                            for (bool2 = i; ; bool2 = false)
                                break;
                        }
                    }
                    while (!paramKeyEvent.hasModifiers(2));
                    if ((resurrectSelectionIfNeeded()) || (fullScroll(130)));
                    for (bool2 = i; ; bool2 = false)
                        break;
                }
                while (!paramKeyEvent.hasNoModifiers());
                if ((resurrectSelectionIfNeeded()) || (fullScroll(33)));
                for (bool2 = i; ; bool2 = false)
                    break;
            }
            while (!paramKeyEvent.hasNoModifiers());
            if ((resurrectSelectionIfNeeded()) || (fullScroll(130)));
            for (bool2 = i; ; bool2 = false)
                break;
            boolean bool1 = super.onKeyDown(paramInt1, paramKeyEvent);
            continue;
            bool1 = super.onKeyUp(paramInt1, paramKeyEvent);
            continue;
            bool1 = super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
        }
    }

    private void correctTooHigh(int paramInt)
    {
        if ((-1 + (paramInt + this.mFirstPosition) == -1 + this.mItemCount) && (paramInt > 0))
        {
            int i = getChildAt(paramInt - 1).getBottom();
            int j = this.mBottom - this.mTop - this.mListPadding.bottom - i;
            View localView = getChildAt(0);
            int k = localView.getTop();
            if ((j > 0) && ((this.mFirstPosition > 0) || (k < this.mListPadding.top)))
            {
                if (this.mFirstPosition == 0)
                    j = Math.min(j, this.mListPadding.top - k);
                offsetChildrenTopAndBottom(j);
                if (this.mFirstPosition > 0)
                {
                    fillUp(-1 + this.mFirstPosition, localView.getTop() - this.mDividerHeight);
                    adjustViewsUpOrDown();
                }
            }
        }
    }

    private void correctTooLow(int paramInt)
    {
        int i1;
        if ((this.mFirstPosition == 0) && (paramInt > 0))
        {
            int i = getChildAt(0).getTop();
            int j = this.mListPadding.top;
            int k = this.mBottom - this.mTop - this.mListPadding.bottom;
            int m = i - j;
            View localView = getChildAt(paramInt - 1);
            int n = localView.getBottom();
            i1 = -1 + (paramInt + this.mFirstPosition);
            if (m > 0)
            {
                if ((i1 >= -1 + this.mItemCount) && (n <= k))
                    break label170;
                if (i1 == -1 + this.mItemCount)
                    m = Math.min(m, n - k);
                offsetChildrenTopAndBottom(-m);
                if (i1 < -1 + this.mItemCount)
                {
                    fillDown(i1 + 1, localView.getBottom() + this.mDividerHeight);
                    adjustViewsUpOrDown();
                }
            }
        }
        while (true)
        {
            return;
            label170: if (i1 == -1 + this.mItemCount)
                adjustViewsUpOrDown();
        }
    }

    private int distanceToView(View paramView)
    {
        int i = 0;
        paramView.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(paramView, this.mTempRect);
        int j = this.mBottom - this.mTop - this.mListPadding.bottom;
        if (this.mTempRect.bottom < this.mListPadding.top)
            i = this.mListPadding.top - this.mTempRect.bottom;
        while (true)
        {
            return i;
            if (this.mTempRect.top > j)
                i = this.mTempRect.top - j;
        }
    }

    private void fillAboveAndBelow(View paramView, int paramInt)
    {
        int i = this.mDividerHeight;
        if (!this.mStackFromBottom)
        {
            fillUp(paramInt - 1, paramView.getTop() - i);
            adjustViewsUpOrDown();
            fillDown(paramInt + 1, i + paramView.getBottom());
        }
        while (true)
        {
            return;
            fillDown(paramInt + 1, i + paramView.getBottom());
            adjustViewsUpOrDown();
            fillUp(paramInt - 1, paramView.getTop() - i);
        }
    }

    private View fillDown(int paramInt1, int paramInt2)
    {
        Object localObject = null;
        int i = this.mBottom - this.mTop;
        if ((0x22 & this.mGroupFlags) == 34)
            i -= this.mListPadding.bottom;
        if ((paramInt2 < i) && (paramInt1 < this.mItemCount))
        {
            if (paramInt1 == this.mSelectedPosition);
            for (boolean bool = true; ; bool = false)
            {
                int j = this.mListPadding.left;
                View localView = makeAndAddView(paramInt1, paramInt2, true, j, bool);
                paramInt2 = localView.getBottom() + this.mDividerHeight;
                if (bool)
                    localObject = localView;
                paramInt1++;
                break;
            }
        }
        setVisibleRangeHint(this.mFirstPosition, -1 + (this.mFirstPosition + getChildCount()));
        return localObject;
    }

    private View fillFromMiddle(int paramInt1, int paramInt2)
    {
        int i = paramInt2 - paramInt1;
        int j = reconcileSelectedPosition();
        View localView = makeAndAddView(j, paramInt1, true, this.mListPadding.left, true);
        this.mFirstPosition = j;
        int k = localView.getMeasuredHeight();
        if (k <= i)
            localView.offsetTopAndBottom((i - k) / 2);
        fillAboveAndBelow(localView, j);
        if (!this.mStackFromBottom)
            correctTooHigh(getChildCount());
        while (true)
        {
            return localView;
            correctTooLow(getChildCount());
        }
    }

    private View fillFromSelection(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = getVerticalFadingEdgeLength();
        int j = this.mSelectedPosition;
        int k = getTopSelectionPixel(paramInt2, i, j);
        int m = getBottomSelectionPixel(paramInt3, i, j);
        View localView = makeAndAddView(j, paramInt1, true, this.mListPadding.left, true);
        if (localView.getBottom() > m)
        {
            localView.offsetTopAndBottom(-Math.min(localView.getTop() - k, localView.getBottom() - m));
            fillAboveAndBelow(localView, j);
            if (this.mStackFromBottom)
                break label150;
            correctTooHigh(getChildCount());
        }
        while (true)
        {
            return localView;
            if (localView.getTop() >= k)
                break;
            localView.offsetTopAndBottom(Math.min(k - localView.getTop(), m - localView.getBottom()));
            break;
            label150: correctTooLow(getChildCount());
        }
    }

    private View fillFromTop(int paramInt)
    {
        this.mFirstPosition = Math.min(this.mFirstPosition, this.mSelectedPosition);
        this.mFirstPosition = Math.min(this.mFirstPosition, -1 + this.mItemCount);
        if (this.mFirstPosition < 0)
            this.mFirstPosition = 0;
        return fillDown(this.mFirstPosition, paramInt);
    }

    private View fillSpecific(int paramInt1, int paramInt2)
    {
        boolean bool;
        Object localObject;
        int i;
        View localView2;
        View localView1;
        if (paramInt1 == this.mSelectedPosition)
        {
            bool = true;
            localObject = makeAndAddView(paramInt1, paramInt2, true, this.mListPadding.left, bool);
            this.mFirstPosition = paramInt1;
            i = this.mDividerHeight;
            if (this.mStackFromBottom)
                break label112;
            localView2 = fillUp(paramInt1 - 1, ((View)localObject).getTop() - i);
            adjustViewsUpOrDown();
            localView1 = fillDown(paramInt1 + 1, i + ((View)localObject).getBottom());
            int k = getChildCount();
            if (k > 0)
                correctTooHigh(k);
            label100: if (!bool)
                break label170;
        }
        while (true)
        {
            return localObject;
            bool = false;
            break;
            label112: localView1 = fillDown(paramInt1 + 1, i + ((View)localObject).getBottom());
            adjustViewsUpOrDown();
            localView2 = fillUp(paramInt1 - 1, ((View)localObject).getTop() - i);
            int j = getChildCount();
            if (j <= 0)
                break label100;
            correctTooLow(j);
            break label100;
            label170: if (localView2 != null)
                localObject = localView2;
            else
                localObject = localView1;
        }
    }

    private View fillUp(int paramInt1, int paramInt2)
    {
        Object localObject = null;
        int i = 0;
        if ((0x22 & this.mGroupFlags) == 34)
            i = this.mListPadding.top;
        if ((paramInt2 > i) && (paramInt1 >= 0))
        {
            if (paramInt1 == this.mSelectedPosition);
            for (boolean bool = true; ; bool = false)
            {
                int j = this.mListPadding.left;
                View localView = makeAndAddView(paramInt1, paramInt2, false, j, bool);
                paramInt2 = localView.getTop() - this.mDividerHeight;
                if (bool)
                    localObject = localView;
                paramInt1--;
                break;
            }
        }
        this.mFirstPosition = (paramInt1 + 1);
        setVisibleRangeHint(this.mFirstPosition, -1 + (this.mFirstPosition + getChildCount()));
        return localObject;
    }

    private int getArrowScrollPreviewLength()
    {
        return Math.max(2, getVerticalFadingEdgeLength());
    }

    private int getBottomSelectionPixel(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1;
        if (paramInt3 != -1 + this.mItemCount)
            i -= paramInt2;
        return i;
    }

    private int getTopSelectionPixel(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1;
        if (paramInt3 > 0)
            i += paramInt2;
        return i;
    }

    private boolean handleHorizontalFocusWithinListItem(int paramInt)
    {
        if ((paramInt != 17) && (paramInt != 66))
            throw new IllegalArgumentException("direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT}");
        int i = getChildCount();
        View localView2;
        boolean bool;
        if ((this.mItemsCanFocus) && (i > 0) && (this.mSelectedPosition != -1))
        {
            View localView1 = getSelectedView();
            if ((localView1 != null) && (localView1.hasFocus()) && ((localView1 instanceof ViewGroup)))
            {
                localView2 = localView1.findFocus();
                View localView3 = FocusFinder.getInstance().findNextFocus((ViewGroup)localView1, localView2, paramInt);
                if (localView3 != null)
                {
                    localView2.getFocusedRect(this.mTempRect);
                    offsetDescendantRectToMyCoords(localView2, this.mTempRect);
                    offsetRectIntoDescendantCoords(localView3, this.mTempRect);
                    if (localView3.requestFocus(paramInt, this.mTempRect))
                        bool = true;
                }
            }
        }
        while (true)
        {
            return bool;
            View localView4 = FocusFinder.getInstance().findNextFocus((ViewGroup)getRootView(), localView2, paramInt);
            if (localView4 != null)
                bool = isViewAncestorOf(localView4, this);
            else
                bool = false;
        }
    }

    private void handleNewSelectionChange(View paramView, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (paramInt2 == -1)
            throw new IllegalArgumentException("newSelectedPosition needs to be valid");
        int i = 0;
        int j = this.mSelectedPosition - this.mFirstPosition;
        int k = paramInt2 - this.mFirstPosition;
        int m;
        int n;
        View localView1;
        View localView2;
        int i1;
        boolean bool2;
        if (paramInt1 == 33)
        {
            m = k;
            n = j;
            localView1 = getChildAt(m);
            localView2 = paramView;
            i = 1;
            i1 = getChildCount();
            if (localView1 != null)
            {
                if ((paramBoolean) || (i == 0))
                    break label166;
                bool2 = true;
                label91: localView1.setSelected(bool2);
                measureAndAdjustDown(localView1, m, i1);
            }
            if (localView2 != null)
                if ((paramBoolean) || (i != 0))
                    break label172;
        }
        label166: label172: for (boolean bool1 = true; ; bool1 = false)
        {
            localView2.setSelected(bool1);
            measureAndAdjustDown(localView2, n, i1);
            return;
            m = j;
            n = k;
            localView1 = paramView;
            localView2 = getChildAt(n);
            break;
            bool2 = false;
            break label91;
        }
    }

    private boolean isDirectChildHeaderOrFooter(View paramView)
    {
        ArrayList localArrayList1 = this.mHeaderViewInfos;
        int i = localArrayList1.size();
        int j = 0;
        boolean bool;
        if (j < i)
            if (paramView == ((FixedViewInfo)localArrayList1.get(j)).view)
                bool = true;
        while (true)
        {
            return bool;
            j++;
            break;
            ArrayList localArrayList2 = this.mFooterViewInfos;
            int k = localArrayList2.size();
            for (int m = 0; ; m++)
            {
                if (m >= k)
                    break label99;
                if (paramView == ((FixedViewInfo)localArrayList2.get(m)).view)
                {
                    bool = true;
                    break;
                }
            }
            label99: bool = false;
        }
    }

    private boolean isViewAncestorOf(View paramView1, View paramView2)
    {
        boolean bool = true;
        if (paramView1 == paramView2);
        while (true)
        {
            return bool;
            ViewParent localViewParent = paramView1.getParent();
            if ((!(localViewParent instanceof ViewGroup)) || (!isViewAncestorOf((View)localViewParent, paramView2)))
                bool = false;
        }
    }

    private int lookForSelectablePositionOnScreen(int paramInt)
    {
        int i = this.mFirstPosition;
        int n;
        int m;
        if (paramInt == 130)
            if (this.mSelectedPosition != -1)
            {
                n = 1 + this.mSelectedPosition;
                if (n < this.mAdapter.getCount())
                    break label56;
                m = -1;
            }
        while (true)
        {
            return m;
            n = i;
            break;
            label56: if (n < i)
                n = i;
            int i1 = getLastVisiblePosition();
            ListAdapter localListAdapter2 = getAdapter();
            m = n;
            while (true)
                if (m <= i1)
                {
                    if ((localListAdapter2.isEnabled(m)) && (getChildAt(m - i).getVisibility() == 0))
                        break;
                    m++;
                    continue;
                    int j = -1 + (i + getChildCount());
                    if (this.mSelectedPosition != -1);
                    for (int k = -1 + this.mSelectedPosition; ; k = -1 + (i + getChildCount()))
                    {
                        if ((k >= 0) && (k < this.mAdapter.getCount()))
                            break label188;
                        m = -1;
                        break;
                    }
                    label188: if (k > j)
                        k = j;
                    ListAdapter localListAdapter1 = getAdapter();
                    for (m = k; ; m--)
                    {
                        if (m < i)
                            break label245;
                        if ((localListAdapter1.isEnabled(m)) && (getChildAt(m - i).getVisibility() == 0))
                            break;
                    }
                }
            label245: m = -1;
        }
    }

    private View makeAndAddView(int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2)
    {
        View localView2;
        if (!this.mDataChanged)
        {
            localView2 = this.mRecycler.getActiveView(paramInt1);
            if (localView2 != null)
                setupChild(localView2, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, true);
        }
        View localView1;
        for (Object localObject = localView2; ; localObject = localView1)
        {
            return localObject;
            localView1 = obtainView(paramInt1, this.mIsScrap);
            setupChild(localView1, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, this.mIsScrap[0]);
        }
    }

    private void measureAndAdjustDown(View paramView, int paramInt1, int paramInt2)
    {
        int i = paramView.getHeight();
        measureItem(paramView);
        if (paramView.getMeasuredHeight() != i)
        {
            relayoutMeasuredItem(paramView);
            int j = paramView.getMeasuredHeight() - i;
            for (int k = paramInt1 + 1; k < paramInt2; k++)
                getChildAt(k).offsetTopAndBottom(j);
        }
    }

    private void measureItem(View paramView)
    {
        ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
        if (localLayoutParams == null)
            localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
        int i = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mListPadding.left + this.mListPadding.right, localLayoutParams.width);
        int j = localLayoutParams.height;
        if (j > 0);
        for (int k = View.MeasureSpec.makeMeasureSpec(j, 1073741824); ; k = View.MeasureSpec.makeMeasureSpec(0, 0))
        {
            paramView.measure(i, k);
            return;
        }
    }

    private void measureScrapChild(View paramView, int paramInt1, int paramInt2)
    {
        AbsListView.LayoutParams localLayoutParams = (AbsListView.LayoutParams)paramView.getLayoutParams();
        if (localLayoutParams == null)
        {
            localLayoutParams = (AbsListView.LayoutParams)generateDefaultLayoutParams();
            paramView.setLayoutParams(localLayoutParams);
        }
        localLayoutParams.viewType = this.mAdapter.getItemViewType(paramInt1);
        localLayoutParams.forceAdd = true;
        int i = ViewGroup.getChildMeasureSpec(paramInt2, this.mListPadding.left + this.mListPadding.right, localLayoutParams.width);
        int j = localLayoutParams.height;
        if (j > 0);
        for (int k = View.MeasureSpec.makeMeasureSpec(j, 1073741824); ; k = View.MeasureSpec.makeMeasureSpec(0, 0))
        {
            paramView.measure(i, k);
            return;
        }
    }

    private View moveSelection(View paramView1, View paramView2, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = getVerticalFadingEdgeLength();
        int j = this.mSelectedPosition;
        int k = getTopSelectionPixel(paramInt2, i, j);
        int m = getBottomSelectionPixel(paramInt2, i, j);
        int i4;
        View localView1;
        if (paramInt1 > 0)
        {
            View localView2 = makeAndAddView(j - 1, paramView1.getTop(), true, this.mListPadding.left, false);
            i4 = this.mDividerHeight;
            localView1 = makeAndAddView(j, i4 + localView2.getBottom(), true, this.mListPadding.left, true);
            if (localView1.getBottom() > m)
            {
                int i5 = localView1.getTop() - k;
                int i6 = localView1.getBottom() - m;
                int i7 = (paramInt3 - paramInt2) / 2;
                int i8 = Math.min(Math.min(i5, i6), i7);
                localView2.offsetTopAndBottom(-i8);
                localView1.offsetTopAndBottom(-i8);
            }
            if (!this.mStackFromBottom)
            {
                fillUp(-2 + this.mSelectedPosition, localView1.getTop() - i4);
                adjustViewsUpOrDown();
                fillDown(1 + this.mSelectedPosition, i4 + localView1.getBottom());
            }
        }
        while (true)
        {
            return localView1;
            fillDown(1 + this.mSelectedPosition, i4 + localView1.getBottom());
            adjustViewsUpOrDown();
            fillUp(-2 + this.mSelectedPosition, localView1.getTop() - i4);
            continue;
            if (paramInt1 < 0)
            {
                if (paramView2 != null);
                for (localView1 = makeAndAddView(j, paramView2.getTop(), true, this.mListPadding.left, true); ; localView1 = makeAndAddView(j, paramView1.getTop(), false, this.mListPadding.left, true))
                {
                    if (localView1.getTop() < k)
                    {
                        int i1 = k - localView1.getTop();
                        int i2 = m - localView1.getBottom();
                        int i3 = (paramInt3 - paramInt2) / 2;
                        localView1.offsetTopAndBottom(Math.min(Math.min(i1, i2), i3));
                    }
                    fillAboveAndBelow(localView1, j);
                    break;
                }
            }
            int n = paramView1.getTop();
            localView1 = makeAndAddView(j, n, true, this.mListPadding.left, true);
            if ((n < paramInt2) && (localView1.getBottom() < paramInt2 + 20))
                localView1.offsetTopAndBottom(paramInt2 - localView1.getTop());
            fillAboveAndBelow(localView1, j);
        }
    }

    private int positionOfNewFocus(View paramView)
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
            if (isViewAncestorOf(paramView, getChildAt(j)))
                return j + this.mFirstPosition;
        throw new IllegalArgumentException("newFocus is not a child of any of the children of the list!");
    }

    private void relayoutMeasuredItem(View paramView)
    {
        int i = paramView.getMeasuredWidth();
        int j = paramView.getMeasuredHeight();
        int k = this.mListPadding.left;
        int m = k + i;
        int n = paramView.getTop();
        paramView.layout(k, n, m, n + j);
    }

    private void removeFixedViewInfo(View paramView, ArrayList<FixedViewInfo> paramArrayList)
    {
        int i = paramArrayList.size();
        for (int j = 0; ; j++)
            if (j < i)
            {
                if (((FixedViewInfo)paramArrayList.get(j)).view == paramView)
                    paramArrayList.remove(j);
            }
            else
                return;
    }

    private void scrollListItemsBy(int paramInt)
    {
        offsetChildrenTopAndBottom(paramInt);
        int i = getHeight() - this.mListPadding.bottom;
        int j = this.mListPadding.top;
        AbsListView.RecycleBin localRecycleBin = this.mRecycler;
        if (paramInt < 0)
        {
            int m = getChildCount();
            View localView3 = getChildAt(m - 1);
            while (localView3.getBottom() < i)
            {
                int n = -1 + (m + this.mFirstPosition);
                if (n >= -1 + this.mItemCount)
                    break;
                localView3 = addViewBelow(localView3, n);
                m++;
            }
            if (localView3.getBottom() < i)
                offsetChildrenTopAndBottom(i - localView3.getBottom());
            View localView4 = getChildAt(0);
            if (localView4.getBottom() < j)
            {
                if (localRecycleBin.shouldRecycleViewType(((AbsListView.LayoutParams)localView4.getLayoutParams()).viewType))
                {
                    detachViewFromParent(localView4);
                    localRecycleBin.addScrapView(localView4, this.mFirstPosition);
                }
                while (true)
                {
                    localView4 = getChildAt(0);
                    this.mFirstPosition = (1 + this.mFirstPosition);
                    break;
                    removeViewInLayout(localView4);
                }
            }
        }
        else
        {
            View localView1 = getChildAt(0);
            while ((localView1.getTop() > j) && (this.mFirstPosition > 0))
            {
                localView1 = addViewAbove(localView1, this.mFirstPosition);
                this.mFirstPosition = (-1 + this.mFirstPosition);
            }
            if (localView1.getTop() > j)
                offsetChildrenTopAndBottom(j - localView1.getTop());
            int k = -1 + getChildCount();
            View localView2 = getChildAt(k);
            if (localView2.getTop() > i)
            {
                if (localRecycleBin.shouldRecycleViewType(((AbsListView.LayoutParams)localView2.getLayoutParams()).viewType))
                {
                    detachViewFromParent(localView2);
                    localRecycleBin.addScrapView(localView2, k + this.mFirstPosition);
                }
                while (true)
                {
                    k--;
                    localView2 = getChildAt(k);
                    break;
                    removeViewInLayout(localView2);
                }
            }
        }
    }

    private void setupChild(View paramView, int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, boolean paramBoolean3)
    {
        boolean bool1;
        int i;
        label27: boolean bool2;
        label55: int k;
        label67: int m;
        label87: AbsListView.LayoutParams localLayoutParams;
        int i7;
        label165: label234: int i6;
        label290: label298: int i2;
        int i3;
        if ((paramBoolean2) && (shouldShowSelector()))
        {
            bool1 = true;
            if (bool1 == paramView.isSelected())
                break label390;
            i = 1;
            int j = this.mTouchMode;
            if ((j <= 0) || (j >= 3) || (this.mMotionPosition != paramInt1))
                break label396;
            bool2 = true;
            if (bool2 == paramView.isPressed())
                break label402;
            k = 1;
            if ((paramBoolean3) && (i == 0) && (!paramView.isLayoutRequested()))
                break label408;
            m = 1;
            localLayoutParams = (AbsListView.LayoutParams)paramView.getLayoutParams();
            if (localLayoutParams == null)
                localLayoutParams = (AbsListView.LayoutParams)generateDefaultLayoutParams();
            localLayoutParams.viewType = this.mAdapter.getItemViewType(paramInt1);
            if (((!paramBoolean3) || (localLayoutParams.forceAdd)) && ((!localLayoutParams.recycledHeaderFooter) || (localLayoutParams.viewType != -2)))
                break label420;
            if (!paramBoolean1)
                break label414;
            i7 = -1;
            attachViewToParent(paramView, i7, localLayoutParams);
            if (i != 0)
                paramView.setSelected(bool1);
            if (k != 0)
                paramView.setPressed(bool2);
            if ((this.mChoiceMode != 0) && (this.mCheckStates != null))
            {
                if (!(paramView instanceof Checkable))
                    break label471;
                ((Checkable)paramView).setChecked(this.mCheckStates.get(paramInt1));
            }
            if (m == 0)
                break label511;
            int i4 = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mListPadding.left + this.mListPadding.right, localLayoutParams.width);
            int i5 = localLayoutParams.height;
            if (i5 <= 0)
                break label501;
            i6 = View.MeasureSpec.makeMeasureSpec(i5, 1073741824);
            paramView.measure(i4, i6);
            int i1 = paramView.getMeasuredWidth();
            i2 = paramView.getMeasuredHeight();
            if (!paramBoolean1)
                break label519;
            i3 = paramInt2;
            label318: if (m == 0)
                break label528;
            paramView.layout(paramInt3, i3, paramInt3 + i1, i3 + i2);
        }
        while (true)
        {
            if ((this.mCachingStarted) && (!paramView.isDrawingCacheEnabled()))
                paramView.setDrawingCacheEnabled(true);
            if ((paramBoolean3) && (((AbsListView.LayoutParams)paramView.getLayoutParams()).scrappedFromPosition != paramInt1))
                paramView.jumpDrawablesToCurrentState();
            return;
            bool1 = false;
            break;
            label390: i = 0;
            break label27;
            label396: bool2 = false;
            break label55;
            label402: k = 0;
            break label67;
            label408: m = 0;
            break label87;
            label414: i7 = 0;
            break label165;
            label420: localLayoutParams.forceAdd = false;
            if (localLayoutParams.viewType == -2)
                localLayoutParams.recycledHeaderFooter = true;
            if (paramBoolean1);
            for (int n = -1; ; n = 0)
            {
                addViewInLayout(paramView, n, localLayoutParams, true);
                break;
            }
            label471: if (getContext().getApplicationInfo().targetSdkVersion < 11)
                break label234;
            paramView.setActivated(this.mCheckStates.get(paramInt1));
            break label234;
            label501: i6 = View.MeasureSpec.makeMeasureSpec(0, 0);
            break label290;
            label511: cleanupLayoutState(paramView);
            break label298;
            label519: i3 = paramInt2 - i2;
            break label318;
            label528: paramView.offsetLeftAndRight(paramInt3 - paramView.getLeft());
            paramView.offsetTopAndBottom(i3 - paramView.getTop());
        }
    }

    private boolean showingBottomFadingEdge()
    {
        int i = getChildCount();
        int j = getChildAt(i - 1).getBottom();
        int k = -1 + (i + this.mFirstPosition);
        int m = this.mScrollY + getHeight() - this.mListPadding.bottom;
        if ((k < -1 + this.mItemCount) || (j < m));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean showingTopFadingEdge()
    {
        boolean bool = false;
        int i = this.mScrollY + this.mListPadding.top;
        if ((this.mFirstPosition > 0) || (getChildAt(0).getTop() > i))
            bool = true;
        return bool;
    }

    public void addFooterView(View paramView)
    {
        addFooterView(paramView, null, true);
    }

    public void addFooterView(View paramView, Object paramObject, boolean paramBoolean)
    {
        FixedViewInfo localFixedViewInfo = new FixedViewInfo();
        localFixedViewInfo.view = paramView;
        localFixedViewInfo.data = paramObject;
        localFixedViewInfo.isSelectable = paramBoolean;
        this.mFooterViewInfos.add(localFixedViewInfo);
        if ((this.mAdapter != null) && (this.mDataSetObserver != null))
            this.mDataSetObserver.onChanged();
    }

    public void addHeaderView(View paramView)
    {
        addHeaderView(paramView, null, true);
    }

    public void addHeaderView(View paramView, Object paramObject, boolean paramBoolean)
    {
        if ((this.mAdapter != null) && (!(this.mAdapter instanceof HeaderViewListAdapter)))
            throw new IllegalStateException("Cannot add header view to list -- setAdapter has already been called.");
        FixedViewInfo localFixedViewInfo = new FixedViewInfo();
        localFixedViewInfo.view = paramView;
        localFixedViewInfo.data = paramObject;
        localFixedViewInfo.isSelectable = paramBoolean;
        this.mHeaderViewInfos.add(localFixedViewInfo);
        if ((this.mAdapter != null) && (this.mDataSetObserver != null))
            this.mDataSetObserver.onChanged();
    }

    boolean arrowScroll(int paramInt)
    {
        try
        {
            this.mInLayout = true;
            boolean bool = arrowScrollImpl(paramInt);
            if (bool)
                playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt));
            return bool;
        }
        finally
        {
            this.mInLayout = false;
        }
    }

    protected boolean canAnimate()
    {
        if ((super.canAnimate()) && (this.mItemCount > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        if (this.mCachingStarted)
            this.mCachingActive = true;
        int i = this.mDividerHeight;
        Drawable localDrawable1 = this.mOverScrollHeader;
        Drawable localDrawable2 = this.mOverScrollFooter;
        int j;
        int k;
        label43: int m;
        label57: Rect localRect;
        int n;
        int i1;
        int i2;
        int i3;
        boolean bool1;
        boolean bool2;
        int i4;
        boolean bool3;
        ListAdapter localListAdapter;
        int i5;
        label189: Paint localPaint;
        int i6;
        int i8;
        int i15;
        label343: int i17;
        if (localDrawable1 != null)
        {
            j = 1;
            if (localDrawable2 == null)
                break label511;
            k = 1;
            if ((i <= 0) || (this.mDivider == null))
                break label517;
            m = 1;
            if ((m == 0) && (j == 0) && (k == 0))
                break label647;
            localRect = this.mTempRect;
            localRect.left = this.mPaddingLeft;
            localRect.right = (this.mRight - this.mLeft - this.mPaddingRight);
            n = getChildCount();
            i1 = this.mHeaderViewInfos.size();
            i2 = this.mItemCount;
            i3 = -1 + (i2 - this.mFooterViewInfos.size());
            bool1 = this.mHeaderDividersEnabled;
            bool2 = this.mFooterDividersEnabled;
            i4 = this.mFirstPosition;
            bool3 = this.mAreAllItemsSelectable;
            localListAdapter = this.mAdapter;
            if ((!isOpaque()) || (super.isOpaque()))
                break label523;
            i5 = 1;
            if ((i5 != 0) && (this.mDividerPaint == null) && (this.mIsCacheColorOpaque))
            {
                this.mDividerPaint = new Paint();
                this.mDividerPaint.setColor(getCacheColorHint());
            }
            localPaint = this.mDividerPaint;
            i6 = 0;
            int i7 = 0;
            if ((0x22 & this.mGroupFlags) == 34)
            {
                i6 = this.mListPadding.top;
                i7 = this.mListPadding.bottom;
            }
            i8 = this.mBottom - this.mTop - i7 + this.mScrollY;
            if (this.mStackFromBottom)
                break label653;
            i15 = 0;
            int i16 = this.mScrollY;
            if ((n > 0) && (i16 < 0))
            {
                if (j == 0)
                    break label529;
                localRect.bottom = 0;
                localRect.top = i16;
                drawOverscrollHeader(paramCanvas, localDrawable1, localRect);
            }
            i17 = 0;
            label346: if (i17 >= n)
                break label591;
            if (((bool1) || (i4 + i17 >= i1)) && ((bool2) || (i4 + i17 < i3)))
            {
                i15 = getChildAt(i17).getBottom();
                if ((m != 0) && (i15 < i8))
                    if (k != 0)
                    {
                        int i20 = n - 1;
                        if (i17 == i20);
                    }
                    else
                    {
                        if (!bool3)
                        {
                            if (!localListAdapter.isEnabled(i4 + i17))
                                break label559;
                            int i19 = n - 1;
                            if ((i17 != i19) && (!localListAdapter.isEnabled(1 + (i4 + i17))))
                                break label559;
                        }
                        localRect.top = i15;
                        localRect.bottom = (i15 + i);
                        drawDivider(paramCanvas, localRect, i17);
                    }
            }
        }
        while (true)
        {
            i17++;
            break label346;
            j = 0;
            break;
            label511: k = 0;
            break label43;
            label517: m = 0;
            break label57;
            label523: i5 = 0;
            break label189;
            label529: if (m == 0)
                break label343;
            localRect.bottom = 0;
            localRect.top = (-i);
            drawDivider(paramCanvas, localRect, -1);
            break label343;
            label559: if (i5 != 0)
            {
                localRect.top = i15;
                localRect.bottom = (i15 + i);
                paramCanvas.drawRect(localRect, localPaint);
            }
        }
        label591: int i18 = this.mBottom + this.mScrollY;
        if ((k != 0) && (i4 + n == i2) && (i18 > i15))
        {
            localRect.top = i15;
            localRect.bottom = i18;
            drawOverscrollFooter(paramCanvas, localDrawable2, localRect);
        }
        while (true)
        {
            label647: super.dispatchDraw(paramCanvas);
            return;
            label653: int i9 = this.mScrollY;
            if ((n > 0) && (j != 0))
            {
                localRect.top = i9;
                localRect.bottom = getChildAt(0).getTop();
                drawOverscrollHeader(paramCanvas, localDrawable1, localRect);
            }
            int i10;
            int i11;
            label709: int i13;
            if (j != 0)
            {
                i10 = 1;
                i11 = i10;
                if (i11 >= n)
                    break label885;
                if (((bool1) || (i4 + i11 >= i1)) && ((bool2) || (i4 + i11 < i3)))
                {
                    i13 = getChildAt(i11).getTop();
                    if (i13 > i6)
                    {
                        if (!bool3)
                        {
                            if (!localListAdapter.isEnabled(i4 + i11))
                                break label853;
                            int i14 = n - 1;
                            if ((i11 != i14) && (!localListAdapter.isEnabled(1 + (i4 + i11))))
                                break label853;
                        }
                        localRect.top = (i13 - i);
                        localRect.bottom = i13;
                        drawDivider(paramCanvas, localRect, i11 - 1);
                    }
                }
            }
            while (true)
            {
                i11++;
                break label709;
                i10 = 0;
                break;
                label853: if (i5 != 0)
                {
                    localRect.top = (i13 - i);
                    localRect.bottom = i13;
                    paramCanvas.drawRect(localRect, localPaint);
                }
            }
            label885: if ((n > 0) && (i9 > 0))
                if (k != 0)
                {
                    int i12 = this.mBottom;
                    localRect.top = i12;
                    localRect.bottom = (i12 + i9);
                    drawOverscrollFooter(paramCanvas, localDrawable2, localRect);
                }
                else if (m != 0)
                {
                    localRect.top = i8;
                    localRect.bottom = (i8 + i);
                    drawDivider(paramCanvas, localRect, -1);
                }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool = super.dispatchKeyEvent(paramKeyEvent);
        if ((!bool) && (getFocusedChild() != null) && (paramKeyEvent.getAction() == 0))
            bool = onKeyDown(paramKeyEvent.getKeyCode(), paramKeyEvent);
        return bool;
    }

    protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
    {
        boolean bool = super.drawChild(paramCanvas, paramView, paramLong);
        if ((this.mCachingActive) && (paramView.mCachingFailed))
            this.mCachingActive = false;
        return bool;
    }

    void drawDivider(Canvas paramCanvas, Rect paramRect, int paramInt)
    {
        Drawable localDrawable = this.mDivider;
        localDrawable.setBounds(paramRect);
        localDrawable.draw(paramCanvas);
    }

    void drawOverscrollFooter(Canvas paramCanvas, Drawable paramDrawable, Rect paramRect)
    {
        int i = paramDrawable.getMinimumHeight();
        paramCanvas.save();
        paramCanvas.clipRect(paramRect);
        if (paramRect.bottom - paramRect.top < i)
            paramRect.bottom = (i + paramRect.top);
        paramDrawable.setBounds(paramRect);
        paramDrawable.draw(paramCanvas);
        paramCanvas.restore();
    }

    void drawOverscrollHeader(Canvas paramCanvas, Drawable paramDrawable, Rect paramRect)
    {
        int i = paramDrawable.getMinimumHeight();
        paramCanvas.save();
        paramCanvas.clipRect(paramRect);
        if (paramRect.bottom - paramRect.top < i)
            paramRect.top = (paramRect.bottom - i);
        paramDrawable.setBounds(paramRect);
        paramDrawable.draw(paramCanvas);
        paramCanvas.restore();
    }

    void fillGap(boolean paramBoolean)
    {
        int i = getChildCount();
        if (paramBoolean)
        {
            int m = 0;
            if ((0x22 & this.mGroupFlags) == 34)
                m = getListPaddingTop();
            if (i > 0);
            for (int n = getChildAt(i - 1).getBottom() + this.mDividerHeight; ; n = m)
            {
                fillDown(i + this.mFirstPosition, n);
                correctTooHigh(getChildCount());
                return;
            }
        }
        int j = 0;
        if ((0x22 & this.mGroupFlags) == 34)
            j = getListPaddingBottom();
        if (i > 0);
        for (int k = getChildAt(0).getTop() - this.mDividerHeight; ; k = getHeight() - j)
        {
            fillUp(-1 + this.mFirstPosition, k);
            correctTooLow(getChildCount());
            break;
        }
    }

    int findMotionRow(int paramInt)
    {
        int i = getChildCount();
        int m;
        int j;
        if (i > 0)
            if (!this.mStackFromBottom)
            {
                m = 0;
                if (m >= i)
                    break label94;
                if (paramInt <= getChildAt(m).getBottom())
                    j = m + this.mFirstPosition;
            }
        while (true)
        {
            return j;
            m++;
            break;
            for (int k = i - 1; ; k--)
            {
                if (k < 0)
                    break label94;
                if (paramInt >= getChildAt(k).getTop())
                {
                    j = k + this.mFirstPosition;
                    break;
                }
            }
            label94: j = -1;
        }
    }

    View findViewByPredicateInHeadersOrFooters(ArrayList<FixedViewInfo> paramArrayList, Predicate<View> paramPredicate, View paramView)
    {
        int j;
        View localView1;
        if (paramArrayList != null)
        {
            int i = paramArrayList.size();
            j = 0;
            if (j < i)
            {
                View localView2 = ((FixedViewInfo)paramArrayList.get(j)).view;
                if ((localView2 != paramView) && (!localView2.isRootNamespace()))
                {
                    localView1 = localView2.findViewByPredicate(paramPredicate);
                    if (localView1 == null);
                }
            }
        }
        while (true)
        {
            return localView1;
            j++;
            break;
            localView1 = null;
        }
    }

    protected View findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView)
    {
        View localView1 = super.findViewByPredicateTraversal(paramPredicate, paramView);
        View localView2;
        if (localView1 == null)
        {
            View localView3 = findViewByPredicateInHeadersOrFooters(this.mHeaderViewInfos, paramPredicate, paramView);
            if (localView3 != null)
                localView2 = localView3;
        }
        while (true)
        {
            return localView2;
            localView1 = findViewByPredicateInHeadersOrFooters(this.mFooterViewInfos, paramPredicate, paramView);
            if (localView1 != null)
                localView2 = localView1;
            else
                localView2 = localView1;
        }
    }

    View findViewInHeadersOrFooters(ArrayList<FixedViewInfo> paramArrayList, int paramInt)
    {
        int j;
        View localView1;
        if (paramArrayList != null)
        {
            int i = paramArrayList.size();
            j = 0;
            if (j < i)
            {
                View localView2 = ((FixedViewInfo)paramArrayList.get(j)).view;
                if (!localView2.isRootNamespace())
                {
                    localView1 = localView2.findViewById(paramInt);
                    if (localView1 == null);
                }
            }
        }
        while (true)
        {
            return localView1;
            j++;
            break;
            localView1 = null;
        }
    }

    protected View findViewTraversal(int paramInt)
    {
        View localView1 = super.findViewTraversal(paramInt);
        View localView2;
        if (localView1 == null)
        {
            View localView3 = findViewInHeadersOrFooters(this.mHeaderViewInfos, paramInt);
            if (localView3 != null)
                localView2 = localView3;
        }
        while (true)
        {
            return localView2;
            localView1 = findViewInHeadersOrFooters(this.mFooterViewInfos, paramInt);
            if (localView1 != null)
                localView2 = localView1;
            else
                localView2 = localView1;
        }
    }

    View findViewWithTagInHeadersOrFooters(ArrayList<FixedViewInfo> paramArrayList, Object paramObject)
    {
        int j;
        View localView1;
        if (paramArrayList != null)
        {
            int i = paramArrayList.size();
            j = 0;
            if (j < i)
            {
                View localView2 = ((FixedViewInfo)paramArrayList.get(j)).view;
                if (!localView2.isRootNamespace())
                {
                    localView1 = localView2.findViewWithTag(paramObject);
                    if (localView1 == null);
                }
            }
        }
        while (true)
        {
            return localView1;
            j++;
            break;
            localView1 = null;
        }
    }

    protected View findViewWithTagTraversal(Object paramObject)
    {
        View localView1 = super.findViewWithTagTraversal(paramObject);
        View localView2;
        if (localView1 == null)
        {
            View localView3 = findViewWithTagInHeadersOrFooters(this.mHeaderViewInfos, paramObject);
            if (localView3 != null)
                localView2 = localView3;
        }
        while (true)
        {
            return localView2;
            localView1 = findViewWithTagInHeadersOrFooters(this.mFooterViewInfos, paramObject);
            if (localView1 != null)
                localView2 = localView1;
            else
                localView2 = localView1;
        }
    }

    boolean fullScroll(int paramInt)
    {
        boolean bool = false;
        if (paramInt == 33)
            if (this.mSelectedPosition != 0)
            {
                int j = lookForSelectablePosition(0, true);
                if (j >= 0)
                {
                    this.mLayoutMode = 1;
                    setSelectionInt(j);
                    invokeOnItemScrollListener();
                }
            }
        for (bool = true; ; bool = true)
        {
            do
            {
                if ((bool) && (!awakenScrollBars()))
                {
                    awakenScrollBars();
                    invalidate();
                }
                return bool;
            }
            while ((paramInt != 130) || (this.mSelectedPosition >= -1 + this.mItemCount));
            int i = lookForSelectablePosition(-1 + this.mItemCount, true);
            if (i >= 0)
            {
                this.mLayoutMode = 3;
                setSelectionInt(i);
                invokeOnItemScrollListener();
            }
        }
    }

    public ListAdapter getAdapter()
    {
        return this.mAdapter;
    }

    @Deprecated
    public long[] getCheckItemIds()
    {
        Object localObject;
        if ((this.mAdapter != null) && (this.mAdapter.hasStableIds()))
        {
            localObject = getCheckedItemIds();
            return localObject;
        }
        int i;
        int j;
        int k;
        label73: int m;
        if ((this.mChoiceMode != 0) && (this.mCheckStates != null) && (this.mAdapter != null))
        {
            SparseBooleanArray localSparseBooleanArray = this.mCheckStates;
            i = localSparseBooleanArray.size();
            localObject = new long[i];
            ListAdapter localListAdapter = this.mAdapter;
            j = 0;
            k = 0;
            if (j < i)
            {
                if (!localSparseBooleanArray.valueAt(j))
                    break label156;
                m = k + 1;
                localObject[k] = localListAdapter.getItemId(localSparseBooleanArray.keyAt(j));
            }
        }
        while (true)
        {
            j++;
            k = m;
            break label73;
            if (k == i)
                break;
            long[] arrayOfLong = new long[k];
            System.arraycopy(localObject, 0, arrayOfLong, 0, k);
            localObject = arrayOfLong;
            break;
            localObject = new long[0];
            break;
            label156: m = k;
        }
    }

    public Drawable getDivider()
    {
        return this.mDivider;
    }

    public int getDividerHeight()
    {
        return this.mDividerHeight;
    }

    public int getFooterViewsCount()
    {
        return this.mFooterViewInfos.size();
    }

    public int getHeaderViewsCount()
    {
        return this.mHeaderViewInfos.size();
    }

    public boolean getItemsCanFocus()
    {
        return this.mItemsCanFocus;
    }

    public int getMaxScrollAmount()
    {
        return (int)(0.33F * (this.mBottom - this.mTop));
    }

    public Drawable getOverscrollFooter()
    {
        return this.mOverScrollFooter;
    }

    public Drawable getOverscrollHeader()
    {
        return this.mOverScrollHeader;
    }

    public boolean isOpaque()
    {
        boolean bool;
        if (((this.mCachingActive) && (this.mIsCacheColorOpaque) && (this.mDividerIsOpaque) && (hasOpaqueScrollbars())) || (super.isOpaque()))
        {
            bool = true;
            if (bool)
                if (this.mListPadding == null)
                    break label83;
        }
        label83: for (int i = this.mListPadding.top; ; i = this.mPaddingTop)
        {
            View localView1 = getChildAt(0);
            if ((localView1 != null) && (localView1.getTop() <= i))
                break label91;
            bool = false;
            return bool;
            bool = false;
            break;
        }
        label91: int j = getHeight();
        if (this.mListPadding != null);
        for (int k = this.mListPadding.bottom; ; k = this.mPaddingBottom)
        {
            int m = j - k;
            View localView2 = getChildAt(-1 + getChildCount());
            if ((localView2 != null) && (localView2.getBottom() >= m))
                break;
            bool = false;
            break;
        }
    }

    protected void layoutChildren()
    {
        boolean bool1 = this.mBlockLayoutRequests;
        if (!bool1)
            this.mBlockLayoutRequests = true;
        label46: int i;
        int j;
        int k;
        int m;
        View localView1;
        View localView2;
        View localView3;
        View localView4;
        boolean bool2;
        try
        {
            super.layoutChildren();
            invalidate();
            if (this.mAdapter == null)
            {
                resetList();
                invokeOnItemScrollListener();
                return;
            }
            i = this.mListPadding.top;
            j = this.mBottom - this.mTop - this.mListPadding.bottom;
            k = getChildCount();
            m = 0;
            localView1 = null;
            localView2 = null;
            localView3 = null;
            localView4 = null;
            int i6;
            switch (this.mLayoutMode)
            {
            default:
                i6 = this.mSelectedPosition - this.mFirstPosition;
                if ((i6 >= 0) && (i6 < k))
                    localView1 = getChildAt(i6);
                localView2 = getChildAt(0);
                if (this.mNextSelectedPosition >= 0)
                    m = this.mNextSelectedPosition - this.mSelectedPosition;
                break;
            case 1:
            case 3:
            case 4:
            case 5:
            case 2:
            }
            int n;
            for (localView3 = getChildAt(i6 + m); ; localView3 = getChildAt(n))
                do
                {
                    bool2 = this.mDataChanged;
                    if (bool2)
                        handleDataChanged();
                    if (this.mItemCount != 0)
                        break label270;
                    resetList();
                    invokeOnItemScrollListener();
                    if (bool1)
                        break label46;
                    break;
                    n = this.mNextSelectedPosition - this.mFirstPosition;
                }
                while ((n < 0) || (n >= k));
            label270: if (this.mItemCount != this.mAdapter.getCount())
                throw new IllegalStateException("The content of the adapter has changed but ListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in ListView(" + getId() + ", " + getClass() + ") with Adapter(" + this.mAdapter.getClass() + ")]");
        }
        finally
        {
            if (!bool1)
                this.mBlockLayoutRequests = false;
        }
        setSelectedPositionInt(this.mNextSelectedPosition);
        int i1 = this.mFirstPosition;
        AbsListView.RecycleBin localRecycleBin = this.mRecycler;
        Object localObject2 = null;
        if (bool2)
            for (int i5 = 0; i5 < k; i5++)
                localRecycleBin.addScrapView(getChildAt(i5), i1 + i5);
        localRecycleBin.fillActiveViews(k, i1);
        View localView5 = getFocusedChild();
        if (localView5 != null)
        {
            if ((!bool2) || (isDirectChildHeaderOrFooter(localView5)))
            {
                localObject2 = localView5;
                localView4 = findFocus();
                if (localView4 != null)
                    localView4.onStartTemporaryDetach();
            }
            requestFocus();
        }
        detachAllViewsFromParent();
        localRecycleBin.removeSkippedScrap();
        View localView6;
        switch (this.mLayoutMode)
        {
        default:
            if (k == 0)
                if (!this.mStackFromBottom)
                {
                    setSelectedPositionInt(lookForSelectablePosition(0, true));
                    localView6 = fillFromTop(i);
                    label565: localRecycleBin.scrapActiveViews();
                    if (localView6 == null)
                        break label1046;
                    if ((!this.mItemsCanFocus) || (!hasFocus()) || (localView6.hasFocus()))
                        break label1035;
                    if ((localView6 == localObject2) && (localView4 != null) && (localView4.requestFocus()))
                        break label1127;
                    if (!localView6.requestFocus())
                        break label1133;
                }
            break;
        case 2:
        case 5:
        case 3:
        case 1:
        case 4:
        case 6:
        }
        while (true)
        {
            if (i2 == 0)
            {
                View localView8 = getFocusedChild();
                if (localView8 != null)
                    localView8.clearFocus();
                positionSelector(-1, localView6);
                label657: this.mSelectedTop = localView6.getTop();
            }
            label1035: label1046: label1125: 
            while (true)
            {
                if ((localView4 != null) && (localView4.getWindowToken() != null))
                    localView4.onFinishTemporaryDetach();
                this.mLayoutMode = 0;
                this.mDataChanged = false;
                if (this.mPositionScrollAfterLayout != null)
                {
                    post(this.mPositionScrollAfterLayout);
                    this.mPositionScrollAfterLayout = null;
                }
                this.mNeedSync = false;
                setNextSelectedPositionInt(this.mSelectedPosition);
                updateScrollIndicators();
                if (this.mItemCount > 0)
                    checkSelectionChanged();
                invokeOnItemScrollListener();
                if (bool1)
                    break label46;
                break;
                if (localView3 != null)
                {
                    localView6 = fillFromSelection(localView3.getTop(), i, j);
                    break label565;
                }
                localView6 = fillFromMiddle(i, j);
                break label565;
                localView6 = fillSpecific(this.mSyncPosition, this.mSpecificTop);
                break label565;
                localView6 = fillUp(-1 + this.mItemCount, j);
                adjustViewsUpOrDown();
                break label565;
                this.mFirstPosition = 0;
                localView6 = fillFromTop(i);
                adjustViewsUpOrDown();
                break label565;
                localView6 = fillSpecific(reconcileSelectedPosition(), this.mSpecificTop);
                break label565;
                localView6 = moveSelection(localView1, localView3, m, i, j);
                break label565;
                setSelectedPositionInt(lookForSelectablePosition(-1 + this.mItemCount, false));
                localView6 = fillUp(-1 + this.mItemCount, j);
                break label565;
                if ((this.mSelectedPosition >= 0) && (this.mSelectedPosition < this.mItemCount))
                {
                    int i4 = this.mSelectedPosition;
                    if (localView1 == null);
                    while (true)
                    {
                        localView6 = fillSpecific(i4, i);
                        break;
                        i = localView1.getTop();
                    }
                }
                if (this.mFirstPosition < this.mItemCount)
                {
                    int i3 = this.mFirstPosition;
                    if (localView2 == null);
                    while (true)
                    {
                        localView6 = fillSpecific(i3, i);
                        break;
                        i = localView2.getTop();
                    }
                }
                localView6 = fillSpecific(0, i);
                break label565;
                localView6.setSelected(false);
                this.mSelectorRect.setEmpty();
                break label657;
                positionSelector(-1, localView6);
                break label657;
                if ((this.mTouchMode > 0) && (this.mTouchMode < 3))
                {
                    View localView7 = getChildAt(this.mMotionPosition - this.mFirstPosition);
                    if (localView7 != null)
                        positionSelector(this.mMotionPosition, localView7);
                }
                while (true)
                {
                    if ((!hasFocus()) || (localView4 == null))
                        break label1125;
                    localView4.requestFocus();
                    break;
                    this.mSelectedTop = 0;
                    this.mSelectorRect.setEmpty();
                }
            }
            label1127: int i2 = 1;
            continue;
            label1133: i2 = 0;
        }
    }

    int lookForSelectablePosition(int paramInt, boolean paramBoolean)
    {
        int i = -1;
        ListAdapter localListAdapter = this.mAdapter;
        if ((localListAdapter == null) || (isInTouchMode()));
        while (true)
        {
            return i;
            int j = localListAdapter.getCount();
            if (!this.mAreAllItemsSelectable)
            {
                if (paramBoolean)
                    for (k = Math.max(0, paramInt); (k < j) && (!localListAdapter.isEnabled(k)); k++);
                for (int k = Math.min(paramInt, j - 1); (k >= 0) && (!localListAdapter.isEnabled(k)); k--);
                if ((k >= 0) && (k < j))
                    i = k;
            }
            else if ((paramInt >= 0) && (paramInt < j))
            {
                i = paramInt;
            }
        }
    }

    final int measureHeightOfChildren(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        ListAdapter localListAdapter = this.mAdapter;
        int k;
        if (localListAdapter == null)
            k = this.mListPadding.top + this.mListPadding.bottom;
        while (true)
        {
            return k;
            int i = this.mListPadding.top + this.mListPadding.bottom;
            int j;
            label68: AbsListView.RecycleBin localRecycleBin;
            boolean bool;
            boolean[] arrayOfBoolean;
            if ((this.mDividerHeight > 0) && (this.mDivider != null))
            {
                j = this.mDividerHeight;
                k = 0;
                if (paramInt3 == -1)
                    paramInt3 = -1 + localListAdapter.getCount();
                localRecycleBin = this.mRecycler;
                bool = recycleOnMeasure();
                arrayOfBoolean = this.mIsScrap;
            }
            for (int m = paramInt2; ; m++)
            {
                if (m > paramInt3)
                    break label255;
                View localView = obtainView(m, arrayOfBoolean);
                measureScrapChild(localView, m, paramInt1);
                if (m > 0)
                    i += j;
                if ((bool) && (localRecycleBin.shouldRecycleViewType(((AbsListView.LayoutParams)localView.getLayoutParams()).viewType)))
                    localRecycleBin.addScrapView(localView, -1);
                i += localView.getMeasuredHeight();
                if (i >= paramInt4)
                {
                    if ((paramInt5 >= 0) && (m > paramInt5) && (k > 0) && (i != paramInt4))
                        break;
                    k = paramInt4;
                    break;
                    j = 0;
                    break label68;
                }
                if ((paramInt5 >= 0) && (m >= paramInt5))
                    k = i;
            }
            label255: k = i;
        }
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        int i = getChildCount();
        if (i > 0)
        {
            for (int j = 0; j < i; j++)
                addHeaderView(getChildAt(j));
            removeAllViews();
        }
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
        ListAdapter localListAdapter = this.mAdapter;
        int i = -1;
        int j = 0;
        if ((localListAdapter != null) && (paramBoolean) && (paramRect != null))
        {
            paramRect.offset(this.mScrollX, this.mScrollY);
            if (localListAdapter.getCount() < getChildCount() + this.mFirstPosition)
            {
                this.mLayoutMode = 0;
                layoutChildren();
            }
            Rect localRect = this.mTempRect;
            int k = 2147483647;
            int m = getChildCount();
            int n = this.mFirstPosition;
            int i1 = 0;
            if (i1 < m)
            {
                if (!localListAdapter.isEnabled(n + i1));
                while (true)
                {
                    i1++;
                    break;
                    View localView = getChildAt(i1);
                    localView.getDrawingRect(localRect);
                    offsetDescendantRectToMyCoords(localView, localRect);
                    int i2 = getDistance(paramRect, localRect, paramInt);
                    if (i2 < k)
                    {
                        k = i2;
                        i = i1;
                        j = localView.getTop();
                    }
                }
            }
        }
        if (i >= 0)
            setSelectionFromTop(i + this.mFirstPosition, j);
        while (true)
        {
            return;
            requestLayout();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ListView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ListView.class.getName());
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        return commonKey(paramInt, 1, paramKeyEvent);
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        return commonKey(paramInt1, paramInt2, paramKeyEvent);
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        return commonKey(paramInt, 1, paramKeyEvent);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt1);
        int m = View.MeasureSpec.getSize(paramInt2);
        int n = 0;
        int i1 = 0;
        int i2 = 0;
        int i3;
        if (this.mAdapter == null)
        {
            i3 = 0;
            this.mItemCount = i3;
            if ((this.mItemCount > 0) && ((i == 0) || (j == 0)))
            {
                View localView = obtainView(0, this.mIsScrap);
                measureScrapChild(localView, 0, paramInt1);
                n = localView.getMeasuredWidth();
                i1 = localView.getMeasuredHeight();
                i2 = combineMeasuredStates(0, localView.getMeasuredState());
                if ((recycleOnMeasure()) && (this.mRecycler.shouldRecycleViewType(((AbsListView.LayoutParams)localView.getLayoutParams()).viewType)))
                    this.mRecycler.addScrapView(localView, -1);
            }
            if (i != 0)
                break label264;
        }
        label264: for (int i4 = n + (this.mListPadding.left + this.mListPadding.right) + getVerticalScrollbarWidth(); ; i4 = k | 0xFF000000 & i2)
        {
            if (j == 0)
                m = i1 + (this.mListPadding.top + this.mListPadding.bottom) + 2 * getVerticalFadingEdgeLength();
            if (j == -2147483648)
                m = measureHeightOfChildren(paramInt1, 0, -1, m, -1);
            setMeasuredDimension(i4, m);
            this.mWidthMeasureSpec = paramInt1;
            return;
            i3 = this.mAdapter.getCount();
            break;
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (getChildCount() > 0)
        {
            View localView = getFocusedChild();
            if (localView != null)
            {
                int i = this.mFirstPosition + indexOfChild(localView);
                int j = Math.max(0, localView.getBottom() - (paramInt2 - this.mPaddingTop));
                int k = localView.getTop() - j;
                if (this.mFocusSelector == null)
                    this.mFocusSelector = new FocusSelector(null);
                post(this.mFocusSelector.setup(i, k));
            }
        }
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    boolean pageScroll(int paramInt)
    {
        int i = 1;
        int j = -1;
        boolean bool = false;
        if (paramInt == 33)
        {
            j = Math.max(0, -1 + (this.mSelectedPosition - getChildCount()));
            if (j < 0)
                break label169;
            int k = lookForSelectablePosition(j, bool);
            if (k < 0)
                break label169;
            this.mLayoutMode = 4;
            this.mSpecificTop = (this.mPaddingTop + getVerticalFadingEdgeLength());
            if ((bool) && (k > this.mItemCount - getChildCount()))
                this.mLayoutMode = 3;
            if ((!bool) && (k < getChildCount()))
                this.mLayoutMode = i;
            setSelectionInt(k);
            invokeOnItemScrollListener();
            if (!awakenScrollBars())
                invalidate();
        }
        while (true)
        {
            return i;
            if (paramInt != 130)
                break;
            j = Math.min(-1 + this.mItemCount, -1 + (this.mSelectedPosition + getChildCount()));
            bool = true;
            break;
            label169: i = 0;
        }
    }

    @ViewDebug.ExportedProperty(category="list")
    protected boolean recycleOnMeasure()
    {
        return true;
    }

    public boolean removeFooterView(View paramView)
    {
        boolean bool;
        if (this.mFooterViewInfos.size() > 0)
        {
            bool = false;
            if ((this.mAdapter != null) && (((HeaderViewListAdapter)this.mAdapter).removeFooter(paramView)))
            {
                if (this.mDataSetObserver != null)
                    this.mDataSetObserver.onChanged();
                bool = true;
            }
            removeFixedViewInfo(paramView, this.mFooterViewInfos);
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean removeHeaderView(View paramView)
    {
        boolean bool;
        if (this.mHeaderViewInfos.size() > 0)
        {
            bool = false;
            if ((this.mAdapter != null) && (((HeaderViewListAdapter)this.mAdapter).removeHeader(paramView)))
            {
                if (this.mDataSetObserver != null)
                    this.mDataSetObserver.onChanged();
                bool = true;
            }
            removeFixedViewInfo(paramView, this.mHeaderViewInfos);
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
    {
        int i = paramRect.top;
        paramRect.offset(paramView.getLeft(), paramView.getTop());
        paramRect.offset(-paramView.getScrollX(), -paramView.getScrollY());
        int j = getHeight();
        int k = getScrollY();
        int m = k + j;
        int n = getVerticalFadingEdgeLength();
        if ((showingTopFadingEdge()) && ((this.mSelectedPosition > 0) || (i > n)))
            k += n;
        int i1 = getChildAt(-1 + getChildCount()).getBottom();
        if ((showingBottomFadingEdge()) && ((this.mSelectedPosition < -1 + this.mItemCount) || (paramRect.bottom < i1 - n)))
            m -= n;
        int i2 = 0;
        int i4;
        if ((paramRect.bottom > m) && (paramRect.top > k))
            if (paramRect.height() > j)
            {
                i4 = 0 + (paramRect.top - k);
                i2 = Math.min(i4, i1 - m);
                label194: if (i2 == 0)
                    break label323;
            }
        label323: for (boolean bool = true; ; bool = false)
        {
            if (bool)
            {
                scrollListItemsBy(-i2);
                positionSelector(-1, paramView);
                this.mSelectedTop = paramView.getTop();
                invalidate();
            }
            return bool;
            i4 = 0 + (paramRect.bottom - m);
            break;
            if ((paramRect.top >= k) || (paramRect.bottom >= m))
                break label194;
            if (paramRect.height() > j);
            for (int i3 = 0 - (m - paramRect.bottom); ; i3 = 0 - (k - paramRect.top))
            {
                i2 = Math.max(i3, getChildAt(0).getTop() - k);
                break;
            }
        }
    }

    void resetList()
    {
        clearRecycledState(this.mHeaderViewInfos);
        clearRecycledState(this.mFooterViewInfos);
        super.resetList();
        this.mLayoutMode = 0;
    }

    public void setAdapter(ListAdapter paramListAdapter)
    {
        if ((this.mAdapter != null) && (this.mDataSetObserver != null))
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
        resetList();
        this.mRecycler.clear();
        int i;
        if ((this.mHeaderViewInfos.size() > 0) || (this.mFooterViewInfos.size() > 0))
        {
            this.mAdapter = new HeaderViewListAdapter(this.mHeaderViewInfos, this.mFooterViewInfos, paramListAdapter);
            this.mOldSelectedPosition = -1;
            this.mOldSelectedRowId = -9223372036854775808L;
            super.setAdapter(paramListAdapter);
            if (this.mAdapter == null)
                break label246;
            this.mAreAllItemsSelectable = this.mAdapter.areAllItemsEnabled();
            this.mOldItemCount = this.mItemCount;
            this.mItemCount = this.mAdapter.getCount();
            checkFocus();
            this.mDataSetObserver = new AbsListView.AdapterDataSetObserver(this);
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            this.mRecycler.setViewTypeCount(this.mAdapter.getViewTypeCount());
            if (!this.mStackFromBottom)
                break label236;
            i = lookForSelectablePosition(-1 + this.mItemCount, false);
            label202: setSelectedPositionInt(i);
            setNextSelectedPositionInt(i);
            if (this.mItemCount == 0)
                checkSelectionChanged();
        }
        while (true)
        {
            requestLayout();
            return;
            this.mAdapter = paramListAdapter;
            break;
            label236: i = lookForSelectablePosition(0, true);
            break label202;
            label246: this.mAreAllItemsSelectable = true;
            checkFocus();
            checkSelectionChanged();
        }
    }

    public void setCacheColorHint(int paramInt)
    {
        if (paramInt >>> 24 == 255);
        for (boolean bool = true; ; bool = false)
        {
            this.mIsCacheColorOpaque = bool;
            if (bool)
            {
                if (this.mDividerPaint == null)
                    this.mDividerPaint = new Paint();
                this.mDividerPaint.setColor(paramInt);
            }
            super.setCacheColorHint(paramInt);
            return;
        }
    }

    public void setDivider(Drawable paramDrawable)
    {
        boolean bool = false;
        if (paramDrawable != null);
        for (this.mDividerHeight = paramDrawable.getIntrinsicHeight(); ; this.mDividerHeight = 0)
        {
            this.mDivider = paramDrawable;
            if ((paramDrawable == null) || (paramDrawable.getOpacity() == -1))
                bool = true;
            this.mDividerIsOpaque = bool;
            requestLayout();
            invalidate();
            return;
        }
    }

    public void setDividerHeight(int paramInt)
    {
        this.mDividerHeight = paramInt;
        requestLayout();
        invalidate();
    }

    public void setFooterDividersEnabled(boolean paramBoolean)
    {
        this.mFooterDividersEnabled = paramBoolean;
        invalidate();
    }

    public void setHeaderDividersEnabled(boolean paramBoolean)
    {
        this.mHeaderDividersEnabled = paramBoolean;
        invalidate();
    }

    public void setItemsCanFocus(boolean paramBoolean)
    {
        this.mItemsCanFocus = paramBoolean;
        if (!paramBoolean)
            setDescendantFocusability(393216);
    }

    public void setOverscrollFooter(Drawable paramDrawable)
    {
        this.mOverScrollFooter = paramDrawable;
        invalidate();
    }

    public void setOverscrollHeader(Drawable paramDrawable)
    {
        this.mOverScrollHeader = paramDrawable;
        if (this.mScrollY < 0)
            invalidate();
    }

    @RemotableViewMethod
    public void setRemoteViewsAdapter(Intent paramIntent)
    {
        super.setRemoteViewsAdapter(paramIntent);
    }

    public void setSelection(int paramInt)
    {
        setSelectionFromTop(paramInt, 0);
    }

    public void setSelectionAfterHeaderView()
    {
        int i = this.mHeaderViewInfos.size();
        if (i > 0)
            this.mNextSelectedPosition = 0;
        while (true)
        {
            return;
            if (this.mAdapter != null)
            {
                setSelection(i);
            }
            else
            {
                this.mNextSelectedPosition = i;
                this.mLayoutMode = 2;
            }
        }
    }

    public void setSelectionFromTop(int paramInt1, int paramInt2)
    {
        if (this.mAdapter == null);
        label106: 
        while (true)
        {
            return;
            if (!isInTouchMode())
            {
                paramInt1 = lookForSelectablePosition(paramInt1, true);
                if (paramInt1 >= 0)
                    setNextSelectedPositionInt(paramInt1);
            }
            while (true)
            {
                if (paramInt1 < 0)
                    break label106;
                this.mLayoutMode = 4;
                this.mSpecificTop = (paramInt2 + this.mListPadding.top);
                if (this.mNeedSync)
                {
                    this.mSyncPosition = paramInt1;
                    this.mSyncRowId = this.mAdapter.getItemId(paramInt1);
                }
                if (this.mPositionScroller != null)
                    this.mPositionScroller.stop();
                requestLayout();
                break;
                this.mResurrectToPosition = paramInt1;
            }
        }
    }

    void setSelectionInt(int paramInt)
    {
        setNextSelectedPositionInt(paramInt);
        int i = 0;
        int j = this.mSelectedPosition;
        if (j >= 0)
        {
            if (paramInt != j - 1)
                break label53;
            i = 1;
        }
        while (true)
        {
            if (this.mPositionScroller != null)
                this.mPositionScroller.stop();
            layoutChildren();
            if (i != 0)
                awakenScrollBars();
            return;
            label53: if (paramInt == j + 1)
                i = 1;
        }
    }

    @RemotableViewMethod
    public void smoothScrollByOffset(int paramInt)
    {
        super.smoothScrollByOffset(paramInt);
    }

    @RemotableViewMethod
    public void smoothScrollToPosition(int paramInt)
    {
        super.smoothScrollToPosition(paramInt);
    }

    private static class ArrowScrollFocusResult
    {
        private int mAmountToScroll;
        private int mSelectedPosition;

        public int getAmountToScroll()
        {
            return this.mAmountToScroll;
        }

        public int getSelectedPosition()
        {
            return this.mSelectedPosition;
        }

        void populate(int paramInt1, int paramInt2)
        {
            this.mSelectedPosition = paramInt1;
            this.mAmountToScroll = paramInt2;
        }
    }

    private class FocusSelector
        implements Runnable
    {
        private int mPosition;
        private int mPositionTop;

        private FocusSelector()
        {
        }

        public void run()
        {
            ListView.this.setSelectionFromTop(this.mPosition, this.mPositionTop);
        }

        public FocusSelector setup(int paramInt1, int paramInt2)
        {
            this.mPosition = paramInt1;
            this.mPositionTop = paramInt2;
            return this;
        }
    }

    public class FixedViewInfo
    {
        public Object data;
        public boolean isSelectable;
        public View view;

        public FixedViewInfo()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ListView
 * JD-Core Version:        0.6.2
 */