package android.widget;

import android.graphics.Canvas;
import android.graphics.Canvas.EdgeType;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class ScrollBarDrawable extends Drawable
{
    private boolean mAlwaysDrawHorizontalTrack;
    private boolean mAlwaysDrawVerticalTrack;
    private boolean mChanged;
    private int mExtent;
    private Drawable mHorizontalThumb;
    private Drawable mHorizontalTrack;
    private int mOffset;
    private int mRange;
    private boolean mRangeChanged;
    private final Rect mTempBounds = new Rect();
    private boolean mVertical;
    private Drawable mVerticalThumb;
    private Drawable mVerticalTrack;

    public void draw(Canvas paramCanvas)
    {
        boolean bool1 = this.mVertical;
        int i = this.mExtent;
        int j = this.mRange;
        boolean bool2 = true;
        int k = 1;
        Rect localRect;
        if ((i <= 0) || (j <= i))
        {
            if (bool1)
            {
                bool2 = this.mAlwaysDrawVerticalTrack;
                k = 0;
            }
        }
        else
        {
            localRect = getBounds();
            if (!paramCanvas.quickReject(localRect.left, localRect.top, localRect.right, localRect.bottom, Canvas.EdgeType.AA))
                break label95;
        }
        label95: 
        do
        {
            return;
            bool2 = this.mAlwaysDrawHorizontalTrack;
            break;
            if (bool2)
                drawTrack(paramCanvas, localRect, bool1);
        }
        while (k == 0);
        int m;
        if (bool1)
        {
            m = localRect.height();
            label124: if (!bool1)
                break label232;
        }
        label232: for (int n = localRect.width(); ; n = localRect.height())
        {
            int i1 = Math.round(m * i / j);
            int i2 = Math.round((m - i1) * this.mOffset / (j - i));
            int i3 = n * 2;
            if (i1 < i3)
                i1 = i3;
            if (i2 + i1 > m)
                i2 = m - i1;
            drawThumb(paramCanvas, localRect, i2, i1, bool1);
            break;
            m = localRect.width();
            break label124;
        }
    }

    protected void drawThumb(Canvas paramCanvas, Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        Rect localRect = this.mTempBounds;
        int i;
        if ((this.mRangeChanged) || (this.mChanged))
        {
            i = 1;
            if (i != 0)
            {
                if (!paramBoolean)
                    break label97;
                localRect.set(paramRect.left, paramInt1 + paramRect.top, paramRect.right, paramInt2 + (paramInt1 + paramRect.top));
            }
            label61: if (!paramBoolean)
                break label128;
            Drawable localDrawable2 = this.mVerticalThumb;
            if (i != 0)
                localDrawable2.setBounds(localRect);
            localDrawable2.draw(paramCanvas);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label97: localRect.set(paramInt1 + paramRect.left, paramRect.top, paramInt2 + (paramInt1 + paramRect.left), paramRect.bottom);
            break label61;
            label128: Drawable localDrawable1 = this.mHorizontalThumb;
            if (i != 0)
                localDrawable1.setBounds(localRect);
            localDrawable1.draw(paramCanvas);
        }
    }

    protected void drawTrack(Canvas paramCanvas, Rect paramRect, boolean paramBoolean)
    {
        if (paramBoolean);
        for (Drawable localDrawable = this.mVerticalTrack; ; localDrawable = this.mHorizontalTrack)
        {
            if (localDrawable != null)
            {
                if (this.mChanged)
                    localDrawable.setBounds(paramRect);
                localDrawable.draw(paramCanvas);
            }
            return;
        }
    }

    public boolean getAlwaysDrawHorizontalTrack()
    {
        return this.mAlwaysDrawHorizontalTrack;
    }

    public boolean getAlwaysDrawVerticalTrack()
    {
        return this.mAlwaysDrawVerticalTrack;
    }

    public int getOpacity()
    {
        return -3;
    }

    public int getSize(boolean paramBoolean)
    {
        int i;
        if (paramBoolean)
        {
            if (this.mVerticalTrack != null);
            for (Drawable localDrawable2 = this.mVerticalTrack; ; localDrawable2 = this.mVerticalThumb)
            {
                i = localDrawable2.getIntrinsicWidth();
                return i;
            }
        }
        if (this.mHorizontalTrack != null);
        for (Drawable localDrawable1 = this.mHorizontalTrack; ; localDrawable1 = this.mHorizontalThumb)
        {
            i = localDrawable1.getIntrinsicHeight();
            break;
        }
    }

    protected void onBoundsChange(Rect paramRect)
    {
        super.onBoundsChange(paramRect);
        this.mChanged = true;
    }

    public void setAlpha(int paramInt)
    {
        if (this.mVerticalTrack != null)
            this.mVerticalTrack.setAlpha(paramInt);
        this.mVerticalThumb.setAlpha(paramInt);
        if (this.mHorizontalTrack != null)
            this.mHorizontalTrack.setAlpha(paramInt);
        this.mHorizontalThumb.setAlpha(paramInt);
    }

    public void setAlwaysDrawHorizontalTrack(boolean paramBoolean)
    {
        this.mAlwaysDrawHorizontalTrack = paramBoolean;
    }

    public void setAlwaysDrawVerticalTrack(boolean paramBoolean)
    {
        this.mAlwaysDrawVerticalTrack = paramBoolean;
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        if (this.mVerticalTrack != null)
            this.mVerticalTrack.setColorFilter(paramColorFilter);
        this.mVerticalThumb.setColorFilter(paramColorFilter);
        if (this.mHorizontalTrack != null)
            this.mHorizontalTrack.setColorFilter(paramColorFilter);
        this.mHorizontalThumb.setColorFilter(paramColorFilter);
    }

    public void setHorizontalThumbDrawable(Drawable paramDrawable)
    {
        if (paramDrawable != null)
            this.mHorizontalThumb = paramDrawable;
    }

    public void setHorizontalTrackDrawable(Drawable paramDrawable)
    {
        this.mHorizontalTrack = paramDrawable;
    }

    public void setParameters(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        if (this.mVertical != paramBoolean)
            this.mChanged = true;
        if ((this.mRange != paramInt1) || (this.mOffset != paramInt2) || (this.mExtent != paramInt3))
            this.mRangeChanged = true;
        this.mRange = paramInt1;
        this.mOffset = paramInt2;
        this.mExtent = paramInt3;
        this.mVertical = paramBoolean;
    }

    public void setVerticalThumbDrawable(Drawable paramDrawable)
    {
        if (paramDrawable != null)
            this.mVerticalThumb = paramDrawable;
    }

    public void setVerticalTrackDrawable(Drawable paramDrawable)
    {
        this.mVerticalTrack = paramDrawable;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder().append("ScrollBarDrawable: range=").append(this.mRange).append(" offset=").append(this.mOffset).append(" extent=").append(this.mExtent);
        if (this.mVertical);
        for (String str = " V"; ; str = " H")
            return str;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ScrollBarDrawable
 * JD-Core Version:        0.6.2
 */