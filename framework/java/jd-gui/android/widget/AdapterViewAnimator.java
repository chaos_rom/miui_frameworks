package android.widget;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public abstract class AdapterViewAnimator extends AdapterView<Adapter>
    implements RemoteViewsAdapter.RemoteAdapterConnectionCallback, Advanceable
{
    private static final int DEFAULT_ANIMATION_DURATION = 200;
    private static final String TAG = "RemoteViewAnimator";
    static final int TOUCH_MODE_DOWN_IN_CURRENT_VIEW = 1;
    static final int TOUCH_MODE_HANDLED = 2;
    static final int TOUCH_MODE_NONE;
    int mActiveOffset = 0;
    Adapter mAdapter;
    boolean mAnimateFirstTime = true;
    int mCurrentWindowEnd = -1;
    int mCurrentWindowStart = 0;
    int mCurrentWindowStartUnbounded = 0;
    AdapterView<Adapter>.AdapterDataSetObserver mDataSetObserver;
    boolean mDeferNotifyDataSetChanged = false;
    boolean mFirstTime = true;
    ObjectAnimator mInAnimation;
    boolean mLoopViews = true;
    int mMaxNumActiveViews = 1;
    ObjectAnimator mOutAnimation;
    private Runnable mPendingCheckForTap;
    ArrayList<Integer> mPreviousViews;
    int mReferenceChildHeight = -1;
    int mReferenceChildWidth = -1;
    RemoteViewsAdapter mRemoteViewsAdapter;
    private int mRestoreWhichChild = -1;
    private int mTouchMode = 0;
    HashMap<Integer, ViewAndMetaData> mViewsMap = new HashMap();
    int mWhichChild = 0;

    public AdapterViewAnimator(Context paramContext)
    {
        this(paramContext, null);
    }

    public AdapterViewAnimator(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public AdapterViewAnimator(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AdapterViewAnimator, paramInt, 0);
        int i = localTypedArray.getResourceId(0, 0);
        if (i > 0)
        {
            setInAnimation(paramContext, i);
            int j = localTypedArray.getResourceId(1, 0);
            if (j <= 0)
                break label189;
            setOutAnimation(paramContext, j);
        }
        while (true)
        {
            setAnimateFirstView(localTypedArray.getBoolean(2, true));
            this.mLoopViews = localTypedArray.getBoolean(3, false);
            localTypedArray.recycle();
            initViewAnimator();
            return;
            setInAnimation(getDefaultInAnimation());
            break;
            label189: setOutAnimation(getDefaultOutAnimation());
        }
    }

    private void addChild(View paramView)
    {
        addViewInLayout(paramView, -1, createOrReuseLayoutParams(paramView));
        if ((this.mReferenceChildWidth == -1) || (this.mReferenceChildHeight == -1))
        {
            int i = View.MeasureSpec.makeMeasureSpec(0, 0);
            paramView.measure(i, i);
            this.mReferenceChildWidth = paramView.getMeasuredWidth();
            this.mReferenceChildHeight = paramView.getMeasuredHeight();
        }
    }

    private ViewAndMetaData getMetaDataForChild(View paramView)
    {
        Iterator localIterator = this.mViewsMap.values().iterator();
        ViewAndMetaData localViewAndMetaData;
        do
        {
            if (!localIterator.hasNext())
                break;
            localViewAndMetaData = (ViewAndMetaData)localIterator.next();
        }
        while (localViewAndMetaData.view != paramView);
        while (true)
        {
            return localViewAndMetaData;
            localViewAndMetaData = null;
        }
    }

    private void initViewAnimator()
    {
        this.mPreviousViews = new ArrayList();
    }

    private void measureChildren()
    {
        int i = getChildCount();
        int j = getMeasuredWidth() - this.mPaddingLeft - this.mPaddingRight;
        int k = getMeasuredHeight() - this.mPaddingTop - this.mPaddingBottom;
        for (int m = 0; m < i; m++)
            getChildAt(m).measure(View.MeasureSpec.makeMeasureSpec(j, 1073741824), View.MeasureSpec.makeMeasureSpec(k, 1073741824));
    }

    private void setDisplayedChild(int paramInt, boolean paramBoolean)
    {
        int k;
        if (this.mAdapter != null)
        {
            this.mWhichChild = paramInt;
            if (paramInt < getWindowSize())
                break label79;
            if (!this.mLoopViews)
                break label67;
            k = 0;
            this.mWhichChild = k;
            label36: if (getFocusedChild() == null)
                break label111;
        }
        label67: label79: label111: for (int j = 1; ; j = 0)
        {
            showOnly(this.mWhichChild, paramBoolean);
            if (j != 0)
                requestFocus(2);
            return;
            k = -1 + getWindowSize();
            break;
            if (paramInt >= 0)
                break label36;
            if (this.mLoopViews);
            for (int i = -1 + getWindowSize(); ; i = 0)
            {
                this.mWhichChild = i;
                break;
            }
        }
    }

    public void advance()
    {
        showNext();
    }

    void applyTransformForChildAtIndex(View paramView, int paramInt)
    {
    }

    void cancelHandleClick()
    {
        View localView = getCurrentView();
        if (localView != null)
            hideTapFeedback(localView);
        this.mTouchMode = 0;
    }

    void checkForAndHandleDataChanged()
    {
        if (this.mDataChanged)
            post(new Runnable()
            {
                public void run()
                {
                    AdapterViewAnimator.this.handleDataChanged();
                    if (AdapterViewAnimator.this.mWhichChild >= AdapterViewAnimator.this.getWindowSize())
                    {
                        AdapterViewAnimator.this.mWhichChild = 0;
                        AdapterViewAnimator.this.showOnly(AdapterViewAnimator.this.mWhichChild, false);
                    }
                    while (true)
                    {
                        AdapterViewAnimator.this.refreshChildren();
                        AdapterViewAnimator.this.requestLayout();
                        return;
                        if (AdapterViewAnimator.this.mOldItemCount != AdapterViewAnimator.this.getCount())
                            AdapterViewAnimator.this.showOnly(AdapterViewAnimator.this.mWhichChild, false);
                    }
                }
            });
        this.mDataChanged = false;
    }

    void configureViewAnimator(int paramInt1, int paramInt2)
    {
        if (paramInt2 > paramInt1 - 1);
        this.mMaxNumActiveViews = paramInt1;
        this.mActiveOffset = paramInt2;
        this.mPreviousViews.clear();
        this.mViewsMap.clear();
        removeAllViewsInLayout();
        this.mCurrentWindowStart = 0;
        this.mCurrentWindowEnd = -1;
    }

    ViewGroup.LayoutParams createOrReuseLayoutParams(View paramView)
    {
        ViewGroup.LayoutParams localLayoutParams1 = paramView.getLayoutParams();
        if ((localLayoutParams1 instanceof ViewGroup.LayoutParams));
        for (ViewGroup.LayoutParams localLayoutParams2 = localLayoutParams1; ; localLayoutParams2 = new ViewGroup.LayoutParams(0, 0))
            return localLayoutParams2;
    }

    public void deferNotifyDataSetChanged()
    {
        this.mDeferNotifyDataSetChanged = true;
    }

    public void fyiWillBeAdvancedByHostKThx()
    {
    }

    public Adapter getAdapter()
    {
        return this.mAdapter;
    }

    public int getBaseline()
    {
        if (getCurrentView() != null);
        for (int i = getCurrentView().getBaseline(); ; i = super.getBaseline())
            return i;
    }

    public View getCurrentView()
    {
        return getViewAtRelativeIndex(this.mActiveOffset);
    }

    ObjectAnimator getDefaultInAnimation()
    {
        float[] arrayOfFloat = new float[2];
        arrayOfFloat[0] = 0.0F;
        arrayOfFloat[1] = 1.0F;
        ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(null, "alpha", arrayOfFloat);
        localObjectAnimator.setDuration(200L);
        return localObjectAnimator;
    }

    ObjectAnimator getDefaultOutAnimation()
    {
        float[] arrayOfFloat = new float[2];
        arrayOfFloat[0] = 1.0F;
        arrayOfFloat[1] = 0.0F;
        ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(null, "alpha", arrayOfFloat);
        localObjectAnimator.setDuration(200L);
        return localObjectAnimator;
    }

    public int getDisplayedChild()
    {
        return this.mWhichChild;
    }

    FrameLayout getFrameForChild()
    {
        return new FrameLayout(this.mContext);
    }

    public ObjectAnimator getInAnimation()
    {
        return this.mInAnimation;
    }

    int getNumActiveViews()
    {
        if (this.mAdapter != null);
        for (int i = Math.min(1 + getCount(), this.mMaxNumActiveViews); ; i = this.mMaxNumActiveViews)
            return i;
    }

    public ObjectAnimator getOutAnimation()
    {
        return this.mOutAnimation;
    }

    public View getSelectedView()
    {
        return getViewAtRelativeIndex(this.mActiveOffset);
    }

    View getViewAtRelativeIndex(int paramInt)
    {
        int i;
        if ((paramInt >= 0) && (paramInt <= -1 + getNumActiveViews()) && (this.mAdapter != null))
        {
            i = modulo(paramInt + this.mCurrentWindowStartUnbounded, getWindowSize());
            if (this.mViewsMap.get(Integer.valueOf(i)) == null);
        }
        for (View localView = ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i))).view; ; localView = null)
            return localView;
    }

    int getWindowSize()
    {
        int i;
        if (this.mAdapter != null)
        {
            i = getCount();
            if ((i <= getNumActiveViews()) && (this.mLoopViews))
                i *= this.mMaxNumActiveViews;
        }
        while (true)
        {
            return i;
            i = 0;
        }
    }

    void hideTapFeedback(View paramView)
    {
        paramView.setPressed(false);
    }

    int modulo(int paramInt1, int paramInt2)
    {
        if (paramInt2 > 0);
        for (int i = (paramInt2 + paramInt1 % paramInt2) % paramInt2; ; i = 0)
            return i;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(AdapterViewAnimator.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(AdapterViewAnimator.class.getName());
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        checkForAndHandleDataChanged();
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            int k = this.mPaddingLeft + localView.getMeasuredWidth();
            int m = this.mPaddingTop + localView.getMeasuredHeight();
            localView.layout(this.mPaddingLeft, this.mPaddingTop, k, m);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getSize(paramInt1);
        int j = View.MeasureSpec.getSize(paramInt2);
        int k = View.MeasureSpec.getMode(paramInt1);
        int m = View.MeasureSpec.getMode(paramInt2);
        int n;
        if ((this.mReferenceChildWidth != -1) && (this.mReferenceChildHeight != -1))
        {
            n = 1;
            if (m != 0)
                break label119;
            if (n == 0)
                break label113;
            j = this.mReferenceChildHeight + this.mPaddingTop + this.mPaddingBottom;
            label70: if (k != 0)
                break label178;
            if (n == 0)
                break label173;
            i = this.mReferenceChildWidth + this.mPaddingLeft + this.mPaddingRight;
        }
        while (true)
        {
            setMeasuredDimension(i, j);
            measureChildren();
            return;
            n = 0;
            break;
            label113: j = 0;
            break label70;
            label119: if ((m != -2147483648) || (n == 0))
                break label70;
            int i1 = this.mReferenceChildHeight + this.mPaddingTop + this.mPaddingBottom;
            if (i1 > j)
            {
                j |= 16777216;
                break label70;
            }
            j = i1;
            break label70;
            label173: i = 0;
            continue;
            label178: if ((m == -2147483648) && (n != 0))
            {
                int i2 = this.mReferenceChildWidth + this.mPaddingLeft + this.mPaddingRight;
                if (i2 > i)
                    i |= 16777216;
                else
                    i = i2;
            }
        }
    }

    public boolean onRemoteAdapterConnected()
    {
        boolean bool = false;
        if (this.mRemoteViewsAdapter != this.mAdapter)
        {
            setAdapter(this.mRemoteViewsAdapter);
            if (this.mDeferNotifyDataSetChanged)
            {
                this.mRemoteViewsAdapter.notifyDataSetChanged();
                this.mDeferNotifyDataSetChanged = false;
            }
            if (this.mRestoreWhichChild > -1)
            {
                setDisplayedChild(this.mRestoreWhichChild, false);
                this.mRestoreWhichChild = -1;
            }
        }
        while (true)
        {
            return bool;
            if (this.mRemoteViewsAdapter != null)
            {
                this.mRemoteViewsAdapter.superNotifyDataSetChanged();
                bool = true;
            }
        }
    }

    public void onRemoteAdapterDisconnected()
    {
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        this.mWhichChild = localSavedState.whichChild;
        if ((this.mRemoteViewsAdapter != null) && (this.mAdapter == null))
            this.mRestoreWhichChild = this.mWhichChild;
        while (true)
        {
            return;
            setDisplayedChild(this.mWhichChild, false);
        }
    }

    public Parcelable onSaveInstanceState()
    {
        Parcelable localParcelable = super.onSaveInstanceState();
        if (this.mRemoteViewsAdapter != null)
            this.mRemoteViewsAdapter.saveRemoteViewsCache();
        return new SavedState(localParcelable, this.mWhichChild);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getAction();
        boolean bool = false;
        switch (i)
        {
        case 2:
        case 4:
        case 5:
        case 6:
        default:
        case 0:
        case 1:
        case 3:
        }
        while (true)
        {
            return bool;
            View localView3 = getCurrentView();
            if ((localView3 != null) && (isTransformedTouchPointInView(paramMotionEvent.getX(), paramMotionEvent.getY(), localView3, null)))
            {
                if (this.mPendingCheckForTap == null)
                    this.mPendingCheckForTap = new CheckForTap();
                this.mTouchMode = 1;
                postDelayed(this.mPendingCheckForTap, ViewConfiguration.getTapTimeout());
                continue;
                if (this.mTouchMode == 1)
                {
                    final View localView2 = getCurrentView();
                    final ViewAndMetaData localViewAndMetaData = getMetaDataForChild(localView2);
                    if ((localView2 != null) && (isTransformedTouchPointInView(paramMotionEvent.getX(), paramMotionEvent.getY(), localView2, null)))
                    {
                        Handler localHandler = getHandler();
                        if (localHandler != null)
                            localHandler.removeCallbacks(this.mPendingCheckForTap);
                        showTapFeedback(localView2);
                        postDelayed(new Runnable()
                        {
                            public void run()
                            {
                                AdapterViewAnimator.this.hideTapFeedback(localView2);
                                AdapterViewAnimator.this.post(new Runnable()
                                {
                                    public void run()
                                    {
                                        if (AdapterViewAnimator.1.this.val$viewData != null)
                                            AdapterViewAnimator.this.performItemClick(AdapterViewAnimator.1.this.val$v, AdapterViewAnimator.1.this.val$viewData.adapterPosition, AdapterViewAnimator.1.this.val$viewData.itemId);
                                        while (true)
                                        {
                                            return;
                                            AdapterViewAnimator.this.performItemClick(AdapterViewAnimator.1.this.val$v, 0, 0L);
                                        }
                                    }
                                });
                            }
                        }
                        , ViewConfiguration.getPressedStateDuration());
                        bool = true;
                    }
                }
                this.mTouchMode = 0;
                continue;
                View localView1 = getCurrentView();
                if (localView1 != null)
                    hideTapFeedback(localView1);
                this.mTouchMode = 0;
            }
        }
    }

    void refreshChildren()
    {
        if (this.mAdapter == null);
        while (true)
        {
            return;
            for (int i = this.mCurrentWindowStart; i <= this.mCurrentWindowEnd; i++)
            {
                int j = modulo(i, getWindowSize());
                int k = getCount();
                View localView = this.mAdapter.getView(modulo(i, k), null, this);
                if (localView.getImportantForAccessibility() == 0)
                    localView.setImportantForAccessibility(1);
                if (this.mViewsMap.containsKey(Integer.valueOf(j)))
                {
                    FrameLayout localFrameLayout = (FrameLayout)((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(j))).view;
                    if (localView != null)
                    {
                        localFrameLayout.removeAllViewsInLayout();
                        localFrameLayout.addView(localView);
                    }
                }
            }
        }
    }

    public void setAdapter(Adapter paramAdapter)
    {
        if ((this.mAdapter != null) && (this.mDataSetObserver != null))
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
        this.mAdapter = paramAdapter;
        checkFocus();
        if (this.mAdapter != null)
        {
            this.mDataSetObserver = new AdapterView.AdapterDataSetObserver(this);
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            this.mItemCount = this.mAdapter.getCount();
        }
        setFocusable(true);
        this.mWhichChild = 0;
        showOnly(this.mWhichChild, false);
    }

    public void setAnimateFirstView(boolean paramBoolean)
    {
        this.mAnimateFirstTime = paramBoolean;
    }

    @RemotableViewMethod
    public void setDisplayedChild(int paramInt)
    {
        setDisplayedChild(paramInt, true);
    }

    public void setInAnimation(ObjectAnimator paramObjectAnimator)
    {
        this.mInAnimation = paramObjectAnimator;
    }

    public void setInAnimation(Context paramContext, int paramInt)
    {
        setInAnimation((ObjectAnimator)AnimatorInflater.loadAnimator(paramContext, paramInt));
    }

    public void setOutAnimation(ObjectAnimator paramObjectAnimator)
    {
        this.mOutAnimation = paramObjectAnimator;
    }

    public void setOutAnimation(Context paramContext, int paramInt)
    {
        setOutAnimation((ObjectAnimator)AnimatorInflater.loadAnimator(paramContext, paramInt));
    }

    @RemotableViewMethod
    public void setRemoteViewsAdapter(Intent paramIntent)
    {
        if ((this.mRemoteViewsAdapter != null) && (new Intent.FilterComparison(paramIntent).equals(new Intent.FilterComparison(this.mRemoteViewsAdapter.getRemoteViewsServiceIntent()))));
        while (true)
        {
            return;
            this.mDeferNotifyDataSetChanged = false;
            this.mRemoteViewsAdapter = new RemoteViewsAdapter(getContext(), paramIntent, this);
            if (this.mRemoteViewsAdapter.isDataReady())
                setAdapter(this.mRemoteViewsAdapter);
        }
    }

    public void setSelection(int paramInt)
    {
        setDisplayedChild(paramInt);
    }

    public void showNext()
    {
        setDisplayedChild(1 + this.mWhichChild);
    }

    void showOnly(int paramInt, boolean paramBoolean)
    {
        if (this.mAdapter == null);
        while (true)
        {
            return;
            int i = getCount();
            if (i != 0)
            {
                for (int j = 0; j < this.mPreviousViews.size(); j++)
                {
                    View localView4 = ((ViewAndMetaData)this.mViewsMap.get(this.mPreviousViews.get(j))).view;
                    this.mViewsMap.remove(this.mPreviousViews.get(j));
                    localView4.clearAnimation();
                    if ((localView4 instanceof ViewGroup))
                        ((ViewGroup)localView4).removeAllViewsInLayout();
                    applyTransformForChildAtIndex(localView4, -1);
                    removeViewInLayout(localView4);
                }
                this.mPreviousViews.clear();
                int k = paramInt - this.mActiveOffset;
                int m = -1 + (k + getNumActiveViews());
                int n = Math.max(0, k);
                int i1 = Math.min(i - 1, m);
                if (this.mLoopViews)
                {
                    n = k;
                    i1 = m;
                }
                int i2 = getWindowSize();
                int i3 = modulo(n, i2);
                int i4 = getWindowSize();
                int i5 = modulo(i1, i4);
                int i6 = 0;
                if (i3 > i5)
                    i6 = 1;
                Iterator localIterator = this.mViewsMap.keySet().iterator();
                label378: 
                while (localIterator.hasNext())
                {
                    Integer localInteger = (Integer)localIterator.next();
                    int i17 = 0;
                    if ((i6 == 0) && ((localInteger.intValue() < i3) || (localInteger.intValue() > i5)));
                    for (i17 = 1; ; i17 = 1)
                        do
                        {
                            if (i17 == 0)
                                break label378;
                            View localView3 = ((ViewAndMetaData)this.mViewsMap.get(localInteger)).view;
                            int i18 = ((ViewAndMetaData)this.mViewsMap.get(localInteger)).relativeIndex;
                            this.mPreviousViews.add(localInteger);
                            transformViewForTransition(i18, -1, localView3, paramBoolean);
                            break;
                        }
                        while ((i6 == 0) || (localInteger.intValue() <= i5) || (localInteger.intValue() >= i3));
                }
                int i7 = this.mCurrentWindowStart;
                if (n == i7)
                {
                    int i16 = this.mCurrentWindowEnd;
                    if ((i1 == i16) && (k == this.mCurrentWindowStartUnbounded));
                }
                else
                {
                    int i8 = n;
                    if (i8 <= i1)
                    {
                        int i11 = modulo(i8, getWindowSize());
                        int i12;
                        label473: int i13;
                        int i14;
                        if (this.mViewsMap.containsKey(Integer.valueOf(i11)))
                        {
                            i12 = ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i11))).relativeIndex;
                            i13 = i8 - k;
                            if ((!this.mViewsMap.containsKey(Integer.valueOf(i11))) || (this.mPreviousViews.contains(Integer.valueOf(i11))))
                                break label611;
                            i14 = 1;
                            label513: if (i14 == 0)
                                break label617;
                            View localView2 = ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i11))).view;
                            ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i11))).relativeIndex = i13;
                            applyTransformForChildAtIndex(localView2, i13);
                            transformViewForTransition(i12, i13, localView2, paramBoolean);
                        }
                        while (true)
                        {
                            ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i11))).view.bringToFront();
                            i8++;
                            break;
                            i12 = -1;
                            break label473;
                            label611: i14 = 0;
                            break label513;
                            label617: int i15 = modulo(i8, i);
                            View localView1 = this.mAdapter.getView(i15, null, this);
                            long l = this.mAdapter.getItemId(i15);
                            FrameLayout localFrameLayout = getFrameForChild();
                            if (localView1 != null)
                                localFrameLayout.addView(localView1);
                            this.mViewsMap.put(Integer.valueOf(i11), new ViewAndMetaData(localFrameLayout, i13, i15, l));
                            addChild(localFrameLayout);
                            applyTransformForChildAtIndex(localFrameLayout, i13);
                            transformViewForTransition(-1, i13, localFrameLayout, paramBoolean);
                        }
                    }
                    this.mCurrentWindowStart = n;
                    this.mCurrentWindowEnd = i1;
                    this.mCurrentWindowStartUnbounded = k;
                    if (this.mRemoteViewsAdapter != null)
                    {
                        int i9 = modulo(this.mCurrentWindowStart, i);
                        int i10 = modulo(this.mCurrentWindowEnd, i);
                        this.mRemoteViewsAdapter.setVisibleRangeHint(i9, i10);
                    }
                }
                requestLayout();
                invalidate();
            }
        }
    }

    public void showPrevious()
    {
        setDisplayedChild(-1 + this.mWhichChild);
    }

    void showTapFeedback(View paramView)
    {
        paramView.setPressed(true);
    }

    void transformViewForTransition(int paramInt1, int paramInt2, View paramView, boolean paramBoolean)
    {
        if (paramInt1 == -1)
        {
            this.mInAnimation.setTarget(paramView);
            this.mInAnimation.start();
        }
        while (true)
        {
            return;
            if (paramInt2 == -1)
            {
                this.mOutAnimation.setTarget(paramView);
                this.mOutAnimation.start();
            }
        }
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public AdapterViewAnimator.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new AdapterViewAnimator.SavedState(paramAnonymousParcel, null);
            }

            public AdapterViewAnimator.SavedState[] newArray(int paramAnonymousInt)
            {
                return new AdapterViewAnimator.SavedState[paramAnonymousInt];
            }
        };
        int whichChild;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.whichChild = paramParcel.readInt();
        }

        SavedState(Parcelable paramParcelable, int paramInt)
        {
            super();
            this.whichChild = paramInt;
        }

        public String toString()
        {
            return "AdapterViewAnimator.SavedState{ whichChild = " + this.whichChild + " }";
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.whichChild);
        }
    }

    final class CheckForTap
        implements Runnable
    {
        CheckForTap()
        {
        }

        public void run()
        {
            if (AdapterViewAnimator.this.mTouchMode == 1)
            {
                View localView = AdapterViewAnimator.this.getCurrentView();
                AdapterViewAnimator.this.showTapFeedback(localView);
            }
        }
    }

    class ViewAndMetaData
    {
        int adapterPosition;
        long itemId;
        int relativeIndex;
        View view;

        ViewAndMetaData(View paramInt1, int paramInt2, int paramLong, long arg5)
        {
            this.view = paramInt1;
            this.relativeIndex = paramInt2;
            this.adapterPosition = paramLong;
            Object localObject;
            this.itemId = localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AdapterViewAnimator
 * JD-Core Version:        0.6.2
 */