package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.ExtractEditText;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.text.BoringLayout;
import android.text.BoringLayout.Metrics;
import android.text.DynamicLayout;
import android.text.Editable;
import android.text.Editable.Factory;
import android.text.GetChars;
import android.text.GraphicsOperations;
import android.text.InputFilter;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.ParcelableSpan;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.text.TextWatcher;
import android.text.method.AllCapsTransformationMethod;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.DateKeyListener;
import android.text.method.DateTimeKeyListener;
import android.text.method.DialerKeyListener;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.text.method.LinkMovementMethod;
import android.text.method.MetaKeyKeyListener;
import android.text.method.MovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.text.method.TextKeyListener;
import android.text.method.TextKeyListener.Capitalize;
import android.text.method.TimeKeyListener;
import android.text.method.TransformationMethod;
import android.text.method.TransformationMethod2;
import android.text.method.WordIterator;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.text.style.ParagraphStyle;
import android.text.style.SpellCheckSpan;
import android.text.style.SuggestionSpan;
import android.text.style.URLSpan;
import android.text.style.UpdateAppearance;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.util.Log;
import android.util.TypedValue;
import android.view.AccessibilityIterators.TextSegmentIterator;
import android.view.ActionMode.Callback;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.MotionEvent;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewDebug.CapturedViewProperty;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewRootImpl;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.textservice.SpellCheckerSubtype;
import android.view.textservice.TextServicesManager;
import com.android.internal.R.styleable;
import com.android.internal.util.FastMath;
import com.android.internal.widget.EditableInputConnection;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import miui.text.util.Linkify;
import org.xmlpull.v1.XmlPullParserException;

@RemoteViews.RemoteView
public class TextView extends View
    implements ViewTreeObserver.OnPreDrawListener
{
    private static final int ANIMATED_SCROLL_GAP = 250;
    private static final int CHANGE_WATCHER_PRIORITY = 100;
    static final boolean DEBUG_EXTRACT = false;
    private static final int DECIMAL = 4;
    private static final Spanned EMPTY_SPANNED;
    private static final int EMS = 1;
    static final int ID_COPY = 16908321;
    static final int ID_CUT = 16908320;
    static final int ID_PASTE = 16908322;
    static final int ID_SELECT_ALL = 16908319;
    static long LAST_CUT_OR_COPY_TIME = 0L;
    private static final int LINES = 1;
    static final String LOG_TAG = "TextView";
    private static final int MARQUEE_FADE_NORMAL = 0;
    private static final int MARQUEE_FADE_SWITCH_SHOW_ELLIPSIS = 1;
    private static final int MARQUEE_FADE_SWITCH_SHOW_FADE = 2;
    private static final int MONOSPACE = 3;
    private static final int[] MULTILINE_STATE_SET;
    private static final InputFilter[] NO_FILTERS;
    private static final int PIXELS = 2;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    private static final int SIGNED = 2;
    private static final RectF TEMP_RECTF = new RectF();
    private static final BoringLayout.Metrics UNKNOWN_BORING = new BoringLayout.Metrics();
    private static final int VERY_WIDE = 1048576;
    private boolean mAllowTransformationLengthChange;
    private int mAutoLinkMask;
    private BoringLayout.Metrics mBoring;
    private BufferType mBufferType;
    private ChangeWatcher mChangeWatcher;
    private CharWrapper mCharWrapper;
    private int mCurHintTextColor;
    private int mCurTextColor;
    int mCursorDrawableRes;
    private int mDesiredHeightAtMeasure;
    private boolean mDispatchTemporaryDetach;
    Drawables mDrawables;
    private Editable.Factory mEditableFactory;
    private Editor mEditor;
    private TextUtils.TruncateAt mEllipsize;
    private InputFilter[] mFilters;
    private boolean mFreezesText;
    private int mGravity;
    int mHighlightColor;
    private final Paint mHighlightPaint;
    private Path mHighlightPath;
    private boolean mHighlightPathBogus;
    private CharSequence mHint;
    private BoringLayout.Metrics mHintBoring;
    private Layout mHintLayout;
    private ColorStateList mHintTextColor;
    private boolean mHorizontallyScrolling;
    private boolean mIncludePad;
    private long mLastScroll;
    private Layout mLayout;
    private Layout.Alignment mLayoutAlignment;
    private ColorStateList mLinkTextColor;
    private boolean mLinksClickable;
    private ArrayList<TextWatcher> mListeners;
    private Marquee mMarquee;
    private int mMarqueeFadeMode;
    private int mMarqueeRepeatLimit;
    private int mMaxMode;
    private int mMaxWidth;
    private int mMaxWidthMode;
    private int mMaximum;
    private int mMinMode;
    private int mMinWidth;
    private int mMinWidthMode;
    private int mMinimum;
    private MovementMethod mMovement;
    private int mOldMaxMode;
    private int mOldMaximum;
    private boolean mPreDrawRegistered;
    private boolean mResolvedDrawables;
    private boolean mRestartMarquee;
    private BoringLayout mSavedHintLayout;
    private BoringLayout mSavedLayout;
    private Layout mSavedMarqueeModeLayout;
    private Scroller mScroller;
    private float mShadowDx;
    private float mShadowDy;
    private float mShadowRadius;
    private boolean mSingleLine;
    private float mSpacingAdd;
    private float mSpacingMult;
    private Spannable.Factory mSpannableFactory;
    private Rect mTempRect;
    private boolean mTemporaryDetach;

    @ViewDebug.ExportedProperty(category="text")
    private CharSequence mText;
    private ColorStateList mTextColor;
    private TextDirectionHeuristic mTextDir;
    int mTextEditSuggestionItemLayout;
    private final TextPaint mTextPaint;
    int mTextSelectHandleLeftRes;
    int mTextSelectHandleRes;
    int mTextSelectHandleRightRes;
    private TransformationMethod mTransformation;
    private CharSequence mTransformed;
    private boolean mUserSetTextScaleX;

    static
    {
        NO_FILTERS = new InputFilter[0];
        EMPTY_SPANNED = new SpannedString("");
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16843597;
        MULTILINE_STATE_SET = arrayOfInt;
        Paint localPaint = new Paint();
        localPaint.setAntiAlias(true);
        localPaint.measureText("H");
    }

    public TextView(Context paramContext)
    {
        this(paramContext, null);
    }

    public TextView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842884);
    }

    // ERROR //
    public TextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: aload_2
        //     3: iload_3
        //     4: invokespecial 252	android/view/View:<init>	(Landroid/content/Context;Landroid/util/AttributeSet;I)V
        //     7: aload_0
        //     8: invokestatic 258	android/text/Editable$Factory:getInstance	()Landroid/text/Editable$Factory;
        //     11: putfield 260	android/widget/TextView:mEditableFactory	Landroid/text/Editable$Factory;
        //     14: aload_0
        //     15: invokestatic 265	android/text/Spannable$Factory:getInstance	()Landroid/text/Spannable$Factory;
        //     18: putfield 267	android/widget/TextView:mSpannableFactory	Landroid/text/Spannable$Factory;
        //     21: aload_0
        //     22: iconst_3
        //     23: putfield 269	android/widget/TextView:mMarqueeRepeatLimit	I
        //     26: aload_0
        //     27: iconst_0
        //     28: putfield 271	android/widget/TextView:mMarqueeFadeMode	I
        //     31: aload_0
        //     32: getstatic 274	android/widget/TextView$BufferType:NORMAL	Landroid/widget/TextView$BufferType;
        //     35: putfield 276	android/widget/TextView:mBufferType	Landroid/widget/TextView$BufferType;
        //     38: aload_0
        //     39: ldc_w 277
        //     42: putfield 279	android/widget/TextView:mGravity	I
        //     45: aload_0
        //     46: iconst_1
        //     47: putfield 281	android/widget/TextView:mLinksClickable	Z
        //     50: aload_0
        //     51: fconst_1
        //     52: putfield 283	android/widget/TextView:mSpacingMult	F
        //     55: aload_0
        //     56: fconst_0
        //     57: putfield 285	android/widget/TextView:mSpacingAdd	F
        //     60: aload_0
        //     61: ldc_w 286
        //     64: putfield 288	android/widget/TextView:mMaximum	I
        //     67: aload_0
        //     68: iconst_1
        //     69: putfield 290	android/widget/TextView:mMaxMode	I
        //     72: aload_0
        //     73: iconst_0
        //     74: putfield 292	android/widget/TextView:mMinimum	I
        //     77: aload_0
        //     78: iconst_1
        //     79: putfield 294	android/widget/TextView:mMinMode	I
        //     82: aload_0
        //     83: aload_0
        //     84: getfield 288	android/widget/TextView:mMaximum	I
        //     87: putfield 296	android/widget/TextView:mOldMaximum	I
        //     90: aload_0
        //     91: aload_0
        //     92: getfield 290	android/widget/TextView:mMaxMode	I
        //     95: putfield 298	android/widget/TextView:mOldMaxMode	I
        //     98: aload_0
        //     99: ldc_w 286
        //     102: putfield 300	android/widget/TextView:mMaxWidth	I
        //     105: aload_0
        //     106: iconst_2
        //     107: putfield 302	android/widget/TextView:mMaxWidthMode	I
        //     110: aload_0
        //     111: iconst_0
        //     112: putfield 304	android/widget/TextView:mMinWidth	I
        //     115: aload_0
        //     116: iconst_2
        //     117: putfield 306	android/widget/TextView:mMinWidthMode	I
        //     120: aload_0
        //     121: bipush 255
        //     123: putfield 308	android/widget/TextView:mDesiredHeightAtMeasure	I
        //     126: aload_0
        //     127: iconst_1
        //     128: putfield 310	android/widget/TextView:mIncludePad	Z
        //     131: aload_0
        //     132: getstatic 201	android/widget/TextView:NO_FILTERS	[Landroid/text/InputFilter;
        //     135: putfield 312	android/widget/TextView:mFilters	[Landroid/text/InputFilter;
        //     138: aload_0
        //     139: ldc_w 313
        //     142: putfield 315	android/widget/TextView:mHighlightColor	I
        //     145: aload_0
        //     146: iconst_1
        //     147: putfield 317	android/widget/TextView:mHighlightPathBogus	Z
        //     150: aload_0
        //     151: ldc 205
        //     153: putfield 319	android/widget/TextView:mText	Ljava/lang/CharSequence;
        //     156: aload_0
        //     157: invokevirtual 323	android/widget/TextView:getResources	()Landroid/content/res/Resources;
        //     160: astore 4
        //     162: aload 4
        //     164: invokevirtual 329	android/content/res/Resources:getCompatibilityInfo	()Landroid/content/res/CompatibilityInfo;
        //     167: astore 5
        //     169: aload_0
        //     170: new 331	android/text/TextPaint
        //     173: dup
        //     174: iconst_1
        //     175: invokespecial 334	android/text/TextPaint:<init>	(I)V
        //     178: putfield 336	android/widget/TextView:mTextPaint	Landroid/text/TextPaint;
        //     181: aload_0
        //     182: getfield 336	android/widget/TextView:mTextPaint	Landroid/text/TextPaint;
        //     185: aload 4
        //     187: invokevirtual 340	android/content/res/Resources:getDisplayMetrics	()Landroid/util/DisplayMetrics;
        //     190: getfield 345	android/util/DisplayMetrics:density	F
        //     193: putfield 346	android/text/TextPaint:density	F
        //     196: aload_0
        //     197: getfield 336	android/widget/TextView:mTextPaint	Landroid/text/TextPaint;
        //     200: aload 5
        //     202: getfield 351	android/content/res/CompatibilityInfo:applicationScale	F
        //     205: invokevirtual 355	android/text/TextPaint:setCompatibilityScaling	(F)V
        //     208: aload_0
        //     209: new 215	android/graphics/Paint
        //     212: dup
        //     213: iconst_1
        //     214: invokespecial 356	android/graphics/Paint:<init>	(I)V
        //     217: putfield 358	android/widget/TextView:mHighlightPaint	Landroid/graphics/Paint;
        //     220: aload_0
        //     221: getfield 358	android/widget/TextView:mHighlightPaint	Landroid/graphics/Paint;
        //     224: aload 5
        //     226: getfield 351	android/content/res/CompatibilityInfo:applicationScale	F
        //     229: invokevirtual 359	android/graphics/Paint:setCompatibilityScaling	(F)V
        //     232: aload_0
        //     233: aload_0
        //     234: invokevirtual 363	android/widget/TextView:getDefaultMovementMethod	()Landroid/text/method/MovementMethod;
        //     237: putfield 365	android/widget/TextView:mMovement	Landroid/text/method/MovementMethod;
        //     240: aload_0
        //     241: aconst_null
        //     242: putfield 367	android/widget/TextView:mTransformation	Landroid/text/method/TransformationMethod;
        //     245: iconst_0
        //     246: istore 6
        //     248: aconst_null
        //     249: astore 7
        //     251: aconst_null
        //     252: astore 8
        //     254: aconst_null
        //     255: astore 9
        //     257: bipush 15
        //     259: istore 10
        //     261: aconst_null
        //     262: astore 11
        //     264: bipush 255
        //     266: istore 12
        //     268: bipush 255
        //     270: istore 13
        //     272: iconst_0
        //     273: istore 14
        //     275: aload_1
        //     276: invokevirtual 373	android/content/Context:getTheme	()Landroid/content/res/Resources$Theme;
        //     279: astore 15
        //     281: aload 15
        //     283: aload_2
        //     284: getstatic 378	com/android/internal/R$styleable:TextViewAppearance	[I
        //     287: iload_3
        //     288: iconst_0
        //     289: invokevirtual 384	android/content/res/Resources$Theme:obtainStyledAttributes	(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
        //     292: astore 16
        //     294: aconst_null
        //     295: astore 17
        //     297: aload 16
        //     299: iconst_0
        //     300: bipush 255
        //     302: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     305: istore 18
        //     307: aload 16
        //     309: invokevirtual 393	android/content/res/TypedArray:recycle	()V
        //     312: iload 18
        //     314: bipush 255
        //     316: if_icmpeq +15 -> 331
        //     319: aload 15
        //     321: iload 18
        //     323: getstatic 396	com/android/internal/R$styleable:TextAppearance	[I
        //     326: invokevirtual 399	android/content/res/Resources$Theme:obtainStyledAttributes	(I[I)Landroid/content/res/TypedArray;
        //     329: astore 17
        //     331: aload 17
        //     333: ifnull +211 -> 544
        //     336: aload 17
        //     338: invokevirtual 403	android/content/res/TypedArray:getIndexCount	()I
        //     341: istore 87
        //     343: iconst_0
        //     344: istore 88
        //     346: iload 88
        //     348: iload 87
        //     350: if_icmpge +189 -> 539
        //     353: aload 17
        //     355: iload 88
        //     357: invokevirtual 407	android/content/res/TypedArray:getIndex	(I)I
        //     360: istore 89
        //     362: iload 89
        //     364: tableswitch	default:+52 -> 416, 0:+108->472, 1:+122->486, 2:+148->512, 3:+72->436, 4:+58->422, 5:+84->448, 6:+96->460, 7:+162->526, 8:+136->500
        //     417: pop2
        //     418: aconst_null
        //     419: goto -73 -> 346
        //     422: aload 17
        //     424: iload 89
        //     426: iload 6
        //     428: invokevirtual 410	android/content/res/TypedArray:getColor	(II)I
        //     431: istore 6
        //     433: goto -17 -> 416
        //     436: aload 17
        //     438: iload 89
        //     440: invokevirtual 414	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
        //     443: astore 7
        //     445: goto -29 -> 416
        //     448: aload 17
        //     450: iload 89
        //     452: invokevirtual 414	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
        //     455: astore 8
        //     457: goto -41 -> 416
        //     460: aload 17
        //     462: iload 89
        //     464: invokevirtual 414	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
        //     467: astore 9
        //     469: goto -53 -> 416
        //     472: aload 17
        //     474: iload 89
        //     476: iload 10
        //     478: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     481: istore 10
        //     483: goto -67 -> 416
        //     486: aload 17
        //     488: iload 89
        //     490: bipush 255
        //     492: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     495: istore 12
        //     497: goto -81 -> 416
        //     500: aload 17
        //     502: iload 89
        //     504: invokevirtual 424	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     507: astore 11
        //     509: goto -93 -> 416
        //     512: aload 17
        //     514: iload 89
        //     516: bipush 255
        //     518: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     521: istore 13
        //     523: goto -107 -> 416
        //     526: aload 17
        //     528: iload 89
        //     530: iconst_0
        //     531: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     534: istore 14
        //     536: goto -120 -> 416
        //     539: aload 17
        //     541: invokevirtual 393	android/content/res/TypedArray:recycle	()V
        //     544: aload_0
        //     545: invokevirtual 432	android/widget/TextView:getDefaultEditable	()Z
        //     548: istore 19
        //     550: aconst_null
        //     551: astore 20
        //     553: iconst_0
        //     554: istore 21
        //     556: aconst_null
        //     557: astore 22
        //     559: iconst_0
        //     560: istore 23
        //     562: iconst_0
        //     563: istore 24
        //     565: bipush 255
        //     567: istore 25
        //     569: iconst_0
        //     570: istore 26
        //     572: iconst_0
        //     573: istore 27
        //     575: aconst_null
        //     576: astore 28
        //     578: aconst_null
        //     579: astore 29
        //     581: aconst_null
        //     582: astore 30
        //     584: aconst_null
        //     585: astore 31
        //     587: aconst_null
        //     588: astore 32
        //     590: aconst_null
        //     591: astore 33
        //     593: iconst_0
        //     594: istore 34
        //     596: bipush 255
        //     598: istore 35
        //     600: iconst_0
        //     601: istore 36
        //     603: bipush 255
        //     605: istore 37
        //     607: ldc 205
        //     609: astore 38
        //     611: aconst_null
        //     612: astore 39
        //     614: iconst_0
        //     615: istore 40
        //     617: fconst_0
        //     618: fstore 41
        //     620: fconst_0
        //     621: fstore 42
        //     623: fconst_0
        //     624: fstore 43
        //     626: iconst_0
        //     627: istore 44
        //     629: iconst_0
        //     630: istore 45
        //     632: aload 15
        //     634: aload_2
        //     635: getstatic 434	com/android/internal/R$styleable:TextView	[I
        //     638: iload_3
        //     639: iconst_0
        //     640: invokevirtual 384	android/content/res/Resources$Theme:obtainStyledAttributes	(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
        //     643: astore 46
        //     645: aload 46
        //     647: invokevirtual 403	android/content/res/TypedArray:getIndexCount	()I
        //     650: istore 47
        //     652: iconst_0
        //     653: istore 48
        //     655: iload 48
        //     657: iload 47
        //     659: if_icmpge +1472 -> 2131
        //     662: aload 46
        //     664: iload 48
        //     666: invokevirtual 407	android/content/res/TypedArray:getIndex	(I)I
        //     669: istore 82
        //     671: iload 82
        //     673: tableswitch	default:+319 -> 992, 0:+994->1667, 1:+319->992, 2:+1062->1735, 3:+1076->1749, 4:+1090->1763, 5:+1026->1699, 6:+1012->1685, 7:+1038->1711, 8:+1050->1723, 9:+828->1501, 10:+755->1428, 11:+447->1120, 12:+462->1135, 13:+675->1348, 14:+579->1252, 15:+739->1412, 16:+643->1316, 17:+419->1092, 18:+783->1456, 19:+771->1444, 20:+912->1585, 21:+879->1552, 22:+563->1236, 23:+595->1268, 24:+611->1284, 25:+627->1300, 26:+659->1332, 27:+691->1364, 28:+707->1380, 29:+723->1396, 30:+795->1468, 31:+1116->1789, 32:+814->1487, 33:+433->1106, 34:+860->1533, 35:+898->1571, 36:+942->1615, 37:+955->1628, 38:+968->1641, 39:+981->1654, 40:+351->1024, 41:+365->1038, 42:+377->1050, 43:+339->1012, 44:+405->1078, 45:+391->1064, 46:+325->998, 47:+927->1600, 48:+489->1162, 49:+513->1186, 50:+477->1150, 51:+501->1174, 52:+549->1222, 53:+1130->1803, 54:+1150->1823, 55:+842->1515, 56:+1168->1841, 57:+1294->1967, 58:+1308->1981, 59:+1181->1854, 60:+1222->1895, 61:+1253->1926, 62:+1370->2043, 63:+1385->2058, 64:+1400->2073, 65:+319->992, 66:+319->992, 67:+1430->2103, 68:+319->992, 69:+319->992, 70:+1355->2028, 71:+1415->2088, 72:+1445->2118, 73:+525->1198, 74:+537->1210, 75:+1104->1777
        //     993: faload
        //     994: aconst_null
        //     995: goto -340 -> 655
        //     998: aload 46
        //     1000: iload 82
        //     1002: iload 19
        //     1004: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1007: istore 19
        //     1009: goto -17 -> 992
        //     1012: aload 46
        //     1014: iload 82
        //     1016: invokevirtual 438	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     1019: astore 20
        //     1021: goto -29 -> 992
        //     1024: aload 46
        //     1026: iload 82
        //     1028: iload 21
        //     1030: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1033: istore 21
        //     1035: goto -43 -> 992
        //     1038: aload 46
        //     1040: iload 82
        //     1042: invokevirtual 438	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     1045: astore 22
        //     1047: goto -55 -> 992
        //     1050: aload 46
        //     1052: iload 82
        //     1054: iload 23
        //     1056: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1059: istore 23
        //     1061: goto -69 -> 992
        //     1064: aload 46
        //     1066: iload 82
        //     1068: iload 24
        //     1070: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1073: istore 24
        //     1075: goto -83 -> 992
        //     1078: aload 46
        //     1080: iload 82
        //     1082: iload 25
        //     1084: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1087: istore 25
        //     1089: goto -97 -> 992
        //     1092: aload 46
        //     1094: iload 82
        //     1096: iload 26
        //     1098: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1101: istore 26
        //     1103: goto -111 -> 992
        //     1106: aload 46
        //     1108: iload 82
        //     1110: iload 27
        //     1112: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1115: istore 27
        //     1117: goto -125 -> 992
        //     1120: aload_0
        //     1121: aload 46
        //     1123: iload 82
        //     1125: iconst_0
        //     1126: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1129: putfield 440	android/widget/TextView:mAutoLinkMask	I
        //     1132: goto -140 -> 992
        //     1135: aload_0
        //     1136: aload 46
        //     1138: iload 82
        //     1140: iconst_1
        //     1141: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1144: putfield 281	android/widget/TextView:mLinksClickable	Z
        //     1147: goto -155 -> 992
        //     1150: aload 46
        //     1152: iload 82
        //     1154: invokevirtual 444	android/content/res/TypedArray:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     1157: astore 28
        //     1159: goto -167 -> 992
        //     1162: aload 46
        //     1164: iload 82
        //     1166: invokevirtual 444	android/content/res/TypedArray:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     1169: astore 29
        //     1171: goto -179 -> 992
        //     1174: aload 46
        //     1176: iload 82
        //     1178: invokevirtual 444	android/content/res/TypedArray:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     1181: astore 30
        //     1183: goto -191 -> 992
        //     1186: aload 46
        //     1188: iload 82
        //     1190: invokevirtual 444	android/content/res/TypedArray:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     1193: astore 31
        //     1195: goto -203 -> 992
        //     1198: aload 46
        //     1200: iload 82
        //     1202: invokevirtual 444	android/content/res/TypedArray:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     1205: astore 32
        //     1207: goto -215 -> 992
        //     1210: aload 46
        //     1212: iload 82
        //     1214: invokevirtual 444	android/content/res/TypedArray:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     1217: astore 33
        //     1219: goto -227 -> 992
        //     1222: aload 46
        //     1224: iload 82
        //     1226: iload 34
        //     1228: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1231: istore 34
        //     1233: goto -241 -> 992
        //     1236: aload_0
        //     1237: aload 46
        //     1239: iload 82
        //     1241: bipush 255
        //     1243: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1246: invokevirtual 447	android/widget/TextView:setMaxLines	(I)V
        //     1249: goto -257 -> 992
        //     1252: aload_0
        //     1253: aload 46
        //     1255: iload 82
        //     1257: bipush 255
        //     1259: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1262: invokevirtual 450	android/widget/TextView:setMaxHeight	(I)V
        //     1265: goto -273 -> 992
        //     1268: aload_0
        //     1269: aload 46
        //     1271: iload 82
        //     1273: bipush 255
        //     1275: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1278: invokevirtual 453	android/widget/TextView:setLines	(I)V
        //     1281: goto -289 -> 992
        //     1284: aload_0
        //     1285: aload 46
        //     1287: iload 82
        //     1289: bipush 255
        //     1291: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1294: invokevirtual 456	android/widget/TextView:setHeight	(I)V
        //     1297: goto -305 -> 992
        //     1300: aload_0
        //     1301: aload 46
        //     1303: iload 82
        //     1305: bipush 255
        //     1307: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1310: invokevirtual 459	android/widget/TextView:setMinLines	(I)V
        //     1313: goto -321 -> 992
        //     1316: aload_0
        //     1317: aload 46
        //     1319: iload 82
        //     1321: bipush 255
        //     1323: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1326: invokevirtual 462	android/widget/TextView:setMinHeight	(I)V
        //     1329: goto -337 -> 992
        //     1332: aload_0
        //     1333: aload 46
        //     1335: iload 82
        //     1337: bipush 255
        //     1339: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1342: invokevirtual 465	android/widget/TextView:setMaxEms	(I)V
        //     1345: goto -353 -> 992
        //     1348: aload_0
        //     1349: aload 46
        //     1351: iload 82
        //     1353: bipush 255
        //     1355: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1358: invokevirtual 468	android/widget/TextView:setMaxWidth	(I)V
        //     1361: goto -369 -> 992
        //     1364: aload_0
        //     1365: aload 46
        //     1367: iload 82
        //     1369: bipush 255
        //     1371: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1374: invokevirtual 471	android/widget/TextView:setEms	(I)V
        //     1377: goto -385 -> 992
        //     1380: aload_0
        //     1381: aload 46
        //     1383: iload 82
        //     1385: bipush 255
        //     1387: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1390: invokevirtual 474	android/widget/TextView:setWidth	(I)V
        //     1393: goto -401 -> 992
        //     1396: aload_0
        //     1397: aload 46
        //     1399: iload 82
        //     1401: bipush 255
        //     1403: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1406: invokevirtual 477	android/widget/TextView:setMinEms	(I)V
        //     1409: goto -417 -> 992
        //     1412: aload_0
        //     1413: aload 46
        //     1415: iload 82
        //     1417: bipush 255
        //     1419: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1422: invokevirtual 480	android/widget/TextView:setMinWidth	(I)V
        //     1425: goto -433 -> 992
        //     1428: aload_0
        //     1429: aload 46
        //     1431: iload 82
        //     1433: bipush 255
        //     1435: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1438: invokevirtual 483	android/widget/TextView:setGravity	(I)V
        //     1441: goto -449 -> 992
        //     1444: aload 46
        //     1446: iload 82
        //     1448: invokevirtual 438	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     1451: astore 39
        //     1453: goto -461 -> 992
        //     1456: aload 46
        //     1458: iload 82
        //     1460: invokevirtual 438	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     1463: astore 38
        //     1465: goto -473 -> 992
        //     1468: aload 46
        //     1470: iload 82
        //     1472: iconst_0
        //     1473: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1476: ifeq -484 -> 992
        //     1479: aload_0
        //     1480: iconst_1
        //     1481: invokevirtual 486	android/widget/TextView:setHorizontallyScrolling	(Z)V
        //     1484: goto -492 -> 992
        //     1487: aload 46
        //     1489: iload 82
        //     1491: iload 36
        //     1493: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1496: istore 36
        //     1498: goto -506 -> 992
        //     1501: aload 46
        //     1503: iload 82
        //     1505: iload 35
        //     1507: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1510: istore 35
        //     1512: goto -520 -> 992
        //     1515: aload_0
        //     1516: aload 46
        //     1518: iload 82
        //     1520: aload_0
        //     1521: getfield 269	android/widget/TextView:mMarqueeRepeatLimit	I
        //     1524: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1527: invokevirtual 489	android/widget/TextView:setMarqueeRepeatLimit	(I)V
        //     1530: goto -538 -> 992
        //     1533: aload 46
        //     1535: iload 82
        //     1537: iconst_1
        //     1538: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1541: ifne -549 -> 992
        //     1544: aload_0
        //     1545: iconst_0
        //     1546: invokevirtual 492	android/widget/TextView:setIncludeFontPadding	(Z)V
        //     1549: goto -557 -> 992
        //     1552: aload 46
        //     1554: iload 82
        //     1556: iconst_1
        //     1557: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1560: ifne -568 -> 992
        //     1563: aload_0
        //     1564: iconst_0
        //     1565: invokevirtual 495	android/widget/TextView:setCursorVisible	(Z)V
        //     1568: goto -576 -> 992
        //     1571: aload 46
        //     1573: iload 82
        //     1575: bipush 255
        //     1577: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1580: istore 37
        //     1582: goto -590 -> 992
        //     1585: aload_0
        //     1586: aload 46
        //     1588: iload 82
        //     1590: fconst_1
        //     1591: invokevirtual 499	android/content/res/TypedArray:getFloat	(IF)F
        //     1594: invokevirtual 502	android/widget/TextView:setTextScaleX	(F)V
        //     1597: goto -605 -> 992
        //     1600: aload_0
        //     1601: aload 46
        //     1603: iload 82
        //     1605: iconst_0
        //     1606: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1609: putfield 504	android/widget/TextView:mFreezesText	Z
        //     1612: goto -620 -> 992
        //     1615: aload 46
        //     1617: iload 82
        //     1619: iconst_0
        //     1620: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1623: istore 40
        //     1625: goto -633 -> 992
        //     1628: aload 46
        //     1630: iload 82
        //     1632: fconst_0
        //     1633: invokevirtual 499	android/content/res/TypedArray:getFloat	(IF)F
        //     1636: fstore 41
        //     1638: goto -646 -> 992
        //     1641: aload 46
        //     1643: iload 82
        //     1645: fconst_0
        //     1646: invokevirtual 499	android/content/res/TypedArray:getFloat	(IF)F
        //     1649: fstore 42
        //     1651: goto -659 -> 992
        //     1654: aload 46
        //     1656: iload 82
        //     1658: fconst_0
        //     1659: invokevirtual 499	android/content/res/TypedArray:getFloat	(IF)F
        //     1662: fstore 43
        //     1664: goto -672 -> 992
        //     1667: aload_0
        //     1668: aload 46
        //     1670: iload 82
        //     1672: aload_0
        //     1673: invokevirtual 507	android/widget/TextView:isEnabled	()Z
        //     1676: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1679: invokevirtual 510	android/widget/TextView:setEnabled	(Z)V
        //     1682: goto -690 -> 992
        //     1685: aload 46
        //     1687: iload 82
        //     1689: iload 6
        //     1691: invokevirtual 410	android/content/res/TypedArray:getColor	(II)I
        //     1694: istore 6
        //     1696: goto -704 -> 992
        //     1699: aload 46
        //     1701: iload 82
        //     1703: invokevirtual 414	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
        //     1706: astore 7
        //     1708: goto -716 -> 992
        //     1711: aload 46
        //     1713: iload 82
        //     1715: invokevirtual 414	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
        //     1718: astore 8
        //     1720: goto -728 -> 992
        //     1723: aload 46
        //     1725: iload 82
        //     1727: invokevirtual 414	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
        //     1730: astore 9
        //     1732: goto -740 -> 992
        //     1735: aload 46
        //     1737: iload 82
        //     1739: iload 10
        //     1741: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1744: istore 10
        //     1746: goto -754 -> 992
        //     1749: aload 46
        //     1751: iload 82
        //     1753: iload 12
        //     1755: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1758: istore 12
        //     1760: goto -768 -> 992
        //     1763: aload 46
        //     1765: iload 82
        //     1767: iload 13
        //     1769: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1772: istore 13
        //     1774: goto -782 -> 992
        //     1777: aload 46
        //     1779: iload 82
        //     1781: invokevirtual 424	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     1784: astore 11
        //     1786: goto -794 -> 992
        //     1789: aload 46
        //     1791: iload 82
        //     1793: iload 44
        //     1795: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     1798: istore 44
        //     1800: goto -808 -> 992
        //     1803: aload_0
        //     1804: aload 46
        //     1806: iload 82
        //     1808: aload_0
        //     1809: getfield 285	android/widget/TextView:mSpacingAdd	F
        //     1812: f2i
        //     1813: invokevirtual 417	android/content/res/TypedArray:getDimensionPixelSize	(II)I
        //     1816: i2f
        //     1817: putfield 285	android/widget/TextView:mSpacingAdd	F
        //     1820: goto -828 -> 992
        //     1823: aload_0
        //     1824: aload 46
        //     1826: iload 82
        //     1828: aload_0
        //     1829: getfield 283	android/widget/TextView:mSpacingMult	F
        //     1832: invokevirtual 499	android/content/res/TypedArray:getFloat	(IF)F
        //     1835: putfield 283	android/widget/TextView:mSpacingMult	F
        //     1838: goto -846 -> 992
        //     1841: aload 46
        //     1843: iload 82
        //     1845: iconst_0
        //     1846: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1849: istore 45
        //     1851: goto -859 -> 992
        //     1854: aload_0
        //     1855: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     1858: aload_0
        //     1859: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1862: invokevirtual 520	android/widget/Editor:createInputContentTypeIfNeeded	()V
        //     1865: aload_0
        //     1866: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1869: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     1872: aload 46
        //     1874: iload 82
        //     1876: aload_0
        //     1877: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1880: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     1883: getfield 529	android/widget/Editor$InputContentType:imeOptions	I
        //     1886: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1889: putfield 529	android/widget/Editor$InputContentType:imeOptions	I
        //     1892: goto -900 -> 992
        //     1895: aload_0
        //     1896: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     1899: aload_0
        //     1900: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1903: invokevirtual 520	android/widget/Editor:createInputContentTypeIfNeeded	()V
        //     1906: aload_0
        //     1907: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1910: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     1913: aload 46
        //     1915: iload 82
        //     1917: invokevirtual 438	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     1920: putfield 532	android/widget/Editor$InputContentType:imeActionLabel	Ljava/lang/CharSequence;
        //     1923: goto -931 -> 992
        //     1926: aload_0
        //     1927: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     1930: aload_0
        //     1931: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1934: invokevirtual 520	android/widget/Editor:createInputContentTypeIfNeeded	()V
        //     1937: aload_0
        //     1938: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1941: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     1944: aload 46
        //     1946: iload 82
        //     1948: aload_0
        //     1949: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     1952: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     1955: getfield 535	android/widget/Editor$InputContentType:imeActionId	I
        //     1958: invokevirtual 420	android/content/res/TypedArray:getInt	(II)I
        //     1961: putfield 535	android/widget/Editor$InputContentType:imeActionId	I
        //     1964: goto -972 -> 992
        //     1967: aload_0
        //     1968: aload 46
        //     1970: iload 82
        //     1972: invokevirtual 424	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     1975: invokevirtual 539	android/widget/TextView:setPrivateImeOptions	(Ljava/lang/String;)V
        //     1978: goto -986 -> 992
        //     1981: aload_0
        //     1982: aload 46
        //     1984: iload 82
        //     1986: iconst_0
        //     1987: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     1990: invokevirtual 542	android/widget/TextView:setInputExtras	(I)V
        //     1993: goto -1001 -> 992
        //     1996: astore 85
        //     1998: ldc 65
        //     2000: ldc_w 544
        //     2003: aload 85
        //     2005: invokestatic 550	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     2008: pop
        //     2009: goto -1017 -> 992
        //     2012: astore 83
        //     2014: ldc 65
        //     2016: ldc_w 544
        //     2019: aload 83
        //     2021: invokestatic 550	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     2024: pop
        //     2025: goto -1033 -> 992
        //     2028: aload_0
        //     2029: aload 46
        //     2031: iload 82
        //     2033: iconst_0
        //     2034: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     2037: putfield 552	android/widget/TextView:mCursorDrawableRes	I
        //     2040: goto -1048 -> 992
        //     2043: aload_0
        //     2044: aload 46
        //     2046: iload 82
        //     2048: iconst_0
        //     2049: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     2052: putfield 554	android/widget/TextView:mTextSelectHandleLeftRes	I
        //     2055: goto -1063 -> 992
        //     2058: aload_0
        //     2059: aload 46
        //     2061: iload 82
        //     2063: iconst_0
        //     2064: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     2067: putfield 556	android/widget/TextView:mTextSelectHandleRightRes	I
        //     2070: goto -1078 -> 992
        //     2073: aload_0
        //     2074: aload 46
        //     2076: iload 82
        //     2078: iconst_0
        //     2079: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     2082: putfield 558	android/widget/TextView:mTextSelectHandleRes	I
        //     2085: goto -1093 -> 992
        //     2088: aload_0
        //     2089: aload 46
        //     2091: iload 82
        //     2093: iconst_0
        //     2094: invokevirtual 390	android/content/res/TypedArray:getResourceId	(II)I
        //     2097: putfield 560	android/widget/TextView:mTextEditSuggestionItemLayout	I
        //     2100: goto -1108 -> 992
        //     2103: aload_0
        //     2104: aload 46
        //     2106: iload 82
        //     2108: iconst_0
        //     2109: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     2112: invokevirtual 563	android/widget/TextView:setTextIsSelectable	(Z)V
        //     2115: goto -1123 -> 992
        //     2118: aload 46
        //     2120: iload 82
        //     2122: iconst_0
        //     2123: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     2126: istore 14
        //     2128: goto -1136 -> 992
        //     2131: aload 46
        //     2133: invokevirtual 393	android/content/res/TypedArray:recycle	()V
        //     2136: getstatic 566	android/widget/TextView$BufferType:EDITABLE	Landroid/widget/TextView$BufferType;
        //     2139: astore 49
        //     2141: sipush 4095
        //     2144: iload 45
        //     2146: iand
        //     2147: istore 50
        //     2149: iload 50
        //     2151: sipush 129
        //     2154: if_icmpne +508 -> 2662
        //     2157: iconst_1
        //     2158: istore 51
        //     2160: iload 50
        //     2162: sipush 225
        //     2165: if_icmpne +503 -> 2668
        //     2168: iconst_1
        //     2169: istore 52
        //     2171: iload 50
        //     2173: bipush 18
        //     2175: if_icmpne +499 -> 2674
        //     2178: iconst_1
        //     2179: istore 53
        //     2181: aload 20
        //     2183: ifnull +579 -> 2762
        //     2186: aload 20
        //     2188: invokevirtual 572	java/lang/Object:toString	()Ljava/lang/String;
        //     2191: invokestatic 578	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
        //     2194: astore 73
        //     2196: aload_0
        //     2197: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     2200: aload_0
        //     2201: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2204: aload 73
        //     2206: invokevirtual 582	java/lang/Class:newInstance	()Ljava/lang/Object;
        //     2209: checkcast 584	android/text/method/KeyListener
        //     2212: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     2215: aload_0
        //     2216: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2219: astore 79
        //     2221: iload 45
        //     2223: ifeq +505 -> 2728
        //     2226: iload 45
        //     2228: istore 81
        //     2230: aload 79
        //     2232: iload 81
        //     2234: putfield 591	android/widget/Editor:mInputType	I
        //     2237: aload_0
        //     2238: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2241: ifnull +18 -> 2259
        //     2244: aload_0
        //     2245: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2248: iload 44
        //     2250: iload 51
        //     2252: iload 52
        //     2254: iload 53
        //     2256: invokevirtual 595	android/widget/Editor:adjustInputType	(ZZZZ)V
        //     2259: iload 27
        //     2261: ifeq +28 -> 2289
        //     2264: aload_0
        //     2265: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     2268: aload_0
        //     2269: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2272: iconst_1
        //     2273: putfield 598	android/widget/Editor:mSelectAllOnFocus	Z
        //     2276: aload 49
        //     2278: getstatic 274	android/widget/TextView$BufferType:NORMAL	Landroid/widget/TextView$BufferType;
        //     2281: if_acmpne +8 -> 2289
        //     2284: getstatic 601	android/widget/TextView$BufferType:SPANNABLE	Landroid/widget/TextView$BufferType;
        //     2287: astore 49
        //     2289: aload_0
        //     2290: aload 28
        //     2292: aload 29
        //     2294: aload 30
        //     2296: aload 31
        //     2298: invokevirtual 605	android/widget/TextView:setCompoundDrawablesWithIntrinsicBounds	(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
        //     2301: aload_0
        //     2302: aload 32
        //     2304: aload 33
        //     2306: invokespecial 609	android/widget/TextView:setRelativeDrawablesIfNeeded	(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
        //     2309: aload_0
        //     2310: iload 34
        //     2312: invokevirtual 612	android/widget/TextView:setCompoundDrawablePadding	(I)V
        //     2315: aload_0
        //     2316: iload 36
        //     2318: invokespecial 615	android/widget/TextView:setInputTypeSingleLine	(Z)V
        //     2321: aload_0
        //     2322: iload 36
        //     2324: iload 36
        //     2326: iload 36
        //     2328: invokespecial 619	android/widget/TextView:applySingleLine	(ZZZ)V
        //     2331: iload 36
        //     2333: ifeq +18 -> 2351
        //     2336: aload_0
        //     2337: invokevirtual 623	android/widget/TextView:getKeyListener	()Landroid/text/method/KeyListener;
        //     2340: ifnonnull +11 -> 2351
        //     2343: iload 35
        //     2345: ifge +6 -> 2351
        //     2348: iconst_3
        //     2349: istore 35
        //     2351: iload 35
        //     2353: tableswitch	default:+31 -> 2384, 1:+902->3255, 2:+912->3265, 3:+922->3275, 4:+932->3285
        //     2385: iconst_4
        //     2386: ifnull +942 -> 3328
        //     2389: aload_0
        //     2390: aload 7
        //     2392: invokevirtual 627	android/widget/TextView:setTextColor	(Landroid/content/res/ColorStateList;)V
        //     2395: aload_0
        //     2396: aload 8
        //     2398: invokevirtual 630	android/widget/TextView:setHintTextColor	(Landroid/content/res/ColorStateList;)V
        //     2401: aload_0
        //     2402: aload 9
        //     2404: invokevirtual 633	android/widget/TextView:setLinkTextColor	(Landroid/content/res/ColorStateList;)V
        //     2407: iload 6
        //     2409: ifeq +9 -> 2418
        //     2412: aload_0
        //     2413: iload 6
        //     2415: invokevirtual 636	android/widget/TextView:setHighlightColor	(I)V
        //     2418: aload_0
        //     2419: iload 10
        //     2421: i2f
        //     2422: invokespecial 639	android/widget/TextView:setRawTextSize	(F)V
        //     2425: iload 14
        //     2427: ifeq +18 -> 2445
        //     2430: aload_0
        //     2431: new 641	android/text/method/AllCapsTransformationMethod
        //     2434: dup
        //     2435: aload_0
        //     2436: invokevirtual 645	android/widget/TextView:getContext	()Landroid/content/Context;
        //     2439: invokespecial 647	android/text/method/AllCapsTransformationMethod:<init>	(Landroid/content/Context;)V
        //     2442: invokevirtual 651	android/widget/TextView:setTransformationMethod	(Landroid/text/method/TransformationMethod;)V
        //     2445: iload 44
        //     2447: ifne +18 -> 2465
        //     2450: iload 51
        //     2452: ifne +13 -> 2465
        //     2455: iload 52
        //     2457: ifne +8 -> 2465
        //     2460: iload 53
        //     2462: ifeq +877 -> 3339
        //     2465: aload_0
        //     2466: invokestatic 656	android/text/method/PasswordTransformationMethod:getInstance	()Landroid/text/method/PasswordTransformationMethod;
        //     2469: invokevirtual 651	android/widget/TextView:setTransformationMethod	(Landroid/text/method/TransformationMethod;)V
        //     2472: iconst_3
        //     2473: istore 12
        //     2475: aload_0
        //     2476: aload 11
        //     2478: iload 12
        //     2480: iload 13
        //     2482: invokespecial 660	android/widget/TextView:setTypefaceFromAttrs	(Ljava/lang/String;II)V
        //     2485: iload 40
        //     2487: ifeq +15 -> 2502
        //     2490: aload_0
        //     2491: fload 43
        //     2493: fload 41
        //     2495: fload 42
        //     2497: iload 40
        //     2499: invokevirtual 664	android/widget/TextView:setShadowLayer	(FFFI)V
        //     2502: iload 37
        //     2504: iflt +865 -> 3369
        //     2507: iconst_1
        //     2508: anewarray 199	android/text/InputFilter
        //     2511: astore 63
        //     2513: new 666	android/text/InputFilter$LengthFilter
        //     2516: dup
        //     2517: iload 37
        //     2519: invokespecial 667	android/text/InputFilter$LengthFilter:<init>	(I)V
        //     2522: astore 64
        //     2524: aload 63
        //     2526: iconst_0
        //     2527: aload 64
        //     2529: aastore
        //     2530: aload_0
        //     2531: aload 63
        //     2533: invokevirtual 671	android/widget/TextView:setFilters	([Landroid/text/InputFilter;)V
        //     2536: aload_0
        //     2537: aload 38
        //     2539: aload 49
        //     2541: invokevirtual 675	android/widget/TextView:setText	(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
        //     2544: aload 39
        //     2546: ifnull +9 -> 2555
        //     2549: aload_0
        //     2550: aload 39
        //     2552: invokevirtual 678	android/widget/TextView:setHint	(Ljava/lang/CharSequence;)V
        //     2555: aload_1
        //     2556: aload_2
        //     2557: getstatic 681	com/android/internal/R$styleable:View	[I
        //     2560: iload_3
        //     2561: iconst_0
        //     2562: invokevirtual 682	android/content/Context:obtainStyledAttributes	(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
        //     2565: astore 56
        //     2567: aload_0
        //     2568: getfield 365	android/widget/TextView:mMovement	Landroid/text/method/MovementMethod;
        //     2571: ifnonnull +10 -> 2581
        //     2574: aload_0
        //     2575: invokevirtual 623	android/widget/TextView:getKeyListener	()Landroid/text/method/KeyListener;
        //     2578: ifnull +801 -> 3379
        //     2581: iconst_1
        //     2582: istore 57
        //     2584: iload 57
        //     2586: istore 58
        //     2588: iload 57
        //     2590: istore 59
        //     2592: aload 56
        //     2594: invokevirtual 403	android/content/res/TypedArray:getIndexCount	()I
        //     2597: istore 60
        //     2599: iconst_0
        //     2600: istore 61
        //     2602: iload 61
        //     2604: iload 60
        //     2606: if_icmpge +821 -> 3427
        //     2609: aload 56
        //     2611: iload 61
        //     2613: invokevirtual 407	android/content/res/TypedArray:getIndex	(I)I
        //     2616: istore 62
        //     2618: iload 62
        //     2620: lookupswitch	default:+36->2656, 18:+765->3385, 29:+779->3399, 30:+793->3413
        //     2657: istore_2
        //     2658: aconst_null
        //     2659: goto -57 -> 2602
        //     2662: iconst_0
        //     2663: istore 51
        //     2665: goto -505 -> 2160
        //     2668: iconst_0
        //     2669: istore 52
        //     2671: goto -500 -> 2171
        //     2674: iconst_0
        //     2675: istore 53
        //     2677: goto -496 -> 2181
        //     2680: astore 71
        //     2682: new 684	java/lang/RuntimeException
        //     2685: dup
        //     2686: aload 71
        //     2688: invokespecial 687	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     2691: astore 72
        //     2693: aload 72
        //     2695: athrow
        //     2696: astore 76
        //     2698: new 684	java/lang/RuntimeException
        //     2701: dup
        //     2702: aload 76
        //     2704: invokespecial 687	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     2707: astore 77
        //     2709: aload 77
        //     2711: athrow
        //     2712: astore 74
        //     2714: new 684	java/lang/RuntimeException
        //     2717: dup
        //     2718: aload 74
        //     2720: invokespecial 687	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     2723: astore 75
        //     2725: aload 75
        //     2727: athrow
        //     2728: aload_0
        //     2729: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2732: getfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     2735: invokeinterface 690 1 0
        //     2740: istore 80
        //     2742: iload 80
        //     2744: istore 81
        //     2746: goto -516 -> 2230
        //     2749: astore 78
        //     2751: aload_0
        //     2752: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2755: iconst_1
        //     2756: putfield 591	android/widget/Editor:mInputType	I
        //     2759: goto -522 -> 2237
        //     2762: aload 22
        //     2764: ifnull +53 -> 2817
        //     2767: aload_0
        //     2768: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     2771: aload_0
        //     2772: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2775: aload 22
        //     2777: invokevirtual 572	java/lang/Object:toString	()Ljava/lang/String;
        //     2780: invokestatic 695	android/text/method/DigitsKeyListener:getInstance	(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;
        //     2783: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     2786: aload_0
        //     2787: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2790: astore 69
        //     2792: iload 45
        //     2794: ifeq +17 -> 2811
        //     2797: iload 45
        //     2799: istore 70
        //     2801: aload 69
        //     2803: iload 70
        //     2805: putfield 591	android/widget/Editor:mInputType	I
        //     2808: goto -571 -> 2237
        //     2811: iconst_1
        //     2812: istore 70
        //     2814: goto -13 -> 2801
        //     2817: iload 45
        //     2819: ifeq +30 -> 2849
        //     2822: aload_0
        //     2823: iload 45
        //     2825: iconst_1
        //     2826: invokespecial 699	android/widget/TextView:setInputType	(IZ)V
        //     2829: iload 45
        //     2831: invokestatic 703	android/widget/TextView:isMultilineInputType	(I)Z
        //     2834: ifne +9 -> 2843
        //     2837: iconst_1
        //     2838: istore 36
        //     2840: goto -603 -> 2237
        //     2843: iconst_0
        //     2844: istore 36
        //     2846: goto -6 -> 2840
        //     2849: iload 23
        //     2851: ifeq +28 -> 2879
        //     2854: aload_0
        //     2855: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     2858: aload_0
        //     2859: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2862: invokestatic 708	android/text/method/DialerKeyListener:getInstance	()Landroid/text/method/DialerKeyListener;
        //     2865: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     2868: aload_0
        //     2869: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2872: iconst_3
        //     2873: putfield 591	android/widget/Editor:mInputType	I
        //     2876: goto -639 -> 2237
        //     2879: iload 21
        //     2881: ifeq +102 -> 2983
        //     2884: aload_0
        //     2885: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     2888: aload_0
        //     2889: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2892: astore 65
        //     2894: iload 21
        //     2896: iconst_2
        //     2897: iand
        //     2898: ifeq +73 -> 2971
        //     2901: iconst_1
        //     2902: istore 66
        //     2904: iload 21
        //     2906: iconst_4
        //     2907: iand
        //     2908: ifeq +69 -> 2977
        //     2911: iconst_1
        //     2912: istore 67
        //     2914: aload 65
        //     2916: iload 66
        //     2918: iload 67
        //     2920: invokestatic 711	android/text/method/DigitsKeyListener:getInstance	(ZZ)Landroid/text/method/DigitsKeyListener;
        //     2923: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     2926: iconst_2
        //     2927: istore 68
        //     2929: iload 21
        //     2931: iconst_2
        //     2932: iand
        //     2933: ifeq +11 -> 2944
        //     2936: sipush 4096
        //     2939: iload 68
        //     2941: ior
        //     2942: istore 68
        //     2944: iload 21
        //     2946: iconst_4
        //     2947: iand
        //     2948: ifeq +11 -> 2959
        //     2951: sipush 8192
        //     2954: iload 68
        //     2956: ior
        //     2957: istore 68
        //     2959: aload_0
        //     2960: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     2963: iload 68
        //     2965: putfield 591	android/widget/Editor:mInputType	I
        //     2968: goto -731 -> 2237
        //     2971: iconst_0
        //     2972: istore 66
        //     2974: goto -70 -> 2904
        //     2977: iconst_0
        //     2978: istore 67
        //     2980: goto -66 -> 2914
        //     2983: iload 24
        //     2985: ifne +10 -> 2995
        //     2988: iload 25
        //     2990: bipush 255
        //     2992: if_icmpeq +119 -> 3111
        //     2995: iconst_1
        //     2996: istore 54
        //     2998: iload 25
        //     3000: tableswitch	default:+28 -> 3028, 1:+63->3063, 2:+79->3079, 3:+95->3095
        //     3029: iconst_m1
        //     3030: <illegal opcode>
        //     3031: astore 55
        //     3033: aload_0
        //     3034: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     3037: aload_0
        //     3038: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3041: iload 24
        //     3043: aload 55
        //     3045: invokestatic 722	android/text/method/TextKeyListener:getInstance	(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;
        //     3048: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     3051: aload_0
        //     3052: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3055: iload 54
        //     3057: putfield 591	android/widget/Editor:mInputType	I
        //     3060: goto -823 -> 2237
        //     3063: getstatic 725	android/text/method/TextKeyListener$Capitalize:SENTENCES	Landroid/text/method/TextKeyListener$Capitalize;
        //     3066: astore 55
        //     3068: sipush 16384
        //     3071: iload 54
        //     3073: ior
        //     3074: istore 54
        //     3076: goto -43 -> 3033
        //     3079: getstatic 728	android/text/method/TextKeyListener$Capitalize:WORDS	Landroid/text/method/TextKeyListener$Capitalize;
        //     3082: astore 55
        //     3084: sipush 8192
        //     3087: iload 54
        //     3089: ior
        //     3090: istore 54
        //     3092: goto -59 -> 3033
        //     3095: getstatic 731	android/text/method/TextKeyListener$Capitalize:CHARACTERS	Landroid/text/method/TextKeyListener$Capitalize;
        //     3098: astore 55
        //     3100: sipush 4096
        //     3103: iload 54
        //     3105: ior
        //     3106: istore 54
        //     3108: goto -75 -> 3033
        //     3111: aload_0
        //     3112: invokevirtual 734	android/widget/TextView:isTextSelectable	()Z
        //     3115: ifeq +41 -> 3156
        //     3118: aload_0
        //     3119: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3122: ifnull +19 -> 3141
        //     3125: aload_0
        //     3126: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3129: aconst_null
        //     3130: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     3133: aload_0
        //     3134: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3137: iconst_0
        //     3138: putfield 591	android/widget/Editor:mInputType	I
        //     3141: getstatic 601	android/widget/TextView$BufferType:SPANNABLE	Landroid/widget/TextView$BufferType;
        //     3144: astore 49
        //     3146: aload_0
        //     3147: invokestatic 738	android/text/method/ArrowKeyMovementMethod:getInstance	()Landroid/text/method/MovementMethod;
        //     3150: invokevirtual 742	android/widget/TextView:setMovementMethod	(Landroid/text/method/MovementMethod;)V
        //     3153: goto -916 -> 2237
        //     3156: iload 19
        //     3158: ifeq +28 -> 3186
        //     3161: aload_0
        //     3162: invokespecial 513	android/widget/TextView:createEditorIfNeeded	()V
        //     3165: aload_0
        //     3166: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3169: invokestatic 745	android/text/method/TextKeyListener:getInstance	()Landroid/text/method/TextKeyListener;
        //     3172: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     3175: aload_0
        //     3176: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3179: iconst_1
        //     3180: putfield 591	android/widget/Editor:mInputType	I
        //     3183: goto -946 -> 2237
        //     3186: aload_0
        //     3187: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3190: ifnull +11 -> 3201
        //     3193: aload_0
        //     3194: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3197: aconst_null
        //     3198: putfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     3201: iload 26
        //     3203: tableswitch	default:+25 -> 3228, 0:+28->3231, 1:+36->3239, 2:+44->3247
        //     3229: <illegal opcode>
        //     3230: lload_3
        //     3231: getstatic 274	android/widget/TextView$BufferType:NORMAL	Landroid/widget/TextView$BufferType;
        //     3234: astore 49
        //     3236: goto -999 -> 2237
        //     3239: getstatic 601	android/widget/TextView$BufferType:SPANNABLE	Landroid/widget/TextView$BufferType;
        //     3242: astore 49
        //     3244: goto -1007 -> 2237
        //     3247: getstatic 566	android/widget/TextView$BufferType:EDITABLE	Landroid/widget/TextView$BufferType;
        //     3250: astore 49
        //     3252: goto -1015 -> 2237
        //     3255: aload_0
        //     3256: getstatic 750	android/text/TextUtils$TruncateAt:START	Landroid/text/TextUtils$TruncateAt;
        //     3259: invokevirtual 754	android/widget/TextView:setEllipsize	(Landroid/text/TextUtils$TruncateAt;)V
        //     3262: goto -878 -> 2384
        //     3265: aload_0
        //     3266: getstatic 757	android/text/TextUtils$TruncateAt:MIDDLE	Landroid/text/TextUtils$TruncateAt;
        //     3269: invokevirtual 754	android/widget/TextView:setEllipsize	(Landroid/text/TextUtils$TruncateAt;)V
        //     3272: goto -888 -> 2384
        //     3275: aload_0
        //     3276: getstatic 760	android/text/TextUtils$TruncateAt:END	Landroid/text/TextUtils$TruncateAt;
        //     3279: invokevirtual 754	android/widget/TextView:setEllipsize	(Landroid/text/TextUtils$TruncateAt;)V
        //     3282: goto -898 -> 2384
        //     3285: aload_1
        //     3286: invokestatic 766	android/view/ViewConfiguration:get	(Landroid/content/Context;)Landroid/view/ViewConfiguration;
        //     3289: invokevirtual 769	android/view/ViewConfiguration:isFadingMarqueeEnabled	()Z
        //     3292: ifeq +23 -> 3315
        //     3295: aload_0
        //     3296: iconst_1
        //     3297: invokevirtual 772	android/widget/TextView:setHorizontalFadingEdgeEnabled	(Z)V
        //     3300: aload_0
        //     3301: iconst_0
        //     3302: putfield 271	android/widget/TextView:mMarqueeFadeMode	I
        //     3305: aload_0
        //     3306: getstatic 775	android/text/TextUtils$TruncateAt:MARQUEE	Landroid/text/TextUtils$TruncateAt;
        //     3309: invokevirtual 754	android/widget/TextView:setEllipsize	(Landroid/text/TextUtils$TruncateAt;)V
        //     3312: goto -928 -> 2384
        //     3315: aload_0
        //     3316: iconst_0
        //     3317: invokevirtual 772	android/widget/TextView:setHorizontalFadingEdgeEnabled	(Z)V
        //     3320: aload_0
        //     3321: iconst_1
        //     3322: putfield 271	android/widget/TextView:mMarqueeFadeMode	I
        //     3325: goto -20 -> 3305
        //     3328: ldc_w 776
        //     3331: invokestatic 781	android/content/res/ColorStateList:valueOf	(I)Landroid/content/res/ColorStateList;
        //     3334: astore 7
        //     3336: goto -947 -> 2389
        //     3339: aload_0
        //     3340: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3343: ifnull -868 -> 2475
        //     3346: sipush 4095
        //     3349: aload_0
        //     3350: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3353: getfield 591	android/widget/Editor:mInputType	I
        //     3356: iand
        //     3357: sipush 129
        //     3360: if_icmpne -885 -> 2475
        //     3363: iconst_3
        //     3364: istore 12
        //     3366: goto -891 -> 2475
        //     3369: aload_0
        //     3370: getstatic 201	android/widget/TextView:NO_FILTERS	[Landroid/text/InputFilter;
        //     3373: invokevirtual 671	android/widget/TextView:setFilters	([Landroid/text/InputFilter;)V
        //     3376: goto -840 -> 2536
        //     3379: iconst_0
        //     3380: istore 57
        //     3382: goto -798 -> 2584
        //     3385: aload 56
        //     3387: iload 62
        //     3389: iload 57
        //     3391: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     3394: istore 57
        //     3396: goto -740 -> 2656
        //     3399: aload 56
        //     3401: iload 62
        //     3403: iload 58
        //     3405: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     3408: istore 58
        //     3410: goto -754 -> 2656
        //     3413: aload 56
        //     3415: iload 62
        //     3417: iload 59
        //     3419: invokevirtual 428	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     3422: istore 59
        //     3424: goto -768 -> 2656
        //     3427: aload 56
        //     3429: invokevirtual 393	android/content/res/TypedArray:recycle	()V
        //     3432: aload_0
        //     3433: iload 57
        //     3435: invokevirtual 784	android/widget/TextView:setFocusable	(Z)V
        //     3438: aload_0
        //     3439: iload 58
        //     3441: invokevirtual 787	android/widget/TextView:setClickable	(Z)V
        //     3444: aload_0
        //     3445: iload 59
        //     3447: invokevirtual 790	android/widget/TextView:setLongClickable	(Z)V
        //     3450: aload_0
        //     3451: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3454: ifnull +10 -> 3464
        //     3457: aload_0
        //     3458: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     3461: invokevirtual 793	android/widget/Editor:prepareCursorControllers	()V
        //     3464: aload_0
        //     3465: invokevirtual 796	android/widget/TextView:getImportantForAccessibility	()I
        //     3468: ifne +8 -> 3476
        //     3471: aload_0
        //     3472: iconst_1
        //     3473: invokevirtual 799	android/widget/TextView:setImportantForAccessibility	(I)V
        //     3476: return
        //
        // Exception table:
        //     from	to	target	type
        //     1981	1993	1996	org/xmlpull/v1/XmlPullParserException
        //     1981	1993	2012	java/io/IOException
        //     2186	2196	2680	java/lang/ClassNotFoundException
        //     2196	2215	2696	java/lang/InstantiationException
        //     2196	2215	2712	java/lang/IllegalAccessException
        //     2215	2237	2749	java/lang/IncompatibleClassChangeError
        //     2728	2742	2749	java/lang/IncompatibleClassChangeError
    }

    private void applySingleLine(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        this.mSingleLine = paramBoolean1;
        if (paramBoolean1)
        {
            setLines(1);
            setHorizontallyScrolling(true);
            if (paramBoolean2)
                setTransformationMethod(SingleLineTransformationMethod.getInstance());
        }
        while (true)
        {
            return;
            if (paramBoolean3)
                setMaxLines(2147483647);
            setHorizontallyScrolling(false);
            if (paramBoolean2)
                setTransformationMethod(null);
        }
    }

    private void assumeLayout()
    {
        int i = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
        if (i < 1)
            i = 0;
        int j = i;
        if (this.mHorizontallyScrolling)
            i = 1048576;
        makeNewLayout(i, j, UNKNOWN_BORING, UNKNOWN_BORING, j, false);
    }

    private boolean bringTextIntoView()
    {
        int i = 1;
        Layout localLayout;
        int j;
        Layout.Alignment localAlignment;
        int k;
        int m;
        int n;
        int i1;
        label117: int i4;
        int i5;
        int i2;
        label171: int i3;
        if (isShowingHint())
        {
            localLayout = this.mHintLayout;
            j = 0;
            if ((0x70 & this.mGravity) == 80)
                j = -1 + localLayout.getLineCount();
            localAlignment = localLayout.getParagraphAlignment(j);
            k = localLayout.getParagraphDirection(j);
            m = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
            n = this.mBottom - this.mTop - getExtendedPaddingTop() - getExtendedPaddingBottom();
            i1 = localLayout.getHeight();
            if (localAlignment != Layout.Alignment.ALIGN_NORMAL)
                break label225;
            if (k != i)
                break label217;
            localAlignment = Layout.Alignment.ALIGN_LEFT;
            if (localAlignment != Layout.Alignment.ALIGN_CENTER)
                break label277;
            i4 = (int)FloatMath.floor(localLayout.getLineLeft(j));
            i5 = (int)FloatMath.ceil(localLayout.getLineRight(j));
            if (i5 - i4 >= m)
                break label255;
            i2 = (i5 + i4) / 2 - m / 2;
            if (i1 >= n)
                break label316;
            i3 = 0;
            label181: if ((i2 == this.mScrollX) && (i3 == this.mScrollY))
                break label344;
            scrollTo(i2, i3);
        }
        while (true)
        {
            return i;
            localLayout = this.mLayout;
            break;
            label217: localAlignment = Layout.Alignment.ALIGN_RIGHT;
            break label117;
            label225: if (localAlignment != Layout.Alignment.ALIGN_OPPOSITE)
                break label117;
            if (k == i);
            for (localAlignment = Layout.Alignment.ALIGN_RIGHT; ; localAlignment = Layout.Alignment.ALIGN_LEFT)
                break;
            label255: if (k < 0)
            {
                i2 = i5 - m;
                break label171;
            }
            i2 = i4;
            break label171;
            label277: if (localAlignment == Layout.Alignment.ALIGN_RIGHT)
            {
                i2 = (int)FloatMath.ceil(localLayout.getLineRight(j)) - m;
                break label171;
            }
            i2 = (int)FloatMath.floor(localLayout.getLineLeft(j));
            break label171;
            label316: if ((0x70 & this.mGravity) == 80)
            {
                i3 = i1 - n;
                break label181;
            }
            i3 = 0;
            break label181;
            label344: i = 0;
        }
    }

    private boolean canMarquee()
    {
        boolean bool = false;
        int i = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
        if ((i > 0) && ((this.mLayout.getLineWidth(0) > i) || ((this.mMarqueeFadeMode != 0) && (this.mSavedMarqueeModeLayout != null) && (this.mSavedMarqueeModeLayout.getLineWidth(0) > i))))
            bool = true;
        return bool;
    }

    private boolean canSelectText()
    {
        if ((this.mText.length() != 0) && (this.mEditor != null) && (this.mEditor.hasSelectionController()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void checkForRelayout()
    {
        int i;
        int k;
        if (((this.mLayoutParams.width != -2) || ((this.mMaxWidthMode == this.mMinWidthMode) && (this.mMaxWidth == this.mMinWidth))) && ((this.mHint == null) || (this.mHintLayout != null)) && (this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight() > 0))
        {
            i = this.mLayout.getHeight();
            int j = this.mLayout.getWidth();
            if (this.mHintLayout == null)
            {
                k = 0;
                makeNewLayout(j, k, UNKNOWN_BORING, UNKNOWN_BORING, this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight(), false);
                if (this.mEllipsize == TextUtils.TruncateAt.MARQUEE)
                    break label213;
                if ((this.mLayoutParams.height == -2) || (this.mLayoutParams.height == -1))
                    break label177;
                invalidate();
            }
        }
        while (true)
        {
            return;
            k = this.mHintLayout.getWidth();
            break;
            label177: if ((this.mLayout.getHeight() == i) && ((this.mHintLayout == null) || (this.mHintLayout.getHeight() == i)))
            {
                invalidate();
            }
            else
            {
                label213: requestLayout();
                invalidate();
                continue;
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    private void checkForResize()
    {
        int i = 0;
        if (this.mLayout != null)
        {
            if (this.mLayoutParams.width == -2)
            {
                i = 1;
                invalidate();
            }
            if (this.mLayoutParams.height != -2)
                break label61;
            if (getDesiredHeight() == getHeight());
        }
        for (i = 1; ; i = 1)
            label61: 
            do
            {
                if (i != 0)
                    requestLayout();
                return;
            }
            while ((this.mLayoutParams.height != -1) || (this.mDesiredHeightAtMeasure < 0) || (getDesiredHeight() == this.mDesiredHeightAtMeasure));
    }

    private boolean compressText(float paramFloat)
    {
        boolean bool = false;
        if (isHardwareAccelerated());
        while (true)
        {
            return bool;
            if ((paramFloat > 0.0F) && (this.mLayout != null) && (getLineCount() == 1) && (!this.mUserSetTextScaleX) && (this.mTextPaint.getTextScaleX() == 1.0F))
            {
                float f = (1.0F + this.mLayout.getLineWidth(0) - paramFloat) / paramFloat;
                if ((f > 0.0F) && (f <= 0.07F))
                {
                    this.mTextPaint.setTextScaleX(1.0F - f - 0.005F);
                    post(new Runnable()
                    {
                        public void run()
                        {
                            TextView.this.requestLayout();
                        }
                    });
                    bool = true;
                }
            }
        }
    }

    private void convertFromViewportToContentCoordinates(Rect paramRect)
    {
        int i = viewportToContentHorizontalOffset();
        paramRect.left = (i + paramRect.left);
        paramRect.right = (i + paramRect.right);
        int j = viewportToContentVerticalOffset();
        paramRect.top = (j + paramRect.top);
        paramRect.bottom = (j + paramRect.bottom);
    }

    private void createEditorIfNeeded()
    {
        if (this.mEditor == null)
            this.mEditor = new Editor(this);
    }

    private static int desired(Layout paramLayout)
    {
        int i = paramLayout.getLineCount();
        CharSequence localCharSequence = paramLayout.getText();
        float f = 0.0F;
        int j = 0;
        if (j < i - 1)
            if (localCharSequence.charAt(-1 + paramLayout.getLineEnd(j)) == '\n');
        for (int m = -1; ; m = (int)FloatMath.ceil(f))
        {
            return m;
            j++;
            break;
            for (int k = 0; k < i; k++)
                f = Math.max(f, paramLayout.getLineWidth(k));
        }
    }

    // ERROR //
    private int doKeyDown(int paramInt, KeyEvent paramKeyEvent1, KeyEvent paramKeyEvent2)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 507	android/widget/TextView:isEnabled	()Z
        //     4: ifne +9 -> 13
        //     7: iconst_0
        //     8: istore 4
        //     10: iload 4
        //     12: ireturn
        //     13: iload_1
        //     14: lookupswitch	default:+42->56, 4:+281->295, 23:+233->247, 61:+253->267, 66:+120->134
        //     57: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     60: ifnull +330 -> 390
        //     63: aload_0
        //     64: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     67: getfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     70: ifnull +320 -> 390
        //     73: aload_0
        //     74: invokevirtual 1046	android/widget/TextView:resetErrorChangedFlag	()V
        //     77: iconst_1
        //     78: istore 8
        //     80: aload_3
        //     81: ifnull +257 -> 338
        //     84: aload_0
        //     85: invokevirtual 1049	android/widget/TextView:beginBatchEdit	()V
        //     88: aload_0
        //     89: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     92: getfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     95: aload_0
        //     96: aload_0
        //     97: getfield 319	android/widget/TextView:mText	Ljava/lang/CharSequence;
        //     100: checkcast 1051	android/text/Editable
        //     103: aload_3
        //     104: invokeinterface 1055 4 0
        //     109: istore 12
        //     111: aload_0
        //     112: invokevirtual 1058	android/widget/TextView:hideErrorIfUnchanged	()V
        //     115: iconst_0
        //     116: istore 8
        //     118: iload 12
        //     120: ifeq +214 -> 334
        //     123: aload_0
        //     124: invokevirtual 1061	android/widget/TextView:endBatchEdit	()V
        //     127: bipush 255
        //     129: istore 4
        //     131: goto -121 -> 10
        //     134: aload_2
        //     135: invokevirtual 1066	android/view/KeyEvent:hasNoModifiers	()Z
        //     138: ifeq -82 -> 56
        //     141: aload_0
        //     142: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     145: ifnull +65 -> 210
        //     148: aload_0
        //     149: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     152: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     155: ifnull +55 -> 210
        //     158: aload_0
        //     159: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     162: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     165: getfield 1070	android/widget/Editor$InputContentType:onEditorActionListener	Landroid/widget/TextView$OnEditorActionListener;
        //     168: ifnull +42 -> 210
        //     171: aload_0
        //     172: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     175: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     178: getfield 1070	android/widget/Editor$InputContentType:onEditorActionListener	Landroid/widget/TextView$OnEditorActionListener;
        //     181: aload_0
        //     182: iconst_0
        //     183: aload_2
        //     184: invokeinterface 1074 4 0
        //     189: ifeq +21 -> 210
        //     192: aload_0
        //     193: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     196: getfield 524	android/widget/Editor:mInputContentType	Landroid/widget/Editor$InputContentType;
        //     199: iconst_1
        //     200: putfield 1077	android/widget/Editor$InputContentType:enterDown	Z
        //     203: bipush 255
        //     205: istore 4
        //     207: goto -197 -> 10
        //     210: bipush 16
        //     212: aload_2
        //     213: invokevirtual 1080	android/view/KeyEvent:getFlags	()I
        //     216: iand
        //     217: ifne +10 -> 227
        //     220: aload_0
        //     221: invokespecial 1083	android/widget/TextView:shouldAdvanceFocusOnEnter	()Z
        //     224: ifeq -168 -> 56
        //     227: aload_0
        //     228: invokevirtual 1086	android/widget/TextView:hasOnClickListeners	()Z
        //     231: ifeq +9 -> 240
        //     234: iconst_0
        //     235: istore 4
        //     237: goto -227 -> 10
        //     240: bipush 255
        //     242: istore 4
        //     244: goto -234 -> 10
        //     247: aload_2
        //     248: invokevirtual 1066	android/view/KeyEvent:hasNoModifiers	()Z
        //     251: ifeq -195 -> 56
        //     254: aload_0
        //     255: invokespecial 1083	android/widget/TextView:shouldAdvanceFocusOnEnter	()Z
        //     258: ifeq -202 -> 56
        //     261: iconst_0
        //     262: istore 4
        //     264: goto -254 -> 10
        //     267: aload_2
        //     268: invokevirtual 1066	android/view/KeyEvent:hasNoModifiers	()Z
        //     271: ifne +11 -> 282
        //     274: aload_2
        //     275: iconst_1
        //     276: invokevirtual 1089	android/view/KeyEvent:hasModifiers	(I)Z
        //     279: ifeq -223 -> 56
        //     282: aload_0
        //     283: invokespecial 1092	android/widget/TextView:shouldAdvanceFocusOnTab	()Z
        //     286: ifeq -230 -> 56
        //     289: iconst_0
        //     290: istore 4
        //     292: goto -282 -> 10
        //     295: aload_0
        //     296: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     299: ifnull -243 -> 56
        //     302: aload_0
        //     303: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     306: getfield 1096	android/widget/Editor:mSelectionActionMode	Landroid/view/ActionMode;
        //     309: ifnull -253 -> 56
        //     312: aload_0
        //     313: invokevirtual 1099	android/widget/TextView:stopSelectionActionMode	()V
        //     316: bipush 255
        //     318: istore 4
        //     320: goto -310 -> 10
        //     323: astore 11
        //     325: aload_0
        //     326: invokevirtual 1061	android/widget/TextView:endBatchEdit	()V
        //     329: aload 11
        //     331: athrow
        //     332: astore 10
        //     334: aload_0
        //     335: invokevirtual 1061	android/widget/TextView:endBatchEdit	()V
        //     338: iload 8
        //     340: ifeq +50 -> 390
        //     343: aload_0
        //     344: invokevirtual 1049	android/widget/TextView:beginBatchEdit	()V
        //     347: aload_0
        //     348: getfield 515	android/widget/TextView:mEditor	Landroid/widget/Editor;
        //     351: getfield 588	android/widget/Editor:mKeyListener	Landroid/text/method/KeyListener;
        //     354: aload_0
        //     355: aload_0
        //     356: getfield 319	android/widget/TextView:mText	Ljava/lang/CharSequence;
        //     359: checkcast 1051	android/text/Editable
        //     362: iload_1
        //     363: aload_2
        //     364: invokeinterface 1103 5 0
        //     369: istore 9
        //     371: aload_0
        //     372: invokevirtual 1061	android/widget/TextView:endBatchEdit	()V
        //     375: aload_0
        //     376: invokevirtual 1058	android/widget/TextView:hideErrorIfUnchanged	()V
        //     379: iload 9
        //     381: ifeq +9 -> 390
        //     384: iconst_1
        //     385: istore 4
        //     387: goto -377 -> 10
        //     390: aload_0
        //     391: getfield 365	android/widget/TextView:mMovement	Landroid/text/method/MovementMethod;
        //     394: ifnull +87 -> 481
        //     397: aload_0
        //     398: getfield 803	android/widget/TextView:mLayout	Landroid/text/Layout;
        //     401: ifnull +80 -> 481
        //     404: iconst_1
        //     405: istore 5
        //     407: aload_3
        //     408: ifnull +40 -> 448
        //     411: aload_0
        //     412: getfield 365	android/widget/TextView:mMovement	Landroid/text/method/MovementMethod;
        //     415: aload_0
        //     416: aload_0
        //     417: getfield 319	android/widget/TextView:mText	Ljava/lang/CharSequence;
        //     420: checkcast 1105	android/text/Spannable
        //     423: aload_3
        //     424: invokeinterface 1110 4 0
        //     429: istore 7
        //     431: iconst_0
        //     432: istore 5
        //     434: iload 7
        //     436: ifeq +12 -> 448
        //     439: bipush 255
        //     441: istore 4
        //     443: goto -433 -> 10
        //     446: astore 6
        //     448: iload 5
        //     450: ifeq +31 -> 481
        //     453: aload_0
        //     454: getfield 365	android/widget/TextView:mMovement	Landroid/text/method/MovementMethod;
        //     457: aload_0
        //     458: aload_0
        //     459: getfield 319	android/widget/TextView:mText	Ljava/lang/CharSequence;
        //     462: checkcast 1105	android/text/Spannable
        //     465: iload_1
        //     466: aload_2
        //     467: invokeinterface 1113 5 0
        //     472: ifeq +9 -> 481
        //     475: iconst_2
        //     476: istore 4
        //     478: goto -468 -> 10
        //     481: iconst_0
        //     482: istore 4
        //     484: goto -474 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     84	115	323	finally
        //     84	115	332	java/lang/AbstractMethodError
        //     411	431	446	java/lang/AbstractMethodError
    }

    private void fixFocusableAndClickableSettings()
    {
        if ((this.mMovement != null) || ((this.mEditor != null) && (this.mEditor.mKeyListener != null)))
        {
            setFocusable(true);
            setClickable(true);
            setLongClickable(true);
        }
        while (true)
        {
            return;
            setFocusable(false);
            setClickable(false);
            setLongClickable(false);
        }
    }

    private int getBottomVerticalOffset(boolean paramBoolean)
    {
        int i = 0;
        int j = 0x70 & this.mGravity;
        Layout localLayout = this.mLayout;
        if ((!paramBoolean) && (this.mText.length() == 0) && (this.mHintLayout != null))
            localLayout = this.mHintLayout;
        int k;
        int m;
        if (j != 80)
        {
            if (localLayout != this.mHintLayout)
                break label104;
            k = getMeasuredHeight() - getCompoundPaddingTop() - getCompoundPaddingBottom();
            m = localLayout.getHeight();
            if (m < k)
                if (j != 48)
                    break label123;
        }
        label104: label123: for (i = k - m; ; i = k - m >> 1)
        {
            return i;
            k = getMeasuredHeight() - getExtendedPaddingTop() - getExtendedPaddingBottom();
            break;
        }
    }

    private int getDesiredHeight()
    {
        boolean bool = true;
        int i = getDesiredHeight(this.mLayout, bool);
        Layout localLayout = this.mHintLayout;
        if (this.mEllipsize != null);
        while (true)
        {
            return Math.max(i, getDesiredHeight(localLayout, bool));
            bool = false;
        }
    }

    private int getDesiredHeight(Layout paramLayout, boolean paramBoolean)
    {
        int n;
        if (paramLayout == null)
        {
            n = 0;
            return n;
        }
        int i = paramLayout.getLineCount();
        int j = getCompoundPaddingTop() + getCompoundPaddingBottom();
        int k = paramLayout.getLineTop(i);
        Drawables localDrawables = this.mDrawables;
        if (localDrawables != null)
            k = Math.max(Math.max(k, localDrawables.mDrawableHeightLeft), localDrawables.mDrawableHeightRight);
        int m = k + j;
        if (this.mMaxMode == 1)
        {
            if ((paramBoolean) && (i > this.mMaximum))
            {
                int i1 = paramLayout.getLineTop(this.mMaximum);
                if (localDrawables != null)
                    i1 = Math.max(Math.max(i1, localDrawables.mDrawableHeightLeft), localDrawables.mDrawableHeightRight);
                m = i1 + j;
                i = this.mMaximum;
            }
            label138: if (this.mMinMode != 1)
                break label198;
            if (i < this.mMinimum)
                m += getLineHeight() * (this.mMinimum - i);
        }
        while (true)
        {
            n = Math.max(m, getSuggestedMinimumHeight());
            break;
            m = Math.min(m, this.mMaximum);
            break label138;
            label198: m = Math.max(m, this.mMinimum);
        }
    }

    private void getInterestingRect(Rect paramRect, int paramInt)
    {
        convertFromViewportToContentCoordinates(paramRect);
        if (paramInt == 0)
            paramRect.top -= getExtendedPaddingTop();
        if (paramInt == -1 + this.mLayout.getLineCount())
            paramRect.bottom += getExtendedPaddingBottom();
    }

    private Layout.Alignment getLayoutAlignment()
    {
        if (this.mLayoutAlignment == null);
        switch (getResolvedTextAlignment())
        {
        default:
            this.mLayoutAlignment = Layout.Alignment.ALIGN_NORMAL;
        case 1:
        case 2:
        case 3:
        case 4:
            while (true)
            {
                return this.mLayoutAlignment;
                switch (0x800007 & this.mGravity)
                {
                default:
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_NORMAL;
                    break;
                case 8388611:
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_NORMAL;
                    break;
                case 8388613:
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_OPPOSITE;
                    break;
                case 3:
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_LEFT;
                    break;
                case 5:
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_RIGHT;
                    break;
                case 1:
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_CENTER;
                    continue;
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_NORMAL;
                    continue;
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_OPPOSITE;
                    continue;
                    this.mLayoutAlignment = Layout.Alignment.ALIGN_CENTER;
                }
            }
        case 5:
            if (getResolvedLayoutDirection() == 1);
            for (Layout.Alignment localAlignment2 = Layout.Alignment.ALIGN_RIGHT; ; localAlignment2 = Layout.Alignment.ALIGN_LEFT)
            {
                this.mLayoutAlignment = localAlignment2;
                break;
            }
        case 6:
        }
        if (getResolvedLayoutDirection() == 1);
        for (Layout.Alignment localAlignment1 = Layout.Alignment.ALIGN_LEFT; ; localAlignment1 = Layout.Alignment.ALIGN_RIGHT)
        {
            this.mLayoutAlignment = localAlignment1;
            break;
        }
    }

    private int getOffsetAtCoordinate(int paramInt, float paramFloat)
    {
        float f = convertToLocalHorizontalCoordinate(paramFloat);
        return getLayout().getOffsetForHorizontal(paramInt, f);
    }

    public static int getTextColor(Context paramContext, TypedArray paramTypedArray, int paramInt)
    {
        ColorStateList localColorStateList = getTextColors(paramContext, paramTypedArray);
        if (localColorStateList == null);
        while (true)
        {
            return paramInt;
            paramInt = localColorStateList.getDefaultColor();
        }
    }

    public static ColorStateList getTextColors(Context paramContext, TypedArray paramTypedArray)
    {
        ColorStateList localColorStateList = paramTypedArray.getColorStateList(5);
        if (localColorStateList == null)
        {
            int i = paramTypedArray.getResourceId(1, -1);
            if (i != -1)
            {
                TypedArray localTypedArray = paramContext.obtainStyledAttributes(i, R.styleable.TextAppearance);
                localColorStateList = localTypedArray.getColorStateList(3);
                localTypedArray.recycle();
            }
        }
        return localColorStateList;
    }

    private Path getUpdatedHighlightPath()
    {
        Path localPath = null;
        Paint localPaint = this.mHighlightPaint;
        int i = getSelectionStart();
        int j = getSelectionEnd();
        if ((this.mMovement != null) && ((isFocused()) || (isPressed())) && (i >= 0))
        {
            if (i != j)
                break label170;
            if ((this.mEditor != null) && (this.mEditor.isCursorVisible()) && ((SystemClock.uptimeMillis() - this.mEditor.mShowCursor) % 1000L < 500L))
            {
                if (this.mHighlightPathBogus)
                {
                    if (this.mHighlightPath == null)
                        this.mHighlightPath = new Path();
                    this.mHighlightPath.reset();
                    this.mLayout.getCursorPath(i, this.mHighlightPath, this.mText);
                    this.mEditor.updateCursorsPositions();
                    this.mHighlightPathBogus = false;
                }
                localPaint.setColor(this.mCurTextColor);
                localPaint.setStyle(Paint.Style.STROKE);
            }
        }
        for (localPath = this.mHighlightPath; ; localPath = this.mHighlightPath)
        {
            return localPath;
            label170: if (this.mHighlightPathBogus)
            {
                if (this.mHighlightPath == null)
                    this.mHighlightPath = new Path();
                this.mHighlightPath.reset();
                this.mLayout.getSelectionPath(i, j, this.mHighlightPath);
                this.mHighlightPathBogus = false;
            }
            localPaint.setColor(this.mHighlightColor);
            localPaint.setStyle(Paint.Style.FILL);
        }
    }

    private boolean hasPasswordTransformationMethod()
    {
        return this.mTransformation instanceof PasswordTransformationMethod;
    }

    private void invalidateCursor(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((paramInt1 >= 0) || (paramInt2 >= 0) || (paramInt3 >= 0))
            invalidateRegion(Math.min(Math.min(paramInt1, paramInt2), paramInt3), Math.max(Math.max(paramInt1, paramInt2), paramInt3), true);
    }

    private static boolean isMultilineInputType(int paramInt)
    {
        if ((0x2000F & paramInt) == 131073);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isPasswordInputType(int paramInt)
    {
        int i = paramInt & 0xFFF;
        if ((i == 129) || (i == 225) || (i == 18));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isShowingHint()
    {
        if ((TextUtils.isEmpty(this.mText)) && (!TextUtils.isEmpty(this.mHint)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isVisiblePasswordInputType(int paramInt)
    {
        if ((paramInt & 0xFFF) == 145);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private Layout makeSingleLayout(int paramInt1, BoringLayout.Metrics paramMetrics, int paramInt2, Layout.Alignment paramAlignment, boolean paramBoolean1, TextUtils.TruncateAt paramTruncateAt, boolean paramBoolean2)
    {
        TextUtils.TruncateAt localTruncateAt;
        Object localObject;
        if ((this.mText instanceof Spannable))
        {
            CharSequence localCharSequence7 = this.mText;
            CharSequence localCharSequence8 = this.mTransformed;
            TextPaint localTextPaint7 = this.mTextPaint;
            TextDirectionHeuristic localTextDirectionHeuristic3 = this.mTextDir;
            float f13 = this.mSpacingMult;
            float f14 = this.mSpacingAdd;
            boolean bool7 = this.mIncludePad;
            if (getKeyListener() == null)
            {
                localTruncateAt = paramTruncateAt;
                localObject = new DynamicLayout(localCharSequence7, localCharSequence8, localTextPaint7, paramInt1, paramAlignment, localTextDirectionHeuristic3, f13, f14, bool7, localTruncateAt, paramInt2);
            }
        }
        while (true)
        {
            return localObject;
            localTruncateAt = null;
            break;
            BoringLayout.Metrics localMetrics = UNKNOWN_BORING;
            if (paramMetrics == localMetrics)
            {
                paramMetrics = BoringLayout.isBoring(this.mTransformed, this.mTextPaint, this.mTextDir, this.mBoring);
                if (paramMetrics != null)
                    this.mBoring = paramMetrics;
            }
            if (paramMetrics != null)
            {
                if ((paramMetrics.width <= paramInt1) && ((paramTruncateAt == null) || (paramMetrics.width <= paramInt2)))
                {
                    BoringLayout localBoringLayout2;
                    CharSequence localCharSequence6;
                    TextPaint localTextPaint6;
                    float f11;
                    float f12;
                    boolean bool6;
                    if ((paramBoolean2) && (this.mSavedLayout != null))
                    {
                        localBoringLayout2 = this.mSavedLayout;
                        localCharSequence6 = this.mTransformed;
                        localTextPaint6 = this.mTextPaint;
                        f11 = this.mSpacingMult;
                        f12 = this.mSpacingAdd;
                        bool6 = this.mIncludePad;
                    }
                    CharSequence localCharSequence5;
                    TextPaint localTextPaint5;
                    float f9;
                    float f10;
                    boolean bool5;
                    for (localObject = localBoringLayout2.replaceOrMake(localCharSequence6, localTextPaint6, paramInt1, paramAlignment, f11, f12, paramMetrics, bool6); ; localObject = BoringLayout.make(localCharSequence5, localTextPaint5, paramInt1, paramAlignment, f9, f10, paramMetrics, bool5))
                    {
                        if (!paramBoolean2)
                            break label302;
                        this.mSavedLayout = ((BoringLayout)localObject);
                        break;
                        localCharSequence5 = this.mTransformed;
                        localTextPaint5 = this.mTextPaint;
                        f9 = this.mSpacingMult;
                        f10 = this.mSpacingAdd;
                        bool5 = this.mIncludePad;
                    }
                }
                else
                {
                    label302: if ((paramBoolean1) && (paramMetrics.width <= paramInt1))
                    {
                        if ((paramBoolean2) && (this.mSavedLayout != null))
                        {
                            BoringLayout localBoringLayout1 = this.mSavedLayout;
                            CharSequence localCharSequence4 = this.mTransformed;
                            TextPaint localTextPaint4 = this.mTextPaint;
                            float f7 = this.mSpacingMult;
                            float f8 = this.mSpacingAdd;
                            boolean bool4 = this.mIncludePad;
                            localObject = localBoringLayout1.replaceOrMake(localCharSequence4, localTextPaint4, paramInt1, paramAlignment, f7, f8, paramMetrics, bool4, paramTruncateAt, paramInt2);
                        }
                        else
                        {
                            CharSequence localCharSequence3 = this.mTransformed;
                            TextPaint localTextPaint3 = this.mTextPaint;
                            float f5 = this.mSpacingMult;
                            float f6 = this.mSpacingAdd;
                            boolean bool3 = this.mIncludePad;
                            localObject = BoringLayout.make(localCharSequence3, localTextPaint3, paramInt1, paramAlignment, f5, f6, paramMetrics, bool3, paramTruncateAt, paramInt2);
                        }
                    }
                    else
                    {
                        if (paramBoolean1)
                        {
                            CharSequence localCharSequence2 = this.mTransformed;
                            int k = this.mTransformed.length();
                            TextPaint localTextPaint2 = this.mTextPaint;
                            TextDirectionHeuristic localTextDirectionHeuristic2 = this.mTextDir;
                            float f3 = this.mSpacingMult;
                            float f4 = this.mSpacingAdd;
                            boolean bool2 = this.mIncludePad;
                            if (this.mMaxMode == 1);
                            for (int m = this.mMaximum; ; m = 2147483647)
                            {
                                localObject = new StaticLayout(localCharSequence2, 0, k, localTextPaint2, paramInt1, paramAlignment, localTextDirectionHeuristic2, f3, f4, bool2, paramTruncateAt, paramInt2, m);
                                break;
                            }
                        }
                        localObject = new StaticLayout(this.mTransformed, this.mTextPaint, paramInt1, paramAlignment, this.mTextDir, this.mSpacingMult, this.mSpacingAdd, this.mIncludePad);
                    }
                }
            }
            else
            {
                if (paramBoolean1)
                {
                    CharSequence localCharSequence1 = this.mTransformed;
                    int i = this.mTransformed.length();
                    TextPaint localTextPaint1 = this.mTextPaint;
                    TextDirectionHeuristic localTextDirectionHeuristic1 = this.mTextDir;
                    float f1 = this.mSpacingMult;
                    float f2 = this.mSpacingAdd;
                    boolean bool1 = this.mIncludePad;
                    if (this.mMaxMode == 1);
                    for (int j = this.mMaximum; ; j = 2147483647)
                    {
                        localObject = new StaticLayout(localCharSequence1, 0, i, localTextPaint1, paramInt1, paramAlignment, localTextDirectionHeuristic1, f1, f2, bool1, paramTruncateAt, paramInt2, j);
                        break;
                    }
                }
                localObject = new StaticLayout(this.mTransformed, this.mTextPaint, paramInt1, paramAlignment, this.mTextDir, this.mSpacingMult, this.mSpacingAdd, this.mIncludePad);
            }
        }
    }

    private void nullLayouts()
    {
        if (((this.mLayout instanceof BoringLayout)) && (this.mSavedLayout == null))
            this.mSavedLayout = ((BoringLayout)this.mLayout);
        if (((this.mHintLayout instanceof BoringLayout)) && (this.mSavedHintLayout == null))
            this.mSavedHintLayout = ((BoringLayout)this.mHintLayout);
        this.mHintLayout = null;
        this.mLayout = null;
        this.mSavedMarqueeModeLayout = null;
        this.mHintBoring = null;
        this.mBoring = null;
        if (this.mEditor != null)
            this.mEditor.prepareCursorControllers();
    }

    private void paste(int paramInt1, int paramInt2)
    {
        ClipData localClipData = ((ClipboardManager)getContext().getSystemService("clipboard")).getPrimaryClip();
        if (localClipData != null)
        {
            int i = 0;
            int j = 0;
            if (j < localClipData.getItemCount())
            {
                CharSequence localCharSequence = localClipData.getItemAt(j).coerceToStyledText(getContext());
                if (localCharSequence != null)
                {
                    if (i != 0)
                        break label120;
                    long l = prepareSpacesAroundPaste(paramInt1, paramInt2, localCharSequence);
                    paramInt1 = TextUtils.unpackRangeStartFromLong(l);
                    paramInt2 = TextUtils.unpackRangeEndFromLong(l);
                    Selection.setSelection((Spannable)this.mText, paramInt2);
                    ((Editable)this.mText).replace(paramInt1, paramInt2, localCharSequence);
                    i = 1;
                }
                while (true)
                {
                    j++;
                    break;
                    label120: ((Editable)this.mText).insert(getSelectionEnd(), "\n");
                    ((Editable)this.mText).insert(getSelectionEnd(), localCharSequence);
                }
            }
            stopSelectionActionMode();
            LAST_CUT_OR_COPY_TIME = 0L;
        }
    }

    private void registerForPreDraw()
    {
        if (!this.mPreDrawRegistered)
        {
            getViewTreeObserver().addOnPreDrawListener(this);
            this.mPreDrawRegistered = true;
        }
    }

    private <T> void removeIntersectingSpans(int paramInt1, int paramInt2, Class<T> paramClass)
    {
        if (!(this.mText instanceof Editable));
        label99: 
        while (true)
        {
            return;
            Editable localEditable = (Editable)this.mText;
            Object[] arrayOfObject = localEditable.getSpans(paramInt1, paramInt2, paramClass);
            int i = arrayOfObject.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label99;
                int k = localEditable.getSpanStart(arrayOfObject[j]);
                if ((localEditable.getSpanEnd(arrayOfObject[j]) == paramInt1) || (k == paramInt2))
                    break;
                localEditable.removeSpan(arrayOfObject[j]);
            }
        }
    }

    static void removeParcelableSpans(Spannable paramSpannable, int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = paramSpannable.getSpans(paramInt1, paramInt2, ParcelableSpan.class);
        int i = arrayOfObject.length;
        while (i > 0)
        {
            i--;
            paramSpannable.removeSpan(arrayOfObject[i]);
        }
    }

    private void restartMarqueeIfNeeded()
    {
        if ((this.mRestartMarquee) && (this.mEllipsize == TextUtils.TruncateAt.MARQUEE))
        {
            this.mRestartMarquee = false;
            startMarquee();
        }
    }

    private void sendBeforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mListeners != null)
        {
            ArrayList localArrayList = this.mListeners;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((TextWatcher)localArrayList.get(j)).beforeTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
        }
        removeIntersectingSpans(paramInt1, paramInt1 + paramInt2, SpellCheckSpan.class);
        removeIntersectingSpans(paramInt1, paramInt1 + paramInt2, SuggestionSpan.class);
    }

    private void setFilters(Editable paramEditable, InputFilter[] paramArrayOfInputFilter)
    {
        if ((this.mEditor != null) && ((this.mEditor.mKeyListener instanceof InputFilter)))
        {
            InputFilter[] arrayOfInputFilter = new InputFilter[1 + paramArrayOfInputFilter.length];
            System.arraycopy(paramArrayOfInputFilter, 0, arrayOfInputFilter, 0, paramArrayOfInputFilter.length);
            arrayOfInputFilter[paramArrayOfInputFilter.length] = ((InputFilter)this.mEditor.mKeyListener);
            paramEditable.setFilters(arrayOfInputFilter);
        }
        while (true)
        {
            return;
            paramEditable.setFilters(paramArrayOfInputFilter);
        }
    }

    private void setInputType(int paramInt, boolean paramBoolean)
    {
        int i = 1;
        int j = paramInt & 0xF;
        TextKeyListener.Capitalize localCapitalize;
        label38: Object localObject;
        if (j == i)
            if ((0x8000 & paramInt) != 0)
            {
                int n = i;
                if ((paramInt & 0x1000) == 0)
                    break label76;
                localCapitalize = TextKeyListener.Capitalize.CHARACTERS;
                localObject = TextKeyListener.getInstance(n, localCapitalize);
                label47: setRawInputType(paramInt);
                if (!paramBoolean)
                    break label246;
                createEditorIfNeeded();
                this.mEditor.mKeyListener = ((KeyListener)localObject);
            }
        while (true)
        {
            return;
            int i1 = 0;
            break;
            label76: if ((paramInt & 0x2000) != 0)
            {
                localCapitalize = TextKeyListener.Capitalize.WORDS;
                break label38;
            }
            if ((paramInt & 0x4000) != 0)
            {
                localCapitalize = TextKeyListener.Capitalize.SENTENCES;
                break label38;
            }
            localCapitalize = TextKeyListener.Capitalize.NONE;
            break label38;
            if (j == 2)
            {
                int k;
                if ((paramInt & 0x1000) != 0)
                {
                    k = i;
                    label133: if ((paramInt & 0x2000) == 0)
                        break label158;
                }
                while (true)
                {
                    localObject = DigitsKeyListener.getInstance(k, i);
                    break;
                    int m = 0;
                    break label133;
                    label158: i = 0;
                }
            }
            if (j == 4)
                switch (paramInt & 0xFF0)
                {
                default:
                    localObject = DateTimeKeyListener.getInstance();
                    break;
                case 16:
                    localObject = DateKeyListener.getInstance();
                    break;
                case 32:
                    localObject = TimeKeyListener.getInstance();
                    break;
                }
            if (j == 3)
            {
                localObject = DialerKeyListener.getInstance();
                break label47;
            }
            localObject = TextKeyListener.getInstance();
            break label47;
            label246: setKeyListenerOnly((KeyListener)localObject);
        }
    }

    private void setInputTypeSingleLine(boolean paramBoolean)
    {
        Editor localEditor2;
        if ((this.mEditor != null) && ((0xF & this.mEditor.mInputType) == 1))
        {
            if (!paramBoolean)
                break label43;
            localEditor2 = this.mEditor;
        }
        label43: Editor localEditor1;
        for (localEditor2.mInputType = (0xFFFDFFFF & localEditor2.mInputType); ; localEditor1.mInputType = (0x20000 | localEditor1.mInputType))
        {
            return;
            localEditor1 = this.mEditor;
        }
    }

    private void setKeyListenerOnly(KeyListener paramKeyListener)
    {
        if ((this.mEditor == null) && (paramKeyListener == null));
        while (true)
        {
            return;
            createEditorIfNeeded();
            if (this.mEditor.mKeyListener != paramKeyListener)
            {
                this.mEditor.mKeyListener = paramKeyListener;
                if ((paramKeyListener != null) && (!(this.mText instanceof Editable)))
                    setText(this.mText);
                setFilters((Editable)this.mText, this.mFilters);
            }
        }
    }

    private void setPrimaryClip(ClipData paramClipData)
    {
        ((ClipboardManager)getContext().getSystemService("clipboard")).setPrimaryClip(paramClipData);
        LAST_CUT_OR_COPY_TIME = SystemClock.uptimeMillis();
    }

    private void setRawTextSize(float paramFloat)
    {
        if (paramFloat != this.mTextPaint.getTextSize())
        {
            this.mTextPaint.setTextSize(paramFloat);
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    private void setRelativeDrawablesIfNeeded(Drawable paramDrawable1, Drawable paramDrawable2)
    {
        int i;
        Drawables localDrawables;
        if ((paramDrawable1 != null) || (paramDrawable2 != null))
        {
            i = 1;
            if (i != 0)
            {
                localDrawables = this.mDrawables;
                if (localDrawables == null)
                {
                    localDrawables = new Drawables();
                    this.mDrawables = localDrawables;
                }
                Rect localRect = localDrawables.mCompoundRect;
                int[] arrayOfInt = getDrawableState();
                if (paramDrawable1 == null)
                    break label183;
                paramDrawable1.setBounds(0, 0, paramDrawable1.getIntrinsicWidth(), paramDrawable1.getIntrinsicHeight());
                paramDrawable1.setState(arrayOfInt);
                paramDrawable1.copyBounds(localRect);
                paramDrawable1.setCallback(this);
                localDrawables.mDrawableStart = paramDrawable1;
                localDrawables.mDrawableSizeStart = localRect.width();
                localDrawables.mDrawableHeightStart = localRect.height();
                label115: if (paramDrawable2 == null)
                    break label198;
                paramDrawable2.setBounds(0, 0, paramDrawable2.getIntrinsicWidth(), paramDrawable2.getIntrinsicHeight());
                paramDrawable2.setState(arrayOfInt);
                paramDrawable2.copyBounds(localRect);
                paramDrawable2.setCallback(this);
                localDrawables.mDrawableEnd = paramDrawable2;
                localDrawables.mDrawableSizeEnd = localRect.width();
                localDrawables.mDrawableHeightEnd = localRect.height();
            }
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label183: localDrawables.mDrawableHeightStart = 0;
            localDrawables.mDrawableSizeStart = 0;
            break label115;
            label198: localDrawables.mDrawableHeightEnd = 0;
            localDrawables.mDrawableSizeEnd = 0;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void setText(CharSequence paramCharSequence, BufferType paramBufferType, boolean paramBoolean, int paramInt)
    {
        if (paramCharSequence == null)
            paramCharSequence = "";
        if (!isSuggestionsEnabled())
            paramCharSequence = removeSuggestionSpans(paramCharSequence);
        CharSequence localCharSequence1 = this.mText;
        int i;
        if (!paramCharSequence.equals(localCharSequence1))
        {
            i = 1;
            if (!this.mUserSetTextScaleX)
                this.mTextPaint.setTextScaleX(1.0F);
            if (((paramCharSequence instanceof Spanned)) && (((Spanned)paramCharSequence).getSpanStart(TextUtils.TruncateAt.MARQUEE) >= 0))
            {
                if (!ViewConfiguration.get(this.mContext).isFadingMarqueeEnabled())
                    break label181;
                setHorizontalFadingEdgeEnabled(true);
            }
        }
        for (this.mMarqueeFadeMode = 0; ; this.mMarqueeFadeMode = 1)
        {
            setEllipsize(TextUtils.TruncateAt.MARQUEE);
            int j = this.mFilters.length;
            for (int k = 0; k < j; k++)
            {
                InputFilter localInputFilter = this.mFilters[k];
                int i5 = paramCharSequence.length();
                Spanned localSpanned = EMPTY_SPANNED;
                CharSequence localCharSequence3 = localInputFilter.filter(paramCharSequence, 0, i5, localSpanned, 0, 0);
                if (localCharSequence3 != null)
                    paramCharSequence = localCharSequence3;
            }
            i = 0;
            break;
            label181: setHorizontalFadingEdgeEnabled(false);
        }
        int m;
        label330: Spannable localSpannable2;
        if (paramBoolean)
        {
            if (this.mText != null)
            {
                paramInt = this.mText.length();
                CharSequence localCharSequence2 = this.mText;
                int i4 = paramCharSequence.length();
                sendBeforeTextChanged(localCharSequence2, 0, paramInt, i4);
            }
        }
        else
        {
            m = 0;
            if ((this.mListeners != null) && (this.mListeners.size() != 0))
                m = 1;
            BufferType localBufferType1 = BufferType.EDITABLE;
            if ((paramBufferType != localBufferType1) && (getKeyListener() == null) && (m == 0))
                break label543;
            createEditorIfNeeded();
            Editable localEditable = this.mEditableFactory.newEditable(paramCharSequence);
            paramCharSequence = localEditable;
            setFilters(localEditable, this.mFilters);
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if (localInputMethodManager != null)
                localInputMethodManager.restartInput(this);
            if (this.mAutoLinkMask != 0)
            {
                BufferType localBufferType2 = BufferType.EDITABLE;
                if ((paramBufferType != localBufferType2) && (!(paramCharSequence instanceof Spannable)))
                    break label588;
                localSpannable2 = (Spannable)paramCharSequence;
                label361: int i3 = this.mAutoLinkMask;
                if (Linkify.addLinks(localSpannable2, i3))
                {
                    paramCharSequence = localSpannable2;
                    BufferType localBufferType3 = BufferType.EDITABLE;
                    if (paramBufferType != localBufferType3)
                        break label601;
                    paramBufferType = BufferType.EDITABLE;
                    label395: this.mText = paramCharSequence;
                    if ((this.mLinksClickable) && (!textCanBeSelected()))
                        setMovementMethod(LinkMovementMethod.getInstance());
                }
            }
            this.mBufferType = paramBufferType;
            this.mText = paramCharSequence;
            if (this.mTransformation != null)
                break label608;
        }
        int n;
        Spannable localSpannable1;
        label543: label588: label601: label608: for (this.mTransformed = paramCharSequence; ; this.mTransformed = this.mTransformation.getTransformation(paramCharSequence, this))
        {
            n = paramCharSequence.length();
            if ((!(paramCharSequence instanceof Spannable)) || (this.mAllowTransformationLengthChange))
                break label738;
            localSpannable1 = (Spannable)paramCharSequence;
            ChangeWatcher[] arrayOfChangeWatcher = (ChangeWatcher[])localSpannable1.getSpans(0, localSpannable1.length(), ChangeWatcher.class);
            int i1 = arrayOfChangeWatcher.length;
            for (int i2 = 0; i2 < i1; i2++)
                localSpannable1.removeSpan(arrayOfChangeWatcher[i2]);
            sendBeforeTextChanged("", 0, 0, paramCharSequence.length());
            break;
            BufferType localBufferType4 = BufferType.SPANNABLE;
            if ((paramBufferType == localBufferType4) || (this.mMovement != null))
            {
                paramCharSequence = this.mSpannableFactory.newSpannable(paramCharSequence);
                break label330;
            }
            if ((paramCharSequence instanceof CharWrapper))
                break label330;
            paramCharSequence = TextUtils.stringOrSpannedString(paramCharSequence);
            break label330;
            localSpannable2 = this.mSpannableFactory.newSpannable(paramCharSequence);
            break label361;
            paramBufferType = BufferType.SPANNABLE;
            break label395;
        }
        if (this.mChangeWatcher == null)
            this.mChangeWatcher = new ChangeWatcher(null);
        localSpannable1.setSpan(this.mChangeWatcher, 0, n, 6553618);
        if (this.mEditor != null)
            this.mEditor.addSpanWatchers(localSpannable1);
        if (this.mTransformation != null)
            localSpannable1.setSpan(this.mTransformation, 0, n, 18);
        if (this.mMovement != null)
        {
            this.mMovement.initialize(this, (Spannable)paramCharSequence);
            if (this.mEditor != null)
                this.mEditor.mSelectionMoved = false;
        }
        label738: if (this.mLayout != null)
            checkForRelayout();
        if (i != 0)
            sendOnTextChanged(paramCharSequence, 0, paramInt, n);
        onTextChanged(paramCharSequence, 0, paramInt, n);
        if (m != 0)
            sendAfterTextChanged((Editable)paramCharSequence);
        if (this.mEditor != null)
            this.mEditor.prepareCursorControllers();
    }

    private void setTypefaceFromAttrs(String paramString, int paramInt1, int paramInt2)
    {
        Typeface localTypeface = null;
        if (paramString != null)
        {
            localTypeface = Typeface.create(paramString, paramInt2);
            if (localTypeface != null)
            {
                setTypeface(localTypeface);
                return;
            }
        }
        switch (paramInt1)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            setTypeface(localTypeface, paramInt2);
            break;
            localTypeface = Typeface.SANS_SERIF;
            continue;
            localTypeface = Typeface.SERIF;
            continue;
            localTypeface = Typeface.MONOSPACE;
        }
    }

    private boolean shouldAdvanceFocusOnEnter()
    {
        boolean bool = false;
        if (getKeyListener() == null);
        while (true)
        {
            return bool;
            if (this.mSingleLine)
            {
                bool = true;
            }
            else if ((this.mEditor != null) && ((0xF & this.mEditor.mInputType) == 1))
            {
                int i = 0xFF0 & this.mEditor.mInputType;
                if ((i == 32) || (i == 48))
                    bool = true;
            }
        }
    }

    private boolean shouldAdvanceFocusOnTab()
    {
        int i = 1;
        if ((getKeyListener() != null) && (!this.mSingleLine) && (this.mEditor != null) && ((0xF & this.mEditor.mInputType) == i))
        {
            int j = 0xFF0 & this.mEditor.mInputType;
            if ((j == 262144) || (j == 131072))
                i = 0;
        }
        return i;
    }

    private boolean shouldSpeakPasswordsForAccessibility()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "speak_password", 0) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void startMarquee()
    {
        if (getKeyListener() != null);
        while (true)
        {
            return;
            if ((!compressText(getWidth() - getCompoundPaddingLeft() - getCompoundPaddingRight())) && ((this.mMarquee == null) || (this.mMarquee.isStopped())) && ((isFocused()) || (isSelected())) && (getLineCount() == 1) && (canMarquee()))
            {
                if (this.mMarqueeFadeMode == 1)
                {
                    this.mMarqueeFadeMode = 2;
                    Layout localLayout = this.mLayout;
                    this.mLayout = this.mSavedMarqueeModeLayout;
                    this.mSavedMarqueeModeLayout = localLayout;
                    setHorizontalFadingEdgeEnabled(true);
                    requestLayout();
                    invalidate();
                }
                if (this.mMarquee == null)
                    this.mMarquee = new Marquee(this);
                this.mMarquee.start(this.mMarqueeRepeatLimit);
            }
        }
    }

    private void startStopMarquee(boolean paramBoolean)
    {
        if (this.mEllipsize == TextUtils.TruncateAt.MARQUEE)
        {
            if (!paramBoolean)
                break label19;
            startMarquee();
        }
        while (true)
        {
            return;
            label19: stopMarquee();
        }
    }

    private void stopMarquee()
    {
        if ((this.mMarquee != null) && (!this.mMarquee.isStopped()))
            this.mMarquee.stop();
        if (this.mMarqueeFadeMode == 2)
        {
            this.mMarqueeFadeMode = 1;
            Layout localLayout = this.mSavedMarqueeModeLayout;
            this.mSavedMarqueeModeLayout = this.mLayout;
            this.mLayout = localLayout;
            setHorizontalFadingEdgeEnabled(false);
            requestLayout();
            invalidate();
        }
    }

    private void updateTextColors()
    {
        int i = 0;
        int j = this.mTextColor.getColorForState(getDrawableState(), 0);
        if (j != this.mCurTextColor)
        {
            this.mCurTextColor = j;
            i = 1;
        }
        if (this.mLinkTextColor != null)
        {
            int m = this.mLinkTextColor.getColorForState(getDrawableState(), 0);
            if (m != this.mTextPaint.linkColor)
            {
                this.mTextPaint.linkColor = m;
                i = 1;
            }
        }
        if (this.mHintTextColor != null)
        {
            int k = this.mHintTextColor.getColorForState(getDrawableState(), 0);
            if ((k != this.mCurHintTextColor) && (this.mText.length() == 0))
            {
                this.mCurHintTextColor = k;
                i = 1;
            }
        }
        if (i != 0)
        {
            if (this.mEditor != null)
                this.mEditor.invalidateTextDisplayList();
            invalidate();
        }
    }

    public void addTextChangedListener(TextWatcher paramTextWatcher)
    {
        if (this.mListeners == null)
            this.mListeners = new ArrayList();
        this.mListeners.add(paramTextWatcher);
    }

    public final void append(CharSequence paramCharSequence)
    {
        append(paramCharSequence, 0, paramCharSequence.length());
    }

    public void append(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        if (!(this.mText instanceof Editable))
            setText(this.mText, BufferType.EDITABLE);
        ((Editable)this.mText).append(paramCharSequence, paramInt1, paramInt2);
    }

    public void beginBatchEdit()
    {
        if (this.mEditor != null)
            this.mEditor.beginBatchEdit();
    }

    public boolean bringPointIntoView(int paramInt)
    {
        boolean bool1 = false;
        if (isShowingHint());
        boolean bool2;
        for (Layout localLayout = this.mHintLayout; localLayout == null; localLayout = this.mLayout)
        {
            bool2 = false;
            return bool2;
        }
        int i = localLayout.getLineForOffset(paramInt);
        int j = (int)localLayout.getPrimaryHorizontal(paramInt);
        int k = localLayout.getLineTop(i);
        int m = i + 1;
        int n = localLayout.getLineTop(m);
        int i1 = (int)FloatMath.floor(localLayout.getLineLeft(i));
        int i2 = (int)FloatMath.ceil(localLayout.getLineRight(i));
        int i3 = localLayout.getHeight();
        int i4;
        label147: int i5;
        int i7;
        int i10;
        int i11;
        switch (3.$SwitchMap$android$text$Layout$Alignment[localLayout.getParagraphAlignment(i).ordinal()])
        {
        default:
            i4 = 0;
            i5 = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
            int i6 = this.mBottom - this.mTop - getExtendedPaddingTop() - getExtendedPaddingBottom();
            i7 = (n - k) / 2;
            int i8 = i7;
            int i9 = i6 / 4;
            if (i8 > i9)
                i8 = i6 / 4;
            if (i7 > i5 / 4)
                i7 = i5 / 4;
            i10 = this.mScrollX;
            i11 = this.mScrollY;
            if (k - i11 < i8)
                i11 = k - i8;
            if (n - i11 > i6 - i8)
                i11 = n - (i6 - i8);
            if (i3 - i11 < i6)
                i11 = i3 - i6;
            if (0 - i11 > 0)
                i11 = 0;
            if (i4 != 0)
            {
                if (j - i10 < i7)
                    i10 = j - i7;
                if (j - i10 > i5 - i7)
                    i10 = j - (i5 - i7);
            }
            if (i4 < 0)
            {
                if (i1 - i10 > 0)
                    i10 = i1;
                if (i2 - i10 < i5)
                    i10 = i2 - i5;
            }
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            if (i10 == this.mScrollX)
            {
                int i14 = this.mScrollY;
                if (i11 == i14);
            }
            else
            {
                if (this.mScroller != null)
                    break label736;
                scrollTo(i10, i11);
                bool1 = true;
            }
            if (isFocused())
            {
                if (this.mTempRect == null)
                    this.mTempRect = new Rect();
                this.mTempRect.set(j - 2, k, j + 2, n);
                getInterestingRect(this.mTempRect, i);
                this.mTempRect.offset(this.mScrollX, this.mScrollY);
                if (requestRectangleOnScreen(this.mTempRect))
                    bool1 = true;
            }
            bool2 = bool1;
            break;
            i4 = 1;
            break label147;
            i4 = -1;
            break label147;
            i4 = localLayout.getParagraphDirection(i);
            break label147;
            i4 = -localLayout.getParagraphDirection(i);
            break label147;
            if (i4 > 0)
            {
                if (i2 - i10 < i5)
                    i10 = i2 - i5;
                if (i1 - i10 > 0)
                    i10 = i1;
            }
            else if (i2 - i1 <= i5)
            {
                i10 = i1 - (i5 - (i2 - i1)) / 2;
            }
            else if (j > i2 - i7)
            {
                i10 = i2 - i5;
            }
            else if (j < i1 + i7)
            {
                i10 = i1;
            }
            else if (i1 > i10)
            {
                i10 = i1;
            }
            else if (i2 < i10 + i5)
            {
                i10 = i2 - i5;
            }
            else
            {
                if (j - i10 < i7)
                    i10 = j - i7;
                if (j - i10 > i5 - i7)
                    i10 = j - (i5 - i7);
            }
        }
        label736: long l = AnimationUtils.currentAnimationTimeMillis() - this.mLastScroll;
        int i12 = i10 - this.mScrollX;
        int i13 = i11 - this.mScrollY;
        if (l > 250L)
        {
            this.mScroller.startScroll(this.mScrollX, this.mScrollY, i12, i13);
            awakenScrollBars(this.mScroller.getDuration());
            invalidate();
        }
        while (true)
        {
            this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
            break;
            if (!this.mScroller.isFinished())
                this.mScroller.abortAnimation();
            scrollBy(i12, i13);
        }
    }

    boolean canCopy()
    {
        boolean bool = false;
        if (hasPasswordTransformationMethod());
        while (true)
        {
            return bool;
            if ((this.mText.length() > 0) && (hasSelection()))
                bool = true;
        }
    }

    boolean canCut()
    {
        boolean bool = false;
        if (hasPasswordTransformationMethod());
        while (true)
        {
            return bool;
            if ((this.mText.length() > 0) && (hasSelection()) && ((this.mText instanceof Editable)) && (this.mEditor != null) && (this.mEditor.mKeyListener != null))
                bool = true;
        }
    }

    boolean canPaste()
    {
        if (((this.mText instanceof Editable)) && (this.mEditor != null) && (this.mEditor.mKeyListener != null) && (getSelectionStart() >= 0) && (getSelectionEnd() >= 0) && (((ClipboardManager)getContext().getSystemService("clipboard")).hasPrimaryClip()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void cancelLongPress()
    {
        super.cancelLongPress();
        if (this.mEditor != null)
            this.mEditor.mIgnoreActionUpEvent = true;
    }

    public void clearComposingText()
    {
        if ((this.mText instanceof Spannable))
            BaseInputConnection.removeComposingSpans((Spannable)this.mText);
    }

    protected int computeHorizontalScrollRange()
    {
        int i;
        if (this.mLayout != null)
            if ((this.mSingleLine) && ((0x7 & this.mGravity) == 3))
                i = (int)this.mLayout.getLineWidth(0);
        while (true)
        {
            return i;
            i = this.mLayout.getWidth();
            continue;
            i = super.computeHorizontalScrollRange();
        }
    }

    public void computeScroll()
    {
        if ((this.mScroller != null) && (this.mScroller.computeScrollOffset()))
        {
            this.mScrollX = this.mScroller.getCurrX();
            this.mScrollY = this.mScroller.getCurrY();
            invalidateParentCaches();
            postInvalidate();
        }
    }

    protected int computeVerticalScrollExtent()
    {
        return getHeight() - getCompoundPaddingTop() - getCompoundPaddingBottom();
    }

    protected int computeVerticalScrollRange()
    {
        if (this.mLayout != null);
        for (int i = this.mLayout.getHeight(); ; i = super.computeVerticalScrollRange())
            return i;
    }

    float convertToLocalHorizontalCoordinate(float paramFloat)
    {
        float f = Math.max(0.0F, paramFloat - getTotalPaddingLeft());
        return Math.min(-1 + (getWidth() - getTotalPaddingRight()), f) + getScrollX();
    }

    public void debug(int paramInt)
    {
        super.debug(paramInt);
        String str1 = debugIndent(paramInt);
        String str2 = str1 + "frame={" + this.mLeft + ", " + this.mTop + ", " + this.mRight + ", " + this.mBottom + "} scroll={" + this.mScrollX + ", " + this.mScrollY + "} ";
        if (this.mText != null)
        {
            str3 = str2 + "mText=\"" + this.mText + "\" ";
            if (this.mLayout == null);
        }
        for (String str3 = str3 + "mLayout width=" + this.mLayout.getWidth() + " height=" + this.mLayout.getHeight(); ; str3 = str2 + "mText=NULL")
        {
            Log.d("View", str3);
            return;
        }
    }

    protected void deleteText_internal(int paramInt1, int paramInt2)
    {
        ((Editable)this.mText).delete(paramInt1, paramInt2);
    }

    public boolean didTouchFocusSelect()
    {
        if ((this.mEditor != null) && (this.mEditor.mTouchFocusSelected));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void dispatchFinishTemporaryDetach()
    {
        this.mDispatchTemporaryDetach = true;
        super.dispatchFinishTemporaryDetach();
        this.mDispatchTemporaryDetach = false;
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (((this.mTextColor != null) && (this.mTextColor.isStateful())) || ((this.mHintTextColor != null) && (this.mHintTextColor.isStateful())) || ((this.mLinkTextColor != null) && (this.mLinkTextColor.isStateful())))
            updateTextColors();
        Drawables localDrawables = this.mDrawables;
        if (localDrawables != null)
        {
            int[] arrayOfInt = getDrawableState();
            if ((localDrawables.mDrawableTop != null) && (localDrawables.mDrawableTop.isStateful()))
                localDrawables.mDrawableTop.setState(arrayOfInt);
            if ((localDrawables.mDrawableBottom != null) && (localDrawables.mDrawableBottom.isStateful()))
                localDrawables.mDrawableBottom.setState(arrayOfInt);
            if ((localDrawables.mDrawableLeft != null) && (localDrawables.mDrawableLeft.isStateful()))
                localDrawables.mDrawableLeft.setState(arrayOfInt);
            if ((localDrawables.mDrawableRight != null) && (localDrawables.mDrawableRight.isStateful()))
                localDrawables.mDrawableRight.setState(arrayOfInt);
            if ((localDrawables.mDrawableStart != null) && (localDrawables.mDrawableStart.isStateful()))
                localDrawables.mDrawableStart.setState(arrayOfInt);
            if ((localDrawables.mDrawableEnd != null) && (localDrawables.mDrawableEnd.isStateful()))
                localDrawables.mDrawableEnd.setState(arrayOfInt);
        }
    }

    public void endBatchEdit()
    {
        if (this.mEditor != null)
            this.mEditor.endBatchEdit();
    }

    public boolean extractText(ExtractedTextRequest paramExtractedTextRequest, ExtractedText paramExtractedText)
    {
        createEditorIfNeeded();
        return this.mEditor.extractText(paramExtractedTextRequest, paramExtractedText);
    }

    public void findViewsWithText(ArrayList<View> paramArrayList, CharSequence paramCharSequence, int paramInt)
    {
        super.findViewsWithText(paramArrayList, paramCharSequence, paramInt);
        if ((!paramArrayList.contains(this)) && ((paramInt & 0x1) != 0) && (!TextUtils.isEmpty(paramCharSequence)) && (!TextUtils.isEmpty(this.mText)))
        {
            String str = paramCharSequence.toString().toLowerCase();
            if (this.mText.toString().toLowerCase().contains(str))
                paramArrayList.add(this);
        }
    }

    public int getAccessibilityCursorPosition()
    {
        int i;
        if (TextUtils.isEmpty(getContentDescription()))
        {
            i = getSelectionEnd();
            if (i < 0);
        }
        while (true)
        {
            return i;
            i = super.getAccessibilityCursorPosition();
        }
    }

    public final int getAutoLinkMask()
    {
        return this.mAutoLinkMask;
    }

    public int getBaseline()
    {
        if (this.mLayout == null);
        int i;
        for (int j = super.getBaseline(); ; j = i + getExtendedPaddingTop() + this.mLayout.getLineBaseline(0))
        {
            return j;
            i = 0;
            if ((0x70 & this.mGravity) != 48)
                i = getVerticalOffset(true);
        }
    }

    protected int getBottomPaddingOffset()
    {
        return (int)Math.max(0.0F, this.mShadowDy + this.mShadowRadius);
    }

    public int getCompoundDrawablePadding()
    {
        Drawables localDrawables = this.mDrawables;
        if (localDrawables != null);
        for (int i = localDrawables.mDrawablePadding; ; i = 0)
            return i;
    }

    public Drawable[] getCompoundDrawables()
    {
        Drawables localDrawables = this.mDrawables;
        Drawable[] arrayOfDrawable;
        if (localDrawables != null)
        {
            arrayOfDrawable = new Drawable[4];
            arrayOfDrawable[0] = localDrawables.mDrawableLeft;
            arrayOfDrawable[1] = localDrawables.mDrawableTop;
            arrayOfDrawable[2] = localDrawables.mDrawableRight;
            arrayOfDrawable[3] = localDrawables.mDrawableBottom;
        }
        while (true)
        {
            return arrayOfDrawable;
            arrayOfDrawable = new Drawable[4];
            arrayOfDrawable[0] = null;
            arrayOfDrawable[1] = null;
            arrayOfDrawable[2] = null;
            arrayOfDrawable[3] = null;
        }
    }

    public Drawable[] getCompoundDrawablesRelative()
    {
        Drawables localDrawables = this.mDrawables;
        Drawable[] arrayOfDrawable;
        if (localDrawables != null)
        {
            arrayOfDrawable = new Drawable[4];
            arrayOfDrawable[0] = localDrawables.mDrawableStart;
            arrayOfDrawable[1] = localDrawables.mDrawableTop;
            arrayOfDrawable[2] = localDrawables.mDrawableEnd;
            arrayOfDrawable[3] = localDrawables.mDrawableBottom;
        }
        while (true)
        {
            return arrayOfDrawable;
            arrayOfDrawable = new Drawable[4];
            arrayOfDrawable[0] = null;
            arrayOfDrawable[1] = null;
            arrayOfDrawable[2] = null;
            arrayOfDrawable[3] = null;
        }
    }

    public int getCompoundPaddingBottom()
    {
        Drawables localDrawables = this.mDrawables;
        if ((localDrawables == null) || (localDrawables.mDrawableBottom == null));
        for (int i = this.mPaddingBottom; ; i = this.mPaddingBottom + localDrawables.mDrawablePadding + localDrawables.mDrawableSizeBottom)
            return i;
    }

    public int getCompoundPaddingEnd()
    {
        resolveDrawables();
        switch (getResolvedLayoutDirection())
        {
        default:
        case 1:
        }
        for (int i = getCompoundPaddingRight(); ; i = getCompoundPaddingLeft())
            return i;
    }

    public int getCompoundPaddingLeft()
    {
        Drawables localDrawables = this.mDrawables;
        if ((localDrawables == null) || (localDrawables.mDrawableLeft == null));
        for (int i = this.mPaddingLeft; ; i = this.mPaddingLeft + localDrawables.mDrawablePadding + localDrawables.mDrawableSizeLeft)
            return i;
    }

    public int getCompoundPaddingRight()
    {
        Drawables localDrawables = this.mDrawables;
        if ((localDrawables == null) || (localDrawables.mDrawableRight == null));
        for (int i = this.mPaddingRight; ; i = this.mPaddingRight + localDrawables.mDrawablePadding + localDrawables.mDrawableSizeRight)
            return i;
    }

    public int getCompoundPaddingStart()
    {
        resolveDrawables();
        switch (getResolvedLayoutDirection())
        {
        default:
        case 1:
        }
        for (int i = getCompoundPaddingLeft(); ; i = getCompoundPaddingRight())
            return i;
    }

    public int getCompoundPaddingTop()
    {
        Drawables localDrawables = this.mDrawables;
        if ((localDrawables == null) || (localDrawables.mDrawableTop == null));
        for (int i = this.mPaddingTop; ; i = this.mPaddingTop + localDrawables.mDrawablePadding + localDrawables.mDrawableSizeTop)
            return i;
    }

    public final int getCurrentHintTextColor()
    {
        if (this.mHintTextColor != null);
        for (int i = this.mCurHintTextColor; ; i = this.mCurTextColor)
            return i;
    }

    public final int getCurrentTextColor()
    {
        return this.mCurTextColor;
    }

    public ActionMode.Callback getCustomSelectionActionModeCallback()
    {
        if (this.mEditor == null);
        for (ActionMode.Callback localCallback = null; ; localCallback = this.mEditor.mCustomSelectionActionModeCallback)
            return localCallback;
    }

    protected boolean getDefaultEditable()
    {
        return false;
    }

    protected MovementMethod getDefaultMovementMethod()
    {
        return null;
    }

    public Editable getEditableText()
    {
        if ((this.mText instanceof Editable));
        for (Editable localEditable = (Editable)this.mText; ; localEditable = null)
            return localEditable;
    }

    @ViewDebug.ExportedProperty
    public TextUtils.TruncateAt getEllipsize()
    {
        return this.mEllipsize;
    }

    public CharSequence getError()
    {
        if (this.mEditor == null);
        for (CharSequence localCharSequence = null; ; localCharSequence = this.mEditor.mError)
            return localCharSequence;
    }

    public int getExtendedPaddingBottom()
    {
        int j;
        if (this.mMaxMode != 1)
            j = getCompoundPaddingBottom();
        while (true)
        {
            return j;
            if (this.mLayout.getLineCount() <= this.mMaximum)
            {
                j = getCompoundPaddingBottom();
            }
            else
            {
                int i = getCompoundPaddingTop();
                j = getCompoundPaddingBottom();
                int k = getHeight() - i - j;
                int m = this.mLayout.getLineTop(this.mMaximum);
                if (m < k)
                {
                    int n = 0x70 & this.mGravity;
                    if (n == 48)
                        j = j + k - m;
                    else if (n != 80)
                        j += (k - m) / 2;
                }
            }
        }
    }

    public int getExtendedPaddingTop()
    {
        int i;
        if (this.mMaxMode != 1)
            i = getCompoundPaddingTop();
        while (true)
        {
            return i;
            if (this.mLayout.getLineCount() <= this.mMaximum)
            {
                i = getCompoundPaddingTop();
            }
            else
            {
                i = getCompoundPaddingTop();
                int j = getCompoundPaddingBottom();
                int k = getHeight() - i - j;
                int m = this.mLayout.getLineTop(this.mMaximum);
                if (m < k)
                {
                    int n = 0x70 & this.mGravity;
                    if (n != 48)
                        if (n == 80)
                            i = i + k - m;
                        else
                            i += (k - m) / 2;
                }
            }
        }
    }

    protected int getFadeHeight(boolean paramBoolean)
    {
        if (this.mLayout != null);
        for (int i = this.mLayout.getHeight(); ; i = 0)
            return i;
    }

    protected int getFadeTop(boolean paramBoolean)
    {
        if (this.mLayout == null);
        int i;
        for (int j = 0; ; j = i + getExtendedPaddingTop())
        {
            return j;
            i = 0;
            if ((0x70 & this.mGravity) != 48)
                i = getVerticalOffset(true);
            if (paramBoolean)
                i += getTopPaddingOffset();
        }
    }

    public InputFilter[] getFilters()
    {
        return this.mFilters;
    }

    public void getFocusedRect(Rect paramRect)
    {
        if (this.mLayout == null)
            super.getFocusedRect(paramRect);
        int i;
        while (true)
        {
            return;
            i = getSelectionEnd();
            if (i >= 0)
                break;
            super.getFocusedRect(paramRect);
        }
        int j = getSelectionStart();
        if ((j < 0) || (j >= i))
        {
            int k = this.mLayout.getLineForOffset(i);
            paramRect.top = this.mLayout.getLineTop(k);
            paramRect.bottom = this.mLayout.getLineBottom(k);
            paramRect.left = (-2 + (int)this.mLayout.getPrimaryHorizontal(i));
            paramRect.right = (4 + paramRect.left);
        }
        while (true)
        {
            int m = getCompoundPaddingLeft();
            int n = getExtendedPaddingTop();
            if ((0x70 & this.mGravity) != 48)
                n += getVerticalOffset(false);
            paramRect.offset(m, n);
            paramRect.bottom = (getExtendedPaddingBottom() + paramRect.bottom);
            break;
            int i1 = this.mLayout.getLineForOffset(j);
            int i2 = this.mLayout.getLineForOffset(i);
            paramRect.top = this.mLayout.getLineTop(i1);
            paramRect.bottom = this.mLayout.getLineBottom(i2);
            if (i1 == i2)
            {
                paramRect.left = ((int)this.mLayout.getPrimaryHorizontal(j));
                paramRect.right = ((int)this.mLayout.getPrimaryHorizontal(i));
                continue;
            }
            if (this.mHighlightPathBogus)
            {
                if (this.mHighlightPath == null)
                    this.mHighlightPath = new Path();
                this.mHighlightPath.reset();
                this.mLayout.getSelectionPath(j, i, this.mHighlightPath);
                this.mHighlightPathBogus = false;
            }
            synchronized (TEMP_RECTF)
            {
                this.mHighlightPath.computeBounds(TEMP_RECTF, true);
                paramRect.left = (-1 + (int)TEMP_RECTF.left);
                paramRect.right = (1 + (int)TEMP_RECTF.right);
            }
        }
    }

    public boolean getFreezesText()
    {
        return this.mFreezesText;
    }

    public int getGravity()
    {
        return this.mGravity;
    }

    public int getHighlightColor()
    {
        return this.mHighlightColor;
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getHint()
    {
        return this.mHint;
    }

    final Layout getHintLayout()
    {
        return this.mHintLayout;
    }

    public final ColorStateList getHintTextColors()
    {
        return this.mHintTextColor;
    }

    public boolean getHorizontallyScrolling()
    {
        return this.mHorizontallyScrolling;
    }

    public int getImeActionId()
    {
        if ((this.mEditor != null) && (this.mEditor.mInputContentType != null));
        for (int i = this.mEditor.mInputContentType.imeActionId; ; i = 0)
            return i;
    }

    public CharSequence getImeActionLabel()
    {
        if ((this.mEditor != null) && (this.mEditor.mInputContentType != null));
        for (CharSequence localCharSequence = this.mEditor.mInputContentType.imeActionLabel; ; localCharSequence = null)
            return localCharSequence;
    }

    public int getImeOptions()
    {
        if ((this.mEditor != null) && (this.mEditor.mInputContentType != null));
        for (int i = this.mEditor.mInputContentType.imeOptions; ; i = 0)
            return i;
    }

    public boolean getIncludeFontPadding()
    {
        return this.mIncludePad;
    }

    public Bundle getInputExtras(boolean paramBoolean)
    {
        Bundle localBundle = null;
        if ((this.mEditor == null) && (!paramBoolean));
        while (true)
        {
            return localBundle;
            createEditorIfNeeded();
            if (this.mEditor.mInputContentType == null)
            {
                if (paramBoolean)
                    this.mEditor.createInputContentTypeIfNeeded();
            }
            else if (this.mEditor.mInputContentType.extras == null)
            {
                if (paramBoolean)
                    this.mEditor.mInputContentType.extras = new Bundle();
            }
            else
                localBundle = this.mEditor.mInputContentType.extras;
        }
    }

    public int getInputType()
    {
        if (this.mEditor == null);
        for (int i = 0; ; i = this.mEditor.mInputType)
            return i;
    }

    public CharSequence getIterableTextForAccessibility()
    {
        if (getContentDescription() == null)
            if (!(this.mText instanceof Spannable))
                setText(this.mText, BufferType.SPANNABLE);
        for (CharSequence localCharSequence = this.mText; ; localCharSequence = super.getIterableTextForAccessibility())
            return localCharSequence;
    }

    public AccessibilityIterators.TextSegmentIterator getIteratorForGranularity(int paramInt)
    {
        Object localObject;
        switch (paramInt)
        {
        default:
            localObject = super.getIteratorForGranularity(paramInt);
        case 4:
        case 16:
        }
        while (true)
        {
            return localObject;
            Spannable localSpannable = (Spannable)getIterableTextForAccessibility();
            if ((TextUtils.isEmpty(localSpannable)) || (getLayout() == null))
                break;
            localObject = AccessibilityIterators.LineTextSegmentIterator.getInstance();
            ((AccessibilityIterators.LineTextSegmentIterator)localObject).initialize(localSpannable, getLayout());
            continue;
            if ((TextUtils.isEmpty((Spannable)getIterableTextForAccessibility())) || (getLayout() == null))
                break;
            localObject = AccessibilityIterators.PageTextSegmentIterator.getInstance();
            ((AccessibilityIterators.PageTextSegmentIterator)localObject).initialize(this);
        }
    }

    public final KeyListener getKeyListener()
    {
        if (this.mEditor == null);
        for (KeyListener localKeyListener = null; ; localKeyListener = this.mEditor.mKeyListener)
            return localKeyListener;
    }

    public final Layout getLayout()
    {
        return this.mLayout;
    }

    protected float getLeftFadingEdgeStrength()
    {
        float f = 0.0F;
        if ((this.mEllipsize == TextUtils.TruncateAt.MARQUEE) && (this.mMarqueeFadeMode != 1))
            if ((this.mMarquee != null) && (!this.mMarquee.isStopped()))
            {
                Marquee localMarquee = this.mMarquee;
                if (localMarquee.shouldDrawLeftFade())
                    f = localMarquee.mScroll / getHorizontalFadingEdgeLength();
            }
        while (true)
        {
            return f;
            int i;
            if (getLineCount() == 1)
                i = getResolvedLayoutDirection();
            switch (0x7 & Gravity.getAbsoluteGravity(this.mGravity, i))
            {
            case 1:
            case 3:
            case 2:
            case 4:
            default:
                f = super.getLeftFadingEdgeStrength();
                break;
            case 5:
                f = (this.mLayout.getLineRight(0) - (this.mRight - this.mLeft) - getCompoundPaddingLeft() - getCompoundPaddingRight() - this.mLayout.getLineLeft(0)) / getHorizontalFadingEdgeLength();
            }
        }
    }

    protected int getLeftPaddingOffset()
    {
        return getCompoundPaddingLeft() - this.mPaddingLeft + (int)Math.min(0.0F, this.mShadowDx - this.mShadowRadius);
    }

    int getLineAtCoordinate(float paramFloat)
    {
        float f1 = Math.max(0.0F, paramFloat - getTotalPaddingTop());
        float f2 = Math.min(-1 + (getHeight() - getTotalPaddingBottom()), f1) + getScrollY();
        return getLayout().getLineForVertical((int)f2);
    }

    public int getLineBounds(int paramInt, Rect paramRect)
    {
        int i = 0;
        if (this.mLayout == null)
            if (paramRect != null)
                paramRect.set(0, 0, 0, 0);
        while (true)
        {
            return i;
            int j = this.mLayout.getLineBounds(paramInt, paramRect);
            int k = getExtendedPaddingTop();
            if ((0x70 & this.mGravity) != 48)
                k += getVerticalOffset(true);
            if (paramRect != null)
                paramRect.offset(getCompoundPaddingLeft(), k);
            i = j + k;
        }
    }

    public int getLineCount()
    {
        if (this.mLayout != null);
        for (int i = this.mLayout.getLineCount(); ; i = 0)
            return i;
    }

    public int getLineHeight()
    {
        return FastMath.round(this.mTextPaint.getFontMetricsInt(null) * this.mSpacingMult + this.mSpacingAdd);
    }

    public float getLineSpacingExtra()
    {
        return this.mSpacingAdd;
    }

    public float getLineSpacingMultiplier()
    {
        return this.mSpacingMult;
    }

    public final ColorStateList getLinkTextColors()
    {
        return this.mLinkTextColor;
    }

    public final boolean getLinksClickable()
    {
        return this.mLinksClickable;
    }

    public int getMarqueeRepeatLimit()
    {
        return this.mMarqueeRepeatLimit;
    }

    public int getMaxEms()
    {
        if (this.mMaxWidthMode == 1);
        for (int i = this.mMaxWidth; ; i = -1)
            return i;
    }

    public int getMaxHeight()
    {
        if (this.mMaxMode == 2);
        for (int i = this.mMaximum; ; i = -1)
            return i;
    }

    public int getMaxLines()
    {
        if (this.mMaxMode == 1);
        for (int i = this.mMaximum; ; i = -1)
            return i;
    }

    public int getMaxWidth()
    {
        if (this.mMaxWidthMode == 2);
        for (int i = this.mMaxWidth; ; i = -1)
            return i;
    }

    public int getMinEms()
    {
        if (this.mMinWidthMode == 1);
        for (int i = this.mMinWidth; ; i = -1)
            return i;
    }

    public int getMinHeight()
    {
        if (this.mMinMode == 2);
        for (int i = this.mMinimum; ; i = -1)
            return i;
    }

    public int getMinLines()
    {
        if (this.mMinMode == 1);
        for (int i = this.mMinimum; ; i = -1)
            return i;
    }

    public int getMinWidth()
    {
        if (this.mMinWidthMode == 2);
        for (int i = this.mMinWidth; ; i = -1)
            return i;
    }

    public final MovementMethod getMovementMethod()
    {
        return this.mMovement;
    }

    public int getOffsetForPosition(float paramFloat1, float paramFloat2)
    {
        if (getLayout() == null);
        for (int i = -1; ; i = getOffsetAtCoordinate(getLineAtCoordinate(paramFloat2), paramFloat1))
            return i;
    }

    public TextPaint getPaint()
    {
        return this.mTextPaint;
    }

    public int getPaintFlags()
    {
        return this.mTextPaint.getFlags();
    }

    public String getPrivateImeOptions()
    {
        if ((this.mEditor != null) && (this.mEditor.mInputContentType != null));
        for (String str = this.mEditor.mInputContentType.privateImeOptions; ; str = null)
            return str;
    }

    public int getResolvedLayoutDirection(Drawable paramDrawable)
    {
        int i;
        if (paramDrawable == null)
            i = 0;
        while (true)
        {
            return i;
            if (this.mDrawables != null)
            {
                Drawables localDrawables = this.mDrawables;
                if ((paramDrawable == localDrawables.mDrawableLeft) || (paramDrawable == localDrawables.mDrawableRight) || (paramDrawable == localDrawables.mDrawableTop) || (paramDrawable == localDrawables.mDrawableBottom) || (paramDrawable == localDrawables.mDrawableStart) || (paramDrawable == localDrawables.mDrawableEnd))
                    i = getResolvedLayoutDirection();
            }
            else
            {
                i = super.getResolvedLayoutDirection(paramDrawable);
            }
        }
    }

    protected float getRightFadingEdgeStrength()
    {
        float f;
        if ((this.mEllipsize == TextUtils.TruncateAt.MARQUEE) && (this.mMarqueeFadeMode != 1))
            if ((this.mMarquee != null) && (!this.mMarquee.isStopped()))
            {
                Marquee localMarquee = this.mMarquee;
                f = (localMarquee.mMaxFadeScroll - localMarquee.mScroll) / getHorizontalFadingEdgeLength();
            }
        while (true)
        {
            return f;
            int i;
            if (getLineCount() == 1)
                i = getResolvedLayoutDirection();
            switch (0x7 & Gravity.getAbsoluteGravity(this.mGravity, i))
            {
            case 2:
            case 4:
            case 6:
            default:
                f = super.getRightFadingEdgeStrength();
                break;
            case 3:
                int j = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
                f = (this.mLayout.getLineWidth(0) - j) / getHorizontalFadingEdgeLength();
                break;
            case 5:
                f = 0.0F;
                break;
            case 1:
            case 7:
                f = (this.mLayout.getLineWidth(0) - (this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight())) / getHorizontalFadingEdgeLength();
            }
        }
    }

    protected int getRightPaddingOffset()
    {
        return -(getCompoundPaddingRight() - this.mPaddingRight) + (int)Math.max(0.0F, this.mShadowDx + this.mShadowRadius);
    }

    @ViewDebug.ExportedProperty(category="text")
    public int getSelectionEnd()
    {
        return Selection.getSelectionEnd(getText());
    }

    @ViewDebug.ExportedProperty(category="text")
    public int getSelectionStart()
    {
        return Selection.getSelectionStart(getText());
    }

    public int getShadowColor()
    {
        return this.mTextPaint.shadowColor;
    }

    public float getShadowDx()
    {
        return this.mShadowDx;
    }

    public float getShadowDy()
    {
        return this.mShadowDy;
    }

    public float getShadowRadius()
    {
        return this.mShadowRadius;
    }

    public final boolean getShowSoftInputOnFocus()
    {
        if ((this.mEditor == null) || (this.mEditor.mShowSoftInputOnFocus));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getText()
    {
        return this.mText;
    }

    public final ColorStateList getTextColors()
    {
        return this.mTextColor;
    }

    public CharSequence getTextForAccessibility()
    {
        CharSequence localCharSequence = getText();
        if (TextUtils.isEmpty(localCharSequence))
            localCharSequence = getHint();
        return localCharSequence;
    }

    public float getTextScaleX()
    {
        return this.mTextPaint.getTextScaleX();
    }

    public Locale getTextServicesLocale()
    {
        Locale localLocale = Locale.getDefault();
        SpellCheckerSubtype localSpellCheckerSubtype = ((TextServicesManager)this.mContext.getSystemService("textservices")).getCurrentSpellCheckerSubtype(true);
        if (localSpellCheckerSubtype != null)
            localLocale = SpellCheckerSubtype.constructLocaleFromString(localSpellCheckerSubtype.getLocale());
        return localLocale;
    }

    @ViewDebug.ExportedProperty(category="text")
    public float getTextSize()
    {
        return this.mTextPaint.getTextSize();
    }

    protected int getTopPaddingOffset()
    {
        return (int)Math.min(0.0F, this.mShadowDy - this.mShadowRadius);
    }

    public int getTotalPaddingBottom()
    {
        return getExtendedPaddingBottom() + getBottomVerticalOffset(true);
    }

    public int getTotalPaddingEnd()
    {
        return getCompoundPaddingEnd();
    }

    public int getTotalPaddingLeft()
    {
        return getCompoundPaddingLeft();
    }

    public int getTotalPaddingRight()
    {
        return getCompoundPaddingRight();
    }

    public int getTotalPaddingStart()
    {
        return getCompoundPaddingStart();
    }

    public int getTotalPaddingTop()
    {
        return getExtendedPaddingTop() + getVerticalOffset(true);
    }

    public final TransformationMethod getTransformationMethod()
    {
        return this.mTransformation;
    }

    CharSequence getTransformedText(int paramInt1, int paramInt2)
    {
        return removeSuggestionSpans(this.mTransformed.subSequence(paramInt1, paramInt2));
    }

    public Typeface getTypeface()
    {
        return this.mTextPaint.getTypeface();
    }

    public URLSpan[] getUrls()
    {
        if ((this.mText instanceof Spanned));
        for (URLSpan[] arrayOfURLSpan = (URLSpan[])((Spanned)this.mText).getSpans(0, this.mText.length(), URLSpan.class); ; arrayOfURLSpan = new URLSpan[0])
            return arrayOfURLSpan;
    }

    int getVerticalOffset(boolean paramBoolean)
    {
        int i = 0;
        int j = 0x70 & this.mGravity;
        Layout localLayout = this.mLayout;
        if ((!paramBoolean) && (this.mText.length() == 0) && (this.mHintLayout != null))
            localLayout = this.mHintLayout;
        int k;
        int m;
        if (j != 48)
        {
            if (localLayout != this.mHintLayout)
                break label104;
            k = getMeasuredHeight() - getCompoundPaddingTop() - getCompoundPaddingBottom();
            m = localLayout.getHeight();
            if (m < k)
                if (j != 80)
                    break label123;
        }
        label104: label123: for (i = k - m; ; i = k - m >> 1)
        {
            return i;
            k = getMeasuredHeight() - getExtendedPaddingTop() - getExtendedPaddingBottom();
            break;
        }
    }

    public WordIterator getWordIterator()
    {
        if (this.mEditor != null);
        for (WordIterator localWordIterator = this.mEditor.getWordIterator(); ; localWordIterator = null)
            return localWordIterator;
    }

    void handleTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        Editor.InputMethodState localInputMethodState;
        if (this.mEditor == null)
        {
            localInputMethodState = null;
            if ((localInputMethodState == null) || (localInputMethodState.mBatchEditNesting == 0))
                updateAfterEdit();
            if (localInputMethodState != null)
            {
                localInputMethodState.mContentChanged = true;
                if (localInputMethodState.mChangedStart >= 0)
                    break label106;
                localInputMethodState.mChangedStart = paramInt1;
            }
        }
        for (localInputMethodState.mChangedEnd = (paramInt1 + paramInt2); ; localInputMethodState.mChangedEnd = Math.max(localInputMethodState.mChangedEnd, paramInt1 + paramInt2 - localInputMethodState.mChangedDelta))
        {
            localInputMethodState.mChangedDelta += paramInt3 - paramInt2;
            sendOnTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
            onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
            return;
            localInputMethodState = this.mEditor.mInputMethodState;
            break;
            label106: localInputMethodState.mChangedStart = Math.min(localInputMethodState.mChangedStart, paramInt1);
        }
    }

    public boolean hasOverlappingRendering()
    {
        if ((getBackground() != null) || ((this.mText instanceof Spannable)) || (hasSelection()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasSelection()
    {
        int i = getSelectionStart();
        int j = getSelectionEnd();
        if ((i >= 0) && (i != j));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void hideErrorIfUnchanged()
    {
        if ((this.mEditor != null) && (this.mEditor.mError != null) && (!this.mEditor.mErrorWasChanged))
            setError(null, null);
    }

    void invalidateCursor()
    {
        int i = getSelectionEnd();
        invalidateCursor(i, i, i);
    }

    void invalidateCursorPath()
    {
        if (this.mHighlightPathBogus)
            invalidateCursor();
        while (true)
        {
            return;
            int i = getCompoundPaddingLeft();
            int j = getExtendedPaddingTop() + getVerticalOffset(true);
            if (this.mEditor.mCursorCount == 0)
                synchronized (TEMP_RECTF)
                {
                    float f1 = FloatMath.ceil(this.mTextPaint.getStrokeWidth());
                    if (f1 < 1.0F)
                        f1 = 1.0F;
                    float f2 = f1 / 2.0F;
                    this.mHighlightPath.computeBounds(TEMP_RECTF, false);
                    invalidate((int)FloatMath.floor(i + TEMP_RECTF.left - f2), (int)FloatMath.floor(j + TEMP_RECTF.top - f2), (int)FloatMath.ceil(f2 + (i + TEMP_RECTF.right)), (int)FloatMath.ceil(f2 + (j + TEMP_RECTF.bottom)));
                }
            for (int k = 0; k < this.mEditor.mCursorCount; k++)
            {
                Rect localRect = this.mEditor.mCursorDrawable[k].getBounds();
                invalidate(i + localRect.left, j + localRect.top, i + localRect.right, j + localRect.bottom);
            }
        }
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        Rect localRect;
        int i;
        int j;
        Drawables localDrawables;
        if (verifyDrawable(paramDrawable))
        {
            localRect = paramDrawable.getBounds();
            i = this.mScrollX;
            j = this.mScrollY;
            localDrawables = this.mDrawables;
            if (localDrawables != null)
            {
                if (paramDrawable != localDrawables.mDrawableLeft)
                    break label129;
                int i5 = getCompoundPaddingTop();
                int i6 = getCompoundPaddingBottom();
                int i7 = this.mBottom - this.mTop - i6 - i5;
                i += this.mPaddingLeft;
                j += i5 + (i7 - localDrawables.mDrawableHeightLeft) / 2;
            }
        }
        while (true)
        {
            invalidate(i + localRect.left, j + localRect.top, i + localRect.right, j + localRect.bottom);
            return;
            label129: if (paramDrawable == localDrawables.mDrawableRight)
            {
                int i2 = getCompoundPaddingTop();
                int i3 = getCompoundPaddingBottom();
                int i4 = this.mBottom - this.mTop - i3 - i2;
                i += this.mRight - this.mLeft - this.mPaddingRight - localDrawables.mDrawableSizeRight;
                j += i2 + (i4 - localDrawables.mDrawableHeightRight) / 2;
            }
            else if (paramDrawable == localDrawables.mDrawableTop)
            {
                int n = getCompoundPaddingLeft();
                int i1 = getCompoundPaddingRight();
                i += n + (this.mRight - this.mLeft - i1 - n - localDrawables.mDrawableWidthTop) / 2;
                j += this.mPaddingTop;
            }
            else if (paramDrawable == localDrawables.mDrawableBottom)
            {
                int k = getCompoundPaddingLeft();
                int m = getCompoundPaddingRight();
                i += k + (this.mRight - this.mLeft - m - k - localDrawables.mDrawableWidthBottom) / 2;
                j += this.mBottom - this.mTop - this.mPaddingBottom - localDrawables.mDrawableSizeBottom;
            }
        }
    }

    void invalidateRegion(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (this.mLayout == null)
        {
            invalidate();
            return;
        }
        int i = this.mLayout.getLineForOffset(paramInt1);
        int j = this.mLayout.getLineTop(i);
        if (i > 0)
            j -= this.mLayout.getLineDescent(i - 1);
        if (paramInt1 == paramInt2);
        int m;
        for (int k = i; ; k = this.mLayout.getLineForOffset(paramInt2))
        {
            m = this.mLayout.getLineBottom(k);
            if ((!paramBoolean) || (this.mEditor == null))
                break;
            for (int i6 = 0; i6 < this.mEditor.mCursorCount; i6++)
            {
                Rect localRect = this.mEditor.mCursorDrawable[i6].getBounds();
                j = Math.min(j, localRect.top);
                m = Math.max(m, localRect.bottom);
            }
        }
        int n = getCompoundPaddingLeft();
        int i1 = getExtendedPaddingTop() + getVerticalOffset(true);
        int i5;
        int i2;
        if ((i == k) && (!paramBoolean))
        {
            int i4 = (int)this.mLayout.getPrimaryHorizontal(paramInt1);
            i5 = (int)(1.0D + this.mLayout.getPrimaryHorizontal(paramInt2));
            i2 = i4 + n;
        }
        for (int i3 = i5 + n; ; i3 = getWidth() - getCompoundPaddingRight())
        {
            invalidate(i2 + this.mScrollX, i1 + j, i3 + this.mScrollX, i1 + m);
            break;
            i2 = n;
        }
    }

    public boolean isCursorVisible()
    {
        if (this.mEditor == null);
        for (boolean bool = true; ; bool = this.mEditor.mCursorVisible)
            return bool;
    }

    boolean isInBatchEditMode()
    {
        boolean bool = false;
        if (this.mEditor == null);
        while (true)
        {
            return bool;
            Editor.InputMethodState localInputMethodState = this.mEditor.mInputMethodState;
            if (localInputMethodState != null)
            {
                if (localInputMethodState.mBatchEditNesting > 0)
                    bool = true;
            }
            else
                bool = this.mEditor.mInBatchEditControllers;
        }
    }

    public boolean isInputMethodTarget()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if ((localInputMethodManager != null) && (localInputMethodManager.isActive(this)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean isPaddingOffsetRequired()
    {
        if ((this.mShadowRadius != 0.0F) || (this.mDrawables != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isSingleLine()
    {
        return this.mSingleLine;
    }

    public boolean isSuggestionsEnabled()
    {
        boolean bool = false;
        if (this.mEditor == null);
        while (true)
        {
            return bool;
            if (((0xF & this.mEditor.mInputType) == 1) && ((0x80000 & this.mEditor.mInputType) <= 0))
            {
                int i = 0xFF0 & this.mEditor.mInputType;
                if ((i == 0) || (i == 48) || (i == 80) || (i == 64) || (i == 160))
                    bool = true;
            }
        }
    }

    boolean isTextEditable()
    {
        if (((this.mText instanceof Editable)) && (onCheckIsTextEditor()) && (isEnabled()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isTextSelectable()
    {
        if (this.mEditor == null);
        for (boolean bool = false; ; bool = this.mEditor.mTextIsSelectable)
            return bool;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mDrawables != null)
        {
            if (this.mDrawables.mDrawableLeft != null)
                this.mDrawables.mDrawableLeft.jumpToCurrentState();
            if (this.mDrawables.mDrawableTop != null)
                this.mDrawables.mDrawableTop.jumpToCurrentState();
            if (this.mDrawables.mDrawableRight != null)
                this.mDrawables.mDrawableRight.jumpToCurrentState();
            if (this.mDrawables.mDrawableBottom != null)
                this.mDrawables.mDrawableBottom.jumpToCurrentState();
            if (this.mDrawables.mDrawableStart != null)
                this.mDrawables.mDrawableStart.jumpToCurrentState();
            if (this.mDrawables.mDrawableEnd != null)
                this.mDrawables.mDrawableEnd.jumpToCurrentState();
        }
    }

    public int length()
    {
        return this.mText.length();
    }

    protected void makeNewLayout(int paramInt1, int paramInt2, BoringLayout.Metrics paramMetrics1, BoringLayout.Metrics paramMetrics2, int paramInt3, boolean paramBoolean)
    {
        stopMarquee();
        this.mOldMaximum = this.mMaximum;
        this.mOldMaxMode = this.mMaxMode;
        this.mHighlightPathBogus = true;
        if (paramInt1 < 0)
            paramInt1 = 0;
        if (paramInt2 < 0)
            paramInt2 = 0;
        Layout.Alignment localAlignment = getLayoutAlignment();
        boolean bool1;
        int i;
        label80: boolean bool2;
        label132: TextUtils.TruncateAt localTruncateAt6;
        label170: boolean bool11;
        label182: int j;
        if ((this.mEllipsize != null) && (getKeyListener() == null))
        {
            bool1 = true;
            if ((this.mEllipsize != TextUtils.TruncateAt.MARQUEE) || (this.mMarqueeFadeMode == 0))
                break label459;
            i = 1;
            TextUtils.TruncateAt localTruncateAt1 = this.mEllipsize;
            if ((this.mEllipsize == TextUtils.TruncateAt.MARQUEE) && (this.mMarqueeFadeMode == 1))
                localTruncateAt1 = TextUtils.TruncateAt.END_SMALL;
            if (this.mTextDir == null)
                resolveTextDirection();
            if (localTruncateAt1 != this.mEllipsize)
                break label465;
            bool2 = true;
            this.mLayout = makeSingleLayout(paramInt1, paramMetrics1, paramInt3, localAlignment, bool1, localTruncateAt1, bool2);
            if (i != 0)
            {
                if (localTruncateAt1 != TextUtils.TruncateAt.MARQUEE)
                    break label471;
                localTruncateAt6 = TextUtils.TruncateAt.END;
                if (localTruncateAt1 == this.mEllipsize)
                    break label479;
                bool11 = true;
                this.mSavedMarqueeModeLayout = makeSingleLayout(paramInt1, paramMetrics1, paramInt3, localAlignment, bool1, localTruncateAt6, bool11);
            }
            if (this.mEllipsize == null)
                break label485;
            j = 1;
            label212: this.mHintLayout = null;
            if (this.mHint != null)
            {
                if (j != 0)
                    paramInt2 = paramInt1;
                BoringLayout.Metrics localMetrics = UNKNOWN_BORING;
                if (paramMetrics2 == localMetrics)
                {
                    paramMetrics2 = BoringLayout.isBoring(this.mHint, this.mTextPaint, this.mTextDir, this.mHintBoring);
                    if (paramMetrics2 != null)
                        this.mHintBoring = paramMetrics2;
                }
                if (paramMetrics2 == null)
                    break label888;
                if ((paramMetrics2.width > paramInt2) || ((j != 0) && (paramMetrics2.width > paramInt3)))
                    break label546;
                if (this.mSavedHintLayout == null)
                    break label491;
                BoringLayout localBoringLayout2 = this.mSavedHintLayout;
                CharSequence localCharSequence8 = this.mHint;
                TextPaint localTextPaint8 = this.mTextPaint;
                float f15 = this.mSpacingMult;
                float f16 = this.mSpacingAdd;
                boolean bool10 = this.mIncludePad;
                this.mHintLayout = localBoringLayout2.replaceOrMake(localCharSequence8, localTextPaint8, paramInt2, localAlignment, f15, f16, paramMetrics2, bool10);
                label371: this.mSavedHintLayout = ((BoringLayout)this.mHintLayout);
            }
            label382: if (paramBoolean)
                registerForPreDraw();
            if ((this.mEllipsize == TextUtils.TruncateAt.MARQUEE) && (!compressText(paramInt3)))
            {
                int k = this.mLayoutParams.height;
                if ((k == -2) || (k == -1))
                    break label1071;
                startMarquee();
            }
        }
        while (true)
        {
            if (this.mEditor != null)
                this.mEditor.prepareCursorControllers();
            return;
            bool1 = false;
            break;
            label459: i = 0;
            break label80;
            label465: bool2 = false;
            break label132;
            label471: localTruncateAt6 = TextUtils.TruncateAt.MARQUEE;
            break label170;
            label479: bool11 = false;
            break label182;
            label485: j = 0;
            break label212;
            label491: CharSequence localCharSequence7 = this.mHint;
            TextPaint localTextPaint7 = this.mTextPaint;
            float f13 = this.mSpacingMult;
            float f14 = this.mSpacingAdd;
            boolean bool9 = this.mIncludePad;
            this.mHintLayout = BoringLayout.make(localCharSequence7, localTextPaint7, paramInt2, localAlignment, f13, f14, paramMetrics2, bool9);
            break label371;
            label546: if ((j != 0) && (paramMetrics2.width <= paramInt2))
            {
                if (this.mSavedHintLayout != null)
                {
                    BoringLayout localBoringLayout1 = this.mSavedHintLayout;
                    CharSequence localCharSequence6 = this.mHint;
                    TextPaint localTextPaint6 = this.mTextPaint;
                    float f11 = this.mSpacingMult;
                    float f12 = this.mSpacingAdd;
                    boolean bool8 = this.mIncludePad;
                    TextUtils.TruncateAt localTruncateAt5 = this.mEllipsize;
                    this.mHintLayout = localBoringLayout1.replaceOrMake(localCharSequence6, localTextPaint6, paramInt2, localAlignment, f11, f12, paramMetrics2, bool8, localTruncateAt5, paramInt3);
                    break label382;
                }
                CharSequence localCharSequence5 = this.mHint;
                TextPaint localTextPaint5 = this.mTextPaint;
                float f9 = this.mSpacingMult;
                float f10 = this.mSpacingAdd;
                boolean bool7 = this.mIncludePad;
                TextUtils.TruncateAt localTruncateAt4 = this.mEllipsize;
                this.mHintLayout = BoringLayout.make(localCharSequence5, localTextPaint5, paramInt2, localAlignment, f9, f10, paramMetrics2, bool7, localTruncateAt4, paramInt3);
                break label382;
            }
            if (j != 0)
            {
                CharSequence localCharSequence4 = this.mHint;
                int i1 = this.mHint.length();
                TextPaint localTextPaint4 = this.mTextPaint;
                TextDirectionHeuristic localTextDirectionHeuristic4 = this.mTextDir;
                float f7 = this.mSpacingMult;
                float f8 = this.mSpacingAdd;
                boolean bool6 = this.mIncludePad;
                TextUtils.TruncateAt localTruncateAt3 = this.mEllipsize;
                if (this.mMaxMode == 1);
                for (int i2 = this.mMaximum; ; i2 = 2147483647)
                {
                    this.mHintLayout = new StaticLayout(localCharSequence4, 0, i1, localTextPaint4, paramInt2, localAlignment, localTextDirectionHeuristic4, f7, f8, bool6, localTruncateAt3, paramInt3, i2);
                    break;
                }
            }
            CharSequence localCharSequence3 = this.mHint;
            TextPaint localTextPaint3 = this.mTextPaint;
            TextDirectionHeuristic localTextDirectionHeuristic3 = this.mTextDir;
            float f5 = this.mSpacingMult;
            float f6 = this.mSpacingAdd;
            boolean bool5 = this.mIncludePad;
            this.mHintLayout = new StaticLayout(localCharSequence3, localTextPaint3, paramInt2, localAlignment, localTextDirectionHeuristic3, f5, f6, bool5);
            break label382;
            label888: if (j != 0)
            {
                CharSequence localCharSequence2 = this.mHint;
                int m = this.mHint.length();
                TextPaint localTextPaint2 = this.mTextPaint;
                TextDirectionHeuristic localTextDirectionHeuristic2 = this.mTextDir;
                float f3 = this.mSpacingMult;
                float f4 = this.mSpacingAdd;
                boolean bool4 = this.mIncludePad;
                TextUtils.TruncateAt localTruncateAt2 = this.mEllipsize;
                if (this.mMaxMode == 1);
                for (int n = this.mMaximum; ; n = 2147483647)
                {
                    this.mHintLayout = new StaticLayout(localCharSequence2, 0, m, localTextPaint2, paramInt2, localAlignment, localTextDirectionHeuristic2, f3, f4, bool4, localTruncateAt2, paramInt3, n);
                    break;
                }
            }
            CharSequence localCharSequence1 = this.mHint;
            TextPaint localTextPaint1 = this.mTextPaint;
            TextDirectionHeuristic localTextDirectionHeuristic1 = this.mTextDir;
            float f1 = this.mSpacingMult;
            float f2 = this.mSpacingAdd;
            boolean bool3 = this.mIncludePad;
            this.mHintLayout = new StaticLayout(localCharSequence1, localTextPaint1, paramInt2, localAlignment, localTextDirectionHeuristic1, f1, f2, bool3);
            break label382;
            label1071: this.mRestartMarquee = true;
        }
    }

    public boolean moveCursorToVisibleOffset()
    {
        boolean bool;
        if (!(this.mText instanceof Spannable))
            bool = false;
        while (true)
        {
            return bool;
            int i = getSelectionStart();
            if (i != getSelectionEnd())
            {
                bool = false;
            }
            else
            {
                int j = this.mLayout.getLineForOffset(i);
                int k = this.mLayout.getLineTop(j);
                int m = this.mLayout.getLineTop(j + 1);
                int n = this.mBottom - this.mTop - getExtendedPaddingTop() - getExtendedPaddingBottom();
                int i1 = (m - k) / 2;
                if (i1 > n / 4)
                    i1 = n / 4;
                int i2 = this.mScrollY;
                label142: int i6;
                int i7;
                label209: int i8;
                label220: int i9;
                if (k < i2 + i1)
                {
                    j = this.mLayout.getLineForVertical(i2 + i1 + (m - k));
                    int i3 = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
                    int i4 = this.mScrollX;
                    int i5 = this.mLayout.getOffsetForHorizontal(j, i4);
                    i6 = this.mLayout.getOffsetForHorizontal(j, i3 + i4);
                    if (i5 >= i6)
                        break label295;
                    i7 = i5;
                    if (i5 <= i6)
                        break label302;
                    i8 = i5;
                    i9 = i;
                    if (i9 >= i7)
                        break label309;
                    i9 = i7;
                }
                while (true)
                {
                    if (i9 == i)
                        break label323;
                    Selection.setSelection((Spannable)this.mText, i9);
                    bool = true;
                    break;
                    if (m <= n + i2 - i1)
                        break label142;
                    j = this.mLayout.getLineForVertical(n + i2 - i1 - (m - k));
                    break label142;
                    label295: i7 = i6;
                    break label209;
                    label302: i8 = i6;
                    break label220;
                    label309: if (i9 > i8)
                        i9 = i8;
                }
                label323: bool = false;
            }
        }
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.mTemporaryDetach = false;
        resolveDrawables();
        if (this.mEditor != null)
            this.mEditor.onAttachedToWindow();
    }

    public void onBeginBatchEdit()
    {
    }

    public boolean onCheckIsTextEditor()
    {
        if ((this.mEditor != null) && (this.mEditor.mInputType != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onCommitCompletion(CompletionInfo paramCompletionInfo)
    {
    }

    public void onCommitCorrection(CorrectionInfo paramCorrectionInfo)
    {
        if (this.mEditor != null)
            this.mEditor.onCommitCorrection(paramCorrectionInfo);
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt1;
        int j;
        label27: int[] arrayOfInt2;
        if (this.mSingleLine)
        {
            arrayOfInt1 = super.onCreateDrawableState(paramInt);
            if (!isTextSelectable())
                break label110;
            int i = arrayOfInt1.length;
            j = 0;
            if (j >= i)
                break label110;
            if (arrayOfInt1[j] != 16842919)
                break label104;
            arrayOfInt2 = new int[i - 1];
            System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, j);
            System.arraycopy(arrayOfInt1, j + 1, arrayOfInt2, j, -1 + (i - j));
        }
        while (true)
        {
            return arrayOfInt2;
            arrayOfInt1 = super.onCreateDrawableState(paramInt + 1);
            mergeDrawableStates(arrayOfInt1, MULTILINE_STATE_SET);
            break;
            label104: j++;
            break label27;
            label110: arrayOfInt2 = arrayOfInt1;
        }
    }

    public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo)
    {
        label184: EditableInputConnection localEditableInputConnection;
        if ((onCheckIsTextEditor()) && (isEnabled()))
        {
            this.mEditor.createInputMethodStateIfNeeded();
            paramEditorInfo.inputType = getInputType();
            if (this.mEditor.mInputContentType != null)
            {
                paramEditorInfo.imeOptions = this.mEditor.mInputContentType.imeOptions;
                paramEditorInfo.privateImeOptions = this.mEditor.mInputContentType.privateImeOptions;
                paramEditorInfo.actionLabel = this.mEditor.mInputContentType.imeActionLabel;
                paramEditorInfo.actionId = this.mEditor.mInputContentType.imeActionId;
                paramEditorInfo.extras = this.mEditor.mInputContentType.extras;
                if (focusSearch(130) != null)
                    paramEditorInfo.imeOptions = (0x8000000 | paramEditorInfo.imeOptions);
                if (focusSearch(33) != null)
                    paramEditorInfo.imeOptions = (0x4000000 | paramEditorInfo.imeOptions);
                if ((0xFF & paramEditorInfo.imeOptions) == 0)
                {
                    if ((0x8000000 & paramEditorInfo.imeOptions) == 0)
                        break label292;
                    paramEditorInfo.imeOptions = (0x5 | paramEditorInfo.imeOptions);
                    if (!shouldAdvanceFocusOnEnter())
                        paramEditorInfo.imeOptions = (0x40000000 | paramEditorInfo.imeOptions);
                }
                if (isMultilineInputType(paramEditorInfo.inputType))
                    paramEditorInfo.imeOptions = (0x40000000 | paramEditorInfo.imeOptions);
                paramEditorInfo.hintText = this.mHint;
                if (!(this.mText instanceof Editable))
                    break label306;
                localEditableInputConnection = new EditableInputConnection(this);
                paramEditorInfo.initialSelStart = getSelectionStart();
                paramEditorInfo.initialSelEnd = getSelectionEnd();
                paramEditorInfo.initialCapsMode = localEditableInputConnection.getCursorCapsMode(getInputType());
            }
        }
        while (true)
        {
            return localEditableInputConnection;
            paramEditorInfo.imeOptions = 0;
            break;
            label292: paramEditorInfo.imeOptions = (0x6 | paramEditorInfo.imeOptions);
            break label184;
            label306: localEditableInputConnection = null;
        }
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.mPreDrawRegistered)
        {
            getViewTreeObserver().removeOnPreDrawListener(this);
            this.mPreDrawRegistered = false;
        }
        resetResolvedDrawables();
        if (this.mEditor != null)
            this.mEditor.onDetachedFromWindow();
    }

    public boolean onDragEvent(DragEvent paramDragEvent)
    {
        boolean bool1 = true;
        switch (paramDragEvent.getAction())
        {
        case 4:
        default:
        case 1:
        case 5:
        case 2:
        case 3:
        }
        while (true)
        {
            return bool1;
            if ((this.mEditor != null) && (this.mEditor.hasInsertionController()));
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                bool1 = bool2;
                break;
            }
            requestFocus();
            continue;
            int i = getOffsetForPosition(paramDragEvent.getX(), paramDragEvent.getY());
            Selection.setSelection((Spannable)this.mText, i);
            continue;
            if (this.mEditor != null)
                this.mEditor.onDrop(paramDragEvent);
        }
    }

    protected void onDraw(Canvas paramCanvas)
    {
        restartMarqueeIfNeeded();
        super.onDraw(paramCanvas);
        int i = getCompoundPaddingLeft();
        int j = getCompoundPaddingTop();
        int k = getCompoundPaddingRight();
        int m = getCompoundPaddingBottom();
        int n = this.mScrollX;
        int i1 = this.mScrollY;
        int i2 = this.mRight;
        int i3 = this.mLeft;
        int i4 = this.mBottom;
        int i5 = this.mTop;
        Drawables localDrawables = this.mDrawables;
        if (localDrawables != null)
        {
            int i17 = i4 - i5 - m - j;
            int i18 = i2 - i3 - k - i;
            if (localDrawables.mDrawableLeft != null)
            {
                paramCanvas.save();
                paramCanvas.translate(n + this.mPaddingLeft, i1 + j + (i17 - localDrawables.mDrawableHeightLeft) / 2);
                localDrawables.mDrawableLeft.draw(paramCanvas);
                paramCanvas.restore();
            }
            if (localDrawables.mDrawableRight != null)
            {
                paramCanvas.save();
                paramCanvas.translate(n + i2 - i3 - this.mPaddingRight - localDrawables.mDrawableSizeRight, i1 + j + (i17 - localDrawables.mDrawableHeightRight) / 2);
                localDrawables.mDrawableRight.draw(paramCanvas);
                paramCanvas.restore();
            }
            if (localDrawables.mDrawableTop != null)
            {
                paramCanvas.save();
                paramCanvas.translate(n + i + (i18 - localDrawables.mDrawableWidthTop) / 2, i1 + this.mPaddingTop);
                localDrawables.mDrawableTop.draw(paramCanvas);
                paramCanvas.restore();
            }
            if (localDrawables.mDrawableBottom != null)
            {
                paramCanvas.save();
                paramCanvas.translate(n + i + (i18 - localDrawables.mDrawableWidthBottom) / 2, i1 + i4 - i5 - this.mPaddingBottom - localDrawables.mDrawableSizeBottom);
                localDrawables.mDrawableBottom.draw(paramCanvas);
                paramCanvas.restore();
            }
        }
        int i6 = this.mCurTextColor;
        if (this.mLayout == null)
            assumeLayout();
        Layout localLayout = this.mLayout;
        if ((this.mHint != null) && (this.mText.length() == 0))
        {
            if (this.mHintTextColor != null)
                i6 = this.mCurHintTextColor;
            localLayout = this.mHintLayout;
        }
        this.mTextPaint.setColor(i6);
        this.mTextPaint.drawableState = getDrawableState();
        paramCanvas.save();
        int i7 = getExtendedPaddingTop();
        int i8 = getExtendedPaddingBottom();
        int i9 = this.mBottom - this.mTop - m - j;
        int i10 = this.mLayout.getHeight() - i9;
        float f1 = i + n;
        float f2;
        int i16;
        Path localPath;
        if (i1 == 0)
        {
            f2 = 0.0F;
            float f3 = n + (i2 - i3 - k);
            int i11 = i1 + (i4 - i5);
            if (i1 == i10)
                i8 = 0;
            float f4 = i11 - i8;
            if (this.mShadowRadius != 0.0F)
            {
                f1 += Math.min(0.0F, this.mShadowDx - this.mShadowRadius);
                f3 += Math.max(0.0F, this.mShadowDx + this.mShadowRadius);
                f2 += Math.min(0.0F, this.mShadowDy - this.mShadowRadius);
                f4 += Math.max(0.0F, this.mShadowDy + this.mShadowRadius);
            }
            paramCanvas.clipRect(f1, f2, f3, f4);
            int i12 = 0;
            int i13 = 0;
            if ((0x70 & this.mGravity) != 48)
            {
                i12 = getVerticalOffset(false);
                i13 = getVerticalOffset(true);
            }
            paramCanvas.translate(i, i7 + i12);
            int i14 = getResolvedLayoutDirection();
            int i15 = Gravity.getAbsoluteGravity(this.mGravity, i14);
            if ((this.mEllipsize == TextUtils.TruncateAt.MARQUEE) && (this.mMarqueeFadeMode != 1))
            {
                if ((!this.mSingleLine) && (getLineCount() == 1) && (canMarquee()) && ((i15 & 0x7) != 3))
                    paramCanvas.translate(this.mLayout.getLineRight(0) - (this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight()), 0.0F);
                if ((this.mMarquee != null) && (this.mMarquee.isRunning()))
                    paramCanvas.translate(-this.mMarquee.mScroll, 0.0F);
            }
            i16 = i13 - i12;
            localPath = getUpdatedHighlightPath();
            if (this.mEditor == null)
                break label892;
            this.mEditor.onDraw(paramCanvas, localLayout, localPath, this.mHighlightPaint, i16);
        }
        while (true)
        {
            if ((this.mMarquee != null) && (this.mMarquee.shouldDrawGhost()))
            {
                paramCanvas.translate((int)this.mMarquee.getGhostOffset(), 0.0F);
                localLayout.draw(paramCanvas, localPath, this.mHighlightPaint, i16);
            }
            paramCanvas.restore();
            return;
            f2 = i7 + i1;
            break;
            label892: localLayout.draw(paramCanvas, localPath, this.mHighlightPaint, i16);
        }
    }

    public void onEditorAction(int paramInt)
    {
        Editor.InputContentType localInputContentType;
        if (this.mEditor == null)
        {
            localInputContentType = null;
            if (localInputContentType == null)
                break label161;
            if ((localInputContentType.onEditorActionListener == null) || (!localInputContentType.onEditorActionListener.onEditorAction(this, paramInt, null)))
                break label47;
        }
        label161: 
        while (true)
        {
            return;
            localInputContentType = this.mEditor.mInputContentType;
            break;
            label47: if (paramInt == 5)
            {
                View localView2 = focusSearch(2);
                if ((localView2 != null) && (!localView2.requestFocus(2)))
                    throw new IllegalStateException("focus search returned a view that wasn't able to take focus!");
            }
            else if (paramInt == 7)
            {
                View localView1 = focusSearch(1);
                if ((localView1 != null) && (!localView1.requestFocus(1)))
                    throw new IllegalStateException("focus search returned a view that wasn't able to take focus!");
            }
            else if (paramInt == 6)
            {
                InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                if ((localInputMethodManager != null) && (localInputMethodManager.isActive(this)))
                    localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            }
            else
            {
                ViewRootImpl localViewRootImpl = getViewRootImpl();
                if (localViewRootImpl != null)
                {
                    long l = SystemClock.uptimeMillis();
                    localViewRootImpl.dispatchKeyFromIme(new KeyEvent(l, l, 0, 66, 0, 0, -1, 0, 22));
                    localViewRootImpl.dispatchKeyFromIme(new KeyEvent(SystemClock.uptimeMillis(), l, 1, 66, 0, 0, -1, 0, 22));
                }
            }
        }
    }

    public void onEndBatchEdit()
    {
    }

    public void onFinishTemporaryDetach()
    {
        super.onFinishTemporaryDetach();
        if (!this.mDispatchTemporaryDetach)
            this.mTemporaryDetach = false;
        if (this.mEditor != null)
            this.mEditor.mTemporaryDetach = false;
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        if (this.mTemporaryDetach)
            super.onFocusChanged(paramBoolean, paramInt, paramRect);
        while (true)
        {
            return;
            if (this.mEditor != null)
                this.mEditor.onFocusChanged(paramBoolean, paramInt);
            if ((paramBoolean) && ((this.mText instanceof Spannable)))
                MetaKeyKeyListener.resetMetaState((Spannable)this.mText);
            startStopMarquee(paramBoolean);
            if (this.mTransformation != null)
                this.mTransformation.onFocusChanged(this, this.mText, paramBoolean, paramInt, paramRect);
            super.onFocusChanged(paramBoolean, paramInt, paramRect);
        }
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if ((this.mMovement != null) && ((this.mText instanceof Spannable)) && (this.mLayout != null));
        while (true)
        {
            try
            {
                boolean bool2 = this.mMovement.onGenericMotionEvent(this, (Spannable)this.mText, paramMotionEvent);
                if (bool2)
                {
                    bool1 = true;
                    return bool1;
                }
            }
            catch (AbstractMethodError localAbstractMethodError)
            {
            }
            boolean bool1 = super.onGenericMotionEvent(paramMotionEvent);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TextView.class.getName());
        paramAccessibilityEvent.setPassword(hasPasswordTransformationMethod());
        if (paramAccessibilityEvent.getEventType() == 8192)
        {
            paramAccessibilityEvent.setFromIndex(Selection.getSelectionStart(this.mText));
            paramAccessibilityEvent.setToIndex(Selection.getSelectionEnd(this.mText));
            paramAccessibilityEvent.setItemCount(this.mText.length());
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TextView.class.getName());
        boolean bool = hasPasswordTransformationMethod();
        paramAccessibilityNodeInfo.setPassword(bool);
        if (!bool)
            paramAccessibilityNodeInfo.setText(getTextForAccessibility());
        if ((TextUtils.isEmpty(getContentDescription())) && (!TextUtils.isEmpty(this.mText)))
        {
            paramAccessibilityNodeInfo.addAction(256);
            paramAccessibilityNodeInfo.addAction(512);
            paramAccessibilityNodeInfo.setMovementGranularities(31);
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if (doKeyDown(paramInt, paramKeyEvent, null) == 0);
        for (boolean bool = super.onKeyDown(paramInt, paramKeyEvent); ; bool = true)
            return bool;
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        KeyEvent localKeyEvent1 = KeyEvent.changeAction(paramKeyEvent, 0);
        int i = doKeyDown(paramInt1, localKeyEvent1, paramKeyEvent);
        if (i == 0);
        for (boolean bool = super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent); ; bool = true)
        {
            return bool;
            if (i != -1)
                break;
        }
        int j = paramInt2 - 1;
        KeyEvent localKeyEvent2 = KeyEvent.changeAction(paramKeyEvent, 1);
        if (i == 1)
        {
            this.mEditor.mKeyListener.onKeyUp(this, (Editable)this.mText, paramInt1, localKeyEvent2);
            while (true)
            {
                j--;
                if (j <= 0)
                    break;
                this.mEditor.mKeyListener.onKeyDown(this, (Editable)this.mText, paramInt1, localKeyEvent1);
                this.mEditor.mKeyListener.onKeyUp(this, (Editable)this.mText, paramInt1, localKeyEvent2);
            }
            hideErrorIfUnchanged();
        }
        while (true)
        {
            bool = true;
            break;
            if (i == 2)
            {
                this.mMovement.onKeyUp(this, (Spannable)this.mText, paramInt1, localKeyEvent2);
                while (true)
                {
                    j--;
                    if (j <= 0)
                        break;
                    this.mMovement.onKeyDown(this, (Spannable)this.mText, paramInt1, localKeyEvent1);
                    this.mMovement.onKeyUp(this, (Spannable)this.mText, paramInt1, localKeyEvent2);
                }
            }
        }
    }

    public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        if (paramInt == 4)
            if ((this.mEditor != null) && (this.mEditor.mSelectionActionMode != null))
            {
                int j = i;
                if (j == 0)
                    break label118;
                if ((paramKeyEvent.getAction() != 0) || (paramKeyEvent.getRepeatCount() != 0))
                    break label72;
                KeyEvent.DispatcherState localDispatcherState2 = getKeyDispatcherState();
                if (localDispatcherState2 != null)
                    localDispatcherState2.startTracking(paramKeyEvent, this);
            }
        while (true)
        {
            return i;
            int k = 0;
            break;
            label72: if (paramKeyEvent.getAction() == i)
            {
                KeyEvent.DispatcherState localDispatcherState1 = getKeyDispatcherState();
                if (localDispatcherState1 != null)
                    localDispatcherState1.handleUpEvent(paramKeyEvent);
                if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                    stopSelectionActionMode();
            }
            else
            {
                label118: boolean bool = super.onKeyPreIme(paramInt, paramKeyEvent);
            }
        }
    }

    public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent)
    {
        if (KeyEvent.metaStateHasNoModifiers(0xFFFF8FFF & paramKeyEvent.getMetaState()));
        boolean bool;
        switch (paramInt)
        {
        default:
            bool = super.onKeyShortcut(paramInt, paramKeyEvent);
        case 29:
        case 52:
        case 31:
        case 50:
        }
        while (true)
        {
            return bool;
            if (!canSelectText())
                break;
            bool = onTextContextMenuItem(16908319);
            continue;
            if (!canCut())
                break;
            bool = onTextContextMenuItem(16908320);
            continue;
            if (!canCopy())
                break;
            bool = onTextContextMenuItem(16908321);
            continue;
            if (!canPaste())
                break;
            bool = onTextContextMenuItem(16908322);
        }
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool;
        if (!isEnabled())
            bool = super.onKeyUp(paramInt, paramKeyEvent);
        while (true)
        {
            return bool;
            switch (paramInt)
            {
            default:
            case 23:
            case 66:
            }
            while (true)
                if ((this.mEditor != null) && (this.mEditor.mKeyListener != null) && (this.mEditor.mKeyListener.onKeyUp(this, (Editable)this.mText, paramInt, paramKeyEvent)))
                {
                    bool = true;
                    break;
                    if ((paramKeyEvent.hasNoModifiers()) && (!hasOnClickListeners()) && (this.mMovement != null) && ((this.mText instanceof Editable)) && (this.mLayout != null) && (onCheckIsTextEditor()))
                    {
                        InputMethodManager localInputMethodManager2 = InputMethodManager.peekInstance();
                        viewClicked(localInputMethodManager2);
                        if ((localInputMethodManager2 != null) && (getShowSoftInputOnFocus()))
                            localInputMethodManager2.showSoftInput(this, 0);
                    }
                    bool = super.onKeyUp(paramInt, paramKeyEvent);
                    break;
                    if (paramKeyEvent.hasNoModifiers())
                    {
                        if ((this.mEditor != null) && (this.mEditor.mInputContentType != null) && (this.mEditor.mInputContentType.onEditorActionListener != null) && (this.mEditor.mInputContentType.enterDown))
                        {
                            this.mEditor.mInputContentType.enterDown = false;
                            if (this.mEditor.mInputContentType.onEditorActionListener.onEditorAction(this, 0, paramKeyEvent))
                            {
                                bool = true;
                                break;
                            }
                        }
                        if ((((0x10 & paramKeyEvent.getFlags()) != 0) || (shouldAdvanceFocusOnEnter())) && (!hasOnClickListeners()))
                        {
                            View localView = focusSearch(130);
                            if (localView != null)
                            {
                                if (!localView.requestFocus(130))
                                    throw new IllegalStateException("focus search returned a view that wasn't able to take focus!");
                                super.onKeyUp(paramInt, paramKeyEvent);
                                bool = true;
                                break;
                            }
                            if ((0x10 & paramKeyEvent.getFlags()) != 0)
                            {
                                InputMethodManager localInputMethodManager1 = InputMethodManager.peekInstance();
                                if ((localInputMethodManager1 != null) && (localInputMethodManager1.isActive(this)))
                                    localInputMethodManager1.hideSoftInputFromWindow(getWindowToken(), 0);
                            }
                        }
                        bool = super.onKeyUp(paramInt, paramKeyEvent);
                        break;
                    }
                }
            if ((this.mMovement != null) && (this.mLayout != null) && (this.mMovement.onKeyUp(this, (Spannable)this.mText, paramInt, paramKeyEvent)))
                bool = true;
            else
                bool = super.onKeyUp(paramInt, paramKeyEvent);
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        if ((paramBoolean) && (this.mEditor != null))
            this.mEditor.invalidateTextDisplayList();
    }

    void onLocaleChanged()
    {
        this.mEditor.mWordIterator = null;
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt1);
        int m = View.MeasureSpec.getSize(paramInt2);
        BoringLayout.Metrics localMetrics1 = UNKNOWN_BORING;
        BoringLayout.Metrics localMetrics2 = UNKNOWN_BORING;
        if (this.mTextDir == null)
            resolveTextDirection();
        int n = -1;
        int i1 = 0;
        int i8;
        int i9;
        int i11;
        int i12;
        label106: int i17;
        if (i == 1073741824)
        {
            i8 = k;
            i9 = i8 - getCompoundPaddingLeft() - getCompoundPaddingRight();
            int i10 = i9;
            if (this.mHorizontallyScrolling)
                i9 = 1048576;
            i11 = i9;
            if (this.mHintLayout != null)
                break label671;
            i12 = i11;
            if (this.mLayout != null)
                break label683;
            makeNewLayout(i9, i11, localMetrics1, localMetrics2, i8 - getCompoundPaddingLeft() - getCompoundPaddingRight(), false);
            label138: if (j != 1073741824)
                break label886;
            i17 = m;
            this.mDesiredHeightAtMeasure = -1;
            label156: int i18 = i17 - getCompoundPaddingTop() - getCompoundPaddingBottom();
            if ((this.mMaxMode == 1) && (this.mLayout.getLineCount() > this.mMaximum))
            {
                int i19 = this.mLayout.getLineTop(this.mMaximum);
                i18 = Math.min(i18, i19);
            }
            if ((this.mMovement == null) && (this.mLayout.getWidth() <= i10) && (this.mLayout.getHeight() <= i18))
                break label922;
            registerForPreDraw();
        }
        while (true)
        {
            setMeasuredDimension(i8, i17);
            return;
            if ((this.mLayout != null) && (this.mEllipsize == null))
                n = desired(this.mLayout);
            label318: int i2;
            label357: int i22;
            label502: int i3;
            int i4;
            label551: int i20;
            if (n < 0)
            {
                localMetrics1 = BoringLayout.isBoring(this.mTransformed, this.mTextPaint, this.mTextDir, this.mBoring);
                if (localMetrics1 != null)
                    this.mBoring = localMetrics1;
                if ((localMetrics1 != null) && (localMetrics1 != UNKNOWN_BORING))
                    break label619;
                if (n < 0)
                    n = (int)FloatMath.ceil(Layout.getDesiredWidth(this.mTransformed, this.mTextPaint));
                i2 = n;
                Drawables localDrawables = this.mDrawables;
                if (localDrawables != null)
                {
                    int i23 = localDrawables.mDrawableWidthTop;
                    i2 = Math.max(Math.max(i2, i23), localDrawables.mDrawableWidthBottom);
                }
                if (this.mHint != null)
                {
                    int i21 = -1;
                    if ((this.mHintLayout != null) && (this.mEllipsize == null))
                        i21 = desired(this.mHintLayout);
                    if (i21 < 0)
                    {
                        localMetrics2 = BoringLayout.isBoring(this.mHint, this.mTextPaint, this.mTextDir, this.mHintBoring);
                        if (localMetrics2 != null)
                            this.mHintBoring = localMetrics2;
                    }
                    if ((localMetrics2 != null) && (localMetrics2 != UNKNOWN_BORING))
                        break label629;
                    if (i21 < 0)
                        i21 = (int)FloatMath.ceil(Layout.getDesiredWidth(this.mHint, this.mTextPaint));
                    i22 = i21;
                    if (i22 > i2)
                        i2 = i22;
                }
                i3 = i2 + (getCompoundPaddingLeft() + getCompoundPaddingRight());
                if (this.mMaxWidthMode != 1)
                    break label639;
                i4 = Math.min(i3, this.mMaxWidth * getLineHeight());
                if (this.mMinWidthMode != 1)
                    break label653;
                i20 = this.mMinWidth * getLineHeight();
            }
            label619: label629: label639: int i5;
            for (int i6 = Math.max(i4, i20); ; i6 = Math.max(i4, i5))
            {
                int i7 = getSuggestedMinimumWidth();
                i8 = Math.max(i6, i7);
                if (i != -2147483648)
                    break;
                i8 = Math.min(k, i8);
                break;
                i1 = 1;
                break label318;
                i2 = localMetrics1.width;
                break label357;
                i22 = localMetrics2.width;
                break label502;
                i4 = Math.min(i3, this.mMaxWidth);
                break label551;
                label653: i5 = this.mMinWidth;
            }
            label671: i12 = this.mHintLayout.getWidth();
            break label106;
            label683: int i13;
            label727: int i14;
            if ((this.mLayout.getWidth() != i9) || (i12 != i11) || (this.mLayout.getEllipsizedWidth() != i8 - getCompoundPaddingLeft() - getCompoundPaddingRight()))
            {
                i13 = 1;
                if ((this.mHint != null) || (this.mEllipsize != null) || (i9 <= this.mLayout.getWidth()) || ((!(this.mLayout instanceof BoringLayout)) && ((i1 == 0) || (n < 0) || (n > i9))))
                    break label846;
                i14 = 1;
                label783: if ((this.mMaxMode == this.mOldMaxMode) && (this.mMaximum == this.mOldMaximum))
                    break label852;
            }
            label846: label852: for (int i15 = 1; ; i15 = 0)
            {
                if ((i13 == 0) && (i15 == 0))
                    break label856;
                if ((i15 != 0) || (i14 == 0))
                    break label858;
                this.mLayout.increaseWidthTo(i9);
                break;
                i13 = 0;
                break label727;
                i14 = 0;
                break label783;
            }
            label856: break label138;
            label858: makeNewLayout(i9, i11, localMetrics1, localMetrics2, i8 - getCompoundPaddingLeft() - getCompoundPaddingRight(), false);
            break label138;
            label886: int i16 = getDesiredHeight();
            i17 = i16;
            this.mDesiredHeightAtMeasure = i16;
            if (j != -2147483648)
                break label156;
            i17 = Math.min(i16, m);
            break label156;
            label922: scrollTo(0, 0);
        }
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
        if ((!hasPasswordTransformationMethod()) || (shouldSpeakPasswordsForAccessibility()))
        {
            CharSequence localCharSequence = getTextForAccessibility();
            if (!TextUtils.isEmpty(localCharSequence))
                paramAccessibilityEvent.getText().add(localCharSequence);
        }
    }

    public boolean onPreDraw()
    {
        boolean bool1 = false;
        if (this.mLayout == null)
            assumeLayout();
        boolean bool2 = false;
        int i;
        if (this.mMovement != null)
        {
            i = getSelectionEnd();
            if ((this.mEditor != null) && (this.mEditor.mSelectionModifierCursorController != null) && (this.mEditor.mSelectionModifierCursorController.isSelectionStartDragged()))
                i = getSelectionStart();
            if ((i < 0) && ((0x70 & this.mGravity) == 80))
                i = this.mText.length();
            if (i < 0);
        }
        for (bool2 = bringPointIntoView(i); ; bool2 = bringTextIntoView())
        {
            if ((this.mEditor != null) && (this.mEditor.mCreatedWithASelection))
            {
                this.mEditor.startSelectionActionMode();
                this.mEditor.mCreatedWithASelection = false;
            }
            if (((this instanceof ExtractEditText)) && (hasSelection()) && (this.mEditor != null))
                this.mEditor.startSelectionActionMode();
            getViewTreeObserver().removeOnPreDrawListener(this);
            this.mPreDrawRegistered = false;
            if (!bool2)
                bool1 = true;
            return bool1;
        }
    }

    public boolean onPrivateIMECommand(String paramString, Bundle paramBundle)
    {
        return false;
    }

    public void onResolvedLayoutDirectionReset()
    {
        if (this.mLayoutAlignment != null)
        {
            int i = getResolvedTextAlignment();
            if ((i == 5) || (i == 6))
                this.mLayoutAlignment = null;
        }
    }

    public void onResolvedTextDirectionChanged()
    {
        int i = 1;
        if (hasPasswordTransformationMethod())
            this.mTextDir = TextDirectionHeuristics.LOCALE;
        while (true)
        {
            return;
            if (getResolvedLayoutDirection() == i);
            label25: switch (getResolvedTextDirection())
            {
            default:
                if (i != 0);
                for (TextDirectionHeuristic localTextDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL; ; localTextDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR)
                {
                    this.mTextDir = localTextDirectionHeuristic;
                    break;
                    i = 0;
                    break label25;
                }
            case 2:
                this.mTextDir = TextDirectionHeuristics.ANYRTL_LTR;
                break;
            case 3:
                this.mTextDir = TextDirectionHeuristics.LTR;
                break;
            case 4:
                this.mTextDir = TextDirectionHeuristics.RTL;
                break;
            case 5:
                this.mTextDir = TextDirectionHeuristics.LOCALE;
            }
        }
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if (!(paramParcelable instanceof SavedState))
            super.onRestoreInstanceState(paramParcelable);
        label203: label241: 
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            if (localSavedState.text != null)
                setText(localSavedState.text);
            if ((localSavedState.selStart >= 0) && (localSavedState.selEnd >= 0) && ((this.mText instanceof Spannable)))
            {
                int i = this.mText.length();
                if ((localSavedState.selStart <= i) && (localSavedState.selEnd <= i))
                    break label203;
                String str = "";
                if (localSavedState.text != null)
                    str = "(restored) ";
                Log.e("TextView", "Saved cursor position " + localSavedState.selStart + "/" + localSavedState.selEnd + " out of range for " + str + "text " + this.mText);
            }
            while (true)
            {
                if (localSavedState.error == null)
                    break label241;
                post(new Runnable()
                {
                    public void run()
                    {
                        TextView.this.setError(this.val$error);
                    }
                });
                break;
                Selection.setSelection((Spannable)this.mText, localSavedState.selStart, localSavedState.selEnd);
                if (localSavedState.frozenWithFocus)
                {
                    createEditorIfNeeded();
                    this.mEditor.mFrozenWithFocus = true;
                }
            }
        }
    }

    public Parcelable onSaveInstanceState()
    {
        Parcelable localParcelable = super.onSaveInstanceState();
        boolean bool = this.mFreezesText;
        int i = 0;
        int j = 0;
        if (this.mText != null)
        {
            i = getSelectionStart();
            j = getSelectionEnd();
            if ((i >= 0) || (j >= 0))
                bool = true;
        }
        Object localObject;
        if (bool)
        {
            localObject = new SavedState(localParcelable);
            ((SavedState)localObject).selStart = i;
            ((SavedState)localObject).selEnd = j;
            if ((this.mText instanceof Spanned))
            {
                SpannableString localSpannableString = new SpannableString(this.mText);
                ChangeWatcher[] arrayOfChangeWatcher = (ChangeWatcher[])localSpannableString.getSpans(0, localSpannableString.length(), ChangeWatcher.class);
                int k = arrayOfChangeWatcher.length;
                for (int m = 0; m < k; m++)
                    localSpannableString.removeSpan(arrayOfChangeWatcher[m]);
                if (this.mEditor != null)
                {
                    removeMisspelledSpans(localSpannableString);
                    localSpannableString.removeSpan(this.mEditor.mSuggestionRangeSpan);
                }
                ((SavedState)localObject).text = localSpannableString;
                if ((isFocused()) && (i >= 0) && (j >= 0))
                    ((SavedState)localObject).frozenWithFocus = true;
                ((SavedState)localObject).error = getError();
            }
        }
        while (true)
        {
            return localObject;
            ((SavedState)localObject).text = this.mText.toString();
            break;
            localObject = localParcelable;
        }
    }

    public void onScreenStateChanged(int paramInt)
    {
        super.onScreenStateChanged(paramInt);
        if (this.mEditor != null)
            this.mEditor.onScreenStateChanged(paramInt);
    }

    protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        if (this.mEditor != null)
            this.mEditor.onScrollChanged();
    }

    protected void onSelectionChanged(int paramInt1, int paramInt2)
    {
        sendAccessibilityEvent(8192);
    }

    public void onStartTemporaryDetach()
    {
        super.onStartTemporaryDetach();
        if (!this.mDispatchTemporaryDetach)
            this.mTemporaryDetach = true;
        if (this.mEditor != null)
            this.mEditor.mTemporaryDetach = true;
    }

    protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public boolean onTextContextMenuItem(int paramInt)
    {
        boolean bool = true;
        int i = 0;
        int j = this.mText.length();
        if (isFocused())
        {
            int k = getSelectionStart();
            int m = getSelectionEnd();
            i = Math.max(0, Math.min(k, m));
            j = Math.max(0, Math.max(k, m));
        }
        switch (paramInt)
        {
        default:
            bool = false;
        case 16908319:
        case 16908322:
        case 16908320:
        case 16908321:
        }
        while (true)
        {
            return bool;
            selectAllText();
            continue;
            paste(i, j);
            continue;
            setPrimaryClip(ClipData.newPlainText(null, getTransformedText(i, j)));
            deleteText_internal(i, j);
            stopSelectionActionMode();
            continue;
            setPrimaryClip(ClipData.newPlainText(null, getTransformedText(i, j)));
            stopSelectionActionMode();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool1 = false;
        int i = 1;
        int j = paramMotionEvent.getActionMasked();
        if ((this.mEditor != null) && (this.mEditor.onTouchEvent(paramMotionEvent)));
        int k;
        while (true)
        {
            return i;
            k = super.onTouchEvent(paramMotionEvent);
            if ((this.mEditor == null) || (!this.mEditor.mDiscardNextActionUp) || (j != i))
                break;
            this.mEditor.mDiscardNextActionUp = false;
            i = k;
        }
        if ((j == i) && ((this.mEditor == null) || (!this.mEditor.mIgnoreActionUpEvent)) && (isFocused()));
        int n;
        for (int m = i; ; n = 0)
        {
            if (((this.mMovement != null) || (onCheckIsTextEditor())) && (isEnabled()) && ((this.mText instanceof Spannable)) && (this.mLayout != null))
            {
                boolean bool2 = false;
                if (this.mMovement != null)
                    bool2 = false | this.mMovement.onTouchEvent(this, (Spannable)this.mText, paramMotionEvent);
                boolean bool3 = isTextSelectable();
                if ((m != 0) && (this.mLinksClickable) && (this.mAutoLinkMask != 0) && (bool3))
                {
                    ClickableSpan[] arrayOfClickableSpan = (ClickableSpan[])((Spannable)this.mText).getSpans(getSelectionStart(), getSelectionEnd(), ClickableSpan.class);
                    if (arrayOfClickableSpan.length > 0)
                    {
                        arrayOfClickableSpan[0].onClick(this);
                        bool2 = true;
                    }
                }
                if ((m != 0) && ((isTextEditable()) || (bool3)))
                {
                    InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                    viewClicked(localInputMethodManager);
                    if ((!bool3) && (this.mEditor.mShowSoftInputOnFocus))
                    {
                        if ((localInputMethodManager != null) && (localInputMethodManager.showSoftInput(this, 0)))
                            bool1 = i;
                        (bool2 | bool1);
                    }
                    this.mEditor.onTouchUpEvent(paramMotionEvent);
                    bool2 = true;
                }
                if (bool2)
                    break;
            }
            i = k;
            break;
        }
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        if ((this.mMovement != null) && ((this.mText instanceof Spannable)) && (this.mLayout != null) && (this.mMovement.onTrackballEvent(this, (Spannable)this.mText, paramMotionEvent)));
        for (boolean bool = true; ; bool = super.onTrackballEvent(paramMotionEvent))
            return bool;
    }

    protected void onVisibilityChanged(View paramView, int paramInt)
    {
        super.onVisibilityChanged(paramView, paramInt);
        if ((this.mEditor != null) && (paramInt != 0))
            this.mEditor.hideControllers();
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        super.onWindowFocusChanged(paramBoolean);
        if (this.mEditor != null)
            this.mEditor.onWindowFocusChanged(paramBoolean);
        startStopMarquee(paramBoolean);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean performLongClick()
    {
        boolean bool = false;
        if (super.performLongClick())
        {
            if (this.mEditor != null)
                this.mEditor.mDiscardNextActionUp = true;
            performHapticFeedback(0);
            bool = true;
        }
        if ((!bool) && (this.mEditor != null))
            bool |= this.mEditor.performLongClick(bool);
        return bool;
    }

    long prepareSpacesAroundPaste(int paramInt1, int paramInt2, CharSequence paramCharSequence)
    {
        char c3;
        char c4;
        char c1;
        char c2;
        if (paramCharSequence.length() > 0)
        {
            if (paramInt1 > 0)
            {
                c3 = this.mTransformed.charAt(paramInt1 - 1);
                c4 = paramCharSequence.charAt(0);
                if ((!Character.isSpaceChar(c3)) || (!Character.isSpaceChar(c4)))
                    break label167;
                int k = this.mText.length();
                deleteText_internal(paramInt1 - 1, paramInt1);
                int m = this.mText.length() - k;
                paramInt1 += m;
                paramInt2 += m;
            }
            if (paramInt2 < this.mText.length())
            {
                c1 = paramCharSequence.charAt(-1 + paramCharSequence.length());
                c2 = this.mTransformed.charAt(paramInt2);
                if ((!Character.isSpaceChar(c1)) || (!Character.isSpaceChar(c2)))
                    break label244;
                deleteText_internal(paramInt2, paramInt2 + 1);
            }
        }
        while (true)
        {
            return TextUtils.packRangeInLong(paramInt1, paramInt2);
            label167: if ((Character.isSpaceChar(c3)) || (c3 == '\n') || (Character.isSpaceChar(c4)) || (c4 == '\n'))
                break;
            int i = this.mText.length();
            replaceText_internal(paramInt1, paramInt1, " ");
            int j = this.mText.length() - i;
            paramInt1 += j;
            paramInt2 += j;
            break;
            label244: if ((!Character.isSpaceChar(c1)) && (c1 != '\n') && (!Character.isSpaceChar(c2)) && (c2 != '\n'))
                replaceText_internal(paramInt2, paramInt2, " ");
        }
    }

    void removeMisspelledSpans(Spannable paramSpannable)
    {
        SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])paramSpannable.getSpans(0, paramSpannable.length(), SuggestionSpan.class);
        for (int i = 0; i < arrayOfSuggestionSpan.length; i++)
        {
            int j = arrayOfSuggestionSpan[i].getFlags();
            if (((j & 0x1) != 0) && ((j & 0x2) != 0))
                paramSpannable.removeSpan(arrayOfSuggestionSpan[i]);
        }
    }

    CharSequence removeSuggestionSpans(CharSequence paramCharSequence)
    {
        if ((paramCharSequence instanceof Spanned))
        {
            Object localObject;
            if ((paramCharSequence instanceof Spannable))
                localObject = (Spannable)paramCharSequence;
            while (true)
            {
                SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])((Spannable)localObject).getSpans(0, paramCharSequence.length(), SuggestionSpan.class);
                for (int i = 0; i < arrayOfSuggestionSpan.length; i++)
                    ((Spannable)localObject).removeSpan(arrayOfSuggestionSpan[i]);
                localObject = new SpannableString(paramCharSequence);
                paramCharSequence = (CharSequence)localObject;
            }
        }
        return paramCharSequence;
    }

    public void removeTextChangedListener(TextWatcher paramTextWatcher)
    {
        if (this.mListeners != null)
        {
            int i = this.mListeners.indexOf(paramTextWatcher);
            if (i >= 0)
                this.mListeners.remove(i);
        }
    }

    protected void replaceText_internal(int paramInt1, int paramInt2, CharSequence paramCharSequence)
    {
        ((Editable)this.mText).replace(paramInt1, paramInt2, paramCharSequence);
    }

    public void resetErrorChangedFlag()
    {
        if (this.mEditor != null)
            this.mEditor.mErrorWasChanged = false;
    }

    protected void resetResolvedDrawables()
    {
        this.mResolvedDrawables = false;
    }

    protected void resolveDrawables()
    {
        if (this.mResolvedDrawables);
        while (true)
        {
            return;
            if (this.mDrawables != null)
            {
                if ((this.mDrawables.mDrawableStart != null) || (this.mDrawables.mDrawableEnd != null))
                    break;
                this.mResolvedDrawables = true;
            }
        }
        Drawables localDrawables = this.mDrawables;
        switch (getResolvedLayoutDirection())
        {
        default:
            if (localDrawables.mDrawableStart != null)
            {
                localDrawables.mDrawableLeft = localDrawables.mDrawableStart;
                localDrawables.mDrawableSizeLeft = localDrawables.mDrawableSizeStart;
                localDrawables.mDrawableHeightLeft = localDrawables.mDrawableHeightStart;
            }
            if (localDrawables.mDrawableEnd != null)
            {
                localDrawables.mDrawableRight = localDrawables.mDrawableEnd;
                localDrawables.mDrawableSizeRight = localDrawables.mDrawableSizeEnd;
                localDrawables.mDrawableHeightRight = localDrawables.mDrawableHeightEnd;
            }
            break;
        case 1:
        }
        while (true)
        {
            this.mResolvedDrawables = true;
            break;
            if (localDrawables.mDrawableStart != null)
            {
                localDrawables.mDrawableRight = localDrawables.mDrawableStart;
                localDrawables.mDrawableSizeRight = localDrawables.mDrawableSizeStart;
                localDrawables.mDrawableHeightRight = localDrawables.mDrawableHeightStart;
            }
            if (localDrawables.mDrawableEnd != null)
            {
                localDrawables.mDrawableLeft = localDrawables.mDrawableEnd;
                localDrawables.mDrawableSizeLeft = localDrawables.mDrawableSizeEnd;
                localDrawables.mDrawableHeightLeft = localDrawables.mDrawableHeightEnd;
            }
        }
    }

    boolean selectAllText()
    {
        int i = this.mText.length();
        Selection.setSelection((Spannable)this.mText, 0, i);
        if (i > 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void sendAccessibilityEvent(int paramInt)
    {
        if (paramInt == 4096);
        while (true)
        {
            return;
            super.sendAccessibilityEvent(paramInt);
        }
    }

    void sendAccessibilityEventTypeViewTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(16);
        localAccessibilityEvent.setFromIndex(paramInt1);
        localAccessibilityEvent.setRemovedCount(paramInt2);
        localAccessibilityEvent.setAddedCount(paramInt3);
        localAccessibilityEvent.setBeforeText(paramCharSequence);
        sendAccessibilityEventUnchecked(localAccessibilityEvent);
    }

    void sendAfterTextChanged(Editable paramEditable)
    {
        if (this.mListeners != null)
        {
            ArrayList localArrayList = this.mListeners;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((TextWatcher)localArrayList.get(j)).afterTextChanged(paramEditable);
        }
    }

    void sendOnTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mListeners != null)
        {
            ArrayList localArrayList = this.mListeners;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((TextWatcher)localArrayList.get(j)).onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
        }
        if (this.mEditor != null)
            this.mEditor.sendOnTextChanged(paramInt1, paramInt3);
    }

    public void setAccessibilityCursorPosition(int paramInt)
    {
        if (getAccessibilityCursorPosition() == paramInt);
        while (true)
        {
            return;
            if (TextUtils.isEmpty(getContentDescription()))
            {
                if ((paramInt >= 0) && (paramInt <= this.mText.length()))
                    Selection.setSelection((Spannable)this.mText, paramInt);
                else
                    Selection.removeSelection((Spannable)this.mText);
            }
            else
                super.setAccessibilityCursorPosition(paramInt);
        }
    }

    public void setAllCaps(boolean paramBoolean)
    {
        if (paramBoolean)
            setTransformationMethod(new AllCapsTransformationMethod(getContext()));
        while (true)
        {
            return;
            setTransformationMethod(null);
        }
    }

    @RemotableViewMethod
    public final void setAutoLinkMask(int paramInt)
    {
        this.mAutoLinkMask = paramInt;
    }

    @RemotableViewMethod
    public void setCompoundDrawablePadding(int paramInt)
    {
        Drawables localDrawables = this.mDrawables;
        if (paramInt == 0)
            if (localDrawables == null);
        for (localDrawables.mDrawablePadding = paramInt; ; localDrawables.mDrawablePadding = paramInt)
        {
            invalidate();
            requestLayout();
            return;
            if (localDrawables == null)
            {
                localDrawables = new Drawables();
                this.mDrawables = localDrawables;
            }
        }
    }

    public void setCompoundDrawables(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4)
    {
        Drawables localDrawables = this.mDrawables;
        int i;
        if ((paramDrawable1 != null) || (paramDrawable2 != null) || (paramDrawable3 != null) || (paramDrawable4 != null))
        {
            i = 1;
            if (i != 0)
                break label207;
            if (localDrawables != null)
            {
                if (localDrawables.mDrawablePadding != 0)
                    break label64;
                this.mDrawables = null;
            }
        }
        while (true)
        {
            invalidate();
            requestLayout();
            return;
            i = 0;
            break;
            label64: if (localDrawables.mDrawableLeft != null)
                localDrawables.mDrawableLeft.setCallback(null);
            localDrawables.mDrawableLeft = null;
            if (localDrawables.mDrawableTop != null)
                localDrawables.mDrawableTop.setCallback(null);
            localDrawables.mDrawableTop = null;
            if (localDrawables.mDrawableRight != null)
                localDrawables.mDrawableRight.setCallback(null);
            localDrawables.mDrawableRight = null;
            if (localDrawables.mDrawableBottom != null)
                localDrawables.mDrawableBottom.setCallback(null);
            localDrawables.mDrawableBottom = null;
            localDrawables.mDrawableHeightLeft = 0;
            localDrawables.mDrawableSizeLeft = 0;
            localDrawables.mDrawableHeightRight = 0;
            localDrawables.mDrawableSizeRight = 0;
            localDrawables.mDrawableWidthTop = 0;
            localDrawables.mDrawableSizeTop = 0;
            localDrawables.mDrawableWidthBottom = 0;
            localDrawables.mDrawableSizeBottom = 0;
            continue;
            label207: if (localDrawables == null)
            {
                localDrawables = new Drawables();
                this.mDrawables = localDrawables;
            }
            if ((localDrawables.mDrawableLeft != paramDrawable1) && (localDrawables.mDrawableLeft != null))
                localDrawables.mDrawableLeft.setCallback(null);
            localDrawables.mDrawableLeft = paramDrawable1;
            if ((localDrawables.mDrawableTop != paramDrawable2) && (localDrawables.mDrawableTop != null))
                localDrawables.mDrawableTop.setCallback(null);
            localDrawables.mDrawableTop = paramDrawable2;
            if ((localDrawables.mDrawableRight != paramDrawable3) && (localDrawables.mDrawableRight != null))
                localDrawables.mDrawableRight.setCallback(null);
            localDrawables.mDrawableRight = paramDrawable3;
            if ((localDrawables.mDrawableBottom != paramDrawable4) && (localDrawables.mDrawableBottom != null))
                localDrawables.mDrawableBottom.setCallback(null);
            localDrawables.mDrawableBottom = paramDrawable4;
            Rect localRect = localDrawables.mCompoundRect;
            int[] arrayOfInt = getDrawableState();
            if (paramDrawable1 != null)
            {
                paramDrawable1.setState(arrayOfInt);
                paramDrawable1.copyBounds(localRect);
                paramDrawable1.setCallback(this);
                localDrawables.mDrawableSizeLeft = localRect.width();
                localDrawables.mDrawableHeightLeft = localRect.height();
                label412: if (paramDrawable3 == null)
                    break label560;
                paramDrawable3.setState(arrayOfInt);
                paramDrawable3.copyBounds(localRect);
                paramDrawable3.setCallback(this);
                localDrawables.mDrawableSizeRight = localRect.width();
                localDrawables.mDrawableHeightRight = localRect.height();
                label454: if (paramDrawable2 == null)
                    break label575;
                paramDrawable2.setState(arrayOfInt);
                paramDrawable2.copyBounds(localRect);
                paramDrawable2.setCallback(this);
                localDrawables.mDrawableSizeTop = localRect.height();
                localDrawables.mDrawableWidthTop = localRect.width();
            }
            while (true)
            {
                if (paramDrawable4 == null)
                    break label590;
                paramDrawable4.setState(arrayOfInt);
                paramDrawable4.copyBounds(localRect);
                paramDrawable4.setCallback(this);
                localDrawables.mDrawableSizeBottom = localRect.height();
                localDrawables.mDrawableWidthBottom = localRect.width();
                break;
                localDrawables.mDrawableHeightLeft = 0;
                localDrawables.mDrawableSizeLeft = 0;
                break label412;
                label560: localDrawables.mDrawableHeightRight = 0;
                localDrawables.mDrawableSizeRight = 0;
                break label454;
                label575: localDrawables.mDrawableWidthTop = 0;
                localDrawables.mDrawableSizeTop = 0;
            }
            label590: localDrawables.mDrawableWidthBottom = 0;
            localDrawables.mDrawableSizeBottom = 0;
        }
    }

    public void setCompoundDrawablesRelative(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4)
    {
        Drawables localDrawables = this.mDrawables;
        int i;
        if ((paramDrawable1 != null) || (paramDrawable2 != null) || (paramDrawable3 != null) || (paramDrawable4 != null))
        {
            i = 1;
            if (i != 0)
                break label211;
            if (localDrawables != null)
            {
                if (localDrawables.mDrawablePadding != 0)
                    break label68;
                this.mDrawables = null;
            }
        }
        while (true)
        {
            resolveDrawables();
            invalidate();
            requestLayout();
            return;
            i = 0;
            break;
            label68: if (localDrawables.mDrawableStart != null)
                localDrawables.mDrawableStart.setCallback(null);
            localDrawables.mDrawableStart = null;
            if (localDrawables.mDrawableTop != null)
                localDrawables.mDrawableTop.setCallback(null);
            localDrawables.mDrawableTop = null;
            if (localDrawables.mDrawableEnd != null)
                localDrawables.mDrawableEnd.setCallback(null);
            localDrawables.mDrawableEnd = null;
            if (localDrawables.mDrawableBottom != null)
                localDrawables.mDrawableBottom.setCallback(null);
            localDrawables.mDrawableBottom = null;
            localDrawables.mDrawableHeightStart = 0;
            localDrawables.mDrawableSizeStart = 0;
            localDrawables.mDrawableHeightEnd = 0;
            localDrawables.mDrawableSizeEnd = 0;
            localDrawables.mDrawableWidthTop = 0;
            localDrawables.mDrawableSizeTop = 0;
            localDrawables.mDrawableWidthBottom = 0;
            localDrawables.mDrawableSizeBottom = 0;
            continue;
            label211: if (localDrawables == null)
            {
                localDrawables = new Drawables();
                this.mDrawables = localDrawables;
            }
            if ((localDrawables.mDrawableStart != paramDrawable1) && (localDrawables.mDrawableStart != null))
                localDrawables.mDrawableStart.setCallback(null);
            localDrawables.mDrawableStart = paramDrawable1;
            if ((localDrawables.mDrawableTop != paramDrawable2) && (localDrawables.mDrawableTop != null))
                localDrawables.mDrawableTop.setCallback(null);
            localDrawables.mDrawableTop = paramDrawable2;
            if ((localDrawables.mDrawableEnd != paramDrawable3) && (localDrawables.mDrawableEnd != null))
                localDrawables.mDrawableEnd.setCallback(null);
            localDrawables.mDrawableEnd = paramDrawable3;
            if ((localDrawables.mDrawableBottom != paramDrawable4) && (localDrawables.mDrawableBottom != null))
                localDrawables.mDrawableBottom.setCallback(null);
            localDrawables.mDrawableBottom = paramDrawable4;
            Rect localRect = localDrawables.mCompoundRect;
            int[] arrayOfInt = getDrawableState();
            if (paramDrawable1 != null)
            {
                paramDrawable1.setState(arrayOfInt);
                paramDrawable1.copyBounds(localRect);
                paramDrawable1.setCallback(this);
                localDrawables.mDrawableSizeStart = localRect.width();
                localDrawables.mDrawableHeightStart = localRect.height();
                label416: if (paramDrawable3 == null)
                    break label564;
                paramDrawable3.setState(arrayOfInt);
                paramDrawable3.copyBounds(localRect);
                paramDrawable3.setCallback(this);
                localDrawables.mDrawableSizeEnd = localRect.width();
                localDrawables.mDrawableHeightEnd = localRect.height();
                label458: if (paramDrawable2 == null)
                    break label579;
                paramDrawable2.setState(arrayOfInt);
                paramDrawable2.copyBounds(localRect);
                paramDrawable2.setCallback(this);
                localDrawables.mDrawableSizeTop = localRect.height();
                localDrawables.mDrawableWidthTop = localRect.width();
            }
            while (true)
            {
                if (paramDrawable4 == null)
                    break label594;
                paramDrawable4.setState(arrayOfInt);
                paramDrawable4.copyBounds(localRect);
                paramDrawable4.setCallback(this);
                localDrawables.mDrawableSizeBottom = localRect.height();
                localDrawables.mDrawableWidthBottom = localRect.width();
                break;
                localDrawables.mDrawableHeightStart = 0;
                localDrawables.mDrawableSizeStart = 0;
                break label416;
                label564: localDrawables.mDrawableHeightEnd = 0;
                localDrawables.mDrawableSizeEnd = 0;
                break label458;
                label579: localDrawables.mDrawableWidthTop = 0;
                localDrawables.mDrawableSizeTop = 0;
            }
            label594: localDrawables.mDrawableWidthBottom = 0;
            localDrawables.mDrawableSizeBottom = 0;
        }
    }

    @RemotableViewMethod
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Drawable localDrawable1 = null;
        resetResolvedDrawables();
        Resources localResources = getContext().getResources();
        Drawable localDrawable2;
        Drawable localDrawable3;
        if (paramInt1 != 0)
        {
            localDrawable2 = localResources.getDrawable(paramInt1);
            if (paramInt2 == 0)
                break label85;
            localDrawable3 = localResources.getDrawable(paramInt2);
            label40: if (paramInt3 == 0)
                break label91;
        }
        label85: label91: for (Drawable localDrawable4 = localResources.getDrawable(paramInt3); ; localDrawable4 = null)
        {
            if (paramInt4 != 0)
                localDrawable1 = localResources.getDrawable(paramInt4);
            setCompoundDrawablesRelativeWithIntrinsicBounds(localDrawable2, localDrawable3, localDrawable4, localDrawable1);
            return;
            localDrawable2 = null;
            break;
            localDrawable3 = null;
            break label40;
        }
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4)
    {
        resetResolvedDrawables();
        if (paramDrawable1 != null)
            paramDrawable1.setBounds(0, 0, paramDrawable1.getIntrinsicWidth(), paramDrawable1.getIntrinsicHeight());
        if (paramDrawable3 != null)
            paramDrawable3.setBounds(0, 0, paramDrawable3.getIntrinsicWidth(), paramDrawable3.getIntrinsicHeight());
        if (paramDrawable2 != null)
            paramDrawable2.setBounds(0, 0, paramDrawable2.getIntrinsicWidth(), paramDrawable2.getIntrinsicHeight());
        if (paramDrawable4 != null)
            paramDrawable4.setBounds(0, 0, paramDrawable4.getIntrinsicWidth(), paramDrawable4.getIntrinsicHeight());
        setCompoundDrawablesRelative(paramDrawable1, paramDrawable2, paramDrawable3, paramDrawable4);
    }

    @RemotableViewMethod
    public void setCompoundDrawablesWithIntrinsicBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Drawable localDrawable1 = null;
        Resources localResources = getContext().getResources();
        Drawable localDrawable2;
        Drawable localDrawable3;
        if (paramInt1 != 0)
        {
            localDrawable2 = localResources.getDrawable(paramInt1);
            if (paramInt2 == 0)
                break label81;
            localDrawable3 = localResources.getDrawable(paramInt2);
            label36: if (paramInt3 == 0)
                break label87;
        }
        label81: label87: for (Drawable localDrawable4 = localResources.getDrawable(paramInt3); ; localDrawable4 = null)
        {
            if (paramInt4 != 0)
                localDrawable1 = localResources.getDrawable(paramInt4);
            setCompoundDrawablesWithIntrinsicBounds(localDrawable2, localDrawable3, localDrawable4, localDrawable1);
            return;
            localDrawable2 = null;
            break;
            localDrawable3 = null;
            break label36;
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4)
    {
        if (paramDrawable1 != null)
            paramDrawable1.setBounds(0, 0, paramDrawable1.getIntrinsicWidth(), paramDrawable1.getIntrinsicHeight());
        if (paramDrawable3 != null)
            paramDrawable3.setBounds(0, 0, paramDrawable3.getIntrinsicWidth(), paramDrawable3.getIntrinsicHeight());
        if (paramDrawable2 != null)
            paramDrawable2.setBounds(0, 0, paramDrawable2.getIntrinsicWidth(), paramDrawable2.getIntrinsicHeight());
        if (paramDrawable4 != null)
            paramDrawable4.setBounds(0, 0, paramDrawable4.getIntrinsicWidth(), paramDrawable4.getIntrinsicHeight());
        setCompoundDrawables(paramDrawable1, paramDrawable2, paramDrawable3, paramDrawable4);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setCursorDrawableRes(int paramInt)
    {
        this.mCursorDrawableRes = paramInt;
        for (int i = 0; i < this.mEditor.mCursorCount; i++)
            this.mEditor.mCursorDrawable[i] = null;
        this.mEditor.mCursorCount = 0;
    }

    protected void setCursorPosition_internal(int paramInt1, int paramInt2)
    {
        Selection.setSelection((Editable)this.mText, paramInt1, paramInt2);
    }

    @RemotableViewMethod
    public void setCursorVisible(boolean paramBoolean)
    {
        if ((paramBoolean) && (this.mEditor == null));
        while (true)
        {
            return;
            createEditorIfNeeded();
            if (this.mEditor.mCursorVisible != paramBoolean)
            {
                this.mEditor.mCursorVisible = paramBoolean;
                invalidate();
                this.mEditor.makeBlink();
                this.mEditor.prepareCursorControllers();
            }
        }
    }

    public void setCustomSelectionActionModeCallback(ActionMode.Callback paramCallback)
    {
        createEditorIfNeeded();
        this.mEditor.mCustomSelectionActionModeCallback = paramCallback;
    }

    public final void setEditableFactory(Editable.Factory paramFactory)
    {
        this.mEditableFactory = paramFactory;
        setText(this.mText);
    }

    public void setEllipsize(TextUtils.TruncateAt paramTruncateAt)
    {
        if (this.mEllipsize != paramTruncateAt)
        {
            this.mEllipsize = paramTruncateAt;
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    @RemotableViewMethod
    public void setEms(int paramInt)
    {
        this.mMinWidth = paramInt;
        this.mMaxWidth = paramInt;
        this.mMinWidthMode = 1;
        this.mMaxWidthMode = 1;
        requestLayout();
        invalidate();
    }

    public void setEnabled(boolean paramBoolean)
    {
        if (paramBoolean == isEnabled());
        while (true)
        {
            return;
            if (!paramBoolean)
            {
                InputMethodManager localInputMethodManager2 = InputMethodManager.peekInstance();
                if ((localInputMethodManager2 != null) && (localInputMethodManager2.isActive(this)))
                    localInputMethodManager2.hideSoftInputFromWindow(getWindowToken(), 0);
            }
            super.setEnabled(paramBoolean);
            if (paramBoolean)
            {
                InputMethodManager localInputMethodManager1 = InputMethodManager.peekInstance();
                if (localInputMethodManager1 != null)
                    localInputMethodManager1.restartInput(this);
            }
            if (this.mEditor != null)
            {
                this.mEditor.invalidateTextDisplayList();
                this.mEditor.prepareCursorControllers();
                this.mEditor.makeBlink();
            }
        }
    }

    @RemotableViewMethod
    public void setError(CharSequence paramCharSequence)
    {
        if (paramCharSequence == null)
            setError(null, null);
        while (true)
        {
            return;
            Drawable localDrawable = getContext().getResources().getDrawable(17302388);
            localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
            setError(paramCharSequence, localDrawable);
        }
    }

    public void setError(CharSequence paramCharSequence, Drawable paramDrawable)
    {
        createEditorIfNeeded();
        this.mEditor.setError(paramCharSequence, paramDrawable);
    }

    public void setExtractedText(ExtractedText paramExtractedText)
    {
        Editable localEditable = getEditableText();
        Spannable localSpannable;
        int i;
        int j;
        label57: int k;
        if (paramExtractedText.text != null)
        {
            if (localEditable == null)
                setText(paramExtractedText.text, BufferType.EDITABLE);
        }
        else
        {
            localSpannable = (Spannable)getText();
            i = localSpannable.length();
            j = paramExtractedText.selectionStart;
            if (j >= 0)
                break label201;
            j = 0;
            k = paramExtractedText.selectionEnd;
            if (k >= 0)
                break label215;
            k = 0;
            label71: Selection.setSelection(localSpannable, j, k);
            if ((0x2 & paramExtractedText.flags) == 0)
                break label229;
            MetaKeyKeyListener.startSelecting(this, localSpannable);
        }
        while (true)
        {
            return;
            if (paramExtractedText.partialStartOffset < 0)
            {
                removeParcelableSpans(localEditable, 0, localEditable.length());
                localEditable.replace(0, localEditable.length(), paramExtractedText.text);
                break;
            }
            int m = localEditable.length();
            int n = paramExtractedText.partialStartOffset;
            if (n > m)
                n = m;
            int i1 = paramExtractedText.partialEndOffset;
            if (i1 > m)
                i1 = m;
            removeParcelableSpans(localEditable, n, i1);
            localEditable.replace(n, i1, paramExtractedText.text);
            break;
            label201: if (j <= i)
                break label57;
            j = i;
            break label57;
            label215: if (k <= i)
                break label71;
            k = i;
            break label71;
            label229: MetaKeyKeyListener.stopSelecting(this, localSpannable);
        }
    }

    public void setExtracting(ExtractedTextRequest paramExtractedTextRequest)
    {
        if (this.mEditor.mInputMethodState != null)
            this.mEditor.mInputMethodState.mExtractedTextRequest = paramExtractedTextRequest;
        this.mEditor.hideControllers();
    }

    public void setFilters(InputFilter[] paramArrayOfInputFilter)
    {
        if (paramArrayOfInputFilter == null)
            throw new IllegalArgumentException();
        this.mFilters = paramArrayOfInputFilter;
        if ((this.mText instanceof Editable))
            setFilters((Editable)this.mText, paramArrayOfInputFilter);
    }

    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        if (this.mEditor != null)
            this.mEditor.setFrame();
        restartMarqueeIfNeeded();
        return bool;
    }

    @RemotableViewMethod
    public void setFreezesText(boolean paramBoolean)
    {
        this.mFreezesText = paramBoolean;
    }

    public void setGravity(int paramInt)
    {
        if ((paramInt & 0x800007) == 0)
            paramInt |= 8388611;
        if ((paramInt & 0x70) == 0)
            paramInt |= 48;
        int i = 0;
        if ((paramInt & 0x800007) != (0x800007 & this.mGravity))
            i = 1;
        if (paramInt != this.mGravity)
        {
            invalidate();
            this.mLayoutAlignment = null;
        }
        this.mGravity = paramInt;
        int j;
        if ((this.mLayout != null) && (i != 0))
        {
            j = this.mLayout.getWidth();
            if (this.mHintLayout != null)
                break label131;
        }
        label131: for (int k = 0; ; k = this.mHintLayout.getWidth())
        {
            makeNewLayout(j, k, UNKNOWN_BORING, UNKNOWN_BORING, this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight(), true);
            return;
        }
    }

    @RemotableViewMethod
    public void setHeight(int paramInt)
    {
        this.mMinimum = paramInt;
        this.mMaximum = paramInt;
        this.mMinMode = 2;
        this.mMaxMode = 2;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setHighlightColor(int paramInt)
    {
        if (this.mHighlightColor != paramInt)
        {
            this.mHighlightColor = paramInt;
            invalidate();
        }
    }

    @RemotableViewMethod
    public final void setHint(int paramInt)
    {
        setHint(getContext().getResources().getText(paramInt));
    }

    @RemotableViewMethod
    public final void setHint(CharSequence paramCharSequence)
    {
        this.mHint = TextUtils.stringOrSpannedString(paramCharSequence);
        if (this.mLayout != null)
            checkForRelayout();
        if (this.mText.length() == 0)
            invalidate();
        if ((this.mEditor != null) && (this.mText.length() == 0) && (this.mHint != null))
            this.mEditor.invalidateTextDisplayList();
    }

    @RemotableViewMethod
    public final void setHintTextColor(int paramInt)
    {
        this.mHintTextColor = ColorStateList.valueOf(paramInt);
        updateTextColors();
    }

    public final void setHintTextColor(ColorStateList paramColorStateList)
    {
        this.mHintTextColor = paramColorStateList;
        updateTextColors();
    }

    public void setHorizontallyScrolling(boolean paramBoolean)
    {
        if (this.mHorizontallyScrolling != paramBoolean)
        {
            this.mHorizontallyScrolling = paramBoolean;
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    public void setImeActionLabel(CharSequence paramCharSequence, int paramInt)
    {
        createEditorIfNeeded();
        this.mEditor.createInputContentTypeIfNeeded();
        this.mEditor.mInputContentType.imeActionLabel = paramCharSequence;
        this.mEditor.mInputContentType.imeActionId = paramInt;
    }

    public void setImeOptions(int paramInt)
    {
        createEditorIfNeeded();
        this.mEditor.createInputContentTypeIfNeeded();
        this.mEditor.mInputContentType.imeOptions = paramInt;
    }

    public void setIncludeFontPadding(boolean paramBoolean)
    {
        if (this.mIncludePad != paramBoolean)
        {
            this.mIncludePad = paramBoolean;
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    public void setInputExtras(int paramInt)
        throws XmlPullParserException, IOException
    {
        createEditorIfNeeded();
        XmlResourceParser localXmlResourceParser = getResources().getXml(paramInt);
        this.mEditor.createInputContentTypeIfNeeded();
        this.mEditor.mInputContentType.extras = new Bundle();
        getResources().parseBundleExtras(localXmlResourceParser, this.mEditor.mInputContentType.extras);
    }

    public void setInputType(int paramInt)
    {
        boolean bool1 = false;
        boolean bool2 = isPasswordInputType(getInputType());
        boolean bool3 = isVisiblePasswordInputType(getInputType());
        setInputType(paramInt, false);
        boolean bool4 = isPasswordInputType(paramInt);
        boolean bool5 = isVisiblePasswordInputType(paramInt);
        int i = 0;
        if (bool4)
        {
            setTransformationMethod(PasswordTransformationMethod.getInstance());
            setTypefaceFromAttrs(null, 3, 0);
            if (isMultilineInputType(paramInt))
                break label196;
        }
        label196: for (boolean bool6 = true; ; bool6 = false)
        {
            if ((this.mSingleLine != bool6) || (i != 0))
            {
                if (!bool4)
                    bool1 = true;
                applySingleLine(bool6, bool1, true);
            }
            if (!isSuggestionsEnabled())
                this.mText = removeSuggestionSpans(this.mText);
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if (localInputMethodManager != null)
                localInputMethodManager.restartInput(this);
            return;
            if (bool5)
            {
                if (this.mTransformation == PasswordTransformationMethod.getInstance())
                    i = 1;
                setTypefaceFromAttrs(null, 3, 0);
                break;
            }
            if ((!bool2) && (!bool3))
                break;
            setTypefaceFromAttrs(null, -1, -1);
            if (this.mTransformation != PasswordTransformationMethod.getInstance())
                break;
            i = 1;
            break;
        }
    }

    public void setKeyListener(KeyListener paramKeyListener)
    {
        setKeyListenerOnly(paramKeyListener);
        fixFocusableAndClickableSettings();
        if (paramKeyListener != null)
            createEditorIfNeeded();
        while (true)
        {
            try
            {
                this.mEditor.mInputType = this.mEditor.mKeyListener.getInputType();
                setInputTypeSingleLine(this.mSingleLine);
                InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                if (localInputMethodManager != null)
                    localInputMethodManager.restartInput(this);
                return;
            }
            catch (IncompatibleClassChangeError localIncompatibleClassChangeError)
            {
                this.mEditor.mInputType = 1;
                continue;
            }
            if (this.mEditor != null)
                this.mEditor.mInputType = 0;
        }
    }

    public void setLineSpacing(float paramFloat1, float paramFloat2)
    {
        if ((this.mSpacingAdd != paramFloat1) || (this.mSpacingMult != paramFloat2))
        {
            this.mSpacingAdd = paramFloat1;
            this.mSpacingMult = paramFloat2;
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    @RemotableViewMethod
    public void setLines(int paramInt)
    {
        this.mMinimum = paramInt;
        this.mMaximum = paramInt;
        this.mMinMode = 1;
        this.mMaxMode = 1;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public final void setLinkTextColor(int paramInt)
    {
        this.mLinkTextColor = ColorStateList.valueOf(paramInt);
        updateTextColors();
    }

    public final void setLinkTextColor(ColorStateList paramColorStateList)
    {
        this.mLinkTextColor = paramColorStateList;
        updateTextColors();
    }

    @RemotableViewMethod
    public final void setLinksClickable(boolean paramBoolean)
    {
        this.mLinksClickable = paramBoolean;
    }

    public void setMarqueeRepeatLimit(int paramInt)
    {
        this.mMarqueeRepeatLimit = paramInt;
    }

    @RemotableViewMethod
    public void setMaxEms(int paramInt)
    {
        this.mMaxWidth = paramInt;
        this.mMaxWidthMode = 1;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMaxHeight(int paramInt)
    {
        this.mMaximum = paramInt;
        this.mMaxMode = 2;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMaxLines(int paramInt)
    {
        this.mMaximum = paramInt;
        this.mMaxMode = 1;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMaxWidth(int paramInt)
    {
        this.mMaxWidth = paramInt;
        this.mMaxWidthMode = 2;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMinEms(int paramInt)
    {
        this.mMinWidth = paramInt;
        this.mMinWidthMode = 1;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMinHeight(int paramInt)
    {
        this.mMinimum = paramInt;
        this.mMinMode = 2;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMinLines(int paramInt)
    {
        this.mMinimum = paramInt;
        this.mMinMode = 1;
        requestLayout();
        invalidate();
    }

    @RemotableViewMethod
    public void setMinWidth(int paramInt)
    {
        this.mMinWidth = paramInt;
        this.mMinWidthMode = 2;
        requestLayout();
        invalidate();
    }

    public final void setMovementMethod(MovementMethod paramMovementMethod)
    {
        if (this.mMovement != paramMovementMethod)
        {
            this.mMovement = paramMovementMethod;
            if ((paramMovementMethod != null) && (!(this.mText instanceof Spannable)))
                setText(this.mText);
            fixFocusableAndClickableSettings();
            if (this.mEditor != null)
                this.mEditor.prepareCursorControllers();
        }
    }

    public void setOnEditorActionListener(OnEditorActionListener paramOnEditorActionListener)
    {
        createEditorIfNeeded();
        this.mEditor.createInputContentTypeIfNeeded();
        this.mEditor.mInputContentType.onEditorActionListener = paramOnEditorActionListener;
    }

    public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramInt1 != this.mPaddingLeft) || (paramInt3 != this.mPaddingRight) || (paramInt2 != this.mPaddingTop) || (paramInt4 != this.mPaddingBottom))
            nullLayouts();
        super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
        invalidate();
    }

    public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramInt1 != getPaddingStart()) || (paramInt3 != getPaddingEnd()) || (paramInt2 != this.mPaddingTop) || (paramInt4 != this.mPaddingBottom))
            nullLayouts();
        super.setPaddingRelative(paramInt1, paramInt2, paramInt3, paramInt4);
        invalidate();
    }

    @RemotableViewMethod
    public void setPaintFlags(int paramInt)
    {
        if (this.mTextPaint.getFlags() != paramInt)
        {
            this.mTextPaint.setFlags(paramInt);
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    public void setPrivateImeOptions(String paramString)
    {
        createEditorIfNeeded();
        this.mEditor.createInputContentTypeIfNeeded();
        this.mEditor.mInputContentType.privateImeOptions = paramString;
    }

    public void setRawInputType(int paramInt)
    {
        if ((paramInt == 0) && (this.mEditor == null));
        while (true)
        {
            return;
            createEditorIfNeeded();
            this.mEditor.mInputType = paramInt;
        }
    }

    public void setScroller(Scroller paramScroller)
    {
        this.mScroller = paramScroller;
    }

    @RemotableViewMethod
    public void setSelectAllOnFocus(boolean paramBoolean)
    {
        createEditorIfNeeded();
        this.mEditor.mSelectAllOnFocus = paramBoolean;
        if ((paramBoolean) && (!(this.mText instanceof Spannable)))
            setText(this.mText, BufferType.SPANNABLE);
    }

    public void setSelected(boolean paramBoolean)
    {
        boolean bool = isSelected();
        super.setSelected(paramBoolean);
        if ((paramBoolean != bool) && (this.mEllipsize == TextUtils.TruncateAt.MARQUEE))
        {
            if (!paramBoolean)
                break label34;
            startMarquee();
        }
        while (true)
        {
            return;
            label34: stopMarquee();
        }
    }

    public void setShadowLayer(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
    {
        this.mTextPaint.setShadowLayer(paramFloat1, paramFloat2, paramFloat3, paramInt);
        this.mShadowRadius = paramFloat1;
        this.mShadowDx = paramFloat2;
        this.mShadowDy = paramFloat3;
        if (this.mEditor != null)
            this.mEditor.invalidateTextDisplayList();
        invalidate();
    }

    @RemotableViewMethod
    public final void setShowSoftInputOnFocus(boolean paramBoolean)
    {
        createEditorIfNeeded();
        this.mEditor.mShowSoftInputOnFocus = paramBoolean;
    }

    public void setSingleLine()
    {
        setSingleLine(true);
    }

    @RemotableViewMethod
    public void setSingleLine(boolean paramBoolean)
    {
        setInputTypeSingleLine(paramBoolean);
        applySingleLine(paramBoolean, true, true);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public final void setSoftInputShownOnFocus(boolean paramBoolean)
    {
        setShowSoftInputOnFocus(paramBoolean);
    }

    protected void setSpan_internal(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        ((Editable)this.mText).setSpan(paramObject, paramInt1, paramInt2, paramInt3);
    }

    public final void setSpannableFactory(Spannable.Factory paramFactory)
    {
        this.mSpannableFactory = paramFactory;
        setText(this.mText);
    }

    @RemotableViewMethod
    public final void setText(int paramInt)
    {
        setText(getContext().getResources().getText(paramInt));
    }

    public final void setText(int paramInt, BufferType paramBufferType)
    {
        setText(getContext().getResources().getText(paramInt), paramBufferType);
    }

    @RemotableViewMethod
    public final void setText(CharSequence paramCharSequence)
    {
        setText(paramCharSequence, this.mBufferType);
    }

    public void setText(CharSequence paramCharSequence, BufferType paramBufferType)
    {
        setText(paramCharSequence, paramBufferType, true, 0);
        if (this.mCharWrapper != null)
            CharWrapper.access$002(this.mCharWrapper, null);
    }

    public final void setText(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = 0;
        if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length))
            throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
        if (this.mText != null)
        {
            i = this.mText.length();
            sendBeforeTextChanged(this.mText, 0, i, paramInt2);
            if (this.mCharWrapper != null)
                break label130;
            this.mCharWrapper = new CharWrapper(paramArrayOfChar, paramInt1, paramInt2);
        }
        while (true)
        {
            setText(this.mCharWrapper, this.mBufferType, false, i);
            return;
            sendBeforeTextChanged("", 0, 0, paramInt2);
            break;
            label130: this.mCharWrapper.set(paramArrayOfChar, paramInt1, paramInt2);
        }
    }

    public void setTextAppearance(Context paramContext, int paramInt)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramInt, R.styleable.TextAppearance);
        int i = localTypedArray.getColor(4, 0);
        if (i != 0)
            setHighlightColor(i);
        ColorStateList localColorStateList1 = localTypedArray.getColorStateList(3);
        if (localColorStateList1 != null)
            setTextColor(localColorStateList1);
        int j = localTypedArray.getDimensionPixelSize(0, 0);
        if (j != 0)
            setRawTextSize(j);
        ColorStateList localColorStateList2 = localTypedArray.getColorStateList(5);
        if (localColorStateList2 != null)
            setHintTextColor(localColorStateList2);
        ColorStateList localColorStateList3 = localTypedArray.getColorStateList(6);
        if (localColorStateList3 != null)
            setLinkTextColor(localColorStateList3);
        setTypefaceFromAttrs(localTypedArray.getString(8), localTypedArray.getInt(1, -1), localTypedArray.getInt(2, -1));
        if (localTypedArray.getBoolean(7, false))
            setTransformationMethod(new AllCapsTransformationMethod(getContext()));
        localTypedArray.recycle();
    }

    @RemotableViewMethod
    public void setTextColor(int paramInt)
    {
        this.mTextColor = ColorStateList.valueOf(paramInt);
        updateTextColors();
    }

    public void setTextColor(ColorStateList paramColorStateList)
    {
        if (paramColorStateList == null)
            throw new NullPointerException();
        this.mTextColor = paramColorStateList;
        updateTextColors();
    }

    public void setTextIsSelectable(boolean paramBoolean)
    {
        if ((!paramBoolean) && (this.mEditor == null));
        do
        {
            return;
            createEditorIfNeeded();
        }
        while (this.mEditor.mTextIsSelectable == paramBoolean);
        this.mEditor.mTextIsSelectable = paramBoolean;
        setFocusableInTouchMode(paramBoolean);
        setFocusable(paramBoolean);
        setClickable(paramBoolean);
        setLongClickable(paramBoolean);
        MovementMethod localMovementMethod;
        label63: CharSequence localCharSequence;
        if (paramBoolean)
        {
            localMovementMethod = ArrowKeyMovementMethod.getInstance();
            setMovementMethod(localMovementMethod);
            localCharSequence = this.mText;
            if (!paramBoolean)
                break label104;
        }
        label104: for (BufferType localBufferType = BufferType.SPANNABLE; ; localBufferType = BufferType.NORMAL)
        {
            setText(localCharSequence, localBufferType);
            this.mEditor.prepareCursorControllers();
            break;
            localMovementMethod = null;
            break label63;
        }
    }

    @RemotableViewMethod
    public final void setTextKeepState(CharSequence paramCharSequence)
    {
        setTextKeepState(paramCharSequence, this.mBufferType);
    }

    public final void setTextKeepState(CharSequence paramCharSequence, BufferType paramBufferType)
    {
        int i = getSelectionStart();
        int j = getSelectionEnd();
        int k = paramCharSequence.length();
        setText(paramCharSequence, paramBufferType);
        if (((i >= 0) || (j >= 0)) && ((this.mText instanceof Spannable)))
            Selection.setSelection((Spannable)this.mText, Math.max(0, Math.min(i, k)), Math.max(0, Math.min(j, k)));
    }

    @RemotableViewMethod
    public void setTextScaleX(float paramFloat)
    {
        if (paramFloat != this.mTextPaint.getTextScaleX())
        {
            this.mUserSetTextScaleX = true;
            this.mTextPaint.setTextScaleX(paramFloat);
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    @RemotableViewMethod
    public void setTextSize(float paramFloat)
    {
        setTextSize(2, paramFloat);
    }

    public void setTextSize(int paramInt, float paramFloat)
    {
        Context localContext = getContext();
        if (localContext == null);
        for (Resources localResources = Resources.getSystem(); ; localResources = localContext.getResources())
        {
            setRawTextSize(TypedValue.applyDimension(paramInt, paramFloat, localResources.getDisplayMetrics()));
            return;
        }
    }

    public final void setTransformationMethod(TransformationMethod paramTransformationMethod)
    {
        if (paramTransformationMethod == this.mTransformation)
            return;
        if ((this.mTransformation != null) && ((this.mText instanceof Spannable)))
            ((Spannable)this.mText).removeSpan(this.mTransformation);
        this.mTransformation = paramTransformationMethod;
        boolean bool;
        if ((paramTransformationMethod instanceof TransformationMethod2))
        {
            TransformationMethod2 localTransformationMethod2 = (TransformationMethod2)paramTransformationMethod;
            if ((!isTextSelectable()) && (!(this.mText instanceof Editable)))
            {
                bool = true;
                label78: this.mAllowTransformationLengthChange = bool;
                localTransformationMethod2.setLengthChangesAllowed(this.mAllowTransformationLengthChange);
            }
        }
        while (true)
        {
            setText(this.mText);
            if (!hasPasswordTransformationMethod())
                break;
            notifyAccessibilityStateChanged();
            break;
            bool = false;
            break label78;
            this.mAllowTransformationLengthChange = false;
        }
    }

    public void setTypeface(Typeface paramTypeface)
    {
        if (this.mTextPaint.getTypeface() != paramTypeface)
        {
            this.mTextPaint.setTypeface(paramTypeface);
            if (this.mLayout != null)
            {
                nullLayouts();
                requestLayout();
                invalidate();
            }
        }
    }

    public void setTypeface(Typeface paramTypeface, int paramInt)
    {
        boolean bool = false;
        Typeface localTypeface;
        int i;
        label34: float f;
        if (paramInt > 0)
            if (paramTypeface == null)
            {
                localTypeface = Typeface.defaultFromStyle(paramInt);
                setTypeface(localTypeface);
                if (localTypeface == null)
                    break label100;
                i = localTypeface.getStyle();
                int j = paramInt & (i ^ 0xFFFFFFFF);
                TextPaint localTextPaint1 = this.mTextPaint;
                if ((j & 0x1) != 0)
                    bool = true;
                localTextPaint1.setFakeBoldText(bool);
                TextPaint localTextPaint2 = this.mTextPaint;
                if ((j & 0x2) == 0)
                    break label106;
                f = -0.25F;
                label82: localTextPaint2.setTextSkewX(f);
            }
        while (true)
        {
            return;
            localTypeface = Typeface.create(paramTypeface, paramInt);
            break;
            label100: i = 0;
            break label34;
            label106: f = 0.0F;
            break label82;
            this.mTextPaint.setFakeBoldText(false);
            this.mTextPaint.setTextSkewX(0.0F);
            setTypeface(paramTypeface);
        }
    }

    @RemotableViewMethod
    public void setWidth(int paramInt)
    {
        this.mMinWidth = paramInt;
        this.mMaxWidth = paramInt;
        this.mMinWidthMode = 2;
        this.mMaxWidthMode = 2;
        requestLayout();
        invalidate();
    }

    void spanChange(Spanned paramSpanned, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = 0;
        int j = -1;
        int k = -1;
        Editor.InputMethodState localInputMethodState;
        if (this.mEditor == null)
        {
            localInputMethodState = null;
            if (paramObject == Selection.SELECTION_END)
            {
                i = 1;
                k = paramInt2;
                if ((paramInt1 >= 0) || (paramInt2 >= 0))
                {
                    invalidateCursor(Selection.getSelectionStart(paramSpanned), paramInt1, paramInt2);
                    registerForPreDraw();
                    if (this.mEditor != null)
                        this.mEditor.makeBlink();
                }
            }
            if (paramObject == Selection.SELECTION_START)
            {
                i = 1;
                j = paramInt2;
                if ((paramInt1 >= 0) || (paramInt2 >= 0))
                    invalidateCursor(Selection.getSelectionEnd(paramSpanned), paramInt1, paramInt2);
            }
            if (i != 0)
            {
                this.mHighlightPathBogus = true;
                if ((this.mEditor != null) && (!isFocused()))
                    this.mEditor.mSelectionMoved = true;
                if ((0x200 & paramSpanned.getSpanFlags(paramObject)) == 0)
                {
                    if (j < 0)
                        j = Selection.getSelectionStart(paramSpanned);
                    if (k < 0)
                        k = Selection.getSelectionEnd(paramSpanned);
                    onSelectionChanged(j, k);
                }
            }
            if (((paramObject instanceof UpdateAppearance)) || ((paramObject instanceof ParagraphStyle)) || ((paramObject instanceof CharacterStyle)))
            {
                if ((localInputMethodState != null) && (localInputMethodState.mBatchEditNesting != 0))
                    break label490;
                invalidate();
                this.mHighlightPathBogus = true;
                checkForResize();
                label230: if (this.mEditor != null)
                {
                    if (paramInt1 >= 0)
                        this.mEditor.invalidateTextDisplayList(this.mLayout, paramInt1, paramInt3);
                    if (paramInt2 >= 0)
                        this.mEditor.invalidateTextDisplayList(this.mLayout, paramInt2, paramInt4);
                }
            }
            if (MetaKeyKeyListener.isMetaTracker(paramSpanned, paramObject))
            {
                this.mHighlightPathBogus = true;
                if ((localInputMethodState != null) && (MetaKeyKeyListener.isSelectingMetaTracker(paramSpanned, paramObject)))
                    localInputMethodState.mSelectionModeChanged = true;
                if (Selection.getSelectionStart(paramSpanned) >= 0)
                {
                    if ((localInputMethodState != null) && (localInputMethodState.mBatchEditNesting != 0))
                        break label499;
                    invalidateCursor();
                }
            }
            label331: if (((paramObject instanceof ParcelableSpan)) && (localInputMethodState != null) && (localInputMethodState.mExtractedTextRequest != null))
            {
                if (localInputMethodState.mBatchEditNesting == 0)
                    break label508;
                if (paramInt1 >= 0)
                {
                    if (localInputMethodState.mChangedStart > paramInt1)
                        localInputMethodState.mChangedStart = paramInt1;
                    if (localInputMethodState.mChangedStart > paramInt3)
                        localInputMethodState.mChangedStart = paramInt3;
                }
                if (paramInt2 >= 0)
                {
                    if (localInputMethodState.mChangedStart > paramInt2)
                        localInputMethodState.mChangedStart = paramInt2;
                    if (localInputMethodState.mChangedStart > paramInt4)
                        localInputMethodState.mChangedStart = paramInt4;
                }
            }
        }
        while (true)
        {
            if ((this.mEditor != null) && (this.mEditor.mSpellChecker != null) && (paramInt2 < 0) && ((paramObject instanceof SpellCheckSpan)))
                this.mEditor.mSpellChecker.onSpellCheckSpanRemoved((SpellCheckSpan)paramObject);
            return;
            localInputMethodState = this.mEditor.mInputMethodState;
            break;
            label490: localInputMethodState.mContentChanged = true;
            break label230;
            label499: localInputMethodState.mCursorChanged = true;
            break label331;
            label508: localInputMethodState.mContentChanged = true;
        }
    }

    protected void stopSelectionActionMode()
    {
        this.mEditor.stopSelectionActionMode();
    }

    boolean textCanBeSelected()
    {
        boolean bool = false;
        if ((this.mMovement == null) || (!this.mMovement.canSelectArbitrarily()));
        while (true)
        {
            return bool;
            if ((isTextEditable()) || ((isTextSelectable()) && ((this.mText instanceof Spannable)) && (isEnabled())))
                bool = true;
        }
    }

    void updateAfterEdit()
    {
        invalidate();
        int i = getSelectionStart();
        if ((i >= 0) || ((0x70 & this.mGravity) == 80))
            registerForPreDraw();
        if (i >= 0)
        {
            this.mHighlightPathBogus = true;
            if (this.mEditor != null)
                this.mEditor.makeBlink();
            bringPointIntoView(i);
        }
        checkForResize();
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        boolean bool1 = super.verifyDrawable(paramDrawable);
        boolean bool2;
        if ((!bool1) && (this.mDrawables != null))
            if ((paramDrawable == this.mDrawables.mDrawableLeft) || (paramDrawable == this.mDrawables.mDrawableTop) || (paramDrawable == this.mDrawables.mDrawableRight) || (paramDrawable == this.mDrawables.mDrawableBottom) || (paramDrawable == this.mDrawables.mDrawableStart) || (paramDrawable == this.mDrawables.mDrawableEnd))
                bool2 = true;
        while (true)
        {
            return bool2;
            bool2 = false;
            continue;
            bool2 = bool1;
        }
    }

    protected void viewClicked(InputMethodManager paramInputMethodManager)
    {
        if (paramInputMethodManager != null)
            paramInputMethodManager.viewClicked(this);
    }

    int viewportToContentHorizontalOffset()
    {
        return getCompoundPaddingLeft() - this.mScrollX;
    }

    int viewportToContentVerticalOffset()
    {
        int i = getExtendedPaddingTop() - this.mScrollY;
        if ((0x70 & this.mGravity) != 48)
            i += getVerticalOffset(false);
        return i;
    }

    private class ChangeWatcher
        implements TextWatcher, SpanWatcher
    {
        private CharSequence mBeforeText;

        private ChangeWatcher()
        {
        }

        public void afterTextChanged(Editable paramEditable)
        {
            TextView.this.sendAfterTextChanged(paramEditable);
            if (MetaKeyKeyListener.getMetaState(paramEditable, 2048) != 0)
                MetaKeyKeyListener.stopSelecting(TextView.this, paramEditable);
        }

        public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
            if ((AccessibilityManager.getInstance(TextView.this.mContext).isEnabled()) && (!TextView.isPasswordInputType(TextView.this.getInputType())) && (!TextView.this.hasPasswordTransformationMethod()))
                this.mBeforeText = paramCharSequence.toString();
            TextView.this.sendBeforeTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
        }

        public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
        {
            TextView.this.spanChange(paramSpannable, paramObject, -1, paramInt1, -1, paramInt2);
        }

        public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            TextView.this.spanChange(paramSpannable, paramObject, paramInt1, paramInt3, paramInt2, paramInt4);
        }

        public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
        {
            TextView.this.spanChange(paramSpannable, paramObject, paramInt1, -1, paramInt2, -1);
        }

        public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
            TextView.this.handleTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
            if ((AccessibilityManager.getInstance(TextView.this.mContext).isEnabled()) && ((TextView.this.isFocused()) || ((TextView.this.isSelected()) && (TextView.this.isShown()))))
            {
                TextView.this.sendAccessibilityEventTypeViewTextChanged(this.mBeforeText, paramInt1, paramInt2, paramInt3);
                this.mBeforeText = null;
            }
        }
    }

    private static final class Marquee extends Handler
    {
        private static final int MARQUEE_DELAY = 1200;
        private static final float MARQUEE_DELTA_MAX = 0.07F;
        private static final int MARQUEE_PIXELS_PER_SECOND = 30;
        private static final int MARQUEE_RESOLUTION = 33;
        private static final int MARQUEE_RESTART_DELAY = 1200;
        private static final byte MARQUEE_RUNNING = 2;
        private static final byte MARQUEE_STARTING = 1;
        private static final byte MARQUEE_STOPPED = 0;
        private static final int MESSAGE_RESTART = 3;
        private static final int MESSAGE_START = 1;
        private static final int MESSAGE_TICK = 2;
        private float mFadeStop;
        private float mGhostOffset;
        private float mGhostStart;
        float mMaxFadeScroll;
        private float mMaxScroll;
        private int mRepeatLimit;
        float mScroll;
        private final float mScrollUnit;
        private byte mStatus = 0;
        private final WeakReference<TextView> mView;

        Marquee(TextView paramTextView)
        {
            this.mScrollUnit = (30.0F * paramTextView.getContext().getResources().getDisplayMetrics().density / 33.0F);
            this.mView = new WeakReference(paramTextView);
        }

        private void resetScroll()
        {
            this.mScroll = 0.0F;
            TextView localTextView = (TextView)this.mView.get();
            if (localTextView != null)
                localTextView.invalidate();
        }

        float getGhostOffset()
        {
            return this.mGhostOffset;
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                this.mStatus = 2;
                tick();
                continue;
                tick();
                continue;
                if (this.mStatus == 2)
                {
                    if (this.mRepeatLimit >= 0)
                        this.mRepeatLimit = (-1 + this.mRepeatLimit);
                    start(this.mRepeatLimit);
                }
            }
        }

        boolean isRunning()
        {
            if (this.mStatus == 2);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isStopped()
        {
            if (this.mStatus == 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean shouldDrawGhost()
        {
            if ((this.mStatus == 2) && (this.mScroll > this.mGhostStart));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean shouldDrawLeftFade()
        {
            if (this.mScroll <= this.mFadeStop);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        void start(int paramInt)
        {
            if (paramInt == 0)
                stop();
            while (true)
            {
                return;
                this.mRepeatLimit = paramInt;
                TextView localTextView = (TextView)this.mView.get();
                if ((localTextView != null) && (localTextView.mLayout != null))
                {
                    this.mStatus = 1;
                    this.mScroll = 0.0F;
                    int i = localTextView.getWidth() - localTextView.getCompoundPaddingLeft() - localTextView.getCompoundPaddingRight();
                    float f1 = localTextView.mLayout.getLineWidth(0);
                    float f2 = i / 3.0F;
                    this.mGhostStart = (f2 + (f1 - i));
                    this.mMaxScroll = (this.mGhostStart + i);
                    this.mGhostOffset = (f1 + f2);
                    this.mFadeStop = (f1 + i / 6.0F);
                    this.mMaxFadeScroll = (f1 + (f1 + this.mGhostStart));
                    localTextView.invalidate();
                    sendEmptyMessageDelayed(1, 1200L);
                }
            }
        }

        void stop()
        {
            this.mStatus = 0;
            removeMessages(1);
            removeMessages(3);
            removeMessages(2);
            resetScroll();
        }

        void tick()
        {
            if (this.mStatus != 2);
            TextView localTextView;
            do
            {
                return;
                removeMessages(2);
                localTextView = (TextView)this.mView.get();
            }
            while ((localTextView == null) || ((!localTextView.isFocused()) && (!localTextView.isSelected())));
            this.mScroll += this.mScrollUnit;
            if (this.mScroll > this.mMaxScroll)
            {
                this.mScroll = this.mMaxScroll;
                sendEmptyMessageDelayed(3, 1200L);
            }
            while (true)
            {
                localTextView.invalidate();
                break;
                sendEmptyMessageDelayed(2, 33L);
            }
        }
    }

    private static class CharWrapper
        implements CharSequence, GetChars, GraphicsOperations
    {
        private char[] mChars;
        private int mLength;
        private int mStart;

        public CharWrapper(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        {
            this.mChars = paramArrayOfChar;
            this.mStart = paramInt1;
            this.mLength = paramInt2;
        }

        public char charAt(int paramInt)
        {
            return this.mChars[(paramInt + this.mStart)];
        }

        public void drawText(Canvas paramCanvas, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
        {
            paramCanvas.drawText(this.mChars, paramInt1 + this.mStart, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
        }

        public void drawTextRun(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
        {
            int i = paramInt2 - paramInt1;
            int j = paramInt4 - paramInt3;
            paramCanvas.drawTextRun(this.mChars, paramInt1 + this.mStart, i, paramInt3 + this.mStart, j, paramFloat1, paramFloat2, paramInt5, paramPaint);
        }

        public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
        {
            if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 > this.mLength) || (paramInt2 > this.mLength))
                throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
            System.arraycopy(this.mChars, paramInt1 + this.mStart, paramArrayOfChar, paramInt3, paramInt2 - paramInt1);
        }

        public float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, Paint paramPaint)
        {
            int i = paramInt2 - paramInt1;
            int j = paramInt4 - paramInt3;
            return paramPaint.getTextRunAdvances(this.mChars, paramInt1 + this.mStart, i, paramInt3 + this.mStart, j, paramInt5, paramArrayOfFloat, paramInt6);
        }

        public float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, Paint paramPaint, int paramInt7)
        {
            int i = paramInt2 - paramInt1;
            int j = paramInt4 - paramInt3;
            return paramPaint.getTextRunAdvances(this.mChars, paramInt1 + this.mStart, i, paramInt3 + this.mStart, j, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
        }

        public int getTextRunCursor(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint)
        {
            int i = paramInt2 - paramInt1;
            return paramPaint.getTextRunCursor(this.mChars, paramInt1 + this.mStart, i, paramInt3, paramInt4 + this.mStart, paramInt5);
        }

        public int getTextWidths(int paramInt1, int paramInt2, float[] paramArrayOfFloat, Paint paramPaint)
        {
            return paramPaint.getTextWidths(this.mChars, paramInt1 + this.mStart, paramInt2 - paramInt1, paramArrayOfFloat);
        }

        public int length()
        {
            return this.mLength;
        }

        public float measureText(int paramInt1, int paramInt2, Paint paramPaint)
        {
            return paramPaint.measureText(this.mChars, paramInt1 + this.mStart, paramInt2 - paramInt1);
        }

        void set(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        {
            this.mChars = paramArrayOfChar;
            this.mStart = paramInt1;
            this.mLength = paramInt2;
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 > this.mLength) || (paramInt2 > this.mLength))
                throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
            return new String(this.mChars, paramInt1 + this.mStart, paramInt2 - paramInt1);
        }

        public String toString()
        {
            return new String(this.mChars, this.mStart, this.mLength);
        }
    }

    public static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public TextView.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new TextView.SavedState(paramAnonymousParcel, null);
            }

            public TextView.SavedState[] newArray(int paramAnonymousInt)
            {
                return new TextView.SavedState[paramAnonymousInt];
            }
        };
        CharSequence error;
        boolean frozenWithFocus;
        int selEnd;
        int selStart;
        CharSequence text;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.selStart = paramParcel.readInt();
            this.selEnd = paramParcel.readInt();
            if (paramParcel.readInt() != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.frozenWithFocus = bool;
                this.text = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
                if (paramParcel.readInt() != 0)
                    this.error = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
                return;
            }
        }

        SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public String toString()
        {
            String str = "TextView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " start=" + this.selStart + " end=" + this.selEnd;
            if (this.text != null)
                str = str + " text=" + this.text;
            return str + "}";
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.selStart);
            paramParcel.writeInt(this.selEnd);
            int i;
            if (this.frozenWithFocus)
            {
                i = 1;
                paramParcel.writeInt(i);
                TextUtils.writeToParcel(this.text, paramParcel, paramInt);
                if (this.error != null)
                    break label63;
                paramParcel.writeInt(0);
            }
            while (true)
            {
                return;
                i = 0;
                break;
                label63: paramParcel.writeInt(1);
                TextUtils.writeToParcel(this.error, paramParcel, paramInt);
            }
        }
    }

    public static enum BufferType
    {
        static
        {
            EDITABLE = new BufferType("EDITABLE", 2);
            BufferType[] arrayOfBufferType = new BufferType[3];
            arrayOfBufferType[0] = NORMAL;
            arrayOfBufferType[1] = SPANNABLE;
            arrayOfBufferType[2] = EDITABLE;
        }
    }

    public static abstract interface OnEditorActionListener
    {
        public abstract boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent);
    }

    static class Drawables
    {
        final Rect mCompoundRect = new Rect();
        Drawable mDrawableBottom;
        Drawable mDrawableEnd;
        int mDrawableHeightEnd;
        int mDrawableHeightLeft;
        int mDrawableHeightRight;
        int mDrawableHeightStart;
        Drawable mDrawableLeft;
        int mDrawablePadding;
        Drawable mDrawableRight;
        int mDrawableSizeBottom;
        int mDrawableSizeEnd;
        int mDrawableSizeLeft;
        int mDrawableSizeRight;
        int mDrawableSizeStart;
        int mDrawableSizeTop;
        Drawable mDrawableStart;
        Drawable mDrawableTop;
        int mDrawableWidthBottom;
        int mDrawableWidthTop;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TextView
 * JD-Core Version:        0.6.2
 */