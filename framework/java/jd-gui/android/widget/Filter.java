package android.widget;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public abstract class Filter
{
    private static final int FILTER_TOKEN = -791613427;
    private static final int FINISH_TOKEN = -559038737;
    private static final String LOG_TAG = "Filter";
    private static final String THREAD_NAME = "Filter";
    private Delayer mDelayer;
    private final Object mLock = new Object();
    private Handler mResultHandler = new ResultsHandler(null);
    private Handler mThreadHandler;

    public CharSequence convertResultToString(Object paramObject)
    {
        if (paramObject == null);
        for (String str = ""; ; str = paramObject.toString())
            return str;
    }

    public final void filter(CharSequence paramCharSequence)
    {
        filter(paramCharSequence, null);
    }

    public final void filter(CharSequence paramCharSequence, FilterListener paramFilterListener)
    {
        String str = null;
        synchronized (this.mLock)
        {
            if (this.mThreadHandler == null)
            {
                HandlerThread localHandlerThread = new HandlerThread("Filter", 10);
                localHandlerThread.start();
                this.mThreadHandler = new RequestHandler(localHandlerThread.getLooper());
            }
            if (this.mDelayer == null)
            {
                l = 0L;
                Message localMessage = this.mThreadHandler.obtainMessage(-791613427);
                RequestArguments localRequestArguments = new RequestArguments(null);
                if (paramCharSequence != null)
                    str = paramCharSequence.toString();
                localRequestArguments.constraint = str;
                localRequestArguments.listener = paramFilterListener;
                localMessage.obj = localRequestArguments;
                this.mThreadHandler.removeMessages(-791613427);
                this.mThreadHandler.removeMessages(-559038737);
                this.mThreadHandler.sendMessageDelayed(localMessage, l);
                return;
            }
            long l = this.mDelayer.getPostingDelay(paramCharSequence);
        }
    }

    protected abstract FilterResults performFiltering(CharSequence paramCharSequence);

    protected abstract void publishResults(CharSequence paramCharSequence, FilterResults paramFilterResults);

    public void setDelayer(Delayer paramDelayer)
    {
        synchronized (this.mLock)
        {
            this.mDelayer = paramDelayer;
            return;
        }
    }

    public static abstract interface Delayer
    {
        public abstract long getPostingDelay(CharSequence paramCharSequence);
    }

    private static class RequestArguments
    {
        CharSequence constraint;
        Filter.FilterListener listener;
        Filter.FilterResults results;
    }

    private class ResultsHandler extends Handler
    {
        private ResultsHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            Filter.RequestArguments localRequestArguments = (Filter.RequestArguments)paramMessage.obj;
            Filter.this.publishResults(localRequestArguments.constraint, localRequestArguments.results);
            if (localRequestArguments.listener != null)
                if (localRequestArguments.results == null)
                    break label56;
            label56: for (int i = localRequestArguments.results.count; ; i = -1)
            {
                localRequestArguments.listener.onFilterComplete(i);
                return;
            }
        }
    }

    private class RequestHandler extends Handler
    {
        public RequestHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            int i = paramMessage.what;
            switch (i)
            {
            default:
            case -791613427:
            case -559038737:
            }
            while (true)
            {
                return;
                Filter.RequestArguments localRequestArguments = (Filter.RequestArguments)paramMessage.obj;
                try
                {
                    localRequestArguments.results = Filter.this.performFiltering(localRequestArguments.constraint);
                    localMessage2 = Filter.this.mResultHandler.obtainMessage(i);
                    localMessage2.obj = localRequestArguments;
                    localMessage2.sendToTarget();
                    synchronized (Filter.this.mLock)
                    {
                        if (Filter.this.mThreadHandler != null)
                        {
                            Message localMessage3 = Filter.this.mThreadHandler.obtainMessage(-559038737);
                            Filter.this.mThreadHandler.sendMessageDelayed(localMessage3, 3000L);
                        }
                    }
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        localRequestArguments.results = new Filter.FilterResults();
                        Log.w("Filter", "An exception occured during performFiltering()!", localException);
                        Message localMessage2 = Filter.this.mResultHandler.obtainMessage(i);
                        localMessage2.obj = localRequestArguments;
                    }
                }
                finally
                {
                    Message localMessage1 = Filter.this.mResultHandler.obtainMessage(i);
                    localMessage1.obj = localRequestArguments;
                    localMessage1.sendToTarget();
                }
                synchronized (Filter.this.mLock)
                {
                    if (Filter.this.mThreadHandler != null)
                    {
                        Filter.this.mThreadHandler.getLooper().quit();
                        Filter.access$402(Filter.this, null);
                    }
                }
            }
        }
    }

    public static abstract interface FilterListener
    {
        public abstract void onFilterComplete(int paramInt);
    }

    protected static class FilterResults
    {
        public int count;
        public Object values;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Filter
 * JD-Core Version:        0.6.2
 */