package android.widget;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SimpleAdapter extends BaseAdapter
    implements Filterable
{
    private List<? extends Map<String, ?>> mData;
    private int mDropDownResource;
    private SimpleFilter mFilter;
    private String[] mFrom;
    private LayoutInflater mInflater;
    private int mResource;
    private int[] mTo;
    private ArrayList<Map<String, ?>> mUnfilteredData;
    private ViewBinder mViewBinder;

    public SimpleAdapter(Context paramContext, List<? extends Map<String, ?>> paramList, int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
    {
        this.mData = paramList;
        this.mDropDownResource = paramInt;
        this.mResource = paramInt;
        this.mFrom = paramArrayOfString;
        this.mTo = paramArrayOfInt;
        this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    }

    private void bindView(int paramInt, View paramView)
    {
        Map localMap = (Map)this.mData.get(paramInt);
        if (localMap == null)
            return;
        ViewBinder localViewBinder = this.mViewBinder;
        String[] arrayOfString = this.mFrom;
        int[] arrayOfInt = this.mTo;
        int i = arrayOfInt.length;
        int j = 0;
        label45: View localView;
        Object localObject1;
        String str;
        if (j < i)
        {
            localView = paramView.findViewById(arrayOfInt[j]);
            if (localView != null)
            {
                localObject1 = localMap.get(arrayOfString[j]);
                if (localObject1 != null)
                    break label167;
                str = "";
                label90: if (str == null)
                    str = "";
                boolean bool = false;
                if (localViewBinder != null)
                    bool = localViewBinder.setViewValue(localView, localObject1, str);
                if (!bool)
                {
                    if (!(localView instanceof Checkable))
                        break label261;
                    if (!(localObject1 instanceof Boolean))
                        break label177;
                    ((Checkable)localView).setChecked(((Boolean)localObject1).booleanValue());
                }
            }
        }
        label261: 
        while (true)
        {
            j++;
            break label45;
            break;
            label167: str = localObject1.toString();
            break label90;
            label177: if ((localView instanceof TextView))
            {
                setViewText((TextView)localView, str);
            }
            else
            {
                StringBuilder localStringBuilder = new StringBuilder().append(localView.getClass().getName()).append(" should be bound to a Boolean, not a ");
                if (localObject1 == null);
                for (Object localObject2 = "<unknown type>"; ; localObject2 = localObject1.getClass())
                    throw new IllegalStateException(localObject2);
                if ((localView instanceof TextView))
                {
                    setViewText((TextView)localView, str);
                }
                else
                {
                    if (!(localView instanceof ImageView))
                        break label333;
                    if ((localObject1 instanceof Integer))
                        setViewImage((ImageView)localView, ((Integer)localObject1).intValue());
                    else
                        setViewImage((ImageView)localView, str);
                }
            }
        }
        label333: throw new IllegalStateException(localView.getClass().getName() + " is not a " + " view that can be bounds by this SimpleAdapter");
    }

    private View createViewFromResource(int paramInt1, View paramView, ViewGroup paramViewGroup, int paramInt2)
    {
        if (paramView == null);
        for (View localView = this.mInflater.inflate(paramInt2, paramViewGroup, false); ; localView = paramView)
        {
            bindView(paramInt1, localView);
            return localView;
        }
    }

    public int getCount()
    {
        return this.mData.size();
    }

    public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        return createViewFromResource(paramInt, paramView, paramViewGroup, this.mDropDownResource);
    }

    public Filter getFilter()
    {
        if (this.mFilter == null)
            this.mFilter = new SimpleFilter(null);
        return this.mFilter;
    }

    public Object getItem(int paramInt)
    {
        return this.mData.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
        return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        return createViewFromResource(paramInt, paramView, paramViewGroup, this.mResource);
    }

    public ViewBinder getViewBinder()
    {
        return this.mViewBinder;
    }

    public void setDropDownViewResource(int paramInt)
    {
        this.mDropDownResource = paramInt;
    }

    public void setViewBinder(ViewBinder paramViewBinder)
    {
        this.mViewBinder = paramViewBinder;
    }

    public void setViewImage(ImageView paramImageView, int paramInt)
    {
        paramImageView.setImageResource(paramInt);
    }

    public void setViewImage(ImageView paramImageView, String paramString)
    {
        try
        {
            paramImageView.setImageResource(Integer.parseInt(paramString));
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                paramImageView.setImageURI(Uri.parse(paramString));
        }
    }

    public void setViewText(TextView paramTextView, String paramString)
    {
        paramTextView.setText(paramString);
    }

    private class SimpleFilter extends Filter
    {
        private SimpleFilter()
        {
        }

        protected Filter.FilterResults performFiltering(CharSequence paramCharSequence)
        {
            Filter.FilterResults localFilterResults = new Filter.FilterResults();
            if (SimpleAdapter.this.mUnfilteredData == null)
                SimpleAdapter.access$102(SimpleAdapter.this, new ArrayList(SimpleAdapter.this.mData));
            ArrayList localArrayList1;
            if ((paramCharSequence == null) || (paramCharSequence.length() == 0))
            {
                localArrayList1 = SimpleAdapter.this.mUnfilteredData;
                localFilterResults.values = localArrayList1;
            }
            ArrayList localArrayList3;
            for (localFilterResults.count = localArrayList1.size(); ; localFilterResults.count = localArrayList3.size())
            {
                return localFilterResults;
                String str = paramCharSequence.toString().toLowerCase();
                ArrayList localArrayList2 = SimpleAdapter.this.mUnfilteredData;
                int i = localArrayList2.size();
                localArrayList3 = new ArrayList(i);
                for (int j = 0; j < i; j++)
                {
                    Map localMap = (Map)localArrayList2.get(j);
                    if (localMap != null)
                    {
                        int k = SimpleAdapter.this.mTo.length;
                        int m = 0;
                        if (m < k)
                        {
                            String[] arrayOfString = ((String)localMap.get(SimpleAdapter.this.mFrom[m])).split(" ");
                            int n = arrayOfString.length;
                            for (int i1 = 0; ; i1++)
                                if (i1 < n)
                                {
                                    if (arrayOfString[i1].toLowerCase().startsWith(str))
                                        localArrayList3.add(localMap);
                                }
                                else
                                {
                                    m++;
                                    break;
                                }
                        }
                    }
                }
                localFilterResults.values = localArrayList3;
            }
        }

        protected void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
        {
            SimpleAdapter.access$202(SimpleAdapter.this, (List)paramFilterResults.values);
            if (paramFilterResults.count > 0)
                SimpleAdapter.this.notifyDataSetChanged();
            while (true)
            {
                return;
                SimpleAdapter.this.notifyDataSetInvalidated();
            }
        }
    }

    public static abstract interface ViewBinder
    {
        public abstract boolean setViewValue(View paramView, Object paramObject, String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SimpleAdapter
 * JD-Core Version:        0.6.2
 */