package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import libcore.icu.LocaleData;

public class CalendarView extends FrameLayout
{
    private static final int ADJUSTMENT_SCROLL_DURATION = 500;
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final int DAYS_PER_WEEK = 7;
    private static final int DEFAULT_DATE_TEXT_SIZE = 14;
    private static final String DEFAULT_MAX_DATE = "01/01/2100";
    private static final String DEFAULT_MIN_DATE = "01/01/1900";
    private static final int DEFAULT_SHOWN_WEEK_COUNT = 6;
    private static final boolean DEFAULT_SHOW_WEEK_NUMBER = true;
    private static final int DEFAULT_WEEK_DAY_TEXT_APPEARANCE_RES_ID = -1;
    private static final int GOTO_SCROLL_DURATION = 1000;
    private static final String LOG_TAG = CalendarView.class.getSimpleName();
    private static final long MILLIS_IN_DAY = 86400000L;
    private static final long MILLIS_IN_WEEK = 604800000L;
    private static final int SCROLL_CHANGE_DELAY = 40;
    private static final int SCROLL_HYST_WEEKS = 2;
    private static final int UNSCALED_BOTTOM_BUFFER = 20;
    private static final int UNSCALED_LIST_SCROLL_TOP_OFFSET = 2;
    private static final int UNSCALED_SELECTED_DATE_VERTICAL_BAR_WIDTH = 6;
    private static final int UNSCALED_WEEK_MIN_VISIBLE_HEIGHT = 12;
    private static final int UNSCALED_WEEK_SEPARATOR_LINE_WIDTH = 1;
    private WeeksAdapter mAdapter;
    private int mBottomBuffer = 20;
    private Locale mCurrentLocale;
    private int mCurrentMonthDisplayed;
    private int mCurrentScrollState = 0;
    private final DateFormat mDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private int mDateTextAppearanceResId;
    private int mDateTextSize;
    private String[] mDayLabels;
    private ViewGroup mDayNamesHeader;
    private int mDaysPerWeek = 7;
    private Calendar mFirstDayOfMonth;
    private int mFirstDayOfWeek;
    private int mFocusedMonthDateColor;
    private float mFriction = 0.05F;
    private boolean mIsScrollingUp = false;
    private int mListScrollTopOffset = 2;
    private ListView mListView;
    private Calendar mMaxDate;
    private Calendar mMinDate;
    private TextView mMonthName;
    private OnDateChangeListener mOnDateChangeListener;
    private long mPreviousScrollPosition;
    private int mPreviousScrollState = 0;
    private ScrollStateRunnable mScrollStateChangedRunnable = new ScrollStateRunnable(null);
    private Drawable mSelectedDateVerticalBar;
    private final int mSelectedDateVerticalBarWidth;
    private int mSelectedWeekBackgroundColor;
    private boolean mShowWeekNumber;
    private int mShownWeekCount;
    private Calendar mTempDate;
    private int mUnfocusedMonthDateColor;
    private float mVelocityScale = 0.333F;
    private int mWeekDayTextAppearanceResId;
    private int mWeekMinVisibleHeight = 12;
    private int mWeekNumberColor;
    private int mWeekSeparatorLineColor;
    private final int mWeekSeperatorLineWidth;

    public CalendarView(Context paramContext)
    {
        this(paramContext, null);
    }

    public CalendarView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public CalendarView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, 0);
        setCurrentLocale(Locale.getDefault());
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CalendarView, 16843613, 0);
        this.mShowWeekNumber = localTypedArray.getBoolean(1, true);
        this.mFirstDayOfWeek = localTypedArray.getInt(0, LocaleData.get(Locale.getDefault()).firstDayOfWeek.intValue());
        String str1 = localTypedArray.getString(2);
        if ((TextUtils.isEmpty(str1)) || (!parseDate(str1, this.mMinDate)))
            parseDate("01/01/1900", this.mMinDate);
        String str2 = localTypedArray.getString(3);
        if ((TextUtils.isEmpty(str2)) || (!parseDate(str2, this.mMaxDate)))
            parseDate("01/01/2100", this.mMaxDate);
        if (this.mMaxDate.before(this.mMinDate))
            throw new IllegalArgumentException("Max date cannot be before min date.");
        this.mShownWeekCount = localTypedArray.getInt(4, 6);
        this.mSelectedWeekBackgroundColor = localTypedArray.getColor(5, 0);
        this.mFocusedMonthDateColor = localTypedArray.getColor(6, 0);
        this.mUnfocusedMonthDateColor = localTypedArray.getColor(7, 0);
        this.mWeekSeparatorLineColor = localTypedArray.getColor(9, 0);
        this.mWeekNumberColor = localTypedArray.getColor(8, 0);
        this.mSelectedDateVerticalBar = localTypedArray.getDrawable(10);
        this.mDateTextAppearanceResId = localTypedArray.getResourceId(12, 16973894);
        updateDateTextSize();
        this.mWeekDayTextAppearanceResId = localTypedArray.getResourceId(11, -1);
        localTypedArray.recycle();
        DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
        this.mWeekMinVisibleHeight = ((int)TypedValue.applyDimension(1, 12.0F, localDisplayMetrics));
        this.mListScrollTopOffset = ((int)TypedValue.applyDimension(1, 2.0F, localDisplayMetrics));
        this.mBottomBuffer = ((int)TypedValue.applyDimension(1, 20.0F, localDisplayMetrics));
        this.mSelectedDateVerticalBarWidth = ((int)TypedValue.applyDimension(1, 6.0F, localDisplayMetrics));
        this.mWeekSeperatorLineWidth = ((int)TypedValue.applyDimension(1, 1.0F, localDisplayMetrics));
        View localView = ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(17367086, null, false);
        addView(localView);
        this.mListView = ((ListView)findViewById(16908298));
        this.mDayNamesHeader = ((ViewGroup)localView.findViewById(16908903));
        this.mMonthName = ((TextView)localView.findViewById(16908902));
        setUpHeader();
        setUpListView();
        setUpAdapter();
        this.mTempDate.setTimeInMillis(System.currentTimeMillis());
        if (this.mTempDate.before(this.mMinDate))
            goTo(this.mMinDate, false, true, true);
        while (true)
        {
            invalidate();
            return;
            if (this.mMaxDate.before(this.mTempDate))
                goTo(this.mMaxDate, false, true, true);
            else
                goTo(this.mTempDate, false, true, true);
        }
    }

    private Calendar getCalendarForLocale(Calendar paramCalendar, Locale paramLocale)
    {
        Calendar localCalendar;
        if (paramCalendar == null)
            localCalendar = Calendar.getInstance(paramLocale);
        while (true)
        {
            return localCalendar;
            long l = paramCalendar.getTimeInMillis();
            localCalendar = Calendar.getInstance(paramLocale);
            localCalendar.setTimeInMillis(l);
        }
    }

    private int getWeeksSinceMinDate(Calendar paramCalendar)
    {
        if (paramCalendar.before(this.mMinDate))
            throw new IllegalArgumentException("fromDate: " + this.mMinDate.getTime() + " does not precede toDate: " + paramCalendar.getTime());
        long l1 = paramCalendar.getTimeInMillis() + paramCalendar.getTimeZone().getOffset(paramCalendar.getTimeInMillis());
        long l2 = this.mMinDate.getTimeInMillis() + this.mMinDate.getTimeZone().getOffset(this.mMinDate.getTimeInMillis());
        return (int)((86400000L * (this.mMinDate.get(7) - this.mFirstDayOfWeek) + (l1 - l2)) / 604800000L);
    }

    private void goTo(Calendar paramCalendar, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        if ((paramCalendar.before(this.mMinDate)) || (paramCalendar.after(this.mMaxDate)))
            throw new IllegalArgumentException("Time not between " + this.mMinDate.getTime() + " and " + this.mMaxDate.getTime());
        int i = this.mListView.getFirstVisiblePosition();
        View localView = this.mListView.getChildAt(0);
        if ((localView != null) && (localView.getTop() < 0))
            i++;
        int j = -1 + (i + this.mShownWeekCount);
        if ((localView != null) && (localView.getTop() > this.mBottomBuffer))
            j--;
        if (paramBoolean2)
            this.mAdapter.setSelectedDay(paramCalendar);
        int k = getWeeksSinceMinDate(paramCalendar);
        int m;
        if ((k < i) || (k > j) || (paramBoolean3))
        {
            this.mFirstDayOfMonth.setTimeInMillis(paramCalendar.getTimeInMillis());
            this.mFirstDayOfMonth.set(5, 1);
            setMonthDisplayed(this.mFirstDayOfMonth);
            if (this.mFirstDayOfMonth.before(this.mMinDate))
            {
                m = 0;
                this.mPreviousScrollState = 2;
                if (!paramBoolean1)
                    break label261;
                this.mListView.smoothScrollToPositionFromTop(m, this.mListScrollTopOffset, 1000);
            }
        }
        while (true)
        {
            return;
            m = getWeeksSinceMinDate(this.mFirstDayOfMonth);
            break;
            label261: this.mListView.setSelectionFromTop(m, this.mListScrollTopOffset);
            onScrollStateChanged(this.mListView, 0);
            continue;
            if (paramBoolean2)
                setMonthDisplayed(paramCalendar);
        }
    }

    private void invalidateAllWeekViews()
    {
        int i = this.mListView.getChildCount();
        for (int j = 0; j < i; j++)
            this.mListView.getChildAt(j).invalidate();
    }

    private boolean isSameDate(Calendar paramCalendar1, Calendar paramCalendar2)
    {
        int i = 1;
        if ((paramCalendar1.get(6) == paramCalendar2.get(6)) && (paramCalendar1.get(i) == paramCalendar2.get(i)));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
    {
        WeekView localWeekView = (WeekView)paramAbsListView.getChildAt(0);
        if (localWeekView == null)
            return;
        long l = paramAbsListView.getFirstVisiblePosition() * localWeekView.getHeight() - localWeekView.getBottom();
        label50: int i;
        label65: label85: int j;
        label99: int k;
        label116: Calendar localCalendar;
        if (l < this.mPreviousScrollPosition)
        {
            this.mIsScrollingUp = true;
            if (localWeekView.getBottom() >= this.mWeekMinVisibleHeight)
                break label203;
            i = 1;
            if (!this.mIsScrollingUp)
                break label209;
            localWeekView = (WeekView)paramAbsListView.getChildAt(i + 2);
            if (!this.mIsScrollingUp)
                break label228;
            j = localWeekView.getMonthOfFirstWeekDay();
            if ((this.mCurrentMonthDisplayed != 11) || (j != 0))
                break label238;
            k = 1;
            if (((!this.mIsScrollingUp) && (k > 0)) || ((this.mIsScrollingUp) && (k < 0)))
            {
                localCalendar = localWeekView.getFirstDay();
                if (!this.mIsScrollingUp)
                    break label271;
                localCalendar.add(5, -7);
            }
        }
        while (true)
        {
            setMonthDisplayed(localCalendar);
            this.mPreviousScrollPosition = l;
            this.mPreviousScrollState = this.mCurrentScrollState;
            break;
            if (l <= this.mPreviousScrollPosition)
                break;
            this.mIsScrollingUp = false;
            break label50;
            label203: i = 0;
            break label65;
            label209: if (i == 0)
                break label85;
            localWeekView = (WeekView)paramAbsListView.getChildAt(i);
            break label85;
            label228: j = localWeekView.getMonthOfLastWeekDay();
            break label99;
            label238: if ((this.mCurrentMonthDisplayed == 0) && (j == 11))
            {
                k = -1;
                break label116;
            }
            k = j - this.mCurrentMonthDisplayed;
            break label116;
            label271: localCalendar.add(5, 7);
        }
    }

    private void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
    {
        this.mScrollStateChangedRunnable.doScrollStateChange(paramAbsListView, paramInt);
    }

    private boolean parseDate(String paramString, Calendar paramCalendar)
    {
        try
        {
            paramCalendar.setTime(this.mDateFormat.parse(paramString));
            bool = true;
            return bool;
        }
        catch (ParseException localParseException)
        {
            while (true)
            {
                Log.w(LOG_TAG, "Date: " + paramString + " not in format: " + "MM/dd/yyyy");
                boolean bool = false;
            }
        }
    }

    private void setCurrentLocale(Locale paramLocale)
    {
        if (paramLocale.equals(this.mCurrentLocale));
        while (true)
        {
            return;
            this.mCurrentLocale = paramLocale;
            this.mTempDate = getCalendarForLocale(this.mTempDate, paramLocale);
            this.mFirstDayOfMonth = getCalendarForLocale(this.mFirstDayOfMonth, paramLocale);
            this.mMinDate = getCalendarForLocale(this.mMinDate, paramLocale);
            this.mMaxDate = getCalendarForLocale(this.mMaxDate, paramLocale);
        }
    }

    private void setMonthDisplayed(Calendar paramCalendar)
    {
        int i = paramCalendar.get(2);
        if (this.mCurrentMonthDisplayed != i)
        {
            this.mCurrentMonthDisplayed = i;
            this.mAdapter.setFocusMonth(this.mCurrentMonthDisplayed);
            long l = paramCalendar.getTimeInMillis();
            String str = DateUtils.formatDateRange(this.mContext, l, l, 52);
            this.mMonthName.setText(str);
            this.mMonthName.invalidate();
        }
    }

    private void setUpAdapter()
    {
        if (this.mAdapter == null)
        {
            this.mAdapter = new WeeksAdapter(getContext());
            this.mAdapter.registerDataSetObserver(new DataSetObserver()
            {
                public void onChanged()
                {
                    if (CalendarView.this.mOnDateChangeListener != null)
                    {
                        Calendar localCalendar = CalendarView.this.mAdapter.getSelectedDay();
                        CalendarView.this.mOnDateChangeListener.onSelectedDayChange(CalendarView.this, localCalendar.get(1), localCalendar.get(2), localCalendar.get(5));
                    }
                }
            });
            this.mListView.setAdapter(this.mAdapter);
        }
        this.mAdapter.notifyDataSetChanged();
    }

    private void setUpHeader()
    {
        this.mDayLabels = new String[this.mDaysPerWeek];
        int i = this.mFirstDayOfWeek;
        int j = this.mFirstDayOfWeek + this.mDaysPerWeek;
        if (i < j)
        {
            if (i > 7);
            for (int n = i - 7; ; n = i)
            {
                this.mDayLabels[(i - this.mFirstDayOfWeek)] = DateUtils.getDayOfWeekString(n, 50);
                i++;
                break;
            }
        }
        TextView localTextView1 = (TextView)this.mDayNamesHeader.getChildAt(0);
        int k;
        label109: TextView localTextView2;
        if (this.mShowWeekNumber)
        {
            localTextView1.setVisibility(0);
            k = 1;
            int m = this.mDayNamesHeader.getChildCount();
            if (k >= m)
                break label208;
            localTextView2 = (TextView)this.mDayNamesHeader.getChildAt(k);
            if (this.mWeekDayTextAppearanceResId > -1)
                localTextView2.setTextAppearance(this.mContext, this.mWeekDayTextAppearanceResId);
            if (k >= 1 + this.mDaysPerWeek)
                break label198;
            localTextView2.setText(this.mDayLabels[(k - 1)]);
            localTextView2.setVisibility(0);
        }
        while (true)
        {
            k++;
            break label109;
            localTextView1.setVisibility(8);
            break;
            label198: localTextView2.setVisibility(8);
        }
        label208: this.mDayNamesHeader.invalidate();
    }

    private void setUpListView()
    {
        this.mListView.setDivider(null);
        this.mListView.setItemsCanFocus(true);
        this.mListView.setVerticalScrollBarEnabled(false);
        this.mListView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            public void onScroll(AbsListView paramAnonymousAbsListView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
            {
                CalendarView.this.onScroll(paramAnonymousAbsListView, paramAnonymousInt1, paramAnonymousInt2, paramAnonymousInt3);
            }

            public void onScrollStateChanged(AbsListView paramAnonymousAbsListView, int paramAnonymousInt)
            {
                CalendarView.this.onScrollStateChanged(paramAnonymousAbsListView, paramAnonymousInt);
            }
        });
        this.mListView.setFriction(this.mFriction);
        this.mListView.setVelocityScale(this.mVelocityScale);
    }

    private void updateDateTextSize()
    {
        TypedArray localTypedArray = getContext().obtainStyledAttributes(this.mDateTextAppearanceResId, R.styleable.TextAppearance);
        this.mDateTextSize = localTypedArray.getDimensionPixelSize(0, 14);
        localTypedArray.recycle();
    }

    public long getDate()
    {
        return this.mAdapter.mSelectedDate.getTimeInMillis();
    }

    public int getDateTextAppearance()
    {
        return this.mDateTextAppearanceResId;
    }

    public int getFirstDayOfWeek()
    {
        return this.mFirstDayOfWeek;
    }

    public int getFocusedMonthDateColor()
    {
        return this.mFocusedMonthDateColor;
    }

    public long getMaxDate()
    {
        return this.mMaxDate.getTimeInMillis();
    }

    public long getMinDate()
    {
        return this.mMinDate.getTimeInMillis();
    }

    public Drawable getSelectedDateVerticalBar()
    {
        return this.mSelectedDateVerticalBar;
    }

    public int getSelectedWeekBackgroundColor()
    {
        return this.mSelectedWeekBackgroundColor;
    }

    public boolean getShowWeekNumber()
    {
        return this.mShowWeekNumber;
    }

    public int getShownWeekCount()
    {
        return this.mShownWeekCount;
    }

    public int getUnfocusedMonthDateColor()
    {
        return this.mFocusedMonthDateColor;
    }

    public int getWeekDayTextAppearance()
    {
        return this.mWeekDayTextAppearanceResId;
    }

    public int getWeekNumberColor()
    {
        return this.mWeekNumberColor;
    }

    public int getWeekSeparatorLineColor()
    {
        return this.mWeekSeparatorLineColor;
    }

    public boolean isEnabled()
    {
        return this.mListView.isEnabled();
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        setCurrentLocale(paramConfiguration.locale);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(CalendarView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(CalendarView.class.getName());
    }

    public void setDate(long paramLong)
    {
        setDate(paramLong, false, false);
    }

    public void setDate(long paramLong, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mTempDate.setTimeInMillis(paramLong);
        if (isSameDate(this.mTempDate, this.mAdapter.mSelectedDate));
        while (true)
        {
            return;
            goTo(this.mTempDate, paramBoolean1, true, paramBoolean2);
        }
    }

    public void setDateTextAppearance(int paramInt)
    {
        if (this.mDateTextAppearanceResId != paramInt)
        {
            this.mDateTextAppearanceResId = paramInt;
            updateDateTextSize();
            invalidateAllWeekViews();
        }
    }

    public void setEnabled(boolean paramBoolean)
    {
        this.mListView.setEnabled(paramBoolean);
    }

    public void setFirstDayOfWeek(int paramInt)
    {
        if (this.mFirstDayOfWeek == paramInt);
        while (true)
        {
            return;
            this.mFirstDayOfWeek = paramInt;
            this.mAdapter.init();
            this.mAdapter.notifyDataSetChanged();
            setUpHeader();
        }
    }

    public void setFocusedMonthDateColor(int paramInt)
    {
        if (this.mFocusedMonthDateColor != paramInt)
        {
            this.mFocusedMonthDateColor = paramInt;
            int i = this.mListView.getChildCount();
            for (int j = 0; j < i; j++)
            {
                WeekView localWeekView = (WeekView)this.mListView.getChildAt(j);
                if (localWeekView.mHasFocusedDay)
                    localWeekView.invalidate();
            }
        }
    }

    public void setMaxDate(long paramLong)
    {
        this.mTempDate.setTimeInMillis(paramLong);
        if (isSameDate(this.mTempDate, this.mMaxDate));
        while (true)
        {
            return;
            this.mMaxDate.setTimeInMillis(paramLong);
            this.mAdapter.init();
            Calendar localCalendar = this.mAdapter.mSelectedDate;
            if (localCalendar.after(this.mMaxDate))
                setDate(this.mMaxDate.getTimeInMillis());
            else
                goTo(localCalendar, false, true, false);
        }
    }

    public void setMinDate(long paramLong)
    {
        this.mTempDate.setTimeInMillis(paramLong);
        if (isSameDate(this.mTempDate, this.mMinDate));
        while (true)
        {
            return;
            this.mMinDate.setTimeInMillis(paramLong);
            Calendar localCalendar = this.mAdapter.mSelectedDate;
            if (localCalendar.before(this.mMinDate))
                this.mAdapter.setSelectedDay(this.mMinDate);
            this.mAdapter.init();
            if (localCalendar.before(this.mMinDate))
                setDate(this.mTempDate.getTimeInMillis());
            else
                goTo(localCalendar, false, true, false);
        }
    }

    public void setOnDateChangeListener(OnDateChangeListener paramOnDateChangeListener)
    {
        this.mOnDateChangeListener = paramOnDateChangeListener;
    }

    public void setSelectedDateVerticalBar(int paramInt)
    {
        setSelectedDateVerticalBar(getResources().getDrawable(paramInt));
    }

    public void setSelectedDateVerticalBar(Drawable paramDrawable)
    {
        if (this.mSelectedDateVerticalBar != paramDrawable)
        {
            this.mSelectedDateVerticalBar = paramDrawable;
            int i = this.mListView.getChildCount();
            for (int j = 0; j < i; j++)
            {
                WeekView localWeekView = (WeekView)this.mListView.getChildAt(j);
                if (localWeekView.mHasSelectedDay)
                    localWeekView.invalidate();
            }
        }
    }

    public void setSelectedWeekBackgroundColor(int paramInt)
    {
        if (this.mSelectedWeekBackgroundColor != paramInt)
        {
            this.mSelectedWeekBackgroundColor = paramInt;
            int i = this.mListView.getChildCount();
            for (int j = 0; j < i; j++)
            {
                WeekView localWeekView = (WeekView)this.mListView.getChildAt(j);
                if (localWeekView.mHasSelectedDay)
                    localWeekView.invalidate();
            }
        }
    }

    public void setShowWeekNumber(boolean paramBoolean)
    {
        if (this.mShowWeekNumber == paramBoolean);
        while (true)
        {
            return;
            this.mShowWeekNumber = paramBoolean;
            this.mAdapter.notifyDataSetChanged();
            setUpHeader();
        }
    }

    public void setShownWeekCount(int paramInt)
    {
        if (this.mShownWeekCount != paramInt)
        {
            this.mShownWeekCount = paramInt;
            invalidate();
        }
    }

    public void setUnfocusedMonthDateColor(int paramInt)
    {
        if (this.mUnfocusedMonthDateColor != paramInt)
        {
            this.mUnfocusedMonthDateColor = paramInt;
            int i = this.mListView.getChildCount();
            for (int j = 0; j < i; j++)
            {
                WeekView localWeekView = (WeekView)this.mListView.getChildAt(j);
                if (localWeekView.mHasUnfocusedDay)
                    localWeekView.invalidate();
            }
        }
    }

    public void setWeekDayTextAppearance(int paramInt)
    {
        if (this.mWeekDayTextAppearanceResId != paramInt)
        {
            this.mWeekDayTextAppearanceResId = paramInt;
            setUpHeader();
        }
    }

    public void setWeekNumberColor(int paramInt)
    {
        if (this.mWeekNumberColor != paramInt)
        {
            this.mWeekNumberColor = paramInt;
            if (this.mShowWeekNumber)
                invalidateAllWeekViews();
        }
    }

    public void setWeekSeparatorLineColor(int paramInt)
    {
        if (this.mWeekSeparatorLineColor != paramInt)
        {
            this.mWeekSeparatorLineColor = paramInt;
            invalidateAllWeekViews();
        }
    }

    private class WeekView extends View
    {
        private String[] mDayNumbers;
        private final Paint mDrawPaint = new Paint();
        private Calendar mFirstDay;
        private boolean[] mFocusDay;
        private boolean mHasFocusedDay;
        private boolean mHasSelectedDay = false;
        private boolean mHasUnfocusedDay;
        private int mHeight;
        private int mLastWeekDayMonth = -1;
        private final Paint mMonthNumDrawPaint = new Paint();
        private int mMonthOfFirstWeekDay = -1;
        private int mNumCells;
        private int mSelectedDay = -1;
        private int mSelectedLeft = -1;
        private int mSelectedRight = -1;
        private final Rect mTempRect = new Rect();
        private int mWeek = -1;
        private int mWidth;

        public WeekView(Context arg2)
        {
            super();
            initilaizePaints();
        }

        private void drawBackground(Canvas paramCanvas)
        {
            if (!this.mHasSelectedDay)
                return;
            this.mDrawPaint.setColor(CalendarView.this.mSelectedWeekBackgroundColor);
            this.mTempRect.top = CalendarView.this.mWeekSeperatorLineWidth;
            this.mTempRect.bottom = this.mHeight;
            Rect localRect = this.mTempRect;
            if (CalendarView.this.mShowWeekNumber);
            for (int i = this.mWidth / this.mNumCells; ; i = 0)
            {
                localRect.left = i;
                this.mTempRect.right = (-2 + this.mSelectedLeft);
                paramCanvas.drawRect(this.mTempRect, this.mDrawPaint);
                this.mTempRect.left = (3 + this.mSelectedRight);
                this.mTempRect.right = this.mWidth;
                paramCanvas.drawRect(this.mTempRect, this.mDrawPaint);
                break;
            }
        }

        private void drawSelectedDateVerticalBars(Canvas paramCanvas)
        {
            if (!this.mHasSelectedDay);
            while (true)
            {
                return;
                CalendarView.this.mSelectedDateVerticalBar.setBounds(this.mSelectedLeft - CalendarView.this.mSelectedDateVerticalBarWidth / 2, CalendarView.this.mWeekSeperatorLineWidth, this.mSelectedLeft + CalendarView.this.mSelectedDateVerticalBarWidth / 2, this.mHeight);
                CalendarView.this.mSelectedDateVerticalBar.draw(paramCanvas);
                CalendarView.this.mSelectedDateVerticalBar.setBounds(this.mSelectedRight - CalendarView.this.mSelectedDateVerticalBarWidth / 2, CalendarView.this.mWeekSeperatorLineWidth, this.mSelectedRight + CalendarView.this.mSelectedDateVerticalBarWidth / 2, this.mHeight);
                CalendarView.this.mSelectedDateVerticalBar.draw(paramCanvas);
            }
        }

        private void drawWeekNumbersAndDates(Canvas paramCanvas)
        {
            int i = (int)((this.mDrawPaint.getTextSize() + this.mHeight) / 2.0F) - CalendarView.this.mWeekSeperatorLineWidth;
            int j = this.mNumCells;
            this.mDrawPaint.setTextAlign(Paint.Align.CENTER);
            this.mDrawPaint.setTextSize(CalendarView.this.mDateTextSize);
            int k = 0;
            int m = j * 2;
            if (CalendarView.this.mShowWeekNumber)
            {
                this.mDrawPaint.setColor(CalendarView.this.mWeekNumberColor);
                int i2 = this.mWidth / m;
                paramCanvas.drawText(this.mDayNumbers[0], i2, i, this.mDrawPaint);
                k = 0 + 1;
            }
            if (k < j)
            {
                Paint localPaint = this.mMonthNumDrawPaint;
                if (this.mFocusDay[k] != 0);
                for (int n = CalendarView.this.mFocusedMonthDateColor; ; n = CalendarView.this.mUnfocusedMonthDateColor)
                {
                    localPaint.setColor(n);
                    int i1 = (1 + k * 2) * this.mWidth / m;
                    paramCanvas.drawText(this.mDayNumbers[k], i1, i, this.mMonthNumDrawPaint);
                    k++;
                    break;
                }
            }
        }

        private void drawWeekSeparators(Canvas paramCanvas)
        {
            int i = CalendarView.this.mListView.getFirstVisiblePosition();
            if (CalendarView.this.mListView.getChildAt(0).getTop() < 0)
                i++;
            if (i == this.mWeek)
                return;
            this.mDrawPaint.setColor(CalendarView.this.mWeekSeparatorLineColor);
            this.mDrawPaint.setStrokeWidth(CalendarView.this.mWeekSeperatorLineWidth);
            if (CalendarView.this.mShowWeekNumber);
            for (float f = this.mWidth / this.mNumCells; ; f = 0.0F)
            {
                paramCanvas.drawLine(f, 0.0F, this.mWidth, 0.0F, this.mDrawPaint);
                break;
            }
        }

        private void initilaizePaints()
        {
            this.mDrawPaint.setFakeBoldText(false);
            this.mDrawPaint.setAntiAlias(true);
            this.mDrawPaint.setStyle(Paint.Style.FILL);
            this.mMonthNumDrawPaint.setFakeBoldText(true);
            this.mMonthNumDrawPaint.setAntiAlias(true);
            this.mMonthNumDrawPaint.setStyle(Paint.Style.FILL);
            this.mMonthNumDrawPaint.setTextAlign(Paint.Align.CENTER);
        }

        private void updateSelectionPositions()
        {
            if (this.mHasSelectedDay)
            {
                int i = this.mSelectedDay - CalendarView.this.mFirstDayOfWeek;
                if (i < 0)
                    i += 7;
                if (CalendarView.this.mShowWeekNumber)
                    i++;
                this.mSelectedLeft = (i * this.mWidth / this.mNumCells);
                this.mSelectedRight = ((i + 1) * this.mWidth / this.mNumCells);
            }
        }

        public boolean getDayFromLocation(float paramFloat, Calendar paramCalendar)
        {
            boolean bool = false;
            int i;
            if (CalendarView.this.mShowWeekNumber)
            {
                i = this.mWidth / this.mNumCells;
                if ((paramFloat >= i) && (paramFloat <= this.mWidth))
                    break label53;
                paramCalendar.clear();
            }
            while (true)
            {
                return bool;
                i = 0;
                break;
                label53: int j = (int)((paramFloat - i) * CalendarView.this.mDaysPerWeek / (this.mWidth - i));
                paramCalendar.setTimeInMillis(this.mFirstDay.getTimeInMillis());
                paramCalendar.add(5, j);
                bool = true;
            }
        }

        public Calendar getFirstDay()
        {
            return this.mFirstDay;
        }

        public int getMonthOfFirstWeekDay()
        {
            return this.mMonthOfFirstWeekDay;
        }

        public int getMonthOfLastWeekDay()
        {
            return this.mLastWeekDayMonth;
        }

        public void init(int paramInt1, int paramInt2, int paramInt3)
        {
            this.mSelectedDay = paramInt2;
            boolean bool1;
            int i;
            label44: int j;
            label238: int m;
            label265: boolean bool3;
            if (this.mSelectedDay != -1)
            {
                bool1 = true;
                this.mHasSelectedDay = bool1;
                if (!CalendarView.this.mShowWeekNumber)
                    break label382;
                i = 1 + CalendarView.this.mDaysPerWeek;
                this.mNumCells = i;
                this.mWeek = paramInt1;
                CalendarView.this.mTempDate.setTimeInMillis(CalendarView.this.mMinDate.getTimeInMillis());
                CalendarView.this.mTempDate.add(3, this.mWeek);
                CalendarView.this.mTempDate.setFirstDayOfWeek(CalendarView.this.mFirstDayOfWeek);
                this.mDayNumbers = new String[this.mNumCells];
                this.mFocusDay = new boolean[this.mNumCells];
                j = 0;
                if (CalendarView.this.mShowWeekNumber)
                {
                    this.mDayNumbers[0] = Integer.toString(CalendarView.this.mTempDate.get(3));
                    j = 0 + 1;
                }
                int k = CalendarView.this.mFirstDayOfWeek - CalendarView.this.mTempDate.get(7);
                CalendarView.this.mTempDate.add(5, k);
                this.mFirstDay = ((Calendar)CalendarView.this.mTempDate.clone());
                this.mMonthOfFirstWeekDay = CalendarView.this.mTempDate.get(2);
                this.mHasUnfocusedDay = true;
                if (j >= this.mNumCells)
                    break label430;
                if (CalendarView.this.mTempDate.get(2) != paramInt3)
                    break label394;
                m = 1;
                this.mFocusDay[j] = m;
                this.mHasFocusedDay = (m | this.mHasFocusedDay);
                boolean bool2 = this.mHasUnfocusedDay;
                if (m != 0)
                    break label400;
                bool3 = true;
                label299: this.mHasUnfocusedDay = (bool3 & bool2);
                if ((!CalendarView.this.mTempDate.before(CalendarView.this.mMinDate)) && (!CalendarView.this.mTempDate.after(CalendarView.this.mMaxDate)))
                    break label406;
                this.mDayNumbers[j] = "";
            }
            while (true)
            {
                CalendarView.this.mTempDate.add(5, 1);
                j++;
                break label238;
                bool1 = false;
                break;
                label382: i = CalendarView.this.mDaysPerWeek;
                break label44;
                label394: m = 0;
                break label265;
                label400: bool3 = false;
                break label299;
                label406: this.mDayNumbers[j] = Integer.toString(CalendarView.this.mTempDate.get(5));
            }
            label430: if (CalendarView.this.mTempDate.get(5) == 1)
                CalendarView.this.mTempDate.add(5, -1);
            this.mLastWeekDayMonth = CalendarView.this.mTempDate.get(2);
            updateSelectionPositions();
        }

        protected void onDraw(Canvas paramCanvas)
        {
            drawBackground(paramCanvas);
            drawWeekNumbersAndDates(paramCanvas);
            drawWeekSeparators(paramCanvas);
            drawSelectedDateVerticalBars(paramCanvas);
        }

        protected void onMeasure(int paramInt1, int paramInt2)
        {
            this.mHeight = ((CalendarView.this.mListView.getHeight() - CalendarView.this.mListView.getPaddingTop() - CalendarView.this.mListView.getPaddingBottom()) / CalendarView.this.mShownWeekCount);
            setMeasuredDimension(View.MeasureSpec.getSize(paramInt1), this.mHeight);
        }

        protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.mWidth = paramInt1;
            updateSelectionPositions();
        }
    }

    private class WeeksAdapter extends BaseAdapter
        implements View.OnTouchListener
    {
        private int mFocusedMonth;
        private GestureDetector mGestureDetector;
        private final Calendar mSelectedDate = Calendar.getInstance();
        private int mSelectedWeek;
        private int mTotalWeekCount;

        public WeeksAdapter(Context arg2)
        {
            Context localContext;
            CalendarView.access$1402(CalendarView.this, localContext);
            this.mGestureDetector = new GestureDetector(CalendarView.access$1500(CalendarView.this), new CalendarGestureListener());
            init();
        }

        private void init()
        {
            this.mSelectedWeek = CalendarView.this.getWeeksSinceMinDate(this.mSelectedDate);
            this.mTotalWeekCount = CalendarView.this.getWeeksSinceMinDate(CalendarView.this.mMaxDate);
            if ((CalendarView.this.mMinDate.get(7) != CalendarView.this.mFirstDayOfWeek) || (CalendarView.this.mMaxDate.get(7) != CalendarView.this.mFirstDayOfWeek))
                this.mTotalWeekCount = (1 + this.mTotalWeekCount);
        }

        private void onDateTapped(Calendar paramCalendar)
        {
            setSelectedDay(paramCalendar);
            CalendarView.this.setMonthDisplayed(paramCalendar);
        }

        public int getCount()
        {
            return this.mTotalWeekCount;
        }

        public Object getItem(int paramInt)
        {
            return null;
        }

        public long getItemId(int paramInt)
        {
            return paramInt;
        }

        public Calendar getSelectedDay()
        {
            return this.mSelectedDate;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            CalendarView.WeekView localWeekView;
            if (paramView != null)
            {
                localWeekView = (CalendarView.WeekView)paramView;
                if (this.mSelectedWeek != paramInt)
                    break label95;
            }
            label95: for (int i = this.mSelectedDate.get(7); ; i = -1)
            {
                localWeekView.init(paramInt, i, this.mFocusedMonth);
                return localWeekView;
                localWeekView = new CalendarView.WeekView(CalendarView.this, CalendarView.access$2000(CalendarView.this));
                localWeekView.setLayoutParams(new AbsListView.LayoutParams(-2, -2));
                localWeekView.setClickable(true);
                localWeekView.setOnTouchListener(this);
                break;
            }
        }

        public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
        {
            boolean bool = true;
            if ((CalendarView.this.mListView.isEnabled()) && (this.mGestureDetector.onTouchEvent(paramMotionEvent)))
                if (((CalendarView.WeekView)paramView).getDayFromLocation(paramMotionEvent.getX(), CalendarView.this.mTempDate));
            while (true)
            {
                return bool;
                if ((!CalendarView.this.mTempDate.before(CalendarView.this.mMinDate)) && (!CalendarView.this.mTempDate.after(CalendarView.this.mMaxDate)))
                {
                    onDateTapped(CalendarView.this.mTempDate);
                    continue;
                    bool = false;
                }
            }
        }

        public void setFocusMonth(int paramInt)
        {
            if (this.mFocusedMonth == paramInt);
            while (true)
            {
                return;
                this.mFocusedMonth = paramInt;
                notifyDataSetChanged();
            }
        }

        public void setSelectedDay(Calendar paramCalendar)
        {
            if ((paramCalendar.get(6) == this.mSelectedDate.get(6)) && (paramCalendar.get(1) == this.mSelectedDate.get(1)));
            while (true)
            {
                return;
                this.mSelectedDate.setTimeInMillis(paramCalendar.getTimeInMillis());
                this.mSelectedWeek = CalendarView.this.getWeeksSinceMinDate(this.mSelectedDate);
                this.mFocusedMonth = this.mSelectedDate.get(2);
                notifyDataSetChanged();
            }
        }

        class CalendarGestureListener extends GestureDetector.SimpleOnGestureListener
        {
            CalendarGestureListener()
            {
            }

            public boolean onSingleTapUp(MotionEvent paramMotionEvent)
            {
                return true;
            }
        }
    }

    private class ScrollStateRunnable
        implements Runnable
    {
        private int mNewState;
        private AbsListView mView;

        private ScrollStateRunnable()
        {
        }

        public void doScrollStateChange(AbsListView paramAbsListView, int paramInt)
        {
            this.mView = paramAbsListView;
            this.mNewState = paramInt;
            CalendarView.this.removeCallbacks(this);
            CalendarView.this.postDelayed(this, 40L);
        }

        public void run()
        {
            CalendarView.access$1002(CalendarView.this, this.mNewState);
            int i;
            if ((this.mNewState == 0) && (CalendarView.this.mPreviousScrollState != 0))
            {
                View localView = this.mView.getChildAt(0);
                if (localView == null)
                    return;
                i = localView.getBottom() - CalendarView.this.mListScrollTopOffset;
                if (i > CalendarView.this.mListScrollTopOffset)
                {
                    if (!CalendarView.this.mIsScrollingUp)
                        break label111;
                    this.mView.smoothScrollBy(i - localView.getHeight(), 500);
                }
            }
            while (true)
            {
                CalendarView.access$1102(CalendarView.this, this.mNewState);
                break;
                label111: this.mView.smoothScrollBy(i, 500);
            }
        }
    }

    public static abstract interface OnDateChangeListener
    {
        public abstract void onSelectedDayChange(CalendarView paramCalendarView, int paramInt1, int paramInt2, int paramInt3);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.CalendarView
 * JD-Core Version:        0.6.2
 */