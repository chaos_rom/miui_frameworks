package android.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewRootImpl;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class ZoomButtonsController
    implements View.OnTouchListener
{
    private static final int MSG_DISMISS_ZOOM_CONTROLS = 3;
    private static final int MSG_POST_CONFIGURATION_CHANGED = 2;
    private static final int MSG_POST_SET_VISIBLE = 4;
    private static final String TAG = "ZoomButtonsController";
    private static final int ZOOM_CONTROLS_TIMEOUT = 0;
    private static final int ZOOM_CONTROLS_TOUCH_PADDING = 20;
    private boolean mAutoDismissControls = true;
    private OnZoomListener mCallback;
    private final IntentFilter mConfigurationChangedFilter = new IntentFilter("android.intent.action.CONFIGURATION_CHANGED");
    private final BroadcastReceiver mConfigurationChangedReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (!ZoomButtonsController.this.mIsVisible);
            while (true)
            {
                return;
                ZoomButtonsController.this.mHandler.removeMessages(2);
                ZoomButtonsController.this.mHandler.sendEmptyMessage(2);
            }
        }
    };
    private final FrameLayout mContainer;
    private WindowManager.LayoutParams mContainerLayoutParams;
    private final int[] mContainerRawLocation = new int[2];
    private final Context mContext;
    private ZoomControls mControls;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                ZoomButtonsController.this.onPostConfigurationChanged();
                continue;
                ZoomButtonsController.this.setVisible(false);
                continue;
                if (ZoomButtonsController.this.mOwnerView.getWindowToken() == null)
                    Log.e("ZoomButtonsController", "Cannot make the zoom controller visible if the owner view is not attached to a window.");
                else
                    ZoomButtonsController.this.setVisible(true);
            }
        }
    };
    private boolean mIsVisible;
    private final View mOwnerView;
    private final int[] mOwnerViewRawLocation = new int[2];
    private Runnable mPostedVisibleInitializer;
    private boolean mReleaseTouchListenerOnUp;
    private final int[] mTempIntArray = new int[2];
    private final Rect mTempRect = new Rect();
    private int mTouchPaddingScaledSq;
    private View mTouchTargetView;
    private final int[] mTouchTargetWindowLocation = new int[2];
    private final WindowManager mWindowManager;

    public ZoomButtonsController(View paramView)
    {
        this.mContext = paramView.getContext();
        this.mWindowManager = ((WindowManager)this.mContext.getSystemService("window"));
        this.mOwnerView = paramView;
        this.mTouchPaddingScaledSq = ((int)(20.0F * this.mContext.getResources().getDisplayMetrics().density));
        this.mTouchPaddingScaledSq *= this.mTouchPaddingScaledSq;
        this.mContainer = createContainer();
    }

    private FrameLayout createContainer()
    {
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(-2, -2);
        localLayoutParams.gravity = 51;
        localLayoutParams.flags = 131608;
        localLayoutParams.height = -2;
        localLayoutParams.width = -1;
        localLayoutParams.type = 1000;
        localLayoutParams.format = -3;
        localLayoutParams.windowAnimations = 16974311;
        this.mContainerLayoutParams = localLayoutParams;
        Container localContainer = new Container(this.mContext);
        localContainer.setLayoutParams(localLayoutParams);
        localContainer.setMeasureAllChildren(true);
        ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(17367244, localContainer);
        this.mControls = ((ZoomControls)localContainer.findViewById(16909165));
        this.mControls.setOnZoomInClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                ZoomButtonsController.this.dismissControlsDelayed(ZoomButtonsController.ZOOM_CONTROLS_TIMEOUT);
                if (ZoomButtonsController.this.mCallback != null)
                    ZoomButtonsController.this.mCallback.onZoom(true);
            }
        });
        this.mControls.setOnZoomOutClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                ZoomButtonsController.this.dismissControlsDelayed(ZoomButtonsController.ZOOM_CONTROLS_TIMEOUT);
                if (ZoomButtonsController.this.mCallback != null)
                    ZoomButtonsController.this.mCallback.onZoom(false);
            }
        });
        return localContainer;
    }

    private void dismissControlsDelayed(int paramInt)
    {
        if (this.mAutoDismissControls)
        {
            this.mHandler.removeMessages(3);
            this.mHandler.sendEmptyMessageDelayed(3, paramInt);
        }
    }

    private View findViewForTouch(int paramInt1, int paramInt2)
    {
        int i = paramInt1 - this.mContainerRawLocation[0];
        int j = paramInt2 - this.mContainerRawLocation[1];
        Rect localRect = this.mTempRect;
        Object localObject1 = null;
        int k = 2147483647;
        int m = -1 + this.mContainer.getChildCount();
        Object localObject2;
        while (true)
            if (m >= 0)
            {
                localObject2 = this.mContainer.getChildAt(m);
                if (((View)localObject2).getVisibility() != 0)
                {
                    m--;
                }
                else
                {
                    ((View)localObject2).getHitRect(localRect);
                    if (!localRect.contains(i, j))
                        break;
                }
            }
        while (true)
        {
            return localObject2;
            int n;
            if ((i >= localRect.left) && (i <= localRect.right))
            {
                n = 0;
                label117: if ((j < localRect.top) || (j > localRect.bottom))
                    break label208;
            }
            label208: for (int i1 = 0; ; i1 = Math.min(Math.abs(localRect.top - j), Math.abs(j - localRect.bottom)))
            {
                int i2 = n * n + i1 * i1;
                if ((i2 >= this.mTouchPaddingScaledSq) || (i2 >= k))
                    break;
                localObject1 = localObject2;
                k = i2;
                break;
                n = Math.min(Math.abs(localRect.left - i), Math.abs(i - localRect.right));
                break label117;
            }
            localObject2 = localObject1;
        }
    }

    private boolean isInterestingKey(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 4:
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        case 66:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private boolean onContainerKey(KeyEvent paramKeyEvent)
    {
        int i = 1;
        int k = paramKeyEvent.getKeyCode();
        if (isInterestingKey(k))
            if (k == 4)
                if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getRepeatCount() == 0))
                    if (this.mOwnerView != null)
                    {
                        KeyEvent.DispatcherState localDispatcherState = this.mOwnerView.getKeyDispatcherState();
                        if (localDispatcherState != null)
                            localDispatcherState.startTracking(paramKeyEvent, this);
                    }
        while (true)
        {
            return i;
            if ((paramKeyEvent.getAction() == i) && (paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
            {
                setVisible(false);
                continue;
                dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
            }
            else
            {
                int j = 0;
                continue;
                ViewRootImpl localViewRootImpl = this.mOwnerView.getViewRootImpl();
                if (localViewRootImpl != null)
                    localViewRootImpl.dispatchKey(paramKeyEvent);
            }
        }
    }

    private void onPostConfigurationChanged()
    {
        dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
        refreshPositioningVariables();
    }

    private void refreshPositioningVariables()
    {
        if (this.mOwnerView.getWindowToken() == null);
        while (true)
        {
            return;
            int i = this.mOwnerView.getHeight();
            int j = this.mOwnerView.getWidth();
            int k = i - this.mContainer.getHeight();
            this.mOwnerView.getLocationOnScreen(this.mOwnerViewRawLocation);
            this.mContainerRawLocation[0] = this.mOwnerViewRawLocation[0];
            this.mContainerRawLocation[1] = (k + this.mOwnerViewRawLocation[1]);
            int[] arrayOfInt = this.mTempIntArray;
            this.mOwnerView.getLocationInWindow(arrayOfInt);
            this.mContainerLayoutParams.x = arrayOfInt[0];
            this.mContainerLayoutParams.width = j;
            this.mContainerLayoutParams.y = (k + arrayOfInt[1]);
            if (this.mIsVisible)
                this.mWindowManager.updateViewLayout(this.mContainer, this.mContainerLayoutParams);
        }
    }

    private void setTouchTargetView(View paramView)
    {
        this.mTouchTargetView = paramView;
        if (paramView != null)
            paramView.getLocationInWindow(this.mTouchTargetWindowLocation);
    }

    public ViewGroup getContainer()
    {
        return this.mContainer;
    }

    public View getZoomControls()
    {
        return this.mControls;
    }

    public boolean isAutoDismissed()
    {
        return this.mAutoDismissControls;
    }

    public boolean isVisible()
    {
        return this.mIsVisible;
    }

    public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        int i = paramMotionEvent.getAction();
        if (paramMotionEvent.getPointerCount() > 1);
        label281: 
        while (true)
        {
            return bool;
            if (this.mReleaseTouchListenerOnUp)
            {
                if ((i == 1) || (i == 3))
                {
                    this.mOwnerView.setOnTouchListener(null);
                    setTouchTargetView(null);
                    this.mReleaseTouchListenerOnUp = false;
                }
                bool = true;
            }
            else
            {
                dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
                View localView = this.mTouchTargetView;
                switch (i)
                {
                case 2:
                default:
                case 0:
                case 1:
                case 3:
                }
                while (true)
                {
                    if (localView == null)
                        break label281;
                    int j = this.mContainerRawLocation[0] + this.mTouchTargetWindowLocation[0];
                    int k = this.mContainerRawLocation[1] + this.mTouchTargetWindowLocation[1];
                    MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
                    localMotionEvent.offsetLocation(this.mOwnerViewRawLocation[0] - j, this.mOwnerViewRawLocation[1] - k);
                    float f1 = localMotionEvent.getX();
                    float f2 = localMotionEvent.getY();
                    if ((f1 < 0.0F) && (f1 > -20.0F))
                        localMotionEvent.offsetLocation(-f1, 0.0F);
                    if ((f2 < 0.0F) && (f2 > -20.0F))
                        localMotionEvent.offsetLocation(0.0F, -f2);
                    bool = localView.dispatchTouchEvent(localMotionEvent);
                    localMotionEvent.recycle();
                    break;
                    localView = findViewForTouch((int)paramMotionEvent.getRawX(), (int)paramMotionEvent.getRawY());
                    setTouchTargetView(localView);
                    continue;
                    setTouchTargetView(null);
                }
            }
        }
    }

    public void setAutoDismissed(boolean paramBoolean)
    {
        if (this.mAutoDismissControls == paramBoolean);
        while (true)
        {
            return;
            this.mAutoDismissControls = paramBoolean;
        }
    }

    public void setFocusable(boolean paramBoolean)
    {
        int i = this.mContainerLayoutParams.flags;
        WindowManager.LayoutParams localLayoutParams2;
        if (paramBoolean)
            localLayoutParams2 = this.mContainerLayoutParams;
        WindowManager.LayoutParams localLayoutParams1;
        for (localLayoutParams2.flags = (0xFFFFFFF7 & localLayoutParams2.flags); ; localLayoutParams1.flags = (0x8 | localLayoutParams1.flags))
        {
            if ((this.mContainerLayoutParams.flags != i) && (this.mIsVisible))
                this.mWindowManager.updateViewLayout(this.mContainer, this.mContainerLayoutParams);
            return;
            localLayoutParams1 = this.mContainerLayoutParams;
        }
    }

    public void setOnZoomListener(OnZoomListener paramOnZoomListener)
    {
        this.mCallback = paramOnZoomListener;
    }

    public void setVisible(boolean paramBoolean)
    {
        if (paramBoolean)
            if (this.mOwnerView.getWindowToken() == null)
                if (!this.mHandler.hasMessages(4))
                    this.mHandler.sendEmptyMessage(4);
        while (true)
        {
            return;
            dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
            if (this.mIsVisible != paramBoolean)
            {
                this.mIsVisible = paramBoolean;
                if (!paramBoolean)
                    break;
                if (this.mContainerLayoutParams.token == null)
                    this.mContainerLayoutParams.token = this.mOwnerView.getWindowToken();
                this.mWindowManager.addView(this.mContainer, this.mContainerLayoutParams);
                if (this.mPostedVisibleInitializer == null)
                    this.mPostedVisibleInitializer = new Runnable()
                    {
                        public void run()
                        {
                            ZoomButtonsController.this.refreshPositioningVariables();
                            if (ZoomButtonsController.this.mCallback != null)
                                ZoomButtonsController.this.mCallback.onVisibilityChanged(true);
                        }
                    };
                this.mHandler.post(this.mPostedVisibleInitializer);
                this.mContext.registerReceiver(this.mConfigurationChangedReceiver, this.mConfigurationChangedFilter);
                this.mOwnerView.setOnTouchListener(this);
                this.mReleaseTouchListenerOnUp = false;
            }
        }
        if (this.mTouchTargetView != null)
            this.mReleaseTouchListenerOnUp = true;
        while (true)
        {
            this.mContext.unregisterReceiver(this.mConfigurationChangedReceiver);
            this.mWindowManager.removeView(this.mContainer);
            this.mHandler.removeCallbacks(this.mPostedVisibleInitializer);
            if (this.mCallback == null)
                break;
            this.mCallback.onVisibilityChanged(false);
            break;
            this.mOwnerView.setOnTouchListener(null);
        }
    }

    public void setZoomInEnabled(boolean paramBoolean)
    {
        this.mControls.setIsZoomInEnabled(paramBoolean);
    }

    public void setZoomOutEnabled(boolean paramBoolean)
    {
        this.mControls.setIsZoomOutEnabled(paramBoolean);
    }

    public void setZoomSpeed(long paramLong)
    {
        this.mControls.setZoomSpeed(paramLong);
    }

    private class Container extends FrameLayout
    {
        public Container(Context arg2)
        {
            super();
        }

        public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
        {
            if (ZoomButtonsController.this.onContainerKey(paramKeyEvent));
            for (boolean bool = true; ; bool = super.dispatchKeyEvent(paramKeyEvent))
                return bool;
        }
    }

    public static abstract interface OnZoomListener
    {
        public abstract void onVisibilityChanged(boolean paramBoolean);

        public abstract void onZoom(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ZoomButtonsController
 * JD-Core Version:        0.6.2
 */