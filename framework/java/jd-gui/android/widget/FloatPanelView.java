package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class FloatPanelView extends FrameLayout
{
    private int mBottomOffset;
    private boolean mIsArrowUp;
    private int mPos;
    private int mTopOffset;

    public FloatPanelView(Context paramContext)
    {
        super(paramContext);
        initArrowOffset(paramContext);
    }

    public FloatPanelView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        initArrowOffset(paramContext);
    }

    private void initArrowOffset(Context paramContext)
    {
        this.mTopOffset = ((int)paramContext.getResources().getDimension(101318695));
        this.mBottomOffset = ((int)paramContext.getResources().getDimension(101318696));
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        View localView1 = findViewById(101384223);
        View localView2 = findViewById(101384224);
        LinearLayout localLinearLayout = (LinearLayout)findViewById(101384219);
        if ((localView1 == null) || (localView2 == null) || (localLinearLayout == null))
        {
            Log.e("FloatPanelView", "couldn't find view");
            return;
        }
        localView1.setVisibility(8);
        localView2.setVisibility(8);
        View localView3;
        label87: int i;
        label97: int j;
        if (this.mIsArrowUp)
        {
            localView3 = localView1;
            if (!this.mIsArrowUp)
                break label210;
            i = 0;
            localView3.setVisibility(0);
            j = this.mPos - localView3.getMeasuredWidth() / 2;
            if (j >= 0)
                break label225;
            j = 0;
            label125: localView3.layout(j, i, j + localView3.getMeasuredWidth(), i + localView3.getMeasuredHeight());
            if (!this.mIsArrowUp)
                break label251;
        }
        label210: label225: label251: for (int k = localView1.getHeight() - this.mTopOffset; ; k = 0)
        {
            localLinearLayout.layout(localLinearLayout.getLeft(), k, localLinearLayout.getLeft() + localLinearLayout.getMeasuredWidth(), k + localLinearLayout.getMeasuredHeight());
            break;
            localView3 = localView2;
            break label87;
            i = localLinearLayout.getMeasuredHeight() - this.mBottomOffset;
            break label97;
            if (j <= paramInt3 - localView3.getMeasuredWidth())
                break label125;
            j = paramInt3 - localView3.getMeasuredWidth();
            break label125;
        }
    }

    public void setArrow(boolean paramBoolean)
    {
        if (paramBoolean != this.mIsArrowUp)
        {
            this.mIsArrowUp = paramBoolean;
            requestLayout();
        }
    }

    public void setArrowPos(int paramInt)
    {
        if (paramInt != this.mPos)
        {
            this.mPos = paramInt;
            requestLayout();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.FloatPanelView
 * JD-Core Version:        0.6.2
 */