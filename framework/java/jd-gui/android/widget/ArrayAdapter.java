package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ArrayAdapter<T> extends BaseAdapter
    implements Filterable
{
    private Context mContext;
    private int mDropDownResource;
    private int mFieldId = 0;
    private ArrayAdapter<T>.ArrayFilter mFilter;
    private LayoutInflater mInflater;
    private final Object mLock = new Object();
    private boolean mNotifyOnChange = true;
    private List<T> mObjects;
    private ArrayList<T> mOriginalValues;
    private int mResource;

    public ArrayAdapter(Context paramContext, int paramInt)
    {
        init(paramContext, paramInt, 0, new ArrayList());
    }

    public ArrayAdapter(Context paramContext, int paramInt1, int paramInt2)
    {
        init(paramContext, paramInt1, paramInt2, new ArrayList());
    }

    public ArrayAdapter(Context paramContext, int paramInt1, int paramInt2, List<T> paramList)
    {
        init(paramContext, paramInt1, paramInt2, paramList);
    }

    public ArrayAdapter(Context paramContext, int paramInt1, int paramInt2, T[] paramArrayOfT)
    {
        init(paramContext, paramInt1, paramInt2, Arrays.asList(paramArrayOfT));
    }

    public ArrayAdapter(Context paramContext, int paramInt, List<T> paramList)
    {
        init(paramContext, paramInt, 0, paramList);
    }

    public ArrayAdapter(Context paramContext, int paramInt, T[] paramArrayOfT)
    {
        init(paramContext, paramInt, 0, Arrays.asList(paramArrayOfT));
    }

    public static ArrayAdapter<CharSequence> createFromResource(Context paramContext, int paramInt1, int paramInt2)
    {
        return new ArrayAdapter(paramContext, paramInt2, paramContext.getResources().getTextArray(paramInt1));
    }

    private View createViewFromResource(int paramInt1, View paramView, ViewGroup paramViewGroup, int paramInt2)
    {
        View localView;
        if (paramView == null)
            localView = this.mInflater.inflate(paramInt2, paramViewGroup, false);
        while (true)
        {
            TextView localTextView;
            Object localObject;
            try
            {
                if (this.mFieldId == 0)
                {
                    localTextView = (TextView)localView;
                    localObject = getItem(paramInt1);
                    if (!(localObject instanceof CharSequence))
                        break label104;
                    localTextView.setText((CharSequence)localObject);
                    return localView;
                    localView = paramView;
                    continue;
                }
                localTextView = (TextView)localView.findViewById(this.mFieldId);
                continue;
            }
            catch (ClassCastException localClassCastException)
            {
                Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
                throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", localClassCastException);
            }
            label104: localTextView.setText(localObject.toString());
        }
    }

    private void init(Context paramContext, int paramInt1, int paramInt2, List<T> paramList)
    {
        this.mContext = paramContext;
        this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
        this.mDropDownResource = paramInt1;
        this.mResource = paramInt1;
        this.mObjects = paramList;
        this.mFieldId = paramInt2;
    }

    public void add(T paramT)
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                this.mOriginalValues.add(paramT);
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            this.mObjects.add(paramT);
        }
    }

    public void addAll(Collection<? extends T> paramCollection)
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                this.mOriginalValues.addAll(paramCollection);
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            this.mObjects.addAll(paramCollection);
        }
    }

    public void addAll(T[] paramArrayOfT)
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                Collections.addAll(this.mOriginalValues, paramArrayOfT);
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            Collections.addAll(this.mObjects, paramArrayOfT);
        }
    }

    public void clear()
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                this.mOriginalValues.clear();
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            this.mObjects.clear();
        }
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public int getCount()
    {
        return this.mObjects.size();
    }

    public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        return createViewFromResource(paramInt, paramView, paramViewGroup, this.mDropDownResource);
    }

    public Filter getFilter()
    {
        if (this.mFilter == null)
            this.mFilter = new ArrayFilter(null);
        return this.mFilter;
    }

    public T getItem(int paramInt)
    {
        return this.mObjects.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
        return paramInt;
    }

    public int getPosition(T paramT)
    {
        return this.mObjects.indexOf(paramT);
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        return createViewFromResource(paramInt, paramView, paramViewGroup, this.mResource);
    }

    public void insert(T paramT, int paramInt)
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                this.mOriginalValues.add(paramInt, paramT);
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            this.mObjects.add(paramInt, paramT);
        }
    }

    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
        this.mNotifyOnChange = true;
    }

    public void remove(T paramT)
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                this.mOriginalValues.remove(paramT);
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            this.mObjects.remove(paramT);
        }
    }

    public void setDropDownViewResource(int paramInt)
    {
        this.mDropDownResource = paramInt;
    }

    public void setNotifyOnChange(boolean paramBoolean)
    {
        this.mNotifyOnChange = paramBoolean;
    }

    public void sort(Comparator<? super T> paramComparator)
    {
        synchronized (this.mLock)
        {
            if (this.mOriginalValues != null)
            {
                Collections.sort(this.mOriginalValues, paramComparator);
                if (this.mNotifyOnChange)
                    notifyDataSetChanged();
                return;
            }
            Collections.sort(this.mObjects, paramComparator);
        }
    }

    private class ArrayFilter extends Filter
    {
        private ArrayFilter()
        {
        }

        protected Filter.FilterResults performFiltering(CharSequence paramCharSequence)
        {
            Filter.FilterResults localFilterResults = new Filter.FilterResults();
            if (ArrayAdapter.this.mOriginalValues == null);
            synchronized (ArrayAdapter.this.mLock)
            {
                ArrayAdapter.access$102(ArrayAdapter.this, new ArrayList(ArrayAdapter.this.mObjects));
                if (paramCharSequence != null)
                    if (paramCharSequence.length() != 0)
                        break label128;
            }
            while (true)
            {
                synchronized (ArrayAdapter.this.mLock)
                {
                    ArrayList localArrayList1 = new ArrayList(ArrayAdapter.this.mOriginalValues);
                    localFilterResults.values = localArrayList1;
                    localFilterResults.count = localArrayList1.size();
                    return localFilterResults;
                    localObject7 = finally;
                    throw localObject7;
                }
                label128: String str1 = paramCharSequence.toString().toLowerCase();
                ArrayList localArrayList3;
                label297: 
                while (true)
                {
                    Object localObject5;
                    String str2;
                    synchronized (ArrayAdapter.this.mLock)
                    {
                        ArrayList localArrayList2 = new ArrayList(ArrayAdapter.this.mOriginalValues);
                        int i = localArrayList2.size();
                        localArrayList3 = new ArrayList();
                        int j = 0;
                        if (j >= i)
                            break;
                        localObject5 = localArrayList2.get(j);
                        str2 = localObject5.toString().toLowerCase();
                        if (str2.startsWith(str1))
                        {
                            localArrayList3.add(localObject5);
                            j++;
                        }
                    }
                    String[] arrayOfString = str2.split(" ");
                    int k = arrayOfString.length;
                    for (int m = 0; ; m++)
                    {
                        if (m >= k)
                            break label297;
                        if (arrayOfString[m].startsWith(str1))
                        {
                            localArrayList3.add(localObject5);
                            break;
                        }
                    }
                }
                localFilterResults.values = localArrayList3;
                localFilterResults.count = localArrayList3.size();
            }
        }

        protected void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
        {
            ArrayAdapter.access$302(ArrayAdapter.this, (List)paramFilterResults.values);
            if (paramFilterResults.count > 0)
                ArrayAdapter.this.notifyDataSetChanged();
            while (true)
            {
                return;
                ArrayAdapter.this.notifyDataSetInvalidated();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ArrayAdapter
 * JD-Core Version:        0.6.2
 */