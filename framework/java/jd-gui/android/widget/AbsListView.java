package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.StrictMode;
import android.os.StrictMode.Span;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.StateSet;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.AccessibilityDelegate;
import android.view.View.BaseSavedState;
import android.view.ViewConfiguration;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnTouchModeChangeListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbsListView extends AdapterView<ListAdapter>
    implements TextWatcher, ViewTreeObserver.OnGlobalLayoutListener, Filter.FilterListener, ViewTreeObserver.OnTouchModeChangeListener, RemoteViewsAdapter.RemoteAdapterConnectionCallback
{
    private static final int CHECK_POSITION_SEARCH_DISTANCE = 20;
    public static final int CHOICE_MODE_MULTIPLE = 2;
    public static final int CHOICE_MODE_MULTIPLE_MODAL = 3;
    public static final int CHOICE_MODE_NONE = 0;
    public static final int CHOICE_MODE_SINGLE = 1;
    private static final int INVALID_POINTER = -1;
    static final int LAYOUT_FORCE_BOTTOM = 3;
    static final int LAYOUT_FORCE_TOP = 1;
    static final int LAYOUT_MOVE_SELECTION = 6;
    static final int LAYOUT_NORMAL = 0;
    static final int LAYOUT_SET_SELECTION = 2;
    static final int LAYOUT_SPECIFIC = 4;
    static final int LAYOUT_SYNC = 5;
    static final int OVERSCROLL_LIMIT_DIVISOR = 3;
    private static final boolean PROFILE_FLINGING = false;
    private static final boolean PROFILE_SCROLLING = false;
    private static final String TAG = "AbsListView";
    static final int TOUCH_MODE_DONE_WAITING = 2;
    static final int TOUCH_MODE_DOWN = 0;
    static final int TOUCH_MODE_FLING = 4;
    private static final int TOUCH_MODE_OFF = 1;
    private static final int TOUCH_MODE_ON = 0;
    static final int TOUCH_MODE_OVERFLING = 6;
    static final int TOUCH_MODE_OVERSCROLL = 5;
    static final int TOUCH_MODE_REST = -1;
    static final int TOUCH_MODE_SCROLL = 3;
    static final int TOUCH_MODE_TAP = 1;
    private static final int TOUCH_MODE_UNKNOWN = -1;
    public static final int TRANSCRIPT_MODE_ALWAYS_SCROLL = 2;
    public static final int TRANSCRIPT_MODE_DISABLED = 0;
    public static final int TRANSCRIPT_MODE_NORMAL = 1;
    static final Interpolator sLinearInterpolator = new LinearInterpolator();
    private ListItemAccessibilityDelegate mAccessibilityDelegate;
    private int mActivePointerId = -1;
    ListAdapter mAdapter;
    boolean mAdapterHasStableIds;
    private int mCacheColorHint;
    boolean mCachingActive;
    boolean mCachingStarted;
    SparseBooleanArray mCheckStates;
    LongSparseArray<Integer> mCheckedIdStates;
    int mCheckedItemCount;
    ActionMode mChoiceActionMode;
    int mChoiceMode = 0;
    private Runnable mClearScrollingCache;
    private ContextMenu.ContextMenuInfo mContextMenuInfo = null;
    AdapterDataSetObserver mDataSetObserver;
    private InputConnection mDefInputConnection;
    private boolean mDeferNotifyDataSetChanged = false;
    private float mDensityScale;
    private int mDirection = 0;
    boolean mDrawSelectorOnTop = false;
    private EdgeEffect mEdgeGlowBottom;
    private EdgeEffect mEdgeGlowTop;
    boolean mFastScrollEnabled;
    private FastScroller mFastScroller;
    private boolean mFiltered;
    private int mFirstPositionDistanceGuess;
    private boolean mFlingProfilingStarted = false;
    private FlingRunnable mFlingRunnable;
    private StrictMode.Span mFlingStrictSpan = null;
    private boolean mForceTranscriptScroll;
    private boolean mGlobalLayoutListenerAddedFilter;
    private int mGlowPaddingLeft;
    private int mGlowPaddingRight;
    boolean mIsAttached;
    private boolean mIsChildViewEnabled;
    final boolean[] mIsScrap = new boolean[1];
    private int mLastAccessibilityScrollEventFromIndex;
    private int mLastAccessibilityScrollEventToIndex;
    private int mLastHandledItemCount;
    private int mLastPositionDistanceGuess;
    private int mLastScrollState = 0;
    private int mLastTouchMode = -1;
    int mLastY;
    int mLayoutMode = 0;
    Rect mListPadding = new Rect();
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    int mMotionCorrection;
    int mMotionPosition;
    int mMotionViewNewTop;
    int mMotionViewOriginalTop;
    int mMotionX;
    int mMotionY;
    MultiChoiceModeWrapper mMultiChoiceModeCallback;
    private OnScrollListener mOnScrollListener;
    int mOverflingDistance;
    int mOverscrollDistance;
    int mOverscrollMax;
    private CheckForKeyLongPress mPendingCheckForKeyLongPress;
    private CheckForLongPress mPendingCheckForLongPress;
    private Runnable mPendingCheckForTap;
    private PerformClick mPerformClick;
    PopupWindow mPopup;
    private boolean mPopupHidden;
    Runnable mPositionScrollAfterLayout;
    PositionScroller mPositionScroller;
    private InputConnectionWrapper mPublicInputConnection;
    final RecycleBin mRecycler = new RecycleBin();
    private RemoteViewsAdapter mRemoteAdapter;
    int mResurrectToPosition = -1;
    View mScrollDown;
    private boolean mScrollProfilingStarted = false;
    private StrictMode.Span mScrollStrictSpan = null;
    View mScrollUp;
    boolean mScrollingCacheEnabled;
    int mSelectedTop = 0;
    int mSelectionBottomPadding = 0;
    int mSelectionLeftPadding = 0;
    int mSelectionRightPadding = 0;
    int mSelectionTopPadding = 0;
    Drawable mSelector;
    int mSelectorPosition = -1;
    Rect mSelectorRect = new Rect();
    private boolean mSmoothScrollbarEnabled = true;
    boolean mStackFromBottom;
    EditText mTextFilter;
    private boolean mTextFilterEnabled;
    private Rect mTouchFrame;
    int mTouchMode = -1;
    private Runnable mTouchModeReset;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    int mTouchPaddingLeft = 0;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    int mTouchPaddingRight = 0;
    private int mTouchSlop;
    private int mTranscriptMode;
    private float mVelocityScale = 1.0F;
    private VelocityTracker mVelocityTracker;
    int mWidthMeasureSpec = 0;

    public AbsListView(Context paramContext)
    {
        super(paramContext);
        initAbsListView();
        setVerticalScrollBarEnabled(true);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(R.styleable.View);
        initializeScrollbars(localTypedArray);
        localTypedArray.recycle();
    }

    public AbsListView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842858);
    }

    public AbsListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initAbsListView();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AbsListView, paramInt, 0);
        Drawable localDrawable = localTypedArray.getDrawable(0);
        if (localDrawable != null)
            setSelector(localDrawable);
        this.mDrawSelectorOnTop = localTypedArray.getBoolean(1, false);
        setStackFromBottom(localTypedArray.getBoolean(2, false));
        setScrollingCacheEnabled(localTypedArray.getBoolean(3, true));
        setTextFilterEnabled(localTypedArray.getBoolean(4, false));
        setTranscriptMode(localTypedArray.getInt(5, 0));
        setCacheColorHint(localTypedArray.getColor(6, 0));
        setFastScrollEnabled(localTypedArray.getBoolean(8, false));
        setSmoothScrollbarEnabled(localTypedArray.getBoolean(9, true));
        setChoiceMode(localTypedArray.getInt(7, 0));
        setFastScrollAlwaysVisible(localTypedArray.getBoolean(10, false));
        localTypedArray.recycle();
    }

    private boolean acceptFilter()
    {
        if ((this.mTextFilterEnabled) && ((getAdapter() instanceof Filterable)) && (((Filterable)getAdapter()).getFilter() != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void clearScrollingCache()
    {
        if (!isHardwareAccelerated())
        {
            if (this.mClearScrollingCache == null)
                this.mClearScrollingCache = new Runnable()
                {
                    public void run()
                    {
                        if (AbsListView.this.mCachingStarted)
                        {
                            AbsListView localAbsListView = AbsListView.this;
                            AbsListView.this.mCachingActive = false;
                            localAbsListView.mCachingStarted = false;
                            AbsListView.this.setChildrenDrawnWithCacheEnabled(false);
                            if ((0x2 & AbsListView.access$3100(AbsListView.this)) == 0)
                                AbsListView.this.setChildrenDrawingCacheEnabled(false);
                            if (!AbsListView.this.isAlwaysDrawnWithCacheEnabled())
                                AbsListView.this.invalidate();
                        }
                    }
                };
            post(this.mClearScrollingCache);
        }
    }

    private boolean contentFits()
    {
        boolean bool = true;
        int i = getChildCount();
        if (i == 0);
        while (true)
        {
            return bool;
            if (i != this.mItemCount)
                bool = false;
            else if ((getChildAt(0).getTop() < this.mListPadding.top) || (getChildAt(i - 1).getBottom() > getHeight() - this.mListPadding.bottom))
                bool = false;
        }
    }

    private void createScrollingCache()
    {
        if ((this.mScrollingCacheEnabled) && (!this.mCachingStarted) && (!isHardwareAccelerated()))
        {
            setChildrenDrawnWithCacheEnabled(true);
            setChildrenDrawingCacheEnabled(true);
            this.mCachingActive = true;
            this.mCachingStarted = true;
        }
    }

    private void createTextFilter(boolean paramBoolean)
    {
        if (this.mPopup == null)
        {
            Context localContext = getContext();
            PopupWindow localPopupWindow = new PopupWindow(localContext);
            this.mTextFilter = ((EditText)((LayoutInflater)localContext.getSystemService("layout_inflater")).inflate(17367233, null));
            this.mTextFilter.setRawInputType(177);
            this.mTextFilter.setImeOptions(268435456);
            this.mTextFilter.addTextChangedListener(this);
            localPopupWindow.setFocusable(false);
            localPopupWindow.setTouchable(false);
            localPopupWindow.setInputMethodMode(2);
            localPopupWindow.setContentView(this.mTextFilter);
            localPopupWindow.setWidth(-2);
            localPopupWindow.setHeight(-2);
            localPopupWindow.setBackgroundDrawable(null);
            this.mPopup = localPopupWindow;
            getViewTreeObserver().addOnGlobalLayoutListener(this);
            this.mGlobalLayoutListenerAddedFilter = true;
        }
        if (paramBoolean)
            this.mPopup.setAnimationStyle(16974305);
        while (true)
        {
            return;
            this.mPopup.setAnimationStyle(16974306);
        }
    }

    private void dismissPopup()
    {
        if (this.mPopup != null)
            this.mPopup.dismiss();
    }

    private void drawSelector(Canvas paramCanvas)
    {
        if (!this.mSelectorRect.isEmpty())
        {
            Drawable localDrawable = this.mSelector;
            localDrawable.setBounds(this.mSelectorRect);
            localDrawable.draw(paramCanvas);
        }
    }

    private void finishGlows()
    {
        if (this.mEdgeGlowTop != null)
        {
            this.mEdgeGlowTop.finish();
            this.mEdgeGlowBottom.finish();
        }
    }

    static int getDistance(Rect paramRect1, Rect paramRect2, int paramInt)
    {
        int i;
        int j;
        int k;
        int m;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}.");
        case 66:
            i = paramRect1.right;
            j = paramRect1.top + paramRect1.height() / 2;
            k = paramRect2.left;
            m = paramRect2.top + paramRect2.height() / 2;
        case 130:
        case 17:
        case 33:
        case 1:
        case 2:
        }
        while (true)
        {
            int n = k - i;
            int i1 = m - j;
            return i1 * i1 + n * n;
            i = paramRect1.left + paramRect1.width() / 2;
            j = paramRect1.bottom;
            k = paramRect2.left + paramRect2.width() / 2;
            m = paramRect2.top;
            continue;
            i = paramRect1.left;
            j = paramRect1.top + paramRect1.height() / 2;
            k = paramRect2.right;
            m = paramRect2.top + paramRect2.height() / 2;
            continue;
            i = paramRect1.left + paramRect1.width() / 2;
            j = paramRect1.top;
            k = paramRect2.left + paramRect2.width() / 2;
            m = paramRect2.bottom;
            continue;
            i = paramRect1.right + paramRect1.width() / 2;
            j = paramRect1.top + paramRect1.height() / 2;
            k = paramRect2.left + paramRect2.width() / 2;
            m = paramRect2.top + paramRect2.height() / 2;
        }
    }

    private void initAbsListView()
    {
        setClickable(true);
        setFocusableInTouchMode(true);
        setWillNotDraw(false);
        setAlwaysDrawnWithCacheEnabled(false);
        setScrollingCacheEnabled(true);
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(this.mContext);
        this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
        this.mMinimumVelocity = localViewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = localViewConfiguration.getScaledMaximumFlingVelocity();
        this.mOverscrollDistance = localViewConfiguration.getScaledOverscrollDistance();
        this.mOverflingDistance = localViewConfiguration.getScaledOverflingDistance();
        this.mDensityScale = getContext().getResources().getDisplayMetrics().density;
    }

    private void initOrResetVelocityTracker()
    {
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        while (true)
        {
            return;
            this.mVelocityTracker.clear();
        }
    }

    private void initVelocityTrackerIfNotExists()
    {
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
    }

    private void onSecondaryPointerUp(MotionEvent paramMotionEvent)
    {
        int i = (0xFF00 & paramMotionEvent.getAction()) >> 8;
        if (paramMotionEvent.getPointerId(i) == this.mActivePointerId)
            if (i != 0)
                break label65;
        label65: for (int j = 1; ; j = 0)
        {
            this.mMotionX = ((int)paramMotionEvent.getX(j));
            this.mMotionY = ((int)paramMotionEvent.getY(j));
            this.mMotionCorrection = 0;
            this.mActivePointerId = paramMotionEvent.getPointerId(j);
            return;
        }
    }

    private void positionPopup()
    {
        int i = getResources().getDisplayMetrics().heightPixels;
        int[] arrayOfInt = new int[2];
        getLocationOnScreen(arrayOfInt);
        int j = i - arrayOfInt[1] - getHeight() + (int)(20.0F * this.mDensityScale);
        if (!this.mPopup.isShowing())
            this.mPopup.showAtLocation(this, 81, arrayOfInt[0], j);
        while (true)
        {
            return;
            this.mPopup.update(arrayOfInt[0], j, -1, -1);
        }
    }

    private void positionSelector(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mSelectorRect.set(paramInt1 - this.mSelectionLeftPadding, paramInt2 - this.mSelectionTopPadding, paramInt3 + this.mSelectionRightPadding, paramInt4 + this.mSelectionBottomPadding);
    }

    private void recycleVelocityTracker()
    {
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    static View retrieveFromScrap(ArrayList<View> paramArrayList, int paramInt)
    {
        int i = paramArrayList.size();
        int j;
        View localView;
        if (i > 0)
        {
            j = 0;
            if (j < i)
            {
                localView = (View)paramArrayList.get(j);
                if (((LayoutParams)localView.getLayoutParams()).scrappedFromPosition == paramInt)
                    paramArrayList.remove(j);
            }
        }
        while (true)
        {
            return localView;
            j++;
            break;
            localView = (View)paramArrayList.remove(i - 1);
            continue;
            localView = null;
        }
    }

    private void scrollIfNeeded(int paramInt)
    {
        int i = paramInt - this.mMotionY;
        int j = i - this.mMotionCorrection;
        int k;
        if (this.mLastY != -2147483648)
        {
            k = paramInt - this.mLastY;
            if (this.mTouchMode != 3)
                break label415;
            if (this.mScrollStrictSpan == null)
                this.mScrollStrictSpan = StrictMode.enterCriticalSpan("AbsListView-scroll");
            if (paramInt != this.mLastY)
            {
                if (((0x80000 & this.mGroupFlags) == 0) && (Math.abs(i) > this.mTouchSlop))
                {
                    localViewParent = getParent();
                    if (localViewParent != null)
                        localViewParent.requestDisallowInterceptTouchEvent(true);
                }
                if (this.mMotionPosition < 0)
                    break label352;
                i7 = this.mMotionPosition - this.mFirstPosition;
                i8 = 0;
                localView2 = getChildAt(i7);
                if (localView2 != null)
                    i8 = localView2.getTop();
                bool = false;
                if (k != 0)
                    bool = trackMotionScroll(j, k);
                localView3 = getChildAt(i7);
                if (localView3 != null)
                {
                    i9 = localView3.getTop();
                    if (bool)
                    {
                        i10 = -k - (i9 - i8);
                        overScrollBy(0, i10, 0, this.mScrollY, 0, 0, 0, this.mOverscrollDistance, true);
                        if ((Math.abs(this.mOverscrollDistance) == Math.abs(this.mScrollY)) && (this.mVelocityTracker != null))
                            this.mVelocityTracker.clear();
                        i11 = getOverScrollMode();
                        if ((i11 == 0) || ((i11 == 1) && (!contentFits())))
                        {
                            this.mDirection = 0;
                            this.mTouchMode = 5;
                            if (i <= 0)
                                break label363;
                            this.mEdgeGlowTop.onPull(i10 / getHeight());
                            if (!this.mEdgeGlowBottom.isFinished())
                                this.mEdgeGlowBottom.onRelease();
                            invalidate(this.mEdgeGlowTop.getBounds(false));
                        }
                    }
                    this.mMotionY = paramInt;
                }
                this.mLastY = paramInt;
            }
        }
        label352: label363: 
        while ((this.mTouchMode != 5) || (paramInt == this.mLastY))
            while (true)
            {
                ViewParent localViewParent;
                int i8;
                View localView2;
                boolean bool;
                View localView3;
                int i9;
                int i10;
                int i11;
                return;
                k = j;
                break;
                int i7 = getChildCount() / 2;
                continue;
                if (i < 0)
                {
                    this.mEdgeGlowBottom.onPull(i10 / getHeight());
                    if (!this.mEdgeGlowTop.isFinished())
                        this.mEdgeGlowTop.onRelease();
                    invalidate(this.mEdgeGlowBottom.getBounds(true));
                }
            }
        label415: int m = this.mScrollY;
        int n = m - k;
        int i1;
        label455: int i2;
        int i3;
        label505: int i4;
        label604: View localView1;
        if (paramInt > this.mLastY)
        {
            i1 = 1;
            if (this.mDirection == 0)
                this.mDirection = i1;
            i2 = -k;
            if (((n >= 0) || (m < 0)) && ((n <= 0) || (m > 0)))
                break label714;
            i2 = -m;
            i3 = k + i2;
            if (i2 != 0)
            {
                overScrollBy(0, i2, 0, this.mScrollY, 0, 0, 0, this.mOverscrollDistance, true);
                int i6 = getOverScrollMode();
                if ((i6 == 0) || ((i6 == 1) && (!contentFits())))
                {
                    if (i <= 0)
                        break label720;
                    this.mEdgeGlowTop.onPull(i2 / getHeight());
                    if (!this.mEdgeGlowBottom.isFinished())
                        this.mEdgeGlowBottom.onRelease();
                    invalidate(this.mEdgeGlowTop.getBounds(false));
                }
            }
            if (i3 != 0)
            {
                if (this.mScrollY != 0)
                {
                    this.mScrollY = 0;
                    invalidateParentIfNeeded();
                }
                trackMotionScroll(i3, i3);
                this.mTouchMode = 3;
                i4 = findClosestMotionRow(paramInt);
                this.mMotionCorrection = 0;
                localView1 = getChildAt(i4 - this.mFirstPosition);
                if (localView1 == null)
                    break label772;
            }
        }
        label772: for (int i5 = localView1.getTop(); ; i5 = 0)
        {
            this.mMotionViewOriginalTop = i5;
            this.mMotionY = paramInt;
            this.mMotionPosition = i4;
            this.mLastY = paramInt;
            this.mDirection = i1;
            break;
            i1 = -1;
            break label455;
            label714: i3 = 0;
            break label505;
            label720: if (i >= 0)
                break label604;
            this.mEdgeGlowBottom.onPull(i2 / getHeight());
            if (!this.mEdgeGlowTop.isFinished())
                this.mEdgeGlowTop.onRelease();
            invalidate(this.mEdgeGlowBottom.getBounds(true));
            break label604;
        }
    }

    private void showPopup()
    {
        if (getWindowVisibility() == 0)
        {
            createTextFilter(true);
            positionPopup();
            checkFocus();
        }
    }

    private boolean startScrollIfNeeded(int paramInt)
    {
        int i = paramInt - this.mMotionY;
        int j = Math.abs(i);
        int k;
        if (this.mScrollY != 0)
        {
            k = 1;
            if ((k == 0) && (j <= this.mTouchSlop))
                break label180;
            createScrollingCache();
            if (k == 0)
                break label146;
            this.mTouchMode = 5;
            this.mMotionCorrection = 0;
            Handler localHandler = getHandler();
            if (localHandler != null)
                localHandler.removeCallbacks(this.mPendingCheckForLongPress);
            setPressed(false);
            View localView = getChildAt(this.mMotionPosition - this.mFirstPosition);
            if (localView != null)
                localView.setPressed(false);
            reportScrollStateChange(1);
            ViewParent localViewParent = getParent();
            if (localViewParent != null)
                localViewParent.requestDisallowInterceptTouchEvent(true);
            scrollIfNeeded(paramInt);
        }
        label146: label180: for (boolean bool = true; ; bool = false)
        {
            return bool;
            k = 0;
            break;
            this.mTouchMode = 3;
            if (i > 0);
            for (int m = this.mTouchSlop; ; m = -this.mTouchSlop)
            {
                this.mMotionCorrection = m;
                break;
            }
        }
    }

    private void updateOnScreenCheckedViews()
    {
        int i = this.mFirstPosition;
        int j = getChildCount();
        int k;
        int m;
        label30: View localView;
        int n;
        if (getContext().getApplicationInfo().targetSdkVersion >= 11)
        {
            k = 1;
            m = 0;
            if (m >= j)
                return;
            localView = getChildAt(m);
            n = i + m;
            if (!(localView instanceof Checkable))
                break label88;
            ((Checkable)localView).setChecked(this.mCheckStates.get(n));
        }
        while (true)
        {
            m++;
            break label30;
            k = 0;
            break;
            label88: if (k != 0)
                localView.setActivated(this.mCheckStates.get(n));
        }
    }

    private void useDefaultSelector()
    {
        setSelector(getResources().getDrawable(17301602));
    }

    public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
    {
        if ((paramInt2 & 0x2) == 2);
        switch (paramInt1)
        {
        default:
            super.addFocusables(paramArrayList, paramInt1, paramInt2);
        case 4097:
        case 4098:
        }
        while (true)
        {
            return;
            if (getChildCount() > 0);
            for (Object localObject = getChildAt(-1 + getChildCount()); ; localObject = this)
            {
                if (!((View)localObject).isAccessibilityFocusable())
                    break label82;
                paramArrayList.add(localObject);
                break;
            }
            label82: continue;
            if (isAccessibilityFocusable())
                paramArrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> paramArrayList)
    {
        int i = getChildCount();
        int j = this.mFirstPosition;
        ListAdapter localListAdapter = this.mAdapter;
        if (localListAdapter == null);
        while (true)
        {
            return;
            for (int k = 0; k < i; k++)
            {
                View localView = getChildAt(k);
                if (localListAdapter.isEnabled(j + k))
                    paramArrayList.add(localView);
                localView.addTouchables(paramArrayList);
            }
        }
    }

    public void afterTextChanged(Editable paramEditable)
    {
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public boolean checkInputConnectionProxy(View paramView)
    {
        if (paramView == this.mTextFilter);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    public void clearChoices()
    {
        if (this.mCheckStates != null)
            this.mCheckStates.clear();
        if (this.mCheckedIdStates != null)
            this.mCheckedIdStates.clear();
        this.mCheckedItemCount = 0;
    }

    public void clearTextFilter()
    {
        if (this.mFiltered)
        {
            this.mTextFilter.setText("");
            this.mFiltered = false;
            if ((this.mPopup != null) && (this.mPopup.isShowing()))
                dismissPopup();
        }
    }

    protected int computeVerticalScrollExtent()
    {
        int i = getChildCount();
        int j;
        if (i > 0)
            if (this.mSmoothScrollbarEnabled)
            {
                j = i * 100;
                View localView1 = getChildAt(0);
                int k = localView1.getTop();
                int m = localView1.getHeight();
                if (m > 0)
                    j += k * 100 / m;
                View localView2 = getChildAt(i - 1);
                int n = localView2.getBottom();
                int i1 = localView2.getHeight();
                if (i1 > 0)
                    j -= 100 * (n - getHeight()) / i1;
            }
        while (true)
        {
            return j;
            j = 1;
            continue;
            j = 0;
        }
    }

    protected int computeVerticalScrollOffset()
    {
        int i = 0;
        int j = this.mFirstPosition;
        int k = getChildCount();
        if ((j >= 0) && (k > 0))
        {
            if (!this.mSmoothScrollbarEnabled)
                break label96;
            View localView = getChildAt(0);
            int i1 = localView.getTop();
            int i2 = localView.getHeight();
            if (i2 > 0)
                i = Math.max(j * 100 - i1 * 100 / i2 + (int)(100.0F * (this.mScrollY / getHeight() * this.mItemCount)), 0);
        }
        return i;
        label96: int m = this.mItemCount;
        int n;
        if (j == 0)
            n = 0;
        while (true)
        {
            i = (int)(j + k * (n / m));
            break;
            if (j + k == m)
                n = m;
            else
                n = j + k / 2;
        }
    }

    protected int computeVerticalScrollRange()
    {
        int i;
        if (this.mSmoothScrollbarEnabled)
        {
            i = Math.max(100 * this.mItemCount, 0);
            if (this.mScrollY != 0)
                i += Math.abs((int)(100.0F * (this.mScrollY / getHeight() * this.mItemCount)));
        }
        while (true)
        {
            return i;
            i = this.mItemCount;
        }
    }

    void confirmCheckedPositionsById()
    {
        this.mCheckStates.clear();
        int i = 0;
        int j = 0;
        if (j < this.mCheckedIdStates.size())
        {
            long l = this.mCheckedIdStates.keyAt(j);
            int k = ((Integer)this.mCheckedIdStates.valueAt(j)).intValue();
            int i2;
            if (l != this.mAdapter.getItemId(k))
            {
                int m = Math.max(0, k - 20);
                int n = Math.min(k + 20, this.mItemCount);
                int i1 = 0;
                i2 = m;
                label95: if (i2 < n)
                {
                    if (l == this.mAdapter.getItemId(i2))
                    {
                        i1 = 1;
                        this.mCheckStates.put(i2, true);
                        this.mCheckedIdStates.setValueAt(j, Integer.valueOf(i2));
                    }
                }
                else if (i1 == 0)
                {
                    this.mCheckedIdStates.delete(l);
                    j--;
                    this.mCheckedItemCount = (-1 + this.mCheckedItemCount);
                    i = 1;
                    if ((this.mChoiceActionMode != null) && (this.mMultiChoiceModeCallback != null))
                        this.mMultiChoiceModeCallback.onItemCheckedStateChanged(this.mChoiceActionMode, k, l, false);
                }
            }
            while (true)
            {
                j++;
                break;
                i2++;
                break label95;
                this.mCheckStates.put(k, true);
            }
        }
        if ((i != 0) && (this.mChoiceActionMode != null))
            this.mChoiceActionMode.invalidate();
    }

    ContextMenu.ContextMenuInfo createContextMenuInfo(View paramView, int paramInt, long paramLong)
    {
        return new AdapterView.AdapterContextMenuInfo(paramView, paramInt, paramLong);
    }

    public void deferNotifyDataSetChanged()
    {
        this.mDeferNotifyDataSetChanged = true;
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        int i = 0;
        if ((0x22 & this.mGroupFlags) == 34);
        for (int j = 1; ; j = 0)
        {
            if (j != 0)
            {
                i = paramCanvas.save();
                int k = this.mScrollX;
                int m = this.mScrollY;
                paramCanvas.clipRect(k + this.mPaddingLeft, m + this.mPaddingTop, k + this.mRight - this.mLeft - this.mPaddingRight, m + this.mBottom - this.mTop - this.mPaddingBottom);
                this.mGroupFlags = (0xFFFFFFDD & this.mGroupFlags);
            }
            boolean bool = this.mDrawSelectorOnTop;
            if (!bool)
                drawSelector(paramCanvas);
            super.dispatchDraw(paramCanvas);
            if (bool)
                drawSelector(paramCanvas);
            if (j != 0)
            {
                paramCanvas.restoreToCount(i);
                this.mGroupFlags = (0x22 | this.mGroupFlags);
            }
            return;
        }
    }

    protected void dispatchSetPressed(boolean paramBoolean)
    {
    }

    public void draw(Canvas paramCanvas)
    {
        super.draw(paramCanvas);
        if (this.mEdgeGlowTop != null)
        {
            int k = this.mScrollY;
            if (!this.mEdgeGlowTop.isFinished())
            {
                int i6 = paramCanvas.save();
                int i7 = this.mListPadding.left + this.mGlowPaddingLeft;
                int i8 = this.mListPadding.right + this.mGlowPaddingRight;
                int i9 = getWidth() - i7 - i8;
                int i10 = Math.min(0, k + this.mFirstPositionDistanceGuess);
                paramCanvas.translate(i7, i10);
                this.mEdgeGlowTop.setSize(i9, getHeight());
                if (this.mEdgeGlowTop.draw(paramCanvas))
                {
                    this.mEdgeGlowTop.setPosition(i7, i10);
                    invalidate(this.mEdgeGlowTop.getBounds(false));
                }
                paramCanvas.restoreToCount(i6);
            }
            if (!this.mEdgeGlowBottom.isFinished())
            {
                int m = paramCanvas.save();
                int n = this.mListPadding.left + this.mGlowPaddingLeft;
                int i1 = this.mListPadding.right + this.mGlowPaddingRight;
                int i2 = getWidth() - n - i1;
                int i3 = getHeight();
                int i4 = n + -i2;
                int i5 = Math.max(i3, k + this.mLastPositionDistanceGuess);
                paramCanvas.translate(i4, i5);
                paramCanvas.rotate(180.0F, i2, 0.0F);
                this.mEdgeGlowBottom.setSize(i2, i3);
                if (this.mEdgeGlowBottom.draw(paramCanvas))
                {
                    this.mEdgeGlowBottom.setPosition(i4 + i2, i5);
                    invalidate(this.mEdgeGlowBottom.getBounds(true));
                }
                paramCanvas.restoreToCount(m);
            }
        }
        if (this.mFastScroller != null)
        {
            int i = this.mScrollY;
            if (i == 0)
                break label351;
            int j = paramCanvas.save();
            paramCanvas.translate(0.0F, i);
            this.mFastScroller.draw(paramCanvas);
            paramCanvas.restoreToCount(j);
        }
        while (true)
        {
            return;
            label351: this.mFastScroller.draw(paramCanvas);
        }
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        updateSelectorState();
    }

    abstract void fillGap(boolean paramBoolean);

    int findClosestMotionRow(int paramInt)
    {
        int i = getChildCount();
        int j;
        if (i == 0)
            j = -1;
        while (true)
        {
            return j;
            j = findMotionRow(paramInt);
            if (j == -1)
                j = -1 + (i + this.mFirstPosition);
        }
    }

    abstract int findMotionRow(int paramInt);

    public View findViewToTakeAccessibilityFocusFromHover(View paramView1, View paramView2)
    {
        int i = getPositionForView(paramView1);
        if (i != -1);
        for (View localView = getChildAt(i - this.mFirstPosition); ; localView = super.findViewToTakeAccessibilityFocusFromHover(paramView1, paramView2))
            return localView;
    }

    public View focusSearch(int paramInt)
    {
        return focusSearch(this, paramInt);
    }

    public View focusSearch(View paramView, int paramInt)
    {
        Object localObject1;
        switch (paramInt)
        {
        default:
            localObject1 = super.focusSearch(paramView, paramInt);
        case 4098:
        case 4097:
        }
        while (true)
        {
            return localObject1;
            if (paramView == this)
            {
                int i1 = getChildCount();
                for (int i2 = 0; ; i2++)
                {
                    if (i2 >= i1)
                        break label78;
                    localObject1 = getChildAt(i2);
                    if (((View)localObject1).getVisibility() == 0)
                        break;
                }
                label78: localObject1 = super.focusSearch(this, paramInt);
            }
            else
            {
                int m = getPositionForView(paramView);
                if ((m < 0) || (m >= getCount()))
                {
                    localObject1 = super.focusSearch(this, paramInt);
                }
                else
                {
                    View localView4 = getChildAt(m - getFirstVisiblePosition());
                    if ((localView4.getVisibility() == 0) && ((localView4 instanceof ViewGroup)))
                    {
                        ViewGroup localViewGroup2 = (ViewGroup)localView4;
                        View localView5 = FocusFinder.getInstance().findNextFocus(localViewGroup2, paramView, paramInt);
                        if ((localView5 != null) && (localView5 != localViewGroup2) && (localView5 != paramView))
                            localObject1 = localView5;
                    }
                    else
                    {
                        for (int n = 1 + (m - getFirstVisiblePosition()); ; n++)
                        {
                            if (n >= getChildCount())
                                break label236;
                            localObject1 = getChildAt(n);
                            if (((View)localObject1).getVisibility() == 0)
                                break;
                        }
                        label236: localObject1 = super.focusSearch(this, paramInt);
                        continue;
                        if (paramView == this)
                        {
                            for (int k = -1 + getChildCount(); ; k--)
                            {
                                if (k < 0)
                                    break label300;
                                View localView3 = getChildAt(k);
                                if (localView3.getVisibility() == 0)
                                {
                                    localObject1 = super.focusSearch(localView3, paramInt);
                                    break;
                                }
                            }
                            label300: localObject1 = super.focusSearch(this, paramInt);
                        }
                        else
                        {
                            int i = getPositionForView(paramView);
                            if ((i < 0) || (i >= getCount()))
                            {
                                localObject1 = super.focusSearch(this, paramInt);
                            }
                            else
                            {
                                Object localObject2 = getChildAt(i - getFirstVisiblePosition());
                                if (localObject2 == paramView)
                                {
                                    localObject2 = null;
                                    paramView = null;
                                    for (int j = -1 + (i - getFirstVisiblePosition()); ; j--)
                                        if (j >= 0)
                                        {
                                            View localView2 = getChildAt(j);
                                            if (localView2.getVisibility() == 0)
                                                localObject2 = localView2;
                                        }
                                        else
                                        {
                                            if (localObject2 != null)
                                                break label416;
                                            localObject1 = this;
                                            break;
                                        }
                                }
                                label416: if (((View)localObject2).getVisibility() == 0)
                                {
                                    if ((localObject2 instanceof ViewGroup))
                                    {
                                        ViewGroup localViewGroup1 = (ViewGroup)localObject2;
                                        View localView1 = FocusFinder.getInstance().findNextFocus(localViewGroup1, paramView, paramInt);
                                        if ((localView1 != null) && (localView1 != localViewGroup1) && (localView1 != paramView))
                                            localObject1 = localView1;
                                    }
                                    else
                                    {
                                        localObject1 = localObject2;
                                    }
                                }
                                else
                                    localObject1 = super.focusSearch(this, paramInt);
                            }
                        }
                    }
                }
            }
        }
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-1, -2, 0);
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    protected float getBottomFadingEdgeStrength()
    {
        int i = getChildCount();
        float f1 = super.getBottomFadingEdgeStrength();
        if (i == 0);
        while (true)
        {
            return f1;
            if (-1 + (i + this.mFirstPosition) < -1 + this.mItemCount)
            {
                f1 = 1.0F;
            }
            else
            {
                int j = getChildAt(i - 1).getBottom();
                int k = getHeight();
                float f2 = getVerticalFadingEdgeLength();
                if (j > k - this.mPaddingBottom)
                    f1 = (j - k + this.mPaddingBottom) / f2;
            }
        }
    }

    protected int getBottomPaddingOffset()
    {
        if ((0x22 & this.mGroupFlags) == 34);
        for (int i = 0; ; i = this.mPaddingBottom)
            return i;
    }

    @ViewDebug.ExportedProperty(category="drawing")
    public int getCacheColorHint()
    {
        return this.mCacheColorHint;
    }

    public int getCheckedItemCount()
    {
        return this.mCheckedItemCount;
    }

    public long[] getCheckedItemIds()
    {
        long[] arrayOfLong;
        if ((this.mChoiceMode == 0) || (this.mCheckedIdStates == null) || (this.mAdapter == null))
            arrayOfLong = new long[0];
        while (true)
        {
            return arrayOfLong;
            LongSparseArray localLongSparseArray = this.mCheckedIdStates;
            int i = localLongSparseArray.size();
            arrayOfLong = new long[i];
            for (int j = 0; j < i; j++)
                arrayOfLong[j] = localLongSparseArray.keyAt(j);
        }
    }

    public int getCheckedItemPosition()
    {
        if ((this.mChoiceMode == 1) && (this.mCheckStates != null) && (this.mCheckStates.size() == 1));
        for (int i = this.mCheckStates.keyAt(0); ; i = -1)
            return i;
    }

    public SparseBooleanArray getCheckedItemPositions()
    {
        if (this.mChoiceMode != 0);
        for (SparseBooleanArray localSparseBooleanArray = this.mCheckStates; ; localSparseBooleanArray = null)
            return localSparseBooleanArray;
    }

    public int getChoiceMode()
    {
        return this.mChoiceMode;
    }

    protected ContextMenu.ContextMenuInfo getContextMenuInfo()
    {
        return this.mContextMenuInfo;
    }

    public void getFocusedRect(Rect paramRect)
    {
        View localView = getSelectedView();
        if ((localView != null) && (localView.getParent() == this))
        {
            localView.getFocusedRect(paramRect);
            offsetDescendantRectToMyCoords(localView, paramRect);
        }
        while (true)
        {
            return;
            super.getFocusedRect(paramRect);
        }
    }

    int getFooterViewsCount()
    {
        return 0;
    }

    int getHeaderViewsCount()
    {
        return 0;
    }

    protected int getLeftPaddingOffset()
    {
        if ((0x22 & this.mGroupFlags) == 34);
        for (int i = 0; ; i = -this.mPaddingLeft)
            return i;
    }

    public int getListPaddingBottom()
    {
        return this.mListPadding.bottom;
    }

    public int getListPaddingLeft()
    {
        return this.mListPadding.left;
    }

    public int getListPaddingRight()
    {
        return this.mListPadding.right;
    }

    public int getListPaddingTop()
    {
        return this.mListPadding.top;
    }

    protected int getRightPaddingOffset()
    {
        if ((0x22 & this.mGroupFlags) == 34);
        for (int i = 0; ; i = this.mPaddingRight)
            return i;
    }

    @ViewDebug.ExportedProperty
    public View getSelectedView()
    {
        if ((this.mItemCount > 0) && (this.mSelectedPosition >= 0));
        for (View localView = getChildAt(this.mSelectedPosition - this.mFirstPosition); ; localView = null)
            return localView;
    }

    public Drawable getSelector()
    {
        return this.mSelector;
    }

    public int getSolidColor()
    {
        return this.mCacheColorHint;
    }

    public CharSequence getTextFilter()
    {
        if ((this.mTextFilterEnabled) && (this.mTextFilter != null));
        for (Editable localEditable = this.mTextFilter.getText(); ; localEditable = null)
            return localEditable;
    }

    protected float getTopFadingEdgeStrength()
    {
        int i = getChildCount();
        float f1 = super.getTopFadingEdgeStrength();
        if (i == 0);
        while (true)
        {
            return f1;
            if (this.mFirstPosition > 0)
            {
                f1 = 1.0F;
            }
            else
            {
                int j = getChildAt(0).getTop();
                float f2 = getVerticalFadingEdgeLength();
                if (j < this.mPaddingTop)
                    f1 = -(j - this.mPaddingTop) / f2;
            }
        }
    }

    protected int getTopPaddingOffset()
    {
        if ((0x22 & this.mGroupFlags) == 34);
        for (int i = 0; ; i = -this.mPaddingTop)
            return i;
    }

    public int getTranscriptMode()
    {
        return this.mTranscriptMode;
    }

    public int getVerticalScrollbarWidth()
    {
        if (isFastScrollAlwaysVisible());
        for (int i = Math.max(super.getVerticalScrollbarWidth(), this.mFastScroller.getWidth()); ; i = super.getVerticalScrollbarWidth())
            return i;
    }

    protected void handleDataChanged()
    {
        int i = 3;
        int j = this.mItemCount;
        int k = this.mLastHandledItemCount;
        this.mLastHandledItemCount = this.mItemCount;
        if ((this.mChoiceMode != 0) && (this.mAdapter != null) && (this.mAdapter.hasStableIds()))
            confirmCheckedPositionsById();
        this.mRecycler.clearTransientStateViews();
        label398: label421: if (j > 0)
        {
            if (this.mNeedSync)
            {
                this.mNeedSync = false;
                if (this.mTranscriptMode == 2)
                    this.mLayoutMode = i;
            }
            label187: 
            do
                while (true)
                {
                    return;
                    if (this.mTranscriptMode == 1)
                    {
                        if (this.mForceTranscriptScroll)
                        {
                            this.mForceTranscriptScroll = false;
                            this.mLayoutMode = i;
                        }
                        else
                        {
                            int i3 = getChildCount();
                            int i4 = getHeight() - getPaddingBottom();
                            View localView = getChildAt(i3 - 1);
                            if (localView != null);
                            for (int i5 = localView.getBottom(); ; i5 = i4)
                            {
                                if ((i3 + this.mFirstPosition < k) || (i5 > i4))
                                    break label187;
                                this.mLayoutMode = i;
                                break;
                            }
                            awakenScrollBars();
                        }
                    }
                    else
                    {
                        int m;
                        switch (this.mSyncMode)
                        {
                        default:
                        case 0:
                            int i2;
                            do
                            {
                                if (isInTouchMode())
                                    break label421;
                                m = getSelectedItemPosition();
                                if (m >= j)
                                    m = j - 1;
                                if (m < 0)
                                    m = 0;
                                int n = lookForSelectablePosition(m, true);
                                if (n < 0)
                                    break label398;
                                setNextSelectedPositionInt(n);
                                break;
                                if (isInTouchMode())
                                {
                                    this.mLayoutMode = 5;
                                    this.mSyncPosition = Math.min(Math.max(0, this.mSyncPosition), j - 1);
                                    break;
                                }
                                i2 = findSyncPosition();
                            }
                            while ((i2 < 0) || (lookForSelectablePosition(i2, true) != i2));
                            this.mSyncPosition = i2;
                            if (this.mSyncHeight == getHeight());
                            for (this.mLayoutMode = 5; ; this.mLayoutMode = 2)
                            {
                                setNextSelectedPositionInt(i2);
                                break;
                            }
                        case 1:
                        }
                        this.mLayoutMode = 5;
                        this.mSyncPosition = Math.min(Math.max(0, this.mSyncPosition), j - 1);
                        continue;
                        int i1 = lookForSelectablePosition(m, false);
                        if (i1 < 0)
                            break label428;
                        setNextSelectedPositionInt(i1);
                    }
                }
            while (this.mResurrectToPosition >= 0);
        }
        label428: if (this.mStackFromBottom);
        while (true)
        {
            this.mLayoutMode = i;
            this.mSelectedPosition = -1;
            this.mSelectedRowId = -9223372036854775808L;
            this.mNextSelectedPosition = -1;
            this.mNextSelectedRowId = -9223372036854775808L;
            this.mNeedSync = false;
            this.mSelectorPosition = -1;
            checkSelectionChanged();
            break;
            i = 1;
        }
    }

    public boolean hasTextFilter()
    {
        return this.mFiltered;
    }

    void hideSelector()
    {
        if (this.mSelectedPosition != -1)
        {
            if (this.mLayoutMode != 4)
                this.mResurrectToPosition = this.mSelectedPosition;
            if ((this.mNextSelectedPosition >= 0) && (this.mNextSelectedPosition != this.mSelectedPosition))
                this.mResurrectToPosition = this.mNextSelectedPosition;
            setSelectedPositionInt(-1);
            setNextSelectedPositionInt(-1);
            this.mSelectedTop = 0;
        }
    }

    public void invalidateViews()
    {
        this.mDataChanged = true;
        rememberSyncState();
        requestLayout();
        invalidate();
    }

    void invokeOnItemScrollListener()
    {
        if (this.mFastScroller != null)
            this.mFastScroller.onScroll(this, this.mFirstPosition, getChildCount(), this.mItemCount);
        if (this.mOnScrollListener != null)
            this.mOnScrollListener.onScroll(this, this.mFirstPosition, getChildCount(), this.mItemCount);
        onScrollChanged(0, 0, 0, 0);
    }

    public boolean isFastScrollAlwaysVisible()
    {
        if ((this.mFastScrollEnabled) && (this.mFastScroller.isAlwaysShowEnabled()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @ViewDebug.ExportedProperty
    public boolean isFastScrollEnabled()
    {
        return this.mFastScrollEnabled;
    }

    protected boolean isInFilterMode()
    {
        return this.mFiltered;
    }

    public boolean isItemChecked(int paramInt)
    {
        if ((this.mChoiceMode != 0) && (this.mCheckStates != null));
        for (boolean bool = this.mCheckStates.get(paramInt); ; bool = false)
            return bool;
    }

    protected boolean isPaddingOffsetRequired()
    {
        if ((0x22 & this.mGroupFlags) != 34);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @ViewDebug.ExportedProperty
    public boolean isScrollingCacheEnabled()
    {
        return this.mScrollingCacheEnabled;
    }

    @ViewDebug.ExportedProperty
    public boolean isSmoothScrollbarEnabled()
    {
        return this.mSmoothScrollbarEnabled;
    }

    @ViewDebug.ExportedProperty
    public boolean isStackFromBottom()
    {
        return this.mStackFromBottom;
    }

    @ViewDebug.ExportedProperty
    public boolean isTextFilterEnabled()
    {
        return this.mTextFilterEnabled;
    }

    protected boolean isVerticalScrollBarHidden()
    {
        if ((this.mFastScroller != null) && (this.mFastScroller.isVisible()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mSelector != null)
            this.mSelector.jumpToCurrentState();
    }

    void keyPressed()
    {
        if ((!isEnabled()) || (!isClickable()));
        label181: label190: 
        while (true)
        {
            return;
            Drawable localDrawable1 = this.mSelector;
            Rect localRect = this.mSelectorRect;
            if ((localDrawable1 != null) && ((isFocused()) || (touchModeDrawsInPressedState())) && (!localRect.isEmpty()))
            {
                View localView = getChildAt(this.mSelectedPosition - this.mFirstPosition);
                if (localView != null)
                {
                    if (!localView.hasFocusable())
                        localView.setPressed(true);
                }
                else
                {
                    setPressed(true);
                    boolean bool = isLongClickable();
                    Drawable localDrawable2 = localDrawable1.getCurrent();
                    if ((localDrawable2 != null) && ((localDrawable2 instanceof TransitionDrawable)))
                    {
                        if (!bool)
                            break label181;
                        ((TransitionDrawable)localDrawable2).startTransition(ViewConfiguration.getLongPressTimeout());
                    }
                    while (true)
                    {
                        if ((!bool) || (this.mDataChanged))
                            break label190;
                        if (this.mPendingCheckForKeyLongPress == null)
                            this.mPendingCheckForKeyLongPress = new CheckForKeyLongPress(null);
                        this.mPendingCheckForKeyLongPress.rememberWindowAttachCount();
                        postDelayed(this.mPendingCheckForKeyLongPress, ViewConfiguration.getLongPressTimeout());
                        break;
                        ((TransitionDrawable)localDrawable2).resetTransition();
                    }
                }
            }
        }
    }

    protected void layoutChildren()
    {
    }

    View obtainView(int paramInt, boolean[] paramArrayOfBoolean)
    {
        paramArrayOfBoolean[0] = false;
        View localView1 = this.mRecycler.getTransientStateView(paramInt);
        View localView3;
        if (localView1 != null)
        {
            localView3 = localView1;
            return localView3;
        }
        View localView2 = this.mRecycler.getScrapView(paramInt);
        label100: ViewGroup.LayoutParams localLayoutParams;
        LayoutParams localLayoutParams1;
        if (localView2 != null)
        {
            localView3 = this.mAdapter.getView(paramInt, localView2, this);
            if (localView3.getImportantForAccessibility() == 0)
                localView3.setImportantForAccessibility(1);
            if (localView3 != localView2)
            {
                this.mRecycler.addScrapView(localView2, paramInt);
                if (this.mCacheColorHint != 0)
                    localView3.setDrawingCacheBackgroundColor(this.mCacheColorHint);
                if (this.mAdapterHasStableIds)
                {
                    localLayoutParams = localView3.getLayoutParams();
                    if (localLayoutParams != null)
                        break label253;
                    localLayoutParams1 = (LayoutParams)generateDefaultLayoutParams();
                }
            }
        }
        while (true)
        {
            localLayoutParams1.itemId = this.mAdapter.getItemId(paramInt);
            localView3.setLayoutParams(localLayoutParams1);
            if (!AccessibilityManager.getInstance(this.mContext).isEnabled())
                break;
            if (this.mAccessibilityDelegate == null)
                this.mAccessibilityDelegate = new ListItemAccessibilityDelegate();
            localView3.setAccessibilityDelegate(this.mAccessibilityDelegate);
            break;
            paramArrayOfBoolean[0] = true;
            localView3.dispatchFinishTemporaryDetach();
            break label100;
            localView3 = this.mAdapter.getView(paramInt, null, this);
            if (localView3.getImportantForAccessibility() == 0)
                localView3.setImportantForAccessibility(1);
            if (this.mCacheColorHint == 0)
                break label100;
            localView3.setDrawingCacheBackgroundColor(this.mCacheColorHint);
            break label100;
            label253: if (!checkLayoutParams(localLayoutParams))
                localLayoutParams1 = (LayoutParams)generateLayoutParams(localLayoutParams);
            else
                localLayoutParams1 = (LayoutParams)localLayoutParams;
        }
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
        localViewTreeObserver.addOnTouchModeChangeListener(this);
        if ((this.mTextFilterEnabled) && (this.mPopup != null) && (!this.mGlobalLayoutListenerAddedFilter))
            localViewTreeObserver.addOnGlobalLayoutListener(this);
        if ((this.mAdapter != null) && (this.mDataSetObserver == null))
        {
            this.mDataSetObserver = new AdapterDataSetObserver();
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            this.mDataChanged = true;
            this.mOldItemCount = this.mItemCount;
            this.mItemCount = this.mAdapter.getCount();
        }
        this.mIsAttached = true;
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        if (this.mIsChildViewEnabled)
        {
            arrayOfInt = super.onCreateDrawableState(paramInt);
            return arrayOfInt;
        }
        int i = ENABLED_STATE_SET[0];
        int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
        int j = -1;
        for (int k = -1 + arrayOfInt.length; ; k--)
            if (k >= 0)
            {
                if (arrayOfInt[k] == i)
                    j = k;
            }
            else
            {
                if (j < 0)
                    break;
                System.arraycopy(arrayOfInt, j + 1, arrayOfInt, j, -1 + (arrayOfInt.length - j));
                break;
            }
    }

    public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo)
    {
        if (isTextFilterEnabled())
        {
            createTextFilter(false);
            if (this.mPublicInputConnection == null)
            {
                this.mDefInputConnection = new BaseInputConnection(this, false);
                this.mPublicInputConnection = new InputConnectionWrapper(this.mTextFilter.onCreateInputConnection(paramEditorInfo), true)
                {
                    public boolean performEditorAction(int paramAnonymousInt)
                    {
                        boolean bool = false;
                        if (paramAnonymousInt == 6)
                        {
                            InputMethodManager localInputMethodManager = (InputMethodManager)AbsListView.this.getContext().getSystemService("input_method");
                            if (localInputMethodManager != null)
                                localInputMethodManager.hideSoftInputFromWindow(AbsListView.this.getWindowToken(), 0);
                            bool = true;
                        }
                        return bool;
                    }

                    public boolean reportFullscreenMode(boolean paramAnonymousBoolean)
                    {
                        return AbsListView.this.mDefInputConnection.reportFullscreenMode(paramAnonymousBoolean);
                    }

                    public boolean sendKeyEvent(KeyEvent paramAnonymousKeyEvent)
                    {
                        return AbsListView.this.mDefInputConnection.sendKeyEvent(paramAnonymousKeyEvent);
                    }
                };
            }
            paramEditorInfo.inputType = 177;
            paramEditorInfo.imeOptions = 6;
        }
        for (InputConnectionWrapper localInputConnectionWrapper = this.mPublicInputConnection; ; localInputConnectionWrapper = null)
            return localInputConnectionWrapper;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        dismissPopup();
        this.mRecycler.clear();
        ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
        localViewTreeObserver.removeOnTouchModeChangeListener(this);
        if ((this.mTextFilterEnabled) && (this.mPopup != null))
        {
            localViewTreeObserver.removeOnGlobalLayoutListener(this);
            this.mGlobalLayoutListenerAddedFilter = false;
        }
        if (this.mAdapter != null)
        {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            this.mDataSetObserver = null;
        }
        if (this.mScrollStrictSpan != null)
        {
            this.mScrollStrictSpan.finish();
            this.mScrollStrictSpan = null;
        }
        if (this.mFlingStrictSpan != null)
        {
            this.mFlingStrictSpan.finish();
            this.mFlingStrictSpan = null;
        }
        if (this.mFlingRunnable != null)
            removeCallbacks(this.mFlingRunnable);
        if (this.mPositionScroller != null)
            this.mPositionScroller.stop();
        if (this.mClearScrollingCache != null)
            removeCallbacks(this.mClearScrollingCache);
        if (this.mPerformClick != null)
            removeCallbacks(this.mPerformClick);
        if (this.mTouchModeReset != null)
        {
            removeCallbacks(this.mTouchModeReset);
            this.mTouchModeReset = null;
        }
        this.mIsAttached = false;
    }

    protected void onDisplayHint(int paramInt)
    {
        super.onDisplayHint(paramInt);
        switch (paramInt)
        {
        default:
            if (paramInt != 4)
                break;
        case 4:
        case 0:
        }
        for (boolean bool = true; ; bool = false)
        {
            this.mPopupHidden = bool;
            return;
            if ((this.mPopup == null) || (!this.mPopup.isShowing()))
                break;
            dismissPopup();
            break;
            if ((!this.mFiltered) || (this.mPopup == null) || (this.mPopup.isShowing()))
                break;
            showPopup();
            break;
        }
    }

    public void onFilterComplete(int paramInt)
    {
        if ((this.mSelectedPosition < 0) && (paramInt > 0))
        {
            this.mResurrectToPosition = -1;
            resurrectSelection();
        }
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
        if ((paramBoolean) && (this.mSelectedPosition < 0) && (!isInTouchMode()))
        {
            if ((!this.mIsAttached) && (this.mAdapter != null))
            {
                this.mDataChanged = true;
                this.mOldItemCount = this.mItemCount;
                this.mItemCount = this.mAdapter.getCount();
            }
            resurrectSelection();
        }
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if ((0x2 & paramMotionEvent.getSource()) != 0)
            switch (paramMotionEvent.getAction())
            {
            default:
            case 8:
            }
        for (boolean bool = super.onGenericMotionEvent(paramMotionEvent); ; bool = true)
        {
            return bool;
            if (this.mTouchMode != -1)
                break;
            float f = paramMotionEvent.getAxisValue(9);
            if (f == 0.0F)
                break;
            int i = (int)(f * getVerticalScrollFactor());
            if (trackMotionScroll(i, i))
                break;
        }
    }

    public void onGlobalLayout()
    {
        if (isShown())
            if ((this.mFiltered) && (this.mPopup != null) && (!this.mPopup.isShowing()) && (!this.mPopupHidden))
                showPopup();
        while (true)
        {
            return;
            if ((this.mPopup != null) && (this.mPopup.isShowing()))
                dismissPopup();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(AbsListView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(AbsListView.class.getName());
        if (isEnabled())
        {
            if (getFirstVisiblePosition() > 0)
                paramAccessibilityNodeInfo.addAction(8192);
            if (getLastVisiblePosition() < -1 + getCount())
                paramAccessibilityNodeInfo.addAction(4096);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        int i = paramMotionEvent.getAction();
        if (this.mPositionScroller != null)
            this.mPositionScroller.stop();
        if (!this.mIsAttached);
        while (true)
        {
            return bool;
            if ((this.mFastScroller != null) && (this.mFastScroller.onInterceptTouchEvent(paramMotionEvent)))
                bool = true;
            else
                switch (i & 0xFF)
                {
                case 4:
                case 5:
                default:
                    break;
                case 0:
                    int m = this.mTouchMode;
                    if ((m == 6) || (m == 5))
                    {
                        this.mMotionCorrection = 0;
                        bool = true;
                    }
                    else
                    {
                        int n = (int)paramMotionEvent.getX();
                        int i1 = (int)paramMotionEvent.getY();
                        this.mActivePointerId = paramMotionEvent.getPointerId(0);
                        int i2 = findMotionRow(i1);
                        if ((m != 4) && (i2 >= 0))
                        {
                            this.mMotionViewOriginalTop = getChildAt(i2 - this.mFirstPosition).getTop();
                            this.mMotionX = n;
                            this.mMotionY = i1;
                            this.mMotionPosition = i2;
                            this.mTouchMode = 0;
                            clearScrollingCache();
                        }
                        this.mLastY = -2147483648;
                        initOrResetVelocityTracker();
                        this.mVelocityTracker.addMovement(paramMotionEvent);
                        if (m == 4)
                            bool = true;
                    }
                    break;
                case 2:
                    switch (this.mTouchMode)
                    {
                    default:
                        break;
                    case 0:
                        int j = paramMotionEvent.findPointerIndex(this.mActivePointerId);
                        if (j == -1)
                        {
                            j = 0;
                            this.mActivePointerId = paramMotionEvent.getPointerId(0);
                        }
                        int k = (int)paramMotionEvent.getY(j);
                        initVelocityTrackerIfNotExists();
                        this.mVelocityTracker.addMovement(paramMotionEvent);
                        if (startScrollIfNeeded(k))
                            bool = true;
                        break;
                    }
                    break;
                case 1:
                case 3:
                    this.mTouchMode = -1;
                    this.mActivePointerId = -1;
                    recycleVelocityTracker();
                    reportScrollStateChange(0);
                    break;
                case 6:
                    onSecondaryPointerUp(paramMotionEvent);
                }
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        switch (paramInt)
        {
        default:
            bool = super.onKeyUp(paramInt, paramKeyEvent);
        case 23:
        case 66:
        }
        while (true)
        {
            return bool;
            if (isEnabled())
            {
                if ((!isClickable()) || (!isPressed()) || (this.mSelectedPosition < 0) || (this.mAdapter == null) || (this.mSelectedPosition >= this.mAdapter.getCount()))
                    break;
                View localView = getChildAt(this.mSelectedPosition - this.mFirstPosition);
                if (localView != null)
                {
                    performItemClick(localView, this.mSelectedPosition, this.mSelectedRowId);
                    localView.setPressed(false);
                }
                setPressed(false);
            }
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        this.mInLayout = true;
        if (paramBoolean)
        {
            int i = getChildCount();
            for (int j = 0; j < i; j++)
                getChildAt(j).forceLayout();
            this.mRecycler.markChildrenDirty();
        }
        if ((this.mFastScroller != null) && (this.mItemCount != this.mOldItemCount))
            this.mFastScroller.onItemCountChanged(this.mOldItemCount, this.mItemCount);
        layoutChildren();
        this.mInLayout = false;
        this.mOverscrollMax = ((paramInt4 - paramInt2) / 3);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = 1;
        if (this.mSelector == null)
            useDefaultSelector();
        Rect localRect = this.mListPadding;
        localRect.left = (this.mSelectionLeftPadding + this.mPaddingLeft);
        localRect.top = (this.mSelectionTopPadding + this.mPaddingTop);
        localRect.right = (this.mSelectionRightPadding + this.mPaddingRight);
        localRect.bottom = (this.mSelectionBottomPadding + this.mPaddingBottom);
        int k;
        int m;
        if (this.mTranscriptMode == i)
        {
            int j = getChildCount();
            k = getHeight() - getPaddingBottom();
            View localView = getChildAt(j - 1);
            if (localView == null)
                break label149;
            m = localView.getBottom();
            if ((j + this.mFirstPosition < this.mLastHandledItemCount) || (m > k))
                break label156;
        }
        while (true)
        {
            this.mForceTranscriptScroll = i;
            return;
            label149: m = k;
            break;
            label156: i = 0;
        }
    }

    protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mScrollY != paramInt2)
        {
            onScrollChanged(this.mScrollX, paramInt2, this.mScrollX, this.mScrollY);
            this.mScrollY = paramInt2;
            invalidateParentIfNeeded();
            awakenScrollBars();
        }
    }

    public boolean onRemoteAdapterConnected()
    {
        boolean bool = false;
        if (this.mRemoteAdapter != this.mAdapter)
        {
            setAdapter(this.mRemoteAdapter);
            if (this.mDeferNotifyDataSetChanged)
            {
                this.mRemoteAdapter.notifyDataSetChanged();
                this.mDeferNotifyDataSetChanged = false;
            }
        }
        while (true)
        {
            return bool;
            if (this.mRemoteAdapter != null)
            {
                this.mRemoteAdapter.superNotifyDataSetChanged();
                bool = true;
            }
        }
    }

    public void onRemoteAdapterDisconnected()
    {
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        this.mDataChanged = true;
        this.mSyncHeight = localSavedState.height;
        if (localSavedState.selectedId >= 0L)
        {
            this.mNeedSync = true;
            this.mSyncRowId = localSavedState.selectedId;
            this.mSyncPosition = localSavedState.position;
            this.mSpecificTop = localSavedState.viewTop;
            this.mSyncMode = 0;
        }
        while (true)
        {
            setFilterText(localSavedState.filter);
            if (localSavedState.checkState != null)
                this.mCheckStates = localSavedState.checkState;
            if (localSavedState.checkIdState != null)
                this.mCheckedIdStates = localSavedState.checkIdState;
            this.mCheckedItemCount = localSavedState.checkedItemCount;
            if ((localSavedState.inActionMode) && (this.mChoiceMode == 3) && (this.mMultiChoiceModeCallback != null))
                this.mChoiceActionMode = startActionMode(this.mMultiChoiceModeCallback);
            requestLayout();
            return;
            if (localSavedState.firstId >= 0L)
            {
                setSelectedPositionInt(-1);
                setNextSelectedPositionInt(-1);
                this.mSelectorPosition = -1;
                this.mNeedSync = true;
                this.mSyncRowId = localSavedState.firstId;
                this.mSyncPosition = localSavedState.position;
                this.mSpecificTop = localSavedState.viewTop;
                this.mSyncMode = 1;
            }
        }
    }

    public Parcelable onSaveInstanceState()
    {
        dismissPopup();
        SavedState localSavedState = new SavedState(super.onSaveInstanceState());
        int i;
        if ((getChildCount() > 0) && (this.mItemCount > 0))
        {
            i = 1;
            long l = getSelectedItemId();
            localSavedState.selectedId = l;
            localSavedState.height = getHeight();
            if (l < 0L)
                break label234;
            localSavedState.viewTop = this.mSelectedTop;
            localSavedState.position = getSelectedItemPosition();
            localSavedState.firstId = -1L;
            label79: localSavedState.filter = null;
            if (this.mFiltered)
            {
                EditText localEditText = this.mTextFilter;
                if (localEditText != null)
                {
                    Editable localEditable = localEditText.getText();
                    if (localEditable != null)
                        localSavedState.filter = localEditable.toString();
                }
            }
            if ((this.mChoiceMode != 3) || (this.mChoiceActionMode == null))
                break label325;
        }
        LongSparseArray localLongSparseArray;
        label325: for (boolean bool = true; ; bool = false)
        {
            localSavedState.inActionMode = bool;
            if (this.mCheckStates != null)
                localSavedState.checkState = this.mCheckStates.clone();
            if (this.mCheckedIdStates == null)
                break label337;
            localLongSparseArray = new LongSparseArray();
            int j = this.mCheckedIdStates.size();
            for (int k = 0; k < j; k++)
                localLongSparseArray.put(this.mCheckedIdStates.keyAt(k), this.mCheckedIdStates.valueAt(k));
            i = 0;
            break;
            label234: if ((i != 0) && (this.mFirstPosition > 0))
            {
                localSavedState.viewTop = getChildAt(0).getTop();
                int m = this.mFirstPosition;
                if (m >= this.mItemCount)
                    m = -1 + this.mItemCount;
                localSavedState.position = m;
                localSavedState.firstId = this.mAdapter.getItemId(m);
                break label79;
            }
            localSavedState.viewTop = 0;
            localSavedState.firstId = -1L;
            localSavedState.position = 0;
            break label79;
        }
        localSavedState.checkIdState = localLongSparseArray;
        label337: localSavedState.checkedItemCount = this.mCheckedItemCount;
        if (this.mRemoteAdapter != null)
            this.mRemoteAdapter.saveRemoteViewsCache();
        return localSavedState;
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (getChildCount() > 0)
        {
            this.mDataChanged = true;
            rememberSyncState();
        }
        if (this.mFastScroller != null)
            this.mFastScroller.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        int i;
        boolean bool;
        if ((this.mPopup != null) && (isTextFilterEnabled()))
        {
            i = paramCharSequence.length();
            bool = this.mPopup.isShowing();
            if ((bool) || (i <= 0))
                break label87;
            showPopup();
        }
        for (this.mFiltered = true; ; this.mFiltered = false)
        {
            label87: 
            do
            {
                if ((this.mAdapter instanceof Filterable))
                {
                    Filter localFilter = ((Filterable)this.mAdapter).getFilter();
                    if (localFilter == null)
                        break;
                    localFilter.filter(paramCharSequence, this);
                }
                return;
            }
            while ((!bool) || (i != 0));
            dismissPopup();
        }
        throw new IllegalStateException("You cannot call onTextChanged with a non filterable adapter");
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if (Injector.isOutOfTouchRange(this, paramMotionEvent))
            bool = true;
        while (true)
        {
            return bool;
            if (!isEnabled())
            {
                if ((isClickable()) || (isLongClickable()))
                    bool = true;
                else
                    bool = false;
            }
            else
            {
                if (this.mPositionScroller != null)
                    this.mPositionScroller.stop();
                if (!this.mIsAttached)
                {
                    bool = false;
                }
                else
                {
                    if ((this.mFastScroller == null) || (!this.mFastScroller.onTouchEvent(paramMotionEvent)))
                        break;
                    bool = true;
                }
            }
        }
        int i = paramMotionEvent.getAction();
        initVelocityTrackerIfNotExists();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        switch (i & 0xFF)
        {
        case 4:
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        case 6:
        case 5:
        }
        while (true)
        {
            label164: bool = true;
            break;
            int i17;
            int i18;
            int i19;
            switch (this.mTouchMode)
            {
            default:
                this.mActivePointerId = paramMotionEvent.getPointerId(0);
                i17 = (int)paramMotionEvent.getX();
                i18 = (int)paramMotionEvent.getY();
                i19 = pointToPosition(i17, i18);
                if (!this.mDataChanged)
                {
                    if ((this.mTouchMode == 4) || (i19 < 0) || (!((ListAdapter)getAdapter()).isEnabled(i19)))
                        break label454;
                    this.mTouchMode = 0;
                    if (this.mPendingCheckForTap == null)
                    {
                        CheckForTap localCheckForTap = new CheckForTap();
                        this.mPendingCheckForTap = localCheckForTap;
                    }
                    postDelayed(this.mPendingCheckForTap, ViewConfiguration.getTapTimeout());
                }
                break;
            case 6:
            }
            while (true)
            {
                if (i19 >= 0)
                    this.mMotionViewOriginalTop = getChildAt(i19 - this.mFirstPosition).getTop();
                this.mMotionX = i17;
                this.mMotionY = i18;
                this.mMotionPosition = i19;
                this.mLastY = -2147483648;
                while ((performButtonActionOnTouchDown(paramMotionEvent)) && (this.mTouchMode == 0))
                {
                    removeCallbacks(this.mPendingCheckForTap);
                    break;
                    this.mFlingRunnable.endFling();
                    if (this.mPositionScroller != null)
                        this.mPositionScroller.stop();
                    this.mTouchMode = 5;
                    this.mMotionX = ((int)paramMotionEvent.getX());
                    int i16 = (int)paramMotionEvent.getY();
                    this.mLastY = i16;
                    this.mMotionY = i16;
                    this.mMotionCorrection = 0;
                    this.mActivePointerId = paramMotionEvent.getPointerId(0);
                    this.mDirection = 0;
                }
                label454: if (this.mTouchMode == 4)
                {
                    createScrollingCache();
                    this.mTouchMode = 3;
                    this.mMotionCorrection = 0;
                    i19 = findMotionRow(i18);
                    this.mFlingRunnable.flywheelTouch();
                }
            }
            int i14 = paramMotionEvent.findPointerIndex(this.mActivePointerId);
            if (i14 == -1)
            {
                i14 = 0;
                this.mActivePointerId = paramMotionEvent.getPointerId(0);
            }
            int i15 = (int)paramMotionEvent.getY(i14);
            if (this.mDataChanged)
                layoutChildren();
            switch (this.mTouchMode)
            {
            case 4:
            default:
                break;
            case 0:
            case 1:
            case 2:
                startScrollIfNeeded(i15);
                break;
            case 3:
            case 5:
                scrollIfNeeded(i15);
                continue;
                switch (this.mTouchMode)
                {
                case 4:
                default:
                case 0:
                case 1:
                case 2:
                case 3:
                case 5:
                }
                while (true)
                {
                    setPressed(false);
                    if (this.mEdgeGlowTop != null)
                    {
                        this.mEdgeGlowTop.onRelease();
                        this.mEdgeGlowBottom.onRelease();
                    }
                    invalidate();
                    Handler localHandler2 = getHandler();
                    if (localHandler2 != null)
                        localHandler2.removeCallbacks(this.mPendingCheckForLongPress);
                    recycleVelocityTracker();
                    this.mActivePointerId = -1;
                    if (this.mScrollStrictSpan == null)
                        break label164;
                    this.mScrollStrictSpan.finish();
                    this.mScrollStrictSpan = null;
                    break label164;
                    int i12 = this.mMotionPosition;
                    final View localView2 = getChildAt(i12 - this.mFirstPosition);
                    float f = paramMotionEvent.getX();
                    int i13;
                    label791: final PerformClick localPerformClick2;
                    Object localObject;
                    if ((f > this.mListPadding.left) && (f < getWidth() - this.mListPadding.right))
                    {
                        i13 = 1;
                        if ((localView2 == null) || (localView2.hasFocusable()) || (i13 == 0))
                            break label1126;
                        if (this.mTouchMode != 0)
                            localView2.setPressed(false);
                        if (this.mPerformClick == null)
                        {
                            PerformClick localPerformClick1 = new PerformClick(null);
                            this.mPerformClick = localPerformClick1;
                        }
                        localPerformClick2 = this.mPerformClick;
                        localPerformClick2.mClickMotionPosition = i12;
                        localPerformClick2.rememberWindowAttachCount();
                        this.mResurrectToPosition = i12;
                        if ((this.mTouchMode != 0) && (this.mTouchMode != 1))
                            break label1100;
                        Handler localHandler3 = getHandler();
                        if (localHandler3 != null)
                        {
                            if (this.mTouchMode != 0)
                                break label1078;
                            localObject = this.mPendingCheckForTap;
                            label909: localHandler3.removeCallbacks((Runnable)localObject);
                        }
                        this.mLayoutMode = 0;
                        if ((this.mDataChanged) || (!this.mAdapter.isEnabled(i12)))
                            break label1087;
                        this.mTouchMode = 1;
                        setSelectedPositionInt(this.mMotionPosition);
                        layoutChildren();
                        localView2.setPressed(true);
                        positionSelector(this.mMotionPosition, localView2);
                        setPressed(true);
                        if (this.mSelector != null)
                        {
                            Drawable localDrawable = this.mSelector.getCurrent();
                            if ((localDrawable != null) && ((localDrawable instanceof TransitionDrawable)))
                                ((TransitionDrawable)localDrawable).resetTransition();
                        }
                        if (this.mTouchModeReset != null)
                            removeCallbacks(this.mTouchModeReset);
                        Runnable local1 = new Runnable()
                        {
                            public void run()
                            {
                                AbsListView.this.mTouchMode = -1;
                                localView2.setPressed(false);
                                AbsListView.this.setPressed(false);
                                if (!AbsListView.this.mDataChanged)
                                    localPerformClick2.run();
                            }
                        };
                        this.mTouchModeReset = local1;
                        postDelayed(this.mTouchModeReset, ViewConfiguration.getPressedStateDuration());
                    }
                    while (true)
                    {
                        bool = true;
                        break;
                        i13 = 0;
                        break label791;
                        label1078: localObject = this.mPendingCheckForLongPress;
                        break label909;
                        label1087: this.mTouchMode = -1;
                        updateSelectorState();
                    }
                    label1100: if ((!this.mDataChanged) && (this.mAdapter.isEnabled(i12)))
                        localPerformClick2.run();
                    label1126: this.mTouchMode = -1;
                    updateSelectorState();
                    continue;
                    int i6 = getChildCount();
                    if (i6 > 0)
                    {
                        int i7 = getChildAt(0).getTop();
                        int i8 = getChildAt(i6 - 1).getBottom();
                        int i9 = this.mListPadding.top;
                        int i10 = getHeight() - this.mListPadding.bottom;
                        if ((this.mFirstPosition == 0) && (i7 >= i9) && (i6 + this.mFirstPosition < this.mItemCount) && (i8 <= getHeight() - i10))
                        {
                            this.mTouchMode = -1;
                            reportScrollStateChange(0);
                        }
                        else
                        {
                            VelocityTracker localVelocityTracker2 = this.mVelocityTracker;
                            localVelocityTracker2.computeCurrentVelocity(1000, this.mMaximumVelocity);
                            int i11 = (int)(localVelocityTracker2.getYVelocity(this.mActivePointerId) * this.mVelocityScale);
                            if ((Math.abs(i11) > this.mMinimumVelocity) && ((this.mFirstPosition != 0) || (i7 != i9 - this.mOverscrollDistance)) && ((i6 + this.mFirstPosition != this.mItemCount) || (i8 != i10 + this.mOverscrollDistance)))
                            {
                                if (this.mFlingRunnable == null)
                                {
                                    FlingRunnable localFlingRunnable3 = new FlingRunnable();
                                    this.mFlingRunnable = localFlingRunnable3;
                                }
                                reportScrollStateChange(2);
                                this.mFlingRunnable.start(-i11);
                            }
                            else
                            {
                                this.mTouchMode = -1;
                                reportScrollStateChange(0);
                                if (this.mFlingRunnable != null)
                                    this.mFlingRunnable.endFling();
                                if (this.mPositionScroller != null)
                                    this.mPositionScroller.stop();
                            }
                        }
                    }
                    else
                    {
                        this.mTouchMode = -1;
                        reportScrollStateChange(0);
                        continue;
                        if (this.mFlingRunnable == null)
                        {
                            FlingRunnable localFlingRunnable2 = new FlingRunnable();
                            this.mFlingRunnable = localFlingRunnable2;
                        }
                        VelocityTracker localVelocityTracker1 = this.mVelocityTracker;
                        localVelocityTracker1.computeCurrentVelocity(1000, this.mMaximumVelocity);
                        int i5 = (int)localVelocityTracker1.getYVelocity(this.mActivePointerId);
                        reportScrollStateChange(2);
                        if (Math.abs(i5) > this.mMinimumVelocity)
                            this.mFlingRunnable.startOverfling(-i5);
                        else
                            this.mFlingRunnable.startSpringback();
                    }
                }
                switch (this.mTouchMode)
                {
                default:
                    this.mTouchMode = -1;
                    setPressed(false);
                    View localView1 = getChildAt(this.mMotionPosition - this.mFirstPosition);
                    if (localView1 != null)
                        localView1.setPressed(false);
                    clearScrollingCache();
                    Handler localHandler1 = getHandler();
                    if (localHandler1 != null)
                        localHandler1.removeCallbacks(this.mPendingCheckForLongPress);
                    recycleVelocityTracker();
                case 6:
                case 5:
                }
                while (true)
                {
                    if (this.mEdgeGlowTop != null)
                    {
                        this.mEdgeGlowTop.onRelease();
                        this.mEdgeGlowBottom.onRelease();
                    }
                    this.mActivePointerId = -1;
                    break;
                    if (this.mFlingRunnable == null)
                    {
                        FlingRunnable localFlingRunnable1 = new FlingRunnable();
                        this.mFlingRunnable = localFlingRunnable1;
                    }
                    this.mFlingRunnable.startSpringback();
                }
                onSecondaryPointerUp(paramMotionEvent);
                int i2 = this.mMotionX;
                int i3 = this.mMotionY;
                int i4 = pointToPosition(i2, i3);
                if (i4 >= 0)
                {
                    this.mMotionViewOriginalTop = getChildAt(i4 - this.mFirstPosition).getTop();
                    this.mMotionPosition = i4;
                }
                this.mLastY = i3;
                continue;
                int j = paramMotionEvent.getActionIndex();
                int k = paramMotionEvent.getPointerId(j);
                int m = (int)paramMotionEvent.getX(j);
                int n = (int)paramMotionEvent.getY(j);
                this.mMotionCorrection = 0;
                this.mActivePointerId = k;
                this.mMotionX = m;
                this.mMotionY = n;
                int i1 = pointToPosition(m, n);
                if (i1 >= 0)
                {
                    this.mMotionViewOriginalTop = getChildAt(i1 - this.mFirstPosition).getTop();
                    this.mMotionPosition = i1;
                }
                this.mLastY = n;
            }
        }
    }

    public void onTouchModeChanged(boolean paramBoolean)
    {
        if (paramBoolean)
        {
            hideSelector();
            if ((getHeight() > 0) && (getChildCount() > 0))
                layoutChildren();
            updateSelectorState();
        }
        while (true)
        {
            return;
            int i = this.mTouchMode;
            if ((i == 5) || (i == 6))
            {
                if (this.mFlingRunnable != null)
                    this.mFlingRunnable.endFling();
                if (this.mPositionScroller != null)
                    this.mPositionScroller.stop();
                if (this.mScrollY != 0)
                {
                    this.mScrollY = 0;
                    invalidateParentCaches();
                    finishGlows();
                    invalidate();
                }
            }
        }
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        super.onWindowFocusChanged(paramBoolean);
        int i;
        if (isInTouchMode())
        {
            i = 0;
            if (paramBoolean)
                break label112;
            setChildrenDrawingCacheEnabled(false);
            if (this.mFlingRunnable != null)
            {
                removeCallbacks(this.mFlingRunnable);
                this.mFlingRunnable.endFling();
                if (this.mPositionScroller != null)
                    this.mPositionScroller.stop();
                if (this.mScrollY != 0)
                {
                    this.mScrollY = 0;
                    invalidateParentCaches();
                    finishGlows();
                    invalidate();
                }
            }
            dismissPopup();
            if (i == 1)
                this.mResurrectToPosition = this.mSelectedPosition;
        }
        while (true)
        {
            this.mLastTouchMode = i;
            return;
            i = 1;
            break;
            label112: if ((this.mFiltered) && (!this.mPopupHidden))
                showPopup();
            if ((i != this.mLastTouchMode) && (this.mLastTouchMode != -1))
                if (i == 1)
                {
                    resurrectSelection();
                }
                else
                {
                    hideSelector();
                    this.mLayoutMode = 0;
                    layoutChildren();
                }
        }
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool = true;
        if (super.performAccessibilityAction(paramInt, paramBundle));
        while (true)
        {
            return bool;
            switch (paramInt)
            {
            default:
                bool = false;
                break;
            case 4096:
                if ((isEnabled()) && (getLastVisiblePosition() < -1 + getCount()))
                    smoothScrollBy(getHeight() - this.mListPadding.top - this.mListPadding.bottom, 200);
                else
                    bool = false;
                break;
            case 8192:
                if ((isEnabled()) && (this.mFirstPosition > 0))
                    smoothScrollBy(-(getHeight() - this.mListPadding.top - this.mListPadding.bottom), 200);
                else
                    bool = false;
                break;
            }
        }
    }

    public boolean performItemClick(View paramView, int paramInt, long paramLong)
    {
        boolean bool1 = false;
        int i = 1;
        int j;
        if (this.mChoiceMode != 0)
        {
            bool1 = true;
            j = 0;
            if ((this.mChoiceMode != 2) && ((this.mChoiceMode != 3) || (this.mChoiceActionMode == null)))
                break label224;
            if (this.mCheckStates.get(paramInt, false))
                break label184;
            bool2 = true;
            this.mCheckStates.put(paramInt, bool2);
            if ((this.mCheckedIdStates != null) && (this.mAdapter.hasStableIds()))
            {
                if (!bool2)
                    break label190;
                this.mCheckedIdStates.put(this.mAdapter.getItemId(paramInt), Integer.valueOf(paramInt));
            }
            if (!bool2)
                break label210;
            this.mCheckedItemCount = (1 + this.mCheckedItemCount);
            if (this.mChoiceActionMode != null)
            {
                this.mMultiChoiceModeCallback.onItemCheckedStateChanged(this.mChoiceActionMode, paramInt, paramLong, bool2);
                i = 0;
            }
            j = 1;
        }
        label184: label190: label210: label224: 
        while (this.mChoiceMode != 1)
            while (true)
            {
                if (j != 0)
                    updateOnScreenCheckedViews();
                if (i != 0)
                    bool1 |= super.performItemClick(paramView, paramInt, paramLong);
                return bool1;
                boolean bool2 = false;
                continue;
                this.mCheckedIdStates.delete(this.mAdapter.getItemId(paramInt));
                continue;
                this.mCheckedItemCount = (-1 + this.mCheckedItemCount);
            }
        int k;
        if (!this.mCheckStates.get(paramInt, false))
        {
            k = 1;
            label247: if (k == 0)
                break label332;
            this.mCheckStates.clear();
            this.mCheckStates.put(paramInt, true);
            if ((this.mCheckedIdStates != null) && (this.mAdapter.hasStableIds()))
            {
                this.mCheckedIdStates.clear();
                this.mCheckedIdStates.put(this.mAdapter.getItemId(paramInt), Integer.valueOf(paramInt));
            }
        }
        for (this.mCheckedItemCount = 1; ; this.mCheckedItemCount = 0)
            label332: 
            do
            {
                j = 1;
                break;
                k = 0;
                break label247;
            }
            while ((this.mCheckStates.size() != 0) && (this.mCheckStates.valueAt(0)));
    }

    boolean performLongPress(View paramView, int paramInt, long paramLong)
    {
        boolean bool = true;
        if (this.mChoiceMode == 3)
            if (this.mChoiceActionMode == null)
            {
                ActionMode localActionMode = startActionMode(this.mMultiChoiceModeCallback);
                this.mChoiceActionMode = localActionMode;
                if (localActionMode != null)
                {
                    setItemChecked(paramInt, bool);
                    performHapticFeedback(0);
                }
            }
        while (true)
        {
            return bool;
            bool = false;
            if (this.mOnItemLongClickListener != null)
                bool = this.mOnItemLongClickListener.onItemLongClick(this, paramView, paramInt, paramLong);
            if (!bool)
            {
                this.mContextMenuInfo = createContextMenuInfo(paramView, paramInt, paramLong);
                bool = super.showContextMenuForChild(this);
            }
            if (bool)
                performHapticFeedback(0);
        }
    }

    public int pointToPosition(int paramInt1, int paramInt2)
    {
        Rect localRect = this.mTouchFrame;
        if (localRect == null)
        {
            this.mTouchFrame = new Rect();
            localRect = this.mTouchFrame;
        }
        int i = -1 + getChildCount();
        if (i >= 0)
        {
            View localView = getChildAt(i);
            if (localView.getVisibility() == 0)
            {
                localView.getHitRect(localRect);
                if (!localRect.contains(paramInt1, paramInt2));
            }
        }
        for (int j = i + this.mFirstPosition; ; j = -1)
        {
            return j;
            i--;
            break;
        }
    }

    public long pointToRowId(int paramInt1, int paramInt2)
    {
        int i = pointToPosition(paramInt1, paramInt2);
        if (i >= 0);
        for (long l = this.mAdapter.getItemId(i); ; l = -9223372036854775808L)
            return l;
    }

    void positionSelector(int paramInt, View paramView)
    {
        if (paramInt != -1)
            this.mSelectorPosition = paramInt;
        Rect localRect = this.mSelectorRect;
        localRect.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
        if ((paramView instanceof SelectionBoundsAdjuster))
            ((SelectionBoundsAdjuster)paramView).adjustListItemSelectionBounds(localRect);
        positionSelector(localRect.left, localRect.top, localRect.right, localRect.bottom);
        boolean bool1 = this.mIsChildViewEnabled;
        if (paramView.isEnabled() != bool1)
            if (bool1)
                break label116;
        label116: for (boolean bool2 = true; ; bool2 = false)
        {
            this.mIsChildViewEnabled = bool2;
            if (getSelectedItemPosition() != -1)
                refreshDrawableState();
            return;
        }
    }

    public void reclaimViews(List<View> paramList)
    {
        int i = getChildCount();
        RecyclerListener localRecyclerListener = this.mRecycler.mRecyclerListener;
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
            if ((localLayoutParams != null) && (this.mRecycler.shouldRecycleViewType(localLayoutParams.viewType)))
            {
                paramList.add(localView);
                localView.setAccessibilityDelegate(null);
                if (localRecyclerListener != null)
                    localRecyclerListener.onMovedToScrapHeap(localView);
            }
        }
        this.mRecycler.reclaimScrapViews(paramList);
        removeAllViewsInLayout();
    }

    int reconcileSelectedPosition()
    {
        int i = this.mSelectedPosition;
        if (i < 0)
            i = this.mResurrectToPosition;
        return Math.min(Math.max(0, i), -1 + this.mItemCount);
    }

    void reportScrollStateChange(int paramInt)
    {
        if ((paramInt != this.mLastScrollState) && (this.mOnScrollListener != null))
        {
            this.mLastScrollState = paramInt;
            this.mOnScrollListener.onScrollStateChanged(this, paramInt);
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
    {
        if (paramBoolean)
            recycleVelocityTracker();
        super.requestDisallowInterceptTouchEvent(paramBoolean);
    }

    public void requestLayout()
    {
        if ((!this.mBlockLayoutRequests) && (!this.mInLayout))
            super.requestLayout();
    }

    void requestLayoutIfNecessary()
    {
        if (getChildCount() > 0)
        {
            resetList();
            requestLayout();
            invalidate();
        }
    }

    void resetList()
    {
        removeAllViewsInLayout();
        this.mFirstPosition = 0;
        this.mDataChanged = false;
        this.mPositionScrollAfterLayout = null;
        this.mNeedSync = false;
        this.mOldSelectedPosition = -1;
        this.mOldSelectedRowId = -9223372036854775808L;
        setSelectedPositionInt(-1);
        setNextSelectedPositionInt(-1);
        this.mSelectedTop = 0;
        this.mSelectorPosition = -1;
        this.mSelectorRect.setEmpty();
        invalidate();
    }

    boolean resurrectSelection()
    {
        int i = getChildCount();
        boolean bool2;
        if (i <= 0)
            bool2 = false;
        while (true)
        {
            return bool2;
            int j = 0;
            int k = this.mListPadding.top;
            int m = this.mBottom - this.mTop - this.mListPadding.bottom;
            int n = this.mFirstPosition;
            int i1 = this.mResurrectToPosition;
            boolean bool1 = true;
            int i3;
            View localView2;
            int i10;
            label117: int i7;
            if ((i1 >= n) && (i1 < n + i))
            {
                i3 = i1;
                localView2 = getChildAt(i3 - this.mFirstPosition);
                j = localView2.getTop();
                i10 = localView2.getBottom();
                if (j < k)
                {
                    j = k + getVerticalFadingEdgeLength();
                    this.mResurrectToPosition = -1;
                    removeCallbacks(this.mFlingRunnable);
                    if (this.mPositionScroller != null)
                        this.mPositionScroller.stop();
                    this.mTouchMode = -1;
                    clearScrollingCache();
                    this.mSpecificTop = j;
                    i7 = lookForSelectablePosition(i3, bool1);
                    if ((i7 < n) || (i7 > getLastVisiblePosition()))
                        break label440;
                    this.mLayoutMode = 4;
                    updateSelectorState();
                    setSelectionInt(i7);
                    invokeOnItemScrollListener();
                }
            }
            while (true)
            {
                reportScrollStateChange(0);
                if (i7 < 0)
                    break label447;
                bool2 = true;
                break;
                if (i10 <= m)
                    break label117;
                j = m - localView2.getMeasuredHeight() - getVerticalFadingEdgeLength();
                break label117;
                if (i1 < n)
                {
                    i3 = n;
                    for (int i8 = 0; ; i8++)
                    {
                        if (i8 >= i)
                            break label326;
                        int i9 = getChildAt(i8).getTop();
                        if (i8 == 0)
                        {
                            j = i9;
                            if ((n > 0) || (i9 < k))
                                k += getVerticalFadingEdgeLength();
                        }
                        if (i9 >= k)
                        {
                            i3 = n + i8;
                            j = i9;
                            break;
                        }
                    }
                    label326: break label117;
                }
                int i2 = this.mItemCount;
                bool1 = false;
                i3 = -1 + (n + i);
                for (int i4 = i - 1; ; i4--)
                {
                    if (i4 < 0)
                        break label438;
                    View localView1 = getChildAt(i4);
                    int i5 = localView1.getTop();
                    int i6 = localView1.getBottom();
                    if (i4 == i - 1)
                    {
                        j = i5;
                        if ((n + i < i2) || (i6 > m))
                            m -= getVerticalFadingEdgeLength();
                    }
                    if (i6 <= m)
                    {
                        i3 = n + i4;
                        j = i5;
                        break;
                    }
                }
                label438: break label117;
                label440: i7 = -1;
            }
            label447: bool2 = false;
        }
    }

    boolean resurrectSelectionIfNeeded()
    {
        if ((this.mSelectedPosition < 0) && (resurrectSelection()))
            updateSelectorState();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void sendAccessibilityEvent(int paramInt)
    {
        int i;
        int j;
        if (paramInt == 4096)
        {
            i = getFirstVisiblePosition();
            j = getLastVisiblePosition();
            if ((this.mLastAccessibilityScrollEventFromIndex != i) || (this.mLastAccessibilityScrollEventToIndex != j));
        }
        while (true)
        {
            return;
            this.mLastAccessibilityScrollEventFromIndex = i;
            this.mLastAccessibilityScrollEventToIndex = j;
            super.sendAccessibilityEvent(paramInt);
        }
    }

    boolean sendToTextFilter(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        boolean bool1;
        if (!acceptFilter())
            bool1 = false;
        while (true)
        {
            return bool1;
            bool1 = false;
            boolean bool2 = true;
            switch (paramInt1)
            {
            default:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 66:
            case 4:
            case 62:
            }
            KeyEvent localKeyEvent;
            while (bool2)
            {
                createTextFilter(true);
                localKeyEvent = paramKeyEvent;
                if (localKeyEvent.getRepeatCount() > 0)
                    localKeyEvent = KeyEvent.changeTimeRepeat(paramKeyEvent, paramKeyEvent.getEventTime(), 0);
                switch (paramKeyEvent.getAction())
                {
                default:
                    break;
                case 0:
                    bool1 = this.mTextFilter.onKeyDown(paramInt1, localKeyEvent);
                    break;
                    bool2 = false;
                    continue;
                    if ((this.mFiltered) && (this.mPopup != null) && (this.mPopup.isShowing()))
                    {
                        if ((paramKeyEvent.getAction() != 0) || (paramKeyEvent.getRepeatCount() != 0))
                            break label249;
                        KeyEvent.DispatcherState localDispatcherState = getKeyDispatcherState();
                        if (localDispatcherState != null)
                            localDispatcherState.startTracking(paramKeyEvent, this);
                        bool1 = true;
                    }
                    while (true)
                    {
                        bool2 = false;
                        break;
                        label249: if ((paramKeyEvent.getAction() == 1) && (paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                        {
                            bool1 = true;
                            this.mTextFilter.setText("");
                        }
                    }
                    bool2 = this.mFiltered;
                case 1:
                case 2:
                }
            }
            bool1 = this.mTextFilter.onKeyUp(paramInt1, localKeyEvent);
            continue;
            bool1 = this.mTextFilter.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
        }
    }

    public void setAdapter(ListAdapter paramListAdapter)
    {
        if (paramListAdapter != null)
        {
            this.mAdapterHasStableIds = this.mAdapter.hasStableIds();
            if ((this.mChoiceMode != 0) && (this.mAdapterHasStableIds) && (this.mCheckedIdStates == null))
                this.mCheckedIdStates = new LongSparseArray();
        }
        if (this.mCheckStates != null)
            this.mCheckStates.clear();
        if (this.mCheckedIdStates != null)
            this.mCheckedIdStates.clear();
    }

    public void setCacheColorHint(int paramInt)
    {
        if (paramInt != this.mCacheColorHint)
        {
            this.mCacheColorHint = paramInt;
            int i = getChildCount();
            for (int j = 0; j < i; j++)
                getChildAt(j).setDrawingCacheBackgroundColor(paramInt);
            this.mRecycler.setCacheColorHint(paramInt);
        }
    }

    public void setChoiceMode(int paramInt)
    {
        this.mChoiceMode = paramInt;
        if (this.mChoiceActionMode != null)
        {
            this.mChoiceActionMode.finish();
            this.mChoiceActionMode = null;
        }
        if (this.mChoiceMode != 0)
        {
            if (this.mCheckStates == null)
                this.mCheckStates = new SparseBooleanArray();
            if ((this.mCheckedIdStates == null) && (this.mAdapter != null) && (this.mAdapter.hasStableIds()))
                this.mCheckedIdStates = new LongSparseArray();
            if (this.mChoiceMode == 3)
            {
                clearChoices();
                setLongClickable(true);
            }
        }
    }

    public void setDrawSelectorOnTop(boolean paramBoolean)
    {
        this.mDrawSelectorOnTop = paramBoolean;
    }

    public void setFastScrollAlwaysVisible(boolean paramBoolean)
    {
        if ((paramBoolean) && (!this.mFastScrollEnabled))
            setFastScrollEnabled(true);
        if (this.mFastScroller != null)
            this.mFastScroller.setAlwaysShow(paramBoolean);
        computeOpaqueFlags();
        recomputePadding();
    }

    public void setFastScrollEnabled(boolean paramBoolean)
    {
        this.mFastScrollEnabled = paramBoolean;
        if (paramBoolean)
            if (this.mFastScroller == null)
                this.mFastScroller = new FastScroller(getContext(), this);
        while (true)
        {
            return;
            if (this.mFastScroller != null)
            {
                this.mFastScroller.stop();
                this.mFastScroller = null;
            }
        }
    }

    public void setFilterText(String paramString)
    {
        if ((this.mTextFilterEnabled) && (!TextUtils.isEmpty(paramString)))
        {
            createTextFilter(false);
            this.mTextFilter.setText(paramString);
            this.mTextFilter.setSelection(paramString.length());
            if ((this.mAdapter instanceof Filterable))
            {
                if (this.mPopup == null)
                    ((Filterable)this.mAdapter).getFilter().filter(paramString);
                this.mFiltered = true;
                this.mDataSetObserver.clearSavedState();
            }
        }
    }

    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        if (bool)
            if (getWindowVisibility() != 0)
                break label62;
        label62: for (int i = 1; ; i = 0)
        {
            if ((this.mFiltered) && (i != 0) && (this.mPopup != null) && (this.mPopup.isShowing()))
                positionPopup();
            return bool;
        }
    }

    public void setFriction(float paramFloat)
    {
        if (this.mFlingRunnable == null)
            this.mFlingRunnable = new FlingRunnable();
        this.mFlingRunnable.mScroller.setFriction(paramFloat);
    }

    public void setItemChecked(int paramInt, boolean paramBoolean)
    {
        if (this.mChoiceMode == 0);
        label136: label220: label367: 
        while (true)
        {
            return;
            if ((paramBoolean) && (this.mChoiceMode == 3) && (this.mChoiceActionMode == null))
                this.mChoiceActionMode = startActionMode(this.mMultiChoiceModeCallback);
            if ((this.mChoiceMode == 2) || (this.mChoiceMode == 3))
            {
                boolean bool = this.mCheckStates.get(paramInt);
                this.mCheckStates.put(paramInt, paramBoolean);
                if ((this.mCheckedIdStates != null) && (this.mAdapter.hasStableIds()))
                {
                    if (paramBoolean)
                        this.mCheckedIdStates.put(this.mAdapter.getItemId(paramInt), Integer.valueOf(paramInt));
                }
                else
                {
                    label117: if (bool != paramBoolean)
                    {
                        if (!paramBoolean)
                            break label220;
                        this.mCheckedItemCount = (1 + this.mCheckedItemCount);
                    }
                    if (this.mChoiceActionMode != null)
                    {
                        long l = this.mAdapter.getItemId(paramInt);
                        this.mMultiChoiceModeCallback.onItemCheckedStateChanged(this.mChoiceActionMode, paramInt, l, paramBoolean);
                    }
                }
            }
            while (true)
            {
                if ((this.mInLayout) || (this.mBlockLayoutRequests))
                    break label367;
                this.mDataChanged = true;
                rememberSyncState();
                requestLayout();
                break;
                this.mCheckedIdStates.delete(this.mAdapter.getItemId(paramInt));
                break label117;
                this.mCheckedItemCount = (-1 + this.mCheckedItemCount);
                break label136;
                if ((this.mCheckedIdStates != null) && (this.mAdapter.hasStableIds()));
                for (int i = 1; ; i = 0)
                {
                    if ((paramBoolean) || (isItemChecked(paramInt)))
                    {
                        this.mCheckStates.clear();
                        if (i != 0)
                            this.mCheckedIdStates.clear();
                    }
                    if (!paramBoolean)
                        break label340;
                    this.mCheckStates.put(paramInt, true);
                    if (i != 0)
                        this.mCheckedIdStates.put(this.mAdapter.getItemId(paramInt), Integer.valueOf(paramInt));
                    this.mCheckedItemCount = 1;
                    break;
                }
                if ((this.mCheckStates.size() == 0) || (!this.mCheckStates.valueAt(0)))
                    this.mCheckedItemCount = 0;
            }
        }
    }

    public void setMultiChoiceModeListener(MultiChoiceModeListener paramMultiChoiceModeListener)
    {
        if (this.mMultiChoiceModeCallback == null)
            this.mMultiChoiceModeCallback = new MultiChoiceModeWrapper();
        this.mMultiChoiceModeCallback.setWrapped(paramMultiChoiceModeListener);
    }

    public void setOnScrollListener(OnScrollListener paramOnScrollListener)
    {
        this.mOnScrollListener = paramOnScrollListener;
        invokeOnItemScrollListener();
    }

    public void setOverScrollEffectPadding(int paramInt1, int paramInt2)
    {
        this.mGlowPaddingLeft = paramInt1;
        this.mGlowPaddingRight = paramInt2;
    }

    public void setOverScrollMode(int paramInt)
    {
        Context localContext;
        if (paramInt != 2)
            if (this.mEdgeGlowTop == null)
            {
                localContext = getContext();
                this.mEdgeGlowTop = new EdgeEffect(localContext);
            }
        for (this.mEdgeGlowBottom = new EdgeEffect(localContext); ; this.mEdgeGlowBottom = null)
        {
            super.setOverScrollMode(paramInt);
            return;
            this.mEdgeGlowTop = null;
        }
    }

    public void setRecyclerListener(RecyclerListener paramRecyclerListener)
    {
        RecycleBin.access$3402(this.mRecycler, paramRecyclerListener);
    }

    public void setRemoteViewsAdapter(Intent paramIntent)
    {
        if ((this.mRemoteAdapter != null) && (new Intent.FilterComparison(paramIntent).equals(new Intent.FilterComparison(this.mRemoteAdapter.getRemoteViewsServiceIntent()))));
        while (true)
        {
            return;
            this.mDeferNotifyDataSetChanged = false;
            this.mRemoteAdapter = new RemoteViewsAdapter(getContext(), paramIntent, this);
            if (this.mRemoteAdapter.isDataReady())
                setAdapter(this.mRemoteAdapter);
        }
    }

    public void setScrollIndicators(View paramView1, View paramView2)
    {
        this.mScrollUp = paramView1;
        this.mScrollDown = paramView2;
    }

    public void setScrollingCacheEnabled(boolean paramBoolean)
    {
        if ((this.mScrollingCacheEnabled) && (!paramBoolean))
            clearScrollingCache();
        this.mScrollingCacheEnabled = paramBoolean;
    }

    abstract void setSelectionInt(int paramInt);

    public void setSelector(int paramInt)
    {
        setSelector(getResources().getDrawable(paramInt));
    }

    public void setSelector(Drawable paramDrawable)
    {
        if (this.mSelector != null)
        {
            this.mSelector.setCallback(null);
            unscheduleDrawable(this.mSelector);
        }
        this.mSelector = paramDrawable;
        Rect localRect = new Rect();
        paramDrawable.getPadding(localRect);
        this.mSelectionLeftPadding = localRect.left;
        this.mSelectionTopPadding = localRect.top;
        this.mSelectionRightPadding = localRect.right;
        this.mSelectionBottomPadding = localRect.bottom;
        paramDrawable.setCallback(this);
        updateSelectorState();
    }

    public void setSmoothScrollbarEnabled(boolean paramBoolean)
    {
        this.mSmoothScrollbarEnabled = paramBoolean;
    }

    public void setStackFromBottom(boolean paramBoolean)
    {
        if (this.mStackFromBottom != paramBoolean)
        {
            this.mStackFromBottom = paramBoolean;
            requestLayoutIfNecessary();
        }
    }

    public void setTextFilterEnabled(boolean paramBoolean)
    {
        this.mTextFilterEnabled = paramBoolean;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setTouchPadding(int paramInt1, int paramInt2)
    {
        this.mTouchPaddingLeft = paramInt1;
        this.mTouchPaddingRight = paramInt2;
    }

    public void setTranscriptMode(int paramInt)
    {
        this.mTranscriptMode = paramInt;
    }

    public void setVelocityScale(float paramFloat)
    {
        this.mVelocityScale = paramFloat;
    }

    public void setVerticalScrollbarPosition(int paramInt)
    {
        super.setVerticalScrollbarPosition(paramInt);
        if (this.mFastScroller != null)
            this.mFastScroller.setScrollbarPosition(paramInt);
    }

    void setVisibleRangeHint(int paramInt1, int paramInt2)
    {
        if (this.mRemoteAdapter != null)
            this.mRemoteAdapter.setVisibleRangeHint(paramInt1, paramInt2);
    }

    boolean shouldShowSelector()
    {
        if (((hasFocus()) && (!isInTouchMode())) || (touchModeDrawsInPressedState()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean showContextMenu(float paramFloat1, float paramFloat2, int paramInt)
    {
        int i = pointToPosition((int)paramFloat1, (int)paramFloat2);
        if (i != -1)
        {
            long l = this.mAdapter.getItemId(i);
            View localView = getChildAt(i - this.mFirstPosition);
            if (localView != null)
                this.mContextMenuInfo = createContextMenuInfo(localView, i, l);
        }
        for (boolean bool = super.showContextMenuForChild(this); ; bool = super.showContextMenu(paramFloat1, paramFloat2, paramInt))
            return bool;
    }

    public boolean showContextMenuForChild(View paramView)
    {
        int i = getPositionForView(paramView);
        if (i >= 0)
        {
            long l = this.mAdapter.getItemId(i);
            bool = false;
            if (this.mOnItemLongClickListener != null)
                bool = this.mOnItemLongClickListener.onItemLongClick(this, paramView, i, l);
            if (!bool)
                this.mContextMenuInfo = createContextMenuInfo(getChildAt(i - this.mFirstPosition), i, l);
        }
        for (boolean bool = super.showContextMenuForChild(paramView); ; bool = false)
            return bool;
    }

    public void smoothScrollBy(int paramInt1, int paramInt2)
    {
        smoothScrollBy(paramInt1, paramInt2, false);
    }

    void smoothScrollBy(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (this.mFlingRunnable == null)
            this.mFlingRunnable = new FlingRunnable();
        int i = this.mFirstPosition;
        int j = getChildCount();
        int k = i + j;
        int m = getPaddingTop();
        int n = getHeight() - getPaddingBottom();
        if ((paramInt1 == 0) || (this.mItemCount == 0) || (j == 0) || ((i == 0) && (getChildAt(0).getTop() == m) && (paramInt1 < 0)) || ((k == this.mItemCount) && (getChildAt(j - 1).getBottom() == n) && (paramInt1 > 0)))
        {
            this.mFlingRunnable.endFling();
            if (this.mPositionScroller != null)
                this.mPositionScroller.stop();
        }
        while (true)
        {
            return;
            reportScrollStateChange(2);
            this.mFlingRunnable.startScroll(paramInt1, paramInt2, paramBoolean);
        }
    }

    void smoothScrollByOffset(int paramInt)
    {
        int i = -1;
        float f;
        if (paramInt < 0)
        {
            i = getFirstVisiblePosition();
            if (i > -1)
            {
                View localView = getChildAt(i - getFirstVisiblePosition());
                if (localView != null)
                {
                    Rect localRect = new Rect();
                    if (localView.getGlobalVisibleRect(localRect))
                    {
                        int j = localView.getWidth() * localView.getHeight();
                        f = localRect.width() * localRect.height() / j;
                        if ((paramInt >= 0) || (f >= 0.75F))
                            break label127;
                        i++;
                    }
                }
            }
        }
        while (true)
        {
            smoothScrollToPosition(Math.max(0, Math.min(getCount(), i + paramInt)));
            return;
            if (paramInt <= 0)
                break;
            i = getLastVisiblePosition();
            break;
            label127: if ((paramInt > 0) && (f < 0.75F))
                i--;
        }
    }

    public void smoothScrollToPosition(int paramInt)
    {
        if (this.mPositionScroller == null)
            this.mPositionScroller = new PositionScroller();
        this.mPositionScroller.start(paramInt);
    }

    public void smoothScrollToPosition(int paramInt1, int paramInt2)
    {
        if (this.mPositionScroller == null)
            this.mPositionScroller = new PositionScroller();
        this.mPositionScroller.start(paramInt1, paramInt2);
    }

    public void smoothScrollToPositionFromTop(int paramInt1, int paramInt2)
    {
        if (this.mPositionScroller == null)
            this.mPositionScroller = new PositionScroller();
        this.mPositionScroller.startWithOffset(paramInt1, paramInt2);
    }

    public void smoothScrollToPositionFromTop(int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mPositionScroller == null)
            this.mPositionScroller = new PositionScroller();
        this.mPositionScroller.startWithOffset(paramInt1, paramInt2, paramInt3);
    }

    boolean touchModeDrawsInPressedState()
    {
        switch (this.mTouchMode)
        {
        default:
        case 1:
        case 2:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    boolean trackMotionScroll(int paramInt1, int paramInt2)
    {
        int i = getChildCount();
        boolean bool1;
        if (i == 0)
            bool1 = true;
        Rect localRect;
        int i1;
        int i2;
        int i4;
        label125: int i5;
        label140: int i6;
        while (true)
        {
            return bool1;
            int j = getChildAt(0).getTop();
            int k = getChildAt(i - 1).getBottom();
            localRect = this.mListPadding;
            int m = 0;
            int n = 0;
            if ((0x22 & this.mGroupFlags) == 34)
            {
                m = localRect.top;
                n = localRect.bottom;
            }
            i1 = m - j;
            i2 = k - (getHeight() - n);
            int i3 = getHeight() - this.mPaddingBottom - this.mPaddingTop;
            label163: label186: int i7;
            if (paramInt1 < 0)
            {
                i4 = Math.max(-(i3 - 1), paramInt1);
                if (paramInt2 >= 0)
                    break label277;
                i5 = Math.max(-(i3 - 1), paramInt2);
                i6 = this.mFirstPosition;
                if (i6 != 0)
                    break label290;
                this.mFirstPositionDistanceGuess = (j - localRect.top);
                if (i6 + i != this.mItemCount)
                    break label304;
                this.mLastPositionDistanceGuess = (k + localRect.bottom);
                if ((i6 != 0) || (j < localRect.top) || (i5 < 0))
                    break label318;
                i7 = 1;
                label209: if ((i6 + i != this.mItemCount) || (k > getHeight() - localRect.bottom) || (i5 > 0))
                    break label324;
            }
            label277: label290: label304: label318: label324: for (int i8 = 1; ; i8 = 0)
            {
                if ((i7 == 0) && (i8 == 0))
                    break label336;
                if (i5 == 0)
                    break label330;
                bool1 = true;
                break;
                i4 = Math.min(i3 - 1, paramInt1);
                break label125;
                i5 = Math.min(i3 - 1, paramInt2);
                break label140;
                this.mFirstPositionDistanceGuess = (i5 + this.mFirstPositionDistanceGuess);
                break label163;
                this.mLastPositionDistanceGuess = (i5 + this.mLastPositionDistanceGuess);
                break label186;
                i7 = 0;
                break label209;
            }
            label330: bool1 = false;
        }
        label336: boolean bool2;
        label344: int i9;
        int i10;
        int i11;
        int i12;
        int i20;
        label417: View localView2;
        if (i5 < 0)
        {
            bool2 = true;
            boolean bool3 = isInTouchMode();
            if (bool3)
                hideSelector();
            i9 = getHeaderViewsCount();
            i10 = this.mItemCount - getFooterViewsCount();
            i11 = 0;
            i12 = 0;
            if (!bool2)
                break label652;
            int i19 = -i5;
            if ((0x22 & this.mGroupFlags) == 34)
                i19 += localRect.top;
            i20 = 0;
            if (i20 < i)
            {
                localView2 = getChildAt(i20);
                if (localView2.getBottom() < i19)
                    break label611;
            }
            label441: this.mMotionViewNewTop = (i4 + this.mMotionViewOriginalTop);
            this.mBlockLayoutRequests = true;
            if (i12 > 0)
            {
                detachViewsFromParent(i11, i12);
                this.mRecycler.removeSkippedScrap();
            }
            if (!awakenScrollBars())
                invalidate();
            offsetChildrenTopAndBottom(i5);
            if (bool2)
                this.mFirstPosition = (i12 + this.mFirstPosition);
            int i16 = Math.abs(i5);
            if ((i1 < i16) || (i2 < i16))
                fillGap(bool2);
            if ((bool3) || (this.mSelectedPosition == -1))
                break label756;
            int i18 = this.mSelectedPosition - this.mFirstPosition;
            if ((i18 >= 0) && (i18 < getChildCount()))
                positionSelector(this.mSelectedPosition, getChildAt(i18));
        }
        while (true)
        {
            this.mBlockLayoutRequests = false;
            invokeOnItemScrollListener();
            bool1 = false;
            break;
            bool2 = false;
            break label344;
            label611: i12++;
            int i21 = i6 + i20;
            if ((i21 >= i9) && (i21 < i10))
                this.mRecycler.addScrapView(localView2, i21);
            i20++;
            break label417;
            label652: int i13 = getHeight() - i5;
            if ((0x22 & this.mGroupFlags) == 34)
                i13 -= localRect.bottom;
            for (int i14 = i - 1; ; i14--)
            {
                if (i14 < 0)
                    break label754;
                View localView1 = getChildAt(i14);
                if (localView1.getTop() <= i13)
                    break;
                i11 = i14;
                i12++;
                int i15 = i6 + i14;
                if ((i15 >= i9) && (i15 < i10))
                    this.mRecycler.addScrapView(localView1, i15);
            }
            label754: break label441;
            label756: if (this.mSelectorPosition != -1)
            {
                int i17 = this.mSelectorPosition - this.mFirstPosition;
                if ((i17 >= 0) && (i17 < getChildCount()))
                    positionSelector(-1, getChildAt(i17));
            }
            else
            {
                this.mSelectorRect.setEmpty();
            }
        }
    }

    void updateScrollIndicators()
    {
        int i = 0;
        int m;
        label52: int n;
        label66: int k;
        label100: View localView1;
        if (this.mScrollUp != null)
        {
            if (this.mFirstPosition > 0)
            {
                m = 1;
                if ((m == 0) && (getChildCount() > 0))
                {
                    if (getChildAt(0).getTop() >= this.mListPadding.top)
                        break label158;
                    m = 1;
                }
                View localView2 = this.mScrollUp;
                if (m == 0)
                    break label164;
                n = 0;
                localView2.setVisibility(n);
            }
        }
        else if (this.mScrollDown != null)
        {
            int j = getChildCount();
            if (j + this.mFirstPosition >= this.mItemCount)
                break label170;
            k = 1;
            if ((k == 0) && (j > 0))
            {
                if (getChildAt(j - 1).getBottom() <= this.mBottom - this.mListPadding.bottom)
                    break label175;
                k = 1;
            }
            label135: localView1 = this.mScrollDown;
            if (k == 0)
                break label180;
        }
        while (true)
        {
            localView1.setVisibility(i);
            return;
            m = 0;
            break;
            label158: m = 0;
            break label52;
            label164: n = 4;
            break label66;
            label170: k = 0;
            break label100;
            label175: k = 0;
            break label135;
            label180: i = 4;
        }
    }

    void updateSelectorState()
    {
        if (this.mSelector != null)
        {
            if (!shouldShowSelector())
                break label27;
            this.mSelector.setState(getDrawableState());
        }
        while (true)
        {
            return;
            label27: this.mSelector.setState(StateSet.NOTHING);
        }
    }

    public boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((this.mSelector == paramDrawable) || (super.verifyDrawable(paramDrawable)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    class RecycleBin
    {
        private View[] mActiveViews = new View[0];
        private ArrayList<View> mCurrentScrap;
        private int mFirstActivePosition;
        private AbsListView.RecyclerListener mRecyclerListener;
        private ArrayList<View>[] mScrapViews;
        private ArrayList<View> mSkippedScrap;
        private SparseArray<View> mTransientStateViews;
        private int mViewTypeCount;

        RecycleBin()
        {
        }

        private void pruneScrapViews()
        {
            int i = this.mActiveViews.length;
            int j = this.mViewTypeCount;
            ArrayList[] arrayOfArrayList = this.mScrapViews;
            for (int k = 0; k < j; k++)
            {
                ArrayList localArrayList = arrayOfArrayList[k];
                int n = localArrayList.size();
                int i1 = n - i;
                int i2 = n - 1;
                int i3 = 0;
                int i5;
                for (int i4 = i2; i3 < i1; i4 = i5)
                {
                    AbsListView localAbsListView = AbsListView.this;
                    i5 = i4 - 1;
                    localAbsListView.removeDetachedView((View)localArrayList.remove(i4), false);
                    i3++;
                }
            }
            if (this.mTransientStateViews != null)
                for (int m = 0; m < this.mTransientStateViews.size(); m++)
                    if (!((View)this.mTransientStateViews.valueAt(m)).hasTransientState())
                    {
                        this.mTransientStateViews.removeAt(m);
                        m--;
                    }
        }

        void addScrapView(View paramView, int paramInt)
        {
            AbsListView.LayoutParams localLayoutParams = (AbsListView.LayoutParams)paramView.getLayoutParams();
            if (localLayoutParams == null);
            int i;
            while (true)
            {
                return;
                localLayoutParams.scrappedFromPosition = paramInt;
                i = localLayoutParams.viewType;
                boolean bool = paramView.hasTransientState();
                if ((shouldRecycleViewType(i)) && (!bool))
                    break;
                if ((i != -2) || (bool))
                {
                    if (this.mSkippedScrap == null)
                        this.mSkippedScrap = new ArrayList();
                    this.mSkippedScrap.add(paramView);
                }
                if (bool)
                {
                    if (this.mTransientStateViews == null)
                        this.mTransientStateViews = new SparseArray();
                    paramView.dispatchStartTemporaryDetach();
                    this.mTransientStateViews.put(paramInt, paramView);
                }
            }
            paramView.dispatchStartTemporaryDetach();
            if (this.mViewTypeCount == 1)
                this.mCurrentScrap.add(paramView);
            while (true)
            {
                paramView.setAccessibilityDelegate(null);
                if (this.mRecyclerListener == null)
                    break;
                this.mRecyclerListener.onMovedToScrapHeap(paramView);
                break;
                this.mScrapViews[i].add(paramView);
            }
        }

        void clear()
        {
            if (this.mViewTypeCount == 1)
            {
                ArrayList localArrayList2 = this.mCurrentScrap;
                int n = localArrayList2.size();
                for (int i1 = 0; i1 < n; i1++)
                    AbsListView.this.removeDetachedView((View)localArrayList2.remove(n - 1 - i1), false);
            }
            int i = this.mViewTypeCount;
            for (int j = 0; j < i; j++)
            {
                ArrayList localArrayList1 = this.mScrapViews[j];
                int k = localArrayList1.size();
                for (int m = 0; m < k; m++)
                    AbsListView.this.removeDetachedView((View)localArrayList1.remove(k - 1 - m), false);
            }
            if (this.mTransientStateViews != null)
                this.mTransientStateViews.clear();
        }

        void clearTransientStateViews()
        {
            if (this.mTransientStateViews != null)
                this.mTransientStateViews.clear();
        }

        void fillActiveViews(int paramInt1, int paramInt2)
        {
            if (this.mActiveViews.length < paramInt1)
                this.mActiveViews = new View[paramInt1];
            this.mFirstActivePosition = paramInt2;
            View[] arrayOfView = this.mActiveViews;
            for (int i = 0; i < paramInt1; i++)
            {
                View localView = AbsListView.this.getChildAt(i);
                AbsListView.LayoutParams localLayoutParams = (AbsListView.LayoutParams)localView.getLayoutParams();
                if ((localLayoutParams != null) && (localLayoutParams.viewType != -2))
                    arrayOfView[i] = localView;
            }
        }

        View getActiveView(int paramInt)
        {
            int i = paramInt - this.mFirstActivePosition;
            View[] arrayOfView = this.mActiveViews;
            View localView;
            if ((i >= 0) && (i < arrayOfView.length))
            {
                localView = arrayOfView[i];
                arrayOfView[i] = null;
            }
            while (true)
            {
                return localView;
                localView = null;
            }
        }

        View getScrapView(int paramInt)
        {
            View localView;
            if (this.mViewTypeCount == 1)
                localView = AbsListView.retrieveFromScrap(this.mCurrentScrap, paramInt);
            while (true)
            {
                return localView;
                int i = AbsListView.this.mAdapter.getItemViewType(paramInt);
                if ((i >= 0) && (i < this.mScrapViews.length))
                    localView = AbsListView.retrieveFromScrap(this.mScrapViews[i], paramInt);
                else
                    localView = null;
            }
        }

        View getTransientStateView(int paramInt)
        {
            View localView = null;
            if (this.mTransientStateViews == null);
            while (true)
            {
                return localView;
                int i = this.mTransientStateViews.indexOfKey(paramInt);
                if (i >= 0)
                {
                    localView = (View)this.mTransientStateViews.valueAt(i);
                    this.mTransientStateViews.removeAt(i);
                }
            }
        }

        public void markChildrenDirty()
        {
            if (this.mViewTypeCount == 1)
            {
                ArrayList localArrayList2 = this.mCurrentScrap;
                int i2 = localArrayList2.size();
                for (int i3 = 0; i3 < i2; i3++)
                    ((View)localArrayList2.get(i3)).forceLayout();
            }
            int i = this.mViewTypeCount;
            for (int j = 0; j < i; j++)
            {
                ArrayList localArrayList1 = this.mScrapViews[j];
                int n = localArrayList1.size();
                for (int i1 = 0; i1 < n; i1++)
                    ((View)localArrayList1.get(i1)).forceLayout();
            }
            if (this.mTransientStateViews != null)
            {
                int k = this.mTransientStateViews.size();
                for (int m = 0; m < k; m++)
                    ((View)this.mTransientStateViews.valueAt(m)).forceLayout();
            }
        }

        void reclaimScrapViews(List<View> paramList)
        {
            if (this.mViewTypeCount == 1)
                paramList.addAll(this.mCurrentScrap);
            while (true)
            {
                return;
                int i = this.mViewTypeCount;
                ArrayList[] arrayOfArrayList = this.mScrapViews;
                for (int j = 0; j < i; j++)
                    paramList.addAll(arrayOfArrayList[j]);
            }
        }

        void removeSkippedScrap()
        {
            if (this.mSkippedScrap == null);
            while (true)
            {
                return;
                int i = this.mSkippedScrap.size();
                for (int j = 0; j < i; j++)
                    AbsListView.this.removeDetachedView((View)this.mSkippedScrap.get(j), false);
                this.mSkippedScrap.clear();
            }
        }

        void scrapActiveViews()
        {
            View[] arrayOfView = this.mActiveViews;
            int i;
            int j;
            label24: ArrayList localArrayList;
            int k;
            label37: View localView;
            AbsListView.LayoutParams localLayoutParams;
            int m;
            if (this.mRecyclerListener != null)
            {
                i = 1;
                if (this.mViewTypeCount <= 1)
                    break label168;
                j = 1;
                localArrayList = this.mCurrentScrap;
                k = -1 + arrayOfView.length;
                if (k < 0)
                    break label235;
                localView = arrayOfView[k];
                if (localView != null)
                {
                    localLayoutParams = (AbsListView.LayoutParams)localView.getLayoutParams();
                    m = localLayoutParams.viewType;
                    arrayOfView[k] = null;
                    boolean bool = localView.hasTransientState();
                    if ((shouldRecycleViewType(m)) && (!bool))
                        break label173;
                    if ((m != -2) || (bool))
                        AbsListView.this.removeDetachedView(localView, false);
                    if (bool)
                    {
                        if (this.mTransientStateViews == null)
                            this.mTransientStateViews = new SparseArray();
                        this.mTransientStateViews.put(k + this.mFirstActivePosition, localView);
                    }
                }
            }
            while (true)
            {
                k--;
                break label37;
                i = 0;
                break;
                label168: j = 0;
                break label24;
                label173: if (j != 0)
                    localArrayList = this.mScrapViews[m];
                localView.dispatchStartTemporaryDetach();
                localLayoutParams.scrappedFromPosition = (k + this.mFirstActivePosition);
                localArrayList.add(localView);
                localView.setAccessibilityDelegate(null);
                if (i != 0)
                    this.mRecyclerListener.onMovedToScrapHeap(localView);
            }
            label235: pruneScrapViews();
        }

        void setCacheColorHint(int paramInt)
        {
            if (this.mViewTypeCount == 1)
            {
                ArrayList localArrayList2 = this.mCurrentScrap;
                int i2 = localArrayList2.size();
                for (int i3 = 0; i3 < i2; i3++)
                    ((View)localArrayList2.get(i3)).setDrawingCacheBackgroundColor(paramInt);
            }
            int i = this.mViewTypeCount;
            for (int j = 0; j < i; j++)
            {
                ArrayList localArrayList1 = this.mScrapViews[j];
                int n = localArrayList1.size();
                for (int i1 = 0; i1 < n; i1++)
                    ((View)localArrayList1.get(i1)).setDrawingCacheBackgroundColor(paramInt);
            }
            for (View localView : this.mActiveViews)
                if (localView != null)
                    localView.setDrawingCacheBackgroundColor(paramInt);
        }

        public void setViewTypeCount(int paramInt)
        {
            if (paramInt < 1)
                throw new IllegalArgumentException("Can't have a viewTypeCount < 1");
            ArrayList[] arrayOfArrayList = new ArrayList[paramInt];
            for (int i = 0; i < paramInt; i++)
                arrayOfArrayList[i] = new ArrayList();
            this.mViewTypeCount = paramInt;
            this.mCurrentScrap = arrayOfArrayList[0];
            this.mScrapViews = arrayOfArrayList;
        }

        public boolean shouldRecycleViewType(int paramInt)
        {
            if (paramInt >= 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    public static abstract interface RecyclerListener
    {
        public abstract void onMovedToScrapHeap(View paramView);
    }

    public static class LayoutParams extends ViewGroup.LayoutParams
    {

        @ViewDebug.ExportedProperty(category="list")
        boolean forceAdd;
        long itemId = -1L;

        @ViewDebug.ExportedProperty(category="list")
        boolean recycledHeaderFooter;
        int scrappedFromPosition;

        @ViewDebug.ExportedProperty(category="list", mapping={@android.view.ViewDebug.IntToString(from=-1, to="ITEM_VIEW_TYPE_IGNORE"), @android.view.ViewDebug.IntToString(from=-2, to="ITEM_VIEW_TYPE_HEADER_OR_FOOTER")})
        int viewType;

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3)
        {
            super(paramInt2);
            this.viewType = paramInt3;
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }
    }

    class MultiChoiceModeWrapper
        implements AbsListView.MultiChoiceModeListener
    {
        private AbsListView.MultiChoiceModeListener mWrapped;

        MultiChoiceModeWrapper()
        {
        }

        public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
        {
            return this.mWrapped.onActionItemClicked(paramActionMode, paramMenuItem);
        }

        public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            boolean bool = false;
            if (this.mWrapped.onCreateActionMode(paramActionMode, paramMenu))
            {
                AbsListView.this.setLongClickable(false);
                bool = true;
            }
            return bool;
        }

        public void onDestroyActionMode(ActionMode paramActionMode)
        {
            this.mWrapped.onDestroyActionMode(paramActionMode);
            AbsListView.this.mChoiceActionMode = null;
            AbsListView.this.clearChoices();
            AbsListView.this.mDataChanged = true;
            AbsListView.this.rememberSyncState();
            AbsListView.this.requestLayout();
            AbsListView.this.setLongClickable(true);
        }

        public void onItemCheckedStateChanged(ActionMode paramActionMode, int paramInt, long paramLong, boolean paramBoolean)
        {
            this.mWrapped.onItemCheckedStateChanged(paramActionMode, paramInt, paramLong, paramBoolean);
            if (AbsListView.this.getCheckedItemCount() == 0)
                paramActionMode.finish();
        }

        public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            return this.mWrapped.onPrepareActionMode(paramActionMode, paramMenu);
        }

        public void setWrapped(AbsListView.MultiChoiceModeListener paramMultiChoiceModeListener)
        {
            this.mWrapped = paramMultiChoiceModeListener;
        }
    }

    public static abstract interface MultiChoiceModeListener extends ActionMode.Callback
    {
        public abstract void onItemCheckedStateChanged(ActionMode paramActionMode, int paramInt, long paramLong, boolean paramBoolean);
    }

    class AdapterDataSetObserver extends AdapterView.AdapterDataSetObserver
    {
        AdapterDataSetObserver()
        {
            super();
        }

        public void onChanged()
        {
            super.onChanged();
            if (AbsListView.this.mFastScroller != null)
                AbsListView.this.mFastScroller.onSectionsChanged();
        }

        public void onInvalidated()
        {
            super.onInvalidated();
            if (AbsListView.this.mFastScroller != null)
                AbsListView.this.mFastScroller.onSectionsChanged();
        }
    }

    class PositionScroller
        implements Runnable
    {
        private static final int MOVE_DOWN_BOUND = 3;
        private static final int MOVE_DOWN_POS = 1;
        private static final int MOVE_OFFSET = 5;
        private static final int MOVE_UP_BOUND = 4;
        private static final int MOVE_UP_POS = 2;
        private static final int SCROLL_DURATION = 200;
        private int mBoundPos;
        private final int mExtraScroll = ViewConfiguration.get(AbsListView.access$2900(AbsListView.this)).getScaledFadingEdgeLength();
        private int mLastSeenPos;
        private int mMode;
        private int mOffsetFromTop;
        private int mScrollDuration;
        private int mTargetPos;

        PositionScroller()
        {
        }

        public void run()
        {
            int i = AbsListView.this.getHeight();
            int j = AbsListView.this.mFirstPosition;
            switch (this.mMode)
            {
            default:
            case 1:
            case 3:
            case 2:
            case 4:
            case 5:
            }
            while (true)
            {
                return;
                int i23 = -1 + AbsListView.this.getChildCount();
                int i24 = j + i23;
                if (i23 >= 0)
                    if (i24 == this.mLastSeenPos)
                    {
                        AbsListView.this.postOnAnimation(this);
                    }
                    else
                    {
                        View localView4 = AbsListView.this.getChildAt(i23);
                        int i25 = localView4.getHeight();
                        int i26 = i - localView4.getTop();
                        if (i24 < -1 + AbsListView.this.mItemCount);
                        for (int i27 = Math.max(AbsListView.this.mListPadding.bottom, this.mExtraScroll); ; i27 = AbsListView.this.mListPadding.bottom)
                        {
                            int i28 = i27 + (i25 - i26);
                            AbsListView.this.smoothScrollBy(i28, this.mScrollDuration, true);
                            this.mLastSeenPos = i24;
                            if (i24 >= this.mTargetPos)
                                break;
                            AbsListView.this.postOnAnimation(this);
                            break;
                        }
                        int i18 = AbsListView.this.getChildCount();
                        if ((j != this.mBoundPos) && (i18 > 1) && (j + i18 < AbsListView.this.mItemCount))
                        {
                            int i19 = j + 1;
                            if (i19 == this.mLastSeenPos)
                            {
                                AbsListView.this.postOnAnimation(this);
                            }
                            else
                            {
                                View localView3 = AbsListView.this.getChildAt(1);
                                int i20 = localView3.getHeight();
                                int i21 = localView3.getTop();
                                int i22 = Math.max(AbsListView.this.mListPadding.bottom, this.mExtraScroll);
                                if (i19 < this.mBoundPos)
                                {
                                    AbsListView.this.smoothScrollBy(Math.max(0, i20 + i21 - i22), this.mScrollDuration, true);
                                    this.mLastSeenPos = i19;
                                    AbsListView.this.postOnAnimation(this);
                                }
                                else if (i21 > i22)
                                {
                                    AbsListView.this.smoothScrollBy(i21 - i22, this.mScrollDuration, true);
                                    continue;
                                    if (j == this.mLastSeenPos)
                                    {
                                        AbsListView.this.postOnAnimation(this);
                                    }
                                    else
                                    {
                                        View localView2 = AbsListView.this.getChildAt(0);
                                        if (localView2 != null)
                                        {
                                            int i16 = localView2.getTop();
                                            if (j > 0);
                                            for (int i17 = Math.max(this.mExtraScroll, AbsListView.this.mListPadding.top); ; i17 = AbsListView.this.mListPadding.top)
                                            {
                                                AbsListView.this.smoothScrollBy(i16 - i17, this.mScrollDuration, true);
                                                this.mLastSeenPos = j;
                                                if (j <= this.mTargetPos)
                                                    break;
                                                AbsListView.this.postOnAnimation(this);
                                                break;
                                            }
                                            int i8 = -2 + AbsListView.this.getChildCount();
                                            if (i8 >= 0)
                                            {
                                                int i9 = j + i8;
                                                if (i9 == this.mLastSeenPos)
                                                {
                                                    AbsListView.this.postOnAnimation(this);
                                                }
                                                else
                                                {
                                                    View localView1 = AbsListView.this.getChildAt(i8);
                                                    int i10 = localView1.getHeight();
                                                    int i11 = localView1.getTop();
                                                    int i12 = i - i11;
                                                    int i13 = Math.max(AbsListView.this.mListPadding.top, this.mExtraScroll);
                                                    this.mLastSeenPos = i9;
                                                    if (i9 > this.mBoundPos)
                                                    {
                                                        AbsListView.this.smoothScrollBy(-(i12 - i13), this.mScrollDuration, true);
                                                        AbsListView.this.postOnAnimation(this);
                                                    }
                                                    else
                                                    {
                                                        int i14 = i - i13;
                                                        int i15 = i11 + i10;
                                                        if (i14 > i15)
                                                        {
                                                            AbsListView.this.smoothScrollBy(-(i14 - i15), this.mScrollDuration, true);
                                                            continue;
                                                            if (this.mLastSeenPos == j)
                                                            {
                                                                AbsListView.this.postOnAnimation(this);
                                                            }
                                                            else
                                                            {
                                                                this.mLastSeenPos = j;
                                                                int k = AbsListView.this.getChildCount();
                                                                int m = this.mTargetPos;
                                                                int n = -1 + (j + k);
                                                                int i1 = 0;
                                                                if (m < j)
                                                                    i1 = 1 + (j - m);
                                                                float f;
                                                                while (true)
                                                                {
                                                                    f = Math.min(Math.abs(i1 / k), 1.0F);
                                                                    if (m >= j)
                                                                        break label856;
                                                                    int i6 = (int)(f * -AbsListView.this.getHeight());
                                                                    int i7 = (int)(f * this.mScrollDuration);
                                                                    AbsListView.this.smoothScrollBy(i6, i7, true);
                                                                    AbsListView.this.postOnAnimation(this);
                                                                    break;
                                                                    if (m > n)
                                                                        i1 = m - n;
                                                                }
                                                                label856: if (m > n)
                                                                {
                                                                    int i4 = (int)(f * AbsListView.this.getHeight());
                                                                    int i5 = (int)(f * this.mScrollDuration);
                                                                    AbsListView.this.smoothScrollBy(i4, i5, true);
                                                                    AbsListView.this.postOnAnimation(this);
                                                                }
                                                                else
                                                                {
                                                                    int i2 = AbsListView.this.getChildAt(m - j).getTop() - this.mOffsetFromTop;
                                                                    int i3 = (int)(this.mScrollDuration * (Math.abs(i2) / AbsListView.this.getHeight()));
                                                                    AbsListView.this.smoothScrollBy(i2, i3, true);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }

        void scrollToVisible(int paramInt1, int paramInt2, int paramInt3)
        {
            int i = AbsListView.this.mFirstPosition;
            int j = -1 + (i + AbsListView.this.getChildCount());
            int k = AbsListView.this.mListPadding.top;
            int m = AbsListView.this.getHeight() - AbsListView.this.mListPadding.bottom;
            if ((paramInt1 < i) || (paramInt1 > j))
                Log.w("AbsListView", "scrollToVisible called with targetPos " + paramInt1 + " not visible [" + i + ", " + j + "]");
            if ((paramInt2 < i) || (paramInt2 > j))
                paramInt2 = -1;
            View localView1 = AbsListView.this.getChildAt(paramInt1 - i);
            int n = localView1.getTop();
            int i1 = localView1.getBottom();
            int i2 = 0;
            if (i1 > m)
                i2 = i1 - m;
            if (n < k)
                i2 = n - k;
            if (i2 == 0)
                return;
            int i3;
            int i4;
            int i5;
            if (paramInt2 >= 0)
            {
                View localView2 = AbsListView.this.getChildAt(paramInt2 - i);
                i3 = localView2.getTop();
                i4 = localView2.getBottom();
                i5 = Math.abs(i2);
                if ((i2 >= 0) || (i4 + i5 <= m))
                    break label274;
            }
            for (i2 = Math.max(0, i4 - m); ; i2 = Math.min(0, i3 - k))
                label274: 
                do
                {
                    AbsListView.this.smoothScrollBy(i2, paramInt3);
                    break;
                }
                while ((i2 <= 0) || (i3 - i5 >= k));
        }

        void start(final int paramInt)
        {
            stop();
            if (AbsListView.this.mDataChanged)
                AbsListView.this.mPositionScrollAfterLayout = new Runnable()
                {
                    public void run()
                    {
                        AbsListView.PositionScroller.this.start(paramInt);
                    }
                };
            int i;
            do
            {
                return;
                i = AbsListView.this.getChildCount();
            }
            while (i == 0);
            int j = AbsListView.this.mFirstPosition;
            int k = -1 + (j + i);
            int m = Math.max(0, Math.min(-1 + AbsListView.this.getCount(), paramInt));
            int n;
            if (m < j)
            {
                n = 1 + (j - m);
                this.mMode = 2;
                label98: if (n <= 0)
                    break label180;
            }
            label180: for (this.mScrollDuration = (200 / n); ; this.mScrollDuration = 200)
            {
                this.mTargetPos = m;
                this.mBoundPos = -1;
                this.mLastSeenPos = -1;
                AbsListView.this.postOnAnimation(this);
                break;
                if (m > k)
                {
                    n = 1 + (m - k);
                    this.mMode = 1;
                    break label98;
                }
                scrollToVisible(m, -1, 200);
                break;
            }
        }

        void start(final int paramInt1, final int paramInt2)
        {
            stop();
            if (paramInt2 == -1)
                start(paramInt1);
            int j;
            int k;
            int m;
            int i4;
            do
            {
                int i;
                do
                {
                    while (true)
                    {
                        return;
                        if (!AbsListView.this.mDataChanged)
                            break;
                        AbsListView.this.mPositionScrollAfterLayout = new Runnable()
                        {
                            public void run()
                            {
                                AbsListView.PositionScroller.this.start(paramInt1, paramInt2);
                            }
                        };
                    }
                    i = AbsListView.this.getChildCount();
                }
                while (i == 0);
                j = AbsListView.this.mFirstPosition;
                k = -1 + (j + i);
                m = Math.max(0, Math.min(-1 + AbsListView.this.getCount(), paramInt1));
                if (m >= j)
                    break;
                i4 = k - paramInt2;
            }
            while (i4 < 1);
            int i5 = 1 + (j - m);
            int i6 = i4 - 1;
            int i3;
            if (i6 < i5)
            {
                i3 = i6;
                this.mMode = 4;
                label146: if (i3 <= 0)
                    break label279;
            }
            label279: for (this.mScrollDuration = (200 / i3); ; this.mScrollDuration = 200)
            {
                this.mTargetPos = m;
                this.mBoundPos = paramInt2;
                this.mLastSeenPos = -1;
                AbsListView.this.postOnAnimation(this);
                break;
                i3 = i5;
                this.mMode = 2;
                break label146;
                if (m > k)
                {
                    int n = paramInt2 - j;
                    if (n < 1)
                        break;
                    int i1 = 1 + (m - k);
                    int i2 = n - 1;
                    if (i2 < i1)
                    {
                        i3 = i2;
                        this.mMode = 3;
                        break label146;
                    }
                    i3 = i1;
                    this.mMode = 1;
                    break label146;
                }
                scrollToVisible(m, paramInt2, 200);
                break;
            }
        }

        void startWithOffset(int paramInt1, int paramInt2)
        {
            startWithOffset(paramInt1, paramInt2, 200);
        }

        void startWithOffset(final int paramInt1, final int paramInt2, final int paramInt3)
        {
            stop();
            if (AbsListView.this.mDataChanged)
                AbsListView.this.mPositionScrollAfterLayout = new Runnable()
                {
                    public void run()
                    {
                        AbsListView.PositionScroller.this.startWithOffset(paramInt1, paramInt2, paramInt3);
                    }
                };
            int i;
            do
            {
                return;
                i = AbsListView.this.getChildCount();
            }
            while (i == 0);
            int j = paramInt2 + AbsListView.this.getPaddingTop();
            this.mTargetPos = Math.max(0, Math.min(-1 + AbsListView.this.getCount(), paramInt1));
            this.mOffsetFromTop = j;
            this.mBoundPos = -1;
            this.mLastSeenPos = -1;
            this.mMode = 5;
            int k = AbsListView.this.mFirstPosition;
            int m = -1 + (k + i);
            int i1;
            label140: float f;
            if (this.mTargetPos < k)
            {
                i1 = k - this.mTargetPos;
                f = i1 / i;
                if (f >= 1.0F)
                    break label235;
            }
            while (true)
            {
                this.mScrollDuration = paramInt3;
                this.mLastSeenPos = -1;
                AbsListView.this.postOnAnimation(this);
                break;
                if (this.mTargetPos > m)
                {
                    i1 = this.mTargetPos - m;
                    break label140;
                }
                int n = AbsListView.this.getChildAt(this.mTargetPos - k).getTop();
                AbsListView.this.smoothScrollBy(n - j, paramInt3, true);
                break;
                label235: paramInt3 = (int)(paramInt3 / f);
            }
        }

        void stop()
        {
            AbsListView.this.removeCallbacks(this);
        }
    }

    private class FlingRunnable
        implements Runnable
    {
        private static final int FLYWHEEL_TIMEOUT = 40;
        private final Runnable mCheckFlywheel = new Runnable()
        {
            public void run()
            {
                int i = AbsListView.this.mActivePointerId;
                VelocityTracker localVelocityTracker = AbsListView.this.mVelocityTracker;
                OverScroller localOverScroller = AbsListView.FlingRunnable.this.mScroller;
                if ((localVelocityTracker == null) || (i == -1));
                while (true)
                {
                    return;
                    localVelocityTracker.computeCurrentVelocity(1000, AbsListView.this.mMaximumVelocity);
                    float f = -localVelocityTracker.getYVelocity(i);
                    if ((Math.abs(f) >= AbsListView.this.mMinimumVelocity) && (localOverScroller.isScrollingInDirection(0.0F, f)))
                    {
                        AbsListView.this.postDelayed(this, 40L);
                    }
                    else
                    {
                        AbsListView.FlingRunnable.this.endFling();
                        AbsListView.this.mTouchMode = 3;
                        AbsListView.this.reportScrollStateChange(1);
                    }
                }
            }
        };
        private int mLastFlingY;
        private final OverScroller mScroller = new OverScroller(AbsListView.this.getContext());

        FlingRunnable()
        {
        }

        void edgeReached(int paramInt)
        {
            this.mScroller.notifyVerticalEdgeReached(AbsListView.access$1600(AbsListView.this), 0, AbsListView.this.mOverflingDistance);
            int i = AbsListView.this.getOverScrollMode();
            int j;
            if ((i == 0) || ((i == 1) && (!AbsListView.this.contentFits())))
            {
                AbsListView.this.mTouchMode = 6;
                j = (int)this.mScroller.getCurrVelocity();
                if (paramInt > 0)
                    AbsListView.this.mEdgeGlowTop.onAbsorb(j);
            }
            while (true)
            {
                AbsListView.this.invalidate();
                AbsListView.this.postOnAnimation(this);
                return;
                AbsListView.this.mEdgeGlowBottom.onAbsorb(j);
                continue;
                AbsListView.this.mTouchMode = -1;
                if (AbsListView.this.mPositionScroller != null)
                    AbsListView.this.mPositionScroller.stop();
            }
        }

        void endFling()
        {
            AbsListView.this.mTouchMode = -1;
            AbsListView.this.removeCallbacks(this);
            AbsListView.this.removeCallbacks(this.mCheckFlywheel);
            AbsListView.this.reportScrollStateChange(0);
            AbsListView.this.clearScrollingCache();
            this.mScroller.abortAnimation();
            if (AbsListView.this.mFlingStrictSpan != null)
            {
                AbsListView.this.mFlingStrictSpan.finish();
                AbsListView.access$1302(AbsListView.this, null);
            }
        }

        void flywheelTouch()
        {
            AbsListView.this.postDelayed(this.mCheckFlywheel, 40L);
        }

        public void run()
        {
            switch (AbsListView.this.mTouchMode)
            {
            case 5:
            default:
                endFling();
            case 3:
            case 4:
            case 6:
            }
            while (true)
            {
                return;
                if (!this.mScroller.isFinished())
                {
                    if (AbsListView.this.mDataChanged)
                        AbsListView.this.layoutChildren();
                    label539: if ((AbsListView.this.mItemCount == 0) || (AbsListView.this.getChildCount() == 0))
                    {
                        endFling();
                    }
                    else
                    {
                        OverScroller localOverScroller2 = this.mScroller;
                        boolean bool1 = localOverScroller2.computeScrollOffset();
                        int i2 = localOverScroller2.getCurrY();
                        int i3 = this.mLastFlingY - i2;
                        int i5;
                        label198: View localView2;
                        int i6;
                        boolean bool2;
                        if (i3 > 0)
                        {
                            AbsListView.this.mMotionPosition = AbsListView.this.mFirstPosition;
                            View localView3 = AbsListView.this.getChildAt(0);
                            AbsListView.this.mMotionViewOriginalTop = localView3.getTop();
                            i5 = Math.min(-1 + (AbsListView.this.getHeight() - AbsListView.access$2100(AbsListView.this) - AbsListView.access$2200(AbsListView.this)), i3);
                            localView2 = AbsListView.this.getChildAt(AbsListView.this.mMotionPosition - AbsListView.this.mFirstPosition);
                            i6 = 0;
                            if (localView2 != null)
                                i6 = localView2.getTop();
                            bool2 = AbsListView.this.trackMotionScroll(i5, i5);
                            if ((!bool2) || (i5 == 0))
                                break label420;
                        }
                        label420: for (int i7 = 1; ; i7 = 0)
                        {
                            if (i7 == 0)
                                break label426;
                            if (localView2 != null)
                            {
                                int i8 = -(i5 - (localView2.getTop() - i6));
                                AbsListView.this.overScrollBy(0, i8, 0, AbsListView.access$2500(AbsListView.this), 0, 0, 0, AbsListView.this.mOverflingDistance, false);
                            }
                            if (!bool1)
                                break;
                            edgeReached(i5);
                            break;
                            int i4 = -1 + AbsListView.this.getChildCount();
                            AbsListView.this.mMotionPosition = (i4 + AbsListView.this.mFirstPosition);
                            View localView1 = AbsListView.this.getChildAt(i4);
                            AbsListView.this.mMotionViewOriginalTop = localView1.getTop();
                            i5 = Math.max(-(-1 + (AbsListView.this.getHeight() - AbsListView.access$2300(AbsListView.this) - AbsListView.access$2400(AbsListView.this))), i3);
                            break label198;
                        }
                        label426: if ((bool1) && (i7 == 0))
                        {
                            if (bool2)
                                AbsListView.this.invalidate();
                            this.mLastFlingY = i2;
                            AbsListView.this.postOnAnimation(this);
                        }
                        else
                        {
                            endFling();
                            continue;
                            OverScroller localOverScroller1 = this.mScroller;
                            if (localOverScroller1.computeScrollOffset())
                            {
                                int i = AbsListView.access$2700(AbsListView.this);
                                int j = localOverScroller1.getCurrY();
                                int k = j - i;
                                if (AbsListView.this.overScrollBy(0, k, 0, i, 0, 0, 0, AbsListView.this.mOverflingDistance, false))
                                {
                                    int m;
                                    if ((i <= 0) && (j > 0))
                                    {
                                        m = 1;
                                        if ((i < 0) || (j >= 0))
                                            break label596;
                                    }
                                    label596: for (int n = 1; ; n = 0)
                                    {
                                        if ((m == 0) && (n == 0))
                                            break label602;
                                        int i1 = (int)localOverScroller1.getCurrVelocity();
                                        if (n != 0)
                                            i1 = -i1;
                                        localOverScroller1.abortAnimation();
                                        start(i1);
                                        break;
                                        m = 0;
                                        break label539;
                                    }
                                    label602: startSpringback();
                                }
                                else
                                {
                                    AbsListView.this.invalidate();
                                    AbsListView.this.postOnAnimation(this);
                                }
                            }
                            else
                            {
                                endFling();
                            }
                        }
                    }
                }
            }
        }

        void start(int paramInt)
        {
            if (paramInt < 0);
            for (int i = 2147483647; ; i = 0)
            {
                this.mLastFlingY = i;
                this.mScroller.setInterpolator(null);
                this.mScroller.fling(0, i, 0, paramInt, 0, 2147483647, 0, 2147483647);
                AbsListView.this.mTouchMode = 4;
                AbsListView.this.postOnAnimation(this);
                if (AbsListView.this.mFlingStrictSpan == null)
                    AbsListView.access$1302(AbsListView.this, StrictMode.enterCriticalSpan("AbsListView-fling"));
                return;
            }
        }

        void startOverfling(int paramInt)
        {
            this.mScroller.setInterpolator(null);
            this.mScroller.fling(0, AbsListView.access$1500(AbsListView.this), 0, paramInt, 0, 0, -2147483648, 2147483647, 0, AbsListView.this.getHeight());
            AbsListView.this.mTouchMode = 6;
            AbsListView.this.invalidate();
            AbsListView.this.postOnAnimation(this);
        }

        void startScroll(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            int i;
            OverScroller localOverScroller;
            if (paramInt1 < 0)
            {
                i = 2147483647;
                this.mLastFlingY = i;
                localOverScroller = this.mScroller;
                if (!paramBoolean)
                    break label72;
            }
            label72: for (Interpolator localInterpolator = AbsListView.sLinearInterpolator; ; localInterpolator = null)
            {
                localOverScroller.setInterpolator(localInterpolator);
                this.mScroller.startScroll(0, i, 0, paramInt1, paramInt2);
                AbsListView.this.mTouchMode = 4;
                AbsListView.this.postOnAnimation(this);
                return;
                i = 0;
                break;
            }
        }

        void startSpringback()
        {
            if (this.mScroller.springBack(0, AbsListView.access$1400(AbsListView.this), 0, 0, 0, 0))
            {
                AbsListView.this.mTouchMode = 6;
                AbsListView.this.invalidate();
                AbsListView.this.postOnAnimation(this);
            }
            while (true)
            {
                return;
                AbsListView.this.mTouchMode = -1;
                AbsListView.this.reportScrollStateChange(0);
            }
        }
    }

    final class CheckForTap
        implements Runnable
    {
        CheckForTap()
        {
        }

        public void run()
        {
            Drawable localDrawable;
            if (AbsListView.this.mTouchMode == 0)
            {
                AbsListView.this.mTouchMode = 1;
                View localView = AbsListView.this.getChildAt(AbsListView.this.mMotionPosition - AbsListView.this.mFirstPosition);
                if ((localView != null) && (!localView.hasFocusable()))
                {
                    AbsListView.this.mLayoutMode = 0;
                    if (AbsListView.this.mDataChanged)
                        break label256;
                    localView.setPressed(true);
                    AbsListView.this.setPressed(true);
                    AbsListView.this.layoutChildren();
                    AbsListView.this.positionSelector(AbsListView.this.mMotionPosition, localView);
                    AbsListView.this.refreshDrawableState();
                    int i = ViewConfiguration.getLongPressTimeout();
                    boolean bool = AbsListView.this.isLongClickable();
                    if (AbsListView.this.mSelector != null)
                    {
                        localDrawable = AbsListView.this.mSelector.getCurrent();
                        if ((localDrawable != null) && ((localDrawable instanceof TransitionDrawable)))
                        {
                            if (!bool)
                                break label234;
                            ((TransitionDrawable)localDrawable).startTransition(i);
                        }
                    }
                    if (!bool)
                        break label245;
                    if (AbsListView.this.mPendingCheckForLongPress == null)
                        AbsListView.access$502(AbsListView.this, new AbsListView.CheckForLongPress(AbsListView.this, null));
                    AbsListView.this.mPendingCheckForLongPress.rememberWindowAttachCount();
                    AbsListView.this.postDelayed(AbsListView.this.mPendingCheckForLongPress, i);
                }
            }
            while (true)
            {
                return;
                label234: ((TransitionDrawable)localDrawable).resetTransition();
                break;
                label245: AbsListView.this.mTouchMode = 2;
                continue;
                label256: AbsListView.this.mTouchMode = 2;
            }
        }
    }

    private class CheckForKeyLongPress extends AbsListView.WindowRunnnable
        implements Runnable
    {
        private CheckForKeyLongPress()
        {
            super(null);
        }

        public void run()
        {
            View localView;
            if ((AbsListView.this.isPressed()) && (AbsListView.this.mSelectedPosition >= 0))
            {
                int i = AbsListView.this.mSelectedPosition - AbsListView.this.mFirstPosition;
                localView = AbsListView.this.getChildAt(i);
                if (AbsListView.this.mDataChanged)
                    break label105;
                boolean bool = false;
                if (sameWindow())
                    bool = AbsListView.this.performLongPress(localView, AbsListView.this.mSelectedPosition, AbsListView.this.mSelectedRowId);
                if (bool)
                {
                    AbsListView.this.setPressed(false);
                    localView.setPressed(false);
                }
            }
            while (true)
            {
                return;
                label105: AbsListView.this.setPressed(false);
                if (localView != null)
                    localView.setPressed(false);
            }
        }
    }

    private class CheckForLongPress extends AbsListView.WindowRunnnable
        implements Runnable
    {
        private CheckForLongPress()
        {
            super(null);
        }

        public void run()
        {
            int i = AbsListView.this.mMotionPosition;
            View localView = AbsListView.this.getChildAt(i - AbsListView.this.mFirstPosition);
            if (localView != null)
            {
                int j = AbsListView.this.mMotionPosition;
                long l = AbsListView.this.mAdapter.getItemId(AbsListView.this.mMotionPosition);
                boolean bool = false;
                if ((sameWindow()) && (!AbsListView.this.mDataChanged))
                    bool = AbsListView.this.performLongPress(localView, j, l);
                if (!bool)
                    break label119;
                AbsListView.this.mTouchMode = -1;
                AbsListView.this.setPressed(false);
                localView.setPressed(false);
            }
            while (true)
            {
                return;
                label119: AbsListView.this.mTouchMode = 2;
            }
        }
    }

    private class PerformClick extends AbsListView.WindowRunnnable
        implements Runnable
    {
        int mClickMotionPosition;

        private PerformClick()
        {
            super(null);
        }

        public void run()
        {
            if (AbsListView.this.mDataChanged);
            while (true)
            {
                return;
                ListAdapter localListAdapter = AbsListView.this.mAdapter;
                int i = this.mClickMotionPosition;
                if ((localListAdapter != null) && (AbsListView.this.mItemCount > 0) && (i != -1) && (i < localListAdapter.getCount()) && (sameWindow()))
                {
                    View localView = AbsListView.this.getChildAt(i - AbsListView.this.mFirstPosition);
                    if (localView != null)
                        AbsListView.this.performItemClick(localView, i, localListAdapter.getItemId(i));
                }
            }
        }
    }

    private class WindowRunnnable
    {
        private int mOriginalAttachCount;

        private WindowRunnnable()
        {
        }

        public void rememberWindowAttachCount()
        {
            this.mOriginalAttachCount = AbsListView.this.getWindowAttachCount();
        }

        public boolean sameWindow()
        {
            if ((AbsListView.this.hasWindowFocus()) && (AbsListView.this.getWindowAttachCount() == this.mOriginalAttachCount));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    class ListItemAccessibilityDelegate extends View.AccessibilityDelegate
    {
        ListItemAccessibilityDelegate()
        {
        }

        public void onInitializeAccessibilityNodeInfo(View paramView, AccessibilityNodeInfo paramAccessibilityNodeInfo)
        {
            super.onInitializeAccessibilityNodeInfo(paramView, paramAccessibilityNodeInfo);
            int i = AbsListView.this.getPositionForView(paramView);
            ListAdapter localListAdapter = (ListAdapter)AbsListView.this.getAdapter();
            if ((i == -1) || (localListAdapter == null))
                return;
            if ((AbsListView.this.isEnabled()) && (localListAdapter.isEnabled(i)))
            {
                if (i != AbsListView.this.getSelectedItemPosition())
                    break label127;
                paramAccessibilityNodeInfo.setSelected(true);
                paramAccessibilityNodeInfo.addAction(8);
            }
            while (true)
            {
                if (AbsListView.this.isClickable())
                {
                    paramAccessibilityNodeInfo.addAction(16);
                    paramAccessibilityNodeInfo.setClickable(true);
                }
                if (!AbsListView.this.isLongClickable())
                    break;
                paramAccessibilityNodeInfo.addAction(32);
                paramAccessibilityNodeInfo.setLongClickable(true);
                break;
                break;
                label127: paramAccessibilityNodeInfo.addAction(4);
            }
        }

        public boolean performAccessibilityAction(View paramView, int paramInt, Bundle paramBundle)
        {
            boolean bool = true;
            if (super.performAccessibilityAction(paramView, paramInt, paramBundle));
            while (true)
            {
                return bool;
                int i = AbsListView.this.getPositionForView(paramView);
                ListAdapter localListAdapter = (ListAdapter)AbsListView.this.getAdapter();
                if ((i == -1) || (localListAdapter == null))
                {
                    bool = false;
                }
                else if ((!AbsListView.this.isEnabled()) || (!localListAdapter.isEnabled(i)))
                {
                    bool = false;
                }
                else
                {
                    long l = AbsListView.this.getItemIdAtPosition(i);
                    switch (paramInt)
                    {
                    default:
                        bool = false;
                        break;
                    case 8:
                        if (AbsListView.this.getSelectedItemPosition() == i)
                            AbsListView.this.setSelection(-1);
                        else
                            bool = false;
                        break;
                    case 4:
                        if (AbsListView.this.getSelectedItemPosition() != i)
                            AbsListView.this.setSelection(i);
                        else
                            bool = false;
                        break;
                    case 16:
                        if (AbsListView.this.isClickable())
                            bool = AbsListView.this.performItemClick(paramView, i, l);
                        else
                            bool = false;
                        break;
                    case 32:
                        if (AbsListView.this.isLongClickable())
                            bool = AbsListView.this.performLongPress(paramView, i, l);
                        else
                            bool = false;
                        break;
                    }
                }
            }
        }
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public AbsListView.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new AbsListView.SavedState(paramAnonymousParcel, null);
            }

            public AbsListView.SavedState[] newArray(int paramAnonymousInt)
            {
                return new AbsListView.SavedState[paramAnonymousInt];
            }
        };
        LongSparseArray<Integer> checkIdState;
        SparseBooleanArray checkState;
        int checkedItemCount;
        String filter;
        long firstId;
        int height;
        boolean inActionMode;
        int position;
        long selectedId;
        int viewTop;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.selectedId = paramParcel.readLong();
            this.firstId = paramParcel.readLong();
            this.viewTop = paramParcel.readInt();
            this.position = paramParcel.readInt();
            this.height = paramParcel.readInt();
            this.filter = paramParcel.readString();
            if (paramParcel.readByte() != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.inActionMode = bool;
                this.checkedItemCount = paramParcel.readInt();
                this.checkState = paramParcel.readSparseBooleanArray();
                int i = paramParcel.readInt();
                if (i <= 0)
                    break;
                this.checkIdState = new LongSparseArray();
                for (int j = 0; j < i; j++)
                {
                    long l = paramParcel.readLong();
                    int k = paramParcel.readInt();
                    this.checkIdState.put(l, Integer.valueOf(k));
                }
            }
        }

        SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public String toString()
        {
            return "AbsListView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.selectedId + " firstId=" + this.firstId + " viewTop=" + this.viewTop + " position=" + this.position + " height=" + this.height + " filter=" + this.filter + " checkState=" + this.checkState + "}";
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeLong(this.selectedId);
            paramParcel.writeLong(this.firstId);
            paramParcel.writeInt(this.viewTop);
            paramParcel.writeInt(this.position);
            paramParcel.writeInt(this.height);
            paramParcel.writeString(this.filter);
            int i;
            if (this.inActionMode)
            {
                i = 1;
                paramParcel.writeByte((byte)i);
                paramParcel.writeInt(this.checkedItemCount);
                paramParcel.writeSparseBooleanArray(this.checkState);
                if (this.checkIdState == null)
                    break label160;
            }
            label160: for (int j = this.checkIdState.size(); ; j = 0)
            {
                paramParcel.writeInt(j);
                for (int k = 0; k < j; k++)
                {
                    paramParcel.writeLong(this.checkIdState.keyAt(k));
                    paramParcel.writeInt(((Integer)this.checkIdState.valueAt(k)).intValue());
                }
                i = 0;
                break;
            }
        }
    }

    public static abstract interface SelectionBoundsAdjuster
    {
        public abstract void adjustListItemSelectionBounds(Rect paramRect);
    }

    public static abstract interface OnScrollListener
    {
        public static final int SCROLL_STATE_FLING = 2;
        public static final int SCROLL_STATE_IDLE = 0;
        public static final int SCROLL_STATE_TOUCH_SCROLL = 1;

        public abstract void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3);

        public abstract void onScrollStateChanged(AbsListView paramAbsListView, int paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean isOutOfTouchRange(AbsListView paramAbsListView, MotionEvent paramMotionEvent)
        {
            if ((paramMotionEvent.getX() < paramAbsListView.mTouchPaddingLeft) || (paramMotionEvent.getX() > paramAbsListView.getWidth() - paramAbsListView.mTouchPaddingRight));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AbsListView
 * JD-Core Version:        0.6.2
 */