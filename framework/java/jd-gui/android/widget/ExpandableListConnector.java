package android.widget;

import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;

class ExpandableListConnector extends BaseAdapter
    implements Filterable
{
    private final DataSetObserver mDataSetObserver = new MyDataSetObserver();
    private ArrayList<GroupMetadata> mExpGroupMetadataList = new ArrayList();
    private ExpandableListAdapter mExpandableListAdapter;
    private int mMaxExpGroupCount = 2147483647;
    private int mTotalExpChildrenCount;

    public ExpandableListConnector(ExpandableListAdapter paramExpandableListAdapter)
    {
        setExpandableListAdapter(paramExpandableListAdapter);
    }

    private void refreshExpGroupMetadataList(boolean paramBoolean1, boolean paramBoolean2)
    {
        ArrayList localArrayList = this.mExpGroupMetadataList;
        int i = localArrayList.size();
        int j = 0;
        this.mTotalExpChildrenCount = 0;
        if (paramBoolean2)
        {
            int i2 = 0;
            for (int i3 = i - 1; i3 >= 0; i3--)
            {
                GroupMetadata localGroupMetadata2 = (GroupMetadata)localArrayList.get(i3);
                int i4 = findGroupPosition(localGroupMetadata2.gId, localGroupMetadata2.gPos);
                if (i4 != localGroupMetadata2.gPos)
                {
                    if (i4 == -1)
                    {
                        localArrayList.remove(i3);
                        i--;
                    }
                    localGroupMetadata2.gPos = i4;
                    if (i2 == 0)
                        i2 = 1;
                }
            }
            if (i2 != 0)
                Collections.sort(localArrayList);
        }
        int k = 0;
        int m = 0;
        if (m < i)
        {
            GroupMetadata localGroupMetadata1 = (GroupMetadata)localArrayList.get(m);
            if ((localGroupMetadata1.lastChildFlPos == -1) || (paramBoolean1));
            for (int n = this.mExpandableListAdapter.getChildrenCount(localGroupMetadata1.gPos); ; n = localGroupMetadata1.lastChildFlPos - localGroupMetadata1.flPos)
            {
                this.mTotalExpChildrenCount = (n + this.mTotalExpChildrenCount);
                int i1 = j + (localGroupMetadata1.gPos - k);
                k = localGroupMetadata1.gPos;
                localGroupMetadata1.flPos = i1;
                j = i1 + n;
                localGroupMetadata1.lastChildFlPos = j;
                m++;
                break;
            }
        }
    }

    public boolean areAllItemsEnabled()
    {
        return this.mExpandableListAdapter.areAllItemsEnabled();
    }

    boolean collapseGroup(int paramInt)
    {
        ExpandableListPosition localExpandableListPosition = ExpandableListPosition.obtain(2, paramInt, -1, -1);
        PositionMetadata localPositionMetadata = getFlattenedPos(localExpandableListPosition);
        localExpandableListPosition.recycle();
        boolean bool;
        if (localPositionMetadata == null)
            bool = false;
        while (true)
        {
            return bool;
            bool = collapseGroup(localPositionMetadata);
            localPositionMetadata.recycle();
        }
    }

    boolean collapseGroup(PositionMetadata paramPositionMetadata)
    {
        boolean bool = false;
        if (paramPositionMetadata.groupMetadata == null);
        while (true)
        {
            return bool;
            this.mExpGroupMetadataList.remove(paramPositionMetadata.groupMetadata);
            refreshExpGroupMetadataList(false, false);
            notifyDataSetChanged();
            this.mExpandableListAdapter.onGroupCollapsed(paramPositionMetadata.groupMetadata.gPos);
            bool = true;
        }
    }

    boolean expandGroup(int paramInt)
    {
        ExpandableListPosition localExpandableListPosition = ExpandableListPosition.obtain(2, paramInt, -1, -1);
        PositionMetadata localPositionMetadata = getFlattenedPos(localExpandableListPosition);
        localExpandableListPosition.recycle();
        boolean bool = expandGroup(localPositionMetadata);
        localPositionMetadata.recycle();
        return bool;
    }

    boolean expandGroup(PositionMetadata paramPositionMetadata)
    {
        boolean bool = false;
        if (paramPositionMetadata.position.groupPos < 0)
            throw new RuntimeException("Need group");
        if (this.mMaxExpGroupCount == 0);
        while (true)
        {
            return bool;
            if (paramPositionMetadata.groupMetadata == null)
            {
                if (this.mExpGroupMetadataList.size() >= this.mMaxExpGroupCount)
                {
                    GroupMetadata localGroupMetadata2 = (GroupMetadata)this.mExpGroupMetadataList.get(0);
                    int i = this.mExpGroupMetadataList.indexOf(localGroupMetadata2);
                    collapseGroup(localGroupMetadata2.gPos);
                    if (paramPositionMetadata.groupInsertIndex > i)
                        paramPositionMetadata.groupInsertIndex = (-1 + paramPositionMetadata.groupInsertIndex);
                }
                GroupMetadata localGroupMetadata1 = GroupMetadata.obtain(-1, -1, paramPositionMetadata.position.groupPos, this.mExpandableListAdapter.getGroupId(paramPositionMetadata.position.groupPos));
                this.mExpGroupMetadataList.add(paramPositionMetadata.groupInsertIndex, localGroupMetadata1);
                refreshExpGroupMetadataList(false, false);
                notifyDataSetChanged();
                this.mExpandableListAdapter.onGroupExpanded(localGroupMetadata1.gPos);
                bool = true;
            }
        }
    }

    int findGroupPosition(long paramLong, int paramInt)
    {
        int i = this.mExpandableListAdapter.getGroupCount();
        int i2;
        if (i == 0)
            i2 = -1;
        int k;
        long l;
        int m;
        int n;
        ExpandableListAdapter localExpandableListAdapter;
        while (true)
        {
            return i2;
            if (paramLong == -9223372036854775808L)
            {
                i2 = -1;
            }
            else
            {
                int j = Math.max(0, paramInt);
                k = Math.min(i - 1, j);
                l = 100L + SystemClock.uptimeMillis();
                m = k;
                n = k;
                i1 = 0;
                localExpandableListAdapter = getAdapter();
                if (localExpandableListAdapter != null)
                    break;
                i2 = -1;
            }
        }
        label94: int i4;
        int i3;
        if ((i4 != 0) || ((i1 != 0) && (i3 == 0)))
        {
            n++;
            k = n;
        }
        for (int i1 = 0; ; i1 = 1)
        {
            label161: label186: label192: label196: 
            do
            {
                if (SystemClock.uptimeMillis() <= l)
                {
                    if (localExpandableListAdapter.getGroupId(k) == paramLong)
                    {
                        i2 = k;
                        break;
                    }
                    if (n != i - 1)
                        break label186;
                    i3 = 1;
                    if (m != 0)
                        break label192;
                }
                for (i4 = 1; ; i4 = 0)
                {
                    if ((i3 == 0) || (i4 == 0))
                        break label196;
                    i2 = -1;
                    break;
                    i3 = 0;
                    break label161;
                }
                break label94;
            }
            while ((i3 == 0) && ((i1 != 0) || (i4 != 0)));
            m--;
            k = m;
        }
    }

    ExpandableListAdapter getAdapter()
    {
        return this.mExpandableListAdapter;
    }

    public int getCount()
    {
        return this.mExpandableListAdapter.getGroupCount() + this.mTotalExpChildrenCount;
    }

    ArrayList<GroupMetadata> getExpandedGroupMetadataList()
    {
        return this.mExpGroupMetadataList;
    }

    public Filter getFilter()
    {
        ExpandableListAdapter localExpandableListAdapter = getAdapter();
        if ((localExpandableListAdapter instanceof Filterable));
        for (Filter localFilter = ((Filterable)localExpandableListAdapter).getFilter(); ; localFilter = null)
            return localFilter;
    }

    PositionMetadata getFlattenedPos(ExpandableListPosition paramExpandableListPosition)
    {
        ArrayList localArrayList = this.mExpGroupMetadataList;
        int i = localArrayList.size();
        int j = 0;
        int k = i - 1;
        PositionMetadata localPositionMetadata;
        if (i == 0)
        {
            localPositionMetadata = PositionMetadata.obtain(paramExpandableListPosition.groupPos, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, null, 0);
            return localPositionMetadata;
        }
        while (true)
        {
            if (j <= k)
            {
                m = j + (k - j) / 2;
                GroupMetadata localGroupMetadata3 = (GroupMetadata)localArrayList.get(m);
                if (paramExpandableListPosition.groupPos > localGroupMetadata3.gPos)
                {
                    j = m + 1;
                    continue;
                }
                if (paramExpandableListPosition.groupPos < localGroupMetadata3.gPos)
                {
                    k = m - 1;
                    continue;
                }
                if (paramExpandableListPosition.groupPos != localGroupMetadata3.gPos)
                    continue;
                if (paramExpandableListPosition.type == 2)
                {
                    localPositionMetadata = PositionMetadata.obtain(localGroupMetadata3.flPos, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, localGroupMetadata3, m);
                    break;
                }
                if (paramExpandableListPosition.type == 1)
                {
                    localPositionMetadata = PositionMetadata.obtain(1 + (localGroupMetadata3.flPos + paramExpandableListPosition.childPos), paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, localGroupMetadata3, m);
                    break;
                }
                localPositionMetadata = null;
                break;
            }
            if (paramExpandableListPosition.type != 2)
            {
                localPositionMetadata = null;
                break;
            }
            if (j > m)
            {
                GroupMetadata localGroupMetadata2 = (GroupMetadata)localArrayList.get(j - 1);
                localPositionMetadata = PositionMetadata.obtain(localGroupMetadata2.lastChildFlPos + (paramExpandableListPosition.groupPos - localGroupMetadata2.gPos), paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, null, j);
                break;
            }
            if (k < m)
            {
                int n = k + 1;
                GroupMetadata localGroupMetadata1 = (GroupMetadata)localArrayList.get(n);
                localPositionMetadata = PositionMetadata.obtain(localGroupMetadata1.flPos - (localGroupMetadata1.gPos - paramExpandableListPosition.groupPos), paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, null, n);
                break;
            }
            localPositionMetadata = null;
            break;
            int m = 0;
        }
    }

    public Object getItem(int paramInt)
    {
        PositionMetadata localPositionMetadata = getUnflattenedPos(paramInt);
        if (localPositionMetadata.position.type == 2);
        for (Object localObject = this.mExpandableListAdapter.getGroup(localPositionMetadata.position.groupPos); ; localObject = this.mExpandableListAdapter.getChild(localPositionMetadata.position.groupPos, localPositionMetadata.position.childPos))
        {
            localPositionMetadata.recycle();
            return localObject;
            if (localPositionMetadata.position.type != 1)
                break;
        }
        throw new RuntimeException("Flat list position is of unknown type");
    }

    public long getItemId(int paramInt)
    {
        PositionMetadata localPositionMetadata = getUnflattenedPos(paramInt);
        long l1 = this.mExpandableListAdapter.getGroupId(localPositionMetadata.position.groupPos);
        if (localPositionMetadata.position.type == 2);
        long l2;
        for (long l3 = this.mExpandableListAdapter.getCombinedGroupId(l1); ; l3 = this.mExpandableListAdapter.getCombinedChildId(l1, l2))
        {
            localPositionMetadata.recycle();
            return l3;
            if (localPositionMetadata.position.type != 1)
                break;
            l2 = this.mExpandableListAdapter.getChildId(localPositionMetadata.position.groupPos, localPositionMetadata.position.childPos);
        }
        throw new RuntimeException("Flat list position is of unknown type");
    }

    public int getItemViewType(int paramInt)
    {
        PositionMetadata localPositionMetadata = getUnflattenedPos(paramInt);
        ExpandableListPosition localExpandableListPosition = localPositionMetadata.position;
        HeterogeneousExpandableList localHeterogeneousExpandableList;
        int i;
        if ((this.mExpandableListAdapter instanceof HeterogeneousExpandableList))
        {
            localHeterogeneousExpandableList = (HeterogeneousExpandableList)this.mExpandableListAdapter;
            if (localExpandableListPosition.type == 2)
                i = localHeterogeneousExpandableList.getGroupType(localExpandableListPosition.groupPos);
        }
        while (true)
        {
            localPositionMetadata.recycle();
            return i;
            i = localHeterogeneousExpandableList.getChildType(localExpandableListPosition.groupPos, localExpandableListPosition.childPos) + localHeterogeneousExpandableList.getGroupTypeCount();
            continue;
            if (localExpandableListPosition.type == 2)
                i = 0;
            else
                i = 1;
        }
    }

    PositionMetadata getUnflattenedPos(int paramInt)
    {
        ArrayList localArrayList = this.mExpGroupMetadataList;
        int i = localArrayList.size();
        int j = 0;
        int k = i - 1;
        PositionMetadata localPositionMetadata;
        if (i == 0)
        {
            localPositionMetadata = PositionMetadata.obtain(paramInt, 2, paramInt, -1, null, 0);
            return localPositionMetadata;
        }
        while (true)
        {
            if (j <= k)
            {
                m = j + (k - j) / 2;
                GroupMetadata localGroupMetadata3 = (GroupMetadata)localArrayList.get(m);
                if (paramInt > localGroupMetadata3.lastChildFlPos)
                {
                    j = m + 1;
                    continue;
                }
                if (paramInt < localGroupMetadata3.flPos)
                {
                    k = m - 1;
                    continue;
                }
                if (paramInt == localGroupMetadata3.flPos)
                {
                    localPositionMetadata = PositionMetadata.obtain(paramInt, 2, localGroupMetadata3.gPos, -1, localGroupMetadata3, m);
                    break;
                }
                if (paramInt > localGroupMetadata3.lastChildFlPos)
                    continue;
                int i3 = paramInt - (1 + localGroupMetadata3.flPos);
                localPositionMetadata = PositionMetadata.obtain(paramInt, 1, localGroupMetadata3.gPos, i3, localGroupMetadata3, m);
                break;
            }
            GroupMetadata localGroupMetadata2;
            int i1;
            if (j > m)
            {
                localGroupMetadata2 = (GroupMetadata)localArrayList.get(j - 1);
                i1 = j;
            }
            GroupMetadata localGroupMetadata1;
            for (int i2 = paramInt - localGroupMetadata2.lastChildFlPos + localGroupMetadata2.gPos; ; i2 = localGroupMetadata1.gPos - (localGroupMetadata1.flPos - paramInt))
            {
                localPositionMetadata = PositionMetadata.obtain(paramInt, 2, i2, -1, null, i1);
                break;
                if (k >= m)
                    break label276;
                int n = k + 1;
                localGroupMetadata1 = (GroupMetadata)localArrayList.get(n);
                i1 = n;
            }
            label276: throw new RuntimeException("Unknown state");
            int m = 0;
        }
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        int i = 1;
        PositionMetadata localPositionMetadata = getUnflattenedPos(paramInt);
        View localView;
        if (localPositionMetadata.position.type == 2)
        {
            localView = this.mExpandableListAdapter.getGroupView(localPositionMetadata.position.groupPos, localPositionMetadata.isExpanded(), paramView, paramViewGroup);
            localPositionMetadata.recycle();
            return localView;
        }
        if (localPositionMetadata.position.type == i)
        {
            if (localPositionMetadata.groupMetadata.lastChildFlPos == paramInt);
            while (true)
            {
                localView = this.mExpandableListAdapter.getChildView(localPositionMetadata.position.groupPos, localPositionMetadata.position.childPos, i, paramView, paramViewGroup);
                break;
                int j = 0;
            }
        }
        throw new RuntimeException("Flat list position is of unknown type");
    }

    public int getViewTypeCount()
    {
        HeterogeneousExpandableList localHeterogeneousExpandableList;
        if ((this.mExpandableListAdapter instanceof HeterogeneousExpandableList))
            localHeterogeneousExpandableList = (HeterogeneousExpandableList)this.mExpandableListAdapter;
        for (int i = localHeterogeneousExpandableList.getGroupTypeCount() + localHeterogeneousExpandableList.getChildTypeCount(); ; i = 2)
            return i;
    }

    public boolean hasStableIds()
    {
        return this.mExpandableListAdapter.hasStableIds();
    }

    public boolean isEmpty()
    {
        ExpandableListAdapter localExpandableListAdapter = getAdapter();
        if (localExpandableListAdapter != null);
        for (boolean bool = localExpandableListAdapter.isEmpty(); ; bool = true)
            return bool;
    }

    public boolean isEnabled(int paramInt)
    {
        PositionMetadata localPositionMetadata = getUnflattenedPos(paramInt);
        ExpandableListPosition localExpandableListPosition = localPositionMetadata.position;
        if (localExpandableListPosition.type == 1);
        for (boolean bool = this.mExpandableListAdapter.isChildSelectable(localExpandableListPosition.groupPos, localExpandableListPosition.childPos); ; bool = true)
        {
            localPositionMetadata.recycle();
            return bool;
        }
    }

    public boolean isGroupExpanded(int paramInt)
    {
        int i = -1 + this.mExpGroupMetadataList.size();
        if (i >= 0)
            if (((GroupMetadata)this.mExpGroupMetadataList.get(i)).gPos != paramInt);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    public void setExpandableListAdapter(ExpandableListAdapter paramExpandableListAdapter)
    {
        if (this.mExpandableListAdapter != null)
            this.mExpandableListAdapter.unregisterDataSetObserver(this.mDataSetObserver);
        this.mExpandableListAdapter = paramExpandableListAdapter;
        paramExpandableListAdapter.registerDataSetObserver(this.mDataSetObserver);
    }

    void setExpandedGroupMetadataList(ArrayList<GroupMetadata> paramArrayList)
    {
        if ((paramArrayList == null) || (this.mExpandableListAdapter == null));
        while (true)
        {
            return;
            int i = this.mExpandableListAdapter.getGroupCount();
            for (int j = -1 + paramArrayList.size(); ; j--)
            {
                if (j < 0)
                    break label55;
                if (((GroupMetadata)paramArrayList.get(j)).gPos >= i)
                    break;
            }
            label55: this.mExpGroupMetadataList = paramArrayList;
            refreshExpGroupMetadataList(true, false);
        }
    }

    public void setMaxExpGroupCount(int paramInt)
    {
        this.mMaxExpGroupCount = paramInt;
    }

    public static class PositionMetadata
    {
        private static final int MAX_POOL_SIZE = 5;
        private static ArrayList<PositionMetadata> sPool = new ArrayList(5);
        public int groupInsertIndex;
        public ExpandableListConnector.GroupMetadata groupMetadata;
        public ExpandableListPosition position;

        private static PositionMetadata getRecycledOrCreate()
        {
            synchronized (sPool)
            {
                if (sPool.size() > 0)
                {
                    localPositionMetadata = (PositionMetadata)sPool.remove(0);
                    localPositionMetadata.resetState();
                    return localPositionMetadata;
                }
                PositionMetadata localPositionMetadata = new PositionMetadata();
            }
        }

        static PositionMetadata obtain(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ExpandableListConnector.GroupMetadata paramGroupMetadata, int paramInt5)
        {
            PositionMetadata localPositionMetadata = getRecycledOrCreate();
            localPositionMetadata.position = ExpandableListPosition.obtain(paramInt2, paramInt3, paramInt4, paramInt1);
            localPositionMetadata.groupMetadata = paramGroupMetadata;
            localPositionMetadata.groupInsertIndex = paramInt5;
            return localPositionMetadata;
        }

        private void resetState()
        {
            if (this.position != null)
            {
                this.position.recycle();
                this.position = null;
            }
            this.groupMetadata = null;
            this.groupInsertIndex = 0;
        }

        public boolean isExpanded()
        {
            if (this.groupMetadata != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void recycle()
        {
            resetState();
            synchronized (sPool)
            {
                if (sPool.size() < 5)
                    sPool.add(this);
                return;
            }
        }
    }

    static class GroupMetadata
        implements Parcelable, Comparable<GroupMetadata>
    {
        public static final Parcelable.Creator<GroupMetadata> CREATOR = new Parcelable.Creator()
        {
            public ExpandableListConnector.GroupMetadata createFromParcel(Parcel paramAnonymousParcel)
            {
                return ExpandableListConnector.GroupMetadata.obtain(paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readLong());
            }

            public ExpandableListConnector.GroupMetadata[] newArray(int paramAnonymousInt)
            {
                return new ExpandableListConnector.GroupMetadata[paramAnonymousInt];
            }
        };
        static final int REFRESH = -1;
        int flPos;
        long gId;
        int gPos;
        int lastChildFlPos;

        static GroupMetadata obtain(int paramInt1, int paramInt2, int paramInt3, long paramLong)
        {
            GroupMetadata localGroupMetadata = new GroupMetadata();
            localGroupMetadata.flPos = paramInt1;
            localGroupMetadata.lastChildFlPos = paramInt2;
            localGroupMetadata.gPos = paramInt3;
            localGroupMetadata.gId = paramLong;
            return localGroupMetadata;
        }

        public int compareTo(GroupMetadata paramGroupMetadata)
        {
            if (paramGroupMetadata == null)
                throw new IllegalArgumentException();
            return this.gPos - paramGroupMetadata.gPos;
        }

        public int describeContents()
        {
            return 0;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.flPos);
            paramParcel.writeInt(this.lastChildFlPos);
            paramParcel.writeInt(this.gPos);
            paramParcel.writeLong(this.gId);
        }
    }

    protected class MyDataSetObserver extends DataSetObserver
    {
        protected MyDataSetObserver()
        {
        }

        public void onChanged()
        {
            ExpandableListConnector.this.refreshExpGroupMetadataList(true, true);
            ExpandableListConnector.this.notifyDataSetChanged();
        }

        public void onInvalidated()
        {
            ExpandableListConnector.this.refreshExpGroupMetadataList(true, true);
            ExpandableListConnector.this.notifyDataSetInvalidated();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ExpandableListConnector
 * JD-Core Version:        0.6.2
 */