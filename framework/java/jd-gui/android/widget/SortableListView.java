package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public class SortableListView extends ListView
{
    private static final int ANIMATION_DURATION = 200;
    private static final float SCROLL_BOUND = 0.25F;
    private static final int SCROLL_SPEED_MAX = 16;
    private static final int SNAPSHOT_ALPHA = 153;
    private static final String TAG = "SortableListView";
    private int mDraggingFrom = -1;
    private int mDraggingItemHeight;
    private int mDraggingItemWidth;
    private int mDraggingTo = -1;
    private int mDraggingY;
    private boolean mInterceptTouchForSorting;
    private int mItemUpperBound = -1;
    private int mOffsetYInDraggingItem;
    private OnOrderChangedListener mOnOrderChangedListener;
    private View.OnTouchListener mOnTouchListener;
    private int mScrollBound;
    private int mScrollLowerBound;
    private int mScrollUpperBound;
    private BitmapDrawable mSnapshot;
    private Drawable mSnapshotBackgroundForOverUpperBound;
    private Drawable mSnapshotShadow;
    private int mSnapshotShadowPaddingBottom;
    private int mSnapshotShadowPaddingTop;
    private int[] mTmpLocation = new int[2];

    public SortableListView(Context paramContext)
    {
        this(paramContext, null);
    }

    public SortableListView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842868);
    }

    public SortableListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        this.mSnapshotShadow = paramContext.getResources().getDrawable(100794632);
        this.mSnapshotShadow.setAlpha(153);
        Rect localRect = new Rect();
        this.mSnapshotShadow.getPadding(localRect);
        this.mSnapshotShadowPaddingTop = localRect.top;
        this.mSnapshotShadowPaddingBottom = localRect.bottom;
        this.mOnTouchListener = new View.OnTouchListener()
        {
            public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
            {
                if ((SortableListView.this.mOnOrderChangedListener != null) && ((0xFF & paramAnonymousMotionEvent.getAction()) == 0))
                {
                    int i = SortableListView.this.getHittenItemPosition(paramAnonymousMotionEvent);
                    if (i >= 0)
                    {
                        SortableListView.access$202(SortableListView.this, i);
                        SortableListView.access$302(SortableListView.this, i);
                        SortableListView.access$402(SortableListView.this, true);
                        View localView = SortableListView.this.getChildAt(i - SortableListView.this.getFirstVisiblePosition());
                        SortableListView.access$502(SortableListView.this, localView.getWidth());
                        SortableListView.access$602(SortableListView.this, localView.getHeight());
                        SortableListView.this.getLocationOnScreen(SortableListView.this.mTmpLocation);
                        SortableListView.access$802(SortableListView.this, (int)paramAnonymousMotionEvent.getRawY() - SortableListView.this.mTmpLocation[1]);
                        SortableListView.access$902(SortableListView.this, SortableListView.this.mDraggingY - localView.getTop());
                        Bitmap localBitmap = Bitmap.createBitmap(SortableListView.this.mDraggingItemWidth, SortableListView.this.mDraggingItemHeight, Bitmap.Config.ARGB_8888);
                        localView.draw(new Canvas(localBitmap));
                        SortableListView.access$1002(SortableListView.this, new BitmapDrawable(SortableListView.this.getResources(), localBitmap));
                        SortableListView.this.mSnapshot.setAlpha(153);
                        SortableListView.this.mSnapshot.setBounds(localView.getLeft(), 0, localView.getRight(), SortableListView.this.mDraggingItemHeight);
                        if (SortableListView.this.mSnapshotBackgroundForOverUpperBound != null)
                        {
                            SortableListView.this.mSnapshotBackgroundForOverUpperBound.setAlpha(153);
                            SortableListView.this.mSnapshotBackgroundForOverUpperBound.setBounds(localView.getLeft(), 0, localView.getRight(), SortableListView.this.mDraggingItemHeight);
                        }
                        SortableListView.this.mSnapshotShadow.setBounds(localView.getLeft(), -SortableListView.this.mSnapshotShadowPaddingTop, localView.getRight(), SortableListView.this.mDraggingItemHeight + SortableListView.this.mSnapshotShadowPaddingBottom);
                        localView.startAnimation(SortableListView.this.createAnimation(SortableListView.this.mDraggingItemWidth, SortableListView.this.mDraggingItemWidth, 0, 0));
                    }
                }
                return SortableListView.this.mInterceptTouchForSorting;
            }
        };
    }

    private Animation createAnimation(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        TranslateAnimation localTranslateAnimation = new TranslateAnimation(paramInt1, paramInt2, paramInt3, paramInt4);
        localTranslateAnimation.setDuration(200L);
        localTranslateAnimation.setFillAfter(true);
        return localTranslateAnimation;
    }

    private int getHittenItemPosition(MotionEvent paramMotionEvent)
    {
        float f1 = paramMotionEvent.getRawX();
        float f2 = paramMotionEvent.getRawY();
        int i = getFirstVisiblePosition();
        int j = getLastVisiblePosition();
        if (j >= i)
        {
            View localView = getChildAt(j - i);
            if (localView != null)
            {
                localView.getLocationOnScreen(this.mTmpLocation);
                if ((this.mTmpLocation[0] > f1) || (this.mTmpLocation[0] + localView.getWidth() < f1) || (this.mTmpLocation[1] > f2) || (this.mTmpLocation[1] + localView.getHeight() < f2));
            }
        }
        while (true)
        {
            return j;
            j--;
            break;
            j = -1;
        }
    }

    private void setViewAnimation(View paramView, Animation paramAnimation)
    {
        if (paramView == null);
        while (true)
        {
            return;
            if (paramAnimation != null)
                paramView.startAnimation(paramAnimation);
            else
                paramView.clearAnimation();
        }
    }

    private void setViewAnimationByPisition(int paramInt, Animation paramAnimation)
    {
        setViewAnimation(getChildAt(paramInt - getFirstVisiblePosition()), paramAnimation);
    }

    private void updateDraggingToPisition(int paramInt)
    {
        if ((paramInt == this.mDraggingTo) || (paramInt < 0));
        while (true)
        {
            return;
            Log.d("SortableListView", "sort item from " + this.mDraggingFrom + " To " + paramInt);
            if (this.mDraggingFrom < Math.max(this.mDraggingTo, paramInt))
                while ((this.mDraggingTo > paramInt) && (this.mDraggingTo > this.mDraggingFrom))
                {
                    Log.d("SortableListView", "item " + this.mDraggingTo + " set move down reverse animation");
                    int m = this.mDraggingTo;
                    this.mDraggingTo = (m - 1);
                    setViewAnimationByPisition(m, createAnimation(0, 0, -this.mDraggingItemHeight, 0));
                }
            if (this.mDraggingFrom > Math.min(this.mDraggingTo, paramInt))
                while ((this.mDraggingTo < paramInt) && (this.mDraggingTo < this.mDraggingFrom))
                {
                    Log.d("SortableListView", "item " + this.mDraggingTo + " set move up reverse animation");
                    int k = this.mDraggingTo;
                    this.mDraggingTo = (k + 1);
                    setViewAnimationByPisition(k, createAnimation(0, 0, this.mDraggingItemHeight, 0));
                }
            if (this.mDraggingFrom < Math.max(this.mDraggingTo, paramInt))
                while (this.mDraggingTo < paramInt)
                {
                    int j = 1 + this.mDraggingTo;
                    this.mDraggingTo = j;
                    setViewAnimationByPisition(j, createAnimation(0, 0, 0, -this.mDraggingItemHeight));
                    Log.d("SortableListView", "item " + this.mDraggingTo + " set move up animation");
                }
            if (this.mDraggingFrom > Math.min(this.mDraggingTo, paramInt))
                while (this.mDraggingTo > paramInt)
                {
                    int i = -1 + this.mDraggingTo;
                    this.mDraggingTo = i;
                    setViewAnimationByPisition(i, createAnimation(0, 0, 0, this.mDraggingItemHeight));
                    Log.d("SortableListView", "item " + this.mDraggingTo + " set move down animation");
                }
        }
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        super.dispatchDraw(paramCanvas);
        if (this.mDraggingFrom >= 0)
        {
            int i = this.mDraggingY - this.mOffsetYInDraggingItem;
            int j = getHeaderViewsCount();
            if ((j < getFirstVisiblePosition()) || (j > getLastVisiblePosition()))
                j = getFirstVisiblePosition();
            int k = Math.max(i, getChildAt(j - getFirstVisiblePosition()).getTop());
            int m = -1 + getCount() - getFooterViewsCount();
            if ((m < getFirstVisiblePosition()) || (m > getLastVisiblePosition()))
                m = getLastVisiblePosition();
            int n = Math.min(k, getChildAt(m - getFirstVisiblePosition()).getBottom() - this.mDraggingItemHeight);
            paramCanvas.translate(0.0F, n);
            this.mSnapshotShadow.draw(paramCanvas);
            this.mSnapshot.draw(paramCanvas);
            if ((this.mSnapshotBackgroundForOverUpperBound != null) && (this.mDraggingTo < this.mItemUpperBound))
                this.mSnapshotBackgroundForOverUpperBound.draw(paramCanvas);
            paramCanvas.translate(0.0F, -n);
        }
    }

    public View.OnTouchListener getListenerForStartingSort()
    {
        return this.mOnTouchListener;
    }

    View obtainView(int paramInt, boolean[] paramArrayOfBoolean)
    {
        View localView = super.obtainView(paramInt, paramArrayOfBoolean);
        Animation localAnimation = null;
        if (this.mDraggingFrom == paramInt)
        {
            localAnimation = createAnimation(this.mDraggingItemWidth, this.mDraggingItemWidth, 0, 0);
            Log.d("SortableListView", "item " + paramInt + " set move out animation");
        }
        while (true)
        {
            setViewAnimation(localView, localAnimation);
            return localView;
            if ((this.mDraggingFrom < paramInt) && (paramInt <= this.mDraggingTo))
            {
                localAnimation = createAnimation(0, 0, 0, -this.mDraggingItemHeight);
                Log.d("SortableListView", "item " + paramInt + " set move up animation");
            }
            else if ((this.mDraggingFrom > paramInt) && (paramInt >= this.mDraggingTo))
            {
                localAnimation = createAnimation(0, 0, 0, this.mDraggingItemHeight);
                Log.d("SortableListView", "item " + paramInt + " set move down animation");
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        if (this.mInterceptTouchForSorting)
        {
            requestDisallowInterceptTouchEvent(bool);
            onTouchEvent(paramMotionEvent);
        }
        while (true)
        {
            return bool;
            bool = super.onInterceptTouchEvent(paramMotionEvent);
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mScrollBound = Math.max(1, (int)(0.25F * paramInt2));
        this.mScrollUpperBound = this.mScrollBound;
        this.mScrollLowerBound = (paramInt2 - this.mScrollBound);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if (!this.mInterceptTouchForSorting)
        {
            bool = super.onTouchEvent(paramMotionEvent);
            return bool;
        }
        switch (0xFF & paramMotionEvent.getAction())
        {
        case 4:
        default:
        case 2:
            int i;
            do
            {
                bool = true;
                break;
                i = (int)paramMotionEvent.getY();
            }
            while ((!this.mInterceptTouchForSorting) && (i == this.mDraggingY));
            int j = getHittenItemPosition(paramMotionEvent);
            if ((j < getHeaderViewsCount()) || (j > getCount() - getFooterViewsCount()))
                j = this.mDraggingTo;
            updateDraggingToPisition(j);
            this.mDraggingY = i;
            invalidate();
            int k = 0;
            if (i > this.mScrollLowerBound)
                k = 16 * (this.mScrollLowerBound - i) / this.mScrollBound;
            while (k != 0)
            {
                View localView = getChildAt(j - getFirstVisiblePosition());
                if (localView == null)
                    break;
                setSelectionFromTop(j, k + localView.getTop());
                break;
                if (i < this.mScrollUpperBound)
                    k = 16 * (this.mScrollUpperBound - i) / this.mScrollBound;
            }
        case 1:
        case 3:
        case 5:
        }
        if (this.mDraggingFrom >= 0)
        {
            if ((this.mOnOrderChangedListener == null) || (this.mDraggingFrom == this.mDraggingTo) || (this.mDraggingTo < 0))
                break label310;
            this.mOnOrderChangedListener.OnOrderChanged(this.mDraggingFrom - getHeaderViewsCount(), this.mDraggingTo - getHeaderViewsCount());
        }
        while (true)
        {
            this.mInterceptTouchForSorting = false;
            this.mDraggingFrom = -1;
            this.mDraggingTo = -1;
            invalidate();
            break;
            label310: setViewAnimationByPisition(this.mDraggingFrom, null);
        }
    }

    public void setItemUpperBound(int paramInt, Drawable paramDrawable)
    {
        this.mItemUpperBound = paramInt;
        this.mSnapshotBackgroundForOverUpperBound = paramDrawable;
    }

    public void setOnOrderChangedListener(OnOrderChangedListener paramOnOrderChangedListener)
    {
        this.mOnOrderChangedListener = paramOnOrderChangedListener;
    }

    public static abstract interface OnOrderChangedListener
    {
        public abstract void OnOrderChanged(int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SortableListView
 * JD-Core Version:        0.6.2
 */