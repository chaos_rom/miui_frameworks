package android.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import com.android.internal.widget.IRemoteViewsAdapterConnection.Stub;
import com.android.internal.widget.IRemoteViewsFactory;
import com.android.internal.widget.IRemoteViewsFactory.Stub;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class RemoteViewsAdapter extends BaseAdapter
    implements Handler.Callback
{
    private static final int REMOTE_VIEWS_CACHE_DURATION = 5000;
    private static final String TAG = "RemoteViewsAdapter";
    private static Handler sCacheRemovalQueue;
    private static HandlerThread sCacheRemovalThread;
    private static final HashMap<Pair<Intent.FilterComparison, Integer>, FixedSizeRemoteViewsCache> sCachedRemoteViewsCaches = new HashMap();
    private static final int sDefaultCacheSize = 40;
    private static final int sDefaultLoadingViewHeight = 50;
    private static final int sDefaultMessageType = 0;
    private static final HashMap<Pair<Intent.FilterComparison, Integer>, Runnable> sRemoteViewsCacheRemoveRunnables = new HashMap();
    private static final int sUnbindServiceDelay = 5000;
    private static final int sUnbindServiceMessageType = 1;
    private final int mAppWidgetId;
    private FixedSizeRemoteViewsCache mCache;
    private WeakReference<RemoteAdapterConnectionCallback> mCallback;
    private final Context mContext;
    private boolean mDataReady = false;
    private final Intent mIntent;
    private LayoutInflater mLayoutInflater;
    private Handler mMainQueue;
    private boolean mNotifyDataSetChangedAfterOnServiceConnected = false;
    private RemoteViewsFrameLayoutRefSet mRequestedViews;
    private RemoteViewsAdapterServiceConnection mServiceConnection;
    private int mVisibleWindowLowerBound;
    private int mVisibleWindowUpperBound;
    private Handler mWorkerQueue;
    private HandlerThread mWorkerThread;

    public RemoteViewsAdapter(Context paramContext, Intent paramIntent, RemoteAdapterConnectionCallback paramRemoteAdapterConnectionCallback)
    {
        this.mContext = paramContext;
        this.mIntent = paramIntent;
        this.mAppWidgetId = paramIntent.getIntExtra("remoteAdapterAppWidgetId", -1);
        this.mLayoutInflater = LayoutInflater.from(paramContext);
        if (this.mIntent == null)
            throw new IllegalArgumentException("Non-null Intent must be specified.");
        this.mRequestedViews = new RemoteViewsFrameLayoutRefSet();
        if (paramIntent.hasExtra("remoteAdapterAppWidgetId"))
            paramIntent.removeExtra("remoteAdapterAppWidgetId");
        this.mWorkerThread = new HandlerThread("RemoteViewsCache-loader");
        this.mWorkerThread.start();
        this.mWorkerQueue = new Handler(this.mWorkerThread.getLooper());
        this.mMainQueue = new Handler(Looper.myLooper(), this);
        if (sCacheRemovalThread == null)
        {
            sCacheRemovalThread = new HandlerThread("RemoteViewsAdapter-cachePruner");
            sCacheRemovalThread.start();
            sCacheRemovalQueue = new Handler(sCacheRemovalThread.getLooper());
        }
        this.mCallback = new WeakReference(paramRemoteAdapterConnectionCallback);
        this.mServiceConnection = new RemoteViewsAdapterServiceConnection(this);
        Pair localPair = new Pair(new Intent.FilterComparison(this.mIntent), Integer.valueOf(this.mAppWidgetId));
        while (true)
        {
            synchronized (sCachedRemoteViewsCaches)
            {
                if (sCachedRemoteViewsCaches.containsKey(localPair))
                {
                    this.mCache = ((FixedSizeRemoteViewsCache)sCachedRemoteViewsCaches.get(localPair));
                    synchronized (this.mCache.mMetaData)
                    {
                        if (this.mCache.mMetaData.count > 0)
                            this.mDataReady = true;
                        if (!this.mDataReady)
                            requestBindService();
                        return;
                    }
                }
            }
            this.mCache = new FixedSizeRemoteViewsCache(40);
        }
    }

    private void enqueueDeferredUnbindServiceMessage()
    {
        this.mMainQueue.removeMessages(1);
        this.mMainQueue.sendEmptyMessageDelayed(1, 5000L);
    }

    private int getConvertViewTypeId(View paramView)
    {
        int i = -1;
        if (paramView != null)
        {
            Object localObject = paramView.getTag(16908858);
            if (localObject != null)
                i = ((Integer)localObject).intValue();
        }
        return i;
    }

    private ArrayList<Integer> getVisibleWindow(int paramInt1, int paramInt2, int paramInt3)
    {
        ArrayList localArrayList = new ArrayList();
        if (((paramInt1 == 0) && (paramInt2 == 0)) || (paramInt1 < 0) || (paramInt2 < 0));
        while (true)
        {
            return localArrayList;
            if (paramInt1 <= paramInt2)
            {
                for (int k = paramInt1; k <= paramInt2; k++)
                    localArrayList.add(Integer.valueOf(k));
            }
            else
            {
                for (int i = paramInt1; i < paramInt3; i++)
                    localArrayList.add(Integer.valueOf(i));
                for (int j = 0; j <= paramInt2; j++)
                    localArrayList.add(Integer.valueOf(j));
            }
        }
    }

    private void loadNextIndexInBackground()
    {
        this.mWorkerQueue.post(new Runnable()
        {
            public void run()
            {
                if (RemoteViewsAdapter.this.mServiceConnection.isConnected());
                while (true)
                {
                    synchronized (RemoteViewsAdapter.this.mCache)
                    {
                        int i = RemoteViewsAdapter.this.mCache.getNextIndexToLoad()[0];
                        if (i > -1)
                        {
                            RemoteViewsAdapter.this.updateRemoteViews(i, true);
                            RemoteViewsAdapter.this.loadNextIndexInBackground();
                            return;
                        }
                    }
                    RemoteViewsAdapter.this.enqueueDeferredUnbindServiceMessage();
                }
            }
        });
    }

    // ERROR //
    private void onNotifyDataSetChanged()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 193	android/widget/RemoteViewsAdapter:mServiceConnection	Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;
        //     4: invokevirtual 314	android/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection:getRemoteViewsFactory	()Lcom/android/internal/widget/IRemoteViewsFactory;
        //     7: astore_1
        //     8: aload_1
        //     9: invokeinterface 319 1 0
        //     14: aload_0
        //     15: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     18: astore 6
        //     20: aload 6
        //     22: monitorenter
        //     23: aload_0
        //     24: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     27: invokevirtual 322	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:reset	()V
        //     30: aload 6
        //     32: monitorexit
        //     33: aload_0
        //     34: invokespecial 262	android/widget/RemoteViewsAdapter:updateTemporaryMetaData	()V
        //     37: aload_0
        //     38: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     41: invokevirtual 326	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getTemporaryMetaData	()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
        //     44: astore 8
        //     46: aload 8
        //     48: monitorenter
        //     49: aload_0
        //     50: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     53: invokevirtual 326	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getTemporaryMetaData	()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
        //     56: getfield 226	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:count	I
        //     59: istore 10
        //     61: aload_0
        //     62: aload_0
        //     63: getfield 328	android/widget/RemoteViewsAdapter:mVisibleWindowLowerBound	I
        //     66: aload_0
        //     67: getfield 330	android/widget/RemoteViewsAdapter:mVisibleWindowUpperBound	I
        //     70: iload 10
        //     72: invokespecial 332	android/widget/RemoteViewsAdapter:getVisibleWindow	(III)Ljava/util/ArrayList;
        //     75: astore 11
        //     77: aload 8
        //     79: monitorexit
        //     80: aload 11
        //     82: invokevirtual 336	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     85: astore 12
        //     87: aload 12
        //     89: invokeinterface 341 1 0
        //     94: ifeq +117 -> 211
        //     97: aload 12
        //     99: invokeinterface 345 1 0
        //     104: checkcast 202	java/lang/Integer
        //     107: invokevirtual 293	java/lang/Integer:intValue	()I
        //     110: istore 14
        //     112: iload 14
        //     114: iload 10
        //     116: if_icmpge -29 -> 87
        //     119: aload_0
        //     120: iload 14
        //     122: iconst_0
        //     123: invokespecial 250	android/widget/RemoteViewsAdapter:updateRemoteViews	(IZ)V
        //     126: goto -39 -> 87
        //     129: astore 4
        //     131: ldc 46
        //     133: new 347	java/lang/StringBuilder
        //     136: dup
        //     137: invokespecial 348	java/lang/StringBuilder:<init>	()V
        //     140: ldc_w 350
        //     143: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     146: aload 4
        //     148: invokevirtual 358	android/os/RemoteException:getMessage	()Ljava/lang/String;
        //     151: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     154: invokevirtual 361	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     157: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     160: pop
        //     161: return
        //     162: astore_2
        //     163: ldc 46
        //     165: new 347	java/lang/StringBuilder
        //     168: dup
        //     169: invokespecial 348	java/lang/StringBuilder:<init>	()V
        //     172: ldc_w 350
        //     175: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     178: aload_2
        //     179: invokevirtual 368	java/lang/RuntimeException:getMessage	()Ljava/lang/String;
        //     182: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     185: invokevirtual 361	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     188: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     191: pop
        //     192: goto -31 -> 161
        //     195: astore 7
        //     197: aload 6
        //     199: monitorexit
        //     200: aload 7
        //     202: athrow
        //     203: astore 9
        //     205: aload 8
        //     207: monitorexit
        //     208: aload 9
        //     210: athrow
        //     211: aload_0
        //     212: getfield 177	android/widget/RemoteViewsAdapter:mMainQueue	Landroid/os/Handler;
        //     215: new 16	android/widget/RemoteViewsAdapter$5
        //     218: dup
        //     219: aload_0
        //     220: invokespecial 369	android/widget/RemoteViewsAdapter$5:<init>	(Landroid/widget/RemoteViewsAdapter;)V
        //     223: invokevirtual 306	android/os/Handler:post	(Ljava/lang/Runnable;)Z
        //     226: pop
        //     227: aload_0
        //     228: iconst_0
        //     229: putfield 103	android/widget/RemoteViewsAdapter:mNotifyDataSetChangedAfterOnServiceConnected	Z
        //     232: goto -71 -> 161
        //
        // Exception table:
        //     from	to	target	type
        //     8	14	129	android/os/RemoteException
        //     8	14	162	java/lang/RuntimeException
        //     23	33	195	finally
        //     197	200	195	finally
        //     49	80	203	finally
        //     205	208	203	finally
    }

    private void processException(String paramString, Exception paramException)
    {
        Log.e("RemoteViewsAdapter", "Error in " + paramString + ": " + paramException.getMessage());
        synchronized (this.mCache.getMetaData())
        {
            ???.reset();
        }
        synchronized (this.mCache)
        {
            this.mCache.reset();
            this.mMainQueue.post(new Runnable()
            {
                public void run()
                {
                    RemoteViewsAdapter.this.superNotifyDataSetChanged();
                }
            });
            return;
            localObject1 = finally;
            throw localObject1;
        }
    }

    private boolean requestBindService()
    {
        if (!this.mServiceConnection.isConnected())
            this.mServiceConnection.bind(this.mContext, this.mAppWidgetId, this.mIntent);
        this.mMainQueue.removeMessages(1);
        return this.mServiceConnection.isConnected();
    }

    // ERROR //
    private void updateRemoteViews(final int paramInt, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 193	android/widget/RemoteViewsAdapter:mServiceConnection	Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;
        //     4: invokevirtual 314	android/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection:getRemoteViewsFactory	()Lcom/android/internal/widget/IRemoteViewsFactory;
        //     7: astore_3
        //     8: aload_3
        //     9: iload_1
        //     10: invokeinterface 394 2 0
        //     15: astore 8
        //     17: aload_3
        //     18: iload_1
        //     19: invokeinterface 398 2 0
        //     24: lstore 9
        //     26: aload 8
        //     28: ifnonnull +138 -> 166
        //     31: ldc 46
        //     33: new 347	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 348	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 400
        //     43: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: iload_1
        //     47: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     50: ldc_w 405
        //     53: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: ldc_w 407
        //     59: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: ldc_w 409
        //     65: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: invokevirtual 361	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     71: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     74: pop
        //     75: return
        //     76: astore 6
        //     78: ldc 46
        //     80: new 347	java/lang/StringBuilder
        //     83: dup
        //     84: invokespecial 348	java/lang/StringBuilder:<init>	()V
        //     87: ldc_w 400
        //     90: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     93: iload_1
        //     94: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     97: ldc_w 405
        //     100: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     103: aload 6
        //     105: invokevirtual 358	android/os/RemoteException:getMessage	()Ljava/lang/String;
        //     108: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     111: invokevirtual 361	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     114: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     117: pop
        //     118: goto -43 -> 75
        //     121: astore 4
        //     123: ldc 46
        //     125: new 347	java/lang/StringBuilder
        //     128: dup
        //     129: invokespecial 348	java/lang/StringBuilder:<init>	()V
        //     132: ldc_w 400
        //     135: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: iload_1
        //     139: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     142: ldc_w 405
        //     145: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     148: aload 4
        //     150: invokevirtual 368	java/lang/RuntimeException:getMessage	()Ljava/lang/String;
        //     153: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: invokevirtual 361	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     159: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     162: pop
        //     163: goto -88 -> 75
        //     166: aload 8
        //     168: invokevirtual 414	android/widget/RemoteViews:getLayoutId	()I
        //     171: istore 11
        //     173: aload_0
        //     174: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     177: invokevirtual 381	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getMetaData	()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
        //     180: astore 12
        //     182: aload 12
        //     184: monitorenter
        //     185: aload 12
        //     187: iload 11
        //     189: invokevirtual 418	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:isViewTypeInRange	(I)Z
        //     192: istore 14
        //     194: aload_0
        //     195: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     198: invokestatic 223	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:access$1100	(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
        //     201: getfield 226	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:count	I
        //     204: istore 15
        //     206: aload 12
        //     208: monitorexit
        //     209: aload_0
        //     210: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     213: astore 16
        //     215: aload 16
        //     217: monitorenter
        //     218: iload 14
        //     220: ifeq +78 -> 298
        //     223: aload_0
        //     224: aload_0
        //     225: getfield 328	android/widget/RemoteViewsAdapter:mVisibleWindowLowerBound	I
        //     228: aload_0
        //     229: getfield 330	android/widget/RemoteViewsAdapter:mVisibleWindowUpperBound	I
        //     232: iload 15
        //     234: invokespecial 332	android/widget/RemoteViewsAdapter:getVisibleWindow	(III)Ljava/util/ArrayList;
        //     237: astore 19
        //     239: aload_0
        //     240: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     243: iload_1
        //     244: aload 8
        //     246: lload 9
        //     248: aload 19
        //     250: invokevirtual 422	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:insert	(ILandroid/widget/RemoteViews;JLjava/util/ArrayList;)V
        //     253: iload_2
        //     254: ifeq +22 -> 276
        //     257: aload_0
        //     258: getfield 177	android/widget/RemoteViewsAdapter:mMainQueue	Landroid/os/Handler;
        //     261: new 14	android/widget/RemoteViewsAdapter$4
        //     264: dup
        //     265: aload_0
        //     266: iload_1
        //     267: aload 8
        //     269: invokespecial 425	android/widget/RemoteViewsAdapter$4:<init>	(Landroid/widget/RemoteViewsAdapter;ILandroid/widget/RemoteViews;)V
        //     272: invokevirtual 306	android/os/Handler:post	(Ljava/lang/Runnable;)Z
        //     275: pop
        //     276: aload 16
        //     278: monitorexit
        //     279: goto -204 -> 75
        //     282: astore 18
        //     284: aload 16
        //     286: monitorexit
        //     287: aload 18
        //     289: athrow
        //     290: astore 13
        //     292: aload 12
        //     294: monitorexit
        //     295: aload 13
        //     297: athrow
        //     298: ldc 46
        //     300: ldc_w 427
        //     303: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     306: pop
        //     307: goto -31 -> 276
        //
        // Exception table:
        //     from	to	target	type
        //     8	26	76	android/os/RemoteException
        //     8	26	121	java/lang/RuntimeException
        //     223	287	282	finally
        //     298	307	282	finally
        //     185	209	290	finally
        //     292	295	290	finally
    }

    private void updateTemporaryMetaData()
    {
        IRemoteViewsFactory localIRemoteViewsFactory = this.mServiceConnection.getRemoteViewsFactory();
        try
        {
            boolean bool = localIRemoteViewsFactory.hasStableIds();
            int i = localIRemoteViewsFactory.getViewTypeCount();
            int j = localIRemoteViewsFactory.getCount();
            RemoteViews localRemoteViews1 = localIRemoteViewsFactory.getLoadingView();
            RemoteViews localRemoteViews2 = null;
            if ((j > 0) && (localRemoteViews1 == null))
                localRemoteViews2 = localIRemoteViewsFactory.getViewAt(0);
            synchronized (this.mCache.getTemporaryMetaData())
            {
                ???.hasStableIds = bool;
                ???.viewTypeCount = (i + 1);
                ???.count = j;
                ???.setLoadingViewTemplates(localRemoteViews1, localRemoteViews2);
            }
        }
        catch (RemoteException localRemoteException)
        {
            processException("updateMetaData", localRemoteException);
        }
        catch (RuntimeException localRuntimeException)
        {
            processException("updateMetaData", localRuntimeException);
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (this.mWorkerThread != null)
                this.mWorkerThread.quit();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getCount()
    {
        synchronized (this.mCache.getMetaData())
        {
            int i = ???.count;
            return i;
        }
    }

    public Object getItem(int paramInt)
    {
        return null;
    }

    public long getItemId(int paramInt)
    {
        long l;
        synchronized (this.mCache)
        {
            if (this.mCache.containsMetaDataAt(paramInt))
                l = this.mCache.getMetaDataAt(paramInt).itemId;
            else
                l = 0L;
        }
        return l;
    }

    public int getItemViewType(int paramInt)
    {
        int j;
        synchronized (this.mCache)
        {
            if (this.mCache.containsMetaDataAt(paramInt))
                j = this.mCache.getMetaDataAt(paramInt).typeId;
        }
        synchronized (this.mCache.getMetaData())
        {
            int i = ???.getMappedViewType(j);
            while (true)
            {
                return i;
                i = 0;
            }
            localObject1 = finally;
            throw localObject1;
        }
    }

    public Intent getRemoteViewsServiceIntent()
    {
        return this.mIntent;
    }

    // ERROR //
    public View getView(int paramInt, View paramView, android.view.ViewGroup paramViewGroup)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     13: iload_1
        //     14: invokevirtual 490	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:containsRemoteViewAt	(I)Z
        //     17: istore 6
        //     19: aload_0
        //     20: getfield 193	android/widget/RemoteViewsAdapter:mServiceConnection	Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;
        //     23: invokevirtual 386	android/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection:isConnected	()Z
        //     26: istore 7
        //     28: iconst_0
        //     29: istore 8
        //     31: iload 6
        //     33: ifne +126 -> 159
        //     36: iload 7
        //     38: ifne +121 -> 159
        //     41: aload_0
        //     42: invokespecial 230	android/widget/RemoteViewsAdapter:requestBindService	()Z
        //     45: pop
        //     46: iload 6
        //     48: ifeq +330 -> 378
        //     51: aconst_null
        //     52: astore 13
        //     54: iconst_0
        //     55: istore 14
        //     57: aload_2
        //     58: instanceof 32
        //     61: ifeq +399 -> 460
        //     64: aload_2
        //     65: checkcast 32	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout
        //     68: astore 28
        //     70: aload 28
        //     72: iconst_0
        //     73: invokevirtual 494	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:getChildAt	(I)Landroid/view/View;
        //     76: astore 13
        //     78: aload_0
        //     79: aload 13
        //     81: invokespecial 496	android/widget/RemoteViewsAdapter:getConvertViewTypeId	(Landroid/view/View;)I
        //     84: istore 14
        //     86: aload 28
        //     88: astore 15
        //     90: aload_3
        //     91: invokevirtual 502	android/view/ViewGroup:getContext	()Landroid/content/Context;
        //     94: astore 16
        //     96: aload_0
        //     97: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     100: iload_1
        //     101: invokevirtual 505	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getRemoteViewsAt	(I)Landroid/widget/RemoteViews;
        //     104: astore 17
        //     106: aload_0
        //     107: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     110: iload_1
        //     111: invokevirtual 471	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getMetaDataAt	(I)Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
        //     114: getfield 480	android/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData:typeId	I
        //     117: istore 18
        //     119: aload 15
        //     121: ifnull +117 -> 238
        //     124: iload 14
        //     126: iload 18
        //     128: if_icmpne +48 -> 176
        //     131: aload 17
        //     133: aload 16
        //     135: aload 13
        //     137: invokevirtual 509	android/widget/RemoteViews:reapply	(Landroid/content/Context;Landroid/view/View;)V
        //     140: iload 8
        //     142: ifeq +7 -> 149
        //     145: aload_0
        //     146: invokespecial 254	android/widget/RemoteViewsAdapter:loadNextIndexInBackground	()V
        //     149: aload 4
        //     151: monitorexit
        //     152: aload 15
        //     154: astore 12
        //     156: goto +310 -> 466
        //     159: aload_0
        //     160: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     163: iload_1
        //     164: invokevirtual 512	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:queuePositionsToBePreloadedFromRequestedPosition	(I)Z
        //     167: istore 9
        //     169: iload 9
        //     171: istore 8
        //     173: goto -127 -> 46
        //     176: aload 15
        //     178: invokevirtual 515	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:removeAllViews	()V
        //     181: aload 15
        //     183: astore 19
        //     185: aload 17
        //     187: aload 16
        //     189: aload_3
        //     190: invokevirtual 519	android/widget/RemoteViews:apply	(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
        //     193: astore 25
        //     195: aload 25
        //     197: ldc_w 283
        //     200: new 202	java/lang/Integer
        //     203: dup
        //     204: iload 18
        //     206: invokespecial 520	java/lang/Integer:<init>	(I)V
        //     209: invokevirtual 524	android/view/View:setTagInternal	(ILjava/lang/Object;)V
        //     212: aload 19
        //     214: aload 25
        //     216: invokevirtual 528	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:addView	(Landroid/view/View;)V
        //     219: iload 8
        //     221: ifeq +7 -> 228
        //     224: aload_0
        //     225: invokespecial 254	android/widget/RemoteViewsAdapter:loadNextIndexInBackground	()V
        //     228: aload 4
        //     230: monitorexit
        //     231: aload 19
        //     233: astore 12
        //     235: goto +231 -> 466
        //     238: new 32	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout
        //     241: dup
        //     242: aload 16
        //     244: invokespecial 531	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:<init>	(Landroid/content/Context;)V
        //     247: astore 19
        //     249: goto -64 -> 185
        //     252: astore 20
        //     254: aload 15
        //     256: pop
        //     257: ldc 46
        //     259: new 347	java/lang/StringBuilder
        //     262: dup
        //     263: invokespecial 348	java/lang/StringBuilder:<init>	()V
        //     266: ldc_w 533
        //     269: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     272: iload_1
        //     273: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     276: ldc_w 535
        //     279: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     282: ldc_w 537
        //     285: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     288: aload 20
        //     290: invokevirtual 540	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     293: invokevirtual 361	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     296: invokestatic 543	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     299: pop
        //     300: aload_0
        //     301: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     304: invokevirtual 381	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getMetaData	()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
        //     307: astore 23
        //     309: aload 23
        //     311: monitorenter
        //     312: aload 23
        //     314: iload_1
        //     315: aload_2
        //     316: aload_3
        //     317: aload_0
        //     318: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     321: aload_0
        //     322: getfield 127	android/widget/RemoteViewsAdapter:mLayoutInflater	Landroid/view/LayoutInflater;
        //     325: invokestatic 547	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:access$1800	(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
        //     328: astore 12
        //     330: aload 23
        //     332: monitorexit
        //     333: iload 8
        //     335: ifeq +7 -> 342
        //     338: aload_0
        //     339: invokespecial 254	android/widget/RemoteViewsAdapter:loadNextIndexInBackground	()V
        //     342: aload 4
        //     344: monitorexit
        //     345: goto +121 -> 466
        //     348: astore 5
        //     350: aload 4
        //     352: monitorexit
        //     353: aload 5
        //     355: athrow
        //     356: astore 24
        //     358: aload 23
        //     360: monitorexit
        //     361: aload 24
        //     363: athrow
        //     364: astore 21
        //     366: iload 8
        //     368: ifeq +7 -> 375
        //     371: aload_0
        //     372: invokespecial 254	android/widget/RemoteViewsAdapter:loadNextIndexInBackground	()V
        //     375: aload 21
        //     377: athrow
        //     378: aload_0
        //     379: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     382: invokevirtual 381	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:getMetaData	()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
        //     385: astore 10
        //     387: aload 10
        //     389: monitorenter
        //     390: aload 10
        //     392: iload_1
        //     393: aload_2
        //     394: aload_3
        //     395: aload_0
        //     396: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     399: aload_0
        //     400: getfield 127	android/widget/RemoteViewsAdapter:mLayoutInflater	Landroid/view/LayoutInflater;
        //     403: invokestatic 547	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:access$1800	(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
        //     406: astore 12
        //     408: aload 10
        //     410: monitorexit
        //     411: aload_0
        //     412: getfield 139	android/widget/RemoteViewsAdapter:mRequestedViews	Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;
        //     415: iload_1
        //     416: aload 12
        //     418: invokevirtual 550	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet:add	(ILandroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;)V
        //     421: aload_0
        //     422: getfield 219	android/widget/RemoteViewsAdapter:mCache	Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
        //     425: iload_1
        //     426: invokevirtual 553	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:queueRequestedPositionToLoad	(I)V
        //     429: aload_0
        //     430: invokespecial 254	android/widget/RemoteViewsAdapter:loadNextIndexInBackground	()V
        //     433: aload 4
        //     435: monitorexit
        //     436: goto +30 -> 466
        //     439: astore 11
        //     441: aload 10
        //     443: monitorexit
        //     444: aload 11
        //     446: athrow
        //     447: astore 21
        //     449: aload 15
        //     451: pop
        //     452: goto -86 -> 366
        //     455: astore 20
        //     457: goto -200 -> 257
        //     460: aconst_null
        //     461: astore 15
        //     463: goto -373 -> 90
        //     466: aload 12
        //     468: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     131	140	252	java/lang/Exception
        //     176	181	252	java/lang/Exception
        //     238	249	252	java/lang/Exception
        //     9	119	348	finally
        //     145	169	348	finally
        //     224	231	348	finally
        //     338	353	348	finally
        //     371	390	348	finally
        //     411	436	348	finally
        //     444	447	348	finally
        //     312	333	356	finally
        //     358	361	356	finally
        //     185	219	364	finally
        //     257	312	364	finally
        //     361	364	364	finally
        //     390	411	439	finally
        //     441	444	439	finally
        //     131	140	447	finally
        //     176	181	447	finally
        //     238	249	447	finally
        //     185	219	455	java/lang/Exception
    }

    public int getViewTypeCount()
    {
        synchronized (this.mCache.getMetaData())
        {
            int i = ???.viewTypeCount;
            return i;
        }
    }

    public boolean handleMessage(Message paramMessage)
    {
        boolean bool = false;
        switch (paramMessage.what)
        {
        default:
        case 1:
        }
        while (true)
        {
            return bool;
            if (this.mServiceConnection.isConnected())
                this.mServiceConnection.unbind(this.mContext, this.mAppWidgetId, this.mIntent);
            bool = true;
        }
    }

    public boolean hasStableIds()
    {
        synchronized (this.mCache.getMetaData())
        {
            boolean bool = ???.hasStableIds;
            return bool;
        }
    }

    public boolean isDataReady()
    {
        return this.mDataReady;
    }

    public boolean isEmpty()
    {
        if (getCount() <= 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void notifyDataSetChanged()
    {
        this.mMainQueue.removeMessages(1);
        if (!this.mServiceConnection.isConnected())
            if (!this.mNotifyDataSetChangedAfterOnServiceConnected);
        while (true)
        {
            return;
            this.mNotifyDataSetChangedAfterOnServiceConnected = true;
            requestBindService();
            continue;
            this.mWorkerQueue.post(new Runnable()
            {
                public void run()
                {
                    RemoteViewsAdapter.this.onNotifyDataSetChanged();
                }
            });
        }
    }

    public void saveRemoteViewsCache()
    {
        final Pair localPair = new Pair(new Intent.FilterComparison(this.mIntent), Integer.valueOf(this.mAppWidgetId));
        int i;
        synchronized (sCachedRemoteViewsCaches)
        {
            if (sRemoteViewsCacheRemoveRunnables.containsKey(localPair))
            {
                sCacheRemovalQueue.removeCallbacks((Runnable)sRemoteViewsCacheRemoveRunnables.get(localPair));
                sRemoteViewsCacheRemoveRunnables.remove(localPair);
            }
            synchronized (this.mCache.mMetaData)
            {
                i = this.mCache.mMetaData.count;
            }
        }
        synchronized (this.mCache)
        {
            int j = this.mCache.mIndexRemoteViews.size();
            if ((i > 0) && (j > 0))
                sCachedRemoteViewsCaches.put(localPair, this.mCache);
            Runnable local1 = new Runnable()
            {
                public void run()
                {
                    synchronized (RemoteViewsAdapter.sCachedRemoteViewsCaches)
                    {
                        if (RemoteViewsAdapter.sCachedRemoteViewsCaches.containsKey(localPair))
                            RemoteViewsAdapter.sCachedRemoteViewsCaches.remove(localPair);
                        if (RemoteViewsAdapter.sRemoteViewsCacheRemoveRunnables.containsKey(localPair))
                            RemoteViewsAdapter.sRemoteViewsCacheRemoveRunnables.remove(localPair);
                        return;
                    }
                }
            };
            sRemoteViewsCacheRemoveRunnables.put(localPair, local1);
            sCacheRemovalQueue.postDelayed(local1, 5000L);
            return;
            localObject2 = finally;
            throw localObject2;
            localObject1 = finally;
            throw localObject1;
        }
    }

    public void setVisibleRangeHint(int paramInt1, int paramInt2)
    {
        this.mVisibleWindowLowerBound = paramInt1;
        this.mVisibleWindowUpperBound = paramInt2;
    }

    void superNotifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }

    private static class FixedSizeRemoteViewsCache
    {
        private static final String TAG = "FixedSizeRemoteViewsCache";
        private static final float sMaxCountSlackPercent = 0.75F;
        private static final int sMaxMemoryLimitInBytes = 2097152;
        private HashMap<Integer, RemoteViewsAdapter.RemoteViewsIndexMetaData> mIndexMetaData;
        private HashMap<Integer, RemoteViews> mIndexRemoteViews;
        private int mLastRequestedIndex;
        private HashSet<Integer> mLoadIndices;
        private int mMaxCount;
        private int mMaxCountSlack;
        private final RemoteViewsAdapter.RemoteViewsMetaData mMetaData;
        private int mPreloadLowerBound;
        private int mPreloadUpperBound;
        private HashSet<Integer> mRequestedIndices;
        private final RemoteViewsAdapter.RemoteViewsMetaData mTemporaryMetaData;

        public FixedSizeRemoteViewsCache(int paramInt)
        {
            this.mMaxCount = paramInt;
            this.mMaxCountSlack = Math.round(0.75F * (this.mMaxCount / 2));
            this.mPreloadLowerBound = 0;
            this.mPreloadUpperBound = -1;
            this.mMetaData = new RemoteViewsAdapter.RemoteViewsMetaData();
            this.mTemporaryMetaData = new RemoteViewsAdapter.RemoteViewsMetaData();
            this.mIndexMetaData = new HashMap();
            this.mIndexRemoteViews = new HashMap();
            this.mRequestedIndices = new HashSet();
            this.mLastRequestedIndex = -1;
            this.mLoadIndices = new HashSet();
        }

        private int getFarthestPositionFrom(int paramInt, ArrayList<Integer> paramArrayList)
        {
            int i = 0;
            int j = -1;
            int k = 0;
            int m = -1;
            Iterator localIterator = this.mIndexRemoteViews.keySet().iterator();
            while (localIterator.hasNext())
            {
                int n = ((Integer)localIterator.next()).intValue();
                int i1 = Math.abs(n - paramInt);
                if ((i1 > k) && (!paramArrayList.contains(Integer.valueOf(n))))
                {
                    m = n;
                    k = i1;
                }
                if (i1 >= i)
                {
                    j = n;
                    i = i1;
                }
            }
            if (m > -1);
            while (true)
            {
                return m;
                m = j;
            }
        }

        private int getRemoteViewsBitmapMemoryUsage()
        {
            int i = 0;
            Iterator localIterator = this.mIndexRemoteViews.keySet().iterator();
            while (localIterator.hasNext())
            {
                Integer localInteger = (Integer)localIterator.next();
                RemoteViews localRemoteViews = (RemoteViews)this.mIndexRemoteViews.get(localInteger);
                if (localRemoteViews != null)
                    i += localRemoteViews.estimateMemoryUsage();
            }
            return i;
        }

        public void commitTemporaryMetaData()
        {
            synchronized (this.mTemporaryMetaData)
            {
                synchronized (this.mMetaData)
                {
                    this.mMetaData.set(this.mTemporaryMetaData);
                    return;
                }
            }
        }

        public boolean containsMetaDataAt(int paramInt)
        {
            return this.mIndexMetaData.containsKey(Integer.valueOf(paramInt));
        }

        public boolean containsRemoteViewAt(int paramInt)
        {
            return this.mIndexRemoteViews.containsKey(Integer.valueOf(paramInt));
        }

        public RemoteViewsAdapter.RemoteViewsMetaData getMetaData()
        {
            return this.mMetaData;
        }

        public RemoteViewsAdapter.RemoteViewsIndexMetaData getMetaDataAt(int paramInt)
        {
            if (this.mIndexMetaData.containsKey(Integer.valueOf(paramInt)));
            for (RemoteViewsAdapter.RemoteViewsIndexMetaData localRemoteViewsIndexMetaData = (RemoteViewsAdapter.RemoteViewsIndexMetaData)this.mIndexMetaData.get(Integer.valueOf(paramInt)); ; localRemoteViewsIndexMetaData = null)
                return localRemoteViewsIndexMetaData;
        }

        // ERROR //
        public int[] getNextIndexToLoad()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 75	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mLoadIndices	Ljava/util/HashSet;
            //     4: astore_1
            //     5: aload_1
            //     6: monitorenter
            //     7: aload_0
            //     8: getfield 71	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mRequestedIndices	Ljava/util/HashSet;
            //     11: invokevirtual 153	java/util/HashSet:isEmpty	()Z
            //     14: ifne +61 -> 75
            //     17: aload_0
            //     18: getfield 71	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mRequestedIndices	Ljava/util/HashSet;
            //     21: invokevirtual 154	java/util/HashSet:iterator	()Ljava/util/Iterator;
            //     24: invokeinterface 101 1 0
            //     29: checkcast 103	java/lang/Integer
            //     32: astore 6
            //     34: aload_0
            //     35: getfield 71	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mRequestedIndices	Ljava/util/HashSet;
            //     38: aload 6
            //     40: invokevirtual 157	java/util/HashSet:remove	(Ljava/lang/Object;)Z
            //     43: pop
            //     44: aload_0
            //     45: getfield 75	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mLoadIndices	Ljava/util/HashSet;
            //     48: aload 6
            //     50: invokevirtual 157	java/util/HashSet:remove	(Ljava/lang/Object;)Z
            //     53: pop
            //     54: iconst_2
            //     55: newarray int
            //     57: astore_3
            //     58: aload_3
            //     59: iconst_0
            //     60: aload 6
            //     62: invokevirtual 107	java/lang/Integer:intValue	()I
            //     65: iastore
            //     66: aload_3
            //     67: iconst_1
            //     68: iconst_1
            //     69: iastore
            //     70: aload_1
            //     71: monitorexit
            //     72: goto +81 -> 153
            //     75: aload_0
            //     76: getfield 75	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mLoadIndices	Ljava/util/HashSet;
            //     79: invokevirtual 153	java/util/HashSet:isEmpty	()Z
            //     82: ifne +56 -> 138
            //     85: aload_0
            //     86: getfield 75	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mLoadIndices	Ljava/util/HashSet;
            //     89: invokevirtual 154	java/util/HashSet:iterator	()Ljava/util/Iterator;
            //     92: invokeinterface 101 1 0
            //     97: checkcast 103	java/lang/Integer
            //     100: astore 4
            //     102: aload_0
            //     103: getfield 75	android/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache:mLoadIndices	Ljava/util/HashSet;
            //     106: aload 4
            //     108: invokevirtual 157	java/util/HashSet:remove	(Ljava/lang/Object;)Z
            //     111: pop
            //     112: iconst_2
            //     113: newarray int
            //     115: astore_3
            //     116: aload_3
            //     117: iconst_0
            //     118: aload 4
            //     120: invokevirtual 107	java/lang/Integer:intValue	()I
            //     123: iastore
            //     124: aload_3
            //     125: iconst_1
            //     126: iconst_0
            //     127: iastore
            //     128: aload_1
            //     129: monitorexit
            //     130: goto +23 -> 153
            //     133: astore_2
            //     134: aload_1
            //     135: monitorexit
            //     136: aload_2
            //     137: athrow
            //     138: iconst_2
            //     139: newarray int
            //     141: astore_3
            //     142: aload_3
            //     143: iconst_0
            //     144: bipush 255
            //     146: iastore
            //     147: aload_3
            //     148: iconst_1
            //     149: iconst_0
            //     150: iastore
            //     151: aload_1
            //     152: monitorexit
            //     153: aload_3
            //     154: areturn
            //
            // Exception table:
            //     from	to	target	type
            //     7	136	133	finally
            //     138	153	133	finally
        }

        public RemoteViews getRemoteViewsAt(int paramInt)
        {
            if (this.mIndexRemoteViews.containsKey(Integer.valueOf(paramInt)));
            for (RemoteViews localRemoteViews = (RemoteViews)this.mIndexRemoteViews.get(Integer.valueOf(paramInt)); ; localRemoteViews = null)
                return localRemoteViews;
        }

        public RemoteViewsAdapter.RemoteViewsMetaData getTemporaryMetaData()
        {
            return this.mTemporaryMetaData;
        }

        public void insert(int paramInt, RemoteViews paramRemoteViews, long paramLong, ArrayList<Integer> paramArrayList)
        {
            if (this.mIndexRemoteViews.size() >= this.mMaxCount)
                this.mIndexRemoteViews.remove(Integer.valueOf(getFarthestPositionFrom(paramInt, paramArrayList)));
            int i;
            if (this.mLastRequestedIndex > -1)
                i = this.mLastRequestedIndex;
            while (getRemoteViewsBitmapMemoryUsage() >= 2097152)
            {
                this.mIndexRemoteViews.remove(Integer.valueOf(getFarthestPositionFrom(i, paramArrayList)));
                continue;
                i = paramInt;
            }
            if (this.mIndexMetaData.containsKey(Integer.valueOf(paramInt)))
                ((RemoteViewsAdapter.RemoteViewsIndexMetaData)this.mIndexMetaData.get(Integer.valueOf(paramInt))).set(paramRemoteViews, paramLong);
            while (true)
            {
                this.mIndexRemoteViews.put(Integer.valueOf(paramInt), paramRemoteViews);
                return;
                this.mIndexMetaData.put(Integer.valueOf(paramInt), new RemoteViewsAdapter.RemoteViewsIndexMetaData(paramRemoteViews, paramLong));
            }
        }

        public boolean queuePositionsToBePreloadedFromRequestedPosition(int paramInt)
        {
            boolean bool = false;
            if ((this.mPreloadLowerBound <= paramInt) && (paramInt <= this.mPreloadUpperBound) && (Math.abs(paramInt - (this.mPreloadUpperBound + this.mPreloadLowerBound) / 2) < this.mMaxCountSlack));
            while (true)
            {
                return bool;
                int i;
                synchronized (this.mMetaData)
                {
                    i = this.mMetaData.count;
                }
                synchronized (this.mLoadIndices)
                {
                    this.mLoadIndices.clear();
                    this.mLoadIndices.addAll(this.mRequestedIndices);
                    int j = this.mMaxCount / 2;
                    this.mPreloadLowerBound = (paramInt - j);
                    this.mPreloadUpperBound = (paramInt + j);
                    int k = Math.max(0, this.mPreloadLowerBound);
                    int m = Math.min(this.mPreloadUpperBound, i - 1);
                    int n = k;
                    while (n <= m)
                    {
                        this.mLoadIndices.add(Integer.valueOf(n));
                        n++;
                        continue;
                        localObject1 = finally;
                        throw localObject1;
                    }
                    this.mLoadIndices.removeAll(this.mIndexRemoteViews.keySet());
                    bool = true;
                }
            }
        }

        public void queueRequestedPositionToLoad(int paramInt)
        {
            this.mLastRequestedIndex = paramInt;
            synchronized (this.mLoadIndices)
            {
                this.mRequestedIndices.add(Integer.valueOf(paramInt));
                this.mLoadIndices.add(Integer.valueOf(paramInt));
                return;
            }
        }

        public void reset()
        {
            this.mPreloadLowerBound = 0;
            this.mPreloadUpperBound = -1;
            this.mLastRequestedIndex = -1;
            this.mIndexRemoteViews.clear();
            this.mIndexMetaData.clear();
            synchronized (this.mLoadIndices)
            {
                this.mRequestedIndices.clear();
                this.mLoadIndices.clear();
                return;
            }
        }
    }

    private static class RemoteViewsIndexMetaData
    {
        long itemId;
        int typeId;

        public RemoteViewsIndexMetaData(RemoteViews paramRemoteViews, long paramLong)
        {
            set(paramRemoteViews, paramLong);
        }

        public void set(RemoteViews paramRemoteViews, long paramLong)
        {
            this.itemId = paramLong;
            if (paramRemoteViews != null);
            for (this.typeId = paramRemoteViews.getLayoutId(); ; this.typeId = 0)
                return;
        }
    }

    private static class RemoteViewsMetaData
    {
        int count;
        boolean hasStableIds;
        RemoteViews mFirstView;
        int mFirstViewHeight;
        private final HashMap<Integer, Integer> mTypeIdIndexMap = new HashMap();
        RemoteViews mUserLoadingView;
        int viewTypeCount;

        public RemoteViewsMetaData()
        {
            reset();
        }

        // ERROR //
        private RemoteViewsAdapter.RemoteViewsFrameLayout createLoadingView(int paramInt, View paramView, android.view.ViewGroup paramViewGroup, Object paramObject, LayoutInflater paramLayoutInflater)
        {
            // Byte code:
            //     0: aload_3
            //     1: invokevirtual 45	android/view/ViewGroup:getContext	()Landroid/content/Context;
            //     4: astore 6
            //     6: new 47	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout
            //     9: dup
            //     10: aload 6
            //     12: invokespecial 50	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:<init>	(Landroid/content/Context;)V
            //     15: astore 7
            //     17: aload 4
            //     19: monitorenter
            //     20: iconst_0
            //     21: istore 8
            //     23: aload_0
            //     24: getfield 52	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mUserLoadingView	Landroid/widget/RemoteViews;
            //     27: astore 10
            //     29: aload 10
            //     31: ifnull +42 -> 73
            //     34: aload_0
            //     35: getfield 52	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mUserLoadingView	Landroid/widget/RemoteViews;
            //     38: aload_3
            //     39: invokevirtual 45	android/view/ViewGroup:getContext	()Landroid/content/Context;
            //     42: aload_3
            //     43: invokevirtual 58	android/widget/RemoteViews:apply	(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
            //     46: astore 18
            //     48: aload 18
            //     50: ldc 59
            //     52: new 61	java/lang/Integer
            //     55: dup
            //     56: iconst_0
            //     57: invokespecial 64	java/lang/Integer:<init>	(I)V
            //     60: invokevirtual 70	android/view/View:setTagInternal	(ILjava/lang/Object;)V
            //     63: aload 7
            //     65: aload 18
            //     67: invokevirtual 74	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:addView	(Landroid/view/View;)V
            //     70: iconst_1
            //     71: istore 8
            //     73: iload 8
            //     75: ifne +101 -> 176
            //     78: aload_0
            //     79: getfield 76	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstViewHeight	I
            //     82: istore 11
            //     84: iload 11
            //     86: ifge +46 -> 132
            //     89: aload_0
            //     90: getfield 78	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstView	Landroid/widget/RemoteViews;
            //     93: aload_3
            //     94: invokevirtual 45	android/view/ViewGroup:getContext	()Landroid/content/Context;
            //     97: aload_3
            //     98: invokevirtual 58	android/widget/RemoteViews:apply	(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
            //     101: astore 15
            //     103: aload 15
            //     105: iconst_0
            //     106: iconst_0
            //     107: invokestatic 84	android/view/View$MeasureSpec:makeMeasureSpec	(II)I
            //     110: iconst_0
            //     111: iconst_0
            //     112: invokestatic 84	android/view/View$MeasureSpec:makeMeasureSpec	(II)I
            //     115: invokevirtual 88	android/view/View:measure	(II)V
            //     118: aload_0
            //     119: aload 15
            //     121: invokevirtual 92	android/view/View:getMeasuredHeight	()I
            //     124: putfield 76	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstViewHeight	I
            //     127: aload_0
            //     128: aconst_null
            //     129: putfield 78	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstView	Landroid/widget/RemoteViews;
            //     132: aload 5
            //     134: ldc 93
            //     136: aload 7
            //     138: iconst_0
            //     139: invokevirtual 99	android/view/LayoutInflater:inflate	(ILandroid/view/ViewGroup;Z)Landroid/view/View;
            //     142: checkcast 101	android/widget/TextView
            //     145: astore 12
            //     147: aload 12
            //     149: aload_0
            //     150: getfield 76	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstViewHeight	I
            //     153: invokevirtual 104	android/widget/TextView:setHeight	(I)V
            //     156: aload 12
            //     158: new 61	java/lang/Integer
            //     161: dup
            //     162: iconst_0
            //     163: invokespecial 64	java/lang/Integer:<init>	(I)V
            //     166: invokevirtual 108	android/widget/TextView:setTag	(Ljava/lang/Object;)V
            //     169: aload 7
            //     171: aload 12
            //     173: invokevirtual 74	android/widget/RemoteViewsAdapter$RemoteViewsFrameLayout:addView	(Landroid/view/View;)V
            //     176: aload 4
            //     178: monitorexit
            //     179: aload 7
            //     181: areturn
            //     182: astore 16
            //     184: ldc 110
            //     186: ldc 112
            //     188: aload 16
            //     190: invokestatic 118	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     193: pop
            //     194: goto -121 -> 73
            //     197: astore 9
            //     199: aload 4
            //     201: monitorexit
            //     202: aload 9
            //     204: athrow
            //     205: astore 13
            //     207: aload_0
            //     208: ldc 119
            //     210: aload 6
            //     212: invokevirtual 125	android/content/Context:getResources	()Landroid/content/res/Resources;
            //     215: invokevirtual 131	android/content/res/Resources:getDisplayMetrics	()Landroid/util/DisplayMetrics;
            //     218: getfield 137	android/util/DisplayMetrics:density	F
            //     221: fmul
            //     222: invokestatic 143	java/lang/Math:round	(F)I
            //     225: putfield 76	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstViewHeight	I
            //     228: aload_0
            //     229: aconst_null
            //     230: putfield 78	android/widget/RemoteViewsAdapter$RemoteViewsMetaData:mFirstView	Landroid/widget/RemoteViews;
            //     233: ldc 110
            //     235: new 145	java/lang/StringBuilder
            //     238: dup
            //     239: invokespecial 146	java/lang/StringBuilder:<init>	()V
            //     242: ldc 148
            //     244: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     247: aload 13
            //     249: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     252: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     255: invokestatic 162	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     258: pop
            //     259: goto -127 -> 132
            //
            // Exception table:
            //     from	to	target	type
            //     34	70	182	java/lang/Exception
            //     23	29	197	finally
            //     34	70	197	finally
            //     78	84	197	finally
            //     89	132	197	finally
            //     132	202	197	finally
            //     207	259	197	finally
            //     89	132	205	java/lang/Exception
        }

        public int getMappedViewType(int paramInt)
        {
            int i;
            if (this.mTypeIdIndexMap.containsKey(Integer.valueOf(paramInt)))
                i = ((Integer)this.mTypeIdIndexMap.get(Integer.valueOf(paramInt))).intValue();
            while (true)
            {
                return i;
                i = 1 + this.mTypeIdIndexMap.size();
                this.mTypeIdIndexMap.put(Integer.valueOf(paramInt), Integer.valueOf(i));
            }
        }

        public boolean isViewTypeInRange(int paramInt)
        {
            if (getMappedViewType(paramInt) >= this.viewTypeCount);
            for (boolean bool = false; ; bool = true)
                return bool;
        }

        public void reset()
        {
            this.count = 0;
            this.viewTypeCount = 1;
            this.hasStableIds = true;
            this.mUserLoadingView = null;
            this.mFirstView = null;
            this.mFirstViewHeight = 0;
            this.mTypeIdIndexMap.clear();
        }

        public void set(RemoteViewsMetaData paramRemoteViewsMetaData)
        {
            try
            {
                this.count = paramRemoteViewsMetaData.count;
                this.viewTypeCount = paramRemoteViewsMetaData.viewTypeCount;
                this.hasStableIds = paramRemoteViewsMetaData.hasStableIds;
                setLoadingViewTemplates(paramRemoteViewsMetaData.mUserLoadingView, paramRemoteViewsMetaData.mFirstView);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void setLoadingViewTemplates(RemoteViews paramRemoteViews1, RemoteViews paramRemoteViews2)
        {
            this.mUserLoadingView = paramRemoteViews1;
            if (paramRemoteViews2 != null)
            {
                this.mFirstView = paramRemoteViews2;
                this.mFirstViewHeight = -1;
            }
        }
    }

    private class RemoteViewsFrameLayoutRefSet
    {
        private HashMap<Integer, LinkedList<RemoteViewsAdapter.RemoteViewsFrameLayout>> mReferences = new HashMap();

        public RemoteViewsFrameLayoutRefSet()
        {
        }

        public void add(int paramInt, RemoteViewsAdapter.RemoteViewsFrameLayout paramRemoteViewsFrameLayout)
        {
            Integer localInteger = Integer.valueOf(paramInt);
            LinkedList localLinkedList;
            if (this.mReferences.containsKey(localInteger))
                localLinkedList = (LinkedList)this.mReferences.get(localInteger);
            while (true)
            {
                localLinkedList.add(paramRemoteViewsFrameLayout);
                return;
                localLinkedList = new LinkedList();
                this.mReferences.put(localInteger, localLinkedList);
            }
        }

        public void clear()
        {
            this.mReferences.clear();
        }

        public void notifyOnRemoteViewsLoaded(int paramInt, RemoteViews paramRemoteViews)
        {
            if (paramRemoteViews == null);
            while (true)
            {
                return;
                Integer localInteger = Integer.valueOf(paramInt);
                if (this.mReferences.containsKey(localInteger))
                {
                    LinkedList localLinkedList = (LinkedList)this.mReferences.get(localInteger);
                    Iterator localIterator = localLinkedList.iterator();
                    while (localIterator.hasNext())
                        ((RemoteViewsAdapter.RemoteViewsFrameLayout)localIterator.next()).onRemoteViewsLoaded(paramRemoteViews);
                    localLinkedList.clear();
                    this.mReferences.remove(localInteger);
                }
            }
        }
    }

    private static class RemoteViewsFrameLayout extends FrameLayout
    {
        public RemoteViewsFrameLayout(Context paramContext)
        {
            super();
        }

        public void onRemoteViewsLoaded(RemoteViews paramRemoteViews)
        {
            try
            {
                removeAllViews();
                addView(paramRemoteViews.apply(getContext(), this));
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    Log.e("RemoteViewsAdapter", "Failed to apply RemoteViews.");
            }
        }
    }

    private static class RemoteViewsAdapterServiceConnection extends IRemoteViewsAdapterConnection.Stub
    {
        private WeakReference<RemoteViewsAdapter> mAdapter;
        private boolean mIsConnected;
        private boolean mIsConnecting;
        private IRemoteViewsFactory mRemoteViewsFactory;

        public RemoteViewsAdapterServiceConnection(RemoteViewsAdapter paramRemoteViewsAdapter)
        {
            this.mAdapter = new WeakReference(paramRemoteViewsAdapter);
        }

        /** @deprecated */
        public void bind(Context paramContext, int paramInt, Intent paramIntent)
        {
            try
            {
                boolean bool = this.mIsConnecting;
                if (!bool);
                try
                {
                    AppWidgetManager.getInstance(paramContext).bindRemoteViewsService(paramInt, paramIntent, asBinder());
                    this.mIsConnecting = true;
                    return;
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        Log.e("RemoteViewsAdapterServiceConnection", "bind(): " + localException.getMessage());
                        this.mIsConnecting = false;
                        this.mIsConnected = false;
                    }
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public IRemoteViewsFactory getRemoteViewsFactory()
        {
            try
            {
                IRemoteViewsFactory localIRemoteViewsFactory = this.mRemoteViewsFactory;
                return localIRemoteViewsFactory;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public boolean isConnected()
        {
            try
            {
                boolean bool = this.mIsConnected;
                return bool;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public void onServiceConnected(IBinder paramIBinder)
        {
            try
            {
                this.mRemoteViewsFactory = IRemoteViewsFactory.Stub.asInterface(paramIBinder);
                final RemoteViewsAdapter localRemoteViewsAdapter = (RemoteViewsAdapter)this.mAdapter.get();
                if (localRemoteViewsAdapter == null);
                while (true)
                {
                    return;
                    localRemoteViewsAdapter.mWorkerQueue.post(new Runnable()
                    {
                        public void run()
                        {
                            if (localRemoteViewsAdapter.mNotifyDataSetChangedAfterOnServiceConnected)
                                localRemoteViewsAdapter.onNotifyDataSetChanged();
                            while (true)
                            {
                                localRemoteViewsAdapter.enqueueDeferredUnbindServiceMessage();
                                RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.access$802(RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.this, true);
                                RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.access$902(RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.this, false);
                                label42: return;
                                IRemoteViewsFactory localIRemoteViewsFactory = localRemoteViewsAdapter.mServiceConnection.getRemoteViewsFactory();
                                try
                                {
                                    if (!localIRemoteViewsFactory.isCreated())
                                        localIRemoteViewsFactory.onDataSetChanged();
                                    localRemoteViewsAdapter.updateTemporaryMetaData();
                                    localRemoteViewsAdapter.mMainQueue.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            synchronized (RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.1.this.val$adapter.mCache)
                                            {
                                                RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.1.this.val$adapter.mCache.commitTemporaryMetaData();
                                                RemoteViewsAdapter.RemoteAdapterConnectionCallback localRemoteAdapterConnectionCallback = (RemoteViewsAdapter.RemoteAdapterConnectionCallback)RemoteViewsAdapter.RemoteViewsAdapterServiceConnection.1.this.val$adapter.mCallback.get();
                                                if (localRemoteAdapterConnectionCallback != null)
                                                    localRemoteAdapterConnectionCallback.onRemoteAdapterConnected();
                                                return;
                                            }
                                        }
                                    });
                                }
                                catch (RemoteException localRemoteException)
                                {
                                    Log.e("RemoteViewsAdapter", "Error notifying factory of data set changed in onServiceConnected(): " + localRemoteException.getMessage());
                                    break label42;
                                }
                                catch (RuntimeException localRuntimeException)
                                {
                                    while (true)
                                        Log.e("RemoteViewsAdapter", "Error notifying factory of data set changed in onServiceConnected(): " + localRuntimeException.getMessage());
                                }
                            }
                        }
                    });
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public void onServiceDisconnected()
        {
            try
            {
                this.mIsConnected = false;
                this.mIsConnecting = false;
                this.mRemoteViewsFactory = null;
                final RemoteViewsAdapter localRemoteViewsAdapter = (RemoteViewsAdapter)this.mAdapter.get();
                if (localRemoteViewsAdapter == null);
                while (true)
                {
                    return;
                    localRemoteViewsAdapter.mMainQueue.post(new Runnable()
                    {
                        public void run()
                        {
                            localRemoteViewsAdapter.mMainQueue.removeMessages(1);
                            RemoteViewsAdapter.RemoteAdapterConnectionCallback localRemoteAdapterConnectionCallback = (RemoteViewsAdapter.RemoteAdapterConnectionCallback)localRemoteViewsAdapter.mCallback.get();
                            if (localRemoteAdapterConnectionCallback != null)
                                localRemoteAdapterConnectionCallback.onRemoteAdapterDisconnected();
                        }
                    });
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public void unbind(Context paramContext, int paramInt, Intent paramIntent)
        {
            try
            {
                AppWidgetManager.getInstance(paramContext).unbindRemoteViewsService(paramInt, paramIntent);
                this.mIsConnecting = false;
                return;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Log.e("RemoteViewsAdapterServiceConnection", "unbind(): " + localException.getMessage());
                    this.mIsConnecting = false;
                    this.mIsConnected = false;
                }
            }
            finally
            {
            }
        }
    }

    public static abstract interface RemoteAdapterConnectionCallback
    {
        public abstract void deferNotifyDataSetChanged();

        public abstract boolean onRemoteAdapterConnected();

        public abstract void onRemoteAdapterDisconnected();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.RemoteViewsAdapter
 * JD-Core Version:        0.6.2
 */