package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class ToggleButton extends CompoundButton
{
    private static final int NO_ALPHA = 255;
    private float mDisabledAlpha;
    private Drawable mIndicatorDrawable;
    private CharSequence mTextOff;
    private CharSequence mTextOn;

    public ToggleButton(Context paramContext)
    {
        this(paramContext, null);
    }

    public ToggleButton(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842827);
    }

    public ToggleButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ToggleButton, paramInt, 0);
        this.mTextOn = localTypedArray.getText(1);
        this.mTextOff = localTypedArray.getText(2);
        this.mDisabledAlpha = localTypedArray.getFloat(0, 0.5F);
        syncTextState();
        localTypedArray.recycle();
    }

    private void syncTextState()
    {
        boolean bool = isChecked();
        if ((bool) && (this.mTextOn != null))
            setText(this.mTextOn);
        while (true)
        {
            return;
            if ((!bool) && (this.mTextOff != null))
                setText(this.mTextOff);
        }
    }

    private void updateReferenceToIndicatorDrawable(Drawable paramDrawable)
    {
        if ((paramDrawable instanceof LayerDrawable));
        for (this.mIndicatorDrawable = ((LayerDrawable)paramDrawable).findDrawableByLayerId(16908311); ; this.mIndicatorDrawable = null)
            return;
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        Drawable localDrawable;
        if (this.mIndicatorDrawable != null)
        {
            localDrawable = this.mIndicatorDrawable;
            if (!isEnabled())
                break label33;
        }
        label33: for (int i = 255; ; i = (int)(255.0F * this.mDisabledAlpha))
        {
            localDrawable.setAlpha(i);
            return;
        }
    }

    public CharSequence getTextOff()
    {
        return this.mTextOff;
    }

    public CharSequence getTextOn()
    {
        return this.mTextOn;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        updateReferenceToIndicatorDrawable(getBackground());
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ToggleButton.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ToggleButton.class.getName());
    }

    public void setBackgroundDrawable(Drawable paramDrawable)
    {
        super.setBackgroundDrawable(paramDrawable);
        updateReferenceToIndicatorDrawable(paramDrawable);
    }

    public void setChecked(boolean paramBoolean)
    {
        super.setChecked(paramBoolean);
        syncTextState();
    }

    public void setTextOff(CharSequence paramCharSequence)
    {
        this.mTextOff = paramCharSequence;
    }

    public void setTextOn(CharSequence paramCharSequence)
    {
        this.mTextOn = paramCharSequence;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ToggleButton
 * JD-Core Version:        0.6.2
 */