package android.widget;

public abstract interface WrapperListAdapter extends ListAdapter
{
    public abstract ListAdapter getWrappedAdapter();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.WrapperListAdapter
 * JD-Core Version:        0.6.2
 */