package android.widget;

import android.graphics.Rect;
import android.text.Layout;
import android.text.Spannable;
import android.view.AccessibilityIterators.AbstractTextSegmentIterator;

final class AccessibilityIterators
{
    static class PageTextSegmentIterator extends AccessibilityIterators.LineTextSegmentIterator
    {
        private static PageTextSegmentIterator sPageInstance;
        private final Rect mTempRect = new Rect();
        private TextView mView;

        public static PageTextSegmentIterator getInstance()
        {
            if (sPageInstance == null)
                sPageInstance = new PageTextSegmentIterator();
            return sPageInstance;
        }

        public int[] following(int paramInt)
        {
            int[] arrayOfInt = null;
            if (this.mText.length() <= 0);
            while ((paramInt >= this.mText.length()) || (!this.mView.getGlobalVisibleRect(this.mTempRect)))
                return arrayOfInt;
            int i = Math.max(0, paramInt);
            int j = this.mLayout.getLineForOffset(i);
            int k = this.mLayout.getLineTop(j) + (this.mTempRect.height() - this.mView.getTotalPaddingTop() - this.mView.getTotalPaddingBottom());
            if (k < this.mLayout.getLineTop(-1 + this.mLayout.getLineCount()));
            for (int m = -1 + this.mLayout.getLineForVertical(k); ; m = -1 + this.mLayout.getLineCount())
            {
                arrayOfInt = getRange(i, 1 + getLineEdgeIndex(m, 1));
                break;
            }
        }

        public void initialize(TextView paramTextView)
        {
            super.initialize((Spannable)paramTextView.getIterableTextForAccessibility(), paramTextView.getLayout());
            this.mView = paramTextView;
        }

        public int[] preceding(int paramInt)
        {
            int[] arrayOfInt = null;
            if (this.mText.length() <= 0);
            while ((paramInt <= 0) || (!this.mView.getGlobalVisibleRect(this.mTempRect)))
                return arrayOfInt;
            int i = Math.min(this.mText.length(), paramInt);
            int j = this.mLayout.getLineForOffset(i);
            int k = this.mLayout.getLineTop(j) - (this.mTempRect.height() - this.mView.getTotalPaddingTop() - this.mView.getTotalPaddingBottom());
            if (k > 0);
            for (int m = 1 + this.mLayout.getLineForVertical(k); ; m = 0)
            {
                arrayOfInt = getRange(getLineEdgeIndex(m, -1), i);
                break;
            }
        }
    }

    static class LineTextSegmentIterator extends AccessibilityIterators.AbstractTextSegmentIterator
    {
        protected static final int DIRECTION_END = 1;
        protected static final int DIRECTION_START = -1;
        private static LineTextSegmentIterator sLineInstance;
        protected Layout mLayout;

        public static LineTextSegmentIterator getInstance()
        {
            if (sLineInstance == null)
                sLineInstance = new LineTextSegmentIterator();
            return sLineInstance;
        }

        public int[] following(int paramInt)
        {
            int[] arrayOfInt = null;
            if (this.mText.length() <= 0);
            label108: 
            while (true)
            {
                return arrayOfInt;
                if (paramInt < this.mText.length())
                {
                    int j;
                    if (paramInt < 0)
                        j = this.mLayout.getLineForOffset(0);
                    while (true)
                    {
                        if (j >= this.mLayout.getLineCount())
                            break label108;
                        arrayOfInt = getRange(getLineEdgeIndex(j, -1), 1 + getLineEdgeIndex(j, 1));
                        break;
                        int i = this.mLayout.getLineForOffset(paramInt);
                        if (getLineEdgeIndex(i, -1) == paramInt)
                            j = i;
                        else
                            j = i + 1;
                    }
                }
            }
        }

        protected int getLineEdgeIndex(int paramInt1, int paramInt2)
        {
            if (paramInt2 * this.mLayout.getParagraphDirection(paramInt1) < 0);
            for (int i = this.mLayout.getLineStart(paramInt1); ; i = -1 + this.mLayout.getLineEnd(paramInt1))
                return i;
        }

        public void initialize(Spannable paramSpannable, Layout paramLayout)
        {
            this.mText = paramSpannable.toString();
            this.mLayout = paramLayout;
        }

        public int[] preceding(int paramInt)
        {
            int[] arrayOfInt = null;
            if (this.mText.length() <= 0);
            label108: 
            while (true)
            {
                return arrayOfInt;
                if (paramInt > 0)
                {
                    int j;
                    if (paramInt > this.mText.length())
                        j = this.mLayout.getLineForOffset(this.mText.length());
                    while (true)
                    {
                        if (j < 0)
                            break label108;
                        arrayOfInt = getRange(getLineEdgeIndex(j, -1), 1 + getLineEdgeIndex(j, 1));
                        break;
                        int i = this.mLayout.getLineForOffset(paramInt);
                        if (1 + getLineEdgeIndex(i, 1) == paramInt)
                            j = i;
                        else
                            j = i - 1;
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AccessibilityIterators
 * JD-Core Version:        0.6.2
 */