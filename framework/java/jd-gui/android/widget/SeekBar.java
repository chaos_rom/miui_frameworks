package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class SeekBar extends AbsSeekBar
{
    private OnSeekBarChangeListener mOnSeekBarChangeListener;

    public SeekBar(Context paramContext)
    {
        this(paramContext, null);
    }

    public SeekBar(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842875);
    }

    public SeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(SeekBar.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(SeekBar.class.getName());
    }

    void onProgressRefresh(float paramFloat, boolean paramBoolean)
    {
        super.onProgressRefresh(paramFloat, paramBoolean);
        if (this.mOnSeekBarChangeListener != null)
            this.mOnSeekBarChangeListener.onProgressChanged(this, getProgress(), paramBoolean);
    }

    void onStartTrackingTouch()
    {
        super.onStartTrackingTouch();
        if (this.mOnSeekBarChangeListener != null)
            this.mOnSeekBarChangeListener.onStartTrackingTouch(this);
    }

    void onStopTrackingTouch()
    {
        super.onStopTrackingTouch();
        if (this.mOnSeekBarChangeListener != null)
            this.mOnSeekBarChangeListener.onStopTrackingTouch(this);
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener paramOnSeekBarChangeListener)
    {
        this.mOnSeekBarChangeListener = paramOnSeekBarChangeListener;
    }

    public static abstract interface OnSeekBarChangeListener
    {
        public abstract void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean);

        public abstract void onStartTrackingTouch(SeekBar paramSeekBar);

        public abstract void onStopTrackingTouch(SeekBar paramSeekBar);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SeekBar
 * JD-Core Version:        0.6.2
 */