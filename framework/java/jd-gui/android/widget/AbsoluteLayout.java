package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.android.internal.R.styleable;

@RemoteViews.RemoteView
@Deprecated
public class AbsoluteLayout extends ViewGroup
{
    public AbsoluteLayout(Context paramContext)
    {
        super(paramContext);
    }

    public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-2, -2, 0, 0);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            if (localView.getVisibility() != 8)
            {
                LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
                int k = this.mPaddingLeft + localLayoutParams.x;
                int m = this.mPaddingTop + localLayoutParams.y;
                localView.layout(k, m, k + localView.getMeasuredWidth(), m + localView.getMeasuredHeight());
            }
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getChildCount();
        int j = 0;
        int k = 0;
        measureChildren(paramInt1, paramInt2);
        for (int m = 0; m < i; m++)
        {
            View localView = getChildAt(m);
            if (localView.getVisibility() != 8)
            {
                LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
                int i2 = localLayoutParams.x + localView.getMeasuredWidth();
                int i3 = localLayoutParams.y + localView.getMeasuredHeight();
                k = Math.max(k, i2);
                j = Math.max(j, i3);
            }
        }
        int n = k + (this.mPaddingLeft + this.mPaddingRight);
        int i1 = Math.max(j + (this.mPaddingTop + this.mPaddingBottom), getSuggestedMinimumHeight());
        setMeasuredDimension(resolveSizeAndState(Math.max(n, getSuggestedMinimumWidth()), paramInt1, 0), resolveSizeAndState(i1, paramInt2, 0));
    }

    public boolean shouldDelayChildPressedState()
    {
        return false;
    }

    public static class LayoutParams extends ViewGroup.LayoutParams
    {
        public int x;
        public int y;

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            super(paramInt2);
            this.x = paramInt3;
            this.y = paramInt4;
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AbsoluteLayout_Layout);
            this.x = localTypedArray.getDimensionPixelOffset(0, 0);
            this.y = localTypedArray.getDimensionPixelOffset(1, 0);
            localTypedArray.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public String debug(String paramString)
        {
            return paramString + "Absolute.LayoutParams={width=" + sizeToString(this.width) + ", height=" + sizeToString(this.height) + " x=" + this.x + " y=" + this.y + "}";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AbsoluteLayout
 * JD-Core Version:        0.6.2
 */