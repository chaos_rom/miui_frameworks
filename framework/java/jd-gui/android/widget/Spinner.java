package android.widget;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class Spinner extends AbsSpinner
    implements DialogInterface.OnClickListener
{
    private static final int MAX_ITEMS_MEASURED = 15;
    public static final int MODE_DIALOG = 0;
    public static final int MODE_DROPDOWN = 1;
    private static final int MODE_THEME = -1;
    private static final String TAG = "Spinner";
    private boolean mDisableChildrenWhenDisabled;
    int mDropDownWidth;
    private int mGravity;
    private SpinnerPopup mPopup;
    private DropDownAdapter mTempAdapter;
    private Rect mTempRect = new Rect();

    public Spinner(Context paramContext)
    {
        this(paramContext, null);
    }

    public Spinner(Context paramContext, int paramInt)
    {
        this(paramContext, null, 16842881, paramInt);
    }

    public Spinner(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842881);
    }

    public Spinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        this(paramContext, paramAttributeSet, paramInt, -1);
    }

    public Spinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
    {
        super(paramContext, paramAttributeSet, paramInt1);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Spinner, paramInt1, 0);
        if (paramInt2 == -1)
            paramInt2 = localTypedArray.getInt(7, 0);
        switch (paramInt2)
        {
        default:
        case 0:
        case 1:
        }
        while (true)
        {
            this.mGravity = localTypedArray.getInt(0, 17);
            this.mPopup.setPromptText(localTypedArray.getString(3));
            this.mDisableChildrenWhenDisabled = localTypedArray.getBoolean(9, false);
            localTypedArray.recycle();
            if (this.mTempAdapter != null)
            {
                this.mPopup.setAdapter(this.mTempAdapter);
                this.mTempAdapter = null;
            }
            return;
            this.mPopup = new DialogPopup(null);
            continue;
            DropdownPopup localDropdownPopup = new DropdownPopup(paramContext, paramAttributeSet, paramInt1);
            this.mDropDownWidth = localTypedArray.getLayoutDimension(4, -2);
            localDropdownPopup.setBackgroundDrawable(localTypedArray.getDrawable(2));
            int i = localTypedArray.getDimensionPixelOffset(6, 0);
            if (i != 0)
                localDropdownPopup.setVerticalOffset(i);
            int j = localTypedArray.getDimensionPixelOffset(5, 0);
            if (j != 0)
                localDropdownPopup.setHorizontalOffset(j);
            this.mPopup = localDropdownPopup;
        }
    }

    private View makeAndAddView(int paramInt)
    {
        View localView2;
        if (!this.mDataChanged)
        {
            localView2 = this.mRecycler.get(paramInt);
            if (localView2 != null)
                setUpChild(localView2);
        }
        View localView1;
        for (Object localObject = localView2; ; localObject = localView1)
        {
            return localObject;
            localView1 = this.mAdapter.getView(paramInt, null, this);
            setUpChild(localView1);
        }
    }

    private void setUpChild(View paramView)
    {
        ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
        if (localLayoutParams == null)
            localLayoutParams = generateDefaultLayoutParams();
        addViewInLayout(paramView, 0, localLayoutParams);
        paramView.setSelected(hasFocus());
        if (this.mDisableChildrenWhenDisabled)
            paramView.setEnabled(isEnabled());
        int i = ViewGroup.getChildMeasureSpec(this.mHeightMeasureSpec, this.mSpinnerPadding.top + this.mSpinnerPadding.bottom, localLayoutParams.height);
        paramView.measure(ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mSpinnerPadding.left + this.mSpinnerPadding.right, localLayoutParams.width), i);
        int j = this.mSpinnerPadding.top + (getMeasuredHeight() - this.mSpinnerPadding.bottom - this.mSpinnerPadding.top - paramView.getMeasuredHeight()) / 2;
        int k = j + paramView.getMeasuredHeight();
        paramView.layout(0, j, 0 + paramView.getMeasuredWidth(), k);
    }

    public int getBaseline()
    {
        int i = -1;
        View localView = null;
        if (getChildCount() > 0)
            localView = getChildAt(0);
        while (true)
        {
            if (localView != null)
            {
                int j = localView.getBaseline();
                if (j >= 0)
                    i = j + localView.getTop();
            }
            return i;
            if ((this.mAdapter != null) && (this.mAdapter.getCount() > 0))
            {
                localView = makeAndAddView(0);
                this.mRecycler.put(0, localView);
                removeAllViewsInLayout();
            }
        }
    }

    public int getDropDownHorizontalOffset()
    {
        return this.mPopup.getHorizontalOffset();
    }

    public int getDropDownVerticalOffset()
    {
        return this.mPopup.getVerticalOffset();
    }

    public int getDropDownWidth()
    {
        return this.mDropDownWidth;
    }

    public int getGravity()
    {
        return this.mGravity;
    }

    public Drawable getPopupBackground()
    {
        return this.mPopup.getBackground();
    }

    public CharSequence getPrompt()
    {
        return this.mPopup.getHintText();
    }

    void layout(int paramInt, boolean paramBoolean)
    {
        int i = this.mSpinnerPadding.left;
        int j = this.mRight - this.mLeft - this.mSpinnerPadding.left - this.mSpinnerPadding.right;
        if (this.mDataChanged)
            handleDataChanged();
        if (this.mItemCount == 0)
        {
            resetList();
            return;
        }
        if (this.mNextSelectedPosition >= 0)
            setSelectedPositionInt(this.mNextSelectedPosition);
        recycleAllViews();
        removeAllViewsInLayout();
        this.mFirstPosition = this.mSelectedPosition;
        View localView = makeAndAddView(this.mSelectedPosition);
        int k = localView.getMeasuredWidth();
        int m = i;
        switch (0x7 & this.mGravity)
        {
        default:
        case 1:
        case 5:
        }
        while (true)
        {
            localView.offsetLeftAndRight(m);
            this.mRecycler.clear();
            invalidate();
            checkSelectionChanged();
            this.mDataChanged = false;
            this.mNeedSync = false;
            setNextSelectedPositionInt(this.mSelectedPosition);
            break;
            m = i + j / 2 - k / 2;
            continue;
            m = i + j - k;
        }
    }

    int measureContentWidth(SpinnerAdapter paramSpinnerAdapter, Drawable paramDrawable)
    {
        int i;
        if (paramSpinnerAdapter == null)
            i = 0;
        while (true)
        {
            return i;
            i = 0;
            View localView = null;
            int j = 0;
            int k = View.MeasureSpec.makeMeasureSpec(0, 0);
            int m = View.MeasureSpec.makeMeasureSpec(0, 0);
            int n = Math.max(0, getSelectedItemPosition());
            int i1 = Math.min(paramSpinnerAdapter.getCount(), n + 15);
            for (int i2 = Math.max(0, n - (15 - (i1 - n))); i2 < i1; i2++)
            {
                int i3 = paramSpinnerAdapter.getItemViewType(i2);
                if (i3 != j)
                {
                    j = i3;
                    localView = null;
                }
                localView = paramSpinnerAdapter.getView(i2, localView, this);
                if (localView.getLayoutParams() == null)
                    localView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
                localView.measure(k, m);
                i = Math.max(i, localView.getMeasuredWidth());
            }
            if (paramDrawable != null)
            {
                paramDrawable.getPadding(this.mTempRect);
                i += this.mTempRect.left + this.mTempRect.right;
            }
        }
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        setSelection(paramInt);
        paramDialogInterface.dismiss();
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if ((this.mPopup != null) && (this.mPopup.isShowing()))
            this.mPopup.dismiss();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(Spinner.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(Spinner.class.getName());
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        this.mInLayout = true;
        layout(0, false);
        this.mInLayout = false;
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        if ((this.mPopup != null) && (View.MeasureSpec.getMode(paramInt1) == -2147483648))
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), measureContentWidth(getAdapter(), getBackground())), View.MeasureSpec.getSize(paramInt1)), getMeasuredHeight());
    }

    public boolean performClick()
    {
        boolean bool = super.performClick();
        if (!bool)
        {
            bool = true;
            if (!this.mPopup.isShowing())
                this.mPopup.show();
        }
        return bool;
    }

    public void setAdapter(SpinnerAdapter paramSpinnerAdapter)
    {
        super.setAdapter(paramSpinnerAdapter);
        if (this.mPopup != null)
            this.mPopup.setAdapter(new DropDownAdapter(paramSpinnerAdapter));
        while (true)
        {
            return;
            this.mTempAdapter = new DropDownAdapter(paramSpinnerAdapter);
        }
    }

    public void setDropDownHorizontalOffset(int paramInt)
    {
        this.mPopup.setHorizontalOffset(paramInt);
    }

    public void setDropDownVerticalOffset(int paramInt)
    {
        this.mPopup.setVerticalOffset(paramInt);
    }

    public void setDropDownWidth(int paramInt)
    {
        if (!(this.mPopup instanceof DropdownPopup))
            Log.e("Spinner", "Cannot set dropdown width for MODE_DIALOG, ignoring");
        while (true)
        {
            return;
            this.mDropDownWidth = paramInt;
        }
    }

    public void setEnabled(boolean paramBoolean)
    {
        super.setEnabled(paramBoolean);
        if (this.mDisableChildrenWhenDisabled)
        {
            int i = getChildCount();
            for (int j = 0; j < i; j++)
                getChildAt(j).setEnabled(paramBoolean);
        }
    }

    public void setGravity(int paramInt)
    {
        if (this.mGravity != paramInt)
        {
            if ((paramInt & 0x7) == 0)
                paramInt |= 3;
            this.mGravity = paramInt;
            requestLayout();
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
    {
        throw new RuntimeException("setOnItemClickListener cannot be used with a spinner.");
    }

    public void setOnItemClickListenerInt(AdapterView.OnItemClickListener paramOnItemClickListener)
    {
        super.setOnItemClickListener(paramOnItemClickListener);
    }

    public void setPopupBackgroundDrawable(Drawable paramDrawable)
    {
        if (!(this.mPopup instanceof DropdownPopup))
            Log.e("Spinner", "setPopupBackgroundDrawable: incompatible spinner mode; ignoring...");
        while (true)
        {
            return;
            ((DropdownPopup)this.mPopup).setBackgroundDrawable(paramDrawable);
        }
    }

    public void setPopupBackgroundResource(int paramInt)
    {
        setPopupBackgroundDrawable(getContext().getResources().getDrawable(paramInt));
    }

    public void setPrompt(CharSequence paramCharSequence)
    {
        this.mPopup.setPromptText(paramCharSequence);
    }

    public void setPromptId(int paramInt)
    {
        setPrompt(getContext().getText(paramInt));
    }

    private class DropdownPopup extends ListPopupWindow
        implements Spinner.SpinnerPopup
    {
        private ListAdapter mAdapter;
        private CharSequence mHintText;

        public DropdownPopup(Context paramAttributeSet, AttributeSet paramInt, int arg4)
        {
            super(paramInt, 0, i);
            setAnchorView(Spinner.this);
            setModal(true);
            setPromptPosition(0);
            setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                public void onItemClick(AdapterView paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                {
                    Spinner.this.setSelection(paramAnonymousInt);
                    if (Spinner.this.mOnItemClickListener != null)
                        Spinner.this.performItemClick(paramAnonymousView, paramAnonymousInt, Spinner.DropdownPopup.this.mAdapter.getItemId(paramAnonymousInt));
                    Spinner.DropdownPopup.this.dismiss();
                }
            });
        }

        public CharSequence getHintText()
        {
            return this.mHintText;
        }

        public void setAdapter(ListAdapter paramListAdapter)
        {
            super.setAdapter(paramListAdapter);
            this.mAdapter = paramListAdapter;
        }

        public void setPromptText(CharSequence paramCharSequence)
        {
            this.mHintText = paramCharSequence;
        }

        public void show()
        {
            Drawable localDrawable = getBackground();
            int i = 0;
            int j;
            if (localDrawable != null)
            {
                localDrawable.getPadding(Spinner.this.mTempRect);
                i = -Spinner.this.mTempRect.left;
                j = Spinner.this.getPaddingLeft();
                if (Spinner.this.mDropDownWidth != -2)
                    break label226;
                int n = Spinner.this.getWidth();
                int i1 = Spinner.this.getPaddingRight();
                int i2 = Spinner.this.measureContentWidth((SpinnerAdapter)this.mAdapter, getBackground());
                int i3 = Spinner.access$300(Spinner.this).getResources().getDisplayMetrics().widthPixels - Spinner.this.mTempRect.left - Spinner.this.mTempRect.right;
                if (i2 > i3)
                    i2 = i3;
                setContentWidth(Math.max(i2, n - j - i1));
            }
            while (true)
            {
                setHorizontalOffset(i + j);
                setInputMethodMode(2);
                super.show();
                getListView().setChoiceMode(1);
                setSelection(Spinner.this.getSelectedItemPosition());
                return;
                Rect localRect = Spinner.this.mTempRect;
                Spinner.this.mTempRect.right = 0;
                localRect.left = 0;
                break;
                label226: if (Spinner.this.mDropDownWidth == -1)
                {
                    int k = Spinner.this.getWidth();
                    int m = Spinner.this.getPaddingRight();
                    setContentWidth(k - j - m);
                }
                else
                {
                    setContentWidth(Spinner.this.mDropDownWidth);
                }
            }
        }
    }

    private class DialogPopup
        implements Spinner.SpinnerPopup, DialogInterface.OnClickListener
    {
        private ListAdapter mListAdapter;
        private AlertDialog mPopup;
        private CharSequence mPrompt;

        private DialogPopup()
        {
        }

        public void dismiss()
        {
            this.mPopup.dismiss();
            this.mPopup = null;
        }

        public Drawable getBackground()
        {
            return null;
        }

        public CharSequence getHintText()
        {
            return this.mPrompt;
        }

        public int getHorizontalOffset()
        {
            return 0;
        }

        public int getVerticalOffset()
        {
            return 0;
        }

        public boolean isShowing()
        {
            if (this.mPopup != null);
            for (boolean bool = this.mPopup.isShowing(); ; bool = false)
                return bool;
        }

        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
            Spinner.this.setSelection(paramInt);
            if (Spinner.this.mOnItemClickListener != null)
                Spinner.this.performItemClick(null, paramInt, this.mListAdapter.getItemId(paramInt));
            dismiss();
        }

        public void setAdapter(ListAdapter paramListAdapter)
        {
            this.mListAdapter = paramListAdapter;
        }

        public void setBackgroundDrawable(Drawable paramDrawable)
        {
            Log.e("Spinner", "Cannot set popup background for MODE_DIALOG, ignoring");
        }

        public void setHorizontalOffset(int paramInt)
        {
            Log.e("Spinner", "Cannot set horizontal offset for MODE_DIALOG, ignoring");
        }

        public void setPromptText(CharSequence paramCharSequence)
        {
            this.mPrompt = paramCharSequence;
        }

        public void setVerticalOffset(int paramInt)
        {
            Log.e("Spinner", "Cannot set vertical offset for MODE_DIALOG, ignoring");
        }

        public void show()
        {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(Spinner.this.getContext());
            if (this.mPrompt != null)
                localBuilder.setTitle(this.mPrompt);
            this.mPopup = localBuilder.setSingleChoiceItems(this.mListAdapter, Spinner.this.getSelectedItemPosition(), this).show();
        }
    }

    private static abstract interface SpinnerPopup
    {
        public abstract void dismiss();

        public abstract Drawable getBackground();

        public abstract CharSequence getHintText();

        public abstract int getHorizontalOffset();

        public abstract int getVerticalOffset();

        public abstract boolean isShowing();

        public abstract void setAdapter(ListAdapter paramListAdapter);

        public abstract void setBackgroundDrawable(Drawable paramDrawable);

        public abstract void setHorizontalOffset(int paramInt);

        public abstract void setPromptText(CharSequence paramCharSequence);

        public abstract void setVerticalOffset(int paramInt);

        public abstract void show();
    }

    private static class DropDownAdapter
        implements ListAdapter, SpinnerAdapter
    {
        private SpinnerAdapter mAdapter;
        private ListAdapter mListAdapter;

        public DropDownAdapter(SpinnerAdapter paramSpinnerAdapter)
        {
            this.mAdapter = paramSpinnerAdapter;
            if ((paramSpinnerAdapter instanceof SpinnerAdapter))
                this.mListAdapter = ((SpinnerAdapter)paramSpinnerAdapter);
        }

        public boolean areAllItemsEnabled()
        {
            ListAdapter localListAdapter = this.mListAdapter;
            if (localListAdapter != null);
            for (boolean bool = localListAdapter.areAllItemsEnabled(); ; bool = true)
                return bool;
        }

        public int getCount()
        {
            if (this.mAdapter == null);
            for (int i = 0; ; i = this.mAdapter.getCount())
                return i;
        }

        public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            if (this.mAdapter == null);
            for (View localView = null; ; localView = this.mAdapter.getDropDownView(paramInt, paramView, paramViewGroup))
                return localView;
        }

        public Object getItem(int paramInt)
        {
            if (this.mAdapter == null);
            for (Object localObject = null; ; localObject = this.mAdapter.getItem(paramInt))
                return localObject;
        }

        public long getItemId(int paramInt)
        {
            if (this.mAdapter == null);
            for (long l = -1L; ; l = this.mAdapter.getItemId(paramInt))
                return l;
        }

        public int getItemViewType(int paramInt)
        {
            return 0;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            return getDropDownView(paramInt, paramView, paramViewGroup);
        }

        public int getViewTypeCount()
        {
            return 1;
        }

        public boolean hasStableIds()
        {
            if ((this.mAdapter != null) && (this.mAdapter.hasStableIds()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isEmpty()
        {
            if (getCount() == 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isEnabled(int paramInt)
        {
            ListAdapter localListAdapter = this.mListAdapter;
            if (localListAdapter != null);
            for (boolean bool = localListAdapter.isEnabled(paramInt); ; bool = true)
                return bool;
        }

        public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
        {
            if (this.mAdapter != null)
                this.mAdapter.registerDataSetObserver(paramDataSetObserver);
        }

        public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
        {
            if (this.mAdapter != null)
                this.mAdapter.unregisterDataSetObserver(paramDataSetObserver);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Spinner
 * JD-Core Version:        0.6.2
 */