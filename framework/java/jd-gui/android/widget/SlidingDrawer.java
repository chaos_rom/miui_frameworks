package android.widget;

import android.R.styleable;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class SlidingDrawer extends ViewGroup
{
    private static final int ANIMATION_FRAME_DURATION = 16;
    private static final int COLLAPSED_FULL_CLOSED = -10002;
    private static final int EXPANDED_FULL_OPEN = -10001;
    private static final float MAXIMUM_ACCELERATION = 2000.0F;
    private static final float MAXIMUM_MAJOR_VELOCITY = 200.0F;
    private static final float MAXIMUM_MINOR_VELOCITY = 150.0F;
    private static final float MAXIMUM_TAP_VELOCITY = 100.0F;
    private static final int MSG_ANIMATE = 1000;
    public static final int ORIENTATION_HORIZONTAL = 0;
    public static final int ORIENTATION_VERTICAL = 1;
    private static final int TAP_THRESHOLD = 6;
    private static final int VELOCITY_UNITS = 1000;
    private boolean mAllowSingleTap;
    private boolean mAnimateOnClick;
    private float mAnimatedAcceleration;
    private float mAnimatedVelocity;
    private boolean mAnimating;
    private long mAnimationLastTime;
    private float mAnimationPosition;
    private int mBottomOffset;
    private View mContent;
    private final int mContentId;
    private long mCurrentAnimationTime;
    private boolean mExpanded;
    private final Rect mFrame = new Rect();
    private View mHandle;
    private int mHandleHeight;
    private final int mHandleId;
    private int mHandleWidth;
    private final Handler mHandler = new SlidingHandler(null);
    private final Rect mInvalidate = new Rect();
    private boolean mLocked;
    private final int mMaximumAcceleration;
    private final int mMaximumMajorVelocity;
    private final int mMaximumMinorVelocity;
    private final int mMaximumTapVelocity;
    private OnDrawerCloseListener mOnDrawerCloseListener;
    private OnDrawerOpenListener mOnDrawerOpenListener;
    private OnDrawerScrollListener mOnDrawerScrollListener;
    private final int mTapThreshold;
    private int mTopOffset;
    private int mTouchDelta;
    private boolean mTracking;
    private VelocityTracker mVelocityTracker;
    private final int mVelocityUnits;
    private boolean mVertical;

    public SlidingDrawer(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public SlidingDrawer(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SlidingDrawer, paramInt, 0);
        if (localTypedArray.getInt(0, 1) == 1);
        int i;
        for (boolean bool = true; ; bool = false)
        {
            this.mVertical = bool;
            this.mBottomOffset = ((int)localTypedArray.getDimension(1, 0.0F));
            this.mTopOffset = ((int)localTypedArray.getDimension(2, 0.0F));
            this.mAllowSingleTap = localTypedArray.getBoolean(3, true);
            this.mAnimateOnClick = localTypedArray.getBoolean(6, true);
            i = localTypedArray.getResourceId(4, 0);
            if (i != 0)
                break;
            throw new IllegalArgumentException("The handle attribute is required and must refer to a valid child.");
        }
        int j = localTypedArray.getResourceId(5, 0);
        if (j == 0)
            throw new IllegalArgumentException("The content attribute is required and must refer to a valid child.");
        if (i == j)
            throw new IllegalArgumentException("The content and handle attributes must refer to different children.");
        this.mHandleId = i;
        this.mContentId = j;
        float f = getResources().getDisplayMetrics().density;
        this.mTapThreshold = ((int)(0.5F + 6.0F * f));
        this.mMaximumTapVelocity = ((int)(0.5F + 100.0F * f));
        this.mMaximumMinorVelocity = ((int)(0.5F + 150.0F * f));
        this.mMaximumMajorVelocity = ((int)(0.5F + 200.0F * f));
        this.mMaximumAcceleration = ((int)(0.5F + 2000.0F * f));
        this.mVelocityUnits = ((int)(0.5F + 1000.0F * f));
        localTypedArray.recycle();
        setAlwaysDrawnWithCacheEnabled(false);
    }

    private void animateClose(int paramInt)
    {
        prepareTracking(paramInt);
        performFling(paramInt, this.mMaximumAcceleration, true);
    }

    private void animateOpen(int paramInt)
    {
        prepareTracking(paramInt);
        performFling(paramInt, -this.mMaximumAcceleration, true);
    }

    private void closeDrawer()
    {
        moveHandle(-10002);
        this.mContent.setVisibility(8);
        this.mContent.destroyDrawingCache();
        if (!this.mExpanded);
        while (true)
        {
            return;
            this.mExpanded = false;
            if (this.mOnDrawerCloseListener != null)
                this.mOnDrawerCloseListener.onDrawerClosed();
        }
    }

    private void doAnimation()
    {
        int j;
        if (this.mAnimating)
        {
            incrementAnimation();
            float f = this.mAnimationPosition;
            int i = this.mBottomOffset;
            if (!this.mVertical)
                break label55;
            j = getHeight();
            if (f < -1 + (j + i))
                break label63;
            this.mAnimating = false;
            closeDrawer();
        }
        while (true)
        {
            return;
            label55: j = getWidth();
            break;
            label63: if (this.mAnimationPosition < this.mTopOffset)
            {
                this.mAnimating = false;
                openDrawer();
            }
            else
            {
                moveHandle((int)this.mAnimationPosition);
                this.mCurrentAnimationTime = (16L + this.mCurrentAnimationTime);
                this.mHandler.sendMessageAtTime(this.mHandler.obtainMessage(1000), this.mCurrentAnimationTime);
            }
        }
    }

    private void incrementAnimation()
    {
        long l = SystemClock.uptimeMillis();
        float f1 = (float)(l - this.mAnimationLastTime) / 1000.0F;
        float f2 = this.mAnimationPosition;
        float f3 = this.mAnimatedVelocity;
        float f4 = this.mAnimatedAcceleration;
        this.mAnimationPosition = (f2 + f3 * f1 + f1 * (f1 * (0.5F * f4)));
        this.mAnimatedVelocity = (f3 + f4 * f1);
        this.mAnimationLastTime = l;
    }

    private void moveHandle(int paramInt)
    {
        View localView = this.mHandle;
        if (this.mVertical)
            if (paramInt == -10001)
            {
                localView.offsetTopAndBottom(this.mTopOffset - localView.getTop());
                invalidate();
            }
        while (true)
        {
            return;
            if (paramInt == -10002)
            {
                localView.offsetTopAndBottom(this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - localView.getTop());
                invalidate();
            }
            else
            {
                int k = localView.getTop();
                int m = paramInt - k;
                if (paramInt < this.mTopOffset)
                    m = this.mTopOffset - k;
                while (true)
                {
                    localView.offsetTopAndBottom(m);
                    Rect localRect3 = this.mFrame;
                    Rect localRect4 = this.mInvalidate;
                    localView.getHitRect(localRect3);
                    localRect4.set(localRect3);
                    localRect4.union(localRect3.left, localRect3.top - m, localRect3.right, localRect3.bottom - m);
                    localRect4.union(0, localRect3.bottom - m, getWidth(), localRect3.bottom - m + this.mContent.getHeight());
                    invalidate(localRect4);
                    break;
                    if (m > this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - k)
                        m = this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - k;
                }
                if (paramInt == -10001)
                {
                    localView.offsetLeftAndRight(this.mTopOffset - localView.getLeft());
                    invalidate();
                }
                else
                {
                    if (paramInt != -10002)
                        break;
                    localView.offsetLeftAndRight(this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - localView.getLeft());
                    invalidate();
                }
            }
        }
        int i = localView.getLeft();
        int j = paramInt - i;
        if (paramInt < this.mTopOffset)
            j = this.mTopOffset - i;
        while (true)
        {
            localView.offsetLeftAndRight(j);
            Rect localRect1 = this.mFrame;
            Rect localRect2 = this.mInvalidate;
            localView.getHitRect(localRect1);
            localRect2.set(localRect1);
            localRect2.union(localRect1.left - j, localRect1.top, localRect1.right - j, localRect1.bottom);
            localRect2.union(localRect1.right - j, 0, localRect1.right - j + this.mContent.getWidth(), getHeight());
            invalidate(localRect2);
            break;
            if (j > this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - i)
                j = this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - i;
        }
    }

    private void openDrawer()
    {
        moveHandle(-10001);
        this.mContent.setVisibility(0);
        if (this.mExpanded);
        while (true)
        {
            return;
            this.mExpanded = true;
            if (this.mOnDrawerOpenListener != null)
                this.mOnDrawerOpenListener.onDrawerOpened();
        }
    }

    private void performFling(int paramInt, float paramFloat, boolean paramBoolean)
    {
        this.mAnimationPosition = paramInt;
        this.mAnimatedVelocity = paramFloat;
        int k;
        if (this.mExpanded)
            if ((!paramBoolean) && (paramFloat <= this.mMaximumMajorVelocity))
            {
                int j = this.mTopOffset;
                if (this.mVertical)
                {
                    k = this.mHandleHeight;
                    if ((paramInt <= k + j) || (paramFloat <= -this.mMaximumMajorVelocity))
                        break label163;
                }
            }
            else
            {
                this.mAnimatedAcceleration = this.mMaximumAcceleration;
                if (paramFloat < 0.0F)
                    this.mAnimatedVelocity = 0.0F;
            }
        while (true)
        {
            long l = SystemClock.uptimeMillis();
            this.mAnimationLastTime = l;
            this.mCurrentAnimationTime = (16L + l);
            this.mAnimating = true;
            this.mHandler.removeMessages(1000);
            this.mHandler.sendMessageAtTime(this.mHandler.obtainMessage(1000), this.mCurrentAnimationTime);
            stopTracking();
            return;
            k = this.mHandleWidth;
            break;
            label163: this.mAnimatedAcceleration = (-this.mMaximumAcceleration);
            if (paramFloat > 0.0F)
            {
                this.mAnimatedVelocity = 0.0F;
                continue;
                if (!paramBoolean)
                {
                    if (paramFloat <= this.mMaximumMajorVelocity)
                        if (!this.mVertical)
                            break label256;
                    label256: for (int i = getHeight(); ; i = getWidth())
                    {
                        if ((paramInt <= i / 2) || (paramFloat <= -this.mMaximumMajorVelocity))
                            break label265;
                        this.mAnimatedAcceleration = this.mMaximumAcceleration;
                        if (paramFloat >= 0.0F)
                            break;
                        this.mAnimatedVelocity = 0.0F;
                        break;
                    }
                }
                label265: this.mAnimatedAcceleration = (-this.mMaximumAcceleration);
                if (paramFloat > 0.0F)
                    this.mAnimatedVelocity = 0.0F;
            }
        }
    }

    private void prepareContent()
    {
        if (this.mAnimating)
            return;
        View localView = this.mContent;
        if (localView.isLayoutRequested())
        {
            if (!this.mVertical)
                break label135;
            int j = this.mHandleHeight;
            int k = this.mBottom - this.mTop - j - this.mTopOffset;
            localView.measure(View.MeasureSpec.makeMeasureSpec(this.mRight - this.mLeft, 1073741824), View.MeasureSpec.makeMeasureSpec(k, 1073741824));
            localView.layout(0, j + this.mTopOffset, localView.getMeasuredWidth(), j + this.mTopOffset + localView.getMeasuredHeight());
        }
        while (true)
        {
            localView.getViewTreeObserver().dispatchOnPreDraw();
            if (!localView.isHardwareAccelerated())
                localView.buildDrawingCache();
            localView.setVisibility(8);
            break;
            label135: int i = this.mHandle.getWidth();
            localView.measure(View.MeasureSpec.makeMeasureSpec(this.mRight - this.mLeft - i - this.mTopOffset, 1073741824), View.MeasureSpec.makeMeasureSpec(this.mBottom - this.mTop, 1073741824));
            localView.layout(i + this.mTopOffset, 0, i + this.mTopOffset + localView.getMeasuredWidth(), localView.getMeasuredHeight());
        }
    }

    private void prepareTracking(int paramInt)
    {
        this.mTracking = true;
        this.mVelocityTracker = VelocityTracker.obtain();
        int i;
        int k;
        if (!this.mExpanded)
        {
            i = 1;
            if (i == 0)
                break label145;
            this.mAnimatedAcceleration = this.mMaximumAcceleration;
            this.mAnimatedVelocity = this.mMaximumMajorVelocity;
            int j = this.mBottomOffset;
            if (!this.mVertical)
                break label131;
            k = getHeight() - this.mHandleHeight;
            label66: this.mAnimationPosition = (k + j);
            moveHandle((int)this.mAnimationPosition);
            this.mAnimating = true;
            this.mHandler.removeMessages(1000);
            long l = SystemClock.uptimeMillis();
            this.mAnimationLastTime = l;
            this.mCurrentAnimationTime = (16L + l);
            this.mAnimating = true;
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label131: k = getWidth() - this.mHandleWidth;
            break label66;
            label145: if (this.mAnimating)
            {
                this.mAnimating = false;
                this.mHandler.removeMessages(1000);
            }
            moveHandle(paramInt);
        }
    }

    private void stopTracking()
    {
        this.mHandle.setPressed(false);
        this.mTracking = false;
        if (this.mOnDrawerScrollListener != null)
            this.mOnDrawerScrollListener.onScrollEnded();
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    public void animateClose()
    {
        prepareContent();
        OnDrawerScrollListener localOnDrawerScrollListener = this.mOnDrawerScrollListener;
        if (localOnDrawerScrollListener != null)
            localOnDrawerScrollListener.onScrollStarted();
        if (this.mVertical);
        for (int i = this.mHandle.getTop(); ; i = this.mHandle.getLeft())
        {
            animateClose(i);
            if (localOnDrawerScrollListener != null)
                localOnDrawerScrollListener.onScrollEnded();
            return;
        }
    }

    public void animateOpen()
    {
        prepareContent();
        OnDrawerScrollListener localOnDrawerScrollListener = this.mOnDrawerScrollListener;
        if (localOnDrawerScrollListener != null)
            localOnDrawerScrollListener.onScrollStarted();
        if (this.mVertical);
        for (int i = this.mHandle.getTop(); ; i = this.mHandle.getLeft())
        {
            animateOpen(i);
            sendAccessibilityEvent(32);
            if (localOnDrawerScrollListener != null)
                localOnDrawerScrollListener.onScrollEnded();
            return;
        }
    }

    public void animateToggle()
    {
        if (!this.mExpanded)
            animateOpen();
        while (true)
        {
            return;
            animateClose();
        }
    }

    public void close()
    {
        closeDrawer();
        invalidate();
        requestLayout();
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        float f1 = 0.0F;
        long l = getDrawingTime();
        View localView = this.mHandle;
        boolean bool = this.mVertical;
        drawChild(paramCanvas, localView, l);
        Bitmap localBitmap;
        if ((this.mTracking) || (this.mAnimating))
        {
            localBitmap = this.mContent.getDrawingCache();
            if (localBitmap != null)
                if (bool)
                    paramCanvas.drawBitmap(localBitmap, 0.0F, localView.getBottom(), null);
        }
        while (true)
        {
            return;
            paramCanvas.drawBitmap(localBitmap, localView.getRight(), 0.0F, null);
            continue;
            paramCanvas.save();
            if (bool);
            for (float f2 = 0.0F; ; f2 = localView.getLeft() - this.mTopOffset)
            {
                if (bool)
                    f1 = localView.getTop() - this.mTopOffset;
                paramCanvas.translate(f2, f1);
                drawChild(paramCanvas, this.mContent, l);
                paramCanvas.restore();
                break;
            }
            if (this.mExpanded)
                drawChild(paramCanvas, this.mContent, l);
        }
    }

    public View getContent()
    {
        return this.mContent;
    }

    public View getHandle()
    {
        return this.mHandle;
    }

    public boolean isMoving()
    {
        if ((this.mTracking) || (this.mAnimating));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isOpened()
    {
        return this.mExpanded;
    }

    public void lock()
    {
        this.mLocked = true;
    }

    protected void onFinishInflate()
    {
        this.mHandle = findViewById(this.mHandleId);
        if (this.mHandle == null)
            throw new IllegalArgumentException("The handle attribute is must refer to an existing child.");
        this.mHandle.setOnClickListener(new DrawerToggler(null));
        this.mContent = findViewById(this.mContentId);
        if (this.mContent == null)
            throw new IllegalArgumentException("The content attribute is must refer to an existing child.");
        this.mContent.setVisibility(8);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(SlidingDrawer.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(SlidingDrawer.class.getName());
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if (this.mLocked);
        int i;
        float f1;
        float f2;
        Rect localRect;
        View localView;
        do
        {
            return bool;
            i = paramMotionEvent.getAction();
            f1 = paramMotionEvent.getX();
            f2 = paramMotionEvent.getY();
            localRect = this.mFrame;
            localView = this.mHandle;
            localView.getHitRect(localRect);
        }
        while ((!this.mTracking) && (!localRect.contains((int)f1, (int)f2)));
        if (i == 0)
        {
            this.mTracking = true;
            localView.setPressed(true);
            prepareContent();
            if (this.mOnDrawerScrollListener != null)
                this.mOnDrawerScrollListener.onScrollStarted();
            if (!this.mVertical)
                break label148;
            int k = this.mHandle.getTop();
            this.mTouchDelta = ((int)f2 - k);
            prepareTracking(k);
        }
        while (true)
        {
            this.mVelocityTracker.addMovement(paramMotionEvent);
            bool = true;
            break;
            label148: int j = this.mHandle.getLeft();
            this.mTouchDelta = ((int)f1 - j);
            prepareTracking(j);
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (this.mTracking)
            return;
        int i = paramInt3 - paramInt1;
        int j = paramInt4 - paramInt2;
        View localView1 = this.mHandle;
        int k = localView1.getMeasuredWidth();
        int m = localView1.getMeasuredHeight();
        View localView2 = this.mContent;
        int i1;
        if (this.mVertical)
        {
            n = (i - k) / 2;
            if (this.mExpanded);
            for (i1 = this.mTopOffset; ; i1 = j - m + this.mBottomOffset)
            {
                localView2.layout(0, m + this.mTopOffset, localView2.getMeasuredWidth(), m + this.mTopOffset + localView2.getMeasuredHeight());
                localView1.layout(n, i1, n + k, i1 + m);
                this.mHandleHeight = localView1.getHeight();
                this.mHandleWidth = localView1.getWidth();
                break;
            }
        }
        if (this.mExpanded);
        for (int n = this.mTopOffset; ; n = i - k + this.mBottomOffset)
        {
            i1 = (j - m) / 2;
            localView2.layout(k + this.mTopOffset, 0, k + this.mTopOffset + localView2.getMeasuredWidth(), localView2.getMeasuredHeight());
            break;
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getSize(paramInt1);
        int k = View.MeasureSpec.getMode(paramInt2);
        int m = View.MeasureSpec.getSize(paramInt2);
        if ((i == 0) || (k == 0))
            throw new RuntimeException("SlidingDrawer cannot have UNSPECIFIED dimensions");
        View localView = this.mHandle;
        measureChild(localView, paramInt1, paramInt2);
        if (this.mVertical)
        {
            int i1 = m - localView.getMeasuredHeight() - this.mTopOffset;
            this.mContent.measure(View.MeasureSpec.makeMeasureSpec(j, 1073741824), View.MeasureSpec.makeMeasureSpec(i1, 1073741824));
        }
        while (true)
        {
            setMeasuredDimension(j, m);
            return;
            int n = j - localView.getMeasuredWidth() - this.mTopOffset;
            this.mContent.measure(View.MeasureSpec.makeMeasureSpec(n, 1073741824), View.MeasureSpec.makeMeasureSpec(m, 1073741824));
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool1;
        if (this.mLocked)
            bool1 = true;
        while (true)
        {
            return bool1;
            if (this.mTracking)
                this.mVelocityTracker.addMovement(paramMotionEvent);
            switch (paramMotionEvent.getAction())
            {
            default:
                if ((this.mTracking) || (this.mAnimating) || (super.onTouchEvent(paramMotionEvent)))
                    bool1 = true;
                break;
            case 2:
                if (this.mVertical);
                for (float f4 = paramMotionEvent.getY(); ; f4 = paramMotionEvent.getX())
                {
                    moveHandle((int)f4 - this.mTouchDelta);
                    break;
                }
            case 1:
            case 3:
                VelocityTracker localVelocityTracker = this.mVelocityTracker;
                localVelocityTracker.computeCurrentVelocity(this.mVelocityUnits);
                float f1 = localVelocityTracker.getYVelocity();
                float f2 = localVelocityTracker.getXVelocity();
                boolean bool2 = this.mVertical;
                int i;
                label166: float f3;
                int j;
                int k;
                if (bool2)
                    if (f1 < 0.0F)
                    {
                        i = 1;
                        if (f2 < 0.0F)
                            f2 = -f2;
                        if (f2 > this.mMaximumMinorVelocity)
                            f2 = this.mMaximumMinorVelocity;
                        f3 = (float)Math.hypot(f2, f1);
                        if (i != 0)
                            f3 = -f3;
                        j = this.mHandle.getTop();
                        k = this.mHandle.getLeft();
                        if (Math.abs(f3) >= this.mMaximumTapVelocity)
                            break label533;
                        if (!bool2)
                            break label400;
                        if (((!this.mExpanded) || (j >= this.mTapThreshold + this.mTopOffset)) && ((this.mExpanded) || (j <= this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - this.mTapThreshold)))
                            break label457;
                    }
                while (true)
                    if (this.mAllowSingleTap)
                    {
                        playSoundEffect(0);
                        if (this.mExpanded)
                            if (bool2)
                            {
                                label336: animateClose(j);
                                break;
                                i = 0;
                                break label166;
                                if (f2 < 0.0F);
                                for (i = 1; ; i = 0)
                                {
                                    if (f1 < 0.0F)
                                        f1 = -f1;
                                    if (f1 <= this.mMaximumMinorVelocity)
                                        break;
                                    f1 = this.mMaximumMinorVelocity;
                                    break;
                                }
                                label400: if (((this.mExpanded) && (k < this.mTapThreshold + this.mTopOffset)) || ((!this.mExpanded) && (k > this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - this.mTapThreshold)))
                                    continue;
                                label457: if (!bool2)
                                    break label526;
                            }
                    }
                while (true)
                {
                    performFling(j, f3, false);
                    break;
                    j = k;
                    break label336;
                    if (bool2);
                    while (true)
                    {
                        animateOpen(j);
                        break;
                        j = k;
                    }
                    if (bool2);
                    while (true)
                    {
                        performFling(j, f3, false);
                        break;
                        j = k;
                    }
                    label526: j = k;
                }
                label533: if (bool2);
                while (true)
                {
                    performFling(j, f3, false);
                    break;
                    j = k;
                }
                bool1 = false;
            }
        }
    }

    public void open()
    {
        openDrawer();
        invalidate();
        requestLayout();
        sendAccessibilityEvent(32);
    }

    public void setOnDrawerCloseListener(OnDrawerCloseListener paramOnDrawerCloseListener)
    {
        this.mOnDrawerCloseListener = paramOnDrawerCloseListener;
    }

    public void setOnDrawerOpenListener(OnDrawerOpenListener paramOnDrawerOpenListener)
    {
        this.mOnDrawerOpenListener = paramOnDrawerOpenListener;
    }

    public void setOnDrawerScrollListener(OnDrawerScrollListener paramOnDrawerScrollListener)
    {
        this.mOnDrawerScrollListener = paramOnDrawerScrollListener;
    }

    public void toggle()
    {
        if (!this.mExpanded)
            openDrawer();
        while (true)
        {
            invalidate();
            requestLayout();
            return;
            closeDrawer();
        }
    }

    public void unlock()
    {
        this.mLocked = false;
    }

    private class SlidingHandler extends Handler
    {
        private SlidingHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1000:
            }
            while (true)
            {
                return;
                SlidingDrawer.this.doAnimation();
            }
        }
    }

    private class DrawerToggler
        implements View.OnClickListener
    {
        private DrawerToggler()
        {
        }

        public void onClick(View paramView)
        {
            if (SlidingDrawer.this.mLocked);
            while (true)
            {
                return;
                if (SlidingDrawer.this.mAnimateOnClick)
                    SlidingDrawer.this.animateToggle();
                else
                    SlidingDrawer.this.toggle();
            }
        }
    }

    public static abstract interface OnDrawerScrollListener
    {
        public abstract void onScrollEnded();

        public abstract void onScrollStarted();
    }

    public static abstract interface OnDrawerCloseListener
    {
        public abstract void onDrawerClosed();
    }

    public static abstract interface OnDrawerOpenListener
    {
        public abstract void onDrawerOpened();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SlidingDrawer
 * JD-Core Version:        0.6.2
 */