package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.QuickContact;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class QuickContactBadge extends ImageView
    implements View.OnClickListener
{
    static final int EMAIL_ID_COLUMN_INDEX = 0;
    static final String[] EMAIL_LOOKUP_PROJECTION;
    static final int EMAIL_LOOKUP_STRING_COLUMN_INDEX = 1;
    static final int PHONE_ID_COLUMN_INDEX = 0;
    static final String[] PHONE_LOOKUP_PROJECTION = arrayOfString2;
    static final int PHONE_LOOKUP_STRING_COLUMN_INDEX = 1;
    private static final int TOKEN_EMAIL_LOOKUP = 0;
    private static final int TOKEN_EMAIL_LOOKUP_AND_TRIGGER = 2;
    private static final int TOKEN_PHONE_LOOKUP = 1;
    private static final int TOKEN_PHONE_LOOKUP_AND_TRIGGER = 3;
    private String mContactEmail;
    private String mContactPhone;
    private Uri mContactUri;
    private Drawable mDefaultAvatar;
    protected String[] mExcludeMimes = null;
    private Drawable mOverlay;
    private QueryHandler mQueryHandler;

    static
    {
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "contact_id";
        arrayOfString1[1] = "lookup";
        EMAIL_LOOKUP_PROJECTION = arrayOfString1;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "lookup";
    }

    public QuickContactBadge(Context paramContext)
    {
        this(paramContext, null);
    }

    public QuickContactBadge(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public QuickContactBadge(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = this.mContext.obtainStyledAttributes(R.styleable.Theme);
        this.mOverlay = localTypedArray.getDrawable(229);
        localTypedArray.recycle();
        this.mQueryHandler = new QueryHandler(this.mContext.getContentResolver());
        setOnClickListener(this);
    }

    private boolean isAssigned()
    {
        if ((this.mContactUri != null) || (this.mContactEmail != null) || (this.mContactPhone != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void onContactUriChanged()
    {
        setEnabled(isAssigned());
    }

    public void assignContactFromEmail(String paramString, boolean paramBoolean)
    {
        this.mContactEmail = paramString;
        if (!paramBoolean)
            this.mQueryHandler.startQuery(0, null, Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, Uri.encode(this.mContactEmail)), EMAIL_LOOKUP_PROJECTION, null, null, null);
        while (true)
        {
            return;
            this.mContactUri = null;
            onContactUriChanged();
        }
    }

    public void assignContactFromPhone(String paramString, boolean paramBoolean)
    {
        this.mContactPhone = paramString;
        if (!paramBoolean)
            this.mQueryHandler.startQuery(1, null, Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, this.mContactPhone), PHONE_LOOKUP_PROJECTION, null, null, null);
        while (true)
        {
            return;
            this.mContactUri = null;
            onContactUriChanged();
        }
    }

    public void assignContactUri(Uri paramUri)
    {
        this.mContactUri = paramUri;
        this.mContactEmail = null;
        this.mContactPhone = null;
        onContactUriChanged();
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if ((this.mOverlay != null) && (this.mOverlay.isStateful()))
        {
            this.mOverlay.setState(getDrawableState());
            invalidate();
        }
    }

    public void onClick(View paramView)
    {
        if (this.mContactUri != null)
            ContactsContract.QuickContact.showQuickContact(getContext(), this, this.mContactUri, 3, this.mExcludeMimes);
        while (true)
        {
            return;
            if (this.mContactEmail != null)
                this.mQueryHandler.startQuery(2, this.mContactEmail, Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, Uri.encode(this.mContactEmail)), EMAIL_LOOKUP_PROJECTION, null, null, null);
            else if (this.mContactPhone != null)
                this.mQueryHandler.startQuery(3, this.mContactPhone, Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, this.mContactPhone), PHONE_LOOKUP_PROJECTION, null, null, null);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        if (!isEnabled());
        while (true)
        {
            return;
            if ((this.mOverlay != null) && (this.mOverlay.getIntrinsicWidth() != 0) && (this.mOverlay.getIntrinsicHeight() != 0))
            {
                this.mOverlay.setBounds(0, 0, getWidth() - this.mPaddingRight, getHeight() - this.mPaddingBottom);
                if ((this.mPaddingTop == 0) && (this.mPaddingLeft == 0))
                {
                    this.mOverlay.draw(paramCanvas);
                }
                else
                {
                    int i = paramCanvas.getSaveCount();
                    paramCanvas.save();
                    paramCanvas.translate(this.mPaddingLeft, this.mPaddingTop);
                    this.mOverlay.draw(paramCanvas);
                    paramCanvas.restoreToCount(i);
                }
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(QuickContactBadge.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(QuickContactBadge.class.getName());
    }

    public void setExcludeMimes(String[] paramArrayOfString)
    {
        this.mExcludeMimes = paramArrayOfString;
    }

    public void setImageToDefault()
    {
        if (this.mDefaultAvatar == null)
            this.mDefaultAvatar = getResources().getDrawable(17302209);
        setImageDrawable(this.mDefaultAvatar);
    }

    public void setMode(int paramInt)
    {
    }

    private class QueryHandler extends AsyncQueryHandler
    {
        public QueryHandler(ContentResolver arg2)
        {
            super();
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        protected void onQueryComplete(int paramInt, Object paramObject, Cursor paramCursor)
        {
            Object localObject1 = null;
            Uri localUri1 = null;
            boolean bool = false;
            Uri localUri3;
            switch (paramInt)
            {
            default:
                if (paramCursor != null)
                    paramCursor.close();
                QuickContactBadge.access$002(QuickContactBadge.this, (Uri)localObject1);
                QuickContactBadge.this.onContactUriChanged();
                localUri3 = QuickContactBadge.Injector.showQuickContactForStranger(QuickContactBadge.this, bool, (Uri)localObject1, localUri1);
                if ((bool) && (localObject1 != null))
                    ContactsContract.QuickContact.showQuickContact(QuickContactBadge.this.getContext(), QuickContactBadge.this, (Uri)localObject1, 3, QuickContactBadge.this.mExcludeMimes);
                break;
            case 3:
            case 1:
            case 2:
            case 0:
            }
            while (true)
            {
                Intent localIntent;
                while (true)
                {
                    return;
                    bool = true;
                    try
                    {
                        localUri1 = Uri.fromParts("tel", (String)paramObject, null);
                        if ((paramCursor == null) || (!paramCursor.moveToFirst()))
                            break;
                        localObject1 = ContactsContract.Contacts.getLookupUri(paramCursor.getLong(0), paramCursor.getString(1));
                        break;
                        bool = true;
                        localUri1 = Uri.fromParts("mailto", (String)paramObject, null);
                        if ((paramCursor == null) || (!paramCursor.moveToFirst()))
                            break;
                        Uri localUri2 = ContactsContract.Contacts.getLookupUri(paramCursor.getLong(0), paramCursor.getString(1));
                        localObject1 = localUri2;
                    }
                    finally
                    {
                        if (paramCursor != null)
                            paramCursor.close();
                    }
                }
                QuickContactBadge.this.getContext().startActivity(localIntent);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static Uri showQuickContactForStranger(QuickContactBadge paramQuickContactBadge, boolean paramBoolean, Uri paramUri1, Uri paramUri2)
        {
            if ((paramBoolean) && (paramUri1 != null));
            while (true)
            {
                return paramUri2;
                ContactsContract.QuickContact.showQuickContact(paramQuickContactBadge.getContext(), paramQuickContactBadge, paramUri2, 3, paramQuickContactBadge.mExcludeMimes);
                paramUri2 = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.QuickContactBadge
 * JD-Core Version:        0.6.2
 */