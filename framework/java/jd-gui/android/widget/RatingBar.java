package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class RatingBar extends AbsSeekBar
{
    private int mNumStars = 5;
    private OnRatingBarChangeListener mOnRatingBarChangeListener;
    private int mProgressOnStartTracking;

    public RatingBar(Context paramContext)
    {
        this(paramContext, null);
    }

    public RatingBar(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842876);
    }

    public RatingBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RatingBar, paramInt, 0);
        int i = localTypedArray.getInt(0, this.mNumStars);
        if (!this.mIsUserSeekable)
            bool = true;
        setIsIndicator(localTypedArray.getBoolean(3, bool));
        float f1 = localTypedArray.getFloat(1, -1.0F);
        float f2 = localTypedArray.getFloat(2, -1.0F);
        localTypedArray.recycle();
        if ((i > 0) && (i != this.mNumStars))
            setNumStars(i);
        if (f2 >= 0.0F)
            setStepSize(f2);
        while (true)
        {
            if (f1 >= 0.0F)
                setRating(f1);
            this.mTouchProgressOffset = 1.1F;
            return;
            setStepSize(0.5F);
        }
    }

    private float getProgressPerStar()
    {
        float f = 1.0F;
        if (this.mNumStars > 0)
            f = f * getMax() / this.mNumStars;
        return f;
    }

    private void updateSecondaryProgress(int paramInt)
    {
        float f = getProgressPerStar();
        if (f > 0.0F)
            setSecondaryProgress((int)(Math.ceil(paramInt / f) * f));
    }

    void dispatchRatingChange(boolean paramBoolean)
    {
        if (this.mOnRatingBarChangeListener != null)
            this.mOnRatingBarChangeListener.onRatingChanged(this, getRating(), paramBoolean);
    }

    Shape getDrawableShape()
    {
        return new RectShape();
    }

    public int getNumStars()
    {
        return this.mNumStars;
    }

    public OnRatingBarChangeListener getOnRatingBarChangeListener()
    {
        return this.mOnRatingBarChangeListener;
    }

    public float getRating()
    {
        return getProgress() / getProgressPerStar();
    }

    public float getStepSize()
    {
        return getNumStars() / getMax();
    }

    public boolean isIndicator()
    {
        if (!this.mIsUserSeekable);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(RatingBar.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(RatingBar.class.getName());
    }

    void onKeyChange()
    {
        super.onKeyChange();
        dispatchRatingChange(true);
    }

    /** @deprecated */
    protected void onMeasure(int paramInt1, int paramInt2)
    {
        try
        {
            super.onMeasure(paramInt1, paramInt2);
            if (this.mSampleTile != null)
                setMeasuredDimension(resolveSizeAndState(this.mSampleTile.getWidth() * this.mNumStars, paramInt1, 0), getMeasuredHeight());
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void onProgressRefresh(float paramFloat, boolean paramBoolean)
    {
        super.onProgressRefresh(paramFloat, paramBoolean);
        updateSecondaryProgress(getProgress());
        if (!paramBoolean)
            dispatchRatingChange(false);
    }

    void onStartTrackingTouch()
    {
        this.mProgressOnStartTracking = getProgress();
        super.onStartTrackingTouch();
    }

    void onStopTrackingTouch()
    {
        super.onStopTrackingTouch();
        if (getProgress() != this.mProgressOnStartTracking)
            dispatchRatingChange(true);
    }

    public void setIsIndicator(boolean paramBoolean)
    {
        boolean bool1 = true;
        boolean bool2;
        if (!paramBoolean)
        {
            bool2 = bool1;
            this.mIsUserSeekable = bool2;
            if (paramBoolean)
                break label28;
        }
        while (true)
        {
            setFocusable(bool1);
            return;
            bool2 = false;
            break;
            label28: bool1 = false;
        }
    }

    /** @deprecated */
    public void setMax(int paramInt)
    {
        if (paramInt <= 0);
        while (true)
        {
            return;
            try
            {
                super.setMax(paramInt);
            }
            finally
            {
            }
        }
    }

    public void setNumStars(int paramInt)
    {
        if (paramInt <= 0);
        while (true)
        {
            return;
            this.mNumStars = paramInt;
            requestLayout();
        }
    }

    public void setOnRatingBarChangeListener(OnRatingBarChangeListener paramOnRatingBarChangeListener)
    {
        this.mOnRatingBarChangeListener = paramOnRatingBarChangeListener;
    }

    public void setRating(float paramFloat)
    {
        setProgress(Math.round(paramFloat * getProgressPerStar()));
    }

    public void setStepSize(float paramFloat)
    {
        if (paramFloat <= 0.0F);
        while (true)
        {
            return;
            float f = this.mNumStars / paramFloat;
            int i = (int)(f / getMax() * getProgress());
            setMax((int)f);
            setProgress(i);
        }
    }

    public static abstract interface OnRatingBarChangeListener
    {
        public abstract void onRatingChanged(RatingBar paramRatingBar, float paramFloat, boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.RatingBar
 * JD-Core Version:        0.6.2
 */