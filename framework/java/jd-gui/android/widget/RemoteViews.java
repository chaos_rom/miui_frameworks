package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityOptions;
import android.app.PendingIntent;
import android.appwidget.AppWidgetHostView;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Filter;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class RemoteViews
    implements Parcelable, LayoutInflater.Filter
{
    public static final Parcelable.Creator<RemoteViews> CREATOR = new Parcelable.Creator()
    {
        public RemoteViews createFromParcel(Parcel paramAnonymousParcel)
        {
            return new RemoteViews(paramAnonymousParcel);
        }

        public RemoteViews[] newArray(int paramAnonymousInt)
        {
            return new RemoteViews[paramAnonymousInt];
        }
    };
    private static final OnClickHandler DEFAULT_ON_CLICK_HANDLER = new OnClickHandler();
    static final String EXTRA_REMOTEADAPTER_APPWIDGET_ID = "remoteAdapterAppWidgetId";
    private static final String LOG_TAG = "RemoteViews";
    private static final int MODE_HAS_LANDSCAPE_AND_PORTRAIT = 1;
    private static final int MODE_NORMAL;
    private ArrayList<Action> mActions;
    private BitmapCache mBitmapCache;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    int mDefaultTheme;
    private boolean mIsRoot;
    private boolean mIsWidgetCollectionChild;
    private RemoteViews mLandscape;
    private final int mLayoutId;
    private MemoryUsageCounter mMemoryUsageCounter;
    private final String mPackage;
    private RemoteViews mPortrait;

    public RemoteViews(Parcel paramParcel)
    {
        this(paramParcel, null);
    }

    private RemoteViews(Parcel paramParcel, BitmapCache paramBitmapCache)
    {
        this.mIsRoot = i;
        this.mLandscape = null;
        this.mPortrait = null;
        this.mIsWidgetCollectionChild = false;
        int j = paramParcel.readInt();
        if (paramBitmapCache == null)
        {
            this.mBitmapCache = new BitmapCache(paramParcel);
            label48: if (j != 0)
                break label513;
            this.mPackage = paramParcel.readString();
            this.mLayoutId = paramParcel.readInt();
            if (paramParcel.readInt() != i)
                break label241;
        }
        int m;
        while (true)
        {
            this.mIsWidgetCollectionChild = i;
            int k = paramParcel.readInt();
            if (k <= 0)
                break label567;
            this.mActions = new ArrayList(k);
            m = 0;
            if (m >= k)
                break label567;
            int n = paramParcel.readInt();
            switch (n)
            {
            case 7:
            default:
                throw new ActionException("Tag " + n + " not found");
                setBitmapCache(paramBitmapCache);
                setNotRoot();
                break label48;
                label241: i = 0;
            case 1:
            case 3:
            case 2:
            case 4:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 12:
            }
        }
        this.mActions.add(new SetOnClickPendingIntent(paramParcel));
        while (true)
        {
            m++;
            break;
            this.mActions.add(new SetDrawableParameters(paramParcel));
            continue;
            this.mActions.add(new ReflectionAction(paramParcel));
            continue;
            this.mActions.add(new ViewGroupAction(paramParcel, this.mBitmapCache));
            continue;
            this.mActions.add(new ReflectionActionWithoutParams(paramParcel));
            continue;
            this.mActions.add(new SetEmptyView(paramParcel));
            continue;
            this.mActions.add(new SetPendingIntentTemplate(paramParcel));
            continue;
            this.mActions.add(new SetOnClickFillInIntent(paramParcel));
            continue;
            this.mActions.add(new SetRemoteViewsAdapterIntent(paramParcel));
            continue;
            this.mActions.add(new TextViewDrawableAction(paramParcel));
            continue;
            this.mActions.add(new TextViewSizeAction(paramParcel));
            continue;
            this.mActions.add(new ViewPaddingAction(paramParcel));
            continue;
            this.mActions.add(new BitmapReflectionAction(paramParcel));
        }
        label513: this.mLandscape = new RemoteViews(paramParcel, this.mBitmapCache);
        this.mPortrait = new RemoteViews(paramParcel, this.mBitmapCache);
        this.mPackage = this.mPortrait.getPackage();
        this.mLayoutId = this.mPortrait.getLayoutId();
        label567: this.mMemoryUsageCounter = new MemoryUsageCounter(null);
        recalculateMemoryUsage();
    }

    public RemoteViews(RemoteViews paramRemoteViews1, RemoteViews paramRemoteViews2)
    {
        this.mIsRoot = true;
        this.mLandscape = null;
        this.mPortrait = null;
        this.mIsWidgetCollectionChild = false;
        if ((paramRemoteViews1 == null) || (paramRemoteViews2 == null))
            throw new RuntimeException("Both RemoteViews must be non-null");
        if (paramRemoteViews1.getPackage().compareTo(paramRemoteViews2.getPackage()) != 0)
            throw new RuntimeException("Both RemoteViews must share the same package");
        this.mPackage = paramRemoteViews2.getPackage();
        this.mLayoutId = paramRemoteViews2.getLayoutId();
        this.mLandscape = paramRemoteViews1;
        this.mPortrait = paramRemoteViews2;
        this.mMemoryUsageCounter = new MemoryUsageCounter(null);
        this.mBitmapCache = new BitmapCache();
        configureRemoteViewsAsChild(paramRemoteViews1);
        configureRemoteViewsAsChild(paramRemoteViews2);
        recalculateMemoryUsage();
    }

    public RemoteViews(String paramString, int paramInt)
    {
        this.mIsRoot = true;
        this.mLandscape = null;
        this.mPortrait = null;
        this.mIsWidgetCollectionChild = false;
        this.mPackage = paramString;
        this.mLayoutId = paramInt;
        this.mBitmapCache = new BitmapCache();
        this.mMemoryUsageCounter = new MemoryUsageCounter(null);
        recalculateMemoryUsage();
    }

    private void addAction(Action paramAction)
    {
        if (hasLandscapeAndPortraitLayouts())
            throw new RuntimeException("RemoteViews specifying separate landscape and portrait layouts cannot be modified. Instead, fully configure the landscape and portrait layouts individually before constructing the combined layout.");
        if (this.mActions == null)
            this.mActions = new ArrayList();
        this.mActions.add(paramAction);
        paramAction.updateMemoryUsageEstimate(this.mMemoryUsageCounter);
    }

    private void configureRemoteViewsAsChild(RemoteViews paramRemoteViews)
    {
        this.mBitmapCache.assimilate(paramRemoteViews.mBitmapCache);
        paramRemoteViews.setBitmapCache(this.mBitmapCache);
        paramRemoteViews.setNotRoot();
    }

    private RemoteViews getRemoteViewsToApply(Context paramContext)
    {
        if (hasLandscapeAndPortraitLayouts())
            if (paramContext.getResources().getConfiguration().orientation != 2)
                break label28;
        label28: for (this = this.mLandscape; ; this = this.mPortrait)
            return this;
    }

    private boolean hasLandscapeAndPortraitLayouts()
    {
        if ((this.mLandscape != null) && (this.mPortrait != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void performApply(View paramView, ViewGroup paramViewGroup, OnClickHandler paramOnClickHandler)
    {
        if (this.mActions != null)
        {
            int i = this.mActions.size();
            for (int j = 0; j < i; j++)
                ((Action)this.mActions.get(j)).apply(paramView, paramViewGroup, paramOnClickHandler);
        }
    }

    private Context prepareContext(Context paramContext)
    {
        String str = this.mPackage;
        if (str != null);
        while (true)
        {
            try
            {
                Context localContext2 = paramContext.createPackageContext(str, 4);
                localContext1 = localContext2;
                return localContext1;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                Log.e("RemoteViews", "Package name " + str + " not found");
                localContext1 = paramContext;
                continue;
            }
            Context localContext1 = paramContext;
        }
    }

    private void recalculateMemoryUsage()
    {
        this.mMemoryUsageCounter.clear();
        if (!hasLandscapeAndPortraitLayouts())
        {
            if (this.mActions != null)
            {
                int i = this.mActions.size();
                for (int j = 0; j < i; j++)
                    ((Action)this.mActions.get(j)).updateMemoryUsageEstimate(this.mMemoryUsageCounter);
            }
            if (this.mIsRoot)
                this.mBitmapCache.addBitmapMemory(this.mMemoryUsageCounter);
        }
        while (true)
        {
            return;
            this.mMemoryUsageCounter.increment(this.mLandscape.estimateMemoryUsage());
            this.mMemoryUsageCounter.increment(this.mPortrait.estimateMemoryUsage());
            this.mBitmapCache.addBitmapMemory(this.mMemoryUsageCounter);
        }
    }

    private void setBitmapCache(BitmapCache paramBitmapCache)
    {
        this.mBitmapCache = paramBitmapCache;
        if (!hasLandscapeAndPortraitLayouts())
        {
            if (this.mActions != null)
            {
                int i = this.mActions.size();
                for (int j = 0; j < i; j++)
                    ((Action)this.mActions.get(j)).setBitmapCache(paramBitmapCache);
            }
        }
        else
        {
            this.mLandscape.setBitmapCache(paramBitmapCache);
            this.mPortrait.setBitmapCache(paramBitmapCache);
        }
    }

    public void addView(int paramInt, RemoteViews paramRemoteViews)
    {
        addAction(new ViewGroupAction(paramInt, paramRemoteViews));
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public View apply(Context paramContext, ViewGroup paramViewGroup)
    {
        return apply(paramContext, paramViewGroup, DEFAULT_ON_CLICK_HANDLER);
    }

    public View apply(Context paramContext, ViewGroup paramViewGroup, OnClickHandler paramOnClickHandler)
    {
        RemoteViews localRemoteViews = getRemoteViewsToApply(paramContext);
        Context localContext = prepareContext(paramContext);
        localContext.setTheme(this.mDefaultTheme);
        LayoutInflater localLayoutInflater = ((LayoutInflater)localContext.getSystemService("layout_inflater")).cloneInContext(localContext);
        localLayoutInflater.setFilter(this);
        View localView = localLayoutInflater.inflate(localRemoteViews.getLayoutId(), paramViewGroup, false);
        localRemoteViews.performApply(localView, paramViewGroup, paramOnClickHandler);
        return localView;
    }

    public RemoteViews clone()
    {
        RemoteViews localRemoteViews;
        if (!hasLandscapeAndPortraitLayouts())
        {
            localRemoteViews = new RemoteViews(this.mPackage, this.mLayoutId);
            if (this.mActions != null)
                localRemoteViews.mActions = ((ArrayList)this.mActions.clone());
        }
        while (true)
        {
            localRemoteViews.recalculateMemoryUsage();
            return localRemoteViews;
            localRemoteViews = new RemoteViews(this.mLandscape.clone(), this.mPortrait.clone());
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int estimateMemoryUsage()
    {
        return this.mMemoryUsageCounter.getMemoryUsage();
    }

    public int getLayoutId()
    {
        return this.mLayoutId;
    }

    public String getPackage()
    {
        return this.mPackage;
    }

    public boolean onLoadClass(Class paramClass)
    {
        return paramClass.isAnnotationPresent(RemoteView.class);
    }

    public void reapply(Context paramContext, View paramView)
    {
        reapply(paramContext, paramView, DEFAULT_ON_CLICK_HANDLER);
    }

    public void reapply(Context paramContext, View paramView, OnClickHandler paramOnClickHandler)
    {
        RemoteViews localRemoteViews = getRemoteViewsToApply(paramContext);
        if ((hasLandscapeAndPortraitLayouts()) && (paramView.getId() != localRemoteViews.getLayoutId()))
            throw new RuntimeException("Attempting to re-apply RemoteViews to a view that that does not share the same root layout id.");
        prepareContext(paramContext);
        localRemoteViews.performApply(paramView, (ViewGroup)paramView.getParent(), paramOnClickHandler);
    }

    public void removeAllViews(int paramInt)
    {
        addAction(new ViewGroupAction(paramInt, null));
    }

    public void setBitmap(int paramInt, String paramString, Bitmap paramBitmap)
    {
        addAction(new BitmapReflectionAction(paramInt, paramString, paramBitmap));
    }

    public void setBoolean(int paramInt, String paramString, boolean paramBoolean)
    {
        addAction(new ReflectionAction(paramInt, paramString, 1, Boolean.valueOf(paramBoolean)));
    }

    public void setBundle(int paramInt, String paramString, Bundle paramBundle)
    {
        addAction(new ReflectionAction(paramInt, paramString, 13, paramBundle));
    }

    public void setByte(int paramInt, String paramString, byte paramByte)
    {
        addAction(new ReflectionAction(paramInt, paramString, 2, Byte.valueOf(paramByte)));
    }

    public void setChar(int paramInt, String paramString, char paramChar)
    {
        addAction(new ReflectionAction(paramInt, paramString, 8, Character.valueOf(paramChar)));
    }

    public void setCharSequence(int paramInt, String paramString, CharSequence paramCharSequence)
    {
        addAction(new ReflectionAction(paramInt, paramString, 10, paramCharSequence));
    }

    public void setChronometer(int paramInt, long paramLong, String paramString, boolean paramBoolean)
    {
        setLong(paramInt, "setBase", paramLong);
        setString(paramInt, "setFormat", paramString);
        setBoolean(paramInt, "setStarted", paramBoolean);
    }

    public void setContentDescription(int paramInt, CharSequence paramCharSequence)
    {
        setCharSequence(paramInt, "setContentDescription", paramCharSequence);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setDefaultTheme(int paramInt)
    {
        this.mDefaultTheme = paramInt;
    }

    public void setDisplayedChild(int paramInt1, int paramInt2)
    {
        setInt(paramInt1, "setDisplayedChild", paramInt2);
    }

    public void setDouble(int paramInt, String paramString, double paramDouble)
    {
        addAction(new ReflectionAction(paramInt, paramString, 7, Double.valueOf(paramDouble)));
    }

    public void setDrawableParameters(int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, PorterDuff.Mode paramMode, int paramInt4)
    {
        addAction(new SetDrawableParameters(paramInt1, paramBoolean, paramInt2, paramInt3, paramMode, paramInt4));
    }

    public void setEmptyView(int paramInt1, int paramInt2)
    {
        addAction(new SetEmptyView(paramInt1, paramInt2));
    }

    public void setFloat(int paramInt, String paramString, float paramFloat)
    {
        addAction(new ReflectionAction(paramInt, paramString, 6, Float.valueOf(paramFloat)));
    }

    public void setImageViewBitmap(int paramInt, Bitmap paramBitmap)
    {
        setBitmap(paramInt, "setImageBitmap", paramBitmap);
    }

    public void setImageViewResource(int paramInt1, int paramInt2)
    {
        setInt(paramInt1, "setImageResource", paramInt2);
    }

    public void setImageViewUri(int paramInt, Uri paramUri)
    {
        setUri(paramInt, "setImageURI", paramUri);
    }

    public void setInt(int paramInt1, String paramString, int paramInt2)
    {
        addAction(new ReflectionAction(paramInt1, paramString, 4, Integer.valueOf(paramInt2)));
    }

    public void setIntent(int paramInt, String paramString, Intent paramIntent)
    {
        addAction(new ReflectionAction(paramInt, paramString, 14, paramIntent));
    }

    void setIsWidgetCollectionChild(boolean paramBoolean)
    {
        this.mIsWidgetCollectionChild = paramBoolean;
    }

    public void setLong(int paramInt, String paramString, long paramLong)
    {
        addAction(new ReflectionAction(paramInt, paramString, 5, Long.valueOf(paramLong)));
    }

    void setNotRoot()
    {
        this.mIsRoot = false;
    }

    public void setOnClickFillInIntent(int paramInt, Intent paramIntent)
    {
        addAction(new SetOnClickFillInIntent(paramInt, paramIntent));
    }

    public void setOnClickPendingIntent(int paramInt, PendingIntent paramPendingIntent)
    {
        addAction(new SetOnClickPendingIntent(paramInt, paramPendingIntent));
    }

    public void setPendingIntentTemplate(int paramInt, PendingIntent paramPendingIntent)
    {
        addAction(new SetPendingIntentTemplate(paramInt, paramPendingIntent));
    }

    public void setProgressBar(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        setBoolean(paramInt1, "setIndeterminate", paramBoolean);
        if (!paramBoolean)
        {
            setInt(paramInt1, "setMax", paramInt2);
            setInt(paramInt1, "setProgress", paramInt3);
        }
    }

    public void setRelativeScrollPosition(int paramInt1, int paramInt2)
    {
        setInt(paramInt1, "smoothScrollByOffset", paramInt2);
    }

    @Deprecated
    public void setRemoteAdapter(int paramInt1, int paramInt2, Intent paramIntent)
    {
        setRemoteAdapter(paramInt2, paramIntent);
    }

    public void setRemoteAdapter(int paramInt, Intent paramIntent)
    {
        addAction(new SetRemoteViewsAdapterIntent(paramInt, paramIntent));
    }

    public void setScrollPosition(int paramInt1, int paramInt2)
    {
        setInt(paramInt1, "smoothScrollToPosition", paramInt2);
    }

    public void setShort(int paramInt, String paramString, short paramShort)
    {
        addAction(new ReflectionAction(paramInt, paramString, 3, Short.valueOf(paramShort)));
    }

    public void setString(int paramInt, String paramString1, String paramString2)
    {
        addAction(new ReflectionAction(paramInt, paramString1, 9, paramString2));
    }

    public void setTextColor(int paramInt1, int paramInt2)
    {
        setInt(paramInt1, "setTextColor", paramInt2);
    }

    public void setTextViewCompoundDrawables(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        addAction(new TextViewDrawableAction(paramInt1, false, paramInt2, paramInt3, paramInt4, paramInt5));
    }

    public void setTextViewCompoundDrawablesRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        addAction(new TextViewDrawableAction(paramInt1, true, paramInt2, paramInt3, paramInt4, paramInt5));
    }

    public void setTextViewText(int paramInt, CharSequence paramCharSequence)
    {
        setCharSequence(paramInt, "setText", paramCharSequence);
    }

    public void setTextViewTextSize(int paramInt1, int paramInt2, float paramFloat)
    {
        addAction(new TextViewSizeAction(paramInt1, paramInt2, paramFloat));
    }

    public void setUri(int paramInt, String paramString, Uri paramUri)
    {
        addAction(new ReflectionAction(paramInt, paramString, 11, paramUri));
    }

    public void setViewPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        addAction(new ViewPaddingAction(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5));
    }

    public void setViewVisibility(int paramInt1, int paramInt2)
    {
        setInt(paramInt1, "setVisibility", paramInt2);
    }

    public void showNext(int paramInt)
    {
        addAction(new ReflectionActionWithoutParams(paramInt, "showNext"));
    }

    public void showPrevious(int paramInt)
    {
        addAction(new ReflectionActionWithoutParams(paramInt, "showPrevious"));
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        if (!hasLandscapeAndPortraitLayouts())
        {
            paramParcel.writeInt(0);
            if (this.mIsRoot)
                this.mBitmapCache.writeBitmapsToParcel(paramParcel, paramInt);
            paramParcel.writeString(this.mPackage);
            paramParcel.writeInt(this.mLayoutId);
            if (this.mIsWidgetCollectionChild)
            {
                paramParcel.writeInt(i);
                if (this.mActions == null)
                    break label118;
            }
            label118: for (int j = this.mActions.size(); ; j = 0)
            {
                paramParcel.writeInt(j);
                for (int k = 0; k < j; k++)
                    ((Action)this.mActions.get(k)).writeToParcel(paramParcel, 0);
                i = 0;
                break;
            }
        }
        paramParcel.writeInt(i);
        if (this.mIsRoot)
            this.mBitmapCache.writeBitmapsToParcel(paramParcel, paramInt);
        this.mLandscape.writeToParcel(paramParcel, paramInt);
        this.mPortrait.writeToParcel(paramParcel, paramInt);
    }

    private class MemoryUsageCounter
    {
        int mMemoryUsage;

        private MemoryUsageCounter()
        {
        }

        public void addBitmapMemory(Bitmap paramBitmap)
        {
            Bitmap.Config localConfig = paramBitmap.getConfig();
            int i = 4;
            if (localConfig != null)
                switch (RemoteViews.2.$SwitchMap$android$graphics$Bitmap$Config[localConfig.ordinal()])
                {
                default:
                case 1:
                case 2:
                case 3:
                case 4:
                }
            while (true)
            {
                increment(i * (paramBitmap.getWidth() * paramBitmap.getHeight()));
                return;
                i = 1;
                continue;
                i = 2;
                continue;
                i = 4;
            }
        }

        public void clear()
        {
            this.mMemoryUsage = 0;
        }

        public int getMemoryUsage()
        {
            return this.mMemoryUsage;
        }

        public void increment(int paramInt)
        {
            this.mMemoryUsage = (paramInt + this.mMemoryUsage);
        }
    }

    private class ViewPaddingAction extends RemoteViews.Action
    {
        public static final int TAG = 14;
        int bottom;
        int left;
        int right;
        int top;
        int viewId;

        public ViewPaddingAction(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int arg6)
        {
            super();
            this.viewId = paramInt1;
            this.left = paramInt2;
            this.top = paramInt3;
            this.right = paramInt4;
            int i;
            this.bottom = i;
        }

        public ViewPaddingAction(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            this.left = localObject.readInt();
            this.top = localObject.readInt();
            this.right = localObject.readInt();
            this.bottom = localObject.readInt();
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            paramView.getContext();
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                localView.setPadding(this.left, this.top, this.right, this.bottom);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(14);
            paramParcel.writeInt(this.viewId);
            paramParcel.writeInt(this.left);
            paramParcel.writeInt(this.top);
            paramParcel.writeInt(this.right);
            paramParcel.writeInt(this.bottom);
        }
    }

    private class TextViewSizeAction extends RemoteViews.Action
    {
        public static final int TAG = 13;
        float size;
        int units;
        int viewId;

        public TextViewSizeAction(int paramInt1, int paramFloat, float arg4)
        {
            super();
            this.viewId = paramInt1;
            this.units = paramFloat;
            Object localObject;
            this.size = localObject;
        }

        public TextViewSizeAction(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            this.units = localObject.readInt();
            this.size = localObject.readFloat();
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            paramView.getContext();
            TextView localTextView = (TextView)paramView.findViewById(this.viewId);
            if (localTextView == null);
            while (true)
            {
                return;
                localTextView.setTextSize(this.units, this.size);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(13);
            paramParcel.writeInt(this.viewId);
            paramParcel.writeInt(this.units);
            paramParcel.writeFloat(this.size);
        }
    }

    private class TextViewDrawableAction extends RemoteViews.Action
    {
        public static final int TAG = 11;
        int d1;
        int d2;
        int d3;
        int d4;
        boolean isRelative = false;
        int viewId;

        public TextViewDrawableAction(int paramBoolean, boolean paramInt1, int paramInt2, int paramInt3, int paramInt4, int arg7)
        {
            super();
            this.viewId = paramBoolean;
            this.isRelative = paramInt1;
            this.d1 = paramInt2;
            this.d2 = paramInt3;
            this.d3 = paramInt4;
            int i;
            this.d4 = i;
        }

        public TextViewDrawableAction(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            if (localObject.readInt() != 0)
                bool = true;
            this.isRelative = bool;
            this.d1 = localObject.readInt();
            this.d2 = localObject.readInt();
            this.d3 = localObject.readInt();
            this.d4 = localObject.readInt();
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            paramView.getContext();
            TextView localTextView = (TextView)paramView.findViewById(this.viewId);
            if (localTextView == null);
            while (true)
            {
                return;
                if (this.isRelative)
                    localTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(this.d1, this.d2, this.d3, this.d4);
                else
                    localTextView.setCompoundDrawablesWithIntrinsicBounds(this.d1, this.d2, this.d3, this.d4);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(11);
            paramParcel.writeInt(this.viewId);
            if (this.isRelative);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                paramParcel.writeInt(this.d1);
                paramParcel.writeInt(this.d2);
                paramParcel.writeInt(this.d3);
                paramParcel.writeInt(this.d4);
                return;
            }
        }
    }

    private class ViewGroupAction extends RemoteViews.Action
    {
        public static final int TAG = 4;
        RemoteViews nestedViews;
        int viewId;

        public ViewGroupAction(int paramRemoteViews, RemoteViews arg3)
        {
            super();
            this.viewId = paramRemoteViews;
            RemoteViews localRemoteViews;
            this.nestedViews = localRemoteViews;
            if (localRemoteViews != null)
                RemoteViews.this.configureRemoteViewsAsChild(localRemoteViews);
        }

        public ViewGroupAction(Parcel paramBitmapCache, RemoteViews.BitmapCache arg3)
        {
            super();
            this.viewId = paramBitmapCache.readInt();
            int i;
            if (paramBitmapCache.readInt() == 0)
            {
                i = 1;
                if (i != 0)
                    break label54;
            }
            RemoteViews.BitmapCache localBitmapCache;
            label54: for (this.nestedViews = new RemoteViews(paramBitmapCache, localBitmapCache, null); ; this.nestedViews = null)
            {
                return;
                i = 0;
                break;
            }
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            Context localContext = paramView.getContext();
            ViewGroup localViewGroup = (ViewGroup)paramView.findViewById(this.viewId);
            if (localViewGroup == null);
            while (true)
            {
                return;
                if (this.nestedViews != null)
                    localViewGroup.addView(this.nestedViews.apply(localContext, localViewGroup, paramOnClickHandler));
                else
                    localViewGroup.removeAllViews();
            }
        }

        public void setBitmapCache(RemoteViews.BitmapCache paramBitmapCache)
        {
            if (this.nestedViews != null)
                this.nestedViews.setBitmapCache(paramBitmapCache);
        }

        public void updateMemoryUsageEstimate(RemoteViews.MemoryUsageCounter paramMemoryUsageCounter)
        {
            if (this.nestedViews != null)
                paramMemoryUsageCounter.increment(this.nestedViews.estimateMemoryUsage());
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(4);
            paramParcel.writeInt(this.viewId);
            if (this.nestedViews != null)
            {
                paramParcel.writeInt(1);
                this.nestedViews.writeToParcel(paramParcel, paramInt);
            }
            while (true)
            {
                return;
                paramParcel.writeInt(0);
            }
        }
    }

    private class ReflectionAction extends RemoteViews.Action
    {
        static final int BITMAP = 12;
        static final int BOOLEAN = 1;
        static final int BUNDLE = 13;
        static final int BYTE = 2;
        static final int CHAR = 8;
        static final int CHAR_SEQUENCE = 10;
        static final int DOUBLE = 7;
        static final int FLOAT = 6;
        static final int INT = 4;
        static final int INTENT = 14;
        static final int LONG = 5;
        static final int SHORT = 3;
        static final int STRING = 9;
        static final int TAG = 2;
        static final int URI = 11;
        String methodName;
        int type;
        Object value;
        int viewId;

        ReflectionAction(int paramString, String paramInt1, int paramObject, Object arg5)
        {
            super();
            this.viewId = paramString;
            this.methodName = paramInt1;
            this.type = paramObject;
            Object localObject;
            this.value = localObject;
        }

        ReflectionAction(Parcel arg2)
        {
            super();
            Parcel localParcel;
            this.viewId = localParcel.readInt();
            this.methodName = localParcel.readString();
            this.type = localParcel.readInt();
            switch (this.type)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            }
            while (true)
            {
                return;
                if (localParcel.readInt() != 0);
                for (boolean bool = true; ; bool = false)
                {
                    this.value = Boolean.valueOf(bool);
                    break;
                }
                this.value = Byte.valueOf(localParcel.readByte());
                continue;
                this.value = Short.valueOf((short)localParcel.readInt());
                continue;
                this.value = Integer.valueOf(localParcel.readInt());
                continue;
                this.value = Long.valueOf(localParcel.readLong());
                continue;
                this.value = Float.valueOf(localParcel.readFloat());
                continue;
                this.value = Double.valueOf(localParcel.readDouble());
                continue;
                this.value = Character.valueOf((char)localParcel.readInt());
                continue;
                this.value = localParcel.readString();
                continue;
                this.value = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(localParcel);
                continue;
                if (localParcel.readInt() != 0)
                {
                    this.value = Uri.CREATOR.createFromParcel(localParcel);
                    continue;
                    if (localParcel.readInt() != 0)
                    {
                        this.value = Bitmap.CREATOR.createFromParcel(localParcel);
                        continue;
                        this.value = localParcel.readBundle();
                        continue;
                        if (localParcel.readInt() != 0)
                            this.value = Intent.CREATOR.createFromParcel(localParcel);
                    }
                }
            }
        }

        private Class getParameterType()
        {
            Object localObject;
            switch (this.type)
            {
            default:
                localObject = null;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            }
            while (true)
            {
                return localObject;
                localObject = Boolean.TYPE;
                continue;
                localObject = Byte.TYPE;
                continue;
                localObject = Short.TYPE;
                continue;
                localObject = Integer.TYPE;
                continue;
                localObject = Long.TYPE;
                continue;
                localObject = Float.TYPE;
                continue;
                localObject = Double.TYPE;
                continue;
                localObject = Character.TYPE;
                continue;
                localObject = String.class;
                continue;
                localObject = CharSequence.class;
                continue;
                localObject = Uri.class;
                continue;
                localObject = Bitmap.class;
                continue;
                localObject = Bundle.class;
                continue;
                localObject = Intent.class;
            }
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                Class localClass1 = getParameterType();
                if (localClass1 == null)
                    throw new RemoteViews.ActionException("bad type: " + this.type);
                Class localClass2 = localView.getClass();
                Method localMethod;
                try
                {
                    String str = this.methodName;
                    Class[] arrayOfClass = new Class[1];
                    arrayOfClass[0] = getParameterType();
                    localMethod = localClass2.getMethod(str, arrayOfClass);
                    if (!localMethod.isAnnotationPresent(RemotableViewMethod.class))
                        throw new RemoteViews.ActionException("view: " + localClass2.getName() + " can't use method with RemoteViews: " + this.methodName + "(" + localClass1.getName() + ")");
                }
                catch (NoSuchMethodException localNoSuchMethodException)
                {
                    throw new RemoteViews.ActionException("view: " + localClass2.getName() + " doesn't have method: " + this.methodName + "(" + localClass1.getName() + ")");
                }
                try
                {
                    Object[] arrayOfObject = new Object[1];
                    arrayOfObject[0] = this.value;
                    localMethod.invoke(localView, arrayOfObject);
                }
                catch (Exception localException)
                {
                    throw new RemoteViews.ActionException(localException);
                }
            }
        }

        public void updateMemoryUsageEstimate(RemoteViews.MemoryUsageCounter paramMemoryUsageCounter)
        {
            switch (this.type)
            {
            default:
            case 12:
            }
            while (true)
            {
                return;
                if (this.value != null)
                    paramMemoryUsageCounter.addBitmapMemory((Bitmap)this.value);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            int i = 1;
            paramParcel.writeInt(2);
            paramParcel.writeInt(this.viewId);
            paramParcel.writeString(this.methodName);
            paramParcel.writeInt(this.type);
            switch (this.type)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                while (true)
                {
                    return;
                    if (((Boolean)this.value).booleanValue());
                    for (int j = i; ; j = 0)
                    {
                        paramParcel.writeInt(j);
                        break;
                    }
                    paramParcel.writeByte(((Byte)this.value).byteValue());
                    continue;
                    paramParcel.writeInt(((Short)this.value).shortValue());
                    continue;
                    paramParcel.writeInt(((Integer)this.value).intValue());
                    continue;
                    paramParcel.writeLong(((Long)this.value).longValue());
                    continue;
                    paramParcel.writeFloat(((Float)this.value).floatValue());
                    continue;
                    paramParcel.writeDouble(((Double)this.value).doubleValue());
                    continue;
                    paramParcel.writeInt(((Character)this.value).charValue());
                    continue;
                    paramParcel.writeString((String)this.value);
                    continue;
                    TextUtils.writeToParcel((CharSequence)this.value, paramParcel, paramInt);
                    continue;
                    if (this.value != null);
                    while (true)
                    {
                        paramParcel.writeInt(i);
                        if (this.value == null)
                            break;
                        ((Uri)this.value).writeToParcel(paramParcel, paramInt);
                        break;
                        i = 0;
                    }
                    if (this.value != null);
                    while (true)
                    {
                        paramParcel.writeInt(i);
                        if (this.value == null)
                            break;
                        ((Bitmap)this.value).writeToParcel(paramParcel, paramInt);
                        break;
                        i = 0;
                    }
                    paramParcel.writeBundle((Bundle)this.value);
                }
            case 14:
            }
            if (this.value != null);
            while (true)
            {
                paramParcel.writeInt(i);
                if (this.value == null)
                    break;
                ((Intent)this.value).writeToParcel(paramParcel, paramInt);
                break;
                i = 0;
            }
        }
    }

    private class BitmapReflectionAction extends RemoteViews.Action
    {
        public static final int TAG = 12;
        Bitmap bitmap;
        int bitmapId;
        String methodName;
        int viewId;

        BitmapReflectionAction(int paramString, String paramBitmap, Bitmap arg4)
        {
            super();
            Bitmap localBitmap;
            this.bitmap = localBitmap;
            this.viewId = paramString;
            this.methodName = paramBitmap;
            this.bitmapId = RemoteViews.this.mBitmapCache.getBitmapId(localBitmap);
        }

        BitmapReflectionAction(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            this.methodName = localObject.readString();
            this.bitmapId = localObject.readInt();
            this.bitmap = RemoteViews.this.mBitmapCache.getBitmapForId(this.bitmapId);
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
            throws RemoteViews.ActionException
        {
            new RemoteViews.ReflectionAction(RemoteViews.this, this.viewId, this.methodName, 12, this.bitmap).apply(paramView, paramViewGroup, paramOnClickHandler);
        }

        public void setBitmapCache(RemoteViews.BitmapCache paramBitmapCache)
        {
            this.bitmapId = paramBitmapCache.getBitmapId(this.bitmap);
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(12);
            paramParcel.writeInt(this.viewId);
            paramParcel.writeString(this.methodName);
            paramParcel.writeInt(this.bitmapId);
        }
    }

    private static class BitmapCache
    {
        ArrayList<Bitmap> mBitmaps;

        public BitmapCache()
        {
            this.mBitmaps = new ArrayList();
        }

        public BitmapCache(Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            this.mBitmaps = new ArrayList();
            for (int j = 0; j < i; j++)
            {
                Bitmap localBitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel);
                this.mBitmaps.add(localBitmap);
            }
        }

        public void addBitmapMemory(RemoteViews.MemoryUsageCounter paramMemoryUsageCounter)
        {
            for (int i = 0; i < this.mBitmaps.size(); i++)
                paramMemoryUsageCounter.addBitmapMemory((Bitmap)this.mBitmaps.get(i));
        }

        public void assimilate(BitmapCache paramBitmapCache)
        {
            ArrayList localArrayList = paramBitmapCache.mBitmaps;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
            {
                Bitmap localBitmap = (Bitmap)localArrayList.get(j);
                if (!this.mBitmaps.contains(localBitmap))
                    this.mBitmaps.add(localBitmap);
            }
        }

        public Bitmap getBitmapForId(int paramInt)
        {
            if ((paramInt == -1) || (paramInt >= this.mBitmaps.size()));
            for (Bitmap localBitmap = null; ; localBitmap = (Bitmap)this.mBitmaps.get(paramInt))
                return localBitmap;
        }

        public int getBitmapId(Bitmap paramBitmap)
        {
            int i;
            if (paramBitmap == null)
                i = -1;
            while (true)
            {
                return i;
                if (this.mBitmaps.contains(paramBitmap))
                {
                    i = this.mBitmaps.indexOf(paramBitmap);
                }
                else
                {
                    this.mBitmaps.add(paramBitmap);
                    i = -1 + this.mBitmaps.size();
                }
            }
        }

        public void writeBitmapsToParcel(Parcel paramParcel, int paramInt)
        {
            int i = this.mBitmaps.size();
            paramParcel.writeInt(i);
            for (int j = 0; j < i; j++)
                ((Bitmap)this.mBitmaps.get(j)).writeToParcel(paramParcel, paramInt);
        }
    }

    private class ReflectionActionWithoutParams extends RemoteViews.Action
    {
        public static final int TAG = 5;
        String methodName;
        int viewId;

        ReflectionActionWithoutParams(int paramString, String arg3)
        {
            super();
            this.viewId = paramString;
            Object localObject;
            this.methodName = localObject;
        }

        ReflectionActionWithoutParams(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            this.methodName = localObject.readString();
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                Class localClass = localView.getClass();
                Method localMethod;
                try
                {
                    localMethod = localClass.getMethod(this.methodName, new Class[0]);
                    if (!localMethod.isAnnotationPresent(RemotableViewMethod.class))
                        throw new RemoteViews.ActionException("view: " + localClass.getName() + " can't use method with RemoteViews: " + this.methodName + "()");
                }
                catch (NoSuchMethodException localNoSuchMethodException)
                {
                    throw new RemoteViews.ActionException("view: " + localClass.getName() + " doesn't have method: " + this.methodName + "()");
                }
                try
                {
                    localMethod.invoke(localView, new Object[0]);
                }
                catch (Exception localException)
                {
                    throw new RemoteViews.ActionException(localException);
                }
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(5);
            paramParcel.writeInt(this.viewId);
            paramParcel.writeString(this.methodName);
        }
    }

    private class SetDrawableParameters extends RemoteViews.Action
    {
        public static final int TAG = 3;
        int alpha;
        int colorFilter;
        PorterDuff.Mode filterMode;
        int level;
        boolean targetBackground;
        int viewId;

        public SetDrawableParameters(int paramBoolean, boolean paramInt1, int paramInt2, int paramMode, PorterDuff.Mode paramInt3, int arg7)
        {
            super();
            this.viewId = paramBoolean;
            this.targetBackground = paramInt1;
            this.alpha = paramInt2;
            this.colorFilter = paramMode;
            this.filterMode = paramInt3;
            int i;
            this.level = i;
        }

        public SetDrawableParameters(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            boolean bool;
            int i;
            if (localObject.readInt() != 0)
            {
                bool = true;
                this.targetBackground = bool;
                this.alpha = localObject.readInt();
                this.colorFilter = localObject.readInt();
                if (localObject.readInt() == 0)
                    break label88;
                i = 1;
                label58: if (i == 0)
                    break label94;
            }
            label88: label94: for (this.filterMode = PorterDuff.Mode.valueOf(localObject.readString()); ; this.filterMode = null)
            {
                this.level = localObject.readInt();
                return;
                bool = false;
                break;
                i = 0;
                break label58;
            }
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            label126: 
            while (true)
            {
                return;
                Drawable localDrawable = null;
                if (this.targetBackground)
                    localDrawable = localView.getBackground();
                while (true)
                {
                    if (localDrawable == null)
                        break label126;
                    if (this.alpha != -1)
                        localDrawable.setAlpha(this.alpha);
                    if ((this.colorFilter != -1) && (this.filterMode != null))
                        localDrawable.setColorFilter(this.colorFilter, this.filterMode);
                    if (this.level == -1)
                        break;
                    localDrawable.setLevel(this.level);
                    break;
                    if ((localView instanceof ImageView))
                        localDrawable = ((ImageView)localView).getDrawable();
                }
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(3);
            paramParcel.writeInt(this.viewId);
            int i;
            if (this.targetBackground)
            {
                i = 1;
                paramParcel.writeInt(i);
                paramParcel.writeInt(this.alpha);
                paramParcel.writeInt(this.colorFilter);
                if (this.filterMode == null)
                    break label80;
                paramParcel.writeInt(1);
                paramParcel.writeString(this.filterMode.toString());
            }
            while (true)
            {
                paramParcel.writeInt(this.level);
                return;
                i = 0;
                break;
                label80: paramParcel.writeInt(0);
            }
        }
    }

    private class SetOnClickPendingIntent extends RemoteViews.Action
    {
        public static final int TAG = 1;
        PendingIntent pendingIntent;
        int viewId;

        public SetOnClickPendingIntent(int paramPendingIntent, PendingIntent arg3)
        {
            super();
            this.viewId = paramPendingIntent;
            Object localObject;
            this.pendingIntent = localObject;
        }

        public SetOnClickPendingIntent(Parcel arg2)
        {
            super();
            Parcel localParcel;
            this.viewId = localParcel.readInt();
            if (localParcel.readInt() != 0)
                this.pendingIntent = PendingIntent.readPendingIntentOrNullFromParcel(localParcel);
        }

        public void apply(View paramView, ViewGroup paramViewGroup, final RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                if (RemoteViews.this.mIsWidgetCollectionChild)
                {
                    Log.w("RemoteViews", "Cannot setOnClickPendingIntent for collection item (id: " + this.viewId + ")");
                    ApplicationInfo localApplicationInfo = paramView.getContext().getApplicationInfo();
                    if ((localApplicationInfo != null) && (localApplicationInfo.targetSdkVersion >= 16));
                }
                else if (localView != null)
                {
                    View.OnClickListener local1 = null;
                    if (this.pendingIntent != null)
                        local1 = new View.OnClickListener()
                        {
                            public void onClick(View paramAnonymousView)
                            {
                                float f = paramAnonymousView.getContext().getResources().getCompatibilityInfo().applicationScale;
                                int[] arrayOfInt = new int[2];
                                paramAnonymousView.getLocationOnScreen(arrayOfInt);
                                Rect localRect = new Rect();
                                localRect.left = ((int)(0.5F + f * arrayOfInt[0]));
                                localRect.top = ((int)(0.5F + f * arrayOfInt[1]));
                                localRect.right = ((int)(0.5F + f * (arrayOfInt[0] + paramAnonymousView.getWidth())));
                                localRect.bottom = ((int)(0.5F + f * (arrayOfInt[1] + paramAnonymousView.getHeight())));
                                Intent localIntent = new Intent();
                                localIntent.setSourceBounds(localRect);
                                paramOnClickHandler.onClickHandler(paramAnonymousView, RemoteViews.SetOnClickPendingIntent.this.pendingIntent, localIntent);
                            }
                        };
                    localView.setOnClickListener(local1);
                }
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            int i = 1;
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.viewId);
            if (this.pendingIntent != null);
            while (true)
            {
                paramParcel.writeInt(i);
                if (this.pendingIntent != null)
                    this.pendingIntent.writeToParcel(paramParcel, 0);
                return;
                i = 0;
            }
        }
    }

    private class SetRemoteViewsAdapterIntent extends RemoteViews.Action
    {
        public static final int TAG = 10;
        Intent intent;
        int viewId;

        public SetRemoteViewsAdapterIntent(int paramIntent, Intent arg3)
        {
            super();
            this.viewId = paramIntent;
            Object localObject;
            this.intent = localObject;
        }

        public SetRemoteViewsAdapterIntent(Parcel arg2)
        {
            super();
            Parcel localParcel;
            this.viewId = localParcel.readInt();
            this.intent = ((Intent)Intent.CREATOR.createFromParcel(localParcel));
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                if (!(paramViewGroup instanceof AppWidgetHostView))
                {
                    Log.e("RemoteViews", "SetRemoteViewsAdapterIntent action can only be used for AppWidgets (root id: " + this.viewId + ")");
                }
                else if ((!(localView instanceof AbsListView)) && (!(localView instanceof AdapterViewAnimator)))
                {
                    Log.e("RemoteViews", "Cannot setRemoteViewsAdapter on a view which is not an AbsListView or AdapterViewAnimator (id: " + this.viewId + ")");
                }
                else
                {
                    AppWidgetHostView localAppWidgetHostView = (AppWidgetHostView)paramViewGroup;
                    this.intent.putExtra("remoteAdapterAppWidgetId", localAppWidgetHostView.getAppWidgetId());
                    if ((localView instanceof AbsListView))
                        ((AbsListView)localView).setRemoteViewsAdapter(this.intent);
                    else if ((localView instanceof AdapterViewAnimator))
                        ((AdapterViewAnimator)localView).setRemoteViewsAdapter(this.intent);
                }
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(10);
            paramParcel.writeInt(this.viewId);
            this.intent.writeToParcel(paramParcel, paramInt);
        }
    }

    private class SetPendingIntentTemplate extends RemoteViews.Action
    {
        public static final int TAG = 8;
        PendingIntent pendingIntentTemplate;
        int viewId;

        public SetPendingIntentTemplate(int paramPendingIntent, PendingIntent arg3)
        {
            super();
            this.viewId = paramPendingIntent;
            Object localObject;
            this.pendingIntentTemplate = localObject;
        }

        public SetPendingIntentTemplate(Parcel arg2)
        {
            super();
            Parcel localParcel;
            this.viewId = localParcel.readInt();
            this.pendingIntentTemplate = PendingIntent.readPendingIntentOrNullFromParcel(localParcel);
        }

        public void apply(View paramView, ViewGroup paramViewGroup, final RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                if ((localView instanceof AdapterView))
                {
                    AdapterView localAdapterView = (AdapterView)localView;
                    localAdapterView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                        {
                            ViewGroup localViewGroup;
                            if ((paramAnonymousView instanceof ViewGroup))
                            {
                                localViewGroup = (ViewGroup)paramAnonymousView;
                                if ((paramAnonymousAdapterView instanceof AdapterViewAnimator))
                                    localViewGroup = (ViewGroup)localViewGroup.getChildAt(0);
                                if (localViewGroup != null);
                            }
                            else
                            {
                                return;
                            }
                            Intent localIntent = null;
                            int i = localViewGroup.getChildCount();
                            for (int j = 0; ; j++)
                                if (j < i)
                                {
                                    Object localObject = localViewGroup.getChildAt(j).getTag(16908857);
                                    if ((localObject instanceof Intent))
                                        localIntent = (Intent)localObject;
                                }
                                else
                                {
                                    if (localIntent == null)
                                        break;
                                    float f = paramAnonymousView.getContext().getResources().getCompatibilityInfo().applicationScale;
                                    int[] arrayOfInt = new int[2];
                                    paramAnonymousView.getLocationOnScreen(arrayOfInt);
                                    Rect localRect = new Rect();
                                    localRect.left = ((int)(0.5F + f * arrayOfInt[0]));
                                    localRect.top = ((int)(0.5F + f * arrayOfInt[1]));
                                    localRect.right = ((int)(0.5F + f * (arrayOfInt[0] + paramAnonymousView.getWidth())));
                                    localRect.bottom = ((int)(0.5F + f * (arrayOfInt[1] + paramAnonymousView.getHeight())));
                                    new Intent().setSourceBounds(localRect);
                                    paramOnClickHandler.onClickHandler(paramAnonymousView, RemoteViews.SetPendingIntentTemplate.this.pendingIntentTemplate, localIntent);
                                    break;
                                }
                        }
                    });
                    localAdapterView.setTag(this.pendingIntentTemplate);
                }
                else
                {
                    Log.e("RemoteViews", "Cannot setPendingIntentTemplate on a view which is notan AdapterView (id: " + this.viewId + ")");
                }
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(8);
            paramParcel.writeInt(this.viewId);
            this.pendingIntentTemplate.writeToParcel(paramParcel, 0);
        }
    }

    private class SetOnClickFillInIntent extends RemoteViews.Action
    {
        public static final int TAG = 9;
        Intent fillInIntent;
        int viewId;

        public SetOnClickFillInIntent(int paramIntent, Intent arg3)
        {
            super();
            this.viewId = paramIntent;
            Object localObject;
            this.fillInIntent = localObject;
        }

        public SetOnClickFillInIntent(Parcel arg2)
        {
            super();
            Parcel localParcel;
            this.viewId = localParcel.readInt();
            this.fillInIntent = ((Intent)Intent.CREATOR.createFromParcel(localParcel));
        }

        public void apply(View paramView, ViewGroup paramViewGroup, final RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView = paramView.findViewById(this.viewId);
            if (localView == null);
            while (true)
            {
                return;
                if (!RemoteViews.this.mIsWidgetCollectionChild)
                    Log.e("RemoteViews", "The method setOnClickFillInIntent is available only from RemoteViewsFactory (ie. on collection items).");
                else if (localView == paramView)
                    localView.setTagInternal(16908857, this.fillInIntent);
                else if ((localView != null) && (this.fillInIntent != null))
                    localView.setOnClickListener(new View.OnClickListener()
                    {
                        public void onClick(View paramAnonymousView)
                        {
                            for (View localView = (View)paramAnonymousView.getParent(); (!(localView instanceof AdapterView)) && (!(localView instanceof AppWidgetHostView)); localView = (View)localView.getParent());
                            if ((localView instanceof AppWidgetHostView))
                                Log.e("RemoteViews", "Collection item doesn't have AdapterView parent");
                            while (true)
                            {
                                return;
                                if (!(localView.getTag() instanceof PendingIntent))
                                {
                                    Log.e("RemoteViews", "Attempting setOnClickFillInIntent without calling setPendingIntentTemplate on parent.");
                                }
                                else
                                {
                                    PendingIntent localPendingIntent = (PendingIntent)localView.getTag();
                                    float f = paramAnonymousView.getContext().getResources().getCompatibilityInfo().applicationScale;
                                    int[] arrayOfInt = new int[2];
                                    paramAnonymousView.getLocationOnScreen(arrayOfInt);
                                    Rect localRect = new Rect();
                                    localRect.left = ((int)(0.5F + f * arrayOfInt[0]));
                                    localRect.top = ((int)(0.5F + f * arrayOfInt[1]));
                                    localRect.right = ((int)(0.5F + f * (arrayOfInt[0] + paramAnonymousView.getWidth())));
                                    localRect.bottom = ((int)(0.5F + f * (arrayOfInt[1] + paramAnonymousView.getHeight())));
                                    RemoteViews.SetOnClickFillInIntent.this.fillInIntent.setSourceBounds(localRect);
                                    paramOnClickHandler.onClickHandler(paramAnonymousView, localPendingIntent, RemoteViews.SetOnClickFillInIntent.this.fillInIntent);
                                }
                            }
                        }
                    });
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(9);
            paramParcel.writeInt(this.viewId);
            this.fillInIntent.writeToParcel(paramParcel, 0);
        }
    }

    private class SetEmptyView extends RemoteViews.Action
    {
        public static final int TAG = 6;
        int emptyViewId;
        int viewId;

        SetEmptyView(int paramInt1, int arg3)
        {
            super();
            this.viewId = paramInt1;
            int i;
            this.emptyViewId = i;
        }

        SetEmptyView(Parcel arg2)
        {
            super();
            Object localObject;
            this.viewId = localObject.readInt();
            this.emptyViewId = localObject.readInt();
        }

        public void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
        {
            View localView1 = paramView.findViewById(this.viewId);
            if (!(localView1 instanceof AdapterView));
            while (true)
            {
                return;
                AdapterView localAdapterView = (AdapterView)localView1;
                View localView2 = paramView.findViewById(this.emptyViewId);
                if (localView2 != null)
                    localAdapterView.setEmptyView(localView2);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(6);
            paramParcel.writeInt(this.viewId);
            paramParcel.writeInt(this.emptyViewId);
        }
    }

    private static abstract class Action
        implements Parcelable
    {
        public abstract void apply(View paramView, ViewGroup paramViewGroup, RemoteViews.OnClickHandler paramOnClickHandler)
            throws RemoteViews.ActionException;

        public int describeContents()
        {
            return 0;
        }

        public void setBitmapCache(RemoteViews.BitmapCache paramBitmapCache)
        {
        }

        public void updateMemoryUsageEstimate(RemoteViews.MemoryUsageCounter paramMemoryUsageCounter)
        {
        }
    }

    public static class OnClickHandler
    {
        public boolean onClickHandler(View paramView, PendingIntent paramPendingIntent, Intent paramIntent)
        {
            try
            {
                Context localContext = paramView.getContext();
                ActivityOptions localActivityOptions = ActivityOptions.makeScaleUpAnimation(paramView, 0, 0, paramView.getMeasuredWidth(), paramView.getMeasuredHeight());
                localContext.startIntentSender(paramPendingIntent.getIntentSender(), paramIntent, 268435456, 268435456, 0, localActivityOptions.toBundle());
                bool = true;
                return bool;
            }
            catch (IntentSender.SendIntentException localSendIntentException)
            {
                while (true)
                {
                    Log.e("RemoteViews", "Cannot send pending intent: ", localSendIntentException);
                    bool = false;
                }
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Log.e("RemoteViews", "Cannot send pending intent due to unknown exception: ", localException);
                    boolean bool = false;
                }
            }
        }
    }

    public static class ActionException extends RuntimeException
    {
        public ActionException(Exception paramException)
        {
            super();
        }

        public ActionException(String paramString)
        {
            super();
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({java.lang.annotation.ElementType.TYPE})
    public static @interface RemoteView
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.RemoteViews
 * JD-Core Version:        0.6.2
 */