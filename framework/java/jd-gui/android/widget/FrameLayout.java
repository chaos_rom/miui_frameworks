package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.ArrayList;

@RemoteViews.RemoteView
public class FrameLayout extends ViewGroup
{
    private static final int DEFAULT_CHILD_GRAVITY = 51;

    @ViewDebug.ExportedProperty(category="drawing")
    private Drawable mForeground;
    boolean mForegroundBoundsChanged = false;

    @ViewDebug.ExportedProperty(category="drawing")
    private int mForegroundGravity = 119;

    @ViewDebug.ExportedProperty(category="drawing")
    protected boolean mForegroundInPadding = true;

    @ViewDebug.ExportedProperty(category="padding")
    private int mForegroundPaddingBottom = 0;

    @ViewDebug.ExportedProperty(category="padding")
    private int mForegroundPaddingLeft = 0;

    @ViewDebug.ExportedProperty(category="padding")
    private int mForegroundPaddingRight = 0;

    @ViewDebug.ExportedProperty(category="padding")
    private int mForegroundPaddingTop = 0;
    private final ArrayList<View> mMatchParentChildren = new ArrayList(1);

    @ViewDebug.ExportedProperty(category="measurement")
    boolean mMeasureAllChildren = false;
    private final Rect mOverlayBounds = new Rect();
    private final Rect mSelfBounds = new Rect();

    public FrameLayout(Context paramContext)
    {
        super(paramContext);
    }

    public FrameLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public FrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FrameLayout, paramInt, 0);
        this.mForegroundGravity = localTypedArray.getInt(2, this.mForegroundGravity);
        Drawable localDrawable = localTypedArray.getDrawable(0);
        if (localDrawable != null)
            setForeground(localDrawable);
        if (localTypedArray.getBoolean(1, false))
            setMeasureAllChildren(true);
        this.mForegroundInPadding = localTypedArray.getBoolean(3, true);
        localTypedArray.recycle();
    }

    private int getPaddingBottomWithForeground()
    {
        if (this.mForegroundInPadding);
        for (int i = Math.max(this.mPaddingBottom, this.mForegroundPaddingBottom); ; i = this.mPaddingBottom + this.mForegroundPaddingBottom)
            return i;
    }

    private int getPaddingLeftWithForeground()
    {
        if (this.mForegroundInPadding);
        for (int i = Math.max(this.mPaddingLeft, this.mForegroundPaddingLeft); ; i = this.mPaddingLeft + this.mForegroundPaddingLeft)
            return i;
    }

    private int getPaddingRightWithForeground()
    {
        if (this.mForegroundInPadding);
        for (int i = Math.max(this.mPaddingRight, this.mForegroundPaddingRight); ; i = this.mPaddingRight + this.mForegroundPaddingRight)
            return i;
    }

    private int getPaddingTopWithForeground()
    {
        if (this.mForegroundInPadding);
        for (int i = Math.max(this.mPaddingTop, this.mForegroundPaddingTop); ; i = this.mPaddingTop + this.mForegroundPaddingTop)
            return i;
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    public void draw(Canvas paramCanvas)
    {
        super.draw(paramCanvas);
        Drawable localDrawable;
        Rect localRect1;
        Rect localRect2;
        int i;
        int j;
        if (this.mForeground != null)
        {
            localDrawable = this.mForeground;
            if (this.mForegroundBoundsChanged)
            {
                this.mForegroundBoundsChanged = false;
                localRect1 = this.mSelfBounds;
                localRect2 = this.mOverlayBounds;
                i = this.mRight - this.mLeft;
                j = this.mBottom - this.mTop;
                if (!this.mForegroundInPadding)
                    break label117;
                localRect1.set(0, 0, i, j);
            }
        }
        while (true)
        {
            int k = getResolvedLayoutDirection();
            Gravity.apply(this.mForegroundGravity, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight(), localRect1, localRect2, k);
            localDrawable.setBounds(localRect2);
            localDrawable.draw(paramCanvas);
            return;
            label117: localRect1.set(this.mPaddingLeft, this.mPaddingTop, i - this.mPaddingRight, j - this.mPaddingBottom);
        }
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if ((this.mForeground != null) && (this.mForeground.isStateful()))
            this.mForeground.setState(getDrawableState());
    }

    public boolean gatherTransparentRegion(Region paramRegion)
    {
        boolean bool = super.gatherTransparentRegion(paramRegion);
        if ((paramRegion != null) && (this.mForeground != null))
            applyDrawableToTransparentRegion(this.mForeground, paramRegion);
        return bool;
    }

    protected LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-1, -1);
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    @Deprecated
    public boolean getConsiderGoneChildrenWhenMeasuring()
    {
        return getMeasureAllChildren();
    }

    public Drawable getForeground()
    {
        return this.mForeground;
    }

    public int getForegroundGravity()
    {
        return this.mForegroundGravity;
    }

    public boolean getMeasureAllChildren()
    {
        return this.mMeasureAllChildren;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mForeground != null)
            this.mForeground.jumpToCurrentState();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(FrameLayout.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(FrameLayout.class.getName());
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = getChildCount();
        int j = getPaddingLeftWithForeground();
        int k = paramInt3 - paramInt1 - getPaddingRightWithForeground();
        int m = getPaddingTopWithForeground();
        int n = paramInt4 - paramInt2 - getPaddingBottomWithForeground();
        this.mForegroundBoundsChanged = true;
        int i1 = 0;
        if (i1 < i)
        {
            View localView = getChildAt(i1);
            LayoutParams localLayoutParams;
            int i2;
            int i3;
            int i7;
            label182: int i8;
            if (localView.getVisibility() != 8)
            {
                localLayoutParams = (LayoutParams)localView.getLayoutParams();
                i2 = localView.getMeasuredWidth();
                i3 = localView.getMeasuredHeight();
                int i4 = localLayoutParams.gravity;
                if (i4 == -1)
                    i4 = 51;
                int i5 = Gravity.getAbsoluteGravity(i4, getResolvedLayoutDirection());
                int i6 = i4 & 0x70;
                switch (i5 & 0x7)
                {
                case 2:
                case 4:
                default:
                    i7 = j + localLayoutParams.leftMargin;
                    switch (i6)
                    {
                    default:
                        i8 = m + localLayoutParams.topMargin;
                    case 48:
                    case 16:
                    case 80:
                    }
                    break;
                case 3:
                case 1:
                case 5:
                }
            }
            while (true)
            {
                localView.layout(i7, i8, i7 + i2, i8 + i3);
                i1++;
                break;
                i7 = j + localLayoutParams.leftMargin;
                break label182;
                i7 = j + (k - j - i2) / 2 + localLayoutParams.leftMargin - localLayoutParams.rightMargin;
                break label182;
                i7 = k - i2 - localLayoutParams.rightMargin;
                break label182;
                i8 = m + localLayoutParams.topMargin;
                continue;
                i8 = m + (n - m - i3) / 2 + localLayoutParams.topMargin - localLayoutParams.bottomMargin;
                continue;
                i8 = n - i3 - localLayoutParams.bottomMargin;
            }
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getChildCount();
        if ((View.MeasureSpec.getMode(paramInt1) != 1073741824) || (View.MeasureSpec.getMode(paramInt2) != 1073741824));
        int k;
        int m;
        int n;
        for (int j = 1; ; j = 0)
        {
            this.mMatchParentChildren.clear();
            k = 0;
            m = 0;
            n = 0;
            for (int i1 = 0; i1 < i; i1++)
            {
                View localView2 = getChildAt(i1);
                if ((this.mMeasureAllChildren) || (localView2.getVisibility() != 8))
                {
                    measureChildWithMargins(localView2, paramInt1, 0, paramInt2, 0);
                    LayoutParams localLayoutParams = (LayoutParams)localView2.getLayoutParams();
                    m = Math.max(m, localView2.getMeasuredWidth() + localLayoutParams.leftMargin + localLayoutParams.rightMargin);
                    k = Math.max(k, localView2.getMeasuredHeight() + localLayoutParams.topMargin + localLayoutParams.bottomMargin);
                    n = combineMeasuredStates(n, localView2.getMeasuredState());
                    if ((j != 0) && ((localLayoutParams.width == -1) || (localLayoutParams.height == -1)))
                        this.mMatchParentChildren.add(localView2);
                }
            }
        }
        int i2 = m + (getPaddingLeftWithForeground() + getPaddingRightWithForeground());
        int i3 = Math.max(k + (getPaddingTopWithForeground() + getPaddingBottomWithForeground()), getSuggestedMinimumHeight());
        int i4 = Math.max(i2, getSuggestedMinimumWidth());
        Drawable localDrawable = getForeground();
        if (localDrawable != null)
        {
            i3 = Math.max(i3, localDrawable.getMinimumHeight());
            i4 = Math.max(i4, localDrawable.getMinimumWidth());
        }
        setMeasuredDimension(resolveSizeAndState(i4, paramInt1, n), resolveSizeAndState(i3, paramInt2, n << 16));
        int i5 = this.mMatchParentChildren.size();
        if (i5 > 1)
        {
            int i6 = 0;
            if (i6 < i5)
            {
                View localView1 = (View)this.mMatchParentChildren.get(i6);
                ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)localView1.getLayoutParams();
                int i7;
                if (localMarginLayoutParams.width == -1)
                {
                    i7 = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth() - getPaddingLeftWithForeground() - getPaddingRightWithForeground() - localMarginLayoutParams.leftMargin - localMarginLayoutParams.rightMargin, 1073741824);
                    label402: if (localMarginLayoutParams.height != -1)
                        break label496;
                }
                label496: for (int i8 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight() - getPaddingTopWithForeground() - getPaddingBottomWithForeground() - localMarginLayoutParams.topMargin - localMarginLayoutParams.bottomMargin, 1073741824); ; i8 = getChildMeasureSpec(paramInt2, getPaddingTopWithForeground() + getPaddingBottomWithForeground() + localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin, localMarginLayoutParams.height))
                {
                    localView1.measure(i7, i8);
                    i6++;
                    break;
                    i7 = getChildMeasureSpec(paramInt1, getPaddingLeftWithForeground() + getPaddingRightWithForeground() + localMarginLayoutParams.leftMargin + localMarginLayoutParams.rightMargin, localMarginLayoutParams.width);
                    break label402;
                }
            }
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mForegroundBoundsChanged = true;
    }

    public void setForeground(Drawable paramDrawable)
    {
        if (this.mForeground != paramDrawable)
        {
            if (this.mForeground != null)
            {
                this.mForeground.setCallback(null);
                unscheduleDrawable(this.mForeground);
            }
            this.mForeground = paramDrawable;
            this.mForegroundPaddingLeft = 0;
            this.mForegroundPaddingTop = 0;
            this.mForegroundPaddingRight = 0;
            this.mForegroundPaddingBottom = 0;
            if (paramDrawable == null)
                break label152;
            setWillNotDraw(false);
            paramDrawable.setCallback(this);
            if (paramDrawable.isStateful())
                paramDrawable.setState(getDrawableState());
            if (this.mForegroundGravity == 119)
            {
                Rect localRect = new Rect();
                if (paramDrawable.getPadding(localRect))
                {
                    this.mForegroundPaddingLeft = localRect.left;
                    this.mForegroundPaddingTop = localRect.top;
                    this.mForegroundPaddingRight = localRect.right;
                    this.mForegroundPaddingBottom = localRect.bottom;
                }
            }
        }
        while (true)
        {
            requestLayout();
            invalidate();
            return;
            label152: setWillNotDraw(true);
        }
    }

    @RemotableViewMethod
    public void setForegroundGravity(int paramInt)
    {
        Rect localRect;
        if (this.mForegroundGravity != paramInt)
        {
            if ((0x800007 & paramInt) == 0)
                paramInt |= 8388611;
            if ((paramInt & 0x70) == 0)
                paramInt |= 48;
            this.mForegroundGravity = paramInt;
            if ((this.mForegroundGravity != 119) || (this.mForeground == null))
                break label111;
            localRect = new Rect();
            if (this.mForeground.getPadding(localRect))
            {
                this.mForegroundPaddingLeft = localRect.left;
                this.mForegroundPaddingTop = localRect.top;
                this.mForegroundPaddingRight = localRect.right;
            }
        }
        for (this.mForegroundPaddingBottom = localRect.bottom; ; this.mForegroundPaddingBottom = 0)
        {
            requestLayout();
            return;
            label111: this.mForegroundPaddingLeft = 0;
            this.mForegroundPaddingTop = 0;
            this.mForegroundPaddingRight = 0;
        }
    }

    @RemotableViewMethod
    public void setMeasureAllChildren(boolean paramBoolean)
    {
        this.mMeasureAllChildren = paramBoolean;
    }

    public boolean shouldDelayChildPressedState()
    {
        return false;
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((super.verifyDrawable(paramDrawable)) || (paramDrawable == this.mForeground));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams
    {
        public int gravity = -1;

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3)
        {
            super(paramInt2);
            this.gravity = paramInt3;
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FrameLayout_Layout);
            this.gravity = localTypedArray.getInt(0, -1);
            localTypedArray.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.FrameLayout
 * JD-Core Version:        0.6.2
 */