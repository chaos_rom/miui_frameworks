package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class TextSwitcher extends ViewSwitcher
{
    public TextSwitcher(Context paramContext)
    {
        super(paramContext);
    }

    public TextSwitcher(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (!(paramView instanceof TextView))
            throw new IllegalArgumentException("TextSwitcher children must be instances of TextView");
        super.addView(paramView, paramInt, paramLayoutParams);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TextSwitcher.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TextSwitcher.class.getName());
    }

    public void setCurrentText(CharSequence paramCharSequence)
    {
        ((TextView)getCurrentView()).setText(paramCharSequence);
    }

    public void setText(CharSequence paramCharSequence)
    {
        ((TextView)getNextView()).setText(paramCharSequence);
        showNext();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TextSwitcher
 * JD-Core Version:        0.6.2
 */