package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

@RemoteViews.RemoteView
public class LinearLayout extends ViewGroup
{
    public static final int HORIZONTAL = 0;
    private static final int INDEX_BOTTOM = 2;
    private static final int INDEX_CENTER_VERTICAL = 0;
    private static final int INDEX_FILL = 3;
    private static final int INDEX_TOP = 1;
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    public static final int VERTICAL = 1;
    private static final int VERTICAL_GRAVITY_COUNT = 4;

    @ViewDebug.ExportedProperty(category="layout")
    private boolean mBaselineAligned = true;

    @ViewDebug.ExportedProperty(category="layout")
    private int mBaselineAlignedChildIndex = -1;

    @ViewDebug.ExportedProperty(category="measurement")
    private int mBaselineChildTop = 0;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;

    @ViewDebug.ExportedProperty(category="measurement", flagMapping={@android.view.ViewDebug.FlagToString(equals=-1, mask=-1, name="NONE"), @android.view.ViewDebug.FlagToString(equals=0, mask=0, name="NONE"), @android.view.ViewDebug.FlagToString(equals=48, mask=48, name="TOP"), @android.view.ViewDebug.FlagToString(equals=80, mask=80, name="BOTTOM"), @android.view.ViewDebug.FlagToString(equals=3, mask=3, name="LEFT"), @android.view.ViewDebug.FlagToString(equals=5, mask=5, name="RIGHT"), @android.view.ViewDebug.FlagToString(equals=8388611, mask=8388611, name="START"), @android.view.ViewDebug.FlagToString(equals=8388613, mask=8388613, name="END"), @android.view.ViewDebug.FlagToString(equals=16, mask=16, name="CENTER_VERTICAL"), @android.view.ViewDebug.FlagToString(equals=112, mask=112, name="FILL_VERTICAL"), @android.view.ViewDebug.FlagToString(equals=1, mask=1, name="CENTER_HORIZONTAL"), @android.view.ViewDebug.FlagToString(equals=7, mask=7, name="FILL_HORIZONTAL"), @android.view.ViewDebug.FlagToString(equals=17, mask=17, name="CENTER"), @android.view.ViewDebug.FlagToString(equals=119, mask=119, name="FILL"), @android.view.ViewDebug.FlagToString(equals=8388608, mask=8388608, name="RELATIVE")})
    private int mGravity = 8388659;
    private int[] mMaxAscent;
    private int[] mMaxDescent;

    @ViewDebug.ExportedProperty(category="measurement")
    private int mOrientation;
    private int mShowDividers;

    @ViewDebug.ExportedProperty(category="measurement")
    private int mTotalLength;

    @ViewDebug.ExportedProperty(category="layout")
    private boolean mUseLargestChild;

    @ViewDebug.ExportedProperty(category="layout")
    private float mWeightSum;

    public LinearLayout(Context paramContext)
    {
        super(paramContext);
    }

    public LinearLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public LinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LinearLayout, paramInt, 0);
        int i = localTypedArray.getInt(1, -1);
        if (i >= 0)
            setOrientation(i);
        int j = localTypedArray.getInt(0, -1);
        if (j >= 0)
            setGravity(j);
        boolean bool = localTypedArray.getBoolean(2, true);
        if (!bool)
            setBaselineAligned(bool);
        this.mWeightSum = localTypedArray.getFloat(4, -1.0F);
        this.mBaselineAlignedChildIndex = localTypedArray.getInt(3, -1);
        this.mUseLargestChild = localTypedArray.getBoolean(6, false);
        setDividerDrawable(localTypedArray.getDrawable(5));
        this.mShowDividers = localTypedArray.getInt(7, 0);
        this.mDividerPadding = localTypedArray.getDimensionPixelSize(8, 0);
        localTypedArray.recycle();
    }

    private void forceUniformHeight(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
        for (int j = 0; j < paramInt1; j++)
        {
            View localView = getVirtualChildAt(j);
            if (localView.getVisibility() != 8)
            {
                LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
                if (localLayoutParams.height == -1)
                {
                    int k = localLayoutParams.width;
                    localLayoutParams.width = localView.getMeasuredWidth();
                    measureChildWithMargins(localView, paramInt2, 0, i, 0);
                    localLayoutParams.width = k;
                }
            }
        }
    }

    private void forceUniformWidth(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        for (int j = 0; j < paramInt1; j++)
        {
            View localView = getVirtualChildAt(j);
            if (localView.getVisibility() != 8)
            {
                LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
                if (localLayoutParams.width == -1)
                {
                    int k = localLayoutParams.height;
                    localLayoutParams.height = localView.getMeasuredHeight();
                    measureChildWithMargins(localView, i, 0, paramInt2, 0);
                    localLayoutParams.height = k;
                }
            }
        }
    }

    private void setChildFrame(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    void drawDividersHorizontal(Canvas paramCanvas)
    {
        int i = getVirtualChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView2 = getVirtualChildAt(j);
            if ((localView2 != null) && (localView2.getVisibility() != 8) && (hasDividerBeforeChildAt(j)))
            {
                LayoutParams localLayoutParams2 = (LayoutParams)localView2.getLayoutParams();
                drawVerticalDivider(paramCanvas, localView2.getLeft() - localLayoutParams2.leftMargin - this.mDividerWidth);
            }
        }
        View localView1;
        if (hasDividerBeforeChildAt(i))
        {
            localView1 = getVirtualChildAt(i - 1);
            if (localView1 != null)
                break label125;
        }
        label125: LayoutParams localLayoutParams1;
        for (int k = getWidth() - getPaddingRight() - this.mDividerWidth; ; k = localView1.getRight() + localLayoutParams1.rightMargin)
        {
            drawVerticalDivider(paramCanvas, k);
            return;
            localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
        }
    }

    void drawDividersVertical(Canvas paramCanvas)
    {
        int i = getVirtualChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView2 = getVirtualChildAt(j);
            if ((localView2 != null) && (localView2.getVisibility() != 8) && (hasDividerBeforeChildAt(j)))
            {
                LayoutParams localLayoutParams2 = (LayoutParams)localView2.getLayoutParams();
                drawHorizontalDivider(paramCanvas, localView2.getTop() - localLayoutParams2.topMargin - this.mDividerHeight);
            }
        }
        View localView1;
        if (hasDividerBeforeChildAt(i))
        {
            localView1 = getVirtualChildAt(i - 1);
            if (localView1 != null)
                break label125;
        }
        label125: LayoutParams localLayoutParams1;
        for (int k = getHeight() - getPaddingBottom() - this.mDividerHeight; ; k = localView1.getBottom() + localLayoutParams1.bottomMargin)
        {
            drawHorizontalDivider(paramCanvas, k);
            return;
            localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
        }
    }

    void drawHorizontalDivider(Canvas paramCanvas, int paramInt)
    {
        this.mDivider.setBounds(getPaddingLeft() + this.mDividerPadding, paramInt, getWidth() - getPaddingRight() - this.mDividerPadding, paramInt + this.mDividerHeight);
        this.mDivider.draw(paramCanvas);
    }

    void drawVerticalDivider(Canvas paramCanvas, int paramInt)
    {
        this.mDivider.setBounds(paramInt, getPaddingTop() + this.mDividerPadding, paramInt + this.mDividerWidth, getHeight() - getPaddingBottom() - this.mDividerPadding);
        this.mDivider.draw(paramCanvas);
    }

    protected LayoutParams generateDefaultLayoutParams()
    {
        LayoutParams localLayoutParams;
        if (this.mOrientation == 0)
            localLayoutParams = new LayoutParams(-2, -2);
        while (true)
        {
            return localLayoutParams;
            if (this.mOrientation == 1)
                localLayoutParams = new LayoutParams(-1, -2);
            else
                localLayoutParams = null;
        }
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    public int getBaseline()
    {
        int i = -1;
        if (this.mBaselineAlignedChildIndex < 0)
            i = super.getBaseline();
        View localView;
        int j;
        do
        {
            return i;
            if (getChildCount() <= this.mBaselineAlignedChildIndex)
                throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
            localView = getChildAt(this.mBaselineAlignedChildIndex);
            j = localView.getBaseline();
            if (j != i)
                break;
        }
        while (this.mBaselineAlignedChildIndex == 0);
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        int k = this.mBaselineChildTop;
        if (this.mOrientation == 1)
        {
            int m = 0x70 & this.mGravity;
            if (m != 48)
                switch (m)
                {
                default:
                case 80:
                case 16:
                }
        }
        while (true)
        {
            i = j + (k + ((LayoutParams)localView.getLayoutParams()).topMargin);
            break;
            k = this.mBottom - this.mTop - this.mPaddingBottom - this.mTotalLength;
            continue;
            k += (this.mBottom - this.mTop - this.mPaddingTop - this.mPaddingBottom - this.mTotalLength) / 2;
        }
    }

    public int getBaselineAlignedChildIndex()
    {
        return this.mBaselineAlignedChildIndex;
    }

    int getChildrenSkipCount(View paramView, int paramInt)
    {
        return 0;
    }

    public Drawable getDividerDrawable()
    {
        return this.mDivider;
    }

    public int getDividerPadding()
    {
        return this.mDividerPadding;
    }

    public int getDividerWidth()
    {
        return this.mDividerWidth;
    }

    int getLocationOffset(View paramView)
    {
        return 0;
    }

    int getNextLocationOffset(View paramView)
    {
        return 0;
    }

    public int getOrientation()
    {
        return this.mOrientation;
    }

    public int getShowDividers()
    {
        return this.mShowDividers;
    }

    View getVirtualChildAt(int paramInt)
    {
        return getChildAt(paramInt);
    }

    int getVirtualChildCount()
    {
        return getChildCount();
    }

    public float getWeightSum()
    {
        return this.mWeightSum;
    }

    protected boolean hasDividerBeforeChildAt(int paramInt)
    {
        boolean bool1 = true;
        if (paramInt == 0)
            if ((0x1 & this.mShowDividers) == 0);
        while (true)
        {
            return bool1;
            bool1 = false;
            continue;
            if (paramInt == getChildCount())
            {
                if ((0x4 & this.mShowDividers) == 0)
                    bool1 = false;
            }
            else
            {
                if ((0x2 & this.mShowDividers) != 0)
                {
                    boolean bool2 = false;
                    for (int i = paramInt - 1; ; i--)
                        if (i >= 0)
                        {
                            if (getChildAt(i).getVisibility() != 8)
                                bool2 = true;
                        }
                        else
                        {
                            bool1 = bool2;
                            break;
                        }
                }
                bool1 = false;
            }
        }
    }

    public boolean isBaselineAligned()
    {
        return this.mBaselineAligned;
    }

    public boolean isMeasureWithLargestChildEnabled()
    {
        return this.mUseLargestChild;
    }

    void layoutHorizontal()
    {
        boolean bool1 = isLayoutRtl();
        int i = this.mPaddingTop;
        int j = this.mBottom - this.mTop;
        int k = j - this.mPaddingBottom;
        int m = j - i - this.mPaddingBottom;
        int n = getVirtualChildCount();
        int i1 = 0x800007 & this.mGravity;
        int i2 = 0x70 & this.mGravity;
        boolean bool2 = this.mBaselineAligned;
        int[] arrayOfInt1 = this.mMaxAscent;
        int[] arrayOfInt2 = this.mMaxDescent;
        int i3;
        int i6;
        label145: int i7;
        View localView;
        switch (Gravity.getAbsoluteGravity(i1, getResolvedLayoutDirection()))
        {
        default:
            i3 = this.mPaddingLeft;
            int i4 = 0;
            int i5 = 1;
            if (bool1)
            {
                i4 = n - 1;
                i5 = -1;
            }
            i6 = 0;
            if (i6 >= n)
                return;
            i7 = i4 + i5 * i6;
            localView = getVirtualChildAt(i7);
            if (localView == null)
                i3 += measureNullChild(i7);
            break;
        case 5:
        case 1:
        }
        while (localView.getVisibility() == 8)
        {
            i6++;
            break label145;
            i3 = this.mPaddingLeft + this.mRight - this.mLeft - this.mTotalLength;
            break;
            i3 = this.mPaddingLeft + (this.mRight - this.mLeft - this.mTotalLength) / 2;
            break;
        }
        int i8 = localView.getMeasuredWidth();
        int i9 = localView.getMeasuredHeight();
        int i10 = -1;
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if ((bool2) && (localLayoutParams.height != -1))
            i10 = localView.getBaseline();
        int i11 = localLayoutParams.gravity;
        if (i11 < 0)
            i11 = i2;
        int i12;
        switch (i11 & 0x70)
        {
        default:
            i12 = i;
        case 48:
        case 16:
        case 80:
        }
        while (true)
        {
            if (hasDividerBeforeChildAt(i7))
                i3 += this.mDividerWidth;
            int i14 = i3 + localLayoutParams.leftMargin;
            setChildFrame(localView, i14 + getLocationOffset(localView), i12, i8, i9);
            i3 = i14 + (i8 + localLayoutParams.rightMargin + getNextLocationOffset(localView));
            i6 += getChildrenSkipCount(localView, i7);
            break;
            i12 = i + localLayoutParams.topMargin;
            if (i10 != -1)
            {
                i12 += arrayOfInt1[1] - i10;
                continue;
                i12 = i + (m - i9) / 2 + localLayoutParams.topMargin - localLayoutParams.bottomMargin;
                continue;
                i12 = k - i9 - localLayoutParams.bottomMargin;
                if (i10 != -1)
                {
                    int i13 = localView.getMeasuredHeight() - i10;
                    i12 -= arrayOfInt2[2] - i13;
                }
            }
        }
    }

    void layoutVertical()
    {
        int i = this.mPaddingLeft;
        int j = this.mRight - this.mLeft;
        int k = j - this.mPaddingRight;
        int m = j - i - this.mPaddingRight;
        int n = getVirtualChildCount();
        int i1 = 0x70 & this.mGravity;
        int i2 = 0x800007 & this.mGravity;
        int i3;
        int i4;
        label93: View localView;
        switch (i1)
        {
        default:
            i3 = this.mPaddingTop;
            i4 = 0;
            if (i4 >= n)
                return;
            localView = getVirtualChildAt(i4);
            if (localView == null)
                i3 += measureNullChild(i4);
            break;
        case 80:
        case 16:
        }
        while (localView.getVisibility() == 8)
        {
            i4++;
            break label93;
            i3 = this.mPaddingTop + this.mBottom - this.mTop - this.mTotalLength;
            break;
            i3 = this.mPaddingTop + (this.mBottom - this.mTop - this.mTotalLength) / 2;
            break;
        }
        int i5 = localView.getMeasuredWidth();
        int i6 = localView.getMeasuredHeight();
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int i7 = localLayoutParams.gravity;
        if (i7 < 0)
            i7 = i2;
        int i8;
        switch (0x7 & Gravity.getAbsoluteGravity(i7, getResolvedLayoutDirection()))
        {
        default:
            i8 = i + localLayoutParams.leftMargin;
        case 1:
        case 5:
        }
        while (true)
        {
            if (hasDividerBeforeChildAt(i4))
                i3 += this.mDividerHeight;
            int i9 = i3 + localLayoutParams.topMargin;
            setChildFrame(localView, i8, i9 + getLocationOffset(localView), i5, i6);
            i3 = i9 + (i6 + localLayoutParams.bottomMargin + getNextLocationOffset(localView));
            i4 += getChildrenSkipCount(localView, i4);
            break;
            i8 = i + (m - i5) / 2 + localLayoutParams.leftMargin - localLayoutParams.rightMargin;
            continue;
            i8 = k - i5 - localLayoutParams.rightMargin;
        }
    }

    void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        measureChildWithMargins(paramView, paramInt2, paramInt3, paramInt4, paramInt5);
    }

    void measureHorizontal(int paramInt1, int paramInt2)
    {
        this.mTotalLength = 0;
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 1;
        float f1 = 0.0F;
        int i1 = getVirtualChildCount();
        int i2 = View.MeasureSpec.getMode(paramInt1);
        int i3 = View.MeasureSpec.getMode(paramInt2);
        int i4 = 0;
        if ((this.mMaxAscent == null) || (this.mMaxDescent == null))
        {
            this.mMaxAscent = new int[4];
            this.mMaxDescent = new int[4];
        }
        int[] arrayOfInt1 = this.mMaxAscent;
        int[] arrayOfInt2 = this.mMaxDescent;
        arrayOfInt1[3] = -1;
        arrayOfInt1[2] = -1;
        arrayOfInt1[1] = -1;
        arrayOfInt1[0] = -1;
        arrayOfInt2[3] = -1;
        arrayOfInt2[2] = -1;
        arrayOfInt2[1] = -1;
        arrayOfInt2[0] = -1;
        boolean bool1 = this.mBaselineAligned;
        boolean bool2 = this.mUseLargestChild;
        int i5;
        int i6;
        int i7;
        label161: View localView4;
        if (i2 == 1073741824)
        {
            i5 = 1;
            i6 = -2147483648;
            i7 = 0;
            if (i7 >= i1)
                break label817;
            localView4 = getVirtualChildAt(i7);
            if (localView4 != null)
                break label208;
            this.mTotalLength += measureNullChild(i7);
        }
        while (true)
        {
            i7++;
            break label161;
            i5 = 0;
            break;
            label208: if (localView4.getVisibility() != 8)
                break label234;
            i7 += getChildrenSkipCount(localView4, i7);
        }
        label234: if (hasDividerBeforeChildAt(i7))
            this.mTotalLength += this.mDividerWidth;
        LayoutParams localLayoutParams3 = (LayoutParams)localView4.getLayoutParams();
        f1 += localLayoutParams3.weight;
        label326: label347: int i32;
        int i33;
        int i34;
        int i37;
        if ((i2 == 1073741824) && (localLayoutParams3.width == 0) && (localLayoutParams3.weight > 0.0F))
            if (i5 != 0)
            {
                this.mTotalLength += localLayoutParams3.leftMargin + localLayoutParams3.rightMargin;
                if (bool1)
                {
                    int i40 = View.MeasureSpec.makeMeasureSpec(0, 0);
                    localView4.measure(i40, i40);
                }
                i32 = 0;
                if ((i3 != 1073741824) && (localLayoutParams3.height == -1))
                {
                    i4 = 1;
                    i32 = 1;
                }
                i33 = localLayoutParams3.topMargin + localLayoutParams3.bottomMargin;
                i34 = i33 + localView4.getMeasuredHeight();
                int i35 = localView4.getMeasuredState();
                j = combineMeasuredStates(j, i35);
                if (bool1)
                {
                    int i36 = localView4.getBaseline();
                    if (i36 != -1)
                    {
                        if (localLayoutParams3.gravity >= 0)
                            break label770;
                        i37 = this.mGravity;
                        label445: int i38 = (0xFFFFFFFE & (i37 & 0x70) >> 4) >> 1;
                        arrayOfInt1[i38] = Math.max(arrayOfInt1[i38], i36);
                        arrayOfInt2[i38] = Math.max(arrayOfInt2[i38], i34 - i36);
                    }
                }
                i = Math.max(i, i34);
                if ((n == 0) || (localLayoutParams3.height != -1))
                    break label780;
                n = 1;
                label517: if (localLayoutParams3.weight <= 0.0F)
                    break label793;
                if (i32 == 0)
                    break label786;
            }
        while (true)
        {
            m = Math.max(m, i33);
            i7 += getChildrenSkipCount(localView4, i7);
            break;
            int i39 = this.mTotalLength;
            this.mTotalLength = Math.max(i39, i39 + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin);
            break label326;
            int i28 = -2147483648;
            if ((localLayoutParams3.width == 0) && (localLayoutParams3.weight > 0.0F))
            {
                i28 = 0;
                localLayoutParams3.width = -2;
            }
            int i29;
            label635: int i30;
            if (f1 == 0.0F)
            {
                i29 = this.mTotalLength;
                measureChildBeforeLayout(localView4, i7, paramInt1, i29, paramInt2, 0);
                if (i28 != -2147483648)
                    localLayoutParams3.width = i28;
                i30 = localView4.getMeasuredWidth();
                if (i5 == 0)
                    break label728;
            }
            label728: int i31;
            for (this.mTotalLength += i30 + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin + getNextLocationOffset(localView4); ; this.mTotalLength = Math.max(i31, i31 + i30 + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin + getNextLocationOffset(localView4)))
            {
                if (!bool2)
                    break label768;
                i6 = Math.max(i30, i6);
                break;
                i29 = 0;
                break label635;
                i31 = this.mTotalLength;
            }
            label768: break label347;
            label770: i37 = localLayoutParams3.gravity;
            break label445;
            label780: n = 0;
            break label517;
            label786: i33 = i34;
        }
        label793: if (i32 != 0);
        while (true)
        {
            k = Math.max(k, i33);
            break;
            i33 = i34;
        }
        label817: if ((this.mTotalLength > 0) && (hasDividerBeforeChildAt(i1)))
            this.mTotalLength += this.mDividerWidth;
        if ((arrayOfInt1[1] != -1) || (arrayOfInt1[0] != -1) || (arrayOfInt1[2] != -1) || (arrayOfInt1[3] != -1))
        {
            int i8 = Math.max(arrayOfInt1[3], Math.max(arrayOfInt1[0], Math.max(arrayOfInt1[1], arrayOfInt1[2]))) + Math.max(arrayOfInt2[3], Math.max(arrayOfInt2[0], Math.max(arrayOfInt2[1], arrayOfInt2[2])));
            i = Math.max(i, i8);
        }
        if ((bool2) && ((i2 == -2147483648) || (i2 == 0)))
        {
            this.mTotalLength = 0;
            int i26 = 0;
            if (i26 < i1)
            {
                View localView3 = getVirtualChildAt(i26);
                if (localView3 == null)
                    this.mTotalLength += measureNullChild(i26);
                while (true)
                {
                    i26++;
                    break;
                    if (localView3.getVisibility() == 8)
                    {
                        i26 += getChildrenSkipCount(localView3, i26);
                    }
                    else
                    {
                        LayoutParams localLayoutParams2 = (LayoutParams)localView3.getLayoutParams();
                        if (i5 != 0)
                        {
                            this.mTotalLength += i6 + localLayoutParams2.leftMargin + localLayoutParams2.rightMargin + getNextLocationOffset(localView3);
                        }
                        else
                        {
                            int i27 = this.mTotalLength;
                            this.mTotalLength = Math.max(i27, i27 + i6 + localLayoutParams2.leftMargin + localLayoutParams2.rightMargin + getNextLocationOffset(localView3));
                        }
                    }
                }
            }
        }
        this.mTotalLength += this.mPaddingLeft + this.mPaddingRight;
        int i9 = resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumWidth()), paramInt1, 0);
        int i10 = (i9 & 0xFFFFFF) - this.mTotalLength;
        if ((i10 != 0) && (f1 > 0.0F))
        {
            float f2;
            int i13;
            label1260: View localView2;
            if (this.mWeightSum > 0.0F)
            {
                f2 = this.mWeightSum;
                arrayOfInt1[3] = -1;
                arrayOfInt1[2] = -1;
                arrayOfInt1[1] = -1;
                arrayOfInt1[0] = -1;
                arrayOfInt2[3] = -1;
                arrayOfInt2[2] = -1;
                arrayOfInt2[1] = -1;
                arrayOfInt2[0] = -1;
                i = -1;
                this.mTotalLength = 0;
                i13 = 0;
                if (i13 >= i1)
                    break label1757;
                localView2 = getVirtualChildAt(i13);
                if ((localView2 != null) && (localView2.getVisibility() != 8))
                    break label1303;
            }
            label1303: LayoutParams localLayoutParams1;
            int i22;
            int i23;
            label1490: int i16;
            label1510: int i17;
            int i18;
            label1545: label1572: int i19;
            do
            {
                i13++;
                break label1260;
                f2 = f1;
                break;
                localLayoutParams1 = (LayoutParams)localView2.getLayoutParams();
                float f3 = localLayoutParams1.weight;
                if (f3 > 0.0F)
                {
                    i22 = (int)(f3 * i10 / f2);
                    f2 -= f3;
                    i10 -= i22;
                    i23 = getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom + localLayoutParams1.topMargin + localLayoutParams1.bottomMargin, localLayoutParams1.height);
                    if ((localLayoutParams1.width == 0) && (i2 == 1073741824))
                        break label1655;
                    int i24 = i22 + localView2.getMeasuredWidth();
                    if (i24 < 0)
                        i24 = 0;
                    localView2.measure(View.MeasureSpec.makeMeasureSpec(i24, 1073741824), i23);
                    int i25 = 0xFF000000 & localView2.getMeasuredState();
                    j = combineMeasuredStates(j, i25);
                }
                if (i5 == 0)
                    break label1683;
                this.mTotalLength += localView2.getMeasuredWidth() + localLayoutParams1.leftMargin + localLayoutParams1.rightMargin + getNextLocationOffset(localView2);
                if ((i3 == 1073741824) || (localLayoutParams1.height != -1))
                    break label1728;
                i16 = 1;
                i17 = localLayoutParams1.topMargin + localLayoutParams1.bottomMargin;
                i18 = i17 + localView2.getMeasuredHeight();
                i = Math.max(i, i18);
                if (i16 == 0)
                    break label1734;
                k = Math.max(k, i17);
                if ((n == 0) || (localLayoutParams1.height != -1))
                    break label1741;
                n = 1;
                if (!bool1)
                    break label1745;
                i19 = localView2.getBaseline();
            }
            while (i19 == -1);
            if (localLayoutParams1.gravity < 0);
            for (int i20 = this.mGravity; ; i20 = localLayoutParams1.gravity)
            {
                int i21 = (0xFFFFFFFE & (i20 & 0x70) >> 4) >> 1;
                arrayOfInt1[i21] = Math.max(arrayOfInt1[i21], i19);
                arrayOfInt2[i21] = Math.max(arrayOfInt2[i21], i18 - i19);
                break;
                label1655: if (i22 > 0);
                while (true)
                {
                    localView2.measure(View.MeasureSpec.makeMeasureSpec(i22, 1073741824), i23);
                    break;
                    i22 = 0;
                }
                label1683: int i15 = this.mTotalLength;
                this.mTotalLength = Math.max(i15, i15 + localView2.getMeasuredWidth() + localLayoutParams1.leftMargin + localLayoutParams1.rightMargin + getNextLocationOffset(localView2));
                break label1490;
                label1728: i16 = 0;
                break label1510;
                label1734: i17 = i18;
                break label1545;
                label1741: n = 0;
                break label1572;
                label1745: break;
            }
            label1757: this.mTotalLength += this.mPaddingLeft + this.mPaddingRight;
            if ((arrayOfInt1[1] != -1) || (arrayOfInt1[0] != -1) || (arrayOfInt1[2] != -1) || (arrayOfInt1[3] != -1))
            {
                int i14 = Math.max(arrayOfInt1[3], Math.max(arrayOfInt1[0], Math.max(arrayOfInt1[1], arrayOfInt1[2]))) + Math.max(arrayOfInt2[3], Math.max(arrayOfInt2[0], Math.max(arrayOfInt2[1], arrayOfInt2[2])));
                i = Math.max(i, i14);
            }
        }
        do
        {
            if ((n == 0) && (i3 != 1073741824))
                i = k;
            int i12 = Math.max(i + (this.mPaddingTop + this.mPaddingBottom), getSuggestedMinimumHeight());
            setMeasuredDimension(i9 | 0xFF000000 & j, resolveSizeAndState(i12, paramInt2, j << 16));
            if (i4 != 0)
                forceUniformHeight(i1, paramInt1);
            return;
            k = Math.max(k, m);
        }
        while ((!bool2) || (i2 == 1073741824));
        int i11 = 0;
        label1967: View localView1;
        if (i11 < i1)
        {
            localView1 = getVirtualChildAt(i11);
            if ((localView1 != null) && (localView1.getVisibility() != 8))
                break label2003;
        }
        while (true)
        {
            i11++;
            break label1967;
            break;
            label2003: if (((LayoutParams)localView1.getLayoutParams()).weight > 0.0F)
                localView1.measure(View.MeasureSpec.makeMeasureSpec(i6, 1073741824), View.MeasureSpec.makeMeasureSpec(localView1.getMeasuredHeight(), 1073741824));
        }
    }

    int measureNullChild(int paramInt)
    {
        return 0;
    }

    void measureVertical(int paramInt1, int paramInt2)
    {
        this.mTotalLength = 0;
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 1;
        float f1 = 0.0F;
        int i1 = getVirtualChildCount();
        int i2 = View.MeasureSpec.getMode(paramInt1);
        int i3 = View.MeasureSpec.getMode(paramInt2);
        int i4 = 0;
        int i5 = this.mBaselineAlignedChildIndex;
        boolean bool = this.mUseLargestChild;
        int i6 = -2147483648;
        int i7 = 0;
        if (i7 < i1)
        {
            View localView4 = getVirtualChildAt(i7);
            if (localView4 == null)
                this.mTotalLength += measureNullChild(i7);
            while (true)
            {
                i7++;
                break;
                if (localView4.getVisibility() != 8)
                    break label130;
                i7 += getChildrenSkipCount(localView4, i7);
            }
            label130: if (hasDividerBeforeChildAt(i7))
                this.mTotalLength += this.mDividerHeight;
            LayoutParams localLayoutParams3 = (LayoutParams)localView4.getLayoutParams();
            f1 += localLayoutParams3.weight;
            if ((i3 == 1073741824) && (localLayoutParams3.height == 0) && (localLayoutParams3.weight > 0.0F))
            {
                int i28 = this.mTotalLength;
                this.mTotalLength = Math.max(i28, i28 + localLayoutParams3.topMargin + localLayoutParams3.bottomMargin);
                if ((i5 >= 0) && (i5 == i7 + 1))
                    this.mBaselineChildTop = this.mTotalLength;
                if ((i7 < i5) && (localLayoutParams3.weight > 0.0F))
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.    Either remove the weight, or don't set mBaselineAlignedChildIndex.");
            }
            else
            {
                int i21 = -2147483648;
                if ((localLayoutParams3.height == 0) && (localLayoutParams3.weight > 0.0F))
                {
                    i21 = 0;
                    localLayoutParams3.height = -2;
                }
                if (f1 == 0.0F);
                for (int i22 = this.mTotalLength; ; i22 = 0)
                {
                    measureChildBeforeLayout(localView4, i7, paramInt1, 0, paramInt2, i22);
                    if (i21 != -2147483648)
                        localLayoutParams3.height = i21;
                    int i23 = localView4.getMeasuredHeight();
                    int i24 = this.mTotalLength;
                    this.mTotalLength = Math.max(i24, i24 + i23 + localLayoutParams3.topMargin + localLayoutParams3.bottomMargin + getNextLocationOffset(localView4));
                    if (!bool)
                        break;
                    i6 = Math.max(i23, i6);
                    break;
                }
            }
            int i25 = 0;
            if ((i2 != 1073741824) && (localLayoutParams3.width == -1))
            {
                i4 = 1;
                i25 = 1;
            }
            int i26 = localLayoutParams3.leftMargin + localLayoutParams3.rightMargin;
            int i27 = i26 + localView4.getMeasuredWidth();
            i = Math.max(i, i27);
            j = combineMeasuredStates(j, localView4.getMeasuredState());
            if ((n != 0) && (localLayoutParams3.width == -1))
            {
                n = 1;
                label505: if (localLayoutParams3.weight <= 0.0F)
                    break label558;
                if (i25 == 0)
                    break label551;
            }
            while (true)
            {
                m = Math.max(m, i26);
                i7 += getChildrenSkipCount(localView4, i7);
                break;
                n = 0;
                break label505;
                label551: i26 = i27;
            }
            label558: if (i25 != 0);
            while (true)
            {
                k = Math.max(k, i26);
                break;
                i26 = i27;
            }
        }
        if ((this.mTotalLength > 0) && (hasDividerBeforeChildAt(i1)))
            this.mTotalLength += this.mDividerHeight;
        if ((bool) && ((i3 == -2147483648) || (i3 == 0)))
        {
            this.mTotalLength = 0;
            int i19 = 0;
            if (i19 < i1)
            {
                View localView3 = getVirtualChildAt(i19);
                if (localView3 == null)
                    this.mTotalLength += measureNullChild(i19);
                while (true)
                {
                    i19++;
                    break;
                    if (localView3.getVisibility() == 8)
                    {
                        i19 += getChildrenSkipCount(localView3, i19);
                    }
                    else
                    {
                        LayoutParams localLayoutParams2 = (LayoutParams)localView3.getLayoutParams();
                        int i20 = this.mTotalLength;
                        this.mTotalLength = Math.max(i20, i20 + i6 + localLayoutParams2.topMargin + localLayoutParams2.bottomMargin + getNextLocationOffset(localView3));
                    }
                }
            }
        }
        this.mTotalLength += this.mPaddingTop + this.mPaddingBottom;
        int i8 = resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumHeight()), paramInt2, 0);
        int i9 = (i8 & 0xFFFFFF) - this.mTotalLength;
        if ((i9 != 0) && (f1 > 0.0F))
        {
            if (this.mWeightSum > 0.0F);
            View localView2;
            for (float f2 = this.mWeightSum; ; f2 = f1)
            {
                this.mTotalLength = 0;
                for (int i11 = 0; ; i11++)
                {
                    if (i11 >= i1)
                        break label1197;
                    localView2 = getVirtualChildAt(i11);
                    if (localView2.getVisibility() != 8)
                        break;
                }
            }
            LayoutParams localLayoutParams1 = (LayoutParams)localView2.getLayoutParams();
            float f3 = localLayoutParams1.weight;
            int i16;
            int i17;
            int i12;
            int i13;
            int i14;
            if (f3 > 0.0F)
            {
                i16 = (int)(f3 * i9 / f2);
                f2 -= f3;
                i9 -= i16;
                i17 = getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight + localLayoutParams1.leftMargin + localLayoutParams1.rightMargin, localLayoutParams1.width);
                if ((localLayoutParams1.height != 0) || (i3 != 1073741824))
                {
                    int i18 = i16 + localView2.getMeasuredHeight();
                    if (i18 < 0)
                        i18 = 0;
                    localView2.measure(i17, View.MeasureSpec.makeMeasureSpec(i18, 1073741824));
                    j = combineMeasuredStates(j, 0xFFFFFF00 & localView2.getMeasuredState());
                }
            }
            else
            {
                i12 = localLayoutParams1.leftMargin + localLayoutParams1.rightMargin;
                i13 = i12 + localView2.getMeasuredWidth();
                i = Math.max(i, i13);
                if ((i2 == 1073741824) || (localLayoutParams1.width != -1))
                    break label1178;
                i14 = 1;
                label1073: if (i14 == 0)
                    break label1184;
                label1078: k = Math.max(k, i12);
                if ((n == 0) || (localLayoutParams1.width != -1))
                    break label1191;
            }
            label1178: label1184: label1191: for (n = 1; ; n = 0)
            {
                int i15 = this.mTotalLength;
                this.mTotalLength = Math.max(i15, i15 + localView2.getMeasuredHeight() + localLayoutParams1.topMargin + localLayoutParams1.bottomMargin + getNextLocationOffset(localView2));
                break;
                if (i16 > 0);
                while (true)
                {
                    localView2.measure(i17, View.MeasureSpec.makeMeasureSpec(i16, 1073741824));
                    break;
                    i16 = 0;
                }
                i14 = 0;
                break label1073;
                i12 = i13;
                break label1078;
            }
            label1197: this.mTotalLength += this.mPaddingTop + this.mPaddingBottom;
        }
        do
        {
            if ((n == 0) && (i2 != 1073741824))
                i = k;
            setMeasuredDimension(resolveSizeAndState(Math.max(i + (this.mPaddingLeft + this.mPaddingRight), getSuggestedMinimumWidth()), paramInt1, j), i8);
            if (i4 != 0)
                forceUniformWidth(i1, paramInt2);
            return;
            k = Math.max(k, m);
        }
        while ((!bool) || (i3 == 1073741824));
        int i10 = 0;
        label1297: View localView1;
        if (i10 < i1)
        {
            localView1 = getVirtualChildAt(i10);
            if ((localView1 != null) && (localView1.getVisibility() != 8))
                break label1333;
        }
        while (true)
        {
            i10++;
            break label1297;
            break;
            label1333: if (((LayoutParams)localView1.getLayoutParams()).weight > 0.0F)
                localView1.measure(View.MeasureSpec.makeMeasureSpec(localView1.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(i6, 1073741824));
        }
    }

    protected void onDraw(Canvas paramCanvas)
    {
        if (this.mDivider == null);
        while (true)
        {
            return;
            if (this.mOrientation == 1)
                drawDividersVertical(paramCanvas);
            else
                drawDividersHorizontal(paramCanvas);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(LinearLayout.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(LinearLayout.class.getName());
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (this.mOrientation == 1)
            layoutVertical();
        while (true)
        {
            return;
            layoutHorizontal();
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        if (this.mOrientation == 1)
            measureVertical(paramInt1, paramInt2);
        while (true)
        {
            return;
            measureHorizontal(paramInt1, paramInt2);
        }
    }

    @RemotableViewMethod
    public void setBaselineAligned(boolean paramBoolean)
    {
        this.mBaselineAligned = paramBoolean;
    }

    @RemotableViewMethod
    public void setBaselineAlignedChildIndex(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= getChildCount()))
            throw new IllegalArgumentException("base aligned child index out of range (0, " + getChildCount() + ")");
        this.mBaselineAlignedChildIndex = paramInt;
    }

    public void setDividerDrawable(Drawable paramDrawable)
    {
        boolean bool = false;
        if (paramDrawable == this.mDivider)
            return;
        this.mDivider = paramDrawable;
        if (paramDrawable != null)
            this.mDividerWidth = paramDrawable.getIntrinsicWidth();
        for (this.mDividerHeight = paramDrawable.getIntrinsicHeight(); ; this.mDividerHeight = 0)
        {
            if (paramDrawable == null)
                bool = true;
            setWillNotDraw(bool);
            requestLayout();
            break;
            this.mDividerWidth = 0;
        }
    }

    public void setDividerPadding(int paramInt)
    {
        this.mDividerPadding = paramInt;
    }

    @RemotableViewMethod
    public void setGravity(int paramInt)
    {
        if (this.mGravity != paramInt)
        {
            if ((0x800007 & paramInt) == 0)
                paramInt |= 8388611;
            if ((paramInt & 0x70) == 0)
                paramInt |= 48;
            this.mGravity = paramInt;
            requestLayout();
        }
    }

    @RemotableViewMethod
    public void setHorizontalGravity(int paramInt)
    {
        int i = paramInt & 0x800007;
        if ((0x800007 & this.mGravity) != i)
        {
            this.mGravity = (i | 0xFF7FFFF8 & this.mGravity);
            requestLayout();
        }
    }

    @RemotableViewMethod
    public void setMeasureWithLargestChildEnabled(boolean paramBoolean)
    {
        this.mUseLargestChild = paramBoolean;
    }

    public void setOrientation(int paramInt)
    {
        if (this.mOrientation != paramInt)
        {
            this.mOrientation = paramInt;
            requestLayout();
        }
    }

    public void setShowDividers(int paramInt)
    {
        if (paramInt != this.mShowDividers)
            requestLayout();
        this.mShowDividers = paramInt;
    }

    @RemotableViewMethod
    public void setVerticalGravity(int paramInt)
    {
        int i = paramInt & 0x70;
        if ((0x70 & this.mGravity) != i)
        {
            this.mGravity = (i | 0xFFFFFF8F & this.mGravity);
            requestLayout();
        }
    }

    @RemotableViewMethod
    public void setWeightSum(float paramFloat)
    {
        this.mWeightSum = Math.max(0.0F, paramFloat);
    }

    public boolean shouldDelayChildPressedState()
    {
        return false;
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams
    {

        @ViewDebug.ExportedProperty(category="layout", mapping={@android.view.ViewDebug.IntToString(from=-1, to="NONE"), @android.view.ViewDebug.IntToString(from=0, to="NONE"), @android.view.ViewDebug.IntToString(from=48, to="TOP"), @android.view.ViewDebug.IntToString(from=80, to="BOTTOM"), @android.view.ViewDebug.IntToString(from=3, to="LEFT"), @android.view.ViewDebug.IntToString(from=5, to="RIGHT"), @android.view.ViewDebug.IntToString(from=8388611, to="START"), @android.view.ViewDebug.IntToString(from=8388613, to="END"), @android.view.ViewDebug.IntToString(from=16, to="CENTER_VERTICAL"), @android.view.ViewDebug.IntToString(from=112, to="FILL_VERTICAL"), @android.view.ViewDebug.IntToString(from=1, to="CENTER_HORIZONTAL"), @android.view.ViewDebug.IntToString(from=7, to="FILL_HORIZONTAL"), @android.view.ViewDebug.IntToString(from=17, to="CENTER"), @android.view.ViewDebug.IntToString(from=119, to="FILL")})
        public int gravity = -1;

        @ViewDebug.ExportedProperty(category="layout")
        public float weight;

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
            this.weight = 0.0F;
        }

        public LayoutParams(int paramInt1, int paramInt2, float paramFloat)
        {
            super(paramInt2);
            this.weight = paramFloat;
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LinearLayout_Layout);
            this.weight = localTypedArray.getFloat(3, 0.0F);
            this.gravity = localTypedArray.getInt(0, -1);
            localTypedArray.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
        {
            super();
        }

        public String debug(String paramString)
        {
            return paramString + "LinearLayout.LayoutParams={width=" + sizeToString(this.width) + ", height=" + sizeToString(this.height) + " weight=" + this.weight + "}";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.LinearLayout
 * JD-Core Version:        0.6.2
 */