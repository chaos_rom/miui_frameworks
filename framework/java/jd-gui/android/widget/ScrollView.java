package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.Span;
import android.util.AttributeSet;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.List;

public class ScrollView extends FrameLayout
{
    static final int ANIMATED_SCROLL_GAP = 250;
    private static final int INVALID_POINTER = -1;
    static final float MAX_SCROLL_FACTOR = 0.5F;
    private int mActivePointerId = -1;
    private View mChildToScrollTo = null;
    private EdgeEffect mEdgeGlowBottom;
    private EdgeEffect mEdgeGlowTop;

    @ViewDebug.ExportedProperty(category="layout")
    private boolean mFillViewport;
    private StrictMode.Span mFlingStrictSpan = null;
    private boolean mIsBeingDragged = false;
    private boolean mIsLayoutDirty = true;
    private int mLastMotionY;
    private long mLastScroll;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private int mOverflingDistance;
    private int mOverscrollDistance;
    private StrictMode.Span mScrollStrictSpan = null;
    private OverScroller mScroller;
    private boolean mSmoothScrollingEnabled = true;
    private final Rect mTempRect = new Rect();
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;

    public ScrollView(Context paramContext)
    {
        this(paramContext, null);
    }

    public ScrollView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842880);
    }

    public ScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initScrollView();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ScrollView, paramInt, 0);
        setFillViewport(localTypedArray.getBoolean(0, false));
        localTypedArray.recycle();
    }

    private boolean canScroll()
    {
        boolean bool = false;
        View localView = getChildAt(0);
        if (localView != null)
        {
            int i = localView.getHeight();
            if (getHeight() < i + this.mPaddingTop + this.mPaddingBottom)
                bool = true;
        }
        return bool;
    }

    private static int clamp(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((paramInt2 >= paramInt3) || (paramInt1 < 0))
            paramInt1 = 0;
        while (true)
        {
            return paramInt1;
            if (paramInt2 + paramInt1 > paramInt3)
                paramInt1 = paramInt3 - paramInt2;
        }
    }

    private void doScrollY(int paramInt)
    {
        if (paramInt != 0)
        {
            if (!this.mSmoothScrollingEnabled)
                break label18;
            smoothScrollBy(0, paramInt);
        }
        while (true)
        {
            return;
            label18: scrollBy(0, paramInt);
        }
    }

    private void endDrag()
    {
        this.mIsBeingDragged = false;
        recycleVelocityTracker();
        if (this.mEdgeGlowTop != null)
        {
            this.mEdgeGlowTop.onRelease();
            this.mEdgeGlowBottom.onRelease();
        }
        if (this.mScrollStrictSpan != null)
        {
            this.mScrollStrictSpan.finish();
            this.mScrollStrictSpan = null;
        }
    }

    private View findFocusableViewInBounds(boolean paramBoolean, int paramInt1, int paramInt2)
    {
        ArrayList localArrayList = getFocusables(2);
        Object localObject = null;
        int i = 0;
        int j = localArrayList.size();
        int k = 0;
        if (k < j)
        {
            View localView = (View)localArrayList.get(k);
            int m = localView.getTop();
            int n = localView.getBottom();
            int i1;
            if ((paramInt1 < n) && (m < paramInt2))
            {
                if ((paramInt1 >= m) || (n >= paramInt2))
                    break label106;
                i1 = 1;
                label87: if (localObject != null)
                    break label112;
                localObject = localView;
                i = i1;
            }
            while (true)
            {
                k++;
                break;
                label106: i1 = 0;
                break label87;
                label112: if (((paramBoolean) && (m < localObject.getTop())) || ((!paramBoolean) && (n > localObject.getBottom())));
                for (int i2 = 1; ; i2 = 0)
                {
                    if (i == 0)
                        break label171;
                    if ((i1 == 0) || (i2 == 0))
                        break;
                    localObject = localView;
                    break;
                }
                label171: if (i1 != 0)
                {
                    localObject = localView;
                    i = 1;
                }
                else if (i2 != 0)
                {
                    localObject = localView;
                }
            }
        }
        return localObject;
    }

    private int getScrollRange()
    {
        int i = 0;
        if (getChildCount() > 0)
            i = Math.max(0, getChildAt(0).getHeight() - (getHeight() - this.mPaddingBottom - this.mPaddingTop));
        return i;
    }

    private boolean inChild(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        if (getChildCount() > 0)
        {
            int i = this.mScrollY;
            View localView = getChildAt(0);
            if ((paramInt2 >= localView.getTop() - i) && (paramInt2 < localView.getBottom() - i) && (paramInt1 >= localView.getLeft()) && (paramInt1 < localView.getRight()))
                bool = true;
        }
        return bool;
    }

    private void initOrResetVelocityTracker()
    {
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        while (true)
        {
            return;
            this.mVelocityTracker.clear();
        }
    }

    private void initScrollView()
    {
        this.mScroller = new OverScroller(getContext());
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(this.mContext);
        this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
        this.mMinimumVelocity = localViewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = localViewConfiguration.getScaledMaximumFlingVelocity();
        this.mOverscrollDistance = localViewConfiguration.getScaledOverscrollDistance();
        this.mOverflingDistance = localViewConfiguration.getScaledOverflingDistance();
    }

    private void initVelocityTrackerIfNotExists()
    {
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
    }

    private boolean isOffScreen(View paramView)
    {
        boolean bool = false;
        if (!isWithinDeltaOfScreen(paramView, 0, getHeight()))
            bool = true;
        return bool;
    }

    private static boolean isViewDescendantOf(View paramView1, View paramView2)
    {
        boolean bool = true;
        if (paramView1 == paramView2);
        while (true)
        {
            return bool;
            ViewParent localViewParent = paramView1.getParent();
            if ((!(localViewParent instanceof ViewGroup)) || (!isViewDescendantOf((View)localViewParent, paramView2)))
                bool = false;
        }
    }

    private boolean isWithinDeltaOfScreen(View paramView, int paramInt1, int paramInt2)
    {
        paramView.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(paramView, this.mTempRect);
        if ((paramInt1 + this.mTempRect.bottom >= getScrollY()) && (this.mTempRect.top - paramInt1 <= paramInt2 + getScrollY()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void onSecondaryPointerUp(MotionEvent paramMotionEvent)
    {
        int i = (0xFF00 & paramMotionEvent.getAction()) >> 8;
        if (paramMotionEvent.getPointerId(i) == this.mActivePointerId)
            if (i != 0)
                break label64;
        label64: for (int j = 1; ; j = 0)
        {
            this.mLastMotionY = ((int)paramMotionEvent.getY(j));
            this.mActivePointerId = paramMotionEvent.getPointerId(j);
            if (this.mVelocityTracker != null)
                this.mVelocityTracker.clear();
            return;
        }
    }

    private void recycleVelocityTracker()
    {
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private boolean scrollAndFocus(int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool1 = true;
        int i = getHeight();
        int j = getScrollY();
        int k = j + i;
        if (paramInt1 == 33);
        for (boolean bool2 = true; ; bool2 = false)
        {
            Object localObject = findFocusableViewInBounds(bool2, paramInt2, paramInt3);
            if (localObject == null)
                localObject = this;
            if ((paramInt2 < j) || (paramInt3 > k))
                break;
            bool1 = false;
            if (localObject != findFocus())
                ((View)localObject).requestFocus(paramInt1);
            return bool1;
        }
        if (bool2);
        for (int m = paramInt2 - j; ; m = paramInt3 - k)
        {
            doScrollY(m);
            break;
        }
    }

    private void scrollToChild(View paramView)
    {
        paramView.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(paramView, this.mTempRect);
        int i = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
        if (i != 0)
            scrollBy(0, i);
    }

    private boolean scrollToChildRect(Rect paramRect, boolean paramBoolean)
    {
        int i = computeScrollDeltaToGetChildRectOnScreen(paramRect);
        boolean bool;
        if (i != 0)
        {
            bool = true;
            if (bool)
            {
                if (!paramBoolean)
                    break label37;
                scrollBy(0, i);
            }
        }
        while (true)
        {
            return bool;
            bool = false;
            break;
            label37: smoothScrollBy(0, i);
        }
    }

    public void addView(View paramView)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("ScrollView can host only one direct child");
        super.addView(paramView);
    }

    public void addView(View paramView, int paramInt)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("ScrollView can host only one direct child");
        super.addView(paramView, paramInt);
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("ScrollView can host only one direct child");
        super.addView(paramView, paramInt, paramLayoutParams);
    }

    public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("ScrollView can host only one direct child");
        super.addView(paramView, paramLayoutParams);
    }

    public boolean arrowScroll(int paramInt)
    {
        boolean bool = false;
        View localView1 = findFocus();
        if (localView1 == this)
            localView1 = null;
        View localView2 = FocusFinder.getInstance().findNextFocus(this, localView1, paramInt);
        int i = getMaxScrollAmount();
        if ((localView2 != null) && (isWithinDeltaOfScreen(localView2, i, getHeight())))
        {
            localView2.getDrawingRect(this.mTempRect);
            offsetDescendantRectToMyCoords(localView2, this.mTempRect);
            doScrollY(computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
            localView2.requestFocus(paramInt);
            if ((localView1 != null) && (localView1.isFocused()) && (isOffScreen(localView1)))
            {
                int i1 = getDescendantFocusability();
                setDescendantFocusability(131072);
                requestFocus();
                setDescendantFocusability(i1);
            }
            bool = true;
            label134: return bool;
        }
        int j = i;
        if ((paramInt == 33) && (getScrollY() < j))
        {
            j = getScrollY();
            label161: if (j == 0)
                break label244;
            if (paramInt != 130)
                break label246;
        }
        label244: label246: for (int n = j; ; n = -j)
        {
            doScrollY(n);
            break;
            if ((paramInt != 130) || (getChildCount() <= 0))
                break label161;
            int k = getChildAt(0).getBottom();
            int m = getScrollY() + getHeight() - this.mPaddingBottom;
            if (k - m >= i)
                break label161;
            j = k - m;
            break label161;
            break label134;
        }
    }

    public void computeScroll()
    {
        int i = 1;
        int k;
        int n;
        int i1;
        if (this.mScroller.computeScrollOffset())
        {
            int j = this.mScrollX;
            k = this.mScrollY;
            int m = this.mScroller.getCurrX();
            n = this.mScroller.getCurrY();
            if ((j != m) || (k != n))
            {
                i1 = getScrollRange();
                int i2 = getOverScrollMode();
                if ((i2 != 0) && ((i2 != i) || (i1 <= 0)))
                    break label158;
                overScrollBy(m - j, n - k, j, k, 0, i1, 0, this.mOverflingDistance, false);
                onScrollChanged(this.mScrollX, this.mScrollY, j, k);
                if (i != 0)
                {
                    if ((n >= 0) || (k < 0))
                        break label163;
                    this.mEdgeGlowTop.onAbsorb((int)this.mScroller.getCurrVelocity());
                }
            }
            label146: if (!awakenScrollBars())
                postInvalidateOnAnimation();
        }
        while (true)
        {
            return;
            label158: i = 0;
            break;
            label163: if ((n <= i1) || (k > i1))
                break label146;
            this.mEdgeGlowBottom.onAbsorb((int)this.mScroller.getCurrVelocity());
            break label146;
            if (this.mFlingStrictSpan != null)
            {
                this.mFlingStrictSpan.finish();
                this.mFlingStrictSpan = null;
            }
        }
    }

    protected int computeScrollDeltaToGetChildRectOnScreen(Rect paramRect)
    {
        int n;
        if (getChildCount() == 0)
            n = 0;
        int i;
        int j;
        int k;
        do
        {
            return n;
            i = getHeight();
            j = getScrollY();
            k = j + i;
            int m = getVerticalFadingEdgeLength();
            if (paramRect.top > 0)
                j += m;
            if (paramRect.bottom < getChildAt(0).getHeight())
                k -= m;
            n = 0;
            if ((paramRect.bottom > k) && (paramRect.top > j))
            {
                if (paramRect.height() > i);
                for (int i2 = 0 + (paramRect.top - j); ; i2 = 0 + (paramRect.bottom - k))
                {
                    n = Math.min(i2, getChildAt(0).getBottom() - k);
                    break;
                }
            }
        }
        while ((paramRect.top >= j) || (paramRect.bottom >= k));
        if (paramRect.height() > i);
        for (int i1 = 0 - (k - paramRect.bottom); ; i1 = 0 - (j - paramRect.top))
        {
            n = Math.max(i1, -getScrollY());
            break;
        }
    }

    protected int computeVerticalScrollOffset()
    {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    protected int computeVerticalScrollRange()
    {
        int i = getChildCount();
        int j = getHeight() - this.mPaddingBottom - this.mPaddingTop;
        if (i == 0)
            return j;
        int k = getChildAt(0).getBottom();
        int m = this.mScrollY;
        int n = Math.max(0, k - j);
        if (m < 0)
            k -= m;
        while (true)
        {
            j = k;
            break;
            if (m > n)
                k += m - n;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if ((super.dispatchKeyEvent(paramKeyEvent)) || (executeKeyEvent(paramKeyEvent)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void draw(Canvas paramCanvas)
    {
        super.draw(paramCanvas);
        if (this.mEdgeGlowTop != null)
        {
            int i = this.mScrollY;
            if (!this.mEdgeGlowTop.isFinished())
            {
                int n = paramCanvas.save();
                int i1 = getWidth() - this.mPaddingLeft - this.mPaddingRight;
                paramCanvas.translate(this.mPaddingLeft, Math.min(0, i));
                this.mEdgeGlowTop.setSize(i1, getHeight());
                if (this.mEdgeGlowTop.draw(paramCanvas))
                    postInvalidateOnAnimation();
                paramCanvas.restoreToCount(n);
            }
            if (!this.mEdgeGlowBottom.isFinished())
            {
                int j = paramCanvas.save();
                int k = getWidth() - this.mPaddingLeft - this.mPaddingRight;
                int m = getHeight();
                paramCanvas.translate(-k + this.mPaddingLeft, m + Math.max(getScrollRange(), i));
                paramCanvas.rotate(180.0F, k, 0.0F);
                this.mEdgeGlowBottom.setSize(k, m);
                if (this.mEdgeGlowBottom.draw(paramCanvas))
                    postInvalidateOnAnimation();
                paramCanvas.restoreToCount(j);
            }
        }
    }

    public boolean executeKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool1 = false;
        this.mTempRect.setEmpty();
        if (!canScroll())
        {
            if ((isFocused()) && (paramKeyEvent.getKeyCode() != 4))
            {
                View localView1 = findFocus();
                if (localView1 == this)
                    localView1 = null;
                View localView2 = FocusFinder.getInstance().findNextFocus(this, localView1, 130);
                if ((localView2 != null) && (localView2 != this) && (localView2.requestFocus(130)))
                    bool1 = true;
            }
            return bool1;
        }
        boolean bool2 = false;
        if (paramKeyEvent.getAction() == 0);
        switch (paramKeyEvent.getKeyCode())
        {
        default:
        case 19:
        case 20:
            while (true)
            {
                bool1 = bool2;
                break;
                if (!paramKeyEvent.isAltPressed())
                {
                    bool2 = arrowScroll(33);
                }
                else
                {
                    bool2 = fullScroll(33);
                    continue;
                    if (!paramKeyEvent.isAltPressed())
                        bool2 = arrowScroll(130);
                    else
                        bool2 = fullScroll(130);
                }
            }
        case 62:
        }
        if (paramKeyEvent.isShiftPressed());
        for (int i = 33; ; i = 130)
        {
            pageScroll(i);
            break;
        }
    }

    public void fling(int paramInt)
    {
        if (getChildCount() > 0)
        {
            int i = getHeight() - this.mPaddingBottom - this.mPaddingTop;
            int j = getChildAt(0).getHeight();
            this.mScroller.fling(this.mScrollX, this.mScrollY, 0, paramInt, 0, 0, 0, Math.max(0, j - i), 0, i / 2);
            if (this.mFlingStrictSpan == null)
                this.mFlingStrictSpan = StrictMode.enterCriticalSpan("ScrollView-fling");
            postInvalidateOnAnimation();
        }
    }

    public boolean fullScroll(int paramInt)
    {
        if (paramInt == 130);
        for (int i = 1; ; i = 0)
        {
            int j = getHeight();
            this.mTempRect.top = 0;
            this.mTempRect.bottom = j;
            if (i != 0)
            {
                int k = getChildCount();
                if (k > 0)
                {
                    View localView = getChildAt(k - 1);
                    this.mTempRect.bottom = (localView.getBottom() + this.mPaddingBottom);
                    this.mTempRect.top = (this.mTempRect.bottom - j);
                }
            }
            return scrollAndFocus(paramInt, this.mTempRect.top, this.mTempRect.bottom);
        }
    }

    protected float getBottomFadingEdgeStrength()
    {
        float f;
        if (getChildCount() == 0)
            f = 0.0F;
        while (true)
        {
            return f;
            int i = getVerticalFadingEdgeLength();
            int j = getHeight() - this.mPaddingBottom;
            int k = getChildAt(0).getBottom() - this.mScrollY - j;
            if (k < i)
                f = k / i;
            else
                f = 1.0F;
        }
    }

    public int getMaxScrollAmount()
    {
        return (int)(0.5F * (this.mBottom - this.mTop));
    }

    protected float getTopFadingEdgeStrength()
    {
        float f;
        if (getChildCount() == 0)
            f = 0.0F;
        while (true)
        {
            return f;
            int i = getVerticalFadingEdgeLength();
            if (this.mScrollY < i)
                f = this.mScrollY / i;
            else
                f = 1.0F;
        }
    }

    public boolean isFillViewport()
    {
        return this.mFillViewport;
    }

    public boolean isSmoothScrollingEnabled()
    {
        return this.mSmoothScrollingEnabled;
    }

    protected void measureChild(View paramView, int paramInt1, int paramInt2)
    {
        ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
        paramView.measure(getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight, localLayoutParams.width), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
        paramView.measure(getChildMeasureSpec(paramInt1, paramInt2 + (this.mPaddingLeft + this.mPaddingRight + localMarginLayoutParams.leftMargin + localMarginLayoutParams.rightMargin), localMarginLayoutParams.width), View.MeasureSpec.makeMeasureSpec(localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin, 0));
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.mScrollStrictSpan != null)
        {
            this.mScrollStrictSpan.finish();
            this.mScrollStrictSpan = null;
        }
        if (this.mFlingStrictSpan != null)
        {
            this.mFlingStrictSpan.finish();
            this.mFlingStrictSpan = null;
        }
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if ((0x2 & paramMotionEvent.getSource()) != 0)
            switch (paramMotionEvent.getAction())
            {
            default:
            case 8:
            }
        label130: 
        while (true)
        {
            boolean bool = super.onGenericMotionEvent(paramMotionEvent);
            return bool;
            if (!this.mIsBeingDragged)
            {
                float f = paramMotionEvent.getAxisValue(9);
                if (f != 0.0F)
                {
                    int i = (int)(f * getVerticalScrollFactor());
                    int j = getScrollRange();
                    int k = this.mScrollY;
                    int m = k - i;
                    if (m < 0)
                        m = 0;
                    while (true)
                    {
                        if (m == k)
                            break label130;
                        super.scrollTo(this.mScrollX, m);
                        bool = true;
                        break;
                        if (m > j)
                            m = j;
                    }
                }
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ScrollView.class.getName());
        if (getScrollRange() > 0);
        for (boolean bool = true; ; bool = false)
        {
            paramAccessibilityEvent.setScrollable(bool);
            paramAccessibilityEvent.setScrollX(this.mScrollX);
            paramAccessibilityEvent.setScrollY(this.mScrollY);
            paramAccessibilityEvent.setMaxScrollX(this.mScrollX);
            paramAccessibilityEvent.setMaxScrollY(getScrollRange());
            return;
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ScrollView.class.getName());
        if (isEnabled())
        {
            int i = getScrollRange();
            if (i > 0)
            {
                paramAccessibilityNodeInfo.setScrollable(true);
                if (this.mScrollY > 0)
                    paramAccessibilityNodeInfo.addAction(8192);
                if (this.mScrollY < i)
                    paramAccessibilityNodeInfo.addAction(4096);
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool1 = true;
        boolean bool2 = false;
        int i = paramMotionEvent.getAction();
        if ((i == 2) && (this.mIsBeingDragged))
            return bool1;
        switch (i & 0xFF)
        {
        case 4:
        case 5:
        default:
        case 2:
        case 0:
        case 1:
        case 3:
        case 6:
        }
        while (true)
        {
            bool1 = this.mIsBeingDragged;
            break;
            int k = this.mActivePointerId;
            if (k != -1)
            {
                int m = (int)paramMotionEvent.getY(paramMotionEvent.findPointerIndex(k));
                if (Math.abs(m - this.mLastMotionY) > this.mTouchSlop)
                {
                    this.mIsBeingDragged = bool1;
                    this.mLastMotionY = m;
                    initVelocityTrackerIfNotExists();
                    this.mVelocityTracker.addMovement(paramMotionEvent);
                    if (this.mScrollStrictSpan == null)
                        this.mScrollStrictSpan = StrictMode.enterCriticalSpan("ScrollView-scroll");
                    ViewParent localViewParent = getParent();
                    if (localViewParent != null)
                    {
                        localViewParent.requestDisallowInterceptTouchEvent(bool1);
                        continue;
                        int j = (int)paramMotionEvent.getY();
                        if (!inChild((int)paramMotionEvent.getX(), j))
                        {
                            this.mIsBeingDragged = false;
                            recycleVelocityTracker();
                        }
                        else
                        {
                            this.mLastMotionY = j;
                            this.mActivePointerId = paramMotionEvent.getPointerId(0);
                            initOrResetVelocityTracker();
                            this.mVelocityTracker.addMovement(paramMotionEvent);
                            if (!this.mScroller.isFinished())
                                bool2 = bool1;
                            this.mIsBeingDragged = bool2;
                            if ((this.mIsBeingDragged) && (this.mScrollStrictSpan == null))
                            {
                                this.mScrollStrictSpan = StrictMode.enterCriticalSpan("ScrollView-scroll");
                                continue;
                                this.mIsBeingDragged = false;
                                this.mActivePointerId = -1;
                                recycleVelocityTracker();
                                if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange()))
                                {
                                    postInvalidateOnAnimation();
                                    continue;
                                    onSecondaryPointerUp(paramMotionEvent);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        this.mIsLayoutDirty = false;
        if ((this.mChildToScrollTo != null) && (isViewDescendantOf(this.mChildToScrollTo, this)))
            scrollToChild(this.mChildToScrollTo);
        this.mChildToScrollTo = null;
        scrollTo(this.mScrollX, this.mScrollY);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        if (!this.mFillViewport);
        while (true)
        {
            return;
            if ((View.MeasureSpec.getMode(paramInt2) != 0) && (getChildCount() > 0))
            {
                View localView = getChildAt(0);
                int i = getMeasuredHeight();
                if (localView.getMeasuredHeight() < i)
                {
                    FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)localView.getLayoutParams();
                    localView.measure(getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight, localLayoutParams.width), View.MeasureSpec.makeMeasureSpec(i - this.mPaddingTop - this.mPaddingBottom, 1073741824));
                }
            }
        }
    }

    protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (!this.mScroller.isFinished())
        {
            this.mScrollX = paramInt1;
            this.mScrollY = paramInt2;
            invalidateParentIfNeeded();
            if (paramBoolean2)
                this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange());
        }
        while (true)
        {
            awakenScrollBars();
            return;
            super.scrollTo(paramInt1, paramInt2);
        }
    }

    protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
    {
        boolean bool = false;
        View localView;
        if (paramInt == 2)
        {
            paramInt = 130;
            if (paramRect != null)
                break label44;
            localView = FocusFinder.getInstance().findNextFocus(this, null, paramInt);
            label26: if (localView != null)
                break label58;
        }
        while (true)
        {
            return bool;
            if (paramInt != 1)
                break;
            paramInt = 33;
            break;
            label44: localView = FocusFinder.getInstance().findNextFocusFromRect(this, paramRect, paramInt);
            break label26;
            label58: if (!isOffScreen(localView))
                bool = localView.requestFocus(paramInt, paramRect);
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        View localView = findFocus();
        if ((localView == null) || (this == localView));
        while (true)
        {
            return;
            if (isWithinDeltaOfScreen(localView, 0, paramInt4))
            {
                localView.getDrawingRect(this.mTempRect);
                offsetDescendantRectToMyCoords(localView, this.mTempRect);
                doScrollY(computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
            }
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        initVelocityTrackerIfNotExists();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        switch (0xFF & paramMotionEvent.getAction())
        {
        case 4:
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        case 5:
        case 6:
        }
        while (true)
        {
            for (boolean bool1 = true; ; bool1 = false)
            {
                return bool1;
                if (getChildCount() != 0)
                    break;
            }
            if (!this.mScroller.isFinished());
            for (boolean bool2 = true; ; bool2 = false)
            {
                this.mIsBeingDragged = bool2;
                if (bool2)
                {
                    ViewParent localViewParent2 = getParent();
                    if (localViewParent2 != null)
                        localViewParent2.requestDisallowInterceptTouchEvent(true);
                }
                if (!this.mScroller.isFinished())
                {
                    this.mScroller.abortAnimation();
                    if (this.mFlingStrictSpan != null)
                    {
                        this.mFlingStrictSpan.finish();
                        this.mFlingStrictSpan = null;
                    }
                }
                this.mLastMotionY = ((int)paramMotionEvent.getY());
                this.mActivePointerId = paramMotionEvent.getPointerId(0);
                break;
            }
            int k = (int)paramMotionEvent.getY(paramMotionEvent.findPointerIndex(this.mActivePointerId));
            int m = this.mLastMotionY - k;
            label267: int i2;
            int i4;
            label323: int i5;
            if ((!this.mIsBeingDragged) && (Math.abs(m) > this.mTouchSlop))
            {
                ViewParent localViewParent1 = getParent();
                if (localViewParent1 != null)
                    localViewParent1.requestDisallowInterceptTouchEvent(true);
                this.mIsBeingDragged = true;
                if (m > 0)
                    m -= this.mTouchSlop;
            }
            else
            {
                if (!this.mIsBeingDragged)
                    continue;
                this.mLastMotionY = k;
                int n = this.mScrollX;
                int i1 = this.mScrollY;
                i2 = getScrollRange();
                int i3 = getOverScrollMode();
                if ((i3 != 0) && ((i3 != 1) || (i2 <= 0)))
                    break label466;
                i4 = 1;
                if (overScrollBy(0, m, 0, this.mScrollY, 0, i2, 0, this.mOverscrollDistance, true))
                    this.mVelocityTracker.clear();
                onScrollChanged(this.mScrollX, this.mScrollY, n, i1);
                if (i4 == 0)
                    continue;
                i5 = i1 + m;
                if (i5 >= 0)
                    break label472;
                this.mEdgeGlowTop.onPull(m / getHeight());
                if (!this.mEdgeGlowBottom.isFinished())
                    this.mEdgeGlowBottom.onRelease();
            }
            while ((this.mEdgeGlowTop != null) && ((!this.mEdgeGlowTop.isFinished()) || (!this.mEdgeGlowBottom.isFinished())))
            {
                postInvalidateOnAnimation();
                break;
                m += this.mTouchSlop;
                break label267;
                label466: i4 = 0;
                break label323;
                label472: if (i5 > i2)
                {
                    this.mEdgeGlowBottom.onPull(m / getHeight());
                    if (!this.mEdgeGlowTop.isFinished())
                        this.mEdgeGlowTop.onRelease();
                }
            }
            if (this.mIsBeingDragged)
            {
                VelocityTracker localVelocityTracker = this.mVelocityTracker;
                localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
                int j = (int)localVelocityTracker.getYVelocity(this.mActivePointerId);
                if (getChildCount() > 0)
                {
                    if (Math.abs(j) <= this.mMinimumVelocity)
                        break label592;
                    fling(-j);
                }
                while (true)
                {
                    this.mActivePointerId = -1;
                    endDrag();
                    break;
                    label592: if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange()))
                        postInvalidateOnAnimation();
                }
                if ((this.mIsBeingDragged) && (getChildCount() > 0))
                {
                    if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange()))
                        postInvalidateOnAnimation();
                    this.mActivePointerId = -1;
                    endDrag();
                    continue;
                    int i = paramMotionEvent.getActionIndex();
                    this.mLastMotionY = ((int)paramMotionEvent.getY(i));
                    this.mActivePointerId = paramMotionEvent.getPointerId(i);
                    continue;
                    onSecondaryPointerUp(paramMotionEvent);
                    this.mLastMotionY = ((int)paramMotionEvent.getY(paramMotionEvent.findPointerIndex(this.mActivePointerId)));
                }
            }
        }
    }

    public boolean pageScroll(int paramInt)
    {
        int i;
        int j;
        if (paramInt == 130)
        {
            i = 1;
            j = getHeight();
            if (i == 0)
                break label124;
            this.mTempRect.top = (j + getScrollY());
            int k = getChildCount();
            if (k > 0)
            {
                View localView = getChildAt(k - 1);
                if (j + this.mTempRect.top > localView.getBottom())
                    this.mTempRect.top = (localView.getBottom() - j);
            }
        }
        while (true)
        {
            this.mTempRect.bottom = (j + this.mTempRect.top);
            return scrollAndFocus(paramInt, this.mTempRect.top, this.mTempRect.bottom);
            i = 0;
            break;
            label124: this.mTempRect.top = (getScrollY() - j);
            if (this.mTempRect.top < 0)
                this.mTempRect.top = 0;
        }
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool = true;
        if (super.performAccessibilityAction(paramInt, paramBundle));
        while (true)
        {
            return bool;
            if (!isEnabled())
                bool = false;
            else
                switch (paramInt)
                {
                default:
                    bool = false;
                    break;
                case 4096:
                    int k = Math.min(getHeight() - this.mPaddingBottom - this.mPaddingTop + this.mScrollY, getScrollRange());
                    if (k != this.mScrollY)
                        smoothScrollTo(0, k);
                    else
                        bool = false;
                    break;
                case 8192:
                    int i = getHeight() - this.mPaddingBottom - this.mPaddingTop;
                    int j = Math.max(this.mScrollY - i, 0);
                    if (j != this.mScrollY)
                        smoothScrollTo(0, j);
                    else
                        bool = false;
                    break;
                }
        }
    }

    public void requestChildFocus(View paramView1, View paramView2)
    {
        if (!this.mIsLayoutDirty)
            scrollToChild(paramView2);
        while (true)
        {
            super.requestChildFocus(paramView1, paramView2);
            return;
            this.mChildToScrollTo = paramView2;
        }
    }

    public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
    {
        paramRect.offset(paramView.getLeft() - paramView.getScrollX(), paramView.getTop() - paramView.getScrollY());
        return scrollToChildRect(paramRect, paramBoolean);
    }

    public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
    {
        if (paramBoolean)
            recycleVelocityTracker();
        super.requestDisallowInterceptTouchEvent(paramBoolean);
    }

    public void requestLayout()
    {
        this.mIsLayoutDirty = true;
        super.requestLayout();
    }

    public void scrollTo(int paramInt1, int paramInt2)
    {
        if (getChildCount() > 0)
        {
            View localView = getChildAt(0);
            int i = clamp(paramInt1, getWidth() - this.mPaddingRight - this.mPaddingLeft, localView.getWidth());
            int j = clamp(paramInt2, getHeight() - this.mPaddingBottom - this.mPaddingTop, localView.getHeight());
            if ((i != this.mScrollX) || (j != this.mScrollY))
                super.scrollTo(i, j);
        }
    }

    public void setFillViewport(boolean paramBoolean)
    {
        if (paramBoolean != this.mFillViewport)
        {
            this.mFillViewport = paramBoolean;
            requestLayout();
        }
    }

    public void setOverScrollMode(int paramInt)
    {
        Context localContext;
        if (paramInt != 2)
            if (this.mEdgeGlowTop == null)
            {
                localContext = getContext();
                this.mEdgeGlowTop = new EdgeEffect(localContext);
            }
        for (this.mEdgeGlowBottom = new EdgeEffect(localContext); ; this.mEdgeGlowBottom = null)
        {
            super.setOverScrollMode(paramInt);
            return;
            this.mEdgeGlowTop = null;
        }
    }

    public void setSmoothScrollingEnabled(boolean paramBoolean)
    {
        this.mSmoothScrollingEnabled = paramBoolean;
    }

    public boolean shouldDelayChildPressedState()
    {
        return true;
    }

    public final void smoothScrollBy(int paramInt1, int paramInt2)
    {
        if (getChildCount() == 0)
            return;
        if (AnimationUtils.currentAnimationTimeMillis() - this.mLastScroll > 250L)
        {
            int i = getHeight() - this.mPaddingBottom - this.mPaddingTop;
            int j = Math.max(0, getChildAt(0).getHeight() - i);
            int k = this.mScrollY;
            int m = Math.max(0, Math.min(k + paramInt2, j)) - k;
            this.mScroller.startScroll(this.mScrollX, k, 0, m);
            postInvalidateOnAnimation();
        }
        while (true)
        {
            this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
            break;
            if (!this.mScroller.isFinished())
            {
                this.mScroller.abortAnimation();
                if (this.mFlingStrictSpan != null)
                {
                    this.mFlingStrictSpan.finish();
                    this.mFlingStrictSpan = null;
                }
            }
            scrollBy(paramInt1, paramInt2);
        }
    }

    public final void smoothScrollTo(int paramInt1, int paramInt2)
    {
        smoothScrollBy(paramInt1 - this.mScrollX, paramInt2 - this.mScrollY);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ScrollView
 * JD-Core Version:        0.6.2
 */