package android.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R.styleable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DatePicker extends FrameLayout
{
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final boolean DEFAULT_CALENDAR_VIEW_SHOWN = true;
    private static final boolean DEFAULT_ENABLED_STATE = true;
    private static final int DEFAULT_END_YEAR = 2100;
    private static final boolean DEFAULT_SPINNERS_SHOWN = true;
    private static final int DEFAULT_START_YEAR = 1900;
    private static final String LOG_TAG = DatePicker.class.getSimpleName();
    private final CalendarView mCalendarView;
    private Calendar mCurrentDate;
    private Locale mCurrentLocale;
    private final java.text.DateFormat mDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private final NumberPicker mDaySpinner;
    private final EditText mDaySpinnerInput;
    private boolean mIsEnabled = true;
    private Calendar mMaxDate;
    private Calendar mMinDate;
    private final NumberPicker mMonthSpinner;
    private final EditText mMonthSpinnerInput;
    private int mNumberOfMonths;
    private OnDateChangedListener mOnDateChangedListener;
    private String[] mShortMonths;
    private final LinearLayout mSpinners;
    private Calendar mTempDate;
    private final NumberPicker mYearSpinner;
    private final EditText mYearSpinnerInput;

    public DatePicker(Context paramContext)
    {
        this(paramContext, null);
    }

    public DatePicker(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843612);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public DatePicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        setCurrentLocale(Locale.getDefault());
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DatePicker, paramInt, 0);
        boolean bool1 = localTypedArray.getBoolean(4, true);
        boolean bool2 = localTypedArray.getBoolean(5, true);
        int i = localTypedArray.getInt(0, 1900);
        int j = localTypedArray.getInt(1, 2100);
        String str1 = localTypedArray.getString(2);
        String str2 = localTypedArray.getString(3);
        int k = localTypedArray.getResourceId(6, 17367093);
        localTypedArray.recycle();
        ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(k, this, true);
        new NumberPicker.OnValueChangeListener()
        {
            public void onValueChange(NumberPicker paramAnonymousNumberPicker, int paramAnonymousInt1, int paramAnonymousInt2)
            {
                DatePicker.this.updateInputState();
                DatePicker.this.mTempDate.setTimeInMillis(DatePicker.this.mCurrentDate.getTimeInMillis());
                int i;
                if (paramAnonymousNumberPicker == DatePicker.this.mDaySpinner)
                {
                    i = DatePicker.this.mTempDate.getActualMaximum(5);
                    if ((paramAnonymousInt1 == i) && (paramAnonymousInt2 == 1))
                        DatePicker.this.mTempDate.add(5, 1);
                }
                while (true)
                {
                    DatePicker.this.setDate(DatePicker.this.mTempDate.get(1), DatePicker.this.mTempDate.get(2), DatePicker.this.mTempDate.get(5));
                    DatePicker.this.updateSpinners();
                    DatePicker.this.updateCalendarView();
                    DatePicker.this.notifyDateChanged();
                    return;
                    if ((paramAnonymousInt1 == 1) && (paramAnonymousInt2 == i))
                    {
                        DatePicker.this.mTempDate.add(5, -1);
                    }
                    else
                    {
                        DatePicker.this.mTempDate.add(5, paramAnonymousInt2 - paramAnonymousInt1);
                        continue;
                        if (paramAnonymousNumberPicker == DatePicker.this.mMonthSpinner)
                        {
                            if ((paramAnonymousInt1 == 11) && (paramAnonymousInt2 == 0))
                                DatePicker.this.mTempDate.add(2, 1);
                            else if ((paramAnonymousInt1 == 0) && (paramAnonymousInt2 == 11))
                                DatePicker.this.mTempDate.add(2, -1);
                            else
                                DatePicker.this.mTempDate.add(2, paramAnonymousInt2 - paramAnonymousInt1);
                        }
                        else
                        {
                            if (paramAnonymousNumberPicker != DatePicker.this.mYearSpinner)
                                break;
                            DatePicker.this.mTempDate.set(1, paramAnonymousInt2);
                        }
                    }
                }
                throw new IllegalArgumentException();
            }
        };
        OnDateChangeListener localOnDateChangeListener = new OnDateChangeListener();
        this.mSpinners = ((LinearLayout)findViewById(16908910));
        this.mCalendarView = ((CalendarView)findViewById(16908914));
        this.mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
        {
            public void onSelectedDayChange(CalendarView paramAnonymousCalendarView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
            {
                DatePicker.this.setDate(paramAnonymousInt1, paramAnonymousInt2, paramAnonymousInt3);
                DatePicker.this.updateSpinners();
                DatePicker.this.notifyDateChanged();
            }
        });
        this.mDaySpinner = ((NumberPicker)findViewById(16908912));
        this.mDaySpinner.setFormatter(NumberPicker.TWO_DIGIT_FORMATTER);
        this.mDaySpinner.setOnLongPressUpdateInterval(100L);
        this.mDaySpinner.setOnValueChangedListener(localOnDateChangeListener);
        this.mDaySpinnerInput = ((EditText)this.mDaySpinner.findViewById(16909052));
        this.mMonthSpinner = ((NumberPicker)findViewById(16908911));
        this.mMonthSpinner.setMinValue(0);
        this.mMonthSpinner.setMaxValue(-1 + this.mNumberOfMonths);
        this.mMonthSpinner.setDisplayedValues(this.mShortMonths);
        this.mMonthSpinner.setOnLongPressUpdateInterval(200L);
        this.mMonthSpinner.setOnValueChangedListener(localOnDateChangeListener);
        this.mMonthSpinnerInput = ((EditText)this.mMonthSpinner.findViewById(16909052));
        this.mYearSpinner = ((NumberPicker)findViewById(16908913));
        this.mYearSpinner.setOnLongPressUpdateInterval(100L);
        this.mYearSpinner.setOnValueChangedListener(localOnDateChangeListener);
        this.mYearSpinnerInput = ((EditText)this.mYearSpinner.findViewById(16909052));
        if ((!bool1) && (!bool2))
        {
            setSpinnersShown(true);
            this.mTempDate.clear();
            if (TextUtils.isEmpty(str1))
                break label572;
            if (!parseDate(str1, this.mTempDate))
                this.mTempDate.set(i, 0, 1);
            label434: setMinDate(this.mTempDate.getTimeInMillis());
            this.mTempDate.clear();
            if (TextUtils.isEmpty(str2))
                break label586;
            if (!parseDate(str2, this.mTempDate))
                this.mTempDate.set(j, 11, 31);
        }
        while (true)
        {
            setMaxDate(this.mTempDate.getTimeInMillis());
            this.mCurrentDate.setTimeInMillis(System.currentTimeMillis());
            init(this.mCurrentDate.get(1), this.mCurrentDate.get(2), this.mCurrentDate.get(5), null);
            reorderSpinners();
            setContentDescriptions();
            if (getImportantForAccessibility() == 0)
                setImportantForAccessibility(1);
            return;
            setSpinnersShown(bool1);
            setCalendarViewShown(bool2);
            break;
            label572: this.mTempDate.set(i, 0, 1);
            break label434;
            label586: this.mTempDate.set(j, 11, 31);
        }
    }

    private Calendar getCalendarForLocale(Calendar paramCalendar, Locale paramLocale)
    {
        Calendar localCalendar;
        if (paramCalendar == null)
            localCalendar = Calendar.getInstance(paramLocale);
        while (true)
        {
            return localCalendar;
            long l = paramCalendar.getTimeInMillis();
            localCalendar = Calendar.getInstance(paramLocale);
            localCalendar.setTimeInMillis(l);
        }
    }

    private boolean isNewDate(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 1;
        if ((this.mCurrentDate.get(i) != paramInt1) || (this.mCurrentDate.get(2) != paramInt3) || (this.mCurrentDate.get(5) != paramInt2));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void notifyDateChanged()
    {
        sendAccessibilityEvent(4);
        if (this.mOnDateChangedListener != null)
            this.mOnDateChangedListener.onDateChanged(this, getYear(), getMonth(), getDayOfMonth());
    }

    private boolean parseDate(String paramString, Calendar paramCalendar)
    {
        try
        {
            paramCalendar.setTime(this.mDateFormat.parse(paramString));
            bool = true;
            return bool;
        }
        catch (ParseException localParseException)
        {
            while (true)
            {
                Log.w(LOG_TAG, "Date: " + paramString + " not in format: " + "MM/dd/yyyy");
                boolean bool = false;
            }
        }
    }

    private void reorderSpinners()
    {
        this.mSpinners.removeAllViews();
        char[] arrayOfChar = android.text.format.DateFormat.getDateFormatOrder(getContext());
        int i = arrayOfChar.length;
        int j = 0;
        if (j < i)
        {
            switch (arrayOfChar[j])
            {
            default:
                throw new IllegalArgumentException();
            case 'd':
                this.mSpinners.addView(this.mDaySpinner);
                setImeOptions(this.mDaySpinner, i, j);
            case 'M':
            case 'y':
            }
            while (true)
            {
                j++;
                break;
                this.mSpinners.addView(this.mMonthSpinner);
                setImeOptions(this.mMonthSpinner, i, j);
                continue;
                this.mSpinners.addView(this.mYearSpinner);
                setImeOptions(this.mYearSpinner, i, j);
            }
        }
    }

    private void setContentDescriptions()
    {
        trySetContentDescription(this.mDaySpinner, 16909051, 17040565);
        trySetContentDescription(this.mDaySpinner, 16909053, 17040566);
        trySetContentDescription(this.mMonthSpinner, 16909051, 17040563);
        trySetContentDescription(this.mMonthSpinner, 16909053, 17040564);
        trySetContentDescription(this.mYearSpinner, 16909051, 17040567);
        trySetContentDescription(this.mYearSpinner, 16909053, 17040568);
    }

    private void setCurrentLocale(Locale paramLocale)
    {
        if (paramLocale.equals(this.mCurrentLocale));
        while (true)
        {
            return;
            this.mCurrentLocale = paramLocale;
            this.mTempDate = getCalendarForLocale(this.mTempDate, paramLocale);
            this.mMinDate = getCalendarForLocale(this.mMinDate, paramLocale);
            this.mMaxDate = getCalendarForLocale(this.mMaxDate, paramLocale);
            this.mCurrentDate = getCalendarForLocale(this.mCurrentDate, paramLocale);
            this.mNumberOfMonths = (1 + this.mTempDate.getActualMaximum(2));
            this.mShortMonths = new String[this.mNumberOfMonths];
            for (int i = 0; i < this.mNumberOfMonths; i++)
                this.mShortMonths[i] = DateUtils.getMonthString(i + 0, 20);
        }
    }

    private void setDate(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mCurrentDate.set(paramInt1, paramInt2, paramInt3);
        if (this.mCurrentDate.before(this.mMinDate))
            this.mCurrentDate.setTimeInMillis(this.mMinDate.getTimeInMillis());
        while (true)
        {
            return;
            if (this.mCurrentDate.after(this.mMaxDate))
                this.mCurrentDate.setTimeInMillis(this.mMaxDate.getTimeInMillis());
        }
    }

    private void setImeOptions(NumberPicker paramNumberPicker, int paramInt1, int paramInt2)
    {
        if (paramInt2 < paramInt1 - 1);
        for (int i = 5; ; i = 6)
        {
            ((TextView)paramNumberPicker.findViewById(16909052)).setImeOptions(i);
            return;
        }
    }

    private void trySetContentDescription(View paramView, int paramInt1, int paramInt2)
    {
        View localView = paramView.findViewById(paramInt1);
        if (localView != null)
            localView.setContentDescription(this.mContext.getString(paramInt2));
    }

    private void updateCalendarView()
    {
        this.mCalendarView.setDate(this.mCurrentDate.getTimeInMillis(), false, false);
    }

    private void updateInputState()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if (localInputMethodManager != null)
        {
            if (!localInputMethodManager.isActive(this.mYearSpinnerInput))
                break label37;
            this.mYearSpinnerInput.clearFocus();
            localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
        while (true)
        {
            return;
            label37: if (localInputMethodManager.isActive(this.mMonthSpinnerInput))
            {
                this.mMonthSpinnerInput.clearFocus();
                localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            }
            else if (localInputMethodManager.isActive(this.mDaySpinnerInput))
            {
                this.mDaySpinnerInput.clearFocus();
                localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        }
    }

    private void updateSpinners()
    {
        if (this.mCurrentDate.equals(this.mMinDate))
        {
            this.mDaySpinner.setMinValue(this.mCurrentDate.get(5));
            this.mDaySpinner.setMaxValue(this.mCurrentDate.getActualMaximum(5));
            this.mDaySpinner.setWrapSelectorWheel(false);
            this.mMonthSpinner.setDisplayedValues(null);
            this.mMonthSpinner.setMinValue(this.mCurrentDate.get(2));
            this.mMonthSpinner.setMaxValue(this.mCurrentDate.getActualMaximum(2));
            this.mMonthSpinner.setWrapSelectorWheel(false);
        }
        while (true)
        {
            String[] arrayOfString = (String[])Arrays.copyOfRange(this.mShortMonths, this.mMonthSpinner.getMinValue(), 1 + this.mMonthSpinner.getMaxValue());
            this.mMonthSpinner.setDisplayedValues(arrayOfString);
            this.mYearSpinner.setMinValue(this.mMinDate.get(1));
            this.mYearSpinner.setMaxValue(this.mMaxDate.get(1));
            this.mYearSpinner.setWrapSelectorWheel(false);
            this.mYearSpinner.setValue(this.mCurrentDate.get(1));
            this.mMonthSpinner.setValue(this.mCurrentDate.get(2));
            this.mDaySpinner.setValue(this.mCurrentDate.get(5));
            return;
            if (this.mCurrentDate.equals(this.mMaxDate))
            {
                this.mDaySpinner.setMinValue(this.mCurrentDate.getActualMinimum(5));
                this.mDaySpinner.setMaxValue(this.mCurrentDate.get(5));
                this.mDaySpinner.setWrapSelectorWheel(false);
                this.mMonthSpinner.setDisplayedValues(null);
                this.mMonthSpinner.setMinValue(this.mCurrentDate.getActualMinimum(2));
                this.mMonthSpinner.setMaxValue(this.mCurrentDate.get(2));
                this.mMonthSpinner.setWrapSelectorWheel(false);
            }
            else
            {
                this.mDaySpinner.setMinValue(1);
                this.mDaySpinner.setMaxValue(this.mCurrentDate.getActualMaximum(5));
                this.mDaySpinner.setWrapSelectorWheel(true);
                this.mMonthSpinner.setDisplayedValues(null);
                this.mMonthSpinner.setMinValue(0);
                this.mMonthSpinner.setMaxValue(11);
                this.mMonthSpinner.setWrapSelectorWheel(true);
            }
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        onPopulateAccessibilityEvent(paramAccessibilityEvent);
        return true;
    }

    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray)
    {
        dispatchThawSelfOnly(paramSparseArray);
    }

    public CalendarView getCalendarView()
    {
        return this.mCalendarView;
    }

    public boolean getCalendarViewShown()
    {
        return this.mCalendarView.isShown();
    }

    public int getDayOfMonth()
    {
        return this.mCurrentDate.get(5);
    }

    public long getMaxDate()
    {
        return this.mCalendarView.getMaxDate();
    }

    public long getMinDate()
    {
        return this.mCalendarView.getMinDate();
    }

    public int getMonth()
    {
        return this.mCurrentDate.get(2);
    }

    public boolean getSpinnersShown()
    {
        return this.mSpinners.isShown();
    }

    public int getYear()
    {
        return this.mCurrentDate.get(1);
    }

    public void init(int paramInt1, int paramInt2, int paramInt3, OnDateChangedListener paramOnDateChangedListener)
    {
        setDate(paramInt1, paramInt2, paramInt3);
        updateSpinners();
        updateCalendarView();
        this.mOnDateChangedListener = paramOnDateChangedListener;
    }

    public boolean isEnabled()
    {
        return this.mIsEnabled;
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        setCurrentLocale(paramConfiguration.locale);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(DatePicker.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(DatePicker.class.getName());
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
        String str = DateUtils.formatDateTime(this.mContext, this.mCurrentDate.getTimeInMillis(), 20);
        paramAccessibilityEvent.getText().add(str);
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        setDate(localSavedState.mYear, localSavedState.mMonth, localSavedState.mDay);
        updateSpinners();
        updateCalendarView();
    }

    protected Parcelable onSaveInstanceState()
    {
        return new SavedState(super.onSaveInstanceState(), getYear(), getMonth(), getDayOfMonth(), null);
    }

    public void setCalendarViewShown(boolean paramBoolean)
    {
        CalendarView localCalendarView = this.mCalendarView;
        if (paramBoolean);
        for (int i = 0; ; i = 8)
        {
            localCalendarView.setVisibility(i);
            return;
        }
    }

    public void setEnabled(boolean paramBoolean)
    {
        if (this.mIsEnabled == paramBoolean);
        while (true)
        {
            return;
            super.setEnabled(paramBoolean);
            this.mDaySpinner.setEnabled(paramBoolean);
            this.mMonthSpinner.setEnabled(paramBoolean);
            this.mYearSpinner.setEnabled(paramBoolean);
            this.mCalendarView.setEnabled(paramBoolean);
            this.mIsEnabled = paramBoolean;
        }
    }

    public void setMaxDate(long paramLong)
    {
        this.mTempDate.setTimeInMillis(paramLong);
        if ((this.mTempDate.get(1) == this.mMaxDate.get(1)) && (this.mTempDate.get(6) != this.mMaxDate.get(6)));
        while (true)
        {
            return;
            this.mMaxDate.setTimeInMillis(paramLong);
            this.mCalendarView.setMaxDate(paramLong);
            if (this.mCurrentDate.after(this.mMaxDate))
            {
                this.mCurrentDate.setTimeInMillis(this.mMaxDate.getTimeInMillis());
                updateCalendarView();
            }
            updateSpinners();
        }
    }

    public void setMinDate(long paramLong)
    {
        this.mTempDate.setTimeInMillis(paramLong);
        if ((this.mTempDate.get(1) == this.mMinDate.get(1)) && (this.mTempDate.get(6) != this.mMinDate.get(6)));
        while (true)
        {
            return;
            this.mMinDate.setTimeInMillis(paramLong);
            this.mCalendarView.setMinDate(paramLong);
            if (this.mCurrentDate.before(this.mMinDate))
            {
                this.mCurrentDate.setTimeInMillis(this.mMinDate.getTimeInMillis());
                updateCalendarView();
            }
            updateSpinners();
        }
    }

    public void setSpinnersShown(boolean paramBoolean)
    {
        LinearLayout localLinearLayout = this.mSpinners;
        if (paramBoolean);
        for (int i = 0; ; i = 8)
        {
            localLinearLayout.setVisibility(i);
            return;
        }
    }

    public void updateDate(int paramInt1, int paramInt2, int paramInt3)
    {
        if (!isNewDate(paramInt1, paramInt2, paramInt3));
        while (true)
        {
            return;
            setDate(paramInt1, paramInt2, paramInt3);
            updateSpinners();
            updateCalendarView();
            notifyDateChanged();
        }
    }

    private static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public DatePicker.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new DatePicker.SavedState(paramAnonymousParcel, null);
            }

            public DatePicker.SavedState[] newArray(int paramAnonymousInt)
            {
                return new DatePicker.SavedState[paramAnonymousInt];
            }
        };
        private final int mDay;
        private final int mMonth;
        private final int mYear;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.mYear = paramParcel.readInt();
            this.mMonth = paramParcel.readInt();
            this.mDay = paramParcel.readInt();
        }

        private SavedState(Parcelable paramParcelable, int paramInt1, int paramInt2, int paramInt3)
        {
            super();
            this.mYear = paramInt1;
            this.mMonth = paramInt2;
            this.mDay = paramInt3;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.mYear);
            paramParcel.writeInt(this.mMonth);
            paramParcel.writeInt(this.mDay);
        }
    }

    public static abstract interface OnDateChangedListener
    {
        public abstract void onDateChanged(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    class OnDateChangeListener
        implements NumberPicker.OnValueChangeListener
    {
        OnDateChangeListener()
        {
        }

        public void onValueChange(NumberPicker paramNumberPicker, int paramInt1, int paramInt2)
        {
            DatePicker.this.updateInputState();
            DatePicker.this.mTempDate.setTimeInMillis(DatePicker.this.mCurrentDate.getTimeInMillis());
            if (paramNumberPicker == DatePicker.this.mDaySpinner)
                DatePicker.this.mTempDate.add(5, paramInt2 - paramInt1);
            while (true)
            {
                DatePicker.this.setDate(DatePicker.this.mTempDate.get(1), DatePicker.this.mTempDate.get(2), DatePicker.this.mTempDate.get(5));
                DatePicker.this.updateSpinners();
                DatePicker.this.updateCalendarView();
                DatePicker.this.notifyDateChanged();
                return;
                if (paramNumberPicker == DatePicker.this.mMonthSpinner)
                {
                    DatePicker.this.mTempDate.add(2, paramInt2 - paramInt1);
                }
                else
                {
                    if (paramNumberPicker != DatePicker.this.mYearSpinner)
                        break;
                    DatePicker.this.mTempDate.set(1, paramInt2);
                }
            }
            throw new IllegalArgumentException();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.DatePicker
 * JD-Core Version:        0.6.2
 */