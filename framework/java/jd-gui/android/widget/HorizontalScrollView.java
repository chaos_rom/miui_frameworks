package android.widget;

import android.R.styleable;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;
import java.util.List;

public class HorizontalScrollView extends FrameLayout
{
    private static final int ANIMATED_SCROLL_GAP = 250;
    private static final int INVALID_POINTER = -1;
    private static final float MAX_SCROLL_FACTOR = 0.5F;
    private int mActivePointerId = -1;
    private View mChildToScrollTo = null;
    private EdgeEffect mEdgeGlowLeft;
    private EdgeEffect mEdgeGlowRight;

    @ViewDebug.ExportedProperty(category="layout")
    private boolean mFillViewport;
    private boolean mIsBeingDragged = false;
    private boolean mIsLayoutDirty = true;
    private int mLastMotionX;
    private long mLastScroll;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private int mOverflingDistance;
    private int mOverscrollDistance;
    private OverScroller mScroller;
    private boolean mSmoothScrollingEnabled = true;
    private final Rect mTempRect = new Rect();
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;

    public HorizontalScrollView(Context paramContext)
    {
        this(paramContext, null);
    }

    public HorizontalScrollView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843603);
    }

    public HorizontalScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initScrollView();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.HorizontalScrollView, paramInt, 0);
        setFillViewport(localTypedArray.getBoolean(0, false));
        localTypedArray.recycle();
    }

    private boolean canScroll()
    {
        boolean bool = false;
        View localView = getChildAt(0);
        if (localView != null)
        {
            int i = localView.getWidth();
            if (getWidth() < i + this.mPaddingLeft + this.mPaddingRight)
                bool = true;
        }
        return bool;
    }

    private static int clamp(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((paramInt2 >= paramInt3) || (paramInt1 < 0))
            paramInt1 = 0;
        while (true)
        {
            return paramInt1;
            if (paramInt2 + paramInt1 > paramInt3)
                paramInt1 = paramInt3 - paramInt2;
        }
    }

    private void doScrollX(int paramInt)
    {
        if (paramInt != 0)
        {
            if (!this.mSmoothScrollingEnabled)
                break label18;
            smoothScrollBy(paramInt, 0);
        }
        while (true)
        {
            return;
            label18: scrollBy(paramInt, 0);
        }
    }

    private View findFocusableViewInBounds(boolean paramBoolean, int paramInt1, int paramInt2)
    {
        ArrayList localArrayList = getFocusables(2);
        Object localObject = null;
        int i = 0;
        int j = localArrayList.size();
        int k = 0;
        if (k < j)
        {
            View localView = (View)localArrayList.get(k);
            int m = localView.getLeft();
            int n = localView.getRight();
            int i1;
            if ((paramInt1 < n) && (m < paramInt2))
            {
                if ((paramInt1 >= m) || (n >= paramInt2))
                    break label106;
                i1 = 1;
                label87: if (localObject != null)
                    break label112;
                localObject = localView;
                i = i1;
            }
            while (true)
            {
                k++;
                break;
                label106: i1 = 0;
                break label87;
                label112: if (((paramBoolean) && (m < localObject.getLeft())) || ((!paramBoolean) && (n > localObject.getRight())));
                for (int i2 = 1; ; i2 = 0)
                {
                    if (i == 0)
                        break label171;
                    if ((i1 == 0) || (i2 == 0))
                        break;
                    localObject = localView;
                    break;
                }
                label171: if (i1 != 0)
                {
                    localObject = localView;
                    i = 1;
                }
                else if (i2 != 0)
                {
                    localObject = localView;
                }
            }
        }
        return localObject;
    }

    private View findFocusableViewInMyBounds(boolean paramBoolean, int paramInt, View paramView)
    {
        int i = getHorizontalFadingEdgeLength() / 2;
        int j = paramInt + i;
        int k = paramInt + getWidth() - i;
        if ((paramView != null) && (paramView.getLeft() < k) && (paramView.getRight() > j));
        while (true)
        {
            return paramView;
            paramView = findFocusableViewInBounds(paramBoolean, j, k);
        }
    }

    private int getScrollRange()
    {
        int i = 0;
        if (getChildCount() > 0)
            i = Math.max(0, getChildAt(0).getWidth() - (getWidth() - this.mPaddingLeft - this.mPaddingRight));
        return i;
    }

    private boolean inChild(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        if (getChildCount() > 0)
        {
            int i = this.mScrollX;
            View localView = getChildAt(0);
            if ((paramInt2 >= localView.getTop()) && (paramInt2 < localView.getBottom()) && (paramInt1 >= localView.getLeft() - i) && (paramInt1 < localView.getRight() - i))
                bool = true;
        }
        return bool;
    }

    private void initOrResetVelocityTracker()
    {
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        while (true)
        {
            return;
            this.mVelocityTracker.clear();
        }
    }

    private void initScrollView()
    {
        this.mScroller = new OverScroller(getContext());
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(this.mContext);
        this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
        this.mMinimumVelocity = localViewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = localViewConfiguration.getScaledMaximumFlingVelocity();
        this.mOverscrollDistance = localViewConfiguration.getScaledOverscrollDistance();
        this.mOverflingDistance = localViewConfiguration.getScaledOverflingDistance();
    }

    private void initVelocityTrackerIfNotExists()
    {
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
    }

    private boolean isOffScreen(View paramView)
    {
        boolean bool = false;
        if (!isWithinDeltaOfScreen(paramView, 0))
            bool = true;
        return bool;
    }

    private static boolean isViewDescendantOf(View paramView1, View paramView2)
    {
        boolean bool = true;
        if (paramView1 == paramView2);
        while (true)
        {
            return bool;
            ViewParent localViewParent = paramView1.getParent();
            if ((!(localViewParent instanceof ViewGroup)) || (!isViewDescendantOf((View)localViewParent, paramView2)))
                bool = false;
        }
    }

    private boolean isWithinDeltaOfScreen(View paramView, int paramInt)
    {
        paramView.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(paramView, this.mTempRect);
        if ((paramInt + this.mTempRect.right >= getScrollX()) && (this.mTempRect.left - paramInt <= getScrollX() + getWidth()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void onSecondaryPointerUp(MotionEvent paramMotionEvent)
    {
        int i = (0xFF00 & paramMotionEvent.getAction()) >> 8;
        if (paramMotionEvent.getPointerId(i) == this.mActivePointerId)
            if (i != 0)
                break label64;
        label64: for (int j = 1; ; j = 0)
        {
            this.mLastMotionX = ((int)paramMotionEvent.getX(j));
            this.mActivePointerId = paramMotionEvent.getPointerId(j);
            if (this.mVelocityTracker != null)
                this.mVelocityTracker.clear();
            return;
        }
    }

    private void recycleVelocityTracker()
    {
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private boolean scrollAndFocus(int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool1 = true;
        int i = getWidth();
        int j = getScrollX();
        int k = j + i;
        if (paramInt1 == 17);
        for (boolean bool2 = true; ; bool2 = false)
        {
            Object localObject = findFocusableViewInBounds(bool2, paramInt2, paramInt3);
            if (localObject == null)
                localObject = this;
            if ((paramInt2 < j) || (paramInt3 > k))
                break;
            bool1 = false;
            if (localObject != findFocus())
                ((View)localObject).requestFocus(paramInt1);
            return bool1;
        }
        if (bool2);
        for (int m = paramInt2 - j; ; m = paramInt3 - k)
        {
            doScrollX(m);
            break;
        }
    }

    private void scrollToChild(View paramView)
    {
        paramView.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(paramView, this.mTempRect);
        int i = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
        if (i != 0)
            scrollBy(i, 0);
    }

    private boolean scrollToChildRect(Rect paramRect, boolean paramBoolean)
    {
        int i = computeScrollDeltaToGetChildRectOnScreen(paramRect);
        boolean bool;
        if (i != 0)
        {
            bool = true;
            if (bool)
            {
                if (!paramBoolean)
                    break label37;
                scrollBy(i, 0);
            }
        }
        while (true)
        {
            return bool;
            bool = false;
            break;
            label37: smoothScrollBy(i, 0);
        }
    }

    public void addView(View paramView)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        super.addView(paramView);
    }

    public void addView(View paramView, int paramInt)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        super.addView(paramView, paramInt);
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        super.addView(paramView, paramInt, paramLayoutParams);
    }

    public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (getChildCount() > 0)
            throw new IllegalStateException("HorizontalScrollView can host only one direct child");
        super.addView(paramView, paramLayoutParams);
    }

    public boolean arrowScroll(int paramInt)
    {
        boolean bool = false;
        View localView1 = findFocus();
        if (localView1 == this)
            localView1 = null;
        View localView2 = FocusFinder.getInstance().findNextFocus(this, localView1, paramInt);
        int i = getMaxScrollAmount();
        if ((localView2 != null) && (isWithinDeltaOfScreen(localView2, i)))
        {
            localView2.getDrawingRect(this.mTempRect);
            offsetDescendantRectToMyCoords(localView2, this.mTempRect);
            doScrollX(computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
            localView2.requestFocus(paramInt);
            if ((localView1 != null) && (localView1.isFocused()) && (isOffScreen(localView1)))
            {
                int i1 = getDescendantFocusability();
                setDescendantFocusability(131072);
                requestFocus();
                setDescendantFocusability(i1);
            }
            bool = true;
            label130: return bool;
        }
        int j = i;
        if ((paramInt == 17) && (getScrollX() < j))
        {
            j = getScrollX();
            label157: if (j == 0)
                break label233;
            if (paramInt != 66)
                break label235;
        }
        label233: label235: for (int n = j; ; n = -j)
        {
            doScrollX(n);
            break;
            if ((paramInt != 66) || (getChildCount() <= 0))
                break label157;
            int k = getChildAt(0).getRight();
            int m = getScrollX() + getWidth();
            if (k - m >= i)
                break label157;
            j = k - m;
            break label157;
            break label130;
        }
    }

    protected int computeHorizontalScrollOffset()
    {
        return Math.max(0, super.computeHorizontalScrollOffset());
    }

    protected int computeHorizontalScrollRange()
    {
        int i = getChildCount();
        int j = getWidth() - this.mPaddingLeft - this.mPaddingRight;
        if (i == 0)
            return j;
        int k = getChildAt(0).getRight();
        int m = this.mScrollX;
        int n = Math.max(0, k - j);
        if (m < 0)
            k -= m;
        while (true)
        {
            j = k;
            break;
            if (m > n)
                k += m - n;
        }
    }

    public void computeScroll()
    {
        int i = 1;
        int j;
        int m;
        int i1;
        if (this.mScroller.computeScrollOffset())
        {
            j = this.mScrollX;
            int k = this.mScrollY;
            m = this.mScroller.getCurrX();
            int n = this.mScroller.getCurrY();
            if ((j != m) || (k != n))
            {
                i1 = getScrollRange();
                int i2 = getOverScrollMode();
                if ((i2 != 0) && ((i2 != i) || (i1 <= 0)))
                    break label158;
                overScrollBy(m - j, n - k, j, k, i1, 0, this.mOverflingDistance, 0, false);
                onScrollChanged(this.mScrollX, this.mScrollY, j, k);
                if (i != 0)
                {
                    if ((m >= 0) || (j < 0))
                        break label163;
                    this.mEdgeGlowLeft.onAbsorb((int)this.mScroller.getCurrVelocity());
                }
            }
        }
        while (true)
        {
            if (!awakenScrollBars())
                postInvalidateOnAnimation();
            return;
            label158: i = 0;
            break;
            label163: if ((m > i1) && (j <= i1))
                this.mEdgeGlowRight.onAbsorb((int)this.mScroller.getCurrVelocity());
        }
    }

    protected int computeScrollDeltaToGetChildRectOnScreen(Rect paramRect)
    {
        int n;
        if (getChildCount() == 0)
            n = 0;
        int i;
        int j;
        int k;
        do
        {
            return n;
            i = getWidth();
            j = getScrollX();
            k = j + i;
            int m = getHorizontalFadingEdgeLength();
            if (paramRect.left > 0)
                j += m;
            if (paramRect.right < getChildAt(0).getWidth())
                k -= m;
            n = 0;
            if ((paramRect.right > k) && (paramRect.left > j))
            {
                if (paramRect.width() > i);
                for (int i2 = 0 + (paramRect.left - j); ; i2 = 0 + (paramRect.right - k))
                {
                    n = Math.min(i2, getChildAt(0).getRight() - k);
                    break;
                }
            }
        }
        while ((paramRect.left >= j) || (paramRect.right >= k));
        if (paramRect.width() > i);
        for (int i1 = 0 - (k - paramRect.right); ; i1 = 0 - (j - paramRect.left))
        {
            n = Math.max(i1, -getScrollX());
            break;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if ((super.dispatchKeyEvent(paramKeyEvent)) || (executeKeyEvent(paramKeyEvent)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void draw(Canvas paramCanvas)
    {
        super.draw(paramCanvas);
        if (this.mEdgeGlowLeft != null)
        {
            int i = this.mScrollX;
            if (!this.mEdgeGlowLeft.isFinished())
            {
                int n = paramCanvas.save();
                int i1 = getHeight() - this.mPaddingTop - this.mPaddingBottom;
                paramCanvas.rotate(270.0F);
                paramCanvas.translate(-i1 + this.mPaddingTop, Math.min(0, i));
                this.mEdgeGlowLeft.setSize(i1, getWidth());
                if (this.mEdgeGlowLeft.draw(paramCanvas))
                    postInvalidateOnAnimation();
                paramCanvas.restoreToCount(n);
            }
            if (!this.mEdgeGlowRight.isFinished())
            {
                int j = paramCanvas.save();
                int k = getWidth();
                int m = getHeight() - this.mPaddingTop - this.mPaddingBottom;
                paramCanvas.rotate(90.0F);
                paramCanvas.translate(-this.mPaddingTop, -(k + Math.max(getScrollRange(), i)));
                this.mEdgeGlowRight.setSize(m, k);
                if (this.mEdgeGlowRight.draw(paramCanvas))
                    postInvalidateOnAnimation();
                paramCanvas.restoreToCount(j);
            }
        }
    }

    public boolean executeKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool1 = false;
        this.mTempRect.setEmpty();
        if (!canScroll())
        {
            if (isFocused())
            {
                View localView1 = findFocus();
                if (localView1 == this)
                    localView1 = null;
                View localView2 = FocusFinder.getInstance().findNextFocus(this, localView1, 66);
                if ((localView2 != null) && (localView2 != this) && (localView2.requestFocus(66)))
                    bool1 = true;
            }
            return bool1;
        }
        boolean bool2 = false;
        if (paramKeyEvent.getAction() == 0);
        switch (paramKeyEvent.getKeyCode())
        {
        default:
        case 21:
        case 22:
            while (true)
            {
                bool1 = bool2;
                break;
                if (!paramKeyEvent.isAltPressed())
                {
                    bool2 = arrowScroll(17);
                }
                else
                {
                    bool2 = fullScroll(17);
                    continue;
                    if (!paramKeyEvent.isAltPressed())
                        bool2 = arrowScroll(66);
                    else
                        bool2 = fullScroll(66);
                }
            }
        case 62:
        }
        if (paramKeyEvent.isShiftPressed());
        for (int i = 17; ; i = 66)
        {
            pageScroll(i);
            break;
        }
    }

    public void fling(int paramInt)
    {
        boolean bool;
        Object localObject;
        if (getChildCount() > 0)
        {
            int i = getWidth() - this.mPaddingRight - this.mPaddingLeft;
            int j = getChildAt(0).getWidth();
            this.mScroller.fling(this.mScrollX, this.mScrollY, paramInt, 0, 0, Math.max(0, j - i), 0, 0, i / 2, 0);
            if (paramInt <= 0)
                break label129;
            bool = true;
            View localView = findFocus();
            localObject = findFocusableViewInMyBounds(bool, this.mScroller.getFinalX(), localView);
            if (localObject == null)
                localObject = this;
            if (localObject != localView)
                if (!bool)
                    break label135;
        }
        label129: label135: for (int k = 66; ; k = 17)
        {
            ((View)localObject).requestFocus(k);
            postInvalidateOnAnimation();
            return;
            bool = false;
            break;
        }
    }

    public boolean fullScroll(int paramInt)
    {
        if (paramInt == 66);
        for (int i = 1; ; i = 0)
        {
            int j = getWidth();
            this.mTempRect.left = 0;
            this.mTempRect.right = j;
            if ((i != 0) && (getChildCount() > 0))
            {
                View localView = getChildAt(0);
                this.mTempRect.right = localView.getRight();
                this.mTempRect.left = (this.mTempRect.right - j);
            }
            return scrollAndFocus(paramInt, this.mTempRect.left, this.mTempRect.right);
        }
    }

    protected float getLeftFadingEdgeStrength()
    {
        float f;
        if (getChildCount() == 0)
            f = 0.0F;
        while (true)
        {
            return f;
            int i = getHorizontalFadingEdgeLength();
            if (this.mScrollX < i)
                f = this.mScrollX / i;
            else
                f = 1.0F;
        }
    }

    public int getMaxScrollAmount()
    {
        return (int)(0.5F * (this.mRight - this.mLeft));
    }

    protected float getRightFadingEdgeStrength()
    {
        float f;
        if (getChildCount() == 0)
            f = 0.0F;
        while (true)
        {
            return f;
            int i = getHorizontalFadingEdgeLength();
            int j = getWidth() - this.mPaddingRight;
            int k = getChildAt(0).getRight() - this.mScrollX - j;
            if (k < i)
                f = k / i;
            else
                f = 1.0F;
        }
    }

    public boolean isFillViewport()
    {
        return this.mFillViewport;
    }

    public boolean isSmoothScrollingEnabled()
    {
        return this.mSmoothScrollingEnabled;
    }

    protected void measureChild(View paramView, int paramInt1, int paramInt2)
    {
        ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
        int i = getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom, localLayoutParams.height);
        paramView.measure(View.MeasureSpec.makeMeasureSpec(0, 0), i);
    }

    protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
        int i = getChildMeasureSpec(paramInt3, paramInt4 + (this.mPaddingTop + this.mPaddingBottom + localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin), localMarginLayoutParams.height);
        paramView.measure(View.MeasureSpec.makeMeasureSpec(localMarginLayoutParams.leftMargin + localMarginLayoutParams.rightMargin, 0), i);
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if ((0x2 & paramMotionEvent.getSource()) != 0)
            switch (paramMotionEvent.getAction())
            {
            default:
            case 8:
            }
        label136: label138: label150: 
        while (true)
        {
            boolean bool = super.onGenericMotionEvent(paramMotionEvent);
            label38: return bool;
            if (!this.mIsBeingDragged)
            {
                float f;
                label64: int j;
                int k;
                int m;
                if ((0x1 & paramMotionEvent.getMetaState()) != 0)
                {
                    f = -paramMotionEvent.getAxisValue(9);
                    if (f == 0.0F)
                        break label136;
                    int i = (int)(f * getHorizontalScrollFactor());
                    j = getScrollRange();
                    k = this.mScrollX;
                    m = k + i;
                    if (m >= 0)
                        break label138;
                    m = 0;
                }
                while (true)
                {
                    if (m == k)
                        break label150;
                    super.scrollTo(m, this.mScrollY);
                    bool = true;
                    break label38;
                    f = paramMotionEvent.getAxisValue(10);
                    break label64;
                    break;
                    if (m > j)
                        m = j;
                }
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(HorizontalScrollView.class.getName());
        if (getScrollRange() > 0);
        for (boolean bool = true; ; bool = false)
        {
            paramAccessibilityEvent.setScrollable(bool);
            paramAccessibilityEvent.setScrollX(this.mScrollX);
            paramAccessibilityEvent.setScrollY(this.mScrollY);
            paramAccessibilityEvent.setMaxScrollX(getScrollRange());
            paramAccessibilityEvent.setMaxScrollY(this.mScrollY);
            return;
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(HorizontalScrollView.class.getName());
        int i = getScrollRange();
        if (i > 0)
        {
            paramAccessibilityNodeInfo.setScrollable(true);
            if ((isEnabled()) && (this.mScrollX > 0))
                paramAccessibilityNodeInfo.addAction(8192);
            if ((isEnabled()) && (this.mScrollX < i))
                paramAccessibilityNodeInfo.addAction(4096);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool1 = true;
        boolean bool2 = false;
        int i = paramMotionEvent.getAction();
        if ((i == 2) && (this.mIsBeingDragged))
            return bool1;
        switch (i & 0xFF)
        {
        case 4:
        default:
        case 2:
        case 0:
        case 1:
        case 3:
        case 5:
        case 6:
        }
        while (true)
        {
            bool1 = this.mIsBeingDragged;
            break;
            int m = this.mActivePointerId;
            if (m != -1)
            {
                int n = (int)paramMotionEvent.getX(paramMotionEvent.findPointerIndex(m));
                if (Math.abs(n - this.mLastMotionX) > this.mTouchSlop)
                {
                    this.mIsBeingDragged = bool1;
                    this.mLastMotionX = n;
                    initVelocityTrackerIfNotExists();
                    this.mVelocityTracker.addMovement(paramMotionEvent);
                    if (this.mParent != null)
                    {
                        this.mParent.requestDisallowInterceptTouchEvent(bool1);
                        continue;
                        int k = (int)paramMotionEvent.getX();
                        if (!inChild(k, (int)paramMotionEvent.getY()))
                        {
                            this.mIsBeingDragged = false;
                            recycleVelocityTracker();
                        }
                        else
                        {
                            this.mLastMotionX = k;
                            this.mActivePointerId = paramMotionEvent.getPointerId(0);
                            initOrResetVelocityTracker();
                            this.mVelocityTracker.addMovement(paramMotionEvent);
                            if (!this.mScroller.isFinished())
                                bool2 = bool1;
                            this.mIsBeingDragged = bool2;
                            continue;
                            this.mIsBeingDragged = false;
                            this.mActivePointerId = -1;
                            if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, getScrollRange(), 0, 0))
                            {
                                postInvalidateOnAnimation();
                                continue;
                                int j = paramMotionEvent.getActionIndex();
                                this.mLastMotionX = ((int)paramMotionEvent.getX(j));
                                this.mActivePointerId = paramMotionEvent.getPointerId(j);
                                continue;
                                onSecondaryPointerUp(paramMotionEvent);
                                this.mLastMotionX = ((int)paramMotionEvent.getX(paramMotionEvent.findPointerIndex(this.mActivePointerId)));
                            }
                        }
                    }
                }
            }
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        this.mIsLayoutDirty = false;
        if ((this.mChildToScrollTo != null) && (isViewDescendantOf(this.mChildToScrollTo, this)))
            scrollToChild(this.mChildToScrollTo);
        this.mChildToScrollTo = null;
        scrollTo(this.mScrollX, this.mScrollY);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        if (!this.mFillViewport);
        while (true)
        {
            return;
            if ((View.MeasureSpec.getMode(paramInt1) != 0) && (getChildCount() > 0))
            {
                View localView = getChildAt(0);
                int i = getMeasuredWidth();
                if (localView.getMeasuredWidth() < i)
                {
                    FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)localView.getLayoutParams();
                    int j = getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom, localLayoutParams.height);
                    localView.measure(View.MeasureSpec.makeMeasureSpec(i - this.mPaddingLeft - this.mPaddingRight, 1073741824), j);
                }
            }
        }
    }

    protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (!this.mScroller.isFinished())
        {
            this.mScrollX = paramInt1;
            this.mScrollY = paramInt2;
            invalidateParentIfNeeded();
            if (paramBoolean1)
                this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, getScrollRange(), 0, 0);
        }
        while (true)
        {
            awakenScrollBars();
            return;
            super.scrollTo(paramInt1, paramInt2);
        }
    }

    protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
    {
        boolean bool = false;
        View localView;
        if (paramInt == 2)
        {
            paramInt = 66;
            if (paramRect != null)
                break label43;
            localView = FocusFinder.getInstance().findNextFocus(this, null, paramInt);
            label25: if (localView != null)
                break label57;
        }
        while (true)
        {
            return bool;
            if (paramInt != 1)
                break;
            paramInt = 17;
            break;
            label43: localView = FocusFinder.getInstance().findNextFocusFromRect(this, paramRect, paramInt);
            break label25;
            label57: if (!isOffScreen(localView))
                bool = localView.requestFocus(paramInt, paramRect);
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        View localView = findFocus();
        if ((localView == null) || (this == localView));
        while (true)
        {
            return;
            if (isWithinDeltaOfScreen(localView, this.mRight - this.mLeft))
            {
                localView.getDrawingRect(this.mTempRect);
                offsetDescendantRectToMyCoords(localView, this.mTempRect);
                doScrollX(computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
            }
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        initVelocityTrackerIfNotExists();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        switch (0xFF & paramMotionEvent.getAction())
        {
        case 4:
        case 5:
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        case 6:
        }
        while (true)
        {
            for (boolean bool1 = true; ; bool1 = false)
            {
                return bool1;
                if (getChildCount() != 0)
                    break;
            }
            if (!this.mScroller.isFinished());
            for (boolean bool2 = true; ; bool2 = false)
            {
                this.mIsBeingDragged = bool2;
                if (bool2)
                {
                    ViewParent localViewParent2 = getParent();
                    if (localViewParent2 != null)
                        localViewParent2.requestDisallowInterceptTouchEvent(true);
                }
                if (!this.mScroller.isFinished())
                    this.mScroller.abortAnimation();
                this.mLastMotionX = ((int)paramMotionEvent.getX());
                this.mActivePointerId = paramMotionEvent.getPointerId(0);
                break;
            }
            int j = (int)paramMotionEvent.getX(paramMotionEvent.findPointerIndex(this.mActivePointerId));
            int k = this.mLastMotionX - j;
            label248: int i1;
            int i3;
            label304: int i4;
            if ((!this.mIsBeingDragged) && (Math.abs(k) > this.mTouchSlop))
            {
                ViewParent localViewParent1 = getParent();
                if (localViewParent1 != null)
                    localViewParent1.requestDisallowInterceptTouchEvent(true);
                this.mIsBeingDragged = true;
                if (k > 0)
                    k -= this.mTouchSlop;
            }
            else
            {
                if (!this.mIsBeingDragged)
                    continue;
                this.mLastMotionX = j;
                int m = this.mScrollX;
                int n = this.mScrollY;
                i1 = getScrollRange();
                int i2 = getOverScrollMode();
                if ((i2 != 0) && ((i2 != 1) || (i1 <= 0)))
                    break label447;
                i3 = 1;
                if (overScrollBy(k, 0, this.mScrollX, 0, i1, 0, this.mOverscrollDistance, 0, true))
                    this.mVelocityTracker.clear();
                onScrollChanged(this.mScrollX, this.mScrollY, m, n);
                if (i3 == 0)
                    continue;
                i4 = m + k;
                if (i4 >= 0)
                    break label453;
                this.mEdgeGlowLeft.onPull(k / getWidth());
                if (!this.mEdgeGlowRight.isFinished())
                    this.mEdgeGlowRight.onRelease();
            }
            while ((this.mEdgeGlowLeft != null) && ((!this.mEdgeGlowLeft.isFinished()) || (!this.mEdgeGlowRight.isFinished())))
            {
                postInvalidateOnAnimation();
                break;
                k += this.mTouchSlop;
                break label248;
                label447: i3 = 0;
                break label304;
                label453: if (i4 > i1)
                {
                    this.mEdgeGlowRight.onPull(k / getWidth());
                    if (!this.mEdgeGlowLeft.isFinished())
                        this.mEdgeGlowLeft.onRelease();
                }
            }
            if (this.mIsBeingDragged)
            {
                VelocityTracker localVelocityTracker = this.mVelocityTracker;
                localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
                int i = (int)localVelocityTracker.getXVelocity(this.mActivePointerId);
                if (getChildCount() > 0)
                {
                    if (Math.abs(i) <= this.mMinimumVelocity)
                        break label596;
                    fling(-i);
                }
                while (true)
                {
                    this.mActivePointerId = -1;
                    this.mIsBeingDragged = false;
                    recycleVelocityTracker();
                    if (this.mEdgeGlowLeft == null)
                        break;
                    this.mEdgeGlowLeft.onRelease();
                    this.mEdgeGlowRight.onRelease();
                    break;
                    label596: if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, getScrollRange(), 0, 0))
                        postInvalidateOnAnimation();
                }
                if ((this.mIsBeingDragged) && (getChildCount() > 0))
                {
                    if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, getScrollRange(), 0, 0))
                        postInvalidateOnAnimation();
                    this.mActivePointerId = -1;
                    this.mIsBeingDragged = false;
                    recycleVelocityTracker();
                    if (this.mEdgeGlowLeft != null)
                    {
                        this.mEdgeGlowLeft.onRelease();
                        this.mEdgeGlowRight.onRelease();
                        continue;
                        onSecondaryPointerUp(paramMotionEvent);
                    }
                }
            }
        }
    }

    public boolean pageScroll(int paramInt)
    {
        int i;
        int j;
        if (paramInt == 66)
        {
            i = 1;
            j = getWidth();
            if (i == 0)
                break label116;
            this.mTempRect.left = (j + getScrollX());
            if (getChildCount() > 0)
            {
                View localView = getChildAt(0);
                if (j + this.mTempRect.left > localView.getRight())
                    this.mTempRect.left = (localView.getRight() - j);
            }
        }
        while (true)
        {
            this.mTempRect.right = (j + this.mTempRect.left);
            return scrollAndFocus(paramInt, this.mTempRect.left, this.mTempRect.right);
            i = 0;
            break;
            label116: this.mTempRect.left = (getScrollX() - j);
            if (this.mTempRect.left < 0)
                this.mTempRect.left = 0;
        }
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool = true;
        if (super.performAccessibilityAction(paramInt, paramBundle));
        while (true)
        {
            return bool;
            switch (paramInt)
            {
            default:
                bool = false;
                break;
            case 4096:
                if (!isEnabled())
                {
                    bool = false;
                }
                else
                {
                    int k = Math.min(getWidth() - this.mPaddingLeft - this.mPaddingRight + this.mScrollX, getScrollRange());
                    if (k != this.mScrollX)
                        smoothScrollTo(k, 0);
                    else
                        bool = false;
                }
                break;
            case 8192:
                if (!isEnabled())
                {
                    bool = false;
                }
                else
                {
                    int i = getWidth() - this.mPaddingLeft - this.mPaddingRight;
                    int j = Math.max(0, this.mScrollX - i);
                    if (j != this.mScrollX)
                        smoothScrollTo(j, 0);
                    else
                        bool = false;
                }
                break;
            }
        }
    }

    public void requestChildFocus(View paramView1, View paramView2)
    {
        if (!this.mIsLayoutDirty)
            scrollToChild(paramView2);
        while (true)
        {
            super.requestChildFocus(paramView1, paramView2);
            return;
            this.mChildToScrollTo = paramView2;
        }
    }

    public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
    {
        paramRect.offset(paramView.getLeft() - paramView.getScrollX(), paramView.getTop() - paramView.getScrollY());
        return scrollToChildRect(paramRect, paramBoolean);
    }

    public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
    {
        if (paramBoolean)
            recycleVelocityTracker();
        super.requestDisallowInterceptTouchEvent(paramBoolean);
    }

    public void requestLayout()
    {
        this.mIsLayoutDirty = true;
        super.requestLayout();
    }

    public void scrollTo(int paramInt1, int paramInt2)
    {
        if (getChildCount() > 0)
        {
            View localView = getChildAt(0);
            int i = clamp(paramInt1, getWidth() - this.mPaddingRight - this.mPaddingLeft, localView.getWidth());
            int j = clamp(paramInt2, getHeight() - this.mPaddingBottom - this.mPaddingTop, localView.getHeight());
            if ((i != this.mScrollX) || (j != this.mScrollY))
                super.scrollTo(i, j);
        }
    }

    public void setFillViewport(boolean paramBoolean)
    {
        if (paramBoolean != this.mFillViewport)
        {
            this.mFillViewport = paramBoolean;
            requestLayout();
        }
    }

    public void setOverScrollMode(int paramInt)
    {
        Context localContext;
        if (paramInt != 2)
            if (this.mEdgeGlowLeft == null)
            {
                localContext = getContext();
                this.mEdgeGlowLeft = new EdgeEffect(localContext);
            }
        for (this.mEdgeGlowRight = new EdgeEffect(localContext); ; this.mEdgeGlowRight = null)
        {
            super.setOverScrollMode(paramInt);
            return;
            this.mEdgeGlowLeft = null;
        }
    }

    public void setSmoothScrollingEnabled(boolean paramBoolean)
    {
        this.mSmoothScrollingEnabled = paramBoolean;
    }

    public boolean shouldDelayChildPressedState()
    {
        return true;
    }

    public final void smoothScrollBy(int paramInt1, int paramInt2)
    {
        if (getChildCount() == 0)
            return;
        if (AnimationUtils.currentAnimationTimeMillis() - this.mLastScroll > 250L)
        {
            int i = getWidth() - this.mPaddingRight - this.mPaddingLeft;
            int j = Math.max(0, getChildAt(0).getWidth() - i);
            int k = this.mScrollX;
            int m = Math.max(0, Math.min(k + paramInt1, j)) - k;
            this.mScroller.startScroll(k, this.mScrollY, m, 0);
            postInvalidateOnAnimation();
        }
        while (true)
        {
            this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
            break;
            if (!this.mScroller.isFinished())
                this.mScroller.abortAnimation();
            scrollBy(paramInt1, paramInt2);
        }
    }

    public final void smoothScrollTo(int paramInt1, int paramInt2)
    {
        smoothScrollBy(paramInt1 - this.mScrollX, paramInt2 - this.mScrollY);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.HorizontalScrollView
 * JD-Core Version:        0.6.2
 */