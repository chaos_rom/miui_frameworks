package android.widget;

import android.view.View;
import android.view.ViewGroup;

public abstract interface SpinnerAdapter extends Adapter
{
    public abstract View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SpinnerAdapter
 * JD-Core Version:        0.6.2
 */