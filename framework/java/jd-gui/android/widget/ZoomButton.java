package android.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class ZoomButton extends ImageButton
    implements View.OnLongClickListener
{
    private final Handler mHandler = new Handler();
    private boolean mIsInLongpress;
    private final Runnable mRunnable = new Runnable()
    {
        public void run()
        {
            if ((ZoomButton.this.hasOnClickListeners()) && (ZoomButton.this.mIsInLongpress) && (ZoomButton.this.isEnabled()))
            {
                ZoomButton.this.callOnClick();
                ZoomButton.this.mHandler.postDelayed(this, ZoomButton.this.mZoomSpeed);
            }
        }
    };
    private long mZoomSpeed = 1000L;

    public ZoomButton(Context paramContext)
    {
        this(paramContext, null);
    }

    public ZoomButton(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public ZoomButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        setOnLongClickListener(this);
    }

    public boolean dispatchUnhandledMove(View paramView, int paramInt)
    {
        clearFocus();
        return super.dispatchUnhandledMove(paramView, paramInt);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ZoomButton.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ZoomButton.class.getName());
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        this.mIsInLongpress = false;
        return super.onKeyUp(paramInt, paramKeyEvent);
    }

    public boolean onLongClick(View paramView)
    {
        this.mIsInLongpress = true;
        this.mHandler.post(this.mRunnable);
        return true;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if ((paramMotionEvent.getAction() == 3) || (paramMotionEvent.getAction() == 1))
            this.mIsInLongpress = false;
        return super.onTouchEvent(paramMotionEvent);
    }

    public void setEnabled(boolean paramBoolean)
    {
        if (!paramBoolean)
            setPressed(false);
        super.setEnabled(paramBoolean);
    }

    public void setZoomSpeed(long paramLong)
    {
        this.mZoomSpeed = paramLong;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ZoomButton
 * JD-Core Version:        0.6.2
 */