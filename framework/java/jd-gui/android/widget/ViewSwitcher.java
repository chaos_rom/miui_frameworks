package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class ViewSwitcher extends ViewAnimator
{
    ViewFactory mFactory;

    public ViewSwitcher(Context paramContext)
    {
        super(paramContext);
    }

    public ViewSwitcher(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    private View obtainView()
    {
        View localView = this.mFactory.makeView();
        FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)localView.getLayoutParams();
        if (localLayoutParams == null)
            localLayoutParams = new FrameLayout.LayoutParams(-1, -2);
        addView(localView, localLayoutParams);
        return localView;
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (getChildCount() >= 2)
            throw new IllegalStateException("Can't add more than 2 views to a ViewSwitcher");
        super.addView(paramView, paramInt, paramLayoutParams);
    }

    public View getNextView()
    {
        if (this.mWhichChild == 0);
        for (int i = 1; ; i = 0)
            return getChildAt(i);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ViewSwitcher.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ViewSwitcher.class.getName());
    }

    public void reset()
    {
        this.mFirstTime = true;
        View localView1 = getChildAt(0);
        if (localView1 != null)
            localView1.setVisibility(8);
        View localView2 = getChildAt(1);
        if (localView2 != null)
            localView2.setVisibility(8);
    }

    public void setFactory(ViewFactory paramViewFactory)
    {
        this.mFactory = paramViewFactory;
        obtainView();
        obtainView();
    }

    public static abstract interface ViewFactory
    {
        public abstract View makeView();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ViewSwitcher
 * JD-Core Version:        0.6.2
 */