package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class NumberPicker extends LinearLayout
{
    private static final int DEFAULT_LAYOUT_RESOURCE_ID = 17367160;
    private static final long DEFAULT_LONG_PRESS_UPDATE_INTERVAL = 300L;
    private static final char[] DIGIT_CHARACTERS = arrayOfChar;
    private static final int SELECTOR_ADJUSTMENT_DURATION_MILLIS = 800;
    private static final int SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT = 8;
    private static final int SELECTOR_MIDDLE_ITEM_INDEX = 1;
    private static final int SELECTOR_WHEEL_ITEM_COUNT = 3;
    private static final int SIZE_UNSPECIFIED = -1;
    private static final int SNAP_SCROLL_DURATION = 300;
    private static final float TOP_AND_BOTTOM_FADING_EDGE_STRENGTH = 0.9F;
    public static final Formatter TWO_DIGIT_FORMATTER = new Formatter()
    {
        final Object[] mArgs = new Object[1];
        final StringBuilder mBuilder = new StringBuilder();
        final Formatter mFmt = new Formatter(this.mBuilder, Locale.US);

        public String format(int paramAnonymousInt)
        {
            this.mArgs[0] = Integer.valueOf(paramAnonymousInt);
            this.mBuilder.delete(0, this.mBuilder.length());
            this.mFmt.format("%02d", this.mArgs);
            return this.mFmt.toString();
        }
    };
    private static final int UNSCALED_DEFAULT_SELECTION_DIVIDERS_DISTANCE = 48;
    private static final int UNSCALED_DEFAULT_SELECTION_DIVIDER_HEIGHT = 2;
    private AccessibilityNodeProviderImpl mAccessibilityNodeProvider;
    private final Scroller mAdjustScroller;
    private BeginSoftInputOnLongPressCommand mBeginSoftInputOnLongPressCommand;
    private int mBottomSelectionDividerBottom;
    private ChangeCurrentByOneFromLongPressCommand mChangeCurrentByOneFromLongPressCommand;
    private final boolean mComputeMaxWidth;
    private int mCurrentScrollOffset;
    private final ImageButton mDecrementButton;
    private boolean mDecrementVirtualButtonPressed;
    private String[] mDisplayedValues;
    private final Scroller mFlingScroller;
    private Formatter mFormatter;
    private final boolean mHasSelectorWheel;
    private final ImageButton mIncrementButton;
    private boolean mIncrementVirtualButtonPressed;
    private boolean mIngonreMoveEvents;
    private int mInitialScrollOffset = -2147483648;
    private final EditText mInputText;
    private long mLastDownEventTime;
    private float mLastDownEventY;
    private float mLastDownOrMoveEventY;
    private int mLastHoveredChildVirtualViewId;
    private long mLongPressUpdateInterval = 300L;
    private final int mMaxHeight;
    private int mMaxValue;
    private int mMaxWidth;
    private int mMaximumFlingVelocity;
    private final int mMinHeight;
    private int mMinValue;
    private final int mMinWidth;
    private int mMinimumFlingVelocity;
    private OnScrollListener mOnScrollListener;
    private OnValueChangeListener mOnValueChangeListener;
    private final PressedStateHelper mPressedStateHelper;
    private int mPreviousScrollerY;
    private int mScrollState = 0;
    private final Drawable mSelectionDivider;
    private final int mSelectionDividerHeight;
    private final int mSelectionDividersDistance;
    private int mSelectorElementHeight;
    private final SparseArray<String> mSelectorIndexToStringCache = new SparseArray();
    private final int[] mSelectorIndices = new int[3];
    private int mSelectorTextGapHeight;
    private final Paint mSelectorWheelPaint;
    private SetSelectionCommand mSetSelectionCommand;
    private boolean mShowSoftInputOnTap;
    private final int mSolidColor;
    private final int mTextSize;
    private int mTopSelectionDividerTop;
    private int mTouchSlop;
    private int mValue;
    private VelocityTracker mVelocityTracker;
    private final Drawable mVirtualButtonPressedDrawable;
    private boolean mWrapSelectorWheel;

    static
    {
        char[] arrayOfChar = new char[10];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
    }

    public NumberPicker(Context paramContext)
    {
        this(paramContext, null);
    }

    public NumberPicker(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843714);
    }

    public NumberPicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.NumberPicker, paramInt, 0);
        int i = localTypedArray.getResourceId(1, 17367160);
        if (i != 17367160);
        for (boolean bool1 = true; ; bool1 = false)
        {
            this.mHasSelectorWheel = bool1;
            this.mSolidColor = localTypedArray.getColor(0, 0);
            this.mSelectionDivider = localTypedArray.getDrawable(2);
            this.mSelectionDividerHeight = localTypedArray.getDimensionPixelSize(3, (int)TypedValue.applyDimension(1, 2.0F, getResources().getDisplayMetrics()));
            this.mSelectionDividersDistance = localTypedArray.getDimensionPixelSize(4, (int)TypedValue.applyDimension(1, 48.0F, getResources().getDisplayMetrics()));
            this.mMinHeight = localTypedArray.getDimensionPixelSize(5, -1);
            this.mMaxHeight = localTypedArray.getDimensionPixelSize(6, -1);
            if ((this.mMinHeight == -1) || (this.mMaxHeight == -1) || (this.mMinHeight <= this.mMaxHeight))
                break;
            throw new IllegalArgumentException("minHeight > maxHeight");
        }
        this.mMinWidth = localTypedArray.getDimensionPixelSize(7, -1);
        this.mMaxWidth = localTypedArray.getDimensionPixelSize(8, -1);
        if ((this.mMinWidth != -1) && (this.mMaxWidth != -1) && (this.mMinWidth > this.mMaxWidth))
            throw new IllegalArgumentException("minWidth > maxWidth");
        boolean bool2;
        boolean bool3;
        if (this.mMaxWidth == -1)
        {
            bool2 = true;
            this.mComputeMaxWidth = bool2;
            this.mVirtualButtonPressedDrawable = localTypedArray.getDrawable(9);
            localTypedArray.recycle();
            this.mPressedStateHelper = new PressedStateHelper();
            if (this.mHasSelectorWheel)
                break label729;
            bool3 = true;
            label341: setWillNotDraw(bool3);
            ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(i, this, true);
            View.OnClickListener local2 = new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    NumberPicker.this.hideSoftInput();
                    NumberPicker.this.mInputText.clearFocus();
                    if (paramAnonymousView.getId() == 16909051)
                        NumberPicker.this.changeValueByOne(true);
                    while (true)
                    {
                        return;
                        NumberPicker.this.changeValueByOne(false);
                    }
                }
            };
            View.OnLongClickListener local3 = new View.OnLongClickListener()
            {
                public boolean onLongClick(View paramAnonymousView)
                {
                    NumberPicker.this.hideSoftInput();
                    NumberPicker.this.mInputText.clearFocus();
                    if (paramAnonymousView.getId() == 16909051)
                        NumberPicker.this.postChangeCurrentByOneFromLongPress(true, 0L);
                    while (true)
                    {
                        return true;
                        NumberPicker.this.postChangeCurrentByOneFromLongPress(false, 0L);
                    }
                }
            };
            if (this.mHasSelectorWheel)
                break label735;
            this.mIncrementButton = ((ImageButton)findViewById(16909051));
            this.mIncrementButton.setOnClickListener(local2);
            this.mIncrementButton.setOnLongClickListener(local3);
            label427: if (this.mHasSelectorWheel)
                break label743;
            this.mDecrementButton = ((ImageButton)findViewById(16909053));
            this.mDecrementButton.setOnClickListener(local2);
            this.mDecrementButton.setOnLongClickListener(local3);
        }
        while (true)
        {
            this.mInputText = ((EditText)findViewById(16909052));
            this.mInputText.setOnFocusChangeListener(new View.OnFocusChangeListener()
            {
                public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
                {
                    if (paramAnonymousBoolean)
                        NumberPicker.this.mInputText.selectAll();
                    while (true)
                    {
                        return;
                        NumberPicker.this.mInputText.setSelection(0, 0);
                        NumberPicker.this.validateInputTextView(paramAnonymousView);
                    }
                }
            });
            EditText localEditText = this.mInputText;
            InputFilter[] arrayOfInputFilter = new InputFilter[1];
            arrayOfInputFilter[0] = new InputTextFilter();
            localEditText.setFilters(arrayOfInputFilter);
            this.mInputText.setRawInputType(2);
            this.mInputText.setImeOptions(6);
            ViewConfiguration localViewConfiguration = ViewConfiguration.get(paramContext);
            this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
            this.mMinimumFlingVelocity = localViewConfiguration.getScaledMinimumFlingVelocity();
            this.mMaximumFlingVelocity = (localViewConfiguration.getScaledMaximumFlingVelocity() / 8);
            this.mTextSize = ((int)this.mInputText.getTextSize());
            Paint localPaint = new Paint();
            localPaint.setAntiAlias(true);
            localPaint.setTextAlign(Paint.Align.CENTER);
            localPaint.setTextSize(this.mTextSize);
            localPaint.setTypeface(this.mInputText.getTypeface());
            localPaint.setColor(this.mInputText.getTextColors().getColorForState(ENABLED_STATE_SET, -1));
            this.mSelectorWheelPaint = localPaint;
            this.mFlingScroller = new Scroller(getContext(), null, true);
            this.mAdjustScroller = new Scroller(getContext(), new DecelerateInterpolator(2.5F));
            updateInputTextView();
            if (getImportantForAccessibility() == 0)
                setImportantForAccessibility(1);
            return;
            bool2 = false;
            break;
            label729: bool3 = false;
            break label341;
            label735: this.mIncrementButton = null;
            break label427;
            label743: this.mDecrementButton = null;
        }
    }

    private void changeValueByOne(boolean paramBoolean)
    {
        if (this.mHasSelectorWheel)
        {
            this.mInputText.setVisibility(4);
            if (!moveToFinalScrollerPosition(this.mFlingScroller))
                moveToFinalScrollerPosition(this.mAdjustScroller);
            this.mPreviousScrollerY = 0;
            if (paramBoolean)
            {
                this.mFlingScroller.startScroll(0, 0, 0, -this.mSelectorElementHeight, 300);
                invalidate();
            }
        }
        while (true)
        {
            return;
            this.mFlingScroller.startScroll(0, 0, 0, this.mSelectorElementHeight, 300);
            break;
            if (paramBoolean)
                setValueInternal(1 + this.mValue, true);
            else
                setValueInternal(-1 + this.mValue, true);
        }
    }

    private void decrementSelectorIndices(int[] paramArrayOfInt)
    {
        for (int i = -1 + paramArrayOfInt.length; i > 0; i--)
            paramArrayOfInt[i] = paramArrayOfInt[(i - 1)];
        int j = -1 + paramArrayOfInt[1];
        if ((this.mWrapSelectorWheel) && (j < this.mMinValue))
            j = this.mMaxValue;
        paramArrayOfInt[0] = j;
        ensureCachedScrollSelectorValue(j);
    }

    private void ensureCachedScrollSelectorValue(int paramInt)
    {
        SparseArray localSparseArray = this.mSelectorIndexToStringCache;
        if ((String)localSparseArray.get(paramInt) != null)
            return;
        String str;
        if ((paramInt < this.mMinValue) || (paramInt > this.mMaxValue))
            str = "";
        while (true)
        {
            localSparseArray.put(paramInt, str);
            break;
            if (this.mDisplayedValues != null)
            {
                int i = paramInt - this.mMinValue;
                str = this.mDisplayedValues[i];
            }
            else
            {
                str = formatNumber(paramInt);
            }
        }
    }

    private boolean ensureScrollWheelAdjusted()
    {
        boolean bool = false;
        int i = this.mInitialScrollOffset - this.mCurrentScrollOffset;
        if (i != 0)
        {
            this.mPreviousScrollerY = 0;
            if (Math.abs(i) > this.mSelectorElementHeight / 2)
                if (i <= 0)
                    break label70;
        }
        label70: for (int j = -this.mSelectorElementHeight; ; j = this.mSelectorElementHeight)
        {
            i += j;
            this.mAdjustScroller.startScroll(0, 0, 0, i, 800);
            invalidate();
            bool = true;
            return bool;
        }
    }

    private void fling(int paramInt)
    {
        this.mPreviousScrollerY = 0;
        if (paramInt > 0)
            this.mFlingScroller.fling(0, 0, 0, paramInt, 0, 0, 0, 2147483647);
        while (true)
        {
            invalidate();
            return;
            this.mFlingScroller.fling(0, 2147483647, 0, paramInt, 0, 0, 0, 2147483647);
        }
    }

    private String formatNumber(int paramInt)
    {
        if (this.mFormatter != null);
        for (String str = this.mFormatter.format(paramInt); ; str = String.valueOf(paramInt))
            return str;
    }

    // ERROR //
    private int getSelectedPos(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 602	android/widget/NumberPicker:mDisplayedValues	[Ljava/lang/String;
        //     4: ifnonnull +16 -> 20
        //     7: aload_1
        //     8: invokestatic 668	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     11: istore 7
        //     13: iload 7
        //     15: istore 4
        //     17: iload 4
        //     19: ireturn
        //     20: iconst_0
        //     21: istore_2
        //     22: iload_2
        //     23: aload_0
        //     24: getfield 602	android/widget/NumberPicker:mDisplayedValues	[Ljava/lang/String;
        //     27: arraylength
        //     28: if_icmpge +41 -> 69
        //     31: aload_1
        //     32: invokevirtual 672	java/lang/String:toLowerCase	()Ljava/lang/String;
        //     35: astore_1
        //     36: aload_0
        //     37: getfield 602	android/widget/NumberPicker:mDisplayedValues	[Ljava/lang/String;
        //     40: iload_2
        //     41: aaload
        //     42: invokevirtual 672	java/lang/String:toLowerCase	()Ljava/lang/String;
        //     45: aload_1
        //     46: invokevirtual 676	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     49: ifeq +14 -> 63
        //     52: iload_2
        //     53: aload_0
        //     54: getfield 590	android/widget/NumberPicker:mMinValue	I
        //     57: iadd
        //     58: istore 4
        //     60: goto -43 -> 17
        //     63: iinc 2 1
        //     66: goto -44 -> 22
        //     69: aload_1
        //     70: invokestatic 668	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     73: istore 5
        //     75: iload 5
        //     77: istore 4
        //     79: goto -62 -> 17
        //     82: astore 6
        //     84: aload_0
        //     85: getfield 590	android/widget/NumberPicker:mMinValue	I
        //     88: istore 4
        //     90: goto -73 -> 17
        //     93: astore_3
        //     94: goto -10 -> 84
        //
        // Exception table:
        //     from	to	target	type
        //     7	13	82	java/lang/NumberFormatException
        //     69	75	93	java/lang/NumberFormatException
    }

    private int getWrappedSelectorIndex(int paramInt)
    {
        if (paramInt > this.mMaxValue)
            paramInt = -1 + (this.mMinValue + (paramInt - this.mMaxValue) % (this.mMaxValue - this.mMinValue));
        while (true)
        {
            return paramInt;
            if (paramInt < this.mMinValue)
                paramInt = 1 + (this.mMaxValue - (this.mMinValue - paramInt) % (this.mMaxValue - this.mMinValue));
        }
    }

    private void hideSoftInput()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if ((localInputMethodManager != null) && (localInputMethodManager.isActive(this.mInputText)))
        {
            localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.mHasSelectorWheel)
                this.mInputText.setVisibility(4);
        }
    }

    private void incrementSelectorIndices(int[] paramArrayOfInt)
    {
        for (int i = 0; i < -1 + paramArrayOfInt.length; i++)
            paramArrayOfInt[i] = paramArrayOfInt[(i + 1)];
        int j = 1 + paramArrayOfInt[(-2 + paramArrayOfInt.length)];
        if ((this.mWrapSelectorWheel) && (j > this.mMaxValue))
            j = this.mMinValue;
        paramArrayOfInt[(-1 + paramArrayOfInt.length)] = j;
        ensureCachedScrollSelectorValue(j);
    }

    private void initializeFadingEdges()
    {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength((this.mBottom - this.mTop - this.mTextSize) / 2);
    }

    private void initializeSelectorWheel()
    {
        initializeSelectorWheelIndices();
        int[] arrayOfInt = this.mSelectorIndices;
        int i = arrayOfInt.length * this.mTextSize;
        this.mSelectorTextGapHeight = ((int)(0.5F + (this.mBottom - this.mTop - i) / arrayOfInt.length));
        this.mSelectorElementHeight = (this.mTextSize + this.mSelectorTextGapHeight);
        this.mInitialScrollOffset = (this.mInputText.getBaseline() + this.mInputText.getTop() - 1 * this.mSelectorElementHeight);
        this.mCurrentScrollOffset = this.mInitialScrollOffset;
        updateInputTextView();
    }

    private void initializeSelectorWheelIndices()
    {
        this.mSelectorIndexToStringCache.clear();
        int[] arrayOfInt = this.mSelectorIndices;
        int i = getValue();
        for (int j = 0; j < this.mSelectorIndices.length; j++)
        {
            int k = i + (j - 1);
            if (this.mWrapSelectorWheel)
                k = getWrappedSelectorIndex(k);
            arrayOfInt[j] = k;
            ensureCachedScrollSelectorValue(arrayOfInt[j]);
        }
    }

    private int makeMeasureSpec(int paramInt1, int paramInt2)
    {
        if (paramInt2 == -1);
        while (true)
        {
            return paramInt1;
            int i = View.MeasureSpec.getSize(paramInt1);
            int j = View.MeasureSpec.getMode(paramInt1);
            switch (j)
            {
            case 1073741824:
            default:
                throw new IllegalArgumentException("Unknown measure mode: " + j);
            case -2147483648:
                paramInt1 = View.MeasureSpec.makeMeasureSpec(Math.min(i, paramInt2), 1073741824);
                break;
            case 0:
                paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
            }
        }
    }

    private boolean moveToFinalScrollerPosition(Scroller paramScroller)
    {
        boolean bool = true;
        paramScroller.forceFinished(bool);
        int i = paramScroller.getFinalY() - paramScroller.getCurrY();
        int j = (i + this.mCurrentScrollOffset) % this.mSelectorElementHeight;
        int k = this.mInitialScrollOffset - j;
        if (k != 0)
            if (Math.abs(k) > this.mSelectorElementHeight / 2)
            {
                if (k > 0)
                    k -= this.mSelectorElementHeight;
            }
            else
                scrollBy(0, i + k);
        while (true)
        {
            return bool;
            k += this.mSelectorElementHeight;
            break;
            bool = false;
        }
    }

    private void notifyChange(int paramInt1, int paramInt2)
    {
        if (this.mOnValueChangeListener != null)
            this.mOnValueChangeListener.onValueChange(this, paramInt1, this.mValue);
    }

    private void onScrollStateChange(int paramInt)
    {
        if (this.mScrollState == paramInt);
        while (true)
        {
            return;
            this.mScrollState = paramInt;
            if (this.mOnScrollListener != null)
                this.mOnScrollListener.onScrollStateChange(this, paramInt);
        }
    }

    private void onScrollerFinished(Scroller paramScroller)
    {
        if (paramScroller == this.mFlingScroller)
        {
            if (!ensureScrollWheelAdjusted())
                updateInputTextView();
            onScrollStateChange(0);
        }
        while (true)
        {
            return;
            if (this.mScrollState != 1)
                updateInputTextView();
        }
    }

    private void postBeginSoftInputOnLongPressCommand()
    {
        if (this.mBeginSoftInputOnLongPressCommand == null)
            this.mBeginSoftInputOnLongPressCommand = new BeginSoftInputOnLongPressCommand();
        while (true)
        {
            postDelayed(this.mBeginSoftInputOnLongPressCommand, ViewConfiguration.getLongPressTimeout());
            return;
            removeCallbacks(this.mBeginSoftInputOnLongPressCommand);
        }
    }

    private void postChangeCurrentByOneFromLongPress(boolean paramBoolean, long paramLong)
    {
        if (this.mChangeCurrentByOneFromLongPressCommand == null)
            this.mChangeCurrentByOneFromLongPressCommand = new ChangeCurrentByOneFromLongPressCommand();
        while (true)
        {
            this.mChangeCurrentByOneFromLongPressCommand.setStep(paramBoolean);
            postDelayed(this.mChangeCurrentByOneFromLongPressCommand, paramLong);
            return;
            removeCallbacks(this.mChangeCurrentByOneFromLongPressCommand);
        }
    }

    private void postSetSelectionCommand(int paramInt1, int paramInt2)
    {
        if (this.mSetSelectionCommand == null)
            this.mSetSelectionCommand = new SetSelectionCommand();
        while (true)
        {
            SetSelectionCommand.access$602(this.mSetSelectionCommand, paramInt1);
            SetSelectionCommand.access$702(this.mSetSelectionCommand, paramInt2);
            post(this.mSetSelectionCommand);
            return;
            removeCallbacks(this.mSetSelectionCommand);
        }
    }

    private void removeAllCallbacks()
    {
        if (this.mChangeCurrentByOneFromLongPressCommand != null)
            removeCallbacks(this.mChangeCurrentByOneFromLongPressCommand);
        if (this.mSetSelectionCommand != null)
            removeCallbacks(this.mSetSelectionCommand);
        if (this.mBeginSoftInputOnLongPressCommand != null)
            removeCallbacks(this.mBeginSoftInputOnLongPressCommand);
        this.mPressedStateHelper.cancel();
    }

    private void removeBeginSoftInputCommand()
    {
        if (this.mBeginSoftInputOnLongPressCommand != null)
            removeCallbacks(this.mBeginSoftInputOnLongPressCommand);
    }

    private void removeChangeCurrentByOneFromLongPress()
    {
        if (this.mChangeCurrentByOneFromLongPressCommand != null)
            removeCallbacks(this.mChangeCurrentByOneFromLongPressCommand);
    }

    private int resolveSizeAndStateRespectingMinSize(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt1 != -1)
            paramInt2 = resolveSizeAndState(Math.max(paramInt1, paramInt2), paramInt3, 0);
        return paramInt2;
    }

    private void setValueInternal(int paramInt, boolean paramBoolean)
    {
        if (this.mValue == paramInt)
            return;
        if (this.mWrapSelectorWheel);
        for (int i = getWrappedSelectorIndex(paramInt); ; i = Math.min(Math.max(paramInt, this.mMinValue), this.mMaxValue))
        {
            int j = this.mValue;
            this.mValue = i;
            updateInputTextView();
            if (paramBoolean)
                notifyChange(j, i);
            initializeSelectorWheelIndices();
            invalidate();
            break;
        }
    }

    private void showSoftInput()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if (localInputMethodManager != null)
        {
            if (this.mHasSelectorWheel)
                this.mInputText.setVisibility(0);
            this.mInputText.requestFocus();
            localInputMethodManager.showSoftInput(this.mInputText, 0);
        }
    }

    private void tryComputeMaxWidth()
    {
        if (!this.mComputeMaxWidth);
        int i;
        label97: int m;
        do
        {
            return;
            i = 0;
            if (this.mDisplayedValues != null)
                break;
            float f2 = 0.0F;
            for (int n = 0; n <= 9; n++)
            {
                float f3 = this.mSelectorWheelPaint.measureText(String.valueOf(n));
                if (f3 > f2)
                    f2 = f3;
            }
            int i1 = 0;
            int i2 = this.mMaxValue;
            while (i2 > 0)
            {
                i1++;
                i2 /= 10;
            }
            i = (int)(f2 * i1);
            m = i + (this.mInputText.getPaddingLeft() + this.mInputText.getPaddingRight());
        }
        while (this.mMaxWidth == m);
        if (m > this.mMinWidth);
        for (this.mMaxWidth = m; ; this.mMaxWidth = this.mMinWidth)
        {
            invalidate();
            break;
            int j = this.mDisplayedValues.length;
            for (int k = 0; k < j; k++)
            {
                float f1 = this.mSelectorWheelPaint.measureText(this.mDisplayedValues[k]);
                if (f1 > i)
                    i = (int)f1;
            }
            break label97;
        }
    }

    private boolean updateInputTextView()
    {
        String str;
        if (this.mDisplayedValues == null)
        {
            str = formatNumber(this.mValue);
            if ((TextUtils.isEmpty(str)) || (str.equals(this.mInputText.getText().toString())))
                break label70;
            this.mInputText.setText(str);
        }
        label70: for (boolean bool = true; ; bool = false)
        {
            return bool;
            str = this.mDisplayedValues[(this.mValue - this.mMinValue)];
            break;
        }
    }

    private void validateInputTextView(View paramView)
    {
        String str = String.valueOf(((TextView)paramView).getText());
        if (TextUtils.isEmpty(str))
            updateInputTextView();
        while (true)
        {
            return;
            setValueInternal(getSelectedPos(str.toString()), true);
        }
    }

    public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
    {
        if (((paramInt2 & 0x2) == 2) && (isAccessibilityFocusable()))
            paramArrayList.add(this);
        while (true)
        {
            return;
            super.addFocusables(paramArrayList, paramInt1, paramInt2);
        }
    }

    public void computeScroll()
    {
        Scroller localScroller = this.mFlingScroller;
        if (localScroller.isFinished())
        {
            localScroller = this.mAdjustScroller;
            if (!localScroller.isFinished());
        }
        while (true)
        {
            return;
            localScroller.computeScrollOffset();
            int i = localScroller.getCurrY();
            if (this.mPreviousScrollerY == 0)
                this.mPreviousScrollerY = localScroller.getStartY();
            scrollBy(0, i - this.mPreviousScrollerY);
            this.mPreviousScrollerY = i;
            if (localScroller.isFinished())
                onScrollerFinished(localScroller);
            else
                invalidate();
        }
    }

    protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if (!this.mHasSelectorWheel)
        {
            bool = super.dispatchHoverEvent(paramMotionEvent);
            return bool;
        }
        int i;
        int j;
        label45: AccessibilityNodeProviderImpl localAccessibilityNodeProviderImpl;
        if (AccessibilityManager.getInstance(this.mContext).isEnabled())
        {
            i = (int)paramMotionEvent.getY();
            if (i >= this.mTopSelectionDividerTop)
                break label97;
            j = 3;
            int k = paramMotionEvent.getActionMasked();
            localAccessibilityNodeProviderImpl = (AccessibilityNodeProviderImpl)getAccessibilityNodeProvider();
            switch (k)
            {
            case 8:
            default:
            case 9:
            case 7:
            case 10:
            }
        }
        while (true)
        {
            bool = false;
            break;
            label97: if (i > this.mBottomSelectionDividerBottom)
            {
                j = 1;
                break label45;
            }
            j = 2;
            break label45;
            localAccessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(j, 128);
            this.mLastHoveredChildVirtualViewId = j;
            localAccessibilityNodeProviderImpl.performAction(j, 64, null);
            continue;
            if ((this.mLastHoveredChildVirtualViewId != j) && (this.mLastHoveredChildVirtualViewId != -1))
            {
                localAccessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(this.mLastHoveredChildVirtualViewId, 256);
                localAccessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(j, 128);
                this.mLastHoveredChildVirtualViewId = j;
                localAccessibilityNodeProviderImpl.performAction(j, 64, null);
                continue;
                localAccessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(j, 256);
                this.mLastHoveredChildVirtualViewId = -1;
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        switch (paramKeyEvent.getKeyCode())
        {
        default:
        case 23:
        case 66:
        }
        while (true)
        {
            return super.dispatchKeyEvent(paramKeyEvent);
            removeAllCallbacks();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        switch (paramMotionEvent.getActionMasked())
        {
        case 2:
        default:
        case 1:
        case 3:
        }
        while (true)
        {
            return super.dispatchTouchEvent(paramMotionEvent);
            removeAllCallbacks();
        }
    }

    public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
    {
        switch (paramMotionEvent.getActionMasked())
        {
        case 2:
        default:
        case 1:
        case 3:
        }
        while (true)
        {
            return super.dispatchTrackballEvent(paramMotionEvent);
            removeAllCallbacks();
        }
    }

    public AccessibilityNodeProvider getAccessibilityNodeProvider()
    {
        if (!this.mHasSelectorWheel);
        for (Object localObject = super.getAccessibilityNodeProvider(); ; localObject = this.mAccessibilityNodeProvider)
        {
            return localObject;
            if (this.mAccessibilityNodeProvider == null)
                this.mAccessibilityNodeProvider = new AccessibilityNodeProviderImpl();
        }
    }

    protected float getBottomFadingEdgeStrength()
    {
        return 0.9F;
    }

    public String[] getDisplayedValues()
    {
        return this.mDisplayedValues;
    }

    public int getMaxValue()
    {
        return this.mMaxValue;
    }

    public int getMinValue()
    {
        return this.mMinValue;
    }

    public int getSolidColor()
    {
        return this.mSolidColor;
    }

    protected float getTopFadingEdgeStrength()
    {
        return 0.9F;
    }

    public int getValue()
    {
        return this.mValue;
    }

    public boolean getWrapSelectorWheel()
    {
        return this.mWrapSelectorWheel;
    }

    protected void onDetachedFromWindow()
    {
        removeAllCallbacks();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        if (!this.mHasSelectorWheel)
            super.onDraw(paramCanvas);
        while (true)
        {
            return;
            float f1 = (this.mRight - this.mLeft) / 2;
            float f2 = this.mCurrentScrollOffset;
            if ((this.mVirtualButtonPressedDrawable != null) && (this.mScrollState == 0))
            {
                if (this.mDecrementVirtualButtonPressed)
                {
                    this.mVirtualButtonPressedDrawable.setState(PRESSED_STATE_SET);
                    this.mVirtualButtonPressedDrawable.setBounds(0, 0, this.mRight, this.mTopSelectionDividerTop);
                    this.mVirtualButtonPressedDrawable.draw(paramCanvas);
                }
                if (this.mIncrementVirtualButtonPressed)
                {
                    this.mVirtualButtonPressedDrawable.setState(PRESSED_STATE_SET);
                    this.mVirtualButtonPressedDrawable.setBounds(0, this.mBottomSelectionDividerBottom, this.mRight, this.mBottom);
                    this.mVirtualButtonPressedDrawable.draw(paramCanvas);
                }
            }
            int[] arrayOfInt = this.mSelectorIndices;
            for (int i = 0; i < arrayOfInt.length; i++)
            {
                int i1 = arrayOfInt[i];
                String str = (String)this.mSelectorIndexToStringCache.get(i1);
                if ((i != 1) || (this.mInputText.getVisibility() != 0))
                    paramCanvas.drawText(str, f1, f2, this.mSelectorWheelPaint);
                f2 += this.mSelectorElementHeight;
            }
            if (this.mSelectionDivider != null)
            {
                int j = this.mTopSelectionDividerTop;
                int k = j + this.mSelectionDividerHeight;
                this.mSelectionDivider.setBounds(0, j, this.mRight, k);
                this.mSelectionDivider.draw(paramCanvas);
                int m = this.mBottomSelectionDividerBottom;
                int n = m - this.mSelectionDividerHeight;
                this.mSelectionDivider.setBounds(0, n, this.mRight, m);
                this.mSelectionDivider.draw(paramCanvas);
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(NumberPicker.class.getName());
        paramAccessibilityEvent.setScrollable(true);
        paramAccessibilityEvent.setScrollY((this.mMinValue + this.mValue) * this.mSelectorElementHeight);
        paramAccessibilityEvent.setMaxScrollY((this.mMaxValue - this.mMinValue) * this.mSelectorElementHeight);
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = 1;
        if ((!this.mHasSelectorWheel) || (!isEnabled()))
            i = 0;
        while (true)
        {
            return i;
            switch (paramMotionEvent.getActionMasked())
            {
            default:
                i = 0;
                break;
            case 0:
                removeAllCallbacks();
                this.mInputText.setVisibility(4);
                float f = paramMotionEvent.getY();
                this.mLastDownEventY = f;
                this.mLastDownOrMoveEventY = f;
                this.mLastDownEventTime = paramMotionEvent.getEventTime();
                this.mIngonreMoveEvents = false;
                this.mShowSoftInputOnTap = false;
                if (this.mLastDownEventY < this.mTopSelectionDividerTop)
                    if (this.mScrollState == 0)
                        this.mPressedStateHelper.buttonPressDelayed(2);
                while (true)
                {
                    getParent().requestDisallowInterceptTouchEvent(i);
                    if (this.mFlingScroller.isFinished())
                        break label197;
                    this.mFlingScroller.forceFinished(i);
                    this.mAdjustScroller.forceFinished(i);
                    onScrollStateChange(0);
                    break;
                    if ((this.mLastDownEventY > this.mBottomSelectionDividerBottom) && (this.mScrollState == 0))
                        this.mPressedStateHelper.buttonPressDelayed(i);
                }
                label197: if (!this.mAdjustScroller.isFinished())
                {
                    this.mFlingScroller.forceFinished(i);
                    this.mAdjustScroller.forceFinished(i);
                }
                else if (this.mLastDownEventY < this.mTopSelectionDividerTop)
                {
                    hideSoftInput();
                    postChangeCurrentByOneFromLongPress(false, ViewConfiguration.getLongPressTimeout());
                }
                else if (this.mLastDownEventY > this.mBottomSelectionDividerBottom)
                {
                    hideSoftInput();
                    postChangeCurrentByOneFromLongPress(i, ViewConfiguration.getLongPressTimeout());
                }
                else
                {
                    this.mShowSoftInputOnTap = i;
                    postBeginSoftInputOnLongPressCommand();
                }
                break;
            }
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (!this.mHasSelectorWheel)
            super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        while (true)
        {
            return;
            int i = getMeasuredWidth();
            int j = getMeasuredHeight();
            int k = this.mInputText.getMeasuredWidth();
            int m = this.mInputText.getMeasuredHeight();
            int n = (i - k) / 2;
            int i1 = (j - m) / 2;
            int i2 = n + k;
            int i3 = i1 + m;
            this.mInputText.layout(n, i1, i2, i3);
            if (paramBoolean)
            {
                initializeSelectorWheel();
                initializeFadingEdges();
                this.mTopSelectionDividerTop = ((getHeight() - this.mSelectionDividersDistance) / 2 - this.mSelectionDividerHeight);
                this.mBottomSelectionDividerBottom = (this.mTopSelectionDividerTop + 2 * this.mSelectionDividerHeight + this.mSelectionDividersDistance);
            }
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        if (!this.mHasSelectorWheel)
            super.onMeasure(paramInt1, paramInt2);
        while (true)
        {
            return;
            super.onMeasure(makeMeasureSpec(paramInt1, this.mMaxWidth), makeMeasureSpec(paramInt2, this.mMaxHeight));
            setMeasuredDimension(resolveSizeAndStateRespectingMinSize(this.mMinWidth, getMeasuredWidth(), paramInt1), resolveSizeAndStateRespectingMinSize(this.mMinHeight, getMeasuredHeight(), paramInt2));
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if ((!isEnabled()) || (!this.mHasSelectorWheel))
        {
            bool = false;
            return bool;
        }
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        switch (paramMotionEvent.getActionMasked())
        {
        default:
        case 2:
        case 1:
        }
        while (true)
        {
            bool = true;
            break;
            if (!this.mIngonreMoveEvents)
            {
                float f = paramMotionEvent.getY();
                if (this.mScrollState != 1)
                    if ((int)Math.abs(f - this.mLastDownEventY) > this.mTouchSlop)
                    {
                        removeAllCallbacks();
                        onScrollStateChange(1);
                    }
                while (true)
                {
                    this.mLastDownOrMoveEventY = f;
                    break;
                    scrollBy(0, (int)(f - this.mLastDownOrMoveEventY));
                    invalidate();
                }
                removeBeginSoftInputCommand();
                removeChangeCurrentByOneFromLongPress();
                this.mPressedStateHelper.cancel();
                VelocityTracker localVelocityTracker = this.mVelocityTracker;
                localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumFlingVelocity);
                int i = (int)localVelocityTracker.getYVelocity();
                if (Math.abs(i) <= this.mMinimumFlingVelocity)
                    break label227;
                fling(i);
                onScrollStateChange(2);
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
            }
        }
        label227: int j = (int)paramMotionEvent.getY();
        int k = (int)Math.abs(j - this.mLastDownEventY);
        long l = paramMotionEvent.getEventTime() - this.mLastDownEventTime;
        if ((k <= this.mTouchSlop) && (l < ViewConfiguration.getTapTimeout()))
            if (this.mShowSoftInputOnTap)
            {
                this.mShowSoftInputOnTap = false;
                showSoftInput();
            }
        while (true)
        {
            onScrollStateChange(0);
            break;
            int m = -1 + j / this.mSelectorElementHeight;
            if (m > 0)
            {
                changeValueByOne(true);
                this.mPressedStateHelper.buttonTapped(1);
            }
            else if (m < 0)
            {
                changeValueByOne(false);
                this.mPressedStateHelper.buttonTapped(2);
                continue;
                ensureScrollWheelAdjusted();
            }
        }
    }

    public void scrollBy(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt = this.mSelectorIndices;
        if ((!this.mWrapSelectorWheel) && (paramInt2 > 0) && (arrayOfInt[1] <= this.mMinValue))
            this.mCurrentScrollOffset = this.mInitialScrollOffset;
        while (true)
        {
            return;
            if ((!this.mWrapSelectorWheel) && (paramInt2 < 0) && (arrayOfInt[1] >= this.mMaxValue))
            {
                this.mCurrentScrollOffset = this.mInitialScrollOffset;
            }
            else
            {
                for (this.mCurrentScrollOffset = (paramInt2 + this.mCurrentScrollOffset); this.mCurrentScrollOffset - this.mInitialScrollOffset > this.mSelectorTextGapHeight; this.mCurrentScrollOffset = this.mInitialScrollOffset)
                {
                    label77: this.mCurrentScrollOffset -= this.mSelectorElementHeight;
                    decrementSelectorIndices(arrayOfInt);
                    setValueInternal(arrayOfInt[1], true);
                    if ((this.mWrapSelectorWheel) || (arrayOfInt[1] > this.mMinValue))
                        break label77;
                }
                while (this.mCurrentScrollOffset - this.mInitialScrollOffset < -this.mSelectorTextGapHeight)
                {
                    this.mCurrentScrollOffset += this.mSelectorElementHeight;
                    incrementSelectorIndices(arrayOfInt);
                    setValueInternal(arrayOfInt[1], true);
                    if ((!this.mWrapSelectorWheel) && (arrayOfInt[1] >= this.mMaxValue))
                        this.mCurrentScrollOffset = this.mInitialScrollOffset;
                }
            }
        }
    }

    public void setDisplayedValues(String[] paramArrayOfString)
    {
        if (this.mDisplayedValues == paramArrayOfString)
            return;
        this.mDisplayedValues = paramArrayOfString;
        if (this.mDisplayedValues != null)
            this.mInputText.setRawInputType(524289);
        while (true)
        {
            updateInputTextView();
            initializeSelectorWheelIndices();
            tryComputeMaxWidth();
            break;
            this.mInputText.setRawInputType(2);
        }
    }

    public void setEnabled(boolean paramBoolean)
    {
        super.setEnabled(paramBoolean);
        if (!this.mHasSelectorWheel)
            this.mIncrementButton.setEnabled(paramBoolean);
        if (!this.mHasSelectorWheel)
            this.mDecrementButton.setEnabled(paramBoolean);
        this.mInputText.setEnabled(paramBoolean);
    }

    public void setFormatter(Formatter paramFormatter)
    {
        if (paramFormatter == this.mFormatter);
        while (true)
        {
            return;
            this.mFormatter = paramFormatter;
            initializeSelectorWheelIndices();
            updateInputTextView();
        }
    }

    public void setMaxValue(int paramInt)
    {
        if (this.mMaxValue == paramInt)
            return;
        if (paramInt < 0)
            throw new IllegalArgumentException("maxValue must be >= 0");
        this.mMaxValue = paramInt;
        if (this.mMaxValue < this.mValue)
            this.mValue = this.mMaxValue;
        if (this.mMaxValue - this.mMinValue > this.mSelectorIndices.length);
        for (boolean bool = true; ; bool = false)
        {
            setWrapSelectorWheel(bool);
            initializeSelectorWheelIndices();
            updateInputTextView();
            tryComputeMaxWidth();
            invalidate();
            break;
        }
    }

    public void setMinValue(int paramInt)
    {
        if (this.mMinValue == paramInt)
            return;
        if (paramInt < 0)
            throw new IllegalArgumentException("minValue must be >= 0");
        this.mMinValue = paramInt;
        if (this.mMinValue > this.mValue)
            this.mValue = this.mMinValue;
        if (this.mMaxValue - this.mMinValue > this.mSelectorIndices.length);
        for (boolean bool = true; ; bool = false)
        {
            setWrapSelectorWheel(bool);
            initializeSelectorWheelIndices();
            updateInputTextView();
            tryComputeMaxWidth();
            invalidate();
            break;
        }
    }

    public void setOnLongPressUpdateInterval(long paramLong)
    {
        this.mLongPressUpdateInterval = paramLong;
    }

    public void setOnScrollListener(OnScrollListener paramOnScrollListener)
    {
        this.mOnScrollListener = paramOnScrollListener;
    }

    public void setOnValueChangedListener(OnValueChangeListener paramOnValueChangeListener)
    {
        this.mOnValueChangeListener = paramOnValueChangeListener;
    }

    public void setValue(int paramInt)
    {
        setValueInternal(paramInt, false);
    }

    public void setWrapSelectorWheel(boolean paramBoolean)
    {
        if (this.mMaxValue - this.mMinValue >= this.mSelectorIndices.length);
        for (int i = 1; ; i = 0)
        {
            if (((!paramBoolean) || (i != 0)) && (paramBoolean != this.mWrapSelectorWheel))
                this.mWrapSelectorWheel = paramBoolean;
            return;
        }
    }

    class AccessibilityNodeProviderImpl extends AccessibilityNodeProvider
    {
        private static final int UNDEFINED = -2147483648;
        private static final int VIRTUAL_VIEW_ID_DECREMENT = 3;
        private static final int VIRTUAL_VIEW_ID_INCREMENT = 1;
        private static final int VIRTUAL_VIEW_ID_INPUT = 2;
        private int mAccessibilityFocusedView = -2147483648;
        private final int[] mTempArray = new int[2];
        private final Rect mTempRect = new Rect();

        AccessibilityNodeProviderImpl()
        {
        }

        private AccessibilityNodeInfo createAccessibilityNodeInfoForNumberPicker(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo = AccessibilityNodeInfo.obtain();
            localAccessibilityNodeInfo.setClassName(NumberPicker.class.getName());
            localAccessibilityNodeInfo.setPackageName(NumberPicker.access$5900(NumberPicker.this).getPackageName());
            localAccessibilityNodeInfo.setSource(NumberPicker.this);
            if (hasVirtualDecrementButton())
                localAccessibilityNodeInfo.addChild(NumberPicker.this, 3);
            localAccessibilityNodeInfo.addChild(NumberPicker.this, 2);
            if (hasVirtualIncrementButton())
                localAccessibilityNodeInfo.addChild(NumberPicker.this, 1);
            localAccessibilityNodeInfo.setParent((View)NumberPicker.this.getParentForAccessibility());
            localAccessibilityNodeInfo.setEnabled(NumberPicker.this.isEnabled());
            localAccessibilityNodeInfo.setScrollable(true);
            Rect localRect = this.mTempRect;
            localRect.set(paramInt1, paramInt2, paramInt3, paramInt4);
            localAccessibilityNodeInfo.setBoundsInParent(localRect);
            localAccessibilityNodeInfo.setVisibleToUser(NumberPicker.this.isVisibleToUser());
            int[] arrayOfInt = this.mTempArray;
            NumberPicker.this.getLocationOnScreen(arrayOfInt);
            localRect.offset(arrayOfInt[0], arrayOfInt[1]);
            localAccessibilityNodeInfo.setBoundsInScreen(localRect);
            if (this.mAccessibilityFocusedView != -1)
                localAccessibilityNodeInfo.addAction(64);
            if (this.mAccessibilityFocusedView == -1)
                localAccessibilityNodeInfo.addAction(128);
            if (NumberPicker.this.isEnabled())
            {
                if ((NumberPicker.this.getWrapSelectorWheel()) || (NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()))
                    localAccessibilityNodeInfo.addAction(4096);
                if ((NumberPicker.this.getWrapSelectorWheel()) || (NumberPicker.this.getValue() > NumberPicker.this.getMinValue()))
                    localAccessibilityNodeInfo.addAction(8192);
            }
            return localAccessibilityNodeInfo;
        }

        private AccessibilityNodeInfo createAccessibilityNodeInfoForVirtualButton(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo = AccessibilityNodeInfo.obtain();
            localAccessibilityNodeInfo.setClassName(Button.class.getName());
            localAccessibilityNodeInfo.setPackageName(NumberPicker.access$5700(NumberPicker.this).getPackageName());
            localAccessibilityNodeInfo.setSource(NumberPicker.this, paramInt1);
            localAccessibilityNodeInfo.setParent(NumberPicker.this);
            localAccessibilityNodeInfo.setText(paramString);
            localAccessibilityNodeInfo.setClickable(true);
            localAccessibilityNodeInfo.setLongClickable(true);
            localAccessibilityNodeInfo.setEnabled(NumberPicker.this.isEnabled());
            Rect localRect = this.mTempRect;
            localRect.set(paramInt2, paramInt3, paramInt4, paramInt5);
            localAccessibilityNodeInfo.setVisibleToUser(NumberPicker.this.isVisibleToUser(localRect));
            localAccessibilityNodeInfo.setBoundsInParent(localRect);
            int[] arrayOfInt = this.mTempArray;
            NumberPicker.this.getLocationOnScreen(arrayOfInt);
            localRect.offset(arrayOfInt[0], arrayOfInt[1]);
            localAccessibilityNodeInfo.setBoundsInScreen(localRect);
            if (this.mAccessibilityFocusedView != paramInt1)
                localAccessibilityNodeInfo.addAction(64);
            if (this.mAccessibilityFocusedView == paramInt1)
                localAccessibilityNodeInfo.addAction(128);
            if (NumberPicker.this.isEnabled())
                localAccessibilityNodeInfo.addAction(16);
            return localAccessibilityNodeInfo;
        }

        private AccessibilityNodeInfo createAccessibiltyNodeInfoForInputText()
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo = NumberPicker.this.mInputText.createAccessibilityNodeInfo();
            localAccessibilityNodeInfo.setSource(NumberPicker.this, 2);
            if (this.mAccessibilityFocusedView != 2)
                localAccessibilityNodeInfo.addAction(64);
            if (this.mAccessibilityFocusedView == 2)
                localAccessibilityNodeInfo.addAction(128);
            return localAccessibilityNodeInfo;
        }

        private void findAccessibilityNodeInfosByTextInChild(String paramString, int paramInt, List<AccessibilityNodeInfo> paramList)
        {
            switch (paramInt)
            {
            default:
            case 3:
            case 2:
            case 1:
            }
            while (true)
            {
                return;
                String str2 = getVirtualDecrementButtonText();
                if ((!TextUtils.isEmpty(str2)) && (str2.toString().toLowerCase().contains(paramString)))
                {
                    paramList.add(createAccessibilityNodeInfo(3));
                    continue;
                    Editable localEditable1 = NumberPicker.this.mInputText.getText();
                    if ((!TextUtils.isEmpty(localEditable1)) && (localEditable1.toString().toLowerCase().contains(paramString)))
                    {
                        paramList.add(createAccessibilityNodeInfo(2));
                    }
                    else
                    {
                        Editable localEditable2 = NumberPicker.this.mInputText.getText();
                        if ((!TextUtils.isEmpty(localEditable2)) && (localEditable2.toString().toLowerCase().contains(paramString)))
                        {
                            paramList.add(createAccessibilityNodeInfo(2));
                            continue;
                            String str1 = getVirtualIncrementButtonText();
                            if ((!TextUtils.isEmpty(str1)) && (str1.toString().toLowerCase().contains(paramString)))
                                paramList.add(createAccessibilityNodeInfo(1));
                        }
                    }
                }
            }
        }

        private String getVirtualDecrementButtonText()
        {
            int i = -1 + NumberPicker.this.mValue;
            if (NumberPicker.this.mWrapSelectorWheel)
                i = NumberPicker.this.getWrappedSelectorIndex(i);
            String str;
            if (i >= NumberPicker.this.mMinValue)
                if (NumberPicker.this.mDisplayedValues == null)
                    str = NumberPicker.this.formatNumber(i);
            while (true)
            {
                return str;
                str = NumberPicker.this.mDisplayedValues[(i - NumberPicker.this.mMinValue)];
                continue;
                str = null;
            }
        }

        private String getVirtualIncrementButtonText()
        {
            int i = 1 + NumberPicker.this.mValue;
            if (NumberPicker.this.mWrapSelectorWheel)
                i = NumberPicker.this.getWrappedSelectorIndex(i);
            String str;
            if (i <= NumberPicker.this.mMaxValue)
                if (NumberPicker.this.mDisplayedValues == null)
                    str = NumberPicker.this.formatNumber(i);
            while (true)
            {
                return str;
                str = NumberPicker.this.mDisplayedValues[(i - NumberPicker.this.mMinValue)];
                continue;
                str = null;
            }
        }

        private boolean hasVirtualDecrementButton()
        {
            if ((NumberPicker.this.getWrapSelectorWheel()) || (NumberPicker.this.getValue() > NumberPicker.this.getMinValue()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean hasVirtualIncrementButton()
        {
            if ((NumberPicker.this.getWrapSelectorWheel()) || (NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void sendAccessibilityEventForVirtualButton(int paramInt1, int paramInt2, String paramString)
        {
            AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt2);
            localAccessibilityEvent.setClassName(Button.class.getName());
            localAccessibilityEvent.setPackageName(NumberPicker.access$5600(NumberPicker.this).getPackageName());
            localAccessibilityEvent.getText().add(paramString);
            localAccessibilityEvent.setEnabled(NumberPicker.this.isEnabled());
            localAccessibilityEvent.setSource(NumberPicker.this, paramInt1);
            NumberPicker.this.requestSendAccessibilityEvent(NumberPicker.this, localAccessibilityEvent);
        }

        private void sendAccessibilityEventForVirtualText(int paramInt)
        {
            AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt);
            NumberPicker.this.mInputText.onInitializeAccessibilityEvent(localAccessibilityEvent);
            NumberPicker.this.mInputText.onPopulateAccessibilityEvent(localAccessibilityEvent);
            localAccessibilityEvent.setSource(NumberPicker.this, 2);
            NumberPicker.this.requestSendAccessibilityEvent(NumberPicker.this, localAccessibilityEvent);
        }

        public AccessibilityNodeInfo accessibilityFocusSearch(int paramInt1, int paramInt2)
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo = null;
            switch (paramInt1)
            {
            default:
            case 4098:
            case 4226:
            case 4097:
            case 4129:
            }
            while (true)
            {
                return localAccessibilityNodeInfo;
                switch (this.mAccessibilityFocusedView)
                {
                default:
                    break;
                case -2147483648:
                    localAccessibilityNodeInfo = createAccessibilityNodeInfo(-1);
                    break;
                case -1:
                    if (hasVirtualDecrementButton())
                        localAccessibilityNodeInfo = createAccessibilityNodeInfo(3);
                    break;
                case 3:
                    localAccessibilityNodeInfo = createAccessibilityNodeInfo(2);
                    break;
                case 2:
                    if (hasVirtualIncrementButton())
                        localAccessibilityNodeInfo = createAccessibilityNodeInfo(1);
                    break;
                case 1:
                    View localView2 = NumberPicker.this.focusSearch(paramInt1);
                    if (localView2 != null)
                    {
                        localAccessibilityNodeInfo = localView2.createAccessibilityNodeInfo();
                        continue;
                        switch (this.mAccessibilityFocusedView)
                        {
                        default:
                            break;
                        case -2147483648:
                            localAccessibilityNodeInfo = createAccessibilityNodeInfo(-1);
                            break;
                        case -1:
                            if (hasVirtualIncrementButton())
                                localAccessibilityNodeInfo = createAccessibilityNodeInfo(1);
                            break;
                        case 1:
                            localAccessibilityNodeInfo = createAccessibilityNodeInfo(2);
                            break;
                        case 2:
                            if (hasVirtualDecrementButton())
                                localAccessibilityNodeInfo = createAccessibilityNodeInfo(3);
                            break;
                        case 3:
                            View localView1 = NumberPicker.this.focusSearch(paramInt1);
                            if (localView1 != null)
                                localAccessibilityNodeInfo = localView1.createAccessibilityNodeInfo();
                            break;
                        }
                    }
                    break;
                }
            }
        }

        public AccessibilityNodeInfo createAccessibilityNodeInfo(int paramInt)
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo;
            switch (paramInt)
            {
            case 0:
            default:
                localAccessibilityNodeInfo = super.createAccessibilityNodeInfo(paramInt);
            case -1:
            case 3:
            case 2:
            case 1:
            }
            while (true)
            {
                return localAccessibilityNodeInfo;
                localAccessibilityNodeInfo = createAccessibilityNodeInfoForNumberPicker(NumberPicker.access$2900(NumberPicker.this), NumberPicker.access$3000(NumberPicker.this), NumberPicker.access$3100(NumberPicker.this) + (NumberPicker.access$3200(NumberPicker.this) - NumberPicker.access$3300(NumberPicker.this)), NumberPicker.access$3400(NumberPicker.this) + (NumberPicker.access$3500(NumberPicker.this) - NumberPicker.access$3600(NumberPicker.this)));
                continue;
                localAccessibilityNodeInfo = createAccessibilityNodeInfoForVirtualButton(3, getVirtualDecrementButtonText(), NumberPicker.access$3700(NumberPicker.this), NumberPicker.access$3800(NumberPicker.this), NumberPicker.access$3900(NumberPicker.this) + (NumberPicker.access$4000(NumberPicker.this) - NumberPicker.access$4100(NumberPicker.this)), NumberPicker.this.mTopSelectionDividerTop + NumberPicker.this.mSelectionDividerHeight);
                continue;
                localAccessibilityNodeInfo = createAccessibiltyNodeInfoForInputText();
                continue;
                localAccessibilityNodeInfo = createAccessibilityNodeInfoForVirtualButton(1, getVirtualIncrementButtonText(), NumberPicker.access$4300(NumberPicker.this), NumberPicker.this.mBottomSelectionDividerBottom - NumberPicker.this.mSelectionDividerHeight, NumberPicker.access$4400(NumberPicker.this) + (NumberPicker.access$4500(NumberPicker.this) - NumberPicker.access$4600(NumberPicker.this)), NumberPicker.access$4700(NumberPicker.this) + (NumberPicker.access$4800(NumberPicker.this) - NumberPicker.access$4900(NumberPicker.this)));
            }
        }

        public AccessibilityNodeInfo findAccessibilityFocus(int paramInt)
        {
            return createAccessibilityNodeInfo(this.mAccessibilityFocusedView);
        }

        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String paramString, int paramInt)
        {
            Object localObject;
            if (TextUtils.isEmpty(paramString))
                localObject = Collections.emptyList();
            while (true)
            {
                return localObject;
                String str = paramString.toLowerCase();
                localObject = new ArrayList();
                switch (paramInt)
                {
                case 0:
                default:
                    localObject = super.findAccessibilityNodeInfosByText(paramString, paramInt);
                    break;
                case -1:
                    findAccessibilityNodeInfosByTextInChild(str, 3, (List)localObject);
                    findAccessibilityNodeInfosByTextInChild(str, 2, (List)localObject);
                    findAccessibilityNodeInfosByTextInChild(str, 1, (List)localObject);
                    break;
                case 1:
                case 2:
                case 3:
                    findAccessibilityNodeInfosByTextInChild(str, paramInt, (List)localObject);
                }
            }
        }

        public boolean performAction(int paramInt1, int paramInt2, Bundle paramBundle)
        {
            boolean bool = false;
            switch (paramInt1)
            {
            case 0:
            default:
                bool = super.performAction(paramInt1, paramInt2, paramBundle);
            case -1:
            case 2:
            case 1:
            case 3:
            }
            while (true)
            {
                return bool;
                switch (paramInt2)
                {
                default:
                    break;
                case 64:
                    if (this.mAccessibilityFocusedView != paramInt1)
                    {
                        this.mAccessibilityFocusedView = paramInt1;
                        NumberPicker.this.requestAccessibilityFocus();
                        bool = true;
                    }
                    break;
                case 128:
                    if (this.mAccessibilityFocusedView == paramInt1)
                    {
                        this.mAccessibilityFocusedView = -2147483648;
                        NumberPicker.this.clearAccessibilityFocus();
                        bool = true;
                    }
                    break;
                case 4096:
                    if ((NumberPicker.this.isEnabled()) && ((NumberPicker.this.getWrapSelectorWheel()) || (NumberPicker.this.getValue() < NumberPicker.this.getMaxValue())))
                    {
                        NumberPicker.this.changeValueByOne(true);
                        bool = true;
                    }
                    break;
                case 8192:
                    if ((NumberPicker.this.isEnabled()) && ((NumberPicker.this.getWrapSelectorWheel()) || (NumberPicker.this.getValue() > NumberPicker.this.getMinValue())))
                    {
                        NumberPicker.this.changeValueByOne(false);
                        bool = true;
                        continue;
                        switch (paramInt2)
                        {
                        default:
                            bool = NumberPicker.this.mInputText.performAccessibilityAction(paramInt2, paramBundle);
                            break;
                        case 1:
                            if ((NumberPicker.this.isEnabled()) && (!NumberPicker.this.mInputText.isFocused()))
                                bool = NumberPicker.this.mInputText.requestFocus();
                            break;
                        case 2:
                            if ((NumberPicker.this.isEnabled()) && (NumberPicker.this.mInputText.isFocused()))
                            {
                                NumberPicker.this.mInputText.clearFocus();
                                bool = true;
                            }
                            break;
                        case 16:
                            if (NumberPicker.this.isEnabled())
                            {
                                NumberPicker.this.showSoftInput();
                                bool = true;
                            }
                            break;
                        case 64:
                            if (this.mAccessibilityFocusedView != paramInt1)
                            {
                                this.mAccessibilityFocusedView = paramInt1;
                                sendAccessibilityEventForVirtualView(paramInt1, 32768);
                                NumberPicker.this.mInputText.invalidate();
                                bool = true;
                            }
                            break;
                        case 128:
                            if (this.mAccessibilityFocusedView == paramInt1)
                            {
                                this.mAccessibilityFocusedView = -2147483648;
                                sendAccessibilityEventForVirtualView(paramInt1, 65536);
                                NumberPicker.this.mInputText.invalidate();
                                bool = true;
                                continue;
                                switch (paramInt2)
                                {
                                default:
                                    break;
                                case 16:
                                    if (NumberPicker.this.isEnabled())
                                    {
                                        NumberPicker.this.changeValueByOne(true);
                                        sendAccessibilityEventForVirtualView(paramInt1, 1);
                                        bool = true;
                                    }
                                    break;
                                case 64:
                                    if (this.mAccessibilityFocusedView != paramInt1)
                                    {
                                        this.mAccessibilityFocusedView = paramInt1;
                                        sendAccessibilityEventForVirtualView(paramInt1, 32768);
                                        NumberPicker.this.invalidate(0, NumberPicker.this.mBottomSelectionDividerBottom, NumberPicker.access$5000(NumberPicker.this), NumberPicker.access$5100(NumberPicker.this));
                                        bool = true;
                                    }
                                    break;
                                case 128:
                                    if (this.mAccessibilityFocusedView == paramInt1)
                                    {
                                        this.mAccessibilityFocusedView = -2147483648;
                                        sendAccessibilityEventForVirtualView(paramInt1, 65536);
                                        NumberPicker.this.invalidate(0, NumberPicker.this.mBottomSelectionDividerBottom, NumberPicker.access$5200(NumberPicker.this), NumberPicker.access$5300(NumberPicker.this));
                                        bool = true;
                                        continue;
                                        switch (paramInt2)
                                        {
                                        default:
                                            break;
                                        case 16:
                                            if (NumberPicker.this.isEnabled())
                                            {
                                                if (paramInt1 == 1)
                                                    bool = true;
                                                NumberPicker.this.changeValueByOne(bool);
                                                sendAccessibilityEventForVirtualView(paramInt1, 1);
                                                bool = true;
                                            }
                                            break;
                                        case 64:
                                            if (this.mAccessibilityFocusedView != paramInt1)
                                            {
                                                this.mAccessibilityFocusedView = paramInt1;
                                                sendAccessibilityEventForVirtualView(paramInt1, 32768);
                                                NumberPicker.this.invalidate(0, 0, NumberPicker.access$5400(NumberPicker.this), NumberPicker.this.mTopSelectionDividerTop);
                                                bool = true;
                                            }
                                            break;
                                        case 128:
                                            if (this.mAccessibilityFocusedView == paramInt1)
                                            {
                                                this.mAccessibilityFocusedView = -2147483648;
                                                sendAccessibilityEventForVirtualView(paramInt1, 65536);
                                                NumberPicker.this.invalidate(0, 0, NumberPicker.access$5500(NumberPicker.this), NumberPicker.this.mTopSelectionDividerTop);
                                                bool = true;
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }

        public void sendAccessibilityEventForVirtualView(int paramInt1, int paramInt2)
        {
            switch (paramInt1)
            {
            default:
            case 3:
            case 2:
            case 1:
            }
            while (true)
            {
                return;
                if (hasVirtualDecrementButton())
                {
                    sendAccessibilityEventForVirtualButton(paramInt1, paramInt2, getVirtualDecrementButtonText());
                    continue;
                    sendAccessibilityEventForVirtualText(paramInt2);
                    continue;
                    if (hasVirtualIncrementButton())
                        sendAccessibilityEventForVirtualButton(paramInt1, paramInt2, getVirtualIncrementButtonText());
                }
            }
        }
    }

    class BeginSoftInputOnLongPressCommand
        implements Runnable
    {
        BeginSoftInputOnLongPressCommand()
        {
        }

        public void run()
        {
            NumberPicker.this.showSoftInput();
            NumberPicker.access$2802(NumberPicker.this, true);
        }
    }

    public static class CustomEditText extends EditText
    {
        public CustomEditText(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        public void onEditorAction(int paramInt)
        {
            super.onEditorAction(paramInt);
            if (paramInt == 6)
                clearFocus();
        }
    }

    class ChangeCurrentByOneFromLongPressCommand
        implements Runnable
    {
        private boolean mIncrement;

        ChangeCurrentByOneFromLongPressCommand()
        {
        }

        private void setStep(boolean paramBoolean)
        {
            this.mIncrement = paramBoolean;
        }

        public void run()
        {
            NumberPicker.this.changeValueByOne(this.mIncrement);
            NumberPicker.this.postDelayed(this, NumberPicker.this.mLongPressUpdateInterval);
        }
    }

    class SetSelectionCommand
        implements Runnable
    {
        private int mSelectionEnd;
        private int mSelectionStart;

        SetSelectionCommand()
        {
        }

        public void run()
        {
            NumberPicker.this.mInputText.setSelection(this.mSelectionStart, this.mSelectionEnd);
        }
    }

    class PressedStateHelper
        implements Runnable
    {
        public static final int BUTTON_DECREMENT = 2;
        public static final int BUTTON_INCREMENT = 1;
        private final int MODE_PRESS = 1;
        private final int MODE_TAPPED = 2;
        private int mManagedButton;
        private int mMode;

        PressedStateHelper()
        {
        }

        public void buttonPressDelayed(int paramInt)
        {
            cancel();
            this.mMode = 1;
            this.mManagedButton = paramInt;
            NumberPicker.this.postDelayed(this, ViewConfiguration.getTapTimeout());
        }

        public void buttonTapped(int paramInt)
        {
            cancel();
            this.mMode = 2;
            this.mManagedButton = paramInt;
            NumberPicker.this.post(this);
        }

        public void cancel()
        {
            this.mMode = 0;
            this.mManagedButton = 0;
            NumberPicker.this.removeCallbacks(this);
            if (NumberPicker.this.mIncrementVirtualButtonPressed)
            {
                NumberPicker.access$1302(NumberPicker.this, false);
                NumberPicker.this.invalidate(0, NumberPicker.this.mBottomSelectionDividerBottom, NumberPicker.access$1500(NumberPicker.this), NumberPicker.access$1600(NumberPicker.this));
            }
            NumberPicker.access$1702(NumberPicker.this, false);
            if (NumberPicker.this.mDecrementVirtualButtonPressed)
                NumberPicker.this.invalidate(0, 0, NumberPicker.access$1800(NumberPicker.this), NumberPicker.this.mTopSelectionDividerTop);
        }

        public void run()
        {
            switch (this.mMode)
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                switch (this.mManagedButton)
                {
                default:
                    break;
                case 1:
                    NumberPicker.access$1302(NumberPicker.this, true);
                    NumberPicker.this.invalidate(0, NumberPicker.this.mBottomSelectionDividerBottom, NumberPicker.access$2000(NumberPicker.this), NumberPicker.access$2100(NumberPicker.this));
                    break;
                case 2:
                    NumberPicker.access$1702(NumberPicker.this, true);
                    NumberPicker.this.invalidate(0, 0, NumberPicker.access$2200(NumberPicker.this), NumberPicker.this.mTopSelectionDividerTop);
                    continue;
                    switch (this.mManagedButton)
                    {
                    default:
                        break;
                    case 1:
                        if (!NumberPicker.this.mIncrementVirtualButtonPressed)
                            NumberPicker.this.postDelayed(this, ViewConfiguration.getPressedStateDuration());
                        NumberPicker.access$1380(NumberPicker.this, 1);
                        NumberPicker.this.invalidate(0, NumberPicker.this.mBottomSelectionDividerBottom, NumberPicker.access$2300(NumberPicker.this), NumberPicker.access$2400(NumberPicker.this));
                        break;
                    case 2:
                        if (!NumberPicker.this.mDecrementVirtualButtonPressed)
                            NumberPicker.this.postDelayed(this, ViewConfiguration.getPressedStateDuration());
                        NumberPicker.access$1780(NumberPicker.this, 1);
                        NumberPicker.this.invalidate(0, 0, NumberPicker.access$2500(NumberPicker.this), NumberPicker.this.mTopSelectionDividerTop);
                    }
                    break;
                }
            }
        }
    }

    class InputTextFilter extends NumberKeyListener
    {
        InputTextFilter()
        {
        }

        public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
        {
            CharSequence localCharSequence;
            Object localObject;
            if (NumberPicker.this.mDisplayedValues == null)
            {
                localCharSequence = super.filter(paramCharSequence, paramInt1, paramInt2, paramSpanned, paramInt3, paramInt4);
                if (localCharSequence == null)
                    localCharSequence = paramCharSequence.subSequence(paramInt1, paramInt2);
                localObject = String.valueOf(paramSpanned.subSequence(0, paramInt3)) + localCharSequence + paramSpanned.subSequence(paramInt4, paramSpanned.length());
                if (!"".equals(localObject));
            }
            while (true)
            {
                return localObject;
                if (NumberPicker.this.getSelectedPos((String)localObject) > NumberPicker.this.mMaxValue)
                {
                    localObject = "";
                }
                else
                {
                    localObject = localCharSequence;
                    continue;
                    String str1 = String.valueOf(paramCharSequence.subSequence(paramInt1, paramInt2));
                    if (TextUtils.isEmpty(str1))
                    {
                        localObject = "";
                    }
                    else
                    {
                        String str2 = String.valueOf(paramSpanned.subSequence(0, paramInt3)) + str1 + paramSpanned.subSequence(paramInt4, paramSpanned.length());
                        String str3 = String.valueOf(str2).toLowerCase();
                        String[] arrayOfString = NumberPicker.this.mDisplayedValues;
                        int i = arrayOfString.length;
                        for (int j = 0; ; j++)
                        {
                            if (j >= i)
                                break label312;
                            String str4 = arrayOfString[j];
                            if (str4.toLowerCase().startsWith(str3))
                            {
                                NumberPicker.this.postSetSelectionCommand(str2.length(), str4.length());
                                localObject = str4.subSequence(paramInt3, str4.length());
                                break;
                            }
                        }
                        label312: localObject = "";
                    }
                }
            }
        }

        protected char[] getAcceptedChars()
        {
            return NumberPicker.DIGIT_CHARACTERS;
        }

        public int getInputType()
        {
            return 1;
        }
    }

    public static abstract interface Formatter
    {
        public abstract String format(int paramInt);
    }

    public static abstract interface OnScrollListener
    {
        public static final int SCROLL_STATE_FLING = 2;
        public static final int SCROLL_STATE_IDLE = 0;
        public static final int SCROLL_STATE_TOUCH_SCROLL = 1;

        public abstract void onScrollStateChange(NumberPicker paramNumberPicker, int paramInt);
    }

    public static abstract interface OnValueChangeListener
    {
        public abstract void onValueChange(NumberPicker paramNumberPicker, int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.NumberPicker
 * JD-Core Version:        0.6.2
 */