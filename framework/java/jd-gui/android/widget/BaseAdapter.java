package android.widget;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseAdapter
    implements ListAdapter, SpinnerAdapter
{
    private final DataSetObservable mDataSetObservable = new DataSetObservable();

    public boolean areAllItemsEnabled()
    {
        return true;
    }

    public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
        return getView(paramInt, paramView, paramViewGroup);
    }

    public int getItemViewType(int paramInt)
    {
        return 0;
    }

    public int getViewTypeCount()
    {
        return 1;
    }

    public boolean hasStableIds()
    {
        return false;
    }

    public boolean isEmpty()
    {
        if (getCount() == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEnabled(int paramInt)
    {
        return true;
    }

    public void notifyDataSetChanged()
    {
        this.mDataSetObservable.notifyChanged();
    }

    public void notifyDataSetInvalidated()
    {
        this.mDataSetObservable.notifyInvalidated();
    }

    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        this.mDataSetObservable.registerObserver(paramDataSetObserver);
    }

    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        this.mDataSetObservable.unregisterObserver(paramDataSetObserver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.BaseAdapter
 * JD-Core Version:        0.6.2
 */