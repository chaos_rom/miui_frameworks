package android.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

public abstract class CursorTreeAdapter extends BaseExpandableListAdapter
    implements Filterable, CursorFilter.CursorFilterClient
{
    private boolean mAutoRequery;
    SparseArray<MyCursorHelper> mChildrenCursorHelpers;
    private Context mContext;
    CursorFilter mCursorFilter;
    FilterQueryProvider mFilterQueryProvider;
    MyCursorHelper mGroupCursorHelper;
    private Handler mHandler;

    public CursorTreeAdapter(Cursor paramCursor, Context paramContext)
    {
        init(paramCursor, paramContext, true);
    }

    public CursorTreeAdapter(Cursor paramCursor, Context paramContext, boolean paramBoolean)
    {
        init(paramCursor, paramContext, paramBoolean);
    }

    private void init(Cursor paramCursor, Context paramContext, boolean paramBoolean)
    {
        this.mContext = paramContext;
        this.mHandler = new Handler();
        this.mAutoRequery = paramBoolean;
        this.mGroupCursorHelper = new MyCursorHelper(paramCursor);
        this.mChildrenCursorHelpers = new SparseArray();
    }

    /** @deprecated */
    private void releaseCursorHelpers()
    {
        try
        {
            for (int i = -1 + this.mChildrenCursorHelpers.size(); i >= 0; i--)
                ((MyCursorHelper)this.mChildrenCursorHelpers.valueAt(i)).deactivate();
            this.mChildrenCursorHelpers.clear();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected abstract void bindChildView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean);

    protected abstract void bindGroupView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean);

    public void changeCursor(Cursor paramCursor)
    {
        this.mGroupCursorHelper.changeCursor(paramCursor, true);
    }

    public String convertToString(Cursor paramCursor)
    {
        if (paramCursor == null);
        for (String str = ""; ; str = paramCursor.toString())
            return str;
    }

    /** @deprecated */
    void deactivateChildrenCursorHelper(int paramInt)
    {
        try
        {
            MyCursorHelper localMyCursorHelper = getChildrenCursorHelper(paramInt, true);
            this.mChildrenCursorHelpers.remove(paramInt);
            localMyCursorHelper.deactivate();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public Cursor getChild(int paramInt1, int paramInt2)
    {
        return getChildrenCursorHelper(paramInt1, true).moveTo(paramInt2);
    }

    public long getChildId(int paramInt1, int paramInt2)
    {
        return getChildrenCursorHelper(paramInt1, true).getId(paramInt2);
    }

    public View getChildView(int paramInt1, int paramInt2, boolean paramBoolean, View paramView, ViewGroup paramViewGroup)
    {
        Cursor localCursor = getChildrenCursorHelper(paramInt1, true).moveTo(paramInt2);
        if (localCursor == null)
            throw new IllegalStateException("this should only be called when the cursor is valid");
        if (paramView == null);
        for (View localView = newChildView(this.mContext, localCursor, paramBoolean, paramViewGroup); ; localView = paramView)
        {
            bindChildView(localView, this.mContext, localCursor, paramBoolean);
            return localView;
        }
    }

    public int getChildrenCount(int paramInt)
    {
        MyCursorHelper localMyCursorHelper = getChildrenCursorHelper(paramInt, true);
        if ((this.mGroupCursorHelper.isValid()) && (localMyCursorHelper != null));
        for (int i = localMyCursorHelper.getCount(); ; i = 0)
            return i;
    }

    protected abstract Cursor getChildrenCursor(Cursor paramCursor);

    /** @deprecated */
    MyCursorHelper getChildrenCursorHelper(int paramInt, boolean paramBoolean)
    {
        try
        {
            MyCursorHelper localMyCursorHelper1 = (MyCursorHelper)this.mChildrenCursorHelpers.get(paramInt);
            if (localMyCursorHelper1 == null)
            {
                Cursor localCursor = this.mGroupCursorHelper.moveTo(paramInt);
                if (localCursor != null);
            }
            for (MyCursorHelper localMyCursorHelper2 = null; ; localMyCursorHelper2 = localMyCursorHelper1)
            {
                return localMyCursorHelper2;
                localMyCursorHelper1 = new MyCursorHelper(getChildrenCursor(this.mGroupCursorHelper.getCursor()));
                this.mChildrenCursorHelpers.put(paramInt, localMyCursorHelper1);
            }
        }
        finally
        {
        }
    }

    public Cursor getCursor()
    {
        return this.mGroupCursorHelper.getCursor();
    }

    public Filter getFilter()
    {
        if (this.mCursorFilter == null)
            this.mCursorFilter = new CursorFilter(this);
        return this.mCursorFilter;
    }

    public FilterQueryProvider getFilterQueryProvider()
    {
        return this.mFilterQueryProvider;
    }

    public Cursor getGroup(int paramInt)
    {
        return this.mGroupCursorHelper.moveTo(paramInt);
    }

    public int getGroupCount()
    {
        return this.mGroupCursorHelper.getCount();
    }

    public long getGroupId(int paramInt)
    {
        return this.mGroupCursorHelper.getId(paramInt);
    }

    public View getGroupView(int paramInt, boolean paramBoolean, View paramView, ViewGroup paramViewGroup)
    {
        Cursor localCursor = this.mGroupCursorHelper.moveTo(paramInt);
        if (localCursor == null)
            throw new IllegalStateException("this should only be called when the cursor is valid");
        if (paramView == null);
        for (View localView = newGroupView(this.mContext, localCursor, paramBoolean, paramViewGroup); ; localView = paramView)
        {
            bindGroupView(localView, this.mContext, localCursor, paramBoolean);
            return localView;
        }
    }

    public boolean hasStableIds()
    {
        return true;
    }

    public boolean isChildSelectable(int paramInt1, int paramInt2)
    {
        return true;
    }

    protected abstract View newChildView(Context paramContext, Cursor paramCursor, boolean paramBoolean, ViewGroup paramViewGroup);

    protected abstract View newGroupView(Context paramContext, Cursor paramCursor, boolean paramBoolean, ViewGroup paramViewGroup);

    public void notifyDataSetChanged()
    {
        notifyDataSetChanged(true);
    }

    public void notifyDataSetChanged(boolean paramBoolean)
    {
        if (paramBoolean)
            releaseCursorHelpers();
        super.notifyDataSetChanged();
    }

    public void notifyDataSetInvalidated()
    {
        releaseCursorHelpers();
        super.notifyDataSetInvalidated();
    }

    public void onGroupCollapsed(int paramInt)
    {
        deactivateChildrenCursorHelper(paramInt);
    }

    public Cursor runQueryOnBackgroundThread(CharSequence paramCharSequence)
    {
        if (this.mFilterQueryProvider != null);
        for (Cursor localCursor = this.mFilterQueryProvider.runQuery(paramCharSequence); ; localCursor = this.mGroupCursorHelper.getCursor())
            return localCursor;
    }

    public void setChildrenCursor(int paramInt, Cursor paramCursor)
    {
        getChildrenCursorHelper(paramInt, false).changeCursor(paramCursor, false);
    }

    public void setFilterQueryProvider(FilterQueryProvider paramFilterQueryProvider)
    {
        this.mFilterQueryProvider = paramFilterQueryProvider;
    }

    public void setGroupCursor(Cursor paramCursor)
    {
        this.mGroupCursorHelper.changeCursor(paramCursor, false);
    }

    class MyCursorHelper
    {
        private MyContentObserver mContentObserver;
        private Cursor mCursor;
        private MyDataSetObserver mDataSetObserver;
        private boolean mDataValid;
        private int mRowIDColumn;

        MyCursorHelper(Cursor arg2)
        {
            Object localObject;
            boolean bool;
            if (localObject != null)
            {
                bool = true;
                this.mCursor = localObject;
                this.mDataValid = bool;
                if (!bool)
                    break label100;
            }
            label100: for (int i = localObject.getColumnIndex("_id"); ; i = -1)
            {
                this.mRowIDColumn = i;
                this.mContentObserver = new MyContentObserver();
                this.mDataSetObserver = new MyDataSetObserver(null);
                if (bool)
                {
                    localObject.registerContentObserver(this.mContentObserver);
                    localObject.registerDataSetObserver(this.mDataSetObserver);
                }
                return;
                bool = false;
                break;
            }
        }

        void changeCursor(Cursor paramCursor, boolean paramBoolean)
        {
            if (paramCursor == this.mCursor);
            while (true)
            {
                return;
                deactivate();
                this.mCursor = paramCursor;
                if (paramCursor != null)
                {
                    paramCursor.registerContentObserver(this.mContentObserver);
                    paramCursor.registerDataSetObserver(this.mDataSetObserver);
                    this.mRowIDColumn = paramCursor.getColumnIndex("_id");
                    this.mDataValid = true;
                    CursorTreeAdapter.this.notifyDataSetChanged(paramBoolean);
                }
                else
                {
                    this.mRowIDColumn = -1;
                    this.mDataValid = false;
                    CursorTreeAdapter.this.notifyDataSetInvalidated();
                }
            }
        }

        void deactivate()
        {
            if (this.mCursor == null);
            while (true)
            {
                return;
                this.mCursor.unregisterContentObserver(this.mContentObserver);
                this.mCursor.unregisterDataSetObserver(this.mDataSetObserver);
                this.mCursor.close();
                this.mCursor = null;
            }
        }

        int getCount()
        {
            if ((this.mDataValid) && (this.mCursor != null));
            for (int i = this.mCursor.getCount(); ; i = 0)
                return i;
        }

        Cursor getCursor()
        {
            return this.mCursor;
        }

        long getId(int paramInt)
        {
            long l = 0L;
            if ((this.mDataValid) && (this.mCursor != null) && (this.mCursor.moveToPosition(paramInt)))
                l = this.mCursor.getLong(this.mRowIDColumn);
            return l;
        }

        boolean isValid()
        {
            if ((this.mDataValid) && (this.mCursor != null));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        Cursor moveTo(int paramInt)
        {
            if ((this.mDataValid) && (this.mCursor != null) && (this.mCursor.moveToPosition(paramInt)));
            for (Cursor localCursor = this.mCursor; ; localCursor = null)
                return localCursor;
        }

        private class MyDataSetObserver extends DataSetObserver
        {
            private MyDataSetObserver()
            {
            }

            public void onChanged()
            {
                CursorTreeAdapter.MyCursorHelper.access$402(CursorTreeAdapter.MyCursorHelper.this, true);
                CursorTreeAdapter.this.notifyDataSetChanged();
            }

            public void onInvalidated()
            {
                CursorTreeAdapter.MyCursorHelper.access$402(CursorTreeAdapter.MyCursorHelper.this, false);
                CursorTreeAdapter.this.notifyDataSetInvalidated();
            }
        }

        private class MyContentObserver extends ContentObserver
        {
            public MyContentObserver()
            {
                super();
            }

            public boolean deliverSelfNotifications()
            {
                return true;
            }

            public void onChange(boolean paramBoolean)
            {
                if ((CursorTreeAdapter.this.mAutoRequery) && (CursorTreeAdapter.MyCursorHelper.this.mCursor != null))
                    CursorTreeAdapter.MyCursorHelper.access$402(CursorTreeAdapter.MyCursorHelper.this, CursorTreeAdapter.MyCursorHelper.this.mCursor.requery());
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.CursorTreeAdapter
 * JD-Core Version:        0.6.2
 */