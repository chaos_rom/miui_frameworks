package android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.WordIterator;
import android.text.style.SpellCheckSpan;
import android.text.style.SuggestionSpan;
import android.util.Log;
import android.util.LruCache;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SpellCheckerSession.SpellCheckerSessionListener;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;
import com.android.internal.util.ArrayUtils;
import java.util.Locale;

public class SpellChecker
    implements SpellCheckerSession.SpellCheckerSessionListener
{
    public static final int AVERAGE_WORD_LENGTH = 7;
    private static final boolean DBG = false;
    public static final int MAX_NUMBER_OF_WORDS = 50;
    private static final int MIN_SENTENCE_LENGTH = 50;
    private static final int SPELL_PAUSE_DURATION = 400;
    private static final int SUGGESTION_SPAN_CACHE_SIZE = 10;
    private static final String TAG = SpellChecker.class.getSimpleName();
    private static final int USE_SPAN_RANGE = -1;
    public static final int WORD_ITERATOR_INTERVAL = 350;
    final int mCookie;
    private Locale mCurrentLocale;
    private int[] mIds;
    private boolean mIsSentenceSpellCheckSupported;
    private int mLength;
    private int mSpanSequenceCounter = 0;
    private SpellCheckSpan[] mSpellCheckSpans;
    SpellCheckerSession mSpellCheckerSession;
    private SpellParser[] mSpellParsers = new SpellParser[0];
    private Runnable mSpellRunnable;
    private final LruCache<Long, SuggestionSpan> mSuggestionSpanCache = new LruCache(10);
    private TextServicesManager mTextServicesManager;
    private final TextView mTextView;
    private WordIterator mWordIterator;

    public SpellChecker(TextView paramTextView)
    {
        this.mTextView = paramTextView;
        int i = ArrayUtils.idealObjectArraySize(1);
        this.mIds = new int[i];
        this.mSpellCheckSpans = new SpellCheckSpan[i];
        setLocale(this.mTextView.getTextServicesLocale());
        this.mCookie = hashCode();
    }

    private void addSpellCheckSpan(Editable paramEditable, int paramInt1, int paramInt2)
    {
        int i = nextSpellCheckSpanIndex();
        SpellCheckSpan localSpellCheckSpan = this.mSpellCheckSpans[i];
        paramEditable.setSpan(localSpellCheckSpan, paramInt1, paramInt2, 33);
        localSpellCheckSpan.setSpellCheckInProgress(false);
        int[] arrayOfInt = this.mIds;
        int j = this.mSpanSequenceCounter;
        this.mSpanSequenceCounter = (j + 1);
        arrayOfInt[i] = j;
    }

    private void createMisspelledSuggestionSpan(Editable paramEditable, SuggestionsInfo paramSuggestionsInfo, SpellCheckSpan paramSpellCheckSpan, int paramInt1, int paramInt2)
    {
        int i = paramEditable.getSpanStart(paramSpellCheckSpan);
        int j = paramEditable.getSpanEnd(paramSpellCheckSpan);
        if ((i < 0) || (j <= i));
        while (true)
        {
            return;
            int k;
            if ((paramInt1 != -1) && (paramInt2 != -1))
                k = i + paramInt1;
            for (int m = k + paramInt2; ; m = j)
            {
                int n = paramSuggestionsInfo.getSuggestionsCount();
                if (n <= 0)
                    break;
                arrayOfString = new String[n];
                for (int i1 = 0; i1 < n; i1++)
                    arrayOfString[i1] = paramSuggestionsInfo.getSuggestionAt(i1);
                k = i;
            }
            String[] arrayOfString = (String[])ArrayUtils.emptyArray(String.class);
            SuggestionSpan localSuggestionSpan1 = new SuggestionSpan(this.mTextView.getContext(), arrayOfString, 3);
            if (this.mIsSentenceSpellCheckSupported)
            {
                Long localLong = Long.valueOf(TextUtils.packRangeInLong(k, m));
                SuggestionSpan localSuggestionSpan2 = (SuggestionSpan)this.mSuggestionSpanCache.get(localLong);
                if (localSuggestionSpan2 != null)
                    paramEditable.removeSpan(localSuggestionSpan2);
                this.mSuggestionSpanCache.put(localLong, localSuggestionSpan1);
            }
            paramEditable.setSpan(localSuggestionSpan1, k, m, 33);
            this.mTextView.invalidateRegion(k, m, false);
        }
    }

    private boolean isSessionActive()
    {
        if (this.mSpellCheckerSession != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private int nextSpellCheckSpanIndex()
    {
        int i = 0;
        if (i < this.mLength)
            if (this.mIds[i] >= 0);
        while (true)
        {
            return i;
            i++;
            break;
            if (this.mLength == this.mSpellCheckSpans.length)
            {
                int j = 2 * this.mLength;
                int[] arrayOfInt = new int[j];
                SpellCheckSpan[] arrayOfSpellCheckSpan = new SpellCheckSpan[j];
                System.arraycopy(this.mIds, 0, arrayOfInt, 0, this.mLength);
                System.arraycopy(this.mSpellCheckSpans, 0, arrayOfSpellCheckSpan, 0, this.mLength);
                this.mIds = arrayOfInt;
                this.mSpellCheckSpans = arrayOfSpellCheckSpan;
            }
            this.mSpellCheckSpans[this.mLength] = new SpellCheckSpan();
            this.mLength = (1 + this.mLength);
            i = -1 + this.mLength;
        }
    }

    private SpellCheckSpan onGetSuggestionsInternal(SuggestionsInfo paramSuggestionsInfo, int paramInt1, int paramInt2)
    {
        SpellCheckSpan localSpellCheckSpan;
        if ((paramSuggestionsInfo == null) || (paramSuggestionsInfo.getCookie() != this.mCookie))
            localSpellCheckSpan = null;
        while (true)
        {
            return localSpellCheckSpan;
            Editable localEditable = (Editable)this.mTextView.getText();
            int i = paramSuggestionsInfo.getSequence();
            for (int j = 0; ; j++)
            {
                if (j >= this.mLength)
                    break label276;
                if (i == this.mIds[j])
                {
                    int k = paramSuggestionsInfo.getSuggestionsAttributes();
                    int m;
                    if ((k & 0x1) > 0)
                    {
                        m = 1;
                        label79: if ((k & 0x2) <= 0)
                            break label128;
                    }
                    label128: for (int n = 1; ; n = 0)
                    {
                        localSpellCheckSpan = this.mSpellCheckSpans[j];
                        if ((m != 0) || (n == 0))
                            break label134;
                        createMisspelledSuggestionSpan(localEditable, paramSuggestionsInfo, localSpellCheckSpan, paramInt1, paramInt2);
                        break;
                        m = 0;
                        break label79;
                    }
                    label134: if (!this.mIsSentenceSpellCheckSupported)
                        break;
                    int i1 = localEditable.getSpanStart(localSpellCheckSpan);
                    int i2 = localEditable.getSpanEnd(localSpellCheckSpan);
                    int i3;
                    if ((paramInt1 != -1) && (paramInt2 != -1))
                        i3 = i1 + paramInt1;
                    for (int i4 = i3 + paramInt2; ; i4 = i2)
                    {
                        if ((i1 < 0) || (i2 <= i1) || (i4 <= i3))
                            break label268;
                        Long localLong = Long.valueOf(TextUtils.packRangeInLong(i3, i4));
                        SuggestionSpan localSuggestionSpan = (SuggestionSpan)this.mSuggestionSpanCache.get(localLong);
                        if (localSuggestionSpan == null)
                            break;
                        localEditable.removeSpan(localSuggestionSpan);
                        this.mSuggestionSpanCache.remove(localLong);
                        break;
                        i3 = i1;
                    }
                    label268: break;
                }
            }
            label276: localSpellCheckSpan = null;
        }
    }

    private void resetSession()
    {
        closeSession();
        this.mTextServicesManager = ((TextServicesManager)this.mTextView.getContext().getSystemService("textservices"));
        if ((!this.mTextServicesManager.isSpellCheckerEnabled()) || (this.mTextServicesManager.getCurrentSpellCheckerSubtype(true) == null))
            this.mSpellCheckerSession = null;
        while (true)
        {
            for (int i = 0; i < this.mLength; i++)
                this.mIds[i] = -1;
            this.mSpellCheckerSession = this.mTextServicesManager.newSpellCheckerSession(null, this.mCurrentLocale, this, false);
            this.mIsSentenceSpellCheckSupported = true;
        }
        this.mLength = 0;
        this.mTextView.removeMisspelledSpans((Editable)this.mTextView.getText());
        this.mSuggestionSpanCache.evictAll();
    }

    private void scheduleNewSpellCheck()
    {
        if (this.mSpellRunnable == null)
            this.mSpellRunnable = new Runnable()
            {
                public void run()
                {
                    int i = SpellChecker.this.mSpellParsers.length;
                    for (int j = 0; ; j++)
                        if (j < i)
                        {
                            SpellChecker.SpellParser localSpellParser = SpellChecker.this.mSpellParsers[j];
                            if (!localSpellParser.isFinished())
                                localSpellParser.parse();
                        }
                        else
                        {
                            return;
                        }
                }
            };
        while (true)
        {
            this.mTextView.postDelayed(this.mSpellRunnable, 400L);
            return;
            this.mTextView.removeCallbacks(this.mSpellRunnable);
        }
    }

    private void setLocale(Locale paramLocale)
    {
        this.mCurrentLocale = paramLocale;
        resetSession();
        this.mWordIterator = new WordIterator(paramLocale);
        this.mTextView.onLocaleChanged();
    }

    private void spellCheck()
    {
        if (this.mSpellCheckerSession == null);
        while (true)
        {
            return;
            Editable localEditable = (Editable)this.mTextView.getText();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            Object localObject = new TextInfo[this.mLength];
            int k = 0;
            int m = 0;
            while (m < this.mLength)
            {
                SpellCheckSpan localSpellCheckSpan = this.mSpellCheckSpans[m];
                if ((this.mIds[m] < 0) || (localSpellCheckSpan.isSpellCheckInProgress()))
                {
                    m++;
                }
                else
                {
                    int n = localEditable.getSpanStart(localSpellCheckSpan);
                    int i1 = localEditable.getSpanEnd(localSpellCheckSpan);
                    int i2;
                    if (this.mIsSentenceSpellCheckSupported)
                        if ((j <= n) || (i > i1))
                        {
                            i2 = 1;
                            label128: if ((n < 0) || (i1 <= n) || (i2 == 0))
                                break label231;
                            if (!(localEditable instanceof SpannableStringBuilder))
                                break label239;
                        }
                    label231: label239: for (String str = ((SpannableStringBuilder)localEditable).substring(n, i1); ; str = localEditable.subSequence(n, i1).toString())
                    {
                        localSpellCheckSpan.setSpellCheckInProgress(true);
                        int i3 = k + 1;
                        localObject[k] = new TextInfo(str, this.mCookie, this.mIds[m]);
                        k = i3;
                        break;
                        i2 = 0;
                        break label128;
                        if ((j < n) || (i > i1));
                        for (i2 = 1; ; i2 = 0)
                        {
                            break label128;
                            break;
                        }
                    }
                }
            }
            if (k > 0)
            {
                if (k < localObject.length)
                {
                    TextInfo[] arrayOfTextInfo = new TextInfo[k];
                    System.arraycopy(localObject, 0, arrayOfTextInfo, 0, k);
                    localObject = arrayOfTextInfo;
                }
                if (this.mIsSentenceSpellCheckSupported)
                    this.mSpellCheckerSession.getSentenceSuggestions((TextInfo[])localObject, 5);
                else
                    this.mSpellCheckerSession.getSuggestions((TextInfo[])localObject, 5, false);
            }
        }
    }

    public void closeSession()
    {
        if (this.mSpellCheckerSession != null)
            this.mSpellCheckerSession.close();
        int i = this.mSpellParsers.length;
        for (int j = 0; j < i; j++)
            this.mSpellParsers[j].stop();
        if (this.mSpellRunnable != null)
            this.mTextView.removeCallbacks(this.mSpellRunnable);
    }

    public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo)
    {
        Editable localEditable = (Editable)this.mTextView.getText();
        int i = 0;
        if (i < paramArrayOfSentenceSuggestionsInfo.length)
        {
            SentenceSuggestionsInfo localSentenceSuggestionsInfo = paramArrayOfSentenceSuggestionsInfo[i];
            if (localSentenceSuggestionsInfo == null);
            while (true)
            {
                i++;
                break;
                Object localObject = null;
                int j = 0;
                if (j < localSentenceSuggestionsInfo.getSuggestionsCount())
                {
                    SuggestionsInfo localSuggestionsInfo = localSentenceSuggestionsInfo.getSuggestionsInfoAt(j);
                    if (localSuggestionsInfo == null);
                    while (true)
                    {
                        j++;
                        break;
                        SpellCheckSpan localSpellCheckSpan = onGetSuggestionsInternal(localSuggestionsInfo, localSentenceSuggestionsInfo.getOffsetAt(j), localSentenceSuggestionsInfo.getLengthAt(j));
                        if ((localObject == null) && (localSpellCheckSpan != null))
                            localObject = localSpellCheckSpan;
                    }
                }
                if (localObject != null)
                    localEditable.removeSpan(localObject);
            }
        }
        scheduleNewSpellCheck();
    }

    public void onGetSuggestions(SuggestionsInfo[] paramArrayOfSuggestionsInfo)
    {
        Editable localEditable = (Editable)this.mTextView.getText();
        for (int i = 0; i < paramArrayOfSuggestionsInfo.length; i++)
        {
            SpellCheckSpan localSpellCheckSpan = onGetSuggestionsInternal(paramArrayOfSuggestionsInfo[i], -1, -1);
            if (localSpellCheckSpan != null)
                localEditable.removeSpan(localSpellCheckSpan);
        }
        scheduleNewSpellCheck();
    }

    public void onSelectionChanged()
    {
        spellCheck();
    }

    public void onSpellCheckSpanRemoved(SpellCheckSpan paramSpellCheckSpan)
    {
        for (int i = 0; ; i++)
            if (i < this.mLength)
            {
                if (this.mSpellCheckSpans[i] == paramSpellCheckSpan)
                    this.mIds[i] = -1;
            }
            else
                return;
    }

    public void spellCheck(int paramInt1, int paramInt2)
    {
        Locale localLocale = this.mTextView.getTextServicesLocale();
        boolean bool = isSessionActive();
        if ((this.mCurrentLocale == null) || (!this.mCurrentLocale.equals(localLocale)))
        {
            setLocale(localLocale);
            paramInt1 = 0;
            paramInt2 = this.mTextView.getText().length();
        }
        label183: 
        while (true)
        {
            if (!bool);
            while (true)
            {
                return;
                if (bool == this.mTextServicesManager.isSpellCheckerEnabled())
                    break label183;
                resetSession();
                break;
                int i = this.mSpellParsers.length;
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label127;
                    SpellParser localSpellParser2 = this.mSpellParsers[j];
                    if (localSpellParser2.isFinished())
                    {
                        localSpellParser2.parse(paramInt1, paramInt2);
                        break;
                    }
                }
                label127: SpellParser[] arrayOfSpellParser = new SpellParser[i + 1];
                System.arraycopy(this.mSpellParsers, 0, arrayOfSpellParser, 0, i);
                this.mSpellParsers = arrayOfSpellParser;
                SpellParser localSpellParser1 = new SpellParser(null);
                this.mSpellParsers[i] = localSpellParser1;
                localSpellParser1.parse(paramInt1, paramInt2);
            }
        }
    }

    private class SpellParser
    {
        private Object mRange = new Object();

        private SpellParser()
        {
        }

        private void removeRangeSpan(Editable paramEditable)
        {
            paramEditable.removeSpan(this.mRange);
        }

        private <T> void removeSpansAt(Editable paramEditable, int paramInt, T[] paramArrayOfT)
        {
            int i = paramArrayOfT.length;
            int j = 0;
            if (j < i)
            {
                T ? = paramArrayOfT[j];
                if (paramEditable.getSpanStart(?) > paramInt);
                while (true)
                {
                    j++;
                    break;
                    if (paramEditable.getSpanEnd(?) >= paramInt)
                        paramEditable.removeSpan(?);
                }
            }
        }

        private void setRangeSpan(Editable paramEditable, int paramInt1, int paramInt2)
        {
            paramEditable.setSpan(this.mRange, paramInt1, paramInt2, 33);
        }

        public boolean isFinished()
        {
            if (((Editable)SpellChecker.this.mTextView.getText()).getSpanStart(this.mRange) < 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void parse()
        {
            Editable localEditable = (Editable)SpellChecker.this.mTextView.getText();
            int i;
            int j;
            int k;
            int m;
            int n;
            if (SpellChecker.this.mIsSentenceSpellCheckSupported)
            {
                i = Math.max(0, -50 + localEditable.getSpanStart(this.mRange));
                j = localEditable.getSpanEnd(this.mRange);
                k = Math.min(j, 350 + i);
                SpellChecker.this.mWordIterator.setCharSequence(localEditable, i, k);
                m = SpellChecker.this.mWordIterator.preceding(i);
                if (m != -1)
                    break label159;
                n = SpellChecker.this.mWordIterator.following(i);
                if (n != -1)
                    m = SpellChecker.this.mWordIterator.getBeginning(n);
            }
            while (true)
            {
                if (n != -1)
                    break label176;
                removeRangeSpan(localEditable);
                return;
                i = localEditable.getSpanStart(this.mRange);
                break;
                label159: n = SpellChecker.this.mWordIterator.getEnd(m);
            }
            label176: SpellCheckSpan[] arrayOfSpellCheckSpan = (SpellCheckSpan[])localEditable.getSpans(i - 1, j + 1, SpellCheckSpan.class);
            SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])localEditable.getSpans(i - 1, j + 1, SuggestionSpan.class);
            int i1 = 0;
            int i2 = 0;
            int i7;
            label263: label311: label317: int i9;
            int i10;
            SpellCheckSpan localSpellCheckSpan;
            label372: int i12;
            int i13;
            if (SpellChecker.this.mIsSentenceSpellCheckSupported)
            {
                if (k < j)
                    i2 = 1;
                i7 = SpellChecker.this.mWordIterator.preceding(k);
                if (i7 != -1)
                {
                    i8 = 1;
                    if (i8 != 0)
                    {
                        i7 = SpellChecker.this.mWordIterator.getEnd(i7);
                        if (i7 == -1)
                            break label311;
                    }
                }
                for (int i8 = 1; ; i8 = 0)
                {
                    if (i8 != 0)
                        break label317;
                    removeRangeSpan(localEditable);
                    break;
                    i8 = 0;
                    break label263;
                }
                i9 = m;
                i10 = 1;
                int i11 = 0;
                if (i11 < SpellChecker.this.mLength)
                {
                    localSpellCheckSpan = SpellChecker.this.mSpellCheckSpans[i11];
                    if ((SpellChecker.this.mIds[i11] < 0) || (localSpellCheckSpan.isSpellCheckInProgress()));
                    do
                    {
                        i11++;
                        break;
                        i12 = localEditable.getSpanStart(localSpellCheckSpan);
                        i13 = localEditable.getSpanEnd(localSpellCheckSpan);
                    }
                    while ((i13 < i9) || (i7 < i12));
                    if ((i12 <= i9) && (i7 <= i13))
                        i10 = 0;
                }
                else
                {
                    if (i7 >= i)
                        break label491;
                    label435: m = i7;
                    break label556;
                    label439: if (i2 == 0)
                        break label859;
                    setRangeSpan(localEditable, m, j);
                }
            }
            while (true)
            {
                SpellChecker.this.spellCheck();
                break;
                localEditable.removeSpan(localSpellCheckSpan);
                i9 = Math.min(i12, i9);
                i7 = Math.max(i13, i7);
                break label372;
                label491: if (i7 <= i9)
                {
                    Log.w(SpellChecker.TAG, "Trying to spellcheck invalid region, from " + i + " to " + j);
                    break label435;
                }
                if (i10 == 0)
                    break label435;
                SpellChecker.this.addSpellCheckSpan(localEditable, i9, i7);
                break label435;
                label556: if (m > j)
                    break label439;
                int i4;
                int i6;
                if ((n >= i) && (n > m))
                {
                    if (i1 >= 50)
                    {
                        i2 = 1;
                        break label439;
                    }
                    if ((m < i) && (n > i))
                    {
                        removeSpansAt(localEditable, i, arrayOfSpellCheckSpan);
                        removeSpansAt(localEditable, i, arrayOfSuggestionSpan);
                    }
                    if ((m < j) && (n > j))
                    {
                        removeSpansAt(localEditable, j, arrayOfSpellCheckSpan);
                        removeSpansAt(localEditable, j, arrayOfSuggestionSpan);
                    }
                    i4 = 1;
                    if (n == i)
                    {
                        i6 = 0;
                        label656: if (i6 < arrayOfSpellCheckSpan.length)
                        {
                            if (localEditable.getSpanEnd(arrayOfSpellCheckSpan[i6]) != i)
                                break label847;
                            i4 = 0;
                        }
                    }
                    if (m != j);
                }
                for (int i5 = 0; ; i5++)
                    if (i5 < arrayOfSpellCheckSpan.length)
                    {
                        if (localEditable.getSpanStart(arrayOfSpellCheckSpan[i5]) == j)
                            i4 = 0;
                    }
                    else
                    {
                        if (i4 != 0)
                            SpellChecker.this.addSpellCheckSpan(localEditable, m, n);
                        i1++;
                        int i3 = n;
                        n = SpellChecker.this.mWordIterator.following(n);
                        if ((k < j) && ((n == -1) || (n >= k)))
                        {
                            k = Math.min(j, i3 + 350);
                            SpellChecker.this.mWordIterator.setCharSequence(localEditable, i3, k);
                            n = SpellChecker.this.mWordIterator.following(i3);
                        }
                        if (n == -1)
                            break label439;
                        m = SpellChecker.this.mWordIterator.getBeginning(n);
                        if (m != -1)
                            break;
                        break label439;
                        label847: i6++;
                        break label656;
                    }
                label859: removeRangeSpan(localEditable);
            }
        }

        public void parse(int paramInt1, int paramInt2)
        {
            int i = SpellChecker.this.mTextView.length();
            if (paramInt2 > i)
                Log.w(SpellChecker.TAG, "Parse invalid region, from " + paramInt1 + " to " + paramInt2);
            for (int j = i; ; j = paramInt2)
            {
                if (j > paramInt1)
                {
                    setRangeSpan((Editable)SpellChecker.this.mTextView.getText(), paramInt1, j);
                    parse();
                }
                return;
            }
        }

        public void stop()
        {
            removeRangeSpan((Editable)SpellChecker.this.mTextView.getText());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SpellChecker
 * JD-Core Version:        0.6.2
 */