package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pool;
import android.util.Poolable;
import android.util.PoolableManager;
import android.util.Pools;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@RemoteViews.RemoteView
public class RelativeLayout extends ViewGroup
{
    public static final int ABOVE = 2;
    public static final int ALIGN_BASELINE = 4;
    public static final int ALIGN_BOTTOM = 8;
    public static final int ALIGN_LEFT = 5;
    public static final int ALIGN_PARENT_BOTTOM = 12;
    public static final int ALIGN_PARENT_LEFT = 9;
    public static final int ALIGN_PARENT_RIGHT = 11;
    public static final int ALIGN_PARENT_TOP = 10;
    public static final int ALIGN_RIGHT = 7;
    public static final int ALIGN_TOP = 6;
    public static final int BELOW = 3;
    public static final int CENTER_HORIZONTAL = 14;
    public static final int CENTER_IN_PARENT = 13;
    public static final int CENTER_VERTICAL = 15;
    private static final boolean DEBUG_GRAPH = false;
    public static final int LEFT_OF = 0;
    private static final String LOG_TAG = "RelativeLayout";
    public static final int RIGHT_OF = 1;
    private static final int[] RULES_HORIZONTAL = arrayOfInt2;
    private static final int[] RULES_VERTICAL;
    public static final int TRUE = -1;
    private static final int VERB_COUNT = 16;
    private View mBaselineView = null;
    private final Rect mContentBounds = new Rect();
    private boolean mDirtyHierarchy;
    private final DependencyGraph mGraph = new DependencyGraph(null);
    private int mGravity = 51;
    private boolean mHasBaselineAlignedChild;
    private int mIgnoreGravity;
    private final Rect mSelfBounds = new Rect();
    private View[] mSortedHorizontalChildren = new View[0];
    private View[] mSortedVerticalChildren = new View[0];
    private SortedSet<View> mTopToBottomLeftToRightSet = null;

    static
    {
        int[] arrayOfInt1 = new int[5];
        arrayOfInt1[0] = 2;
        arrayOfInt1[1] = 3;
        arrayOfInt1[2] = 4;
        arrayOfInt1[3] = 6;
        arrayOfInt1[4] = 8;
        RULES_VERTICAL = arrayOfInt1;
        int[] arrayOfInt2 = new int[4];
        arrayOfInt2[0] = 0;
        arrayOfInt2[1] = 1;
        arrayOfInt2[2] = 5;
        arrayOfInt2[3] = 7;
    }

    public RelativeLayout(Context paramContext)
    {
        super(paramContext);
    }

    public RelativeLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        initFromAttributes(paramContext, paramAttributeSet);
    }

    public RelativeLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initFromAttributes(paramContext, paramAttributeSet);
    }

    private void alignBaseline(View paramView, LayoutParams paramLayoutParams)
    {
        int[] arrayOfInt = paramLayoutParams.getRules();
        int i = getRelatedViewBaseline(arrayOfInt, 4);
        if (i != -1)
        {
            LayoutParams localLayoutParams2 = getRelatedViewParams(arrayOfInt, 4);
            if (localLayoutParams2 != null)
            {
                int j = i + localLayoutParams2.mTop;
                int k = paramView.getBaseline();
                if (k != -1)
                    j -= k;
                int m = paramLayoutParams.mBottom - paramLayoutParams.mTop;
                LayoutParams.access$402(paramLayoutParams, j);
                LayoutParams.access$202(paramLayoutParams, m + paramLayoutParams.mTop);
            }
        }
        if (this.mBaselineView == null);
        for (this.mBaselineView = paramView; ; this.mBaselineView = paramView)
        {
            LayoutParams localLayoutParams1;
            do
            {
                return;
                localLayoutParams1 = (LayoutParams)this.mBaselineView.getLayoutParams();
            }
            while ((paramLayoutParams.mTop >= localLayoutParams1.mTop) && ((paramLayoutParams.mTop != localLayoutParams1.mTop) || (paramLayoutParams.mLeft >= localLayoutParams1.mLeft)));
        }
    }

    private void applyHorizontalSizeRules(LayoutParams paramLayoutParams, int paramInt)
    {
        int[] arrayOfInt = paramLayoutParams.getRules();
        LayoutParams.access$302(paramLayoutParams, -1);
        LayoutParams.access$102(paramLayoutParams, -1);
        LayoutParams localLayoutParams1 = getRelatedViewParams(arrayOfInt, 0);
        if (localLayoutParams1 != null)
        {
            LayoutParams.access$102(paramLayoutParams, localLayoutParams1.mLeft - (localLayoutParams1.leftMargin + paramLayoutParams.rightMargin));
            LayoutParams localLayoutParams2 = getRelatedViewParams(arrayOfInt, 1);
            if (localLayoutParams2 == null)
                break label229;
            LayoutParams.access$302(paramLayoutParams, localLayoutParams2.mRight + (localLayoutParams2.rightMargin + paramLayoutParams.leftMargin));
            label87: LayoutParams localLayoutParams3 = getRelatedViewParams(arrayOfInt, 5);
            if (localLayoutParams3 == null)
                break label259;
            LayoutParams.access$302(paramLayoutParams, localLayoutParams3.mLeft + paramLayoutParams.leftMargin);
            label115: LayoutParams localLayoutParams4 = getRelatedViewParams(arrayOfInt, 7);
            if (localLayoutParams4 == null)
                break label289;
            LayoutParams.access$102(paramLayoutParams, localLayoutParams4.mRight - paramLayoutParams.rightMargin);
        }
        while (true)
        {
            if (arrayOfInt[9] != 0)
                LayoutParams.access$302(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
            if ((arrayOfInt[11] != 0) && (paramInt >= 0))
                LayoutParams.access$102(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin);
            return;
            if ((!paramLayoutParams.alignWithParent) || (arrayOfInt[0] == 0) || (paramInt < 0))
                break;
            LayoutParams.access$102(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin);
            break;
            label229: if ((!paramLayoutParams.alignWithParent) || (arrayOfInt[1] == 0))
                break label87;
            LayoutParams.access$302(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
            break label87;
            label259: if ((!paramLayoutParams.alignWithParent) || (arrayOfInt[5] == 0))
                break label115;
            LayoutParams.access$302(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
            break label115;
            label289: if ((paramLayoutParams.alignWithParent) && (arrayOfInt[7] != 0) && (paramInt >= 0))
                LayoutParams.access$102(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin);
        }
    }

    private void applyVerticalSizeRules(LayoutParams paramLayoutParams, int paramInt)
    {
        int[] arrayOfInt = paramLayoutParams.getRules();
        LayoutParams.access$402(paramLayoutParams, -1);
        LayoutParams.access$202(paramLayoutParams, -1);
        LayoutParams localLayoutParams1 = getRelatedViewParams(arrayOfInt, 2);
        if (localLayoutParams1 != null)
        {
            LayoutParams.access$202(paramLayoutParams, localLayoutParams1.mTop - (localLayoutParams1.topMargin + paramLayoutParams.bottomMargin));
            LayoutParams localLayoutParams2 = getRelatedViewParams(arrayOfInt, 3);
            if (localLayoutParams2 == null)
                break label241;
            LayoutParams.access$402(paramLayoutParams, localLayoutParams2.mBottom + (localLayoutParams2.bottomMargin + paramLayoutParams.topMargin));
            label87: LayoutParams localLayoutParams3 = getRelatedViewParams(arrayOfInt, 6);
            if (localLayoutParams3 == null)
                break label271;
            LayoutParams.access$402(paramLayoutParams, localLayoutParams3.mTop + paramLayoutParams.topMargin);
            label116: LayoutParams localLayoutParams4 = getRelatedViewParams(arrayOfInt, 8);
            if (localLayoutParams4 == null)
                break label302;
            LayoutParams.access$202(paramLayoutParams, localLayoutParams4.mBottom - paramLayoutParams.bottomMargin);
        }
        while (true)
        {
            if (arrayOfInt[10] != 0)
                LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
            if ((arrayOfInt[12] != 0) && (paramInt >= 0))
                LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingBottom - paramLayoutParams.bottomMargin);
            if (arrayOfInt[4] != 0)
                this.mHasBaselineAlignedChild = true;
            return;
            if ((!paramLayoutParams.alignWithParent) || (arrayOfInt[2] == 0) || (paramInt < 0))
                break;
            LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingBottom - paramLayoutParams.bottomMargin);
            break;
            label241: if ((!paramLayoutParams.alignWithParent) || (arrayOfInt[3] == 0))
                break label87;
            LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
            break label87;
            label271: if ((!paramLayoutParams.alignWithParent) || (arrayOfInt[6] == 0))
                break label116;
            LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
            break label116;
            label302: if ((paramLayoutParams.alignWithParent) && (arrayOfInt[8] != 0) && (paramInt >= 0))
                LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingBottom - paramLayoutParams.bottomMargin);
        }
    }

    private void centerHorizontal(View paramView, LayoutParams paramLayoutParams, int paramInt)
    {
        int i = paramView.getMeasuredWidth();
        int j = (paramInt - i) / 2;
        LayoutParams.access$302(paramLayoutParams, j);
        LayoutParams.access$102(paramLayoutParams, j + i);
    }

    private void centerVertical(View paramView, LayoutParams paramLayoutParams, int paramInt)
    {
        int i = paramView.getMeasuredHeight();
        int j = (paramInt - i) / 2;
        LayoutParams.access$402(paramLayoutParams, j);
        LayoutParams.access$202(paramLayoutParams, j + i);
    }

    private int getChildMeasureSpec(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        int i = 0;
        int j = 0;
        int k = paramInt1;
        int m = paramInt2;
        if (k < 0)
            k = paramInt6 + paramInt4;
        if (m < 0)
            m = paramInt8 - paramInt7 - paramInt5;
        int n = m - k;
        if ((paramInt1 >= 0) && (paramInt2 >= 0))
        {
            i = 1073741824;
            j = n;
        }
        while (true)
        {
            return View.MeasureSpec.makeMeasureSpec(j, i);
            if (paramInt3 >= 0)
            {
                i = 1073741824;
                if (n >= 0)
                    j = Math.min(n, paramInt3);
                else
                    j = paramInt3;
            }
            else if (paramInt3 == -1)
            {
                i = 1073741824;
                j = n;
            }
            else if (paramInt3 == -2)
            {
                if (n >= 0)
                {
                    i = -2147483648;
                    j = n;
                }
                else
                {
                    i = 0;
                    j = 0;
                }
            }
        }
    }

    private View getRelatedView(int[] paramArrayOfInt, int paramInt)
    {
        int i = paramArrayOfInt[paramInt];
        RelativeLayout.DependencyGraph.Node localNode1;
        View localView;
        if (i != 0)
        {
            localNode1 = (RelativeLayout.DependencyGraph.Node)this.mGraph.mKeyNodes.get(i);
            if (localNode1 == null)
                localView = null;
        }
        while (true)
        {
            return localView;
            RelativeLayout.DependencyGraph.Node localNode2;
            for (localView = localNode1.view; ; localView = localNode2.view)
            {
                if (localView.getVisibility() != 8)
                    break label103;
                int[] arrayOfInt = ((LayoutParams)localView.getLayoutParams()).getRules();
                localNode2 = (RelativeLayout.DependencyGraph.Node)this.mGraph.mKeyNodes.get(arrayOfInt[paramInt]);
                if (localNode2 == null)
                {
                    localView = null;
                    break;
                }
            }
            label103: continue;
            localView = null;
        }
    }

    private int getRelatedViewBaseline(int[] paramArrayOfInt, int paramInt)
    {
        View localView = getRelatedView(paramArrayOfInt, paramInt);
        if (localView != null);
        for (int i = localView.getBaseline(); ; i = -1)
            return i;
    }

    private LayoutParams getRelatedViewParams(int[] paramArrayOfInt, int paramInt)
    {
        View localView = getRelatedView(paramArrayOfInt, paramInt);
        if ((localView != null) && ((localView.getLayoutParams() instanceof LayoutParams)));
        for (LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams(); ; localLayoutParams = null)
            return localLayoutParams;
    }

    private void initFromAttributes(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RelativeLayout);
        this.mIgnoreGravity = localTypedArray.getResourceId(1, -1);
        this.mGravity = localTypedArray.getInt(0, this.mGravity);
        localTypedArray.recycle();
    }

    private void measureChild(View paramView, LayoutParams paramLayoutParams, int paramInt1, int paramInt2)
    {
        paramView.measure(getChildMeasureSpec(paramLayoutParams.mLeft, paramLayoutParams.mRight, paramLayoutParams.width, paramLayoutParams.leftMargin, paramLayoutParams.rightMargin, this.mPaddingLeft, this.mPaddingRight, paramInt1), getChildMeasureSpec(paramLayoutParams.mTop, paramLayoutParams.mBottom, paramLayoutParams.height, paramLayoutParams.topMargin, paramLayoutParams.bottomMargin, this.mPaddingTop, this.mPaddingBottom, paramInt2));
    }

    private void measureChildHorizontal(View paramView, LayoutParams paramLayoutParams, int paramInt1, int paramInt2)
    {
        int i = getChildMeasureSpec(paramLayoutParams.mLeft, paramLayoutParams.mRight, paramLayoutParams.width, paramLayoutParams.leftMargin, paramLayoutParams.rightMargin, this.mPaddingLeft, this.mPaddingRight, paramInt1);
        if (paramLayoutParams.width == -1);
        for (int j = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824); ; j = View.MeasureSpec.makeMeasureSpec(paramInt2, -2147483648))
        {
            paramView.measure(i, j);
            return;
        }
    }

    private boolean positionChildHorizontal(View paramView, LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean)
    {
        boolean bool = true;
        int[] arrayOfInt = paramLayoutParams.getRules();
        if ((paramLayoutParams.mLeft < 0) && (paramLayoutParams.mRight >= 0))
        {
            LayoutParams.access$302(paramLayoutParams, paramLayoutParams.mRight - paramView.getMeasuredWidth());
            if (arrayOfInt[11] == 0)
                break label186;
        }
        while (true)
        {
            return bool;
            if ((paramLayoutParams.mLeft >= 0) && (paramLayoutParams.mRight < 0))
            {
                LayoutParams.access$102(paramLayoutParams, paramLayoutParams.mLeft + paramView.getMeasuredWidth());
                break;
            }
            if ((paramLayoutParams.mLeft >= 0) || (paramLayoutParams.mRight >= 0))
                break;
            if ((arrayOfInt[13] != 0) || (arrayOfInt[14] != 0))
            {
                if (!paramBoolean)
                {
                    centerHorizontal(paramView, paramLayoutParams, paramInt);
                }
                else
                {
                    LayoutParams.access$302(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
                    LayoutParams.access$102(paramLayoutParams, paramLayoutParams.mLeft + paramView.getMeasuredWidth());
                }
            }
            else
            {
                LayoutParams.access$302(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
                LayoutParams.access$102(paramLayoutParams, paramLayoutParams.mLeft + paramView.getMeasuredWidth());
                break;
                label186: bool = false;
            }
        }
    }

    private boolean positionChildVertical(View paramView, LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean)
    {
        boolean bool = true;
        int[] arrayOfInt = paramLayoutParams.getRules();
        if ((paramLayoutParams.mTop < 0) && (paramLayoutParams.mBottom >= 0))
        {
            LayoutParams.access$402(paramLayoutParams, paramLayoutParams.mBottom - paramView.getMeasuredHeight());
            if (arrayOfInt[12] == 0)
                break label186;
        }
        while (true)
        {
            return bool;
            if ((paramLayoutParams.mTop >= 0) && (paramLayoutParams.mBottom < 0))
            {
                LayoutParams.access$202(paramLayoutParams, paramLayoutParams.mTop + paramView.getMeasuredHeight());
                break;
            }
            if ((paramLayoutParams.mTop >= 0) || (paramLayoutParams.mBottom >= 0))
                break;
            if ((arrayOfInt[13] != 0) || (arrayOfInt[15] != 0))
            {
                if (!paramBoolean)
                {
                    centerVertical(paramView, paramLayoutParams, paramInt);
                }
                else
                {
                    LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
                    LayoutParams.access$202(paramLayoutParams, paramLayoutParams.mTop + paramView.getMeasuredHeight());
                }
            }
            else
            {
                LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
                LayoutParams.access$202(paramLayoutParams, paramLayoutParams.mTop + paramView.getMeasuredHeight());
                break;
                label186: bool = false;
            }
        }
    }

    private void sortChildren()
    {
        int i = getChildCount();
        if (this.mSortedVerticalChildren.length != i)
            this.mSortedVerticalChildren = new View[i];
        if (this.mSortedHorizontalChildren.length != i)
            this.mSortedHorizontalChildren = new View[i];
        DependencyGraph localDependencyGraph = this.mGraph;
        localDependencyGraph.clear();
        for (int j = 0; j < i; j++)
            localDependencyGraph.add(getChildAt(j));
        localDependencyGraph.getSortedViews(this.mSortedVerticalChildren, RULES_VERTICAL);
        localDependencyGraph.getSortedViews(this.mSortedHorizontalChildren, RULES_HORIZONTAL);
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        if (this.mTopToBottomLeftToRightSet == null)
            this.mTopToBottomLeftToRightSet = new TreeSet(new TopToBottomLeftToRightComparator(null));
        int i = 0;
        int j = getChildCount();
        while (i < j)
        {
            this.mTopToBottomLeftToRightSet.add(getChildAt(i));
            i++;
        }
        Iterator localIterator = this.mTopToBottomLeftToRightSet.iterator();
        while (localIterator.hasNext())
        {
            View localView = (View)localIterator.next();
            if ((localView.getVisibility() == 0) && (localView.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent)))
                this.mTopToBottomLeftToRightSet.clear();
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            this.mTopToBottomLeftToRightSet.clear();
        }
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-2, -2);
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    public int getBaseline()
    {
        if (this.mBaselineView != null);
        for (int i = this.mBaselineView.getBaseline(); ; i = super.getBaseline())
            return i;
    }

    public int getGravity()
    {
        return this.mGravity;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(RelativeLayout.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(RelativeLayout.class.getName());
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            if (localView.getVisibility() != 8)
            {
                LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
                localView.layout(localLayoutParams.mLeft, localLayoutParams.mTop, localLayoutParams.mRight, localLayoutParams.mBottom);
            }
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        if (this.mDirtyHierarchy)
        {
            this.mDirtyHierarchy = false;
            sortChildren();
        }
        int i = -1;
        int j = -1;
        int k = 0;
        int m = 0;
        int n = View.MeasureSpec.getMode(paramInt1);
        int i1 = View.MeasureSpec.getMode(paramInt2);
        int i2 = View.MeasureSpec.getSize(paramInt1);
        int i3 = View.MeasureSpec.getSize(paramInt2);
        if (n != 0)
            i = i2;
        if (i1 != 0)
            j = i3;
        if (n == 1073741824)
            k = i;
        if (i1 == 1073741824)
            m = j;
        this.mHasBaselineAlignedChild = false;
        View localView1 = null;
        int i4 = 0x800007 & this.mGravity;
        int i5;
        int i7;
        label147: int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        boolean bool1;
        if ((i4 != 3) && (i4 != 0))
        {
            i5 = 1;
            int i6 = 0x70 & this.mGravity;
            if ((i6 == 48) || (i6 == 0))
                break label315;
            i7 = 1;
            i8 = 2147483647;
            i9 = 2147483647;
            i10 = -2147483648;
            i11 = -2147483648;
            i12 = 0;
            i13 = 0;
            if (((i5 != 0) || (i7 != 0)) && (this.mIgnoreGravity != -1))
                localView1 = findViewById(this.mIgnoreGravity);
            if (n == 1073741824)
                break label321;
            bool1 = true;
            label210: if (i1 == 1073741824)
                break label327;
        }
        label315: label321: label327: for (boolean bool2 = true; ; bool2 = false)
        {
            for (View localView7 : this.mSortedHorizontalChildren)
                if (localView7.getVisibility() != 8)
                {
                    LayoutParams localLayoutParams6 = (LayoutParams)localView7.getLayoutParams();
                    applyHorizontalSizeRules(localLayoutParams6, i);
                    measureChildHorizontal(localView7, localLayoutParams6, i, j);
                    if (positionChildHorizontal(localView7, localLayoutParams6, i, bool1))
                        i12 = 1;
                }
            i5 = 0;
            break;
            i7 = 0;
            break label147;
            bool1 = false;
            break label210;
        }
        for (View localView6 : this.mSortedVerticalChildren)
            if (localView6.getVisibility() != 8)
            {
                LayoutParams localLayoutParams5 = (LayoutParams)localView6.getLayoutParams();
                applyVerticalSizeRules(localLayoutParams5, j);
                measureChild(localView6, localLayoutParams5, i, j);
                if (positionChildVertical(localView6, localLayoutParams5, j, bool2))
                    i13 = 1;
                if (bool1)
                {
                    int i40 = localLayoutParams5.mRight;
                    k = Math.max(k, i40);
                }
                if (bool2)
                {
                    int i39 = localLayoutParams5.mBottom;
                    m = Math.max(m, i39);
                }
                if ((localView6 != localView1) || (i7 != 0))
                {
                    int i36 = localLayoutParams5.mLeft - localLayoutParams5.leftMargin;
                    i8 = Math.min(i8, i36);
                    int i37 = localLayoutParams5.mTop - localLayoutParams5.topMargin;
                    i9 = Math.min(i9, i37);
                }
                if ((localView6 != localView1) || (i5 != 0))
                {
                    int i38 = localLayoutParams5.mRight + localLayoutParams5.rightMargin;
                    i10 = Math.max(i10, i38);
                    i11 = Math.max(i11, localLayoutParams5.mBottom + localLayoutParams5.bottomMargin);
                }
            }
        if (this.mHasBaselineAlignedChild)
            for (int i32 = 0; i32 < ???; i32++)
            {
                View localView5 = getChildAt(i32);
                if (localView5.getVisibility() != 8)
                {
                    LayoutParams localLayoutParams4 = (LayoutParams)localView5.getLayoutParams();
                    alignBaseline(localView5, localLayoutParams4);
                    if ((localView5 != localView1) || (i7 != 0))
                    {
                        int i33 = localLayoutParams4.mLeft - localLayoutParams4.leftMargin;
                        i8 = Math.min(i8, i33);
                        int i34 = localLayoutParams4.mTop - localLayoutParams4.topMargin;
                        i9 = Math.min(i9, i34);
                    }
                    if ((localView5 != localView1) || (i5 != 0))
                    {
                        int i35 = localLayoutParams4.mRight + localLayoutParams4.rightMargin;
                        i10 = Math.max(i10, i35);
                        i11 = Math.max(i11, localLayoutParams4.mBottom + localLayoutParams4.bottomMargin);
                    }
                }
            }
        if (bool1)
        {
            int i27 = k + this.mPaddingRight;
            if (this.mLayoutParams.width >= 0)
            {
                int i31 = this.mLayoutParams.width;
                i27 = Math.max(i27, i31);
            }
            int i28 = getSuggestedMinimumWidth();
            k = resolveSize(Math.max(i27, i28), paramInt1);
            if (i12 != 0)
            {
                int i29 = 0;
                if (i29 < ???)
                {
                    View localView4 = getChildAt(i29);
                    LayoutParams localLayoutParams3;
                    int[] arrayOfInt2;
                    if (localView4.getVisibility() != 8)
                    {
                        localLayoutParams3 = (LayoutParams)localView4.getLayoutParams();
                        arrayOfInt2 = localLayoutParams3.getRules();
                        if ((arrayOfInt2[13] == 0) && (arrayOfInt2[14] == 0))
                            break label884;
                        centerHorizontal(localView4, localLayoutParams3, k);
                    }
                    while (true)
                    {
                        i29++;
                        break;
                        label884: if (arrayOfInt2[11] != 0)
                        {
                            int i30 = localView4.getMeasuredWidth();
                            LayoutParams.access$302(localLayoutParams3, k - this.mPaddingRight - i30);
                            LayoutParams.access$102(localLayoutParams3, i30 + localLayoutParams3.mLeft);
                        }
                    }
                }
            }
        }
        if (bool2)
        {
            int i22 = m + this.mPaddingBottom;
            if (this.mLayoutParams.height >= 0)
            {
                int i26 = this.mLayoutParams.height;
                i22 = Math.max(i22, i26);
            }
            int i23 = getSuggestedMinimumHeight();
            m = resolveSize(Math.max(i22, i23), paramInt2);
            if (i13 != 0)
            {
                int i24 = 0;
                if (i24 < ???)
                {
                    View localView3 = getChildAt(i24);
                    LayoutParams localLayoutParams2;
                    int[] arrayOfInt1;
                    if (localView3.getVisibility() != 8)
                    {
                        localLayoutParams2 = (LayoutParams)localView3.getLayoutParams();
                        arrayOfInt1 = localLayoutParams2.getRules();
                        if ((arrayOfInt1[13] == 0) && (arrayOfInt1[15] == 0))
                            break label1075;
                        centerVertical(localView3, localLayoutParams2, m);
                    }
                    while (true)
                    {
                        i24++;
                        break;
                        label1075: if (arrayOfInt1[12] != 0)
                        {
                            int i25 = localView3.getMeasuredHeight();
                            LayoutParams.access$402(localLayoutParams2, m - this.mPaddingBottom - i25);
                            LayoutParams.access$202(localLayoutParams2, i25 + localLayoutParams2.mTop);
                        }
                    }
                }
            }
        }
        if ((i5 != 0) || (i7 != 0))
        {
            Rect localRect1 = this.mSelfBounds;
            localRect1.set(this.mPaddingLeft, this.mPaddingTop, k - this.mPaddingRight, m - this.mPaddingBottom);
            Rect localRect2 = this.mContentBounds;
            int i18 = getResolvedLayoutDirection();
            Gravity.apply(this.mGravity, i10 - i8, i11 - i9, localRect1, localRect2, i18);
            int i19 = localRect2.left - i8;
            int i20 = localRect2.top - i9;
            if ((i19 != 0) || (i20 != 0))
                for (int i21 = 0; i21 < ???; i21++)
                {
                    View localView2 = getChildAt(i21);
                    if ((localView2.getVisibility() != 8) && (localView2 != localView1))
                    {
                        LayoutParams localLayoutParams1 = (LayoutParams)localView2.getLayoutParams();
                        if (i5 != 0)
                        {
                            LayoutParams.access$312(localLayoutParams1, i19);
                            LayoutParams.access$112(localLayoutParams1, i19);
                        }
                        if (i7 != 0)
                        {
                            LayoutParams.access$412(localLayoutParams1, i20);
                            LayoutParams.access$212(localLayoutParams1, i20);
                        }
                    }
                }
        }
        setMeasuredDimension(k, m);
    }

    public void requestLayout()
    {
        super.requestLayout();
        this.mDirtyHierarchy = true;
    }

    @RemotableViewMethod
    public void setGravity(int paramInt)
    {
        if (this.mGravity != paramInt)
        {
            if ((0x800007 & paramInt) == 0)
                paramInt |= 8388611;
            if ((paramInt & 0x70) == 0)
                paramInt |= 48;
            this.mGravity = paramInt;
            requestLayout();
        }
    }

    @RemotableViewMethod
    public void setHorizontalGravity(int paramInt)
    {
        int i = paramInt & 0x800007;
        if ((0x800007 & this.mGravity) != i)
        {
            this.mGravity = (i | 0xFF7FFFF8 & this.mGravity);
            requestLayout();
        }
    }

    @RemotableViewMethod
    public void setIgnoreGravity(int paramInt)
    {
        this.mIgnoreGravity = paramInt;
    }

    @RemotableViewMethod
    public void setVerticalGravity(int paramInt)
    {
        int i = paramInt & 0x70;
        if ((0x70 & this.mGravity) != i)
        {
            this.mGravity = (i | 0xFFFFFF8F & this.mGravity);
            requestLayout();
        }
    }

    public boolean shouldDelayChildPressedState()
    {
        return false;
    }

    private static class DependencyGraph
    {
        private SparseArray<Node> mKeyNodes = new SparseArray();
        private ArrayList<Node> mNodes = new ArrayList();
        private ArrayDeque<Node> mRoots = new ArrayDeque();

        private static void appendViewId(Resources paramResources, Node paramNode, StringBuilder paramStringBuilder)
        {
            if (paramNode.view.getId() != -1)
                paramStringBuilder.append(paramResources.getResourceEntryName(paramNode.view.getId()));
            while (true)
            {
                return;
                paramStringBuilder.append("NO_ID");
            }
        }

        private ArrayDeque<Node> findRoots(int[] paramArrayOfInt)
        {
            SparseArray localSparseArray = this.mKeyNodes;
            ArrayList localArrayList = this.mNodes;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
            {
                Node localNode4 = (Node)localArrayList.get(j);
                localNode4.dependents.clear();
                localNode4.dependencies.clear();
            }
            for (int k = 0; k < i; k++)
            {
                Node localNode2 = (Node)localArrayList.get(k);
                int[] arrayOfInt = RelativeLayout.LayoutParams.access$700((RelativeLayout.LayoutParams)localNode2.view.getLayoutParams());
                int n = paramArrayOfInt.length;
                int i1 = 0;
                if (i1 < n)
                {
                    int i2 = arrayOfInt[paramArrayOfInt[i1]];
                    Node localNode3;
                    if (i2 > 0)
                    {
                        localNode3 = (Node)localSparseArray.get(i2);
                        if ((localNode3 != null) && (localNode3 != localNode2))
                            break label153;
                    }
                    while (true)
                    {
                        i1++;
                        break;
                        label153: localNode3.dependents.put(localNode2, this);
                        localNode2.dependencies.put(i2, localNode3);
                    }
                }
            }
            ArrayDeque localArrayDeque = this.mRoots;
            localArrayDeque.clear();
            for (int m = 0; m < i; m++)
            {
                Node localNode1 = (Node)localArrayList.get(m);
                if (localNode1.dependencies.size() == 0)
                    localArrayDeque.addLast(localNode1);
            }
            return localArrayDeque;
        }

        private static void printNode(Resources paramResources, Node paramNode)
        {
            if (paramNode.dependents.size() == 0)
                printViewId(paramResources, paramNode.view);
            while (true)
            {
                return;
                Iterator localIterator = paramNode.dependents.keySet().iterator();
                while (localIterator.hasNext())
                {
                    Node localNode = (Node)localIterator.next();
                    StringBuilder localStringBuilder = new StringBuilder();
                    appendViewId(paramResources, paramNode, localStringBuilder);
                    printdependents(paramResources, localNode, localStringBuilder);
                }
            }
        }

        static void printViewId(Resources paramResources, View paramView)
        {
            if (paramView.getId() != -1)
                Log.d("RelativeLayout", paramResources.getResourceEntryName(paramView.getId()));
            while (true)
            {
                return;
                Log.d("RelativeLayout", "NO_ID");
            }
        }

        private static void printdependents(Resources paramResources, Node paramNode, StringBuilder paramStringBuilder)
        {
            paramStringBuilder.append(" -> ");
            appendViewId(paramResources, paramNode, paramStringBuilder);
            if (paramNode.dependents.size() == 0)
                Log.d("RelativeLayout", paramStringBuilder.toString());
            while (true)
            {
                return;
                Iterator localIterator = paramNode.dependents.keySet().iterator();
                while (localIterator.hasNext())
                    printdependents(paramResources, (Node)localIterator.next(), new StringBuilder(paramStringBuilder));
            }
        }

        void add(View paramView)
        {
            int i = paramView.getId();
            Node localNode = Node.acquire(paramView);
            if (i != -1)
                this.mKeyNodes.put(i, localNode);
            this.mNodes.add(localNode);
        }

        void clear()
        {
            ArrayList localArrayList = this.mNodes;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((Node)localArrayList.get(j)).release();
            localArrayList.clear();
            this.mKeyNodes.clear();
            this.mRoots.clear();
        }

        void getSortedViews(View[] paramArrayOfView, int[] paramArrayOfInt)
        {
            ArrayDeque localArrayDeque = findRoots(paramArrayOfInt);
            int k;
            for (int i = 0; ; i = k)
            {
                Node localNode1 = (Node)localArrayDeque.pollLast();
                if (localNode1 == null)
                    break;
                View localView = localNode1.view;
                int j = localView.getId();
                k = i + 1;
                paramArrayOfView[i] = localView;
                Iterator localIterator = localNode1.dependents.keySet().iterator();
                while (localIterator.hasNext())
                {
                    Node localNode2 = (Node)localIterator.next();
                    SparseArray localSparseArray = localNode2.dependencies;
                    localSparseArray.remove(j);
                    if (localSparseArray.size() == 0)
                        localArrayDeque.add(localNode2);
                }
            }
            if (i < paramArrayOfView.length)
                throw new IllegalStateException("Circular dependencies cannot exist in RelativeLayout");
        }

        void log(Resources paramResources, int[] paramArrayOfInt)
        {
            Iterator localIterator = findRoots(paramArrayOfInt).iterator();
            while (localIterator.hasNext())
                printNode(paramResources, (Node)localIterator.next());
        }

        static class Node
            implements Poolable<Node>
        {
            private static final int POOL_LIMIT = 100;
            private static final Pool<Node> sPool = Pools.synchronizedPool(Pools.finitePool(new PoolableManager()
            {
                public RelativeLayout.DependencyGraph.Node newInstance()
                {
                    return new RelativeLayout.DependencyGraph.Node();
                }

                public void onAcquired(RelativeLayout.DependencyGraph.Node paramAnonymousNode)
                {
                }

                public void onReleased(RelativeLayout.DependencyGraph.Node paramAnonymousNode)
                {
                }
            }
            , 100));
            final SparseArray<Node> dependencies = new SparseArray();
            final HashMap<Node, RelativeLayout.DependencyGraph> dependents = new HashMap();
            private boolean mIsPooled;
            private Node mNext;
            View view;

            static Node acquire(View paramView)
            {
                Node localNode = (Node)sPool.acquire();
                localNode.view = paramView;
                return localNode;
            }

            public Node getNextPoolable()
            {
                return this.mNext;
            }

            public boolean isPooled()
            {
                return this.mIsPooled;
            }

            void release()
            {
                this.view = null;
                this.dependents.clear();
                this.dependencies.clear();
                sPool.release(this);
            }

            public void setNextPoolable(Node paramNode)
            {
                this.mNext = paramNode;
            }

            public void setPooled(boolean paramBoolean)
            {
                this.mIsPooled = paramBoolean;
            }
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams
    {

        @ViewDebug.ExportedProperty(category="layout")
        public boolean alignWithParent;
        private int mBottom;
        private int mLeft;
        private int mRight;

        @ViewDebug.ExportedProperty(category="layout", indexMapping={@android.view.ViewDebug.IntToString(from=2, to="above"), @android.view.ViewDebug.IntToString(from=4, to="alignBaseline"), @android.view.ViewDebug.IntToString(from=8, to="alignBottom"), @android.view.ViewDebug.IntToString(from=5, to="alignLeft"), @android.view.ViewDebug.IntToString(from=12, to="alignParentBottom"), @android.view.ViewDebug.IntToString(from=9, to="alignParentLeft"), @android.view.ViewDebug.IntToString(from=11, to="alignParentRight"), @android.view.ViewDebug.IntToString(from=10, to="alignParentTop"), @android.view.ViewDebug.IntToString(from=7, to="alignRight"), @android.view.ViewDebug.IntToString(from=6, to="alignTop"), @android.view.ViewDebug.IntToString(from=3, to="below"), @android.view.ViewDebug.IntToString(from=14, to="centerHorizontal"), @android.view.ViewDebug.IntToString(from=13, to="center"), @android.view.ViewDebug.IntToString(from=15, to="centerVertical"), @android.view.ViewDebug.IntToString(from=0, to="leftOf"), @android.view.ViewDebug.IntToString(from=1, to="rightOf")}, mapping={@android.view.ViewDebug.IntToString(from=-1, to="true"), @android.view.ViewDebug.IntToString(from=0, to="false/NO_ID")}, resolveId=true)
        private int[] mRules = new int[16];
        private int mTop;

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RelativeLayout_Layout);
            int[] arrayOfInt = this.mRules;
            int i = localTypedArray.getIndexCount();
            int j = 0;
            if (j < i)
            {
                int k = localTypedArray.getIndex(j);
                switch (k)
                {
                default:
                case 16:
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    while (true)
                    {
                        j++;
                        break;
                        this.alignWithParent = localTypedArray.getBoolean(k, false);
                        continue;
                        arrayOfInt[0] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[1] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[2] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[3] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[4] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[5] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[6] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[7] = localTypedArray.getResourceId(k, 0);
                        continue;
                        arrayOfInt[8] = localTypedArray.getResourceId(k, 0);
                    }
                case 9:
                    if (localTypedArray.getBoolean(k, false));
                    for (int i5 = -1; ; i5 = 0)
                    {
                        arrayOfInt[9] = i5;
                        break;
                    }
                case 10:
                    if (localTypedArray.getBoolean(k, false));
                    for (int i4 = -1; ; i4 = 0)
                    {
                        arrayOfInt[10] = i4;
                        break;
                    }
                case 11:
                    if (localTypedArray.getBoolean(k, false));
                    for (int i3 = -1; ; i3 = 0)
                    {
                        arrayOfInt[11] = i3;
                        break;
                    }
                case 12:
                    if (localTypedArray.getBoolean(k, false));
                    for (int i2 = -1; ; i2 = 0)
                    {
                        arrayOfInt[12] = i2;
                        break;
                    }
                case 13:
                    if (localTypedArray.getBoolean(k, false));
                    for (int i1 = -1; ; i1 = 0)
                    {
                        arrayOfInt[13] = i1;
                        break;
                    }
                case 14:
                    if (localTypedArray.getBoolean(k, false));
                    for (int n = -1; ; n = 0)
                    {
                        arrayOfInt[14] = n;
                        break;
                    }
                case 15:
                }
                if (localTypedArray.getBoolean(k, false));
                for (int m = -1; ; m = 0)
                {
                    arrayOfInt[15] = m;
                    break;
                }
            }
            localTypedArray.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
        {
            super();
        }

        public void addRule(int paramInt)
        {
            this.mRules[paramInt] = -1;
        }

        public void addRule(int paramInt1, int paramInt2)
        {
            this.mRules[paramInt1] = paramInt2;
        }

        public String debug(String paramString)
        {
            return paramString + "ViewGroup.LayoutParams={ width=" + sizeToString(this.width) + ", height=" + sizeToString(this.height) + " }";
        }

        public int[] getRules()
        {
            return this.mRules;
        }
    }

    private class TopToBottomLeftToRightComparator
        implements Comparator<View>
    {
        private TopToBottomLeftToRightComparator()
        {
        }

        public int compare(View paramView1, View paramView2)
        {
            int i = paramView1.getTop() - paramView2.getTop();
            if (i != 0);
            while (true)
            {
                return i;
                int j = paramView1.getLeft() - paramView2.getLeft();
                if (j != 0)
                {
                    i = j;
                }
                else
                {
                    int k = paramView1.getHeight() - paramView2.getHeight();
                    if (k != 0)
                    {
                        i = k;
                    }
                    else
                    {
                        int m = paramView1.getWidth() - paramView2.getWidth();
                        if (m != 0)
                            i = m;
                        else
                            i = 0;
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.RelativeLayout
 * JD-Core Version:        0.6.2
 */