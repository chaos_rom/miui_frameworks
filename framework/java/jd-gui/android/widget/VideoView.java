package android.widget;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.media.Metadata;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.io.IOException;
import java.util.Map;

public class VideoView extends SurfaceView
    implements MediaController.MediaPlayerControl
{
    private static final int STATE_ERROR = -1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PAUSED = 4;
    private static final int STATE_PLAYBACK_COMPLETED = 5;
    private static final int STATE_PLAYING = 3;
    private static final int STATE_PREPARED = 2;
    private static final int STATE_PREPARING = 1;
    private String TAG = "VideoView";
    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener()
    {
        public void onBufferingUpdate(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt)
        {
            VideoView.access$1802(VideoView.this, paramAnonymousInt);
        }
    };
    private boolean mCanPause;
    private boolean mCanSeekBack;
    private boolean mCanSeekForward;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener()
    {
        public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
        {
            VideoView.access$202(VideoView.this, 5);
            VideoView.access$1202(VideoView.this, 5);
            if (VideoView.this.mMediaController != null)
                VideoView.this.mMediaController.hide();
            if (VideoView.this.mOnCompletionListener != null)
                VideoView.this.mOnCompletionListener.onCompletion(VideoView.this.mMediaPlayer);
        }
    };
    private int mCurrentBufferPercentage;
    private int mCurrentState = 0;
    private int mDuration;
    private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener()
    {
        public boolean onError(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            Log.d(VideoView.this.TAG, "Error: " + paramAnonymousInt1 + "," + paramAnonymousInt2);
            VideoView.access$202(VideoView.this, -1);
            VideoView.access$1202(VideoView.this, -1);
            if (VideoView.this.mMediaController != null)
                VideoView.this.mMediaController.hide();
            if ((VideoView.this.mOnErrorListener != null) && (VideoView.this.mOnErrorListener.onError(VideoView.this.mMediaPlayer, paramAnonymousInt1, paramAnonymousInt2)));
            while (VideoView.this.getWindowToken() == null)
                return true;
            VideoView.access$1600(VideoView.this).getResources();
            if (paramAnonymousInt1 == 200);
            for (int i = 17039381; ; i = 17039377)
            {
                new AlertDialog.Builder(VideoView.access$1700(VideoView.this)).setMessage(i).setPositiveButton(17039376, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
                    {
                        if (VideoView.this.mOnCompletionListener != null)
                            VideoView.this.mOnCompletionListener.onCompletion(VideoView.this.mMediaPlayer);
                    }
                }).setCancelable(false).show();
                break;
            }
        }
    };
    private Map<String, String> mHeaders;
    private MediaController mMediaController;
    private MediaPlayer mMediaPlayer = null;
    private MediaPlayer.OnCompletionListener mOnCompletionListener;
    private MediaPlayer.OnErrorListener mOnErrorListener;
    private MediaPlayer.OnPreparedListener mOnPreparedListener;
    MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener()
    {
        public void onPrepared(MediaPlayer paramAnonymousMediaPlayer)
        {
            VideoView.access$202(VideoView.this, 2);
            Metadata localMetadata = paramAnonymousMediaPlayer.getMetadata(false, false);
            boolean bool1;
            boolean bool2;
            label78: boolean bool3;
            label111: label119: int i;
            if (localMetadata != null)
            {
                VideoView localVideoView1 = VideoView.this;
                if ((!localMetadata.has(1)) || (localMetadata.getBoolean(1)))
                {
                    bool1 = true;
                    VideoView.access$302(localVideoView1, bool1);
                    VideoView localVideoView2 = VideoView.this;
                    if ((localMetadata.has(2)) && (!localMetadata.getBoolean(2)))
                        break label341;
                    bool2 = true;
                    VideoView.access$402(localVideoView2, bool2);
                    VideoView localVideoView3 = VideoView.this;
                    if ((localMetadata.has(3)) && (!localMetadata.getBoolean(3)))
                        break label347;
                    bool3 = true;
                    VideoView.access$502(localVideoView3, bool3);
                    if (VideoView.this.mOnPreparedListener != null)
                        VideoView.this.mOnPreparedListener.onPrepared(VideoView.this.mMediaPlayer);
                    if (VideoView.this.mMediaController != null)
                        VideoView.this.mMediaController.setEnabled(true);
                    VideoView.access$002(VideoView.this, paramAnonymousMediaPlayer.getVideoWidth());
                    VideoView.access$102(VideoView.this, paramAnonymousMediaPlayer.getVideoHeight());
                    i = VideoView.this.mSeekWhenPrepared;
                    if (i != 0)
                        VideoView.this.seekTo(i);
                    if ((VideoView.this.mVideoWidth == 0) || (VideoView.this.mVideoHeight == 0))
                        break label428;
                    VideoView.this.getHolder().setFixedSize(VideoView.this.mVideoWidth, VideoView.this.mVideoHeight);
                    if ((VideoView.this.mSurfaceWidth == VideoView.this.mVideoWidth) && (VideoView.this.mSurfaceHeight == VideoView.this.mVideoHeight))
                    {
                        if (VideoView.this.mTargetState != 3)
                            break label379;
                        VideoView.this.start();
                        if (VideoView.this.mMediaController != null)
                            VideoView.this.mMediaController.show();
                    }
                }
            }
            label428: 
            while (true)
            {
                return;
                bool1 = false;
                break;
                label341: bool2 = false;
                break label78;
                label347: bool3 = false;
                break label111;
                VideoView.access$302(VideoView.this, VideoView.access$402(VideoView.this, VideoView.access$502(VideoView.this, true)));
                break label119;
                label379: if ((!VideoView.this.isPlaying()) && ((i != 0) || (VideoView.this.getCurrentPosition() > 0)) && (VideoView.this.mMediaController != null))
                {
                    VideoView.this.mMediaController.show(0);
                    continue;
                    if (VideoView.this.mTargetState == 3)
                        VideoView.this.start();
                }
            }
        }
    };
    SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback()
    {
        public void surfaceChanged(SurfaceHolder paramAnonymousSurfaceHolder, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
        {
            VideoView.access$1002(VideoView.this, paramAnonymousInt2);
            VideoView.access$1102(VideoView.this, paramAnonymousInt3);
            int i;
            if (VideoView.this.mTargetState == 3)
            {
                i = 1;
                if ((VideoView.this.mVideoWidth != paramAnonymousInt2) || (VideoView.this.mVideoHeight != paramAnonymousInt3))
                    break label117;
            }
            label117: for (int j = 1; ; j = 0)
            {
                if ((VideoView.this.mMediaPlayer != null) && (i != 0) && (j != 0))
                {
                    if (VideoView.this.mSeekWhenPrepared != 0)
                        VideoView.this.seekTo(VideoView.this.mSeekWhenPrepared);
                    VideoView.this.start();
                }
                return;
                i = 0;
                break;
            }
        }

        public void surfaceCreated(SurfaceHolder paramAnonymousSurfaceHolder)
        {
            VideoView.access$1902(VideoView.this, paramAnonymousSurfaceHolder);
            VideoView.this.openVideo();
        }

        public void surfaceDestroyed(SurfaceHolder paramAnonymousSurfaceHolder)
        {
            VideoView.access$1902(VideoView.this, null);
            if (VideoView.this.mMediaController != null)
                VideoView.this.mMediaController.hide();
            VideoView.this.release(true);
        }
    };
    private int mSeekWhenPrepared;
    MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener()
    {
        public void onVideoSizeChanged(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            VideoView.access$002(VideoView.this, paramAnonymousMediaPlayer.getVideoWidth());
            VideoView.access$102(VideoView.this, paramAnonymousMediaPlayer.getVideoHeight());
            if ((VideoView.this.mVideoWidth != 0) && (VideoView.this.mVideoHeight != 0))
                VideoView.this.getHolder().setFixedSize(VideoView.this.mVideoWidth, VideoView.this.mVideoHeight);
        }
    };
    private int mSurfaceHeight;
    private SurfaceHolder mSurfaceHolder = null;
    private int mSurfaceWidth;
    private int mTargetState = 0;
    private Uri mUri;
    private int mVideoHeight;
    private int mVideoWidth;

    public VideoView(Context paramContext)
    {
        super(paramContext);
        initVideoView();
    }

    public VideoView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
        initVideoView();
    }

    public VideoView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initVideoView();
    }

    private void attachMediaController()
    {
        if ((this.mMediaPlayer != null) && (this.mMediaController != null))
        {
            this.mMediaController.setMediaPlayer(this);
            if (!(getParent() instanceof View))
                break label60;
        }
        label60: for (Object localObject = (View)getParent(); ; localObject = this)
        {
            this.mMediaController.setAnchorView((View)localObject);
            this.mMediaController.setEnabled(isInPlaybackState());
            return;
        }
    }

    private void initVideoView()
    {
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        getHolder().addCallback(this.mSHCallback);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.mCurrentState = 0;
        this.mTargetState = 0;
    }

    private boolean isInPlaybackState()
    {
        int i = 1;
        if ((this.mMediaPlayer != null) && (this.mCurrentState != -1) && (this.mCurrentState != 0) && (this.mCurrentState != i));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void openVideo()
    {
        if ((this.mUri == null) || (this.mSurfaceHolder == null));
        while (true)
        {
            return;
            Intent localIntent = new Intent("com.android.music.musicservicecommand");
            localIntent.putExtra("command", "pause");
            this.mContext.sendBroadcast(localIntent);
            release(false);
            try
            {
                this.mMediaPlayer = new MediaPlayer();
                this.mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
                this.mMediaPlayer.setOnVideoSizeChangedListener(this.mSizeChangedListener);
                this.mDuration = -1;
                this.mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
                this.mMediaPlayer.setOnErrorListener(this.mErrorListener);
                this.mMediaPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
                this.mCurrentBufferPercentage = 0;
                this.mMediaPlayer.setDataSource(this.mContext, this.mUri, this.mHeaders);
                this.mMediaPlayer.setDisplay(this.mSurfaceHolder);
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setScreenOnWhilePlaying(true);
                this.mMediaPlayer.prepareAsync();
                this.mCurrentState = 1;
                attachMediaController();
            }
            catch (IOException localIOException)
            {
                Log.w(this.TAG, "Unable to open content: " + this.mUri, localIOException);
                this.mCurrentState = -1;
                this.mTargetState = -1;
                this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                Log.w(this.TAG, "Unable to open content: " + this.mUri, localIllegalArgumentException);
                this.mCurrentState = -1;
                this.mTargetState = -1;
                this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
            }
        }
    }

    private void release(boolean paramBoolean)
    {
        if (this.mMediaPlayer != null)
        {
            this.mMediaPlayer.reset();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            this.mCurrentState = 0;
            if (paramBoolean)
                this.mTargetState = 0;
        }
    }

    private void toggleMediaControlsVisiblity()
    {
        if (this.mMediaController.isShowing())
            this.mMediaController.hide();
        while (true)
        {
            return;
            this.mMediaController.show();
        }
    }

    public boolean canPause()
    {
        return this.mCanPause;
    }

    public boolean canSeekBackward()
    {
        return this.mCanSeekBack;
    }

    public boolean canSeekForward()
    {
        return this.mCanSeekForward;
    }

    public int getBufferPercentage()
    {
        if (this.mMediaPlayer != null);
        for (int i = this.mCurrentBufferPercentage; ; i = 0)
            return i;
    }

    public int getCurrentPosition()
    {
        if (isInPlaybackState());
        for (int i = this.mMediaPlayer.getCurrentPosition(); ; i = 0)
            return i;
    }

    public int getDuration()
    {
        int i;
        if (isInPlaybackState())
            if (this.mDuration > 0)
                i = this.mDuration;
        while (true)
        {
            return i;
            this.mDuration = this.mMediaPlayer.getDuration();
            i = this.mDuration;
            continue;
            this.mDuration = -1;
            i = this.mDuration;
        }
    }

    public boolean isPlaying()
    {
        if ((isInPlaybackState()) && (this.mMediaPlayer.isPlaying()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(VideoView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(VideoView.class.getName());
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool1 = true;
        boolean bool2;
        if ((paramInt != 4) && (paramInt != 24) && (paramInt != 25) && (paramInt != 164) && (paramInt != 82) && (paramInt != 5) && (paramInt != 6))
        {
            bool2 = bool1;
            if ((!isInPlaybackState()) || (!bool2) || (this.mMediaController == null))
                break label190;
            if ((paramInt != 79) && (paramInt != 85))
                break label120;
            if (!this.mMediaPlayer.isPlaying())
                break label106;
            pause();
            this.mMediaController.show();
        }
        label190: 
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label106: start();
            this.mMediaController.hide();
            continue;
            label120: if (paramInt == 126)
            {
                if (!this.mMediaPlayer.isPlaying())
                {
                    start();
                    this.mMediaController.hide();
                }
            }
            else if ((paramInt == 86) || (paramInt == 127))
            {
                if (this.mMediaPlayer.isPlaying())
                {
                    pause();
                    this.mMediaController.show();
                }
            }
            else
            {
                toggleMediaControlsVisiblity();
                bool1 = super.onKeyDown(paramInt, paramKeyEvent);
            }
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getDefaultSize(this.mVideoWidth, paramInt1);
        int j = getDefaultSize(this.mVideoHeight, paramInt2);
        if ((this.mVideoWidth > 0) && (this.mVideoHeight > 0))
        {
            if (j * this.mVideoWidth <= i * this.mVideoHeight)
                break label70;
            j = i * this.mVideoHeight / this.mVideoWidth;
        }
        while (true)
        {
            setMeasuredDimension(i, j);
            return;
            label70: if (j * this.mVideoWidth < i * this.mVideoHeight)
                i = j * this.mVideoWidth / this.mVideoHeight;
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if ((isInPlaybackState()) && (this.mMediaController != null))
            toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        if ((isInPlaybackState()) && (this.mMediaController != null))
            toggleMediaControlsVisiblity();
        return false;
    }

    public void pause()
    {
        if ((isInPlaybackState()) && (this.mMediaPlayer.isPlaying()))
        {
            this.mMediaPlayer.pause();
            this.mCurrentState = 4;
        }
        this.mTargetState = 4;
    }

    public int resolveAdjustedSize(int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt2);
        switch (j)
        {
        default:
        case 0:
        case -2147483648:
        case 1073741824:
        }
        while (true)
        {
            return i;
            i = paramInt1;
            continue;
            i = Math.min(paramInt1, k);
            continue;
            i = k;
        }
    }

    public void resume()
    {
        openVideo();
    }

    public void seekTo(int paramInt)
    {
        if (isInPlaybackState())
            this.mMediaPlayer.seekTo(paramInt);
        for (this.mSeekWhenPrepared = 0; ; this.mSeekWhenPrepared = paramInt)
            return;
    }

    public void setMediaController(MediaController paramMediaController)
    {
        if (this.mMediaController != null)
            this.mMediaController.hide();
        this.mMediaController = paramMediaController;
        attachMediaController();
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener paramOnCompletionListener)
    {
        this.mOnCompletionListener = paramOnCompletionListener;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener paramOnErrorListener)
    {
        this.mOnErrorListener = paramOnErrorListener;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener paramOnPreparedListener)
    {
        this.mOnPreparedListener = paramOnPreparedListener;
    }

    public void setVideoPath(String paramString)
    {
        setVideoURI(Uri.parse(paramString));
    }

    public void setVideoURI(Uri paramUri)
    {
        setVideoURI(paramUri, null);
    }

    public void setVideoURI(Uri paramUri, Map<String, String> paramMap)
    {
        this.mUri = paramUri;
        this.mHeaders = paramMap;
        this.mSeekWhenPrepared = 0;
        openVideo();
        requestLayout();
        invalidate();
    }

    public void start()
    {
        if (isInPlaybackState())
        {
            this.mMediaPlayer.start();
            this.mCurrentState = 3;
        }
        this.mTargetState = 3;
    }

    public void stopPlayback()
    {
        if (this.mMediaPlayer != null)
        {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            this.mCurrentState = 0;
            this.mTargetState = 0;
        }
    }

    public void suspend()
    {
        release(false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.VideoView
 * JD-Core Version:        0.6.2
 */