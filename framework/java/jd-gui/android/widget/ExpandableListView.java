package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.ArrayList;

public class ExpandableListView extends ListView
{
    public static final int CHILD_INDICATOR_INHERIT = -1;
    private static final int[] CHILD_LAST_STATE_SET = arrayOfInt4;
    private static final int[] EMPTY_STATE_SET = new int[0];
    private static final int[] GROUP_EMPTY_STATE_SET;
    private static final int[] GROUP_EXPANDED_EMPTY_STATE_SET;
    private static final int[] GROUP_EXPANDED_STATE_SET;
    private static final int[][] GROUP_STATE_SETS;
    private static final long PACKED_POSITION_INT_MASK_CHILD = -1L;
    private static final long PACKED_POSITION_INT_MASK_GROUP = 2147483647L;
    private static final long PACKED_POSITION_MASK_CHILD = 4294967295L;
    private static final long PACKED_POSITION_MASK_GROUP = 9223372032559808512L;
    private static final long PACKED_POSITION_MASK_TYPE = -9223372036854775808L;
    private static final long PACKED_POSITION_SHIFT_GROUP = 32L;
    private static final long PACKED_POSITION_SHIFT_TYPE = 63L;
    public static final int PACKED_POSITION_TYPE_CHILD = 1;
    public static final int PACKED_POSITION_TYPE_GROUP = 0;
    public static final int PACKED_POSITION_TYPE_NULL = 2;
    public static final long PACKED_POSITION_VALUE_NULL = 4294967295L;
    private ExpandableListAdapter mAdapter;
    private Drawable mChildDivider;
    private Drawable mChildIndicator;
    private int mChildIndicatorLeft;
    private int mChildIndicatorRight;
    private ExpandableListConnector mConnector;
    private Drawable mGroupIndicator;
    private int mIndicatorLeft;
    private final Rect mIndicatorRect = new Rect();
    private int mIndicatorRight;
    private OnChildClickListener mOnChildClickListener;
    private OnGroupClickListener mOnGroupClickListener;
    private OnGroupCollapseListener mOnGroupCollapseListener;
    private OnGroupExpandListener mOnGroupExpandListener;

    static
    {
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = 16842920;
        GROUP_EXPANDED_STATE_SET = arrayOfInt1;
        int[] arrayOfInt2 = new int[1];
        arrayOfInt2[0] = 16842921;
        GROUP_EMPTY_STATE_SET = arrayOfInt2;
        int[] arrayOfInt3 = new int[2];
        arrayOfInt3[0] = 16842920;
        arrayOfInt3[1] = 16842921;
        GROUP_EXPANDED_EMPTY_STATE_SET = arrayOfInt3;
        int[][] arrayOfInt = new int[4][];
        arrayOfInt[0] = EMPTY_STATE_SET;
        arrayOfInt[1] = GROUP_EXPANDED_STATE_SET;
        arrayOfInt[2] = GROUP_EMPTY_STATE_SET;
        arrayOfInt[3] = GROUP_EXPANDED_EMPTY_STATE_SET;
        GROUP_STATE_SETS = arrayOfInt;
        int[] arrayOfInt4 = new int[1];
        arrayOfInt4[0] = 16842918;
    }

    public ExpandableListView(Context paramContext)
    {
        this(paramContext, null);
    }

    public ExpandableListView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842863);
    }

    public ExpandableListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ExpandableListView, paramInt, 0);
        this.mGroupIndicator = localTypedArray.getDrawable(0);
        this.mChildIndicator = localTypedArray.getDrawable(1);
        this.mIndicatorLeft = localTypedArray.getDimensionPixelSize(2, 0);
        this.mIndicatorRight = localTypedArray.getDimensionPixelSize(3, 0);
        if ((this.mIndicatorRight == 0) && (this.mGroupIndicator != null))
            this.mIndicatorRight = (this.mIndicatorLeft + this.mGroupIndicator.getIntrinsicWidth());
        this.mChildIndicatorLeft = localTypedArray.getDimensionPixelSize(4, -1);
        this.mChildIndicatorRight = localTypedArray.getDimensionPixelSize(5, -1);
        this.mChildDivider = localTypedArray.getDrawable(6);
        localTypedArray.recycle();
    }

    private int getAbsoluteFlatPosition(int paramInt)
    {
        return paramInt + getHeaderViewsCount();
    }

    private long getChildOrGroupId(ExpandableListPosition paramExpandableListPosition)
    {
        if (paramExpandableListPosition.type == 1);
        for (long l = this.mAdapter.getChildId(paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos); ; l = this.mAdapter.getGroupId(paramExpandableListPosition.groupPos))
            return l;
    }

    private int getFlatPositionForConnector(int paramInt)
    {
        return paramInt - getHeaderViewsCount();
    }

    private Drawable getIndicator(ExpandableListConnector.PositionMetadata paramPositionMetadata)
    {
        int i = 1;
        int j = 0;
        Drawable localDrawable;
        int k;
        if (paramPositionMetadata.position.type == 2)
        {
            localDrawable = this.mGroupIndicator;
            if ((localDrawable != null) && (localDrawable.isStateful()))
            {
                if ((paramPositionMetadata.groupMetadata != null) && (paramPositionMetadata.groupMetadata.lastChildFlPos != paramPositionMetadata.groupMetadata.flPos))
                    break label95;
                k = i;
                if (!paramPositionMetadata.isExpanded())
                    break label101;
                label68: if (k != 0)
                    j = 2;
                int m = i | j;
                localDrawable.setState(GROUP_STATE_SETS[m]);
            }
        }
        label95: label101: 
        do
        {
            return localDrawable;
            k = 0;
            break;
            i = 0;
            break label68;
            localDrawable = this.mChildIndicator;
        }
        while ((localDrawable == null) || (!localDrawable.isStateful()));
        if (paramPositionMetadata.position.flatListPos == paramPositionMetadata.groupMetadata.lastChildFlPos);
        for (int[] arrayOfInt = CHILD_LAST_STATE_SET; ; arrayOfInt = EMPTY_STATE_SET)
        {
            localDrawable.setState(arrayOfInt);
            break;
        }
    }

    public static int getPackedPositionChild(long paramLong)
    {
        int i = -1;
        if (paramLong == 4294967295L);
        while (true)
        {
            return i;
            if ((paramLong & 0x0) == -9223372036854775808L)
                i = (int)(paramLong & 0xFFFFFFFF);
        }
    }

    public static long getPackedPositionForChild(int paramInt1, int paramInt2)
    {
        return 0x0 | (0x7FFFFFFF & paramInt1) << 32 | 0xFFFFFFFF & paramInt2;
    }

    public static long getPackedPositionForGroup(int paramInt)
    {
        return (0x7FFFFFFF & paramInt) << 32;
    }

    public static int getPackedPositionGroup(long paramLong)
    {
        if (paramLong == 4294967295L);
        for (int i = -1; ; i = (int)((0x0 & paramLong) >> 32))
            return i;
    }

    public static int getPackedPositionType(long paramLong)
    {
        int i;
        if (paramLong == 4294967295L)
            i = 2;
        while (true)
        {
            return i;
            if ((paramLong & 0x0) == -9223372036854775808L)
                i = 1;
            else
                i = 0;
        }
    }

    private boolean isHeaderOrFooterPosition(int paramInt)
    {
        int i = this.mItemCount - getFooterViewsCount();
        if ((paramInt < getHeaderViewsCount()) || (paramInt >= i));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean collapseGroup(int paramInt)
    {
        boolean bool = this.mConnector.collapseGroup(paramInt);
        if (this.mOnGroupCollapseListener != null)
            this.mOnGroupCollapseListener.onGroupCollapse(paramInt);
        return bool;
    }

    ContextMenu.ContextMenuInfo createContextMenuInfo(View paramView, int paramInt, long paramLong)
    {
        if (isHeaderOrFooterPosition(paramInt));
        long l1;
        long l2;
        for (Object localObject = new AdapterView.AdapterContextMenuInfo(paramView, paramInt, paramLong); ; localObject = new ExpandableListContextMenuInfo(paramView, l2, l1))
        {
            return localObject;
            int i = getFlatPositionForConnector(paramInt);
            ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getUnflattenedPos(i);
            ExpandableListPosition localExpandableListPosition = localPositionMetadata.position;
            l1 = getChildOrGroupId(localExpandableListPosition);
            l2 = localExpandableListPosition.getPackedPosition();
            localPositionMetadata.recycle();
        }
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        super.dispatchDraw(paramCanvas);
        if ((this.mChildIndicator == null) && (this.mGroupIndicator == null))
            return;
        int i = 0;
        int j;
        label36: int m;
        int n;
        int i1;
        Rect localRect;
        int i3;
        int i4;
        if ((0x22 & this.mGroupFlags) == 34)
        {
            j = 1;
            if (j != 0)
            {
                i = paramCanvas.save();
                int i9 = this.mScrollX;
                int i10 = this.mScrollY;
                paramCanvas.clipRect(i9 + this.mPaddingLeft, i10 + this.mPaddingTop, i9 + this.mRight - this.mLeft - this.mPaddingRight, i10 + this.mBottom - this.mTop - this.mPaddingBottom);
            }
            int k = getHeaderViewsCount();
            m = -1 + (this.mItemCount - getFooterViewsCount() - k);
            n = this.mBottom;
            i1 = -4;
            localRect = this.mIndicatorRect;
            int i2 = getChildCount();
            i3 = 0;
            i4 = this.mFirstPosition - k;
            label167: if (i3 >= i2)
                break label200;
            if (i4 >= 0)
                break label193;
        }
        label193: label200: int i5;
        int i6;
        do
        {
            i3++;
            i4++;
            break label167;
            j = 0;
            break label36;
            if (i4 > m)
            {
                if (j == 0)
                    break;
                paramCanvas.restoreToCount(i);
                break;
            }
            View localView = getChildAt(i3);
            i5 = localView.getTop();
            i6 = localView.getBottom();
        }
        while ((i6 < 0) || (i5 > n));
        ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getUnflattenedPos(i4);
        int i7;
        label297: int i8;
        if (localPositionMetadata.position.type != i1)
        {
            if (localPositionMetadata.position.type != 1)
                break label452;
            if (this.mChildIndicatorLeft == -1)
            {
                i7 = this.mIndicatorLeft;
                localRect.left = i7;
                if (this.mChildIndicatorRight != -1)
                    break label443;
                i8 = this.mIndicatorRight;
                label319: localRect.right = i8;
                label326: localRect.left += this.mPaddingLeft;
                localRect.right += this.mPaddingLeft;
                i1 = localPositionMetadata.position.type;
            }
        }
        else if (localRect.left != localRect.right)
        {
            if (!this.mStackFromBottom)
                break label473;
            localRect.top = i5;
        }
        for (localRect.bottom = i6; ; localRect.bottom = i6)
        {
            Drawable localDrawable = getIndicator(localPositionMetadata);
            if (localDrawable != null)
            {
                localDrawable.setBounds(localRect);
                localDrawable.draw(paramCanvas);
            }
            localPositionMetadata.recycle();
            break;
            i7 = this.mChildIndicatorLeft;
            break label297;
            label443: i8 = this.mChildIndicatorRight;
            break label319;
            label452: localRect.left = this.mIndicatorLeft;
            localRect.right = this.mIndicatorRight;
            break label326;
            label473: localRect.top = i5;
        }
    }

    void drawDivider(Canvas paramCanvas, Rect paramRect, int paramInt)
    {
        int i = paramInt + this.mFirstPosition;
        ExpandableListConnector.PositionMetadata localPositionMetadata;
        if (i >= 0)
        {
            int j = getFlatPositionForConnector(i);
            localPositionMetadata = this.mConnector.getUnflattenedPos(j);
            if ((localPositionMetadata.position.type == 1) || ((localPositionMetadata.isExpanded()) && (localPositionMetadata.groupMetadata.lastChildFlPos != localPositionMetadata.groupMetadata.flPos)))
            {
                Drawable localDrawable = this.mChildDivider;
                localDrawable.setBounds(paramRect);
                localDrawable.draw(paramCanvas);
                localPositionMetadata.recycle();
            }
        }
        while (true)
        {
            return;
            localPositionMetadata.recycle();
            super.drawDivider(paramCanvas, paramRect, i);
        }
    }

    public boolean expandGroup(int paramInt)
    {
        return expandGroup(paramInt, false);
    }

    public boolean expandGroup(int paramInt, boolean paramBoolean)
    {
        ExpandableListPosition localExpandableListPosition = ExpandableListPosition.obtain(2, paramInt, -1, -1);
        ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getFlattenedPos(localExpandableListPosition);
        localExpandableListPosition.recycle();
        boolean bool = this.mConnector.expandGroup(localPositionMetadata);
        if (this.mOnGroupExpandListener != null)
            this.mOnGroupExpandListener.onGroupExpand(paramInt);
        if (paramBoolean)
        {
            int i = localPositionMetadata.position.flatListPos + getHeaderViewsCount();
            smoothScrollToPosition(i + this.mAdapter.getChildrenCount(paramInt), i);
        }
        localPositionMetadata.recycle();
        return bool;
    }

    public ListAdapter getAdapter()
    {
        return super.getAdapter();
    }

    public ExpandableListAdapter getExpandableListAdapter()
    {
        return this.mAdapter;
    }

    public long getExpandableListPosition(int paramInt)
    {
        long l;
        if (isHeaderOrFooterPosition(paramInt))
            l = 4294967295L;
        while (true)
        {
            return l;
            int i = getFlatPositionForConnector(paramInt);
            ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getUnflattenedPos(i);
            l = localPositionMetadata.position.getPackedPosition();
            localPositionMetadata.recycle();
        }
    }

    public int getFlatListPosition(long paramLong)
    {
        ExpandableListPosition localExpandableListPosition = ExpandableListPosition.obtainPosition(paramLong);
        ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getFlattenedPos(localExpandableListPosition);
        localExpandableListPosition.recycle();
        int i = localPositionMetadata.position.flatListPos;
        localPositionMetadata.recycle();
        return getAbsoluteFlatPosition(i);
    }

    public long getSelectedId()
    {
        long l1 = getSelectedPosition();
        long l2;
        if (l1 == 4294967295L)
            l2 = -1L;
        while (true)
        {
            return l2;
            int i = getPackedPositionGroup(l1);
            if (getPackedPositionType(l1) == 0)
                l2 = this.mAdapter.getGroupId(i);
            else
                l2 = this.mAdapter.getChildId(i, getPackedPositionChild(l1));
        }
    }

    public long getSelectedPosition()
    {
        return getExpandableListPosition(getSelectedItemPosition());
    }

    boolean handleItemClick(View paramView, int paramInt, long paramLong)
    {
        ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getUnflattenedPos(paramInt);
        long l = getChildOrGroupId(localPositionMetadata.position);
        if (localPositionMetadata.position.type == 2)
        {
            if ((this.mOnGroupClickListener != null) && (this.mOnGroupClickListener.onGroupClick(this, paramView, localPositionMetadata.position.groupPos, l)))
            {
                localPositionMetadata.recycle();
                bool = true;
                return bool;
            }
            if (localPositionMetadata.isExpanded())
            {
                this.mConnector.collapseGroup(localPositionMetadata);
                playSoundEffect(0);
                if (this.mOnGroupCollapseListener != null)
                    this.mOnGroupCollapseListener.onGroupCollapse(localPositionMetadata.position.groupPos);
            }
        }
        label122: for (boolean bool = true; ; bool = false)
        {
            localPositionMetadata.recycle();
            break;
            this.mConnector.expandGroup(localPositionMetadata);
            playSoundEffect(0);
            if (this.mOnGroupExpandListener != null)
                this.mOnGroupExpandListener.onGroupExpand(localPositionMetadata.position.groupPos);
            int i = localPositionMetadata.position.groupPos;
            int j = localPositionMetadata.position.flatListPos + getHeaderViewsCount();
            smoothScrollToPosition(j + this.mAdapter.getChildrenCount(i), j);
            break label122;
            if (this.mOnChildClickListener != null)
            {
                playSoundEffect(0);
                bool = this.mOnChildClickListener.onChildClick(this, paramView, localPositionMetadata.position.groupPos, localPositionMetadata.position.childPos, l);
                break;
            }
        }
    }

    public boolean isGroupExpanded(int paramInt)
    {
        return this.mConnector.isGroupExpanded(paramInt);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ExpandableListView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ExpandableListView.class.getName());
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if (!(paramParcelable instanceof SavedState))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            if ((this.mConnector != null) && (localSavedState.expandedGroupMetadataList != null))
                this.mConnector.setExpandedGroupMetadataList(localSavedState.expandedGroupMetadataList);
        }
    }

    public Parcelable onSaveInstanceState()
    {
        Parcelable localParcelable = super.onSaveInstanceState();
        if (this.mConnector != null);
        for (ArrayList localArrayList = this.mConnector.getExpandedGroupMetadataList(); ; localArrayList = null)
            return new SavedState(localParcelable, localArrayList);
    }

    public boolean performItemClick(View paramView, int paramInt, long paramLong)
    {
        if (isHeaderOrFooterPosition(paramInt));
        for (boolean bool = super.performItemClick(paramView, paramInt, paramLong); ; bool = handleItemClick(paramView, getFlatPositionForConnector(paramInt), paramLong))
            return bool;
    }

    public void setAdapter(ExpandableListAdapter paramExpandableListAdapter)
    {
        this.mAdapter = paramExpandableListAdapter;
        if (paramExpandableListAdapter != null);
        for (this.mConnector = new ExpandableListConnector(paramExpandableListAdapter); ; this.mConnector = null)
        {
            super.setAdapter(this.mConnector);
            return;
        }
    }

    public void setAdapter(ListAdapter paramListAdapter)
    {
        throw new RuntimeException("For ExpandableListView, use setAdapter(ExpandableListAdapter) instead of setAdapter(ListAdapter)");
    }

    public void setChildDivider(Drawable paramDrawable)
    {
        this.mChildDivider = paramDrawable;
    }

    public void setChildIndicator(Drawable paramDrawable)
    {
        this.mChildIndicator = paramDrawable;
    }

    public void setChildIndicatorBounds(int paramInt1, int paramInt2)
    {
        this.mChildIndicatorLeft = paramInt1;
        this.mChildIndicatorRight = paramInt2;
    }

    public void setGroupIndicator(Drawable paramDrawable)
    {
        this.mGroupIndicator = paramDrawable;
        if ((this.mIndicatorRight == 0) && (this.mGroupIndicator != null))
            this.mIndicatorRight = (this.mIndicatorLeft + this.mGroupIndicator.getIntrinsicWidth());
    }

    public void setIndicatorBounds(int paramInt1, int paramInt2)
    {
        this.mIndicatorLeft = paramInt1;
        this.mIndicatorRight = paramInt2;
    }

    public void setOnChildClickListener(OnChildClickListener paramOnChildClickListener)
    {
        this.mOnChildClickListener = paramOnChildClickListener;
    }

    public void setOnGroupClickListener(OnGroupClickListener paramOnGroupClickListener)
    {
        this.mOnGroupClickListener = paramOnGroupClickListener;
    }

    public void setOnGroupCollapseListener(OnGroupCollapseListener paramOnGroupCollapseListener)
    {
        this.mOnGroupCollapseListener = paramOnGroupCollapseListener;
    }

    public void setOnGroupExpandListener(OnGroupExpandListener paramOnGroupExpandListener)
    {
        this.mOnGroupExpandListener = paramOnGroupExpandListener;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
    {
        super.setOnItemClickListener(paramOnItemClickListener);
    }

    public boolean setSelectedChild(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        ExpandableListPosition localExpandableListPosition = ExpandableListPosition.obtainChildPosition(paramInt1, paramInt2);
        ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getFlattenedPos(localExpandableListPosition);
        if (localPositionMetadata == null)
            if (paramBoolean);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            expandGroup(paramInt1);
            localPositionMetadata = this.mConnector.getFlattenedPos(localExpandableListPosition);
            if (localPositionMetadata == null)
                throw new IllegalStateException("Could not find child");
            super.setSelection(getAbsoluteFlatPosition(localPositionMetadata.position.flatListPos));
            localExpandableListPosition.recycle();
            localPositionMetadata.recycle();
        }
    }

    public void setSelectedGroup(int paramInt)
    {
        ExpandableListPosition localExpandableListPosition = ExpandableListPosition.obtainGroupPosition(paramInt);
        ExpandableListConnector.PositionMetadata localPositionMetadata = this.mConnector.getFlattenedPos(localExpandableListPosition);
        localExpandableListPosition.recycle();
        super.setSelection(getAbsoluteFlatPosition(localPositionMetadata.position.flatListPos));
        localPositionMetadata.recycle();
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public ExpandableListView.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ExpandableListView.SavedState(paramAnonymousParcel, null);
            }

            public ExpandableListView.SavedState[] newArray(int paramAnonymousInt)
            {
                return new ExpandableListView.SavedState[paramAnonymousInt];
            }
        };
        ArrayList<ExpandableListConnector.GroupMetadata> expandedGroupMetadataList;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.expandedGroupMetadataList = new ArrayList();
            paramParcel.readList(this.expandedGroupMetadataList, ExpandableListConnector.class.getClassLoader());
        }

        SavedState(Parcelable paramParcelable, ArrayList<ExpandableListConnector.GroupMetadata> paramArrayList)
        {
            super();
            this.expandedGroupMetadataList = paramArrayList;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeList(this.expandedGroupMetadataList);
        }
    }

    public static class ExpandableListContextMenuInfo
        implements ContextMenu.ContextMenuInfo
    {
        public long id;
        public long packedPosition;
        public View targetView;

        public ExpandableListContextMenuInfo(View paramView, long paramLong1, long paramLong2)
        {
            this.targetView = paramView;
            this.packedPosition = paramLong1;
            this.id = paramLong2;
        }
    }

    public static abstract interface OnChildClickListener
    {
        public abstract boolean onChildClick(ExpandableListView paramExpandableListView, View paramView, int paramInt1, int paramInt2, long paramLong);
    }

    public static abstract interface OnGroupClickListener
    {
        public abstract boolean onGroupClick(ExpandableListView paramExpandableListView, View paramView, int paramInt, long paramLong);
    }

    public static abstract interface OnGroupExpandListener
    {
        public abstract void onGroupExpand(int paramInt);
    }

    public static abstract interface OnGroupCollapseListener
    {
        public abstract void onGroupCollapse(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ExpandableListView
 * JD-Core Version:        0.6.2
 */