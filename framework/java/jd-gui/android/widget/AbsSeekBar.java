package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public abstract class AbsSeekBar extends ProgressBar
{
    private static final int NO_ALPHA = 255;
    private float mDisabledAlpha;
    private boolean mIsDragging;
    boolean mIsUserSeekable = true;
    private int mKeyProgressIncrement = 1;
    private int mScaledTouchSlop;
    private Drawable mThumb;
    private int mThumbOffset;
    private float mTouchDownX;
    float mTouchProgressOffset;

    public AbsSeekBar(Context paramContext)
    {
        super(paramContext);
    }

    public AbsSeekBar(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public AbsSeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SeekBar, paramInt, 0);
        setThumb(localTypedArray1.getDrawable(0));
        setThumbOffset(localTypedArray1.getDimensionPixelOffset(1, getThumbOffset()));
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Theme, 0, 0);
        this.mDisabledAlpha = localTypedArray2.getFloat(3, 0.5F);
        localTypedArray2.recycle();
        this.mScaledTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    }

    private void attemptClaimDrag()
    {
        if (this.mParent != null)
            this.mParent.requestDisallowInterceptTouchEvent(true);
    }

    private void setThumbPos(int paramInt1, Drawable paramDrawable, float paramFloat, int paramInt2)
    {
        int i = paramInt1 - this.mPaddingLeft - this.mPaddingRight;
        int j = paramDrawable.getIntrinsicWidth();
        int k = paramDrawable.getIntrinsicHeight();
        int m = (int)(paramFloat * (i - j + 2 * this.mThumbOffset));
        Rect localRect;
        int n;
        if (paramInt2 == -2147483648)
        {
            localRect = paramDrawable.getBounds();
            n = localRect.top;
        }
        for (int i1 = localRect.bottom; ; i1 = paramInt2 + k)
        {
            paramDrawable.setBounds(m, n, m + j, i1);
            return;
            n = paramInt2;
        }
    }

    private void trackTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = getWidth();
        int j = i - this.mPaddingLeft - this.mPaddingRight;
        int k = (int)paramMotionEvent.getX();
        float f1 = 0.0F;
        float f2;
        if (k < this.mPaddingLeft)
            f2 = 0.0F;
        while (true)
        {
            setProgress((int)(f1 + f2 * getMax()), true);
            return;
            if (k > i - this.mPaddingRight)
            {
                f2 = 1.0F;
            }
            else
            {
                f2 = (k - this.mPaddingLeft) / j;
                f1 = this.mTouchProgressOffset;
            }
        }
    }

    private void updateThumbPos(int paramInt1, int paramInt2)
    {
        Drawable localDrawable1 = getCurrentDrawable();
        Drawable localDrawable2 = this.mThumb;
        int i;
        int j;
        float f;
        if (localDrawable2 == null)
        {
            i = 0;
            j = Math.min(this.mMaxHeight, paramInt2 - this.mPaddingTop - this.mPaddingBottom);
            int k = getMax();
            if (k <= 0)
                break label139;
            f = getProgress() / k;
            label61: if (i <= j)
                break label145;
            if (localDrawable2 != null)
                setThumbPos(paramInt1, localDrawable2, f, 0);
            int n = (i - j) / 2;
            if (localDrawable1 != null)
                localDrawable1.setBounds(0, n, paramInt1 - this.mPaddingRight - this.mPaddingLeft, paramInt2 - this.mPaddingBottom - n - this.mPaddingTop);
        }
        while (true)
        {
            return;
            i = localDrawable2.getIntrinsicHeight();
            break;
            label139: f = 0.0F;
            break label61;
            label145: if (localDrawable1 != null)
                localDrawable1.setBounds(0, 0, paramInt1 - this.mPaddingRight - this.mPaddingLeft, paramInt2 - this.mPaddingBottom - this.mPaddingTop);
            int m = (j - i) / 2;
            if (localDrawable2 != null)
                setThumbPos(paramInt1, localDrawable2, f, m);
        }
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        Drawable localDrawable = getProgressDrawable();
        if (localDrawable != null)
            if (!isEnabled())
                break label63;
        label63: for (int i = 255; ; i = (int)(255.0F * this.mDisabledAlpha))
        {
            localDrawable.setAlpha(i);
            if ((this.mThumb != null) && (this.mThumb.isStateful()))
            {
                int[] arrayOfInt = getDrawableState();
                this.mThumb.setState(arrayOfInt);
            }
            return;
        }
    }

    public int getKeyProgressIncrement()
    {
        return this.mKeyProgressIncrement;
    }

    public Drawable getThumb()
    {
        return this.mThumb;
    }

    public int getThumbOffset()
    {
        return this.mThumbOffset;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mThumb != null)
            this.mThumb.jumpToCurrentState();
    }

    /** @deprecated */
    protected void onDraw(Canvas paramCanvas)
    {
        try
        {
            super.onDraw(paramCanvas);
            if (this.mThumb != null)
            {
                paramCanvas.save();
                paramCanvas.translate(this.mPaddingLeft - this.mThumbOffset, this.mPaddingTop);
                this.mThumb.draw(paramCanvas);
                paramCanvas.restore();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(AbsSeekBar.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(AbsSeekBar.class.getName());
        if (isEnabled())
        {
            int i = getProgress();
            if (i > 0)
                paramAccessibilityNodeInfo.addAction(8192);
            if (i < getMax())
                paramAccessibilityNodeInfo.addAction(4096);
        }
    }

    void onKeyChange()
    {
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        int i;
        if (isEnabled())
            i = getProgress();
        switch (paramInt)
        {
        default:
            bool = super.onKeyDown(paramInt, paramKeyEvent);
        case 21:
        case 22:
        }
        while (true)
        {
            return bool;
            if (i <= 0)
                break;
            setProgress(i - this.mKeyProgressIncrement, bool);
            onKeyChange();
            continue;
            if (i >= getMax())
                break;
            setProgress(i + this.mKeyProgressIncrement, bool);
            onKeyChange();
        }
    }

    /** @deprecated */
    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = 0;
        try
        {
            Drawable localDrawable = getCurrentDrawable();
            if (this.mThumb == null);
            while (true)
            {
                int k = 0;
                int m = 0;
                if (localDrawable != null)
                {
                    k = Math.max(this.mMinWidth, Math.min(this.mMaxWidth, localDrawable.getIntrinsicWidth()));
                    m = Math.max(i, Math.max(this.mMinHeight, Math.min(this.mMaxHeight, localDrawable.getIntrinsicHeight())));
                }
                int n = k + (this.mPaddingLeft + this.mPaddingRight);
                int i1 = m + (this.mPaddingTop + this.mPaddingBottom);
                setMeasuredDimension(resolveSizeAndState(n, paramInt1, 0), resolveSizeAndState(i1, paramInt2, 0));
                return;
                int j = this.mThumb.getIntrinsicHeight();
                i = j;
            }
        }
        finally
        {
        }
    }

    void onProgressRefresh(float paramFloat, boolean paramBoolean)
    {
        super.onProgressRefresh(paramFloat, paramBoolean);
        Drawable localDrawable = this.mThumb;
        if (localDrawable != null)
        {
            setThumbPos(getWidth(), localDrawable, paramFloat, -2147483648);
            invalidate();
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        updateThumbPos(paramInt1, paramInt2);
    }

    void onStartTrackingTouch()
    {
        this.mIsDragging = true;
    }

    void onStopTrackingTouch()
    {
        this.mIsDragging = false;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        if ((!this.mIsUserSeekable) || (!isEnabled()))
            bool = false;
        while (true)
        {
            return bool;
            switch (paramMotionEvent.getAction())
            {
            default:
                break;
            case 0:
                if (isInScrollingContainer())
                {
                    this.mTouchDownX = paramMotionEvent.getX();
                }
                else
                {
                    setPressed(bool);
                    if (this.mThumb != null)
                        invalidate(this.mThumb.getBounds());
                    onStartTrackingTouch();
                    trackTouchEvent(paramMotionEvent);
                    attemptClaimDrag();
                }
                break;
            case 2:
                if (this.mIsDragging)
                {
                    trackTouchEvent(paramMotionEvent);
                }
                else if (Math.abs(paramMotionEvent.getX() - this.mTouchDownX) > this.mScaledTouchSlop)
                {
                    setPressed(bool);
                    if (this.mThumb != null)
                        invalidate(this.mThumb.getBounds());
                    onStartTrackingTouch();
                    trackTouchEvent(paramMotionEvent);
                    attemptClaimDrag();
                }
                break;
            case 1:
                if (this.mIsDragging)
                {
                    trackTouchEvent(paramMotionEvent);
                    onStopTrackingTouch();
                    setPressed(false);
                }
                while (true)
                {
                    invalidate();
                    break;
                    onStartTrackingTouch();
                    trackTouchEvent(paramMotionEvent);
                    onStopTrackingTouch();
                }
            case 3:
                if (this.mIsDragging)
                {
                    onStopTrackingTouch();
                    setPressed(false);
                }
                invalidate();
            }
        }
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        int i = 1;
        if (super.performAccessibilityAction(paramInt, paramBundle));
        while (true)
        {
            return i;
            if (!isEnabled())
            {
                i = 0;
            }
            else
            {
                int j = getProgress();
                int k = Math.max(i, Math.round(getMax() / 5.0F));
                boolean bool;
                switch (paramInt)
                {
                default:
                    bool = false;
                    break;
                case 8192:
                    if (j <= 0)
                    {
                        bool = false;
                    }
                    else
                    {
                        setProgress(j - k, bool);
                        onKeyChange();
                    }
                    break;
                case 4096:
                    if (j >= getMax())
                    {
                        bool = false;
                    }
                    else
                    {
                        setProgress(j + k, bool);
                        onKeyChange();
                    }
                    break;
                }
            }
        }
    }

    public void setKeyProgressIncrement(int paramInt)
    {
        if (paramInt < 0)
            paramInt = -paramInt;
        this.mKeyProgressIncrement = paramInt;
    }

    /** @deprecated */
    public void setMax(int paramInt)
    {
        try
        {
            super.setMax(paramInt);
            if ((this.mKeyProgressIncrement == 0) || (getMax() / this.mKeyProgressIncrement > 20))
                setKeyProgressIncrement(Math.max(1, Math.round(getMax() / 20.0F)));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setThumb(Drawable paramDrawable)
    {
        if ((this.mThumb != null) && (paramDrawable != this.mThumb))
            this.mThumb.setCallback(null);
        for (int i = 1; ; i = 0)
        {
            if (paramDrawable != null)
            {
                paramDrawable.setCallback(this);
                this.mThumbOffset = (paramDrawable.getIntrinsicWidth() / 2);
                if ((i != 0) && ((paramDrawable.getIntrinsicWidth() != this.mThumb.getIntrinsicWidth()) || (paramDrawable.getIntrinsicHeight() != this.mThumb.getIntrinsicHeight())))
                    requestLayout();
            }
            this.mThumb = paramDrawable;
            invalidate();
            if (i != 0)
            {
                updateThumbPos(getWidth(), getHeight());
                if ((paramDrawable != null) && (paramDrawable.isStateful()))
                    paramDrawable.setState(getDrawableState());
            }
            return;
        }
    }

    public void setThumbOffset(int paramInt)
    {
        this.mThumbOffset = paramInt;
        invalidate();
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((paramDrawable == this.mThumb) || (super.verifyDrawable(paramDrawable)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AbsSeekBar
 * JD-Core Version:        0.6.2
 */