package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AlphaAnimation;

public class ZoomControls extends LinearLayout
{
    private final ZoomButton mZoomIn;
    private final ZoomButton mZoomOut;

    public ZoomControls(Context paramContext)
    {
        this(paramContext, null);
    }

    public ZoomControls(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        setFocusable(false);
        ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(17367245, this, true);
        this.mZoomIn = ((ZoomButton)findViewById(16909167));
        this.mZoomOut = ((ZoomButton)findViewById(16909166));
    }

    private void fade(int paramInt, float paramFloat1, float paramFloat2)
    {
        AlphaAnimation localAlphaAnimation = new AlphaAnimation(paramFloat1, paramFloat2);
        localAlphaAnimation.setDuration(500L);
        startAnimation(localAlphaAnimation);
        setVisibility(paramInt);
    }

    public boolean hasFocus()
    {
        if ((this.mZoomIn.hasFocus()) || (this.mZoomOut.hasFocus()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void hide()
    {
        fade(8, 1.0F, 0.0F);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ZoomControls.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(ZoomControls.class.getName());
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        return true;
    }

    public void setIsZoomInEnabled(boolean paramBoolean)
    {
        this.mZoomIn.setEnabled(paramBoolean);
    }

    public void setIsZoomOutEnabled(boolean paramBoolean)
    {
        this.mZoomOut.setEnabled(paramBoolean);
    }

    public void setOnZoomInClickListener(View.OnClickListener paramOnClickListener)
    {
        this.mZoomIn.setOnClickListener(paramOnClickListener);
    }

    public void setOnZoomOutClickListener(View.OnClickListener paramOnClickListener)
    {
        this.mZoomOut.setOnClickListener(paramOnClickListener);
    }

    public void setZoomSpeed(long paramLong)
    {
        this.mZoomIn.setZoomSpeed(paramLong);
        this.mZoomOut.setZoomSpeed(paramLong);
    }

    public void show()
    {
        fade(0, 0.0F, 1.0F);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ZoomControls
 * JD-Core Version:        0.6.2
 */