package android.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

@RemoteViews.RemoteView
public class AdapterViewFlipper extends AdapterViewAnimator
{
    private static final int DEFAULT_INTERVAL = 10000;
    private static final boolean LOGD = false;
    private static final String TAG = "ViewFlipper";
    private final int FLIP_MSG = 1;
    private boolean mAdvancedByHost = false;
    private boolean mAutoStart = false;
    private int mFlipInterval = 10000;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            if ((paramAnonymousMessage.what == 1) && (AdapterViewFlipper.this.mRunning))
                AdapterViewFlipper.this.showNext();
        }
    };
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if ("android.intent.action.SCREEN_OFF".equals(str))
            {
                AdapterViewFlipper.access$002(AdapterViewFlipper.this, false);
                AdapterViewFlipper.this.updateRunning();
            }
            while (true)
            {
                return;
                if ("android.intent.action.USER_PRESENT".equals(str))
                {
                    AdapterViewFlipper.access$002(AdapterViewFlipper.this, true);
                    AdapterViewFlipper.this.updateRunning(false);
                }
            }
        }
    };
    private boolean mRunning = false;
    private boolean mStarted = false;
    private boolean mUserPresent = true;
    private boolean mVisible = false;

    public AdapterViewFlipper(Context paramContext)
    {
        super(paramContext);
    }

    public AdapterViewFlipper(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AdapterViewFlipper);
        this.mFlipInterval = localTypedArray.getInt(0, 10000);
        this.mAutoStart = localTypedArray.getBoolean(1, false);
        this.mLoopViews = true;
        localTypedArray.recycle();
    }

    private void updateRunning()
    {
        updateRunning(true);
    }

    private void updateRunning(boolean paramBoolean)
    {
        boolean bool;
        if ((!this.mAdvancedByHost) && (this.mVisible) && (this.mStarted) && (this.mUserPresent) && (this.mAdapter != null))
        {
            bool = true;
            if (bool != this.mRunning)
            {
                if (!bool)
                    break label92;
                showOnly(this.mWhichChild, paramBoolean);
                Message localMessage = this.mHandler.obtainMessage(1);
                this.mHandler.sendMessageDelayed(localMessage, this.mFlipInterval);
            }
        }
        while (true)
        {
            this.mRunning = bool;
            return;
            bool = false;
            break;
            label92: this.mHandler.removeMessages(1);
        }
    }

    public void fyiWillBeAdvancedByHostKThx()
    {
        this.mAdvancedByHost = true;
        updateRunning(false);
    }

    public int getFlipInterval()
    {
        return this.mFlipInterval;
    }

    public boolean isAutoStart()
    {
        return this.mAutoStart;
    }

    public boolean isFlipping()
    {
        return this.mStarted;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
        localIntentFilter.addAction("android.intent.action.USER_PRESENT");
        getContext().registerReceiver(this.mReceiver, localIntentFilter);
        if (this.mAutoStart)
            startFlipping();
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.mVisible = false;
        getContext().unregisterReceiver(this.mReceiver);
        updateRunning();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(AdapterViewFlipper.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(AdapterViewFlipper.class.getName());
    }

    protected void onWindowVisibilityChanged(int paramInt)
    {
        super.onWindowVisibilityChanged(paramInt);
        if (paramInt == 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mVisible = bool;
            updateRunning(false);
            return;
        }
    }

    public void setAdapter(Adapter paramAdapter)
    {
        super.setAdapter(paramAdapter);
        updateRunning();
    }

    public void setAutoStart(boolean paramBoolean)
    {
        this.mAutoStart = paramBoolean;
    }

    public void setFlipInterval(int paramInt)
    {
        this.mFlipInterval = paramInt;
    }

    @RemotableViewMethod
    public void showNext()
    {
        if (this.mRunning)
        {
            this.mHandler.removeMessages(1);
            Message localMessage = this.mHandler.obtainMessage(1);
            this.mHandler.sendMessageDelayed(localMessage, this.mFlipInterval);
        }
        super.showNext();
    }

    @RemotableViewMethod
    public void showPrevious()
    {
        if (this.mRunning)
        {
            this.mHandler.removeMessages(1);
            Message localMessage = this.mHandler.obtainMessage(1);
            this.mHandler.sendMessageDelayed(localMessage, this.mFlipInterval);
        }
        super.showPrevious();
    }

    public void startFlipping()
    {
        this.mStarted = true;
        updateRunning();
    }

    public void stopFlipping()
    {
        this.mStarted = false;
        updateRunning();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AdapterViewFlipper
 * JD-Core Version:        0.6.2
 */