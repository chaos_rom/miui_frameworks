package android.widget;

public abstract interface ListAdapter extends Adapter
{
    public abstract boolean areAllItemsEnabled();

    public abstract boolean isEnabled(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.ListAdapter
 * JD-Core Version:        0.6.2
 */