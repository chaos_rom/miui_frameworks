package android.widget;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.AllCaps;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.method.DialerKeyListener;
import android.text.method.KeyListener;
import android.text.method.TextKeyListener;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class DialerFilter extends RelativeLayout
{
    public static final int DIGITS_AND_LETTERS = 1;
    public static final int DIGITS_AND_LETTERS_NO_DIGITS = 2;
    public static final int DIGITS_AND_LETTERS_NO_LETTERS = 3;
    public static final int DIGITS_ONLY = 4;
    public static final int LETTERS_ONLY = 5;
    EditText mDigits;
    EditText mHint;
    ImageView mIcon;
    InputFilter[] mInputFilters;
    private boolean mIsQwerty;
    EditText mLetters;
    int mMode;
    EditText mPrimary;

    public DialerFilter(Context paramContext)
    {
        super(paramContext);
    }

    public DialerFilter(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    private void makeDigitsPrimary()
    {
        if (this.mPrimary == this.mLetters)
            swapPrimaryAndHint(false);
    }

    private void makeLettersPrimary()
    {
        if (this.mPrimary == this.mDigits)
            swapPrimaryAndHint(true);
    }

    private void swapPrimaryAndHint(boolean paramBoolean)
    {
        Editable localEditable1 = this.mLetters.getText();
        Editable localEditable2 = this.mDigits.getText();
        KeyListener localKeyListener1 = this.mLetters.getKeyListener();
        KeyListener localKeyListener2 = this.mDigits.getKeyListener();
        if (paramBoolean)
            this.mLetters = this.mPrimary;
        for (this.mDigits = this.mHint; ; this.mDigits = this.mPrimary)
        {
            this.mLetters.setKeyListener(localKeyListener1);
            this.mLetters.setText(localEditable1);
            Editable localEditable3 = this.mLetters.getText();
            Selection.setSelection(localEditable3, localEditable3.length());
            this.mDigits.setKeyListener(localKeyListener2);
            this.mDigits.setText(localEditable2);
            Editable localEditable4 = this.mDigits.getText();
            Selection.setSelection(localEditable4, localEditable4.length());
            this.mPrimary.setFilters(this.mInputFilters);
            this.mHint.setFilters(this.mInputFilters);
            return;
            this.mLetters = this.mHint;
        }
    }

    public void append(String paramString)
    {
        switch (this.mMode)
        {
        default:
        case 1:
        case 3:
        case 4:
        case 2:
        case 5:
        }
        while (true)
        {
            return;
            this.mDigits.getText().append(paramString);
            this.mLetters.getText().append(paramString);
            continue;
            this.mDigits.getText().append(paramString);
            continue;
            this.mLetters.getText().append(paramString);
        }
    }

    public void clearText()
    {
        this.mLetters.getText().clear();
        this.mDigits.getText().clear();
        if (this.mIsQwerty)
            setMode(1);
        while (true)
        {
            return;
            setMode(4);
        }
    }

    public CharSequence getDigits()
    {
        if (this.mDigits.getVisibility() == 0);
        for (Object localObject = this.mDigits.getText(); ; localObject = "")
            return localObject;
    }

    public CharSequence getFilterText()
    {
        if (this.mMode != 4);
        for (CharSequence localCharSequence = getLetters(); ; localCharSequence = getDigits())
            return localCharSequence;
    }

    public CharSequence getLetters()
    {
        if (this.mLetters.getVisibility() == 0);
        for (Object localObject = this.mLetters.getText(); ; localObject = "")
            return localObject;
    }

    public int getMode()
    {
        return this.mMode;
    }

    public boolean isQwertyKeyboard()
    {
        return this.mIsQwerty;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        InputFilter[] arrayOfInputFilter = new InputFilter[1];
        arrayOfInputFilter[0] = new InputFilter.AllCaps();
        this.mInputFilters = arrayOfInputFilter;
        this.mHint = ((EditText)findViewById(16908293));
        if (this.mHint == null)
            throw new IllegalStateException("DialerFilter must have a child EditText named hint");
        this.mHint.setFilters(this.mInputFilters);
        this.mLetters = this.mHint;
        this.mLetters.setKeyListener(TextKeyListener.getInstance());
        this.mLetters.setMovementMethod(null);
        this.mLetters.setFocusable(false);
        this.mPrimary = ((EditText)findViewById(16908300));
        if (this.mPrimary == null)
            throw new IllegalStateException("DialerFilter must have a child EditText named primary");
        this.mPrimary.setFilters(this.mInputFilters);
        this.mDigits = this.mPrimary;
        this.mDigits.setKeyListener(DialerKeyListener.getInstance());
        this.mDigits.setMovementMethod(null);
        this.mDigits.setFocusable(false);
        this.mIcon = ((ImageView)findViewById(16908294));
        setFocusable(true);
        this.mIsQwerty = true;
        setMode(1);
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
        ImageView localImageView;
        if (this.mIcon != null)
        {
            localImageView = this.mIcon;
            if (!paramBoolean)
                break label35;
        }
        label35: for (int i = 0; ; i = 8)
        {
            localImageView.setVisibility(i);
            return;
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        boolean bool = false;
        switch (paramInt)
        {
        default:
            switch (this.mMode)
            {
            default:
            case 1:
            case 3:
            case 4:
            case 2:
            case 5:
            }
            break;
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        case 66:
        case 67:
        }
        while (true)
        {
            if (!bool)
                i = super.onKeyDown(paramInt, paramKeyEvent);
            return i;
            switch (this.mMode)
            {
            default:
                break;
            case 1:
                bool = this.mDigits.onKeyDown(paramInt, paramKeyEvent) & this.mLetters.onKeyDown(paramInt, paramKeyEvent);
                break;
            case 2:
                bool = this.mLetters.onKeyDown(paramInt, paramKeyEvent);
                if (this.mLetters.getText().length() == this.mDigits.getText().length())
                    setMode(i);
                break;
            case 3:
                if (this.mDigits.getText().length() == this.mLetters.getText().length())
                {
                    this.mLetters.onKeyDown(paramInt, paramKeyEvent);
                    setMode(i);
                }
                bool = this.mDigits.onKeyDown(paramInt, paramKeyEvent);
                break;
            case 4:
                bool = this.mDigits.onKeyDown(paramInt, paramKeyEvent);
                break;
            case 5:
                bool = this.mLetters.onKeyDown(paramInt, paramKeyEvent);
                continue;
                bool = this.mLetters.onKeyDown(paramInt, paramKeyEvent);
                if (KeyEvent.isModifierKey(paramInt))
                {
                    this.mDigits.onKeyDown(paramInt, paramKeyEvent);
                    bool = true;
                }
                else if ((paramKeyEvent.isPrintingKey()) || (paramInt == 62) || (paramInt == 61))
                {
                    if (paramKeyEvent.getMatch(DialerKeyListener.CHARACTERS) != 0)
                    {
                        bool &= this.mDigits.onKeyDown(paramInt, paramKeyEvent);
                    }
                    else
                    {
                        setMode(2);
                        continue;
                        bool = this.mDigits.onKeyDown(paramInt, paramKeyEvent);
                        continue;
                        bool = this.mLetters.onKeyDown(paramInt, paramKeyEvent);
                    }
                }
                break;
            }
        }
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool1 = this.mLetters.onKeyUp(paramInt, paramKeyEvent);
        boolean bool2 = this.mDigits.onKeyUp(paramInt, paramKeyEvent);
        if ((bool1) || (bool2));
        for (boolean bool3 = true; ; bool3 = false)
            return bool3;
    }

    protected void onModeChange(int paramInt1, int paramInt2)
    {
    }

    public void removeFilterWatcher(TextWatcher paramTextWatcher)
    {
        if (this.mMode != 4);
        for (Editable localEditable = this.mLetters.getText(); ; localEditable = this.mDigits.getText())
        {
            localEditable.removeSpan(paramTextWatcher);
            return;
        }
    }

    public void setDigitsWatcher(TextWatcher paramTextWatcher)
    {
        Editable localEditable = this.mDigits.getText();
        ((Spannable)localEditable).setSpan(paramTextWatcher, 0, localEditable.length(), 18);
    }

    public void setFilterWatcher(TextWatcher paramTextWatcher)
    {
        if (this.mMode != 4)
            setLettersWatcher(paramTextWatcher);
        while (true)
        {
            return;
            setDigitsWatcher(paramTextWatcher);
        }
    }

    public void setLettersWatcher(TextWatcher paramTextWatcher)
    {
        Editable localEditable = this.mLetters.getText();
        ((Spannable)localEditable).setSpan(paramTextWatcher, 0, localEditable.length(), 18);
    }

    public void setMode(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 1:
        case 4:
        case 5:
        case 3:
        case 2:
        }
        while (true)
        {
            int i = this.mMode;
            this.mMode = paramInt;
            onModeChange(i, paramInt);
            return;
            makeDigitsPrimary();
            this.mLetters.setVisibility(0);
            this.mDigits.setVisibility(0);
            continue;
            makeDigitsPrimary();
            this.mLetters.setVisibility(8);
            this.mDigits.setVisibility(0);
            continue;
            makeLettersPrimary();
            this.mLetters.setVisibility(0);
            this.mDigits.setVisibility(8);
            continue;
            makeDigitsPrimary();
            this.mLetters.setVisibility(4);
            this.mDigits.setVisibility(0);
            continue;
            makeLettersPrimary();
            this.mLetters.setVisibility(0);
            this.mDigits.setVisibility(4);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.DialerFilter
 * JD-Core Version:        0.6.2
 */