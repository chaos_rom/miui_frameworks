package android.widget;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;

public abstract class SimpleCursorTreeAdapter extends ResourceCursorTreeAdapter
{
    private int[] mChildFrom;
    private String[] mChildFromNames;
    private int[] mChildTo;
    private int[] mGroupFrom;
    private String[] mGroupFromNames;
    private int[] mGroupTo;
    private ViewBinder mViewBinder;

    public SimpleCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2, String[] paramArrayOfString1, int[] paramArrayOfInt1, int paramInt3, int paramInt4, String[] paramArrayOfString2, int[] paramArrayOfInt2)
    {
        super(paramContext, paramCursor, paramInt1, paramInt2, paramInt3, paramInt4);
        init(paramArrayOfString1, paramArrayOfInt1, paramArrayOfString2, paramArrayOfInt2);
    }

    public SimpleCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2, String[] paramArrayOfString1, int[] paramArrayOfInt1, int paramInt3, String[] paramArrayOfString2, int[] paramArrayOfInt2)
    {
        super(paramContext, paramCursor, paramInt1, paramInt2, paramInt3);
        init(paramArrayOfString1, paramArrayOfInt1, paramArrayOfString2, paramArrayOfInt2);
    }

    public SimpleCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, String[] paramArrayOfString1, int[] paramArrayOfInt1, int paramInt2, String[] paramArrayOfString2, int[] paramArrayOfInt2)
    {
        super(paramContext, paramCursor, paramInt1, paramInt2);
        init(paramArrayOfString1, paramArrayOfInt1, paramArrayOfString2, paramArrayOfInt2);
    }

    private void bindView(View paramView, Context paramContext, Cursor paramCursor, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        ViewBinder localViewBinder = this.mViewBinder;
        int i = 0;
        if (i < paramArrayOfInt2.length)
        {
            View localView = paramView.findViewById(paramArrayOfInt2[i]);
            String str;
            if (localView != null)
            {
                boolean bool = false;
                if (localViewBinder != null)
                    bool = localViewBinder.setViewValue(localView, paramCursor, paramArrayOfInt1[i]);
                if (!bool)
                {
                    str = paramCursor.getString(paramArrayOfInt1[i]);
                    if (str == null)
                        str = "";
                    if (!(localView instanceof TextView))
                        break label110;
                    setViewText((TextView)localView, str);
                }
            }
            while (true)
            {
                i++;
                break;
                label110: if (!(localView instanceof ImageView))
                    break label132;
                setViewImage((ImageView)localView, str);
            }
            label132: throw new IllegalStateException("SimpleCursorTreeAdapter can bind values only to TextView and ImageView!");
        }
    }

    private void init(String[] paramArrayOfString1, int[] paramArrayOfInt1, String[] paramArrayOfString2, int[] paramArrayOfInt2)
    {
        this.mGroupFromNames = paramArrayOfString1;
        this.mGroupTo = paramArrayOfInt1;
        this.mChildFromNames = paramArrayOfString2;
        this.mChildTo = paramArrayOfInt2;
    }

    private void initFromColumns(Cursor paramCursor, String[] paramArrayOfString, int[] paramArrayOfInt)
    {
        for (int i = -1 + paramArrayOfString.length; i >= 0; i--)
            paramArrayOfInt[i] = paramCursor.getColumnIndexOrThrow(paramArrayOfString[i]);
    }

    protected void bindChildView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean)
    {
        if (this.mChildFrom == null)
        {
            this.mChildFrom = new int[this.mChildFromNames.length];
            initFromColumns(paramCursor, this.mChildFromNames, this.mChildFrom);
        }
        bindView(paramView, paramContext, paramCursor, this.mChildFrom, this.mChildTo);
    }

    protected void bindGroupView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean)
    {
        if (this.mGroupFrom == null)
        {
            this.mGroupFrom = new int[this.mGroupFromNames.length];
            initFromColumns(paramCursor, this.mGroupFromNames, this.mGroupFrom);
        }
        bindView(paramView, paramContext, paramCursor, this.mGroupFrom, this.mGroupTo);
    }

    public ViewBinder getViewBinder()
    {
        return this.mViewBinder;
    }

    public void setViewBinder(ViewBinder paramViewBinder)
    {
        this.mViewBinder = paramViewBinder;
    }

    protected void setViewImage(ImageView paramImageView, String paramString)
    {
        try
        {
            paramImageView.setImageResource(Integer.parseInt(paramString));
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                paramImageView.setImageURI(Uri.parse(paramString));
        }
    }

    public void setViewText(TextView paramTextView, String paramString)
    {
        paramTextView.setText(paramString);
    }

    public static abstract interface ViewBinder
    {
        public abstract boolean setViewValue(View paramView, Cursor paramCursor, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SimpleCursorTreeAdapter
 * JD-Core Version:        0.6.2
 */