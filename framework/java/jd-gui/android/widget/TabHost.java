package android.widget;

import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnTouchModeChangeListener;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.List;

public class TabHost extends FrameLayout
    implements ViewTreeObserver.OnTouchModeChangeListener
{
    protected int mCurrentTab = -1;
    private View mCurrentView = null;
    protected LocalActivityManager mLocalActivityManager = null;
    private OnTabChangeListener mOnTabChangeListener;
    private FrameLayout mTabContent;
    private View.OnKeyListener mTabKeyListener;
    private int mTabLayoutId;
    private List<TabSpec> mTabSpecs = new ArrayList(2);
    private TabWidget mTabWidget;

    public TabHost(Context paramContext)
    {
        super(paramContext);
        initTabHost();
    }

    public TabHost(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TabWidget, 16842883, 0);
        this.mTabLayoutId = localTypedArray.getResourceId(4, 0);
        localTypedArray.recycle();
        if (this.mTabLayoutId == 0)
            this.mTabLayoutId = 17367218;
        initTabHost();
    }

    private void initTabHost()
    {
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        this.mCurrentTab = -1;
        this.mCurrentView = null;
    }

    private void invokeOnTabChangeListener()
    {
        if (this.mOnTabChangeListener != null)
            this.mOnTabChangeListener.onTabChanged(getCurrentTabTag());
    }

    public void addTab(TabSpec paramTabSpec)
    {
        if (paramTabSpec.mIndicatorStrategy == null)
            throw new IllegalArgumentException("you must specify a way to create the tab indicator.");
        if (paramTabSpec.mContentStrategy == null)
            throw new IllegalArgumentException("you must specify a way to create the tab content");
        View localView = paramTabSpec.mIndicatorStrategy.createIndicatorView();
        localView.setOnKeyListener(this.mTabKeyListener);
        if ((paramTabSpec.mIndicatorStrategy instanceof ViewIndicatorStrategy))
            this.mTabWidget.setStripEnabled(false);
        this.mTabWidget.addView(localView);
        this.mTabSpecs.add(paramTabSpec);
        if (this.mCurrentTab == -1)
            setCurrentTab(0);
    }

    public void clearAllTabs()
    {
        this.mTabWidget.removeAllViews();
        initTabHost();
        this.mTabContent.removeAllViews();
        this.mTabSpecs.clear();
        requestLayout();
        invalidate();
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool = super.dispatchKeyEvent(paramKeyEvent);
        if ((!bool) && (paramKeyEvent.getAction() == 0) && (paramKeyEvent.getKeyCode() == 19) && (this.mCurrentView != null) && (this.mCurrentView.isRootNamespace()) && (this.mCurrentView.hasFocus()) && (this.mCurrentView.findFocus().focusSearch(33) == null))
        {
            this.mTabWidget.getChildTabViewAt(this.mCurrentTab).requestFocus();
            playSoundEffect(2);
            bool = true;
        }
        return bool;
    }

    public void dispatchWindowFocusChanged(boolean paramBoolean)
    {
        if (this.mCurrentView != null)
            this.mCurrentView.dispatchWindowFocusChanged(paramBoolean);
    }

    public int getCurrentTab()
    {
        return this.mCurrentTab;
    }

    public String getCurrentTabTag()
    {
        if ((this.mCurrentTab >= 0) && (this.mCurrentTab < this.mTabSpecs.size()));
        for (String str = ((TabSpec)this.mTabSpecs.get(this.mCurrentTab)).getTag(); ; str = null)
            return str;
    }

    public View getCurrentTabView()
    {
        if ((this.mCurrentTab >= 0) && (this.mCurrentTab < this.mTabSpecs.size()));
        for (View localView = this.mTabWidget.getChildTabViewAt(this.mCurrentTab); ; localView = null)
            return localView;
    }

    public View getCurrentView()
    {
        return this.mCurrentView;
    }

    public FrameLayout getTabContentView()
    {
        return this.mTabContent;
    }

    public TabWidget getTabWidget()
    {
        return this.mTabWidget;
    }

    public TabSpec newTabSpec(String paramString)
    {
        return new TabSpec(paramString, null);
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnTouchModeChangeListener(this);
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeOnTouchModeChangeListener(this);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TabHost.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TabHost.class.getName());
    }

    public void onTouchModeChanged(boolean paramBoolean)
    {
        if ((!paramBoolean) && (this.mCurrentView != null) && ((!this.mCurrentView.hasFocus()) || (this.mCurrentView.isFocused())))
            this.mTabWidget.getChildTabViewAt(this.mCurrentTab).requestFocus();
    }

    public void sendAccessibilityEvent(int paramInt)
    {
    }

    public void setCurrentTab(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mTabSpecs.size()));
        while (true)
        {
            return;
            if (paramInt != this.mCurrentTab)
            {
                if (this.mCurrentTab != -1)
                    ((TabSpec)this.mTabSpecs.get(this.mCurrentTab)).mContentStrategy.tabClosed();
                this.mCurrentTab = paramInt;
                TabSpec localTabSpec = (TabSpec)this.mTabSpecs.get(paramInt);
                this.mTabWidget.focusCurrentTab(this.mCurrentTab);
                this.mCurrentView = localTabSpec.mContentStrategy.getContentView();
                if (this.mCurrentView.getParent() == null)
                    this.mTabContent.addView(this.mCurrentView, new ViewGroup.LayoutParams(-1, -1));
                if (!this.mTabWidget.hasFocus())
                    this.mCurrentView.requestFocus();
                invokeOnTabChangeListener();
            }
        }
    }

    public void setCurrentTabByTag(String paramString)
    {
        for (int i = 0; ; i++)
            if (i < this.mTabSpecs.size())
            {
                if (((TabSpec)this.mTabSpecs.get(i)).getTag().equals(paramString))
                    setCurrentTab(i);
            }
            else
                return;
    }

    public void setOnTabChangedListener(OnTabChangeListener paramOnTabChangeListener)
    {
        this.mOnTabChangeListener = paramOnTabChangeListener;
    }

    public void setup()
    {
        this.mTabWidget = ((TabWidget)findViewById(16908307));
        if (this.mTabWidget == null)
            throw new RuntimeException("Your TabHost must have a TabWidget whose id attribute is 'android.R.id.tabs'");
        this.mTabKeyListener = new View.OnKeyListener()
        {
            public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
            {
                switch (paramAnonymousInt)
                {
                default:
                    TabHost.this.mTabContent.requestFocus(2);
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 66:
                }
                for (boolean bool = TabHost.this.mTabContent.dispatchKeyEvent(paramAnonymousKeyEvent); ; bool = false)
                    return bool;
            }
        };
        this.mTabWidget.setTabSelectionListener(new TabWidget.OnTabSelectionChanged()
        {
            public void onTabSelectionChanged(int paramAnonymousInt, boolean paramAnonymousBoolean)
            {
                TabHost.this.setCurrentTab(paramAnonymousInt);
                if (paramAnonymousBoolean)
                    TabHost.this.mTabContent.requestFocus(2);
            }
        });
        this.mTabContent = ((FrameLayout)findViewById(16908305));
        if (this.mTabContent == null)
            throw new RuntimeException("Your TabHost must have a FrameLayout whose id attribute is 'android.R.id.tabcontent'");
    }

    public void setup(LocalActivityManager paramLocalActivityManager)
    {
        setup();
        this.mLocalActivityManager = paramLocalActivityManager;
    }

    private class IntentContentStrategy
        implements TabHost.ContentStrategy
    {
        private final Intent mIntent;
        private View mLaunchedView;
        private final String mTag;

        private IntentContentStrategy(String paramIntent, Intent arg3)
        {
            this.mTag = paramIntent;
            Object localObject;
            this.mIntent = localObject;
        }

        public View getContentView()
        {
            if (TabHost.this.mLocalActivityManager == null)
                throw new IllegalStateException("Did you forget to call 'public void setup(LocalActivityManager activityGroup)'?");
            Window localWindow = TabHost.this.mLocalActivityManager.startActivity(this.mTag, this.mIntent);
            if (localWindow != null);
            for (View localView = localWindow.getDecorView(); ; localView = null)
            {
                if ((this.mLaunchedView != localView) && (this.mLaunchedView != null) && (this.mLaunchedView.getParent() != null))
                    TabHost.this.mTabContent.removeView(this.mLaunchedView);
                this.mLaunchedView = localView;
                if (this.mLaunchedView != null)
                {
                    this.mLaunchedView.setVisibility(0);
                    this.mLaunchedView.setFocusableInTouchMode(true);
                    ((ViewGroup)this.mLaunchedView).setDescendantFocusability(262144);
                }
                return this.mLaunchedView;
            }
        }

        public void tabClosed()
        {
            if (this.mLaunchedView != null)
                this.mLaunchedView.setVisibility(8);
        }
    }

    private class FactoryContentStrategy
        implements TabHost.ContentStrategy
    {
        private TabHost.TabContentFactory mFactory;
        private View mTabContent;
        private final CharSequence mTag;

        public FactoryContentStrategy(CharSequence paramTabContentFactory, TabHost.TabContentFactory arg3)
        {
            this.mTag = paramTabContentFactory;
            Object localObject;
            this.mFactory = localObject;
        }

        public View getContentView()
        {
            if (this.mTabContent == null)
                this.mTabContent = this.mFactory.createTabContent(this.mTag.toString());
            this.mTabContent.setVisibility(0);
            return this.mTabContent;
        }

        public void tabClosed()
        {
            this.mTabContent.setVisibility(8);
        }
    }

    private class ViewIdContentStrategy
        implements TabHost.ContentStrategy
    {
        private final View mView;

        private ViewIdContentStrategy(int arg2)
        {
            int i;
            this.mView = TabHost.this.mTabContent.findViewById(i);
            if (this.mView != null)
            {
                this.mView.setVisibility(8);
                return;
            }
            throw new RuntimeException("Could not create tab content because could not find view with id " + i);
        }

        public View getContentView()
        {
            this.mView.setVisibility(0);
            return this.mView;
        }

        public void tabClosed()
        {
            this.mView.setVisibility(8);
        }
    }

    private class ViewIndicatorStrategy
        implements TabHost.IndicatorStrategy
    {
        private final View mView;

        private ViewIndicatorStrategy(View arg2)
        {
            Object localObject;
            this.mView = localObject;
        }

        public View createIndicatorView()
        {
            return this.mView;
        }
    }

    private class LabelAndIconIndicatorStrategy
        implements TabHost.IndicatorStrategy
    {
        private final Drawable mIcon;
        private final CharSequence mLabel;

        private LabelAndIconIndicatorStrategy(CharSequence paramDrawable, Drawable arg3)
        {
            this.mLabel = paramDrawable;
            Object localObject;
            this.mIcon = localObject;
        }

        public View createIndicatorView()
        {
            int i = 1;
            Context localContext = TabHost.this.getContext();
            View localView = ((LayoutInflater)localContext.getSystemService("layout_inflater")).inflate(TabHost.this.mTabLayoutId, TabHost.this.mTabWidget, false);
            TextView localTextView = (TextView)localView.findViewById(16908310);
            ImageView localImageView = (ImageView)localView.findViewById(16908294);
            int j;
            if (localImageView.getVisibility() == 8)
            {
                j = i;
                if ((j != 0) && (!TextUtils.isEmpty(this.mLabel)))
                    break label162;
            }
            while (true)
            {
                localTextView.setText(this.mLabel);
                if ((i != 0) && (this.mIcon != null))
                {
                    localImageView.setImageDrawable(this.mIcon);
                    localImageView.setVisibility(0);
                }
                if (localContext.getApplicationInfo().targetSdkVersion <= 4)
                {
                    localView.setBackgroundResource(17302946);
                    localTextView.setTextColor(localContext.getResources().getColorStateList(17170556));
                }
                return localView;
                j = 0;
                break;
                label162: i = 0;
            }
        }
    }

    private class LabelIndicatorStrategy
        implements TabHost.IndicatorStrategy
    {
        private final CharSequence mLabel;

        private LabelIndicatorStrategy(CharSequence arg2)
        {
            Object localObject;
            this.mLabel = localObject;
        }

        public View createIndicatorView()
        {
            Context localContext = TabHost.this.getContext();
            View localView = ((LayoutInflater)localContext.getSystemService("layout_inflater")).inflate(TabHost.this.mTabLayoutId, TabHost.this.mTabWidget, false);
            TextView localTextView = (TextView)localView.findViewById(16908310);
            localTextView.setText(this.mLabel);
            if (localContext.getApplicationInfo().targetSdkVersion <= 4)
            {
                localView.setBackgroundResource(17302946);
                localTextView.setTextColor(localContext.getResources().getColorStateList(17170556));
            }
            return localView;
        }
    }

    private static abstract interface ContentStrategy
    {
        public abstract View getContentView();

        public abstract void tabClosed();
    }

    private static abstract interface IndicatorStrategy
    {
        public abstract View createIndicatorView();
    }

    public class TabSpec
    {
        private TabHost.ContentStrategy mContentStrategy;
        private TabHost.IndicatorStrategy mIndicatorStrategy;
        private String mTag;

        private TabSpec(String arg2)
        {
            Object localObject;
            this.mTag = localObject;
        }

        public String getTag()
        {
            return this.mTag;
        }

        public TabSpec setContent(int paramInt)
        {
            this.mContentStrategy = new TabHost.ViewIdContentStrategy(TabHost.this, paramInt, null);
            return this;
        }

        public TabSpec setContent(Intent paramIntent)
        {
            this.mContentStrategy = new TabHost.IntentContentStrategy(TabHost.this, this.mTag, paramIntent, null);
            return this;
        }

        public TabSpec setContent(TabHost.TabContentFactory paramTabContentFactory)
        {
            this.mContentStrategy = new TabHost.FactoryContentStrategy(TabHost.this, this.mTag, paramTabContentFactory);
            return this;
        }

        public TabSpec setIndicator(View paramView)
        {
            this.mIndicatorStrategy = new TabHost.ViewIndicatorStrategy(TabHost.this, paramView, null);
            return this;
        }

        public TabSpec setIndicator(CharSequence paramCharSequence)
        {
            this.mIndicatorStrategy = new TabHost.LabelIndicatorStrategy(TabHost.this, paramCharSequence, null);
            return this;
        }

        public TabSpec setIndicator(CharSequence paramCharSequence, Drawable paramDrawable)
        {
            this.mIndicatorStrategy = new TabHost.LabelAndIconIndicatorStrategy(TabHost.this, paramCharSequence, paramDrawable, null);
            return this;
        }
    }

    public static abstract interface TabContentFactory
    {
        public abstract View createTabContent(String paramString);
    }

    public static abstract interface OnTabChangeListener
    {
        public abstract void onTabChanged(String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TabHost
 * JD-Core Version:        0.6.2
 */