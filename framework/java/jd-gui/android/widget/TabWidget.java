package android.widget;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R.styleable;

public class TabWidget extends LinearLayout
    implements View.OnFocusChangeListener
{
    private final Rect mBounds = new Rect();
    private boolean mDrawBottomStrips = true;
    private int[] mImposedTabWidths;
    private int mImposedTabsHeight = -1;
    private Drawable mLeftStrip;
    private Drawable mRightStrip;
    private int mSelectedTab = -1;
    private OnTabSelectionChanged mSelectionChangedListener;
    private boolean mStripMoved;

    public TabWidget(Context paramContext)
    {
        this(paramContext, null);
    }

    public TabWidget(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842883);
    }

    public TabWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TabWidget, paramInt, 0);
        setStripEnabled(localTypedArray.getBoolean(3, true));
        setLeftStripDrawable(localTypedArray.getDrawable(1));
        setRightStripDrawable(localTypedArray.getDrawable(2));
        localTypedArray.recycle();
        initTabWidget();
    }

    private void initTabWidget()
    {
        setChildrenDrawingOrderEnabled(true);
        Context localContext = this.mContext;
        Resources localResources = localContext.getResources();
        if (localContext.getApplicationInfo().targetSdkVersion <= 4)
        {
            if (this.mLeftStrip == null)
                this.mLeftStrip = localResources.getDrawable(17302937);
            if (this.mRightStrip == null)
                this.mRightStrip = localResources.getDrawable(17302939);
        }
        while (true)
        {
            setFocusable(true);
            setOnFocusChangeListener(this);
            return;
            if (this.mLeftStrip == null)
                this.mLeftStrip = localResources.getDrawable(17302936);
            if (this.mRightStrip == null)
                this.mRightStrip = localResources.getDrawable(17302938);
        }
    }

    public void addView(View paramView)
    {
        if (paramView.getLayoutParams() == null)
        {
            LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(0, -1, 1.0F);
            localLayoutParams.setMargins(0, 0, 0, 0);
            paramView.setLayoutParams(localLayoutParams);
        }
        paramView.setFocusable(true);
        paramView.setClickable(true);
        super.addView(paramView);
        paramView.setOnClickListener(new TabClickListener(-1 + getTabCount(), null));
        paramView.setOnFocusChangeListener(this);
    }

    public void childDrawableStateChanged(View paramView)
    {
        if ((getTabCount() > 0) && (paramView == getChildTabViewAt(this.mSelectedTab)))
            invalidate();
        super.childDrawableStateChanged(paramView);
    }

    public void dispatchDraw(Canvas paramCanvas)
    {
        super.dispatchDraw(paramCanvas);
        if (getTabCount() == 0);
        while (true)
        {
            return;
            if (this.mDrawBottomStrips)
            {
                View localView = getChildTabViewAt(this.mSelectedTab);
                Drawable localDrawable1 = this.mLeftStrip;
                Drawable localDrawable2 = this.mRightStrip;
                localDrawable1.setState(localView.getDrawableState());
                localDrawable2.setState(localView.getDrawableState());
                if (this.mStripMoved)
                {
                    Rect localRect = this.mBounds;
                    localRect.left = localView.getLeft();
                    localRect.right = localView.getRight();
                    int i = getHeight();
                    localDrawable1.setBounds(Math.min(0, localRect.left - localDrawable1.getIntrinsicWidth()), i - localDrawable1.getIntrinsicHeight(), localRect.left, i);
                    localDrawable2.setBounds(localRect.right, i - localDrawable2.getIntrinsicHeight(), Math.max(getWidth(), localRect.right + localDrawable2.getIntrinsicWidth()), i);
                    this.mStripMoved = false;
                }
                localDrawable1.draw(paramCanvas);
                localDrawable2.draw(paramCanvas);
            }
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        onPopulateAccessibilityEvent(paramAccessibilityEvent);
        View localView;
        if (this.mSelectedTab != -1)
        {
            localView = getChildTabViewAt(this.mSelectedTab);
            if ((localView == null) || (localView.getVisibility() != 0));
        }
        for (boolean bool = localView.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent); ; bool = false)
            return bool;
    }

    public void focusCurrentTab(int paramInt)
    {
        int i = this.mSelectedTab;
        setCurrentTab(paramInt);
        if (i != paramInt)
            getChildTabViewAt(paramInt).requestFocus();
    }

    protected int getChildDrawingOrder(int paramInt1, int paramInt2)
    {
        if (this.mSelectedTab == -1);
        while (true)
        {
            return paramInt2;
            if (paramInt2 == paramInt1 - 1)
                paramInt2 = this.mSelectedTab;
            else if (paramInt2 >= this.mSelectedTab)
                paramInt2++;
        }
    }

    public View getChildTabViewAt(int paramInt)
    {
        return getChildAt(paramInt);
    }

    public int getTabCount()
    {
        return getChildCount();
    }

    public boolean isStripEnabled()
    {
        return this.mDrawBottomStrips;
    }

    void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if ((!isMeasureWithLargestChildEnabled()) && (this.mImposedTabsHeight >= 0))
        {
            paramInt2 = View.MeasureSpec.makeMeasureSpec(paramInt3 + this.mImposedTabWidths[paramInt1], 1073741824);
            paramInt4 = View.MeasureSpec.makeMeasureSpec(this.mImposedTabsHeight, 1073741824);
        }
        super.measureChildBeforeLayout(paramView, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    }

    void measureHorizontal(int paramInt1, int paramInt2)
    {
        if (View.MeasureSpec.getMode(paramInt1) == 0)
            super.measureHorizontal(paramInt1, paramInt2);
        while (true)
        {
            return;
            int i = View.MeasureSpec.makeMeasureSpec(0, 0);
            this.mImposedTabsHeight = -1;
            super.measureHorizontal(i, paramInt2);
            int j = getMeasuredWidth() - View.MeasureSpec.getSize(paramInt1);
            if (j > 0)
            {
                int k = getChildCount();
                int m = 0;
                int n = 0;
                if (n < k)
                {
                    if (getChildAt(n).getVisibility() == 8);
                    while (true)
                    {
                        n++;
                        break;
                        m++;
                    }
                }
                if (m > 0)
                {
                    if ((this.mImposedTabWidths == null) || (this.mImposedTabWidths.length != k))
                        this.mImposedTabWidths = new int[k];
                    int i1 = 0;
                    if (i1 < k)
                    {
                        View localView = getChildAt(i1);
                        if (localView.getVisibility() == 8);
                        while (true)
                        {
                            i1++;
                            break;
                            int i2 = localView.getMeasuredWidth();
                            int i3 = Math.max(0, i2 - j / m);
                            this.mImposedTabWidths[i1] = i3;
                            j -= i2 - i3;
                            m--;
                            this.mImposedTabsHeight = Math.max(this.mImposedTabsHeight, localView.getMeasuredHeight());
                        }
                    }
                }
            }
            super.measureHorizontal(paramInt1, paramInt2);
        }
    }

    public void onFocusChange(View paramView, boolean paramBoolean)
    {
        if ((paramView == this) && (paramBoolean) && (getTabCount() > 0))
            getChildTabViewAt(this.mSelectedTab).requestFocus();
        label92: 
        while (true)
        {
            return;
            int i;
            int j;
            if (paramBoolean)
            {
                i = 0;
                j = getTabCount();
            }
            while (true)
            {
                if (i >= j)
                    break label92;
                if (getChildTabViewAt(i) == paramView)
                {
                    setCurrentTab(i);
                    this.mSelectionChangedListener.onTabSelectionChanged(i, false);
                    if (!isShown())
                        break;
                    sendAccessibilityEvent(8);
                    break;
                    break;
                }
                i++;
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(TabWidget.class.getName());
        paramAccessibilityEvent.setItemCount(getTabCount());
        paramAccessibilityEvent.setCurrentItemIndex(this.mSelectedTab);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(TabWidget.class.getName());
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mStripMoved = true;
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void removeAllViews()
    {
        super.removeAllViews();
        this.mSelectedTab = -1;
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent)
    {
        if ((paramAccessibilityEvent.getEventType() == 8) && (isFocused()))
            paramAccessibilityEvent.recycle();
        while (true)
        {
            return;
            super.sendAccessibilityEventUnchecked(paramAccessibilityEvent);
        }
    }

    public void setCurrentTab(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= getTabCount()) || (paramInt == this.mSelectedTab));
        while (true)
        {
            return;
            if (this.mSelectedTab != -1)
                getChildTabViewAt(this.mSelectedTab).setSelected(false);
            this.mSelectedTab = paramInt;
            getChildTabViewAt(this.mSelectedTab).setSelected(true);
            this.mStripMoved = true;
            if (isShown())
                sendAccessibilityEvent(4);
        }
    }

    public void setDividerDrawable(int paramInt)
    {
        setDividerDrawable(getResources().getDrawable(paramInt));
    }

    public void setDividerDrawable(Drawable paramDrawable)
    {
        super.setDividerDrawable(paramDrawable);
    }

    public void setEnabled(boolean paramBoolean)
    {
        super.setEnabled(paramBoolean);
        int i = getTabCount();
        for (int j = 0; j < i; j++)
            getChildTabViewAt(j).setEnabled(paramBoolean);
    }

    public void setLeftStripDrawable(int paramInt)
    {
        setLeftStripDrawable(getResources().getDrawable(paramInt));
    }

    public void setLeftStripDrawable(Drawable paramDrawable)
    {
        this.mLeftStrip = paramDrawable;
        requestLayout();
        invalidate();
    }

    public void setRightStripDrawable(int paramInt)
    {
        setRightStripDrawable(getResources().getDrawable(paramInt));
    }

    public void setRightStripDrawable(Drawable paramDrawable)
    {
        this.mRightStrip = paramDrawable;
        requestLayout();
        invalidate();
    }

    public void setStripEnabled(boolean paramBoolean)
    {
        this.mDrawBottomStrips = paramBoolean;
        invalidate();
    }

    void setTabSelectionListener(OnTabSelectionChanged paramOnTabSelectionChanged)
    {
        this.mSelectionChangedListener = paramOnTabSelectionChanged;
    }

    static abstract interface OnTabSelectionChanged
    {
        public abstract void onTabSelectionChanged(int paramInt, boolean paramBoolean);
    }

    private class TabClickListener
        implements View.OnClickListener
    {
        private final int mTabIndex;

        private TabClickListener(int arg2)
        {
            int i;
            this.mTabIndex = i;
        }

        public void onClick(View paramView)
        {
            TabWidget.this.mSelectionChangedListener.onTabSelectionChanged(this.mTabIndex, true);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.TabWidget
 * JD-Core Version:        0.6.2
 */