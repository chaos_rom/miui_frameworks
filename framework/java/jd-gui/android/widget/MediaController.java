package android.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.policy.PolicyManager;
import java.util.Formatter;
import java.util.Locale;

public class MediaController extends FrameLayout
{
    private static final int FADE_OUT = 1;
    private static final int SHOW_PROGRESS = 2;
    private static final int sDefaultTimeout = 3000;
    private View mAnchor;
    private Context mContext;
    private TextView mCurrentTime;
    private View mDecor;
    private WindowManager.LayoutParams mDecorLayoutParams;
    private boolean mDragging;
    private TextView mEndTime;
    private ImageButton mFfwdButton;
    private View.OnClickListener mFfwdListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            int i = 15000 + MediaController.this.mPlayer.getCurrentPosition();
            MediaController.this.mPlayer.seekTo(i);
            MediaController.this.setProgress();
            MediaController.this.show(3000);
        }
    };
    StringBuilder mFormatBuilder;
    Formatter mFormatter;
    private boolean mFromXml;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                MediaController.this.hide();
                continue;
                int i = MediaController.this.setProgress();
                if ((!MediaController.this.mDragging) && (MediaController.this.mShowing) && (MediaController.this.mPlayer.isPlaying()))
                    sendMessageDelayed(obtainMessage(2), 1000 - i % 1000);
            }
        }
    };
    private View.OnLayoutChangeListener mLayoutChangeListener = new View.OnLayoutChangeListener()
    {
        public void onLayoutChange(View paramAnonymousView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4, int paramAnonymousInt5, int paramAnonymousInt6, int paramAnonymousInt7, int paramAnonymousInt8)
        {
            MediaController.this.updateFloatingWindowLayout();
            if (MediaController.this.mShowing)
                MediaController.this.mWindowManager.updateViewLayout(MediaController.this.mDecor, MediaController.this.mDecorLayoutParams);
        }
    };
    private boolean mListenersSet;
    private ImageButton mNextButton;
    private View.OnClickListener mNextListener;
    private ImageButton mPauseButton;
    private View.OnClickListener mPauseListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            MediaController.this.doPauseResume();
            MediaController.this.show(3000);
        }
    };
    private MediaPlayerControl mPlayer;
    private ImageButton mPrevButton;
    private View.OnClickListener mPrevListener;
    private ProgressBar mProgress;
    private ImageButton mRewButton;
    private View.OnClickListener mRewListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            int i = -5000 + MediaController.this.mPlayer.getCurrentPosition();
            MediaController.this.mPlayer.seekTo(i);
            MediaController.this.setProgress();
            MediaController.this.show(3000);
        }
    };
    private View mRoot;
    private SeekBar.OnSeekBarChangeListener mSeekListener = new SeekBar.OnSeekBarChangeListener()
    {
        public void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
        {
            if (!paramAnonymousBoolean);
            while (true)
            {
                return;
                long l = MediaController.this.mPlayer.getDuration() * paramAnonymousInt / 1000L;
                MediaController.this.mPlayer.seekTo((int)l);
                if (MediaController.this.mCurrentTime != null)
                    MediaController.this.mCurrentTime.setText(MediaController.this.stringForTime((int)l));
            }
        }

        public void onStartTrackingTouch(SeekBar paramAnonymousSeekBar)
        {
            MediaController.this.show(3600000);
            MediaController.access$602(MediaController.this, true);
            MediaController.this.mHandler.removeMessages(2);
        }

        public void onStopTrackingTouch(SeekBar paramAnonymousSeekBar)
        {
            MediaController.access$602(MediaController.this, false);
            MediaController.this.setProgress();
            MediaController.this.updatePausePlay();
            MediaController.this.show(3000);
            MediaController.this.mHandler.sendEmptyMessage(2);
        }
    };
    private boolean mShowing;
    private View.OnTouchListener mTouchListener = new View.OnTouchListener()
    {
        public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
        {
            if ((paramAnonymousMotionEvent.getAction() == 0) && (MediaController.this.mShowing))
                MediaController.this.hide();
            return false;
        }
    };
    private boolean mUseFastForward;
    private Window mWindow;
    private WindowManager mWindowManager;

    public MediaController(Context paramContext)
    {
        this(paramContext, true);
    }

    public MediaController(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        this.mRoot = this;
        this.mContext = paramContext;
        this.mUseFastForward = true;
        this.mFromXml = true;
    }

    public MediaController(Context paramContext, boolean paramBoolean)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mUseFastForward = paramBoolean;
        initFloatingWindowLayout();
        initFloatingWindow();
    }

    private void disableUnsupportedButtons()
    {
        try
        {
            if ((this.mPauseButton != null) && (!this.mPlayer.canPause()))
                this.mPauseButton.setEnabled(false);
            if ((this.mRewButton != null) && (!this.mPlayer.canSeekBackward()))
                this.mRewButton.setEnabled(false);
            if ((this.mFfwdButton != null) && (!this.mPlayer.canSeekForward()))
                this.mFfwdButton.setEnabled(false);
            label81: return;
        }
        catch (IncompatibleClassChangeError localIncompatibleClassChangeError)
        {
            break label81;
        }
    }

    private void doPauseResume()
    {
        if (this.mPlayer.isPlaying())
            this.mPlayer.pause();
        while (true)
        {
            updatePausePlay();
            return;
            this.mPlayer.start();
        }
    }

    private void initControllerView(View paramView)
    {
        int i = 0;
        this.mPauseButton = ((ImageButton)paramView.findViewById(16909014));
        if (this.mPauseButton != null)
        {
            this.mPauseButton.requestFocus();
            this.mPauseButton.setOnClickListener(this.mPauseListener);
        }
        this.mFfwdButton = ((ImageButton)paramView.findViewById(16909015));
        int j;
        if (this.mFfwdButton != null)
        {
            this.mFfwdButton.setOnClickListener(this.mFfwdListener);
            if (!this.mFromXml)
            {
                ImageButton localImageButton2 = this.mFfwdButton;
                if (!this.mUseFastForward)
                    break label359;
                j = 0;
                localImageButton2.setVisibility(j);
            }
        }
        this.mRewButton = ((ImageButton)paramView.findViewById(16909013));
        ImageButton localImageButton1;
        if (this.mRewButton != null)
        {
            this.mRewButton.setOnClickListener(this.mRewListener);
            if (!this.mFromXml)
            {
                localImageButton1 = this.mRewButton;
                if (!this.mUseFastForward)
                    break label366;
            }
        }
        while (true)
        {
            localImageButton1.setVisibility(i);
            this.mNextButton = ((ImageButton)paramView.findViewById(16909016));
            if ((this.mNextButton != null) && (!this.mFromXml) && (!this.mListenersSet))
                this.mNextButton.setVisibility(8);
            this.mPrevButton = ((ImageButton)paramView.findViewById(16909012));
            if ((this.mPrevButton != null) && (!this.mFromXml) && (!this.mListenersSet))
                this.mPrevButton.setVisibility(8);
            this.mProgress = ((ProgressBar)paramView.findViewById(16909018));
            if (this.mProgress != null)
            {
                if ((this.mProgress instanceof SeekBar))
                    ((SeekBar)this.mProgress).setOnSeekBarChangeListener(this.mSeekListener);
                this.mProgress.setMax(1000);
            }
            this.mEndTime = ((TextView)paramView.findViewById(16908388));
            this.mCurrentTime = ((TextView)paramView.findViewById(16909017));
            this.mFormatBuilder = new StringBuilder();
            this.mFormatter = new Formatter(this.mFormatBuilder, Locale.getDefault());
            installPrevNextListeners();
            return;
            label359: j = 8;
            break;
            label366: i = 8;
        }
    }

    private void initFloatingWindow()
    {
        this.mWindowManager = ((WindowManager)this.mContext.getSystemService("window"));
        this.mWindow = PolicyManager.makeNewWindow(this.mContext);
        this.mWindow.setWindowManager(this.mWindowManager, null, null);
        this.mWindow.requestFeature(1);
        this.mDecor = this.mWindow.getDecorView();
        this.mDecor.setOnTouchListener(this.mTouchListener);
        this.mWindow.setContentView(this);
        this.mWindow.setBackgroundDrawableResource(17170445);
        this.mWindow.setVolumeControlStream(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        requestFocus();
    }

    private void initFloatingWindowLayout()
    {
        this.mDecorLayoutParams = new WindowManager.LayoutParams();
        WindowManager.LayoutParams localLayoutParams = this.mDecorLayoutParams;
        localLayoutParams.gravity = 48;
        localLayoutParams.height = -2;
        localLayoutParams.x = 0;
        localLayoutParams.format = -3;
        localLayoutParams.type = 1000;
        localLayoutParams.flags = (0x820020 | localLayoutParams.flags);
        localLayoutParams.token = null;
        localLayoutParams.windowAnimations = 0;
    }

    private void installPrevNextListeners()
    {
        boolean bool1 = true;
        boolean bool2;
        ImageButton localImageButton1;
        if (this.mNextButton != null)
        {
            this.mNextButton.setOnClickListener(this.mNextListener);
            ImageButton localImageButton2 = this.mNextButton;
            if (this.mNextListener != null)
            {
                bool2 = bool1;
                localImageButton2.setEnabled(bool2);
            }
        }
        else if (this.mPrevButton != null)
        {
            this.mPrevButton.setOnClickListener(this.mPrevListener);
            localImageButton1 = this.mPrevButton;
            if (this.mPrevListener == null)
                break label83;
        }
        while (true)
        {
            localImageButton1.setEnabled(bool1);
            return;
            bool2 = false;
            break;
            label83: bool1 = false;
        }
    }

    private int setProgress()
    {
        int i;
        if ((this.mPlayer == null) || (this.mDragging))
            i = 0;
        while (true)
        {
            return i;
            i = this.mPlayer.getCurrentPosition();
            int j = this.mPlayer.getDuration();
            if (this.mProgress != null)
            {
                if (j > 0)
                {
                    long l = 1000L * i / j;
                    this.mProgress.setProgress((int)l);
                }
                int k = this.mPlayer.getBufferPercentage();
                this.mProgress.setSecondaryProgress(k * 10);
            }
            if (this.mEndTime != null)
                this.mEndTime.setText(stringForTime(j));
            if (this.mCurrentTime != null)
                this.mCurrentTime.setText(stringForTime(i));
        }
    }

    private String stringForTime(int paramInt)
    {
        int i = paramInt / 1000;
        int j = i % 60;
        int k = i / 60 % 60;
        int m = i / 3600;
        this.mFormatBuilder.setLength(0);
        Formatter localFormatter2;
        Object[] arrayOfObject2;
        if (m > 0)
        {
            localFormatter2 = this.mFormatter;
            arrayOfObject2 = new Object[3];
            arrayOfObject2[0] = Integer.valueOf(m);
            arrayOfObject2[1] = Integer.valueOf(k);
            arrayOfObject2[2] = Integer.valueOf(j);
        }
        Formatter localFormatter1;
        Object[] arrayOfObject1;
        for (String str = localFormatter2.format("%d:%02d:%02d", arrayOfObject2).toString(); ; str = localFormatter1.format("%02d:%02d", arrayOfObject1).toString())
        {
            return str;
            localFormatter1 = this.mFormatter;
            arrayOfObject1 = new Object[2];
            arrayOfObject1[0] = Integer.valueOf(k);
            arrayOfObject1[1] = Integer.valueOf(j);
        }
    }

    private void updateFloatingWindowLayout()
    {
        int[] arrayOfInt = new int[2];
        this.mAnchor.getLocationOnScreen(arrayOfInt);
        WindowManager.LayoutParams localLayoutParams = this.mDecorLayoutParams;
        localLayoutParams.width = this.mAnchor.getWidth();
        localLayoutParams.y = (arrayOfInt[1] + this.mAnchor.getHeight());
    }

    private void updatePausePlay()
    {
        if ((this.mRoot == null) || (this.mPauseButton == null));
        while (true)
        {
            return;
            if (this.mPlayer.isPlaying())
                this.mPauseButton.setImageResource(17301539);
            else
                this.mPauseButton.setImageResource(17301540);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool1 = true;
        int i = paramKeyEvent.getKeyCode();
        boolean bool2;
        if ((paramKeyEvent.getRepeatCount() == 0) && (paramKeyEvent.getAction() == 0))
        {
            bool2 = bool1;
            if ((i != 79) && (i != 85) && (i != 62))
                break label81;
            if (bool2)
            {
                doPauseResume();
                show(3000);
                if (this.mPauseButton != null)
                    this.mPauseButton.requestFocus();
            }
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label81: if (i == 126)
            {
                if ((bool2) && (!this.mPlayer.isPlaying()))
                {
                    this.mPlayer.start();
                    updatePausePlay();
                    show(3000);
                }
            }
            else if ((i == 86) || (i == 127))
            {
                if ((bool2) && (this.mPlayer.isPlaying()))
                {
                    this.mPlayer.pause();
                    updatePausePlay();
                    show(3000);
                }
            }
            else if ((i == 25) || (i == 24) || (i == 164))
            {
                bool1 = super.dispatchKeyEvent(paramKeyEvent);
            }
            else if ((i == 4) || (i == 82))
            {
                if (bool2)
                    hide();
            }
            else
            {
                show(3000);
                bool1 = super.dispatchKeyEvent(paramKeyEvent);
            }
        }
    }

    public void hide()
    {
        if (this.mAnchor == null);
        while (true)
        {
            return;
            if (!this.mShowing)
                continue;
            try
            {
                this.mHandler.removeMessages(2);
                this.mWindowManager.removeView(this.mDecor);
                this.mShowing = false;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                while (true)
                    Log.w("MediaController", "already removed");
            }
        }
    }

    public boolean isShowing()
    {
        return this.mShowing;
    }

    protected View makeControllerView()
    {
        this.mRoot = ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(17367141, null);
        initControllerView(this.mRoot);
        return this.mRoot;
    }

    public void onFinishInflate()
    {
        if (this.mRoot != null)
            initControllerView(this.mRoot);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(MediaController.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(MediaController.class.getName());
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        show(3000);
        return true;
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        show(3000);
        return false;
    }

    public void setAnchorView(View paramView)
    {
        if (this.mAnchor != null)
            this.mAnchor.removeOnLayoutChangeListener(this.mLayoutChangeListener);
        this.mAnchor = paramView;
        if (this.mAnchor != null)
            this.mAnchor.addOnLayoutChangeListener(this.mLayoutChangeListener);
        FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
        removeAllViews();
        addView(makeControllerView(), localLayoutParams);
    }

    public void setEnabled(boolean paramBoolean)
    {
        boolean bool1 = true;
        if (this.mPauseButton != null)
            this.mPauseButton.setEnabled(paramBoolean);
        if (this.mFfwdButton != null)
            this.mFfwdButton.setEnabled(paramBoolean);
        if (this.mRewButton != null)
            this.mRewButton.setEnabled(paramBoolean);
        boolean bool2;
        ImageButton localImageButton1;
        if (this.mNextButton != null)
        {
            ImageButton localImageButton2 = this.mNextButton;
            if ((paramBoolean) && (this.mNextListener != null))
            {
                bool2 = bool1;
                localImageButton2.setEnabled(bool2);
            }
        }
        else if (this.mPrevButton != null)
        {
            localImageButton1 = this.mPrevButton;
            if ((!paramBoolean) || (this.mPrevListener == null))
                break label140;
        }
        while (true)
        {
            localImageButton1.setEnabled(bool1);
            if (this.mProgress != null)
                this.mProgress.setEnabled(paramBoolean);
            disableUnsupportedButtons();
            super.setEnabled(paramBoolean);
            return;
            bool2 = false;
            break;
            label140: bool1 = false;
        }
    }

    public void setMediaPlayer(MediaPlayerControl paramMediaPlayerControl)
    {
        this.mPlayer = paramMediaPlayerControl;
        updatePausePlay();
    }

    public void setPrevNextListeners(View.OnClickListener paramOnClickListener1, View.OnClickListener paramOnClickListener2)
    {
        this.mNextListener = paramOnClickListener1;
        this.mPrevListener = paramOnClickListener2;
        this.mListenersSet = true;
        if (this.mRoot != null)
        {
            installPrevNextListeners();
            if ((this.mNextButton != null) && (!this.mFromXml))
                this.mNextButton.setVisibility(0);
            if ((this.mPrevButton != null) && (!this.mFromXml))
                this.mPrevButton.setVisibility(0);
        }
    }

    public void show()
    {
        show(3000);
    }

    public void show(int paramInt)
    {
        if ((!this.mShowing) && (this.mAnchor != null))
        {
            setProgress();
            if (this.mPauseButton != null)
                this.mPauseButton.requestFocus();
            disableUnsupportedButtons();
            updateFloatingWindowLayout();
            this.mWindowManager.addView(this.mDecor, this.mDecorLayoutParams);
            this.mShowing = true;
        }
        updatePausePlay();
        this.mHandler.sendEmptyMessage(2);
        Message localMessage = this.mHandler.obtainMessage(1);
        if (paramInt != 0)
        {
            this.mHandler.removeMessages(1);
            this.mHandler.sendMessageDelayed(localMessage, paramInt);
        }
    }

    public static abstract interface MediaPlayerControl
    {
        public abstract boolean canPause();

        public abstract boolean canSeekBackward();

        public abstract boolean canSeekForward();

        public abstract int getBufferPercentage();

        public abstract int getCurrentPosition();

        public abstract int getDuration();

        public abstract boolean isPlaying();

        public abstract void pause();

        public abstract void seekTo(int paramInt);

        public abstract void start();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.MediaController
 * JD-Core Version:        0.6.2
 */