package android.widget;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings.System;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import java.text.SimpleDateFormat;
import java.util.Date;

@RemoteViews.RemoteView
public class DateTimeView extends TextView
{
    private static final int SHOW_MONTH_DAY_YEAR = 1;
    private static final int SHOW_TIME = 0;
    private static final String TAG = "DateTimeView";
    private static final long TWELVE_HOURS_IN_MINUTES = 720L;
    private static final long TWENTY_FOUR_HOURS_IN_MILLIS = 86400000L;
    private boolean mAttachedToWindow;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (("android.intent.action.TIME_TICK".equals(paramAnonymousIntent.getAction())) && (System.currentTimeMillis() < DateTimeView.this.mUpdateTimeMillis));
            while (true)
            {
                return;
                DateTimeView.this.mLastFormat = null;
                DateTimeView.this.update();
            }
        }
    };
    private ContentObserver mContentObserver = new ContentObserver(new Handler())
    {
        public void onChange(boolean paramAnonymousBoolean)
        {
            DateTimeView.this.mLastFormat = null;
            DateTimeView.this.update();
        }
    };
    int mLastDisplay = -1;
    java.text.DateFormat mLastFormat;
    Date mTime;
    long mTimeMillis;
    private long mUpdateTimeMillis;

    public DateTimeView(Context paramContext)
    {
        super(paramContext);
    }

    public DateTimeView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    private java.text.DateFormat getDateFormat()
    {
        String str = Settings.System.getString(getContext().getContentResolver(), "date_format");
        Object localObject;
        if ((str == null) || ("".equals(str)))
            localObject = java.text.DateFormat.getDateInstance(3);
        while (true)
        {
            return localObject;
            try
            {
                localObject = new SimpleDateFormat(str);
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                localObject = java.text.DateFormat.getDateInstance(3);
            }
        }
    }

    private java.text.DateFormat getTimeFormat()
    {
        Context localContext = getContext();
        if (android.text.format.DateFormat.is24HourFormat(localContext));
        for (int i = 17039490; ; i = 17039489)
            return new SimpleDateFormat(localContext.getString(i));
    }

    private void registerReceivers()
    {
        Context localContext = getContext();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.TIME_TICK");
        localIntentFilter.addAction("android.intent.action.TIME_SET");
        localIntentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
        localIntentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        localContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter);
        Uri localUri = Settings.System.getUriFor("date_format");
        localContext.getContentResolver().registerContentObserver(localUri, true, this.mContentObserver);
    }

    private void unregisterReceivers()
    {
        Context localContext = getContext();
        localContext.unregisterReceiver(this.mBroadcastReceiver);
        localContext.getContentResolver().unregisterContentObserver(this.mContentObserver);
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        registerReceivers();
        this.mAttachedToWindow = true;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        unregisterReceivers();
        this.mAttachedToWindow = false;
    }

    @RemotableViewMethod
    public void setTime(long paramLong)
    {
        Time localTime = new Time();
        localTime.set(paramLong);
        localTime.second = 0;
        this.mTimeMillis = localTime.toMillis(false);
        this.mTime = new Date(-1900 + localTime.year, localTime.month, localTime.monthDay, localTime.hour, localTime.minute, 0);
        update();
    }

    void update()
    {
        if (this.mTime == null)
            return;
        System.nanoTime();
        Time localTime = new Time();
        localTime.set(this.mTimeMillis);
        localTime.second = 0;
        localTime.hour = (-12 + localTime.hour);
        long l1 = localTime.toMillis(false);
        localTime.hour = (12 + localTime.hour);
        long l2 = localTime.toMillis(false);
        localTime.hour = 0;
        localTime.minute = 0;
        long l3 = localTime.toMillis(false);
        localTime.monthDay = (1 + localTime.monthDay);
        long l4 = localTime.toMillis(false);
        localTime.set(System.currentTimeMillis());
        localTime.second = 0;
        long l5 = localTime.normalize(false);
        int i;
        label180: java.text.DateFormat localDateFormat;
        if (((l5 >= l3) && (l5 < l4)) || ((l5 >= l1) && (l5 < l2)))
        {
            i = 0;
            if ((i != this.mLastDisplay) || (this.mLastFormat == null))
                break label247;
            localDateFormat = this.mLastFormat;
            setText(localDateFormat.format(this.mTime));
            if (i != 0)
                break label331;
            if (l2 <= l4)
                break label324;
        }
        label228: for (this.mUpdateTimeMillis = l2; ; this.mUpdateTimeMillis = 0L)
        {
            System.nanoTime();
            break;
            i = 1;
            break label180;
            switch (i)
            {
            default:
                label247: throw new RuntimeException("unknown display value: " + i);
            case 0:
            case 1:
            }
            for (localDateFormat = getTimeFormat(); ; localDateFormat = getDateFormat())
            {
                this.mLastFormat = localDateFormat;
                break;
            }
            l2 = l4;
            break label228;
            if (this.mTimeMillis >= l5)
                break label349;
        }
        label324: label331: label349: if (l1 < l3);
        while (true)
        {
            this.mUpdateTimeMillis = l1;
            break;
            l1 = l3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.DateTimeView
 * JD-Core Version:        0.6.2
 */