package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Transformation;
import com.android.internal.R.styleable;

@Deprecated
public class Gallery extends AbsSpinner
    implements GestureDetector.OnGestureListener
{
    private static final int SCROLL_TO_FLING_UNCERTAINTY_TIMEOUT = 250;
    private static final String TAG = "Gallery";
    private static final boolean localLOGV;
    private int mAnimationDuration = 400;
    private AdapterView.AdapterContextMenuInfo mContextMenuInfo;
    private Runnable mDisableSuppressSelectionChangedRunnable = new Runnable()
    {
        public void run()
        {
            Gallery.access$002(Gallery.this, false);
            Gallery.this.selectionChanged();
        }
    };
    private int mDownTouchPosition;
    private View mDownTouchView;
    private FlingRunnable mFlingRunnable = new FlingRunnable();
    private GestureDetector mGestureDetector = new GestureDetector(paramContext, this);
    private int mGravity;
    private boolean mIsFirstScroll;
    private boolean mIsRtl = true;
    private int mLeftMost;
    private boolean mReceivedInvokeKeyDown;
    private int mRightMost;
    private View mSelectedChild;
    private boolean mShouldCallbackDuringFling = true;
    private boolean mShouldCallbackOnUnselectedItemClick = true;
    private boolean mShouldStopFling;
    private int mSpacing = 0;
    private boolean mSuppressSelectionChanged;
    private float mUnselectedAlpha;

    public Gallery(Context paramContext)
    {
        this(paramContext, null);
    }

    public Gallery(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842864);
    }

    public Gallery(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        this.mGestureDetector.setIsLongpressEnabled(true);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Gallery, paramInt, 0);
        int i = localTypedArray.getInt(0, -1);
        if (i >= 0)
            setGravity(i);
        int j = localTypedArray.getInt(1, -1);
        if (j > 0)
            setAnimationDuration(j);
        setSpacing(localTypedArray.getDimensionPixelOffset(2, 0));
        setUnselectedAlpha(localTypedArray.getFloat(3, 0.5F));
        localTypedArray.recycle();
        this.mGroupFlags = (0x400 | this.mGroupFlags);
        this.mGroupFlags = (0x800 | this.mGroupFlags);
    }

    private int calculateTop(View paramView, boolean paramBoolean)
    {
        int i;
        int j;
        label19: int k;
        if (paramBoolean)
        {
            i = getMeasuredHeight();
            if (!paramBoolean)
                break label71;
            j = paramView.getMeasuredHeight();
            k = 0;
            switch (this.mGravity)
            {
            default:
            case 48:
            case 16:
            case 80:
            }
        }
        while (true)
        {
            return k;
            i = getHeight();
            break;
            label71: j = paramView.getHeight();
            break label19;
            k = this.mSpinnerPadding.top;
            continue;
            int m = i - this.mSpinnerPadding.bottom - this.mSpinnerPadding.top - j;
            k = this.mSpinnerPadding.top + m / 2;
            continue;
            k = i - this.mSpinnerPadding.bottom - j;
        }
    }

    private void detachOffScreenChildren(boolean paramBoolean)
    {
        int i = getChildCount();
        int j = this.mFirstPosition;
        int k = 0;
        int m = 0;
        if (paramBoolean)
        {
            int i3 = this.mPaddingLeft;
            for (int i4 = 0; ; i4++)
            {
                if (i4 < i)
                    if (!this.mIsRtl)
                        break label106;
                View localView2;
                label106: for (int i5 = i - 1 - i4; ; i5 = i4)
                {
                    localView2 = getChildAt(i5);
                    if (localView2.getRight() < i3)
                        break;
                    if (!this.mIsRtl)
                        k = 0;
                    detachViewsFromParent(k, m);
                    if (paramBoolean != this.mIsRtl)
                        this.mFirstPosition = (m + this.mFirstPosition);
                    return;
                }
                k = i5;
                m++;
                this.mRecycler.put(j + i5, localView2);
            }
        }
        int n = getWidth() - this.mPaddingRight;
        for (int i1 = i - 1; ; i1--)
        {
            if (i1 >= 0)
                if (!this.mIsRtl)
                    break label206;
            View localView1;
            label206: for (int i2 = i - 1 - i1; ; i2 = i1)
            {
                localView1 = getChildAt(i2);
                if (localView1.getLeft() > n)
                    break label213;
                if (!this.mIsRtl)
                    break;
                k = 0;
                break;
            }
            label213: k = i2;
            m++;
            this.mRecycler.put(j + i2, localView1);
        }
    }

    private boolean dispatchLongPress(View paramView, int paramInt, long paramLong)
    {
        boolean bool = false;
        if (this.mOnItemLongClickListener != null)
            bool = this.mOnItemLongClickListener.onItemLongClick(this, this.mDownTouchView, this.mDownTouchPosition, paramLong);
        if (!bool)
        {
            this.mContextMenuInfo = new AdapterView.AdapterContextMenuInfo(paramView, paramInt, paramLong);
            bool = super.showContextMenuForChild(this);
        }
        if (bool)
            performHapticFeedback(0);
        return bool;
    }

    private void dispatchPress(View paramView)
    {
        if (paramView != null)
            paramView.setPressed(true);
        setPressed(true);
    }

    private void dispatchUnpress()
    {
        for (int i = -1 + getChildCount(); i >= 0; i--)
            getChildAt(i).setPressed(false);
        setPressed(false);
    }

    private void fillToGalleryLeft()
    {
        if (this.mIsRtl)
            fillToGalleryLeftRtl();
        while (true)
        {
            return;
            fillToGalleryLeftLtr();
        }
    }

    private void fillToGalleryLeftLtr()
    {
        int i = this.mSpacing;
        int j = this.mPaddingLeft;
        View localView1 = getChildAt(0);
        int k;
        int m;
        if (localView1 != null)
        {
            k = -1 + this.mFirstPosition;
            m = localView1.getLeft() - i;
        }
        while ((m > j) && (k >= 0))
        {
            View localView2 = makeAndAddView(k, k - this.mSelectedPosition, m, false);
            this.mFirstPosition = k;
            m = localView2.getLeft() - i;
            k--;
            continue;
            k = 0;
            m = this.mRight - this.mLeft - this.mPaddingRight;
            this.mShouldStopFling = true;
        }
    }

    private void fillToGalleryLeftRtl()
    {
        int i = this.mSpacing;
        int j = this.mPaddingLeft;
        int k = getChildCount();
        View localView = getChildAt(k - 1);
        int m;
        int n;
        if (localView != null)
        {
            m = k + this.mFirstPosition;
            n = localView.getLeft() - i;
        }
        while ((n > j) && (m < this.mItemCount))
        {
            n = makeAndAddView(m, m - this.mSelectedPosition, n, false).getLeft() - i;
            m++;
            continue;
            m = -1 + this.mItemCount;
            this.mFirstPosition = m;
            n = this.mRight - this.mLeft - this.mPaddingRight;
            this.mShouldStopFling = true;
        }
    }

    private void fillToGalleryRight()
    {
        if (this.mIsRtl)
            fillToGalleryRightRtl();
        while (true)
        {
            return;
            fillToGalleryRightLtr();
        }
    }

    private void fillToGalleryRightLtr()
    {
        int i = this.mSpacing;
        int j = this.mRight - this.mLeft - this.mPaddingRight;
        int k = getChildCount();
        int m = this.mItemCount;
        View localView = getChildAt(k - 1);
        int n;
        int i1;
        if (localView != null)
        {
            n = k + this.mFirstPosition;
            i1 = i + localView.getRight();
        }
        while ((i1 < j) && (n < m))
        {
            i1 = i + makeAndAddView(n, n - this.mSelectedPosition, i1, true).getRight();
            n++;
            continue;
            n = -1 + this.mItemCount;
            this.mFirstPosition = n;
            i1 = this.mPaddingLeft;
            this.mShouldStopFling = true;
        }
    }

    private void fillToGalleryRightRtl()
    {
        int i = this.mSpacing;
        int j = this.mRight - this.mLeft - this.mPaddingRight;
        View localView1 = getChildAt(0);
        int k;
        int m;
        if (localView1 != null)
        {
            k = -1 + this.mFirstPosition;
            m = i + localView1.getRight();
        }
        while ((m < j) && (k >= 0))
        {
            View localView2 = makeAndAddView(k, k - this.mSelectedPosition, m, true);
            this.mFirstPosition = k;
            m = i + localView2.getRight();
            k--;
            continue;
            k = 0;
            m = this.mPaddingLeft;
            this.mShouldStopFling = true;
        }
    }

    private int getCenterOfGallery()
    {
        return (getWidth() - this.mPaddingLeft - this.mPaddingRight) / 2 + this.mPaddingLeft;
    }

    private static int getCenterOfView(View paramView)
    {
        return paramView.getLeft() + paramView.getWidth() / 2;
    }

    private View makeAndAddView(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        View localView2;
        if (!this.mDataChanged)
        {
            localView2 = this.mRecycler.get(paramInt1);
            if (localView2 != null)
            {
                int i = localView2.getLeft();
                this.mRightMost = Math.max(this.mRightMost, i + localView2.getMeasuredWidth());
                this.mLeftMost = Math.min(this.mLeftMost, i);
                setUpChild(localView2, paramInt2, paramInt3, paramBoolean);
            }
        }
        View localView1;
        for (Object localObject = localView2; ; localObject = localView1)
        {
            return localObject;
            localView1 = this.mAdapter.getView(paramInt1, null, this);
            setUpChild(localView1, paramInt2, paramInt3, paramBoolean);
        }
    }

    private void offsetChildrenLeftAndRight(int paramInt)
    {
        for (int i = -1 + getChildCount(); i >= 0; i--)
            getChildAt(i).offsetLeftAndRight(paramInt);
    }

    private void onFinishedMovement()
    {
        if (this.mSuppressSelectionChanged)
        {
            this.mSuppressSelectionChanged = false;
            super.selectionChanged();
        }
        invalidate();
    }

    private void scrollIntoSlots()
    {
        if ((getChildCount() == 0) || (this.mSelectedChild == null));
        while (true)
        {
            return;
            int i = getCenterOfView(this.mSelectedChild);
            int j = getCenterOfGallery() - i;
            if (j != 0)
                this.mFlingRunnable.startUsingDistance(j);
            else
                onFinishedMovement();
        }
    }

    private boolean scrollToChild(int paramInt)
    {
        View localView = getChildAt(paramInt);
        if (localView != null)
        {
            int i = getCenterOfGallery() - getCenterOfView(localView);
            this.mFlingRunnable.startUsingDistance(i);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void setSelectionToCenterChild()
    {
        View localView1 = this.mSelectedChild;
        if (this.mSelectedChild == null);
        int i;
        do
        {
            return;
            i = getCenterOfGallery();
        }
        while ((localView1.getLeft() <= i) && (localView1.getRight() >= i));
        int j = 2147483647;
        int k = 0;
        for (int m = -1 + getChildCount(); ; m--)
        {
            View localView2;
            if (m >= 0)
            {
                localView2 = getChildAt(m);
                if ((localView2.getLeft() <= i) && (localView2.getRight() >= i))
                    k = m;
            }
            else
            {
                int n = k + this.mFirstPosition;
                if (n == this.mSelectedPosition)
                    break;
                setSelectedPositionInt(n);
                setNextSelectedPositionInt(n);
                checkSelectionChanged();
                break;
            }
            int i1 = Math.min(Math.abs(localView2.getLeft() - i), Math.abs(localView2.getRight() - i));
            if (i1 < j)
            {
                j = i1;
                k = m;
            }
        }
    }

    private void setUpChild(View paramView, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
        if (localLayoutParams == null)
            localLayoutParams = (LayoutParams)generateDefaultLayoutParams();
        int i;
        boolean bool;
        label53: int k;
        int m;
        int n;
        int i1;
        if (paramBoolean != this.mIsRtl)
        {
            i = -1;
            addViewInLayout(paramView, i, localLayoutParams);
            if (paramInt1 != 0)
                break label178;
            bool = true;
            paramView.setSelected(bool);
            int j = ViewGroup.getChildMeasureSpec(this.mHeightMeasureSpec, this.mSpinnerPadding.top + this.mSpinnerPadding.bottom, localLayoutParams.height);
            paramView.measure(ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mSpinnerPadding.left + this.mSpinnerPadding.right, localLayoutParams.width), j);
            k = calculateTop(paramView, true);
            m = k + paramView.getMeasuredHeight();
            n = paramView.getMeasuredWidth();
            if (!paramBoolean)
                break label184;
            i1 = paramInt2;
        }
        for (int i2 = i1 + n; ; i2 = paramInt2)
        {
            paramView.layout(i1, k, i2, m);
            return;
            i = 0;
            break;
            label178: bool = false;
            break label53;
            label184: i1 = paramInt2 - n;
        }
    }

    private void updateSelectedItemMetadata()
    {
        View localView1 = this.mSelectedChild;
        View localView2 = getChildAt(this.mSelectedPosition - this.mFirstPosition);
        this.mSelectedChild = localView2;
        if (localView2 == null);
        while (true)
        {
            return;
            localView2.setSelected(true);
            localView2.setFocusable(true);
            if (hasFocus())
                localView2.requestFocus();
            if ((localView1 != null) && (localView1 != localView2))
            {
                localView1.setSelected(false);
                localView1.setFocusable(false);
            }
        }
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    protected int computeHorizontalScrollExtent()
    {
        return 1;
    }

    protected int computeHorizontalScrollOffset()
    {
        return this.mSelectedPosition;
    }

    protected int computeHorizontalScrollRange()
    {
        return this.mItemCount;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        return paramKeyEvent.dispatch(this, null, null);
    }

    protected void dispatchSetPressed(boolean paramBoolean)
    {
        if (this.mSelectedChild != null)
            this.mSelectedChild.setPressed(paramBoolean);
    }

    public void dispatchSetSelected(boolean paramBoolean)
    {
    }

    protected ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-2, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    protected int getChildDrawingOrder(int paramInt1, int paramInt2)
    {
        int i = this.mSelectedPosition - this.mFirstPosition;
        if (i < 0);
        while (true)
        {
            return paramInt2;
            if (paramInt2 == paramInt1 - 1)
                paramInt2 = i;
            else if (paramInt2 >= i)
                paramInt2++;
        }
    }

    int getChildHeight(View paramView)
    {
        return paramView.getMeasuredHeight();
    }

    protected boolean getChildStaticTransformation(View paramView, Transformation paramTransformation)
    {
        paramTransformation.clear();
        if (paramView == this.mSelectedChild);
        for (float f = 1.0F; ; f = this.mUnselectedAlpha)
        {
            paramTransformation.setAlpha(f);
            return true;
        }
    }

    protected ContextMenu.ContextMenuInfo getContextMenuInfo()
    {
        return this.mContextMenuInfo;
    }

    int getLimitedMotionScrollAmount(boolean paramBoolean, int paramInt)
    {
        int i = 0;
        int j;
        View localView;
        if (paramBoolean != this.mIsRtl)
        {
            j = -1 + this.mItemCount;
            localView = getChildAt(j - this.mFirstPosition);
            if (localView != null)
                break label47;
            i = paramInt;
        }
        while (true)
        {
            return i;
            j = 0;
            break;
            label47: int k = getCenterOfView(localView);
            int m = getCenterOfGallery();
            if (paramBoolean)
            {
                if (k <= m);
            }
            else
            {
                int n;
                while (k < m)
                {
                    n = m - k;
                    if (!paramBoolean)
                        break label102;
                    i = Math.max(n, paramInt);
                    break;
                }
                continue;
                label102: i = Math.min(n, paramInt);
            }
        }
    }

    void layout(int paramInt, boolean paramBoolean)
    {
        this.mIsRtl = isLayoutRtl();
        int i = this.mSpinnerPadding.left;
        int j = this.mRight - this.mLeft - this.mSpinnerPadding.left - this.mSpinnerPadding.right;
        if (this.mDataChanged)
            handleDataChanged();
        if (this.mItemCount == 0)
            resetList();
        while (true)
        {
            return;
            if (this.mNextSelectedPosition >= 0)
                setSelectedPositionInt(this.mNextSelectedPosition);
            recycleAllViews();
            detachAllViewsFromParent();
            this.mRightMost = 0;
            this.mLeftMost = 0;
            this.mFirstPosition = this.mSelectedPosition;
            View localView = makeAndAddView(this.mSelectedPosition, 0, 0, true);
            localView.offsetLeftAndRight(i + j / 2 - localView.getWidth() / 2);
            fillToGalleryRight();
            fillToGalleryLeft();
            this.mRecycler.clear();
            invalidate();
            checkSelectionChanged();
            this.mDataChanged = false;
            this.mNeedSync = false;
            setNextSelectedPositionInt(this.mSelectedPosition);
            updateSelectedItemMetadata();
        }
    }

    boolean moveNext()
    {
        if ((this.mItemCount > 0) && (this.mSelectedPosition < -1 + this.mItemCount))
            scrollToChild(1 + (this.mSelectedPosition - this.mFirstPosition));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean movePrevious()
    {
        if ((this.mItemCount > 0) && (this.mSelectedPosition > 0))
            scrollToChild(-1 + (this.mSelectedPosition - this.mFirstPosition));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void onCancel()
    {
        onUp();
    }

    public boolean onDown(MotionEvent paramMotionEvent)
    {
        this.mFlingRunnable.stop(false);
        this.mDownTouchPosition = pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
        if (this.mDownTouchPosition >= 0)
        {
            this.mDownTouchView = getChildAt(this.mDownTouchPosition - this.mFirstPosition);
            this.mDownTouchView.setPressed(true);
        }
        this.mIsFirstScroll = true;
        return true;
    }

    public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
        if (!this.mShouldCallbackDuringFling)
        {
            removeCallbacks(this.mDisableSuppressSelectionChangedRunnable);
            if (!this.mSuppressSelectionChanged)
                this.mSuppressSelectionChanged = true;
        }
        this.mFlingRunnable.startUsingVelocity((int)-paramFloat1);
        return true;
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
        if ((paramBoolean) && (this.mSelectedChild != null))
        {
            this.mSelectedChild.requestFocus(paramInt);
            this.mSelectedChild.setSelected(true);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(Gallery.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        int i = 1;
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(Gallery.class.getName());
        if (this.mItemCount > i);
        while (true)
        {
            paramAccessibilityNodeInfo.setScrollable(i);
            if (isEnabled())
            {
                if ((this.mItemCount > 0) && (this.mSelectedPosition < -1 + this.mItemCount))
                    paramAccessibilityNodeInfo.addAction(4096);
                if ((isEnabled()) && (this.mItemCount > 0) && (this.mSelectedPosition > 0))
                    paramAccessibilityNodeInfo.addAction(8192);
            }
            return;
            int j = 0;
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
        case 21:
        case 22:
        case 23:
        case 66:
        }
        while (true)
        {
            i = super.onKeyDown(paramInt, paramKeyEvent);
            while (true)
            {
                return i;
                if (movePrevious())
                {
                    playSoundEffect(i);
                    continue;
                    if (moveNext())
                        playSoundEffect(3);
                }
            }
            this.mReceivedInvokeKeyDown = i;
        }
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        switch (paramInt)
        {
        default:
        case 23:
        case 66:
        }
        for (boolean bool = super.onKeyUp(paramInt, paramKeyEvent); ; bool = true)
        {
            return bool;
            if ((this.mReceivedInvokeKeyDown) && (this.mItemCount > 0))
            {
                dispatchPress(this.mSelectedChild);
                postDelayed(new Runnable()
                {
                    public void run()
                    {
                        Gallery.this.dispatchUnpress();
                    }
                }
                , ViewConfiguration.getPressedStateDuration());
                performItemClick(getChildAt(this.mSelectedPosition - this.mFirstPosition), this.mSelectedPosition, this.mAdapter.getItemId(this.mSelectedPosition));
            }
            this.mReceivedInvokeKeyDown = false;
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        this.mInLayout = true;
        layout(0, false);
        this.mInLayout = false;
    }

    public void onLongPress(MotionEvent paramMotionEvent)
    {
        if (this.mDownTouchPosition < 0);
        while (true)
        {
            return;
            performHapticFeedback(0);
            long l = getItemIdAtPosition(this.mDownTouchPosition);
            dispatchLongPress(this.mDownTouchView, this.mDownTouchPosition, l);
        }
    }

    public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
        this.mParent.requestDisallowInterceptTouchEvent(true);
        if (!this.mShouldCallbackDuringFling)
            if (this.mIsFirstScroll)
            {
                if (!this.mSuppressSelectionChanged)
                    this.mSuppressSelectionChanged = true;
                postDelayed(this.mDisableSuppressSelectionChangedRunnable, 250L);
            }
        while (true)
        {
            trackMotionScroll(-1 * (int)paramFloat1);
            this.mIsFirstScroll = false;
            return true;
            if (this.mSuppressSelectionChanged)
                this.mSuppressSelectionChanged = false;
        }
    }

    public void onShowPress(MotionEvent paramMotionEvent)
    {
    }

    public boolean onSingleTapUp(MotionEvent paramMotionEvent)
    {
        if (this.mDownTouchPosition >= 0)
        {
            scrollToChild(this.mDownTouchPosition - this.mFirstPosition);
            if ((this.mShouldCallbackOnUnselectedItemClick) || (this.mDownTouchPosition == this.mSelectedPosition))
                performItemClick(this.mDownTouchView, this.mDownTouchPosition, this.mAdapter.getItemId(this.mDownTouchPosition));
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = this.mGestureDetector.onTouchEvent(paramMotionEvent);
        int i = paramMotionEvent.getAction();
        if (i == 1)
            onUp();
        while (true)
        {
            return bool;
            if (i == 3)
                onCancel();
        }
    }

    void onUp()
    {
        if (this.mFlingRunnable.mScroller.isFinished())
            scrollIntoSlots();
        dispatchUnpress();
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool = false;
        if (super.performAccessibilityAction(paramInt, paramBundle))
            bool = true;
        while (true)
        {
            return bool;
            switch (paramInt)
            {
            default:
                break;
            case 4096:
                if ((isEnabled()) && (this.mItemCount > 0) && (this.mSelectedPosition < -1 + this.mItemCount))
                    bool = scrollToChild(1 + (this.mSelectedPosition - this.mFirstPosition));
                break;
            case 8192:
                if ((isEnabled()) && (this.mItemCount > 0) && (this.mSelectedPosition > 0))
                    bool = scrollToChild(-1 + (this.mSelectedPosition - this.mFirstPosition));
                break;
            }
        }
    }

    void selectionChanged()
    {
        if (!this.mSuppressSelectionChanged)
            super.selectionChanged();
    }

    public void setAnimationDuration(int paramInt)
    {
        this.mAnimationDuration = paramInt;
    }

    public void setCallbackDuringFling(boolean paramBoolean)
    {
        this.mShouldCallbackDuringFling = paramBoolean;
    }

    public void setCallbackOnUnselectedItemClick(boolean paramBoolean)
    {
        this.mShouldCallbackOnUnselectedItemClick = paramBoolean;
    }

    public void setGravity(int paramInt)
    {
        if (this.mGravity != paramInt)
        {
            this.mGravity = paramInt;
            requestLayout();
        }
    }

    void setSelectedPositionInt(int paramInt)
    {
        super.setSelectedPositionInt(paramInt);
        updateSelectedItemMetadata();
    }

    public void setSpacing(int paramInt)
    {
        this.mSpacing = paramInt;
    }

    public void setUnselectedAlpha(float paramFloat)
    {
        this.mUnselectedAlpha = paramFloat;
    }

    public boolean showContextMenu()
    {
        if ((isPressed()) && (this.mSelectedPosition >= 0));
        for (boolean bool = dispatchLongPress(getChildAt(this.mSelectedPosition - this.mFirstPosition), this.mSelectedPosition, this.mSelectedRowId); ; bool = false)
            return bool;
    }

    public boolean showContextMenuForChild(View paramView)
    {
        int i = getPositionForView(paramView);
        if (i < 0);
        for (boolean bool = false; ; bool = dispatchLongPress(paramView, i, this.mAdapter.getItemId(i)))
            return bool;
    }

    void trackMotionScroll(int paramInt)
    {
        if (getChildCount() == 0)
            return;
        boolean bool;
        if (paramInt < 0)
        {
            bool = true;
            label14: int i = getLimitedMotionScrollAmount(bool, paramInt);
            if (i != paramInt)
            {
                this.mFlingRunnable.endFling(false);
                onFinishedMovement();
            }
            offsetChildrenLeftAndRight(i);
            detachOffScreenChildren(bool);
            if (!bool)
                break label87;
            fillToGalleryRight();
        }
        while (true)
        {
            this.mRecycler.clear();
            setSelectionToCenterChild();
            onScrollChanged(0, 0, 0, 0);
            invalidate();
            break;
            bool = false;
            break label14;
            label87: fillToGalleryLeft();
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams
    {
        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }
    }

    private class FlingRunnable
        implements Runnable
    {
        private int mLastFlingX;
        private Scroller mScroller = new Scroller(Gallery.this.getContext());

        public FlingRunnable()
        {
        }

        private void endFling(boolean paramBoolean)
        {
            this.mScroller.forceFinished(true);
            if (paramBoolean)
                Gallery.this.scrollIntoSlots();
        }

        private void startCommon()
        {
            Gallery.this.removeCallbacks(this);
        }

        public void run()
        {
            if (Gallery.this.mItemCount == 0)
                endFling(true);
            while (true)
            {
                return;
                Gallery.access$602(Gallery.this, false);
                Scroller localScroller = this.mScroller;
                boolean bool = localScroller.computeScrollOffset();
                int i = localScroller.getCurrX();
                int j = this.mLastFlingX - i;
                int m;
                if (j > 0)
                {
                    Gallery localGallery2 = Gallery.this;
                    if (Gallery.this.mIsRtl);
                    for (int n = -1 + (Gallery.this.mFirstPosition + Gallery.this.getChildCount()); ; n = Gallery.this.mFirstPosition)
                    {
                        Gallery.access$702(localGallery2, n);
                        m = Math.min(-1 + (Gallery.this.getWidth() - Gallery.access$900(Gallery.this) - Gallery.access$1000(Gallery.this)), j);
                        Gallery.this.trackMotionScroll(m);
                        if ((!bool) || (Gallery.this.mShouldStopFling))
                            break label289;
                        this.mLastFlingX = i;
                        Gallery.this.post(this);
                        break;
                    }
                }
                (-1 + Gallery.this.getChildCount());
                Gallery localGallery1 = Gallery.this;
                if (Gallery.this.mIsRtl);
                for (int k = Gallery.this.mFirstPosition; ; k = -1 + (Gallery.this.mFirstPosition + Gallery.this.getChildCount()))
                {
                    Gallery.access$702(localGallery1, k);
                    m = Math.max(-(-1 + (Gallery.this.getWidth() - Gallery.access$1100(Gallery.this) - Gallery.access$1200(Gallery.this))), j);
                    break;
                }
                label289: endFling(true);
            }
        }

        public void startUsingDistance(int paramInt)
        {
            if (paramInt == 0);
            while (true)
            {
                return;
                startCommon();
                this.mLastFlingX = 0;
                this.mScroller.startScroll(0, 0, -paramInt, 0, Gallery.this.mAnimationDuration);
                Gallery.this.post(this);
            }
        }

        public void startUsingVelocity(int paramInt)
        {
            if (paramInt == 0)
                return;
            startCommon();
            if (paramInt < 0);
            for (int i = 2147483647; ; i = 0)
            {
                this.mLastFlingX = i;
                this.mScroller.fling(i, 0, paramInt, 0, 0, 2147483647, 0, 2147483647);
                Gallery.this.post(this);
                break;
            }
        }

        public void stop(boolean paramBoolean)
        {
            Gallery.this.removeCallbacks(this);
            endFling(paramBoolean);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.Gallery
 * JD-Core Version:        0.6.2
 */