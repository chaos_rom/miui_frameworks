package android.widget;

public abstract interface SectionIndexer
{
    public abstract int getPositionForSection(int paramInt);

    public abstract int getSectionForPosition(int paramInt);

    public abstract Object[] getSections();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.SectionIndexer
 * JD-Core Version:        0.6.2
 */