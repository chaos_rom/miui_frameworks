package android.widget;

import android.os.Handler;

class DoubleDigitManager
{
    private Integer intermediateDigit;
    private final CallBack mCallBack;
    private final long timeoutInMillis;

    public DoubleDigitManager(long paramLong, CallBack paramCallBack)
    {
        this.timeoutInMillis = paramLong;
        this.mCallBack = paramCallBack;
    }

    public void reportDigit(int paramInt)
    {
        if (this.intermediateDigit == null)
        {
            this.intermediateDigit = Integer.valueOf(paramInt);
            new Handler().postDelayed(new Runnable()
            {
                public void run()
                {
                    if (DoubleDigitManager.this.intermediateDigit != null)
                    {
                        DoubleDigitManager.this.mCallBack.singleDigitFinal(DoubleDigitManager.this.intermediateDigit.intValue());
                        DoubleDigitManager.access$002(DoubleDigitManager.this, null);
                    }
                }
            }
            , this.timeoutInMillis);
            if (!this.mCallBack.singleDigitIntermediate(paramInt))
            {
                this.intermediateDigit = null;
                this.mCallBack.singleDigitFinal(paramInt);
            }
        }
        while (true)
        {
            return;
            if (this.mCallBack.twoDigitsFinal(this.intermediateDigit.intValue(), paramInt))
                this.intermediateDigit = null;
        }
    }

    static abstract interface CallBack
    {
        public abstract void singleDigitFinal(int paramInt);

        public abstract boolean singleDigitIntermediate(int paramInt);

        public abstract boolean twoDigitsFinal(int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.DoubleDigitManager
 * JD-Core Version:        0.6.2
 */