package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R.styleable;

public class AutoCompleteTextView extends EditText
    implements Filter.FilterListener
{
    static final boolean DEBUG = false;
    static final int EXPAND_MAX = 3;
    static final String TAG = "AutoCompleteTextView";
    private ListAdapter mAdapter;
    private boolean mBlockCompletion;
    private int mDropDownAnchorId;
    private boolean mDropDownDismissedOnCompletion = true;
    private Filter mFilter;
    private int mHintResource;
    private CharSequence mHintText;
    private TextView mHintView;
    private AdapterView.OnItemClickListener mItemClickListener;
    private AdapterView.OnItemSelectedListener mItemSelectedListener;
    private int mLastKeyCode = 0;
    private PopupDataSetObserver mObserver;
    private boolean mOpenBefore;
    private PassThroughClickListener mPassThroughClickListener;
    private ListPopupWindow mPopup;
    private boolean mPopupCanBeUpdated = true;
    private int mThreshold;
    private Validator mValidator = null;

    public AutoCompleteTextView(Context paramContext)
    {
        this(paramContext, null);
    }

    public AutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842859);
    }

    public AutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        this.mPopup = new ListPopupWindow(paramContext, paramAttributeSet, 16842859);
        this.mPopup.setSoftInputMode(16);
        this.mPopup.setPromptPosition(1);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AutoCompleteTextView, paramInt, 0);
        this.mThreshold = localTypedArray.getInt(2, 2);
        this.mPopup.setListSelector(localTypedArray.getDrawable(3));
        this.mPopup.setVerticalOffset((int)localTypedArray.getDimension(9, 0.0F));
        this.mPopup.setHorizontalOffset((int)localTypedArray.getDimension(8, 0.0F));
        this.mDropDownAnchorId = localTypedArray.getResourceId(6, -1);
        this.mPopup.setWidth(localTypedArray.getLayoutDimension(5, -2));
        this.mPopup.setHeight(localTypedArray.getLayoutDimension(7, -2));
        this.mHintResource = localTypedArray.getResourceId(1, 17367209);
        this.mPopup.setOnItemClickListener(new DropDownItemClickListener(null));
        setCompletionHint(localTypedArray.getText(0));
        int i = getInputType();
        if ((i & 0xF) == 1)
            setRawInputType(i | 0x10000);
        localTypedArray.recycle();
        setFocusable(true);
        addTextChangedListener(new MyWatcher(null));
        this.mPassThroughClickListener = new PassThroughClickListener(null);
        super.setOnClickListener(this.mPassThroughClickListener);
    }

    private void buildImeCompletions()
    {
        ListAdapter localListAdapter = this.mAdapter;
        if (localListAdapter != null)
        {
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if (localInputMethodManager != null)
            {
                int i = Math.min(localListAdapter.getCount(), 20);
                Object localObject1 = new CompletionInfo[i];
                int j = 0;
                for (int k = 0; k < i; k++)
                    if (localListAdapter.isEnabled(k))
                    {
                        Object localObject2 = localListAdapter.getItem(k);
                        localObject1[j] = new CompletionInfo(localListAdapter.getItemId(k), j, convertSelectionToString(localObject2));
                        j++;
                    }
                if (j != i)
                {
                    CompletionInfo[] arrayOfCompletionInfo = new CompletionInfo[j];
                    System.arraycopy(localObject1, 0, arrayOfCompletionInfo, 0, j);
                    localObject1 = arrayOfCompletionInfo;
                }
                localInputMethodManager.displayCompletions(this, (CompletionInfo[])localObject1);
            }
        }
    }

    private void onClickImpl()
    {
        if (isPopupShowing())
            ensureImeVisible(true);
    }

    private void performCompletion(View paramView, int paramInt, long paramLong)
    {
        Object localObject;
        if (isPopupShowing())
            if (paramInt < 0)
            {
                localObject = this.mPopup.getSelectedItem();
                if (localObject != null)
                    break label50;
                Log.w("AutoCompleteTextView", "performCompletion: no selected item");
            }
        while (true)
        {
            return;
            localObject = this.mAdapter.getItem(paramInt);
            break;
            label50: this.mBlockCompletion = true;
            replaceText(convertSelectionToString(localObject));
            this.mBlockCompletion = false;
            if (this.mItemClickListener != null)
            {
                ListPopupWindow localListPopupWindow = this.mPopup;
                if ((paramView == null) || (paramInt < 0))
                {
                    paramView = localListPopupWindow.getSelectedView();
                    paramInt = localListPopupWindow.getSelectedItemPosition();
                    paramLong = localListPopupWindow.getSelectedItemId();
                }
                this.mItemClickListener.onItemClick(localListPopupWindow.getListView(), paramView, paramInt, paramLong);
            }
            if ((this.mDropDownDismissedOnCompletion) && (!this.mPopup.isDropDownAlwaysVisible()))
                dismissDropDown();
        }
    }

    private void updateDropDownForFilter(int paramInt)
    {
        if (getWindowVisibility() == 8);
        while (true)
        {
            return;
            boolean bool1 = this.mPopup.isDropDownAlwaysVisible();
            boolean bool2 = enoughToFilter();
            if (((paramInt > 0) || (bool1)) && (bool2))
            {
                if ((hasFocus()) && (hasWindowFocus()) && (this.mPopupCanBeUpdated))
                    showDropDown();
            }
            else if ((!bool1) && (isPopupShowing()))
            {
                dismissDropDown();
                this.mPopupCanBeUpdated = true;
            }
        }
    }

    public void clearListSelection()
    {
        this.mPopup.clearListSelection();
    }

    protected CharSequence convertSelectionToString(Object paramObject)
    {
        return this.mFilter.convertResultToString(paramObject);
    }

    public void dismissDropDown()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if (localInputMethodManager != null)
            localInputMethodManager.displayCompletions(this, null);
        this.mPopup.dismiss();
        this.mPopupCanBeUpdated = false;
    }

    void doAfterTextChanged()
    {
        if (this.mBlockCompletion);
        while (true)
        {
            return;
            if ((!this.mOpenBefore) || (isPopupShowing()))
                if (enoughToFilter())
                {
                    if (this.mFilter != null)
                    {
                        this.mPopupCanBeUpdated = true;
                        performFiltering(getText(), this.mLastKeyCode);
                    }
                }
                else
                {
                    if (!this.mPopup.isDropDownAlwaysVisible())
                        dismissDropDown();
                    if (this.mFilter != null)
                        this.mFilter.filter(null);
                }
        }
    }

    void doBeforeTextChanged()
    {
        if (this.mBlockCompletion);
        while (true)
        {
            return;
            this.mOpenBefore = isPopupShowing();
        }
    }

    public boolean enoughToFilter()
    {
        if (getText().length() >= this.mThreshold);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void ensureImeVisible(boolean paramBoolean)
    {
        ListPopupWindow localListPopupWindow = this.mPopup;
        if (paramBoolean);
        for (int i = 1; ; i = 2)
        {
            localListPopupWindow.setInputMethodMode(i);
            if ((this.mPopup.isDropDownAlwaysVisible()) || ((this.mFilter != null) && (enoughToFilter())))
                showDropDown();
            return;
        }
    }

    public ListAdapter getAdapter()
    {
        return this.mAdapter;
    }

    public CharSequence getCompletionHint()
    {
        return this.mHintText;
    }

    public int getDropDownAnchor()
    {
        return this.mDropDownAnchorId;
    }

    public int getDropDownAnimationStyle()
    {
        return this.mPopup.getAnimationStyle();
    }

    public Drawable getDropDownBackground()
    {
        return this.mPopup.getBackground();
    }

    public int getDropDownHeight()
    {
        return this.mPopup.getHeight();
    }

    public int getDropDownHorizontalOffset()
    {
        return this.mPopup.getHorizontalOffset();
    }

    public int getDropDownVerticalOffset()
    {
        return this.mPopup.getVerticalOffset();
    }

    public int getDropDownWidth()
    {
        return this.mPopup.getWidth();
    }

    protected Filter getFilter()
    {
        return this.mFilter;
    }

    @Deprecated
    public AdapterView.OnItemClickListener getItemClickListener()
    {
        return this.mItemClickListener;
    }

    @Deprecated
    public AdapterView.OnItemSelectedListener getItemSelectedListener()
    {
        return this.mItemSelectedListener;
    }

    public int getListSelection()
    {
        return this.mPopup.getSelectedItemPosition();
    }

    public AdapterView.OnItemClickListener getOnItemClickListener()
    {
        return this.mItemClickListener;
    }

    public AdapterView.OnItemSelectedListener getOnItemSelectedListener()
    {
        return this.mItemSelectedListener;
    }

    public int getThreshold()
    {
        return this.mThreshold;
    }

    public Validator getValidator()
    {
        return this.mValidator;
    }

    public boolean isDropDownAlwaysVisible()
    {
        return this.mPopup.isDropDownAlwaysVisible();
    }

    public boolean isDropDownDismissedOnCompletion()
    {
        return this.mDropDownDismissedOnCompletion;
    }

    public boolean isInputMethodNotNeeded()
    {
        if (this.mPopup.getInputMethodMode() == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPerformingCompletion()
    {
        return this.mBlockCompletion;
    }

    public boolean isPopupShowing()
    {
        return this.mPopup.isShowing();
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
    }

    public void onCommitCompletion(CompletionInfo paramCompletionInfo)
    {
        if (isPopupShowing())
            this.mPopup.performItemClick(paramCompletionInfo.getPosition());
    }

    protected void onDetachedFromWindow()
    {
        dismissDropDown();
        super.onDetachedFromWindow();
    }

    protected void onDisplayHint(int paramInt)
    {
        super.onDisplayHint(paramInt);
        switch (paramInt)
        {
        default:
        case 4:
        }
        while (true)
        {
            return;
            if (!this.mPopup.isDropDownAlwaysVisible())
                dismissDropDown();
        }
    }

    public void onFilterComplete(int paramInt)
    {
        updateDropDownForFilter(paramInt);
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
        if (!paramBoolean)
            performValidation();
        if ((!paramBoolean) && (!this.mPopup.isDropDownAlwaysVisible()))
            dismissDropDown();
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        if (this.mPopup.onKeyDown(paramInt, paramKeyEvent));
        label111: 
        while (true)
        {
            return bool;
            if (!isPopupShowing())
                switch (paramInt)
                {
                default:
                case 20:
                }
            while (true)
            {
                if ((isPopupShowing()) && (paramInt == 61) && (paramKeyEvent.hasNoModifiers()))
                    break label111;
                this.mLastKeyCode = paramInt;
                bool = super.onKeyDown(paramInt, paramKeyEvent);
                this.mLastKeyCode = 0;
                if ((!bool) || (!isPopupShowing()))
                    break;
                clearListSelection();
                break;
                if (paramKeyEvent.hasNoModifiers())
                    performValidation();
            }
        }
    }

    public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        if ((paramInt == 4) && (isPopupShowing()) && (!this.mPopup.isDropDownAlwaysVisible()))
            if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getRepeatCount() == 0))
            {
                KeyEvent.DispatcherState localDispatcherState2 = getKeyDispatcherState();
                if (localDispatcherState2 != null)
                    localDispatcherState2.startTracking(paramKeyEvent, this);
            }
        while (true)
        {
            return i;
            if (paramKeyEvent.getAction() == i)
            {
                KeyEvent.DispatcherState localDispatcherState1 = getKeyDispatcherState();
                if (localDispatcherState1 != null)
                    localDispatcherState1.handleUpEvent(paramKeyEvent);
                if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                    dismissDropDown();
            }
            else
            {
                boolean bool = super.onKeyPreIme(paramInt, paramKeyEvent);
            }
        }
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        if (this.mPopup.onKeyUp(paramInt, paramKeyEvent));
        switch (paramInt)
        {
        default:
            if ((isPopupShowing()) && (paramInt == 61) && (paramKeyEvent.hasNoModifiers()))
                performCompletion();
            break;
        case 23:
        case 61:
        case 66:
        }
        while (true)
        {
            return bool;
            if (paramKeyEvent.hasNoModifiers())
            {
                performCompletion();
                continue;
                bool = super.onKeyUp(paramInt, paramKeyEvent);
            }
        }
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        super.onWindowFocusChanged(paramBoolean);
        if ((!paramBoolean) && (!this.mPopup.isDropDownAlwaysVisible()))
            dismissDropDown();
    }

    public void performCompletion()
    {
        performCompletion(null, -1, -1L);
    }

    protected void performFiltering(CharSequence paramCharSequence, int paramInt)
    {
        this.mFilter.filter(paramCharSequence, this);
    }

    public void performValidation()
    {
        if (this.mValidator == null);
        while (true)
        {
            return;
            Editable localEditable = getText();
            if ((!TextUtils.isEmpty(localEditable)) && (!this.mValidator.isValid(localEditable)))
                setText(this.mValidator.fixText(localEditable));
        }
    }

    protected void replaceText(CharSequence paramCharSequence)
    {
        clearComposingText();
        setText(paramCharSequence);
        Editable localEditable = getText();
        Selection.setSelection(localEditable, localEditable.length());
    }

    public <T extends ListAdapter,    extends Filterable> void setAdapter(T paramT)
    {
        if (this.mObserver == null)
        {
            this.mObserver = new PopupDataSetObserver(null);
            this.mAdapter = paramT;
            if (this.mAdapter == null)
                break label93;
            this.mFilter = ((Filterable)this.mAdapter).getFilter();
            paramT.registerDataSetObserver(this.mObserver);
        }
        while (true)
        {
            this.mPopup.setAdapter(this.mAdapter);
            return;
            if (this.mAdapter == null)
                break;
            this.mAdapter.unregisterDataSetObserver(this.mObserver);
            break;
            label93: this.mFilter = null;
        }
    }

    public void setCompletionHint(CharSequence paramCharSequence)
    {
        this.mHintText = paramCharSequence;
        if (paramCharSequence != null)
            if (this.mHintView == null)
            {
                TextView localTextView = (TextView)LayoutInflater.from(getContext()).inflate(this.mHintResource, null).findViewById(16908308);
                localTextView.setText(this.mHintText);
                this.mHintView = localTextView;
                this.mPopup.setPromptView(localTextView);
            }
        while (true)
        {
            return;
            this.mHintView.setText(paramCharSequence);
            continue;
            this.mPopup.setPromptView(null);
            this.mHintView = null;
        }
    }

    public void setDropDownAlwaysVisible(boolean paramBoolean)
    {
        this.mPopup.setDropDownAlwaysVisible(paramBoolean);
    }

    public void setDropDownAnchor(int paramInt)
    {
        this.mDropDownAnchorId = paramInt;
        this.mPopup.setAnchorView(null);
    }

    public void setDropDownAnimationStyle(int paramInt)
    {
        this.mPopup.setAnimationStyle(paramInt);
    }

    public void setDropDownBackgroundDrawable(Drawable paramDrawable)
    {
        this.mPopup.setBackgroundDrawable(paramDrawable);
    }

    public void setDropDownBackgroundResource(int paramInt)
    {
        this.mPopup.setBackgroundDrawable(getResources().getDrawable(paramInt));
    }

    public void setDropDownDismissedOnCompletion(boolean paramBoolean)
    {
        this.mDropDownDismissedOnCompletion = paramBoolean;
    }

    public void setDropDownHeight(int paramInt)
    {
        this.mPopup.setHeight(paramInt);
    }

    public void setDropDownHorizontalOffset(int paramInt)
    {
        this.mPopup.setHorizontalOffset(paramInt);
    }

    public void setDropDownVerticalOffset(int paramInt)
    {
        this.mPopup.setVerticalOffset(paramInt);
    }

    public void setDropDownWidth(int paramInt)
    {
        this.mPopup.setWidth(paramInt);
    }

    public void setForceIgnoreOutsideTouch(boolean paramBoolean)
    {
        this.mPopup.setForceIgnoreOutsideTouch(paramBoolean);
    }

    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        if (isPopupShowing())
            showDropDown();
        return bool;
    }

    public void setListSelection(int paramInt)
    {
        this.mPopup.setSelection(paramInt);
    }

    public void setOnClickListener(View.OnClickListener paramOnClickListener)
    {
        PassThroughClickListener.access$302(this.mPassThroughClickListener, paramOnClickListener);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
    {
        this.mItemClickListener = paramOnItemClickListener;
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener paramOnItemSelectedListener)
    {
        this.mItemSelectedListener = paramOnItemSelectedListener;
    }

    public void setText(CharSequence paramCharSequence, boolean paramBoolean)
    {
        if (paramBoolean)
            setText(paramCharSequence);
        while (true)
        {
            return;
            this.mBlockCompletion = true;
            setText(paramCharSequence);
            this.mBlockCompletion = false;
        }
    }

    public void setThreshold(int paramInt)
    {
        if (paramInt <= 0)
            paramInt = 1;
        this.mThreshold = paramInt;
    }

    public void setValidator(Validator paramValidator)
    {
        this.mValidator = paramValidator;
    }

    public void showDropDown()
    {
        buildImeCompletions();
        if (this.mPopup.getAnchorView() == null)
        {
            if (this.mDropDownAnchorId == -1)
                break label83;
            this.mPopup.setAnchorView(getRootView().findViewById(this.mDropDownAnchorId));
        }
        while (true)
        {
            if (!isPopupShowing())
            {
                this.mPopup.setInputMethodMode(1);
                this.mPopup.setListItemExpandMax(3);
            }
            this.mPopup.show();
            this.mPopup.getListView().setOverScrollMode(0);
            return;
            label83: this.mPopup.setAnchorView(this);
        }
    }

    public void showDropDownAfterLayout()
    {
        this.mPopup.postShow();
    }

    private class PopupDataSetObserver extends DataSetObserver
    {
        private PopupDataSetObserver()
        {
        }

        public void onChanged()
        {
            if (AutoCompleteTextView.this.mAdapter != null)
                AutoCompleteTextView.this.post(new Runnable()
                {
                    public void run()
                    {
                        ListAdapter localListAdapter = AutoCompleteTextView.this.mAdapter;
                        if (localListAdapter != null)
                            AutoCompleteTextView.this.updateDropDownForFilter(localListAdapter.getCount());
                    }
                });
        }
    }

    private class PassThroughClickListener
        implements View.OnClickListener
    {
        private View.OnClickListener mWrapped;

        private PassThroughClickListener()
        {
        }

        public void onClick(View paramView)
        {
            AutoCompleteTextView.this.onClickImpl();
            if (this.mWrapped != null)
                this.mWrapped.onClick(paramView);
        }
    }

    public static abstract interface Validator
    {
        public abstract CharSequence fixText(CharSequence paramCharSequence);

        public abstract boolean isValid(CharSequence paramCharSequence);
    }

    private class DropDownItemClickListener
        implements AdapterView.OnItemClickListener
    {
        private DropDownItemClickListener()
        {
        }

        public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
        {
            AutoCompleteTextView.this.performCompletion(paramView, paramInt, paramLong);
        }
    }

    private class MyWatcher
        implements TextWatcher
    {
        private MyWatcher()
        {
        }

        public void afterTextChanged(Editable paramEditable)
        {
            AutoCompleteTextView.this.doAfterTextChanged();
        }

        public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
            AutoCompleteTextView.this.doBeforeTextChanged();
        }

        public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.AutoCompleteTextView
 * JD-Core Version:        0.6.2
 */