package android.widget;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

class FastScroller
{
    private static final int[] ATTRS = arrayOfInt2;
    private static final int[] DEFAULT_STATES;
    private static final int FADE_TIMEOUT = 1500;
    private static int MIN_PAGES = 0;
    private static final int OVERLAY_AT_THUMB = 1;
    private static final int OVERLAY_FLOATING = 0;
    private static final int OVERLAY_POSITION = 5;
    private static final int PENDING_DRAG_DELAY = 180;
    private static final int[] PRESSED_STATES;
    private static final int PREVIEW_BACKGROUND_LEFT = 3;
    private static final int PREVIEW_BACKGROUND_RIGHT = 4;
    private static final int STATE_DRAGGING = 3;
    private static final int STATE_ENTER = 1;
    private static final int STATE_EXIT = 4;
    private static final int STATE_NONE = 0;
    private static final int STATE_VISIBLE = 2;
    private static final String TAG = "FastScroller";
    private static final int TEXT_COLOR = 0;
    private static final int THUMB_DRAWABLE = 1;
    private static final int TRACK_DRAWABLE = 2;
    private boolean mAlwaysShow;
    private boolean mChangedBounds;
    private final Runnable mDeferStartDrag = new Runnable()
    {
        public void run()
        {
            int i;
            int j;
            if (FastScroller.this.mList.mIsAttached)
            {
                FastScroller.this.beginDrag();
                i = FastScroller.this.mList.getHeight();
                j = 10 + ((int)FastScroller.this.mInitialTouchY - FastScroller.this.mThumbH);
                if (j >= 0)
                    break label100;
                j = 0;
            }
            while (true)
            {
                FastScroller.this.mThumbY = j;
                FastScroller.this.scrollTo(FastScroller.this.mThumbY / (i - FastScroller.this.mThumbH));
                FastScroller.this.mPendingDrag = false;
                return;
                label100: if (j + FastScroller.this.mThumbH > i)
                    j = i - FastScroller.this.mThumbH;
            }
        }
    };
    private boolean mDrawOverlay;
    private Handler mHandler = new Handler();
    float mInitialTouchY;
    private int mItemCount = -1;
    AbsListView mList;
    BaseAdapter mListAdapter;
    private int mListOffset;
    private boolean mLongList;
    private boolean mMatchDragPosition;
    private Drawable mOverlayDrawable;
    private Drawable mOverlayDrawableLeft;
    private Drawable mOverlayDrawableRight;
    private RectF mOverlayPos;
    private int mOverlayPosition;
    private int mOverlaySize;
    private Paint mPaint;
    boolean mPendingDrag;
    private int mPosition;
    private int mScaledTouchSlop;
    boolean mScrollCompleted;
    private ScrollFade mScrollFade;
    private SectionIndexer mSectionIndexer;
    private String mSectionText;
    private Object[] mSections;
    private int mState;
    private Drawable mThumbDrawable;
    int mThumbH;
    int mThumbW;
    int mThumbY;
    private final Rect mTmpRect = new Rect();
    private Drawable mTrackDrawable;
    private int mVisibleItem;

    static
    {
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = 16842919;
        PRESSED_STATES = arrayOfInt1;
        DEFAULT_STATES = new int[0];
        int[] arrayOfInt2 = new int[6];
        arrayOfInt2[0] = 16843609;
        arrayOfInt2[1] = 16843574;
        arrayOfInt2[2] = 16843577;
        arrayOfInt2[3] = 16843575;
        arrayOfInt2[4] = 16843576;
        arrayOfInt2[5] = 16843578;
    }

    public FastScroller(Context paramContext, AbsListView paramAbsListView)
    {
        this.mList = paramAbsListView;
        init(paramContext);
    }

    private void cancelFling()
    {
        MotionEvent localMotionEvent = MotionEvent.obtain(0L, 0L, 3, 0.0F, 0.0F, 0);
        this.mList.onTouchEvent(localMotionEvent);
        localMotionEvent.recycle();
    }

    private int getThumbPositionForListPosition(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((this.mSectionIndexer == null) || (this.mListAdapter == null))
            getSectionsFromIndexer();
        if ((this.mSectionIndexer == null) || (!this.mMatchDragPosition));
        int j;
        for (int i = paramInt1 * (this.mList.getHeight() - this.mThumbH) / (paramInt3 - paramInt2); ; i = 0)
        {
            return i;
            j = paramInt1 - this.mListOffset;
            if (j >= 0)
                break;
        }
        int k = paramInt3 - this.mListOffset;
        int m = this.mList.getHeight() - this.mThumbH;
        int n = this.mSectionIndexer.getSectionForPosition(j);
        int i1 = this.mSectionIndexer.getPositionForSection(n);
        int i2 = this.mSectionIndexer.getPositionForSection(n + 1);
        int i3 = this.mSections.length;
        int i4 = i2 - i1;
        View localView1 = this.mList.getChildAt(0);
        if (localView1 == null);
        for (float f1 = 0.0F; ; f1 = j + (this.mList.getPaddingTop() - localView1.getTop()) / localView1.getHeight())
        {
            i = (int)(((f1 - i1) / i4 + n) / i3 * m);
            if ((j <= 0) || (j + paramInt2 != k))
                break;
            View localView2 = this.mList.getChildAt(paramInt2 - 1);
            float f2 = (this.mList.getHeight() - this.mList.getPaddingBottom() - localView2.getTop()) / localView2.getHeight();
            i = (int)(i + f2 * (m - i));
            break;
        }
    }

    private void init(Context paramContext)
    {
        int i = 1;
        TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(ATTRS);
        useThumbDrawable(paramContext, localTypedArray.getDrawable(i));
        this.mTrackDrawable = localTypedArray.getDrawable(2);
        this.mOverlayDrawableLeft = localTypedArray.getDrawable(3);
        this.mOverlayDrawableRight = localTypedArray.getDrawable(4);
        this.mOverlayPosition = localTypedArray.getInt(5, 0);
        this.mScrollCompleted = i;
        getSectionsFromIndexer();
        this.mOverlaySize = paramContext.getResources().getDimensionPixelSize(17104915);
        this.mOverlayPos = new RectF();
        this.mScrollFade = new ScrollFade();
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(i);
        this.mPaint.setTextAlign(Paint.Align.CENTER);
        this.mPaint.setTextSize(this.mOverlaySize / 2);
        int k = localTypedArray.getColorStateList(0).getDefaultColor();
        this.mPaint.setColor(k);
        this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        if ((this.mList.getWidth() > 0) && (this.mList.getHeight() > 0))
            onSizeChanged(this.mList.getWidth(), this.mList.getHeight(), 0, 0);
        this.mState = 0;
        refreshDrawableState();
        localTypedArray.recycle();
        this.mScaledTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
        if (paramContext.getApplicationInfo().targetSdkVersion >= 11);
        while (true)
        {
            this.mMatchDragPosition = i;
            setScrollbarPosition(this.mList.getVerticalScrollbarPosition());
            return;
            int j = 0;
        }
    }

    private void refreshDrawableState()
    {
        if (this.mState == 3);
        for (int[] arrayOfInt = PRESSED_STATES; ; arrayOfInt = DEFAULT_STATES)
        {
            if ((this.mThumbDrawable != null) && (this.mThumbDrawable.isStateful()))
                this.mThumbDrawable.setState(arrayOfInt);
            if ((this.mTrackDrawable != null) && (this.mTrackDrawable.isStateful()))
                this.mTrackDrawable.setState(arrayOfInt);
            return;
        }
    }

    private void resetThumbPos()
    {
        int i = this.mList.getWidth();
        switch (this.mPosition)
        {
        default:
        case 0:
        case 2:
        case 1:
        }
        while (true)
        {
            this.mThumbDrawable.setAlpha(208);
            return;
            this.mThumbDrawable.setBounds(i - this.mThumbW, 0, i, this.mThumbH);
            continue;
            this.mThumbDrawable.setBounds(0, 0, this.mThumbW, this.mThumbH);
        }
    }

    private void useThumbDrawable(Context paramContext, Drawable paramDrawable)
    {
        this.mThumbDrawable = paramDrawable;
        if ((paramDrawable instanceof NinePatchDrawable))
            this.mThumbW = paramContext.getResources().getDimensionPixelSize(17104916);
        for (this.mThumbH = paramContext.getResources().getDimensionPixelSize(17104917); ; this.mThumbH = paramDrawable.getIntrinsicHeight())
        {
            this.mChangedBounds = true;
            return;
            this.mThumbW = paramDrawable.getIntrinsicWidth();
        }
    }

    void beginDrag()
    {
        setState(3);
        if ((this.mListAdapter == null) && (this.mList != null))
            getSectionsFromIndexer();
        if (this.mList != null)
        {
            this.mList.requestDisallowInterceptTouchEvent(true);
            this.mList.reportScrollStateChange(1);
        }
        cancelFling();
    }

    void cancelPendingDrag()
    {
        this.mList.removeCallbacks(this.mDeferStartDrag);
        this.mPendingDrag = false;
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mState == 0);
        while (true)
        {
            return;
            int i = this.mThumbY;
            int j = this.mList.getWidth();
            ScrollFade localScrollFade = this.mScrollFade;
            int k = -1;
            int i7;
            if (this.mState == 4)
            {
                k = localScrollFade.getAlpha();
                if (k < 104)
                    this.mThumbDrawable.setAlpha(k * 2);
                i7 = 0;
            }
            switch (this.mPosition)
            {
            default:
                label96: this.mThumbDrawable.setBounds(i7, 0, i7 + this.mThumbW, this.mThumbH);
                this.mChangedBounds = true;
                if (this.mTrackDrawable != null)
                {
                    Rect localRect2 = this.mThumbDrawable.getBounds();
                    int i3 = localRect2.left;
                    int i4 = (localRect2.bottom - localRect2.top) / 2;
                    int i5 = this.mTrackDrawable.getIntrinsicWidth();
                    int i6 = i3 + this.mThumbW / 2 - i5 / 2;
                    this.mTrackDrawable.setBounds(i6, i4, i6 + i5, this.mList.getHeight() - i4);
                    this.mTrackDrawable.draw(paramCanvas);
                }
                paramCanvas.translate(0.0F, i);
                this.mThumbDrawable.draw(paramCanvas);
                paramCanvas.translate(0.0F, -i);
                if ((this.mState != 3) || (!this.mDrawOverlay))
                    break label648;
                if (this.mOverlayPosition == 1)
                    switch (this.mPosition)
                    {
                    default:
                    case 1:
                    }
                break;
            case 0:
            case 2:
            case 1:
            }
            for (int i1 = Math.max(0, this.mThumbDrawable.getBounds().left - this.mThumbW - this.mOverlaySize); ; i1 = Math.min(this.mThumbDrawable.getBounds().right + this.mThumbW, this.mList.getWidth() - this.mOverlaySize))
            {
                int i2 = Math.max(0, Math.min(i + (this.mThumbH - this.mOverlaySize) / 2, this.mList.getHeight() - this.mOverlaySize));
                RectF localRectF2 = this.mOverlayPos;
                localRectF2.left = i1;
                localRectF2.right = (localRectF2.left + this.mOverlaySize);
                localRectF2.top = i2;
                localRectF2.bottom = (localRectF2.top + this.mOverlaySize);
                if (this.mOverlayDrawable != null)
                    this.mOverlayDrawable.setBounds((int)localRectF2.left, (int)localRectF2.top, (int)localRectF2.right, (int)localRectF2.bottom);
                this.mOverlayDrawable.draw(paramCanvas);
                Paint localPaint = this.mPaint;
                float f = localPaint.descent();
                RectF localRectF1 = this.mOverlayPos;
                Rect localRect1 = this.mTmpRect;
                this.mOverlayDrawable.getPadding(localRect1);
                int m = (localRect1.right - localRect1.left) / 2;
                int n = (localRect1.bottom - localRect1.top) / 2;
                paramCanvas.drawText(this.mSectionText, (int)(localRectF1.left + localRectF1.right) / 2 - m, (int)(localRectF1.bottom + localRectF1.top) / 2 + this.mOverlaySize / 4 - f - n, localPaint);
                break;
                i7 = j - k * this.mThumbW / 208;
                break label96;
                i7 = -this.mThumbW + k * this.mThumbW / 208;
                break label96;
            }
            label648: if (this.mState == 4)
                if (k == 0)
                    setState(0);
                else if (this.mTrackDrawable != null)
                    this.mList.invalidate(j - this.mThumbW, 0, j, this.mList.getHeight());
                else
                    this.mList.invalidate(j - this.mThumbW, i, j, i + this.mThumbH);
        }
    }

    SectionIndexer getSectionIndexer()
    {
        return this.mSectionIndexer;
    }

    Object[] getSections()
    {
        if ((this.mListAdapter == null) && (this.mList != null))
            getSectionsFromIndexer();
        return this.mSections;
    }

    void getSectionsFromIndexer()
    {
        Object localObject = this.mList.getAdapter();
        this.mSectionIndexer = null;
        if ((localObject instanceof HeaderViewListAdapter))
        {
            this.mListOffset = ((HeaderViewListAdapter)localObject).getHeadersCount();
            localObject = ((HeaderViewListAdapter)localObject).getWrappedAdapter();
        }
        if ((localObject instanceof ExpandableListConnector))
        {
            ExpandableListAdapter localExpandableListAdapter = ((ExpandableListConnector)localObject).getAdapter();
            if ((localExpandableListAdapter instanceof SectionIndexer))
            {
                this.mSectionIndexer = ((SectionIndexer)localExpandableListAdapter);
                this.mListAdapter = ((BaseAdapter)localObject);
                this.mSections = this.mSectionIndexer.getSections();
            }
        }
        while (true)
        {
            return;
            if ((localObject instanceof SectionIndexer))
            {
                this.mListAdapter = ((BaseAdapter)localObject);
                this.mSectionIndexer = ((SectionIndexer)localObject);
                this.mSections = this.mSectionIndexer.getSections();
                if (this.mSections == null)
                {
                    String[] arrayOfString2 = new String[1];
                    arrayOfString2[0] = " ";
                    this.mSections = arrayOfString2;
                }
            }
            else
            {
                this.mListAdapter = ((BaseAdapter)localObject);
                String[] arrayOfString1 = new String[1];
                arrayOfString1[0] = " ";
                this.mSections = arrayOfString1;
            }
        }
    }

    public int getState()
    {
        return this.mState;
    }

    public int getWidth()
    {
        return this.mThumbW;
    }

    public boolean isAlwaysShowEnabled()
    {
        return this.mAlwaysShow;
    }

    boolean isPointInside(float paramFloat1, float paramFloat2)
    {
        boolean bool1 = true;
        boolean bool2;
        switch (this.mPosition)
        {
        default:
            if (paramFloat1 > this.mList.getWidth() - this.mThumbW)
            {
                bool2 = bool1;
                if ((!bool2) || ((this.mTrackDrawable == null) && ((paramFloat2 < this.mThumbY) || (paramFloat2 > this.mThumbY + this.mThumbH))))
                    break label112;
            }
            break;
        case 1:
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            if (paramFloat1 < this.mThumbW);
            for (bool2 = bool1; ; bool2 = false)
                break;
            label112: bool1 = false;
        }
    }

    boolean isVisible()
    {
        if (this.mState != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        switch (paramMotionEvent.getActionMasked())
        {
        case 2:
        default:
        case 0:
        case 1:
        case 3:
        }
        while (true)
        {
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                if ((this.mState <= 0) || (!isPointInside(paramMotionEvent.getX(), paramMotionEvent.getY())))
                    break;
                if (this.mList.isInScrollingContainer())
                    break label81;
                beginDrag();
            }
            label81: this.mInitialTouchY = paramMotionEvent.getY();
            startPendingDrag();
            continue;
            cancelPendingDrag();
        }
    }

    void onItemCountChanged(int paramInt1, int paramInt2)
    {
        if (this.mAlwaysShow)
            this.mLongList = true;
    }

    void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool;
        if ((this.mItemCount != paramInt3) && (paramInt2 > 0))
        {
            this.mItemCount = paramInt3;
            if (this.mItemCount / paramInt2 >= MIN_PAGES)
            {
                bool = true;
                this.mLongList = bool;
            }
        }
        else
        {
            if (this.mAlwaysShow)
                this.mLongList = true;
            if (this.mLongList)
                break label78;
            if (this.mState != 0)
                setState(0);
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label78: if ((paramInt3 - paramInt2 > 0) && (this.mState != 3))
            {
                this.mThumbY = getThumbPositionForListPosition(paramInt1, paramInt2, paramInt3);
                if (this.mChangedBounds)
                {
                    resetThumbPos();
                    this.mChangedBounds = false;
                }
            }
            this.mScrollCompleted = true;
            if (paramInt1 != this.mVisibleItem)
            {
                this.mVisibleItem = paramInt1;
                if (this.mState != 3)
                {
                    setState(2);
                    if (!this.mAlwaysShow)
                        this.mHandler.postDelayed(this.mScrollFade, 1500L);
                }
            }
        }
    }

    public void onSectionsChanged()
    {
        this.mListAdapter = null;
    }

    void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (this.mThumbDrawable != null)
            switch (this.mPosition)
            {
            default:
                this.mThumbDrawable.setBounds(paramInt1 - this.mThumbW, 0, paramInt1, this.mThumbH);
            case 1:
            }
        while (true)
        {
            if (this.mOverlayPosition == 0)
            {
                RectF localRectF = this.mOverlayPos;
                localRectF.left = ((paramInt1 - this.mOverlaySize) / 2);
                localRectF.right = (localRectF.left + this.mOverlaySize);
                localRectF.top = (paramInt2 / 10);
                localRectF.bottom = (localRectF.top + this.mOverlaySize);
                if (this.mOverlayDrawable != null)
                    this.mOverlayDrawable.setBounds((int)localRectF.left, (int)localRectF.top, (int)localRectF.right, (int)localRectF.bottom);
            }
            return;
            this.mThumbDrawable.setBounds(0, 0, this.mThumbW, this.mThumbH);
        }
    }

    boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if (this.mState == 0);
        while (true)
        {
            return bool;
            int i = paramMotionEvent.getAction();
            if (i == 0)
            {
                if (isPointInside(paramMotionEvent.getX(), paramMotionEvent.getY()))
                    if (!this.mList.isInScrollingContainer())
                    {
                        beginDrag();
                        bool = true;
                    }
                    else
                    {
                        this.mInitialTouchY = paramMotionEvent.getY();
                        startPendingDrag();
                    }
            }
            else
            {
                if (i == 1)
                {
                    int m;
                    int n;
                    if (this.mPendingDrag)
                    {
                        beginDrag();
                        m = this.mList.getHeight();
                        n = 10 + ((int)paramMotionEvent.getY() - this.mThumbH);
                        if (n >= 0)
                            break label228;
                        n = 0;
                    }
                    while (true)
                    {
                        this.mThumbY = n;
                        scrollTo(this.mThumbY / (m - this.mThumbH));
                        cancelPendingDrag();
                        if (this.mState != 3)
                            break;
                        if (this.mList != null)
                        {
                            this.mList.requestDisallowInterceptTouchEvent(false);
                            this.mList.reportScrollStateChange(0);
                        }
                        setState(2);
                        Handler localHandler = this.mHandler;
                        localHandler.removeCallbacks(this.mScrollFade);
                        if (!this.mAlwaysShow)
                            localHandler.postDelayed(this.mScrollFade, 1000L);
                        this.mList.invalidate();
                        bool = true;
                        break;
                        label228: if (n + this.mThumbH > m)
                            n = m - this.mThumbH;
                    }
                }
                if (i == 2)
                {
                    if ((this.mPendingDrag) && (Math.abs(paramMotionEvent.getY() - this.mInitialTouchY) > this.mScaledTouchSlop))
                    {
                        setState(3);
                        if ((this.mListAdapter == null) && (this.mList != null))
                            getSectionsFromIndexer();
                        if (this.mList != null)
                        {
                            this.mList.requestDisallowInterceptTouchEvent(true);
                            this.mList.reportScrollStateChange(1);
                        }
                        cancelFling();
                        cancelPendingDrag();
                    }
                    if (this.mState == 3)
                    {
                        int j = this.mList.getHeight();
                        int k = 10 + ((int)paramMotionEvent.getY() - this.mThumbH);
                        if (k < 0)
                            k = 0;
                        while (true)
                        {
                            if (Math.abs(this.mThumbY - k) >= 2)
                                break label422;
                            bool = true;
                            break;
                            if (k + this.mThumbH > j)
                                k = j - this.mThumbH;
                        }
                        label422: this.mThumbY = k;
                        if (this.mScrollCompleted)
                            scrollTo(this.mThumbY / (j - this.mThumbH));
                        bool = true;
                    }
                }
                else if (i == 3)
                {
                    cancelPendingDrag();
                }
            }
        }
    }

    void scrollTo(float paramFloat)
    {
        int i = this.mList.getCount();
        this.mScrollCompleted = false;
        float f1 = 1.0F / i / 8.0F;
        Object[] arrayOfObject = this.mSections;
        int k;
        int i4;
        int i5;
        label209: float f2;
        float f3;
        int i9;
        label311: boolean bool;
        if ((arrayOfObject != null) && (arrayOfObject.length > 1))
        {
            int n = arrayOfObject.length;
            int i1 = (int)(paramFloat * n);
            if (i1 >= n)
                i1 = n - 1;
            int i2 = i1;
            k = i1;
            int i3 = this.mSectionIndexer.getPositionForSection(i1);
            i4 = i;
            i5 = i3;
            int i6 = i1;
            int i7 = i1 + 1;
            if (i1 < n - 1)
                i4 = this.mSectionIndexer.getPositionForSection(i1 + 1);
            if (i4 == i3)
                if (i1 > 0)
                {
                    i1--;
                    i5 = this.mSectionIndexer.getPositionForSection(i1);
                    if (i5 == i3)
                        break label209;
                    i6 = i1;
                }
            for (k = i1; ; k = 0)
            {
                int i8 = i7 + 1;
                while ((i8 < n) && (this.mSectionIndexer.getPositionForSection(i8) == i4))
                {
                    i8++;
                    i7++;
                }
                if (i1 != 0)
                    break;
            }
            f2 = i6 / n;
            f3 = i7 / n;
            if ((i6 == i2) && (paramFloat - f2 < f1))
            {
                i9 = i5;
                if (i9 > i - 1)
                    i9 = i - 1;
                if (!(this.mList instanceof ExpandableListView))
                    break label400;
                ExpandableListView localExpandableListView2 = (ExpandableListView)this.mList;
                localExpandableListView2.setSelectionFromTop(localExpandableListView2.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(i9 + this.mListOffset)), 0);
                if (k < 0)
                    break label569;
                String str = arrayOfObject[k].toString();
                this.mSectionText = str;
                if ((str.length() == 1) && (str.charAt(0) == ' '))
                    break label563;
                int m = arrayOfObject.length;
                if (k >= m)
                    break label563;
                bool = true;
            }
        }
        label367: for (this.mDrawOverlay = bool; ; this.mDrawOverlay = false)
        {
            return;
            i9 = i5 + (int)((i4 - i5) * (paramFloat - f2) / (f3 - f2));
            break;
            if ((this.mList instanceof ListView))
            {
                ((ListView)this.mList).setSelectionFromTop(i9 + this.mListOffset, 0);
                break label311;
            }
            this.mList.setSelection(i9 + this.mListOffset);
            break label311;
            int j = (int)(paramFloat * i);
            if (j > i - 1)
                j = i - 1;
            if ((this.mList instanceof ExpandableListView))
            {
                ExpandableListView localExpandableListView1 = (ExpandableListView)this.mList;
                localExpandableListView1.setSelectionFromTop(localExpandableListView1.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(j + this.mListOffset)), 0);
            }
            while (true)
            {
                k = -1;
                break;
                if ((this.mList instanceof ListView))
                    ((ListView)this.mList).setSelectionFromTop(j + this.mListOffset, 0);
                else
                    this.mList.setSelection(j + this.mListOffset);
            }
            bool = false;
            break label367;
        }
    }

    public void setAlwaysShow(boolean paramBoolean)
    {
        this.mAlwaysShow = paramBoolean;
        if (paramBoolean)
        {
            this.mHandler.removeCallbacks(this.mScrollFade);
            setState(2);
        }
        while (true)
        {
            return;
            if (this.mState == 2)
                this.mHandler.postDelayed(this.mScrollFade, 1500L);
        }
    }

    public void setScrollbarPosition(int paramInt)
    {
        this.mPosition = paramInt;
        switch (paramInt)
        {
        default:
        case 1:
        }
        for (this.mOverlayDrawable = this.mOverlayDrawableRight; ; this.mOverlayDrawable = this.mOverlayDrawableLeft)
            return;
    }

    public void setState(int paramInt)
    {
        switch (paramInt)
        {
        case 1:
        default:
        case 0:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            this.mState = paramInt;
            refreshDrawableState();
            return;
            this.mHandler.removeCallbacks(this.mScrollFade);
            this.mList.invalidate();
            continue;
            if (this.mState != 2)
                resetThumbPos();
            this.mHandler.removeCallbacks(this.mScrollFade);
            continue;
            int i = this.mList.getWidth();
            this.mList.invalidate(i - this.mThumbW, this.mThumbY, i, this.mThumbY + this.mThumbH);
        }
    }

    void startPendingDrag()
    {
        this.mPendingDrag = true;
        this.mList.postDelayed(this.mDeferStartDrag, 180L);
    }

    void stop()
    {
        setState(0);
    }

    public class ScrollFade
        implements Runnable
    {
        static final int ALPHA_MAX = 208;
        static final long FADE_DURATION = 200L;
        long mFadeDuration;
        long mStartTime;

        public ScrollFade()
        {
        }

        int getAlpha()
        {
            int i;
            if (FastScroller.this.getState() != 4)
                i = 208;
            while (true)
            {
                return i;
                long l = SystemClock.uptimeMillis();
                if (l > this.mStartTime + this.mFadeDuration)
                    i = 0;
                else
                    i = (int)(208L - 208L * (l - this.mStartTime) / this.mFadeDuration);
            }
        }

        public void run()
        {
            if (FastScroller.this.getState() != 4)
                startFade();
            while (true)
            {
                return;
                if (getAlpha() > 0)
                    FastScroller.this.mList.invalidate();
                else
                    FastScroller.this.setState(0);
            }
        }

        void startFade()
        {
            this.mFadeDuration = 200L;
            this.mStartTime = SystemClock.uptimeMillis();
            FastScroller.this.setState(4);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.FastScroller
 * JD-Core Version:        0.6.2
 */