package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.util.Log;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

public class OverScroller
{
    private static final int DEFAULT_DURATION = 250;
    private static final int FLING_MODE = 1;
    private static final int SCROLL_MODE;
    private final boolean mFlywheel;
    private Interpolator mInterpolator;
    private int mMode;
    private final SplineOverScroller mScrollerX;
    private final SplineOverScroller mScrollerY;

    public OverScroller(Context paramContext)
    {
        this(paramContext, null);
    }

    public OverScroller(Context paramContext, Interpolator paramInterpolator)
    {
        this(paramContext, paramInterpolator, true);
    }

    public OverScroller(Context paramContext, Interpolator paramInterpolator, float paramFloat1, float paramFloat2)
    {
        this(paramContext, paramInterpolator, true);
    }

    public OverScroller(Context paramContext, Interpolator paramInterpolator, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramContext, paramInterpolator, paramBoolean);
    }

    public OverScroller(Context paramContext, Interpolator paramInterpolator, boolean paramBoolean)
    {
        this.mInterpolator = paramInterpolator;
        this.mFlywheel = paramBoolean;
        this.mScrollerX = new SplineOverScroller();
        this.mScrollerY = new SplineOverScroller();
        SplineOverScroller.initFromContext(paramContext);
    }

    public void abortAnimation()
    {
        this.mScrollerX.finish();
        this.mScrollerY.finish();
    }

    public boolean computeScrollOffset()
    {
        boolean bool;
        if (isFinished())
        {
            bool = false;
            return bool;
        }
        switch (this.mMode)
        {
        default:
        case 0:
        case 1:
        }
        while (true)
        {
            bool = true;
            break;
            long l = AnimationUtils.currentAnimationTimeMillis() - this.mScrollerX.mStartTime;
            int i = this.mScrollerX.mDuration;
            if (l < i)
            {
                float f1 = (float)l / i;
                if (this.mInterpolator == null);
                for (float f2 = Scroller.viscousFluid(f1); ; f2 = this.mInterpolator.getInterpolation(f1))
                {
                    this.mScrollerX.updateScroll(f2);
                    this.mScrollerY.updateScroll(f2);
                    break;
                }
            }
            abortAnimation();
            continue;
            if ((!this.mScrollerX.mFinished) && (!this.mScrollerX.update()) && (!this.mScrollerX.continueWhenFinished()))
                this.mScrollerX.finish();
            if ((!this.mScrollerY.mFinished) && (!this.mScrollerY.update()) && (!this.mScrollerY.continueWhenFinished()))
                this.mScrollerY.finish();
        }
    }

    @Deprecated
    public void extendDuration(int paramInt)
    {
        this.mScrollerX.extendDuration(paramInt);
        this.mScrollerY.extendDuration(paramInt);
    }

    public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        fling(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, 0, 0);
    }

    public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10)
    {
        if ((this.mFlywheel) && (!isFinished()))
        {
            float f1 = this.mScrollerX.mCurrVelocity;
            float f2 = this.mScrollerY.mCurrVelocity;
            if ((Math.signum(paramInt3) == Math.signum(f1)) && (Math.signum(paramInt4) == Math.signum(f2)))
            {
                paramInt3 = (int)(f1 + paramInt3);
                paramInt4 = (int)(f2 + paramInt4);
            }
        }
        this.mMode = 1;
        this.mScrollerX.fling(paramInt1, paramInt3, paramInt5, paramInt6, paramInt9);
        this.mScrollerY.fling(paramInt2, paramInt4, paramInt7, paramInt8, paramInt10);
    }

    public final void forceFinished(boolean paramBoolean)
    {
        SplineOverScroller.access$002(this.mScrollerX, SplineOverScroller.access$002(this.mScrollerY, paramBoolean));
    }

    public float getCurrVelocity()
    {
        return FloatMath.sqrt(this.mScrollerX.mCurrVelocity * this.mScrollerX.mCurrVelocity + this.mScrollerY.mCurrVelocity * this.mScrollerY.mCurrVelocity);
    }

    public final int getCurrX()
    {
        return this.mScrollerX.mCurrentPosition;
    }

    public final int getCurrY()
    {
        return this.mScrollerY.mCurrentPosition;
    }

    @Deprecated
    public final int getDuration()
    {
        return Math.max(this.mScrollerX.mDuration, this.mScrollerY.mDuration);
    }

    public final int getFinalX()
    {
        return this.mScrollerX.mFinal;
    }

    public final int getFinalY()
    {
        return this.mScrollerY.mFinal;
    }

    public final int getStartX()
    {
        return this.mScrollerX.mStart;
    }

    public final int getStartY()
    {
        return this.mScrollerY.mStart;
    }

    public final boolean isFinished()
    {
        if ((this.mScrollerX.mFinished) && (this.mScrollerY.mFinished));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isOverScrolled()
    {
        if (((!this.mScrollerX.mFinished) && (this.mScrollerX.mState != 0)) || ((!this.mScrollerY.mFinished) && (this.mScrollerY.mState != 0)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isScrollingInDirection(float paramFloat1, float paramFloat2)
    {
        int i = this.mScrollerX.mFinal - this.mScrollerX.mStart;
        int j = this.mScrollerY.mFinal - this.mScrollerY.mStart;
        if ((!isFinished()) && (Math.signum(paramFloat1) == Math.signum(i)) && (Math.signum(paramFloat2) == Math.signum(j)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void notifyHorizontalEdgeReached(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mScrollerX.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
    }

    public void notifyVerticalEdgeReached(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mScrollerY.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
    }

    @Deprecated
    public void setFinalX(int paramInt)
    {
        this.mScrollerX.setFinalPosition(paramInt);
    }

    @Deprecated
    public void setFinalY(int paramInt)
    {
        this.mScrollerY.setFinalPosition(paramInt);
    }

    public final void setFriction(float paramFloat)
    {
        this.mScrollerX.setFriction(paramFloat);
        this.mScrollerY.setFriction(paramFloat);
    }

    void setInterpolator(Interpolator paramInterpolator)
    {
        this.mInterpolator = paramInterpolator;
    }

    public boolean springBack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        int i = 1;
        this.mMode = i;
        boolean bool1 = this.mScrollerX.springback(paramInt1, paramInt3, paramInt4);
        boolean bool2 = this.mScrollerY.springback(paramInt2, paramInt5, paramInt6);
        if ((bool1) || (bool2));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        startScroll(paramInt1, paramInt2, paramInt3, paramInt4, 250);
    }

    public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        this.mMode = 0;
        this.mScrollerX.startScroll(paramInt1, paramInt3, paramInt5);
        this.mScrollerY.startScroll(paramInt2, paramInt4, paramInt5);
    }

    public int timePassed()
    {
        return (int)(AnimationUtils.currentAnimationTimeMillis() - Math.min(this.mScrollerX.mStartTime, this.mScrollerY.mStartTime));
    }

    static class SplineOverScroller
    {
        private static final int BALLISTIC = 2;
        private static final int CUBIC = 1;
        private static float DECELERATION_RATE = 0.0F;
        private static final float END_TENSION = 1.0F;
        private static final float GRAVITY = 2000.0F;
        private static final float INFLEXION = 0.35F;
        private static final int NB_SAMPLES = 100;
        private static final float P1 = 0.175F;
        private static final float P2 = 0.35F;
        private static float PHYSICAL_COEF = 0.0F;
        private static final int SPLINE = 0;
        private static final float[] SPLINE_POSITION = new float[101];
        private static final float[] SPLINE_TIME = new float[101];
        private static final float START_TENSION = 0.5F;
        private float mCurrVelocity;
        private int mCurrentPosition;
        private float mDeceleration;
        private int mDuration;
        private int mFinal;
        private boolean mFinished = true;
        private float mFlingFriction = ViewConfiguration.getScrollFriction();
        private int mOver;
        private int mSplineDistance;
        private int mSplineDuration;
        private int mStart;
        private long mStartTime;
        private int mState = 0;
        private int mVelocity;

        static
        {
            float f1 = 0.0F;
            float f2 = 0.0F;
            int i = 0;
            if (i < 100)
            {
                float f3 = i / 100.0F;
                float f4 = 1.0F;
                label53: float f5 = f1 + (f4 - f1) / 2.0F;
                float f6 = 3.0F * f5 * (1.0F - f5);
                float f7 = f6 * (0.175F * (1.0F - f5) + 0.35F * f5) + f5 * (f5 * f5);
                float f8;
                if (Math.abs(f7 - f3) < 1.E-05D)
                {
                    SPLINE_POSITION[i] = (f6 * (f5 + 0.5F * (1.0F - f5)) + f5 * (f5 * f5));
                    f8 = 1.0F;
                }
                while (true)
                {
                    float f9 = f2 + (f8 - f2) / 2.0F;
                    float f10 = 3.0F * f9 * (1.0F - f9);
                    float f11 = f10 * (f9 + 0.5F * (1.0F - f9)) + f9 * (f9 * f9);
                    if (Math.abs(f11 - f3) < 1.E-05D)
                    {
                        SPLINE_TIME[i] = (f10 * (0.175F * (1.0F - f9) + 0.35F * f9) + f9 * (f9 * f9));
                        i++;
                        break;
                        if (f7 > f3)
                        {
                            f4 = f5;
                            break label53;
                        }
                        f1 = f5;
                        break label53;
                    }
                    if (f11 > f3)
                        f8 = f9;
                    else
                        f2 = f9;
                }
            }
            float[] arrayOfFloat = SPLINE_POSITION;
            SPLINE_TIME[100] = 1.0F;
            arrayOfFloat[100] = 1.0F;
        }

        private void adjustDuration(int paramInt1, int paramInt2, int paramInt3)
        {
            int i = paramInt2 - paramInt1;
            float f1 = Math.abs((paramInt3 - paramInt1) / i);
            int j = (int)(100.0F * f1);
            if (j < 100)
            {
                float f2 = j / 100.0F;
                float f3 = (j + 1) / 100.0F;
                float f4 = SPLINE_TIME[j];
                float f5 = SPLINE_TIME[(j + 1)];
                this.mDuration = ((int)((f4 + (f1 - f2) / (f3 - f2) * (f5 - f4)) * this.mDuration));
            }
        }

        private void fitOnBounceCurve(int paramInt1, int paramInt2, int paramInt3)
        {
            float f1 = -paramInt3 / this.mDeceleration;
            float f2 = (float)Math.sqrt(2.0D * (paramInt3 * paramInt3 / 2.0F / Math.abs(this.mDeceleration) + Math.abs(paramInt2 - paramInt1)) / Math.abs(this.mDeceleration));
            this.mStartTime -= (int)(1000.0F * (f2 - f1));
            this.mStart = paramInt2;
            this.mVelocity = ((int)(f2 * -this.mDeceleration));
        }

        private static float getDeceleration(int paramInt)
        {
            if (paramInt > 0);
            for (float f = -2000.0F; ; f = 2000.0F)
                return f;
        }

        private double getSplineDeceleration(int paramInt)
        {
            return Math.log(0.35F * Math.abs(paramInt) / (this.mFlingFriction * PHYSICAL_COEF));
        }

        private double getSplineFlingDistance(int paramInt)
        {
            double d1 = getSplineDeceleration(paramInt);
            double d2 = DECELERATION_RATE - 1.0D;
            return this.mFlingFriction * PHYSICAL_COEF * Math.exp(d1 * (DECELERATION_RATE / d2));
        }

        private int getSplineFlingDuration(int paramInt)
        {
            return (int)(1000.0D * Math.exp(getSplineDeceleration(paramInt) / (DECELERATION_RATE - 1.0D)));
        }

        static void initFromContext(Context paramContext)
        {
            PHYSICAL_COEF = 0.84F * (386.0878F * (160.0F * paramContext.getResources().getDisplayMetrics().density));
        }

        private void onEdgeReached()
        {
            float f1 = this.mVelocity * this.mVelocity / (2.0F * Math.abs(this.mDeceleration));
            float f2 = Math.signum(this.mVelocity);
            if (f1 > this.mOver)
            {
                this.mDeceleration = (-f2 * this.mVelocity * this.mVelocity / (2.0F * this.mOver));
                f1 = this.mOver;
            }
            this.mOver = ((int)f1);
            this.mState = 2;
            int i = this.mStart;
            if (this.mVelocity > 0);
            while (true)
            {
                this.mFinal = (i + (int)f1);
                this.mDuration = (-(int)(1000.0F * this.mVelocity / this.mDeceleration));
                return;
                f1 = -f1;
            }
        }

        private void startAfterEdge(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            if ((paramInt1 > paramInt2) && (paramInt1 < paramInt3))
            {
                Log.e("OverScroller", "startAfterEdge called from a valid position");
                this.mFinished = true;
            }
            while (true)
            {
                return;
                int i;
                label32: int j;
                label40: int k;
                if (paramInt1 > paramInt3)
                {
                    i = 1;
                    if (i == 0)
                        break label80;
                    j = paramInt3;
                    k = paramInt1 - j;
                    if (k * paramInt4 < 0)
                        break label86;
                }
                label80: label86: for (int m = 1; ; m = 0)
                {
                    if (m == 0)
                        break label92;
                    startBounceAfterEdge(paramInt1, j, paramInt4);
                    break;
                    i = 0;
                    break label32;
                    j = paramInt2;
                    break label40;
                }
                label92: if (getSplineFlingDistance(paramInt4) > Math.abs(k))
                {
                    int n;
                    if (i != 0)
                    {
                        n = paramInt2;
                        label116: if (i == 0)
                            break label148;
                    }
                    for (int i1 = paramInt1; ; i1 = paramInt3)
                    {
                        fling(paramInt1, paramInt4, n, i1, this.mOver);
                        break;
                        n = paramInt1;
                        break label116;
                    }
                }
                label148: startSpringback(paramInt1, j, paramInt4);
            }
        }

        private void startBounceAfterEdge(int paramInt1, int paramInt2, int paramInt3)
        {
            if (paramInt3 == 0);
            for (int i = paramInt1 - paramInt2; ; i = paramInt3)
            {
                this.mDeceleration = getDeceleration(i);
                fitOnBounceCurve(paramInt1, paramInt2, paramInt3);
                onEdgeReached();
                return;
            }
        }

        private void startSpringback(int paramInt1, int paramInt2, int paramInt3)
        {
            this.mFinished = false;
            this.mState = 1;
            this.mStart = paramInt1;
            this.mFinal = paramInt2;
            int i = paramInt1 - paramInt2;
            this.mDeceleration = getDeceleration(i);
            this.mVelocity = (-i);
            this.mOver = Math.abs(i);
            this.mDuration = ((int)(1000.0D * Math.sqrt(-2.0D * i / this.mDeceleration)));
        }

        boolean continueWhenFinished()
        {
            boolean bool = false;
            switch (this.mState)
            {
            default:
            case 1:
            case 0:
            case 2:
            }
            while (true)
            {
                update();
                bool = true;
                do
                    return bool;
                while (this.mDuration >= this.mSplineDuration);
                this.mStart = this.mFinal;
                this.mVelocity = ((int)this.mCurrVelocity);
                this.mDeceleration = getDeceleration(this.mVelocity);
                this.mStartTime += this.mDuration;
                onEdgeReached();
                continue;
                this.mStartTime += this.mDuration;
                startSpringback(this.mFinal, this.mStart, 0);
            }
        }

        void extendDuration(int paramInt)
        {
            this.mDuration = (paramInt + (int)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime));
            this.mFinished = false;
        }

        void finish()
        {
            this.mCurrentPosition = this.mFinal;
            this.mFinished = true;
        }

        void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.mOver = paramInt5;
            this.mFinished = false;
            this.mVelocity = paramInt2;
            this.mCurrVelocity = paramInt2;
            this.mSplineDuration = 0;
            this.mDuration = 0;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mStart = paramInt1;
            this.mCurrentPosition = paramInt1;
            if ((paramInt1 > paramInt4) || (paramInt1 < paramInt3))
                startAfterEdge(paramInt1, paramInt3, paramInt4, paramInt2);
            while (true)
            {
                return;
                this.mState = 0;
                double d = 0.0D;
                if (paramInt2 != 0)
                {
                    int i = getSplineFlingDuration(paramInt2);
                    this.mSplineDuration = i;
                    this.mDuration = i;
                    d = getSplineFlingDistance(paramInt2);
                }
                this.mSplineDistance = ((int)(d * Math.signum(paramInt2)));
                this.mFinal = (paramInt1 + this.mSplineDistance);
                if (this.mFinal < paramInt3)
                {
                    adjustDuration(this.mStart, this.mFinal, paramInt3);
                    this.mFinal = paramInt3;
                }
                if (this.mFinal > paramInt4)
                {
                    adjustDuration(this.mStart, this.mFinal, paramInt4);
                    this.mFinal = paramInt4;
                }
            }
        }

        void notifyEdgeReached(int paramInt1, int paramInt2, int paramInt3)
        {
            if (this.mState == 0)
            {
                this.mOver = paramInt3;
                this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
                startAfterEdge(paramInt1, paramInt2, paramInt2, (int)this.mCurrVelocity);
            }
        }

        void setFinalPosition(int paramInt)
        {
            this.mFinal = paramInt;
            this.mFinished = false;
        }

        void setFriction(float paramFloat)
        {
            this.mFlingFriction = paramFloat;
        }

        boolean springback(int paramInt1, int paramInt2, int paramInt3)
        {
            boolean bool = true;
            this.mFinished = bool;
            this.mFinal = paramInt1;
            this.mStart = paramInt1;
            this.mVelocity = 0;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = 0;
            if (paramInt1 < paramInt2)
            {
                startSpringback(paramInt1, paramInt2, 0);
                if (this.mFinished)
                    break label73;
            }
            while (true)
            {
                return bool;
                if (paramInt1 <= paramInt3)
                    break;
                startSpringback(paramInt1, paramInt3, 0);
                break;
                label73: bool = false;
            }
        }

        void startScroll(int paramInt1, int paramInt2, int paramInt3)
        {
            this.mFinished = false;
            this.mStart = paramInt1;
            this.mFinal = (paramInt1 + paramInt2);
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = paramInt3;
            this.mDeceleration = 0.0F;
            this.mVelocity = 0;
        }

        boolean update()
        {
            long l = AnimationUtils.currentAnimationTimeMillis() - this.mStartTime;
            boolean bool;
            if (l > this.mDuration)
            {
                bool = false;
                return bool;
            }
            double d = 0.0D;
            switch (this.mState)
            {
            default:
            case 0:
            case 2:
            case 1:
            }
            while (true)
            {
                this.mCurrentPosition = (this.mStart + (int)Math.round(d));
                bool = true;
                break;
                float f5 = (float)l / this.mSplineDuration;
                int i = (int)(100.0F * f5);
                float f6 = 1.0F;
                float f7 = 0.0F;
                if (i < 100)
                {
                    float f8 = i / 100.0F;
                    float f9 = (i + 1) / 100.0F;
                    float f10 = SPLINE_POSITION[i];
                    f7 = (SPLINE_POSITION[(i + 1)] - f10) / (f9 - f8);
                    f6 = f10 + f7 * (f5 - f8);
                }
                d = f6 * this.mSplineDistance;
                this.mCurrVelocity = (1000.0F * (f7 * this.mSplineDistance / this.mSplineDuration));
                continue;
                float f4 = (float)l / 1000.0F;
                this.mCurrVelocity = (this.mVelocity + f4 * this.mDeceleration);
                d = f4 * this.mVelocity + f4 * (f4 * this.mDeceleration) / 2.0F;
                continue;
                float f1 = (float)l / this.mDuration;
                float f2 = f1 * f1;
                float f3 = Math.signum(this.mVelocity);
                d = f3 * this.mOver * (3.0F * f2 - f2 * (2.0F * f1));
                this.mCurrVelocity = (6.0F * (f3 * this.mOver) * (f2 + -f1));
            }
        }

        void updateScroll(float paramFloat)
        {
            this.mCurrentPosition = (this.mStart + Math.round(paramFloat * (this.mFinal - this.mStart)));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.widget.OverScroller
 * JD-Core Version:        0.6.2
 */