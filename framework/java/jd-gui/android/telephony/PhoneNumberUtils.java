package android.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Country;
import android.location.CountryDetector;
import android.net.Uri;
import android.os.SystemProperties;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import com.android.i18n.phonenumbers.NumberParseException;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.android.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.android.i18n.phonenumbers.ShortNumberUtil;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberUtils
{
    private static final int CCC_LENGTH = 0;
    private static final String CLIR_OFF = "#31#";
    private static final String CLIR_ON = "*31#";
    private static final boolean[] COUNTRY_CALLING_CALL;
    private static final boolean DBG = false;
    public static final int FORMAT_JAPAN = 2;
    public static final int FORMAT_NANP = 1;
    public static final int FORMAT_UNKNOWN = 0;
    private static final Pattern GLOBAL_PHONE_NUMBER_PATTERN = Pattern.compile("[\\+]?[0-9.-]+");
    private static final SparseIntArray KEYPAD_MAP;
    static final String LOG_TAG = "PhoneNumberUtils";
    static final int MIN_MATCH = 7;
    private static final String[] NANP_COUNTRIES;
    private static final String NANP_IDP_STRING = "011";
    private static final int NANP_LENGTH = 10;
    private static final int NANP_STATE_DASH = 4;
    private static final int NANP_STATE_DIGIT = 1;
    private static final int NANP_STATE_ONE = 3;
    private static final int NANP_STATE_PLUS = 2;
    public static final char PAUSE = ',';
    private static final char PLUS_SIGN_CHAR = '+';
    private static final String PLUS_SIGN_STRING = "+";
    public static final int TOA_International = 145;
    public static final int TOA_Unknown = 129;
    public static final char WAIT = ';';
    public static final char WILD = 'N';

    static
    {
        String[] arrayOfString = new String[24];
        arrayOfString[0] = "US";
        arrayOfString[1] = "CA";
        arrayOfString[2] = "AS";
        arrayOfString[3] = "AI";
        arrayOfString[4] = "AG";
        arrayOfString[5] = "BS";
        arrayOfString[6] = "BB";
        arrayOfString[7] = "BM";
        arrayOfString[8] = "VG";
        arrayOfString[9] = "KY";
        arrayOfString[10] = "DM";
        arrayOfString[11] = "DO";
        arrayOfString[12] = "GD";
        arrayOfString[13] = "GU";
        arrayOfString[14] = "JM";
        arrayOfString[15] = "PR";
        arrayOfString[16] = "MS";
        arrayOfString[17] = "MP";
        arrayOfString[18] = "KN";
        arrayOfString[19] = "LC";
        arrayOfString[20] = "VC";
        arrayOfString[21] = "TT";
        arrayOfString[22] = "TC";
        arrayOfString[23] = "VI";
        NANP_COUNTRIES = arrayOfString;
        KEYPAD_MAP = new SparseIntArray();
        KEYPAD_MAP.put(97, 50);
        KEYPAD_MAP.put(98, 50);
        KEYPAD_MAP.put(99, 50);
        KEYPAD_MAP.put(65, 50);
        KEYPAD_MAP.put(66, 50);
        KEYPAD_MAP.put(67, 50);
        KEYPAD_MAP.put(100, 51);
        KEYPAD_MAP.put(101, 51);
        KEYPAD_MAP.put(102, 51);
        KEYPAD_MAP.put(68, 51);
        KEYPAD_MAP.put(69, 51);
        KEYPAD_MAP.put(70, 51);
        KEYPAD_MAP.put(103, 52);
        KEYPAD_MAP.put(104, 52);
        KEYPAD_MAP.put(105, 52);
        KEYPAD_MAP.put(71, 52);
        KEYPAD_MAP.put(72, 52);
        KEYPAD_MAP.put(73, 52);
        KEYPAD_MAP.put(106, 53);
        KEYPAD_MAP.put(107, 53);
        KEYPAD_MAP.put(108, 53);
        KEYPAD_MAP.put(74, 53);
        KEYPAD_MAP.put(75, 53);
        KEYPAD_MAP.put(76, 53);
        KEYPAD_MAP.put(109, 54);
        KEYPAD_MAP.put(110, 54);
        KEYPAD_MAP.put(111, 54);
        KEYPAD_MAP.put(77, 54);
        KEYPAD_MAP.put(78, 54);
        KEYPAD_MAP.put(79, 54);
        KEYPAD_MAP.put(112, 55);
        KEYPAD_MAP.put(113, 55);
        KEYPAD_MAP.put(114, 55);
        KEYPAD_MAP.put(115, 55);
        KEYPAD_MAP.put(80, 55);
        KEYPAD_MAP.put(81, 55);
        KEYPAD_MAP.put(82, 55);
        KEYPAD_MAP.put(83, 55);
        KEYPAD_MAP.put(116, 56);
        KEYPAD_MAP.put(117, 56);
        KEYPAD_MAP.put(118, 56);
        KEYPAD_MAP.put(84, 56);
        KEYPAD_MAP.put(85, 56);
        KEYPAD_MAP.put(86, 56);
        KEYPAD_MAP.put(119, 57);
        KEYPAD_MAP.put(120, 57);
        KEYPAD_MAP.put(121, 57);
        KEYPAD_MAP.put(122, 57);
        KEYPAD_MAP.put(87, 57);
        KEYPAD_MAP.put(88, 57);
        KEYPAD_MAP.put(89, 57);
        KEYPAD_MAP.put(90, 57);
        boolean[] arrayOfBoolean = new boolean[100];
        arrayOfBoolean[0] = 1;
        arrayOfBoolean[1] = 1;
        arrayOfBoolean[2] = 0;
        arrayOfBoolean[3] = 0;
        arrayOfBoolean[4] = 0;
        arrayOfBoolean[5] = 0;
        arrayOfBoolean[6] = 0;
        arrayOfBoolean[7] = 1;
        arrayOfBoolean[8] = 0;
        arrayOfBoolean[9] = 0;
        arrayOfBoolean[10] = 0;
        arrayOfBoolean[11] = 0;
        arrayOfBoolean[12] = 0;
        arrayOfBoolean[13] = 0;
        arrayOfBoolean[14] = 0;
        arrayOfBoolean[15] = 0;
        arrayOfBoolean[16] = 0;
        arrayOfBoolean[17] = 0;
        arrayOfBoolean[18] = 0;
        arrayOfBoolean[19] = 0;
        arrayOfBoolean[20] = 1;
        arrayOfBoolean[21] = 0;
        arrayOfBoolean[22] = 0;
        arrayOfBoolean[23] = 0;
        arrayOfBoolean[24] = 0;
        arrayOfBoolean[25] = 0;
        arrayOfBoolean[26] = 0;
        arrayOfBoolean[27] = 1;
        arrayOfBoolean[28] = 1;
        arrayOfBoolean[29] = 0;
        arrayOfBoolean[30] = 1;
        arrayOfBoolean[31] = 1;
        arrayOfBoolean[32] = 1;
        arrayOfBoolean[33] = 1;
        arrayOfBoolean[34] = 1;
        arrayOfBoolean[35] = 0;
        arrayOfBoolean[36] = 1;
        arrayOfBoolean[37] = 0;
        arrayOfBoolean[38] = 0;
        arrayOfBoolean[39] = 1;
        arrayOfBoolean[40] = 1;
        arrayOfBoolean[41] = 0;
        arrayOfBoolean[42] = 0;
        arrayOfBoolean[43] = 1;
        arrayOfBoolean[44] = 1;
        arrayOfBoolean[45] = 1;
        arrayOfBoolean[46] = 1;
        arrayOfBoolean[47] = 1;
        arrayOfBoolean[48] = 1;
        arrayOfBoolean[49] = 1;
        arrayOfBoolean[50] = 0;
        arrayOfBoolean[51] = 1;
        arrayOfBoolean[52] = 1;
        arrayOfBoolean[53] = 1;
        arrayOfBoolean[54] = 1;
        arrayOfBoolean[55] = 1;
        arrayOfBoolean[56] = 1;
        arrayOfBoolean[57] = 1;
        arrayOfBoolean[58] = 1;
        arrayOfBoolean[59] = 0;
        arrayOfBoolean[60] = 1;
        arrayOfBoolean[61] = 1;
        arrayOfBoolean[62] = 1;
        arrayOfBoolean[63] = 1;
        arrayOfBoolean[64] = 1;
        arrayOfBoolean[65] = 1;
        arrayOfBoolean[66] = 1;
        arrayOfBoolean[67] = 0;
        arrayOfBoolean[68] = 0;
        arrayOfBoolean[69] = 0;
        arrayOfBoolean[70] = 0;
        arrayOfBoolean[71] = 0;
        arrayOfBoolean[72] = 0;
        arrayOfBoolean[73] = 0;
        arrayOfBoolean[74] = 0;
        arrayOfBoolean[75] = 0;
        arrayOfBoolean[76] = 0;
        arrayOfBoolean[77] = 0;
        arrayOfBoolean[78] = 0;
        arrayOfBoolean[79] = 0;
        arrayOfBoolean[80] = 0;
        arrayOfBoolean[81] = 1;
        arrayOfBoolean[82] = 1;
        arrayOfBoolean[83] = 1;
        arrayOfBoolean[84] = 1;
        arrayOfBoolean[85] = 0;
        arrayOfBoolean[86] = 1;
        arrayOfBoolean[87] = 0;
        arrayOfBoolean[88] = 0;
        arrayOfBoolean[89] = 1;
        arrayOfBoolean[90] = 1;
        arrayOfBoolean[91] = 1;
        arrayOfBoolean[92] = 1;
        arrayOfBoolean[93] = 1;
        arrayOfBoolean[94] = 1;
        arrayOfBoolean[95] = 1;
        arrayOfBoolean[96] = 0;
        arrayOfBoolean[97] = 0;
        arrayOfBoolean[98] = 1;
        arrayOfBoolean[99] = 0;
        COUNTRY_CALLING_CALL = arrayOfBoolean;
    }

    private static String appendPwCharBackToOrigDialStr(int paramInt, String paramString1, String paramString2)
    {
        if (paramInt == 1);
        for (String str = paramString1 + paramString2.charAt(0); ; str = paramString1.concat(paramString2.substring(0, paramInt)))
            return str;
    }

    private static char bcdToChar(byte paramByte)
    {
        char c;
        if (paramByte < 10)
            c = (char)(paramByte + 48);
        while (true)
        {
            return c;
            switch (paramByte)
            {
            default:
                c = '\000';
                break;
            case 10:
                c = '*';
                break;
            case 11:
                c = '#';
                break;
            case 12:
                c = ',';
                break;
            case 13:
                c = 'N';
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static int callIndexOfLastNetworkChar(String paramString)
    {
        return indexOfLastNetworkChar(paramString);
    }

    public static String calledPartyBCDFragmentToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramInt2 * 2);
        internalCalledPartyBCDFragmentToString(localStringBuilder, paramArrayOfByte, paramInt1, paramInt2);
        return localStringBuilder.toString();
    }

    public static String calledPartyBCDToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        int i = 0;
        StringBuilder localStringBuilder = new StringBuilder(1 + paramInt2 * 2);
        if (paramInt2 < 2);
        for (String str1 = ""; ; str1 = "")
        {
            return str1;
            if ((0xF0 & paramArrayOfByte[paramInt1]) == 144)
                i = 1;
            internalCalledPartyBCDFragmentToString(localStringBuilder, paramArrayOfByte, paramInt1 + 1, paramInt2 - 1);
            if ((i == 0) || (localStringBuilder.length() != 0))
                break;
        }
        String str2;
        Matcher localMatcher1;
        if (i != 0)
        {
            str2 = localStringBuilder.toString();
            localMatcher1 = Pattern.compile("(^[#*])(.*)([#*])(.*)(#)$").matcher(str2);
            if (!localMatcher1.matches())
                break label274;
            if (!"".equals(localMatcher1.group(2)))
                break label194;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append(localMatcher1.group(1));
            localStringBuilder.append(localMatcher1.group(3));
            localStringBuilder.append(localMatcher1.group(4));
            localStringBuilder.append(localMatcher1.group(5));
            localStringBuilder.append("+");
        }
        while (true)
        {
            str1 = localStringBuilder.toString();
            break;
            label194: localStringBuilder = new StringBuilder();
            localStringBuilder.append(localMatcher1.group(1));
            localStringBuilder.append(localMatcher1.group(2));
            localStringBuilder.append(localMatcher1.group(3));
            localStringBuilder.append("+");
            localStringBuilder.append(localMatcher1.group(4));
            localStringBuilder.append(localMatcher1.group(5));
            continue;
            label274: Matcher localMatcher2 = Pattern.compile("(^[#*])(.*)([#*])(.*)").matcher(str2);
            if (localMatcher2.matches())
            {
                localStringBuilder = new StringBuilder();
                localStringBuilder.append(localMatcher2.group(1));
                localStringBuilder.append(localMatcher2.group(2));
                localStringBuilder.append(localMatcher2.group(3));
                localStringBuilder.append("+");
                localStringBuilder.append(localMatcher2.group(4));
            }
            else
            {
                localStringBuilder = new StringBuilder();
                localStringBuilder.append('+');
                localStringBuilder.append(str2);
            }
        }
    }

    public static String cdmaCheckAndProcessPlusCode(String paramString)
    {
        if ((!TextUtils.isEmpty(paramString)) && (isReallyDialable(paramString.charAt(0))) && (isNonSeparator(paramString)))
        {
            String str1 = SystemProperties.get("gsm.operator.iso-country", "");
            String str2 = SystemProperties.get("gsm.sim.operator.iso-country", "");
            if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2)))
                paramString = cdmaCheckAndProcessPlusCodeByNumberFormat(paramString, getFormatTypeFromCountryCode(str1), getFormatTypeFromCountryCode(str2));
        }
        return paramString;
    }

    public static String cdmaCheckAndProcessPlusCodeByNumberFormat(String paramString, int paramInt1, int paramInt2)
    {
        Object localObject = paramString;
        String str2;
        String str3;
        int i;
        if ((paramString != null) && (paramString.lastIndexOf("+") != -1))
        {
            if ((paramInt1 != paramInt2) || (paramInt1 != 1))
                break label166;
            String str1 = paramString;
            localObject = null;
            str2 = processPlusCodeWithinNanp(extractNetworkPortion(str1));
            if (TextUtils.isEmpty(str2))
                break label133;
            if (localObject != null)
                break label123;
            localObject = str2;
            str3 = extractPostDialPortion(str1);
            if (!TextUtils.isEmpty(str3))
            {
                i = findDialableIndexFromPostDialStr(str3);
                if (i < 1)
                    break label145;
                localObject = appendPwCharBackToOrigDialStr(i, (String)localObject, str3);
                str1 = str3.substring(i);
            }
            label103: if ((!TextUtils.isEmpty(str3)) && (!TextUtils.isEmpty(str1)))
                break label164;
        }
        while (true)
        {
            paramString = (String)localObject;
            while (true)
            {
                return paramString;
                label123: localObject = ((String)localObject).concat(str2);
                break;
                label133: Log.e("checkAndProcessPlusCode: null newDialStr", str2);
            }
            label145: if (i < 0)
                str3 = "";
            Log.e("wrong postDialStr=", str3);
            break label103;
            label164: break;
            label166: Log.e("checkAndProcessPlusCode:non-NANP not supported", paramString);
        }
    }

    private static int charToBCD(char paramChar)
    {
        int i;
        if ((paramChar >= '0') && (paramChar <= '9'))
            i = paramChar + '\0*0';
        while (true)
        {
            return i;
            if (paramChar == '*')
            {
                i = 10;
            }
            else if (paramChar == '#')
            {
                i = 11;
            }
            else if (paramChar == ',')
            {
                i = 12;
            }
            else
            {
                if (paramChar != 'N')
                    break;
                i = 13;
            }
        }
        throw new RuntimeException("invalid char for BCD " + paramChar);
    }

    private static boolean checkPrefixIsIgnorable(String paramString, int paramInt1, int paramInt2)
    {
        boolean bool = false;
        int i = 0;
        if (paramInt2 >= paramInt1)
            if (tryGetISODigit(paramString.charAt(paramInt2)) >= 0)
                if (i == 0);
        while (true)
        {
            return bool;
            i = 1;
            do
            {
                paramInt2--;
                break;
            }
            while (!isDialable(paramString.charAt(paramInt2)));
            continue;
            bool = true;
        }
    }

    public static boolean compare(Context paramContext, String paramString1, String paramString2)
    {
        return compare(paramString1, paramString2, paramContext.getResources().getBoolean(17891356));
    }

    public static boolean compare(String paramString1, String paramString2)
    {
        return compare(paramString1, paramString2, false);
    }

    public static boolean compare(String paramString1, String paramString2, boolean paramBoolean)
    {
        if (paramBoolean);
        for (boolean bool = compareStrictly(paramString1, paramString2); ; bool = compareLoosely(paramString1, paramString2))
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static boolean compareLoosely(String paramString1, String paramString2)
    {
        int i = 0;
        int j = 0;
        boolean bool;
        if ((paramString1 == null) || (paramString2 == null))
            if (paramString1 == paramString2)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if ((paramString1.length() == 0) || (paramString2.length() == 0))
            {
                bool = false;
            }
            else
            {
                int k = indexOfLastNetworkChar(paramString1);
                int m = indexOfLastNetworkChar(paramString2);
                int n = 0;
                while (true)
                    if ((k >= 0) && (m >= 0))
                    {
                        int i2 = 0;
                        char c1 = paramString1.charAt(k);
                        if (!isDialable(c1))
                        {
                            k--;
                            i2 = 1;
                            i++;
                        }
                        char c2 = paramString2.charAt(m);
                        if (!isDialable(c2))
                        {
                            m--;
                            i2 = 1;
                            j++;
                        }
                        if (i2 == 0)
                            if ((c2 == c1) || (c1 == 'N') || (c2 == 'N'));
                    }
                    else
                    {
                        if (n >= 7)
                            break label210;
                        int i1 = Injector.getEffectiveLength(paramString1) - i;
                        if ((i1 != Injector.getEffectiveLength(paramString2) - j) || (i1 != n))
                            break label204;
                        bool = true;
                        break;
                        k--;
                        m--;
                        n++;
                    }
                label204: bool = false;
                continue;
                label210: if ((n >= 7) && ((k < 0) || (m < 0)))
                    bool = true;
                else if ((matchIntlPrefix(paramString1, k + 1)) && (matchIntlPrefix(paramString2, m + 1)))
                    bool = true;
                else if ((matchTrunkPrefix(paramString1, k + 1)) && (matchIntlPrefixAndCC(paramString2, m + 1)))
                    bool = true;
                else if ((matchTrunkPrefix(paramString2, m + 1)) && (matchIntlPrefixAndCC(paramString1, k + 1)))
                    bool = true;
                else
                    bool = false;
            }
        }
    }

    public static boolean compareStrictly(String paramString1, String paramString2)
    {
        return compareStrictly(paramString1, paramString2, true);
    }

    public static boolean compareStrictly(String paramString1, String paramString2, boolean paramBoolean)
    {
        boolean bool;
        if ((paramString1 == null) || (paramString2 == null))
            if (paramString1 == paramString2)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if ((paramString1.length() == 0) && (paramString2.length() == 0))
            {
                bool = false;
            }
            else
            {
                int i = 0;
                int j = 0;
                CountryCallingCodeAndNewIndex localCountryCallingCodeAndNewIndex1 = tryGetCountryCallingCodeAndNewIndex(paramString1, paramBoolean);
                CountryCallingCodeAndNewIndex localCountryCallingCodeAndNewIndex2 = tryGetCountryCallingCodeAndNewIndex(paramString2, paramBoolean);
                int k = 0;
                int m = 1;
                int n = 0;
                int i1 = 0;
                label121: int i4;
                int i5;
                if ((localCountryCallingCodeAndNewIndex1 != null) && (localCountryCallingCodeAndNewIndex2 != null))
                {
                    if (localCountryCallingCodeAndNewIndex1.countryCallingCode != localCountryCallingCodeAndNewIndex2.countryCallingCode)
                    {
                        bool = false;
                    }
                    else
                    {
                        m = 0;
                        k = 1;
                        i = localCountryCallingCodeAndNewIndex1.newIndex;
                        j = localCountryCallingCodeAndNewIndex2.newIndex;
                        i4 = -1 + paramString1.length();
                        i5 = -1 + paramString2.length();
                    }
                }
                else
                {
                    while (true)
                    {
                        if ((i4 < i) || (i5 < j))
                            break label313;
                        int i7 = 0;
                        char c3 = paramString1.charAt(i4);
                        char c4 = paramString2.charAt(i5);
                        if (isSeparator(c3))
                        {
                            i4--;
                            i7 = 1;
                        }
                        if (isSeparator(c4))
                        {
                            i5--;
                            i7 = 1;
                        }
                        if (i7 == 0)
                        {
                            if (c3 != c4)
                            {
                                bool = false;
                                break;
                                if ((localCountryCallingCodeAndNewIndex1 == null) && (localCountryCallingCodeAndNewIndex2 == null))
                                {
                                    m = 0;
                                    break label121;
                                }
                                if (localCountryCallingCodeAndNewIndex1 != null)
                                    i = localCountryCallingCodeAndNewIndex1.newIndex;
                                while (true)
                                {
                                    if (localCountryCallingCodeAndNewIndex2 == null)
                                        break label282;
                                    j = localCountryCallingCodeAndNewIndex2.newIndex;
                                    break;
                                    int i2 = tryGetTrunkPrefixOmittedIndex(paramString2, 0);
                                    if (i2 >= 0)
                                    {
                                        i = i2;
                                        n = 1;
                                    }
                                }
                                label282: int i3 = tryGetTrunkPrefixOmittedIndex(paramString2, 0);
                                if (i3 < 0)
                                    break label121;
                                j = i3;
                                i1 = 1;
                                break label121;
                            }
                            i4--;
                            i5--;
                        }
                    }
                    label313: if (m != 0)
                    {
                        if (((n != 0) && (i <= i4)) || (!checkPrefixIsIgnorable(paramString1, i, i4)))
                        {
                            if (paramBoolean)
                                bool = compare(paramString1, paramString2, false);
                            else
                                bool = false;
                        }
                        else if (((i1 != 0) && (j <= i5)) || (!checkPrefixIsIgnorable(paramString2, i, i5)))
                        {
                            if (paramBoolean)
                            {
                                bool = compare(paramString1, paramString2, false);
                                continue;
                            }
                            bool = false;
                        }
                    }
                    else
                    {
                        int i6;
                        if (k == 0)
                            i6 = 1;
                        while (true)
                            if (i4 >= i)
                            {
                                char c2 = paramString1.charAt(i4);
                                if (isDialable(c2))
                                {
                                    if ((i6 != 0) && (tryGetISODigit(c2) == 1))
                                        i6 = 0;
                                }
                                else
                                {
                                    i4--;
                                    continue;
                                    i6 = 0;
                                    continue;
                                }
                                bool = false;
                                break;
                            }
                        while (true)
                            if (i5 >= j)
                            {
                                char c1 = paramString2.charAt(i5);
                                if (isDialable(c1))
                                {
                                    if ((i6 != 0) && (tryGetISODigit(c1) == 1))
                                        i6 = 0;
                                }
                                else
                                {
                                    i5--;
                                    continue;
                                }
                                bool = false;
                                break;
                            }
                        bool = true;
                    }
                }
            }
        }
    }

    public static String convertAndStrip(String paramString)
    {
        return stripSeparators(convertKeypadLettersToDigits(paramString));
    }

    public static String convertKeypadLettersToDigits(String paramString)
    {
        if (paramString == null);
        while (true)
        {
            return paramString;
            int i = paramString.length();
            if (i != 0)
            {
                char[] arrayOfChar = paramString.toCharArray();
                for (int j = 0; j < i; j++)
                {
                    int k = arrayOfChar[j];
                    arrayOfChar[j] = ((char)KEYPAD_MAP.get(k, k));
                }
                paramString = new String(arrayOfChar);
            }
        }
    }

    public static String convertPreDial(String paramString)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            int i = paramString.length();
            localStringBuilder = new StringBuilder(i);
            int j = 0;
            if (j < i)
            {
                char c = paramString.charAt(j);
                if (isPause(c))
                    c = ',';
                while (true)
                {
                    localStringBuilder.append(c);
                    j++;
                    break;
                    if (isToneWait(c))
                        c = ';';
                }
            }
        }
    }

    public static String extractNetworkPortion(String paramString)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str1 = null; ; str1 = localStringBuilder.toString())
        {
            return str1;
            int i = paramString.length();
            localStringBuilder = new StringBuilder(i);
            int j = 0;
            if (j < i)
            {
                char c = paramString.charAt(j);
                int k = Character.digit(c, 10);
                if (k != -1)
                    localStringBuilder.append(k);
                label136: 
                do
                    while (true)
                    {
                        j++;
                        break;
                        if (c == '+')
                        {
                            String str2 = localStringBuilder.toString();
                            if ((str2.length() == 0) || (str2.equals("*31#")) || (str2.equals("#31#")))
                                localStringBuilder.append(c);
                        }
                        else
                        {
                            if (!isDialable(c))
                                break label136;
                            localStringBuilder.append(c);
                        }
                    }
                while (!isStartsPostDial(c));
            }
        }
    }

    public static String extractNetworkPortionAlt(String paramString)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            int i = paramString.length();
            localStringBuilder = new StringBuilder(i);
            int j = 0;
            int k = 0;
            if (k < i)
            {
                char c = paramString.charAt(k);
                if (c == '+')
                    if (j == 0);
                label80: 
                do
                    while (true)
                    {
                        k++;
                        break;
                        j = 1;
                        if (!isDialable(c))
                            break label80;
                        localStringBuilder.append(c);
                    }
                while (!isStartsPostDial(c));
            }
        }
    }

    public static String extractPostDialPortion(String paramString)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            localStringBuilder = new StringBuilder();
            int i = 1 + indexOfLastNetworkChar(paramString);
            int j = paramString.length();
            while (i < j)
            {
                char c = paramString.charAt(i);
                if (isNonSeparator(c))
                    localStringBuilder.append(c);
                i++;
            }
        }
    }

    private static int findDialableIndexFromPostDialStr(String paramString)
    {
        int i = 0;
        if (i < paramString.length())
            if (!isReallyDialable(paramString.charAt(i)));
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public static void formatJapaneseNumber(Editable paramEditable)
    {
        JapanesePhoneNumberFormatter.format(paramEditable);
    }

    public static void formatNanpNumber(Editable paramEditable)
    {
        int i = paramEditable.length();
        if (i > "+1-nnn-nnn-nnnn".length());
        int[] arrayOfInt;
        int k;
        int m;
        int n;
        int i1;
        label148: int i6;
        while (true)
        {
            return;
            if (i > 5)
            {
                CharSequence localCharSequence = paramEditable.subSequence(0, i);
                removeDashes(paramEditable);
                int j = paramEditable.length();
                arrayOfInt = new int[3];
                k = 1;
                m = 0;
                n = 0;
                for (i1 = 0; ; i1 = i6)
                {
                    if (n >= j)
                        break label296;
                    switch (paramEditable.charAt(n))
                    {
                    case ',':
                    case '.':
                    case '/':
                    default:
                        paramEditable.replace(0, j, localCharSequence);
                        break;
                    case '1':
                        if ((m != 0) && (k != 2))
                            break label189;
                        k = 3;
                        i6 = i1;
                        label179: n++;
                    case '0':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '-':
                    case '+':
                    }
                }
                label189: if (k != 2)
                    break;
                paramEditable.replace(0, j, localCharSequence);
            }
        }
        if (k == 3)
        {
            i6 = i1 + 1;
            arrayOfInt[i1] = n;
        }
        while (true)
        {
            k = 1;
            m++;
            break label179;
            if ((k != 4) && ((m == 3) || (m == 6)))
            {
                i6 = i1 + 1;
                arrayOfInt[i1] = n;
                continue;
                k = 4;
                i6 = i1;
                break label179;
                if (n != 0)
                    break label148;
                k = 2;
                i6 = i1;
                break label179;
                label296: if (m == 7);
                for (int i2 = i1 - 1; ; i2 = i1)
                {
                    for (int i3 = 0; i3 < i2; i3++)
                    {
                        int i5 = arrayOfInt[i3];
                        paramEditable.replace(i5 + i3, i5 + i3, "-");
                    }
                    for (int i4 = paramEditable.length(); (i4 > 0) && (paramEditable.charAt(i4 - 1) == '-'); i4--)
                        paramEditable.delete(i4 - 1, i4);
                    break;
                }
            }
            i6 = i1;
        }
    }

    public static String formatNumber(String paramString)
    {
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramString);
        formatNumber(localSpannableStringBuilder, getFormatTypeForLocale(Locale.getDefault()));
        return localSpannableStringBuilder.toString();
    }

    public static String formatNumber(String paramString, int paramInt)
    {
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramString);
        formatNumber(localSpannableStringBuilder, paramInt);
        return localSpannableStringBuilder.toString();
    }

    public static String formatNumber(String paramString1, String paramString2)
    {
        Object localObject;
        if ((paramString1.startsWith("#")) || (paramString1.startsWith("*")))
            localObject = paramString1;
        while (true)
        {
            return localObject;
            PhoneNumberUtil localPhoneNumberUtil = PhoneNumberUtil.getInstance();
            localObject = null;
            try
            {
                String str = localPhoneNumberUtil.formatInOriginalFormat(localPhoneNumberUtil.parseAndKeepRawInput(paramString1, paramString2), paramString2);
                localObject = str;
            }
            catch (NumberParseException localNumberParseException)
            {
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static String formatNumber(String paramString1, String paramString2, String paramString3)
    {
        Object localObject1 = miui.telephony.PhoneNumberUtils.removeDashesAndBlanks(paramString1);
        int i = ((String)localObject1).length();
        int j = 0;
        if (j < i)
            if (isDialable(((String)localObject1).charAt(j)));
        while (true)
        {
            return localObject1;
            j++;
            break;
            PhoneNumberUtil localPhoneNumberUtil = PhoneNumberUtil.getInstance();
            if ((paramString2 != null) && (paramString2.length() >= 2) && (paramString2.charAt(0) == '+'));
            try
            {
                String str = localPhoneNumberUtil.getRegionCodeForNumber(localPhoneNumberUtil.parse(paramString2, "ZZ"));
                if (!TextUtils.isEmpty(str))
                {
                    int k = normalizeNumber((String)localObject1).indexOf(paramString2.substring(1));
                    if (k <= 0)
                        paramString3 = str;
                }
                label114: Object localObject2 = formatNumber((String)localObject1, paramString3);
                if (localObject2 != null);
                while (true)
                {
                    localObject1 = localObject2;
                    break;
                    localObject2 = localObject1;
                }
            }
            catch (NumberParseException localNumberParseException)
            {
                break label114;
            }
        }
    }

    public static void formatNumber(Editable paramEditable, int paramInt)
    {
        int i = paramInt;
        if ((paramEditable.length() > 2) && (paramEditable.charAt(0) == '+'))
        {
            if (paramEditable.charAt(1) == '1')
                i = 1;
        }
        else
            switch (i)
            {
            default:
            case 1:
            case 2:
            case 0:
            }
        while (true)
        {
            return;
            if ((paramEditable.length() >= 3) && (paramEditable.charAt(1) == '8') && (paramEditable.charAt(2) == '1'))
            {
                i = 2;
                break;
            }
            i = 0;
            break;
            formatNanpNumber(paramEditable);
            continue;
            formatJapaneseNumber(paramEditable);
            continue;
            removeDashes(paramEditable);
        }
    }

    public static String formatNumberToE164(String paramString1, String paramString2)
    {
        PhoneNumberUtil localPhoneNumberUtil = PhoneNumberUtil.getInstance();
        Object localObject = null;
        try
        {
            Phonenumber.PhoneNumber localPhoneNumber = localPhoneNumberUtil.parse(paramString1, paramString2);
            if (localPhoneNumberUtil.isValidNumber(localPhoneNumber))
            {
                String str = localPhoneNumberUtil.format(localPhoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                localObject = str;
            }
            label37: return localObject;
        }
        catch (NumberParseException localNumberParseException)
        {
            break label37;
        }
    }

    private static String getDefaultIdp()
    {
        String str = null;
        SystemProperties.get("ro.cdma.idpstring", null);
        if (TextUtils.isEmpty(null))
            str = "011";
        return str;
    }

    public static int getFormatTypeForLocale(Locale paramLocale)
    {
        return getFormatTypeFromCountryCode(paramLocale.getCountry());
    }

    private static int getFormatTypeFromCountryCode(String paramString)
    {
        int i = NANP_COUNTRIES.length;
        int j = 0;
        int k;
        if (j < i)
            if (NANP_COUNTRIES[j].compareToIgnoreCase(paramString) == 0)
                k = 1;
        while (true)
        {
            return k;
            j++;
            break;
            if ("jp".compareToIgnoreCase(paramString) == 0)
                k = 2;
            else
                k = 0;
        }
    }

    public static String getNumberFromIntent(Intent paramIntent, Context paramContext)
    {
        Object localObject1 = null;
        Object localObject2 = null;
        Uri localUri = paramIntent.getData();
        String str1 = localUri.getScheme();
        if ((str1.equals("tel")) || (str1.equals("sip")))
            localObject1 = localUri.getSchemeSpecificPart();
        while (true)
        {
            return localObject1;
            if (str1.equals("voicemail"))
            {
                localObject1 = TelephonyManager.getDefault().getCompleteVoiceMailNumber();
                continue;
            }
            if (paramContext == null)
                continue;
            paramIntent.resolveType(paramContext);
            String str2 = null;
            String str3 = localUri.getAuthority();
            label104: Cursor localCursor;
            if ("contacts".equals(str3))
            {
                str2 = "number";
                ContentResolver localContentResolver = paramContext.getContentResolver();
                String[] arrayOfString = new String[1];
                arrayOfString[0] = str2;
                localCursor = localContentResolver.query(localUri, arrayOfString, null, null, null);
                if (localCursor == null);
            }
            try
            {
                if (localCursor.moveToFirst())
                {
                    String str4 = localCursor.getString(localCursor.getColumnIndex(str2));
                    localObject2 = str4;
                }
                localCursor.close();
                localObject1 = localObject2;
                continue;
                if (!"com.android.contacts".equals(str3))
                    break label104;
                str2 = "data1";
            }
            finally
            {
                localCursor.close();
            }
        }
    }

    public static String getStrippedReversed(String paramString)
    {
        String str1 = extractNetworkPortionAlt(paramString);
        if (str1 == null);
        for (String str2 = null; ; str2 = internalGetStrippedReversed(str1, str1.length()))
            return str2;
    }

    public static String getUsernameFromUriNumber(String paramString)
    {
        int i = paramString.indexOf('@');
        if (i < 0)
            i = paramString.indexOf("%40");
        if (i < 0)
        {
            Log.w("PhoneNumberUtils", "getUsernameFromUriNumber: no delimiter found in SIP addr '" + paramString + "'");
            i = paramString.length();
        }
        return paramString.substring(0, i);
    }

    private static int indexOfLastNetworkChar(String paramString)
    {
        int i = paramString.length();
        int j = minPositive(paramString.indexOf(','), paramString.indexOf(';'));
        if (j < 0);
        for (int k = i - 1; ; k = j - 1)
            return k;
    }

    private static void internalCalledPartyBCDFragmentToString(StringBuilder paramStringBuilder, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        for (int i = paramInt1; ; i++)
        {
            char c1;
            if (i < paramInt2 + paramInt1)
            {
                c1 = bcdToChar((byte)(0xF & paramArrayOfByte[i]));
                if (c1 != 0)
                    break label30;
            }
            label30: char c2;
            do
            {
                byte b;
                do
                {
                    return;
                    paramStringBuilder.append(c1);
                    b = (byte)(0xF & paramArrayOfByte[i] >> 4);
                }
                while ((b == 15) && (i + 1 == paramInt2 + paramInt1));
                c2 = bcdToChar(b);
            }
            while (c2 == 0);
            paramStringBuilder.append(c2);
        }
    }

    private static String internalGetStrippedReversed(String paramString, int paramInt)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            localStringBuilder = new StringBuilder(paramInt);
            int i = paramString.length();
            for (int j = i - 1; (j >= 0) && (i - j <= paramInt); j--)
                localStringBuilder.append(paramString.charAt(j));
        }
    }

    public static final boolean is12Key(char paramChar)
    {
        if (((paramChar >= '0') && (paramChar <= '9')) || (paramChar == '*') || (paramChar == '#'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isCountryCallingCode(int paramInt)
    {
        if ((paramInt > 0) && (paramInt < CCC_LENGTH) && (COUNTRY_CALLING_CALL[paramInt] != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static final boolean isDialable(char paramChar)
    {
        if (((paramChar >= '0') && (paramChar <= '9')) || (paramChar == '*') || (paramChar == '#') || (paramChar == '+') || (paramChar == 'N'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isDialable(String paramString)
    {
        int i = 0;
        int j = paramString.length();
        if (i < j)
            if (isDialable(paramString.charAt(i)));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    public static boolean isEmergencyNumber(String paramString)
    {
        return isEmergencyNumberInternal(paramString, true);
    }

    public static boolean isEmergencyNumber(String paramString1, String paramString2)
    {
        return isEmergencyNumberInternal(paramString1, paramString2, true);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static boolean isEmergencyNumberInternal(String paramString1, String paramString2, boolean paramBoolean)
    {
        boolean bool = false;
        if (paramString1 == null);
        while (true)
        {
            return bool;
            if (!isUriNumber(paramString1))
            {
                String str1 = miui.telephony.PhoneNumberUtils.parseNumber(extractNetworkPortionAlt(paramString1));
                if (miui.telephony.PhoneNumberUtils.isMiuiEmergencyNumber(str1, paramBoolean))
                {
                    bool = true;
                }
                else
                {
                    String str2 = SystemProperties.get("ril.ecclist");
                    if (TextUtils.isEmpty(str2))
                        str2 = SystemProperties.get("ro.ril.ecclist");
                    if (!TextUtils.isEmpty(str2))
                    {
                        String[] arrayOfString = str2.split(",");
                        int i = arrayOfString.length;
                        for (int j = 0; ; j++)
                        {
                            if (j >= i)
                                break label150;
                            String str3 = arrayOfString[j];
                            if ((paramBoolean) || ("BR".equalsIgnoreCase(paramString2)))
                            {
                                if (!str1.equals(str3))
                                    continue;
                                bool = true;
                                break;
                            }
                            if (str1.startsWith(str3))
                            {
                                bool = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        label150: Log.d("PhoneNumberUtils", "System property doesn't provide any emergency numbers. Use embedded logic for determining ones.");
                        if (paramString2 != null)
                        {
                            ShortNumberUtil localShortNumberUtil = new ShortNumberUtil();
                            if (paramBoolean)
                                bool = localShortNumberUtil.isEmergencyNumber(str1, paramString2);
                            else
                                bool = localShortNumberUtil.connectsToEmergencyNumber(str1, paramString2);
                        }
                        else if (paramBoolean)
                        {
                            if ((str1.equals("112")) || (str1.equals("911")))
                                bool = true;
                        }
                        else if ((str1.startsWith("112")) || (str1.startsWith("911")))
                        {
                            bool = true;
                        }
                    }
                }
            }
        }
    }

    private static boolean isEmergencyNumberInternal(String paramString, boolean paramBoolean)
    {
        return isEmergencyNumberInternal(paramString, null, paramBoolean);
    }

    public static boolean isGlobalPhoneNumber(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (boolean bool = false; ; bool = GLOBAL_PHONE_NUMBER_PATTERN.matcher(paramString).matches())
            return bool;
    }

    public static boolean isISODigit(char paramChar)
    {
        if ((paramChar >= '0') && (paramChar <= '9'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isLocalEmergencyNumber(String paramString, Context paramContext)
    {
        return isLocalEmergencyNumberInternal(paramString, paramContext, true);
    }

    private static boolean isLocalEmergencyNumberInternal(String paramString, Context paramContext, boolean paramBoolean)
    {
        CountryDetector localCountryDetector = (CountryDetector)paramContext.getSystemService("country_detector");
        String str;
        if (localCountryDetector != null)
            str = localCountryDetector.detectCountry().getCountryIso();
        while (true)
        {
            return isEmergencyNumberInternal(paramString, str, paramBoolean);
            str = paramContext.getResources().getConfiguration().locale.getCountry();
            Log.w("PhoneNumberUtils", "No CountryDetector; falling back to countryIso based on locale: " + str);
        }
    }

    private static boolean isNanp(String paramString)
    {
        boolean bool = false;
        int i;
        if (paramString != null)
            if ((paramString.length() == 10) && (isTwoToNine(paramString.charAt(0))) && (isTwoToNine(paramString.charAt(3))))
            {
                bool = true;
                i = 1;
                if (i < 10)
                {
                    if (isISODigit(paramString.charAt(i)))
                        break label62;
                    bool = false;
                }
            }
        while (true)
        {
            return bool;
            label62: i++;
            break;
            Log.e("isNanp: null dialStr passed in", paramString);
        }
    }

    public static final boolean isNonSeparator(char paramChar)
    {
        if (((paramChar >= '0') && (paramChar <= '9')) || (paramChar == '*') || (paramChar == '#') || (paramChar == '+') || (paramChar == 'N') || (paramChar == ';') || (paramChar == ','));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isNonSeparator(String paramString)
    {
        int i = 0;
        int j = paramString.length();
        if (i < j)
            if (isNonSeparator(paramString.charAt(i)));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    private static boolean isOneNanp(String paramString)
    {
        boolean bool = false;
        if (paramString != null)
        {
            String str = paramString.substring(1);
            if ((paramString.charAt(0) == '1') && (isNanp(str)))
                bool = true;
        }
        while (true)
        {
            return bool;
            Log.e("isOneNanp: null dialStr passed in", paramString);
        }
    }

    private static boolean isPause(char paramChar)
    {
        if ((paramChar == 'p') || (paramChar == 'P'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isPotentialEmergencyNumber(String paramString)
    {
        return isEmergencyNumberInternal(paramString, false);
    }

    public static boolean isPotentialEmergencyNumber(String paramString1, String paramString2)
    {
        return isEmergencyNumberInternal(paramString1, paramString2, false);
    }

    public static boolean isPotentialLocalEmergencyNumber(String paramString, Context paramContext)
    {
        return isLocalEmergencyNumberInternal(paramString, paramContext, false);
    }

    public static final boolean isReallyDialable(char paramChar)
    {
        if (((paramChar >= '0') && (paramChar <= '9')) || (paramChar == '*') || (paramChar == '#') || (paramChar == '+'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isSeparator(char paramChar)
    {
        if ((!isDialable(paramChar)) && (('a' > paramChar) || (paramChar > 'z')) && (('A' > paramChar) || (paramChar > 'Z')));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static final boolean isStartsPostDial(char paramChar)
    {
        if ((paramChar == ',') || (paramChar == ';'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isToneWait(char paramChar)
    {
        if ((paramChar == 'w') || (paramChar == 'W'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isTwoToNine(char paramChar)
    {
        if ((paramChar >= '2') && (paramChar <= '9'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isUriNumber(String paramString)
    {
        if ((paramString != null) && ((paramString.contains("@")) || (paramString.contains("%40"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isVoiceMailNumber(String paramString)
    {
        boolean bool = false;
        try
        {
            String str1 = TelephonyManager.getDefault().getVoiceMailNumber();
            String str2 = extractNetworkPortionAlt(paramString);
            if ((!TextUtils.isEmpty(str2)) && (compare(str2, str1)))
                bool = true;
            label34: return bool;
        }
        catch (SecurityException localSecurityException)
        {
            break label34;
        }
    }

    public static boolean isWellFormedSmsAddress(String paramString)
    {
        String str = extractNetworkPortion(paramString);
        if ((!str.equals("+")) && (!TextUtils.isEmpty(str)) && (isDialable(str)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static void log(String paramString)
    {
        Log.d("PhoneNumberUtils", paramString);
    }

    private static boolean matchIntlPrefix(String paramString, int paramInt)
    {
        boolean bool = false;
        int i = 0;
        int j = 0;
        char c;
        if (j < paramInt)
        {
            c = paramString.charAt(j);
            switch (i)
            {
            case 1:
            case 3:
            default:
                if (!isNonSeparator(c))
                    break;
            case 0:
            case 2:
            case 4:
            }
        }
        while (true)
        {
            label64: return bool;
            if (c == '+')
                i = 1;
            label151: 
            do
                while (true)
                {
                    j++;
                    break;
                    if (c == '0')
                    {
                        i = 2;
                    }
                    else if (isNonSeparator(c))
                    {
                        break label64;
                        if (c == '0')
                        {
                            i = 3;
                        }
                        else if (c == '1')
                        {
                            i = 4;
                        }
                        else if (isNonSeparator(c))
                        {
                            break label64;
                            if (c != '1')
                                break label151;
                            i = 5;
                        }
                    }
                }
            while (!isNonSeparator(c));
            continue;
            if ((i == 1) || (i == 3) || (i == 5))
                bool = true;
        }
    }

    private static boolean matchIntlPrefixAndCC(String paramString, int paramInt)
    {
        boolean bool = false;
        int i = 0;
        int j = 0;
        char c;
        if (j < paramInt)
        {
            c = paramString.charAt(j);
            switch (i)
            {
            default:
                if (!isNonSeparator(c))
                    break;
            case 0:
            case 2:
            case 4:
            case 1:
            case 3:
            case 5:
            case 6:
            case 7:
            }
        }
        while (true)
        {
            label76: return bool;
            if (c == '+')
                i = 1;
            label213: 
            do
                while (true)
                {
                    j++;
                    break;
                    if (c == '0')
                    {
                        i = 2;
                    }
                    else if (isNonSeparator(c))
                    {
                        break label76;
                        if (c == '0')
                        {
                            i = 3;
                        }
                        else if (c == '1')
                        {
                            i = 4;
                        }
                        else if (isNonSeparator(c))
                        {
                            break label76;
                            if (c == '1')
                            {
                                i = 5;
                            }
                            else if (isNonSeparator(c))
                            {
                                break label76;
                                if (isISODigit(c))
                                {
                                    i = 6;
                                }
                                else if (isNonSeparator(c))
                                {
                                    break label76;
                                    if (!isISODigit(c))
                                        break label213;
                                    i++;
                                }
                            }
                        }
                    }
                }
            while (!isNonSeparator(c));
            continue;
            if ((i == 6) || (i == 7) || (i == 8))
                bool = true;
        }
    }

    private static boolean matchTrunkPrefix(String paramString, int paramInt)
    {
        boolean bool = false;
        int i = 0;
        if (i < paramInt)
        {
            char c = paramString.charAt(i);
            if ((c == '0') && (!bool))
                bool = true;
            while (!isNonSeparator(c))
            {
                i++;
                break;
            }
            bool = false;
        }
        return bool;
    }

    private static int minPositive(int paramInt1, int paramInt2)
    {
        if ((paramInt1 >= 0) && (paramInt2 >= 0))
            if (paramInt1 >= paramInt2);
        while (true)
        {
            return paramInt1;
            paramInt1 = paramInt2;
            continue;
            if (paramInt1 < 0)
                if (paramInt2 >= 0)
                    paramInt1 = paramInt2;
                else
                    paramInt1 = -1;
        }
    }

    public static byte[] networkPortionToCalledPartyBCD(String paramString)
    {
        return numberToCalledPartyBCDHelper(extractNetworkPortion(paramString), false);
    }

    public static byte[] networkPortionToCalledPartyBCDWithLength(String paramString)
    {
        return numberToCalledPartyBCDHelper(extractNetworkPortion(paramString), true);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static String normalizeNumber(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramString.length();
        int j = 0;
        if (j < i)
        {
            char c = paramString.charAt(j);
            Injector.appendNonSeparator(localStringBuilder, c);
            int k = Character.digit(c, 10);
            if (k != -1)
                localStringBuilder.append(k);
            label83: 
            do
                while (true)
                {
                    j++;
                    break;
                    if ((j != 0) || (c != '+'))
                        break label83;
                    localStringBuilder.append(c);
                }
            while (((c < 'a') || (c > 'z')) && ((c < 'A') || (c > 'Z')));
        }
        for (String str = normalizeNumber(convertKeypadLettersToDigits(paramString)); ; str = localStringBuilder.toString())
            return str;
    }

    public static byte[] numberToCalledPartyBCD(String paramString)
    {
        return numberToCalledPartyBCDHelper(paramString, false);
    }

    private static byte[] numberToCalledPartyBCDHelper(String paramString, boolean paramBoolean)
    {
        int i = paramString.length();
        int j = i;
        if (paramString.indexOf('+') != -1);
        for (int k = 1; ; k = 0)
        {
            if (k != 0)
                j--;
            if (j != 0)
                break;
            arrayOfByte = null;
            return arrayOfByte;
        }
        int m = (j + 1) / 2;
        int n = 1;
        if (paramBoolean)
            n++;
        int i1 = m + n;
        byte[] arrayOfByte = new byte[i1];
        int i2 = 0;
        int i3 = 0;
        while (i3 < i)
        {
            char c = paramString.charAt(i3);
            if (c == '+')
            {
                i3++;
            }
            else
            {
                if ((i2 & 0x1) == 1);
                for (int i8 = 4; ; i8 = 0)
                {
                    int i9 = n + (i2 >> 1);
                    arrayOfByte[i9] |= (byte)((0xF & charToBCD(c)) << i8);
                    i2++;
                    break;
                }
            }
        }
        if ((i2 & 0x1) == 1)
        {
            int i7 = n + (i2 >> 1);
            arrayOfByte[i7] = ((byte)(0xF0 | arrayOfByte[i7]));
        }
        int i4 = 0;
        if (paramBoolean)
        {
            int i6 = 0 + 1;
            arrayOfByte[i4] = ((byte)(i1 - 1));
            i4 = i6;
        }
        if (k != 0);
        for (int i5 = 145; ; i5 = 129)
        {
            arrayOfByte[i4] = ((byte)i5);
            break;
        }
    }

    private static String processPlusCodeWithinNanp(String paramString)
    {
        Object localObject = paramString;
        String str;
        if ((paramString != null) && (paramString.charAt(0) == '+') && (paramString.length() > 1))
        {
            str = paramString.substring(1);
            if (!isOneNanp(str))
                break label41;
        }
        label41: for (localObject = str; ; localObject = paramString.replaceFirst("[+]", getDefaultIdp()))
            return localObject;
    }

    private static void removeDashes(Editable paramEditable)
    {
        int i = 0;
        while (i < paramEditable.length())
            if (paramEditable.charAt(i) == '-')
                paramEditable.delete(i, i + 1);
            else
                i++;
    }

    public static String replaceUnicodeDigits(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramString.length());
        char[] arrayOfChar = paramString.toCharArray();
        int i = arrayOfChar.length;
        int j = 0;
        if (j < i)
        {
            char c = arrayOfChar[j];
            int k = Character.digit(c, 10);
            if (k != -1)
                localStringBuilder.append(k);
            while (true)
            {
                j++;
                break;
                localStringBuilder.append(c);
            }
        }
        return localStringBuilder.toString();
    }

    public static String stringFromStringAndTOA(String paramString, int paramInt)
    {
        if (paramString == null);
        for (paramString = null; ; paramString = "+" + paramString)
            do
                return paramString;
            while ((paramInt != 145) || (paramString.length() <= 0) || (paramString.charAt(0) == '+'));
    }

    public static String stripSeparators(String paramString)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            int i = paramString.length();
            localStringBuilder = new StringBuilder(i);
            int j = 0;
            if (j < i)
            {
                char c = paramString.charAt(j);
                int k = Character.digit(c, 10);
                if (k != -1)
                    localStringBuilder.append(k);
                while (true)
                {
                    j++;
                    break;
                    if (isNonSeparator(c))
                        localStringBuilder.append(c);
                }
            }
        }
    }

    public static String toCallerIDMinMatch(String paramString)
    {
        return internalGetStrippedReversed(extractNetworkPortionAlt(paramString), 7);
    }

    public static int toaFromString(String paramString)
    {
        if ((paramString != null) && (paramString.length() > 0) && (paramString.charAt(0) == '+'));
        for (int i = 145; ; i = 129)
            return i;
    }

    private static CountryCallingCodeAndNewIndex tryGetCountryCallingCodeAndNewIndex(String paramString, boolean paramBoolean)
    {
        CountryCallingCodeAndNewIndex localCountryCallingCodeAndNewIndex = null;
        int i = 0;
        int j = 0;
        int k = paramString.length();
        int m = 0;
        char c;
        if (m < k)
        {
            c = paramString.charAt(m);
            switch (i)
            {
            default:
            case 0:
            case 2:
            case 4:
            case 1:
            case 3:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
        }
        while (true)
        {
            label88: return localCountryCallingCodeAndNewIndex;
            if (c == '+')
                i = 1;
            label308: 
            do
                while (true)
                {
                    m++;
                    break;
                    if (c == '0')
                    {
                        i = 2;
                    }
                    else
                    {
                        if (c == '1')
                        {
                            if (!paramBoolean)
                                break label88;
                            i = 8;
                            continue;
                        }
                        if (isDialable(c))
                        {
                            break label88;
                            if (c == '0')
                            {
                                i = 3;
                            }
                            else if (c == '1')
                            {
                                i = 4;
                            }
                            else if (isDialable(c))
                            {
                                break label88;
                                if (c == '1')
                                {
                                    i = 5;
                                }
                                else if (isDialable(c))
                                {
                                    break label88;
                                    int n = tryGetISODigit(c);
                                    if (n > 0)
                                    {
                                        j = n + j * 10;
                                        if ((j >= 100) || (isCountryCallingCode(j)))
                                        {
                                            localCountryCallingCodeAndNewIndex = new CountryCallingCodeAndNewIndex(j, m + 1);
                                            break label88;
                                        }
                                        if ((i == 1) || (i == 3) || (i == 5))
                                        {
                                            i = 6;
                                            continue;
                                        }
                                        i++;
                                        continue;
                                    }
                                    if (isDialable(c))
                                    {
                                        break label88;
                                        if (c != '6')
                                            break label308;
                                        i = 9;
                                    }
                                }
                            }
                        }
                    }
                }
            while (!isDialable(c));
            if ((goto 88) && (c == '6'))
                localCountryCallingCodeAndNewIndex = new CountryCallingCodeAndNewIndex(66, m + 1);
        }
    }

    private static int tryGetISODigit(char paramChar)
    {
        if (('0' <= paramChar) && (paramChar <= '9'));
        for (int i = paramChar + '\0*0'; ; i = -1)
            return i;
    }

    private static int tryGetTrunkPrefixOmittedIndex(String paramString, int paramInt)
    {
        int i = -1;
        int j = paramString.length();
        for (int k = paramInt; ; k++)
        {
            char c;
            if (k < j)
            {
                c = paramString.charAt(k);
                if (tryGetISODigit(c) < 0)
                    break label40;
                i = k + 1;
            }
            label40: 
            while (isDialable(c))
                return i;
        }
    }

    private static class CountryCallingCodeAndNewIndex
    {
        public final int countryCallingCode;
        public final int newIndex;

        public CountryCallingCodeAndNewIndex(int paramInt1, int paramInt2)
        {
            this.countryCallingCode = paramInt1;
            this.newIndex = paramInt2;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void appendNonSeparator(StringBuilder paramStringBuilder, char paramChar)
        {
            if ((Character.digit(paramChar, 10) == -1) && (PhoneNumberUtils.isNonSeparator(paramChar)))
                paramStringBuilder.append(paramChar);
        }

        static int getEffectiveLength(String paramString)
        {
            return 1 + PhoneNumberUtils.callIndexOfLastNetworkChar(paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.PhoneNumberUtils
 * JD-Core Version:        0.6.2
 */