package android.telephony;

import android.os.Parcel;
import android.util.Log;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.SmsMessageBase.SubmitPduBase;
import com.android.internal.telephony.SmsMessageBase.TextEncodingDetails;
import java.util.ArrayList;
import java.util.Arrays;

public class SmsMessage
{
    public static final int ENCODING_16BIT = 3;
    public static final int ENCODING_7BIT = 1;
    public static final int ENCODING_8BIT = 2;
    public static final int ENCODING_KSC5601 = 4;
    public static final int ENCODING_UNKNOWN = 0;
    public static final String FORMAT_3GPP = "3gpp";
    public static final String FORMAT_3GPP2 = "3gpp2";
    private static final String LOG_TAG = "SMS";
    public static final int MAX_USER_DATA_BYTES = 140;
    public static final int MAX_USER_DATA_BYTES_WITH_HEADER = 134;
    public static final int MAX_USER_DATA_SEPTETS = 160;
    public static final int MAX_USER_DATA_SEPTETS_WITH_HEADER = 153;
    public SmsMessageBase mWrappedSmsMessage;

    private SmsMessage(SmsMessageBase paramSmsMessageBase)
    {
        this.mWrappedSmsMessage = paramSmsMessageBase;
    }

    public static int[] calculateLength(CharSequence paramCharSequence, boolean paramBoolean)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (SmsMessageBase.TextEncodingDetails localTextEncodingDetails = com.android.internal.telephony.cdma.SmsMessage.calculateLength(paramCharSequence, paramBoolean); ; localTextEncodingDetails = com.android.internal.telephony.gsm.SmsMessage.calculateLength(paramCharSequence, paramBoolean))
        {
            int[] arrayOfInt = new int[4];
            arrayOfInt[0] = localTextEncodingDetails.msgCount;
            arrayOfInt[1] = localTextEncodingDetails.codeUnitCount;
            arrayOfInt[2] = localTextEncodingDetails.codeUnitsRemaining;
            arrayOfInt[3] = localTextEncodingDetails.codeUnitSize;
            return arrayOfInt;
        }
    }

    public static int[] calculateLength(String paramString, boolean paramBoolean)
    {
        return calculateLength(paramString, paramBoolean);
    }

    public static SmsMessage createFromEfRecord(int paramInt, byte[] paramArrayOfByte)
    {
        Object localObject;
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType())
        {
            localObject = com.android.internal.telephony.cdma.SmsMessage.createFromEfRecord(paramInt, paramArrayOfByte);
            if (localObject == null)
                break label40;
        }
        label40: for (SmsMessage localSmsMessage = new SmsMessage((SmsMessageBase)localObject); ; localSmsMessage = null)
        {
            return localSmsMessage;
            localObject = com.android.internal.telephony.gsm.SmsMessage.createFromEfRecord(paramInt, paramArrayOfByte);
            break;
        }
    }

    public static SmsMessage createFromPdu(byte[] paramArrayOfByte)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (String str = "3gpp2"; ; str = "3gpp")
            return createFromPdu(paramArrayOfByte, str);
    }

    public static SmsMessage createFromPdu(byte[] paramArrayOfByte, String paramString)
    {
        Object localObject;
        if ("3gpp2".equals(paramString))
            localObject = com.android.internal.telephony.cdma.SmsMessage.createFromPdu(paramArrayOfByte);
        for (SmsMessage localSmsMessage = new SmsMessage((SmsMessageBase)localObject); ; localSmsMessage = null)
        {
            return localSmsMessage;
            if ("3gpp".equals(paramString))
            {
                localObject = com.android.internal.telephony.gsm.SmsMessage.createFromPdu(paramArrayOfByte);
                break;
            }
            Log.e("SMS", "createFromPdu(): unsupported message format " + paramString);
        }
    }

    public static ArrayList<String> fragmentText(String paramString)
    {
        int i = TelephonyManager.getDefault().getCurrentPhoneType();
        SmsMessageBase.TextEncodingDetails localTextEncodingDetails;
        int i1;
        label44: int j;
        label70: int k;
        int m;
        ArrayList localArrayList;
        if (2 == i)
        {
            localTextEncodingDetails = com.android.internal.telephony.cdma.SmsMessage.calculateLength(paramString, false);
            if (localTextEncodingDetails.codeUnitSize != 1)
                break label247;
            if ((localTextEncodingDetails.languageTable == 0) || (localTextEncodingDetails.languageShiftTable == 0))
                break label221;
            i1 = 7;
            if (localTextEncodingDetails.msgCount > 1)
                i1 += 6;
            if (i1 != 0)
                i1++;
            j = 160 - i1;
            k = 0;
            m = paramString.length();
            localArrayList = new ArrayList(localTextEncodingDetails.msgCount);
        }
        while (true)
        {
            int n;
            if (k < m)
            {
                if (localTextEncodingDetails.codeUnitSize != 1)
                    break label289;
                if ((i != 2) || (localTextEncodingDetails.msgCount != 1))
                    break label269;
                n = k + Math.min(j, m - k);
            }
            while (true)
            {
                if ((n > k) && (n <= m))
                    break label308;
                Log.e("SMS", "fragmentText failed (" + k + " >= " + n + " or " + n + " >= " + m + ")");
                return localArrayList;
                localTextEncodingDetails = com.android.internal.telephony.gsm.SmsMessage.calculateLength(paramString, false);
                break;
                label221: if ((localTextEncodingDetails.languageTable != 0) || (localTextEncodingDetails.languageShiftTable != 0))
                {
                    i1 = 4;
                    break label44;
                }
                i1 = 0;
                break label44;
                label247: if (localTextEncodingDetails.msgCount > 1)
                {
                    j = 134;
                    break label70;
                }
                j = 140;
                break label70;
                label269: n = GsmAlphabet.findGsmSeptetLimitIndex(paramString, k, j, localTextEncodingDetails.languageTable, localTextEncodingDetails.languageShiftTable);
                continue;
                label289: n = k + Math.min(j / 2, m - k);
            }
            label308: localArrayList.add(paramString.substring(k, n));
            k = n;
        }
    }

    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, null); ; localObject = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean))
            return new SubmitPdu((SmsMessageBase.SubmitPduBase)localObject);
    }

    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, short paramShort, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramShort, paramArrayOfByte, paramBoolean); ; localObject = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu(paramString1, paramString2, paramShort, paramArrayOfByte, paramBoolean))
            return new SubmitPdu((SmsMessageBase.SubmitPduBase)localObject);
    }

    public static int getTPLayerLengthForPDU(String paramString)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (int i = com.android.internal.telephony.cdma.SmsMessage.getTPLayerLengthForPDU(paramString); ; i = com.android.internal.telephony.gsm.SmsMessage.getTPLayerLengthForPDU(paramString))
            return i;
    }

    public static SmsMessage newFromCMT(String[] paramArrayOfString)
    {
        return new SmsMessage(com.android.internal.telephony.gsm.SmsMessage.newFromCMT(paramArrayOfString));
    }

    public static SmsMessage newFromParcel(Parcel paramParcel)
    {
        return new SmsMessage(com.android.internal.telephony.cdma.SmsMessage.newFromParcel(paramParcel));
    }

    public String getDisplayMessageBody()
    {
        return this.mWrappedSmsMessage.getDisplayMessageBody();
    }

    public String getDisplayOriginatingAddress()
    {
        return this.mWrappedSmsMessage.getDisplayOriginatingAddress();
    }

    public String getEmailBody()
    {
        return this.mWrappedSmsMessage.getEmailBody();
    }

    public String getEmailFrom()
    {
        return this.mWrappedSmsMessage.getEmailFrom();
    }

    public int getIndexOnIcc()
    {
        return this.mWrappedSmsMessage.getIndexOnIcc();
    }

    @Deprecated
    public int getIndexOnSim()
    {
        return this.mWrappedSmsMessage.getIndexOnIcc();
    }

    public String getMessageBody()
    {
        return this.mWrappedSmsMessage.getMessageBody();
    }

    public MessageClass getMessageClass()
    {
        return this.mWrappedSmsMessage.getMessageClass();
    }

    public String getOriginatingAddress()
    {
        return this.mWrappedSmsMessage.getOriginatingAddress();
    }

    public byte[] getPdu()
    {
        return this.mWrappedSmsMessage.getPdu();
    }

    public int getProtocolIdentifier()
    {
        return this.mWrappedSmsMessage.getProtocolIdentifier();
    }

    public String getPseudoSubject()
    {
        return this.mWrappedSmsMessage.getPseudoSubject();
    }

    public String getServiceCenterAddress()
    {
        return this.mWrappedSmsMessage.getServiceCenterAddress();
    }

    public int getStatus()
    {
        return this.mWrappedSmsMessage.getStatus();
    }

    public int getStatusOnIcc()
    {
        return this.mWrappedSmsMessage.getStatusOnIcc();
    }

    @Deprecated
    public int getStatusOnSim()
    {
        return this.mWrappedSmsMessage.getStatusOnIcc();
    }

    public long getTimestampMillis()
    {
        return this.mWrappedSmsMessage.getTimestampMillis();
    }

    public byte[] getUserData()
    {
        return this.mWrappedSmsMessage.getUserData();
    }

    public boolean isCphsMwiMessage()
    {
        return this.mWrappedSmsMessage.isCphsMwiMessage();
    }

    public boolean isEmail()
    {
        return this.mWrappedSmsMessage.isEmail();
    }

    public boolean isMWIClearMessage()
    {
        return this.mWrappedSmsMessage.isMWIClearMessage();
    }

    public boolean isMWISetMessage()
    {
        return this.mWrappedSmsMessage.isMWISetMessage();
    }

    public boolean isMwiDontStore()
    {
        return this.mWrappedSmsMessage.isMwiDontStore();
    }

    public boolean isReplace()
    {
        return this.mWrappedSmsMessage.isReplace();
    }

    public boolean isReplyPathPresent()
    {
        return this.mWrappedSmsMessage.isReplyPathPresent();
    }

    public boolean isStatusReportMessage()
    {
        return this.mWrappedSmsMessage.isStatusReportMessage();
    }

    public static class SubmitPdu
    {
        public byte[] encodedMessage;
        public byte[] encodedScAddress;

        protected SubmitPdu(SmsMessageBase.SubmitPduBase paramSubmitPduBase)
        {
            this.encodedMessage = paramSubmitPduBase.encodedMessage;
            this.encodedScAddress = paramSubmitPduBase.encodedScAddress;
        }

        public String toString()
        {
            return "SubmitPdu: encodedScAddress = " + Arrays.toString(this.encodedScAddress) + ", encodedMessage = " + Arrays.toString(this.encodedMessage);
        }
    }

    public static enum MessageClass
    {
        static
        {
            CLASS_0 = new MessageClass("CLASS_0", 1);
            CLASS_1 = new MessageClass("CLASS_1", 2);
            CLASS_2 = new MessageClass("CLASS_2", 3);
            CLASS_3 = new MessageClass("CLASS_3", 4);
            MessageClass[] arrayOfMessageClass = new MessageClass[5];
            arrayOfMessageClass[0] = UNKNOWN;
            arrayOfMessageClass[1] = CLASS_0;
            arrayOfMessageClass[2] = CLASS_1;
            arrayOfMessageClass[3] = CLASS_2;
            arrayOfMessageClass[4] = CLASS_3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.SmsMessage
 * JD-Core Version:        0.6.2
 */