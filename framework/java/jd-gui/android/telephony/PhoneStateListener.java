package android.telephony;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.android.internal.telephony.IPhoneStateListener;
import com.android.internal.telephony.IPhoneStateListener.Stub;

public class PhoneStateListener
{
    public static final int LISTEN_CALL_FORWARDING_INDICATOR = 8;
    public static final int LISTEN_CALL_STATE = 32;
    public static final int LISTEN_CELL_INFO = 1024;
    public static final int LISTEN_CELL_LOCATION = 16;
    public static final int LISTEN_DATA_ACTIVITY = 128;
    public static final int LISTEN_DATA_CONNECTION_STATE = 64;
    public static final int LISTEN_MESSAGE_WAITING_INDICATOR = 4;
    public static final int LISTEN_NONE = 0;
    public static final int LISTEN_OTASP_CHANGED = 512;
    public static final int LISTEN_SERVICE_STATE = 1;

    @Deprecated
    public static final int LISTEN_SIGNAL_STRENGTH = 2;
    public static final int LISTEN_SIGNAL_STRENGTHS = 256;
    IPhoneStateListener callback = new IPhoneStateListener.Stub()
    {
        public void onCallForwardingIndicatorChanged(boolean paramAnonymousBoolean)
        {
            Handler localHandler = PhoneStateListener.this.mHandler;
            if (paramAnonymousBoolean);
            for (int i = 1; ; i = 0)
            {
                Message.obtain(localHandler, 8, i, 0, null).sendToTarget();
                return;
            }
        }

        public void onCallStateChanged(int paramAnonymousInt, String paramAnonymousString)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 32, paramAnonymousInt, 0, paramAnonymousString).sendToTarget();
        }

        public void onCellInfoChanged(CellInfo paramAnonymousCellInfo)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 1024, 0, 0).sendToTarget();
        }

        public void onCellLocationChanged(Bundle paramAnonymousBundle)
        {
            CellLocation localCellLocation = CellLocation.newFromBundle(paramAnonymousBundle);
            Message.obtain(PhoneStateListener.this.mHandler, 16, 0, 0, localCellLocation).sendToTarget();
        }

        public void onDataActivity(int paramAnonymousInt)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 128, paramAnonymousInt, 0, null).sendToTarget();
        }

        public void onDataConnectionStateChanged(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 64, paramAnonymousInt1, paramAnonymousInt2).sendToTarget();
        }

        public void onMessageWaitingIndicatorChanged(boolean paramAnonymousBoolean)
        {
            Handler localHandler = PhoneStateListener.this.mHandler;
            if (paramAnonymousBoolean);
            for (int i = 1; ; i = 0)
            {
                Message.obtain(localHandler, 4, i, 0, null).sendToTarget();
                return;
            }
        }

        public void onOtaspChanged(int paramAnonymousInt)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 512, paramAnonymousInt, 0).sendToTarget();
        }

        public void onServiceStateChanged(ServiceState paramAnonymousServiceState)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 1, 0, 0, paramAnonymousServiceState).sendToTarget();
        }

        public void onSignalStrengthChanged(int paramAnonymousInt)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 2, paramAnonymousInt, 0, null).sendToTarget();
        }

        public void onSignalStrengthsChanged(SignalStrength paramAnonymousSignalStrength)
        {
            Message.obtain(PhoneStateListener.this.mHandler, 256, 0, 0, paramAnonymousSignalStrength).sendToTarget();
        }
    };
    Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            boolean bool = true;
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 4:
            case 8:
            case 16:
            case 32:
            case 64:
            case 128:
            case 256:
            case 512:
            case 1024:
            }
            while (true)
            {
                return;
                PhoneStateListener.this.onServiceStateChanged((ServiceState)paramAnonymousMessage.obj);
                continue;
                PhoneStateListener.this.onSignalStrengthChanged(paramAnonymousMessage.arg1);
                continue;
                PhoneStateListener localPhoneStateListener2 = PhoneStateListener.this;
                if (paramAnonymousMessage.arg1 != 0);
                while (true)
                {
                    localPhoneStateListener2.onMessageWaitingIndicatorChanged(bool);
                    break;
                    bool = false;
                }
                PhoneStateListener localPhoneStateListener1 = PhoneStateListener.this;
                if (paramAnonymousMessage.arg1 != 0);
                while (true)
                {
                    localPhoneStateListener1.onCallForwardingIndicatorChanged(bool);
                    break;
                    bool = false;
                }
                PhoneStateListener.this.onCellLocationChanged((CellLocation)paramAnonymousMessage.obj);
                continue;
                PhoneStateListener.this.onCallStateChanged(paramAnonymousMessage.arg1, (String)paramAnonymousMessage.obj);
                continue;
                PhoneStateListener.this.onDataConnectionStateChanged(paramAnonymousMessage.arg1, paramAnonymousMessage.arg2);
                PhoneStateListener.this.onDataConnectionStateChanged(paramAnonymousMessage.arg1);
                continue;
                PhoneStateListener.this.onDataActivity(paramAnonymousMessage.arg1);
                continue;
                PhoneStateListener.this.onSignalStrengthsChanged((SignalStrength)paramAnonymousMessage.obj);
                continue;
                PhoneStateListener.this.onOtaspChanged(paramAnonymousMessage.arg1);
                continue;
                PhoneStateListener.this.onCellInfoChanged((CellInfo)paramAnonymousMessage.obj);
            }
        }
    };

    public void onCallForwardingIndicatorChanged(boolean paramBoolean)
    {
    }

    public void onCallStateChanged(int paramInt, String paramString)
    {
    }

    public void onCellInfoChanged(CellInfo paramCellInfo)
    {
    }

    public void onCellLocationChanged(CellLocation paramCellLocation)
    {
    }

    public void onDataActivity(int paramInt)
    {
    }

    public void onDataConnectionStateChanged(int paramInt)
    {
    }

    public void onDataConnectionStateChanged(int paramInt1, int paramInt2)
    {
    }

    public void onMessageWaitingIndicatorChanged(boolean paramBoolean)
    {
    }

    public void onOtaspChanged(int paramInt)
    {
    }

    public void onServiceStateChanged(ServiceState paramServiceState)
    {
    }

    @Deprecated
    public void onSignalStrengthChanged(int paramInt)
    {
    }

    public void onSignalStrengthsChanged(SignalStrength paramSignalStrength)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.PhoneStateListener
 * JD-Core Version:        0.6.2
 */