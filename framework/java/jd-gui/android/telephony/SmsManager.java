package android.telephony;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import com.android.internal.telephony.ISms;
import com.android.internal.telephony.ISms.Stub;
import com.android.internal.telephony.SmsRawData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class SmsManager
{
    public static final int RESULT_ERROR_FDN_CHECK_FAILURE = 6;
    public static final int RESULT_ERROR_GENERIC_FAILURE = 1;
    public static final int RESULT_ERROR_LIMIT_EXCEEDED = 5;
    public static final int RESULT_ERROR_NO_SERVICE = 4;
    public static final int RESULT_ERROR_NULL_PDU = 3;
    public static final int RESULT_ERROR_RADIO_OFF = 2;
    public static final int STATUS_ON_ICC_FREE = 0;
    public static final int STATUS_ON_ICC_READ = 1;
    public static final int STATUS_ON_ICC_SENT = 5;
    public static final int STATUS_ON_ICC_UNREAD = 3;
    public static final int STATUS_ON_ICC_UNSENT = 7;
    private static final SmsManager sInstance = new SmsManager();

    private static ArrayList<SmsMessage> createMessageListFromRawRecords(List<SmsRawData> paramList)
    {
        ArrayList localArrayList = new ArrayList();
        if (paramList != null)
        {
            int i = paramList.size();
            for (int j = 0; j < i; j++)
            {
                SmsRawData localSmsRawData = (SmsRawData)paramList.get(j);
                if (localSmsRawData != null)
                {
                    SmsMessage localSmsMessage = SmsMessage.createFromEfRecord(j + 1, localSmsRawData.getBytes());
                    if (localSmsMessage != null)
                        localArrayList.add(localSmsMessage);
                }
            }
        }
        return localArrayList;
    }

    public static ArrayList<SmsMessage> getAllMessagesFromIcc()
    {
        Object localObject = null;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                List localList = localISms.getAllMessagesFromIccEf();
                localObject = localList;
            }
            label24: return createMessageListFromRawRecords(localObject);
        }
        catch (RemoteException localRemoteException)
        {
            break label24;
        }
    }

    public static SmsManager getDefault()
    {
        return sInstance;
    }

    public boolean copyMessageToIcc(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt)
    {
        boolean bool1 = false;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.copyMessageToIccEf(paramInt, paramArrayOfByte2, paramArrayOfByte1);
                bool1 = bool2;
            }
            label34: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label34;
        }
    }

    public boolean deleteMessageFromIcc(int paramInt)
    {
        boolean bool1 = false;
        byte[] arrayOfByte = new byte['¯'];
        Arrays.fill(arrayOfByte, (byte)-1);
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.updateMessageOnIccEf(paramInt, 0, arrayOfByte);
                bool1 = bool2;
            }
            label44: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label44;
        }
    }

    public boolean disableCellBroadcast(int paramInt)
    {
        boolean bool1 = false;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.disableCellBroadcast(paramInt);
                bool1 = bool2;
            }
            label30: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    public boolean disableCellBroadcastRange(int paramInt1, int paramInt2)
    {
        boolean bool1 = false;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.disableCellBroadcastRange(paramInt1, paramInt2);
                bool1 = bool2;
            }
            label31: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label31;
        }
    }

    public ArrayList<String> divideMessage(String paramString)
    {
        return SmsMessage.fragmentText(paramString);
    }

    public boolean enableCellBroadcast(int paramInt)
    {
        boolean bool1 = false;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.enableCellBroadcast(paramInt);
                bool1 = bool2;
            }
            label30: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    public boolean enableCellBroadcastRange(int paramInt1, int paramInt2)
    {
        boolean bool1 = false;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.enableCellBroadcastRange(paramInt1, paramInt2);
                bool1 = bool2;
            }
            label31: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label31;
        }
    }

    public void sendDataMessage(String paramString1, String paramString2, short paramShort, byte[] paramArrayOfByte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("Invalid destinationAddress");
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length == 0))
            throw new IllegalArgumentException("Invalid message data");
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
                localISms.sendData(paramString1, paramString2, paramShort & 0xFFFF, paramArrayOfByte, paramPendingIntent1, paramPendingIntent2);
            label72: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label72;
        }
    }

    public void sendMultipartTextMessage(String paramString1, String paramString2, ArrayList<String> paramArrayList, ArrayList<PendingIntent> paramArrayList1, ArrayList<PendingIntent> paramArrayList2)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("Invalid destinationAddress");
        if ((paramArrayList == null) || (paramArrayList.size() < 1))
            throw new IllegalArgumentException("Invalid message body");
        if (paramArrayList.size() > 1);
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
                localISms.sendMultipartText(paramString1, paramString2, paramArrayList, paramArrayList1, paramArrayList2);
            while (true)
            {
                label76: return;
                PendingIntent localPendingIntent1 = null;
                PendingIntent localPendingIntent2 = null;
                if ((paramArrayList1 != null) && (paramArrayList1.size() > 0))
                    localPendingIntent1 = (PendingIntent)paramArrayList1.get(0);
                if ((paramArrayList2 != null) && (paramArrayList2.size() > 0))
                    localPendingIntent2 = (PendingIntent)paramArrayList2.get(0);
                sendTextMessage(paramString1, paramString2, (String)paramArrayList.get(0), localPendingIntent1, localPendingIntent2);
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label76;
        }
    }

    public void sendTextMessage(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("Invalid destinationAddress");
        if (TextUtils.isEmpty(paramString3))
            throw new IllegalArgumentException("Invalid message body");
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
                localISms.sendText(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
            label63: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label63;
        }
    }

    public boolean updateMessageOnIcc(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        boolean bool1 = false;
        try
        {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
            if (localISms != null)
            {
                boolean bool2 = localISms.updateMessageOnIccEf(paramInt1, paramInt2, paramArrayOfByte);
                bool1 = bool2;
            }
            label34: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label34;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.SmsManager
 * JD-Core Version:        0.6.2
 */