package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public abstract class CellIdentity
    implements Parcelable
{
    public static final int CELLID_TYPE_CDMA = 2;
    public static final int CELLID_TYPE_GSM = 1;
    public static final int CELLID_TYPE_LTE = 3;
    private String mCellIdAttributes;
    private int mCellIdType;

    protected CellIdentity(int paramInt, String paramString)
    {
        this.mCellIdType = paramInt;
        this.mCellIdAttributes = new String(paramString);
    }

    protected CellIdentity(Parcel paramParcel)
    {
        this.mCellIdType = paramParcel.readInt();
        this.mCellIdAttributes = new String(paramParcel.readString());
    }

    protected CellIdentity(CellIdentity paramCellIdentity)
    {
        this.mCellIdType = paramCellIdentity.mCellIdType;
        this.mCellIdAttributes = new String(paramCellIdentity.mCellIdAttributes);
    }

    public int describeContents()
    {
        return 0;
    }

    public String getCellIdAttributes()
    {
        return this.mCellIdAttributes;
    }

    public int getCellIdType()
    {
        return this.mCellIdType;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mCellIdType);
        paramParcel.writeString(this.mCellIdAttributes);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.CellIdentity
 * JD-Core Version:        0.6.2
 */