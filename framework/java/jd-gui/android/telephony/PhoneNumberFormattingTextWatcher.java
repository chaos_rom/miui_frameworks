package android.telephony;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import com.android.i18n.phonenumbers.AsYouTypeFormatter;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import java.util.Locale;

public class PhoneNumberFormattingTextWatcher
    implements TextWatcher
{
    private AsYouTypeFormatter mFormatter;
    private boolean mSelfChange = false;
    private boolean mStopFormatting;

    public PhoneNumberFormattingTextWatcher()
    {
        this(Locale.getDefault().getCountry());
    }

    public PhoneNumberFormattingTextWatcher(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException();
        this.mFormatter = PhoneNumberUtil.getInstance().getAsYouTypeFormatter(paramString);
    }

    private String getFormattedNumber(char paramChar, boolean paramBoolean)
    {
        if (paramBoolean);
        for (String str = this.mFormatter.inputDigitAndRememberPosition(paramChar); ; str = this.mFormatter.inputDigit(paramChar))
            return str;
    }

    private boolean hasSeparator(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        if (i < paramInt1 + paramInt2)
            if (PhoneNumberUtils.isNonSeparator(paramCharSequence.charAt(i)));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    private String reformat(CharSequence paramCharSequence, int paramInt)
    {
        int i = paramInt - 1;
        String str = null;
        this.mFormatter.clear();
        char c1 = '\000';
        boolean bool = false;
        int j = paramCharSequence.length();
        for (int k = 0; k < j; k++)
        {
            char c2 = paramCharSequence.charAt(k);
            if (PhoneNumberUtils.isNonSeparator(c2))
            {
                if (c1 != 0)
                {
                    str = getFormattedNumber(c1, bool);
                    bool = false;
                }
                c1 = c2;
            }
            if (k == i)
                bool = true;
        }
        if (c1 != 0)
            str = getFormattedNumber(c1, bool);
        return str;
    }

    private void stopFormatting()
    {
        this.mStopFormatting = true;
        this.mFormatter.clear();
    }

    /** @deprecated */
    public void afterTextChanged(Editable paramEditable)
    {
        boolean bool = true;
        try
        {
            if (this.mStopFormatting)
                if (paramEditable.length() != 0)
                    this.mStopFormatting = bool;
            while (true)
            {
                return;
                bool = false;
                break;
                if (!this.mSelfChange)
                {
                    String str = reformat(paramEditable, Selection.getSelectionEnd(paramEditable));
                    if (str != null)
                    {
                        int i = this.mFormatter.getRememberedPosition();
                        this.mSelfChange = true;
                        paramEditable.replace(0, paramEditable.length(), str, 0, str.length());
                        if (str.equals(paramEditable.toString()))
                            Selection.setSelection(paramEditable, i);
                        this.mSelfChange = false;
                    }
                }
            }
        }
        finally
        {
        }
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        if ((this.mSelfChange) || (this.mStopFormatting));
        while (true)
        {
            return;
            if ((paramInt2 > 0) && (hasSeparator(paramCharSequence, paramInt1, paramInt2)))
                stopFormatting();
        }
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        if ((this.mSelfChange) || (this.mStopFormatting));
        while (true)
        {
            return;
            if ((paramInt3 > 0) && (hasSeparator(paramCharSequence, paramInt1, paramInt3)))
                stopFormatting();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.PhoneNumberFormattingTextWatcher
 * JD-Core Version:        0.6.2
 */