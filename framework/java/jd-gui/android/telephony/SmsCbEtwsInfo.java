package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.format.Time;
import com.android.internal.telephony.IccUtils;
import java.util.Arrays;

public class SmsCbEtwsInfo
    implements Parcelable
{
    public static final Parcelable.Creator<SmsCbEtwsInfo> CREATOR = new Parcelable.Creator()
    {
        public SmsCbEtwsInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SmsCbEtwsInfo(paramAnonymousParcel);
        }

        public SmsCbEtwsInfo[] newArray(int paramAnonymousInt)
        {
            return new SmsCbEtwsInfo[paramAnonymousInt];
        }
    };
    public static final int ETWS_WARNING_TYPE_EARTHQUAKE = 0;
    public static final int ETWS_WARNING_TYPE_EARTHQUAKE_AND_TSUNAMI = 2;
    public static final int ETWS_WARNING_TYPE_OTHER_EMERGENCY = 4;
    public static final int ETWS_WARNING_TYPE_TEST_MESSAGE = 3;
    public static final int ETWS_WARNING_TYPE_TSUNAMI = 1;
    public static final int ETWS_WARNING_TYPE_UNKNOWN = -1;
    private final boolean mActivatePopup;
    private final boolean mEmergencyUserAlert;
    private final byte[] mWarningSecurityInformation;
    private final int mWarningType;

    public SmsCbEtwsInfo(int paramInt, boolean paramBoolean1, boolean paramBoolean2, byte[] paramArrayOfByte)
    {
        this.mWarningType = paramInt;
        this.mEmergencyUserAlert = paramBoolean1;
        this.mActivatePopup = paramBoolean2;
        this.mWarningSecurityInformation = paramArrayOfByte;
    }

    SmsCbEtwsInfo(Parcel paramParcel)
    {
        this.mWarningType = paramParcel.readInt();
        boolean bool2;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.mEmergencyUserAlert = bool2;
            if (paramParcel.readInt() == 0)
                break label54;
        }
        while (true)
        {
            this.mActivatePopup = bool1;
            this.mWarningSecurityInformation = paramParcel.createByteArray();
            return;
            bool2 = false;
            break;
            label54: bool1 = false;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public byte[] getPrimaryNotificationSignature()
    {
        if ((this.mWarningSecurityInformation == null) || (this.mWarningSecurityInformation.length < 50));
        for (byte[] arrayOfByte = null; ; arrayOfByte = Arrays.copyOfRange(this.mWarningSecurityInformation, 7, 50))
            return arrayOfByte;
    }

    public long getPrimaryNotificationTimestamp()
    {
        long l;
        if ((this.mWarningSecurityInformation == null) || (this.mWarningSecurityInformation.length < 7))
        {
            l = 0L;
            return l;
        }
        int i = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[0]);
        int j = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[1]);
        int k = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[2]);
        int m = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[3]);
        int n = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[4]);
        int i1 = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[5]);
        int i2 = this.mWarningSecurityInformation[6];
        int i3 = IccUtils.gsmBcdByteToInt((byte)(i2 & 0xFFFFFFF7));
        if ((i2 & 0x8) == 0);
        while (true)
        {
            Time localTime = new Time("UTC");
            localTime.year = (i + 2000);
            localTime.month = (j - 1);
            localTime.monthDay = k;
            localTime.hour = m;
            localTime.minute = n;
            localTime.second = i1;
            l = localTime.toMillis(true) - 1000 * (60 * (i3 * 15));
            break;
            i3 = -i3;
        }
    }

    public int getWarningType()
    {
        return this.mWarningType;
    }

    public boolean isEmergencyUserAlert()
    {
        return this.mEmergencyUserAlert;
    }

    public boolean isPopupAlert()
    {
        return this.mActivatePopup;
    }

    public String toString()
    {
        return "SmsCbEtwsInfo{warningType=" + this.mWarningType + ", emergencyUserAlert=" + this.mEmergencyUserAlert + ", activatePopup=" + this.mActivatePopup + '}';
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(this.mWarningType);
        int j;
        if (this.mEmergencyUserAlert)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.mActivatePopup)
                break label53;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeByteArray(this.mWarningSecurityInformation);
            return;
            j = 0;
            break;
            label53: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.SmsCbEtwsInfo
 * JD-Core Version:        0.6.2
 */