package android.telephony.cdma;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CdmaSmsCbProgramResults
    implements Parcelable
{
    public static final Parcelable.Creator<CdmaSmsCbProgramResults> CREATOR = new Parcelable.Creator()
    {
        public CdmaSmsCbProgramResults createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CdmaSmsCbProgramResults(paramAnonymousParcel);
        }

        public CdmaSmsCbProgramResults[] newArray(int paramAnonymousInt)
        {
            return new CdmaSmsCbProgramResults[paramAnonymousInt];
        }
    };
    public static final int RESULT_CATEGORY_ALREADY_ADDED = 3;
    public static final int RESULT_CATEGORY_ALREADY_DELETED = 4;
    public static final int RESULT_CATEGORY_LIMIT_EXCEEDED = 2;
    public static final int RESULT_INVALID_ALERT_OPTION = 6;
    public static final int RESULT_INVALID_CATEGORY_NAME = 7;
    public static final int RESULT_INVALID_MAX_MESSAGES = 5;
    public static final int RESULT_MEMORY_LIMIT_EXCEEDED = 1;
    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_UNSPECIFIED_FAILURE = 8;
    private final int mCategory;
    private final int mCategoryResult;
    private final int mLanguage;

    public CdmaSmsCbProgramResults(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mCategory = paramInt1;
        this.mLanguage = paramInt2;
        this.mCategoryResult = paramInt3;
    }

    CdmaSmsCbProgramResults(Parcel paramParcel)
    {
        this.mCategory = paramParcel.readInt();
        this.mLanguage = paramParcel.readInt();
        this.mCategoryResult = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCategory()
    {
        return this.mCategory;
    }

    public int getCategoryResult()
    {
        return this.mCategoryResult;
    }

    public int getLanguage()
    {
        return this.mLanguage;
    }

    public String toString()
    {
        return "CdmaSmsCbProgramResults{category=" + this.mCategory + ", language=" + this.mLanguage + ", result=" + this.mCategoryResult + '}';
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mCategory);
        paramParcel.writeInt(this.mLanguage);
        paramParcel.writeInt(this.mCategoryResult);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.cdma.CdmaSmsCbProgramResults
 * JD-Core Version:        0.6.2
 */