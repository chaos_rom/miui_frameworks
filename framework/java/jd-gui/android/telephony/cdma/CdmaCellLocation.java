package android.telephony.cdma;

import android.os.Bundle;
import android.telephony.CellLocation;

public class CdmaCellLocation extends CellLocation
{
    public static final int INVALID_LAT_LONG = 2147483647;
    private int mBaseStationId = -1;
    private int mBaseStationLatitude = 2147483647;
    private int mBaseStationLongitude = 2147483647;
    private int mNetworkId = -1;
    private int mSystemId = -1;

    public CdmaCellLocation()
    {
        this.mBaseStationId = -1;
        this.mBaseStationLatitude = 2147483647;
        this.mBaseStationLongitude = 2147483647;
        this.mSystemId = -1;
        this.mNetworkId = -1;
    }

    public CdmaCellLocation(Bundle paramBundle)
    {
        this.mBaseStationId = paramBundle.getInt("baseStationId", this.mBaseStationId);
        this.mBaseStationLatitude = paramBundle.getInt("baseStationLatitude", this.mBaseStationLatitude);
        this.mBaseStationLongitude = paramBundle.getInt("baseStationLongitude", this.mBaseStationLongitude);
        this.mSystemId = paramBundle.getInt("systemId", this.mSystemId);
        this.mNetworkId = paramBundle.getInt("networkId", this.mNetworkId);
    }

    private static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2)
    {
        boolean bool;
        if (paramObject1 == null)
            if (paramObject2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramObject1.equals(paramObject2);
        }
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        try
        {
            localCdmaCellLocation = (CdmaCellLocation)paramObject;
            if (paramObject == null)
                return bool;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                CdmaCellLocation localCdmaCellLocation;
                continue;
                if ((equalsHandlesNulls(Integer.valueOf(this.mBaseStationId), Integer.valueOf(localCdmaCellLocation.mBaseStationId))) && (equalsHandlesNulls(Integer.valueOf(this.mBaseStationLatitude), Integer.valueOf(localCdmaCellLocation.mBaseStationLatitude))) && (equalsHandlesNulls(Integer.valueOf(this.mBaseStationLongitude), Integer.valueOf(localCdmaCellLocation.mBaseStationLongitude))) && (equalsHandlesNulls(Integer.valueOf(this.mSystemId), Integer.valueOf(localCdmaCellLocation.mSystemId))) && (equalsHandlesNulls(Integer.valueOf(this.mNetworkId), Integer.valueOf(localCdmaCellLocation.mNetworkId))))
                    bool = true;
            }
        }
    }

    public void fillInNotifierBundle(Bundle paramBundle)
    {
        paramBundle.putInt("baseStationId", this.mBaseStationId);
        paramBundle.putInt("baseStationLatitude", this.mBaseStationLatitude);
        paramBundle.putInt("baseStationLongitude", this.mBaseStationLongitude);
        paramBundle.putInt("systemId", this.mSystemId);
        paramBundle.putInt("networkId", this.mNetworkId);
    }

    public int getBaseStationId()
    {
        return this.mBaseStationId;
    }

    public int getBaseStationLatitude()
    {
        return this.mBaseStationLatitude;
    }

    public int getBaseStationLongitude()
    {
        return this.mBaseStationLongitude;
    }

    public int getNetworkId()
    {
        return this.mNetworkId;
    }

    public int getSystemId()
    {
        return this.mSystemId;
    }

    public int hashCode()
    {
        return this.mBaseStationId ^ this.mBaseStationLatitude ^ this.mBaseStationLongitude ^ this.mSystemId ^ this.mNetworkId;
    }

    public boolean isEmpty()
    {
        if ((this.mBaseStationId == -1) && (this.mBaseStationLatitude == 2147483647) && (this.mBaseStationLongitude == 2147483647) && (this.mSystemId == -1) && (this.mNetworkId == -1));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setCellLocationData(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mBaseStationId = paramInt1;
        this.mBaseStationLatitude = paramInt2;
        this.mBaseStationLongitude = paramInt3;
    }

    public void setCellLocationData(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        this.mBaseStationId = paramInt1;
        this.mBaseStationLatitude = paramInt2;
        this.mBaseStationLongitude = paramInt3;
        this.mSystemId = paramInt4;
        this.mNetworkId = paramInt5;
    }

    public void setStateInvalid()
    {
        this.mBaseStationId = -1;
        this.mBaseStationLatitude = 2147483647;
        this.mBaseStationLongitude = 2147483647;
        this.mSystemId = -1;
        this.mNetworkId = -1;
    }

    public String toString()
    {
        return "[" + this.mBaseStationId + "," + this.mBaseStationLatitude + "," + this.mBaseStationLongitude + "," + this.mSystemId + "," + this.mNetworkId + "]";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.cdma.CdmaCellLocation
 * JD-Core Version:        0.6.2
 */