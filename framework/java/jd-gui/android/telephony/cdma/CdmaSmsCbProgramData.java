package android.telephony.cdma;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CdmaSmsCbProgramData
    implements Parcelable
{
    public static final int ALERT_OPTION_DEFAULT_ALERT = 1;
    public static final int ALERT_OPTION_HIGH_PRIORITY_ONCE = 10;
    public static final int ALERT_OPTION_HIGH_PRIORITY_REPEAT = 11;
    public static final int ALERT_OPTION_LOW_PRIORITY_ONCE = 6;
    public static final int ALERT_OPTION_LOW_PRIORITY_REPEAT = 7;
    public static final int ALERT_OPTION_MED_PRIORITY_ONCE = 8;
    public static final int ALERT_OPTION_MED_PRIORITY_REPEAT = 9;
    public static final int ALERT_OPTION_NO_ALERT = 0;
    public static final int ALERT_OPTION_VIBRATE_ONCE = 2;
    public static final int ALERT_OPTION_VIBRATE_REPEAT = 3;
    public static final int ALERT_OPTION_VISUAL_ONCE = 4;
    public static final int ALERT_OPTION_VISUAL_REPEAT = 5;
    public static final Parcelable.Creator<CdmaSmsCbProgramData> CREATOR = new Parcelable.Creator()
    {
        public CdmaSmsCbProgramData createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CdmaSmsCbProgramData(paramAnonymousParcel);
        }

        public CdmaSmsCbProgramData[] newArray(int paramAnonymousInt)
        {
            return new CdmaSmsCbProgramData[paramAnonymousInt];
        }
    };
    public static final int OPERATION_ADD_CATEGORY = 1;
    public static final int OPERATION_CLEAR_CATEGORIES = 2;
    public static final int OPERATION_DELETE_CATEGORY;
    private final int mAlertOption;
    private final int mCategory;
    private final String mCategoryName;
    private final int mLanguage;
    private final int mMaxMessages;
    private final int mOperation;

    public CdmaSmsCbProgramData(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString)
    {
        this.mOperation = paramInt1;
        this.mCategory = paramInt2;
        this.mLanguage = paramInt3;
        this.mMaxMessages = paramInt4;
        this.mAlertOption = paramInt5;
        this.mCategoryName = paramString;
    }

    CdmaSmsCbProgramData(Parcel paramParcel)
    {
        this.mOperation = paramParcel.readInt();
        this.mCategory = paramParcel.readInt();
        this.mLanguage = paramParcel.readInt();
        this.mMaxMessages = paramParcel.readInt();
        this.mAlertOption = paramParcel.readInt();
        this.mCategoryName = paramParcel.readString();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getAlertOption()
    {
        return this.mAlertOption;
    }

    public int getCategory()
    {
        return this.mCategory;
    }

    public String getCategoryName()
    {
        return this.mCategoryName;
    }

    public int getLanguage()
    {
        return this.mLanguage;
    }

    public int getMaxMessages()
    {
        return this.mMaxMessages;
    }

    public int getOperation()
    {
        return this.mOperation;
    }

    public String toString()
    {
        return "CdmaSmsCbProgramData{operation=" + this.mOperation + ", category=" + this.mCategory + ", language=" + this.mLanguage + ", max messages=" + this.mMaxMessages + ", alert option=" + this.mAlertOption + ", category name=" + this.mCategoryName + '}';
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mOperation);
        paramParcel.writeInt(this.mCategory);
        paramParcel.writeInt(this.mLanguage);
        paramParcel.writeInt(this.mMaxMessages);
        paramParcel.writeInt(this.mAlertOption);
        paramParcel.writeString(this.mCategoryName);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.cdma.CdmaSmsCbProgramData
 * JD-Core Version:        0.6.2
 */