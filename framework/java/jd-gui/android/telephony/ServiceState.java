package android.telephony;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

public class ServiceState
    implements Parcelable
{
    public static final Parcelable.Creator<ServiceState> CREATOR = new Parcelable.Creator()
    {
        public ServiceState createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ServiceState(paramAnonymousParcel);
        }

        public ServiceState[] newArray(int paramAnonymousInt)
        {
            return new ServiceState[paramAnonymousInt];
        }
    };
    static final String LOG_TAG = "PHONE";
    public static final int REGISTRATION_STATE_HOME_NETWORK = 1;
    public static final int REGISTRATION_STATE_NOT_REGISTERED_AND_NOT_SEARCHING = 0;
    public static final int REGISTRATION_STATE_NOT_REGISTERED_AND_SEARCHING = 2;
    public static final int REGISTRATION_STATE_REGISTRATION_DENIED = 3;
    public static final int REGISTRATION_STATE_ROAMING = 5;
    public static final int REGISTRATION_STATE_UNKNOWN = 4;
    public static final int RIL_RADIO_TECHNOLOGY_1xRTT = 6;
    public static final int RIL_RADIO_TECHNOLOGY_EDGE = 2;
    public static final int RIL_RADIO_TECHNOLOGY_EHRPD = 13;
    public static final int RIL_RADIO_TECHNOLOGY_EVDO_0 = 7;
    public static final int RIL_RADIO_TECHNOLOGY_EVDO_A = 8;
    public static final int RIL_RADIO_TECHNOLOGY_EVDO_B = 12;
    public static final int RIL_RADIO_TECHNOLOGY_GPRS = 1;
    public static final int RIL_RADIO_TECHNOLOGY_GSM = 16;
    public static final int RIL_RADIO_TECHNOLOGY_HSDPA = 9;
    public static final int RIL_RADIO_TECHNOLOGY_HSPA = 11;
    public static final int RIL_RADIO_TECHNOLOGY_HSPAP = 15;
    public static final int RIL_RADIO_TECHNOLOGY_HSUPA = 10;
    public static final int RIL_RADIO_TECHNOLOGY_IS95A = 4;
    public static final int RIL_RADIO_TECHNOLOGY_IS95B = 5;
    public static final int RIL_RADIO_TECHNOLOGY_LTE = 14;
    public static final int RIL_RADIO_TECHNOLOGY_UMTS = 3;
    public static final int RIL_RADIO_TECHNOLOGY_UNKNOWN = 0;
    public static final int STATE_EMERGENCY_ONLY = 2;
    public static final int STATE_IN_SERVICE = 0;
    public static final int STATE_OUT_OF_SERVICE = 1;
    public static final int STATE_POWER_OFF = 3;
    private int mCdmaDefaultRoamingIndicator;
    private int mCdmaEriIconIndex;
    private int mCdmaEriIconMode;
    private int mCdmaRoamingIndicator;
    private boolean mCssIndicator;
    private boolean mIsEmergencyOnly;
    private boolean mIsManualNetworkSelection;
    private int mNetworkId;
    private String mOperatorAlphaLong;
    private String mOperatorAlphaShort;
    private String mOperatorNumeric;
    private int mRadioTechnology;
    private boolean mRoaming;
    private int mState;
    private int mSystemId;

    public ServiceState()
    {
        this.mState = 1;
    }

    public ServiceState(Parcel paramParcel)
    {
        this.mState = i;
        this.mState = paramParcel.readInt();
        boolean bool1;
        boolean bool2;
        label67: boolean bool3;
        if (paramParcel.readInt() != 0)
        {
            bool1 = i;
            this.mRoaming = bool1;
            this.mOperatorAlphaLong = paramParcel.readString();
            this.mOperatorAlphaShort = paramParcel.readString();
            this.mOperatorNumeric = paramParcel.readString();
            if (paramParcel.readInt() == 0)
                break label163;
            bool2 = i;
            this.mIsManualNetworkSelection = bool2;
            this.mRadioTechnology = paramParcel.readInt();
            if (paramParcel.readInt() == 0)
                break label169;
            bool3 = i;
            label91: this.mCssIndicator = bool3;
            this.mNetworkId = paramParcel.readInt();
            this.mSystemId = paramParcel.readInt();
            this.mCdmaRoamingIndicator = paramParcel.readInt();
            this.mCdmaDefaultRoamingIndicator = paramParcel.readInt();
            this.mCdmaEriIconIndex = paramParcel.readInt();
            this.mCdmaEriIconMode = paramParcel.readInt();
            if (paramParcel.readInt() == 0)
                break label175;
        }
        while (true)
        {
            this.mIsEmergencyOnly = i;
            return;
            bool1 = false;
            break;
            label163: bool2 = false;
            break label67;
            label169: bool3 = false;
            break label91;
            label175: i = 0;
        }
    }

    public ServiceState(ServiceState paramServiceState)
    {
        this.mState = 1;
        copyFrom(paramServiceState);
    }

    private static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2)
    {
        boolean bool;
        if (paramObject1 == null)
            if (paramObject2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramObject1.equals(paramObject2);
        }
    }

    public static boolean isCdma(int paramInt)
    {
        if ((paramInt == 4) || (paramInt == 5) || (paramInt == 6) || (paramInt == 7) || (paramInt == 8) || (paramInt == 12) || (paramInt == 13));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isGsm(int paramInt)
    {
        int i = 1;
        if ((paramInt == i) || (paramInt == 2) || (paramInt == 3) || (paramInt == 9) || (paramInt == 10) || (paramInt == 11) || (paramInt == 14) || (paramInt == 15) || (paramInt == 16));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public static ServiceState newFromBundle(Bundle paramBundle)
    {
        ServiceState localServiceState = new ServiceState();
        localServiceState.setFromNotifierBundle(paramBundle);
        return localServiceState;
    }

    public static String rilRadioTechnologyToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "Unexpected";
            Log.w("PHONE", "Unexpected radioTechnology=" + paramInt);
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        }
        while (true)
        {
            return str;
            str = "Unknown";
            continue;
            str = "GPRS";
            continue;
            str = "EDGE";
            continue;
            str = "UMTS";
            continue;
            str = "CDMA-IS95A";
            continue;
            str = "CDMA-IS95B";
            continue;
            str = "1xRTT";
            continue;
            str = "EvDo-rev.0";
            continue;
            str = "EvDo-rev.A";
            continue;
            str = "HSDPA";
            continue;
            str = "HSUPA";
            continue;
            str = "HSPA";
            continue;
            str = "EvDo-rev.B";
            continue;
            str = "eHRPD";
            continue;
            str = "LTE";
            continue;
            str = "HSPAP";
            continue;
            str = "GSM";
        }
    }

    private void setFromNotifierBundle(Bundle paramBundle)
    {
        this.mState = paramBundle.getInt("state");
        this.mRoaming = paramBundle.getBoolean("roaming");
        this.mOperatorAlphaLong = paramBundle.getString("operator-alpha-long");
        this.mOperatorAlphaShort = paramBundle.getString("operator-alpha-short");
        this.mOperatorNumeric = paramBundle.getString("operator-numeric");
        this.mIsManualNetworkSelection = paramBundle.getBoolean("manual");
        this.mRadioTechnology = paramBundle.getInt("radioTechnology");
        this.mCssIndicator = paramBundle.getBoolean("cssIndicator");
        this.mNetworkId = paramBundle.getInt("networkId");
        this.mSystemId = paramBundle.getInt("systemId");
        this.mCdmaRoamingIndicator = paramBundle.getInt("cdmaRoamingIndicator");
        this.mCdmaDefaultRoamingIndicator = paramBundle.getInt("cdmaDefaultRoamingIndicator");
        this.mIsEmergencyOnly = paramBundle.getBoolean("emergencyOnly");
    }

    private void setNullState(int paramInt)
    {
        this.mState = paramInt;
        this.mRoaming = false;
        this.mOperatorAlphaLong = null;
        this.mOperatorAlphaShort = null;
        this.mOperatorNumeric = null;
        this.mIsManualNetworkSelection = false;
        this.mRadioTechnology = 0;
        this.mCssIndicator = false;
        this.mNetworkId = -1;
        this.mSystemId = -1;
        this.mCdmaRoamingIndicator = -1;
        this.mCdmaDefaultRoamingIndicator = -1;
        this.mCdmaEriIconIndex = -1;
        this.mCdmaEriIconMode = -1;
        this.mIsEmergencyOnly = false;
    }

    protected void copyFrom(ServiceState paramServiceState)
    {
        this.mState = paramServiceState.mState;
        this.mRoaming = paramServiceState.mRoaming;
        this.mOperatorAlphaLong = paramServiceState.mOperatorAlphaLong;
        this.mOperatorAlphaShort = paramServiceState.mOperatorAlphaShort;
        this.mOperatorNumeric = paramServiceState.mOperatorNumeric;
        this.mIsManualNetworkSelection = paramServiceState.mIsManualNetworkSelection;
        this.mRadioTechnology = paramServiceState.mRadioTechnology;
        this.mCssIndicator = paramServiceState.mCssIndicator;
        this.mNetworkId = paramServiceState.mNetworkId;
        this.mSystemId = paramServiceState.mSystemId;
        this.mCdmaRoamingIndicator = paramServiceState.mCdmaRoamingIndicator;
        this.mCdmaDefaultRoamingIndicator = paramServiceState.mCdmaDefaultRoamingIndicator;
        this.mCdmaEriIconIndex = paramServiceState.mCdmaEriIconIndex;
        this.mCdmaEriIconMode = paramServiceState.mCdmaEriIconMode;
        this.mIsEmergencyOnly = paramServiceState.mIsEmergencyOnly;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        try
        {
            localServiceState = (ServiceState)paramObject;
            if (paramObject == null)
                return bool;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                ServiceState localServiceState;
                continue;
                if ((this.mState == localServiceState.mState) && (this.mRoaming == localServiceState.mRoaming) && (this.mIsManualNetworkSelection == localServiceState.mIsManualNetworkSelection) && (equalsHandlesNulls(this.mOperatorAlphaLong, localServiceState.mOperatorAlphaLong)) && (equalsHandlesNulls(this.mOperatorAlphaShort, localServiceState.mOperatorAlphaShort)) && (equalsHandlesNulls(this.mOperatorNumeric, localServiceState.mOperatorNumeric)) && (equalsHandlesNulls(Integer.valueOf(this.mRadioTechnology), Integer.valueOf(localServiceState.mRadioTechnology))) && (equalsHandlesNulls(Boolean.valueOf(this.mCssIndicator), Boolean.valueOf(localServiceState.mCssIndicator))) && (equalsHandlesNulls(Integer.valueOf(this.mNetworkId), Integer.valueOf(localServiceState.mNetworkId))) && (equalsHandlesNulls(Integer.valueOf(this.mSystemId), Integer.valueOf(localServiceState.mSystemId))) && (equalsHandlesNulls(Integer.valueOf(this.mCdmaRoamingIndicator), Integer.valueOf(localServiceState.mCdmaRoamingIndicator))) && (equalsHandlesNulls(Integer.valueOf(this.mCdmaDefaultRoamingIndicator), Integer.valueOf(localServiceState.mCdmaDefaultRoamingIndicator))) && (this.mIsEmergencyOnly == localServiceState.mIsEmergencyOnly))
                    bool = true;
            }
        }
    }

    public void fillInNotifierBundle(Bundle paramBundle)
    {
        paramBundle.putInt("state", this.mState);
        paramBundle.putBoolean("roaming", Boolean.valueOf(this.mRoaming).booleanValue());
        paramBundle.putString("operator-alpha-long", this.mOperatorAlphaLong);
        paramBundle.putString("operator-alpha-short", this.mOperatorAlphaShort);
        paramBundle.putString("operator-numeric", this.mOperatorNumeric);
        paramBundle.putBoolean("manual", Boolean.valueOf(this.mIsManualNetworkSelection).booleanValue());
        paramBundle.putInt("radioTechnology", this.mRadioTechnology);
        paramBundle.putBoolean("cssIndicator", this.mCssIndicator);
        paramBundle.putInt("networkId", this.mNetworkId);
        paramBundle.putInt("systemId", this.mSystemId);
        paramBundle.putInt("cdmaRoamingIndicator", this.mCdmaRoamingIndicator);
        paramBundle.putInt("cdmaDefaultRoamingIndicator", this.mCdmaDefaultRoamingIndicator);
        paramBundle.putBoolean("emergencyOnly", Boolean.valueOf(this.mIsEmergencyOnly).booleanValue());
    }

    public int getCdmaDefaultRoamingIndicator()
    {
        return this.mCdmaDefaultRoamingIndicator;
    }

    public int getCdmaEriIconIndex()
    {
        return this.mCdmaEriIconIndex;
    }

    public int getCdmaEriIconMode()
    {
        return this.mCdmaEriIconMode;
    }

    public int getCdmaRoamingIndicator()
    {
        return this.mCdmaRoamingIndicator;
    }

    public int getCssIndicator()
    {
        if (this.mCssIndicator);
        for (int i = 1; ; i = 0)
            return i;
    }

    public boolean getIsManualSelection()
    {
        return this.mIsManualNetworkSelection;
    }

    public int getNetworkId()
    {
        return this.mNetworkId;
    }

    public int getNetworkType()
    {
        int i;
        switch (this.mRadioTechnology)
        {
        default:
            i = 0;
        case 1:
        case 2:
        case 3:
        case 9:
        case 10:
        case 11:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 12:
        case 13:
        case 14:
        case 15:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
            continue;
            i = 8;
            continue;
            i = 9;
            continue;
            i = 10;
            continue;
            i = 4;
            continue;
            i = 7;
            continue;
            i = 5;
            continue;
            i = 6;
            continue;
            i = 12;
            continue;
            i = 14;
            continue;
            i = 13;
            continue;
            i = 15;
        }
    }

    public String getOperatorAlphaLong()
    {
        return this.mOperatorAlphaLong;
    }

    public String getOperatorAlphaShort()
    {
        return this.mOperatorAlphaShort;
    }

    public String getOperatorNumeric()
    {
        return this.mOperatorNumeric;
    }

    public int getRadioTechnology()
    {
        return getRilRadioTechnology();
    }

    public int getRilRadioTechnology()
    {
        return this.mRadioTechnology;
    }

    public boolean getRoaming()
    {
        return this.mRoaming;
    }

    public int getState()
    {
        return this.mState;
    }

    public int getSystemId()
    {
        return this.mSystemId;
    }

    public int hashCode()
    {
        int i = 1;
        int j = 4660 * this.mState;
        int k;
        int n;
        label35: int i2;
        label52: int i4;
        label69: int i6;
        label86: int i7;
        if (this.mRoaming)
        {
            k = i;
            int m = j + k;
            if (!this.mIsManualNetworkSelection)
                break label120;
            n = i;
            int i1 = m + n;
            if (this.mOperatorAlphaLong != null)
                break label126;
            i2 = 0;
            int i3 = i1 + i2;
            if (this.mOperatorAlphaShort != null)
                break label138;
            i4 = 0;
            int i5 = i3 + i4;
            if (this.mOperatorNumeric != null)
                break label150;
            i6 = 0;
            i7 = i6 + i5 + this.mCdmaRoamingIndicator + this.mCdmaDefaultRoamingIndicator;
            if (!this.mIsEmergencyOnly)
                break label162;
        }
        while (true)
        {
            return i7 + i;
            k = 0;
            break;
            label120: n = 0;
            break label35;
            label126: i2 = this.mOperatorAlphaLong.hashCode();
            break label52;
            label138: i4 = this.mOperatorAlphaShort.hashCode();
            break label69;
            label150: i6 = this.mOperatorNumeric.hashCode();
            break label86;
            label162: i = 0;
        }
    }

    public boolean isEmergencyOnly()
    {
        return this.mIsEmergencyOnly;
    }

    public void setCdmaDefaultRoamingIndicator(int paramInt)
    {
        this.mCdmaDefaultRoamingIndicator = paramInt;
    }

    public void setCdmaEriIconIndex(int paramInt)
    {
        this.mCdmaEriIconIndex = paramInt;
    }

    public void setCdmaEriIconMode(int paramInt)
    {
        this.mCdmaEriIconMode = paramInt;
    }

    public void setCdmaRoamingIndicator(int paramInt)
    {
        this.mCdmaRoamingIndicator = paramInt;
    }

    public void setCssIndicator(int paramInt)
    {
        if (paramInt != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mCssIndicator = bool;
            return;
        }
    }

    public void setEmergencyOnly(boolean paramBoolean)
    {
        this.mIsEmergencyOnly = paramBoolean;
    }

    public void setIsManualSelection(boolean paramBoolean)
    {
        this.mIsManualNetworkSelection = paramBoolean;
    }

    public void setOperatorAlphaLong(String paramString)
    {
        this.mOperatorAlphaLong = paramString;
    }

    public void setOperatorName(String paramString1, String paramString2, String paramString3)
    {
        this.mOperatorAlphaLong = paramString1;
        this.mOperatorAlphaShort = paramString2;
        this.mOperatorNumeric = paramString3;
    }

    public void setRadioTechnology(int paramInt)
    {
        this.mRadioTechnology = paramInt;
    }

    public void setRoaming(boolean paramBoolean)
    {
        this.mRoaming = paramBoolean;
    }

    public void setState(int paramInt)
    {
        this.mState = paramInt;
    }

    public void setStateOff()
    {
        setNullState(3);
    }

    public void setStateOutOfService()
    {
        setNullState(1);
    }

    public void setSystemAndNetworkId(int paramInt1, int paramInt2)
    {
        this.mSystemId = paramInt1;
        this.mNetworkId = paramInt2;
    }

    public String toString()
    {
        String str1 = rilRadioTechnologyToString(this.mRadioTechnology);
        StringBuilder localStringBuilder1 = new StringBuilder().append(this.mState).append(" ");
        String str2;
        String str3;
        label103: StringBuilder localStringBuilder3;
        if (this.mRoaming)
        {
            str2 = "roaming";
            StringBuilder localStringBuilder2 = localStringBuilder1.append(str2).append(" ").append(this.mOperatorAlphaLong).append(" ").append(this.mOperatorAlphaShort).append(" ").append(this.mOperatorNumeric).append(" ");
            if (!this.mIsManualNetworkSelection)
                break label223;
            str3 = "(manual)";
            localStringBuilder3 = localStringBuilder2.append(str3).append(" ").append(str1).append(" ");
            if (!this.mCssIndicator)
                break label231;
        }
        label223: label231: for (String str4 = "CSS supported"; ; str4 = "CSS not supported")
        {
            return str4 + " " + this.mNetworkId + " " + this.mSystemId + " RoamInd=" + this.mCdmaRoamingIndicator + " DefRoamInd=" + this.mCdmaDefaultRoamingIndicator + " EmergOnly=" + this.mIsEmergencyOnly;
            str2 = "home";
            break;
            str3 = "";
            break label103;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(this.mState);
        int j;
        int k;
        label60: int m;
        if (this.mRoaming)
        {
            j = i;
            paramParcel.writeInt(j);
            paramParcel.writeString(this.mOperatorAlphaLong);
            paramParcel.writeString(this.mOperatorAlphaShort);
            paramParcel.writeString(this.mOperatorNumeric);
            if (!this.mIsManualNetworkSelection)
                break label157;
            k = i;
            paramParcel.writeInt(k);
            paramParcel.writeInt(this.mRadioTechnology);
            if (!this.mCssIndicator)
                break label163;
            m = i;
            label84: paramParcel.writeInt(m);
            paramParcel.writeInt(this.mNetworkId);
            paramParcel.writeInt(this.mSystemId);
            paramParcel.writeInt(this.mCdmaRoamingIndicator);
            paramParcel.writeInt(this.mCdmaDefaultRoamingIndicator);
            paramParcel.writeInt(this.mCdmaEriIconIndex);
            paramParcel.writeInt(this.mCdmaEriIconMode);
            if (!this.mIsEmergencyOnly)
                break label169;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 0;
            break;
            label157: k = 0;
            break label60;
            label163: m = 0;
            break label84;
            label169: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.ServiceState
 * JD-Core Version:        0.6.2
 */