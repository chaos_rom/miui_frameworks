package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class LteCellIdentity extends CellIdentity
    implements Parcelable
{
    public static final Parcelable.Creator<LteCellIdentity> CREATOR = new Parcelable.Creator()
    {
        public LteCellIdentity createFromParcel(Parcel paramAnonymousParcel)
        {
            return new LteCellIdentity(paramAnonymousParcel, null);
        }

        public LteCellIdentity[] newArray(int paramAnonymousInt)
        {
            return new LteCellIdentity[paramAnonymousInt];
        }
    };
    private final int mCi;
    private final int mMcc;
    private final int mMnc;
    private final int mPci;
    private final int mTac;

    public LteCellIdentity(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString)
    {
        super(2, paramString);
        this.mMcc = paramInt1;
        this.mMnc = paramInt2;
        this.mCi = paramInt3;
        this.mPci = paramInt4;
        this.mTac = paramInt5;
    }

    private LteCellIdentity(Parcel paramParcel)
    {
        super(paramParcel);
        this.mMcc = paramParcel.readInt();
        this.mMnc = paramParcel.readInt();
        this.mCi = paramParcel.readInt();
        this.mPci = paramParcel.readInt();
        this.mTac = paramParcel.readInt();
    }

    LteCellIdentity(LteCellIdentity paramLteCellIdentity)
    {
        super(paramLteCellIdentity);
        this.mMcc = paramLteCellIdentity.mMcc;
        this.mMnc = paramLteCellIdentity.mMnc;
        this.mCi = paramLteCellIdentity.mCi;
        this.mPci = paramLteCellIdentity.mPci;
        this.mTac = paramLteCellIdentity.mTac;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCi()
    {
        return this.mCi;
    }

    public int getMcc()
    {
        return this.mMcc;
    }

    public int getMnc()
    {
        return this.mMnc;
    }

    public int getPci()
    {
        return this.mPci;
    }

    public int getTac()
    {
        return this.mTac;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.mMcc);
        paramParcel.writeInt(this.mMnc);
        paramParcel.writeInt(this.mCi);
        paramParcel.writeInt(this.mPci);
        paramParcel.writeInt(this.mTac);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.LteCellIdentity
 * JD-Core Version:        0.6.2
 */