package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class CdmaCellIdentity extends CellIdentity
    implements Parcelable
{
    public static final Parcelable.Creator<CdmaCellIdentity> CREATOR = new Parcelable.Creator()
    {
        public CdmaCellIdentity createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CdmaCellIdentity(paramAnonymousParcel, null);
        }

        public CdmaCellIdentity[] newArray(int paramAnonymousInt)
        {
            return new CdmaCellIdentity[paramAnonymousInt];
        }
    };
    private final int mBasestationId;
    private final int mLatitude;
    private final int mLongitude;
    private final int mNetworkId;
    private final int mSystemId;

    public CdmaCellIdentity(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString)
    {
        super(2, paramString);
        this.mNetworkId = paramInt1;
        this.mSystemId = paramInt2;
        this.mBasestationId = paramInt3;
        this.mLongitude = paramInt4;
        this.mLatitude = paramInt5;
    }

    private CdmaCellIdentity(Parcel paramParcel)
    {
        super(paramParcel);
        this.mNetworkId = paramParcel.readInt();
        this.mSystemId = paramParcel.readInt();
        this.mBasestationId = paramParcel.readInt();
        this.mLongitude = paramParcel.readInt();
        this.mLatitude = paramParcel.readInt();
    }

    CdmaCellIdentity(CdmaCellIdentity paramCdmaCellIdentity)
    {
        super(paramCdmaCellIdentity);
        this.mNetworkId = paramCdmaCellIdentity.mNetworkId;
        this.mSystemId = paramCdmaCellIdentity.mSystemId;
        this.mBasestationId = paramCdmaCellIdentity.mBasestationId;
        this.mLongitude = paramCdmaCellIdentity.mLongitude;
        this.mLatitude = paramCdmaCellIdentity.mLatitude;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getBasestationId()
    {
        return this.mBasestationId;
    }

    public int getLatitude()
    {
        return this.mLatitude;
    }

    public int getLongitude()
    {
        return this.mLongitude;
    }

    public int getNetworkId()
    {
        return this.mNetworkId;
    }

    public int getSystemId()
    {
        return this.mSystemId;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.mNetworkId);
        paramParcel.writeInt(this.mSystemId);
        paramParcel.writeInt(this.mBasestationId);
        paramParcel.writeInt(this.mLongitude);
        paramParcel.writeInt(this.mLatitude);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.CdmaCellIdentity
 * JD-Core Version:        0.6.2
 */