package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class CellInfo
    implements Parcelable
{
    public static final int CELL_INFO_TIMESTAMP_TYPE_ANTENNA = 1;
    public static final int CELL_INFO_TIMESTAMP_TYPE_JAVA_RIL = 4;
    public static final int CELL_INFO_TIMESTAMP_TYPE_MODEM = 2;
    public static final int CELL_INFO_TIMESTAMP_TYPE_OEM_RIL = 3;
    public static final int CELL_INFO_TIMESTAMP_TYPE_UNKNOWN;
    public static final Parcelable.Creator<CellInfo> CREATOR = new Parcelable.Creator()
    {
        public CellInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CellInfo(paramAnonymousParcel, null);
        }

        public CellInfo[] newArray(int paramAnonymousInt)
        {
            return new CellInfo[paramAnonymousInt];
        }
    };
    private final CellIdentity mCellIdentity;
    private final int mCellIdentityType;
    private final boolean mRegistered;
    private final SignalStrength mStrength;
    private final long mTimeStamp;
    private final int mTimeStampType;
    private final long mTimingAdvance;

    public CellInfo(int paramInt, long paramLong1, long paramLong2, boolean paramBoolean, SignalStrength paramSignalStrength, CellIdentity paramCellIdentity)
    {
        if ((paramInt < 0) || (paramInt > 4));
        for (this.mTimeStampType = 0; ; this.mTimeStampType = paramInt)
        {
            this.mRegistered = paramBoolean;
            this.mTimeStamp = paramLong1;
            this.mTimingAdvance = paramLong2;
            this.mStrength = new SignalStrength(paramSignalStrength);
            this.mCellIdentityType = paramCellIdentity.getCellIdType();
            this.mCellIdentity = paramCellIdentity;
            return;
        }
    }

    private CellInfo(Parcel paramParcel)
    {
        this.mTimeStampType = paramParcel.readInt();
        if (paramParcel.readInt() == i)
        {
            this.mRegistered = i;
            this.mTimeStamp = paramParcel.readLong();
            this.mTimingAdvance = paramParcel.readLong();
            this.mCellIdentityType = paramParcel.readInt();
            this.mStrength = ((SignalStrength)SignalStrength.CREATOR.createFromParcel(paramParcel));
            switch (this.mCellIdentityType)
            {
            default:
            case 1:
            }
        }
        for (this.mCellIdentity = null; ; this.mCellIdentity = ((CellIdentity)GsmCellIdentity.CREATOR.createFromParcel(paramParcel)))
        {
            return;
            i = 0;
            break;
        }
    }

    public CellInfo(CellInfo paramCellInfo)
    {
        this.mTimeStampType = paramCellInfo.mTimeStampType;
        this.mRegistered = paramCellInfo.mRegistered;
        this.mTimeStamp = paramCellInfo.mTimeStamp;
        this.mTimingAdvance = paramCellInfo.mTimingAdvance;
        this.mCellIdentityType = paramCellInfo.mCellIdentityType;
        this.mStrength = new SignalStrength(paramCellInfo.mStrength);
        switch (this.mCellIdentityType)
        {
        default:
        case 1:
        }
        for (this.mCellIdentity = null; ; this.mCellIdentity = new GsmCellIdentity((GsmCellIdentity)paramCellInfo.mCellIdentity))
            return;
    }

    public int describeContents()
    {
        return 0;
    }

    public CellIdentity getCellIdentity()
    {
        return this.mCellIdentity;
    }

    public SignalStrength getSignalStrength()
    {
        return new SignalStrength(this.mStrength);
    }

    public long getTimeStamp()
    {
        return this.mTimeStamp;
    }

    public int getTimeStampType()
    {
        return this.mTimeStampType;
    }

    public long getTimingAdvance()
    {
        return this.mTimingAdvance;
    }

    public boolean isRegistered()
    {
        return this.mRegistered;
    }

    public String toString()
    {
        StringBuffer localStringBuffer1 = new StringBuffer();
        localStringBuffer1.append("TimeStampType: ");
        StringBuffer localStringBuffer2;
        switch (this.mTimeStampType)
        {
        default:
            localStringBuffer1.append("unknown");
            localStringBuffer1.append(", TimeStamp: ").append(this.mTimeStamp).append(" ns");
            localStringBuffer2 = localStringBuffer1.append(", Registered: ");
            if (!this.mRegistered)
                break;
        case 1:
        case 2:
        case 3:
        case 4:
        }
        for (String str = "YES"; ; str = "NO")
        {
            localStringBuffer2.append(str);
            localStringBuffer1.append(", TimingAdvance: ").append(this.mTimingAdvance);
            localStringBuffer1.append(", Strength : " + this.mStrength);
            localStringBuffer1.append(", Cell Iden: " + this.mCellIdentity);
            return localStringBuffer1.toString();
            localStringBuffer1.append("antenna");
            break;
            localStringBuffer1.append("modem");
            break;
            localStringBuffer1.append("oem_ril");
            break;
            localStringBuffer1.append("java_ril");
            break;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mTimeStampType);
        if (this.mRegistered);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            paramParcel.writeLong(this.mTimeStamp);
            paramParcel.writeLong(this.mTimingAdvance);
            paramParcel.writeInt(this.mCellIdentityType);
            this.mStrength.writeToParcel(paramParcel, paramInt);
            this.mCellIdentity.writeToParcel(paramParcel, paramInt);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.CellInfo
 * JD-Core Version:        0.6.2
 */