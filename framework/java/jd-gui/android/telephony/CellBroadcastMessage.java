package android.telephony;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.format.DateUtils;

public class CellBroadcastMessage
    implements Parcelable
{
    public static final Parcelable.Creator<CellBroadcastMessage> CREATOR = new Parcelable.Creator()
    {
        public CellBroadcastMessage createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CellBroadcastMessage(paramAnonymousParcel, null);
        }

        public CellBroadcastMessage[] newArray(int paramAnonymousInt)
        {
            return new CellBroadcastMessage[paramAnonymousInt];
        }
    };
    public static final String SMS_CB_MESSAGE_EXTRA = "com.android.cellbroadcastreceiver.SMS_CB_MESSAGE";
    private final long mDeliveryTime;
    private boolean mIsRead;
    private final SmsCbMessage mSmsCbMessage;

    private CellBroadcastMessage(Parcel paramParcel)
    {
        this.mSmsCbMessage = new SmsCbMessage(paramParcel);
        this.mDeliveryTime = paramParcel.readLong();
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mIsRead = bool;
            return;
        }
    }

    public CellBroadcastMessage(SmsCbMessage paramSmsCbMessage)
    {
        this.mSmsCbMessage = paramSmsCbMessage;
        this.mDeliveryTime = System.currentTimeMillis();
        this.mIsRead = false;
    }

    private CellBroadcastMessage(SmsCbMessage paramSmsCbMessage, long paramLong, boolean paramBoolean)
    {
        this.mSmsCbMessage = paramSmsCbMessage;
        this.mDeliveryTime = paramLong;
        this.mIsRead = paramBoolean;
    }

    public static CellBroadcastMessage createFromCursor(Cursor paramCursor)
    {
        int i = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("geo_scope"));
        int j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("serial_number"));
        int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("service_category"));
        String str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("language"));
        String str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("body"));
        int m = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("format"));
        int n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("priority"));
        int i1 = paramCursor.getColumnIndex("plmn");
        String str3;
        int i3;
        label185: int i5;
        label223: SmsCbEtwsInfo localSmsCbEtwsInfo;
        label290: int i10;
        label366: int i12;
        label404: int i14;
        label442: int i16;
        label480: int i18;
        label518: SmsCbCmasInfo localSmsCbCmasInfo;
        label539: SmsCbMessage localSmsCbMessage;
        long l;
        if ((i1 != -1) && (!paramCursor.isNull(i1)))
        {
            str3 = paramCursor.getString(i1);
            int i2 = paramCursor.getColumnIndex("lac");
            if ((i2 == -1) || (paramCursor.isNull(i2)))
                break label625;
            i3 = paramCursor.getInt(i2);
            int i4 = paramCursor.getColumnIndex("cid");
            if ((i4 == -1) || (paramCursor.isNull(i4)))
                break label632;
            i5 = paramCursor.getInt(i4);
            SmsCbLocation localSmsCbLocation = new SmsCbLocation(str3, i3, i5);
            int i6 = paramCursor.getColumnIndex("etws_warning_type");
            if ((i6 == -1) || (paramCursor.isNull(i6)))
                break label639;
            int i19 = paramCursor.getInt(i6);
            localSmsCbEtwsInfo = new SmsCbEtwsInfo(i19, false, false, null);
            int i7 = paramCursor.getColumnIndex("cmas_message_class");
            if ((i7 == -1) || (paramCursor.isNull(i7)))
                break label680;
            int i8 = paramCursor.getInt(i7);
            int i9 = paramCursor.getColumnIndex("cmas_category");
            if ((i9 == -1) || (paramCursor.isNull(i9)))
                break label645;
            i10 = paramCursor.getInt(i9);
            int i11 = paramCursor.getColumnIndex("cmas_response_type");
            if ((i11 == -1) || (paramCursor.isNull(i11)))
                break label652;
            i12 = paramCursor.getInt(i11);
            int i13 = paramCursor.getColumnIndex("cmas_severity");
            if ((i13 == -1) || (paramCursor.isNull(i13)))
                break label659;
            i14 = paramCursor.getInt(i13);
            int i15 = paramCursor.getColumnIndex("cmas_urgency");
            if ((i15 == -1) || (paramCursor.isNull(i15)))
                break label666;
            i16 = paramCursor.getInt(i15);
            int i17 = paramCursor.getColumnIndex("cmas_certainty");
            if ((i17 == -1) || (paramCursor.isNull(i17)))
                break label673;
            i18 = paramCursor.getInt(i17);
            localSmsCbCmasInfo = new SmsCbCmasInfo(i8, i10, i12, i14, i16, i18);
            localSmsCbMessage = new SmsCbMessage(m, i, j, localSmsCbLocation, k, str1, str2, n, localSmsCbEtwsInfo, localSmsCbCmasInfo);
            l = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("date"));
            if (paramCursor.getInt(paramCursor.getColumnIndexOrThrow("read")) == 0)
                break label686;
        }
        label645: label652: label659: label666: label673: label680: label686: for (boolean bool = true; ; bool = false)
        {
            CellBroadcastMessage localCellBroadcastMessage = new CellBroadcastMessage(localSmsCbMessage, l, bool);
            return localCellBroadcastMessage;
            str3 = null;
            break;
            label625: i3 = -1;
            break label185;
            label632: i5 = -1;
            break label223;
            label639: localSmsCbEtwsInfo = null;
            break label290;
            i10 = -1;
            break label366;
            i12 = -1;
            break label404;
            i14 = -1;
            break label442;
            i16 = -1;
            break label480;
            i18 = -1;
            break label518;
            localSmsCbCmasInfo = null;
            break label539;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCmasMessageClass()
    {
        if (this.mSmsCbMessage.isCmasMessage());
        for (int i = this.mSmsCbMessage.getCmasWarningInfo().getMessageClass(); ; i = -1)
            return i;
    }

    public SmsCbCmasInfo getCmasWarningInfo()
    {
        return this.mSmsCbMessage.getCmasWarningInfo();
    }

    public ContentValues getContentValues()
    {
        ContentValues localContentValues = new ContentValues(16);
        SmsCbMessage localSmsCbMessage = this.mSmsCbMessage;
        localContentValues.put("geo_scope", Integer.valueOf(localSmsCbMessage.getGeographicalScope()));
        SmsCbLocation localSmsCbLocation = localSmsCbMessage.getLocation();
        if (localSmsCbLocation.getPlmn() != null)
            localContentValues.put("plmn", localSmsCbLocation.getPlmn());
        if (localSmsCbLocation.getLac() != -1)
            localContentValues.put("lac", Integer.valueOf(localSmsCbLocation.getLac()));
        if (localSmsCbLocation.getCid() != -1)
            localContentValues.put("cid", Integer.valueOf(localSmsCbLocation.getCid()));
        localContentValues.put("serial_number", Integer.valueOf(localSmsCbMessage.getSerialNumber()));
        localContentValues.put("service_category", Integer.valueOf(localSmsCbMessage.getServiceCategory()));
        localContentValues.put("language", localSmsCbMessage.getLanguageCode());
        localContentValues.put("body", localSmsCbMessage.getMessageBody());
        localContentValues.put("date", Long.valueOf(this.mDeliveryTime));
        localContentValues.put("read", Boolean.valueOf(this.mIsRead));
        localContentValues.put("format", Integer.valueOf(localSmsCbMessage.getMessageFormat()));
        localContentValues.put("priority", Integer.valueOf(localSmsCbMessage.getMessagePriority()));
        SmsCbEtwsInfo localSmsCbEtwsInfo = this.mSmsCbMessage.getEtwsWarningInfo();
        if (localSmsCbEtwsInfo != null)
            localContentValues.put("etws_warning_type", Integer.valueOf(localSmsCbEtwsInfo.getWarningType()));
        SmsCbCmasInfo localSmsCbCmasInfo = this.mSmsCbMessage.getCmasWarningInfo();
        if (localSmsCbCmasInfo != null)
        {
            localContentValues.put("cmas_message_class", Integer.valueOf(localSmsCbCmasInfo.getMessageClass()));
            localContentValues.put("cmas_category", Integer.valueOf(localSmsCbCmasInfo.getCategory()));
            localContentValues.put("cmas_response_type", Integer.valueOf(localSmsCbCmasInfo.getResponseType()));
            localContentValues.put("cmas_severity", Integer.valueOf(localSmsCbCmasInfo.getSeverity()));
            localContentValues.put("cmas_urgency", Integer.valueOf(localSmsCbCmasInfo.getUrgency()));
            localContentValues.put("cmas_certainty", Integer.valueOf(localSmsCbCmasInfo.getCertainty()));
        }
        return localContentValues;
    }

    public String getDateString(Context paramContext)
    {
        return DateUtils.formatDateTime(paramContext, this.mDeliveryTime, 527121);
    }

    public long getDeliveryTime()
    {
        return this.mDeliveryTime;
    }

    public SmsCbEtwsInfo getEtwsWarningInfo()
    {
        return this.mSmsCbMessage.getEtwsWarningInfo();
    }

    public String getLanguageCode()
    {
        return this.mSmsCbMessage.getLanguageCode();
    }

    public String getMessageBody()
    {
        return this.mSmsCbMessage.getMessageBody();
    }

    public int getSerialNumber()
    {
        return this.mSmsCbMessage.getSerialNumber();
    }

    public int getServiceCategory()
    {
        return this.mSmsCbMessage.getServiceCategory();
    }

    public String getSpokenDateString(Context paramContext)
    {
        return DateUtils.formatDateTime(paramContext, this.mDeliveryTime, 17);
    }

    public boolean isCmasMessage()
    {
        return this.mSmsCbMessage.isCmasMessage();
    }

    public boolean isEmergencyAlertMessage()
    {
        boolean bool = false;
        if (!this.mSmsCbMessage.isEmergencyMessage());
        while (true)
        {
            return bool;
            SmsCbCmasInfo localSmsCbCmasInfo = this.mSmsCbMessage.getCmasWarningInfo();
            if ((localSmsCbCmasInfo == null) || (localSmsCbCmasInfo.getMessageClass() != 3))
                bool = true;
        }
    }

    public boolean isEtwsEmergencyUserAlert()
    {
        SmsCbEtwsInfo localSmsCbEtwsInfo = this.mSmsCbMessage.getEtwsWarningInfo();
        if ((localSmsCbEtwsInfo != null) && (localSmsCbEtwsInfo.isEmergencyUserAlert()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEtwsMessage()
    {
        return this.mSmsCbMessage.isEtwsMessage();
    }

    public boolean isEtwsPopupAlert()
    {
        SmsCbEtwsInfo localSmsCbEtwsInfo = this.mSmsCbMessage.getEtwsWarningInfo();
        if ((localSmsCbEtwsInfo != null) && (localSmsCbEtwsInfo.isPopupAlert()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEtwsTestMessage()
    {
        SmsCbEtwsInfo localSmsCbEtwsInfo = this.mSmsCbMessage.getEtwsWarningInfo();
        if ((localSmsCbEtwsInfo != null) && (localSmsCbEtwsInfo.getWarningType() == 3));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPublicAlertMessage()
    {
        return this.mSmsCbMessage.isEmergencyMessage();
    }

    public boolean isRead()
    {
        return this.mIsRead;
    }

    public void setIsRead(boolean paramBoolean)
    {
        this.mIsRead = paramBoolean;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        this.mSmsCbMessage.writeToParcel(paramParcel, paramInt);
        paramParcel.writeLong(this.mDeliveryTime);
        if (this.mIsRead);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.CellBroadcastMessage
 * JD-Core Version:        0.6.2
 */