package android.telephony.gsm;

import android.os.Bundle;
import android.telephony.CellLocation;

public class GsmCellLocation extends CellLocation
{
    private int mCid;
    private int mLac;
    private int mPsc;

    public GsmCellLocation()
    {
        this.mLac = -1;
        this.mCid = -1;
        this.mPsc = -1;
    }

    public GsmCellLocation(Bundle paramBundle)
    {
        this.mLac = paramBundle.getInt("lac", this.mLac);
        this.mCid = paramBundle.getInt("cid", this.mCid);
        this.mPsc = paramBundle.getInt("psc", this.mPsc);
    }

    private static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2)
    {
        boolean bool;
        if (paramObject1 == null)
            if (paramObject2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramObject1.equals(paramObject2);
        }
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        try
        {
            localGsmCellLocation = (GsmCellLocation)paramObject;
            if (paramObject == null)
                return bool;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                GsmCellLocation localGsmCellLocation;
                continue;
                if ((equalsHandlesNulls(Integer.valueOf(this.mLac), Integer.valueOf(localGsmCellLocation.mLac))) && (equalsHandlesNulls(Integer.valueOf(this.mCid), Integer.valueOf(localGsmCellLocation.mCid))) && (equalsHandlesNulls(Integer.valueOf(this.mPsc), Integer.valueOf(localGsmCellLocation.mPsc))))
                    bool = true;
            }
        }
    }

    public void fillInNotifierBundle(Bundle paramBundle)
    {
        paramBundle.putInt("lac", this.mLac);
        paramBundle.putInt("cid", this.mCid);
        paramBundle.putInt("psc", this.mPsc);
    }

    public int getCid()
    {
        return this.mCid;
    }

    public int getLac()
    {
        return this.mLac;
    }

    public int getPsc()
    {
        return this.mPsc;
    }

    public int hashCode()
    {
        return this.mLac ^ this.mCid;
    }

    public boolean isEmpty()
    {
        if ((this.mLac == -1) && (this.mCid == -1) && (this.mPsc == -1));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setLacAndCid(int paramInt1, int paramInt2)
    {
        this.mLac = paramInt1;
        this.mCid = paramInt2;
    }

    public void setPsc(int paramInt)
    {
        this.mPsc = paramInt;
    }

    public void setStateInvalid()
    {
        this.mLac = -1;
        this.mCid = -1;
        this.mPsc = -1;
    }

    public String toString()
    {
        return "[" + this.mLac + "," + this.mCid + "," + this.mPsc + "]";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.gsm.GsmCellLocation
 * JD-Core Version:        0.6.2
 */