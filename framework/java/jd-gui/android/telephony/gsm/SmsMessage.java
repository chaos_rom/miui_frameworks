package android.telephony.gsm;

import android.telephony.TelephonyManager;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.SmsMessageBase.SubmitPduBase;
import com.android.internal.telephony.SmsMessageBase.TextEncodingDetails;
import java.util.Arrays;

@Deprecated
public class SmsMessage
{

    @Deprecated
    public static final int ENCODING_16BIT = 3;

    @Deprecated
    public static final int ENCODING_7BIT = 1;

    @Deprecated
    public static final int ENCODING_8BIT = 2;

    @Deprecated
    public static final int ENCODING_UNKNOWN = 0;
    private static final boolean LOCAL_DEBUG = true;
    private static final String LOG_TAG = "SMS";

    @Deprecated
    public static final int MAX_USER_DATA_BYTES = 140;

    @Deprecated
    public static final int MAX_USER_DATA_BYTES_WITH_HEADER = 134;

    @Deprecated
    public static final int MAX_USER_DATA_SEPTETS = 160;

    @Deprecated
    public static final int MAX_USER_DATA_SEPTETS_WITH_HEADER = 153;

    @Deprecated
    public SmsMessageBase mWrappedSmsMessage;

    @Deprecated
    public SmsMessage()
    {
        this(getSmsFacility());
    }

    private SmsMessage(SmsMessageBase paramSmsMessageBase)
    {
        this.mWrappedSmsMessage = paramSmsMessageBase;
    }

    @Deprecated
    public static int[] calculateLength(CharSequence paramCharSequence, boolean paramBoolean)
    {
        SmsMessageBase.TextEncodingDetails localTextEncodingDetails = com.android.internal.telephony.gsm.SmsMessage.calculateLength(paramCharSequence, paramBoolean);
        int[] arrayOfInt = new int[4];
        arrayOfInt[0] = localTextEncodingDetails.msgCount;
        arrayOfInt[1] = localTextEncodingDetails.codeUnitCount;
        arrayOfInt[2] = localTextEncodingDetails.codeUnitsRemaining;
        arrayOfInt[3] = localTextEncodingDetails.codeUnitSize;
        return arrayOfInt;
    }

    @Deprecated
    public static int[] calculateLength(String paramString, boolean paramBoolean)
    {
        return calculateLength(paramString, paramBoolean);
    }

    @Deprecated
    public static SmsMessage createFromPdu(byte[] paramArrayOfByte)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = com.android.internal.telephony.cdma.SmsMessage.createFromPdu(paramArrayOfByte); ; localObject = com.android.internal.telephony.gsm.SmsMessage.createFromPdu(paramArrayOfByte))
            return new SmsMessage((SmsMessageBase)localObject);
    }

    private static final SmsMessageBase getSmsFacility()
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = new com.android.internal.telephony.cdma.SmsMessage(); ; localObject = new com.android.internal.telephony.gsm.SmsMessage())
            return localObject;
    }

    @Deprecated
    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, null); ; localObject = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean))
            return new SubmitPdu((SmsMessageBase.SubmitPduBase)localObject);
    }

    @Deprecated
    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, byte[] paramArrayOfByte)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, SmsHeader.fromByteArray(paramArrayOfByte)); ; localObject = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, paramArrayOfByte))
            return new SubmitPdu((SmsMessageBase.SubmitPduBase)localObject);
    }

    @Deprecated
    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, short paramShort, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (Object localObject = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramShort, paramArrayOfByte, paramBoolean); ; localObject = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu(paramString1, paramString2, paramShort, paramArrayOfByte, paramBoolean))
            return new SubmitPdu((SmsMessageBase.SubmitPduBase)localObject);
    }

    @Deprecated
    public static int getTPLayerLengthForPDU(String paramString)
    {
        if (2 == TelephonyManager.getDefault().getCurrentPhoneType());
        for (int i = com.android.internal.telephony.cdma.SmsMessage.getTPLayerLengthForPDU(paramString); ; i = com.android.internal.telephony.gsm.SmsMessage.getTPLayerLengthForPDU(paramString))
            return i;
    }

    @Deprecated
    public String getDisplayMessageBody()
    {
        return this.mWrappedSmsMessage.getDisplayMessageBody();
    }

    @Deprecated
    public String getDisplayOriginatingAddress()
    {
        return this.mWrappedSmsMessage.getDisplayOriginatingAddress();
    }

    @Deprecated
    public String getEmailBody()
    {
        return this.mWrappedSmsMessage.getEmailBody();
    }

    @Deprecated
    public String getEmailFrom()
    {
        return this.mWrappedSmsMessage.getEmailFrom();
    }

    @Deprecated
    public int getIndexOnIcc()
    {
        return this.mWrappedSmsMessage.getIndexOnIcc();
    }

    @Deprecated
    public int getIndexOnSim()
    {
        return this.mWrappedSmsMessage.getIndexOnIcc();
    }

    @Deprecated
    public String getMessageBody()
    {
        return this.mWrappedSmsMessage.getMessageBody();
    }

    @Deprecated
    public MessageClass getMessageClass()
    {
        int i = this.mWrappedSmsMessage.getMessageClass().ordinal();
        return MessageClass.values()[i];
    }

    @Deprecated
    public String getOriginatingAddress()
    {
        return this.mWrappedSmsMessage.getOriginatingAddress();
    }

    @Deprecated
    public byte[] getPdu()
    {
        return this.mWrappedSmsMessage.getPdu();
    }

    @Deprecated
    public int getProtocolIdentifier()
    {
        return this.mWrappedSmsMessage.getProtocolIdentifier();
    }

    @Deprecated
    public String getPseudoSubject()
    {
        return this.mWrappedSmsMessage.getPseudoSubject();
    }

    @Deprecated
    public String getServiceCenterAddress()
    {
        return this.mWrappedSmsMessage.getServiceCenterAddress();
    }

    @Deprecated
    public int getStatus()
    {
        return this.mWrappedSmsMessage.getStatus();
    }

    @Deprecated
    public int getStatusOnIcc()
    {
        return this.mWrappedSmsMessage.getStatusOnIcc();
    }

    @Deprecated
    public int getStatusOnSim()
    {
        return this.mWrappedSmsMessage.getStatusOnIcc();
    }

    @Deprecated
    public long getTimestampMillis()
    {
        return this.mWrappedSmsMessage.getTimestampMillis();
    }

    @Deprecated
    public byte[] getUserData()
    {
        return this.mWrappedSmsMessage.getUserData();
    }

    @Deprecated
    public boolean isCphsMwiMessage()
    {
        return this.mWrappedSmsMessage.isCphsMwiMessage();
    }

    @Deprecated
    public boolean isEmail()
    {
        return this.mWrappedSmsMessage.isEmail();
    }

    @Deprecated
    public boolean isMWIClearMessage()
    {
        return this.mWrappedSmsMessage.isMWIClearMessage();
    }

    @Deprecated
    public boolean isMWISetMessage()
    {
        return this.mWrappedSmsMessage.isMWISetMessage();
    }

    @Deprecated
    public boolean isMwiDontStore()
    {
        return this.mWrappedSmsMessage.isMwiDontStore();
    }

    @Deprecated
    public boolean isReplace()
    {
        return this.mWrappedSmsMessage.isReplace();
    }

    @Deprecated
    public boolean isReplyPathPresent()
    {
        return this.mWrappedSmsMessage.isReplyPathPresent();
    }

    @Deprecated
    public boolean isStatusReportMessage()
    {
        return this.mWrappedSmsMessage.isStatusReportMessage();
    }

    @Deprecated
    public static class SubmitPdu
    {

        @Deprecated
        public byte[] encodedMessage;

        @Deprecated
        public byte[] encodedScAddress;

        @Deprecated
        public SubmitPdu()
        {
        }

        @Deprecated
        protected SubmitPdu(SmsMessageBase.SubmitPduBase paramSubmitPduBase)
        {
            this.encodedMessage = paramSubmitPduBase.encodedMessage;
            this.encodedScAddress = paramSubmitPduBase.encodedScAddress;
        }

        @Deprecated
        public String toString()
        {
            return "SubmitPdu: encodedScAddress = " + Arrays.toString(this.encodedScAddress) + ", encodedMessage = " + Arrays.toString(this.encodedMessage);
        }
    }

    @Deprecated
    public static enum MessageClass
    {
        static
        {
            CLASS_0 = new MessageClass("CLASS_0", 1);
            CLASS_1 = new MessageClass("CLASS_1", 2);
            CLASS_2 = new MessageClass("CLASS_2", 3);
            CLASS_3 = new MessageClass("CLASS_3", 4);
            MessageClass[] arrayOfMessageClass = new MessageClass[5];
            arrayOfMessageClass[0] = UNKNOWN;
            arrayOfMessageClass[1] = CLASS_0;
            arrayOfMessageClass[2] = CLASS_1;
            arrayOfMessageClass[3] = CLASS_2;
            arrayOfMessageClass[4] = CLASS_3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.gsm.SmsMessage
 * JD-Core Version:        0.6.2
 */