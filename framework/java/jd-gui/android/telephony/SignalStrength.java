package android.telephony;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

public class SignalStrength
    implements Parcelable
{
    public static final Parcelable.Creator<SignalStrength> CREATOR = new Parcelable.Creator()
    {
        public SignalStrength createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SignalStrength(paramAnonymousParcel);
        }

        public SignalStrength[] newArray(int paramAnonymousInt)
        {
            return new SignalStrength[paramAnonymousInt];
        }
    };
    private static final boolean DBG = false;
    public static final int INVALID_SNR = 2147483647;
    private static final String LOG_TAG = "SignalStrength";
    public static final int NUM_SIGNAL_STRENGTH_BINS = 5;
    public static final int SIGNAL_STRENGTH_GOOD = 3;
    public static final int SIGNAL_STRENGTH_GREAT = 4;
    public static final int SIGNAL_STRENGTH_MODERATE = 2;
    public static final String[] SIGNAL_STRENGTH_NAMES;
    public static final int SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;
    public static final int SIGNAL_STRENGTH_POOR = 1;
    private boolean isGsm;
    private int mCdmaDbm;
    private int mCdmaEcio;
    private int mEvdoDbm;
    private int mEvdoEcio;
    private int mEvdoSnr;
    private int mGsmBitErrorRate;
    private int mGsmSignalStrength;
    private int mLteCqi;
    private int mLteRsrp;
    private int mLteRsrq;
    private int mLteRssnr;
    private int mLteSignalStrength;

    static
    {
        String[] arrayOfString = new String[5];
        arrayOfString[0] = "none";
        arrayOfString[1] = "poor";
        arrayOfString[2] = "moderate";
        arrayOfString[3] = "good";
        arrayOfString[4] = "great";
        SIGNAL_STRENGTH_NAMES = arrayOfString;
    }

    public SignalStrength()
    {
        this.mGsmSignalStrength = 99;
        this.mGsmBitErrorRate = -1;
        this.mCdmaDbm = -1;
        this.mCdmaEcio = -1;
        this.mEvdoDbm = -1;
        this.mEvdoEcio = -1;
        this.mEvdoSnr = -1;
        this.mLteSignalStrength = -1;
        this.mLteRsrp = -1;
        this.mLteRsrq = -1;
        this.mLteRssnr = 2147483647;
        this.mLteCqi = -1;
        this.isGsm = true;
    }

    public SignalStrength(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, boolean paramBoolean)
    {
        this.mGsmSignalStrength = paramInt1;
        this.mGsmBitErrorRate = paramInt2;
        this.mCdmaDbm = paramInt3;
        this.mCdmaEcio = paramInt4;
        this.mEvdoDbm = paramInt5;
        this.mEvdoEcio = paramInt6;
        this.mEvdoSnr = paramInt7;
        this.mLteSignalStrength = paramInt8;
        this.mLteRsrp = paramInt9;
        this.mLteRsrq = paramInt10;
        this.mLteRssnr = paramInt11;
        this.mLteCqi = paramInt12;
        this.isGsm = paramBoolean;
    }

    public SignalStrength(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean)
    {
        this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, -1, -1, -1, 2147483647, -1, paramBoolean);
    }

    public SignalStrength(Parcel paramParcel)
    {
        this.mGsmSignalStrength = paramParcel.readInt();
        this.mGsmBitErrorRate = paramParcel.readInt();
        this.mCdmaDbm = paramParcel.readInt();
        this.mCdmaEcio = paramParcel.readInt();
        this.mEvdoDbm = paramParcel.readInt();
        this.mEvdoEcio = paramParcel.readInt();
        this.mEvdoSnr = paramParcel.readInt();
        this.mLteSignalStrength = paramParcel.readInt();
        this.mLteRsrp = paramParcel.readInt();
        this.mLteRsrq = paramParcel.readInt();
        this.mLteRssnr = paramParcel.readInt();
        this.mLteCqi = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.isGsm = bool;
            return;
        }
    }

    public SignalStrength(SignalStrength paramSignalStrength)
    {
        copyFrom(paramSignalStrength);
    }

    private static void log(String paramString)
    {
        Log.w("SignalStrength", paramString);
    }

    public static SignalStrength newFromBundle(Bundle paramBundle)
    {
        SignalStrength localSignalStrength = new SignalStrength();
        localSignalStrength.setFromNotifierBundle(paramBundle);
        return localSignalStrength;
    }

    private void setFromNotifierBundle(Bundle paramBundle)
    {
        this.mGsmSignalStrength = paramBundle.getInt("GsmSignalStrength");
        this.mGsmBitErrorRate = paramBundle.getInt("GsmBitErrorRate");
        this.mCdmaDbm = paramBundle.getInt("CdmaDbm");
        this.mCdmaEcio = paramBundle.getInt("CdmaEcio");
        this.mEvdoDbm = paramBundle.getInt("EvdoDbm");
        this.mEvdoEcio = paramBundle.getInt("EvdoEcio");
        this.mEvdoSnr = paramBundle.getInt("EvdoSnr");
        this.mLteSignalStrength = paramBundle.getInt("LteSignalStrength");
        this.mLteRsrp = paramBundle.getInt("LteRsrp");
        this.mLteRsrq = paramBundle.getInt("LteRsrq");
        this.mLteRssnr = paramBundle.getInt("LteRssnr");
        this.mLteCqi = paramBundle.getInt("LteCqi");
        this.isGsm = paramBundle.getBoolean("isGsm");
    }

    protected void copyFrom(SignalStrength paramSignalStrength)
    {
        this.mGsmSignalStrength = paramSignalStrength.mGsmSignalStrength;
        this.mGsmBitErrorRate = paramSignalStrength.mGsmBitErrorRate;
        this.mCdmaDbm = paramSignalStrength.mCdmaDbm;
        this.mCdmaEcio = paramSignalStrength.mCdmaEcio;
        this.mEvdoDbm = paramSignalStrength.mEvdoDbm;
        this.mEvdoEcio = paramSignalStrength.mEvdoEcio;
        this.mEvdoSnr = paramSignalStrength.mEvdoSnr;
        this.mLteSignalStrength = paramSignalStrength.mLteSignalStrength;
        this.mLteRsrp = paramSignalStrength.mLteRsrp;
        this.mLteRsrq = paramSignalStrength.mLteRsrq;
        this.mLteRssnr = paramSignalStrength.mLteRssnr;
        this.mLteCqi = paramSignalStrength.mLteCqi;
        this.isGsm = paramSignalStrength.isGsm;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        try
        {
            localSignalStrength = (SignalStrength)paramObject;
            if (paramObject == null)
                return bool;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                SignalStrength localSignalStrength;
                continue;
                if ((this.mGsmSignalStrength == localSignalStrength.mGsmSignalStrength) && (this.mGsmBitErrorRate == localSignalStrength.mGsmBitErrorRate) && (this.mCdmaDbm == localSignalStrength.mCdmaDbm) && (this.mCdmaEcio == localSignalStrength.mCdmaEcio) && (this.mEvdoDbm == localSignalStrength.mEvdoDbm) && (this.mEvdoEcio == localSignalStrength.mEvdoEcio) && (this.mEvdoSnr == localSignalStrength.mEvdoSnr) && (this.mLteSignalStrength == localSignalStrength.mLteSignalStrength) && (this.mLteRsrp == localSignalStrength.mLteRsrp) && (this.mLteRsrq == localSignalStrength.mLteRsrq) && (this.mLteRssnr == localSignalStrength.mLteRssnr) && (this.mLteCqi == localSignalStrength.mLteCqi) && (this.isGsm == localSignalStrength.isGsm))
                    bool = true;
            }
        }
    }

    public void fillInNotifierBundle(Bundle paramBundle)
    {
        paramBundle.putInt("GsmSignalStrength", this.mGsmSignalStrength);
        paramBundle.putInt("GsmBitErrorRate", this.mGsmBitErrorRate);
        paramBundle.putInt("CdmaDbm", this.mCdmaDbm);
        paramBundle.putInt("CdmaEcio", this.mCdmaEcio);
        paramBundle.putInt("EvdoDbm", this.mEvdoDbm);
        paramBundle.putInt("EvdoEcio", this.mEvdoEcio);
        paramBundle.putInt("EvdoSnr", this.mEvdoSnr);
        paramBundle.putInt("LteSignalStrength", this.mLteSignalStrength);
        paramBundle.putInt("LteRsrp", this.mLteRsrp);
        paramBundle.putInt("LteRsrq", this.mLteRsrq);
        paramBundle.putInt("LteRssnr", this.mLteRssnr);
        paramBundle.putInt("LteCqi", this.mLteCqi);
        paramBundle.putBoolean("isGsm", Boolean.valueOf(this.isGsm).booleanValue());
    }

    public int getAsuLevel()
    {
        if (this.isGsm)
            if ((this.mLteSignalStrength == -1) && (this.mLteRsrp == -1) && (this.mLteRsrq == -1) && (this.mLteCqi == -1))
                k = getGsmAsuLevel();
        int i;
        int j;
        while (true)
        {
            return k;
            k = getLteAsuLevel();
            continue;
            i = getCdmaAsuLevel();
            j = getEvdoAsuLevel();
            if (j == 0)
            {
                k = i;
            }
            else
            {
                if (i != 0)
                    break;
                k = j;
            }
        }
        if (i < j);
        for (int k = i; ; k = j)
            break;
    }

    public int getCdmaAsuLevel()
    {
        int i = getCdmaDbm();
        int j = getCdmaEcio();
        int k;
        int m;
        if (i >= -75)
        {
            k = 16;
            if (j < -90)
                break label92;
            m = 16;
            label29: if (k >= m)
                break label150;
        }
        label150: for (int n = k; ; n = m)
        {
            return n;
            if (i >= -82)
            {
                k = 8;
                break;
            }
            if (i >= -90)
            {
                k = 4;
                break;
            }
            if (i >= -95)
            {
                k = 2;
                break;
            }
            if (i >= -100)
            {
                k = 1;
                break;
            }
            k = 99;
            break;
            label92: if (j >= -100)
            {
                m = 8;
                break label29;
            }
            if (j >= -115)
            {
                m = 4;
                break label29;
            }
            if (j >= -130)
            {
                m = 2;
                break label29;
            }
            if (j >= -150)
            {
                m = 1;
                break label29;
            }
            m = 99;
            break label29;
        }
    }

    public int getCdmaDbm()
    {
        return this.mCdmaDbm;
    }

    public int getCdmaEcio()
    {
        return this.mCdmaEcio;
    }

    public int getCdmaLevel()
    {
        int i = getCdmaDbm();
        int j = getCdmaEcio();
        int k;
        int m;
        if (i >= -75)
        {
            k = 4;
            if (j < -90)
                break label77;
            m = 4;
            label27: if (k >= m)
                break label121;
        }
        label77: label121: for (int n = k; ; n = m)
        {
            return n;
            if (i >= -85)
            {
                k = 3;
                break;
            }
            if (i >= -95)
            {
                k = 2;
                break;
            }
            if (i >= -100)
            {
                k = 1;
                break;
            }
            k = 0;
            break;
            if (j >= -110)
            {
                m = 3;
                break label27;
            }
            if (j >= -130)
            {
                m = 2;
                break label27;
            }
            if (j >= -150)
            {
                m = 1;
                break label27;
            }
            m = 0;
            break label27;
        }
    }

    public int getDbm()
    {
        int i;
        if (isGsm())
            if ((this.mLteSignalStrength == -1) && (this.mLteRsrp == -1) && (this.mLteRsrq == -1) && (this.mLteCqi == -1))
                i = getGsmDbm();
        while (true)
        {
            return i;
            i = getLteDbm();
            continue;
            i = getCdmaDbm();
        }
    }

    public int getEvdoAsuLevel()
    {
        int i = getEvdoDbm();
        int j = getEvdoSnr();
        int k;
        int m;
        if (i >= -65)
        {
            k = 16;
            if (j < 7)
                break label92;
            m = 16;
            label29: if (k >= m)
                break label145;
        }
        label145: for (int n = k; ; n = m)
        {
            return n;
            if (i >= -75)
            {
                k = 8;
                break;
            }
            if (i >= -85)
            {
                k = 4;
                break;
            }
            if (i >= -95)
            {
                k = 2;
                break;
            }
            if (i >= -105)
            {
                k = 1;
                break;
            }
            k = 99;
            break;
            label92: if (j >= 6)
            {
                m = 8;
                break label29;
            }
            if (j >= 5)
            {
                m = 4;
                break label29;
            }
            if (j >= 3)
            {
                m = 2;
                break label29;
            }
            if (j >= 1)
            {
                m = 1;
                break label29;
            }
            m = 99;
            break label29;
        }
    }

    public int getEvdoDbm()
    {
        return this.mEvdoDbm;
    }

    public int getEvdoEcio()
    {
        return this.mEvdoEcio;
    }

    public int getEvdoLevel()
    {
        int i = getEvdoDbm();
        int j = getEvdoSnr();
        int k;
        int m;
        if (i >= -65)
        {
            k = 4;
            if (j < 7)
                break label77;
            m = 4;
            label27: if (k >= m)
                break label116;
        }
        label77: label116: for (int n = k; ; n = m)
        {
            return n;
            if (i >= -75)
            {
                k = 3;
                break;
            }
            if (i >= -90)
            {
                k = 2;
                break;
            }
            if (i >= -105)
            {
                k = 1;
                break;
            }
            k = 0;
            break;
            if (j >= 5)
            {
                m = 3;
                break label27;
            }
            if (j >= 3)
            {
                m = 2;
                break label27;
            }
            if (j >= 1)
            {
                m = 1;
                break label27;
            }
            m = 0;
            break label27;
        }
    }

    public int getEvdoSnr()
    {
        return this.mEvdoSnr;
    }

    public int getGsmAsuLevel()
    {
        return getGsmSignalStrength();
    }

    public int getGsmBitErrorRate()
    {
        return this.mGsmBitErrorRate;
    }

    public int getGsmDbm()
    {
        int i = getGsmSignalStrength();
        int j;
        if (i == 99)
        {
            j = -1;
            if (j == -1)
                break label34;
        }
        label34: for (int k = -113 + j * 2; ; k = -1)
        {
            return k;
            j = i;
            break;
        }
    }

    public int getGsmLevel()
    {
        int i = getGsmSignalStrength();
        int j;
        if ((i <= 2) || (i == 99))
            j = 0;
        while (true)
        {
            return j;
            if (i >= 12)
                j = 4;
            else if (i >= 8)
                j = 3;
            else if (i >= 5)
                j = 2;
            else
                j = 1;
        }
    }

    public int getGsmSignalStrength()
    {
        return this.mGsmSignalStrength;
    }

    public int getLevel()
    {
        if (this.isGsm)
            if ((this.mLteSignalStrength == -1) && (this.mLteRsrp == -1) && (this.mLteRsrq == -1) && (this.mLteCqi == -1))
                k = getGsmLevel();
        int i;
        int j;
        while (true)
        {
            return k;
            k = getLteLevel();
            continue;
            i = getCdmaLevel();
            j = getEvdoLevel();
            if (j == 0)
            {
                k = getCdmaLevel();
            }
            else
            {
                if (i != 0)
                    break;
                k = getEvdoLevel();
            }
        }
        if (i < j);
        for (int k = i; ; k = j)
            break;
    }

    public int getLteAsuLevel()
    {
        int i = getLteDbm();
        int j;
        if (i <= -140)
            j = 0;
        while (true)
        {
            return j;
            if (i >= -43)
                j = 97;
            else
                j = i + 140;
        }
    }

    public int getLteDbm()
    {
        return this.mLteRsrp;
    }

    public int getLteLevel()
    {
        int i;
        int j;
        if (this.mLteRsrp == -1)
        {
            i = 0;
            if (this.mLteRssnr != 2147483647)
                break label82;
            j = 0;
            label22: if (this.mLteRsrp != -1)
                break label129;
        }
        for (int k = j; ; k = i)
        {
            return k;
            if (this.mLteRsrp >= -95)
            {
                i = 4;
                break;
            }
            if (this.mLteRsrp >= -105)
            {
                i = 3;
                break;
            }
            if (this.mLteRsrp >= -115)
            {
                i = 2;
                break;
            }
            i = 1;
            break;
            label82: if (this.mLteRssnr >= 45)
            {
                j = 4;
                break label22;
            }
            if (this.mLteRssnr >= 10)
            {
                j = 3;
                break label22;
            }
            if (this.mLteRssnr >= -30)
            {
                j = 2;
                break label22;
            }
            j = 1;
            break label22;
            label129: if (this.mLteRssnr != 2147483647)
                break label143;
        }
        label143: if (j < i);
        for (k = j; ; k = i)
            break;
    }

    public int hashCode()
    {
        int i = 31 * this.mGsmSignalStrength + 31 * this.mGsmBitErrorRate + 31 * this.mCdmaDbm + 31 * this.mCdmaEcio + 31 * this.mEvdoDbm + 31 * this.mEvdoEcio + 31 * this.mEvdoSnr + 31 * this.mLteSignalStrength + 31 * this.mLteRsrp + 31 * this.mLteRsrq + 31 * this.mLteRssnr + 31 * this.mLteCqi;
        if (this.isGsm);
        for (int j = 1; ; j = 0)
            return j + i;
    }

    public boolean isGsm()
    {
        return this.isGsm;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder().append("SignalStrength: ").append(this.mGsmSignalStrength).append(" ").append(this.mGsmBitErrorRate).append(" ").append(this.mCdmaDbm).append(" ").append(this.mCdmaEcio).append(" ").append(this.mEvdoDbm).append(" ").append(this.mEvdoEcio).append(" ").append(this.mEvdoSnr).append(" ").append(this.mLteSignalStrength).append(" ").append(this.mLteRsrp).append(" ").append(this.mLteRsrq).append(" ").append(this.mLteRssnr).append(" ").append(this.mLteCqi).append(" ");
        if (this.isGsm);
        for (String str = "gsm|lte"; ; str = "cdma")
            return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mGsmSignalStrength);
        paramParcel.writeInt(this.mGsmBitErrorRate);
        paramParcel.writeInt(this.mCdmaDbm);
        paramParcel.writeInt(this.mCdmaEcio);
        paramParcel.writeInt(this.mEvdoDbm);
        paramParcel.writeInt(this.mEvdoEcio);
        paramParcel.writeInt(this.mEvdoSnr);
        paramParcel.writeInt(this.mLteSignalStrength);
        paramParcel.writeInt(this.mLteRsrp);
        paramParcel.writeInt(this.mLteRsrq);
        paramParcel.writeInt(this.mLteRssnr);
        paramParcel.writeInt(this.mLteCqi);
        if (this.isGsm);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.SignalStrength
 * JD-Core Version:        0.6.2
 */