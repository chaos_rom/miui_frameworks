package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class NeighboringCellInfo
    implements Parcelable
{
    public static final Parcelable.Creator<NeighboringCellInfo> CREATOR = new Parcelable.Creator()
    {
        public NeighboringCellInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new NeighboringCellInfo(paramAnonymousParcel);
        }

        public NeighboringCellInfo[] newArray(int paramAnonymousInt)
        {
            return new NeighboringCellInfo[paramAnonymousInt];
        }
    };
    public static final int UNKNOWN_CID = -1;
    public static final int UNKNOWN_RSSI = 99;
    private int mCid;
    private int mLac;
    private int mNetworkType;
    private int mPsc;
    private int mRssi;

    @Deprecated
    public NeighboringCellInfo()
    {
        this.mRssi = 99;
        this.mLac = -1;
        this.mCid = -1;
        this.mPsc = -1;
        this.mNetworkType = 0;
    }

    @Deprecated
    public NeighboringCellInfo(int paramInt1, int paramInt2)
    {
        this.mRssi = paramInt1;
        this.mCid = paramInt2;
    }

    public NeighboringCellInfo(int paramInt1, String paramString, int paramInt2)
    {
        this.mRssi = paramInt1;
        this.mNetworkType = 0;
        this.mPsc = -1;
        this.mLac = -1;
        this.mCid = -1;
        int i = paramString.length();
        if (i > 8);
        while (true)
        {
            return;
            if (i < 8)
                for (int j = 0; j < 8 - i; j++)
                    paramString = "0" + paramString;
            switch (paramInt2)
            {
            case 4:
            case 5:
            case 6:
            case 7:
            default:
                break;
            case 1:
            case 2:
                try
                {
                    this.mNetworkType = paramInt2;
                    if (!paramString.equalsIgnoreCase("FFFFFFFF"))
                    {
                        this.mCid = Integer.valueOf(paramString.substring(4), 16).intValue();
                        this.mLac = Integer.valueOf(paramString.substring(0, 4), 16).intValue();
                    }
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    this.mPsc = -1;
                    this.mLac = -1;
                    this.mCid = -1;
                    this.mNetworkType = 0;
                }
                break;
            case 3:
            case 8:
            case 9:
            case 10:
                this.mNetworkType = paramInt2;
                this.mPsc = Integer.valueOf(paramString, 16).intValue();
            }
        }
    }

    public NeighboringCellInfo(Parcel paramParcel)
    {
        this.mRssi = paramParcel.readInt();
        this.mLac = paramParcel.readInt();
        this.mCid = paramParcel.readInt();
        this.mPsc = paramParcel.readInt();
        this.mNetworkType = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCid()
    {
        return this.mCid;
    }

    public int getLac()
    {
        return this.mLac;
    }

    public int getNetworkType()
    {
        return this.mNetworkType;
    }

    public int getPsc()
    {
        return this.mPsc;
    }

    public int getRssi()
    {
        return this.mRssi;
    }

    @Deprecated
    public void setCid(int paramInt)
    {
        this.mCid = paramInt;
    }

    @Deprecated
    public void setRssi(int paramInt)
    {
        this.mRssi = paramInt;
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("[");
        if (this.mPsc != -1)
        {
            localStringBuilder3 = localStringBuilder1.append(Integer.toHexString(this.mPsc)).append("@");
            if (this.mRssi == 99)
            {
                localObject2 = "-";
                localStringBuilder3.append(localObject2);
            }
        }
        while ((this.mLac == -1) || (this.mCid == -1))
            while (true)
            {
                StringBuilder localStringBuilder3;
                localStringBuilder1.append("]");
                return localStringBuilder1.toString();
                Object localObject2 = Integer.valueOf(this.mRssi);
            }
        StringBuilder localStringBuilder2 = localStringBuilder1.append(Integer.toHexString(this.mLac)).append(Integer.toHexString(this.mCid)).append("@");
        if (this.mRssi == 99);
        for (Object localObject1 = "-"; ; localObject1 = Integer.valueOf(this.mRssi))
        {
            localStringBuilder2.append(localObject1);
            break;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mRssi);
        paramParcel.writeInt(this.mLac);
        paramParcel.writeInt(this.mCid);
        paramParcel.writeInt(this.mPsc);
        paramParcel.writeInt(this.mNetworkType);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.NeighboringCellInfo
 * JD-Core Version:        0.6.2
 */