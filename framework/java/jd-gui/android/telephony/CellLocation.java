package android.telephony;

import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;

public abstract class CellLocation
{
    public static CellLocation getEmpty()
    {
        Object localObject;
        switch (TelephonyManager.getDefault().getCurrentPhoneType())
        {
        default:
            localObject = null;
        case 2:
        case 1:
        }
        while (true)
        {
            return localObject;
            localObject = new CdmaCellLocation();
            continue;
            localObject = new GsmCellLocation();
        }
    }

    public static CellLocation newFromBundle(Bundle paramBundle)
    {
        Object localObject;
        switch (TelephonyManager.getDefault().getCurrentPhoneType())
        {
        default:
            localObject = null;
        case 2:
        case 1:
        }
        while (true)
        {
            return localObject;
            localObject = new CdmaCellLocation(paramBundle);
            continue;
            localObject = new GsmCellLocation(paramBundle);
        }
    }

    public static void requestLocationUpdate()
    {
        try
        {
            ITelephony localITelephony = ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
            if (localITelephony != null)
                localITelephony.updateServiceLocation();
            label19: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label19;
        }
    }

    public abstract void fillInNotifierBundle(Bundle paramBundle);

    public abstract boolean isEmpty();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.CellLocation
 * JD-Core Version:        0.6.2
 */