package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class GsmCellIdentity extends CellIdentity
    implements Parcelable
{
    public static final Parcelable.Creator<GsmCellIdentity> CREATOR = new Parcelable.Creator()
    {
        public GsmCellIdentity createFromParcel(Parcel paramAnonymousParcel)
        {
            return new GsmCellIdentity(paramAnonymousParcel, null);
        }

        public GsmCellIdentity[] newArray(int paramAnonymousInt)
        {
            return new GsmCellIdentity[paramAnonymousInt];
        }
    };
    private final int mCid;
    private final int mLac;
    private final int mMcc;
    private final int mMnc;
    private final int mPsc;

    public GsmCellIdentity(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString)
    {
        super(1, paramString);
        this.mMcc = paramInt1;
        this.mMnc = paramInt2;
        this.mLac = paramInt3;
        this.mCid = paramInt4;
        this.mPsc = paramInt5;
    }

    private GsmCellIdentity(Parcel paramParcel)
    {
        super(paramParcel);
        this.mMcc = paramParcel.readInt();
        this.mMnc = paramParcel.readInt();
        this.mLac = paramParcel.readInt();
        this.mCid = paramParcel.readInt();
        this.mPsc = paramParcel.readInt();
    }

    GsmCellIdentity(GsmCellIdentity paramGsmCellIdentity)
    {
        super(paramGsmCellIdentity);
        this.mMcc = paramGsmCellIdentity.mMcc;
        this.mMnc = paramGsmCellIdentity.mMnc;
        this.mLac = paramGsmCellIdentity.mLac;
        this.mCid = paramGsmCellIdentity.mCid;
        this.mPsc = paramGsmCellIdentity.mPsc;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCid()
    {
        return this.mCid;
    }

    public int getLac()
    {
        return this.mLac;
    }

    public int getMcc()
    {
        return this.mMcc;
    }

    public int getMnc()
    {
        return this.mMnc;
    }

    public int getPsc()
    {
        return this.mPsc;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.mMcc);
        paramParcel.writeInt(this.mMnc);
        paramParcel.writeInt(this.mLac);
        paramParcel.writeInt(this.mCid);
        paramParcel.writeInt(this.mPsc);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.GsmCellIdentity
 * JD-Core Version:        0.6.2
 */