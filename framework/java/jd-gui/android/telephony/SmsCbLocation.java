package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SmsCbLocation
    implements Parcelable
{
    public static final Parcelable.Creator<SmsCbLocation> CREATOR = new Parcelable.Creator()
    {
        public SmsCbLocation createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SmsCbLocation(paramAnonymousParcel);
        }

        public SmsCbLocation[] newArray(int paramAnonymousInt)
        {
            return new SmsCbLocation[paramAnonymousInt];
        }
    };
    private final int mCid;
    private final int mLac;
    private final String mPlmn;

    public SmsCbLocation()
    {
        this.mPlmn = "";
        this.mLac = -1;
        this.mCid = -1;
    }

    public SmsCbLocation(Parcel paramParcel)
    {
        this.mPlmn = paramParcel.readString();
        this.mLac = paramParcel.readInt();
        this.mCid = paramParcel.readInt();
    }

    public SmsCbLocation(String paramString)
    {
        this.mPlmn = paramString;
        this.mLac = -1;
        this.mCid = -1;
    }

    public SmsCbLocation(String paramString, int paramInt1, int paramInt2)
    {
        this.mPlmn = paramString;
        this.mLac = paramInt1;
        this.mCid = paramInt2;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (!(paramObject instanceof SmsCbLocation)))
            {
                bool = false;
            }
            else
            {
                SmsCbLocation localSmsCbLocation = (SmsCbLocation)paramObject;
                if ((!this.mPlmn.equals(localSmsCbLocation.mPlmn)) || (this.mLac != localSmsCbLocation.mLac) || (this.mCid != localSmsCbLocation.mCid))
                    bool = false;
            }
        }
    }

    public int getCid()
    {
        return this.mCid;
    }

    public int getLac()
    {
        return this.mLac;
    }

    public String getPlmn()
    {
        return this.mPlmn;
    }

    public int hashCode()
    {
        return 31 * (31 * this.mPlmn.hashCode() + this.mLac) + this.mCid;
    }

    public boolean isInLocationArea(SmsCbLocation paramSmsCbLocation)
    {
        boolean bool = false;
        if ((this.mCid != -1) && (this.mCid != paramSmsCbLocation.mCid));
        while (true)
        {
            return bool;
            if ((this.mLac == -1) || (this.mLac == paramSmsCbLocation.mLac))
                bool = this.mPlmn.equals(paramSmsCbLocation.mPlmn);
        }
    }

    public boolean isInLocationArea(String paramString, int paramInt1, int paramInt2)
    {
        boolean bool = false;
        if (!this.mPlmn.equals(paramString));
        while (true)
        {
            return bool;
            if (((this.mLac == -1) || (this.mLac == paramInt1)) && ((this.mCid == -1) || (this.mCid == paramInt2)))
                bool = true;
        }
    }

    public String toString()
    {
        return '[' + this.mPlmn + ',' + this.mLac + ',' + this.mCid + ']';
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mPlmn);
        paramParcel.writeInt(this.mLac);
        paramParcel.writeInt(this.mCid);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.SmsCbLocation
 * JD-Core Version:        0.6.2
 */