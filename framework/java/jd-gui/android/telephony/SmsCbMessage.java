package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SmsCbMessage
    implements Parcelable
{
    public static final Parcelable.Creator<SmsCbMessage> CREATOR = new Parcelable.Creator()
    {
        public SmsCbMessage createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SmsCbMessage(paramAnonymousParcel);
        }

        public SmsCbMessage[] newArray(int paramAnonymousInt)
        {
            return new SmsCbMessage[paramAnonymousInt];
        }
    };
    public static final int GEOGRAPHICAL_SCOPE_CELL_WIDE = 3;
    public static final int GEOGRAPHICAL_SCOPE_CELL_WIDE_IMMEDIATE = 0;
    public static final int GEOGRAPHICAL_SCOPE_LA_WIDE = 2;
    public static final int GEOGRAPHICAL_SCOPE_PLMN_WIDE = 1;
    protected static final String LOG_TAG = "SMSCB";
    public static final int MESSAGE_FORMAT_3GPP = 1;
    public static final int MESSAGE_FORMAT_3GPP2 = 2;
    public static final int MESSAGE_PRIORITY_EMERGENCY = 3;
    public static final int MESSAGE_PRIORITY_INTERACTIVE = 1;
    public static final int MESSAGE_PRIORITY_NORMAL = 0;
    public static final int MESSAGE_PRIORITY_URGENT = 2;
    private final String mBody;
    private final SmsCbCmasInfo mCmasWarningInfo;
    private final SmsCbEtwsInfo mEtwsWarningInfo;
    private final int mGeographicalScope;
    private final String mLanguage;
    private final SmsCbLocation mLocation;
    private final int mMessageFormat;
    private final int mPriority;
    private final int mSerialNumber;
    private final int mServiceCategory;

    public SmsCbMessage(int paramInt1, int paramInt2, int paramInt3, SmsCbLocation paramSmsCbLocation, int paramInt4, String paramString1, String paramString2, int paramInt5, SmsCbEtwsInfo paramSmsCbEtwsInfo, SmsCbCmasInfo paramSmsCbCmasInfo)
    {
        this.mMessageFormat = paramInt1;
        this.mGeographicalScope = paramInt2;
        this.mSerialNumber = paramInt3;
        this.mLocation = paramSmsCbLocation;
        this.mServiceCategory = paramInt4;
        this.mLanguage = paramString1;
        this.mBody = paramString2;
        this.mPriority = paramInt5;
        this.mEtwsWarningInfo = paramSmsCbEtwsInfo;
        this.mCmasWarningInfo = paramSmsCbCmasInfo;
    }

    public SmsCbMessage(Parcel paramParcel)
    {
        this.mMessageFormat = paramParcel.readInt();
        this.mGeographicalScope = paramParcel.readInt();
        this.mSerialNumber = paramParcel.readInt();
        this.mLocation = new SmsCbLocation(paramParcel);
        this.mServiceCategory = paramParcel.readInt();
        this.mLanguage = paramParcel.readString();
        this.mBody = paramParcel.readString();
        this.mPriority = paramParcel.readInt();
        switch (paramParcel.readInt())
        {
        case 68:
        default:
            this.mEtwsWarningInfo = null;
            this.mCmasWarningInfo = null;
        case 69:
        case 67:
        }
        while (true)
        {
            return;
            this.mEtwsWarningInfo = new SmsCbEtwsInfo(paramParcel);
            this.mCmasWarningInfo = null;
            continue;
            this.mEtwsWarningInfo = null;
            this.mCmasWarningInfo = new SmsCbCmasInfo(paramParcel);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public SmsCbCmasInfo getCmasWarningInfo()
    {
        return this.mCmasWarningInfo;
    }

    public SmsCbEtwsInfo getEtwsWarningInfo()
    {
        return this.mEtwsWarningInfo;
    }

    public int getGeographicalScope()
    {
        return this.mGeographicalScope;
    }

    public String getLanguageCode()
    {
        return this.mLanguage;
    }

    public SmsCbLocation getLocation()
    {
        return this.mLocation;
    }

    public String getMessageBody()
    {
        return this.mBody;
    }

    public int getMessageFormat()
    {
        return this.mMessageFormat;
    }

    public int getMessagePriority()
    {
        return this.mPriority;
    }

    public int getSerialNumber()
    {
        return this.mSerialNumber;
    }

    public int getServiceCategory()
    {
        return this.mServiceCategory;
    }

    public boolean isCmasMessage()
    {
        if (this.mCmasWarningInfo != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEmergencyMessage()
    {
        if (this.mPriority == 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEtwsMessage()
    {
        if (this.mEtwsWarningInfo != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder().append("SmsCbMessage{geographicalScope=").append(this.mGeographicalScope).append(", serialNumber=").append(this.mSerialNumber).append(", location=").append(this.mLocation).append(", serviceCategory=").append(this.mServiceCategory).append(", language=").append(this.mLanguage).append(", body=").append(this.mBody).append(", priority=").append(this.mPriority);
        String str1;
        StringBuilder localStringBuilder2;
        if (this.mEtwsWarningInfo != null)
        {
            str1 = ", " + this.mEtwsWarningInfo.toString();
            localStringBuilder2 = localStringBuilder1.append(str1);
            if (this.mCmasWarningInfo == null)
                break label186;
        }
        label186: for (String str2 = ", " + this.mCmasWarningInfo.toString(); ; str2 = "")
        {
            return str2 + '}';
            str1 = "";
            break;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mMessageFormat);
        paramParcel.writeInt(this.mGeographicalScope);
        paramParcel.writeInt(this.mSerialNumber);
        this.mLocation.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.mServiceCategory);
        paramParcel.writeString(this.mLanguage);
        paramParcel.writeString(this.mBody);
        paramParcel.writeInt(this.mPriority);
        if (this.mEtwsWarningInfo != null)
        {
            paramParcel.writeInt(69);
            this.mEtwsWarningInfo.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            return;
            if (this.mCmasWarningInfo != null)
            {
                paramParcel.writeInt(67);
                this.mCmasWarningInfo.writeToParcel(paramParcel, paramInt);
            }
            else
            {
                paramParcel.writeInt(48);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.telephony.SmsCbMessage
 * JD-Core Version:        0.6.2
 */