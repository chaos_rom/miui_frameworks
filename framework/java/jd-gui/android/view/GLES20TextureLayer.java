package android.view;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;

class GLES20TextureLayer extends GLES20Layer
{
    private SurfaceTexture mSurface;
    private int mTexture;

    GLES20TextureLayer(SurfaceTexture paramSurfaceTexture, boolean paramBoolean)
    {
        this(paramBoolean);
        this.mSurface = paramSurfaceTexture;
        this.mSurface.attachToGLContext(this.mTexture);
    }

    GLES20TextureLayer(boolean paramBoolean)
    {
        int[] arrayOfInt = new int[2];
        this.mLayer = GLES20Canvas.nCreateTextureLayer(paramBoolean, arrayOfInt);
        if (this.mLayer != 0)
            this.mTexture = arrayOfInt[0];
        for (this.mFinalizer = new GLES20Layer.Finalizer(this.mLayer); ; this.mFinalizer = null)
            return;
    }

    void end(Canvas paramCanvas)
    {
    }

    HardwareCanvas getCanvas()
    {
        return null;
    }

    SurfaceTexture getSurfaceTexture()
    {
        if (this.mSurface == null)
            this.mSurface = new SurfaceTexture(this.mTexture, false);
        return this.mSurface;
    }

    boolean isValid()
    {
        if ((this.mLayer != 0) && (this.mTexture != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void redraw(DisplayList paramDisplayList, Rect paramRect)
    {
    }

    void resize(int paramInt1, int paramInt2)
    {
    }

    void setSurfaceTexture(SurfaceTexture paramSurfaceTexture)
    {
        if (this.mSurface != null)
            this.mSurface.release();
        this.mSurface = paramSurfaceTexture;
        this.mSurface.attachToGLContext(this.mTexture);
    }

    void setTransform(Matrix paramMatrix)
    {
        GLES20Canvas.nSetTextureLayerTransform(this.mLayer, paramMatrix.native_instance);
    }

    HardwareCanvas start(Canvas paramCanvas)
    {
        return null;
    }

    void update(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        super.update(paramInt1, paramInt2, paramBoolean);
        GLES20Canvas.nUpdateTextureLayer(this.mLayer, paramInt1, paramInt2, paramBoolean, this.mSurface);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GLES20TextureLayer
 * JD-Core Version:        0.6.2
 */