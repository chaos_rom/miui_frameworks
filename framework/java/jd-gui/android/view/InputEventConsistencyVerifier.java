package android.view;

import android.os.Build;
import android.util.Log;

public final class InputEventConsistencyVerifier
{
    private static final String EVENT_TYPE_GENERIC_MOTION = "GenericMotionEvent";
    private static final String EVENT_TYPE_KEY = "KeyEvent";
    private static final String EVENT_TYPE_TOUCH = "TouchEvent";
    private static final String EVENT_TYPE_TRACKBALL = "TrackballEvent";
    public static final int FLAG_RAW_DEVICE_INPUT = 1;
    private static final boolean IS_ENG_BUILD = false;
    private static final int RECENT_EVENTS_TO_LOG = 5;
    private final Object mCaller;
    private InputEvent mCurrentEvent;
    private String mCurrentEventType;
    private final int mFlags;
    private boolean mHoverEntered;
    private KeyState mKeyStateList;
    private int mLastEventSeq;
    private String mLastEventType;
    private int mLastNestingLevel;
    private final String mLogTag;
    private int mMostRecentEventIndex;
    private InputEvent[] mRecentEvents;
    private boolean[] mRecentEventsUnhandled;
    private int mTouchEventStreamDeviceId = -1;
    private boolean mTouchEventStreamIsTainted;
    private int mTouchEventStreamPointers;
    private int mTouchEventStreamSource;
    private boolean mTouchEventStreamUnhandled;
    private boolean mTrackballDown;
    private boolean mTrackballUnhandled;
    private StringBuilder mViolationMessage;

    public InputEventConsistencyVerifier(Object paramObject, int paramInt)
    {
        this(paramObject, paramInt, InputEventConsistencyVerifier.class.getSimpleName());
    }

    public InputEventConsistencyVerifier(Object paramObject, int paramInt, String paramString)
    {
        this.mCaller = paramObject;
        this.mFlags = paramInt;
        if (paramString != null);
        while (true)
        {
            this.mLogTag = paramString;
            return;
            paramString = "InputEventConsistencyVerifier";
        }
    }

    private void addKeyState(int paramInt1, int paramInt2, int paramInt3)
    {
        KeyState localKeyState = KeyState.obtain(paramInt1, paramInt2, paramInt3);
        localKeyState.next = this.mKeyStateList;
        this.mKeyStateList = localKeyState;
    }

    private static void appendEvent(StringBuilder paramStringBuilder, int paramInt, InputEvent paramInputEvent, boolean paramBoolean)
    {
        paramStringBuilder.append(paramInt).append(": sent at ").append(paramInputEvent.getEventTimeNano());
        paramStringBuilder.append(", ");
        if (paramBoolean)
            paramStringBuilder.append("(unhandled) ");
        paramStringBuilder.append(paramInputEvent);
    }

    private void ensureHistorySizeIsZeroForThisAction(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getHistorySize();
        if (i != 0)
            problem("History size is " + i + " but it should always be 0 for " + MotionEvent.actionToString(paramMotionEvent.getAction()));
    }

    private void ensureMetaStateIsNormalized(int paramInt)
    {
        int i = KeyEvent.normalizeMetaState(paramInt);
        if (i != paramInt)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = Integer.valueOf(paramInt);
            arrayOfObject[1] = Integer.valueOf(i);
            problem(String.format("Metastate not normalized.    Was 0x%08x but expected 0x%08x.", arrayOfObject));
        }
    }

    private void ensurePointerCountIsOneForThisAction(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getPointerCount();
        if (i != 1)
            problem("Pointer count is " + i + " but it should always be 1 for " + MotionEvent.actionToString(paramMotionEvent.getAction()));
    }

    private KeyState findKeyState(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        Object localObject = null;
        KeyState localKeyState = this.mKeyStateList;
        if (localKeyState != null)
            if ((localKeyState.deviceId == paramInt1) && (localKeyState.source == paramInt2) && (localKeyState.keyCode == paramInt3))
                if (paramBoolean)
                {
                    if (localObject == null)
                        break label70;
                    localObject.next = localKeyState.next;
                    label61: localKeyState.next = null;
                }
        while (true)
        {
            return localKeyState;
            label70: this.mKeyStateList = localKeyState.next;
            break label61;
            localObject = localKeyState;
            localKeyState = localKeyState.next;
            break;
            localKeyState = null;
        }
    }

    private void finishEvent()
    {
        if ((this.mViolationMessage != null) && (this.mViolationMessage.length() != 0))
            if (!this.mCurrentEvent.isTainted())
            {
                this.mViolationMessage.append("\n    in ").append(this.mCaller);
                this.mViolationMessage.append("\n    ");
                appendEvent(this.mViolationMessage, 0, this.mCurrentEvent, false);
                if (this.mRecentEvents != null)
                    this.mViolationMessage.append("\n    -- recent events --");
            }
        for (int j = 0; ; j++)
        {
            int k;
            InputEvent localInputEvent;
            if (j < 5)
            {
                k = (5 + this.mMostRecentEventIndex - j) % 5;
                localInputEvent = this.mRecentEvents[k];
                if (localInputEvent != null);
            }
            else
            {
                Log.d(this.mLogTag, this.mViolationMessage.toString());
                this.mCurrentEvent.setTainted(true);
                this.mViolationMessage.setLength(0);
                if (this.mRecentEvents == null)
                {
                    this.mRecentEvents = new InputEvent[5];
                    this.mRecentEventsUnhandled = new boolean[5];
                }
                int i = (1 + this.mMostRecentEventIndex) % 5;
                this.mMostRecentEventIndex = i;
                if (this.mRecentEvents[i] != null)
                    this.mRecentEvents[i].recycle();
                this.mRecentEvents[i] = this.mCurrentEvent.copy();
                this.mRecentEventsUnhandled[i] = false;
                this.mCurrentEvent = null;
                this.mCurrentEventType = null;
                return;
            }
            this.mViolationMessage.append("\n    ");
            appendEvent(this.mViolationMessage, j + 1, localInputEvent, this.mRecentEventsUnhandled[k]);
        }
    }

    public static boolean isInstrumentationEnabled()
    {
        return IS_ENG_BUILD;
    }

    private void problem(String paramString)
    {
        if (this.mViolationMessage == null)
            this.mViolationMessage = new StringBuilder();
        if (this.mViolationMessage.length() == 0)
            this.mViolationMessage.append(this.mCurrentEventType).append(": ");
        while (true)
        {
            this.mViolationMessage.append(paramString);
            return;
            this.mViolationMessage.append("\n    ");
        }
    }

    private boolean startEvent(InputEvent paramInputEvent, int paramInt, String paramString)
    {
        boolean bool = false;
        int i = paramInputEvent.getSequenceNumber();
        if ((i == this.mLastEventSeq) && (paramInt < this.mLastNestingLevel) && (paramString == this.mLastEventType))
            return bool;
        if (paramInt > 0)
        {
            this.mLastEventSeq = i;
            this.mLastEventType = paramString;
        }
        for (this.mLastNestingLevel = paramInt; ; this.mLastNestingLevel = 0)
        {
            this.mCurrentEvent = paramInputEvent;
            this.mCurrentEventType = paramString;
            bool = true;
            break;
            this.mLastEventSeq = -1;
            this.mLastEventType = null;
        }
    }

    public void onGenericMotionEvent(MotionEvent paramMotionEvent, int paramInt)
    {
        if (!startEvent(paramMotionEvent, paramInt, "GenericMotionEvent"))
            return;
        int i;
        int j;
        label172: 
        do
            while (true)
            {
                try
                {
                    ensureMetaStateIsNormalized(paramMotionEvent.getMetaState());
                    i = paramMotionEvent.getAction();
                    j = paramMotionEvent.getSource();
                    if ((j & 0x2) == 0)
                        break label172;
                    switch (i)
                    {
                    default:
                        problem("Invalid action for generic pointer event.");
                        finishEvent();
                        break;
                    case 9:
                        ensurePointerCountIsOneForThisAction(paramMotionEvent);
                        this.mHoverEntered = true;
                        continue;
                    case 7:
                    case 10:
                    case 8:
                    }
                }
                finally
                {
                    finishEvent();
                }
                ensurePointerCountIsOneForThisAction(paramMotionEvent);
                continue;
                ensurePointerCountIsOneForThisAction(paramMotionEvent);
                if (!this.mHoverEntered)
                    problem("ACTION_HOVER_EXIT without prior ACTION_HOVER_ENTER");
                this.mHoverEntered = false;
                continue;
                ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                ensurePointerCountIsOneForThisAction(paramMotionEvent);
                continue;
                problem("Invalid action for generic joystick event.");
                continue;
                ensurePointerCountIsOneForThisAction(paramMotionEvent);
            }
        while ((j & 0x10) == 0);
        switch (i)
        {
        default:
        case 2:
        }
    }

    public void onInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        if ((paramInputEvent instanceof KeyEvent))
            onKeyEvent((KeyEvent)paramInputEvent, paramInt);
        while (true)
        {
            return;
            MotionEvent localMotionEvent = (MotionEvent)paramInputEvent;
            if (localMotionEvent.isTouchEvent())
                onTouchEvent(localMotionEvent, paramInt);
            else if ((0x4 & localMotionEvent.getSource()) != 0)
                onTrackballEvent(localMotionEvent, paramInt);
            else
                onGenericMotionEvent(localMotionEvent, paramInt);
        }
    }

    public void onKeyEvent(KeyEvent paramKeyEvent, int paramInt)
    {
        if (!startEvent(paramKeyEvent, paramInt, "KeyEvent"))
            return;
        while (true)
        {
            int j;
            int k;
            int m;
            try
            {
                ensureMetaStateIsNormalized(paramKeyEvent.getMetaState());
                int i = paramKeyEvent.getAction();
                j = paramKeyEvent.getDeviceId();
                k = paramKeyEvent.getSource();
                m = paramKeyEvent.getKeyCode();
                switch (i)
                {
                default:
                    problem("Invalid action " + KeyEvent.actionToString(i) + " for key event.");
                case 2:
                    finishEvent();
                    break;
                case 0:
                    KeyState localKeyState2 = findKeyState(j, k, m, false);
                    if (localKeyState2 == null)
                        break label181;
                    if (localKeyState2.unhandled)
                    {
                        localKeyState2.unhandled = false;
                        continue;
                    }
                    break;
                case 1:
                }
            }
            finally
            {
                finishEvent();
            }
            if (((0x1 & this.mFlags) == 0) && (paramKeyEvent.getRepeatCount() == 0))
            {
                problem("ACTION_DOWN but key is already down and this event is not a key repeat.");
                continue;
                label181: addKeyState(j, k, m);
                continue;
                KeyState localKeyState1 = findKeyState(j, k, m, true);
                if (localKeyState1 == null)
                    problem("ACTION_UP but key was not down.");
                else
                    localKeyState1.recycle();
            }
        }
    }

    public void onTouchEvent(MotionEvent paramMotionEvent, int paramInt)
    {
        if (!startEvent(paramMotionEvent, paramInt, "TouchEvent"))
            return;
        int i = paramMotionEvent.getAction();
        int j;
        if ((i == 0) || (i == 3))
        {
            j = 1;
            if ((j != 0) && ((this.mTouchEventStreamIsTainted) || (this.mTouchEventStreamUnhandled)))
            {
                this.mTouchEventStreamIsTainted = false;
                this.mTouchEventStreamUnhandled = false;
                this.mTouchEventStreamPointers = 0;
            }
            if (this.mTouchEventStreamIsTainted)
                paramMotionEvent.setTainted(true);
        }
        int n;
        int i2;
        int i3;
        while (true)
        {
            try
            {
                ensureMetaStateIsNormalized(paramMotionEvent.getMetaState());
                int k = paramMotionEvent.getDeviceId();
                int m = paramMotionEvent.getSource();
                if ((j == 0) && (this.mTouchEventStreamDeviceId != -1) && ((this.mTouchEventStreamDeviceId != k) || (this.mTouchEventStreamSource != m)))
                    problem("Touch event stream contains events from multiple sources: previous device id " + this.mTouchEventStreamDeviceId + ", previous source " + Integer.toHexString(this.mTouchEventStreamSource) + ", new device id " + k + ", new source " + Integer.toHexString(m));
                this.mTouchEventStreamDeviceId = k;
                this.mTouchEventStreamSource = m;
                n = paramMotionEvent.getPointerCount();
                if ((m & 0x2) == 0)
                    break label790;
                switch (i)
                {
                default:
                    i2 = paramMotionEvent.getActionMasked();
                    i3 = paramMotionEvent.getActionIndex();
                    if (i2 != 5)
                        break label815;
                    if (this.mTouchEventStreamPointers != 0)
                        break label800;
                    problem("ACTION_POINTER_DOWN but no other pointers were down.");
                    this.mTouchEventStreamIsTainted = true;
                    break label800;
                    problem("ACTION_POINTER_DOWN index is " + i3 + " but the pointer count is " + n + ".");
                    this.mTouchEventStreamIsTainted = true;
                    ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                    finishEvent();
                    break;
                    j = 0;
                    break;
                case 0:
                    label348: if (this.mTouchEventStreamPointers != 0)
                        problem("ACTION_DOWN but pointers are already down.    Probably missing ACTION_UP from previous gesture.");
                    ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                    ensurePointerCountIsOneForThisAction(paramMotionEvent);
                    this.mTouchEventStreamPointers = (1 << paramMotionEvent.getPointerId(0));
                    continue;
                case 1:
                case 2:
                case 3:
                case 4:
                }
            }
            finally
            {
                finishEvent();
            }
            ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
            ensurePointerCountIsOneForThisAction(paramMotionEvent);
            this.mTouchEventStreamPointers = 0;
            this.mTouchEventStreamIsTainted = false;
            continue;
            int i1 = Integer.bitCount(this.mTouchEventStreamPointers);
            if (n != i1)
            {
                problem("ACTION_MOVE contained " + n + " pointers but there are currently " + i1 + " pointers down.");
                this.mTouchEventStreamIsTainted = true;
                continue;
                this.mTouchEventStreamPointers = 0;
                this.mTouchEventStreamIsTainted = false;
                continue;
                if (this.mTouchEventStreamPointers != 0)
                    problem("ACTION_OUTSIDE but pointers are still down.");
                ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                ensurePointerCountIsOneForThisAction(paramMotionEvent);
                this.mTouchEventStreamIsTainted = false;
                continue;
                label542: int i6 = paramMotionEvent.getPointerId(i3);
                int i7 = 1 << i6;
                if ((i7 & this.mTouchEventStreamPointers) != 0)
                {
                    problem("ACTION_POINTER_DOWN specified pointer id " + i6 + " which is already down.");
                    this.mTouchEventStreamIsTainted = true;
                }
                else
                {
                    this.mTouchEventStreamPointers = (i7 | this.mTouchEventStreamPointers);
                }
            }
        }
        while (true)
        {
            label619: problem("ACTION_POINTER_UP index is " + i3 + " but the pointer count is " + n + ".");
            this.mTouchEventStreamIsTainted = true;
            label666: ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
            break label348;
            label790: label800: label815: 
            do
            {
                int i4 = paramMotionEvent.getPointerId(i3);
                int i5 = 1 << i4;
                if ((i5 & this.mTouchEventStreamPointers) == 0)
                {
                    problem("ACTION_POINTER_UP specified pointer id " + i4 + " which is not currently down.");
                    this.mTouchEventStreamIsTainted = true;
                    break label666;
                }
                this.mTouchEventStreamPointers &= (i5 ^ 0xFFFFFFFF);
                break label666;
                do
                {
                    problem("Invalid action " + MotionEvent.actionToString(i) + " for touch event.");
                    break label348;
                    problem("Source was not SOURCE_CLASS_POINTER.");
                    break label348;
                    if (i3 < 0)
                        break;
                    if (i3 < n)
                        break label542;
                    break;
                }
                while (i2 != 6);
                if (i3 < 0)
                    break label619;
            }
            while (i3 < n);
        }
    }

    public void onTrackballEvent(MotionEvent paramMotionEvent, int paramInt)
    {
        if (!startEvent(paramMotionEvent, paramInt, "TrackballEvent"))
            return;
        label256: 
        while (true)
        {
            try
            {
                ensureMetaStateIsNormalized(paramMotionEvent.getMetaState());
                int i = paramMotionEvent.getAction();
                if ((0x4 & paramMotionEvent.getSource()) == 0)
                    break label256;
                switch (i)
                {
                default:
                    problem("Invalid action " + MotionEvent.actionToString(i) + " for trackball event.");
                    if ((!this.mTrackballDown) || (paramMotionEvent.getPressure() > 0.0F))
                        break label230;
                    problem("Trackball is down but pressure is not greater than 0.");
                    finishEvent();
                    break;
                case 0:
                    if ((this.mTrackballDown) && (!this.mTrackballUnhandled))
                    {
                        problem("ACTION_DOWN but trackball is already down.");
                        ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                        ensurePointerCountIsOneForThisAction(paramMotionEvent);
                        continue;
                    }
                    break;
                case 1:
                case 2:
                }
            }
            finally
            {
                finishEvent();
            }
            this.mTrackballDown = true;
            this.mTrackballUnhandled = false;
            continue;
            if (!this.mTrackballDown)
                problem("ACTION_UP but trackball is not down.");
            while (true)
            {
                ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                ensurePointerCountIsOneForThisAction(paramMotionEvent);
                break;
                this.mTrackballDown = false;
                this.mTrackballUnhandled = false;
            }
            ensurePointerCountIsOneForThisAction(paramMotionEvent);
            continue;
            label230: if ((!this.mTrackballDown) && (paramMotionEvent.getPressure() != 0.0F))
            {
                problem("Trackball is up but pressure is not equal to 0.");
                continue;
                problem("Source was not SOURCE_CLASS_TRACKBALL.");
            }
        }
    }

    public void onUnhandledEvent(InputEvent paramInputEvent, int paramInt)
    {
        if (paramInt != this.mLastNestingLevel);
        while (true)
        {
            return;
            if (this.mRecentEventsUnhandled != null)
                this.mRecentEventsUnhandled[this.mMostRecentEventIndex] = true;
            if ((paramInputEvent instanceof KeyEvent))
            {
                KeyEvent localKeyEvent = (KeyEvent)paramInputEvent;
                KeyState localKeyState = findKeyState(localKeyEvent.getDeviceId(), localKeyEvent.getSource(), localKeyEvent.getKeyCode(), false);
                if (localKeyState != null)
                    localKeyState.unhandled = true;
            }
            else
            {
                MotionEvent localMotionEvent = (MotionEvent)paramInputEvent;
                if (localMotionEvent.isTouchEvent())
                    this.mTouchEventStreamUnhandled = true;
                else if (((0x4 & localMotionEvent.getSource()) != 0) && (this.mTrackballDown))
                    this.mTrackballUnhandled = true;
            }
        }
    }

    public void reset()
    {
        this.mLastEventSeq = -1;
        this.mLastNestingLevel = 0;
        this.mTrackballDown = false;
        this.mTrackballUnhandled = false;
        this.mTouchEventStreamPointers = 0;
        this.mTouchEventStreamIsTainted = false;
        this.mTouchEventStreamUnhandled = false;
        this.mHoverEntered = false;
        while (this.mKeyStateList != null)
        {
            KeyState localKeyState = this.mKeyStateList;
            this.mKeyStateList = localKeyState.next;
            localKeyState.recycle();
        }
    }

    private static final class KeyState
    {
        private static KeyState mRecycledList;
        private static Object mRecycledListLock = new Object();
        public int deviceId;
        public int keyCode;
        public KeyState next;
        public int source;
        public boolean unhandled;

        public static KeyState obtain(int paramInt1, int paramInt2, int paramInt3)
        {
            synchronized (mRecycledListLock)
            {
                KeyState localKeyState = mRecycledList;
                if (localKeyState != null)
                {
                    mRecycledList = localKeyState.next;
                    localKeyState.deviceId = paramInt1;
                    localKeyState.source = paramInt2;
                    localKeyState.keyCode = paramInt3;
                    localKeyState.unhandled = false;
                    return localKeyState;
                }
                localKeyState = new KeyState();
            }
        }

        public void recycle()
        {
            synchronized (mRecycledListLock)
            {
                this.next = mRecycledList;
                mRecycledList = this.next;
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InputEventConsistencyVerifier
 * JD-Core Version:        0.6.2
 */