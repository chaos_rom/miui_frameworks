package android.view;

import android.content.Context;
import android.util.FloatMath;

public class ScaleGestureDetector
{
    private static final String TAG = "ScaleGestureDetector";
    private final Context mContext;
    private float mCurrSpan;
    private float mCurrSpanX;
    private float mCurrSpanY;
    private long mCurrTime;
    private float mFocusX;
    private float mFocusY;
    private boolean mInProgress;
    private float mInitialSpan;
    private final InputEventConsistencyVerifier mInputEventConsistencyVerifier;
    private final OnScaleGestureListener mListener;
    private float mPrevSpan;
    private float mPrevSpanX;
    private float mPrevSpanY;
    private long mPrevTime;
    private int mSpanSlop;

    public ScaleGestureDetector(Context paramContext, OnScaleGestureListener paramOnScaleGestureListener)
    {
        if (InputEventConsistencyVerifier.isInstrumentationEnabled());
        for (InputEventConsistencyVerifier localInputEventConsistencyVerifier = new InputEventConsistencyVerifier(this, 0); ; localInputEventConsistencyVerifier = null)
        {
            this.mInputEventConsistencyVerifier = localInputEventConsistencyVerifier;
            this.mContext = paramContext;
            this.mListener = paramOnScaleGestureListener;
            this.mSpanSlop = (2 * ViewConfiguration.get(paramContext).getScaledTouchSlop());
            return;
        }
    }

    public float getCurrentSpan()
    {
        return this.mCurrSpan;
    }

    public float getCurrentSpanX()
    {
        return this.mCurrSpanX;
    }

    public float getCurrentSpanY()
    {
        return this.mCurrSpanY;
    }

    public long getEventTime()
    {
        return this.mCurrTime;
    }

    public float getFocusX()
    {
        return this.mFocusX;
    }

    public float getFocusY()
    {
        return this.mFocusY;
    }

    public float getPreviousSpan()
    {
        return this.mPrevSpan;
    }

    public float getPreviousSpanX()
    {
        return this.mPrevSpanX;
    }

    public float getPreviousSpanY()
    {
        return this.mPrevSpanY;
    }

    public float getScaleFactor()
    {
        if (this.mPrevSpan > 0.0F);
        for (float f = this.mCurrSpan / this.mPrevSpan; ; f = 1.0F)
            return f;
    }

    public long getTimeDelta()
    {
        return this.mCurrTime - this.mPrevTime;
    }

    public boolean isInProgress()
    {
        return this.mInProgress;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mInputEventConsistencyVerifier != null)
            this.mInputEventConsistencyVerifier.onTouchEvent(paramMotionEvent, 0);
        int i = paramMotionEvent.getActionMasked();
        int j;
        if ((i == 1) || (i == 3))
        {
            j = 1;
            if ((i != 0) && (j == 0))
                break label83;
            if (this.mInProgress)
            {
                this.mListener.onScaleEnd(this);
                this.mInProgress = false;
                this.mInitialSpan = 0.0F;
            }
            if (j == 0)
                break label83;
        }
        for (boolean bool2 = true; ; bool2 = true)
        {
            return bool2;
            j = 0;
            break;
            label83: int k;
            int m;
            label106: int n;
            label117: float f1;
            float f2;
            int i1;
            int i2;
            if ((i == 6) || (i == 5))
            {
                k = 1;
                if (i != 6)
                    break label158;
                m = 1;
                if (m == 0)
                    break label164;
                n = paramMotionEvent.getActionIndex();
                f1 = 0.0F;
                f2 = 0.0F;
                i1 = paramMotionEvent.getPointerCount();
                i2 = 0;
                label132: if (i2 >= i1)
                    break label196;
                if (n != i2)
                    break label171;
            }
            while (true)
            {
                i2++;
                break label132;
                k = 0;
                break;
                label158: m = 0;
                break label106;
                label164: n = -1;
                break label117;
                label171: f1 += paramMotionEvent.getX(i2);
                f2 += paramMotionEvent.getY(i2);
            }
            label196: int i3;
            float f3;
            float f4;
            float f5;
            float f6;
            int i4;
            if (m != 0)
            {
                i3 = i1 - 1;
                f3 = f1 / i3;
                f4 = f2 / i3;
                f5 = 0.0F;
                f6 = 0.0F;
                i4 = 0;
                label232: if (i4 >= i1)
                    break label296;
                if (n != i4)
                    break label259;
            }
            while (true)
            {
                i4++;
                break label232;
                i3 = i1;
                break;
                label259: f5 += Math.abs(paramMotionEvent.getX(i4) - f3);
                f6 += Math.abs(paramMotionEvent.getY(i4) - f4);
            }
            label296: float f7 = f5 / i3;
            float f8 = f6 / i3;
            float f9 = f7 * 2.0F;
            float f10 = f8 * 2.0F;
            float f11 = FloatMath.sqrt(f9 * f9 + f10 * f10);
            boolean bool1 = this.mInProgress;
            this.mFocusX = f3;
            this.mFocusY = f4;
            if ((this.mInProgress) && ((f11 == 0.0F) || (k != 0)))
            {
                this.mListener.onScaleEnd(this);
                this.mInProgress = false;
                this.mInitialSpan = f11;
            }
            if (k != 0)
            {
                this.mCurrSpanX = f9;
                this.mPrevSpanX = f9;
                this.mCurrSpanY = f10;
                this.mPrevSpanY = f10;
                this.mCurrSpan = f11;
                this.mPrevSpan = f11;
                this.mInitialSpan = f11;
            }
            if ((!this.mInProgress) && (f11 != 0.0F) && ((bool1) || (Math.abs(f11 - this.mInitialSpan) > this.mSpanSlop)))
            {
                this.mCurrSpanX = f9;
                this.mPrevSpanX = f9;
                this.mCurrSpanY = f10;
                this.mPrevSpanY = f10;
                this.mCurrSpan = f11;
                this.mPrevSpan = f11;
                this.mInProgress = this.mListener.onScaleBegin(this);
            }
            if (i == 2)
            {
                this.mCurrSpanX = f9;
                this.mCurrSpanY = f10;
                this.mCurrSpan = f11;
                boolean bool3 = true;
                if (this.mInProgress)
                    bool3 = this.mListener.onScale(this);
                if (bool3)
                {
                    this.mPrevSpanX = this.mCurrSpanX;
                    this.mPrevSpanY = this.mCurrSpanY;
                    this.mPrevSpan = this.mCurrSpan;
                }
            }
        }
    }

    public static class SimpleOnScaleGestureListener
        implements ScaleGestureDetector.OnScaleGestureListener
    {
        public boolean onScale(ScaleGestureDetector paramScaleGestureDetector)
        {
            return false;
        }

        public boolean onScaleBegin(ScaleGestureDetector paramScaleGestureDetector)
        {
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector paramScaleGestureDetector)
        {
        }
    }

    public static abstract interface OnScaleGestureListener
    {
        public abstract boolean onScale(ScaleGestureDetector paramScaleGestureDetector);

        public abstract boolean onScaleBegin(ScaleGestureDetector paramScaleGestureDetector);

        public abstract void onScaleEnd(ScaleGestureDetector paramScaleGestureDetector);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ScaleGestureDetector
 * JD-Core Version:        0.6.2
 */