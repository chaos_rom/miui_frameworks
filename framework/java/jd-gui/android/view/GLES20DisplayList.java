package android.view;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import java.util.ArrayList;

class GLES20DisplayList extends DisplayList
{
    final ArrayList<Bitmap> mBitmaps = new ArrayList(5);
    private GLES20RecordingCanvas mCanvas;
    private DisplayListFinalizer mFinalizer;
    private final String mName;
    private boolean mValid;

    GLES20DisplayList(String paramString)
    {
        this.mName = paramString;
    }

    private static native void nOffsetLeftRight(int paramInt1, int paramInt2);

    private static native void nOffsetTopBottom(int paramInt1, int paramInt2);

    private static native void nSetAlpha(int paramInt, float paramFloat);

    private static native void nSetAnimationMatrix(int paramInt1, int paramInt2);

    private static native void nSetBottom(int paramInt1, int paramInt2);

    private static native void nSetCaching(int paramInt, boolean paramBoolean);

    private static native void nSetCameraDistance(int paramInt, float paramFloat);

    private static native void nSetClipChildren(int paramInt, boolean paramBoolean);

    private static native void nSetHasOverlappingRendering(int paramInt, boolean paramBoolean);

    private static native void nSetLeft(int paramInt1, int paramInt2);

    private static native void nSetLeftTop(int paramInt1, int paramInt2, int paramInt3);

    private static native void nSetLeftTopRightBottom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private static native void nSetPivotX(int paramInt, float paramFloat);

    private static native void nSetPivotY(int paramInt, float paramFloat);

    private static native void nSetRight(int paramInt1, int paramInt2);

    private static native void nSetRotation(int paramInt, float paramFloat);

    private static native void nSetRotationX(int paramInt, float paramFloat);

    private static native void nSetRotationY(int paramInt, float paramFloat);

    private static native void nSetScaleX(int paramInt, float paramFloat);

    private static native void nSetScaleY(int paramInt, float paramFloat);

    private static native void nSetStaticMatrix(int paramInt1, int paramInt2);

    private static native void nSetTop(int paramInt1, int paramInt2);

    private static native void nSetTransformationInfo(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8);

    private static native void nSetTranslationX(int paramInt, float paramFloat);

    private static native void nSetTranslationY(int paramInt, float paramFloat);

    public void clear()
    {
        if (!this.mValid)
            this.mBitmaps.clear();
    }

    public void end()
    {
        if (this.mCanvas != null)
        {
            if (this.mFinalizer == null)
                break label47;
            this.mCanvas.end(this.mFinalizer.mNativeDisplayList);
        }
        while (true)
        {
            this.mCanvas.recycle();
            this.mCanvas = null;
            this.mValid = true;
            return;
            label47: this.mFinalizer = new DisplayListFinalizer(this.mCanvas.end(0));
            GLES20Canvas.setDisplayListName(this.mFinalizer.mNativeDisplayList, this.mName);
        }
    }

    int getNativeDisplayList()
    {
        if ((!this.mValid) || (this.mFinalizer == null))
            throw new IllegalStateException("The display list is not valid.");
        return this.mFinalizer.mNativeDisplayList;
    }

    public int getSize()
    {
        if (this.mFinalizer == null);
        for (int i = 0; ; i = GLES20Canvas.getDisplayListSize(this.mFinalizer.mNativeDisplayList))
            return i;
    }

    boolean hasNativeDisplayList()
    {
        if ((this.mValid) && (this.mFinalizer != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void invalidate()
    {
        if (this.mCanvas != null)
        {
            this.mCanvas.recycle();
            this.mCanvas = null;
        }
        this.mValid = false;
    }

    public boolean isValid()
    {
        return this.mValid;
    }

    public void offsetLeftRight(int paramInt)
    {
        if (hasNativeDisplayList())
            nOffsetLeftRight(this.mFinalizer.mNativeDisplayList, paramInt);
    }

    public void offsetTopBottom(int paramInt)
    {
        if (hasNativeDisplayList())
            nOffsetTopBottom(this.mFinalizer.mNativeDisplayList, paramInt);
    }

    public void setAlpha(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetAlpha(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setAnimationMatrix(Matrix paramMatrix)
    {
        int i;
        if (hasNativeDisplayList())
        {
            i = this.mFinalizer.mNativeDisplayList;
            if (paramMatrix == null)
                break label30;
        }
        label30: for (int j = paramMatrix.native_instance; ; j = 0)
        {
            nSetAnimationMatrix(i, j);
            return;
        }
    }

    public void setBottom(int paramInt)
    {
        if (hasNativeDisplayList())
            nSetBottom(this.mFinalizer.mNativeDisplayList, paramInt);
    }

    public void setCaching(boolean paramBoolean)
    {
        if (hasNativeDisplayList())
            nSetCaching(this.mFinalizer.mNativeDisplayList, paramBoolean);
    }

    public void setCameraDistance(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetCameraDistance(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setClipChildren(boolean paramBoolean)
    {
        if (hasNativeDisplayList())
            nSetClipChildren(this.mFinalizer.mNativeDisplayList, paramBoolean);
    }

    public void setHasOverlappingRendering(boolean paramBoolean)
    {
        if (hasNativeDisplayList())
            nSetHasOverlappingRendering(this.mFinalizer.mNativeDisplayList, paramBoolean);
    }

    public void setLeft(int paramInt)
    {
        if (hasNativeDisplayList())
            nSetLeft(this.mFinalizer.mNativeDisplayList, paramInt);
    }

    public void setLeftTop(int paramInt1, int paramInt2)
    {
        if (hasNativeDisplayList())
            nSetLeftTop(this.mFinalizer.mNativeDisplayList, paramInt1, paramInt2);
    }

    public void setLeftTopRightBottom(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (hasNativeDisplayList())
            nSetLeftTopRightBottom(this.mFinalizer.mNativeDisplayList, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void setPivotX(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetPivotX(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setPivotY(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetPivotY(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setRight(int paramInt)
    {
        if (hasNativeDisplayList())
            nSetRight(this.mFinalizer.mNativeDisplayList, paramInt);
    }

    public void setRotation(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetRotation(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setRotationX(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetRotationX(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setRotationY(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetRotationY(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setScaleX(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetScaleX(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setScaleY(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetScaleY(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setStaticMatrix(Matrix paramMatrix)
    {
        if (hasNativeDisplayList())
            nSetStaticMatrix(this.mFinalizer.mNativeDisplayList, paramMatrix.native_instance);
    }

    public void setTop(int paramInt)
    {
        if (hasNativeDisplayList())
            nSetTop(this.mFinalizer.mNativeDisplayList, paramInt);
    }

    public void setTransformationInfo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8)
    {
        if (hasNativeDisplayList())
            nSetTransformationInfo(this.mFinalizer.mNativeDisplayList, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8);
    }

    public void setTranslationX(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetTranslationX(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public void setTranslationY(float paramFloat)
    {
        if (hasNativeDisplayList())
            nSetTranslationY(this.mFinalizer.mNativeDisplayList, paramFloat);
    }

    public HardwareCanvas start()
    {
        if (this.mCanvas != null)
            throw new IllegalStateException("Recording has already started");
        this.mValid = false;
        this.mCanvas = GLES20RecordingCanvas.obtain(this);
        this.mCanvas.start();
        return this.mCanvas;
    }

    private static class DisplayListFinalizer
    {
        final int mNativeDisplayList;

        public DisplayListFinalizer(int paramInt)
        {
            this.mNativeDisplayList = paramInt;
        }

        protected void finalize()
            throws Throwable
        {
            try
            {
                GLES20Canvas.destroyDisplayList(this.mNativeDisplayList);
                return;
            }
            finally
            {
                super.finalize();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GLES20DisplayList
 * JD-Core Version:        0.6.2
 */