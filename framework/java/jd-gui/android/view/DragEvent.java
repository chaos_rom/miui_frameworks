package android.view;

import android.content.ClipData;
import android.content.ClipDescription;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class DragEvent
    implements Parcelable
{
    public static final int ACTION_DRAG_ENDED = 4;
    public static final int ACTION_DRAG_ENTERED = 5;
    public static final int ACTION_DRAG_EXITED = 6;
    public static final int ACTION_DRAG_LOCATION = 2;
    public static final int ACTION_DRAG_STARTED = 1;
    public static final int ACTION_DROP = 3;
    public static final Parcelable.Creator<DragEvent> CREATOR = new Parcelable.Creator()
    {
        public DragEvent createFromParcel(Parcel paramAnonymousParcel)
        {
            DragEvent localDragEvent = DragEvent.obtain();
            localDragEvent.mAction = paramAnonymousParcel.readInt();
            localDragEvent.mX = paramAnonymousParcel.readFloat();
            localDragEvent.mY = paramAnonymousParcel.readFloat();
            if (paramAnonymousParcel.readInt() != 0);
            for (boolean bool = true; ; bool = false)
            {
                localDragEvent.mDragResult = bool;
                if (paramAnonymousParcel.readInt() != 0)
                    localDragEvent.mClipData = ((ClipData)ClipData.CREATOR.createFromParcel(paramAnonymousParcel));
                if (paramAnonymousParcel.readInt() != 0)
                    localDragEvent.mClipDescription = ((ClipDescription)ClipDescription.CREATOR.createFromParcel(paramAnonymousParcel));
                return localDragEvent;
            }
        }

        public DragEvent[] newArray(int paramAnonymousInt)
        {
            return new DragEvent[paramAnonymousInt];
        }
    };
    private static final int MAX_RECYCLED = 10;
    private static final boolean TRACK_RECYCLED_LOCATION;
    private static final Object gRecyclerLock = new Object();
    private static DragEvent gRecyclerTop;
    private static int gRecyclerUsed = 0;
    int mAction;
    ClipData mClipData;
    ClipDescription mClipDescription;
    boolean mDragResult;
    Object mLocalState;
    private DragEvent mNext;
    private boolean mRecycled;
    private RuntimeException mRecycledLocation;
    float mX;
    float mY;

    static
    {
        gRecyclerTop = null;
    }

    private void init(int paramInt, float paramFloat1, float paramFloat2, ClipDescription paramClipDescription, ClipData paramClipData, Object paramObject, boolean paramBoolean)
    {
        this.mAction = paramInt;
        this.mX = paramFloat1;
        this.mY = paramFloat2;
        this.mClipDescription = paramClipDescription;
        this.mClipData = paramClipData;
        this.mLocalState = paramObject;
        this.mDragResult = paramBoolean;
    }

    static DragEvent obtain()
    {
        return obtain(0, 0.0F, 0.0F, null, null, null, false);
    }

    public static DragEvent obtain(int paramInt, float paramFloat1, float paramFloat2, Object paramObject, ClipDescription paramClipDescription, ClipData paramClipData, boolean paramBoolean)
    {
        Object localObject3;
        synchronized (gRecyclerLock)
        {
            if (gRecyclerTop == null)
            {
                DragEvent localDragEvent1 = new DragEvent();
                localDragEvent1.init(paramInt, paramFloat1, paramFloat2, paramClipDescription, paramClipData, paramObject, paramBoolean);
                localObject3 = localDragEvent1;
            }
            else
            {
                DragEvent localDragEvent2 = gRecyclerTop;
                gRecyclerTop = localDragEvent2.mNext;
                gRecyclerUsed = -1 + gRecyclerUsed;
                localDragEvent2.mRecycledLocation = null;
                localDragEvent2.mRecycled = false;
                localDragEvent2.mNext = null;
                localDragEvent2.init(paramInt, paramFloat1, paramFloat2, paramClipDescription, paramClipData, paramObject, paramBoolean);
                localObject3 = localDragEvent2;
            }
        }
        return localObject3;
    }

    public static DragEvent obtain(DragEvent paramDragEvent)
    {
        return obtain(paramDragEvent.mAction, paramDragEvent.mX, paramDragEvent.mY, paramDragEvent.mLocalState, paramDragEvent.mClipDescription, paramDragEvent.mClipData, paramDragEvent.mDragResult);
    }

    public int describeContents()
    {
        return 0;
    }

    public int getAction()
    {
        return this.mAction;
    }

    public ClipData getClipData()
    {
        return this.mClipData;
    }

    public ClipDescription getClipDescription()
    {
        return this.mClipDescription;
    }

    public Object getLocalState()
    {
        return this.mLocalState;
    }

    public boolean getResult()
    {
        return this.mDragResult;
    }

    public float getX()
    {
        return this.mX;
    }

    public float getY()
    {
        return this.mY;
    }

    public final void recycle()
    {
        if (this.mRecycled)
            throw new RuntimeException(toString() + " recycled twice!");
        this.mRecycled = true;
        this.mClipData = null;
        this.mClipDescription = null;
        this.mLocalState = null;
        synchronized (gRecyclerLock)
        {
            if (gRecyclerUsed < 10)
            {
                gRecyclerUsed = 1 + gRecyclerUsed;
                this.mNext = gRecyclerTop;
                gRecyclerTop = this;
            }
            return;
        }
    }

    public String toString()
    {
        return "DragEvent{" + Integer.toHexString(System.identityHashCode(this)) + " action=" + this.mAction + " @ (" + this.mX + ", " + this.mY + ") desc=" + this.mClipDescription + " data=" + this.mClipData + " local=" + this.mLocalState + " result=" + this.mDragResult + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mAction);
        paramParcel.writeFloat(this.mX);
        paramParcel.writeFloat(this.mY);
        int i;
        if (this.mDragResult)
        {
            i = 1;
            paramParcel.writeInt(i);
            if (this.mClipData != null)
                break label68;
            paramParcel.writeInt(0);
            label50: if (this.mClipDescription != null)
                break label85;
            paramParcel.writeInt(0);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label68: paramParcel.writeInt(1);
            this.mClipData.writeToParcel(paramParcel, paramInt);
            break label50;
            label85: paramParcel.writeInt(1);
            this.mClipDescription.writeToParcel(paramParcel, paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.DragEvent
 * JD-Core Version:        0.6.2
 */