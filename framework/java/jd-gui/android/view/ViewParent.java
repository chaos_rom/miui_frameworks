package android.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.accessibility.AccessibilityEvent;

public abstract interface ViewParent
{
    public abstract void bringChildToFront(View paramView);

    public abstract void childAccessibilityStateChanged(View paramView);

    public abstract void childDrawableStateChanged(View paramView);

    public abstract void childHasTransientStateChanged(View paramView, boolean paramBoolean);

    public abstract void clearChildFocus(View paramView);

    public abstract void createContextMenu(ContextMenu paramContextMenu);

    public abstract View findViewToTakeAccessibilityFocusFromHover(View paramView1, View paramView2);

    public abstract View focusSearch(View paramView, int paramInt);

    public abstract void focusableViewAvailable(View paramView);

    public abstract boolean getChildVisibleRect(View paramView, Rect paramRect, Point paramPoint);

    public abstract ViewParent getParent();

    public abstract ViewParent getParentForAccessibility();

    public abstract void invalidateChild(View paramView, Rect paramRect);

    public abstract ViewParent invalidateChildInParent(int[] paramArrayOfInt, Rect paramRect);

    public abstract boolean isLayoutRequested();

    public abstract void recomputeViewAttributes(View paramView);

    public abstract void requestChildFocus(View paramView1, View paramView2);

    public abstract boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean);

    public abstract void requestDisallowInterceptTouchEvent(boolean paramBoolean);

    public abstract void requestFitSystemWindows();

    public abstract void requestLayout();

    public abstract boolean requestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent);

    public abstract void requestTransparentRegion(View paramView);

    public abstract boolean showContextMenuForChild(View paramView);

    public abstract ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewParent
 * JD-Core Version:        0.6.2
 */