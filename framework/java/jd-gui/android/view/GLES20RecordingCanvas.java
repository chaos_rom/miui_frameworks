package android.view;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas.VertexMode;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.Pool;
import android.util.Poolable;
import android.util.PoolableManager;
import android.util.Pools;
import java.util.ArrayList;

class GLES20RecordingCanvas extends GLES20Canvas
    implements Poolable<GLES20RecordingCanvas>
{
    private static final int POOL_LIMIT = 25;
    private static final Pool<GLES20RecordingCanvas> sPool = Pools.synchronizedPool(Pools.finitePool(new PoolableManager()
    {
        public GLES20RecordingCanvas newInstance()
        {
            return new GLES20RecordingCanvas(null);
        }

        public void onAcquired(GLES20RecordingCanvas paramAnonymousGLES20RecordingCanvas)
        {
        }

        public void onReleased(GLES20RecordingCanvas paramAnonymousGLES20RecordingCanvas)
        {
        }
    }
    , 25));
    private GLES20DisplayList mDisplayList;
    private boolean mIsPooled;
    private GLES20RecordingCanvas mNextPoolable;

    private GLES20RecordingCanvas()
    {
        super(true, true);
    }

    static GLES20RecordingCanvas obtain(GLES20DisplayList paramGLES20DisplayList)
    {
        GLES20RecordingCanvas localGLES20RecordingCanvas = (GLES20RecordingCanvas)sPool.acquire();
        localGLES20RecordingCanvas.mDisplayList = paramGLES20DisplayList;
        return localGLES20RecordingCanvas;
    }

    private void recordShaderBitmap(Paint paramPaint)
    {
        if (paramPaint != null)
        {
            Shader localShader = paramPaint.getShader();
            if ((localShader instanceof BitmapShader))
                this.mDisplayList.mBitmaps.add(((BitmapShader)localShader).mBitmap);
        }
    }

    public void drawBitmap(Bitmap paramBitmap, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawBitmap(paramBitmap, paramFloat1, paramFloat2, paramPaint);
        this.mDisplayList.mBitmaps.add(paramBitmap);
    }

    public void drawBitmap(Bitmap paramBitmap, Matrix paramMatrix, Paint paramPaint)
    {
        super.drawBitmap(paramBitmap, paramMatrix, paramPaint);
        this.mDisplayList.mBitmaps.add(paramBitmap);
    }

    public void drawBitmap(Bitmap paramBitmap, Rect paramRect1, Rect paramRect2, Paint paramPaint)
    {
        super.drawBitmap(paramBitmap, paramRect1, paramRect2, paramPaint);
        this.mDisplayList.mBitmaps.add(paramBitmap);
    }

    public void drawBitmap(Bitmap paramBitmap, Rect paramRect, RectF paramRectF, Paint paramPaint)
    {
        super.drawBitmap(paramBitmap, paramRect, paramRectF, paramPaint);
        this.mDisplayList.mBitmaps.add(paramBitmap);
    }

    public void drawBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4, boolean paramBoolean, Paint paramPaint)
    {
        super.drawBitmap(paramArrayOfInt, paramInt1, paramInt2, paramFloat1, paramFloat2, paramInt3, paramInt4, paramBoolean, paramPaint);
    }

    public void drawBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, Paint paramPaint)
    {
        super.drawBitmap(paramArrayOfInt, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBoolean, paramPaint);
    }

    public void drawBitmapMesh(Bitmap paramBitmap, int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3, int[] paramArrayOfInt, int paramInt4, Paint paramPaint)
    {
        super.drawBitmapMesh(paramBitmap, paramInt1, paramInt2, paramArrayOfFloat, paramInt3, paramArrayOfInt, paramInt4, paramPaint);
        this.mDisplayList.mBitmaps.add(paramBitmap);
    }

    public void drawCircle(float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint)
    {
        super.drawCircle(paramFloat1, paramFloat2, paramFloat3, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawLine(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint)
    {
        super.drawLine(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawLines(float[] paramArrayOfFloat, int paramInt1, int paramInt2, Paint paramPaint)
    {
        super.drawLines(paramArrayOfFloat, paramInt1, paramInt2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawLines(float[] paramArrayOfFloat, Paint paramPaint)
    {
        super.drawLines(paramArrayOfFloat, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawOval(RectF paramRectF, Paint paramPaint)
    {
        super.drawOval(paramRectF, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPaint(Paint paramPaint)
    {
        super.drawPaint(paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPatch(Bitmap paramBitmap, byte[] paramArrayOfByte, RectF paramRectF, Paint paramPaint)
    {
        super.drawPatch(paramBitmap, paramArrayOfByte, paramRectF, paramPaint);
        this.mDisplayList.mBitmaps.add(paramBitmap);
    }

    public void drawPath(Path paramPath, Paint paramPaint)
    {
        super.drawPath(paramPath, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPoint(float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawPoint(paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPoints(float[] paramArrayOfFloat, int paramInt1, int paramInt2, Paint paramPaint)
    {
        super.drawPoints(paramArrayOfFloat, paramInt1, paramInt2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPoints(float[] paramArrayOfFloat, Paint paramPaint)
    {
        super.drawPoints(paramArrayOfFloat, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPosText(String paramString, float[] paramArrayOfFloat, Paint paramPaint)
    {
        super.drawPosText(paramString, paramArrayOfFloat, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawPosText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float[] paramArrayOfFloat, Paint paramPaint)
    {
        super.drawPosText(paramArrayOfChar, paramInt1, paramInt2, paramArrayOfFloat, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint)
    {
        super.drawRect(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawRect(Rect paramRect, Paint paramPaint)
    {
        super.drawRect(paramRect, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawRect(RectF paramRectF, Paint paramPaint)
    {
        super.drawRect(paramRectF, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawRoundRect(RectF paramRectF, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawRoundRect(paramRectF, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawText(CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawText(paramCharSequence, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawText(String paramString, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawText(paramString, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawText(String paramString, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawText(paramString, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawText(paramArrayOfChar, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawTextOnPath(String paramString, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawTextOnPath(paramString, paramPath, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawTextOnPath(char[] paramArrayOfChar, int paramInt1, int paramInt2, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        super.drawTextOnPath(paramArrayOfChar, paramInt1, paramInt2, paramPath, paramFloat1, paramFloat2, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawTextRun(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        super.drawTextRun(paramCharSequence, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramInt5, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawTextRun(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        super.drawTextRun(paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramInt5, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    public void drawVertices(Canvas.VertexMode paramVertexMode, int paramInt1, float[] paramArrayOfFloat1, int paramInt2, float[] paramArrayOfFloat2, int paramInt3, int[] paramArrayOfInt, int paramInt4, short[] paramArrayOfShort, int paramInt5, int paramInt6, Paint paramPaint)
    {
        super.drawVertices(paramVertexMode, paramInt1, paramArrayOfFloat1, paramInt2, paramArrayOfFloat2, paramInt3, paramArrayOfInt, paramInt4, paramArrayOfShort, paramInt5, paramInt6, paramPaint);
        recordShaderBitmap(paramPaint);
    }

    int end(int paramInt)
    {
        return getDisplayList(paramInt);
    }

    public GLES20RecordingCanvas getNextPoolable()
    {
        return this.mNextPoolable;
    }

    public boolean isPooled()
    {
        return this.mIsPooled;
    }

    void recycle()
    {
        this.mDisplayList = null;
        resetDisplayListRenderer();
        sPool.release(this);
    }

    public void setNextPoolable(GLES20RecordingCanvas paramGLES20RecordingCanvas)
    {
        this.mNextPoolable = paramGLES20RecordingCanvas;
    }

    public void setPooled(boolean paramBoolean)
    {
        this.mIsPooled = paramBoolean;
    }

    void start()
    {
        this.mDisplayList.mBitmaps.clear();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GLES20RecordingCanvas
 * JD-Core Version:        0.6.2
 */