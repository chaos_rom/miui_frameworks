package android.view;

import android.content.Context;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.IBinder;
import android.os.LocalPowerManager;
import android.os.Looper;
import android.view.animation.Animation;
import java.io.PrintWriter;

public abstract interface WindowManagerPolicy
{
    public static final int ACTION_GO_TO_SLEEP = 4;
    public static final String ACTION_HDMI_PLUGGED = "android.intent.action.HDMI_PLUGGED";
    public static final int ACTION_PASS_TO_USER = 1;
    public static final int ACTION_POKE_USER_ACTIVITY = 2;
    public static final String EXTRA_HDMI_PLUGGED_STATE = "state";
    public static final int FINISH_LAYOUT_REDO_ANIM = 8;
    public static final int FINISH_LAYOUT_REDO_CONFIG = 2;
    public static final int FINISH_LAYOUT_REDO_LAYOUT = 1;
    public static final int FINISH_LAYOUT_REDO_WALLPAPER = 4;
    public static final int FLAG_ALT = 16;
    public static final int FLAG_ALT_GR = 32;
    public static final int FLAG_BRIGHT_HERE = 536870912;
    public static final int FLAG_CAPS_LOCK = 8;
    public static final int FLAG_DISABLE_KEY_REPEAT = 134217728;
    public static final int FLAG_FILTERED = 67108864;
    public static final int FLAG_INJECTED = 16777216;
    public static final int FLAG_LAUNCHER = 128;
    public static final int FLAG_MENU = 64;
    public static final int FLAG_PASS_TO_USER = 1073741824;
    public static final int FLAG_SHIFT = 4;
    public static final int FLAG_TRUSTED = 33554432;
    public static final int FLAG_VIRTUAL = 256;
    public static final int FLAG_WAKE = 1;
    public static final int FLAG_WAKE_DROPPED = 2;
    public static final int FLAG_WOKE_HERE = 268435456;
    public static final int OFF_BECAUSE_OF_ADMIN = 1;
    public static final int OFF_BECAUSE_OF_PROX_SENSOR = 4;
    public static final int OFF_BECAUSE_OF_TIMEOUT = 3;
    public static final int OFF_BECAUSE_OF_USER = 2;
    public static final int PRESENCE_EXTERNAL = 2;
    public static final int PRESENCE_INTERNAL = 1;
    public static final int TRANSIT_ACTIVITY_CLOSE = 8199;
    public static final int TRANSIT_ACTIVITY_OPEN = 4102;
    public static final int TRANSIT_ENTER = 4097;
    public static final int TRANSIT_ENTER_MASK = 4096;
    public static final int TRANSIT_EXIT = 8194;
    public static final int TRANSIT_EXIT_MASK = 8192;
    public static final int TRANSIT_HIDE = 8196;
    public static final int TRANSIT_NONE = 0;
    public static final int TRANSIT_PREVIEW_DONE = 5;
    public static final int TRANSIT_SHOW = 4099;
    public static final int TRANSIT_TASK_CLOSE = 8201;
    public static final int TRANSIT_TASK_OPEN = 4104;
    public static final int TRANSIT_TASK_TO_BACK = 8203;
    public static final int TRANSIT_TASK_TO_FRONT = 4106;
    public static final int TRANSIT_UNSET = -1;
    public static final int TRANSIT_WALLPAPER_CLOSE = 8204;
    public static final int TRANSIT_WALLPAPER_INTRA_CLOSE = 8207;
    public static final int TRANSIT_WALLPAPER_INTRA_OPEN = 4110;
    public static final int TRANSIT_WALLPAPER_OPEN = 4109;
    public static final int USER_ROTATION_FREE = 0;
    public static final int USER_ROTATION_LOCKED = 1;
    public static final boolean WATCH_POINTER;

    public abstract View addStartingWindow(IBinder paramIBinder, String paramString, int paramInt1, CompatibilityInfo paramCompatibilityInfo, CharSequence paramCharSequence, int paramInt2, int paramInt3, int paramInt4);

    public abstract void adjustConfigurationLw(Configuration paramConfiguration, int paramInt1, int paramInt2);

    public abstract int adjustSystemUiVisibilityLw(int paramInt);

    public abstract void adjustWindowParamsLw(WindowManager.LayoutParams paramLayoutParams);

    public abstract boolean allowAppAnimationsLw();

    public abstract boolean allowKeyRepeat();

    public abstract void animatingWindowLw(WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams);

    public abstract void beginAnimationLw(int paramInt1, int paramInt2);

    public abstract void beginLayoutLw(int paramInt1, int paramInt2, int paramInt3);

    public abstract boolean canBeForceHidden(WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams);

    public abstract int checkAddPermission(WindowManager.LayoutParams paramLayoutParams);

    public abstract Animation createForceHideEnterAnimation(boolean paramBoolean);

    public abstract void dismissKeyguardLw();

    public abstract KeyEvent dispatchUnhandledKey(WindowState paramWindowState, KeyEvent paramKeyEvent, int paramInt);

    public abstract boolean doesForceHide(WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams);

    public abstract void dump(String paramString, PrintWriter paramPrintWriter, String[] paramArrayOfString);

    public abstract void enableKeyguard(boolean paramBoolean);

    public abstract void enableScreenAfterBoot();

    public abstract void exitKeyguardSecurely(OnKeyguardExitResult paramOnKeyguardExitResult);

    public abstract int finishAnimationLw();

    public abstract void finishLayoutLw();

    public abstract int focusChangedLw(WindowState paramWindowState1, WindowState paramWindowState2);

    public abstract int getConfigDisplayHeight(int paramInt1, int paramInt2, int paramInt3);

    public abstract int getConfigDisplayWidth(int paramInt1, int paramInt2, int paramInt3);

    public abstract void getContentInsetHintLw(WindowManager.LayoutParams paramLayoutParams, Rect paramRect);

    public abstract int getMaxWallpaperLayer();

    public abstract int getNonDecorDisplayHeight(int paramInt1, int paramInt2, int paramInt3);

    public abstract int getNonDecorDisplayWidth(int paramInt1, int paramInt2, int paramInt3);

    public abstract int getSystemDecorRectLw(Rect paramRect);

    public abstract boolean hasNavigationBar();

    public abstract boolean hasSystemNavBar();

    public abstract void hideBootMessages();

    public abstract boolean inKeyguardRestrictedKeyInputMode();

    public abstract void init(Context paramContext, IWindowManager paramIWindowManager, WindowManagerFuncs paramWindowManagerFuncs, LocalPowerManager paramLocalPowerManager);

    public abstract long interceptKeyBeforeDispatching(WindowState paramWindowState, KeyEvent paramKeyEvent, int paramInt);

    public abstract int interceptKeyBeforeQueueing(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean);

    public abstract int interceptMotionBeforeQueueingWhenScreenOff(int paramInt);

    public abstract boolean isKeyguardLocked();

    public abstract boolean isKeyguardSecure();

    public abstract boolean isScreenOnEarly();

    public abstract boolean isScreenOnFully();

    public abstract boolean isScreenSaverEnabled();

    public abstract void layoutWindowLw(WindowState paramWindowState1, WindowManager.LayoutParams paramLayoutParams, WindowState paramWindowState2);

    public abstract void lockNow();

    public abstract void notifyLidSwitchChanged(long paramLong, boolean paramBoolean);

    public abstract boolean performHapticFeedbackLw(WindowState paramWindowState, int paramInt, boolean paramBoolean);

    public abstract int prepareAddWindowLw(WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams);

    public abstract void removeStartingWindow(IBinder paramIBinder, View paramView);

    public abstract void removeWindowLw(WindowState paramWindowState);

    public abstract int rotationForOrientationLw(int paramInt1, int paramInt2);

    public abstract boolean rotationHasCompatibleMetricsLw(int paramInt1, int paramInt2);

    public abstract void screenOnStartedLw();

    public abstract void screenOnStoppedLw();

    public abstract void screenTurnedOff(int paramInt);

    public abstract void screenTurningOn(ScreenOnListener paramScreenOnListener);

    public abstract int selectAnimationLw(WindowState paramWindowState, int paramInt);

    public abstract void setCurrentOrientationLw(int paramInt);

    public abstract void setInitialDisplaySize(Display paramDisplay, int paramInt1, int paramInt2);

    public abstract void setLastInputMethodWindowLw(WindowState paramWindowState1, WindowState paramWindowState2);

    public abstract void setRotationLw(int paramInt);

    public abstract void setSafeMode(boolean paramBoolean);

    public abstract void setUserRotationMode(int paramInt1, int paramInt2);

    public abstract void showBootMessage(CharSequence paramCharSequence, boolean paramBoolean);

    public abstract boolean startScreenSaver();

    public abstract void stopScreenSaver();

    public abstract int subWindowTypeToLayerLw(int paramInt);

    public abstract void systemBooted();

    public abstract void systemReady();

    public abstract void userActivity();

    public abstract int windowTypeToLayerLw(int paramInt);

    public static abstract interface OnKeyguardExitResult
    {
        public abstract void onKeyguardExitResult(boolean paramBoolean);
    }

    public static abstract interface ScreenOnListener
    {
        public abstract void onScreenOn();
    }

    public static abstract interface WindowManagerFuncs
    {
        public static final int LID_ABSENT = -1;
        public static final int LID_CLOSED = 0;
        public static final int LID_OPEN = 1;

        public abstract WindowManagerPolicy.FakeWindow addFakeWindow(Looper paramLooper, InputEventReceiver.Factory paramFactory, String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);

        public abstract int getLidState();

        public abstract InputChannel monitorInput(String paramString);

        public abstract void rebootSafeMode();

        public abstract void reevaluateStatusBarVisibility();

        public abstract void shutdown();

        public abstract void switchKeyboardLayout(int paramInt1, int paramInt2);
    }

    public static abstract interface FakeWindow
    {
        public abstract void dismiss();
    }

    public static abstract interface WindowState
    {
        public abstract void computeFrameLw(Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4);

        public abstract IApplicationToken getAppToken();

        public abstract WindowManager.LayoutParams getAttrs();

        public abstract Rect getContentFrameLw();

        public abstract Rect getDisplayFrameLw();

        public abstract Rect getFrameLw();

        public abstract Rect getGivenContentInsetsLw();

        public abstract boolean getGivenInsetsPendingLw();

        public abstract Rect getGivenVisibleInsetsLw();

        public abstract boolean getNeedsMenuLw(WindowState paramWindowState);

        public abstract RectF getShownFrameLw();

        public abstract int getSurfaceLayer();

        public abstract int getSystemUiVisibility();

        public abstract Rect getVisibleFrameLw();

        public abstract boolean hasAppShownWindows();

        public abstract boolean hasDrawnLw();

        public abstract boolean hideLw(boolean paramBoolean);

        public abstract boolean isAlive();

        public abstract boolean isAnimatingLw();

        public abstract boolean isDisplayedLw();

        public abstract boolean isGoneForLayoutLw();

        public abstract boolean isVisibleLw();

        public abstract boolean isVisibleOrBehindKeyguardLw();

        public abstract boolean showLw(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.WindowManagerPolicy
 * JD-Core Version:        0.6.2
 */