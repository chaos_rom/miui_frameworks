package android.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

public class GestureDetector
{
    private static final int DOUBLE_TAP_TIMEOUT = 0;
    private static final int LONGPRESS_TIMEOUT = 0;
    private static final int LONG_PRESS = 2;
    private static final int SHOW_PRESS = 1;
    private static final int TAP = 3;
    private static final int TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
    private boolean mAlwaysInBiggerTapRegion;
    private boolean mAlwaysInTapRegion;
    private MotionEvent mCurrentDownEvent;
    private OnDoubleTapListener mDoubleTapListener;
    private int mDoubleTapSlopSquare;
    private int mDoubleTapTouchSlopSquare;
    private float mDownFocusX;
    private float mDownFocusY;
    private final Handler mHandler;
    private boolean mInLongPress;
    private final InputEventConsistencyVerifier mInputEventConsistencyVerifier;
    private boolean mIsDoubleTapping;
    private boolean mIsLongpressEnabled;
    private float mLastFocusX;
    private float mLastFocusY;
    private final OnGestureListener mListener;
    private int mMaximumFlingVelocity;
    private int mMinimumFlingVelocity;
    private MotionEvent mPreviousUpEvent;
    private boolean mStillDown;
    private int mTouchSlopSquare;
    private VelocityTracker mVelocityTracker;

    public GestureDetector(Context paramContext, OnGestureListener paramOnGestureListener)
    {
        this(paramContext, paramOnGestureListener, null);
    }

    public GestureDetector(Context paramContext, OnGestureListener paramOnGestureListener, Handler paramHandler)
    {
        InputEventConsistencyVerifier localInputEventConsistencyVerifier;
        if (InputEventConsistencyVerifier.isInstrumentationEnabled())
        {
            localInputEventConsistencyVerifier = new InputEventConsistencyVerifier(this, 0);
            this.mInputEventConsistencyVerifier = localInputEventConsistencyVerifier;
            if (paramHandler == null)
                break label76;
        }
        label76: for (this.mHandler = new GestureHandler(paramHandler); ; this.mHandler = new GestureHandler())
        {
            this.mListener = paramOnGestureListener;
            if ((paramOnGestureListener instanceof OnDoubleTapListener))
                setOnDoubleTapListener((OnDoubleTapListener)paramOnGestureListener);
            init(paramContext);
            return;
            localInputEventConsistencyVerifier = null;
            break;
        }
    }

    public GestureDetector(Context paramContext, OnGestureListener paramOnGestureListener, Handler paramHandler, boolean paramBoolean)
    {
        this(paramContext, paramOnGestureListener, paramHandler);
    }

    @Deprecated
    public GestureDetector(OnGestureListener paramOnGestureListener)
    {
        this(null, paramOnGestureListener, null);
    }

    @Deprecated
    public GestureDetector(OnGestureListener paramOnGestureListener, Handler paramHandler)
    {
        this(null, paramOnGestureListener, paramHandler);
    }

    private void cancel()
    {
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
        this.mHandler.removeMessages(3);
        this.mVelocityTracker.recycle();
        this.mVelocityTracker = null;
        this.mIsDoubleTapping = false;
        this.mStillDown = false;
        this.mAlwaysInTapRegion = false;
        this.mAlwaysInBiggerTapRegion = false;
        if (this.mInLongPress)
            this.mInLongPress = false;
    }

    private void cancelTaps()
    {
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
        this.mHandler.removeMessages(3);
        this.mIsDoubleTapping = false;
        this.mAlwaysInTapRegion = false;
        this.mAlwaysInBiggerTapRegion = false;
        if (this.mInLongPress)
            this.mInLongPress = false;
    }

    private void dispatchLongPress()
    {
        this.mHandler.removeMessages(3);
        this.mInLongPress = true;
        this.mListener.onLongPress(this.mCurrentDownEvent);
    }

    private void init(Context paramContext)
    {
        if (this.mListener == null)
            throw new NullPointerException("OnGestureListener must not be null");
        this.mIsLongpressEnabled = true;
        int i;
        int j;
        int k;
        if (paramContext == null)
        {
            i = ViewConfiguration.getTouchSlop();
            j = i;
            k = ViewConfiguration.getDoubleTapSlop();
            this.mMinimumFlingVelocity = ViewConfiguration.getMinimumFlingVelocity();
        }
        ViewConfiguration localViewConfiguration;
        for (this.mMaximumFlingVelocity = ViewConfiguration.getMaximumFlingVelocity(); ; this.mMaximumFlingVelocity = localViewConfiguration.getScaledMaximumFlingVelocity())
        {
            this.mTouchSlopSquare = (i * i);
            this.mDoubleTapTouchSlopSquare = (j * j);
            this.mDoubleTapSlopSquare = (k * k);
            return;
            localViewConfiguration = ViewConfiguration.get(paramContext);
            i = localViewConfiguration.getScaledTouchSlop();
            j = localViewConfiguration.getScaledDoubleTapTouchSlop();
            k = localViewConfiguration.getScaledDoubleTapSlop();
            this.mMinimumFlingVelocity = localViewConfiguration.getScaledMinimumFlingVelocity();
        }
    }

    private boolean isConsideredDoubleTap(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, MotionEvent paramMotionEvent3)
    {
        boolean bool = false;
        if (!this.mAlwaysInBiggerTapRegion);
        while (true)
        {
            return bool;
            if (paramMotionEvent3.getEventTime() - paramMotionEvent2.getEventTime() <= DOUBLE_TAP_TIMEOUT)
            {
                int i = (int)paramMotionEvent1.getX() - (int)paramMotionEvent3.getX();
                int j = (int)paramMotionEvent1.getY() - (int)paramMotionEvent3.getY();
                if (i * i + j * j < this.mDoubleTapSlopSquare)
                    bool = true;
            }
        }
    }

    public boolean isLongpressEnabled()
    {
        return this.mIsLongpressEnabled;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mInputEventConsistencyVerifier != null)
            this.mInputEventConsistencyVerifier.onTouchEvent(paramMotionEvent, 0);
        int i = paramMotionEvent.getAction();
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        int j;
        int k;
        label65: float f1;
        float f2;
        int m;
        int n;
        if ((i & 0xFF) == 6)
        {
            j = 1;
            if (j == 0)
                break label105;
            k = paramMotionEvent.getActionIndex();
            f1 = 0.0F;
            f2 = 0.0F;
            m = paramMotionEvent.getPointerCount();
            n = 0;
            label80: if (n >= m)
                break label137;
            if (k != n)
                break label112;
        }
        while (true)
        {
            n++;
            break label80;
            j = 0;
            break;
            label105: k = -1;
            break label65;
            label112: f1 += paramMotionEvent.getX(n);
            f2 += paramMotionEvent.getY(n);
        }
        label137: int i1;
        float f3;
        float f4;
        boolean bool1;
        if (j != 0)
        {
            i1 = m - 1;
            f3 = f1 / i1;
            f4 = f2 / i1;
            bool1 = false;
            switch (i & 0xFF)
            {
            case 4:
            default:
            case 5:
            case 6:
            case 0:
            case 2:
            case 1:
            case 3:
            }
        }
        while (true)
        {
            if ((!bool1) && (this.mInputEventConsistencyVerifier != null))
                this.mInputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, 0);
            return bool1;
            i1 = m;
            break;
            this.mLastFocusX = f3;
            this.mDownFocusX = f3;
            this.mLastFocusY = f4;
            this.mDownFocusY = f4;
            cancelTaps();
            continue;
            this.mLastFocusX = f3;
            this.mDownFocusX = f3;
            this.mLastFocusY = f4;
            this.mDownFocusY = f4;
            continue;
            if (this.mDoubleTapListener != null)
            {
                boolean bool2 = this.mHandler.hasMessages(3);
                if (bool2)
                    this.mHandler.removeMessages(3);
                if ((this.mCurrentDownEvent == null) || (this.mPreviousUpEvent == null) || (!bool2) || (!isConsideredDoubleTap(this.mCurrentDownEvent, this.mPreviousUpEvent, paramMotionEvent)))
                    break label545;
                this.mIsDoubleTapping = true;
                bool1 = false | this.mDoubleTapListener.onDoubleTap(this.mCurrentDownEvent) | this.mDoubleTapListener.onDoubleTapEvent(paramMotionEvent);
            }
            while (true)
            {
                this.mLastFocusX = f3;
                this.mDownFocusX = f3;
                this.mLastFocusY = f4;
                this.mDownFocusY = f4;
                if (this.mCurrentDownEvent != null)
                    this.mCurrentDownEvent.recycle();
                this.mCurrentDownEvent = MotionEvent.obtain(paramMotionEvent);
                this.mAlwaysInTapRegion = true;
                this.mAlwaysInBiggerTapRegion = true;
                this.mStillDown = true;
                this.mInLongPress = false;
                if (this.mIsLongpressEnabled)
                {
                    this.mHandler.removeMessages(2);
                    this.mHandler.sendEmptyMessageAtTime(2, this.mCurrentDownEvent.getDownTime() + TAP_TIMEOUT + LONGPRESS_TIMEOUT);
                }
                this.mHandler.sendEmptyMessageAtTime(1, this.mCurrentDownEvent.getDownTime() + TAP_TIMEOUT);
                bool1 |= this.mListener.onDown(paramMotionEvent);
                break;
                label545: this.mHandler.sendEmptyMessageDelayed(3, DOUBLE_TAP_TIMEOUT);
            }
            if (!this.mInLongPress)
            {
                float f7 = this.mLastFocusX - f3;
                float f8 = this.mLastFocusY - f4;
                if (this.mIsDoubleTapping)
                {
                    bool1 = false | this.mDoubleTapListener.onDoubleTapEvent(paramMotionEvent);
                }
                else if (this.mAlwaysInTapRegion)
                {
                    int i3 = (int)(f3 - this.mDownFocusX);
                    int i4 = (int)(f4 - this.mDownFocusY);
                    int i5 = i3 * i3 + i4 * i4;
                    if (i5 > this.mTouchSlopSquare)
                    {
                        bool1 = this.mListener.onScroll(this.mCurrentDownEvent, paramMotionEvent, f7, f8);
                        this.mLastFocusX = f3;
                        this.mLastFocusY = f4;
                        this.mAlwaysInTapRegion = false;
                        this.mHandler.removeMessages(3);
                        this.mHandler.removeMessages(1);
                        this.mHandler.removeMessages(2);
                    }
                    if (i5 > this.mDoubleTapTouchSlopSquare)
                        this.mAlwaysInBiggerTapRegion = false;
                }
                else if ((Math.abs(f7) >= 1.0F) || (Math.abs(f8) >= 1.0F))
                {
                    bool1 = this.mListener.onScroll(this.mCurrentDownEvent, paramMotionEvent, f7, f8);
                    this.mLastFocusX = f3;
                    this.mLastFocusY = f4;
                    continue;
                    this.mStillDown = false;
                    MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
                    if (this.mIsDoubleTapping)
                        bool1 = false | this.mDoubleTapListener.onDoubleTapEvent(paramMotionEvent);
                    while (true)
                    {
                        if (this.mPreviousUpEvent != null)
                            this.mPreviousUpEvent.recycle();
                        this.mPreviousUpEvent = localMotionEvent;
                        if (this.mVelocityTracker != null)
                        {
                            this.mVelocityTracker.recycle();
                            this.mVelocityTracker = null;
                        }
                        this.mIsDoubleTapping = false;
                        this.mHandler.removeMessages(1);
                        this.mHandler.removeMessages(2);
                        break;
                        if (this.mInLongPress)
                        {
                            this.mHandler.removeMessages(3);
                            this.mInLongPress = false;
                        }
                        else if (this.mAlwaysInTapRegion)
                        {
                            bool1 = this.mListener.onSingleTapUp(paramMotionEvent);
                        }
                        else
                        {
                            VelocityTracker localVelocityTracker = this.mVelocityTracker;
                            int i2 = paramMotionEvent.getPointerId(0);
                            localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumFlingVelocity);
                            float f5 = localVelocityTracker.getYVelocity(i2);
                            float f6 = localVelocityTracker.getXVelocity(i2);
                            if ((Math.abs(f5) > this.mMinimumFlingVelocity) || (Math.abs(f6) > this.mMinimumFlingVelocity))
                                bool1 = this.mListener.onFling(this.mCurrentDownEvent, paramMotionEvent, f6, f5);
                        }
                    }
                    cancel();
                }
            }
        }
    }

    public void setIsLongpressEnabled(boolean paramBoolean)
    {
        this.mIsLongpressEnabled = paramBoolean;
    }

    public void setOnDoubleTapListener(OnDoubleTapListener paramOnDoubleTapListener)
    {
        this.mDoubleTapListener = paramOnDoubleTapListener;
    }

    private class GestureHandler extends Handler
    {
        GestureHandler()
        {
        }

        GestureHandler(Handler arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                throw new RuntimeException("Unknown message " + paramMessage);
            case 1:
                GestureDetector.this.mListener.onShowPress(GestureDetector.this.mCurrentDownEvent);
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                GestureDetector.this.dispatchLongPress();
                continue;
                if ((GestureDetector.this.mDoubleTapListener != null) && (!GestureDetector.this.mStillDown))
                    GestureDetector.this.mDoubleTapListener.onSingleTapConfirmed(GestureDetector.this.mCurrentDownEvent);
            }
        }
    }

    public static class SimpleOnGestureListener
        implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener
    {
        public boolean onDoubleTap(MotionEvent paramMotionEvent)
        {
            return false;
        }

        public boolean onDoubleTapEvent(MotionEvent paramMotionEvent)
        {
            return false;
        }

        public boolean onDown(MotionEvent paramMotionEvent)
        {
            return false;
        }

        public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
        {
            return false;
        }

        public void onLongPress(MotionEvent paramMotionEvent)
        {
        }

        public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
        {
            return false;
        }

        public void onShowPress(MotionEvent paramMotionEvent)
        {
        }

        public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
        {
            return false;
        }

        public boolean onSingleTapUp(MotionEvent paramMotionEvent)
        {
            return false;
        }
    }

    public static abstract interface OnDoubleTapListener
    {
        public abstract boolean onDoubleTap(MotionEvent paramMotionEvent);

        public abstract boolean onDoubleTapEvent(MotionEvent paramMotionEvent);

        public abstract boolean onSingleTapConfirmed(MotionEvent paramMotionEvent);
    }

    public static abstract interface OnGestureListener
    {
        public abstract boolean onDown(MotionEvent paramMotionEvent);

        public abstract boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2);

        public abstract void onLongPress(MotionEvent paramMotionEvent);

        public abstract boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2);

        public abstract void onShowPress(MotionEvent paramMotionEvent);

        public abstract boolean onSingleTapUp(MotionEvent paramMotionEvent);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GestureDetector
 * JD-Core Version:        0.6.2
 */