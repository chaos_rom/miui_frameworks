package android.view;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

class GLES20RenderLayer extends GLES20Layer
{
    private final GLES20Canvas mCanvas;
    private int mLayerHeight;
    private int mLayerWidth;

    GLES20RenderLayer(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        super(paramInt1, paramInt2, paramBoolean);
        int[] arrayOfInt = new int[2];
        this.mLayer = GLES20Canvas.nCreateLayer(paramInt1, paramInt2, paramBoolean, arrayOfInt);
        if (this.mLayer != 0)
        {
            this.mLayerWidth = arrayOfInt[0];
            this.mLayerHeight = arrayOfInt[bool];
            int i = this.mLayer;
            if (!paramBoolean)
                this.mCanvas = new GLES20Canvas(i, bool);
        }
        for (this.mFinalizer = new GLES20Layer.Finalizer(this.mLayer); ; this.mFinalizer = null)
        {
            return;
            bool = false;
            break;
            this.mCanvas = null;
        }
    }

    void end(Canvas paramCanvas)
    {
        if ((paramCanvas instanceof GLES20Canvas))
            ((GLES20Canvas)paramCanvas).resume();
    }

    HardwareCanvas getCanvas()
    {
        return this.mCanvas;
    }

    boolean isValid()
    {
        if ((this.mLayer != 0) && (this.mLayerWidth > 0) && (this.mLayerHeight > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void redraw(DisplayList paramDisplayList, Rect paramRect)
    {
        GLES20Canvas.nUpdateRenderLayer(this.mLayer, this.mCanvas.getRenderer(), ((GLES20DisplayList)paramDisplayList).getNativeDisplayList(), paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    void resize(int paramInt1, int paramInt2)
    {
        if ((!isValid()) || (paramInt1 <= 0) || (paramInt2 <= 0));
        while (true)
        {
            return;
            this.mWidth = paramInt1;
            this.mHeight = paramInt2;
            if ((paramInt1 != this.mLayerWidth) || (paramInt2 != this.mLayerHeight))
            {
                int[] arrayOfInt = new int[2];
                GLES20Canvas.nResizeLayer(this.mLayer, paramInt1, paramInt2, arrayOfInt);
                this.mLayerWidth = arrayOfInt[0];
                this.mLayerHeight = arrayOfInt[1];
            }
        }
    }

    void setTransform(Matrix paramMatrix)
    {
    }

    HardwareCanvas start(Canvas paramCanvas)
    {
        if ((paramCanvas instanceof GLES20Canvas))
            ((GLES20Canvas)paramCanvas).interrupt();
        return getCanvas();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GLES20RenderLayer
 * JD-Core Version:        0.6.2
 */