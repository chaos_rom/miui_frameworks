package android.view;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public abstract class OrientationEventListener
{
    private static final boolean DEBUG = false;
    public static final int ORIENTATION_UNKNOWN = -1;
    private static final String TAG = "OrientationEventListener";
    private static final boolean localLOGV;
    private boolean mEnabled = false;
    private OrientationListener mOldListener;
    private int mOrientation = -1;
    private int mRate;
    private Sensor mSensor;
    private SensorEventListener mSensorEventListener;
    private SensorManager mSensorManager;

    public OrientationEventListener(Context paramContext)
    {
        this(paramContext, 3);
    }

    public OrientationEventListener(Context paramContext, int paramInt)
    {
        this.mSensorManager = ((SensorManager)paramContext.getSystemService("sensor"));
        this.mRate = paramInt;
        this.mSensor = this.mSensorManager.getDefaultSensor(1);
        if (this.mSensor != null)
            this.mSensorEventListener = new SensorEventListenerImpl();
    }

    public boolean canDetectOrientation()
    {
        if (this.mSensor != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void disable()
    {
        if (this.mSensor == null)
            Log.w("OrientationEventListener", "Cannot detect sensors. Invalid disable");
        while (true)
        {
            return;
            if (this.mEnabled == true)
            {
                this.mSensorManager.unregisterListener(this.mSensorEventListener);
                this.mEnabled = false;
            }
        }
    }

    public void enable()
    {
        if (this.mSensor == null)
            Log.w("OrientationEventListener", "Cannot detect sensors. Not enabled");
        while (true)
        {
            return;
            if (!this.mEnabled)
            {
                this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensor, this.mRate);
                this.mEnabled = true;
            }
        }
    }

    public abstract void onOrientationChanged(int paramInt);

    void registerListener(OrientationListener paramOrientationListener)
    {
        this.mOldListener = paramOrientationListener;
    }

    class SensorEventListenerImpl
        implements SensorEventListener
    {
        private static final int _DATA_X = 0;
        private static final int _DATA_Y = 1;
        private static final int _DATA_Z = 2;

        SensorEventListenerImpl()
        {
        }

        public void onAccuracyChanged(Sensor paramSensor, int paramInt)
        {
        }

        public void onSensorChanged(SensorEvent paramSensorEvent)
        {
            float[] arrayOfFloat = paramSensorEvent.values;
            int i = -1;
            float f1 = -arrayOfFloat[0];
            float f2 = -arrayOfFloat[1];
            float f3 = -arrayOfFloat[2];
            if (4.0F * (f1 * f1 + f2 * f2) >= f3 * f3)
            {
                for (i = 90 - Math.round(57.29578F * (float)Math.atan2(-f2, f1)); i >= 360; i -= 360);
                while (i < 0)
                    i += 360;
            }
            if (OrientationEventListener.this.mOldListener != null)
                OrientationEventListener.this.mOldListener.onSensorChanged(1, paramSensorEvent.values);
            if (i != OrientationEventListener.this.mOrientation)
            {
                OrientationEventListener.access$102(OrientationEventListener.this, i);
                OrientationEventListener.this.onOrientationChanged(i);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.OrientationEventListener
 * JD-Core Version:        0.6.2
 */