package android.view;

public class SoundEffectConstants
{
    public static final int CLICK = 0;
    public static final int NAVIGATION_DOWN = 4;
    public static final int NAVIGATION_LEFT = 1;
    public static final int NAVIGATION_RIGHT = 3;
    public static final int NAVIGATION_UP = 2;

    public static int getContantForFocusDirection(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}.");
        case 66:
            i = 3;
        case 2:
        case 130:
        case 17:
        case 1:
        case 33:
        }
        while (true)
        {
            return i;
            i = 4;
            continue;
            i = 1;
            continue;
            i = 2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.SoundEffectConstants
 * JD-Core Version:        0.6.2
 */