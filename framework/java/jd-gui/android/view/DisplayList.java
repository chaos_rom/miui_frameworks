package android.view;

import android.graphics.Matrix;

public abstract class DisplayList
{
    public static final int FLAG_CLIP_CHILDREN = 1;
    public static final int STATUS_DONE = 0;
    public static final int STATUS_DRAW = 1;
    public static final int STATUS_DREW = 4;
    public static final int STATUS_INVOKE = 2;

    public abstract void clear();

    public abstract void end();

    public abstract int getSize();

    public abstract void invalidate();

    public abstract boolean isValid();

    public abstract void offsetLeftRight(int paramInt);

    public abstract void offsetTopBottom(int paramInt);

    public abstract void setAlpha(float paramFloat);

    public abstract void setAnimationMatrix(Matrix paramMatrix);

    public abstract void setBottom(int paramInt);

    public abstract void setCaching(boolean paramBoolean);

    public abstract void setCameraDistance(float paramFloat);

    public abstract void setClipChildren(boolean paramBoolean);

    public abstract void setHasOverlappingRendering(boolean paramBoolean);

    public abstract void setLeft(int paramInt);

    public abstract void setLeftTop(int paramInt1, int paramInt2);

    public abstract void setLeftTopRightBottom(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    public abstract void setPivotX(float paramFloat);

    public abstract void setPivotY(float paramFloat);

    public abstract void setRight(int paramInt);

    public abstract void setRotation(float paramFloat);

    public abstract void setRotationX(float paramFloat);

    public abstract void setRotationY(float paramFloat);

    public abstract void setScaleX(float paramFloat);

    public abstract void setScaleY(float paramFloat);

    public abstract void setStaticMatrix(Matrix paramMatrix);

    public abstract void setTop(int paramInt);

    public abstract void setTransformationInfo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8);

    public abstract void setTranslationX(float paramFloat);

    public abstract void setTranslationY(float paramFloat);

    public abstract HardwareCanvas start();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.DisplayList
 * JD-Core Version:        0.6.2
 */