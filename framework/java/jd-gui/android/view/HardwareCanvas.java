package android.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public abstract class HardwareCanvas extends Canvas
{
    abstract void attachFunctor(int paramInt);

    public int callDrawGLFunction(int paramInt)
    {
        return 0;
    }

    abstract void detachFunctor(int paramInt);

    public abstract int drawDisplayList(DisplayList paramDisplayList, Rect paramRect, int paramInt);

    abstract void drawHardwareLayer(HardwareLayer paramHardwareLayer, float paramFloat1, float paramFloat2, Paint paramPaint);

    public int invokeFunctors(Rect paramRect)
    {
        return 0;
    }

    public boolean isHardwareAccelerated()
    {
        return true;
    }

    public abstract void onPostDraw();

    public abstract int onPreDraw(Rect paramRect);

    abstract void outputDisplayList(DisplayList paramDisplayList);

    public void setBitmap(Bitmap paramBitmap)
    {
        throw new UnsupportedOperationException();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.HardwareCanvas
 * JD-Core Version:        0.6.2
 */