package android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.android.internal.R.styleable;
import com.android.internal.util.XmlUtils;

public final class PointerIcon
    implements Parcelable
{
    public static final Parcelable.Creator<PointerIcon> CREATOR = new Parcelable.Creator()
    {
        public PointerIcon createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = paramAnonymousParcel.readInt();
            PointerIcon localPointerIcon;
            if (i == 0)
                localPointerIcon = PointerIcon.getNullIcon();
            while (true)
            {
                return localPointerIcon;
                int j = paramAnonymousParcel.readInt();
                if (j != 0)
                {
                    localPointerIcon = new PointerIcon(i, null);
                    PointerIcon.access$102(localPointerIcon, j);
                }
                else
                {
                    localPointerIcon = PointerIcon.createCustomIcon((Bitmap)Bitmap.CREATOR.createFromParcel(paramAnonymousParcel), paramAnonymousParcel.readFloat(), paramAnonymousParcel.readFloat());
                }
            }
        }

        public PointerIcon[] newArray(int paramAnonymousInt)
        {
            return new PointerIcon[paramAnonymousInt];
        }
    };
    public static final int STYLE_ARROW = 1000;
    public static final int STYLE_CUSTOM = -1;
    private static final int STYLE_DEFAULT = 1000;
    public static final int STYLE_NULL = 0;
    private static final int STYLE_OEM_FIRST = 10000;
    public static final int STYLE_SPOT_ANCHOR = 2002;
    public static final int STYLE_SPOT_HOVER = 2000;
    public static final int STYLE_SPOT_TOUCH = 2001;
    private static final String TAG = "PointerIcon";
    private static final PointerIcon gNullIcon = new PointerIcon(0);
    private Bitmap mBitmap;
    private float mHotSpotX;
    private float mHotSpotY;
    private final int mStyle;
    private int mSystemIconResourceId;

    private PointerIcon(int paramInt)
    {
        this.mStyle = paramInt;
    }

    public static PointerIcon createCustomIcon(Bitmap paramBitmap, float paramFloat1, float paramFloat2)
    {
        if (paramBitmap == null)
            throw new IllegalArgumentException("bitmap must not be null");
        validateHotSpot(paramBitmap, paramFloat1, paramFloat2);
        PointerIcon localPointerIcon = new PointerIcon(-1);
        localPointerIcon.mBitmap = paramBitmap;
        localPointerIcon.mHotSpotX = paramFloat1;
        localPointerIcon.mHotSpotY = paramFloat2;
        return localPointerIcon;
    }

    public static PointerIcon getDefaultIcon(Context paramContext)
    {
        return getSystemIcon(paramContext, 1000);
    }

    public static PointerIcon getNullIcon()
    {
        return gNullIcon;
    }

    public static PointerIcon getSystemIcon(Context paramContext, int paramInt)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context must not be null");
        Object localObject;
        if (paramInt == 0)
            localObject = gNullIcon;
        int j;
        while (true)
        {
            return localObject;
            int i = getSystemIconStyleIndex(paramInt);
            if (i == 0)
                i = getSystemIconStyleIndex(1000);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(null, R.styleable.Pointer, 16843739, 0);
            j = localTypedArray.getResourceId(i, -1);
            localTypedArray.recycle();
            if (j != -1)
                break;
            Log.w("PointerIcon", "Missing theme resources for pointer icon style " + paramInt);
            if (paramInt == 1000)
                localObject = gNullIcon;
            else
                localObject = getSystemIcon(paramContext, 1000);
        }
        PointerIcon localPointerIcon = new PointerIcon(paramInt);
        if ((0xFF000000 & j) == 16777216)
            localPointerIcon.mSystemIconResourceId = j;
        while (true)
        {
            localObject = localPointerIcon;
            break;
            localPointerIcon.loadResource(paramContext.getResources(), j);
        }
    }

    private static int getSystemIconStyleIndex(int paramInt)
    {
        int i = 0;
        switch (paramInt)
        {
        case 1000:
        default:
        case 2000:
        case 2001:
        case 2002:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
        }
    }

    public static PointerIcon loadCustomIcon(Resources paramResources, int paramInt)
    {
        if (paramResources == null)
            throw new IllegalArgumentException("resources must not be null");
        PointerIcon localPointerIcon = new PointerIcon(-1);
        localPointerIcon.loadResource(paramResources, paramInt);
        return localPointerIcon;
    }

    private void loadResource(Resources paramResources, int paramInt)
    {
        XmlResourceParser localXmlResourceParser = paramResources.getXml(paramInt);
        int i;
        float f1;
        float f2;
        try
        {
            XmlUtils.beginDocument(localXmlResourceParser, "pointer-icon");
            TypedArray localTypedArray = paramResources.obtainAttributes(localXmlResourceParser, R.styleable.PointerIcon);
            i = localTypedArray.getResourceId(0, 0);
            f1 = localTypedArray.getFloat(1, 0.0F);
            f2 = localTypedArray.getFloat(2, 0.0F);
            localTypedArray.recycle();
            localXmlResourceParser.close();
            if (i == 0)
                throw new IllegalArgumentException("<pointer-icon> is missing bitmap attribute.");
        }
        catch (Exception localException)
        {
            throw new IllegalArgumentException("Exception parsing pointer icon resource.", localException);
        }
        finally
        {
            localXmlResourceParser.close();
        }
        Drawable localDrawable = paramResources.getDrawable(i);
        if (!(localDrawable instanceof BitmapDrawable))
            throw new IllegalArgumentException("<pointer-icon> bitmap attribute must refer to a bitmap drawable.");
        this.mBitmap = ((BitmapDrawable)localDrawable).getBitmap();
        this.mHotSpotX = f1;
        this.mHotSpotY = f2;
    }

    private void throwIfIconIsNotLoaded()
    {
        if (!isLoaded())
            throw new IllegalStateException("The icon is not loaded.");
    }

    private static void validateHotSpot(Bitmap paramBitmap, float paramFloat1, float paramFloat2)
    {
        if ((paramFloat1 < 0.0F) || (paramFloat1 >= paramBitmap.getWidth()))
            throw new IllegalArgumentException("x hotspot lies outside of the bitmap area");
        if ((paramFloat2 < 0.0F) || (paramFloat2 >= paramBitmap.getHeight()))
            throw new IllegalArgumentException("y hotspot lies outside of the bitmap area");
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (!(paramObject instanceof PointerIcon)))
            {
                bool = false;
            }
            else
            {
                PointerIcon localPointerIcon = (PointerIcon)paramObject;
                if ((this.mStyle != localPointerIcon.mStyle) || (this.mSystemIconResourceId != localPointerIcon.mSystemIconResourceId))
                    bool = false;
                else if ((this.mSystemIconResourceId == 0) && ((this.mBitmap != localPointerIcon.mBitmap) || (this.mHotSpotX != localPointerIcon.mHotSpotX) || (this.mHotSpotY != localPointerIcon.mHotSpotY)))
                    bool = false;
            }
        }
    }

    public Bitmap getBitmap()
    {
        throwIfIconIsNotLoaded();
        return this.mBitmap;
    }

    public float getHotSpotX()
    {
        throwIfIconIsNotLoaded();
        return this.mHotSpotX;
    }

    public float getHotSpotY()
    {
        throwIfIconIsNotLoaded();
        return this.mHotSpotY;
    }

    public int getStyle()
    {
        return this.mStyle;
    }

    public boolean isLoaded()
    {
        if ((this.mBitmap != null) || (this.mStyle == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isNullIcon()
    {
        if (this.mStyle == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public PointerIcon load(Context paramContext)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context must not be null");
        PointerIcon localPointerIcon;
        if ((this.mSystemIconResourceId == 0) || (this.mBitmap != null))
            localPointerIcon = this;
        while (true)
        {
            return localPointerIcon;
            localPointerIcon = new PointerIcon(this.mStyle);
            localPointerIcon.mSystemIconResourceId = this.mSystemIconResourceId;
            localPointerIcon.loadResource(paramContext.getResources(), this.mSystemIconResourceId);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mStyle);
        if (this.mStyle != 0)
        {
            paramParcel.writeInt(this.mSystemIconResourceId);
            if (this.mSystemIconResourceId == 0)
            {
                this.mBitmap.writeToParcel(paramParcel, paramInt);
                paramParcel.writeFloat(this.mHotSpotX);
                paramParcel.writeFloat(this.mHotSpotY);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.PointerIcon
 * JD-Core Version:        0.6.2
 */