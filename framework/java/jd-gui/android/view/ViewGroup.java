package android.view;

import android.animation.LayoutTransition;
import android.animation.LayoutTransition.TransitionListener;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.LayoutAnimationController.AnimationParameters;
import android.view.animation.Transformation;
import com.android.internal.R.styleable;
import com.android.internal.util.Predicate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

public abstract class ViewGroup extends View
    implements ViewParent, ViewManager
{
    private static final int ARRAY_CAPACITY_INCREMENT = 12;
    private static final int ARRAY_INITIAL_CAPACITY = 12;
    private static final int CHILD_LEFT_INDEX = 0;
    private static final int CHILD_TOP_INDEX = 1;
    public static final int CLIP_BOUNDS = 0;
    protected static final int CLIP_TO_PADDING_MASK = 34;
    private static final boolean DBG = false;
    private static final int[] DESCENDANT_FOCUSABILITY_FLAGS = arrayOfInt;
    private static final int FLAG_ADD_STATES_FROM_CHILDREN = 8192;
    static final int FLAG_ALPHA_LOWER_THAN_ONE = 4096;
    static final int FLAG_ALWAYS_DRAWN_WITH_CACHE = 16384;
    private static final int FLAG_ANIMATION_CACHE = 64;
    static final int FLAG_ANIMATION_DONE = 16;
    static final int FLAG_CHILDREN_DRAWN_WITH_CACHE = 32768;
    static final int FLAG_CLEAR_TRANSFORMATION = 256;
    static final int FLAG_CLIP_CHILDREN = 1;
    private static final int FLAG_CLIP_TO_PADDING = 2;
    protected static final int FLAG_DISALLOW_INTERCEPT = 524288;
    static final int FLAG_INVALIDATE_REQUIRED = 4;
    private static final int FLAG_MASK_FOCUSABILITY = 393216;
    private static final int FLAG_NOTIFY_ANIMATION_LISTENER = 512;
    private static final int FLAG_NOTIFY_CHILDREN_ON_DRAWABLE_STATE_CHANGE = 65536;
    static final int FLAG_OPTIMIZE_INVALIDATE = 128;
    private static final int FLAG_PADDING_NOT_NULL = 32;
    private static final int FLAG_PREVENT_DISPATCH_ATTACHED_TO_WINDOW = 4194304;
    private static final int FLAG_RUN_ANIMATION = 8;
    private static final int FLAG_SPLIT_MOTION_EVENTS = 2097152;
    protected static final int FLAG_SUPPORT_STATIC_TRANSFORMATIONS = 2048;
    protected static final int FLAG_USE_CHILD_DRAWING_ORDER = 1024;
    public static final int FOCUS_AFTER_DESCENDANTS = 262144;
    public static final int FOCUS_BEFORE_DESCENDANTS = 131072;
    public static final int FOCUS_BLOCK_DESCENDANTS = 393216;
    public static final int OPTICAL_BOUNDS = 1;
    public static final int PERSISTENT_ALL_CACHES = 3;
    public static final int PERSISTENT_ANIMATION_CACHE = 1;
    public static final int PERSISTENT_NO_CACHE = 0;
    public static final int PERSISTENT_SCROLLING_CACHE = 2;
    private static final String TAG = "ViewGroup";
    private static float[] sDebugLines;
    private static Paint sDebugPaint;
    private Animation.AnimationListener mAnimationListener;
    Paint mCachePaint;
    private boolean mChildAcceptsDrag;

    @ViewDebug.ExportedProperty(category="layout")
    private int mChildCountWithTransientState = 0;
    final Transformation mChildTransformation = new Transformation();
    private View[] mChildren;
    private int mChildrenCount;
    private DragEvent mCurrentDrag;
    private View mCurrentDragView;
    protected ArrayList<View> mDisappearingChildren;
    private HashSet<View> mDragNotifiedChildren;

    @ViewDebug.ExportedProperty(category="drawing")
    boolean mDrawLayers = true;
    private HoverTarget mFirstHoverTarget;
    private TouchTarget mFirstTouchTarget;
    private View mFocused;
    protected int mGroupFlags;
    private boolean mHoveredSelf;
    RectF mInvalidateRegion;
    Transformation mInvalidationTransformation;

    @ViewDebug.ExportedProperty(category="events")
    private int mLastTouchDownIndex = -1;

    @ViewDebug.ExportedProperty(category="events")
    private long mLastTouchDownTime;

    @ViewDebug.ExportedProperty(category="events")
    private float mLastTouchDownX;

    @ViewDebug.ExportedProperty(category="events")
    private float mLastTouchDownY;
    private LayoutAnimationController mLayoutAnimationController;
    private int mLayoutMode = 0;
    private boolean mLayoutSuppressed = false;
    private LayoutTransition.TransitionListener mLayoutTransitionListener = new LayoutTransition.TransitionListener()
    {
        public void endTransition(LayoutTransition paramAnonymousLayoutTransition, ViewGroup paramAnonymousViewGroup, View paramAnonymousView, int paramAnonymousInt)
        {
            if ((ViewGroup.this.mLayoutSuppressed) && (!paramAnonymousLayoutTransition.isChangingLayout()))
            {
                ViewGroup.this.requestLayout();
                ViewGroup.access$302(ViewGroup.this, false);
            }
            if ((paramAnonymousInt == 3) && (ViewGroup.this.mTransitioningViews != null))
                ViewGroup.this.endViewTransition(paramAnonymousView);
        }

        public void startTransition(LayoutTransition paramAnonymousLayoutTransition, ViewGroup paramAnonymousViewGroup, View paramAnonymousView, int paramAnonymousInt)
        {
            if (paramAnonymousInt == 3)
                ViewGroup.this.startViewTransition(paramAnonymousView);
        }
    };
    private final PointF mLocalPoint = new PointF();
    protected OnHierarchyChangeListener mOnHierarchyChangeListener;
    protected int mPersistentDrawingCache;
    private LayoutTransition mTransition;
    private ArrayList<View> mTransitioningViews;
    private ArrayList<View> mVisibilityChangingChildren;

    static
    {
        int[] arrayOfInt = new int[3];
        arrayOfInt[0] = 131072;
        arrayOfInt[1] = 262144;
        arrayOfInt[2] = 393216;
    }

    public ViewGroup(Context paramContext)
    {
        super(paramContext);
        initViewGroup();
    }

    public ViewGroup(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        initViewGroup();
        initFromAttributes(paramContext, paramAttributeSet);
    }

    public ViewGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initViewGroup();
        initFromAttributes(paramContext, paramAttributeSet);
    }

    private void addDisappearingView(View paramView)
    {
        ArrayList localArrayList = this.mDisappearingChildren;
        if (localArrayList == null)
        {
            localArrayList = new ArrayList();
            this.mDisappearingChildren = localArrayList;
        }
        localArrayList.add(paramView);
    }

    private void addInArray(View paramView, int paramInt)
    {
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        int j = arrayOfView.length;
        if (paramInt == i)
        {
            if (j == i)
            {
                this.mChildren = new View[j + 12];
                System.arraycopy(arrayOfView, 0, this.mChildren, 0, j);
                arrayOfView = this.mChildren;
            }
            int k = this.mChildrenCount;
            this.mChildrenCount = (k + 1);
            arrayOfView[k] = paramView;
            return;
        }
        if (paramInt < i)
        {
            if (j == i)
            {
                this.mChildren = new View[j + 12];
                System.arraycopy(arrayOfView, 0, this.mChildren, 0, paramInt);
                System.arraycopy(arrayOfView, paramInt, this.mChildren, paramInt + 1, i - paramInt);
                arrayOfView = this.mChildren;
            }
            while (true)
            {
                arrayOfView[paramInt] = paramView;
                this.mChildrenCount = (1 + this.mChildrenCount);
                if (this.mLastTouchDownIndex < paramInt)
                    break;
                this.mLastTouchDownIndex = (1 + this.mLastTouchDownIndex);
                break;
                System.arraycopy(arrayOfView, paramInt, arrayOfView, paramInt + 1, i - paramInt);
            }
        }
        throw new IndexOutOfBoundsException("index=" + paramInt + " count=" + i);
    }

    private TouchTarget addTouchTarget(View paramView, int paramInt)
    {
        TouchTarget localTouchTarget = TouchTarget.obtain(paramView, paramInt);
        localTouchTarget.next = this.mFirstTouchTarget;
        this.mFirstTouchTarget = localTouchTarget;
        return localTouchTarget;
    }

    private void addViewInner(View paramView, int paramInt, LayoutParams paramLayoutParams, boolean paramBoolean)
    {
        if (this.mTransition != null)
            this.mTransition.cancel(3);
        if (paramView.getParent() != null)
            throw new IllegalStateException("The specified child already has a parent. You must call removeView() on the child's parent first.");
        if (this.mTransition != null)
            this.mTransition.addChild(this, paramView);
        if (!checkLayoutParams(paramLayoutParams))
            paramLayoutParams = generateLayoutParams(paramLayoutParams);
        if (paramBoolean)
        {
            paramView.mLayoutParams = paramLayoutParams;
            if (paramInt < 0)
                paramInt = this.mChildrenCount;
            addInArray(paramView, paramInt);
            if (!paramBoolean)
                break label233;
            paramView.assignParent(this);
        }
        while (true)
        {
            if (paramView.hasFocus())
                requestChildFocus(paramView, paramView.findFocus());
            View.AttachInfo localAttachInfo = this.mAttachInfo;
            if ((localAttachInfo != null) && ((0x400000 & this.mGroupFlags) == 0))
            {
                boolean bool = localAttachInfo.mKeepScreenOn;
                localAttachInfo.mKeepScreenOn = false;
                paramView.dispatchAttachedToWindow(this.mAttachInfo, 0xC & this.mViewFlags);
                if (localAttachInfo.mKeepScreenOn)
                    needGlobalAttributesUpdate(true);
                localAttachInfo.mKeepScreenOn = bool;
            }
            onViewAdded(paramView);
            if ((0x400000 & paramView.mViewFlags) == 4194304)
                this.mGroupFlags = (0x10000 | this.mGroupFlags);
            if (paramView.hasTransientState())
                childHasTransientStateChanged(paramView, true);
            return;
            paramView.setLayoutParams(paramLayoutParams);
            break;
            label233: paramView.mParent = this;
        }
    }

    private void bindLayoutAnimation(View paramView)
    {
        paramView.setAnimation(this.mLayoutAnimationController.getAnimationForView(paramView));
    }

    private static boolean canViewReceivePointerEvents(View paramView)
    {
        if (((0xC & paramView.mViewFlags) == 0) || (paramView.getAnimation() != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void cancelAndClearTouchTargets(MotionEvent paramMotionEvent)
    {
        if (this.mFirstTouchTarget != null)
        {
            int i = 0;
            if (paramMotionEvent == null)
            {
                long l = SystemClock.uptimeMillis();
                paramMotionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
                paramMotionEvent.setSource(4098);
                i = 1;
            }
            for (TouchTarget localTouchTarget = this.mFirstTouchTarget; localTouchTarget != null; localTouchTarget = localTouchTarget.next)
            {
                resetCancelNextUpFlag(localTouchTarget.child);
                dispatchTransformedTouchEvent(paramMotionEvent, true, localTouchTarget.child, localTouchTarget.pointerIdBits);
            }
            clearTouchTargets();
            if (i != 0)
                paramMotionEvent.recycle();
        }
    }

    private void cancelHoverTarget(View paramView)
    {
        Object localObject1 = null;
        HoverTarget localHoverTarget;
        for (Object localObject2 = this.mFirstHoverTarget; ; localObject2 = localHoverTarget)
        {
            if (localObject2 != null)
            {
                localHoverTarget = ((HoverTarget)localObject2).next;
                if (((HoverTarget)localObject2).child != paramView)
                    break label88;
                if (localObject1 != null)
                    break label79;
                this.mFirstHoverTarget = localHoverTarget;
            }
            while (true)
            {
                ((HoverTarget)localObject2).recycle();
                long l = SystemClock.uptimeMillis();
                MotionEvent localMotionEvent = MotionEvent.obtain(l, l, 10, 0.0F, 0.0F, 0);
                localMotionEvent.setSource(4098);
                paramView.dispatchHoverEvent(localMotionEvent);
                localMotionEvent.recycle();
                return;
                label79: localObject1.next = localHoverTarget;
            }
            label88: localObject1 = localObject2;
        }
    }

    private void cancelTouchTarget(View paramView)
    {
        Object localObject1 = null;
        TouchTarget localTouchTarget;
        for (Object localObject2 = this.mFirstTouchTarget; ; localObject2 = localTouchTarget)
        {
            if (localObject2 != null)
            {
                localTouchTarget = ((TouchTarget)localObject2).next;
                if (((TouchTarget)localObject2).child != paramView)
                    break label87;
                if (localObject1 != null)
                    break label78;
                this.mFirstTouchTarget = localTouchTarget;
            }
            while (true)
            {
                ((TouchTarget)localObject2).recycle();
                long l = SystemClock.uptimeMillis();
                MotionEvent localMotionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
                localMotionEvent.setSource(4098);
                paramView.dispatchTouchEvent(localMotionEvent);
                localMotionEvent.recycle();
                return;
                label78: localObject1.next = localTouchTarget;
            }
            label87: localObject1 = localObject2;
        }
    }

    private void clearTouchTargets()
    {
        Object localObject = this.mFirstTouchTarget;
        if (localObject != null)
        {
            do
            {
                TouchTarget localTouchTarget = ((TouchTarget)localObject).next;
                ((TouchTarget)localObject).recycle();
                localObject = localTouchTarget;
            }
            while (localObject != null);
            this.mFirstTouchTarget = null;
        }
    }

    private boolean debugDraw()
    {
        if ((this.mAttachInfo != null) && (this.mAttachInfo.mDebugLayout));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean dispatchTransformedGenericPointerEvent(MotionEvent paramMotionEvent, View paramView)
    {
        float f1 = this.mScrollX - paramView.mLeft;
        float f2 = this.mScrollY - paramView.mTop;
        boolean bool;
        if (!paramView.hasIdentityMatrix())
        {
            MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
            localMotionEvent.offsetLocation(f1, f2);
            localMotionEvent.transform(paramView.getInverseMatrix());
            bool = paramView.dispatchGenericMotionEvent(localMotionEvent);
            localMotionEvent.recycle();
        }
        while (true)
        {
            return bool;
            paramMotionEvent.offsetLocation(f1, f2);
            bool = paramView.dispatchGenericMotionEvent(paramMotionEvent);
            paramMotionEvent.offsetLocation(-f1, -f2);
        }
    }

    private boolean dispatchTransformedTouchEvent(MotionEvent paramMotionEvent, boolean paramBoolean, View paramView, int paramInt)
    {
        int i = paramMotionEvent.getAction();
        if ((paramBoolean) || (i == 3))
        {
            paramMotionEvent.setAction(3);
            if (paramView == null)
            {
                bool = super.dispatchTouchEvent(paramMotionEvent);
                paramMotionEvent.setAction(i);
            }
        }
        int k;
        while (true)
        {
            return bool;
            bool = paramView.dispatchTouchEvent(paramMotionEvent);
            break;
            int j = paramMotionEvent.getPointerIdBits();
            k = j & paramInt;
            if (k == 0)
            {
                bool = false;
            }
            else
            {
                if (k != j)
                    break label185;
                if ((paramView != null) && (!paramView.hasIdentityMatrix()))
                    break label159;
                if (paramView == null)
                {
                    bool = super.dispatchTouchEvent(paramMotionEvent);
                }
                else
                {
                    float f1 = this.mScrollX - paramView.mLeft;
                    float f2 = this.mScrollY - paramView.mTop;
                    paramMotionEvent.offsetLocation(f1, f2);
                    bool = paramView.dispatchTouchEvent(paramMotionEvent);
                    paramMotionEvent.offsetLocation(-f1, -f2);
                }
            }
        }
        label159: MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
        label165: if (paramView == null);
        for (boolean bool = super.dispatchTouchEvent(localMotionEvent); ; bool = paramView.dispatchTouchEvent(localMotionEvent))
        {
            localMotionEvent.recycle();
            break;
            label185: localMotionEvent = paramMotionEvent.split(k);
            break label165;
            localMotionEvent.offsetLocation(this.mScrollX - paramView.mLeft, this.mScrollY - paramView.mTop);
            if (!paramView.hasIdentityMatrix())
                localMotionEvent.transform(paramView.getInverseMatrix());
        }
    }

    private static void drawRect(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        Paint localPaint = getDebugPaint();
        localPaint.setColor(paramInt5);
        paramCanvas.drawLines(getDebugLines(paramInt1, paramInt2, paramInt3, paramInt4), localPaint);
    }

    private void exitHoverTargets()
    {
        if ((this.mHoveredSelf) || (this.mFirstHoverTarget != null))
        {
            long l = SystemClock.uptimeMillis();
            MotionEvent localMotionEvent = MotionEvent.obtain(l, l, 10, 0.0F, 0.0F, 0);
            localMotionEvent.setSource(4098);
            dispatchHoverEvent(localMotionEvent);
            localMotionEvent.recycle();
        }
    }

    public static int getChildMeasureSpec(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = Math.max(0, View.MeasureSpec.getSize(paramInt1) - paramInt2);
        int k = 0;
        int m = 0;
        switch (i)
        {
        default:
        case 1073741824:
        case -2147483648:
        case 0:
        }
        while (true)
        {
            return View.MeasureSpec.makeMeasureSpec(k, m);
            if (paramInt3 >= 0)
            {
                k = paramInt3;
                m = 1073741824;
            }
            else if (paramInt3 == -1)
            {
                k = j;
                m = 1073741824;
            }
            else if (paramInt3 == -2)
            {
                k = j;
                m = -2147483648;
                continue;
                if (paramInt3 >= 0)
                {
                    k = paramInt3;
                    m = 1073741824;
                }
                else if (paramInt3 == -1)
                {
                    k = j;
                    m = -2147483648;
                }
                else if (paramInt3 == -2)
                {
                    k = j;
                    m = -2147483648;
                    continue;
                    if (paramInt3 >= 0)
                    {
                        k = paramInt3;
                        m = 1073741824;
                    }
                    else if (paramInt3 == -1)
                    {
                        k = 0;
                        m = 0;
                    }
                    else if (paramInt3 == -2)
                    {
                        k = 0;
                        m = 0;
                    }
                }
            }
        }
    }

    private static float[] getDebugLines(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (sDebugLines == null)
            sDebugLines = new float[16];
        int i = paramInt3 - 1;
        int j = paramInt4 - 1;
        sDebugLines[0] = paramInt1;
        sDebugLines[1] = paramInt2;
        sDebugLines[2] = i;
        sDebugLines[3] = paramInt2;
        sDebugLines[4] = i;
        sDebugLines[5] = paramInt2;
        sDebugLines[6] = i;
        sDebugLines[7] = (j + 1);
        sDebugLines[8] = (i + 1);
        sDebugLines[9] = j;
        sDebugLines[10] = paramInt1;
        sDebugLines[11] = j;
        sDebugLines[12] = paramInt1;
        sDebugLines[13] = j;
        sDebugLines[14] = paramInt1;
        sDebugLines[15] = paramInt2;
        return sDebugLines;
    }

    private static Paint getDebugPaint()
    {
        if (sDebugPaint == null)
        {
            sDebugPaint = new Paint();
            sDebugPaint.setAntiAlias(false);
        }
        return sDebugPaint;
    }

    private TouchTarget getTouchTarget(View paramView)
    {
        TouchTarget localTouchTarget = this.mFirstTouchTarget;
        if (localTouchTarget != null)
            if (localTouchTarget.child != paramView);
        while (true)
        {
            return localTouchTarget;
            localTouchTarget = localTouchTarget.next;
            break;
            localTouchTarget = null;
        }
    }

    private void initFromAttributes(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewGroup);
        int i = localTypedArray.getIndexCount();
        int j = 0;
        if (j < i)
        {
            int k = localTypedArray.getIndex(j);
            switch (k)
            {
            default:
            case 0:
            case 1:
            case 3:
            case 4:
            case 6:
            case 5:
            case 2:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                j++;
                break;
                setClipChildren(localTypedArray.getBoolean(k, true));
                continue;
                setClipToPadding(localTypedArray.getBoolean(k, true));
                continue;
                setAnimationCacheEnabled(localTypedArray.getBoolean(k, true));
                continue;
                setPersistentDrawingCache(localTypedArray.getInt(k, 2));
                continue;
                setAddStatesFromChildren(localTypedArray.getBoolean(k, false));
                continue;
                setAlwaysDrawnWithCacheEnabled(localTypedArray.getBoolean(k, true));
                continue;
                int m = localTypedArray.getResourceId(k, -1);
                if (m > 0)
                {
                    setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this.mContext, m));
                    continue;
                    setDescendantFocusability(DESCENDANT_FOCUSABILITY_FLAGS[localTypedArray.getInt(k, 0)]);
                    continue;
                    setMotionEventSplittingEnabled(localTypedArray.getBoolean(k, false));
                    continue;
                    if (localTypedArray.getBoolean(k, false))
                        setLayoutTransition(new LayoutTransition());
                }
            }
        }
        localTypedArray.recycle();
    }

    private void initViewGroup()
    {
        if (!debugDraw())
            setFlags(128, 128);
        this.mGroupFlags = (0x1 | this.mGroupFlags);
        this.mGroupFlags = (0x2 | this.mGroupFlags);
        this.mGroupFlags = (0x10 | this.mGroupFlags);
        this.mGroupFlags = (0x40 | this.mGroupFlags);
        this.mGroupFlags = (0x4000 | this.mGroupFlags);
        if (this.mContext.getApplicationInfo().targetSdkVersion >= 11)
            this.mGroupFlags = (0x200000 | this.mGroupFlags);
        setDescendantFocusability(131072);
        this.mChildren = new View[12];
        this.mChildrenCount = 0;
        this.mPersistentDrawingCache = 2;
    }

    private ViewParent invalidateChildInParentFast(int paramInt1, int paramInt2, Rect paramRect)
    {
        if (((0x20 & this.mPrivateFlags) == 32) || ((0x8000 & this.mPrivateFlags) == 32768))
        {
            paramRect.offset(paramInt1 - this.mScrollX, paramInt2 - this.mScrollY);
            if (((0x1 & this.mGroupFlags) == 0) || (paramRect.intersect(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop)))
            {
                if (this.mLayerType != 0)
                    this.mLocalDirtyRect.union(paramRect);
                if (!getMatrix().isIdentity())
                    transformRect(paramRect);
            }
        }
        for (ViewParent localViewParent = this.mParent; ; localViewParent = null)
            return localViewParent;
    }

    private void notifyAnimationListener()
    {
        this.mGroupFlags = (0xFFFFFDFF & this.mGroupFlags);
        this.mGroupFlags = (0x10 | this.mGroupFlags);
        if (this.mAnimationListener != null)
            post(new Runnable()
            {
                public void run()
                {
                    ViewGroup.this.mAnimationListener.onAnimationEnd(ViewGroup.this.mLayoutAnimationController.getAnimation());
                }
            });
        if ((0x40 & this.mGroupFlags) == 64)
        {
            this.mGroupFlags = (0xFFFF7FFF & this.mGroupFlags);
            if ((0x1 & this.mPersistentDrawingCache) == 0)
                setChildrenDrawingCacheEnabled(false);
        }
        invalidate(true);
    }

    private static MotionEvent obtainMotionEventNoHistoryOrSelf(MotionEvent paramMotionEvent)
    {
        if (paramMotionEvent.getHistorySize() == 0);
        while (true)
        {
            return paramMotionEvent;
            paramMotionEvent = MotionEvent.obtainNoHistory(paramMotionEvent);
        }
    }

    private void removeFromArray(int paramInt)
    {
        View[] arrayOfView = this.mChildren;
        if ((this.mTransitioningViews == null) || (!this.mTransitioningViews.contains(arrayOfView[paramInt])))
            arrayOfView[paramInt].mParent = null;
        int i = this.mChildrenCount;
        if (paramInt == i - 1)
        {
            int k = -1 + this.mChildrenCount;
            this.mChildrenCount = k;
            arrayOfView[k] = null;
            if (this.mLastTouchDownIndex != paramInt)
                break label139;
            this.mLastTouchDownTime = 0L;
            this.mLastTouchDownIndex = -1;
        }
        while (true)
        {
            return;
            if ((paramInt >= 0) && (paramInt < i))
            {
                System.arraycopy(arrayOfView, paramInt + 1, arrayOfView, paramInt, -1 + (i - paramInt));
                int j = -1 + this.mChildrenCount;
                this.mChildrenCount = j;
                arrayOfView[j] = null;
                break;
            }
            throw new IndexOutOfBoundsException();
            label139: if (this.mLastTouchDownIndex > paramInt)
                this.mLastTouchDownIndex = (-1 + this.mLastTouchDownIndex);
        }
    }

    private void removeFromArray(int paramInt1, int paramInt2)
    {
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        int j = Math.max(0, paramInt1);
        int k = Math.min(i, j + paramInt2);
        if (j == k);
        while (true)
        {
            return;
            if (k == i)
                for (int i1 = j; i1 < k; i1++)
                {
                    arrayOfView[i1].mParent = null;
                    arrayOfView[i1] = null;
                }
            for (int m = j; m < k; m++)
                arrayOfView[m].mParent = null;
            System.arraycopy(arrayOfView, k, arrayOfView, j, i - k);
            for (int n = i - (k - j); n < i; n++)
                arrayOfView[n] = null;
            this.mChildrenCount -= k - j;
        }
    }

    private void removePointersFromTouchTargets(int paramInt)
    {
        Object localObject1 = null;
        Object localObject2 = this.mFirstTouchTarget;
        while (localObject2 != null)
        {
            TouchTarget localTouchTarget = ((TouchTarget)localObject2).next;
            if ((paramInt & ((TouchTarget)localObject2).pointerIdBits) != 0)
            {
                ((TouchTarget)localObject2).pointerIdBits &= (paramInt ^ 0xFFFFFFFF);
                if (((TouchTarget)localObject2).pointerIdBits == 0)
                {
                    if (localObject1 == null)
                        this.mFirstTouchTarget = localTouchTarget;
                    while (true)
                    {
                        ((TouchTarget)localObject2).recycle();
                        localObject2 = localTouchTarget;
                        break;
                        localObject1.next = localTouchTarget;
                    }
                }
            }
            localObject1 = localObject2;
            localObject2 = localTouchTarget;
        }
    }

    private void removeViewInternal(int paramInt, View paramView)
    {
        if (this.mTransition != null)
            this.mTransition.removeChild(this, paramView);
        int i = 0;
        if (paramView == this.mFocused)
        {
            paramView.unFocus();
            i = 1;
        }
        paramView.clearAccessibilityFocus();
        cancelTouchTarget(paramView);
        cancelHoverTarget(paramView);
        if ((paramView.getAnimation() != null) || ((this.mTransitioningViews != null) && (this.mTransitioningViews.contains(paramView))))
            addDisappearingView(paramView);
        while (true)
        {
            if (paramView.hasTransientState())
                childHasTransientStateChanged(paramView, false);
            onViewRemoved(paramView);
            needGlobalAttributesUpdate(false);
            removeFromArray(paramInt);
            if (i != 0)
            {
                clearChildFocus(paramView);
                ensureInputFocusOnFirstFocusable();
            }
            if (paramView.isAccessibilityFocused())
                paramView.clearAccessibilityFocus();
            return;
            if (paramView.mAttachInfo != null)
                paramView.dispatchDetachedFromWindow();
        }
    }

    private void removeViewInternal(View paramView)
    {
        int i = indexOfChild(paramView);
        if (i >= 0)
            removeViewInternal(i, paramView);
    }

    private void removeViewsInternal(int paramInt1, int paramInt2)
    {
        View localView1 = this.mFocused;
        int i;
        Object localObject;
        int k;
        label32: View localView2;
        if (this.mAttachInfo != null)
        {
            i = 1;
            localObject = null;
            View[] arrayOfView = this.mChildren;
            int j = paramInt1 + paramInt2;
            k = paramInt1;
            if (k >= j)
                break label179;
            localView2 = arrayOfView[k];
            if (this.mTransition != null)
                this.mTransition.removeChild(this, localView2);
            if (localView2 == localView1)
            {
                localView2.unFocus();
                localObject = localView2;
            }
            localView2.clearAccessibilityFocus();
            cancelTouchTarget(localView2);
            cancelHoverTarget(localView2);
            if ((localView2.getAnimation() == null) && ((this.mTransitioningViews == null) || (!this.mTransitioningViews.contains(localView2))))
                break label166;
            addDisappearingView(localView2);
        }
        while (true)
        {
            if (localView2.hasTransientState())
                childHasTransientStateChanged(localView2, false);
            needGlobalAttributesUpdate(false);
            onViewRemoved(localView2);
            k++;
            break label32;
            i = 0;
            break;
            label166: if (i != 0)
                localView2.dispatchDetachedFromWindow();
        }
        label179: removeFromArray(paramInt1, paramInt2);
        if (localObject != null)
        {
            clearChildFocus(localObject);
            ensureInputFocusOnFirstFocusable();
        }
    }

    private static boolean resetCancelNextUpFlag(View paramView)
    {
        if ((0x4000000 & paramView.mPrivateFlags) != 0)
            paramView.mPrivateFlags = (0xFBFFFFFF & paramView.mPrivateFlags);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void resetTouchState()
    {
        clearTouchTargets();
        resetCancelNextUpFlag(this);
        this.mGroupFlags = (0xFFF7FFFF & this.mGroupFlags);
    }

    private void setBooleanFlag(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean);
        for (this.mGroupFlags = (paramInt | this.mGroupFlags); ; this.mGroupFlags &= (paramInt ^ 0xFFFFFFFF))
            return;
    }

    public void addChildrenForAccessibility(ArrayList<View> paramArrayList)
    {
        ChildListForAccessibility localChildListForAccessibility = ChildListForAccessibility.obtain(this, true);
        while (true)
        {
            int j;
            try
            {
                int i = localChildListForAccessibility.getChildCount();
                j = 0;
                if (j < i)
                {
                    View localView = localChildListForAccessibility.getChildAt(j);
                    if ((0xC & localView.mViewFlags) != 0)
                        break label80;
                    if (localView.includeForAccessibility())
                        paramArrayList.add(localView);
                    else
                        localView.addChildrenForAccessibility(paramArrayList);
                }
            }
            finally
            {
                localChildListForAccessibility.recycle();
            }
            return;
            label80: j++;
        }
    }

    public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
    {
        int i = paramArrayList.size();
        int j = getDescendantFocusability();
        if ((j != 393216) || ((paramInt2 & 0x2) == 2))
        {
            int k = this.mChildrenCount;
            View[] arrayOfView = this.mChildren;
            for (int m = 0; m < k; m++)
            {
                View localView = arrayOfView[m];
                if ((0xC & localView.mViewFlags) == 0)
                    localView.addFocusables(paramArrayList, paramInt1, paramInt2);
            }
        }
        if ((j != 262144) || (i == paramArrayList.size()) || ((paramInt2 & 0x2) == 2))
            super.addFocusables(paramArrayList, paramInt1, paramInt2);
    }

    public boolean addStatesFromChildren()
    {
        if ((0x2000 & this.mGroupFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void addTouchables(ArrayList<View> paramArrayList)
    {
        super.addTouchables(paramArrayList);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if ((0xC & localView.mViewFlags) == 0)
                localView.addTouchables(paramArrayList);
        }
    }

    public void addView(View paramView)
    {
        addView(paramView, -1);
    }

    public void addView(View paramView, int paramInt)
    {
        LayoutParams localLayoutParams = paramView.getLayoutParams();
        if (localLayoutParams == null)
        {
            localLayoutParams = generateDefaultLayoutParams();
            if (localLayoutParams == null)
                throw new IllegalArgumentException("generateDefaultLayoutParams() cannot return null");
        }
        addView(paramView, paramInt, localLayoutParams);
    }

    public void addView(View paramView, int paramInt1, int paramInt2)
    {
        LayoutParams localLayoutParams = generateDefaultLayoutParams();
        localLayoutParams.width = paramInt1;
        localLayoutParams.height = paramInt2;
        addView(paramView, -1, localLayoutParams);
    }

    public void addView(View paramView, int paramInt, LayoutParams paramLayoutParams)
    {
        requestLayout();
        invalidate(true);
        addViewInner(paramView, paramInt, paramLayoutParams, false);
    }

    public void addView(View paramView, LayoutParams paramLayoutParams)
    {
        addView(paramView, -1, paramLayoutParams);
    }

    protected boolean addViewInLayout(View paramView, int paramInt, LayoutParams paramLayoutParams)
    {
        return addViewInLayout(paramView, paramInt, paramLayoutParams, false);
    }

    protected boolean addViewInLayout(View paramView, int paramInt, LayoutParams paramLayoutParams, boolean paramBoolean)
    {
        paramView.mParent = null;
        addViewInner(paramView, paramInt, paramLayoutParams, paramBoolean);
        paramView.mPrivateFlags = (0x20 | 0xFF9FFFFF & paramView.mPrivateFlags);
        return true;
    }

    protected void attachLayoutAnimationParameters(View paramView, LayoutParams paramLayoutParams, int paramInt1, int paramInt2)
    {
        LayoutAnimationController.AnimationParameters localAnimationParameters = paramLayoutParams.layoutAnimationParameters;
        if (localAnimationParameters == null)
        {
            localAnimationParameters = new LayoutAnimationController.AnimationParameters();
            paramLayoutParams.layoutAnimationParameters = localAnimationParameters;
        }
        localAnimationParameters.count = paramInt2;
        localAnimationParameters.index = paramInt1;
    }

    protected void attachViewToParent(View paramView, int paramInt, LayoutParams paramLayoutParams)
    {
        paramView.mLayoutParams = paramLayoutParams;
        if (paramInt < 0)
            paramInt = this.mChildrenCount;
        addInArray(paramView, paramInt);
        paramView.mParent = this;
        paramView.mPrivateFlags = (0x80000000 | (0x20 | 0xFFFF7FFF & (0xFF9FFFFF & paramView.mPrivateFlags)));
        this.mPrivateFlags = (0x80000000 | this.mPrivateFlags);
        if (paramView.hasFocus())
            requestChildFocus(paramView, paramView.findFocus());
    }

    public void bringChildToFront(View paramView)
    {
        int i = indexOfChild(paramView);
        if (i >= 0)
        {
            removeFromArray(i);
            addInArray(paramView, this.mChildrenCount);
            paramView.mParent = this;
        }
    }

    protected boolean canAnimate()
    {
        if (this.mLayoutAnimationController != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean checkLayoutParams(LayoutParams paramLayoutParams)
    {
        if (paramLayoutParams != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void childAccessibilityStateChanged(View paramView)
    {
        if (this.mParent != null)
            this.mParent.childAccessibilityStateChanged(paramView);
    }

    public void childDrawableStateChanged(View paramView)
    {
        if ((0x2000 & this.mGroupFlags) != 0)
            refreshDrawableState();
    }

    public void childHasTransientStateChanged(View paramView, boolean paramBoolean)
    {
        boolean bool1 = hasTransientState();
        if (paramBoolean)
            this.mChildCountWithTransientState = (1 + this.mChildCountWithTransientState);
        while (true)
        {
            boolean bool2 = hasTransientState();
            if ((this.mParent != null) && (bool1 != bool2));
            try
            {
                this.mParent.childHasTransientStateChanged(this, bool2);
                return;
                this.mChildCountWithTransientState = (-1 + this.mChildCountWithTransientState);
            }
            catch (AbstractMethodError localAbstractMethodError)
            {
                while (true)
                    Log.e("ViewGroup", this.mParent.getClass().getSimpleName() + " does not fully implement ViewParent", localAbstractMethodError);
            }
        }
    }

    protected void cleanupLayoutState(View paramView)
    {
        paramView.mPrivateFlags = (0xFFFFEFFF & paramView.mPrivateFlags);
    }

    public void clearChildFocus(View paramView)
    {
        this.mFocused = null;
        if (this.mParent != null)
            this.mParent.clearChildFocus(this);
    }

    public void clearDisappearingChildren()
    {
        if (this.mDisappearingChildren != null)
        {
            this.mDisappearingChildren.clear();
            invalidate();
        }
    }

    public void clearFocus()
    {
        if (this.mFocused == null)
            super.clearFocus();
        while (true)
        {
            return;
            View localView = this.mFocused;
            this.mFocused = null;
            localView.clearFocus();
        }
    }

    Bitmap createSnapshot(Bitmap.Config paramConfig, int paramInt, boolean paramBoolean)
    {
        int i = this.mChildrenCount;
        int[] arrayOfInt = null;
        if (paramBoolean)
        {
            arrayOfInt = new int[i];
            for (int k = 0; k < i; k++)
            {
                View localView = getChildAt(k);
                arrayOfInt[k] = localView.getVisibility();
                if (arrayOfInt[k] == 0)
                    localView.setVisibility(4);
            }
        }
        Bitmap localBitmap = super.createSnapshot(paramConfig, paramInt, paramBoolean);
        if (paramBoolean)
            for (int j = 0; j < i; j++)
                getChildAt(j).setVisibility(arrayOfInt[j]);
        return localBitmap;
    }

    protected void debug(int paramInt)
    {
        super.debug(paramInt);
        if (this.mFocused != null)
        {
            String str3 = debugIndent(paramInt);
            Log.d("View", str3 + "mFocused");
        }
        if (this.mChildrenCount != 0)
        {
            String str2 = debugIndent(paramInt);
            Log.d("View", str2 + "{");
        }
        int i = this.mChildrenCount;
        for (int j = 0; j < i; j++)
            this.mChildren[j].debug(paramInt + 1);
        if (this.mChildrenCount != 0)
        {
            String str1 = debugIndent(paramInt);
            Log.d("View", str1 + "}");
        }
    }

    protected void detachAllViewsFromParent()
    {
        int i = this.mChildrenCount;
        if (i <= 0);
        while (true)
        {
            return;
            View[] arrayOfView = this.mChildren;
            this.mChildrenCount = 0;
            for (int j = i - 1; j >= 0; j--)
            {
                arrayOfView[j].mParent = null;
                arrayOfView[j] = null;
            }
        }
    }

    protected void detachViewFromParent(int paramInt)
    {
        removeFromArray(paramInt);
    }

    protected void detachViewFromParent(View paramView)
    {
        removeFromArray(indexOfChild(paramView));
    }

    protected void detachViewsFromParent(int paramInt1, int paramInt2)
    {
        removeFromArray(paramInt1, paramInt2);
    }

    void dispatchAttachedToWindow(View.AttachInfo paramAttachInfo, int paramInt)
    {
        this.mGroupFlags = (0x400000 | this.mGroupFlags);
        super.dispatchAttachedToWindow(paramAttachInfo, paramInt);
        this.mGroupFlags = (0xFFBFFFFF & this.mGroupFlags);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            localView.dispatchAttachedToWindow(paramAttachInfo, paramInt | 0xC & localView.mViewFlags);
        }
    }

    void dispatchCollectViewAttributes(View.AttachInfo paramAttachInfo, int paramInt)
    {
        if ((paramInt & 0xC) == 0)
        {
            super.dispatchCollectViewAttributes(paramAttachInfo, paramInt);
            int i = this.mChildrenCount;
            View[] arrayOfView = this.mChildren;
            for (int j = 0; j < i; j++)
            {
                View localView = arrayOfView[j];
                localView.dispatchCollectViewAttributes(paramAttachInfo, paramInt | 0xC & localView.mViewFlags);
            }
        }
    }

    public void dispatchConfigurationChanged(Configuration paramConfiguration)
    {
        super.dispatchConfigurationChanged(paramConfiguration);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchConfigurationChanged(paramConfiguration);
    }

    void dispatchDetachedFromWindow()
    {
        cancelAndClearTouchTargets(null);
        exitHoverTargets();
        this.mLayoutSuppressed = false;
        this.mDragNotifiedChildren = null;
        if (this.mCurrentDrag != null)
        {
            this.mCurrentDrag.recycle();
            this.mCurrentDrag = null;
        }
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchDetachedFromWindow();
        super.dispatchDetachedFromWindow();
    }

    public void dispatchDisplayHint(int paramInt)
    {
        super.dispatchDisplayHint(paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchDisplayHint(paramInt);
    }

    public boolean dispatchDragEvent(DragEvent paramDragEvent)
    {
        boolean bool = false;
        float f1 = paramDragEvent.mX;
        float f2 = paramDragEvent.mY;
        ViewRootImpl localViewRootImpl = getViewRootImpl();
        switch (paramDragEvent.mAction)
        {
        case 5:
        default:
        case 1:
        case 4:
        case 2:
        case 6:
        case 3:
        }
        while (true)
        {
            if (!bool)
                bool = super.dispatchDragEvent(paramDragEvent);
            return bool;
            this.mCurrentDragView = null;
            this.mCurrentDrag = DragEvent.obtain(paramDragEvent);
            if (this.mDragNotifiedChildren == null)
                this.mDragNotifiedChildren = new HashSet();
            while (true)
            {
                this.mChildAcceptsDrag = false;
                int j = this.mChildrenCount;
                View[] arrayOfView = this.mChildren;
                for (int k = 0; k < j; k++)
                {
                    View localView6 = arrayOfView[k];
                    localView6.mPrivateFlags2 = (0xFFFFFFFC & localView6.mPrivateFlags2);
                    if ((localView6.getVisibility() == 0) && (notifyChildOfDrag(arrayOfView[k])))
                        this.mChildAcceptsDrag = true;
                }
                this.mDragNotifiedChildren.clear();
            }
            if (this.mChildAcceptsDrag)
            {
                bool = true;
                continue;
                if (this.mDragNotifiedChildren != null)
                {
                    Iterator localIterator = this.mDragNotifiedChildren.iterator();
                    while (localIterator.hasNext())
                    {
                        View localView5 = (View)localIterator.next();
                        localView5.dispatchDragEvent(paramDragEvent);
                        localView5.mPrivateFlags2 = (0xFFFFFFFC & localView5.mPrivateFlags2);
                        localView5.refreshDrawableState();
                    }
                    this.mDragNotifiedChildren.clear();
                    this.mCurrentDrag.recycle();
                    this.mCurrentDrag = null;
                }
                if (this.mChildAcceptsDrag)
                {
                    bool = true;
                    continue;
                    View localView3 = findFrontmostDroppableChildAt(paramDragEvent.mX, paramDragEvent.mY, this.mLocalPoint);
                    if (this.mCurrentDragView != localView3)
                    {
                        localViewRootImpl.setDragFocus(localView3);
                        int i = paramDragEvent.mAction;
                        if (this.mCurrentDragView != null)
                        {
                            View localView4 = this.mCurrentDragView;
                            paramDragEvent.mAction = 6;
                            localView4.dispatchDragEvent(paramDragEvent);
                            localView4.mPrivateFlags2 = (0xFFFFFFFD & localView4.mPrivateFlags2);
                            localView4.refreshDrawableState();
                        }
                        this.mCurrentDragView = localView3;
                        if (localView3 != null)
                        {
                            paramDragEvent.mAction = 5;
                            localView3.dispatchDragEvent(paramDragEvent);
                            localView3.mPrivateFlags2 = (0x2 | localView3.mPrivateFlags2);
                            localView3.refreshDrawableState();
                        }
                        paramDragEvent.mAction = i;
                    }
                    if (localView3 != null)
                    {
                        paramDragEvent.mX = this.mLocalPoint.x;
                        paramDragEvent.mY = this.mLocalPoint.y;
                        bool = localView3.dispatchDragEvent(paramDragEvent);
                        paramDragEvent.mX = f1;
                        paramDragEvent.mY = f2;
                        continue;
                        if (this.mCurrentDragView != null)
                        {
                            View localView2 = this.mCurrentDragView;
                            localView2.dispatchDragEvent(paramDragEvent);
                            localView2.mPrivateFlags2 = (0xFFFFFFFD & localView2.mPrivateFlags2);
                            localView2.refreshDrawableState();
                            this.mCurrentDragView = null;
                            continue;
                            View localView1 = findFrontmostDroppableChildAt(paramDragEvent.mX, paramDragEvent.mY, this.mLocalPoint);
                            if (localView1 != null)
                            {
                                paramDragEvent.mX = this.mLocalPoint.x;
                                paramDragEvent.mY = this.mLocalPoint.y;
                                bool = localView1.dispatchDragEvent(paramDragEvent);
                                paramDragEvent.mX = f1;
                                paramDragEvent.mY = f2;
                            }
                        }
                    }
                }
            }
        }
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        int j = this.mGroupFlags;
        if (((j & 0x8) != 0) && (canAnimate()))
        {
            int i4;
            if ((0x40 & this.mGroupFlags) == 64)
            {
                i4 = 1;
                if (isHardwareAccelerated())
                    break label136;
            }
            label136: for (int i5 = 1; ; i5 = 0)
            {
                for (int i6 = 0; i6 < i; i6++)
                {
                    View localView3 = arrayOfView[i6];
                    if ((0xC & localView3.mViewFlags) == 0)
                    {
                        attachLayoutAnimationParameters(localView3, localView3.getLayoutParams(), i6, i);
                        bindLayoutAnimation(localView3);
                        if (i4 != 0)
                        {
                            localView3.setDrawingCacheEnabled(true);
                            if (i5 != 0)
                                localView3.buildDrawingCache(true);
                        }
                    }
                }
                i4 = 0;
                break;
            }
            LayoutAnimationController localLayoutAnimationController = this.mLayoutAnimationController;
            if (localLayoutAnimationController.willOverlap())
                this.mGroupFlags = (0x80 | this.mGroupFlags);
            localLayoutAnimationController.start();
            this.mGroupFlags = (0xFFFFFFF7 & this.mGroupFlags);
            this.mGroupFlags = (0xFFFFFFEF & this.mGroupFlags);
            if (i4 != 0)
                this.mGroupFlags = (0x8000 | this.mGroupFlags);
            if (this.mAnimationListener != null)
                this.mAnimationListener.onAnimationStart(localLayoutAnimationController.getAnimation());
        }
        int k = 0;
        if ((j & 0x22) == 34);
        boolean bool;
        long l;
        for (int m = 1; ; m = 0)
        {
            if (m != 0)
            {
                k = paramCanvas.save();
                paramCanvas.clipRect(this.mScrollX + this.mPaddingLeft, this.mScrollY + this.mPaddingTop, this.mScrollX + this.mRight - this.mLeft - this.mPaddingRight, this.mScrollY + this.mBottom - this.mTop - this.mPaddingBottom);
            }
            this.mPrivateFlags = (0xFFFFFFBF & this.mPrivateFlags);
            this.mGroupFlags = (0xFFFFFFFB & this.mGroupFlags);
            bool = false;
            l = getDrawingTime();
            if ((j & 0x400) != 0)
                break;
            for (int i3 = 0; i3 < i; i3++)
            {
                View localView2 = arrayOfView[i3];
                if (((0xC & localView2.mViewFlags) == 0) || (localView2.getAnimation() != null))
                    bool |= drawChild(paramCanvas, localView2, l);
            }
        }
        for (int n = 0; n < i; n++)
        {
            View localView1 = arrayOfView[getChildDrawingOrder(i, n)];
            if (((0xC & localView1.mViewFlags) == 0) || (localView1.getAnimation() != null))
                bool |= drawChild(paramCanvas, localView1, l);
        }
        if (this.mDisappearingChildren != null)
        {
            ArrayList localArrayList = this.mDisappearingChildren;
            for (int i2 = -1 + localArrayList.size(); i2 >= 0; i2--)
                bool |= drawChild(paramCanvas, (View)localArrayList.get(i2), l);
        }
        if (debugDraw())
            onDebugDraw(paramCanvas);
        if (m != 0)
            paramCanvas.restoreToCount(k);
        int i1 = this.mGroupFlags;
        if ((i1 & 0x4) == 4)
            invalidate(true);
        if (((i1 & 0x10) == 0) && ((i1 & 0x200) == 0) && (this.mLayoutAnimationController.isDone()) && (!bool))
        {
            this.mGroupFlags = (0x200 | this.mGroupFlags);
            Runnable local1 = new Runnable()
            {
                public void run()
                {
                    ViewGroup.this.notifyAnimationListener();
                }
            };
            post(local1);
        }
    }

    public void dispatchFinishTemporaryDetach()
    {
        super.dispatchFinishTemporaryDetach();
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchFinishTemporaryDetach();
    }

    protected void dispatchFreezeSelfOnly(SparseArray<Parcelable> paramSparseArray)
    {
        super.dispatchSaveInstanceState(paramSparseArray);
    }

    protected boolean dispatchGenericFocusedEvent(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if ((0x12 & this.mPrivateFlags) == 18)
            bool = super.dispatchGenericFocusedEvent(paramMotionEvent);
        while (true)
        {
            return bool;
            if ((this.mFocused != null) && ((0x10 & this.mFocused.mPrivateFlags) == 16))
                bool = this.mFocused.dispatchGenericMotionEvent(paramMotionEvent);
            else
                bool = false;
        }
    }

    protected boolean dispatchGenericPointerEvent(MotionEvent paramMotionEvent)
    {
        int i = this.mChildrenCount;
        if (i != 0)
        {
            View[] arrayOfView = this.mChildren;
            float f1 = paramMotionEvent.getX();
            float f2 = paramMotionEvent.getY();
            int j = i - 1;
            if (j >= 0)
            {
                View localView = arrayOfView[j];
                if ((!canViewReceivePointerEvents(localView)) || (!isTransformedTouchPointInView(f1, f2, localView, null)));
                while (!dispatchTransformedGenericPointerEvent(paramMotionEvent, localView))
                {
                    j--;
                    break;
                }
            }
        }
        for (boolean bool = true; ; bool = super.dispatchGenericPointerEvent(paramMotionEvent))
            return bool;
    }

    protected void dispatchGetDisplayList()
    {
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        int j = 0;
        if (j < i)
        {
            View localView = arrayOfView[j];
            if ((((0xC & localView.mViewFlags) == 0) || (localView.getAnimation() != null)) && (localView.hasStaticLayer()))
                if ((0x80000000 & localView.mPrivateFlags) != -2147483648)
                    break label106;
            label106: for (boolean bool = true; ; bool = false)
            {
                localView.mRecreateDisplayList = bool;
                localView.mPrivateFlags = (0x7FFFFFFF & localView.mPrivateFlags);
                localView.getDisplayList();
                localView.mRecreateDisplayList = false;
                j++;
                break;
            }
        }
    }

    protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getAction();
        boolean bool1 = onInterceptHoverEvent(paramMotionEvent);
        paramMotionEvent.setAction(i);
        MotionEvent localMotionEvent = paramMotionEvent;
        boolean bool2 = false;
        Object localObject1 = this.mFirstHoverTarget;
        this.mFirstHoverTarget = null;
        Object localObject2;
        View localView2;
        Object localObject3;
        Object localObject4;
        label128: int n;
        if ((!bool1) && (i != 10))
        {
            float f1 = paramMotionEvent.getX();
            float f2 = paramMotionEvent.getY();
            int k = this.mChildrenCount;
            if (k != 0)
            {
                View[] arrayOfView = this.mChildren;
                localObject2 = null;
                int m = k - 1;
                while (m >= 0)
                {
                    localView2 = arrayOfView[m];
                    if ((!canViewReceivePointerEvents(localView2)) || (!isTransformedTouchPointInView(f1, f2, localView2, null)))
                    {
                        m--;
                    }
                    else
                    {
                        localObject3 = localObject1;
                        localObject4 = null;
                        if (localObject3 != null)
                            break label232;
                        localObject3 = HoverTarget.obtain(localView2);
                        n = 0;
                        if (localObject2 == null)
                            break label297;
                        localObject2.next = ((HoverTarget)localObject3);
                        label155: if (i != 9)
                            break label310;
                        if (n == 0)
                            bool2 |= dispatchTransformedGenericPointerEvent(paramMotionEvent, localView2);
                        label178: if (!bool2)
                            break label383;
                    }
                }
            }
        }
        label183: if (localObject1 != null)
        {
            View localView1 = ((HoverTarget)localObject1).child;
            if (i == 10)
                bool2 |= dispatchTransformedGenericPointerEvent(paramMotionEvent, localView1);
            while (true)
            {
                HoverTarget localHoverTarget1 = ((HoverTarget)localObject1).next;
                ((HoverTarget)localObject1).recycle();
                localObject1 = localHoverTarget1;
                break label183;
                label232: if (((HoverTarget)localObject3).child == localView2)
                {
                    if (localObject4 != null)
                    {
                        HoverTarget localHoverTarget2 = ((HoverTarget)localObject3).next;
                        localObject4.next = localHoverTarget2;
                    }
                    while (true)
                    {
                        ((HoverTarget)localObject3).next = null;
                        n = 1;
                        break;
                        localObject1 = ((HoverTarget)localObject3).next;
                    }
                }
                localObject4 = localObject3;
                localObject3 = ((HoverTarget)localObject3).next;
                break label128;
                localObject2 = localObject3;
                this.mFirstHoverTarget = ((HoverTarget)localObject3);
                break label155;
                if (i != 7)
                    break label178;
                if (n == 0)
                {
                    localMotionEvent = obtainMotionEventNoHistoryOrSelf(localMotionEvent);
                    localMotionEvent.setAction(9);
                    boolean bool4 = bool2 | dispatchTransformedGenericPointerEvent(localMotionEvent, localView2);
                    localMotionEvent.setAction(i);
                    bool2 = bool4 | dispatchTransformedGenericPointerEvent(localMotionEvent, localView2);
                    break label178;
                }
                bool2 |= dispatchTransformedGenericPointerEvent(paramMotionEvent, localView2);
                break label178;
                label383: break;
                if (i == 7)
                    dispatchTransformedGenericPointerEvent(paramMotionEvent, localView1);
                localMotionEvent = obtainMotionEventNoHistoryOrSelf(localMotionEvent);
                localMotionEvent.setAction(10);
                dispatchTransformedGenericPointerEvent(localMotionEvent, localView1);
                localMotionEvent.setAction(i);
            }
        }
        label297: label310: int j;
        if (!bool2)
        {
            j = 1;
            if (j != this.mHoveredSelf)
                break label483;
            if (j != 0)
                bool2 |= super.dispatchHoverEvent(paramMotionEvent);
        }
        while (true)
        {
            if (localMotionEvent != paramMotionEvent)
                localMotionEvent.recycle();
            return bool2;
            j = 0;
            break;
            label483: if (this.mHoveredSelf)
            {
                if (i != 10)
                    break label540;
                bool2 |= super.dispatchHoverEvent(paramMotionEvent);
            }
            while (true)
            {
                this.mHoveredSelf = false;
                if (j == 0)
                    break;
                if (i != 9)
                    break label582;
                bool2 |= super.dispatchHoverEvent(paramMotionEvent);
                this.mHoveredSelf = true;
                break;
                label540: if (i == 7)
                    super.dispatchHoverEvent(paramMotionEvent);
                localMotionEvent = obtainMotionEventNoHistoryOrSelf(localMotionEvent);
                localMotionEvent.setAction(10);
                super.dispatchHoverEvent(localMotionEvent);
                localMotionEvent.setAction(i);
            }
            label582: if (i == 7)
            {
                localMotionEvent = obtainMotionEventNoHistoryOrSelf(localMotionEvent);
                localMotionEvent.setAction(9);
                boolean bool3 = bool2 | super.dispatchHoverEvent(localMotionEvent);
                localMotionEvent.setAction(i);
                bool2 = bool3 | super.dispatchHoverEvent(localMotionEvent);
                this.mHoveredSelf = true;
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        int i = 1;
        if (this.mInputEventConsistencyVerifier != null)
            this.mInputEventConsistencyVerifier.onKeyEvent(paramKeyEvent, i);
        if ((0x12 & this.mPrivateFlags) == 18)
            if (!super.dispatchKeyEvent(paramKeyEvent))
                break label73;
        while (true)
        {
            return i;
            if ((this.mFocused == null) || ((0x10 & this.mFocused.mPrivateFlags) != 16) || (!this.mFocused.dispatchKeyEvent(paramKeyEvent)))
            {
                label73: if (this.mInputEventConsistencyVerifier != null)
                    this.mInputEventConsistencyVerifier.onUnhandledEvent(paramKeyEvent, i);
                int j = 0;
            }
        }
    }

    public boolean dispatchKeyEventPreIme(KeyEvent paramKeyEvent)
    {
        boolean bool;
        if ((0x12 & this.mPrivateFlags) == 18)
            bool = super.dispatchKeyEventPreIme(paramKeyEvent);
        while (true)
        {
            return bool;
            if ((this.mFocused != null) && ((0x10 & this.mFocused.mPrivateFlags) == 16))
                bool = this.mFocused.dispatchKeyEventPreIme(paramKeyEvent);
            else
                bool = false;
        }
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
    {
        boolean bool;
        if ((0x12 & this.mPrivateFlags) == 18)
            bool = super.dispatchKeyShortcutEvent(paramKeyEvent);
        while (true)
        {
            return bool;
            if ((this.mFocused != null) && ((0x10 & this.mFocused.mPrivateFlags) == 16))
                bool = this.mFocused.dispatchKeyShortcutEvent(paramKeyEvent);
            else
                bool = false;
        }
    }

    boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent)
    {
        boolean bool3;
        if (includeForAccessibility())
        {
            bool3 = super.dispatchPopulateAccessibilityEventInternal(paramAccessibilityEvent);
            if (!bool3);
        }
        for (boolean bool1 = bool3; ; bool1 = false)
            while (true)
            {
                return bool1;
                ChildListForAccessibility localChildListForAccessibility = ChildListForAccessibility.obtain(this, true);
                try
                {
                    int i = localChildListForAccessibility.getChildCount();
                    int j = 0;
                    if (j < i)
                    {
                        View localView = localChildListForAccessibility.getChildAt(j);
                        if ((0xC & localView.mViewFlags) == 0)
                        {
                            boolean bool2 = localView.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
                            if (bool2)
                            {
                                localChildListForAccessibility.recycle();
                                bool1 = bool2;
                                continue;
                            }
                        }
                        j++;
                    }
                }
                finally
                {
                    localChildListForAccessibility.recycle();
                }
            }
    }

    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray)
    {
        super.dispatchRestoreInstanceState(paramSparseArray);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if ((0x20000000 & localView.mViewFlags) != 536870912)
                localView.dispatchRestoreInstanceState(paramSparseArray);
        }
    }

    protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray)
    {
        super.dispatchSaveInstanceState(paramSparseArray);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if ((0x20000000 & localView.mViewFlags) != 536870912)
                localView.dispatchSaveInstanceState(paramSparseArray);
        }
    }

    void dispatchScreenStateChanged(int paramInt)
    {
        super.dispatchScreenStateChanged(paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchScreenStateChanged(paramInt);
    }

    public void dispatchSetActivated(boolean paramBoolean)
    {
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        for (int j = 0; j < i; j++)
            arrayOfView[j].setActivated(paramBoolean);
    }

    protected void dispatchSetPressed(boolean paramBoolean)
    {
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if ((!paramBoolean) || ((!localView.isClickable()) && (!localView.isLongClickable())))
                localView.setPressed(paramBoolean);
        }
    }

    public void dispatchSetSelected(boolean paramBoolean)
    {
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        for (int j = 0; j < i; j++)
            arrayOfView[j].setSelected(paramBoolean);
    }

    public void dispatchStartTemporaryDetach()
    {
        super.dispatchStartTemporaryDetach();
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchStartTemporaryDetach();
    }

    public void dispatchSystemUiVisibilityChanged(int paramInt)
    {
        super.dispatchSystemUiVisibilityChanged(paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchSystemUiVisibilityChanged(paramInt);
    }

    protected void dispatchThawSelfOnly(SparseArray<Parcelable> paramSparseArray)
    {
        super.dispatchRestoreInstanceState(paramSparseArray);
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mInputEventConsistencyVerifier != null)
            this.mInputEventConsistencyVerifier.onTouchEvent(paramMotionEvent, 1);
        boolean bool1 = false;
        int j;
        boolean bool2;
        label94: label110: int m;
        label123: TouchTarget localTouchTarget1;
        int n;
        if (onFilterTouchEventForSecurity(paramMotionEvent))
        {
            int i = paramMotionEvent.getAction();
            j = i & 0xFF;
            if (j == 0)
            {
                cancelAndClearTouchTargets(paramMotionEvent);
                resetTouchState();
            }
            int k;
            boolean bool3;
            int i1;
            if ((j == 0) || (this.mFirstTouchTarget != null))
                if ((0x80000 & this.mGroupFlags) != 0)
                {
                    k = 1;
                    if (k != 0)
                        break label274;
                    bool2 = onInterceptTouchEvent(paramMotionEvent);
                    paramMotionEvent.setAction(i);
                    if ((!resetCancelNextUpFlag(this)) && (j != 3))
                        break label286;
                    bool3 = true;
                    if ((0x200000 & this.mGroupFlags) == 0)
                        break label292;
                    m = 1;
                    localTouchTarget1 = null;
                    n = 0;
                    if ((bool3) || (bool2) || ((j != 0) && ((m == 0) || (j != 5)) && (j != 7)))
                        break label453;
                    i1 = paramMotionEvent.getActionIndex();
                    if (m == 0)
                        break label298;
                }
            int i5;
            View localView;
            label274: label286: label292: label298: for (int i2 = 1 << paramMotionEvent.getPointerId(i1); ; i2 = -1)
            {
                removePointersFromTouchTargets(i2);
                int i3 = this.mChildrenCount;
                if (i3 == 0)
                    break label335;
                View[] arrayOfView = this.mChildren;
                float f1 = paramMotionEvent.getX(i1);
                float f2 = paramMotionEvent.getY(i1);
                for (i5 = i3 - 1; ; i5--)
                {
                    if (i5 < 0)
                        break label335;
                    localView = arrayOfView[i5];
                    if ((canViewReceivePointerEvents(localView)) && (isTransformedTouchPointInView(f1, f2, localView, null)))
                        break;
                }
                k = 0;
                break;
                bool2 = false;
                break label94;
                bool2 = true;
                break label94;
                bool3 = false;
                break label110;
                m = 0;
                break label123;
            }
            localTouchTarget1 = getTouchTarget(localView);
            if (localTouchTarget1 != null)
            {
                int i6 = i2 | localTouchTarget1.pointerIdBits;
                localTouchTarget1.pointerIdBits = i6;
            }
            while (true)
                label335: if ((localTouchTarget1 == null) && (this.mFirstTouchTarget != null))
                {
                    localTouchTarget1 = this.mFirstTouchTarget;
                    while (true)
                        if (localTouchTarget1.next != null)
                        {
                            localTouchTarget1 = localTouchTarget1.next;
                            continue;
                            resetCancelNextUpFlag(localView);
                            if (!dispatchTransformedTouchEvent(paramMotionEvent, false, localView, i2))
                                break;
                            this.mLastTouchDownTime = paramMotionEvent.getDownTime();
                            this.mLastTouchDownIndex = i5;
                            this.mLastTouchDownX = paramMotionEvent.getX();
                            this.mLastTouchDownY = paramMotionEvent.getY();
                            localTouchTarget1 = addTouchTarget(localView, i2);
                            n = 1;
                            break label335;
                        }
                    int i4 = i2 | localTouchTarget1.pointerIdBits;
                    localTouchTarget1.pointerIdBits = i4;
                }
            label453: if (this.mFirstTouchTarget != null)
                break label515;
            bool1 = dispatchTransformedTouchEvent(paramMotionEvent, bool3, null, -1);
            if ((!bool3) && (j != 1) && (j != 7))
                break label646;
            resetTouchState();
        }
        while (true)
        {
            if ((!bool1) && (this.mInputEventConsistencyVerifier != null))
                this.mInputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, 1);
            return bool1;
            label515: Object localObject1 = null;
            Object localObject2 = this.mFirstTouchTarget;
            label524: TouchTarget localTouchTarget2;
            boolean bool4;
            if (localObject2 != null)
            {
                localTouchTarget2 = ((TouchTarget)localObject2).next;
                if ((n != 0) && (localObject2 == localTouchTarget1))
                    bool1 = true;
                label580: 
                do
                {
                    localObject1 = localObject2;
                    localObject2 = localTouchTarget2;
                    break;
                    if ((!resetCancelNextUpFlag(((TouchTarget)localObject2).child)) && (!bool2))
                        break label630;
                    bool4 = true;
                    if (dispatchTransformedTouchEvent(paramMotionEvent, bool4, ((TouchTarget)localObject2).child, ((TouchTarget)localObject2).pointerIdBits))
                        bool1 = true;
                }
                while (!bool4);
                if (localObject1 != null)
                    break label636;
                this.mFirstTouchTarget = localTouchTarget2;
            }
            while (true)
            {
                ((TouchTarget)localObject2).recycle();
                localObject2 = localTouchTarget2;
                break label524;
                break;
                label630: bool4 = false;
                break label580;
                label636: localObject1.next = localTouchTarget2;
            }
            label646: if ((m != 0) && (j == 6))
                removePointersFromTouchTargets(1 << paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex()));
        }
    }

    public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
    {
        int i = 1;
        if (this.mInputEventConsistencyVerifier != null)
            this.mInputEventConsistencyVerifier.onTrackballEvent(paramMotionEvent, i);
        if ((0x12 & this.mPrivateFlags) == 18)
            if (!super.dispatchTrackballEvent(paramMotionEvent))
                break label73;
        while (true)
        {
            return i;
            if ((this.mFocused == null) || ((0x10 & this.mFocused.mPrivateFlags) != 16) || (!this.mFocused.dispatchTrackballEvent(paramMotionEvent)))
            {
                label73: if (this.mInputEventConsistencyVerifier != null)
                    this.mInputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, i);
                int j = 0;
            }
        }
    }

    public boolean dispatchUnhandledMove(View paramView, int paramInt)
    {
        if ((this.mFocused != null) && (this.mFocused.dispatchUnhandledMove(paramView, paramInt)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void dispatchVisibilityChanged(View paramView, int paramInt)
    {
        super.dispatchVisibilityChanged(paramView, paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchVisibilityChanged(paramView, paramInt);
    }

    public void dispatchWindowFocusChanged(boolean paramBoolean)
    {
        super.dispatchWindowFocusChanged(paramBoolean);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchWindowFocusChanged(paramBoolean);
    }

    public void dispatchWindowSystemUiVisiblityChanged(int paramInt)
    {
        super.dispatchWindowSystemUiVisiblityChanged(paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchWindowSystemUiVisiblityChanged(paramInt);
    }

    public void dispatchWindowVisibilityChanged(int paramInt)
    {
        super.dispatchWindowVisibilityChanged(paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].dispatchWindowVisibilityChanged(paramInt);
    }

    protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
    {
        return paramView.draw(paramCanvas, this, paramLong);
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if ((0x10000 & this.mGroupFlags) != 0)
        {
            if ((0x2000 & this.mGroupFlags) != 0)
                throw new IllegalStateException("addStateFromChildren cannot be enabled if a child has duplicateParentState set to true");
            View[] arrayOfView = this.mChildren;
            int i = this.mChildrenCount;
            for (int j = 0; j < i; j++)
            {
                View localView = arrayOfView[j];
                if ((0x400000 & localView.mViewFlags) != 0)
                    localView.refreshDrawableState();
            }
        }
    }

    public void endViewTransition(View paramView)
    {
        if (this.mTransitioningViews != null)
        {
            this.mTransitioningViews.remove(paramView);
            ArrayList localArrayList = this.mDisappearingChildren;
            if ((localArrayList != null) && (localArrayList.contains(paramView)))
            {
                localArrayList.remove(paramView);
                if ((this.mVisibilityChangingChildren == null) || (!this.mVisibilityChangingChildren.contains(paramView)))
                    break label71;
                this.mVisibilityChangingChildren.remove(paramView);
            }
        }
        while (true)
        {
            invalidate();
            return;
            label71: if (paramView.mAttachInfo != null)
                paramView.dispatchDetachedFromWindow();
            if (paramView.mParent != null)
                paramView.mParent = null;
        }
    }

    public View findFocus()
    {
        if (isFocused());
        while (true)
        {
            return this;
            if (this.mFocused != null)
                this = this.mFocused.findFocus();
            else
                this = null;
        }
    }

    View findFrontmostDroppableChildAt(float paramFloat1, float paramFloat2, PointF paramPointF)
    {
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        int j = i - 1;
        View localView;
        if (j >= 0)
        {
            localView = arrayOfView[j];
            if (!localView.canAcceptDrag());
            while (!isTransformedTouchPointInView(paramFloat1, paramFloat2, localView, paramPointF))
            {
                j--;
                break;
            }
        }
        while (true)
        {
            return localView;
            localView = null;
        }
    }

    View findViewByAccessibilityIdTraversal(int paramInt)
    {
        View localView1 = super.findViewByAccessibilityIdTraversal(paramInt);
        Object localObject;
        if (localView1 != null)
            localObject = localView1;
        while (true)
        {
            return localObject;
            int i = this.mChildrenCount;
            View[] arrayOfView = this.mChildren;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label65;
                View localView2 = arrayOfView[j].findViewByAccessibilityIdTraversal(paramInt);
                if (localView2 != null)
                {
                    localObject = localView2;
                    break;
                }
            }
            label65: localObject = null;
        }
    }

    protected View findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView)
    {
        if (paramPredicate.apply(this));
        while (true)
        {
            return this;
            View[] arrayOfView = this.mChildren;
            int i = this.mChildrenCount;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label81;
                View localView1 = arrayOfView[j];
                if ((localView1 != paramView) && ((0x8 & localView1.mPrivateFlags) == 0))
                {
                    View localView2 = localView1.findViewByPredicate(paramPredicate);
                    if (localView2 != null)
                    {
                        this = localView2;
                        break;
                    }
                }
            }
            label81: this = null;
        }
    }

    public View findViewToTakeAccessibilityFocusFromHover(View paramView1, View paramView2)
    {
        if ((includeForAccessibility()) && (isActionableForAccessibility()));
        while (true)
        {
            return this;
            if (this.mParent != null)
                this = this.mParent.findViewToTakeAccessibilityFocusFromHover(this, paramView2);
            else
                this = null;
        }
    }

    protected View findViewTraversal(int paramInt)
    {
        if (paramInt == this.mID);
        while (true)
        {
            return this;
            View[] arrayOfView = this.mChildren;
            int i = this.mChildrenCount;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label71;
                View localView1 = arrayOfView[j];
                if ((0x8 & localView1.mPrivateFlags) == 0)
                {
                    View localView2 = localView1.findViewById(paramInt);
                    if (localView2 != null)
                    {
                        this = localView2;
                        break;
                    }
                }
            }
            label71: this = null;
        }
    }

    protected View findViewWithTagTraversal(Object paramObject)
    {
        if ((paramObject != null) && (paramObject.equals(this.mTag)));
        while (true)
        {
            return this;
            View[] arrayOfView = this.mChildren;
            int i = this.mChildrenCount;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label78;
                View localView1 = arrayOfView[j];
                if ((0x8 & localView1.mPrivateFlags) == 0)
                {
                    View localView2 = localView1.findViewWithTag(paramObject);
                    if (localView2 != null)
                    {
                        this = localView2;
                        break;
                    }
                }
            }
            label78: this = null;
        }
    }

    public void findViewsWithText(ArrayList<View> paramArrayList, CharSequence paramCharSequence, int paramInt)
    {
        super.findViewsWithText(paramArrayList, paramCharSequence, paramInt);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if (((0xC & localView.mViewFlags) == 0) && ((0x8 & localView.mPrivateFlags) == 0))
                localView.findViewsWithText(paramArrayList, paramCharSequence, paramInt);
        }
    }

    void finishAnimatingView(View paramView, Animation paramAnimation)
    {
        ArrayList localArrayList = this.mDisappearingChildren;
        if ((localArrayList != null) && (localArrayList.contains(paramView)))
        {
            localArrayList.remove(paramView);
            if (paramView.mAttachInfo != null)
                paramView.dispatchDetachedFromWindow();
            paramView.clearAnimation();
            this.mGroupFlags = (0x4 | this.mGroupFlags);
        }
        if ((paramAnimation != null) && (!paramAnimation.getFillAfter()))
            paramView.clearAnimation();
        if ((0x10000 & paramView.mPrivateFlags) == 65536)
        {
            paramView.onAnimationEnd();
            paramView.mPrivateFlags = (0xFFFEFFFF & paramView.mPrivateFlags);
            this.mGroupFlags = (0x4 | this.mGroupFlags);
        }
    }

    protected boolean fitSystemWindows(Rect paramRect)
    {
        boolean bool = super.fitSystemWindows(paramRect);
        int i;
        View[] arrayOfView;
        if (!bool)
        {
            i = this.mChildrenCount;
            arrayOfView = this.mChildren;
        }
        for (int j = 0; ; j++)
            if (j < i)
            {
                bool = arrayOfView[j].fitSystemWindows(paramRect);
                if (!bool);
            }
            else
            {
                return bool;
            }
    }

    public View focusSearch(View paramView, int paramInt)
    {
        View localView;
        if ((isRootNamespace()) && ((paramInt & 0x1000) == 0))
            localView = FocusFinder.getInstance().findNextFocus(this, paramView, paramInt);
        while (true)
        {
            return localView;
            if (this.mParent != null)
                localView = this.mParent.focusSearch(paramView, paramInt);
            else
                localView = null;
        }
    }

    public void focusableViewAvailable(View paramView)
    {
        if ((this.mParent != null) && (getDescendantFocusability() != 393216) && ((!isFocused()) || (getDescendantFocusability() == 262144)))
            this.mParent.focusableViewAvailable(paramView);
    }

    public boolean gatherTransparentRegion(Region paramRegion)
    {
        boolean bool1 = false;
        boolean bool2 = true;
        boolean bool3;
        if ((0x200 & this.mPrivateFlags) == 0)
        {
            bool3 = bool2;
            if ((!bool3) || (paramRegion != null))
                break label35;
        }
        while (true)
        {
            return bool2;
            bool3 = false;
            break;
            label35: super.gatherTransparentRegion(paramRegion);
            View[] arrayOfView = this.mChildren;
            int i = this.mChildrenCount;
            int j = 1;
            for (int k = 0; k < i; k++)
            {
                View localView = arrayOfView[k];
                if ((((0xC & localView.mViewFlags) == 0) || (localView.getAnimation() != null)) && (!localView.gatherTransparentRegion(paramRegion)))
                    j = 0;
            }
            if ((bool3) || (j != 0))
                bool1 = bool2;
            bool2 = bool1;
        }
    }

    protected LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-2, -2);
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    protected LayoutParams generateLayoutParams(LayoutParams paramLayoutParams)
    {
        return paramLayoutParams;
    }

    public View getChildAt(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mChildrenCount));
        for (View localView = null; ; localView = this.mChildren[paramInt])
            return localView;
    }

    public int getChildCount()
    {
        return this.mChildrenCount;
    }

    protected int getChildDrawingOrder(int paramInt1, int paramInt2)
    {
        return paramInt2;
    }

    protected boolean getChildStaticTransformation(View paramView, Transformation paramTransformation)
    {
        return false;
    }

    public boolean getChildVisibleRect(View paramView, Rect paramRect, Point paramPoint)
    {
        boolean bool = true;
        RectF localRectF;
        float[] arrayOfFloat;
        if (this.mAttachInfo != null)
        {
            localRectF = this.mAttachInfo.mTmpTransformRect;
            localRectF.set(paramRect);
            if (!paramView.hasIdentityMatrix())
                paramView.getMatrix().mapRect(localRectF);
            int i = paramView.mLeft - this.mScrollX;
            int j = paramView.mTop - this.mScrollY;
            localRectF.offset(i, j);
            if (paramPoint != null)
            {
                if (!paramView.hasIdentityMatrix())
                {
                    if (this.mAttachInfo == null)
                        break label231;
                    arrayOfFloat = this.mAttachInfo.mTmpTransformLocation;
                    label102: arrayOfFloat[0] = paramPoint.x;
                    arrayOfFloat[bool] = paramPoint.y;
                    paramView.getMatrix().mapPoints(arrayOfFloat);
                    paramPoint.x = ((int)(0.5F + arrayOfFloat[0]));
                    paramPoint.y = ((int)(0.5F + arrayOfFloat[bool]));
                }
                paramPoint.x = (i + paramPoint.x);
                paramPoint.y = (j + paramPoint.y);
            }
            if (!localRectF.intersect(0.0F, 0.0F, this.mRight - this.mLeft, this.mBottom - this.mTop))
                break label300;
            if (this.mParent != null)
                break label239;
        }
        while (true)
        {
            return bool;
            localRectF = new RectF();
            break;
            label231: arrayOfFloat = new float[2];
            break label102;
            label239: paramRect.set((int)(0.5F + localRectF.left), (int)(0.5F + localRectF.top), (int)(0.5F + localRectF.right), (int)(0.5F + localRectF.bottom));
            bool = this.mParent.getChildVisibleRect(this, paramRect, paramPoint);
            continue;
            label300: bool = false;
        }
    }

    @ViewDebug.ExportedProperty(category="focus", mapping={@ViewDebug.IntToString(from=131072, to="FOCUS_BEFORE_DESCENDANTS"), @ViewDebug.IntToString(from=262144, to="FOCUS_AFTER_DESCENDANTS"), @ViewDebug.IntToString(from=393216, to="FOCUS_BLOCK_DESCENDANTS")})
    public int getDescendantFocusability()
    {
        return 0x60000 & this.mGroupFlags;
    }

    public View getFocusedChild()
    {
        return this.mFocused;
    }

    public LayoutAnimationController getLayoutAnimation()
    {
        return this.mLayoutAnimationController;
    }

    public Animation.AnimationListener getLayoutAnimationListener()
    {
        return this.mAnimationListener;
    }

    public int getLayoutMode()
    {
        return this.mLayoutMode;
    }

    public LayoutTransition getLayoutTransition()
    {
        return this.mTransition;
    }

    @ViewDebug.ExportedProperty(category="drawing", mapping={@ViewDebug.IntToString(from=0, to="NONE"), @ViewDebug.IntToString(from=1, to="ANIMATION"), @ViewDebug.IntToString(from=2, to="SCROLLING"), @ViewDebug.IntToString(from=3, to="ALL")})
    public int getPersistentDrawingCache()
    {
        return this.mPersistentDrawingCache;
    }

    void handleFocusGainInternal(int paramInt, Rect paramRect)
    {
        if (this.mFocused != null)
        {
            this.mFocused.unFocus();
            this.mFocused = null;
        }
        super.handleFocusGainInternal(paramInt, paramRect);
    }

    public boolean hasFocus()
    {
        if (((0x2 & this.mPrivateFlags) != 0) || (this.mFocused != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasFocusable()
    {
        boolean bool = false;
        if ((0xC & this.mViewFlags) != 0);
        label73: 
        while (true)
        {
            return bool;
            if (isFocusable())
            {
                bool = true;
            }
            else if (getDescendantFocusability() != 393216)
            {
                int i = this.mChildrenCount;
                View[] arrayOfView = this.mChildren;
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label73;
                    if (arrayOfView[j].hasFocusable())
                    {
                        bool = true;
                        break;
                    }
                }
            }
        }
    }

    protected boolean hasHoveredChild()
    {
        if (this.mFirstHoverTarget != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasTransientState()
    {
        if ((this.mChildCountWithTransientState > 0) || (super.hasTransientState()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int indexOfChild(View paramView)
    {
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        int j = 0;
        if (j < i)
            if (arrayOfView[j] != paramView);
        while (true)
        {
            return j;
            j++;
            break;
            j = -1;
        }
    }

    public final void invalidateChild(View paramView, Rect paramRect)
    {
        Object localObject = this;
        View.AttachInfo localAttachInfo = this.mAttachInfo;
        int i;
        int j;
        label64: int k;
        label73: int[] arrayOfInt;
        label208: View localView;
        if (localAttachInfo != null)
        {
            if ((0x40 & paramView.mPrivateFlags) != 64)
                break label407;
            i = 1;
            Matrix localMatrix1 = paramView.getMatrix();
            if ((!paramView.isOpaque()) || (i != 0) || (paramView.getAnimation() != null) || (!localMatrix1.isIdentity()))
                break label413;
            j = 1;
            if (j == 0)
                break label419;
            k = 4194304;
            if (paramView.mLayerType != 0)
            {
                this.mPrivateFlags = (0x80000000 | this.mPrivateFlags);
                this.mPrivateFlags = (0xFFFF7FFF & this.mPrivateFlags);
                paramView.mLocalDirtyRect.union(paramRect);
            }
            arrayOfInt = localAttachInfo.mInvalidateChildLocation;
            arrayOfInt[0] = paramView.mLeft;
            arrayOfInt[1] = paramView.mTop;
            if (!localMatrix1.isIdentity())
            {
                RectF localRectF2 = localAttachInfo.mTmpTransformRect;
                localRectF2.set(paramRect);
                localMatrix1.mapRect(localRectF2);
                paramRect.set((int)(localRectF2.left - 0.5F), (int)(localRectF2.top - 0.5F), (int)(0.5F + localRectF2.right), (int)(0.5F + localRectF2.bottom));
            }
            localView = null;
            if ((localObject instanceof View))
                localView = (View)localObject;
            if (i != 0)
            {
                if (localView == null)
                    break label426;
                localView.mPrivateFlags = (0x40 | localView.mPrivateFlags);
            }
        }
        while (true)
        {
            if (localView != null)
            {
                if (((0x3000 & localView.mViewFlags) != 0) && (localView.getSolidColor() == 0))
                    k = 2097152;
                if ((0x600000 & localView.mPrivateFlags) != 2097152)
                    localView.mPrivateFlags = (k | 0xFF9FFFFF & localView.mPrivateFlags);
            }
            localObject = ((ViewManager)localObject).invalidateChildInParent(arrayOfInt, paramRect);
            if (localView != null)
            {
                Matrix localMatrix2 = localView.getMatrix();
                if (!localMatrix2.isIdentity())
                {
                    RectF localRectF1 = localAttachInfo.mTmpTransformRect;
                    localRectF1.set(paramRect);
                    localMatrix2.mapRect(localRectF1);
                    paramRect.set((int)(localRectF1.left - 0.5F), (int)(localRectF1.top - 0.5F), (int)(0.5F + localRectF1.right), (int)(0.5F + localRectF1.bottom));
                }
            }
            if (localObject != null)
                break label208;
            return;
            label407: i = 0;
            break;
            label413: j = 0;
            break label64;
            label419: k = 2097152;
            break label73;
            label426: if ((localObject instanceof ViewRootImpl))
                ((ViewRootImpl)localObject).mIsAnimating = true;
        }
    }

    public void invalidateChildFast(View paramView, Rect paramRect)
    {
        Object localObject = this;
        View.AttachInfo localAttachInfo = this.mAttachInfo;
        int i;
        int j;
        if (localAttachInfo != null)
        {
            if (paramView.mLayerType != 0)
                paramView.mLocalDirtyRect.union(paramRect);
            i = paramView.mLeft;
            j = paramView.mTop;
            if (!paramView.getMatrix().isIdentity())
                paramView.transformRect(paramRect);
        }
        while (true)
        {
            ViewGroup localViewGroup;
            if ((localObject instanceof ViewGroup))
            {
                localViewGroup = (ViewGroup)localObject;
                if (localViewGroup.mLayerType != 0)
                {
                    localViewGroup.invalidate();
                    localObject = null;
                }
            }
            while (localObject == null)
            {
                return;
                localObject = localViewGroup.invalidateChildInParentFast(i, j, paramRect);
                i = localViewGroup.mLeft;
                j = localViewGroup.mTop;
                continue;
                int[] arrayOfInt = localAttachInfo.mInvalidateChildLocation;
                arrayOfInt[0] = i;
                arrayOfInt[1] = j;
                localObject = ((ViewManager)localObject).invalidateChildInParent(arrayOfInt, paramRect);
            }
        }
    }

    public ViewParent invalidateChildInParent(int[] paramArrayOfInt, Rect paramRect)
    {
        ViewParent localViewParent;
        if (((0x20 & this.mPrivateFlags) == 32) || ((0x8000 & this.mPrivateFlags) == 32768))
            if ((0x90 & this.mGroupFlags) != 128)
            {
                paramRect.offset(paramArrayOfInt[0] - this.mScrollX, paramArrayOfInt[1] - this.mScrollY);
                int i = this.mLeft;
                int j = this.mTop;
                if (((0x1 & this.mGroupFlags) == 1) && (!paramRect.intersect(0, 0, this.mRight - i, this.mBottom - j)))
                    paramRect.setEmpty();
                this.mPrivateFlags = (0xFFFF7FFF & this.mPrivateFlags);
                paramArrayOfInt[0] = i;
                paramArrayOfInt[1] = j;
                if (this.mLayerType != 0)
                {
                    this.mPrivateFlags = (0x80000000 | this.mPrivateFlags);
                    this.mLocalDirtyRect.union(paramRect);
                }
                localViewParent = this.mParent;
            }
        while (true)
        {
            return localViewParent;
            this.mPrivateFlags = (0xFFFF7FDF & this.mPrivateFlags);
            paramArrayOfInt[0] = this.mLeft;
            paramArrayOfInt[1] = this.mTop;
            if ((0x1 & this.mGroupFlags) == 1)
                paramRect.set(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
            while (true)
            {
                if (this.mLayerType != 0)
                {
                    this.mPrivateFlags = (0x80000000 | this.mPrivateFlags);
                    this.mLocalDirtyRect.union(paramRect);
                }
                localViewParent = this.mParent;
                break;
                paramRect.union(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
            }
            localViewParent = null;
        }
    }

    @ViewDebug.ExportedProperty(category="drawing")
    public boolean isAlwaysDrawnWithCacheEnabled()
    {
        if ((0x4000 & this.mGroupFlags) == 16384);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @ViewDebug.ExportedProperty
    public boolean isAnimationCacheEnabled()
    {
        if ((0x40 & this.mGroupFlags) == 64);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @ViewDebug.ExportedProperty(category="drawing")
    protected boolean isChildrenDrawingOrderEnabled()
    {
        if ((0x400 & this.mGroupFlags) == 1024);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @ViewDebug.ExportedProperty(category="drawing")
    protected boolean isChildrenDrawnWithCacheEnabled()
    {
        if ((0x8000 & this.mGroupFlags) == 32768);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isMotionEventSplittingEnabled()
    {
        if ((0x200000 & this.mGroupFlags) == 2097152);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean isTransformedTouchPointInView(float paramFloat1, float paramFloat2, View paramView, PointF paramPointF)
    {
        float f1 = paramFloat1 + this.mScrollX - paramView.mLeft;
        float f2 = paramFloat2 + this.mScrollY - paramView.mTop;
        if ((!paramView.hasIdentityMatrix()) && (this.mAttachInfo != null))
        {
            float[] arrayOfFloat = this.mAttachInfo.mTmpTransformLocation;
            arrayOfFloat[0] = f1;
            arrayOfFloat[1] = f2;
            paramView.getInverseMatrix().mapPoints(arrayOfFloat);
            f1 = arrayOfFloat[0];
            f2 = arrayOfFloat[1];
        }
        boolean bool = paramView.pointInView(f1, f2);
        if ((bool) && (paramPointF != null))
            paramPointF.set(f1, f2);
        return bool;
    }

    boolean isViewTransitioning(View paramView)
    {
        if ((this.mTransitioningViews != null) && (this.mTransitioningViews.contains(paramView)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        for (int j = 0; j < i; j++)
            arrayOfView[j].jumpDrawablesToCurrentState();
    }

    public final void layout(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((this.mTransition == null) || (!this.mTransition.isChangingLayout()))
        {
            if (this.mTransition != null)
                this.mTransition.layoutChange(this);
            super.layout(paramInt1, paramInt2, paramInt3, paramInt4);
        }
        while (true)
        {
            return;
            this.mLayoutSuppressed = true;
        }
    }

    public void makeOptionalFitsSystemWindows()
    {
        super.makeOptionalFitsSystemWindows();
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            arrayOfView[j].makeOptionalFitsSystemWindows();
    }

    protected void measureChild(View paramView, int paramInt1, int paramInt2)
    {
        LayoutParams localLayoutParams = paramView.getLayoutParams();
        paramView.measure(getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight, localLayoutParams.width), getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom, localLayoutParams.height));
    }

    protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        MarginLayoutParams localMarginLayoutParams = (MarginLayoutParams)paramView.getLayoutParams();
        paramView.measure(getChildMeasureSpec(paramInt1, paramInt2 + (this.mPaddingLeft + this.mPaddingRight + localMarginLayoutParams.leftMargin + localMarginLayoutParams.rightMargin), localMarginLayoutParams.width), getChildMeasureSpec(paramInt3, paramInt4 + (this.mPaddingTop + this.mPaddingBottom + localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin), localMarginLayoutParams.height));
    }

    protected void measureChildren(int paramInt1, int paramInt2)
    {
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if ((0xC & localView.mViewFlags) != 8)
                measureChild(localView, paramInt1, paramInt2);
        }
    }

    boolean notifyChildOfDrag(View paramView)
    {
        boolean bool = false;
        if (!this.mDragNotifiedChildren.contains(paramView))
        {
            this.mDragNotifiedChildren.add(paramView);
            bool = paramView.dispatchDragEvent(this.mCurrentDrag);
            if ((bool) && (!paramView.canAcceptDrag()))
            {
                paramView.mPrivateFlags2 = (0x1 | paramView.mPrivateFlags2);
                paramView.refreshDrawableState();
            }
        }
        return bool;
    }

    public void offsetChildrenTopAndBottom(int paramInt)
    {
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            localView.mTop = (paramInt + localView.mTop);
            localView.mBottom = (paramInt + localView.mBottom);
            if (localView.mDisplayList != null)
            {
                localView.mDisplayList.offsetTopBottom(paramInt);
                invalidateViewProperty(false, false);
            }
        }
    }

    public final void offsetDescendantRectToMyCoords(View paramView, Rect paramRect)
    {
        offsetRectBetweenParentAndChild(paramView, paramRect, true, false);
    }

    void offsetRectBetweenParentAndChild(View paramView, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramView == this);
        while (true)
        {
            return;
            ViewParent localViewParent = paramView.mParent;
            if ((localViewParent != null) && ((localViewParent instanceof View)) && (localViewParent != this))
            {
                if (paramBoolean1)
                {
                    paramRect.offset(paramView.mLeft - paramView.mScrollX, paramView.mTop - paramView.mScrollY);
                    if (paramBoolean2)
                    {
                        View localView2 = (View)localViewParent;
                        paramRect.intersect(0, 0, localView2.mRight - localView2.mLeft, localView2.mBottom - localView2.mTop);
                    }
                }
                while (true)
                {
                    paramView = (View)localViewParent;
                    localViewParent = paramView.mParent;
                    break;
                    if (paramBoolean2)
                    {
                        View localView1 = (View)localViewParent;
                        paramRect.intersect(0, 0, localView1.mRight - localView1.mLeft, localView1.mBottom - localView1.mTop);
                    }
                    paramRect.offset(paramView.mScrollX - paramView.mLeft, paramView.mScrollY - paramView.mTop);
                }
            }
            if (localViewParent != this)
                break;
            if (paramBoolean1)
                paramRect.offset(paramView.mLeft - paramView.mScrollX, paramView.mTop - paramView.mScrollY);
            else
                paramRect.offset(paramView.mScrollX - paramView.mLeft, paramView.mScrollY - paramView.mTop);
        }
        throw new IllegalArgumentException("parameter must be a descendant of this view");
    }

    public final void offsetRectIntoDescendantCoords(View paramView, Rect paramRect)
    {
        offsetRectBetweenParentAndChild(paramView, paramRect, false, false);
    }

    protected void onAnimationEnd()
    {
        super.onAnimationEnd();
        if ((0x40 & this.mGroupFlags) == 64)
        {
            this.mGroupFlags = (0xFFFF7FFF & this.mGroupFlags);
            if ((0x1 & this.mPersistentDrawingCache) == 0)
                setChildrenDrawingCacheEnabled(false);
        }
    }

    protected void onAnimationStart()
    {
        super.onAnimationStart();
        if ((0x40 & this.mGroupFlags) == 64)
        {
            int i = this.mChildrenCount;
            View[] arrayOfView = this.mChildren;
            if (!isHardwareAccelerated());
            for (int j = 1; ; j = 0)
                for (int k = 0; k < i; k++)
                {
                    View localView = arrayOfView[k];
                    if ((0xC & localView.mViewFlags) == 0)
                    {
                        localView.setDrawingCacheEnabled(true);
                        if (j != 0)
                            localView.buildDrawingCache(true);
                    }
                }
            this.mGroupFlags = (0x8000 | this.mGroupFlags);
        }
    }

    protected void onChildVisibilityChanged(View paramView, int paramInt1, int paramInt2)
    {
        if (this.mTransition != null)
        {
            if (paramInt2 != 0)
                break label39;
            this.mTransition.showChild(this, paramView, paramInt1);
        }
        while (true)
        {
            if ((this.mCurrentDrag != null) && (paramInt2 == 0))
                notifyChildOfDrag(paramView);
            return;
            label39: this.mTransition.hideChild(this, paramView, paramInt2);
            if ((this.mTransitioningViews != null) && (this.mTransitioningViews.contains(paramView)))
            {
                if (this.mVisibilityChangingChildren == null)
                    this.mVisibilityChangingChildren = new ArrayList();
                this.mVisibilityChangingChildren.add(paramView);
                addDisappearingView(paramView);
            }
        }
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt1;
        if ((0x2000 & this.mGroupFlags) == 0)
            arrayOfInt1 = super.onCreateDrawableState(paramInt);
        while (true)
        {
            return arrayOfInt1;
            int i = 0;
            int j = getChildCount();
            for (int k = 0; k < j; k++)
            {
                int[] arrayOfInt3 = getChildAt(k).getDrawableState();
                if (arrayOfInt3 != null)
                    i += arrayOfInt3.length;
            }
            arrayOfInt1 = super.onCreateDrawableState(paramInt + i);
            for (int m = 0; m < j; m++)
            {
                int[] arrayOfInt2 = getChildAt(m).getDrawableState();
                if (arrayOfInt2 != null)
                    arrayOfInt1 = mergeDrawableStates(arrayOfInt1, arrayOfInt2);
            }
        }
    }

    protected void onDebugDraw(Canvas paramCanvas)
    {
        if (getLayoutMode() == 1)
            for (int j = 0; j < getChildCount(); j++)
            {
                View localView2 = getChildAt(j);
                Insets localInsets = localView2.getOpticalInsets();
                drawRect(paramCanvas, localView2.getLeft() + localInsets.left, localView2.getTop() + localInsets.top, localView2.getRight() - localInsets.right, localView2.getBottom() - localInsets.bottom, -65536);
            }
        onDebugDrawMargins(paramCanvas);
        for (int i = 0; i < getChildCount(); i++)
        {
            View localView1 = getChildAt(i);
            drawRect(paramCanvas, localView1.getLeft(), localView1.getTop(), localView1.getRight(), localView1.getBottom(), -16776961);
        }
    }

    protected void onDebugDrawMargins(Canvas paramCanvas)
    {
        for (int i = 0; i < getChildCount(); i++)
        {
            View localView = getChildAt(i);
            localView.getLayoutParams().onDebugDraw(localView, paramCanvas);
        }
    }

    void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(ViewGroup.class.getName());
    }

    void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
        if (this.mAttachInfo != null)
        {
            ArrayList localArrayList = this.mAttachInfo.mTempArrayList;
            localArrayList.clear();
            addChildrenForAccessibility(localArrayList);
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                paramAccessibilityNodeInfo.addChild((View)localArrayList.get(j));
            localArrayList.clear();
        }
    }

    public boolean onInterceptHoverEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    protected abstract void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
    {
        int i = this.mChildrenCount;
        int j;
        int k;
        int m;
        int n;
        if ((paramInt & 0x2) != 0)
        {
            j = 0;
            k = 1;
            m = i;
            View[] arrayOfView = this.mChildren;
            n = j;
            label30: if (n == m)
                break label97;
            View localView = arrayOfView[n];
            if (((0xC & localView.mViewFlags) != 0) || (!localView.requestFocus(paramInt, paramRect)))
                break label87;
        }
        label87: label97: for (boolean bool = true; ; bool = false)
        {
            return bool;
            j = i - 1;
            k = -1;
            m = -1;
            break;
            n += k;
            break label30;
        }
    }

    public boolean onRequestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
        if (this.mAccessibilityDelegate != null);
        for (boolean bool = this.mAccessibilityDelegate.onRequestSendAccessibilityEvent(this, paramView, paramAccessibilityEvent); ; bool = onRequestSendAccessibilityEventInternal(paramView, paramAccessibilityEvent))
            return bool;
    }

    boolean onRequestSendAccessibilityEventInternal(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
        return true;
    }

    public void onResolvedLayoutDirectionReset()
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            if (localView.getLayoutDirection() == 2)
                localView.resetResolvedLayoutDirection();
        }
    }

    public void onResolvedTextAlignmentReset()
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            if (localView.getTextAlignment() == 0)
                localView.resetResolvedTextAlignment();
        }
    }

    public void onResolvedTextDirectionReset()
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            if (localView.getTextDirection() == 0)
                localView.resetResolvedTextDirection();
        }
    }

    protected void onSetLayoutParams(View paramView, LayoutParams paramLayoutParams)
    {
    }

    protected void onViewAdded(View paramView)
    {
        if (this.mOnHierarchyChangeListener != null)
            this.mOnHierarchyChangeListener.onChildViewAdded(this, paramView);
    }

    protected void onViewRemoved(View paramView)
    {
        if (this.mOnHierarchyChangeListener != null)
            this.mOnHierarchyChangeListener.onChildViewRemoved(this, paramView);
    }

    public void recomputeViewAttributes(View paramView)
    {
        if ((this.mAttachInfo != null) && (!this.mAttachInfo.mRecomputeGlobalAttributes))
        {
            ViewParent localViewParent = this.mParent;
            if (localViewParent != null)
                localViewParent.recomputeViewAttributes(this);
        }
    }

    public void removeAllViews()
    {
        removeAllViewsInLayout();
        requestLayout();
        invalidate(true);
    }

    public void removeAllViewsInLayout()
    {
        int i = this.mChildrenCount;
        if (i <= 0);
        while (true)
        {
            return;
            View[] arrayOfView = this.mChildren;
            this.mChildrenCount = 0;
            View localView1 = this.mFocused;
            int j;
            Object localObject;
            int k;
            label48: View localView2;
            if (this.mAttachInfo != null)
            {
                j = 1;
                localObject = null;
                needGlobalAttributesUpdate(false);
                k = i - 1;
                if (k < 0)
                    break label198;
                localView2 = arrayOfView[k];
                if (this.mTransition != null)
                    this.mTransition.removeChild(this, localView2);
                if (localView2 == localView1)
                {
                    localView2.unFocus();
                    localObject = localView2;
                }
                localView2.clearAccessibilityFocus();
                cancelTouchTarget(localView2);
                cancelHoverTarget(localView2);
                if ((localView2.getAnimation() == null) && ((this.mTransitioningViews == null) || (!this.mTransitioningViews.contains(localView2))))
                    break label185;
                addDisappearingView(localView2);
            }
            while (true)
            {
                if (localView2.hasTransientState())
                    childHasTransientStateChanged(localView2, false);
                onViewRemoved(localView2);
                localView2.mParent = null;
                arrayOfView[k] = null;
                k--;
                break label48;
                j = 0;
                break;
                label185: if (j != 0)
                    localView2.dispatchDetachedFromWindow();
            }
            label198: if (localObject != null)
            {
                clearChildFocus(localObject);
                ensureInputFocusOnFirstFocusable();
            }
        }
    }

    protected void removeDetachedView(View paramView, boolean paramBoolean)
    {
        if (this.mTransition != null)
            this.mTransition.removeChild(this, paramView);
        if (paramView == this.mFocused)
            paramView.clearFocus();
        paramView.clearAccessibilityFocus();
        cancelTouchTarget(paramView);
        cancelHoverTarget(paramView);
        if (((paramBoolean) && (paramView.getAnimation() != null)) || ((this.mTransitioningViews != null) && (this.mTransitioningViews.contains(paramView))))
            addDisappearingView(paramView);
        while (true)
        {
            if (paramView.hasTransientState())
                childHasTransientStateChanged(paramView, false);
            onViewRemoved(paramView);
            return;
            if (paramView.mAttachInfo != null)
                paramView.dispatchDetachedFromWindow();
        }
    }

    public void removeView(View paramView)
    {
        removeViewInternal(paramView);
        requestLayout();
        invalidate(true);
    }

    public void removeViewAt(int paramInt)
    {
        removeViewInternal(paramInt, getChildAt(paramInt));
        requestLayout();
        invalidate(true);
    }

    public void removeViewInLayout(View paramView)
    {
        removeViewInternal(paramView);
    }

    public void removeViews(int paramInt1, int paramInt2)
    {
        removeViewsInternal(paramInt1, paramInt2);
        requestLayout();
        invalidate(true);
    }

    public void removeViewsInLayout(int paramInt1, int paramInt2)
    {
        removeViewsInternal(paramInt1, paramInt2);
    }

    public void requestChildFocus(View paramView1, View paramView2)
    {
        if (getDescendantFocusability() == 393216);
        while (true)
        {
            return;
            super.unFocus();
            if (this.mFocused != paramView1)
            {
                if (this.mFocused != null)
                    this.mFocused.unFocus();
                this.mFocused = paramView1;
            }
            if (this.mParent != null)
                this.mParent.requestChildFocus(this, paramView2);
        }
    }

    public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
    {
        return false;
    }

    public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
    {
        boolean bool;
        if ((0x80000 & this.mGroupFlags) != 0)
        {
            bool = true;
            if (paramBoolean != bool)
                break label23;
        }
        label23: label71: 
        while (true)
        {
            return;
            bool = false;
            break;
            if (paramBoolean);
            for (this.mGroupFlags = (0x80000 | this.mGroupFlags); ; this.mGroupFlags = (0xFFF7FFFF & this.mGroupFlags))
            {
                if (this.mParent == null)
                    break label71;
                this.mParent.requestDisallowInterceptTouchEvent(paramBoolean);
                break;
            }
        }
    }

    public boolean requestFocus(int paramInt, Rect paramRect)
    {
        int i = getDescendantFocusability();
        boolean bool;
        switch (i)
        {
        default:
            throw new IllegalStateException("descendant focusability must be one of FOCUS_BEFORE_DESCENDANTS, FOCUS_AFTER_DESCENDANTS, FOCUS_BLOCK_DESCENDANTS but is " + i);
        case 393216:
            bool = super.requestFocus(paramInt, paramRect);
        case 131072:
        case 262144:
        }
        while (true)
        {
            return bool;
            bool = super.requestFocus(paramInt, paramRect);
            if (!bool)
            {
                bool = onRequestFocusInDescendants(paramInt, paramRect);
                continue;
                bool = onRequestFocusInDescendants(paramInt, paramRect);
                if (!bool)
                    bool = super.requestFocus(paramInt, paramRect);
            }
        }
    }

    public boolean requestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
        boolean bool = false;
        ViewParent localViewParent = this.mParent;
        if (localViewParent == null);
        while (true)
        {
            return bool;
            if (onRequestSendAccessibilityEvent(paramView, paramAccessibilityEvent))
                bool = localViewParent.requestSendAccessibilityEvent(this, paramAccessibilityEvent);
        }
    }

    public void requestTransitionStart(LayoutTransition paramLayoutTransition)
    {
        ViewRootImpl localViewRootImpl = getViewRootImpl();
        if (localViewRootImpl != null)
            localViewRootImpl.requestTransitionStart(paramLayoutTransition);
    }

    public void requestTransparentRegion(View paramView)
    {
        if (paramView != null)
        {
            paramView.mPrivateFlags = (0x200 | paramView.mPrivateFlags);
            if (this.mParent != null)
                this.mParent.requestTransparentRegion(this);
        }
    }

    public void resetAccessibilityStateChanged()
    {
        super.resetAccessibilityStateChanged();
        View[] arrayOfView = this.mChildren;
        int i = this.mChildrenCount;
        for (int j = 0; j < i; j++)
            arrayOfView[j].resetAccessibilityStateChanged();
    }

    public void scheduleLayoutAnimation()
    {
        this.mGroupFlags = (0x8 | this.mGroupFlags);
    }

    public void setAddStatesFromChildren(boolean paramBoolean)
    {
        if (paramBoolean);
        for (this.mGroupFlags = (0x2000 | this.mGroupFlags); ; this.mGroupFlags = (0xFFFFDFFF & this.mGroupFlags))
        {
            refreshDrawableState();
            return;
        }
    }

    public void setAlwaysDrawnWithCacheEnabled(boolean paramBoolean)
    {
        setBooleanFlag(16384, paramBoolean);
    }

    public void setAnimationCacheEnabled(boolean paramBoolean)
    {
        setBooleanFlag(64, paramBoolean);
    }

    protected void setChildrenDrawingCacheEnabled(boolean paramBoolean)
    {
        if ((paramBoolean) || ((0x3 & this.mPersistentDrawingCache) != 3))
        {
            View[] arrayOfView = this.mChildren;
            int i = this.mChildrenCount;
            for (int j = 0; j < i; j++)
                arrayOfView[j].setDrawingCacheEnabled(paramBoolean);
        }
    }

    protected void setChildrenDrawingOrderEnabled(boolean paramBoolean)
    {
        setBooleanFlag(1024, paramBoolean);
    }

    protected void setChildrenDrawnWithCacheEnabled(boolean paramBoolean)
    {
        setBooleanFlag(32768, paramBoolean);
    }

    public void setChildrenLayersEnabled(boolean paramBoolean)
    {
        if (paramBoolean != this.mDrawLayers)
        {
            this.mDrawLayers = paramBoolean;
            invalidate(true);
            if (!paramBoolean)
            {
                i = 1;
                View.AttachInfo localAttachInfo = this.mAttachInfo;
                if ((localAttachInfo == null) || (localAttachInfo.mHardwareRenderer == null) || (!localAttachInfo.mHardwareRenderer.isEnabled()))
                    break label117;
                if (localAttachInfo.mHardwareRenderer.validate());
            }
            label117: for (int i = 0; ; i = 0)
            {
                for (int j = 0; j < this.mChildrenCount; j++)
                {
                    View localView = this.mChildren[j];
                    if (localView.mLayerType != 0)
                    {
                        if (i != 0)
                            localView.flushLayer();
                        localView.invalidate(true);
                    }
                }
                i = 0;
                break;
            }
        }
    }

    public void setClipChildren(boolean paramBoolean)
    {
        if ((0x1 & this.mGroupFlags) == 1);
        for (boolean bool = true; paramBoolean != bool; bool = false)
        {
            setBooleanFlag(1, paramBoolean);
            for (int i = 0; i < this.mChildrenCount; i++)
            {
                View localView = getChildAt(i);
                if (localView.mDisplayList != null)
                    localView.mDisplayList.setClipChildren(paramBoolean);
            }
        }
    }

    public void setClipToPadding(boolean paramBoolean)
    {
        setBooleanFlag(2, paramBoolean);
    }

    public void setDescendantFocusability(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("must be one of FOCUS_BEFORE_DESCENDANTS, FOCUS_AFTER_DESCENDANTS, FOCUS_BLOCK_DESCENDANTS");
        case 131072:
        case 262144:
        case 393216:
        }
        this.mGroupFlags = (0xFFF9FFFF & this.mGroupFlags);
        this.mGroupFlags |= 0x60000 & paramInt;
    }

    public void setLayoutAnimation(LayoutAnimationController paramLayoutAnimationController)
    {
        this.mLayoutAnimationController = paramLayoutAnimationController;
        if (this.mLayoutAnimationController != null)
            this.mGroupFlags = (0x8 | this.mGroupFlags);
    }

    public void setLayoutAnimationListener(Animation.AnimationListener paramAnimationListener)
    {
        this.mAnimationListener = paramAnimationListener;
    }

    public void setLayoutMode(int paramInt)
    {
        if (this.mLayoutMode != paramInt)
        {
            this.mLayoutMode = paramInt;
            requestLayout();
        }
    }

    public void setLayoutTransition(LayoutTransition paramLayoutTransition)
    {
        if (this.mTransition != null)
            this.mTransition.removeTransitionListener(this.mLayoutTransitionListener);
        this.mTransition = paramLayoutTransition;
        if (this.mTransition != null)
            this.mTransition.addTransitionListener(this.mLayoutTransitionListener);
    }

    public void setMotionEventSplittingEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (this.mGroupFlags = (0x200000 | this.mGroupFlags); ; this.mGroupFlags = (0xFFDFFFFF & this.mGroupFlags))
            return;
    }

    public void setOnHierarchyChangeListener(OnHierarchyChangeListener paramOnHierarchyChangeListener)
    {
        this.mOnHierarchyChangeListener = paramOnHierarchyChangeListener;
    }

    public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
        if ((this.mPaddingLeft | this.mPaddingTop | this.mPaddingRight | this.mPaddingBottom) != 0);
        for (this.mGroupFlags = (0x20 | this.mGroupFlags); ; this.mGroupFlags = (0xFFFFFFDF & this.mGroupFlags))
            return;
    }

    public void setPersistentDrawingCache(int paramInt)
    {
        this.mPersistentDrawingCache = (paramInt & 0x3);
    }

    protected void setStaticTransformationsEnabled(boolean paramBoolean)
    {
        setBooleanFlag(2048, paramBoolean);
    }

    public boolean shouldDelayChildPressedState()
    {
        return true;
    }

    public boolean showContextMenuForChild(View paramView)
    {
        if ((this.mParent != null) && (this.mParent.showContextMenuForChild(paramView)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback)
    {
        if (this.mParent != null);
        for (ActionMode localActionMode = this.mParent.startActionModeForChild(paramView, paramCallback); ; localActionMode = null)
            return localActionMode;
    }

    public void startLayoutAnimation()
    {
        if (this.mLayoutAnimationController != null)
        {
            this.mGroupFlags = (0x8 | this.mGroupFlags);
            requestLayout();
        }
    }

    public void startViewTransition(View paramView)
    {
        if (paramView.mParent == this)
        {
            if (this.mTransitioningViews == null)
                this.mTransitioningViews = new ArrayList();
            this.mTransitioningViews.add(paramView);
        }
    }

    void unFocus()
    {
        if (this.mFocused == null)
            super.unFocus();
        while (true)
        {
            return;
            this.mFocused.unFocus();
            this.mFocused = null;
        }
    }

    boolean updateLocalSystemUiVisibility(int paramInt1, int paramInt2)
    {
        boolean bool = super.updateLocalSystemUiVisibility(paramInt1, paramInt2);
        int i = this.mChildrenCount;
        View[] arrayOfView = this.mChildren;
        for (int j = 0; j < i; j++)
            bool |= arrayOfView[j].updateLocalSystemUiVisibility(paramInt1, paramInt2);
        return bool;
    }

    public void updateViewLayout(View paramView, LayoutParams paramLayoutParams)
    {
        if (!checkLayoutParams(paramLayoutParams))
            throw new IllegalArgumentException("Invalid LayoutParams supplied to " + this);
        if (paramView.mParent != this)
            throw new IllegalArgumentException("Given view not a child of " + this);
        paramView.setLayoutParams(paramLayoutParams);
    }

    static class ViewLocationHolder
        implements Comparable<ViewLocationHolder>
    {
        private static final int MAX_POOL_SIZE = 32;
        private static ViewLocationHolder sPool;
        private static final Object sPoolLock = new Object();
        private static int sPoolSize;
        private boolean mIsPooled;
        private int mLayoutDirection;
        private final Rect mLocation = new Rect();
        private ViewLocationHolder mNext;
        public View mView;

        private void clear()
        {
            this.mView = null;
            this.mLocation.set(0, 0, 0, 0);
        }

        private void init(ViewGroup paramViewGroup, View paramView)
        {
            Rect localRect = this.mLocation;
            paramView.getDrawingRect(localRect);
            paramViewGroup.offsetDescendantRectToMyCoords(paramView, localRect);
            this.mView = paramView;
            this.mLayoutDirection = paramViewGroup.getResolvedLayoutDirection();
        }

        public static ViewLocationHolder obtain(ViewGroup paramViewGroup, View paramView)
        {
            synchronized (sPoolLock)
            {
                if (sPool != null)
                {
                    localViewLocationHolder = sPool;
                    sPool = localViewLocationHolder.mNext;
                    localViewLocationHolder.mNext = null;
                    localViewLocationHolder.mIsPooled = false;
                    sPoolSize = -1 + sPoolSize;
                    localViewLocationHolder.init(paramViewGroup, paramView);
                    return localViewLocationHolder;
                }
                ViewLocationHolder localViewLocationHolder = new ViewLocationHolder();
            }
        }

        public int compareTo(ViewLocationHolder paramViewLocationHolder)
        {
            int i = 1;
            if (paramViewLocationHolder == null);
            while (true)
            {
                return i;
                if (getClass() == paramViewLocationHolder.getClass())
                    if (this.mLocation.bottom - paramViewLocationHolder.mLocation.top <= 0)
                        i = -1;
                    else if (this.mLocation.top - paramViewLocationHolder.mLocation.bottom < 0)
                        if (this.mLayoutDirection == 0)
                        {
                            i = this.mLocation.left - paramViewLocationHolder.mLocation.left;
                            if (i != 0);
                        }
                        else
                        {
                            int j;
                            do
                            {
                                int k = this.mLocation.top - paramViewLocationHolder.mLocation.top;
                                if (k == 0)
                                    break label142;
                                i = k;
                                break;
                                j = this.mLocation.right - paramViewLocationHolder.mLocation.right;
                            }
                            while (j == 0);
                            i = -j;
                            continue;
                            label142: int m = this.mLocation.height() - paramViewLocationHolder.mLocation.height();
                            if (m != 0)
                            {
                                i = -m;
                            }
                            else
                            {
                                int n = this.mLocation.width() - paramViewLocationHolder.mLocation.width();
                                if (n != 0)
                                    i = -n;
                                else
                                    i = this.mView.getAccessibilityViewId() - paramViewLocationHolder.mView.getAccessibilityViewId();
                            }
                        }
            }
        }

        public void recycle()
        {
            if (this.mIsPooled)
                throw new IllegalStateException("Instance already recycled.");
            clear();
            synchronized (sPoolLock)
            {
                if (sPoolSize < 32)
                {
                    this.mNext = sPool;
                    this.mIsPooled = true;
                    sPool = this;
                    sPoolSize = 1 + sPoolSize;
                }
                return;
            }
        }
    }

    static class ChildListForAccessibility
    {
        private static final int MAX_POOL_SIZE = 32;
        private static ChildListForAccessibility sPool;
        private static final Object sPoolLock = new Object();
        private static int sPoolSize;
        private final ArrayList<View> mChildren = new ArrayList();
        private final ArrayList<ViewGroup.ViewLocationHolder> mHolders = new ArrayList();
        private boolean mIsPooled;
        private ChildListForAccessibility mNext;

        private void clear()
        {
            this.mChildren.clear();
        }

        private void init(ViewGroup paramViewGroup, boolean paramBoolean)
        {
            ArrayList localArrayList1 = this.mChildren;
            int i = paramViewGroup.getChildCount();
            for (int j = 0; j < i; j++)
                localArrayList1.add(paramViewGroup.getChildAt(j));
            if (paramBoolean)
            {
                ArrayList localArrayList2 = this.mHolders;
                for (int k = 0; k < i; k++)
                    localArrayList2.add(ViewGroup.ViewLocationHolder.obtain(paramViewGroup, (View)localArrayList1.get(k)));
                Collections.sort(localArrayList2);
                for (int m = 0; m < i; m++)
                {
                    ViewGroup.ViewLocationHolder localViewLocationHolder = (ViewGroup.ViewLocationHolder)localArrayList2.get(m);
                    localArrayList1.set(m, localViewLocationHolder.mView);
                    localViewLocationHolder.recycle();
                }
                localArrayList2.clear();
            }
        }

        public static ChildListForAccessibility obtain(ViewGroup paramViewGroup, boolean paramBoolean)
        {
            synchronized (sPoolLock)
            {
                if (sPool != null)
                {
                    localChildListForAccessibility = sPool;
                    sPool = localChildListForAccessibility.mNext;
                    localChildListForAccessibility.mNext = null;
                    localChildListForAccessibility.mIsPooled = false;
                    sPoolSize = -1 + sPoolSize;
                    localChildListForAccessibility.init(paramViewGroup, paramBoolean);
                    return localChildListForAccessibility;
                }
                ChildListForAccessibility localChildListForAccessibility = new ChildListForAccessibility();
            }
        }

        public View getChildAt(int paramInt)
        {
            return (View)this.mChildren.get(paramInt);
        }

        public int getChildCount()
        {
            return this.mChildren.size();
        }

        public int getChildIndex(View paramView)
        {
            return this.mChildren.indexOf(paramView);
        }

        public void recycle()
        {
            if (this.mIsPooled)
                throw new IllegalStateException("Instance already recycled.");
            clear();
            synchronized (sPoolLock)
            {
                if (sPoolSize < 32)
                {
                    this.mNext = sPool;
                    this.mIsPooled = true;
                    sPool = this;
                    sPoolSize = 1 + sPoolSize;
                }
                return;
            }
        }
    }

    private static final class HoverTarget
    {
        private static final int MAX_RECYCLED = 32;
        private static HoverTarget sRecycleBin;
        private static final Object sRecycleLock = new Object();
        private static int sRecycledCount;
        public View child;
        public HoverTarget next;

        public static HoverTarget obtain(View paramView)
        {
            synchronized (sRecycleLock)
            {
                if (sRecycleBin == null)
                {
                    localHoverTarget = new HoverTarget();
                    localHoverTarget.child = paramView;
                    return localHoverTarget;
                }
                HoverTarget localHoverTarget = sRecycleBin;
                sRecycleBin = localHoverTarget.next;
                sRecycledCount = -1 + sRecycledCount;
                localHoverTarget.next = null;
            }
        }

        public void recycle()
        {
            synchronized (sRecycleLock)
            {
                if (sRecycledCount < 32)
                {
                    this.next = sRecycleBin;
                    sRecycleBin = this;
                    sRecycledCount = 1 + sRecycledCount;
                    this.child = null;
                    return;
                }
                this.next = null;
            }
        }
    }

    private static final class TouchTarget
    {
        public static final int ALL_POINTER_IDS = -1;
        private static final int MAX_RECYCLED = 32;
        private static TouchTarget sRecycleBin;
        private static final Object sRecycleLock = new Object();
        private static int sRecycledCount;
        public View child;
        public TouchTarget next;
        public int pointerIdBits;

        public static TouchTarget obtain(View paramView, int paramInt)
        {
            synchronized (sRecycleLock)
            {
                if (sRecycleBin == null)
                {
                    localTouchTarget = new TouchTarget();
                    localTouchTarget.child = paramView;
                    localTouchTarget.pointerIdBits = paramInt;
                    return localTouchTarget;
                }
                TouchTarget localTouchTarget = sRecycleBin;
                sRecycleBin = localTouchTarget.next;
                sRecycledCount = -1 + sRecycledCount;
                localTouchTarget.next = null;
            }
        }

        public void recycle()
        {
            synchronized (sRecycleLock)
            {
                if (sRecycledCount < 32)
                {
                    this.next = sRecycleBin;
                    sRecycleBin = this;
                    sRecycledCount = 1 + sRecycledCount;
                    this.child = null;
                    return;
                }
                this.next = null;
            }
        }
    }

    public static class MarginLayoutParams extends ViewGroup.LayoutParams
    {
        private static final int DEFAULT_RELATIVE = -2147483648;

        @ViewDebug.ExportedProperty(category="layout")
        public int bottomMargin;

        @ViewDebug.ExportedProperty(category="layout")
        public int endMargin = -2147483648;

        @ViewDebug.ExportedProperty(category="layout")
        public int leftMargin;

        @ViewDebug.ExportedProperty(category="layout")
        public int rightMargin;

        @ViewDebug.ExportedProperty(category="layout")
        public int startMargin = -2147483648;

        @ViewDebug.ExportedProperty(category="layout")
        public int topMargin;

        public MarginLayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public MarginLayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewGroup_MarginLayout);
            setBaseAttributes(localTypedArray, 0, 1);
            int i = localTypedArray.getDimensionPixelSize(2, -1);
            if (i >= 0)
            {
                this.leftMargin = i;
                this.topMargin = i;
                this.rightMargin = i;
                this.bottomMargin = i;
            }
            while (true)
            {
                localTypedArray.recycle();
                return;
                this.leftMargin = localTypedArray.getDimensionPixelSize(3, 0);
                this.topMargin = localTypedArray.getDimensionPixelSize(4, 0);
                this.rightMargin = localTypedArray.getDimensionPixelSize(5, 0);
                this.bottomMargin = localTypedArray.getDimensionPixelSize(6, 0);
                this.startMargin = localTypedArray.getDimensionPixelSize(7, -2147483648);
                this.endMargin = localTypedArray.getDimensionPixelSize(8, -2147483648);
            }
        }

        public MarginLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
        }

        public MarginLayoutParams(MarginLayoutParams paramMarginLayoutParams)
        {
            this.width = paramMarginLayoutParams.width;
            this.height = paramMarginLayoutParams.height;
            this.leftMargin = paramMarginLayoutParams.leftMargin;
            this.topMargin = paramMarginLayoutParams.topMargin;
            this.rightMargin = paramMarginLayoutParams.rightMargin;
            this.bottomMargin = paramMarginLayoutParams.bottomMargin;
            this.startMargin = paramMarginLayoutParams.startMargin;
            this.endMargin = paramMarginLayoutParams.endMargin;
        }

        public int getMarginEnd()
        {
            return this.endMargin;
        }

        public int getMarginStart()
        {
            return this.startMargin;
        }

        public boolean isMarginRelative()
        {
            if ((this.startMargin != -2147483648) || (this.endMargin != -2147483648));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onDebugDraw(View paramView, Canvas paramCanvas)
        {
            ViewGroup.drawRect(paramCanvas, paramView.getLeft() - this.leftMargin, paramView.getTop() - this.topMargin, paramView.getRight() + this.rightMargin, paramView.getBottom() + this.bottomMargin, -65281);
        }

        public void onResolveLayoutDirection(int paramInt)
        {
            int k;
            switch (paramInt)
            {
            default:
                if (this.startMargin > -2147483648)
                {
                    k = this.startMargin;
                    this.leftMargin = k;
                    if (this.endMargin <= -2147483648)
                        break label129;
                }
                break;
            case 1:
            }
            label129: for (int m = this.endMargin; ; m = this.rightMargin)
            {
                this.rightMargin = m;
                return;
                int i;
                if (this.endMargin > -2147483648)
                {
                    i = this.endMargin;
                    label77: this.leftMargin = i;
                    if (this.startMargin <= -2147483648)
                        break label112;
                }
                label112: for (int j = this.startMargin; ; j = this.rightMargin)
                {
                    this.rightMargin = j;
                    break;
                    i = this.leftMargin;
                    break label77;
                }
                k = this.leftMargin;
                break;
            }
        }

        public void setMargins(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.leftMargin = paramInt1;
            this.topMargin = paramInt2;
            this.rightMargin = paramInt3;
            this.bottomMargin = paramInt4;
        }

        public void setMarginsRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.startMargin = paramInt1;
            this.topMargin = paramInt2;
            this.endMargin = paramInt3;
            this.bottomMargin = paramInt4;
        }
    }

    public static class LayoutParams
    {

        @Deprecated
        public static final int FILL_PARENT = -1;
        public static final int MATCH_PARENT = -1;
        public static final int WRAP_CONTENT = -2;

        @ViewDebug.ExportedProperty(category="layout", mapping={@ViewDebug.IntToString(from=-1, to="MATCH_PARENT"), @ViewDebug.IntToString(from=-2, to="WRAP_CONTENT")})
        public int height;
        public LayoutAnimationController.AnimationParameters layoutAnimationParameters;

        @ViewDebug.ExportedProperty(category="layout", mapping={@ViewDebug.IntToString(from=-1, to="MATCH_PARENT"), @ViewDebug.IntToString(from=-2, to="WRAP_CONTENT")})
        public int width;

        LayoutParams()
        {
        }

        public LayoutParams(int paramInt1, int paramInt2)
        {
            this.width = paramInt1;
            this.height = paramInt2;
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewGroup_Layout);
            setBaseAttributes(localTypedArray, 0, 1);
            localTypedArray.recycle();
        }

        public LayoutParams(LayoutParams paramLayoutParams)
        {
            this.width = paramLayoutParams.width;
            this.height = paramLayoutParams.height;
        }

        protected static String sizeToString(int paramInt)
        {
            String str;
            if (paramInt == -2)
                str = "wrap-content";
            while (true)
            {
                return str;
                if (paramInt == -1)
                    str = "match-parent";
                else
                    str = String.valueOf(paramInt);
            }
        }

        public String debug(String paramString)
        {
            return paramString + "ViewGroup.LayoutParams={ width=" + sizeToString(this.width) + ", height=" + sizeToString(this.height) + " }";
        }

        public void onDebugDraw(View paramView, Canvas paramCanvas)
        {
        }

        public void onResolveLayoutDirection(int paramInt)
        {
        }

        protected void setBaseAttributes(TypedArray paramTypedArray, int paramInt1, int paramInt2)
        {
            this.width = paramTypedArray.getLayoutDimension(paramInt1, "layout_width");
            this.height = paramTypedArray.getLayoutDimension(paramInt2, "layout_height");
        }
    }

    public static abstract interface OnHierarchyChangeListener
    {
        public abstract void onChildViewAdded(View paramView1, View paramView2);

        public abstract void onChildViewRemoved(View paramView1, View paramView2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewGroup
 * JD-Core Version:        0.6.2
 */