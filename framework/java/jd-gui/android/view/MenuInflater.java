package android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R.styleable;
import com.android.internal.view.menu.MenuItemImpl;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MenuInflater
{
    private static final Class<?>[] ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE = ACTION_VIEW_CONSTRUCTOR_SIGNATURE;
    private static final Class<?>[] ACTION_VIEW_CONSTRUCTOR_SIGNATURE;
    private static final String LOG_TAG = "MenuInflater";
    private static final int NO_ID = 0;
    private static final String XML_GROUP = "group";
    private static final String XML_ITEM = "item";
    private static final String XML_MENU = "menu";
    private final Object[] mActionProviderConstructorArguments;
    private final Object[] mActionViewConstructorArguments;
    private Context mContext;
    private Object mRealOwner;

    static
    {
        Class[] arrayOfClass = new Class[1];
        arrayOfClass[0] = Context.class;
        ACTION_VIEW_CONSTRUCTOR_SIGNATURE = arrayOfClass;
    }

    public MenuInflater(Context paramContext)
    {
        this.mContext = paramContext;
        this.mRealOwner = paramContext;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = paramContext;
        this.mActionViewConstructorArguments = arrayOfObject;
        this.mActionProviderConstructorArguments = this.mActionViewConstructorArguments;
    }

    public MenuInflater(Context paramContext, Object paramObject)
    {
        this.mContext = paramContext;
        this.mRealOwner = paramObject;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = paramContext;
        this.mActionViewConstructorArguments = arrayOfObject;
        this.mActionProviderConstructorArguments = this.mActionViewConstructorArguments;
    }

    private void parseMenu(XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Menu paramMenu)
        throws XmlPullParserException, IOException
    {
        MenuState localMenuState = new MenuState(paramMenu);
        int i = paramXmlPullParser.getEventType();
        int j = 0;
        Object localObject = null;
        String str3;
        label57: int k;
        if (i == 2)
        {
            str3 = paramXmlPullParser.getName();
            if (str3.equals("menu"))
            {
                i = paramXmlPullParser.next();
                k = 0;
                label60: if (k != 0)
                    return;
            }
        }
        switch (i)
        {
        default:
        case 2:
        case 3:
            while (true)
            {
                i = paramXmlPullParser.next();
                break label60;
                throw new RuntimeException("Expecting menu, got " + str3);
                i = paramXmlPullParser.next();
                if (i != 1)
                    break;
                break label57;
                if (j == 0)
                {
                    String str2 = paramXmlPullParser.getName();
                    if (str2.equals("group"))
                    {
                        localMenuState.readGroup(paramAttributeSet);
                    }
                    else if (str2.equals("item"))
                    {
                        localMenuState.readItem(paramAttributeSet);
                    }
                    else if (str2.equals("menu"))
                    {
                        parseMenu(paramXmlPullParser, paramAttributeSet, localMenuState.addSubMenuItem());
                    }
                    else
                    {
                        j = 1;
                        localObject = str2;
                        continue;
                        String str1 = paramXmlPullParser.getName();
                        if ((j != 0) && (str1.equals(localObject)))
                        {
                            j = 0;
                            localObject = null;
                        }
                        else if (str1.equals("group"))
                        {
                            localMenuState.resetGroup();
                        }
                        else if (str1.equals("item"))
                        {
                            if (!localMenuState.hasAddedItem())
                                if ((localMenuState.itemActionProvider != null) && (localMenuState.itemActionProvider.hasSubMenu()))
                                    localMenuState.addSubMenuItem();
                                else
                                    localMenuState.addItem();
                        }
                        else if (str1.equals("menu"))
                        {
                            k = 1;
                        }
                    }
                }
            }
        case 1:
        }
        throw new RuntimeException("Unexpected end of document");
    }

    // ERROR //
    public void inflate(int paramInt, Menu paramMenu)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: getfield 53	android/view/MenuInflater:mContext	Landroid/content/Context;
        //     6: invokevirtual 155	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     9: iload_1
        //     10: invokevirtual 161	android/content/res/Resources:getLayout	(I)Landroid/content/res/XmlResourceParser;
        //     13: astore_3
        //     14: aload_0
        //     15: aload_3
        //     16: aload_3
        //     17: invokestatic 167	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     20: aload_2
        //     21: invokespecial 128	android/view/MenuInflater:parseMenu	(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
        //     24: aload_3
        //     25: ifnull +9 -> 34
        //     28: aload_3
        //     29: invokeinterface 172 1 0
        //     34: return
        //     35: astore 6
        //     37: new 174	android/view/InflateException
        //     40: dup
        //     41: ldc 176
        //     43: aload 6
        //     45: invokespecial 179	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     48: athrow
        //     49: astore 5
        //     51: aload_3
        //     52: ifnull +9 -> 61
        //     55: aload_3
        //     56: invokeinterface 172 1 0
        //     61: aload 5
        //     63: athrow
        //     64: astore 4
        //     66: new 174	android/view/InflateException
        //     69: dup
        //     70: ldc 176
        //     72: aload 4
        //     74: invokespecial 179	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     77: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	24	35	org/xmlpull/v1/XmlPullParserException
        //     2	24	49	finally
        //     37	49	49	finally
        //     66	78	49	finally
        //     2	24	64	java/io/IOException
    }

    private class MenuState
    {
        private static final int defaultGroupId = 0;
        private static final int defaultItemCategory = 0;
        private static final int defaultItemCheckable = 0;
        private static final boolean defaultItemChecked = false;
        private static final boolean defaultItemEnabled = true;
        private static final int defaultItemId = 0;
        private static final int defaultItemOrder = 0;
        private static final boolean defaultItemVisible = true;
        private int groupCategory;
        private int groupCheckable;
        private boolean groupEnabled;
        private int groupId;
        private int groupOrder;
        private boolean groupVisible;
        private ActionProvider itemActionProvider;
        private String itemActionProviderClassName;
        private String itemActionViewClassName;
        private int itemActionViewLayout;
        private boolean itemAdded;
        private char itemAlphabeticShortcut;
        private int itemCategoryOrder;
        private int itemCheckable;
        private boolean itemChecked;
        private boolean itemEnabled;
        private int itemIconResId;
        private int itemId;
        private String itemListenerMethodName;
        private char itemNumericShortcut;
        private int itemShowAsAction;
        private CharSequence itemTitle;
        private CharSequence itemTitleCondensed;
        private boolean itemVisible;
        private Menu menu;

        public MenuState(Menu arg2)
        {
            Object localObject;
            this.menu = localObject;
            resetGroup();
        }

        private char getShortcut(String paramString)
        {
            char c = '\000';
            if (paramString == null);
            while (true)
            {
                return c;
                c = paramString.charAt(0);
            }
        }

        private <T> T newInstance(String paramString, Class<?>[] paramArrayOfClass, Object[] paramArrayOfObject)
        {
            try
            {
                Object localObject2 = MenuInflater.this.mContext.getClassLoader().loadClass(paramString).getConstructor(paramArrayOfClass).newInstance(paramArrayOfObject);
                localObject1 = localObject2;
                return localObject1;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Log.w("MenuInflater", "Cannot instantiate class: " + paramString, localException);
                    Object localObject1 = null;
                }
            }
        }

        private void setItem(MenuItem paramMenuItem)
        {
            MenuItem localMenuItem = paramMenuItem.setChecked(this.itemChecked).setVisible(this.itemVisible).setEnabled(this.itemEnabled);
            if (this.itemCheckable >= 1);
            for (boolean bool = true; ; bool = false)
            {
                localMenuItem.setCheckable(bool).setTitleCondensed(this.itemTitleCondensed).setIcon(this.itemIconResId).setAlphabeticShortcut(this.itemAlphabeticShortcut).setNumericShortcut(this.itemNumericShortcut);
                if (this.itemShowAsAction >= 0)
                    paramMenuItem.setShowAsAction(this.itemShowAsAction);
                if (this.itemListenerMethodName == null)
                    break label160;
                if (!MenuInflater.this.mContext.isRestricted())
                    break;
                throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
            }
            paramMenuItem.setOnMenuItemClickListener(new MenuInflater.InflatedOnMenuItemClickListener(MenuInflater.this.mRealOwner, this.itemListenerMethodName));
            label160: if ((paramMenuItem instanceof MenuItemImpl))
            {
                MenuItemImpl localMenuItemImpl = (MenuItemImpl)paramMenuItem;
                if (this.itemCheckable >= 2)
                    localMenuItemImpl.setExclusiveCheckable(true);
            }
            int i = 0;
            if (this.itemActionViewClassName != null)
            {
                paramMenuItem.setActionView((View)newInstance(this.itemActionViewClassName, MenuInflater.ACTION_VIEW_CONSTRUCTOR_SIGNATURE, MenuInflater.this.mActionViewConstructorArguments));
                i = 1;
            }
            if (this.itemActionViewLayout > 0)
            {
                if (i != 0)
                    break label270;
                paramMenuItem.setActionView(this.itemActionViewLayout);
            }
            while (true)
            {
                if (this.itemActionProvider != null)
                    paramMenuItem.setActionProvider(this.itemActionProvider);
                return;
                label270: Log.w("MenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
            }
        }

        public void addItem()
        {
            this.itemAdded = true;
            setItem(this.menu.add(this.groupId, this.itemId, this.itemCategoryOrder, this.itemTitle));
        }

        public SubMenu addSubMenuItem()
        {
            this.itemAdded = true;
            SubMenu localSubMenu = this.menu.addSubMenu(this.groupId, this.itemId, this.itemCategoryOrder, this.itemTitle);
            setItem(localSubMenu.getItem());
            return localSubMenu;
        }

        public boolean hasAddedItem()
        {
            return this.itemAdded;
        }

        public void readGroup(AttributeSet paramAttributeSet)
        {
            TypedArray localTypedArray = MenuInflater.this.mContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuGroup);
            this.groupId = localTypedArray.getResourceId(1, 0);
            this.groupCategory = localTypedArray.getInt(3, 0);
            this.groupOrder = localTypedArray.getInt(4, 0);
            this.groupCheckable = localTypedArray.getInt(5, 0);
            this.groupVisible = localTypedArray.getBoolean(2, true);
            this.groupEnabled = localTypedArray.getBoolean(0, true);
            localTypedArray.recycle();
        }

        public void readItem(AttributeSet paramAttributeSet)
        {
            TypedArray localTypedArray = MenuInflater.this.mContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuItem);
            this.itemId = localTypedArray.getResourceId(2, 0);
            int i = localTypedArray.getInt(5, this.groupCategory);
            int j = localTypedArray.getInt(6, this.groupOrder);
            this.itemCategoryOrder = (0xFFFF0000 & i | 0xFFFF & j);
            this.itemTitle = localTypedArray.getText(7);
            this.itemTitleCondensed = localTypedArray.getText(8);
            this.itemIconResId = localTypedArray.getResourceId(0, 0);
            this.itemAlphabeticShortcut = getShortcut(localTypedArray.getString(9));
            this.itemNumericShortcut = getShortcut(localTypedArray.getString(10));
            int m;
            label149: int k;
            if (localTypedArray.hasValue(11))
                if (localTypedArray.getBoolean(11, false))
                {
                    m = 1;
                    this.itemCheckable = m;
                    this.itemChecked = localTypedArray.getBoolean(3, false);
                    this.itemVisible = localTypedArray.getBoolean(4, this.groupVisible);
                    this.itemEnabled = localTypedArray.getBoolean(1, this.groupEnabled);
                    this.itemShowAsAction = localTypedArray.getInt(13, -1);
                    this.itemListenerMethodName = localTypedArray.getString(12);
                    this.itemActionViewLayout = localTypedArray.getResourceId(14, 0);
                    this.itemActionViewClassName = localTypedArray.getString(15);
                    this.itemActionProviderClassName = localTypedArray.getString(16);
                    if (this.itemActionProviderClassName == null)
                        break label319;
                    k = 1;
                    label248: if ((k == 0) || (this.itemActionViewLayout != 0) || (this.itemActionViewClassName != null))
                        break label325;
                }
            for (this.itemActionProvider = ((ActionProvider)newInstance(this.itemActionProviderClassName, MenuInflater.ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE, MenuInflater.this.mActionProviderConstructorArguments)); ; this.itemActionProvider = null)
            {
                localTypedArray.recycle();
                this.itemAdded = false;
                return;
                m = 0;
                break;
                this.itemCheckable = this.groupCheckable;
                break label149;
                label319: k = 0;
                break label248;
                label325: if (k != 0)
                    Log.w("MenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
            }
        }

        public void resetGroup()
        {
            this.groupId = 0;
            this.groupCategory = 0;
            this.groupOrder = 0;
            this.groupCheckable = 0;
            this.groupVisible = true;
            this.groupEnabled = true;
        }
    }

    private static class InflatedOnMenuItemClickListener
        implements MenuItem.OnMenuItemClickListener
    {
        private static final Class<?>[] PARAM_TYPES = arrayOfClass;
        private Method mMethod;
        private Object mRealOwner;

        static
        {
            Class[] arrayOfClass = new Class[1];
            arrayOfClass[0] = MenuItem.class;
        }

        public InflatedOnMenuItemClickListener(Object paramObject, String paramString)
        {
            this.mRealOwner = paramObject;
            Class localClass = paramObject.getClass();
            try
            {
                this.mMethod = localClass.getMethod(paramString, PARAM_TYPES);
                return;
            }
            catch (Exception localException)
            {
                InflateException localInflateException = new InflateException("Couldn't resolve menu item onClick handler " + paramString + " in class " + localClass.getName());
                localInflateException.initCause(localException);
                throw localInflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem paramMenuItem)
        {
            boolean bool = true;
            try
            {
                if (this.mMethod.getReturnType() == Boolean.TYPE)
                {
                    Method localMethod2 = this.mMethod;
                    Object localObject2 = this.mRealOwner;
                    Object[] arrayOfObject2 = new Object[1];
                    arrayOfObject2[0] = paramMenuItem;
                    bool = ((Boolean)localMethod2.invoke(localObject2, arrayOfObject2)).booleanValue();
                }
                else
                {
                    Method localMethod1 = this.mMethod;
                    Object localObject1 = this.mRealOwner;
                    Object[] arrayOfObject1 = new Object[1];
                    arrayOfObject1[0] = paramMenuItem;
                    localMethod1.invoke(localObject1, arrayOfObject1);
                }
            }
            catch (Exception localException)
            {
                throw new RuntimeException(localException);
            }
            return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.MenuInflater
 * JD-Core Version:        0.6.2
 */