package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IOnKeyguardExitResult extends IInterface
{
    public abstract void onKeyguardExitResult(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IOnKeyguardExitResult
    {
        private static final String DESCRIPTOR = "android.view.IOnKeyguardExitResult";
        static final int TRANSACTION_onKeyguardExitResult = 1;

        public Stub()
        {
            attachInterface(this, "android.view.IOnKeyguardExitResult");
        }

        public static IOnKeyguardExitResult asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.IOnKeyguardExitResult");
                if ((localIInterface != null) && ((localIInterface instanceof IOnKeyguardExitResult)))
                    localObject = (IOnKeyguardExitResult)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.view.IOnKeyguardExitResult");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.view.IOnKeyguardExitResult");
            if (paramParcel1.readInt() != 0);
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                onKeyguardExitResult(bool2);
                break;
            }
        }

        private static class Proxy
            implements IOnKeyguardExitResult
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.IOnKeyguardExitResult";
            }

            public void onKeyguardExitResult(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IOnKeyguardExitResult");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.IOnKeyguardExitResult
 * JD-Core Version:        0.6.2
 */