package android.view;

import android.graphics.Rect;
import android.graphics.Region;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class ViewTreeObserver
{
    private boolean mAlive = true;
    private CopyOnWriteArrayList<OnComputeInternalInsetsListener> mOnComputeInternalInsetsListeners;
    private ArrayList<OnDrawListener> mOnDrawListeners;
    private CopyOnWriteArrayList<OnGlobalFocusChangeListener> mOnGlobalFocusListeners;
    private CopyOnWriteArrayList<OnGlobalLayoutListener> mOnGlobalLayoutListeners;
    private ArrayList<OnPreDrawListener> mOnPreDrawListeners;
    private CopyOnWriteArrayList<OnScrollChangedListener> mOnScrollChangedListeners;
    private CopyOnWriteArrayList<OnTouchModeChangeListener> mOnTouchModeChangeListeners;

    private void checkIsAlive()
    {
        if (!this.mAlive)
            throw new IllegalStateException("This ViewTreeObserver is not alive, call getViewTreeObserver() again");
    }

    private void kill()
    {
        this.mAlive = false;
    }

    public void addOnComputeInternalInsetsListener(OnComputeInternalInsetsListener paramOnComputeInternalInsetsListener)
    {
        checkIsAlive();
        if (this.mOnComputeInternalInsetsListeners == null)
            this.mOnComputeInternalInsetsListeners = new CopyOnWriteArrayList();
        this.mOnComputeInternalInsetsListeners.add(paramOnComputeInternalInsetsListener);
    }

    public void addOnDrawListener(OnDrawListener paramOnDrawListener)
    {
        checkIsAlive();
        if (this.mOnDrawListeners == null)
            this.mOnDrawListeners = new ArrayList();
        this.mOnDrawListeners.add(paramOnDrawListener);
    }

    public void addOnGlobalFocusChangeListener(OnGlobalFocusChangeListener paramOnGlobalFocusChangeListener)
    {
        checkIsAlive();
        if (this.mOnGlobalFocusListeners == null)
            this.mOnGlobalFocusListeners = new CopyOnWriteArrayList();
        this.mOnGlobalFocusListeners.add(paramOnGlobalFocusChangeListener);
    }

    public void addOnGlobalLayoutListener(OnGlobalLayoutListener paramOnGlobalLayoutListener)
    {
        checkIsAlive();
        if (this.mOnGlobalLayoutListeners == null)
            this.mOnGlobalLayoutListeners = new CopyOnWriteArrayList();
        this.mOnGlobalLayoutListeners.add(paramOnGlobalLayoutListener);
    }

    public void addOnPreDrawListener(OnPreDrawListener paramOnPreDrawListener)
    {
        checkIsAlive();
        if (this.mOnPreDrawListeners == null)
            this.mOnPreDrawListeners = new ArrayList();
        this.mOnPreDrawListeners.add(paramOnPreDrawListener);
    }

    public void addOnScrollChangedListener(OnScrollChangedListener paramOnScrollChangedListener)
    {
        checkIsAlive();
        if (this.mOnScrollChangedListeners == null)
            this.mOnScrollChangedListeners = new CopyOnWriteArrayList();
        this.mOnScrollChangedListeners.add(paramOnScrollChangedListener);
    }

    public void addOnTouchModeChangeListener(OnTouchModeChangeListener paramOnTouchModeChangeListener)
    {
        checkIsAlive();
        if (this.mOnTouchModeChangeListeners == null)
            this.mOnTouchModeChangeListeners = new CopyOnWriteArrayList();
        this.mOnTouchModeChangeListeners.add(paramOnTouchModeChangeListener);
    }

    final void dispatchOnComputeInternalInsets(InternalInsetsInfo paramInternalInsetsInfo)
    {
        CopyOnWriteArrayList localCopyOnWriteArrayList = this.mOnComputeInternalInsetsListeners;
        if ((localCopyOnWriteArrayList != null) && (localCopyOnWriteArrayList.size() > 0))
        {
            Iterator localIterator = localCopyOnWriteArrayList.iterator();
            while (localIterator.hasNext())
                ((OnComputeInternalInsetsListener)localIterator.next()).onComputeInternalInsets(paramInternalInsetsInfo);
        }
    }

    public final void dispatchOnDraw()
    {
        if (this.mOnDrawListeners != null)
        {
            ArrayList localArrayList = this.mOnDrawListeners;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((OnDrawListener)localArrayList.get(j)).onDraw();
        }
    }

    final void dispatchOnGlobalFocusChange(View paramView1, View paramView2)
    {
        CopyOnWriteArrayList localCopyOnWriteArrayList = this.mOnGlobalFocusListeners;
        if ((localCopyOnWriteArrayList != null) && (localCopyOnWriteArrayList.size() > 0))
        {
            Iterator localIterator = localCopyOnWriteArrayList.iterator();
            while (localIterator.hasNext())
                ((OnGlobalFocusChangeListener)localIterator.next()).onGlobalFocusChanged(paramView1, paramView2);
        }
    }

    public final void dispatchOnGlobalLayout()
    {
        CopyOnWriteArrayList localCopyOnWriteArrayList = this.mOnGlobalLayoutListeners;
        if ((localCopyOnWriteArrayList != null) && (localCopyOnWriteArrayList.size() > 0))
        {
            Iterator localIterator = localCopyOnWriteArrayList.iterator();
            while (localIterator.hasNext())
                ((OnGlobalLayoutListener)localIterator.next()).onGlobalLayout();
        }
    }

    public final boolean dispatchOnPreDraw()
    {
        boolean bool1 = false;
        if ((this.mOnPreDrawListeners != null) && (this.mOnPreDrawListeners.size() > 0))
        {
            ArrayList localArrayList = (ArrayList)this.mOnPreDrawListeners.clone();
            int i = localArrayList.size();
            int j = 0;
            if (j < i)
            {
                if (!((OnPreDrawListener)localArrayList.get(j)).onPreDraw());
                for (boolean bool2 = true; ; bool2 = false)
                {
                    bool1 |= bool2;
                    j++;
                    break;
                }
            }
        }
        return bool1;
    }

    final void dispatchOnScrollChanged()
    {
        CopyOnWriteArrayList localCopyOnWriteArrayList = this.mOnScrollChangedListeners;
        if ((localCopyOnWriteArrayList != null) && (localCopyOnWriteArrayList.size() > 0))
        {
            Iterator localIterator = localCopyOnWriteArrayList.iterator();
            while (localIterator.hasNext())
                ((OnScrollChangedListener)localIterator.next()).onScrollChanged();
        }
    }

    final void dispatchOnTouchModeChanged(boolean paramBoolean)
    {
        CopyOnWriteArrayList localCopyOnWriteArrayList = this.mOnTouchModeChangeListeners;
        if ((localCopyOnWriteArrayList != null) && (localCopyOnWriteArrayList.size() > 0))
        {
            Iterator localIterator = localCopyOnWriteArrayList.iterator();
            while (localIterator.hasNext())
                ((OnTouchModeChangeListener)localIterator.next()).onTouchModeChanged(paramBoolean);
        }
    }

    final boolean hasComputeInternalInsetsListeners()
    {
        CopyOnWriteArrayList localCopyOnWriteArrayList = this.mOnComputeInternalInsetsListeners;
        if ((localCopyOnWriteArrayList != null) && (localCopyOnWriteArrayList.size() > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isAlive()
    {
        return this.mAlive;
    }

    void merge(ViewTreeObserver paramViewTreeObserver)
    {
        if (paramViewTreeObserver.mOnGlobalFocusListeners != null)
        {
            if (this.mOnGlobalFocusListeners != null)
                this.mOnGlobalFocusListeners.addAll(paramViewTreeObserver.mOnGlobalFocusListeners);
        }
        else
        {
            if (paramViewTreeObserver.mOnGlobalLayoutListeners != null)
            {
                if (this.mOnGlobalLayoutListeners == null)
                    break label172;
                this.mOnGlobalLayoutListeners.addAll(paramViewTreeObserver.mOnGlobalLayoutListeners);
            }
            label52: if (paramViewTreeObserver.mOnPreDrawListeners != null)
            {
                if (this.mOnPreDrawListeners == null)
                    break label183;
                this.mOnPreDrawListeners.addAll(paramViewTreeObserver.mOnPreDrawListeners);
            }
            label78: if (paramViewTreeObserver.mOnTouchModeChangeListeners != null)
            {
                if (this.mOnTouchModeChangeListeners == null)
                    break label194;
                this.mOnTouchModeChangeListeners.addAll(paramViewTreeObserver.mOnTouchModeChangeListeners);
            }
            label104: if (paramViewTreeObserver.mOnComputeInternalInsetsListeners != null)
            {
                if (this.mOnComputeInternalInsetsListeners == null)
                    break label205;
                this.mOnComputeInternalInsetsListeners.addAll(paramViewTreeObserver.mOnComputeInternalInsetsListeners);
            }
            label130: if (paramViewTreeObserver.mOnScrollChangedListeners != null)
            {
                if (this.mOnScrollChangedListeners == null)
                    break label216;
                this.mOnScrollChangedListeners.addAll(paramViewTreeObserver.mOnScrollChangedListeners);
            }
        }
        while (true)
        {
            paramViewTreeObserver.kill();
            return;
            this.mOnGlobalFocusListeners = paramViewTreeObserver.mOnGlobalFocusListeners;
            break;
            label172: this.mOnGlobalLayoutListeners = paramViewTreeObserver.mOnGlobalLayoutListeners;
            break label52;
            label183: this.mOnPreDrawListeners = paramViewTreeObserver.mOnPreDrawListeners;
            break label78;
            label194: this.mOnTouchModeChangeListeners = paramViewTreeObserver.mOnTouchModeChangeListeners;
            break label104;
            label205: this.mOnComputeInternalInsetsListeners = paramViewTreeObserver.mOnComputeInternalInsetsListeners;
            break label130;
            label216: this.mOnScrollChangedListeners = paramViewTreeObserver.mOnScrollChangedListeners;
        }
    }

    @Deprecated
    public void removeGlobalOnLayoutListener(OnGlobalLayoutListener paramOnGlobalLayoutListener)
    {
        removeOnGlobalLayoutListener(paramOnGlobalLayoutListener);
    }

    public void removeOnComputeInternalInsetsListener(OnComputeInternalInsetsListener paramOnComputeInternalInsetsListener)
    {
        checkIsAlive();
        if (this.mOnComputeInternalInsetsListeners == null);
        while (true)
        {
            return;
            this.mOnComputeInternalInsetsListeners.remove(paramOnComputeInternalInsetsListener);
        }
    }

    public void removeOnDrawListener(OnDrawListener paramOnDrawListener)
    {
        checkIsAlive();
        if (this.mOnDrawListeners == null);
        while (true)
        {
            return;
            this.mOnDrawListeners.remove(paramOnDrawListener);
        }
    }

    public void removeOnGlobalFocusChangeListener(OnGlobalFocusChangeListener paramOnGlobalFocusChangeListener)
    {
        checkIsAlive();
        if (this.mOnGlobalFocusListeners == null);
        while (true)
        {
            return;
            this.mOnGlobalFocusListeners.remove(paramOnGlobalFocusChangeListener);
        }
    }

    public void removeOnGlobalLayoutListener(OnGlobalLayoutListener paramOnGlobalLayoutListener)
    {
        checkIsAlive();
        if (this.mOnGlobalLayoutListeners == null);
        while (true)
        {
            return;
            this.mOnGlobalLayoutListeners.remove(paramOnGlobalLayoutListener);
        }
    }

    public void removeOnPreDrawListener(OnPreDrawListener paramOnPreDrawListener)
    {
        checkIsAlive();
        if (this.mOnPreDrawListeners == null);
        while (true)
        {
            return;
            this.mOnPreDrawListeners.remove(paramOnPreDrawListener);
        }
    }

    public void removeOnScrollChangedListener(OnScrollChangedListener paramOnScrollChangedListener)
    {
        checkIsAlive();
        if (this.mOnScrollChangedListeners == null);
        while (true)
        {
            return;
            this.mOnScrollChangedListeners.remove(paramOnScrollChangedListener);
        }
    }

    public void removeOnTouchModeChangeListener(OnTouchModeChangeListener paramOnTouchModeChangeListener)
    {
        checkIsAlive();
        if (this.mOnTouchModeChangeListeners == null);
        while (true)
        {
            return;
            this.mOnTouchModeChangeListeners.remove(paramOnTouchModeChangeListener);
        }
    }

    public static abstract interface OnComputeInternalInsetsListener
    {
        public abstract void onComputeInternalInsets(ViewTreeObserver.InternalInsetsInfo paramInternalInsetsInfo);
    }

    public static final class InternalInsetsInfo
    {
        public static final int TOUCHABLE_INSETS_CONTENT = 1;
        public static final int TOUCHABLE_INSETS_FRAME = 0;
        public static final int TOUCHABLE_INSETS_REGION = 3;
        public static final int TOUCHABLE_INSETS_VISIBLE = 2;
        public final Rect contentInsets = new Rect();
        int mTouchableInsets;
        public final Region touchableRegion = new Region();
        public final Rect visibleInsets = new Rect();

        public boolean equals(Object paramObject)
        {
            boolean bool = true;
            if (this == paramObject);
            while (true)
            {
                return bool;
                if ((paramObject == null) || (getClass() != paramObject.getClass()))
                {
                    bool = false;
                }
                else
                {
                    InternalInsetsInfo localInternalInsetsInfo = (InternalInsetsInfo)paramObject;
                    if ((this.mTouchableInsets != localInternalInsetsInfo.mTouchableInsets) || (!this.contentInsets.equals(localInternalInsetsInfo.contentInsets)) || (!this.visibleInsets.equals(localInternalInsetsInfo.visibleInsets)) || (!this.touchableRegion.equals(localInternalInsetsInfo.touchableRegion)))
                        bool = false;
                }
            }
        }

        public int hashCode()
        {
            int i = 0;
            int j;
            int k;
            if (this.contentInsets != null)
            {
                j = this.contentInsets.hashCode();
                k = j * 31;
                if (this.visibleInsets == null)
                    break label80;
            }
            label80: for (int m = this.visibleInsets.hashCode(); ; m = 0)
            {
                int n = 31 * (k + m);
                if (this.touchableRegion != null)
                    i = this.touchableRegion.hashCode();
                return 31 * (n + i) + this.mTouchableInsets;
                j = 0;
                break;
            }
        }

        void reset()
        {
            this.contentInsets.setEmpty();
            this.visibleInsets.setEmpty();
            this.touchableRegion.setEmpty();
            this.mTouchableInsets = 0;
        }

        void set(InternalInsetsInfo paramInternalInsetsInfo)
        {
            this.contentInsets.set(paramInternalInsetsInfo.contentInsets);
            this.visibleInsets.set(paramInternalInsetsInfo.visibleInsets);
            this.touchableRegion.set(paramInternalInsetsInfo.touchableRegion);
            this.mTouchableInsets = paramInternalInsetsInfo.mTouchableInsets;
        }

        public void setTouchableInsets(int paramInt)
        {
            this.mTouchableInsets = paramInt;
        }
    }

    public static abstract interface OnScrollChangedListener
    {
        public abstract void onScrollChanged();
    }

    public static abstract interface OnTouchModeChangeListener
    {
        public abstract void onTouchModeChanged(boolean paramBoolean);
    }

    public static abstract interface OnDrawListener
    {
        public abstract void onDraw();
    }

    public static abstract interface OnPreDrawListener
    {
        public abstract boolean onPreDraw();
    }

    public static abstract interface OnGlobalLayoutListener
    {
        public abstract void onGlobalLayout();
    }

    public static abstract interface OnGlobalFocusChangeListener
    {
        public abstract void onGlobalFocusChanged(View paramView1, View paramView2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewTreeObserver
 * JD-Core Version:        0.6.2
 */