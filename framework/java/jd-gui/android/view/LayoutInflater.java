package android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Xml;
import android.widget.FrameLayout;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public abstract class LayoutInflater
{
    private static final String TAG_1995 = "blink";
    private static final String TAG_INCLUDE = "include";
    private static final String TAG_MERGE = "merge";
    private static final String TAG_REQUEST_FOCUS = "requestFocus";
    static final Class<?>[] mConstructorSignature = arrayOfClass;
    private static final HashMap<String, Constructor<? extends View>> sConstructorMap = new HashMap();
    private final boolean DEBUG = false;
    final Object[] mConstructorArgs = new Object[2];
    protected final Context mContext;
    private Factory mFactory;
    private Factory2 mFactory2;
    private boolean mFactorySet;
    private Filter mFilter;
    private HashMap<String, Boolean> mFilterMap;
    private Factory2 mPrivateFactory;

    static
    {
        Class[] arrayOfClass = new Class[2];
        arrayOfClass[0] = Context.class;
        arrayOfClass[1] = AttributeSet.class;
    }

    protected LayoutInflater(Context paramContext)
    {
        this.mContext = paramContext;
    }

    protected LayoutInflater(LayoutInflater paramLayoutInflater, Context paramContext)
    {
        this.mContext = paramContext;
        this.mFactory = paramLayoutInflater.mFactory;
        this.mFactory2 = paramLayoutInflater.mFactory2;
        this.mPrivateFactory = paramLayoutInflater.mPrivateFactory;
        this.mFilter = paramLayoutInflater.mFilter;
    }

    private void failNotAllowed(String paramString1, String paramString2, AttributeSet paramAttributeSet)
    {
        StringBuilder localStringBuilder = new StringBuilder().append(paramAttributeSet.getPositionDescription()).append(": Class not allowed to be inflated ");
        if (paramString2 != null)
            paramString1 = paramString2 + paramString1;
        throw new InflateException(paramString1);
    }

    public static LayoutInflater from(Context paramContext)
    {
        LayoutInflater localLayoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
        if (localLayoutInflater == null)
            throw new AssertionError("LayoutInflater not found.");
        return localLayoutInflater;
    }

    private void parseInclude(XmlPullParser paramXmlPullParser, View paramView, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        int m;
        if ((paramView instanceof ViewGroup))
        {
            int i = paramAttributeSet.getAttributeResourceValue(null, "layout", 0);
            if (i == 0)
            {
                String str2 = paramAttributeSet.getAttributeValue(null, "layout");
                if (str2 == null)
                    throw new InflateException("You must specifiy a layout in the include tag: <include layout=\"@layout/layoutID\" />");
                throw new InflateException("You must specifiy a valid layout reference. The layout ID " + str2 + " is not valid.");
            }
            XmlResourceParser localXmlResourceParser = getContext().getResources().getLayout(i);
            AttributeSet localAttributeSet;
            try
            {
                localAttributeSet = Xml.asAttributeSet(localXmlResourceParser);
                int j;
                do
                    j = localXmlResourceParser.next();
                while ((j != 2) && (j != 1));
                if (j != 2)
                    throw new InflateException(localXmlResourceParser.getPositionDescription() + ": No start tag found!");
            }
            finally
            {
                localXmlResourceParser.close();
            }
            String str1 = localXmlResourceParser.getName();
            if ("merge".equals(str1))
            {
                rInflate(localXmlResourceParser, paramView, localAttributeSet, false);
                localXmlResourceParser.close();
                int n = paramXmlPullParser.getDepth();
                int i1;
                do
                    i1 = paramXmlPullParser.next();
                while (((i1 != 3) || (paramXmlPullParser.getDepth() > n)) && (i1 != 1));
                return;
            }
            View localView = createViewFromTag(paramView, str1, localAttributeSet);
            ViewGroup localViewGroup = (ViewGroup)paramView;
            while (true)
            {
                try
                {
                    ViewGroup.LayoutParams localLayoutParams2 = localViewGroup.generateLayoutParams(paramAttributeSet);
                    if (localLayoutParams2 != null)
                        localView.setLayoutParams(localLayoutParams2);
                    rInflate(localXmlResourceParser, localView, localAttributeSet, true);
                    TypedArray localTypedArray = this.mContext.obtainStyledAttributes(paramAttributeSet, R.styleable.View, 0, 0);
                    int k = localTypedArray.getResourceId(8, -1);
                    m = localTypedArray.getInt(20, -1);
                    localTypedArray.recycle();
                    if (k == -1)
                        break label448;
                    localView.setId(k);
                    break label448;
                    localViewGroup.addView(localView);
                    break;
                }
                catch (RuntimeException localRuntimeException)
                {
                    ViewGroup.LayoutParams localLayoutParams1 = localViewGroup.generateLayoutParams(localAttributeSet);
                    if (localLayoutParams1 == null)
                        continue;
                    localView.setLayoutParams(localLayoutParams1);
                    continue;
                }
                finally
                {
                    if (0 != 0)
                        localView.setLayoutParams(null);
                }
                localView.setVisibility(0);
                continue;
                localView.setVisibility(4);
                continue;
                localView.setVisibility(8);
            }
        }
        throw new InflateException("<include /> can only be used inside of a ViewGroup");
        label448: switch (m)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
    }

    private void parseRequestFocus(XmlPullParser paramXmlPullParser, View paramView)
        throws XmlPullParserException, IOException
    {
        paramView.requestFocus();
        int i = paramXmlPullParser.getDepth();
        int j;
        do
            j = paramXmlPullParser.next();
        while (((j != 3) || (paramXmlPullParser.getDepth() > i)) && (j != 1));
    }

    public abstract LayoutInflater cloneInContext(Context paramContext);

    public final View createView(String paramString1, String paramString2, AttributeSet paramAttributeSet)
        throws ClassNotFoundException, InflateException
    {
        boolean bool = true;
        Constructor localConstructor = (Constructor)sConstructorMap.get(paramString1);
        Class localClass = null;
        if (localConstructor == null);
        while (true)
        {
            View localView;
            try
            {
                ClassLoader localClassLoader2 = this.mContext.getClassLoader();
                if (paramString2 == null)
                    break label559;
                str3 = paramString2 + paramString1;
                localClass = localClassLoader2.loadClass(str3).asSubclass(View.class);
                if ((this.mFilter != null) && (localClass != null) && (!this.mFilter.onLoadClass(localClass)))
                    failNotAllowed(paramString1, paramString2, paramAttributeSet);
                localConstructor = localClass.getConstructor(mConstructorSignature);
                sConstructorMap.put(paramString1, localConstructor);
                Object[] arrayOfObject = this.mConstructorArgs;
                arrayOfObject[1] = paramAttributeSet;
                localView = (View)localConstructor.newInstance(arrayOfObject);
                if ((localView instanceof ViewStub))
                {
                    ((ViewStub)localView).setLayoutInflater(this);
                    break label556;
                    if (this.mFilter == null)
                        continue;
                    localBoolean = (Boolean)this.mFilterMap.get(paramString1);
                    if (localBoolean == null)
                    {
                        ClassLoader localClassLoader1 = this.mContext.getClassLoader();
                        if (paramString2 != null)
                        {
                            str2 = paramString2 + paramString1;
                            localClass = localClassLoader1.loadClass(str2).asSubclass(View.class);
                            if ((localClass == null) || (!this.mFilter.onLoadClass(localClass)))
                                continue;
                            this.mFilterMap.put(paramString1, Boolean.valueOf(bool));
                            if (bool)
                                continue;
                            failNotAllowed(paramString1, paramString2, paramAttributeSet);
                            continue;
                        }
                    }
                }
            }
            catch (NoSuchMethodException localNoSuchMethodException)
            {
                Boolean localBoolean;
                StringBuilder localStringBuilder3 = new StringBuilder().append(paramAttributeSet.getPositionDescription()).append(": Error inflating class ");
                if (paramString2 != null)
                    paramString1 = paramString2 + paramString1;
                InflateException localInflateException3 = new InflateException(paramString1);
                localInflateException3.initCause(localNoSuchMethodException);
                throw localInflateException3;
                String str2 = paramString1;
                continue;
                bool = false;
                continue;
                if (!localBoolean.equals(Boolean.FALSE))
                    continue;
                failNotAllowed(paramString1, paramString2, paramAttributeSet);
                continue;
            }
            catch (ClassCastException localClassCastException)
            {
                StringBuilder localStringBuilder2 = new StringBuilder().append(paramAttributeSet.getPositionDescription()).append(": Class is not a View ");
                if (paramString2 != null)
                    paramString1 = paramString2 + paramString1;
                InflateException localInflateException2 = new InflateException(paramString1);
                localInflateException2.initCause(localClassCastException);
                throw localInflateException2;
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
                throw localClassNotFoundException;
            }
            catch (Exception localException)
            {
                StringBuilder localStringBuilder1 = new StringBuilder().append(paramAttributeSet.getPositionDescription()).append(": Error inflating class ");
                if (localClass == null)
                {
                    str1 = "<unknown>";
                    InflateException localInflateException1 = new InflateException(str1);
                    localInflateException1.initCause(localException);
                    throw localInflateException1;
                }
                String str1 = localClass.getName();
                continue;
            }
            label556: return localView;
            label559: String str3 = paramString1;
        }
    }

    View createViewFromTag(View paramView, String paramString, AttributeSet paramAttributeSet)
    {
        if (paramString.equals("view"))
            paramString = paramAttributeSet.getAttributeValue(null, "class");
        while (true)
        {
            try
            {
                if (this.mFactory2 != null)
                {
                    localObject = this.mFactory2.onCreateView(paramView, paramString, this.mContext, paramAttributeSet);
                    if ((localObject == null) && (this.mPrivateFactory != null))
                        localObject = this.mPrivateFactory.onCreateView(paramView, paramString, this.mContext, paramAttributeSet);
                    if (localObject != null)
                        break label254;
                    if (-1 == paramString.indexOf('.'))
                    {
                        localObject = onCreateView(paramView, paramString, paramAttributeSet);
                        break label254;
                    }
                }
                else
                {
                    if (this.mFactory == null)
                        break label257;
                    localObject = this.mFactory.onCreateView(paramString, this.mContext, paramAttributeSet);
                    continue;
                }
                View localView = createView(paramString, null, paramAttributeSet);
                localObject = localView;
            }
            catch (InflateException localInflateException3)
            {
                throw localInflateException3;
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
                InflateException localInflateException2 = new InflateException(paramAttributeSet.getPositionDescription() + ": Error inflating class " + paramString);
                localInflateException2.initCause(localClassNotFoundException);
                throw localInflateException2;
            }
            catch (Exception localException)
            {
                InflateException localInflateException1 = new InflateException(paramAttributeSet.getPositionDescription() + ": Error inflating class " + paramString);
                localInflateException1.initCause(localException);
                throw localInflateException1;
            }
            label254: return localObject;
            label257: Object localObject = null;
        }
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public final Factory getFactory()
    {
        return this.mFactory;
    }

    public final Factory2 getFactory2()
    {
        return this.mFactory2;
    }

    public Filter getFilter()
    {
        return this.mFilter;
    }

    public View inflate(int paramInt, ViewGroup paramViewGroup)
    {
        if (paramViewGroup != null);
        for (boolean bool = true; ; bool = false)
            return inflate(paramInt, paramViewGroup, bool);
    }

    public View inflate(int paramInt, ViewGroup paramViewGroup, boolean paramBoolean)
    {
        XmlResourceParser localXmlResourceParser = getContext().getResources().getLayout(paramInt);
        try
        {
            View localView = inflate(localXmlResourceParser, paramViewGroup, paramBoolean);
            return localView;
        }
        finally
        {
            localXmlResourceParser.close();
        }
    }

    public View inflate(XmlPullParser paramXmlPullParser, ViewGroup paramViewGroup)
    {
        if (paramViewGroup != null);
        for (boolean bool = true; ; bool = false)
            return inflate(paramXmlPullParser, paramViewGroup, bool);
    }

    // ERROR //
    public View inflate(XmlPullParser paramXmlPullParser, ViewGroup paramViewGroup, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_1
        //     10: invokestatic 172	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     13: astore 6
        //     15: aload_0
        //     16: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     19: iconst_0
        //     20: aaload
        //     21: checkcast 60	android/content/Context
        //     24: astore 7
        //     26: aload_0
        //     27: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     30: iconst_0
        //     31: aload_0
        //     32: getfield 79	android/view/LayoutInflater:mContext	Landroid/content/Context;
        //     35: aastore
        //     36: aload_2
        //     37: astore 8
        //     39: aload_1
        //     40: invokeinterface 203 1 0
        //     45: istore 16
        //     47: iload 16
        //     49: iconst_2
        //     50: if_icmpeq +9 -> 59
        //     53: iload 16
        //     55: iconst_1
        //     56: if_icmpne -17 -> 39
        //     59: iload 16
        //     61: iconst_2
        //     62: if_icmpeq +90 -> 152
        //     65: new 108	android/view/InflateException
        //     68: dup
        //     69: new 92	java/lang/StringBuilder
        //     72: dup
        //     73: invokespecial 93	java/lang/StringBuilder:<init>	()V
        //     76: aload_1
        //     77: invokeinterface 371 1 0
        //     82: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     85: ldc 181
        //     87: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     90: invokevirtual 106	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     93: invokespecial 111	android/view/InflateException:<init>	(Ljava/lang/String;)V
        //     96: athrow
        //     97: astore 13
        //     99: new 108	android/view/InflateException
        //     102: dup
        //     103: aload 13
        //     105: invokevirtual 374	org/xmlpull/v1/XmlPullParserException:getMessage	()Ljava/lang/String;
        //     108: invokespecial 111	android/view/InflateException:<init>	(Ljava/lang/String;)V
        //     111: astore 14
        //     113: aload 14
        //     115: aload 13
        //     117: invokevirtual 326	android/view/InflateException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     120: pop
        //     121: aload 14
        //     123: athrow
        //     124: astore 12
        //     126: aload_0
        //     127: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     130: iconst_0
        //     131: aload 7
        //     133: aastore
        //     134: aload_0
        //     135: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     138: iconst_1
        //     139: aconst_null
        //     140: aastore
        //     141: aload 12
        //     143: athrow
        //     144: astore 5
        //     146: aload 4
        //     148: monitorexit
        //     149: aload 5
        //     151: athrow
        //     152: aload_1
        //     153: invokeinterface 375 1 0
        //     158: astore 17
        //     160: ldc 29
        //     162: aload 17
        //     164: invokevirtual 193	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     167: ifeq +107 -> 274
        //     170: aload_2
        //     171: ifnull +7 -> 178
        //     174: iload_3
        //     175: ifne +69 -> 244
        //     178: new 108	android/view/InflateException
        //     181: dup
        //     182: ldc_w 377
        //     185: invokespecial 111	android/view/InflateException:<init>	(Ljava/lang/String;)V
        //     188: athrow
        //     189: astore 9
        //     191: new 108	android/view/InflateException
        //     194: dup
        //     195: new 92	java/lang/StringBuilder
        //     198: dup
        //     199: invokespecial 93	java/lang/StringBuilder:<init>	()V
        //     202: aload_1
        //     203: invokeinterface 371 1 0
        //     208: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: ldc_w 379
        //     214: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     217: aload 9
        //     219: invokevirtual 380	java/io/IOException:getMessage	()Ljava/lang/String;
        //     222: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     225: invokevirtual 106	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     228: invokespecial 111	android/view/InflateException:<init>	(Ljava/lang/String;)V
        //     231: astore 10
        //     233: aload 10
        //     235: aload 9
        //     237: invokevirtual 326	android/view/InflateException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     240: pop
        //     241: aload 10
        //     243: athrow
        //     244: aload_0
        //     245: aload_1
        //     246: aload_2
        //     247: aload 6
        //     249: iconst_0
        //     250: invokevirtual 197	android/view/LayoutInflater:rInflate	(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V
        //     253: aload_0
        //     254: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     257: iconst_0
        //     258: aload 7
        //     260: aastore
        //     261: aload_0
        //     262: getfield 77	android/view/LayoutInflater:mConstructorArgs	[Ljava/lang/Object;
        //     265: iconst_1
        //     266: aconst_null
        //     267: aastore
        //     268: aload 4
        //     270: monitorexit
        //     271: aload 8
        //     273: areturn
        //     274: ldc 23
        //     276: aload 17
        //     278: invokevirtual 193	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     281: ifeq +73 -> 354
        //     284: new 6	android/view/LayoutInflater$BlinkLayout
        //     287: dup
        //     288: aload_0
        //     289: getfield 79	android/view/LayoutInflater:mContext	Landroid/content/Context;
        //     292: aload 6
        //     294: invokespecial 383	android/view/LayoutInflater$BlinkLayout:<init>	(Landroid/content/Context;Landroid/util/AttributeSet;)V
        //     297: astore 18
        //     299: aconst_null
        //     300: astore 19
        //     302: aload_2
        //     303: ifnull +22 -> 325
        //     306: aload_2
        //     307: aload 6
        //     309: invokevirtual 211	android/view/ViewGroup:generateLayoutParams	(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
        //     312: astore 19
        //     314: iload_3
        //     315: ifne +10 -> 325
        //     318: aload 18
        //     320: aload 19
        //     322: invokevirtual 217	android/view/View:setLayoutParams	(Landroid/view/ViewGroup$LayoutParams;)V
        //     325: aload_0
        //     326: aload_1
        //     327: aload 18
        //     329: aload 6
        //     331: iconst_1
        //     332: invokevirtual 197	android/view/LayoutInflater:rInflate	(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V
        //     335: aload_2
        //     336: ifnull +36 -> 372
        //     339: iload_3
        //     340: ifeq +32 -> 372
        //     343: aload_2
        //     344: aload 18
        //     346: aload 19
        //     348: invokevirtual 386	android/view/ViewGroup:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
        //     351: goto +21 -> 372
        //     354: aload_0
        //     355: aload_2
        //     356: aload 17
        //     358: aload 6
        //     360: invokevirtual 207	android/view/LayoutInflater:createViewFromTag	(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
        //     363: astore 20
        //     365: aload 20
        //     367: astore 18
        //     369: goto -70 -> 299
        //     372: aload_2
        //     373: ifnull +7 -> 380
        //     376: iload_3
        //     377: ifne -124 -> 253
        //     380: aload 18
        //     382: astore 8
        //     384: goto -131 -> 253
        //
        // Exception table:
        //     from	to	target	type
        //     39	97	97	org/xmlpull/v1/XmlPullParserException
        //     152	189	97	org/xmlpull/v1/XmlPullParserException
        //     244	253	97	org/xmlpull/v1/XmlPullParserException
        //     274	365	97	org/xmlpull/v1/XmlPullParserException
        //     39	97	124	finally
        //     99	124	124	finally
        //     152	189	124	finally
        //     191	244	124	finally
        //     244	253	124	finally
        //     274	365	124	finally
        //     9	36	144	finally
        //     126	149	144	finally
        //     253	271	144	finally
        //     39	97	189	java/io/IOException
        //     152	189	189	java/io/IOException
        //     244	253	189	java/io/IOException
        //     274	365	189	java/io/IOException
    }

    protected View onCreateView(View paramView, String paramString, AttributeSet paramAttributeSet)
        throws ClassNotFoundException
    {
        return onCreateView(paramString, paramAttributeSet);
    }

    protected View onCreateView(String paramString, AttributeSet paramAttributeSet)
        throws ClassNotFoundException
    {
        return createView(paramString, "android.view.", paramAttributeSet);
    }

    void rInflate(XmlPullParser paramXmlPullParser, View paramView, AttributeSet paramAttributeSet, boolean paramBoolean)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        while (true)
        {
            int j = paramXmlPullParser.next();
            if (((j == 3) && (paramXmlPullParser.getDepth() <= i)) || (j == 1))
                break;
            if (j == 2)
            {
                String str = paramXmlPullParser.getName();
                if ("requestFocus".equals(str))
                {
                    parseRequestFocus(paramXmlPullParser, paramView);
                }
                else if ("include".equals(str))
                {
                    if (paramXmlPullParser.getDepth() == 0)
                        throw new InflateException("<include /> cannot be the root element");
                    parseInclude(paramXmlPullParser, paramView, paramAttributeSet);
                }
                else
                {
                    if ("merge".equals(str))
                        throw new InflateException("<merge /> must be the root element");
                    if ("blink".equals(str))
                    {
                        BlinkLayout localBlinkLayout = new BlinkLayout(this.mContext, paramAttributeSet);
                        ViewGroup localViewGroup1 = (ViewGroup)paramView;
                        ViewGroup.LayoutParams localLayoutParams1 = localViewGroup1.generateLayoutParams(paramAttributeSet);
                        rInflate(paramXmlPullParser, localBlinkLayout, paramAttributeSet, true);
                        localViewGroup1.addView(localBlinkLayout, localLayoutParams1);
                    }
                    else
                    {
                        View localView = createViewFromTag(paramView, str, paramAttributeSet);
                        ViewGroup localViewGroup2 = (ViewGroup)paramView;
                        ViewGroup.LayoutParams localLayoutParams2 = localViewGroup2.generateLayoutParams(paramAttributeSet);
                        rInflate(paramXmlPullParser, localView, paramAttributeSet, true);
                        localViewGroup2.addView(localView, localLayoutParams2);
                    }
                }
            }
        }
        if (paramBoolean)
            paramView.onFinishInflate();
    }

    public void setFactory(Factory paramFactory)
    {
        if (this.mFactorySet)
            throw new IllegalStateException("A factory has already been set on this LayoutInflater");
        if (paramFactory == null)
            throw new NullPointerException("Given factory can not be null");
        this.mFactorySet = true;
        if (this.mFactory == null);
        for (this.mFactory = paramFactory; ; this.mFactory = new FactoryMerger(paramFactory, null, this.mFactory, this.mFactory2))
            return;
    }

    public void setFactory2(Factory2 paramFactory2)
    {
        if (this.mFactorySet)
            throw new IllegalStateException("A factory has already been set on this LayoutInflater");
        if (paramFactory2 == null)
            throw new NullPointerException("Given factory can not be null");
        this.mFactorySet = true;
        if (this.mFactory == null)
            this.mFactory2 = paramFactory2;
        for (this.mFactory = paramFactory2; ; this.mFactory = new FactoryMerger(paramFactory2, paramFactory2, this.mFactory, this.mFactory2))
            return;
    }

    public void setFilter(Filter paramFilter)
    {
        this.mFilter = paramFilter;
        if (paramFilter != null)
            this.mFilterMap = new HashMap();
    }

    public void setPrivateFactory(Factory2 paramFactory2)
    {
        this.mPrivateFactory = paramFactory2;
    }

    private static class BlinkLayout extends FrameLayout
    {
        private static final int BLINK_DELAY = 500;
        private static final int MESSAGE_BLINK = 66;
        private boolean mBlink;
        private boolean mBlinkState;
        private final Handler mHandler = new Handler(new Handler.Callback()
        {
            public boolean handleMessage(Message paramAnonymousMessage)
            {
                boolean bool1 = true;
                boolean bool2 = false;
                if (paramAnonymousMessage.what == 66)
                {
                    if (LayoutInflater.BlinkLayout.this.mBlink)
                    {
                        LayoutInflater.BlinkLayout localBlinkLayout = LayoutInflater.BlinkLayout.this;
                        if (!LayoutInflater.BlinkLayout.this.mBlinkState)
                            bool2 = bool1;
                        LayoutInflater.BlinkLayout.access$102(localBlinkLayout, bool2);
                        LayoutInflater.BlinkLayout.this.makeBlink();
                    }
                    LayoutInflater.BlinkLayout.this.invalidate();
                }
                while (true)
                {
                    return bool1;
                    bool1 = false;
                }
            }
        });

        public BlinkLayout(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        private void makeBlink()
        {
            Message localMessage = this.mHandler.obtainMessage(66);
            this.mHandler.sendMessageDelayed(localMessage, 500L);
        }

        protected void dispatchDraw(Canvas paramCanvas)
        {
            if (this.mBlinkState)
                super.dispatchDraw(paramCanvas);
        }

        protected void onAttachedToWindow()
        {
            super.onAttachedToWindow();
            this.mBlink = true;
            this.mBlinkState = true;
            makeBlink();
        }

        protected void onDetachedFromWindow()
        {
            super.onDetachedFromWindow();
            this.mBlink = false;
            this.mBlinkState = true;
            this.mHandler.removeMessages(66);
        }
    }

    private static class FactoryMerger
        implements LayoutInflater.Factory2
    {
        private final LayoutInflater.Factory mF1;
        private final LayoutInflater.Factory2 mF12;
        private final LayoutInflater.Factory mF2;
        private final LayoutInflater.Factory2 mF22;

        FactoryMerger(LayoutInflater.Factory paramFactory1, LayoutInflater.Factory2 paramFactory21, LayoutInflater.Factory paramFactory2, LayoutInflater.Factory2 paramFactory22)
        {
            this.mF1 = paramFactory1;
            this.mF2 = paramFactory2;
            this.mF12 = paramFactory21;
            this.mF22 = paramFactory22;
        }

        public View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
        {
            if (this.mF12 != null);
            for (Object localObject = this.mF12.onCreateView(paramView, paramString, paramContext, paramAttributeSet); localObject != null; localObject = this.mF1.onCreateView(paramString, paramContext, paramAttributeSet))
                return localObject;
            if (this.mF22 != null);
            for (View localView = this.mF22.onCreateView(paramView, paramString, paramContext, paramAttributeSet); ; localView = this.mF2.onCreateView(paramString, paramContext, paramAttributeSet))
            {
                localObject = localView;
                break;
            }
        }

        public View onCreateView(String paramString, Context paramContext, AttributeSet paramAttributeSet)
        {
            View localView = this.mF1.onCreateView(paramString, paramContext, paramAttributeSet);
            if (localView != null);
            while (true)
            {
                return localView;
                localView = this.mF2.onCreateView(paramString, paramContext, paramAttributeSet);
            }
        }
    }

    public static abstract interface Factory2 extends LayoutInflater.Factory
    {
        public abstract View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet);
    }

    public static abstract interface Factory
    {
        public abstract View onCreateView(String paramString, Context paramContext, AttributeSet paramAttributeSet);
    }

    public static abstract interface Filter
    {
        public abstract boolean onLoadClass(Class paramClass);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.LayoutInflater
 * JD-Core Version:        0.6.2
 */