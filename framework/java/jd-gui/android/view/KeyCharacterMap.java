package android.view;

import android.hardware.input.InputManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AndroidRuntimeException;
import android.util.SparseIntArray;
import java.text.Normalizer;
import java.text.Normalizer.Form;

public class KeyCharacterMap
    implements Parcelable
{
    private static final int ACCENT_ACUTE = 180;
    private static final int ACCENT_BREVE = 728;
    private static final int ACCENT_CARON = 711;
    private static final int ACCENT_CEDILLA = 184;
    private static final int ACCENT_CIRCUMFLEX = 710;
    private static final int ACCENT_CIRCUMFLEX_LEGACY = 94;
    private static final int ACCENT_COMMA_ABOVE = 8125;
    private static final int ACCENT_COMMA_ABOVE_RIGHT = 700;
    private static final int ACCENT_DOT_ABOVE = 729;
    private static final int ACCENT_DOT_BELOW = 46;
    private static final int ACCENT_DOUBLE_ACUTE = 733;
    private static final int ACCENT_GRAVE = 715;
    private static final int ACCENT_GRAVE_LEGACY = 96;
    private static final int ACCENT_HOOK_ABOVE = 704;
    private static final int ACCENT_HORN = 39;
    private static final int ACCENT_MACRON = 175;
    private static final int ACCENT_MACRON_BELOW = 717;
    private static final int ACCENT_OGONEK = 731;
    private static final int ACCENT_REVERSED_COMMA_ABOVE = 701;
    private static final int ACCENT_RING_ABOVE = 730;
    private static final int ACCENT_STROKE = 45;
    private static final int ACCENT_TILDE = 732;
    private static final int ACCENT_TILDE_LEGACY = 126;
    private static final int ACCENT_TURNED_COMMA_ABOVE = 699;
    private static final int ACCENT_UMLAUT = 168;
    private static final int ACCENT_VERTICAL_LINE_ABOVE = 712;
    private static final int ACCENT_VERTICAL_LINE_BELOW = 716;
    public static final int ALPHA = 3;

    @Deprecated
    public static final int BUILT_IN_KEYBOARD = 0;
    public static final int COMBINING_ACCENT = -2147483648;
    public static final int COMBINING_ACCENT_MASK = 2147483647;
    public static final Parcelable.Creator<KeyCharacterMap> CREATOR = new Parcelable.Creator()
    {
        public KeyCharacterMap createFromParcel(Parcel paramAnonymousParcel)
        {
            return new KeyCharacterMap(paramAnonymousParcel, null);
        }

        public KeyCharacterMap[] newArray(int paramAnonymousInt)
        {
            return new KeyCharacterMap[paramAnonymousInt];
        }
    };
    public static final int FULL = 4;
    public static final char HEX_INPUT = '';
    public static final int MODIFIER_BEHAVIOR_CHORDED = 0;
    public static final int MODIFIER_BEHAVIOR_CHORDED_OR_TOGGLED = 1;
    public static final int NUMERIC = 1;
    public static final char PICKER_DIALOG_INPUT = '';
    public static final int PREDICTIVE = 2;
    public static final int SPECIAL_FUNCTION = 5;
    public static final int VIRTUAL_KEYBOARD = -1;
    private static final SparseIntArray sAccentToCombining;
    private static final SparseIntArray sCombiningToAccent = new SparseIntArray();
    private static final StringBuilder sDeadKeyBuilder;
    private static final SparseIntArray sDeadKeyCache;
    private int mPtr;

    static
    {
        sAccentToCombining = new SparseIntArray();
        addCombining(768, 715);
        addCombining(769, 180);
        addCombining(770, 710);
        addCombining(771, 732);
        addCombining(772, 175);
        addCombining(774, 728);
        addCombining(775, 729);
        addCombining(776, 168);
        addCombining(777, 704);
        addCombining(778, 730);
        addCombining(779, 733);
        addCombining(780, 711);
        addCombining(781, 712);
        addCombining(786, 699);
        addCombining(787, 8125);
        addCombining(788, 701);
        addCombining(789, 700);
        addCombining(795, 39);
        addCombining(803, 46);
        addCombining(807, 184);
        addCombining(808, 731);
        addCombining(809, 716);
        addCombining(817, 717);
        addCombining(821, 45);
        sCombiningToAccent.append(832, 715);
        sCombiningToAccent.append(833, 180);
        sCombiningToAccent.append(835, 8125);
        sAccentToCombining.append(96, 768);
        sAccentToCombining.append(94, 770);
        sAccentToCombining.append(126, 771);
        sDeadKeyCache = new SparseIntArray();
        sDeadKeyBuilder = new StringBuilder();
        addDeadKey(45, 68, 272);
        addDeadKey(45, 71, 484);
        addDeadKey(45, 72, 294);
        addDeadKey(45, 73, 407);
        addDeadKey(45, 76, 321);
        addDeadKey(45, 79, 216);
        addDeadKey(45, 84, 358);
        addDeadKey(45, 100, 273);
        addDeadKey(45, 103, 485);
        addDeadKey(45, 104, 295);
        addDeadKey(45, 105, 616);
        addDeadKey(45, 108, 322);
        addDeadKey(45, 111, 248);
        addDeadKey(45, 116, 359);
    }

    private KeyCharacterMap(int paramInt)
    {
        this.mPtr = paramInt;
    }

    private KeyCharacterMap(Parcel paramParcel)
    {
        if (paramParcel == null)
            throw new IllegalArgumentException("parcel must not be null");
        this.mPtr = nativeReadFromParcel(paramParcel);
        if (this.mPtr == 0)
            throw new RuntimeException("Could not read KeyCharacterMap from parcel.");
    }

    private static void addCombining(int paramInt1, int paramInt2)
    {
        sCombiningToAccent.append(paramInt1, paramInt2);
        sAccentToCombining.append(paramInt2, paramInt1);
    }

    private static void addDeadKey(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = sAccentToCombining.get(paramInt1);
        if (i == 0)
            throw new IllegalStateException("Invalid dead key declaration.");
        int j = paramInt2 | i << 16;
        sDeadKeyCache.put(j, paramInt3);
    }

    public static boolean deviceHasKey(int paramInt)
    {
        InputManager localInputManager = InputManager.getInstance();
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = paramInt;
        return localInputManager.deviceHasKeys(arrayOfInt)[0];
    }

    public static boolean[] deviceHasKeys(int[] paramArrayOfInt)
    {
        return InputManager.getInstance().deviceHasKeys(paramArrayOfInt);
    }

    public static int getDeadChar(int paramInt1, int paramInt2)
    {
        int i = sAccentToCombining.get(paramInt1);
        int k;
        if (i == 0)
        {
            k = 0;
            return k;
        }
        int j = paramInt2 | i << 16;
        while (true)
        {
            synchronized (sDeadKeyCache)
            {
                k = sDeadKeyCache.get(j, -1);
                if (k == -1)
                {
                    sDeadKeyBuilder.setLength(0);
                    sDeadKeyBuilder.append((char)paramInt2);
                    sDeadKeyBuilder.append((char)i);
                    String str = Normalizer.normalize(sDeadKeyBuilder, Normalizer.Form.NFC);
                    if (str.length() != 1)
                        break label127;
                    k = str.charAt(0);
                    sDeadKeyCache.put(j, k);
                }
            }
            label127: k = 0;
        }
    }

    public static KeyCharacterMap load(int paramInt)
    {
        InputManager localInputManager = InputManager.getInstance();
        InputDevice localInputDevice = localInputManager.getInputDevice(paramInt);
        if (localInputDevice == null)
        {
            localInputDevice = localInputManager.getInputDevice(-1);
            if (localInputDevice == null)
                throw new UnavailableException("Could not load key character map for device " + paramInt);
        }
        return localInputDevice.getKeyCharacterMap();
    }

    private static native void nativeDispose(int paramInt);

    private static native char nativeGetCharacter(int paramInt1, int paramInt2, int paramInt3);

    private static native char nativeGetDisplayLabel(int paramInt1, int paramInt2);

    private static native KeyEvent[] nativeGetEvents(int paramInt, char[] paramArrayOfChar);

    private static native boolean nativeGetFallbackAction(int paramInt1, int paramInt2, int paramInt3, FallbackAction paramFallbackAction);

    private static native int nativeGetKeyboardType(int paramInt);

    private static native char nativeGetMatch(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3);

    private static native char nativeGetNumber(int paramInt1, int paramInt2);

    private static native int nativeReadFromParcel(Parcel paramParcel);

    private static native void nativeWriteToParcel(int paramInt, Parcel paramParcel);

    public int describeContents()
    {
        return 0;
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mPtr != 0)
        {
            nativeDispose(this.mPtr);
            this.mPtr = 0;
        }
    }

    public int get(int paramInt1, int paramInt2)
    {
        int i = KeyEvent.normalizeMetaState(paramInt2);
        int j = nativeGetCharacter(this.mPtr, paramInt1, i);
        int k = sCombiningToAccent.get(j);
        if (k != 0)
            j = k | 0x80000000;
        return j;
    }

    public char getDisplayLabel(int paramInt)
    {
        return nativeGetDisplayLabel(this.mPtr, paramInt);
    }

    public KeyEvent[] getEvents(char[] paramArrayOfChar)
    {
        if (paramArrayOfChar == null)
            throw new IllegalArgumentException("chars must not be null.");
        return nativeGetEvents(this.mPtr, paramArrayOfChar);
    }

    public FallbackAction getFallbackAction(int paramInt1, int paramInt2)
    {
        FallbackAction localFallbackAction = FallbackAction.obtain();
        int i = KeyEvent.normalizeMetaState(paramInt2);
        if (nativeGetFallbackAction(this.mPtr, paramInt1, i, localFallbackAction))
            localFallbackAction.metaState = KeyEvent.normalizeMetaState(localFallbackAction.metaState);
        while (true)
        {
            return localFallbackAction;
            localFallbackAction.recycle();
            localFallbackAction = null;
        }
    }

    @Deprecated
    public boolean getKeyData(int paramInt, KeyData paramKeyData)
    {
        boolean bool = false;
        if (paramKeyData.meta.length < 4)
            throw new IndexOutOfBoundsException("results.meta.length must be >= 4");
        char c = nativeGetDisplayLabel(this.mPtr, paramInt);
        if (c == 0);
        while (true)
        {
            return bool;
            paramKeyData.displayLabel = c;
            paramKeyData.number = nativeGetNumber(this.mPtr, paramInt);
            paramKeyData.meta[bool] = nativeGetCharacter(this.mPtr, paramInt, 0);
            paramKeyData.meta[1] = nativeGetCharacter(this.mPtr, paramInt, 1);
            paramKeyData.meta[2] = nativeGetCharacter(this.mPtr, paramInt, 2);
            paramKeyData.meta[3] = nativeGetCharacter(this.mPtr, paramInt, 3);
            bool = true;
        }
    }

    public int getKeyboardType()
    {
        return nativeGetKeyboardType(this.mPtr);
    }

    public char getMatch(int paramInt, char[] paramArrayOfChar)
    {
        return getMatch(paramInt, paramArrayOfChar, 0);
    }

    public char getMatch(int paramInt1, char[] paramArrayOfChar, int paramInt2)
    {
        if (paramArrayOfChar == null)
            throw new IllegalArgumentException("chars must not be null.");
        int i = KeyEvent.normalizeMetaState(paramInt2);
        return nativeGetMatch(this.mPtr, paramInt1, paramArrayOfChar, i);
    }

    public int getModifierBehavior()
    {
        switch (getKeyboardType())
        {
        default:
        case 4:
        case 5:
        }
        for (int i = 1; ; i = 0)
            return i;
    }

    public char getNumber(int paramInt)
    {
        return nativeGetNumber(this.mPtr, paramInt);
    }

    public boolean isPrintingKey(int paramInt)
    {
        switch (Character.getType(nativeGetDisplayLabel(this.mPtr, paramInt)))
        {
        default:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (paramParcel == null)
            throw new IllegalArgumentException("parcel must not be null");
        nativeWriteToParcel(this.mPtr, paramParcel);
    }

    public static final class FallbackAction
    {
        private static final int MAX_RECYCLED = 10;
        private static FallbackAction sRecycleBin;
        private static final Object sRecycleLock = new Object();
        private static int sRecycledCount;
        public int keyCode;
        public int metaState;
        private FallbackAction next;

        public static FallbackAction obtain()
        {
            synchronized (sRecycleLock)
            {
                if (sRecycleBin == null)
                {
                    localFallbackAction = new FallbackAction();
                    return localFallbackAction;
                }
                FallbackAction localFallbackAction = sRecycleBin;
                sRecycleBin = localFallbackAction.next;
                sRecycledCount = -1 + sRecycledCount;
                localFallbackAction.next = null;
            }
        }

        public void recycle()
        {
            synchronized (sRecycleLock)
            {
                if (sRecycledCount < 10)
                {
                    this.next = sRecycleBin;
                    sRecycleBin = this;
                    sRecycledCount = 1 + sRecycledCount;
                    return;
                }
                this.next = null;
            }
        }
    }

    public static class UnavailableException extends AndroidRuntimeException
    {
        public UnavailableException(String paramString)
        {
            super();
        }
    }

    @Deprecated
    public static class KeyData
    {
        public static final int META_LENGTH = 4;
        public char displayLabel;
        public char[] meta = new char[4];
        public char number;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.KeyCharacterMap
 * JD-Core Version:        0.6.2
 */