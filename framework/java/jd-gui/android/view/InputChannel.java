package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class InputChannel
    implements Parcelable
{
    public static final Parcelable.Creator<InputChannel> CREATOR = new Parcelable.Creator()
    {
        public InputChannel createFromParcel(Parcel paramAnonymousParcel)
        {
            InputChannel localInputChannel = new InputChannel();
            localInputChannel.readFromParcel(paramAnonymousParcel);
            return localInputChannel;
        }

        public InputChannel[] newArray(int paramAnonymousInt)
        {
            return new InputChannel[paramAnonymousInt];
        }
    };
    private static final boolean DEBUG = false;
    private static final String TAG = "InputChannel";
    private int mPtr;

    private native void nativeDispose(boolean paramBoolean);

    private native String nativeGetName();

    private static native InputChannel[] nativeOpenInputChannelPair(String paramString);

    private native void nativeReadFromParcel(Parcel paramParcel);

    private native void nativeTransferTo(InputChannel paramInputChannel);

    private native void nativeWriteToParcel(Parcel paramParcel);

    public static InputChannel[] openInputChannelPair(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("name must not be null");
        return nativeOpenInputChannelPair(paramString);
    }

    public int describeContents()
    {
        return 1;
    }

    public void dispose()
    {
        nativeDispose(false);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDispose(true);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public String getName()
    {
        String str = nativeGetName();
        if (str != null);
        while (true)
        {
            return str;
            str = "uninitialized";
        }
    }

    public void readFromParcel(Parcel paramParcel)
    {
        if (paramParcel == null)
            throw new IllegalArgumentException("in must not be null");
        nativeReadFromParcel(paramParcel);
    }

    public String toString()
    {
        return getName();
    }

    public void transferTo(InputChannel paramInputChannel)
    {
        if (paramInputChannel == null)
            throw new IllegalArgumentException("outParameter must not be null");
        nativeTransferTo(paramInputChannel);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (paramParcel == null)
            throw new IllegalArgumentException("out must not be null");
        nativeWriteToParcel(paramParcel);
        if ((paramInt & 0x1) != 0)
            dispose();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InputChannel
 * JD-Core Version:        0.6.2
 */