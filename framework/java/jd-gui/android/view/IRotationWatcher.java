package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IRotationWatcher extends IInterface
{
    public abstract void onRotationChanged(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRotationWatcher
    {
        private static final String DESCRIPTOR = "android.view.IRotationWatcher";
        static final int TRANSACTION_onRotationChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.view.IRotationWatcher");
        }

        public static IRotationWatcher asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.IRotationWatcher");
                if ((localIInterface != null) && ((localIInterface instanceof IRotationWatcher)))
                    localObject = (IRotationWatcher)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.view.IRotationWatcher");
                continue;
                paramParcel1.enforceInterface("android.view.IRotationWatcher");
                onRotationChanged(paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IRotationWatcher
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.IRotationWatcher";
            }

            public void onRotationChanged(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IRotationWatcher");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.IRotationWatcher
 * JD-Core Version:        0.6.2
 */