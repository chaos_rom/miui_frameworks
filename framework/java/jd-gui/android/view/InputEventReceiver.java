package android.view;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import android.util.SparseIntArray;
import dalvik.system.CloseGuard;

public abstract class InputEventReceiver
{
    private static final String TAG = "InputEventReceiver";
    private final CloseGuard mCloseGuard = CloseGuard.get();
    private InputChannel mInputChannel;
    private MessageQueue mMessageQueue;
    private int mReceiverPtr;
    private final SparseIntArray mSeqMap = new SparseIntArray();

    public InputEventReceiver(InputChannel paramInputChannel, Looper paramLooper)
    {
        if (paramInputChannel == null)
            throw new IllegalArgumentException("inputChannel must not be null");
        if (paramLooper == null)
            throw new IllegalArgumentException("looper must not be null");
        this.mInputChannel = paramInputChannel;
        this.mMessageQueue = paramLooper.getQueue();
        this.mReceiverPtr = nativeInit(this, paramInputChannel, this.mMessageQueue);
        this.mCloseGuard.open("dispose");
    }

    private void dispatchBatchedInputEventPending()
    {
        onBatchedInputEventPending();
    }

    private void dispatchInputEvent(int paramInt, InputEvent paramInputEvent)
    {
        this.mSeqMap.put(paramInputEvent.getSequenceNumber(), paramInt);
        onInputEvent(paramInputEvent);
    }

    private static native void nativeConsumeBatchedInputEvents(int paramInt, long paramLong);

    private static native void nativeDispose(int paramInt);

    private static native void nativeFinishInputEvent(int paramInt1, int paramInt2, boolean paramBoolean);

    private static native int nativeInit(InputEventReceiver paramInputEventReceiver, InputChannel paramInputChannel, MessageQueue paramMessageQueue);

    public final void consumeBatchedInputEvents(long paramLong)
    {
        if (this.mReceiverPtr == 0)
            Log.w("InputEventReceiver", "Attempted to consume batched input events but the input event receiver has already been disposed.");
        while (true)
        {
            return;
            nativeConsumeBatchedInputEvents(this.mReceiverPtr, paramLong);
        }
    }

    public void dispose()
    {
        if (this.mCloseGuard != null)
            this.mCloseGuard.close();
        if (this.mReceiverPtr != 0)
        {
            nativeDispose(this.mReceiverPtr);
            this.mReceiverPtr = 0;
        }
        this.mInputChannel = null;
        this.mMessageQueue = null;
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            dispose();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public final void finishInputEvent(InputEvent paramInputEvent, boolean paramBoolean)
    {
        if (paramInputEvent == null)
            throw new IllegalArgumentException("event must not be null");
        if (this.mReceiverPtr == 0)
            Log.w("InputEventReceiver", "Attempted to finish an input event but the input event receiver has already been disposed.");
        while (true)
        {
            paramInputEvent.recycleIfNeededAfterDispatch();
            return;
            int i = this.mSeqMap.indexOfKey(paramInputEvent.getSequenceNumber());
            if (i < 0)
            {
                Log.w("InputEventReceiver", "Attempted to finish an input event that is not in progress.");
            }
            else
            {
                int j = this.mSeqMap.valueAt(i);
                this.mSeqMap.removeAt(i);
                nativeFinishInputEvent(this.mReceiverPtr, j, paramBoolean);
            }
        }
    }

    public void onBatchedInputEventPending()
    {
        consumeBatchedInputEvents(-1L);
    }

    public void onInputEvent(InputEvent paramInputEvent)
    {
        finishInputEvent(paramInputEvent, false);
    }

    public static abstract interface Factory
    {
        public abstract InputEventReceiver createInputEventReceiver(InputChannel paramInputChannel, Looper paramLooper);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InputEventReceiver
 * JD-Core Version:        0.6.2
 */