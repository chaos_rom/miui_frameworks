package android.view;

import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IWindow extends IInterface
{
    public abstract void closeSystemDialogs(String paramString)
        throws RemoteException;

    public abstract void dispatchAppVisibility(boolean paramBoolean)
        throws RemoteException;

    public abstract void dispatchDragEvent(DragEvent paramDragEvent)
        throws RemoteException;

    public abstract void dispatchGetNewSurface()
        throws RemoteException;

    public abstract void dispatchScreenState(boolean paramBoolean)
        throws RemoteException;

    public abstract void dispatchSystemUiVisibilityChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        throws RemoteException;

    public abstract void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
        throws RemoteException;

    public abstract void dispatchWallpaperOffsets(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, boolean paramBoolean)
        throws RemoteException;

    public abstract void doneAnimating()
        throws RemoteException;

    public abstract void executeCommand(String paramString1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException;

    public abstract void resized(int paramInt1, int paramInt2, Rect paramRect1, Rect paramRect2, boolean paramBoolean, Configuration paramConfiguration)
        throws RemoteException;

    public abstract void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWindow
    {
        private static final String DESCRIPTOR = "android.view.IWindow";
        static final int TRANSACTION_closeSystemDialogs = 7;
        static final int TRANSACTION_dispatchAppVisibility = 3;
        static final int TRANSACTION_dispatchDragEvent = 10;
        static final int TRANSACTION_dispatchGetNewSurface = 4;
        static final int TRANSACTION_dispatchScreenState = 5;
        static final int TRANSACTION_dispatchSystemUiVisibilityChanged = 11;
        static final int TRANSACTION_dispatchWallpaperCommand = 9;
        static final int TRANSACTION_dispatchWallpaperOffsets = 8;
        static final int TRANSACTION_doneAnimating = 12;
        static final int TRANSACTION_executeCommand = 1;
        static final int TRANSACTION_resized = 2;
        static final int TRANSACTION_windowFocusChanged = 6;

        public Stub()
        {
            attachInterface(this, "android.view.IWindow");
        }

        public static IWindow asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.IWindow");
                if ((localIInterface != null) && ((localIInterface instanceof IWindow)))
                    localObject = (IWindow)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.view.IWindow");
                continue;
                paramParcel1.enforceInterface("android.view.IWindow");
                String str2 = paramParcel1.readString();
                String str3 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (ParcelFileDescriptor localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor = null)
                {
                    executeCommand(str2, str3, localParcelFileDescriptor);
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                int m = paramParcel1.readInt();
                int n = paramParcel1.readInt();
                Rect localRect1;
                label240: Rect localRect2;
                label261: boolean bool8;
                if (paramParcel1.readInt() != 0)
                {
                    localRect1 = (Rect)Rect.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label318;
                    localRect2 = (Rect)Rect.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label324;
                    bool8 = bool1;
                    label272: if (paramParcel1.readInt() == 0)
                        break label330;
                }
                label318: label324: label330: for (Configuration localConfiguration = (Configuration)Configuration.CREATOR.createFromParcel(paramParcel1); ; localConfiguration = null)
                {
                    resized(m, n, localRect1, localRect2, bool8, localConfiguration);
                    break;
                    localRect1 = null;
                    break label240;
                    localRect2 = null;
                    break label261;
                    bool8 = false;
                    break label272;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                if (paramParcel1.readInt() != 0);
                for (boolean bool7 = bool1; ; bool7 = false)
                {
                    dispatchAppVisibility(bool7);
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                dispatchGetNewSurface();
                continue;
                paramParcel1.enforceInterface("android.view.IWindow");
                if (paramParcel1.readInt() != 0);
                for (boolean bool6 = bool1; ; bool6 = false)
                {
                    dispatchScreenState(bool6);
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                boolean bool4;
                if (paramParcel1.readInt() != 0)
                {
                    bool4 = bool1;
                    label430: if (paramParcel1.readInt() == 0)
                        break label458;
                }
                label458: for (boolean bool5 = bool1; ; bool5 = false)
                {
                    windowFocusChanged(bool4, bool5);
                    break;
                    bool4 = false;
                    break label430;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                closeSystemDialogs(paramParcel1.readString());
                continue;
                paramParcel1.enforceInterface("android.view.IWindow");
                float f1 = paramParcel1.readFloat();
                float f2 = paramParcel1.readFloat();
                float f3 = paramParcel1.readFloat();
                float f4 = paramParcel1.readFloat();
                if (paramParcel1.readInt() != 0);
                for (boolean bool3 = bool1; ; bool3 = false)
                {
                    dispatchWallpaperOffsets(f1, f2, f3, f4, bool3);
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                String str1 = paramParcel1.readString();
                int i = paramParcel1.readInt();
                int j = paramParcel1.readInt();
                int k = paramParcel1.readInt();
                Bundle localBundle;
                if (paramParcel1.readInt() != 0)
                {
                    localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                    label596: if (paramParcel1.readInt() == 0)
                        break label632;
                }
                label632: for (boolean bool2 = bool1; ; bool2 = false)
                {
                    dispatchWallpaperCommand(str1, i, j, k, localBundle, bool2);
                    break;
                    localBundle = null;
                    break label596;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                if (paramParcel1.readInt() != 0);
                for (DragEvent localDragEvent = (DragEvent)DragEvent.CREATOR.createFromParcel(paramParcel1); ; localDragEvent = null)
                {
                    dispatchDragEvent(localDragEvent);
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindow");
                dispatchSystemUiVisibilityChanged(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.view.IWindow");
                doneAnimating();
            }
        }

        private static class Proxy
            implements IWindow
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void closeSystemDialogs(String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    localParcel.writeString(paramString);
                    this.mRemote.transact(7, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchAppVisibility(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchDragEvent(DragEvent paramDragEvent)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    if (paramDragEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramDragEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(10, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchGetNewSurface()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchScreenState(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchSystemUiVisibilityChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    this.mRemote.transact(11, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.view.IWindow");
                        localParcel.writeString(paramString);
                        localParcel.writeInt(paramInt1);
                        localParcel.writeInt(paramInt2);
                        localParcel.writeInt(paramInt3);
                        if (paramBundle != null)
                        {
                            localParcel.writeInt(1);
                            paramBundle.writeToParcel(localParcel, 0);
                            break label116;
                            localParcel.writeInt(i);
                            this.mRemote.transact(9, localParcel, null, 1);
                        }
                        else
                        {
                            localParcel.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    label116: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void dispatchWallpaperOffsets(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    localParcel.writeFloat(paramFloat1);
                    localParcel.writeFloat(paramFloat2);
                    localParcel.writeFloat(paramFloat3);
                    localParcel.writeFloat(paramFloat4);
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(8, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void doneAnimating()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    this.mRemote.transact(12, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void executeCommand(String paramString1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    localParcel.writeString(paramString1);
                    localParcel.writeString(paramString2);
                    if (paramParcelFileDescriptor != null)
                    {
                        localParcel.writeInt(1);
                        paramParcelFileDescriptor.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.IWindow";
            }

            public void resized(int paramInt1, int paramInt2, Rect paramRect1, Rect paramRect2, boolean paramBoolean, Configuration paramConfiguration)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.view.IWindow");
                        localParcel.writeInt(paramInt1);
                        localParcel.writeInt(paramInt2);
                        if (paramRect1 != null)
                        {
                            localParcel.writeInt(1);
                            paramRect1.writeToParcel(localParcel, 0);
                            if (paramRect2 != null)
                            {
                                localParcel.writeInt(1);
                                paramRect2.writeToParcel(localParcel, 0);
                                break label150;
                                localParcel.writeInt(i);
                                if (paramConfiguration == null)
                                    break label141;
                                localParcel.writeInt(1);
                                paramConfiguration.writeToParcel(localParcel, 0);
                                this.mRemote.transact(2, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                    break label150;
                    label141: localParcel.writeInt(0);
                    continue;
                    label150: if (!paramBoolean)
                        i = 0;
                }
            }

            public void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.IWindow");
                    if (paramBoolean1);
                    for (int j = i; ; j = 0)
                    {
                        localParcel.writeInt(j);
                        if (!paramBoolean2)
                            break;
                        localParcel.writeInt(i);
                        this.mRemote.transact(6, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.IWindow
 * JD-Core Version:        0.6.2
 */