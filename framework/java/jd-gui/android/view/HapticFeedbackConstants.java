package android.view;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;

public class HapticFeedbackConstants
{
    public static final int FLAG_IGNORE_GLOBAL_SETTING = 2;
    public static final int FLAG_IGNORE_VIEW_SETTING = 1;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int FLAG_WHEN_ENABLED_EXPLICITLY = 4;
    public static final int KEYBOARD_TAP = 3;
    public static final int LONG_PRESS = 0;
    public static final int SAFE_MODE_DISABLED = 10000;
    public static final int SAFE_MODE_ENABLED = 10001;
    public static final int VIRTUAL_KEY = 1;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int VIRTUAL_RELEASED = 2;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.HapticFeedbackConstants
 * JD-Core Version:        0.6.2
 */