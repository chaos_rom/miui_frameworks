package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class DecelerateInterpolator
    implements Interpolator
{
    private float mFactor = 1.0F;

    public DecelerateInterpolator()
    {
    }

    public DecelerateInterpolator(float paramFloat)
    {
        this.mFactor = paramFloat;
    }

    public DecelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DecelerateInterpolator);
        this.mFactor = localTypedArray.getFloat(0, 1.0F);
        localTypedArray.recycle();
    }

    public float getInterpolation(float paramFloat)
    {
        if (this.mFactor == 1.0F);
        for (float f = 1.0F - (1.0F - paramFloat) * (1.0F - paramFloat); ; f = (float)(1.0D - Math.pow(1.0F - paramFloat, 2.0F * this.mFactor)))
            return f;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.DecelerateInterpolator
 * JD-Core Version:        0.6.2
 */