package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class AccelerateInterpolator
    implements Interpolator
{
    private final double mDoubleFactor;
    private final float mFactor;

    public AccelerateInterpolator()
    {
        this.mFactor = 1.0F;
        this.mDoubleFactor = 2.0D;
    }

    public AccelerateInterpolator(float paramFloat)
    {
        this.mFactor = paramFloat;
        this.mDoubleFactor = (2.0F * this.mFactor);
    }

    public AccelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AccelerateInterpolator);
        this.mFactor = localTypedArray.getFloat(0, 1.0F);
        this.mDoubleFactor = (2.0F * this.mFactor);
        localTypedArray.recycle();
    }

    public float getInterpolation(float paramFloat)
    {
        if (this.mFactor == 1.0F);
        for (float f = paramFloat * paramFloat; ; f = (float)Math.pow(paramFloat, this.mDoubleFactor))
            return f;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AccelerateInterpolator
 * JD-Core Version:        0.6.2
 */