package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Handler;
import android.os.SystemProperties;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import dalvik.system.CloseGuard;

public abstract class Animation
    implements Cloneable
{
    public static final int ABSOLUTE = 0;
    public static final int INFINITE = -1;
    public static final int RELATIVE_TO_PARENT = 2;
    public static final int RELATIVE_TO_SELF = 1;
    public static final int RESTART = 1;
    public static final int REVERSE = 2;
    public static final int START_ON_FIRST_FRAME = -1;
    private static final boolean USE_CLOSEGUARD = false;
    public static final int ZORDER_BOTTOM = -1;
    public static final int ZORDER_NORMAL = 0;
    public static final int ZORDER_TOP = 1;
    private final CloseGuard guard = CloseGuard.get();
    private int mBackgroundColor;
    boolean mCycleFlip = false;
    private boolean mDetachWallpaper = false;
    long mDuration;
    boolean mEnded = false;
    boolean mFillAfter = false;
    boolean mFillBefore = true;
    boolean mFillEnabled = false;
    boolean mInitialized = false;
    Interpolator mInterpolator;
    AnimationListener mListener;
    private Handler mListenerHandler;
    private boolean mMore = true;
    private Runnable mOnEnd;
    private Runnable mOnRepeat;
    private Runnable mOnStart;
    private boolean mOneMoreTime = true;
    RectF mPreviousRegion = new RectF();
    Transformation mPreviousTransformation = new Transformation();
    RectF mRegion = new RectF();
    int mRepeatCount = 0;
    int mRepeatMode = 1;
    int mRepeated = 0;
    private float mScaleFactor = 1.0F;
    long mStartOffset;
    long mStartTime = -1L;
    boolean mStarted = false;
    Transformation mTransformation = new Transformation();
    private int mZAdjustment;

    public Animation()
    {
        ensureInterpolator();
    }

    public Animation(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Animation);
        setDuration(localTypedArray.getInt(2, 0));
        setStartOffset(localTypedArray.getInt(5, 0));
        setFillEnabled(localTypedArray.getBoolean(9, this.mFillEnabled));
        setFillBefore(localTypedArray.getBoolean(3, this.mFillBefore));
        setFillAfter(localTypedArray.getBoolean(4, this.mFillAfter));
        setRepeatCount(localTypedArray.getInt(6, this.mRepeatCount));
        setRepeatMode(localTypedArray.getInt(7, 1));
        setZAdjustment(localTypedArray.getInt(8, 0));
        setBackgroundColor(localTypedArray.getInt(0, 0));
        setDetachWallpaper(localTypedArray.getBoolean(10, false));
        int i = localTypedArray.getResourceId(1, 0);
        localTypedArray.recycle();
        if (i > 0)
            setInterpolator(paramContext, i);
        ensureInterpolator();
    }

    private void fireAnimationEnd()
    {
        if (this.mListener != null)
        {
            if (this.mListenerHandler != null)
                break label25;
            this.mListener.onAnimationEnd(this);
        }
        while (true)
        {
            return;
            label25: this.mListenerHandler.postAtFrontOfQueue(this.mOnEnd);
        }
    }

    private void fireAnimationRepeat()
    {
        if (this.mListener != null)
        {
            if (this.mListenerHandler != null)
                break label25;
            this.mListener.onAnimationRepeat(this);
        }
        while (true)
        {
            return;
            label25: this.mListenerHandler.postAtFrontOfQueue(this.mOnRepeat);
        }
    }

    private void fireAnimationStart()
    {
        if (this.mListener != null)
        {
            if (this.mListenerHandler != null)
                break label25;
            this.mListener.onAnimationStart(this);
        }
        while (true)
        {
            return;
            label25: this.mListenerHandler.postAtFrontOfQueue(this.mOnStart);
        }
    }

    protected void applyTransformation(float paramFloat, Transformation paramTransformation)
    {
    }

    public void cancel()
    {
        if ((this.mStarted) && (!this.mEnded))
        {
            fireAnimationEnd();
            this.mEnded = true;
            this.guard.close();
        }
        this.mStartTime = -9223372036854775808L;
        this.mOneMoreTime = false;
        this.mMore = false;
    }

    protected Animation clone()
        throws CloneNotSupportedException
    {
        Animation localAnimation = (Animation)super.clone();
        localAnimation.mPreviousRegion = new RectF();
        localAnimation.mRegion = new RectF();
        localAnimation.mTransformation = new Transformation();
        localAnimation.mPreviousTransformation = new Transformation();
        return localAnimation;
    }

    public long computeDurationHint()
    {
        return (getStartOffset() + getDuration()) * (1 + getRepeatCount());
    }

    public void detach()
    {
        if ((this.mStarted) && (!this.mEnded))
        {
            this.mEnded = true;
            this.guard.close();
            fireAnimationEnd();
        }
    }

    protected void ensureInterpolator()
    {
        if (this.mInterpolator == null)
            this.mInterpolator = new AccelerateDecelerateInterpolator();
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (this.guard != null)
                this.guard.warnIfOpen();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getBackgroundColor()
    {
        return this.mBackgroundColor;
    }

    public boolean getDetachWallpaper()
    {
        return this.mDetachWallpaper;
    }

    public long getDuration()
    {
        return this.mDuration;
    }

    public boolean getFillAfter()
    {
        return this.mFillAfter;
    }

    public boolean getFillBefore()
    {
        return this.mFillBefore;
    }

    public Interpolator getInterpolator()
    {
        return this.mInterpolator;
    }

    public void getInvalidateRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4, RectF paramRectF, Transformation paramTransformation)
    {
        RectF localRectF1 = this.mRegion;
        RectF localRectF2 = this.mPreviousRegion;
        paramRectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
        paramTransformation.getMatrix().mapRect(paramRectF);
        paramRectF.inset(-1.0F, -1.0F);
        localRectF1.set(paramRectF);
        paramRectF.union(localRectF2);
        localRectF2.set(localRectF1);
        Transformation localTransformation1 = this.mTransformation;
        Transformation localTransformation2 = this.mPreviousTransformation;
        localTransformation1.set(paramTransformation);
        paramTransformation.set(localTransformation2);
        localTransformation2.set(localTransformation1);
    }

    public int getRepeatCount()
    {
        return this.mRepeatCount;
    }

    public int getRepeatMode()
    {
        return this.mRepeatMode;
    }

    protected float getScaleFactor()
    {
        return this.mScaleFactor;
    }

    public long getStartOffset()
    {
        return this.mStartOffset;
    }

    public long getStartTime()
    {
        return this.mStartTime;
    }

    public boolean getTransformation(long paramLong, Transformation paramTransformation)
    {
        if (this.mStartTime == -1L)
            this.mStartTime = paramLong;
        long l1 = getStartOffset();
        long l2 = this.mDuration;
        float f;
        int i;
        label61: boolean bool1;
        if (l2 != 0L)
        {
            f = (float)(paramLong - (l1 + this.mStartTime)) / (float)l2;
            if (f < 1.0F)
                break label287;
            i = 1;
            if (i != 0)
                break label293;
            bool1 = true;
            label69: this.mMore = bool1;
            if (!this.mFillEnabled)
                f = Math.max(Math.min(f, 1.0F), 0.0F);
            if (((f >= 0.0F) || (this.mFillBefore)) && ((f <= 1.0F) || (this.mFillAfter)))
            {
                if (!this.mStarted)
                {
                    fireAnimationStart();
                    this.mStarted = true;
                    if (USE_CLOSEGUARD)
                        this.guard.open("cancel or detach or getTransformation");
                }
                if (this.mFillEnabled)
                    f = Math.max(Math.min(f, 1.0F), 0.0F);
                if (this.mCycleFlip)
                    f = 1.0F - f;
                applyTransformation(this.mInterpolator.getInterpolation(f), paramTransformation);
            }
            if (i != 0)
            {
                if (this.mRepeatCount != this.mRepeated)
                    break label299;
                if (!this.mEnded)
                {
                    this.mEnded = true;
                    this.guard.close();
                    fireAnimationEnd();
                }
            }
            if ((this.mMore) || (!this.mOneMoreTime))
                break label365;
            this.mOneMoreTime = false;
        }
        label287: label293: label299: label359: label365: for (boolean bool2 = true; ; bool2 = this.mMore)
        {
            return bool2;
            if (paramLong < this.mStartTime);
            for (f = 0.0F; ; f = 1.0F)
                break;
            i = 0;
            break label61;
            bool1 = false;
            break label69;
            if (this.mRepeatCount > 0)
                this.mRepeated = (1 + this.mRepeated);
            if (this.mRepeatMode == 2)
                if (this.mCycleFlip)
                    break label359;
            for (boolean bool3 = true; ; bool3 = false)
            {
                this.mCycleFlip = bool3;
                this.mStartTime = -1L;
                this.mMore = true;
                fireAnimationRepeat();
                break;
            }
        }
    }

    public boolean getTransformation(long paramLong, Transformation paramTransformation, float paramFloat)
    {
        this.mScaleFactor = paramFloat;
        return getTransformation(paramLong, paramTransformation);
    }

    public int getZAdjustment()
    {
        return this.mZAdjustment;
    }

    public boolean hasAlpha()
    {
        return false;
    }

    public boolean hasEnded()
    {
        return this.mEnded;
    }

    public boolean hasStarted()
    {
        return this.mStarted;
    }

    public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        reset();
        this.mInitialized = true;
    }

    public void initializeInvalidateRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        RectF localRectF = this.mPreviousRegion;
        localRectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
        localRectF.inset(-1.0F, -1.0F);
        if (this.mFillBefore)
        {
            Transformation localTransformation = this.mPreviousTransformation;
            applyTransformation(this.mInterpolator.getInterpolation(0.0F), localTransformation);
        }
    }

    public boolean isFillEnabled()
    {
        return this.mFillEnabled;
    }

    public boolean isInitialized()
    {
        return this.mInitialized;
    }

    public void reset()
    {
        this.mPreviousRegion.setEmpty();
        this.mPreviousTransformation.clear();
        this.mInitialized = false;
        this.mCycleFlip = false;
        this.mRepeated = 0;
        this.mMore = true;
        this.mOneMoreTime = true;
        this.mListenerHandler = null;
    }

    protected float resolveSize(int paramInt1, float paramFloat, int paramInt2, int paramInt3)
    {
        switch (paramInt1)
        {
        case 0:
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            return paramFloat;
            paramFloat *= paramInt2;
            continue;
            paramFloat *= paramInt3;
        }
    }

    public void restrictDuration(long paramLong)
    {
        if (this.mStartOffset > paramLong)
        {
            this.mStartOffset = paramLong;
            this.mDuration = 0L;
            this.mRepeatCount = 0;
        }
        while (true)
        {
            return;
            long l = this.mDuration + this.mStartOffset;
            if (l > paramLong)
            {
                this.mDuration = (paramLong - this.mStartOffset);
                l = paramLong;
            }
            if (this.mDuration <= 0L)
            {
                this.mDuration = 0L;
                this.mRepeatCount = 0;
            }
            else if ((this.mRepeatCount < 0) || (this.mRepeatCount > paramLong) || (l * this.mRepeatCount > paramLong))
            {
                this.mRepeatCount = (-1 + (int)(paramLong / l));
                if (this.mRepeatCount < 0)
                    this.mRepeatCount = 0;
            }
        }
    }

    public void scaleCurrentDuration(float paramFloat)
    {
        this.mDuration = (()(paramFloat * (float)this.mDuration));
        this.mStartOffset = (()(paramFloat * (float)this.mStartOffset));
    }

    public void setAnimationListener(AnimationListener paramAnimationListener)
    {
        this.mListener = paramAnimationListener;
    }

    public void setBackgroundColor(int paramInt)
    {
        this.mBackgroundColor = paramInt;
    }

    public void setDetachWallpaper(boolean paramBoolean)
    {
        this.mDetachWallpaper = paramBoolean;
    }

    public void setDuration(long paramLong)
    {
        if (paramLong < 0L)
            throw new IllegalArgumentException("Animation duration cannot be negative");
        this.mDuration = paramLong;
    }

    public void setFillAfter(boolean paramBoolean)
    {
        this.mFillAfter = paramBoolean;
    }

    public void setFillBefore(boolean paramBoolean)
    {
        this.mFillBefore = paramBoolean;
    }

    public void setFillEnabled(boolean paramBoolean)
    {
        this.mFillEnabled = paramBoolean;
    }

    public void setInterpolator(Context paramContext, int paramInt)
    {
        setInterpolator(AnimationUtils.loadInterpolator(paramContext, paramInt));
    }

    public void setInterpolator(Interpolator paramInterpolator)
    {
        this.mInterpolator = paramInterpolator;
    }

    public void setListenerHandler(Handler paramHandler)
    {
        if (this.mListenerHandler == null)
        {
            this.mOnStart = new Runnable()
            {
                public void run()
                {
                    if (Animation.this.mListener != null)
                        Animation.this.mListener.onAnimationStart(Animation.this);
                }
            };
            this.mOnRepeat = new Runnable()
            {
                public void run()
                {
                    if (Animation.this.mListener != null)
                        Animation.this.mListener.onAnimationRepeat(Animation.this);
                }
            };
            this.mOnEnd = new Runnable()
            {
                public void run()
                {
                    if (Animation.this.mListener != null)
                        Animation.this.mListener.onAnimationEnd(Animation.this);
                }
            };
        }
        this.mListenerHandler = paramHandler;
    }

    public void setRepeatCount(int paramInt)
    {
        if (paramInt < 0)
            paramInt = -1;
        this.mRepeatCount = paramInt;
    }

    public void setRepeatMode(int paramInt)
    {
        this.mRepeatMode = paramInt;
    }

    public void setStartOffset(long paramLong)
    {
        this.mStartOffset = paramLong;
    }

    public void setStartTime(long paramLong)
    {
        this.mStartTime = paramLong;
        this.mEnded = false;
        this.mStarted = false;
        this.mCycleFlip = false;
        this.mRepeated = 0;
        this.mMore = true;
    }

    public void setZAdjustment(int paramInt)
    {
        this.mZAdjustment = paramInt;
    }

    public void start()
    {
        setStartTime(-1L);
    }

    public void startNow()
    {
        setStartTime(AnimationUtils.currentAnimationTimeMillis());
    }

    public boolean willChangeBounds()
    {
        return true;
    }

    public boolean willChangeTransformationMatrix()
    {
        return true;
    }

    public static abstract interface AnimationListener
    {
        public abstract void onAnimationEnd(Animation paramAnimation);

        public abstract void onAnimationRepeat(Animation paramAnimation);

        public abstract void onAnimationStart(Animation paramAnimation);
    }

    protected static class Description
    {
        public int type;
        public float value;

        static Description parseValue(TypedValue paramTypedValue)
        {
            int i = 1;
            Description localDescription = new Description();
            if (paramTypedValue == null)
            {
                localDescription.type = 0;
                localDescription.value = 0.0F;
                localDescription.type = 0;
                localDescription.value = 0.0F;
            }
            while (true)
            {
                return localDescription;
                if (paramTypedValue.type == 6)
                {
                    if ((0xF & paramTypedValue.data) == i)
                        i = 2;
                    localDescription.type = i;
                    localDescription.value = TypedValue.complexToFloat(paramTypedValue.data);
                }
                else if (paramTypedValue.type == 4)
                {
                    localDescription.type = 0;
                    localDescription.value = paramTypedValue.getFloat();
                }
                else
                {
                    if ((paramTypedValue.type < 16) || (paramTypedValue.type > 31))
                        break;
                    localDescription.type = 0;
                    localDescription.value = paramTypedValue.data;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.Animation
 * JD-Core Version:        0.6.2
 */