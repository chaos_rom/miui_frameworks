package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.android.internal.R.styleable;

public class ScaleAnimation extends Animation
{
    private float mFromX;
    private int mFromXData = 0;
    private int mFromXType = 0;
    private float mFromY;
    private int mFromYData = 0;
    private int mFromYType = 0;
    private float mPivotX;
    private int mPivotXType = 0;
    private float mPivotXValue = 0.0F;
    private float mPivotY;
    private int mPivotYType = 0;
    private float mPivotYValue = 0.0F;
    private final Resources mResources;
    private float mToX;
    private int mToXData = 0;
    private int mToXType = 0;
    private float mToY;
    private int mToYData = 0;
    private int mToYType = 0;

    public ScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.mResources = null;
        this.mFromX = paramFloat1;
        this.mToX = paramFloat2;
        this.mFromY = paramFloat3;
        this.mToY = paramFloat4;
        this.mPivotX = 0.0F;
        this.mPivotY = 0.0F;
    }

    public ScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        this.mResources = null;
        this.mFromX = paramFloat1;
        this.mToX = paramFloat2;
        this.mFromY = paramFloat3;
        this.mToY = paramFloat4;
        this.mPivotXType = 0;
        this.mPivotYType = 0;
        this.mPivotXValue = paramFloat5;
        this.mPivotYValue = paramFloat6;
        initializePivotPoint();
    }

    public ScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, float paramFloat5, int paramInt2, float paramFloat6)
    {
        this.mResources = null;
        this.mFromX = paramFloat1;
        this.mToX = paramFloat2;
        this.mFromY = paramFloat3;
        this.mToY = paramFloat4;
        this.mPivotXValue = paramFloat5;
        this.mPivotXType = paramInt1;
        this.mPivotYValue = paramFloat6;
        this.mPivotYType = paramInt2;
        initializePivotPoint();
    }

    public ScaleAnimation(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        this.mResources = paramContext.getResources();
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ScaleAnimation);
        TypedValue localTypedValue1 = localTypedArray.peekValue(2);
        this.mFromX = 0.0F;
        TypedValue localTypedValue2;
        label153: TypedValue localTypedValue3;
        label188: TypedValue localTypedValue4;
        if (localTypedValue1 != null)
        {
            if (localTypedValue1.type == 4)
                this.mFromX = localTypedValue1.getFloat();
        }
        else
        {
            localTypedValue2 = localTypedArray.peekValue(3);
            this.mToX = 0.0F;
            if (localTypedValue2 != null)
            {
                if (localTypedValue2.type != 4)
                    break label309;
                this.mToX = localTypedValue2.getFloat();
            }
            localTypedValue3 = localTypedArray.peekValue(4);
            this.mFromY = 0.0F;
            if (localTypedValue3 != null)
            {
                if (localTypedValue3.type != 4)
                    break label330;
                this.mFromY = localTypedValue3.getFloat();
            }
            localTypedValue4 = localTypedArray.peekValue(5);
            this.mToY = 0.0F;
            if (localTypedValue4 != null)
            {
                if (localTypedValue4.type != 4)
                    break label351;
                this.mToY = localTypedValue4.getFloat();
            }
        }
        while (true)
        {
            Animation.Description localDescription1 = Animation.Description.parseValue(localTypedArray.peekValue(0));
            this.mPivotXType = localDescription1.type;
            this.mPivotXValue = localDescription1.value;
            Animation.Description localDescription2 = Animation.Description.parseValue(localTypedArray.peekValue(1));
            this.mPivotYType = localDescription2.type;
            this.mPivotYValue = localDescription2.value;
            localTypedArray.recycle();
            initializePivotPoint();
            return;
            this.mFromXType = localTypedValue1.type;
            this.mFromXData = localTypedValue1.data;
            break;
            label309: this.mToXType = localTypedValue2.type;
            this.mToXData = localTypedValue2.data;
            break label153;
            label330: this.mFromYType = localTypedValue3.type;
            this.mFromYData = localTypedValue3.data;
            break label188;
            label351: this.mToYType = localTypedValue4.type;
            this.mToYData = localTypedValue4.data;
        }
    }

    private void initializePivotPoint()
    {
        if (this.mPivotXType == 0)
            this.mPivotX = this.mPivotXValue;
        if (this.mPivotYType == 0)
            this.mPivotY = this.mPivotYValue;
    }

    protected void applyTransformation(float paramFloat, Transformation paramTransformation)
    {
        float f1 = 1.0F;
        float f2 = 1.0F;
        float f3 = getScaleFactor();
        if ((this.mFromX != 1.0F) || (this.mToX != 1.0F))
            f1 = this.mFromX + paramFloat * (this.mToX - this.mFromX);
        if ((this.mFromY != 1.0F) || (this.mToY != 1.0F))
            f2 = this.mFromY + paramFloat * (this.mToY - this.mFromY);
        if ((this.mPivotX == 0.0F) && (this.mPivotY == 0.0F))
            paramTransformation.getMatrix().setScale(f1, f2);
        while (true)
        {
            return;
            paramTransformation.getMatrix().setScale(f1, f2, f3 * this.mPivotX, f3 * this.mPivotY);
        }
    }

    public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mFromX = resolveScale(this.mFromX, this.mFromXType, this.mFromXData, paramInt1, paramInt3);
        this.mToX = resolveScale(this.mToX, this.mToXType, this.mToXData, paramInt1, paramInt3);
        this.mFromY = resolveScale(this.mFromY, this.mFromYType, this.mFromYData, paramInt2, paramInt4);
        this.mToY = resolveScale(this.mToY, this.mToYType, this.mToYData, paramInt2, paramInt4);
        this.mPivotX = resolveSize(this.mPivotXType, this.mPivotXValue, paramInt1, paramInt3);
        this.mPivotY = resolveSize(this.mPivotYType, this.mPivotYValue, paramInt2, paramInt4);
    }

    float resolveScale(float paramFloat, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        float f;
        if (paramInt1 == 6)
        {
            f = TypedValue.complexToFraction(paramInt2, paramInt3, paramInt4);
            if (paramInt3 != 0)
                break label48;
            paramFloat = 1.0F;
        }
        while (true)
        {
            return paramFloat;
            if (paramInt1 == 5)
            {
                f = TypedValue.complexToDimension(paramInt2, this.mResources.getDisplayMetrics());
                break;
                label48: paramFloat = f / paramInt3;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.ScaleAnimation
 * JD-Core Version:        0.6.2
 */