package android.view.animation;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Xml;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimationUtils
{
    private static final int SEQUENTIALLY = 1;
    private static final int TOGETHER;

    private static Animation createAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        return createAnimationFromXml(paramContext, paramXmlPullParser, null, Xml.asAttributeSet(paramXmlPullParser));
    }

    private static Animation createAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser, AnimationSet paramAnimationSet, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        Object localObject = null;
        int i = paramXmlPullParser.getDepth();
        label196: 
        while (true)
        {
            int j = paramXmlPullParser.next();
            if (((j == 3) && (paramXmlPullParser.getDepth() <= i)) || (j == 1))
                break label230;
            if (j == 2)
            {
                String str = paramXmlPullParser.getName();
                if (str.equals("set"))
                {
                    localObject = new AnimationSet(paramContext, paramAttributeSet);
                    createAnimationFromXml(paramContext, paramXmlPullParser, (AnimationSet)localObject, paramAttributeSet);
                }
                while (true)
                {
                    if (paramAnimationSet == null)
                        break label196;
                    paramAnimationSet.addAnimation((Animation)localObject);
                    break;
                    if (str.equals("alpha"))
                    {
                        localObject = new AlphaAnimation(paramContext, paramAttributeSet);
                    }
                    else if (str.equals("scale"))
                    {
                        localObject = new ScaleAnimation(paramContext, paramAttributeSet);
                    }
                    else if (str.equals("rotate"))
                    {
                        localObject = new RotateAnimation(paramContext, paramAttributeSet);
                    }
                    else
                    {
                        if (!str.equals("translate"))
                            break label198;
                        localObject = new TranslateAnimation(paramContext, paramAttributeSet);
                    }
                }
            }
        }
        label198: throw new RuntimeException("Unknown animation name: " + paramXmlPullParser.getName());
        label230: return localObject;
    }

    private static Interpolator createInterpolatorFromXml(Context paramContext, XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        Object localObject = null;
        int i = paramXmlPullParser.getDepth();
        while (true)
        {
            int j = paramXmlPullParser.next();
            if (((j == 3) && (paramXmlPullParser.getDepth() <= i)) || (j == 1))
                break label307;
            if (j == 2)
            {
                AttributeSet localAttributeSet = Xml.asAttributeSet(paramXmlPullParser);
                String str = paramXmlPullParser.getName();
                if (str.equals("linearInterpolator"))
                {
                    localObject = new LinearInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("accelerateInterpolator"))
                {
                    localObject = new AccelerateInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("decelerateInterpolator"))
                {
                    localObject = new DecelerateInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("accelerateDecelerateInterpolator"))
                {
                    localObject = new AccelerateDecelerateInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("cycleInterpolator"))
                {
                    localObject = new CycleInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("anticipateInterpolator"))
                {
                    localObject = new AnticipateInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("overshootInterpolator"))
                {
                    localObject = new OvershootInterpolator(paramContext, localAttributeSet);
                }
                else if (str.equals("anticipateOvershootInterpolator"))
                {
                    localObject = new AnticipateOvershootInterpolator(paramContext, localAttributeSet);
                }
                else
                {
                    if (!str.equals("bounceInterpolator"))
                        break;
                    localObject = new BounceInterpolator(paramContext, localAttributeSet);
                }
            }
        }
        throw new RuntimeException("Unknown interpolator name: " + paramXmlPullParser.getName());
        label307: return localObject;
    }

    private static LayoutAnimationController createLayoutAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        return createLayoutAnimationFromXml(paramContext, paramXmlPullParser, Xml.asAttributeSet(paramXmlPullParser));
    }

    private static LayoutAnimationController createLayoutAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        Object localObject = null;
        int i = paramXmlPullParser.getDepth();
        String str;
        while (true)
        {
            int j = paramXmlPullParser.next();
            if (((j == 3) && (paramXmlPullParser.getDepth() <= i)) || (j == 1))
                break label129;
            if (j == 2)
            {
                str = paramXmlPullParser.getName();
                if ("layoutAnimation".equals(str))
                {
                    localObject = new LayoutAnimationController(paramContext, paramAttributeSet);
                }
                else
                {
                    if (!"gridLayoutAnimation".equals(str))
                        break;
                    localObject = new GridLayoutAnimationController(paramContext, paramAttributeSet);
                }
            }
        }
        throw new RuntimeException("Unknown layout animation name: " + str);
        label129: return localObject;
    }

    public static long currentAnimationTimeMillis()
    {
        return SystemClock.uptimeMillis();
    }

    // ERROR //
    public static Animation loadAnimation(Context paramContext, int paramInt)
        throws android.content.res.Resources.NotFoundException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: invokevirtual 177	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     6: iload_1
        //     7: invokevirtual 183	android/content/res/Resources:getAnimation	(I)Landroid/content/res/XmlResourceParser;
        //     10: astore_2
        //     11: aload_0
        //     12: aload_2
        //     13: invokestatic 185	android/view/animation/AnimationUtils:createAnimationFromXml	(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Landroid/view/animation/Animation;
        //     16: astore 10
        //     18: aload_2
        //     19: ifnull +9 -> 28
        //     22: aload_2
        //     23: invokeinterface 190 1 0
        //     28: aload 10
        //     30: areturn
        //     31: astore 7
        //     33: new 171	android/content/res/Resources$NotFoundException
        //     36: dup
        //     37: new 81	java/lang/StringBuilder
        //     40: dup
        //     41: invokespecial 82	java/lang/StringBuilder:<init>	()V
        //     44: ldc 192
        //     46: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     49: iload_1
        //     50: invokestatic 198	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     53: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 199	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     62: astore 8
        //     64: aload 8
        //     66: aload 7
        //     68: invokevirtual 203	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     71: pop
        //     72: aload 8
        //     74: athrow
        //     75: astore 6
        //     77: aload_2
        //     78: ifnull +9 -> 87
        //     81: aload_2
        //     82: invokeinterface 190 1 0
        //     87: aload 6
        //     89: athrow
        //     90: astore_3
        //     91: new 171	android/content/res/Resources$NotFoundException
        //     94: dup
        //     95: new 81	java/lang/StringBuilder
        //     98: dup
        //     99: invokespecial 82	java/lang/StringBuilder:<init>	()V
        //     102: ldc 192
        //     104: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: iload_1
        //     108: invokestatic 198	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     111: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     117: invokespecial 199	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     120: astore 4
        //     122: aload 4
        //     124: aload_3
        //     125: invokevirtual 203	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     128: pop
        //     129: aload 4
        //     131: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	18	31	org/xmlpull/v1/XmlPullParserException
        //     2	18	75	finally
        //     33	75	75	finally
        //     91	132	75	finally
        //     2	18	90	java/io/IOException
    }

    // ERROR //
    public static Interpolator loadInterpolator(Context paramContext, int paramInt)
        throws android.content.res.Resources.NotFoundException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: invokevirtual 177	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     6: iload_1
        //     7: invokevirtual 183	android/content/res/Resources:getAnimation	(I)Landroid/content/res/XmlResourceParser;
        //     10: astore_2
        //     11: aload_0
        //     12: aload_2
        //     13: invokestatic 207	android/view/animation/AnimationUtils:createInterpolatorFromXml	(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Landroid/view/animation/Interpolator;
        //     16: astore 10
        //     18: aload_2
        //     19: ifnull +9 -> 28
        //     22: aload_2
        //     23: invokeinterface 190 1 0
        //     28: aload 10
        //     30: areturn
        //     31: astore 7
        //     33: new 171	android/content/res/Resources$NotFoundException
        //     36: dup
        //     37: new 81	java/lang/StringBuilder
        //     40: dup
        //     41: invokespecial 82	java/lang/StringBuilder:<init>	()V
        //     44: ldc 192
        //     46: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     49: iload_1
        //     50: invokestatic 198	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     53: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 199	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     62: astore 8
        //     64: aload 8
        //     66: aload 7
        //     68: invokevirtual 203	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     71: pop
        //     72: aload 8
        //     74: athrow
        //     75: astore 6
        //     77: aload_2
        //     78: ifnull +9 -> 87
        //     81: aload_2
        //     82: invokeinterface 190 1 0
        //     87: aload 6
        //     89: athrow
        //     90: astore_3
        //     91: new 171	android/content/res/Resources$NotFoundException
        //     94: dup
        //     95: new 81	java/lang/StringBuilder
        //     98: dup
        //     99: invokespecial 82	java/lang/StringBuilder:<init>	()V
        //     102: ldc 192
        //     104: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: iload_1
        //     108: invokestatic 198	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     111: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     117: invokespecial 199	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     120: astore 4
        //     122: aload 4
        //     124: aload_3
        //     125: invokevirtual 203	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     128: pop
        //     129: aload 4
        //     131: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	18	31	org/xmlpull/v1/XmlPullParserException
        //     2	18	75	finally
        //     33	75	75	finally
        //     91	132	75	finally
        //     2	18	90	java/io/IOException
    }

    // ERROR //
    public static LayoutAnimationController loadLayoutAnimation(Context paramContext, int paramInt)
        throws android.content.res.Resources.NotFoundException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: invokevirtual 177	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     6: iload_1
        //     7: invokevirtual 183	android/content/res/Resources:getAnimation	(I)Landroid/content/res/XmlResourceParser;
        //     10: astore_2
        //     11: aload_0
        //     12: aload_2
        //     13: invokestatic 211	android/view/animation/AnimationUtils:createLayoutAnimationFromXml	(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Landroid/view/animation/LayoutAnimationController;
        //     16: astore 10
        //     18: aload_2
        //     19: ifnull +9 -> 28
        //     22: aload_2
        //     23: invokeinterface 190 1 0
        //     28: aload 10
        //     30: areturn
        //     31: astore 7
        //     33: new 171	android/content/res/Resources$NotFoundException
        //     36: dup
        //     37: new 81	java/lang/StringBuilder
        //     40: dup
        //     41: invokespecial 82	java/lang/StringBuilder:<init>	()V
        //     44: ldc 192
        //     46: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     49: iload_1
        //     50: invokestatic 198	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     53: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 199	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     62: astore 8
        //     64: aload 8
        //     66: aload 7
        //     68: invokevirtual 203	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     71: pop
        //     72: aload 8
        //     74: athrow
        //     75: astore 6
        //     77: aload_2
        //     78: ifnull +9 -> 87
        //     81: aload_2
        //     82: invokeinterface 190 1 0
        //     87: aload 6
        //     89: athrow
        //     90: astore_3
        //     91: new 171	android/content/res/Resources$NotFoundException
        //     94: dup
        //     95: new 81	java/lang/StringBuilder
        //     98: dup
        //     99: invokespecial 82	java/lang/StringBuilder:<init>	()V
        //     102: ldc 192
        //     104: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: iload_1
        //     108: invokestatic 198	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     111: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     117: invokespecial 199	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
        //     120: astore 4
        //     122: aload 4
        //     124: aload_3
        //     125: invokevirtual 203	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //     128: pop
        //     129: aload 4
        //     131: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	18	31	org/xmlpull/v1/XmlPullParserException
        //     2	18	75	finally
        //     33	75	75	finally
        //     91	132	75	finally
        //     2	18	90	java/io/IOException
    }

    public static Animation makeInAnimation(Context paramContext, boolean paramBoolean)
    {
        if (paramBoolean);
        for (Animation localAnimation = loadAnimation(paramContext, 17432578); ; localAnimation = loadAnimation(paramContext, 17432655))
        {
            localAnimation.setInterpolator(new DecelerateInterpolator());
            localAnimation.setStartTime(currentAnimationTimeMillis());
            return localAnimation;
        }
    }

    public static Animation makeInChildBottomAnimation(Context paramContext)
    {
        Animation localAnimation = loadAnimation(paramContext, 17432654);
        localAnimation.setInterpolator(new AccelerateInterpolator());
        localAnimation.setStartTime(currentAnimationTimeMillis());
        return localAnimation;
    }

    public static Animation makeOutAnimation(Context paramContext, boolean paramBoolean)
    {
        if (paramBoolean);
        for (Animation localAnimation = loadAnimation(paramContext, 17432579); ; localAnimation = loadAnimation(paramContext, 17432658))
        {
            localAnimation.setInterpolator(new AccelerateInterpolator());
            localAnimation.setStartTime(currentAnimationTimeMillis());
            return localAnimation;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AnimationUtils
 * JD-Core Version:        0.6.2
 */