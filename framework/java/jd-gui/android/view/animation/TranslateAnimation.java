package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class TranslateAnimation extends Animation
{
    private float mFromXDelta;
    private int mFromXType = 0;
    private float mFromXValue = 0.0F;
    private float mFromYDelta;
    private int mFromYType = 0;
    private float mFromYValue = 0.0F;
    private float mToXDelta;
    private int mToXType = 0;
    private float mToXValue = 0.0F;
    private float mToYDelta;
    private int mToYType = 0;
    private float mToYValue = 0.0F;

    public TranslateAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.mFromXValue = paramFloat1;
        this.mToXValue = paramFloat2;
        this.mFromYValue = paramFloat3;
        this.mToYValue = paramFloat4;
        this.mFromXType = 0;
        this.mToXType = 0;
        this.mFromYType = 0;
        this.mToYType = 0;
    }

    public TranslateAnimation(int paramInt1, float paramFloat1, int paramInt2, float paramFloat2, int paramInt3, float paramFloat3, int paramInt4, float paramFloat4)
    {
        this.mFromXValue = paramFloat1;
        this.mToXValue = paramFloat2;
        this.mFromYValue = paramFloat3;
        this.mToYValue = paramFloat4;
        this.mFromXType = paramInt1;
        this.mToXType = paramInt2;
        this.mFromYType = paramInt3;
        this.mToYType = paramInt4;
    }

    public TranslateAnimation(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TranslateAnimation);
        Animation.Description localDescription1 = Animation.Description.parseValue(localTypedArray.peekValue(0));
        this.mFromXType = localDescription1.type;
        this.mFromXValue = localDescription1.value;
        Animation.Description localDescription2 = Animation.Description.parseValue(localTypedArray.peekValue(1));
        this.mToXType = localDescription2.type;
        this.mToXValue = localDescription2.value;
        Animation.Description localDescription3 = Animation.Description.parseValue(localTypedArray.peekValue(2));
        this.mFromYType = localDescription3.type;
        this.mFromYValue = localDescription3.value;
        Animation.Description localDescription4 = Animation.Description.parseValue(localTypedArray.peekValue(3));
        this.mToYType = localDescription4.type;
        this.mToYValue = localDescription4.value;
        localTypedArray.recycle();
    }

    protected void applyTransformation(float paramFloat, Transformation paramTransformation)
    {
        float f1 = this.mFromXDelta;
        float f2 = this.mFromYDelta;
        if (this.mFromXDelta != this.mToXDelta)
            f1 = this.mFromXDelta + paramFloat * (this.mToXDelta - this.mFromXDelta);
        if (this.mFromYDelta != this.mToYDelta)
            f2 = this.mFromYDelta + paramFloat * (this.mToYDelta - this.mFromYDelta);
        paramTransformation.getMatrix().setTranslate(f1, f2);
    }

    public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mFromXDelta = resolveSize(this.mFromXType, this.mFromXValue, paramInt1, paramInt3);
        this.mToXDelta = resolveSize(this.mToXType, this.mToXValue, paramInt1, paramInt3);
        this.mFromYDelta = resolveSize(this.mFromYType, this.mFromYValue, paramInt2, paramInt4);
        this.mToYDelta = resolveSize(this.mToYType, this.mToYValue, paramInt2, paramInt4);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.TranslateAnimation
 * JD-Core Version:        0.6.2
 */