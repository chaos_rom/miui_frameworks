package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class AnticipateOvershootInterpolator
    implements Interpolator
{
    private final float mTension;

    public AnticipateOvershootInterpolator()
    {
        this.mTension = 3.0F;
    }

    public AnticipateOvershootInterpolator(float paramFloat)
    {
        this.mTension = (1.5F * paramFloat);
    }

    public AnticipateOvershootInterpolator(float paramFloat1, float paramFloat2)
    {
        this.mTension = (paramFloat1 * paramFloat2);
    }

    public AnticipateOvershootInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AnticipateOvershootInterpolator);
        this.mTension = (localTypedArray.getFloat(0, 2.0F) * localTypedArray.getFloat(1, 1.5F));
        localTypedArray.recycle();
    }

    private static float a(float paramFloat1, float paramFloat2)
    {
        return paramFloat1 * paramFloat1 * (paramFloat1 * (1.0F + paramFloat2) - paramFloat2);
    }

    private static float o(float paramFloat1, float paramFloat2)
    {
        return paramFloat1 * paramFloat1 * (paramFloat2 + paramFloat1 * (1.0F + paramFloat2));
    }

    public float getInterpolation(float paramFloat)
    {
        if (paramFloat < 0.5F);
        for (float f = 0.5F * a(paramFloat * 2.0F, this.mTension); ; f = 0.5F * (2.0F + o(paramFloat * 2.0F - 2.0F, this.mTension)))
            return f;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AnticipateOvershootInterpolator
 * JD-Core Version:        0.6.2
 */