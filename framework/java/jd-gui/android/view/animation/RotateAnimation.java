package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class RotateAnimation extends Animation
{
    private float mFromDegrees;
    private float mPivotX;
    private int mPivotXType = 0;
    private float mPivotXValue = 0.0F;
    private float mPivotY;
    private int mPivotYType = 0;
    private float mPivotYValue = 0.0F;
    private float mToDegrees;

    public RotateAnimation(float paramFloat1, float paramFloat2)
    {
        this.mFromDegrees = paramFloat1;
        this.mToDegrees = paramFloat2;
        this.mPivotX = 0.0F;
        this.mPivotY = 0.0F;
    }

    public RotateAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.mFromDegrees = paramFloat1;
        this.mToDegrees = paramFloat2;
        this.mPivotXType = 0;
        this.mPivotYType = 0;
        this.mPivotXValue = paramFloat3;
        this.mPivotYValue = paramFloat4;
        initializePivotPoint();
    }

    public RotateAnimation(float paramFloat1, float paramFloat2, int paramInt1, float paramFloat3, int paramInt2, float paramFloat4)
    {
        this.mFromDegrees = paramFloat1;
        this.mToDegrees = paramFloat2;
        this.mPivotXValue = paramFloat3;
        this.mPivotXType = paramInt1;
        this.mPivotYValue = paramFloat4;
        this.mPivotYType = paramInt2;
        initializePivotPoint();
    }

    public RotateAnimation(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RotateAnimation);
        this.mFromDegrees = localTypedArray.getFloat(0, 0.0F);
        this.mToDegrees = localTypedArray.getFloat(1, 0.0F);
        Animation.Description localDescription1 = Animation.Description.parseValue(localTypedArray.peekValue(2));
        this.mPivotXType = localDescription1.type;
        this.mPivotXValue = localDescription1.value;
        Animation.Description localDescription2 = Animation.Description.parseValue(localTypedArray.peekValue(3));
        this.mPivotYType = localDescription2.type;
        this.mPivotYValue = localDescription2.value;
        localTypedArray.recycle();
        initializePivotPoint();
    }

    private void initializePivotPoint()
    {
        if (this.mPivotXType == 0)
            this.mPivotX = this.mPivotXValue;
        if (this.mPivotYType == 0)
            this.mPivotY = this.mPivotYValue;
    }

    protected void applyTransformation(float paramFloat, Transformation paramTransformation)
    {
        float f1 = this.mFromDegrees + paramFloat * (this.mToDegrees - this.mFromDegrees);
        float f2 = getScaleFactor();
        if ((this.mPivotX == 0.0F) && (this.mPivotY == 0.0F))
            paramTransformation.getMatrix().setRotate(f1);
        while (true)
        {
            return;
            paramTransformation.getMatrix().setRotate(f1, f2 * this.mPivotX, f2 * this.mPivotY);
        }
    }

    public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mPivotX = resolveSize(this.mPivotXType, this.mPivotXValue, paramInt1, paramInt3);
        this.mPivotY = resolveSize(this.mPivotYType, this.mPivotYValue, paramInt2, paramInt4);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.RotateAnimation
 * JD-Core Version:        0.6.2
 */