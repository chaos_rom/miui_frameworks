package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.android.internal.R.styleable;
import java.util.Random;

public class GridLayoutAnimationController extends LayoutAnimationController
{
    public static final int DIRECTION_BOTTOM_TO_TOP = 2;
    public static final int DIRECTION_HORIZONTAL_MASK = 1;
    public static final int DIRECTION_LEFT_TO_RIGHT = 0;
    public static final int DIRECTION_RIGHT_TO_LEFT = 1;
    public static final int DIRECTION_TOP_TO_BOTTOM = 0;
    public static final int DIRECTION_VERTICAL_MASK = 2;
    public static final int PRIORITY_COLUMN = 1;
    public static final int PRIORITY_NONE = 0;
    public static final int PRIORITY_ROW = 2;
    private float mColumnDelay;
    private int mDirection;
    private int mDirectionPriority;
    private float mRowDelay;

    public GridLayoutAnimationController(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.GridLayoutAnimation);
        this.mColumnDelay = Animation.Description.parseValue(localTypedArray.peekValue(0)).value;
        this.mRowDelay = Animation.Description.parseValue(localTypedArray.peekValue(1)).value;
        this.mDirection = localTypedArray.getInt(2, 0);
        this.mDirectionPriority = localTypedArray.getInt(3, 0);
        localTypedArray.recycle();
    }

    public GridLayoutAnimationController(Animation paramAnimation)
    {
        this(paramAnimation, 0.5F, 0.5F);
    }

    public GridLayoutAnimationController(Animation paramAnimation, float paramFloat1, float paramFloat2)
    {
        super(paramAnimation);
        this.mColumnDelay = paramFloat1;
        this.mRowDelay = paramFloat2;
    }

    private int getTransformedColumnIndex(AnimationParameters paramAnimationParameters)
    {
        int i;
        switch (getOrder())
        {
        default:
            i = paramAnimationParameters.column;
        case 1:
        case 2:
        }
        while (true)
        {
            if ((0x1 & this.mDirection) == 1)
                i = -1 + paramAnimationParameters.columnsCount - i;
            return i;
            i = -1 + paramAnimationParameters.columnsCount - paramAnimationParameters.column;
            continue;
            if (this.mRandomizer == null)
                this.mRandomizer = new Random();
            i = (int)(paramAnimationParameters.columnsCount * this.mRandomizer.nextFloat());
        }
    }

    private int getTransformedRowIndex(AnimationParameters paramAnimationParameters)
    {
        int i;
        switch (getOrder())
        {
        default:
            i = paramAnimationParameters.row;
        case 1:
        case 2:
        }
        while (true)
        {
            if ((0x2 & this.mDirection) == 2)
                i = -1 + paramAnimationParameters.rowsCount - i;
            return i;
            i = -1 + paramAnimationParameters.rowsCount - paramAnimationParameters.row;
            continue;
            if (this.mRandomizer == null)
                this.mRandomizer = new Random();
            i = (int)(paramAnimationParameters.rowsCount * this.mRandomizer.nextFloat());
        }
    }

    public float getColumnDelay()
    {
        return this.mColumnDelay;
    }

    protected long getDelayForView(View paramView)
    {
        AnimationParameters localAnimationParameters = (AnimationParameters)paramView.getLayoutParams().layoutAnimationParameters;
        long l3;
        if (localAnimationParameters == null)
        {
            l3 = 0L;
            return l3;
        }
        int i = getTransformedColumnIndex(localAnimationParameters);
        int j = getTransformedRowIndex(localAnimationParameters);
        int k = localAnimationParameters.rowsCount;
        int m = localAnimationParameters.columnsCount;
        long l1 = this.mAnimation.getDuration();
        float f1 = this.mColumnDelay * (float)l1;
        float f2 = this.mRowDelay * (float)l1;
        if (this.mInterpolator == null)
            this.mInterpolator = new LinearInterpolator();
        long l2;
        float f3;
        switch (this.mDirectionPriority)
        {
        default:
            l2 = ()(f1 * i + f2 * j);
            f3 = f1 * m + f2 * k;
        case 1:
        case 2:
        }
        while (true)
        {
            float f4 = (float)l2 / f3;
            l3 = ()(f3 * this.mInterpolator.getInterpolation(f4));
            break;
            l2 = ()(f2 * j + f2 * (i * k));
            f3 = f2 * k + f2 * (m * k);
            continue;
            l2 = ()(f1 * i + f1 * (j * m));
            f3 = f1 * m + f1 * (k * m);
        }
    }

    public int getDirection()
    {
        return this.mDirection;
    }

    public int getDirectionPriority()
    {
        return this.mDirectionPriority;
    }

    public float getRowDelay()
    {
        return this.mRowDelay;
    }

    public void setColumnDelay(float paramFloat)
    {
        this.mColumnDelay = paramFloat;
    }

    public void setDirection(int paramInt)
    {
        this.mDirection = paramInt;
    }

    public void setDirectionPriority(int paramInt)
    {
        this.mDirectionPriority = paramInt;
    }

    public void setRowDelay(float paramFloat)
    {
        this.mRowDelay = paramFloat;
    }

    public boolean willOverlap()
    {
        if ((this.mColumnDelay < 1.0F) || (this.mRowDelay < 1.0F));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static class AnimationParameters extends LayoutAnimationController.AnimationParameters
    {
        public int column;
        public int columnsCount;
        public int row;
        public int rowsCount;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.GridLayoutAnimationController
 * JD-Core Version:        0.6.2
 */