package android.view.animation;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.util.ArrayList;
import java.util.List;

public class AnimationSet extends Animation
{
    private static final int PROPERTY_CHANGE_BOUNDS_MASK = 128;
    private static final int PROPERTY_DURATION_MASK = 32;
    private static final int PROPERTY_FILL_AFTER_MASK = 1;
    private static final int PROPERTY_FILL_BEFORE_MASK = 2;
    private static final int PROPERTY_MORPH_MATRIX_MASK = 64;
    private static final int PROPERTY_REPEAT_MODE_MASK = 4;
    private static final int PROPERTY_SHARE_INTERPOLATOR_MASK = 16;
    private static final int PROPERTY_START_OFFSET_MASK = 8;
    private ArrayList<Animation> mAnimations = new ArrayList();
    private boolean mDirty;
    private int mFlags = 0;
    private boolean mHasAlpha;
    private long mLastEnd;
    private long[] mStoredOffsets;
    private Transformation mTempTransformation = new Transformation();

    public AnimationSet(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AnimationSet);
        setFlag(16, localTypedArray.getBoolean(1, true));
        init();
        if (paramContext.getApplicationInfo().targetSdkVersion >= 14)
        {
            if (localTypedArray.hasValue(0))
                this.mFlags = (0x20 | this.mFlags);
            if (localTypedArray.hasValue(2))
                this.mFlags = (0x2 | this.mFlags);
            if (localTypedArray.hasValue(3))
                this.mFlags = (0x1 | this.mFlags);
            if (localTypedArray.hasValue(5))
                this.mFlags = (0x4 | this.mFlags);
            if (localTypedArray.hasValue(4))
                this.mFlags = (0x8 | this.mFlags);
        }
        localTypedArray.recycle();
    }

    public AnimationSet(boolean paramBoolean)
    {
        setFlag(16, paramBoolean);
        init();
    }

    private void init()
    {
        this.mStartTime = 0L;
    }

    private void setFlag(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean);
        for (this.mFlags = (paramInt | this.mFlags); ; this.mFlags &= (paramInt ^ 0xFFFFFFFF))
            return;
    }

    public void addAnimation(Animation paramAnimation)
    {
        int i = 0;
        this.mAnimations.add(paramAnimation);
        int j;
        if ((0x40 & this.mFlags) == 0)
        {
            j = 1;
            if ((j != 0) && (paramAnimation.willChangeTransformationMatrix()))
                this.mFlags = (0x40 | this.mFlags);
            if ((0x80 & this.mFlags) == 0)
                i = 1;
            if ((i != 0) && (paramAnimation.willChangeBounds()))
                this.mFlags = (0x80 | this.mFlags);
            if ((0x20 & this.mFlags) != 32)
                break label120;
            this.mLastEnd = (this.mStartOffset + this.mDuration);
        }
        while (true)
        {
            this.mDirty = true;
            return;
            j = 0;
            break;
            label120: if (this.mAnimations.size() == 1)
            {
                this.mDuration = (paramAnimation.getStartOffset() + paramAnimation.getDuration());
                this.mLastEnd = (this.mStartOffset + this.mDuration);
            }
            else
            {
                this.mLastEnd = Math.max(this.mLastEnd, paramAnimation.getStartOffset() + paramAnimation.getDuration());
                this.mDuration = (this.mLastEnd - this.mStartOffset);
            }
        }
    }

    protected AnimationSet clone()
        throws CloneNotSupportedException
    {
        AnimationSet localAnimationSet = (AnimationSet)super.clone();
        localAnimationSet.mTempTransformation = new Transformation();
        localAnimationSet.mAnimations = new ArrayList();
        int i = this.mAnimations.size();
        ArrayList localArrayList = this.mAnimations;
        for (int j = 0; j < i; j++)
            localAnimationSet.mAnimations.add(((Animation)localArrayList.get(j)).clone());
        return localAnimationSet;
    }

    public long computeDurationHint()
    {
        long l1 = 0L;
        int i = this.mAnimations.size();
        ArrayList localArrayList = this.mAnimations;
        for (int j = i - 1; j >= 0; j--)
        {
            long l2 = ((Animation)localArrayList.get(j)).computeDurationHint();
            if (l2 > l1)
                l1 = l2;
        }
        return l1;
    }

    public List<Animation> getAnimations()
    {
        return this.mAnimations;
    }

    public long getDuration()
    {
        ArrayList localArrayList = this.mAnimations;
        int i = localArrayList.size();
        long l = 0L;
        int j;
        if ((0x20 & this.mFlags) == 32)
        {
            j = 1;
            if (j == 0)
                break label45;
            l = this.mDuration;
        }
        while (true)
        {
            return l;
            j = 0;
            break;
            label45: for (int k = 0; k < i; k++)
                l = Math.max(l, ((Animation)localArrayList.get(k)).getDuration());
        }
    }

    public long getStartTime()
    {
        long l = 9223372036854775807L;
        int i = this.mAnimations.size();
        ArrayList localArrayList = this.mAnimations;
        for (int j = 0; j < i; j++)
            l = Math.min(l, ((Animation)localArrayList.get(j)).getStartTime());
        return l;
    }

    public boolean getTransformation(long paramLong, Transformation paramTransformation)
    {
        int i = this.mAnimations.size();
        ArrayList localArrayList = this.mAnimations;
        Transformation localTransformation = this.mTempTransformation;
        boolean bool1 = false;
        int j = 0;
        boolean bool2 = true;
        paramTransformation.clear();
        int k = i - 1;
        if (k >= 0)
        {
            Animation localAnimation = (Animation)localArrayList.get(k);
            localTransformation.clear();
            if ((localAnimation.getTransformation(paramLong, localTransformation, getScaleFactor())) || (bool1))
            {
                bool1 = true;
                label85: paramTransformation.compose(localTransformation);
                if ((j == 0) && (!localAnimation.hasStarted()))
                    break label135;
                j = 1;
                label107: if ((!localAnimation.hasEnded()) || (!bool2))
                    break label141;
            }
            label135: label141: for (bool2 = true; ; bool2 = false)
            {
                k--;
                break;
                bool1 = false;
                break label85;
                j = 0;
                break label107;
            }
        }
        if ((j != 0) && (!this.mStarted))
        {
            if (this.mListener != null)
                this.mListener.onAnimationStart(this);
            this.mStarted = true;
        }
        if (bool2 != this.mEnded)
        {
            if (this.mListener != null)
                this.mListener.onAnimationEnd(this);
            this.mEnded = bool2;
        }
        return bool1;
    }

    public boolean hasAlpha()
    {
        int i;
        ArrayList localArrayList;
        if (this.mDirty)
        {
            this.mHasAlpha = false;
            this.mDirty = false;
            i = this.mAnimations.size();
            localArrayList = this.mAnimations;
        }
        for (int j = 0; ; j++)
            if (j < i)
            {
                if (((Animation)localArrayList.get(j)).hasAlpha())
                    this.mHasAlpha = true;
            }
            else
                return this.mHasAlpha;
    }

    public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
        int i;
        int j;
        label37: int k;
        label50: int m;
        label63: int n;
        label78: int i1;
        label93: ArrayList localArrayList;
        int i2;
        long l1;
        boolean bool1;
        boolean bool2;
        int i3;
        Interpolator localInterpolator;
        long l2;
        long[] arrayOfLong;
        if ((0x20 & this.mFlags) == 32)
        {
            i = 1;
            if ((0x1 & this.mFlags) != 1)
                break label320;
            j = 1;
            if ((0x2 & this.mFlags) != 2)
                break label326;
            k = 1;
            if ((0x4 & this.mFlags) != 4)
                break label332;
            m = 1;
            if ((0x10 & this.mFlags) != 16)
                break label338;
            n = 1;
            if ((0x8 & this.mFlags) != 8)
                break label344;
            i1 = 1;
            if (n != 0)
                ensureInterpolator();
            localArrayList = this.mAnimations;
            i2 = localArrayList.size();
            l1 = this.mDuration;
            bool1 = this.mFillAfter;
            bool2 = this.mFillBefore;
            i3 = this.mRepeatMode;
            localInterpolator = this.mInterpolator;
            l2 = this.mStartOffset;
            arrayOfLong = this.mStoredOffsets;
            if (i1 == 0)
                break label350;
            if ((arrayOfLong == null) || (arrayOfLong.length != i2))
            {
                arrayOfLong = new long[i2];
                this.mStoredOffsets = arrayOfLong;
            }
        }
        while (true)
        {
            for (int i4 = 0; i4 < i2; i4++)
            {
                Animation localAnimation = (Animation)localArrayList.get(i4);
                if (i != 0)
                    localAnimation.setDuration(l1);
                if (j != 0)
                    localAnimation.setFillAfter(bool1);
                if (k != 0)
                    localAnimation.setFillBefore(bool2);
                if (m != 0)
                    localAnimation.setRepeatMode(i3);
                if (n != 0)
                    localAnimation.setInterpolator(localInterpolator);
                if (i1 != 0)
                {
                    long l3 = localAnimation.getStartOffset();
                    localAnimation.setStartOffset(l3 + l2);
                    arrayOfLong[i4] = l3;
                }
                localAnimation.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
            }
            i = 0;
            break;
            label320: j = 0;
            break label37;
            label326: k = 0;
            break label50;
            label332: m = 0;
            break label63;
            label338: n = 0;
            break label78;
            label344: i1 = 0;
            break label93;
            label350: if (arrayOfLong != null)
            {
                arrayOfLong = null;
                this.mStoredOffsets = null;
            }
        }
    }

    public void initializeInvalidateRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        RectF localRectF = this.mPreviousRegion;
        localRectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
        localRectF.inset(-1.0F, -1.0F);
        if (this.mFillBefore)
        {
            int i = this.mAnimations.size();
            ArrayList localArrayList = this.mAnimations;
            Transformation localTransformation1 = this.mTempTransformation;
            Transformation localTransformation2 = this.mPreviousTransformation;
            int j = i - 1;
            if (j >= 0)
            {
                Animation localAnimation = (Animation)localArrayList.get(j);
                Interpolator localInterpolator;
                if ((!localAnimation.isFillEnabled()) || (localAnimation.getFillBefore()) || (localAnimation.getStartOffset() == 0L))
                {
                    localTransformation1.clear();
                    localInterpolator = localAnimation.mInterpolator;
                    if (localInterpolator == null)
                        break label163;
                }
                label163: for (float f = localInterpolator.getInterpolation(0.0F); ; f = 0.0F)
                {
                    localAnimation.applyTransformation(f, localTransformation1);
                    localTransformation2.compose(localTransformation1);
                    j--;
                    break;
                }
            }
        }
    }

    public void reset()
    {
        super.reset();
        restoreChildrenStartOffset();
    }

    void restoreChildrenStartOffset()
    {
        long[] arrayOfLong = this.mStoredOffsets;
        if (arrayOfLong == null);
        while (true)
        {
            return;
            ArrayList localArrayList = this.mAnimations;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((Animation)localArrayList.get(j)).setStartOffset(arrayOfLong[j]);
        }
    }

    public void restrictDuration(long paramLong)
    {
        super.restrictDuration(paramLong);
        ArrayList localArrayList = this.mAnimations;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            ((Animation)localArrayList.get(j)).restrictDuration(paramLong);
    }

    public void scaleCurrentDuration(float paramFloat)
    {
        ArrayList localArrayList = this.mAnimations;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            ((Animation)localArrayList.get(j)).scaleCurrentDuration(paramFloat);
    }

    public void setDuration(long paramLong)
    {
        this.mFlags = (0x20 | this.mFlags);
        super.setDuration(paramLong);
        this.mLastEnd = (this.mStartOffset + this.mDuration);
    }

    public void setFillAfter(boolean paramBoolean)
    {
        this.mFlags = (0x1 | this.mFlags);
        super.setFillAfter(paramBoolean);
    }

    public void setFillBefore(boolean paramBoolean)
    {
        this.mFlags = (0x2 | this.mFlags);
        super.setFillBefore(paramBoolean);
    }

    public void setRepeatMode(int paramInt)
    {
        this.mFlags = (0x4 | this.mFlags);
        super.setRepeatMode(paramInt);
    }

    public void setStartOffset(long paramLong)
    {
        this.mFlags = (0x8 | this.mFlags);
        super.setStartOffset(paramLong);
    }

    public void setStartTime(long paramLong)
    {
        super.setStartTime(paramLong);
        int i = this.mAnimations.size();
        ArrayList localArrayList = this.mAnimations;
        for (int j = 0; j < i; j++)
            ((Animation)localArrayList.get(j)).setStartTime(paramLong);
    }

    public boolean willChangeBounds()
    {
        if ((0x80 & this.mFlags) == 128);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean willChangeTransformationMatrix()
    {
        if ((0x40 & this.mFlags) == 64);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AnimationSet
 * JD-Core Version:        0.6.2
 */