package android.view.animation;

import android.content.Context;
import android.util.AttributeSet;

public class BounceInterpolator
    implements Interpolator
{
    public BounceInterpolator()
    {
    }

    public BounceInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
    }

    private static float bounce(float paramFloat)
    {
        return 8.0F * (paramFloat * paramFloat);
    }

    public float getInterpolation(float paramFloat)
    {
        float f1 = paramFloat * 1.1226F;
        float f2;
        if (f1 < 0.3535F)
            f2 = bounce(f1);
        while (true)
        {
            return f2;
            if (f1 < 0.7408F)
                f2 = 0.7F + bounce(f1 - 0.54719F);
            else if (f1 < 0.9644F)
                f2 = 0.9F + bounce(f1 - 0.8526F);
            else
                f2 = 0.95F + bounce(f1 - 1.0435F);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.BounceInterpolator
 * JD-Core Version:        0.6.2
 */