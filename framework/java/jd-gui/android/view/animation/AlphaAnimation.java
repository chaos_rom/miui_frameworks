package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class AlphaAnimation extends Animation
{
    private float mFromAlpha;
    private float mToAlpha;

    public AlphaAnimation(float paramFloat1, float paramFloat2)
    {
        this.mFromAlpha = paramFloat1;
        this.mToAlpha = paramFloat2;
    }

    public AlphaAnimation(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AlphaAnimation);
        this.mFromAlpha = localTypedArray.getFloat(0, 1.0F);
        this.mToAlpha = localTypedArray.getFloat(1, 1.0F);
        localTypedArray.recycle();
    }

    protected void applyTransformation(float paramFloat, Transformation paramTransformation)
    {
        float f = this.mFromAlpha;
        paramTransformation.setAlpha(f + paramFloat * (this.mToAlpha - f));
    }

    public boolean hasAlpha()
    {
        return true;
    }

    public boolean willChangeBounds()
    {
        return false;
    }

    public boolean willChangeTransformationMatrix()
    {
        return false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AlphaAnimation
 * JD-Core Version:        0.6.2
 */