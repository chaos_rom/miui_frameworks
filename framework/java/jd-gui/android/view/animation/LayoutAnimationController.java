package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.android.internal.R.styleable;
import java.util.Random;

public class LayoutAnimationController
{
    public static final int ORDER_NORMAL = 0;
    public static final int ORDER_RANDOM = 2;
    public static final int ORDER_REVERSE = 1;
    protected Animation mAnimation;
    private float mDelay;
    private long mDuration;
    protected Interpolator mInterpolator;
    private long mMaxDelay;
    private int mOrder;
    protected Random mRandomizer;

    public LayoutAnimationController(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LayoutAnimation);
        this.mDelay = Animation.Description.parseValue(localTypedArray.peekValue(1)).value;
        this.mOrder = localTypedArray.getInt(3, 0);
        int i = localTypedArray.getResourceId(2, 0);
        if (i > 0)
            setAnimation(paramContext, i);
        int j = localTypedArray.getResourceId(0, 0);
        if (j > 0)
            setInterpolator(paramContext, j);
        localTypedArray.recycle();
    }

    public LayoutAnimationController(Animation paramAnimation)
    {
        this(paramAnimation, 0.5F);
    }

    public LayoutAnimationController(Animation paramAnimation, float paramFloat)
    {
        this.mDelay = paramFloat;
        setAnimation(paramAnimation);
    }

    public Animation getAnimation()
    {
        return this.mAnimation;
    }

    public final Animation getAnimationForView(View paramView)
    {
        long l = getDelayForView(paramView) + this.mAnimation.getStartOffset();
        this.mMaxDelay = Math.max(this.mMaxDelay, l);
        try
        {
            localAnimation = this.mAnimation.clone();
            localAnimation.setStartOffset(l);
            return localAnimation;
        }
        catch (CloneNotSupportedException localCloneNotSupportedException)
        {
            while (true)
                Animation localAnimation = null;
        }
    }

    public float getDelay()
    {
        return this.mDelay;
    }

    protected long getDelayForView(View paramView)
    {
        AnimationParameters localAnimationParameters = paramView.getLayoutParams().layoutAnimationParameters;
        if (localAnimationParameters == null);
        float f2;
        float f3;
        for (long l2 = 0L; ; l2 = ()(f2 * this.mInterpolator.getInterpolation(f3)))
        {
            return l2;
            float f1 = this.mDelay * (float)this.mAnimation.getDuration();
            long l1 = ()(f1 * getTransformedIndex(localAnimationParameters));
            f2 = f1 * localAnimationParameters.count;
            if (this.mInterpolator == null)
                this.mInterpolator = new LinearInterpolator();
            f3 = (float)l1 / f2;
        }
    }

    public Interpolator getInterpolator()
    {
        return this.mInterpolator;
    }

    public int getOrder()
    {
        return this.mOrder;
    }

    protected int getTransformedIndex(AnimationParameters paramAnimationParameters)
    {
        int i;
        switch (getOrder())
        {
        default:
            i = paramAnimationParameters.index;
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            i = -1 + paramAnimationParameters.count - paramAnimationParameters.index;
            continue;
            if (this.mRandomizer == null)
                this.mRandomizer = new Random();
            i = (int)(paramAnimationParameters.count * this.mRandomizer.nextFloat());
        }
    }

    public boolean isDone()
    {
        if (AnimationUtils.currentAnimationTimeMillis() > this.mAnimation.getStartTime() + this.mMaxDelay + this.mDuration);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setAnimation(Context paramContext, int paramInt)
    {
        setAnimation(AnimationUtils.loadAnimation(paramContext, paramInt));
    }

    public void setAnimation(Animation paramAnimation)
    {
        this.mAnimation = paramAnimation;
        this.mAnimation.setFillBefore(true);
    }

    public void setDelay(float paramFloat)
    {
        this.mDelay = paramFloat;
    }

    public void setInterpolator(Context paramContext, int paramInt)
    {
        setInterpolator(AnimationUtils.loadInterpolator(paramContext, paramInt));
    }

    public void setInterpolator(Interpolator paramInterpolator)
    {
        this.mInterpolator = paramInterpolator;
    }

    public void setOrder(int paramInt)
    {
        this.mOrder = paramInt;
    }

    public void start()
    {
        this.mDuration = this.mAnimation.getDuration();
        this.mMaxDelay = -9223372036854775808L;
        this.mAnimation.setStartTime(-1L);
    }

    public boolean willOverlap()
    {
        if (this.mDelay < 1.0F);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static class AnimationParameters
    {
        public int count;
        public int index;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.LayoutAnimationController
 * JD-Core Version:        0.6.2
 */