package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class OvershootInterpolator
    implements Interpolator
{
    private final float mTension;

    public OvershootInterpolator()
    {
        this.mTension = 2.0F;
    }

    public OvershootInterpolator(float paramFloat)
    {
        this.mTension = paramFloat;
    }

    public OvershootInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OvershootInterpolator);
        this.mTension = localTypedArray.getFloat(0, 2.0F);
        localTypedArray.recycle();
    }

    public float getInterpolation(float paramFloat)
    {
        float f = paramFloat - 1.0F;
        return 1.0F + f * f * (f * (1.0F + this.mTension) + this.mTension);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.OvershootInterpolator
 * JD-Core Version:        0.6.2
 */