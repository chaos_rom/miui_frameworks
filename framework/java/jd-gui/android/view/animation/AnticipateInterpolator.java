package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class AnticipateInterpolator
    implements Interpolator
{
    private final float mTension;

    public AnticipateInterpolator()
    {
        this.mTension = 2.0F;
    }

    public AnticipateInterpolator(float paramFloat)
    {
        this.mTension = paramFloat;
    }

    public AnticipateInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AnticipateInterpolator);
        this.mTension = localTypedArray.getFloat(0, 2.0F);
        localTypedArray.recycle();
    }

    public float getInterpolation(float paramFloat)
    {
        return paramFloat * paramFloat * (paramFloat * (1.0F + this.mTension) - this.mTension);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AnticipateInterpolator
 * JD-Core Version:        0.6.2
 */