package android.view.animation;

import android.content.Context;
import android.util.AttributeSet;

public class AccelerateDecelerateInterpolator
    implements Interpolator
{
    public AccelerateDecelerateInterpolator()
    {
    }

    public AccelerateDecelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
    }

    public float getInterpolation(float paramFloat)
    {
        return 0.5F + (float)(Math.cos(3.141592653589793D * (1.0F + paramFloat)) / 2.0D);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.AccelerateDecelerateInterpolator
 * JD-Core Version:        0.6.2
 */