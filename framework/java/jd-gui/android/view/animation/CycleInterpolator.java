package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;

public class CycleInterpolator
    implements Interpolator
{
    private float mCycles;

    public CycleInterpolator(float paramFloat)
    {
        this.mCycles = paramFloat;
    }

    public CycleInterpolator(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CycleInterpolator);
        this.mCycles = localTypedArray.getFloat(0, 1.0F);
        localTypedArray.recycle();
    }

    public float getInterpolation(float paramFloat)
    {
        return (float)Math.sin(3.141592653589793D * (2.0F * this.mCycles) * paramFloat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.animation.CycleInterpolator
 * JD-Core Version:        0.6.2
 */