package android.view;

import android.app.ActivityManager;
import android.content.res.CompatibilityInfo;
import android.opengl.ManagedEGLContext;
import android.os.IBinder;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;

public class WindowManagerImpl
    implements WindowManager
{
    public static final int ADD_APP_EXITING = -4;
    public static final int ADD_BAD_APP_TOKEN = -1;
    public static final int ADD_BAD_SUBWINDOW_TOKEN = -2;
    public static final int ADD_DUPLICATE_ADD = -5;
    public static final int ADD_FLAG_APP_VISIBLE = 2;
    public static final int ADD_FLAG_IN_TOUCH_MODE = 1;
    public static final int ADD_MULTIPLE_SINGLETON = -7;
    public static final int ADD_NOT_APP_TOKEN = -3;
    public static final int ADD_OKAY = 0;
    public static final int ADD_PERMISSION_DENIED = -8;
    public static final int ADD_STARTING_NOT_NEEDED = -6;
    public static final int RELAYOUT_DEFER_SURFACE_DESTROY = 2;
    public static final int RELAYOUT_INSETS_PENDING = 1;
    public static final int RELAYOUT_RES_ANIMATING = 8;
    public static final int RELAYOUT_RES_FIRST_TIME = 2;
    public static final int RELAYOUT_RES_IN_TOUCH_MODE = 1;
    public static final int RELAYOUT_RES_SURFACE_CHANGED = 4;
    private static final HashMap<CompatibilityInfo, WindowManager> sCompatWindowManagers = new HashMap();
    private static final Object sLock = new Object();
    private static final WindowManagerImpl sWindowManager = new WindowManagerImpl();
    private boolean mNeedsEglTerminate;
    private WindowManager.LayoutParams[] mParams;
    private ViewRootImpl[] mRoots;
    private Runnable mSystemPropertyUpdater = null;
    private View[] mViews;

    // ERROR //
    private void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams, CompatibilityInfoHolder paramCompatibilityInfoHolder, boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 5
        //     3: aload_2
        //     4: instanceof 84
        //     7: ifne +13 -> 20
        //     10: new 86	java/lang/IllegalArgumentException
        //     13: dup
        //     14: ldc 88
        //     16: invokespecial 91	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     19: athrow
        //     20: aload_2
        //     21: checkcast 84	android/view/WindowManager$LayoutParams
        //     24: astore 6
        //     26: aconst_null
        //     27: astore 7
        //     29: aload_0
        //     30: monitorenter
        //     31: aload_0
        //     32: getfield 76	android/view/WindowManagerImpl:mSystemPropertyUpdater	Ljava/lang/Runnable;
        //     35: ifnonnull +22 -> 57
        //     38: aload_0
        //     39: new 8	android/view/WindowManagerImpl$1
        //     42: dup
        //     43: aload_0
        //     44: invokespecial 94	android/view/WindowManagerImpl$1:<init>	(Landroid/view/WindowManagerImpl;)V
        //     47: putfield 76	android/view/WindowManagerImpl:mSystemPropertyUpdater	Ljava/lang/Runnable;
        //     50: aload_0
        //     51: getfield 76	android/view/WindowManagerImpl:mSystemPropertyUpdater	Ljava/lang/Runnable;
        //     54: invokestatic 100	android/os/SystemProperties:addChangeCallback	(Ljava/lang/Runnable;)V
        //     57: aload_0
        //     58: aload_1
        //     59: iconst_0
        //     60: invokespecial 104	android/view/WindowManagerImpl:findViewLocked	(Landroid/view/View;Z)I
        //     63: istore 9
        //     65: iload 9
        //     67: iflt +87 -> 154
        //     70: iload 4
        //     72: ifne +42 -> 114
        //     75: new 106	java/lang/IllegalStateException
        //     78: dup
        //     79: new 108	java/lang/StringBuilder
        //     82: dup
        //     83: invokespecial 109	java/lang/StringBuilder:<init>	()V
        //     86: ldc 111
        //     88: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     91: aload_1
        //     92: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     95: ldc 120
        //     97: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     103: invokespecial 125	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     106: athrow
        //     107: astore 8
        //     109: aload_0
        //     110: monitorexit
        //     111: aload 8
        //     113: athrow
        //     114: aload_0
        //     115: getfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     118: iload 9
        //     120: aaload
        //     121: astore 17
        //     123: aload 17
        //     125: iconst_1
        //     126: aload 17
        //     128: getfield 130	android/view/ViewRootImpl:mAddNesting	I
        //     131: iadd
        //     132: putfield 130	android/view/ViewRootImpl:mAddNesting	I
        //     135: aload_1
        //     136: aload 6
        //     138: invokevirtual 136	android/view/View:setLayoutParams	(Landroid/view/ViewGroup$LayoutParams;)V
        //     141: aload 17
        //     143: aload 6
        //     145: iconst_1
        //     146: invokevirtual 139	android/view/ViewRootImpl:setLayoutParams	(Landroid/view/WindowManager$LayoutParams;Z)V
        //     149: aload_0
        //     150: monitorexit
        //     151: goto +315 -> 466
        //     154: aload 6
        //     156: getfield 142	android/view/WindowManager$LayoutParams:type	I
        //     159: sipush 1000
        //     162: if_icmplt +71 -> 233
        //     165: aload 6
        //     167: getfield 142	android/view/WindowManager$LayoutParams:type	I
        //     170: sipush 1999
        //     173: if_icmpgt +60 -> 233
        //     176: aload_0
        //     177: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     180: ifnull +287 -> 467
        //     183: aload_0
        //     184: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     187: arraylength
        //     188: istore 5
        //     190: goto +277 -> 467
        //     193: iload 16
        //     195: iload 5
        //     197: if_icmpge +36 -> 233
        //     200: aload_0
        //     201: getfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     204: iload 16
        //     206: aaload
        //     207: getfield 148	android/view/ViewRootImpl:mWindow	Landroid/view/ViewRootImpl$W;
        //     210: invokevirtual 154	android/view/ViewRootImpl$W:asBinder	()Landroid/os/IBinder;
        //     213: aload 6
        //     215: getfield 158	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     218: if_acmpne +255 -> 473
        //     221: aload_0
        //     222: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     225: iload 16
        //     227: aaload
        //     228: astore 7
        //     230: goto +243 -> 473
        //     233: new 127	android/view/ViewRootImpl
        //     236: dup
        //     237: aload_1
        //     238: invokevirtual 162	android/view/View:getContext	()Landroid/content/Context;
        //     241: invokespecial 165	android/view/ViewRootImpl:<init>	(Landroid/content/Context;)V
        //     244: astore 10
        //     246: aload 10
        //     248: iconst_1
        //     249: putfield 130	android/view/ViewRootImpl:mAddNesting	I
        //     252: aload_3
        //     253: ifnonnull +102 -> 355
        //     256: aload 10
        //     258: new 167	android/view/CompatibilityInfoHolder
        //     261: dup
        //     262: invokespecial 168	android/view/CompatibilityInfoHolder:<init>	()V
        //     265: putfield 172	android/view/ViewRootImpl:mCompatibilityInfo	Landroid/view/CompatibilityInfoHolder;
        //     268: aload_1
        //     269: aload 6
        //     271: invokevirtual 136	android/view/View:setLayoutParams	(Landroid/view/ViewGroup$LayoutParams;)V
        //     274: aload_0
        //     275: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     278: ifnonnull +86 -> 364
        //     281: iconst_1
        //     282: istore 11
        //     284: aload_0
        //     285: iconst_1
        //     286: anewarray 132	android/view/View
        //     289: putfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     292: aload_0
        //     293: iconst_1
        //     294: anewarray 127	android/view/ViewRootImpl
        //     297: putfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     300: aload_0
        //     301: iconst_1
        //     302: anewarray 84	android/view/WindowManager$LayoutParams
        //     305: putfield 174	android/view/WindowManagerImpl:mParams	[Landroid/view/WindowManager$LayoutParams;
        //     308: iload 11
        //     310: iconst_1
        //     311: isub
        //     312: istore 15
        //     314: aload_0
        //     315: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     318: iload 15
        //     320: aload_1
        //     321: aastore
        //     322: aload_0
        //     323: getfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     326: iload 15
        //     328: aload 10
        //     330: aastore
        //     331: aload_0
        //     332: getfield 174	android/view/WindowManagerImpl:mParams	[Landroid/view/WindowManager$LayoutParams;
        //     335: iload 15
        //     337: aload 6
        //     339: aastore
        //     340: aload_0
        //     341: monitorexit
        //     342: aload 10
        //     344: aload_1
        //     345: aload 6
        //     347: aload 7
        //     349: invokevirtual 178	android/view/ViewRootImpl:setView	(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/View;)V
        //     352: goto +114 -> 466
        //     355: aload 10
        //     357: aload_3
        //     358: putfield 172	android/view/ViewRootImpl:mCompatibilityInfo	Landroid/view/CompatibilityInfoHolder;
        //     361: goto -93 -> 268
        //     364: iconst_1
        //     365: aload_0
        //     366: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     369: arraylength
        //     370: iadd
        //     371: istore 11
        //     373: aload_0
        //     374: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     377: astore 12
        //     379: aload_0
        //     380: iload 11
        //     382: anewarray 132	android/view/View
        //     385: putfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     388: aload 12
        //     390: iconst_0
        //     391: aload_0
        //     392: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     395: iconst_0
        //     396: iload 11
        //     398: iconst_1
        //     399: isub
        //     400: invokestatic 184	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     403: aload_0
        //     404: getfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     407: astore 13
        //     409: aload_0
        //     410: iload 11
        //     412: anewarray 127	android/view/ViewRootImpl
        //     415: putfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     418: aload 13
        //     420: iconst_0
        //     421: aload_0
        //     422: getfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     425: iconst_0
        //     426: iload 11
        //     428: iconst_1
        //     429: isub
        //     430: invokestatic 184	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     433: aload_0
        //     434: getfield 174	android/view/WindowManagerImpl:mParams	[Landroid/view/WindowManager$LayoutParams;
        //     437: astore 14
        //     439: aload_0
        //     440: iload 11
        //     442: anewarray 84	android/view/WindowManager$LayoutParams
        //     445: putfield 174	android/view/WindowManagerImpl:mParams	[Landroid/view/WindowManager$LayoutParams;
        //     448: aload 14
        //     450: iconst_0
        //     451: aload_0
        //     452: getfield 174	android/view/WindowManagerImpl:mParams	[Landroid/view/WindowManager$LayoutParams;
        //     455: iconst_0
        //     456: iload 11
        //     458: iconst_1
        //     459: isub
        //     460: invokestatic 184	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     463: goto -155 -> 308
        //     466: return
        //     467: iconst_0
        //     468: istore 16
        //     470: goto -277 -> 193
        //     473: iinc 16 1
        //     476: goto -283 -> 193
        //
        // Exception table:
        //     from	to	target	type
        //     31	111	107	finally
        //     114	342	107	finally
        //     355	463	107	finally
    }

    // ERROR //
    private int findViewLocked(View paramView, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     6: ifnull +73 -> 79
        //     9: aload_0
        //     10: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     13: arraylength
        //     14: istore 4
        //     16: goto +54 -> 70
        //     19: iload 5
        //     21: iload 4
        //     23: if_icmpge +19 -> 42
        //     26: aload_0
        //     27: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     30: iload 5
        //     32: aaload
        //     33: aload_1
        //     34: if_acmpne +51 -> 85
        //     37: aload_0
        //     38: monitorexit
        //     39: goto +37 -> 76
        //     42: iload_2
        //     43: ifeq +18 -> 61
        //     46: new 86	java/lang/IllegalArgumentException
        //     49: dup
        //     50: ldc 186
        //     52: invokespecial 91	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     55: athrow
        //     56: astore_3
        //     57: aload_0
        //     58: monitorexit
        //     59: aload_3
        //     60: athrow
        //     61: bipush 255
        //     63: istore 5
        //     65: aload_0
        //     66: monitorexit
        //     67: goto +9 -> 76
        //     70: iconst_0
        //     71: istore 5
        //     73: goto -54 -> 19
        //     76: iload 5
        //     78: ireturn
        //     79: iconst_0
        //     80: istore 4
        //     82: goto -12 -> 70
        //     85: iinc 5 1
        //     88: goto -69 -> 19
        //
        // Exception table:
        //     from	to	target	type
        //     2	59	56	finally
        //     65	67	56	finally
    }

    public static WindowManager getDefault(CompatibilityInfo paramCompatibilityInfo)
    {
        CompatibilityInfoHolder localCompatibilityInfoHolder = new CompatibilityInfoHolder();
        localCompatibilityInfoHolder.set(paramCompatibilityInfo);
        Object localObject3;
        if (localCompatibilityInfoHolder.getIfNeeded() == null)
            localObject3 = sWindowManager;
        while (true)
        {
            return localObject3;
            synchronized (sLock)
            {
                localObject3 = (WindowManager)sCompatWindowManagers.get(paramCompatibilityInfo);
                if (localObject3 == null)
                {
                    localObject3 = new CompatModeWrapper(sWindowManager, localCompatibilityInfoHolder);
                    sCompatWindowManagers.put(paramCompatibilityInfo, localObject3);
                }
            }
        }
    }

    public static WindowManager getDefault(CompatibilityInfoHolder paramCompatibilityInfoHolder)
    {
        return new CompatModeWrapper(sWindowManager, paramCompatibilityInfoHolder);
    }

    public static WindowManagerImpl getDefault()
    {
        return sWindowManager;
    }

    private static String getWindowName(ViewRootImpl paramViewRootImpl)
    {
        return paramViewRootImpl.mWindowAttributes.getTitle() + "/" + paramViewRootImpl.getClass().getName() + '@' + Integer.toHexString(paramViewRootImpl.hashCode());
    }

    private static void removeItem(Object[] paramArrayOfObject1, Object[] paramArrayOfObject2, int paramInt)
    {
        if (paramArrayOfObject1.length > 0)
        {
            if (paramInt > 0)
                System.arraycopy(paramArrayOfObject2, 0, paramArrayOfObject1, 0, paramInt);
            if (paramInt < paramArrayOfObject1.length)
                System.arraycopy(paramArrayOfObject2, paramInt + 1, paramArrayOfObject1, paramInt, -1 + (paramArrayOfObject2.length - paramInt));
        }
    }

    public void addView(View paramView)
    {
        addView(paramView, new WindowManager.LayoutParams(2, 0, -1));
    }

    public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        addView(paramView, paramLayoutParams, null, false);
    }

    public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams, CompatibilityInfoHolder paramCompatibilityInfoHolder)
    {
        addView(paramView, paramLayoutParams, paramCompatibilityInfoHolder, false);
    }

    public void closeAll()
    {
        closeAll(null, null, null);
    }

    public void closeAll(IBinder paramIBinder, String paramString1, String paramString2)
    {
        while (true)
        {
            int j;
            try
            {
                if (this.mViews == null)
                    break label176;
                int i = this.mViews.length;
                j = 0;
                if (j < i)
                {
                    if ((paramIBinder != null) && (this.mParams[j].token != paramIBinder))
                        break label177;
                    ViewRootImpl localViewRootImpl = this.mRoots[j];
                    localViewRootImpl.mAddNesting = 1;
                    if (paramString1 != null)
                    {
                        WindowLeaked localWindowLeaked = new WindowLeaked(paramString2 + " " + paramString1 + " has leaked window " + localViewRootImpl.getView() + " that was originally added here");
                        localWindowLeaked.setStackTrace(localViewRootImpl.getLocation().getStackTrace());
                        Log.e("WindowManager", localWindowLeaked.getMessage(), localWindowLeaked);
                    }
                    removeViewLocked(j);
                    j--;
                    i--;
                    break label177;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label176: return;
            label177: j++;
        }
    }

    public void dumpGfxInfo(FileDescriptor paramFileDescriptor)
    {
        PrintWriter localPrintWriter = new PrintWriter(new FileOutputStream(paramFileDescriptor));
        while (true)
        {
            int j;
            try
            {
                try
                {
                    if (this.mViews != null)
                    {
                        int i = this.mViews.length;
                        localPrintWriter.println("Profile data in ms:");
                        j = 0;
                        if (j < i)
                        {
                            ViewRootImpl localViewRootImpl2 = this.mRoots[j];
                            String str2 = getWindowName(localViewRootImpl2);
                            Object[] arrayOfObject6 = new Object[1];
                            arrayOfObject6[0] = str2;
                            localPrintWriter.printf("\n\t%s", arrayOfObject6);
                            HardwareRenderer localHardwareRenderer2 = localViewRootImpl2.getView().mAttachInfo.mHardwareRenderer;
                            if (localHardwareRenderer2 != null)
                                localHardwareRenderer2.dumpGfxInfo(localPrintWriter);
                        }
                        else
                        {
                            localPrintWriter.println("\nView hierarchy:\n");
                            int k = 0;
                            int m = 0;
                            int[] arrayOfInt = new int[2];
                            int n = 0;
                            if (n < i)
                            {
                                ViewRootImpl localViewRootImpl1 = this.mRoots[n];
                                localViewRootImpl1.dumpGfxInfo(arrayOfInt);
                                String str1 = getWindowName(localViewRootImpl1);
                                Object[] arrayOfObject4 = new Object[3];
                                arrayOfObject4[0] = str1;
                                arrayOfObject4[1] = Integer.valueOf(arrayOfInt[0]);
                                arrayOfObject4[2] = Float.valueOf(arrayOfInt[1] / 1024.0F);
                                localPrintWriter.printf("    %s\n    %d views, %.2f kB of display lists", arrayOfObject4);
                                HardwareRenderer localHardwareRenderer1 = localViewRootImpl1.getView().mAttachInfo.mHardwareRenderer;
                                if (localHardwareRenderer1 != null)
                                {
                                    Object[] arrayOfObject5 = new Object[1];
                                    arrayOfObject5[0] = Long.valueOf(localHardwareRenderer1.getFrameCount());
                                    localPrintWriter.printf(", %d frames rendered", arrayOfObject5);
                                }
                                localPrintWriter.printf("\n\n", new Object[0]);
                                k += arrayOfInt[0];
                                m += arrayOfInt[1];
                                n++;
                                continue;
                            }
                            Object[] arrayOfObject1 = new Object[1];
                            arrayOfObject1[0] = Integer.valueOf(i);
                            localPrintWriter.printf("\nTotal ViewRootImpl: %d\n", arrayOfObject1);
                            Object[] arrayOfObject2 = new Object[1];
                            arrayOfObject2[0] = Integer.valueOf(k);
                            localPrintWriter.printf("Total Views:                %d\n", arrayOfObject2);
                            Object[] arrayOfObject3 = new Object[1];
                            arrayOfObject3[0] = Float.valueOf(m / 1024.0F);
                            localPrintWriter.printf("Total DisplayList:    %.2f kB\n\n", arrayOfObject3);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                finally
                {
                }
            }
            finally
            {
                localPrintWriter.flush();
            }
            j++;
        }
    }

    public void endTrimMemory()
    {
        HardwareRenderer.endTrimMemory();
        if (this.mNeedsEglTerminate)
        {
            ManagedEGLContext.doTerminate();
            this.mNeedsEglTerminate = false;
        }
    }

    void finishRemoveViewLocked(View paramView, int paramInt)
    {
        int i = this.mViews.length;
        View[] arrayOfView = new View[i - 1];
        removeItem(arrayOfView, this.mViews, paramInt);
        this.mViews = arrayOfView;
        ViewRootImpl[] arrayOfViewRootImpl = new ViewRootImpl[i - 1];
        removeItem(arrayOfViewRootImpl, this.mRoots, paramInt);
        this.mRoots = arrayOfViewRootImpl;
        WindowManager.LayoutParams[] arrayOfLayoutParams = new WindowManager.LayoutParams[i - 1];
        removeItem(arrayOfLayoutParams, this.mParams, paramInt);
        this.mParams = arrayOfLayoutParams;
        if (paramView != null)
            paramView.assignParent(null);
    }

    public Display getDefaultDisplay()
    {
        return new Display(0, null);
    }

    public WindowManager.LayoutParams getRootViewLayoutParameter(View paramView)
    {
        WindowManager.LayoutParams localLayoutParams = null;
        for (ViewParent localViewParent = paramView.getParent(); (localViewParent != null) && (!(localViewParent instanceof ViewRootImpl)); localViewParent = localViewParent.getParent());
        if (localViewParent == null);
        label84: 
        while (true)
        {
            return localLayoutParams;
            ViewRootImpl localViewRootImpl = (ViewRootImpl)localViewParent;
            int i = this.mRoots.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label84;
                if (this.mRoots[j] == localViewRootImpl)
                {
                    localLayoutParams = this.mParams[j];
                    break;
                }
            }
        }
    }

    public boolean isHardwareAccelerated()
    {
        return false;
    }

    public void removeView(View paramView)
    {
        try
        {
            View localView = removeViewLocked(findViewLocked(paramView, true));
            if (localView == paramView)
                return;
            throw new IllegalStateException("Calling with view " + paramView + " but the ViewAncestor is attached to " + localView);
        }
        finally
        {
        }
    }

    public void removeViewImmediate(View paramView)
    {
        try
        {
            int i = findViewLocked(paramView, true);
            ViewRootImpl localViewRootImpl = this.mRoots[i];
            View localView = localViewRootImpl.getView();
            localViewRootImpl.mAddNesting = 0;
            if (paramView != null)
            {
                InputMethodManager localInputMethodManager = InputMethodManager.getInstance(paramView.getContext());
                if (localInputMethodManager != null)
                    localInputMethodManager.windowDismissed(this.mViews[i].getWindowToken());
            }
            localViewRootImpl.die(true);
            finishRemoveViewLocked(localView, i);
            if (localView == paramView)
                return;
            throw new IllegalStateException("Calling with view " + paramView + " but the ViewAncestor is attached to " + localView);
        }
        finally
        {
        }
    }

    View removeViewLocked(int paramInt)
    {
        ViewRootImpl localViewRootImpl = this.mRoots[paramInt];
        View localView = localViewRootImpl.getView();
        localViewRootImpl.mAddNesting = (-1 + localViewRootImpl.mAddNesting);
        if (localViewRootImpl.mAddNesting > 0);
        while (true)
        {
            return localView;
            if (localView != null)
            {
                InputMethodManager localInputMethodManager = InputMethodManager.getInstance(localView.getContext());
                if (localInputMethodManager != null)
                    localInputMethodManager.windowDismissed(this.mViews[paramInt].getWindowToken());
            }
            localViewRootImpl.die(false);
            finishRemoveViewLocked(localView, paramInt);
        }
    }

    // ERROR //
    public void reportNewConfiguration(android.content.res.Configuration paramConfiguration)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 144	android/view/WindowManagerImpl:mViews	[Landroid/view/View;
        //     6: arraylength
        //     7: istore_3
        //     8: new 442	android/content/res/Configuration
        //     11: dup
        //     12: aload_1
        //     13: invokespecial 444	android/content/res/Configuration:<init>	(Landroid/content/res/Configuration;)V
        //     16: astore 4
        //     18: iconst_0
        //     19: istore 5
        //     21: iload 5
        //     23: iload_3
        //     24: if_icmpge +21 -> 45
        //     27: aload_0
        //     28: getfield 80	android/view/WindowManagerImpl:mRoots	[Landroid/view/ViewRootImpl;
        //     31: iload 5
        //     33: aaload
        //     34: aload 4
        //     36: invokevirtual 447	android/view/ViewRootImpl:requestUpdateConfiguration	(Landroid/content/res/Configuration;)V
        //     39: iinc 5 1
        //     42: goto -21 -> 21
        //     45: aload_0
        //     46: monitorexit
        //     47: return
        //     48: astore_2
        //     49: aload_0
        //     50: monitorexit
        //     51: aload_2
        //     52: athrow
        //     53: astore_2
        //     54: goto -5 -> 49
        //
        // Exception table:
        //     from	to	target	type
        //     2	18	48	finally
        //     49	51	48	finally
        //     27	47	53	finally
    }

    public void setStoppedState(IBinder paramIBinder, boolean paramBoolean)
    {
        while (true)
        {
            int j;
            try
            {
                if (this.mViews == null)
                    break label73;
                int i = this.mViews.length;
                j = 0;
                if (j < i)
                {
                    if ((paramIBinder != null) && (this.mParams[j].token != paramIBinder))
                        break label74;
                    this.mRoots[j].setStopped(paramBoolean);
                    break label74;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label73: return;
            label74: j++;
        }
    }

    public void startTrimMemory(int paramInt)
    {
        if (HardwareRenderer.isAvailable())
            if ((paramInt >= 80) || ((paramInt >= 60) && (!ActivityManager.isHighEndGfx(getDefaultDisplay()))))
                try
                {
                    if (this.mViews == null)
                        return;
                    int i = this.mViews.length;
                    for (int j = 0; j < i; j++)
                        this.mRoots[j].terminateHardwareResources();
                    this.mNeedsEglTerminate = true;
                    HardwareRenderer.startTrimMemory(80);
                }
                finally
                {
                }
            else
                HardwareRenderer.startTrimMemory(paramInt);
    }

    public void trimLocalMemory()
    {
        try
        {
            if (this.mViews == null)
                return;
            int i = this.mViews.length;
            for (int j = 0; j < i; j++)
                this.mRoots[j].destroyHardwareLayers();
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void updateViewLayout(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (!(paramLayoutParams instanceof WindowManager.LayoutParams))
            throw new IllegalArgumentException("Params must be WindowManager.LayoutParams");
        WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)paramLayoutParams;
        paramView.setLayoutParams(localLayoutParams);
        try
        {
            int i = findViewLocked(paramView, true);
            ViewRootImpl localViewRootImpl = this.mRoots[i];
            this.mParams[i] = localLayoutParams;
            localViewRootImpl.setLayoutParams(localLayoutParams, false);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    static class CompatModeWrapper
        implements WindowManager
    {
        private final CompatibilityInfoHolder mCompatibilityInfo;
        private final Display mDefaultDisplay;
        private final WindowManagerImpl mWindowManager;

        CompatModeWrapper(WindowManager paramWindowManager, CompatibilityInfoHolder paramCompatibilityInfoHolder)
        {
            WindowManagerImpl localWindowManagerImpl;
            if ((paramWindowManager instanceof CompatModeWrapper))
            {
                localWindowManagerImpl = ((CompatModeWrapper)paramWindowManager).mWindowManager;
                this.mWindowManager = localWindowManagerImpl;
                if (paramCompatibilityInfoHolder != null)
                    break label53;
            }
            label53: for (this.mDefaultDisplay = this.mWindowManager.getDefaultDisplay(); ; this.mDefaultDisplay = Display.createCompatibleDisplay(this.mWindowManager.getDefaultDisplay().getDisplayId(), paramCompatibilityInfoHolder))
            {
                this.mCompatibilityInfo = paramCompatibilityInfoHolder;
                return;
                localWindowManagerImpl = (WindowManagerImpl)paramWindowManager;
                break;
            }
        }

        public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
        {
            this.mWindowManager.addView(paramView, paramLayoutParams, this.mCompatibilityInfo);
        }

        public Display getDefaultDisplay()
        {
            return this.mDefaultDisplay;
        }

        public boolean isHardwareAccelerated()
        {
            return this.mWindowManager.isHardwareAccelerated();
        }

        public void removeView(View paramView)
        {
            this.mWindowManager.removeView(paramView);
        }

        public void removeViewImmediate(View paramView)
        {
            this.mWindowManager.removeViewImmediate(paramView);
        }

        public void updateViewLayout(View paramView, ViewGroup.LayoutParams paramLayoutParams)
        {
            this.mWindowManager.updateViewLayout(paramView, paramLayoutParams);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.WindowManagerImpl
 * JD-Core Version:        0.6.2
 */