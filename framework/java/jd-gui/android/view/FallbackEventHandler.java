package android.view;

public abstract interface FallbackEventHandler
{
    public abstract boolean dispatchKeyEvent(KeyEvent paramKeyEvent);

    public abstract void preDispatchKeyEvent(KeyEvent paramKeyEvent);

    public abstract void setView(View paramView);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.FallbackEventHandler
 * JD-Core Version:        0.6.2
 */