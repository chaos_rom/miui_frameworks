package android.view;

import android.util.Pool;
import android.util.Poolable;
import android.util.PoolableManager;
import android.util.Pools;

public final class VelocityTracker
    implements Poolable<VelocityTracker>
{
    private static final int ACTIVE_POINTER_ID = -1;
    private static final Pool<VelocityTracker> sPool = Pools.synchronizedPool(Pools.finitePool(new PoolableManager()
    {
        public VelocityTracker newInstance()
        {
            return new VelocityTracker(null, null);
        }

        public void onAcquired(VelocityTracker paramAnonymousVelocityTracker)
        {
        }

        public void onReleased(VelocityTracker paramAnonymousVelocityTracker)
        {
            paramAnonymousVelocityTracker.clear();
        }
    }
    , 2));
    private boolean mIsPooled;
    private VelocityTracker mNext;
    private int mPtr;
    private final String mStrategy;

    private VelocityTracker(String paramString)
    {
        this.mPtr = nativeInitialize(paramString);
        this.mStrategy = paramString;
    }

    private static native void nativeAddMovement(int paramInt, MotionEvent paramMotionEvent);

    private static native void nativeClear(int paramInt);

    private static native void nativeComputeCurrentVelocity(int paramInt1, int paramInt2, float paramFloat);

    private static native void nativeDispose(int paramInt);

    private static native boolean nativeGetEstimator(int paramInt1, int paramInt2, Estimator paramEstimator);

    private static native float nativeGetXVelocity(int paramInt1, int paramInt2);

    private static native float nativeGetYVelocity(int paramInt1, int paramInt2);

    private static native int nativeInitialize(String paramString);

    public static VelocityTracker obtain()
    {
        return (VelocityTracker)sPool.acquire();
    }

    public static VelocityTracker obtain(String paramString)
    {
        if (paramString == null);
        for (VelocityTracker localVelocityTracker = obtain(); ; localVelocityTracker = new VelocityTracker(paramString))
            return localVelocityTracker;
    }

    public void addMovement(MotionEvent paramMotionEvent)
    {
        if (paramMotionEvent == null)
            throw new IllegalArgumentException("event must not be null");
        nativeAddMovement(this.mPtr, paramMotionEvent);
    }

    public void clear()
    {
        nativeClear(this.mPtr);
    }

    public void computeCurrentVelocity(int paramInt)
    {
        nativeComputeCurrentVelocity(this.mPtr, paramInt, 3.4028235E+38F);
    }

    public void computeCurrentVelocity(int paramInt, float paramFloat)
    {
        nativeComputeCurrentVelocity(this.mPtr, paramInt, paramFloat);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (this.mPtr != 0)
            {
                nativeDispose(this.mPtr);
                this.mPtr = 0;
            }
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public boolean getEstimator(int paramInt, Estimator paramEstimator)
    {
        if (paramEstimator == null)
            throw new IllegalArgumentException("outEstimator must not be null");
        return nativeGetEstimator(this.mPtr, paramInt, paramEstimator);
    }

    public VelocityTracker getNextPoolable()
    {
        return this.mNext;
    }

    public float getXVelocity()
    {
        return nativeGetXVelocity(this.mPtr, -1);
    }

    public float getXVelocity(int paramInt)
    {
        return nativeGetXVelocity(this.mPtr, paramInt);
    }

    public float getYVelocity()
    {
        return nativeGetYVelocity(this.mPtr, -1);
    }

    public float getYVelocity(int paramInt)
    {
        return nativeGetYVelocity(this.mPtr, paramInt);
    }

    public boolean isPooled()
    {
        return this.mIsPooled;
    }

    public void recycle()
    {
        if (this.mStrategy == null)
            sPool.release(this);
    }

    public void setNextPoolable(VelocityTracker paramVelocityTracker)
    {
        this.mNext = paramVelocityTracker;
    }

    public void setPooled(boolean paramBoolean)
    {
        this.mIsPooled = paramBoolean;
    }

    public static final class Estimator
    {
        private static final int MAX_DEGREE = 4;
        public float confidence;
        public int degree;
        public final float[] xCoeff = new float[5];
        public final float[] yCoeff = new float[5];

        private float estimate(float paramFloat, float[] paramArrayOfFloat)
        {
            float f1 = 0.0F;
            float f2 = 1.0F;
            for (int i = 0; i <= this.degree; i++)
            {
                f1 += f2 * paramArrayOfFloat[i];
                f2 *= paramFloat;
            }
            return f1;
        }

        public float estimateX(float paramFloat)
        {
            return estimate(paramFloat, this.xCoeff);
        }

        public float estimateY(float paramFloat)
        {
            return estimate(paramFloat, this.yCoeff);
        }

        public float getXCoeff(int paramInt)
        {
            if (paramInt <= this.degree);
            for (float f = this.xCoeff[paramInt]; ; f = 0.0F)
                return f;
        }

        public float getYCoeff(int paramInt)
        {
            if (paramInt <= this.degree);
            for (float f = this.yCoeff[paramInt]; ; f = 0.0F)
                return f;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.VelocityTracker
 * JD-Core Version:        0.6.2
 */