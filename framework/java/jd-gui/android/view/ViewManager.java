package android.view;

public abstract interface ViewManager
{
    public abstract void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams);

    public abstract void removeView(View paramView);

    public abstract void updateViewLayout(View paramView, ViewGroup.LayoutParams paramLayoutParams);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewManager
 * JD-Core Version:        0.6.2
 */