package android.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.util.Pool;
import android.util.Poolable;
import android.util.PoolableManager;
import android.util.Pools;
import android.util.SparseLongArray;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

final class AccessibilityInteractionController
{
    private static final int POOL_SIZE = 5;
    private final Handler mHandler;
    private final long mMyLooperThreadId;
    private final int mMyProcessId;
    private final Pool<SomeArgs> mPool = Pools.synchronizedPool(Pools.finitePool(new PoolableManager()
    {
        public AccessibilityInteractionController.SomeArgs newInstance()
        {
            return new AccessibilityInteractionController.SomeArgs(AccessibilityInteractionController.this, null);
        }

        public void onAcquired(AccessibilityInteractionController.SomeArgs paramAnonymousSomeArgs)
        {
        }

        public void onReleased(AccessibilityInteractionController.SomeArgs paramAnonymousSomeArgs)
        {
            AccessibilityInteractionController.SomeArgs.access$200(paramAnonymousSomeArgs);
        }
    }
    , 5));
    private final AccessibilityNodePrefetcher mPrefetcher;
    private ArrayList<AccessibilityNodeInfo> mTempAccessibilityNodeInfoList = new ArrayList();
    private final ArrayList<View> mTempArrayList = new ArrayList();
    private final ViewRootImpl mViewRootImpl;

    public AccessibilityInteractionController(ViewRootImpl paramViewRootImpl)
    {
        Looper localLooper = paramViewRootImpl.mHandler.getLooper();
        this.mMyLooperThreadId = localLooper.getThread().getId();
        this.mMyProcessId = Process.myPid();
        this.mHandler = new PrivateHandler(localLooper);
        this.mViewRootImpl = paramViewRootImpl;
        this.mPrefetcher = new AccessibilityNodePrefetcher(null);
    }

    private void findAccessibilityNodeInfoByAccessibilityIdUiThread(Message paramMessage)
    {
        boolean bool = false;
        int i = paramMessage.arg1;
        SomeArgs localSomeArgs1 = (SomeArgs)paramMessage.obj;
        int j = localSomeArgs1.argi1;
        int k = localSomeArgs1.argi2;
        int m = localSomeArgs1.argi3;
        IAccessibilityInteractionConnectionCallback localIAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)localSomeArgs1.arg1;
        SomeArgs localSomeArgs2 = (SomeArgs)localSomeArgs1.arg2;
        this.mViewRootImpl.mAttachInfo.mActualWindowLeft = localSomeArgs2.argi1;
        this.mViewRootImpl.mAttachInfo.mActualWindowTop = localSomeArgs2.argi2;
        this.mPool.release(localSomeArgs2);
        this.mPool.release(localSomeArgs1);
        ArrayList localArrayList = this.mTempAccessibilityNodeInfoList;
        localArrayList.clear();
        try
        {
            if (this.mViewRootImpl.mView != null)
            {
                View.AttachInfo localAttachInfo1 = this.mViewRootImpl.mAttachInfo;
                if (localAttachInfo1 != null)
                    break label176;
            }
            try
            {
                this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(localArrayList, m);
                localArrayList.clear();
                while (true)
                {
                    return;
                    label176: View.AttachInfo localAttachInfo2 = this.mViewRootImpl.mAttachInfo;
                    if ((i & 0x8) != 0)
                        bool = true;
                    localAttachInfo2.mIncludeNotImportantViews = bool;
                    if (j != -1)
                        break;
                    localObject2 = this.mViewRootImpl.mView;
                    if ((localObject2 != null) && (isShown((View)localObject2)))
                        this.mPrefetcher.prefetchAccessibilityNodeInfos((View)localObject2, k, i, localArrayList);
                    this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(localArrayList, m);
                    localArrayList.clear();
                }
            }
            catch (RemoteException localRemoteException2)
            {
                while (true)
                {
                    continue;
                    View localView = findViewByAccessibilityId(j);
                    Object localObject2 = localView;
                }
            }
        }
        finally
        {
        }
        try
        {
            this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
            localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(localArrayList, m);
            localArrayList.clear();
            label327: throw localObject1;
        }
        catch (RemoteException localRemoteException1)
        {
            break label327;
        }
    }

    private void findAccessibilityNodeInfoByViewIdUiThread(Message paramMessage)
    {
        boolean bool = false;
        int i = paramMessage.arg1;
        int j = paramMessage.arg2;
        SomeArgs localSomeArgs1 = (SomeArgs)paramMessage.obj;
        int k = localSomeArgs1.argi1;
        int m = localSomeArgs1.argi2;
        IAccessibilityInteractionConnectionCallback localIAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)localSomeArgs1.arg1;
        SomeArgs localSomeArgs2 = (SomeArgs)localSomeArgs1.arg2;
        this.mViewRootImpl.mAttachInfo.mActualWindowLeft = localSomeArgs2.argi1;
        this.mViewRootImpl.mAttachInfo.mActualWindowTop = localSomeArgs2.argi2;
        this.mPool.release(localSomeArgs2);
        this.mPool.release(localSomeArgs1);
        Object localObject1 = null;
        try
        {
            if (this.mViewRootImpl.mView != null)
            {
                View.AttachInfo localAttachInfo1 = this.mViewRootImpl.mAttachInfo;
                if (localAttachInfo1 != null)
                    break label157;
            }
            try
            {
                this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(null, m);
                while (true)
                {
                    return;
                    label157: View.AttachInfo localAttachInfo2 = this.mViewRootImpl.mAttachInfo;
                    if ((i & 0x8) != 0)
                        bool = true;
                    localAttachInfo2.mIncludeNotImportantViews = bool;
                    if (j == -1)
                        break;
                    localView1 = findViewByAccessibilityId(j);
                    if (localView1 != null)
                    {
                        View localView2 = localView1.findViewById(k);
                        if ((localView2 != null) && (isShown(localView2)))
                        {
                            AccessibilityNodeInfo localAccessibilityNodeInfo = localView2.createAccessibilityNodeInfo();
                            localObject1 = localAccessibilityNodeInfo;
                        }
                    }
                    this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(localObject1, m);
                }
            }
            catch (RemoteException localRemoteException2)
            {
                while (true)
                {
                    continue;
                    View localView1 = this.mViewRootImpl.mView;
                }
            }
        }
        finally
        {
        }
        try
        {
            this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
            localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(null, m);
            label300: throw localObject2;
        }
        catch (RemoteException localRemoteException1)
        {
            break label300;
        }
    }

    private void findAccessibilityNodeInfosByTextUiThread(Message paramMessage)
    {
        int i = paramMessage.arg1;
        SomeArgs localSomeArgs1 = (SomeArgs)paramMessage.obj;
        String str = (String)localSomeArgs1.arg1;
        int j = localSomeArgs1.argi1;
        int k = localSomeArgs1.argi2;
        int m = localSomeArgs1.argi3;
        SomeArgs localSomeArgs2 = (SomeArgs)localSomeArgs1.arg2;
        IAccessibilityInteractionConnectionCallback localIAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)localSomeArgs2.arg1;
        this.mViewRootImpl.mAttachInfo.mActualWindowLeft = localSomeArgs2.argi1;
        this.mViewRootImpl.mAttachInfo.mActualWindowTop = localSomeArgs2.argi2;
        this.mPool.release(localSomeArgs2);
        this.mPool.release(localSomeArgs1);
        Object localObject1 = null;
        while (true)
        {
            int i1;
            try
            {
                if (this.mViewRootImpl.mView != null)
                {
                    View.AttachInfo localAttachInfo1 = this.mViewRootImpl.mAttachInfo;
                    if (localAttachInfo1 != null)
                        continue;
                }
                ArrayList localArrayList;
                int n;
                try
                {
                    this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(null, m);
                    return;
                    View.AttachInfo localAttachInfo2 = this.mViewRootImpl.mAttachInfo;
                    if ((i & 0x8) != 0)
                    {
                        bool = true;
                        localAttachInfo2.mIncludeNotImportantViews = bool;
                        if (j == -1)
                            continue;
                        localView1 = findViewByAccessibilityId(j);
                        if ((localView1 != null) && (isShown(localView1)))
                        {
                            AccessibilityNodeProvider localAccessibilityNodeProvider1 = localView1.getAccessibilityNodeProvider();
                            if (localAccessibilityNodeProvider1 == null)
                                continue;
                            List localList2 = localAccessibilityNodeProvider1.findAccessibilityNodeInfosByText(str, k);
                            localObject1 = localList2;
                        }
                        this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                        localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult((List)localObject1, m);
                        continue;
                    }
                }
                catch (RemoteException localRemoteException2)
                {
                    continue;
                    boolean bool = false;
                    continue;
                    View localView1 = this.mViewRootImpl.mView;
                    continue;
                    if (k != -1)
                        continue;
                    localArrayList = this.mTempArrayList;
                    localArrayList.clear();
                    localView1.findViewsWithText(localArrayList, str, 7);
                    if (localArrayList.isEmpty())
                        continue;
                    localObject1 = this.mTempAccessibilityNodeInfoList;
                    ((List)localObject1).clear();
                    n = localArrayList.size();
                    i1 = 0;
                }
                if (i1 >= n)
                    continue;
                View localView2 = (View)localArrayList.get(i1);
                if (isShown(localView2))
                {
                    AccessibilityNodeProvider localAccessibilityNodeProvider2 = localView2.getAccessibilityNodeProvider();
                    if (localAccessibilityNodeProvider2 != null)
                    {
                        List localList1 = localAccessibilityNodeProvider2.findAccessibilityNodeInfosByText(str, -1);
                        if (localList1 != null)
                            ((List)localObject1).addAll(localList1);
                    }
                    else
                    {
                        ((List)localObject1).add(localView2.createAccessibilityNodeInfo());
                    }
                }
            }
            finally
            {
                try
                {
                    this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult((List)localObject1, m);
                    throw localObject2;
                }
                catch (RemoteException localRemoteException1)
                {
                    continue;
                }
            }
            i1++;
        }
    }

    // ERROR //
    private void findFocusUiThread(Message paramMessage)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 146	android/os/Message:arg1	I
        //     4: istore_2
        //     5: aload_1
        //     6: getfield 211	android/os/Message:arg2	I
        //     9: istore_3
        //     10: aload_1
        //     11: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     14: checkcast 14	android/view/AccessibilityInteractionController$SomeArgs
        //     17: astore 4
        //     19: aload 4
        //     21: getfield 153	android/view/AccessibilityInteractionController$SomeArgs:argi1	I
        //     24: istore 5
        //     26: aload 4
        //     28: getfield 156	android/view/AccessibilityInteractionController$SomeArgs:argi2	I
        //     31: istore 6
        //     33: aload 4
        //     35: getfield 159	android/view/AccessibilityInteractionController$SomeArgs:argi3	I
        //     38: istore 7
        //     40: aload 4
        //     42: getfield 161	android/view/AccessibilityInteractionController$SomeArgs:arg1	Ljava/lang/Object;
        //     45: checkcast 163	android/view/accessibility/IAccessibilityInteractionConnectionCallback
        //     48: astore 8
        //     50: aload 4
        //     52: getfield 166	android/view/AccessibilityInteractionController$SomeArgs:arg2	Ljava/lang/Object;
        //     55: checkcast 14	android/view/AccessibilityInteractionController$SomeArgs
        //     58: astore 9
        //     60: aload_0
        //     61: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     64: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     67: aload 9
        //     69: getfield 153	android/view/AccessibilityInteractionController$SomeArgs:argi1	I
        //     72: putfield 175	android/view/View$AttachInfo:mActualWindowLeft	I
        //     75: aload_0
        //     76: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     79: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     82: aload 9
        //     84: getfield 156	android/view/AccessibilityInteractionController$SomeArgs:argi2	I
        //     87: putfield 178	android/view/View$AttachInfo:mActualWindowTop	I
        //     90: aload_0
        //     91: getfield 62	android/view/AccessibilityInteractionController:mPool	Landroid/util/Pool;
        //     94: aload 9
        //     96: invokeinterface 184 2 0
        //     101: aload_0
        //     102: getfield 62	android/view/AccessibilityInteractionController:mPool	Landroid/util/Pool;
        //     105: aload 4
        //     107: invokeinterface 184 2 0
        //     112: aconst_null
        //     113: astore 10
        //     115: aload_0
        //     116: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     119: getfield 193	android/view/ViewRootImpl:mView	Landroid/view/View;
        //     122: ifnull +17 -> 139
        //     125: aload_0
        //     126: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     129: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     132: astore 14
        //     134: aload 14
        //     136: ifnonnull +25 -> 161
        //     139: aload_0
        //     140: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     143: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     146: iconst_0
        //     147: putfield 197	android/view/View$AttachInfo:mIncludeNotImportantViews	Z
        //     150: aload 8
        //     152: aconst_null
        //     153: iload 5
        //     155: invokeinterface 215 3 0
        //     160: return
        //     161: aload_0
        //     162: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     165: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     168: astore 15
        //     170: iload_2
        //     171: bipush 8
        //     173: iand
        //     174: ifeq +120 -> 294
        //     177: iconst_1
        //     178: istore 16
        //     180: aload 15
        //     182: iload 16
        //     184: putfield 197	android/view/View$AttachInfo:mIncludeNotImportantViews	Z
        //     187: iload 6
        //     189: bipush 255
        //     191: if_icmpeq +109 -> 300
        //     194: aload_0
        //     195: iload 6
        //     197: invokespecial 209	android/view/AccessibilityInteractionController:findViewByAccessibilityId	(I)Landroid/view/View;
        //     200: astore 17
        //     202: aload 17
        //     204: ifnull +136 -> 340
        //     207: aload_0
        //     208: aload 17
        //     210: invokespecial 113	android/view/AccessibilityInteractionController:isShown	(Landroid/view/View;)Z
        //     213: ifeq +127 -> 340
        //     216: iload_3
        //     217: tableswitch	default:+23 -> 240, 1:+203->420, 2:+95->312
        //     241: aconst_null
        //     242: iconst_3
        //     243: dup
        //     244: new 264	java/lang/StringBuilder
        //     247: dup
        //     248: invokespecial 265	java/lang/StringBuilder:<init>	()V
        //     251: ldc_w 267
        //     254: invokevirtual 271	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     257: iload_3
        //     258: invokevirtual 274	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     261: invokevirtual 278	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     264: invokespecial 281	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     267: athrow
        //     268: astore 11
        //     270: aload_0
        //     271: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     274: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     277: iconst_0
        //     278: putfield 197	android/view/View$AttachInfo:mIncludeNotImportantViews	Z
        //     281: aload 8
        //     283: aconst_null
        //     284: iload 5
        //     286: invokeinterface 215 3 0
        //     291: aload 11
        //     293: athrow
        //     294: iconst_0
        //     295: istore 16
        //     297: goto -117 -> 180
        //     300: aload_0
        //     301: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     304: getfield 193	android/view/ViewRootImpl:mView	Landroid/view/View;
        //     307: astore 17
        //     309: goto -107 -> 202
        //     312: aload_0
        //     313: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     316: getfield 284	android/view/ViewRootImpl:mAccessibilityFocusedHost	Landroid/view/View;
        //     319: astore 20
        //     321: aload 20
        //     323: ifnull +17 -> 340
        //     326: aload 20
        //     328: aload 17
        //     330: invokestatic 288	android/view/ViewRootImpl:isViewDescendantOf	(Landroid/view/View;Landroid/view/View;)Z
        //     333: istore 21
        //     335: iload 21
        //     337: ifne +33 -> 370
        //     340: aload_0
        //     341: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     344: getfield 170	android/view/ViewRootImpl:mAttachInfo	Landroid/view/View$AttachInfo;
        //     347: iconst_0
        //     348: putfield 197	android/view/View$AttachInfo:mIncludeNotImportantViews	Z
        //     351: aload 8
        //     353: aload 10
        //     355: iload 5
        //     357: invokeinterface 215 3 0
        //     362: goto -202 -> 160
        //     365: astore 13
        //     367: goto -207 -> 160
        //     370: aload 20
        //     372: invokevirtual 230	android/view/View:getAccessibilityNodeProvider	()Landroid/view/accessibility/AccessibilityNodeProvider;
        //     375: ifnull +28 -> 403
        //     378: aload_0
        //     379: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     382: getfield 292	android/view/ViewRootImpl:mAccessibilityFocusedVirtualView	Landroid/view/accessibility/AccessibilityNodeInfo;
        //     385: ifnull -45 -> 340
        //     388: aload_0
        //     389: getfield 102	android/view/AccessibilityInteractionController:mViewRootImpl	Landroid/view/ViewRootImpl;
        //     392: getfield 292	android/view/ViewRootImpl:mAccessibilityFocusedVirtualView	Landroid/view/accessibility/AccessibilityNodeInfo;
        //     395: invokestatic 298	android/view/accessibility/AccessibilityNodeInfo:obtain	(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;
        //     398: astore 10
        //     400: goto -60 -> 340
        //     403: iload 7
        //     405: bipush 255
        //     407: if_icmpne -67 -> 340
        //     410: aload 20
        //     412: invokevirtual 224	android/view/View:createAccessibilityNodeInfo	()Landroid/view/accessibility/AccessibilityNodeInfo;
        //     415: astore 10
        //     417: goto -77 -> 340
        //     420: aload 17
        //     422: invokevirtual 302	android/view/View:findFocus	()Landroid/view/View;
        //     425: astore 18
        //     427: aload 18
        //     429: ifnull -89 -> 340
        //     432: aload_0
        //     433: aload 18
        //     435: invokespecial 113	android/view/AccessibilityInteractionController:isShown	(Landroid/view/View;)Z
        //     438: ifeq -98 -> 340
        //     441: aload 18
        //     443: invokevirtual 224	android/view/View:createAccessibilityNodeInfo	()Landroid/view/accessibility/AccessibilityNodeInfo;
        //     446: astore 19
        //     448: aload 19
        //     450: astore 10
        //     452: goto -112 -> 340
        //     455: astore 12
        //     457: goto -166 -> 291
        //
        // Exception table:
        //     from	to	target	type
        //     115	134	268	finally
        //     161	268	268	finally
        //     300	335	268	finally
        //     370	448	268	finally
        //     139	160	365	android/os/RemoteException
        //     340	362	365	android/os/RemoteException
        //     270	291	455	android/os/RemoteException
    }

    private View findViewByAccessibilityId(int paramInt)
    {
        View localView1 = this.mViewRootImpl.mView;
        if (localView1 == null);
        for (View localView2 = null; ; localView2 = null)
            do
            {
                return localView2;
                localView2 = localView1.findViewByAccessibilityId(paramInt);
            }
            while ((localView2 == null) || (isShown(localView2)));
    }

    private void focusSearchUiThread(Message paramMessage)
    {
        int i = paramMessage.arg1;
        int j = paramMessage.arg2;
        SomeArgs localSomeArgs1 = (SomeArgs)paramMessage.obj;
        int k = localSomeArgs1.argi1;
        int m = localSomeArgs1.argi2;
        int n = localSomeArgs1.argi3;
        IAccessibilityInteractionConnectionCallback localIAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)localSomeArgs1.arg1;
        SomeArgs localSomeArgs2 = (SomeArgs)localSomeArgs1.arg2;
        this.mViewRootImpl.mAttachInfo.mActualWindowLeft = localSomeArgs2.argi1;
        this.mViewRootImpl.mAttachInfo.mActualWindowTop = localSomeArgs2.argi2;
        this.mPool.release(localSomeArgs2);
        this.mPool.release(localSomeArgs1);
        Object localObject1 = null;
        try
        {
            if (this.mViewRootImpl.mView != null)
            {
                View.AttachInfo localAttachInfo1 = this.mViewRootImpl.mAttachInfo;
                if (localAttachInfo1 != null)
                    break label161;
            }
            try
            {
                this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(null, n);
                while (true)
                {
                    return;
                    label161: View.AttachInfo localAttachInfo2 = this.mViewRootImpl.mAttachInfo;
                    if ((i & 0x8) == 0)
                        break;
                    bool = true;
                    localAttachInfo2.mIncludeNotImportantViews = bool;
                    if (j == -1)
                        break label294;
                    localView1 = findViewByAccessibilityId(j);
                    if ((localView1 == null) || (!isShown(localView1)))
                        break label352;
                    if ((m & 0x1000) != 4096)
                        break label399;
                    AccessibilityNodeProvider localAccessibilityNodeProvider1 = localView1.getAccessibilityNodeProvider();
                    if (localAccessibilityNodeProvider1 == null)
                        break label306;
                    AccessibilityNodeInfo localAccessibilityNodeInfo3 = localAccessibilityNodeProvider1.accessibilityFocusSearch(m, k);
                    localObject1 = localAccessibilityNodeInfo3;
                    if (localObject1 == null)
                        break label306;
                    this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult((AccessibilityNodeInfo)localObject1, n);
                }
            }
            catch (RemoteException localRemoteException2)
            {
                View localView1;
                while (true)
                {
                    continue;
                    boolean bool = false;
                    continue;
                    label294: localView1 = this.mViewRootImpl.mView;
                }
                label306: View localView3 = localView1.focusSearch(m);
                label315: if (localView3 != null)
                {
                    AccessibilityNodeProvider localAccessibilityNodeProvider2 = localView3.getAccessibilityNodeProvider();
                    if (localAccessibilityNodeProvider2 == null)
                        break label389;
                    AccessibilityNodeInfo localAccessibilityNodeInfo2 = localAccessibilityNodeProvider2.accessibilityFocusSearch(m, -1);
                    localObject1 = localAccessibilityNodeInfo2;
                    if (localObject1 == null)
                        break label377;
                }
                while (true)
                {
                    label352: this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult((AccessibilityNodeInfo)localObject1, n);
                    break;
                    label377: localView3 = localView3.focusSearch(m);
                    break label315;
                    label389: localObject1 = localView3.createAccessibilityNodeInfo();
                    continue;
                    label399: View localView2 = localView1.focusSearch(m);
                    if (localView2 != null)
                    {
                        AccessibilityNodeInfo localAccessibilityNodeInfo1 = localView2.createAccessibilityNodeInfo();
                        localObject1 = localAccessibilityNodeInfo1;
                    }
                }
            }
        }
        finally
        {
        }
        try
        {
            this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
            localIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult((AccessibilityNodeInfo)localObject1, n);
            label451: throw localObject2;
        }
        catch (RemoteException localRemoteException1)
        {
            break label451;
        }
    }

    private boolean isShown(View paramView)
    {
        if ((paramView.mAttachInfo != null) && (paramView.mAttachInfo.mWindowVisibility == 0) && (paramView.isShown()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void perfromAccessibilityActionUiThread(Message paramMessage)
    {
        int i = paramMessage.arg1;
        int j = paramMessage.arg2;
        SomeArgs localSomeArgs = (SomeArgs)paramMessage.obj;
        int k = localSomeArgs.argi1;
        int m = localSomeArgs.argi2;
        int n = localSomeArgs.argi3;
        IAccessibilityInteractionConnectionCallback localIAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)localSomeArgs.arg1;
        Bundle localBundle = (Bundle)localSomeArgs.arg2;
        this.mPool.release(localSomeArgs);
        boolean bool1 = false;
        try
        {
            if (this.mViewRootImpl.mView != null)
            {
                View.AttachInfo localAttachInfo1 = this.mViewRootImpl.mAttachInfo;
                if (localAttachInfo1 != null)
                    break label120;
            }
            try
            {
                this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                localIAccessibilityInteractionConnectionCallback.setPerformAccessibilityActionResult(false, n);
                while (true)
                {
                    return;
                    label120: View.AttachInfo localAttachInfo2 = this.mViewRootImpl.mAttachInfo;
                    if ((i & 0x8) == 0)
                        break;
                    bool2 = true;
                    localAttachInfo2.mIncludeNotImportantViews = bool2;
                    if (j == -1)
                        break label238;
                    localView = findViewByAccessibilityId(j);
                    if ((localView != null) && (isShown(localView)))
                    {
                        AccessibilityNodeProvider localAccessibilityNodeProvider = localView.getAccessibilityNodeProvider();
                        if (localAccessibilityNodeProvider == null)
                            break label250;
                        boolean bool4 = localAccessibilityNodeProvider.performAction(k, m, localBundle);
                        bool1 = bool4;
                    }
                    this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
                    localIAccessibilityInteractionConnectionCallback.setPerformAccessibilityActionResult(bool1, n);
                }
            }
            catch (RemoteException localRemoteException2)
            {
                while (true)
                {
                    continue;
                    boolean bool2 = false;
                    continue;
                    label238: View localView = this.mViewRootImpl.mView;
                    continue;
                    label250: if (k == -1)
                    {
                        boolean bool3 = localView.performAccessibilityAction(m, localBundle);
                        bool1 = bool3;
                    }
                }
            }
        }
        finally
        {
        }
        try
        {
            this.mViewRootImpl.mAttachInfo.mIncludeNotImportantViews = false;
            localIAccessibilityInteractionConnectionCallback.setPerformAccessibilityActionResult(false, n);
            label298: throw localObject;
        }
        catch (RemoteException localRemoteException1)
        {
            break label298;
        }
    }

    public void findAccessibilityNodeInfoByAccessibilityIdClientThread(long paramLong1, int paramInt1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt4, int paramInt5, long paramLong2)
    {
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 2;
        localMessage.arg1 = paramInt4;
        SomeArgs localSomeArgs1 = (SomeArgs)this.mPool.acquire();
        localSomeArgs1.argi1 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
        localSomeArgs1.argi2 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
        localSomeArgs1.argi3 = paramInt3;
        localSomeArgs1.arg1 = paramIAccessibilityInteractionConnectionCallback;
        SomeArgs localSomeArgs2 = (SomeArgs)this.mPool.acquire();
        localSomeArgs2.argi1 = paramInt1;
        localSomeArgs2.argi2 = paramInt2;
        localSomeArgs1.arg2 = localSomeArgs2;
        localMessage.obj = localSomeArgs1;
        if ((paramInt5 == this.mMyProcessId) && (paramLong2 == this.mMyLooperThreadId))
            AccessibilityInteractionClient.getInstanceForThread(paramLong2).setSameThreadMessage(localMessage);
        while (true)
        {
            return;
            this.mHandler.sendMessage(localMessage);
        }
    }

    public void findAccessibilityNodeInfoByViewIdClientThread(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
    {
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 3;
        localMessage.arg1 = paramInt5;
        localMessage.arg2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
        SomeArgs localSomeArgs1 = (SomeArgs)this.mPool.acquire();
        localSomeArgs1.argi1 = paramInt1;
        localSomeArgs1.argi2 = paramInt4;
        localSomeArgs1.arg1 = paramIAccessibilityInteractionConnectionCallback;
        SomeArgs localSomeArgs2 = (SomeArgs)this.mPool.acquire();
        localSomeArgs2.argi1 = paramInt2;
        localSomeArgs2.argi2 = paramInt3;
        localSomeArgs1.arg2 = localSomeArgs2;
        localMessage.obj = localSomeArgs1;
        if ((paramInt6 == this.mMyProcessId) && (paramLong2 == this.mMyLooperThreadId))
            AccessibilityInteractionClient.getInstanceForThread(paramLong2).setSameThreadMessage(localMessage);
        while (true)
        {
            return;
            this.mHandler.sendMessage(localMessage);
        }
    }

    public void findAccessibilityNodeInfosByTextClientThread(long paramLong1, String paramString, int paramInt1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt4, int paramInt5, long paramLong2)
    {
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 4;
        localMessage.arg1 = paramInt4;
        SomeArgs localSomeArgs1 = (SomeArgs)this.mPool.acquire();
        localSomeArgs1.arg1 = paramString;
        localSomeArgs1.argi1 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
        localSomeArgs1.argi2 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
        localSomeArgs1.argi3 = paramInt3;
        SomeArgs localSomeArgs2 = (SomeArgs)this.mPool.acquire();
        localSomeArgs2.arg1 = paramIAccessibilityInteractionConnectionCallback;
        localSomeArgs2.argi1 = paramInt1;
        localSomeArgs2.argi2 = paramInt2;
        localSomeArgs1.arg2 = localSomeArgs2;
        localMessage.obj = localSomeArgs1;
        if ((paramInt5 == this.mMyProcessId) && (paramLong2 == this.mMyLooperThreadId))
            AccessibilityInteractionClient.getInstanceForThread(paramLong2).setSameThreadMessage(localMessage);
        while (true)
        {
            return;
            this.mHandler.sendMessage(localMessage);
        }
    }

    public void findFocusClientThread(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
    {
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 5;
        localMessage.arg1 = paramInt5;
        localMessage.arg2 = paramInt1;
        SomeArgs localSomeArgs1 = (SomeArgs)this.mPool.acquire();
        localSomeArgs1.argi1 = paramInt4;
        localSomeArgs1.argi2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
        localSomeArgs1.argi3 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
        localSomeArgs1.arg1 = paramIAccessibilityInteractionConnectionCallback;
        SomeArgs localSomeArgs2 = (SomeArgs)this.mPool.acquire();
        localSomeArgs2.argi1 = paramInt2;
        localSomeArgs2.argi2 = paramInt3;
        localSomeArgs1.arg2 = localSomeArgs2;
        localMessage.obj = localSomeArgs1;
        if ((paramInt6 == this.mMyProcessId) && (paramLong2 == this.mMyLooperThreadId))
            AccessibilityInteractionClient.getInstanceForThread(paramLong2).setSameThreadMessage(localMessage);
        while (true)
        {
            return;
            this.mHandler.sendMessage(localMessage);
        }
    }

    public void focusSearchClientThread(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
    {
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 6;
        localMessage.arg1 = paramInt5;
        localMessage.arg2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
        SomeArgs localSomeArgs1 = (SomeArgs)this.mPool.acquire();
        localSomeArgs1.argi1 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
        localSomeArgs1.argi2 = paramInt1;
        localSomeArgs1.argi3 = paramInt4;
        localSomeArgs1.arg1 = paramIAccessibilityInteractionConnectionCallback;
        SomeArgs localSomeArgs2 = (SomeArgs)this.mPool.acquire();
        localSomeArgs2.argi1 = paramInt2;
        localSomeArgs2.argi2 = paramInt3;
        localSomeArgs1.arg2 = localSomeArgs2;
        localMessage.obj = localSomeArgs1;
        if ((paramInt6 == this.mMyProcessId) && (paramLong2 == this.mMyLooperThreadId))
            AccessibilityInteractionClient.getInstanceForThread(paramLong2).setSameThreadMessage(localMessage);
        while (true)
        {
            return;
            this.mHandler.sendMessage(localMessage);
        }
    }

    public void performAccessibilityActionClientThread(long paramLong1, int paramInt1, Bundle paramBundle, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2)
    {
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 1;
        localMessage.arg1 = paramInt3;
        localMessage.arg2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
        SomeArgs localSomeArgs = (SomeArgs)this.mPool.acquire();
        localSomeArgs.argi1 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
        localSomeArgs.argi2 = paramInt1;
        localSomeArgs.argi3 = paramInt2;
        localSomeArgs.arg1 = paramIAccessibilityInteractionConnectionCallback;
        localSomeArgs.arg2 = paramBundle;
        localMessage.obj = localSomeArgs;
        if ((paramInt4 == this.mMyProcessId) && (paramLong2 == this.mMyLooperThreadId))
            AccessibilityInteractionClient.getInstanceForThread(paramLong2).setSameThreadMessage(localMessage);
        while (true)
        {
            return;
            this.mHandler.sendMessage(localMessage);
        }
    }

    private class PrivateHandler extends Handler
    {
        private static final int MSG_FIND_ACCESSIBLITY_NODE_INFO_BY_ACCESSIBILITY_ID = 2;
        private static final int MSG_FIND_ACCESSIBLITY_NODE_INFO_BY_TEXT = 4;
        private static final int MSG_FIND_ACCESSIBLITY_NODE_INFO_BY_VIEW_ID = 3;
        private static final int MSG_FIND_FOCUS = 5;
        private static final int MSG_FOCUS_SEARCH = 6;
        private static final int MSG_PERFORM_ACCESSIBILITY_ACTION = 1;

        public PrivateHandler(Looper arg2)
        {
            super();
        }

        public String getMessageName(Message paramMessage)
        {
            int i = paramMessage.what;
            String str;
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unknown message type: " + i);
            case 1:
                str = "MSG_PERFORM_ACCESSIBILITY_ACTION";
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            }
            while (true)
            {
                return str;
                str = "MSG_FIND_ACCESSIBLITY_NODE_INFO_BY_ACCESSIBILITY_ID";
                continue;
                str = "MSG_FIND_ACCESSIBLITY_NODE_INFO_BY_VIEW_ID";
                continue;
                str = "MSG_FIND_ACCESSIBLITY_NODE_INFO_BY_TEXT";
                continue;
                str = "MSG_FIND_FOCUS";
                continue;
                str = "MSG_FOCUS_SEARCH";
            }
        }

        public void handleMessage(Message paramMessage)
        {
            int i = paramMessage.what;
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unknown message type: " + i);
            case 2:
                AccessibilityInteractionController.this.findAccessibilityNodeInfoByAccessibilityIdUiThread(paramMessage);
            case 1:
            case 3:
            case 4:
            case 5:
            case 6:
            }
            while (true)
            {
                return;
                AccessibilityInteractionController.this.perfromAccessibilityActionUiThread(paramMessage);
                continue;
                AccessibilityInteractionController.this.findAccessibilityNodeInfoByViewIdUiThread(paramMessage);
                continue;
                AccessibilityInteractionController.this.findAccessibilityNodeInfosByTextUiThread(paramMessage);
                continue;
                AccessibilityInteractionController.this.findFocusUiThread(paramMessage);
                continue;
                AccessibilityInteractionController.this.focusSearchUiThread(paramMessage);
            }
        }
    }

    private class AccessibilityNodePrefetcher
    {
        private static final int MAX_ACCESSIBILITY_NODE_INFO_BATCH_SIZE = 50;
        private final ArrayList<View> mTempViewList = new ArrayList();

        private AccessibilityNodePrefetcher()
        {
        }

        private void prefetchDescendantsOfRealNode(View paramView, List<AccessibilityNodeInfo> paramList)
        {
            if (!(paramView instanceof ViewGroup))
                return;
            HashMap localHashMap = new HashMap();
            ArrayList localArrayList = this.mTempViewList;
            localArrayList.clear();
            while (true)
            {
                int j;
                try
                {
                    paramView.addChildrenForAccessibility(localArrayList);
                    int i = localArrayList.size();
                    j = 0;
                    if (j < i)
                    {
                        int k = paramList.size();
                        if (k >= 50)
                        {
                            localArrayList.clear();
                            break;
                        }
                        View localView2 = (View)localArrayList.get(j);
                        if (!AccessibilityInteractionController.this.isShown(localView2))
                            break label289;
                        AccessibilityNodeProvider localAccessibilityNodeProvider = localView2.getAccessibilityNodeProvider();
                        if (localAccessibilityNodeProvider == null)
                        {
                            AccessibilityNodeInfo localAccessibilityNodeInfo3 = localView2.createAccessibilityNodeInfo();
                            if (localAccessibilityNodeInfo3 != null)
                            {
                                paramList.add(localAccessibilityNodeInfo3);
                                localHashMap.put(localView2, null);
                            }
                        }
                        else
                        {
                            AccessibilityNodeInfo localAccessibilityNodeInfo2 = localAccessibilityNodeProvider.createAccessibilityNodeInfo(-1);
                            if (localAccessibilityNodeInfo2 != null)
                            {
                                paramList.add(localAccessibilityNodeInfo2);
                                localHashMap.put(localView2, localAccessibilityNodeInfo2);
                            }
                        }
                    }
                }
                finally
                {
                    localArrayList.clear();
                }
                localArrayList.clear();
                if (paramList.size() >= 50)
                    break;
                Iterator localIterator = localHashMap.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    View localView1 = (View)localEntry.getKey();
                    AccessibilityNodeInfo localAccessibilityNodeInfo1 = (AccessibilityNodeInfo)localEntry.getValue();
                    if (localAccessibilityNodeInfo1 == null)
                        prefetchDescendantsOfRealNode(localView1, paramList);
                    else
                        prefetchDescendantsOfVirtualNode(localAccessibilityNodeInfo1, localView1.getAccessibilityNodeProvider(), paramList);
                }
                break;
                label289: j++;
            }
        }

        private void prefetchDescendantsOfVirtualNode(AccessibilityNodeInfo paramAccessibilityNodeInfo, AccessibilityNodeProvider paramAccessibilityNodeProvider, List<AccessibilityNodeInfo> paramList)
        {
            SparseLongArray localSparseLongArray = paramAccessibilityNodeInfo.getChildNodeIds();
            int i = paramList.size();
            int j = localSparseLongArray.size();
            int k = 0;
            if (k < j)
                if (paramList.size() < 50);
            while (true)
            {
                return;
                AccessibilityNodeInfo localAccessibilityNodeInfo = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(AccessibilityNodeInfo.getVirtualDescendantId(localSparseLongArray.get(k)));
                if (localAccessibilityNodeInfo != null)
                    paramList.add(localAccessibilityNodeInfo);
                k++;
                break;
                if (paramList.size() < 50)
                {
                    int m = paramList.size() - i;
                    for (int n = 0; n < m; n++)
                        prefetchDescendantsOfVirtualNode((AccessibilityNodeInfo)paramList.get(i + n), paramAccessibilityNodeProvider, paramList);
                }
            }
        }

        private void prefetchPredecessorsOfRealNode(View paramView, List<AccessibilityNodeInfo> paramList)
        {
            for (ViewParent localViewParent = paramView.getParentForAccessibility(); ((localViewParent instanceof View)) && (paramList.size() < 50); localViewParent = localViewParent.getParentForAccessibility())
            {
                AccessibilityNodeInfo localAccessibilityNodeInfo = ((View)localViewParent).createAccessibilityNodeInfo();
                if (localAccessibilityNodeInfo != null)
                    paramList.add(localAccessibilityNodeInfo);
            }
        }

        private void prefetchPredecessorsOfVirtualNode(AccessibilityNodeInfo paramAccessibilityNodeInfo, View paramView, AccessibilityNodeProvider paramAccessibilityNodeProvider, List<AccessibilityNodeInfo> paramList)
        {
            long l = paramAccessibilityNodeInfo.getParentNodeId();
            int i = AccessibilityNodeInfo.getAccessibilityViewId(l);
            if ((i == -1) || (paramList.size() >= 50));
            while (true)
            {
                return;
                int j = AccessibilityNodeInfo.getVirtualDescendantId(l);
                if ((j != -1) || (i == paramView.getAccessibilityViewId()))
                {
                    AccessibilityNodeInfo localAccessibilityNodeInfo = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(j);
                    if (localAccessibilityNodeInfo != null)
                        paramList.add(localAccessibilityNodeInfo);
                    l = localAccessibilityNodeInfo.getParentNodeId();
                    i = AccessibilityNodeInfo.getAccessibilityViewId(l);
                    break;
                }
                prefetchPredecessorsOfRealNode(paramView, paramList);
            }
        }

        private void prefetchSiblingsOfRealNode(View paramView, List<AccessibilityNodeInfo> paramList)
        {
            ViewParent localViewParent = paramView.getParentForAccessibility();
            ViewGroup localViewGroup;
            ArrayList localArrayList;
            if ((localViewParent instanceof ViewGroup))
            {
                localViewGroup = (ViewGroup)localViewParent;
                localArrayList = this.mTempViewList;
                localArrayList.clear();
            }
            while (true)
            {
                int j;
                try
                {
                    localViewGroup.addChildrenForAccessibility(localArrayList);
                    int i = localArrayList.size();
                    j = 0;
                    if (j < i)
                    {
                        int k = paramList.size();
                        if (k < 50);
                    }
                    else
                    {
                        return;
                    }
                    View localView = (View)localArrayList.get(j);
                    if ((localView.getAccessibilityViewId() != paramView.getAccessibilityViewId()) && (AccessibilityInteractionController.this.isShown(localView)))
                    {
                        AccessibilityNodeProvider localAccessibilityNodeProvider = localView.getAccessibilityNodeProvider();
                        Object localObject2;
                        if (localAccessibilityNodeProvider == null)
                        {
                            localObject2 = localView.createAccessibilityNodeInfo();
                            if (localObject2 != null)
                                paramList.add(localObject2);
                        }
                        else
                        {
                            AccessibilityNodeInfo localAccessibilityNodeInfo = localAccessibilityNodeProvider.createAccessibilityNodeInfo(-1);
                            localObject2 = localAccessibilityNodeInfo;
                            continue;
                        }
                    }
                }
                finally
                {
                    localArrayList.clear();
                }
                j++;
            }
        }

        private void prefetchSiblingsOfVirtualNode(AccessibilityNodeInfo paramAccessibilityNodeInfo, View paramView, AccessibilityNodeProvider paramAccessibilityNodeProvider, List<AccessibilityNodeInfo> paramList)
        {
            long l1 = paramAccessibilityNodeInfo.getParentNodeId();
            int i = AccessibilityNodeInfo.getAccessibilityViewId(l1);
            int j = AccessibilityNodeInfo.getVirtualDescendantId(l1);
            SparseLongArray localSparseLongArray;
            int m;
            if ((j != -1) || (i == paramView.getAccessibilityViewId()))
            {
                AccessibilityNodeInfo localAccessibilityNodeInfo1 = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(j);
                if (localAccessibilityNodeInfo1 != null)
                {
                    localSparseLongArray = localAccessibilityNodeInfo1.getChildNodeIds();
                    int k = localSparseLongArray.size();
                    m = 0;
                    if ((m < k) && (paramList.size() < 50))
                        break label86;
                }
            }
            while (true)
            {
                return;
                label86: long l2 = localSparseLongArray.get(m);
                if (l2 != paramAccessibilityNodeInfo.getSourceNodeId())
                {
                    AccessibilityNodeInfo localAccessibilityNodeInfo2 = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(AccessibilityNodeInfo.getVirtualDescendantId(l2));
                    if (localAccessibilityNodeInfo2 != null)
                        paramList.add(localAccessibilityNodeInfo2);
                }
                m++;
                break;
                prefetchSiblingsOfRealNode(paramView, paramList);
            }
        }

        public void prefetchAccessibilityNodeInfos(View paramView, int paramInt1, int paramInt2, List<AccessibilityNodeInfo> paramList)
        {
            AccessibilityNodeProvider localAccessibilityNodeProvider = paramView.getAccessibilityNodeProvider();
            if (localAccessibilityNodeProvider == null)
            {
                AccessibilityNodeInfo localAccessibilityNodeInfo2 = paramView.createAccessibilityNodeInfo();
                if (localAccessibilityNodeInfo2 != null)
                {
                    paramList.add(localAccessibilityNodeInfo2);
                    if ((paramInt2 & 0x1) != 0)
                        prefetchPredecessorsOfRealNode(paramView, paramList);
                    if ((paramInt2 & 0x2) != 0)
                        prefetchSiblingsOfRealNode(paramView, paramList);
                    if ((paramInt2 & 0x4) != 0)
                        prefetchDescendantsOfRealNode(paramView, paramList);
                }
            }
            while (true)
            {
                return;
                AccessibilityNodeInfo localAccessibilityNodeInfo1 = localAccessibilityNodeProvider.createAccessibilityNodeInfo(paramInt1);
                if (localAccessibilityNodeInfo1 != null)
                {
                    paramList.add(localAccessibilityNodeInfo1);
                    if ((paramInt2 & 0x1) != 0)
                        prefetchPredecessorsOfVirtualNode(localAccessibilityNodeInfo1, paramView, localAccessibilityNodeProvider, paramList);
                    if ((paramInt2 & 0x2) != 0)
                        prefetchSiblingsOfVirtualNode(localAccessibilityNodeInfo1, paramView, localAccessibilityNodeProvider, paramList);
                    if ((paramInt2 & 0x4) != 0)
                        prefetchDescendantsOfVirtualNode(localAccessibilityNodeInfo1, localAccessibilityNodeProvider, paramList);
                }
            }
        }
    }

    private class SomeArgs
        implements Poolable<SomeArgs>
    {
        public Object arg1;
        public Object arg2;
        public int argi1;
        public int argi2;
        public int argi3;
        private boolean mIsPooled;
        private SomeArgs mNext;

        private SomeArgs()
        {
        }

        private void clear()
        {
            this.arg1 = null;
            this.arg2 = null;
            this.argi1 = 0;
            this.argi2 = 0;
            this.argi3 = 0;
        }

        public SomeArgs getNextPoolable()
        {
            return this.mNext;
        }

        public boolean isPooled()
        {
            return this.mIsPooled;
        }

        public void setNextPoolable(SomeArgs paramSomeArgs)
        {
            this.mNext = paramSomeArgs;
        }

        public void setPooled(boolean paramBoolean)
        {
            this.mIsPooled = paramBoolean;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.AccessibilityInteractionController
 * JD-Core Version:        0.6.2
 */