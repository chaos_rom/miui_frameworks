package android.view;

public abstract interface CollapsibleActionView
{
    public abstract void onActionViewCollapsed();

    public abstract void onActionViewExpanded();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.CollapsibleActionView
 * JD-Core Version:        0.6.2
 */