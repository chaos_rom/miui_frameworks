package android.view;

public class SurfaceSession
{
    private int mClient;

    public SurfaceSession()
    {
        init();
    }

    private native void destroy();

    private native void init();

    protected void finalize()
        throws Throwable
    {
        destroy();
    }

    public native void kill();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.SurfaceSession
 * JD-Core Version:        0.6.2
 */