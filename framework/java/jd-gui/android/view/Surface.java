package android.view;

import android.content.res.CompatibilityInfo.Translator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.SurfaceTexture;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemProperties;
import android.util.Log;

public class Surface
    implements Parcelable
{
    public static final Parcelable.Creator<Surface> CREATOR = new Parcelable.Creator()
    {
        public Surface createFromParcel(Parcel paramAnonymousParcel)
        {
            try
            {
                localSurface = new Surface(paramAnonymousParcel, null);
                return localSurface;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Log.e("Surface", "Exception creating surface from parcel", localException);
                    Surface localSurface = null;
                }
            }
        }

        public Surface[] newArray(int paramAnonymousInt)
        {
            return new Surface[paramAnonymousInt];
        }
    };
    private static final boolean DEBUG_RELEASE = false;

    @Deprecated
    public static final int FX_SURFACE_BLUR = 65536;
    public static final int FX_SURFACE_DIM = 131072;
    public static final int FX_SURFACE_MASK = 983040;
    public static final int FX_SURFACE_NORMAL = 0;
    public static final int FX_SURFACE_SCREENSHOT = 196608;
    public static final int HIDDEN = 4;
    private static final String LOG_TAG = "Surface";
    public static final int NON_PREMULTIPLIED = 256;
    public static final int OPAQUE = 1024;
    public static final int PROTECTED_APP = 2048;
    public static final int ROTATION_0 = 0;
    public static final int ROTATION_180 = 2;
    public static final int ROTATION_270 = 3;
    public static final int ROTATION_90 = 1;
    public static final int SECURE = 128;
    public static final int SURFACE_DITHER = 4;
    public static final int SURFACE_FROZEN = 2;
    public static final int SURFACE_HIDDEN = 1;
    private static final boolean headless = "1".equals(SystemProperties.get("ro.config.headless", "0"));
    private Canvas mCanvas;
    private CompatibilityInfo.Translator mCompatibilityTranslator;
    private Matrix mCompatibleMatrix;
    private Exception mCreationStack;
    private String mName;
    private int mNativeSurface;
    private int mSaveCount;
    private int mSurfaceControl;
    private int mSurfaceGenerationId;

    static
    {
        nativeClassInit();
    }

    public Surface()
    {
        checkHeadless();
        this.mCanvas = new CompatibleCanvas(null);
    }

    public Surface(SurfaceTexture paramSurfaceTexture)
    {
        checkHeadless();
        this.mCanvas = new CompatibleCanvas(null);
        initFromSurfaceTexture(paramSurfaceTexture);
    }

    private Surface(Parcel paramParcel)
        throws Surface.OutOfResourcesException
    {
        init(paramParcel);
    }

    public Surface(SurfaceSession paramSurfaceSession, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        throws Surface.OutOfResourcesException
    {
        checkHeadless();
        this.mCanvas = new CompatibleCanvas(null);
        init(paramSurfaceSession, paramInt1, null, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
    }

    public Surface(SurfaceSession paramSurfaceSession, int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        throws Surface.OutOfResourcesException
    {
        checkHeadless();
        this.mCanvas = new CompatibleCanvas(null);
        init(paramSurfaceSession, paramInt1, paramString, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
        this.mName = paramString;
    }

    private static void checkHeadless()
    {
        if (headless)
            throw new UnsupportedOperationException("Device is headless");
    }

    public static native void closeTransaction();

    public static native void freezeDisplay(int paramInt);

    private native int getIdentity();

    private native void init(Parcel paramParcel);

    private native void init(SurfaceSession paramSurfaceSession, int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        throws Surface.OutOfResourcesException;

    private native void initFromSurfaceTexture(SurfaceTexture paramSurfaceTexture);

    private native Canvas lockCanvasNative(Rect paramRect);

    private static native void nativeClassInit();

    public static native void openTransaction();

    public static native Bitmap screenshot(int paramInt1, int paramInt2);

    public static native Bitmap screenshot(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    public static void setOrientation(int paramInt1, int paramInt2)
    {
        setOrientation(paramInt1, paramInt2, 0);
    }

    public static native void setOrientation(int paramInt1, int paramInt2, int paramInt3);

    public static native void unfreezeDisplay(int paramInt);

    public native void copyFrom(Surface paramSurface);

    public int describeContents()
    {
        return 0;
    }

    public native void destroy();

    protected void finalize()
        throws Throwable
    {
        try
        {
            super.finalize();
            return;
        }
        finally
        {
            if ((this.mNativeSurface != 0) || (this.mSurfaceControl != 0))
                Log.w("Surface", "Surface.finalize() has work. You should have called release() (" + this.mNativeSurface + ", " + this.mSurfaceControl + ")");
            release();
        }
    }

    public native void freeze();

    public int getGenerationId()
    {
        return this.mSurfaceGenerationId;
    }

    public native void hide();

    public native boolean isConsumerRunningBehind();

    public native boolean isValid();

    public Canvas lockCanvas(Rect paramRect)
        throws Surface.OutOfResourcesException, IllegalArgumentException
    {
        return lockCanvasNative(paramRect);
    }

    public native void readFromParcel(Parcel paramParcel);

    public native void release();

    public native void setAlpha(float paramFloat);

    void setCompatibilityTranslator(CompatibilityInfo.Translator paramTranslator)
    {
        if (paramTranslator != null)
        {
            float f = paramTranslator.applicationScale;
            this.mCompatibleMatrix = new Matrix();
            this.mCompatibleMatrix.setScale(f, f);
        }
    }

    public native void setFlags(int paramInt1, int paramInt2);

    public native void setFreezeTint(int paramInt);

    public native void setLayer(int paramInt);

    public native void setMatrix(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    public native void setPosition(float paramFloat1, float paramFloat2);

    public void setPosition(int paramInt1, int paramInt2)
    {
        setPosition(paramInt1, paramInt2);
    }

    public native void setSize(int paramInt1, int paramInt2);

    public native void setTransparentRegionHint(Region paramRegion);

    public native void setWindowCrop(Rect paramRect);

    public native void show();

    public String toString()
    {
        return "Surface(name=" + this.mName + ", identity=" + getIdentity() + ")";
    }

    public native void transferFrom(Surface paramSurface);

    public native void unfreeze();

    public native void unlockCanvas(Canvas paramCanvas);

    public native void unlockCanvasAndPost(Canvas paramCanvas);

    public native void writeToParcel(Parcel paramParcel, int paramInt);

    private class CompatibleCanvas extends Canvas
    {
        private Matrix mOrigMatrix = null;

        private CompatibleCanvas()
        {
        }

        public int getHeight()
        {
            int i = super.getHeight();
            if (Surface.this.mCompatibilityTranslator != null)
                i = (int)(0.5F + i * Surface.this.mCompatibilityTranslator.applicationInvertedScale);
            return i;
        }

        public void getMatrix(Matrix paramMatrix)
        {
            super.getMatrix(paramMatrix);
            if (this.mOrigMatrix == null)
                this.mOrigMatrix = new Matrix();
            this.mOrigMatrix.set(paramMatrix);
        }

        public int getWidth()
        {
            int i = super.getWidth();
            if (Surface.this.mCompatibilityTranslator != null)
                i = (int)(0.5F + i * Surface.this.mCompatibilityTranslator.applicationInvertedScale);
            return i;
        }

        public void setMatrix(Matrix paramMatrix)
        {
            if ((Surface.this.mCompatibleMatrix == null) || (this.mOrigMatrix == null) || (this.mOrigMatrix.equals(paramMatrix)))
                super.setMatrix(paramMatrix);
            while (true)
            {
                return;
                Matrix localMatrix = new Matrix(Surface.this.mCompatibleMatrix);
                localMatrix.preConcat(paramMatrix);
                super.setMatrix(localMatrix);
            }
        }
    }

    public static class OutOfResourcesException extends Exception
    {
        public OutOfResourcesException()
        {
        }

        public OutOfResourcesException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.Surface
 * JD-Core Version:        0.6.2
 */