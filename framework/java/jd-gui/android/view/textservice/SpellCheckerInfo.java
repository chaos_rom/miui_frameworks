package android.view.textservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Slog;
import android.util.Xml;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public final class SpellCheckerInfo
    implements Parcelable
{
    public static final Parcelable.Creator<SpellCheckerInfo> CREATOR = new Parcelable.Creator()
    {
        public SpellCheckerInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SpellCheckerInfo(paramAnonymousParcel);
        }

        public SpellCheckerInfo[] newArray(int paramAnonymousInt)
        {
            return new SpellCheckerInfo[paramAnonymousInt];
        }
    };
    private static final String TAG = SpellCheckerInfo.class.getSimpleName();
    private final String mId;
    private final int mLabel;
    private final ResolveInfo mService;
    private final String mSettingsActivityName;
    private final ArrayList<SpellCheckerSubtype> mSubtypes = new ArrayList();

    public SpellCheckerInfo(Context paramContext, ResolveInfo paramResolveInfo)
        throws XmlPullParserException, IOException
    {
        this.mService = paramResolveInfo;
        ServiceInfo localServiceInfo = paramResolveInfo.serviceInfo;
        this.mId = new ComponentName(localServiceInfo.packageName, localServiceInfo.name).flattenToShortString();
        PackageManager localPackageManager = paramContext.getPackageManager();
        XmlResourceParser localXmlResourceParser = null;
        try
        {
            localXmlResourceParser = localServiceInfo.loadXmlMetaData(localPackageManager, "android.view.textservice.scs");
            if (localXmlResourceParser == null)
                throw new XmlPullParserException("No android.view.textservice.scs meta-data");
        }
        catch (Exception localException)
        {
            Slog.e(TAG, "Caught exception: " + localException);
            throw new XmlPullParserException("Unable to create context for: " + localServiceInfo.packageName);
        }
        finally
        {
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
        Resources localResources = localPackageManager.getResourcesForApplication(localServiceInfo.applicationInfo);
        AttributeSet localAttributeSet = Xml.asAttributeSet(localXmlResourceParser);
        int i;
        do
            i = localXmlResourceParser.next();
        while ((i != 1) && (i != 2));
        if (!"spell-checker".equals(localXmlResourceParser.getName()))
            throw new XmlPullParserException("Meta-data does not start with spell-checker tag");
        TypedArray localTypedArray1 = localResources.obtainAttributes(localAttributeSet, R.styleable.SpellChecker);
        int j = localTypedArray1.getResourceId(0, 0);
        String str = localTypedArray1.getString(1);
        localTypedArray1.recycle();
        int k = localXmlResourceParser.getDepth();
        while (true)
        {
            int m = localXmlResourceParser.next();
            if (((m == 3) && (localXmlResourceParser.getDepth() <= k)) || (m == 1))
                break;
            if (m == 2)
            {
                if (!"subtype".equals(localXmlResourceParser.getName()))
                    throw new XmlPullParserException("Meta-data in spell-checker does not start with subtype tag");
                TypedArray localTypedArray2 = localResources.obtainAttributes(localAttributeSet, R.styleable.SpellChecker_Subtype);
                SpellCheckerSubtype localSpellCheckerSubtype = new SpellCheckerSubtype(localTypedArray2.getResourceId(0, 0), localTypedArray2.getString(1), localTypedArray2.getString(2));
                this.mSubtypes.add(localSpellCheckerSubtype);
            }
        }
        if (localXmlResourceParser != null)
            localXmlResourceParser.close();
        this.mLabel = j;
        this.mSettingsActivityName = str;
    }

    public SpellCheckerInfo(Parcel paramParcel)
    {
        this.mLabel = paramParcel.readInt();
        this.mId = paramParcel.readString();
        this.mSettingsActivityName = paramParcel.readString();
        this.mService = ((ResolveInfo)ResolveInfo.CREATOR.createFromParcel(paramParcel));
        paramParcel.readTypedList(this.mSubtypes, SpellCheckerSubtype.CREATOR);
    }

    public int describeContents()
    {
        return 0;
    }

    public ComponentName getComponent()
    {
        return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
    }

    public String getId()
    {
        return this.mId;
    }

    public String getPackageName()
    {
        return this.mService.serviceInfo.packageName;
    }

    public ServiceInfo getServiceInfo()
    {
        return this.mService.serviceInfo;
    }

    public String getSettingsActivity()
    {
        return this.mSettingsActivityName;
    }

    public SpellCheckerSubtype getSubtypeAt(int paramInt)
    {
        return (SpellCheckerSubtype)this.mSubtypes.get(paramInt);
    }

    public int getSubtypeCount()
    {
        return this.mSubtypes.size();
    }

    public Drawable loadIcon(PackageManager paramPackageManager)
    {
        return this.mService.loadIcon(paramPackageManager);
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        if ((this.mLabel == 0) || (paramPackageManager == null));
        for (Object localObject = ""; ; localObject = paramPackageManager.getText(getPackageName(), this.mLabel, this.mService.serviceInfo.applicationInfo))
            return localObject;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mLabel);
        paramParcel.writeString(this.mId);
        paramParcel.writeString(this.mSettingsActivityName);
        this.mService.writeToParcel(paramParcel, paramInt);
        paramParcel.writeTypedList(this.mSubtypes);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.textservice.SpellCheckerInfo
 * JD-Core Version:        0.6.2
 */