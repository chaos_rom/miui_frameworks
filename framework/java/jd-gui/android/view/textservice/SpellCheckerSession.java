package android.view.textservice;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.textservice.ISpellCheckerSession;
import com.android.internal.textservice.ISpellCheckerSessionListener;
import com.android.internal.textservice.ISpellCheckerSessionListener.Stub;
import com.android.internal.textservice.ITextServicesManager;
import com.android.internal.textservice.ITextServicesSessionListener;
import com.android.internal.textservice.ITextServicesSessionListener.Stub;
import java.util.LinkedList;
import java.util.Queue;

public class SpellCheckerSession
{
    private static final boolean DBG = false;
    private static final int MSG_ON_GET_SUGGESTION_MULTIPLE = 1;
    private static final int MSG_ON_GET_SUGGESTION_MULTIPLE_FOR_SENTENCE = 2;
    public static final String SERVICE_META_DATA = "android.view.textservice.scs";
    private static final String TAG = SpellCheckerSession.class.getSimpleName();
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                SpellCheckerSession.this.handleOnGetSuggestionsMultiple((SuggestionsInfo[])paramAnonymousMessage.obj);
                continue;
                SpellCheckerSession.this.handleOnGetSentenceSuggestionsMultiple((SentenceSuggestionsInfo[])paramAnonymousMessage.obj);
            }
        }
    };
    private final InternalListener mInternalListener;
    private boolean mIsUsed;
    private final SpellCheckerInfo mSpellCheckerInfo;
    private SpellCheckerSessionListener mSpellCheckerSessionListener;
    private final SpellCheckerSessionListenerImpl mSpellCheckerSessionListenerImpl;
    private final SpellCheckerSubtype mSubtype;
    private final ITextServicesManager mTextServicesManager;

    public SpellCheckerSession(SpellCheckerInfo paramSpellCheckerInfo, ITextServicesManager paramITextServicesManager, SpellCheckerSessionListener paramSpellCheckerSessionListener, SpellCheckerSubtype paramSpellCheckerSubtype)
    {
        if ((paramSpellCheckerInfo == null) || (paramSpellCheckerSessionListener == null) || (paramITextServicesManager == null))
            throw new NullPointerException();
        this.mSpellCheckerInfo = paramSpellCheckerInfo;
        this.mSpellCheckerSessionListenerImpl = new SpellCheckerSessionListenerImpl(this.mHandler);
        this.mInternalListener = new InternalListener(this.mSpellCheckerSessionListenerImpl);
        this.mTextServicesManager = paramITextServicesManager;
        this.mIsUsed = true;
        this.mSpellCheckerSessionListener = paramSpellCheckerSessionListener;
        this.mSubtype = paramSpellCheckerSubtype;
    }

    private void handleOnGetSentenceSuggestionsMultiple(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo)
    {
        this.mSpellCheckerSessionListener.onGetSentenceSuggestions(paramArrayOfSentenceSuggestionsInfo);
    }

    private void handleOnGetSuggestionsMultiple(SuggestionsInfo[] paramArrayOfSuggestionsInfo)
    {
        this.mSpellCheckerSessionListener.onGetSuggestions(paramArrayOfSuggestionsInfo);
    }

    public void cancel()
    {
        this.mSpellCheckerSessionListenerImpl.cancel();
    }

    public void close()
    {
        this.mIsUsed = false;
        try
        {
            this.mSpellCheckerSessionListenerImpl.close();
            this.mTextServicesManager.finishSpellCheckerService(this.mSpellCheckerSessionListenerImpl);
            label25: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label25;
        }
    }

    protected void finalize()
        throws Throwable
    {
        super.finalize();
        if (this.mIsUsed)
        {
            Log.e(TAG, "SpellCheckerSession was not finished properly.You should call finishShession() when you finished to use a spell checker.");
            close();
        }
    }

    public void getSentenceSuggestions(TextInfo[] paramArrayOfTextInfo, int paramInt)
    {
        this.mSpellCheckerSessionListenerImpl.getSentenceSuggestionsMultiple(paramArrayOfTextInfo, paramInt);
    }

    public SpellCheckerInfo getSpellChecker()
    {
        return this.mSpellCheckerInfo;
    }

    public ISpellCheckerSessionListener getSpellCheckerSessionListener()
    {
        return this.mSpellCheckerSessionListenerImpl;
    }

    @Deprecated
    public void getSuggestions(TextInfo paramTextInfo, int paramInt)
    {
        TextInfo[] arrayOfTextInfo = new TextInfo[1];
        arrayOfTextInfo[0] = paramTextInfo;
        getSuggestions(arrayOfTextInfo, paramInt, false);
    }

    @Deprecated
    public void getSuggestions(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean)
    {
        this.mSpellCheckerSessionListenerImpl.getSuggestionsMultiple(paramArrayOfTextInfo, paramInt, paramBoolean);
    }

    public ITextServicesSessionListener getTextServicesSessionListener()
    {
        return this.mInternalListener;
    }

    public boolean isSessionDisconnected()
    {
        return this.mSpellCheckerSessionListenerImpl.isDisconnected();
    }

    private static class InternalListener extends ITextServicesSessionListener.Stub
    {
        private final SpellCheckerSession.SpellCheckerSessionListenerImpl mParentSpellCheckerSessionListenerImpl;

        public InternalListener(SpellCheckerSession.SpellCheckerSessionListenerImpl paramSpellCheckerSessionListenerImpl)
        {
            this.mParentSpellCheckerSessionListenerImpl = paramSpellCheckerSessionListenerImpl;
        }

        public void onServiceConnected(ISpellCheckerSession paramISpellCheckerSession)
        {
            this.mParentSpellCheckerSessionListenerImpl.onServiceConnected(paramISpellCheckerSession);
        }
    }

    public static abstract interface SpellCheckerSessionListener
    {
        public abstract void onGetSentenceSuggestions(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo);

        public abstract void onGetSuggestions(SuggestionsInfo[] paramArrayOfSuggestionsInfo);
    }

    private static class SpellCheckerSessionListenerImpl extends ISpellCheckerSessionListener.Stub
    {
        private static final int TASK_CANCEL = 1;
        private static final int TASK_CLOSE = 3;
        private static final int TASK_GET_SUGGESTIONS_MULTIPLE = 2;
        private static final int TASK_GET_SUGGESTIONS_MULTIPLE_FOR_SENTENCE = 4;
        private Handler mAsyncHandler;
        private Handler mHandler;
        private ISpellCheckerSession mISpellCheckerSession;
        private boolean mOpened = false;
        private final Queue<SpellCheckerParams> mPendingTasks = new LinkedList();
        private HandlerThread mThread;

        public SpellCheckerSessionListenerImpl(Handler paramHandler)
        {
            this.mHandler = paramHandler;
        }

        private void processOrEnqueueTask(SpellCheckerParams paramSpellCheckerParams)
        {
            try
            {
                ISpellCheckerSession localISpellCheckerSession = this.mISpellCheckerSession;
                if (localISpellCheckerSession == null)
                {
                    Object localObject2 = null;
                    if (paramSpellCheckerParams.mWhat == 1)
                        while (!this.mPendingTasks.isEmpty())
                        {
                            SpellCheckerParams localSpellCheckerParams = (SpellCheckerParams)this.mPendingTasks.poll();
                            if (localSpellCheckerParams.mWhat == 3)
                                localObject2 = localSpellCheckerParams;
                        }
                    this.mPendingTasks.offer(paramSpellCheckerParams);
                    if (localObject2 != null)
                        this.mPendingTasks.offer(localObject2);
                }
                else
                {
                    processTask(localISpellCheckerSession, paramSpellCheckerParams, false);
                }
            }
            finally
            {
            }
        }

        private void processTask(ISpellCheckerSession paramISpellCheckerSession, SpellCheckerParams paramSpellCheckerParams, boolean paramBoolean)
        {
            if ((paramBoolean) || (this.mAsyncHandler == null))
                switch (paramSpellCheckerParams.mWhat)
                {
                default:
                case 1:
                case 2:
                case 4:
                case 3:
                }
            while (true)
            {
                if (paramSpellCheckerParams.mWhat == 3);
                try
                {
                    this.mISpellCheckerSession = null;
                    this.mHandler = null;
                    if (this.mThread != null)
                        this.mThread.quit();
                    this.mThread = null;
                    this.mAsyncHandler = null;
                    return;
                    try
                    {
                        paramISpellCheckerSession.onCancel();
                    }
                    catch (RemoteException localRemoteException4)
                    {
                        Log.e(SpellCheckerSession.TAG, "Failed to cancel " + localRemoteException4);
                    }
                    continue;
                    try
                    {
                        paramISpellCheckerSession.onGetSuggestionsMultiple(paramSpellCheckerParams.mTextInfos, paramSpellCheckerParams.mSuggestionsLimit, paramSpellCheckerParams.mSequentialWords);
                    }
                    catch (RemoteException localRemoteException3)
                    {
                        Log.e(SpellCheckerSession.TAG, "Failed to get suggestions " + localRemoteException3);
                    }
                    continue;
                    try
                    {
                        paramISpellCheckerSession.onGetSentenceSuggestionsMultiple(paramSpellCheckerParams.mTextInfos, paramSpellCheckerParams.mSuggestionsLimit);
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        Log.e(SpellCheckerSession.TAG, "Failed to get suggestions " + localRemoteException2);
                    }
                    continue;
                    try
                    {
                        paramISpellCheckerSession.onClose();
                    }
                    catch (RemoteException localRemoteException1)
                    {
                        Log.e(SpellCheckerSession.TAG, "Failed to close " + localRemoteException1);
                    }
                    continue;
                    paramSpellCheckerParams.mSession = paramISpellCheckerSession;
                    this.mAsyncHandler.sendMessage(Message.obtain(this.mAsyncHandler, 1, paramSpellCheckerParams));
                }
                finally
                {
                }
            }
        }

        public void cancel()
        {
            processOrEnqueueTask(new SpellCheckerParams(1, null, 0, false));
        }

        public void close()
        {
            processOrEnqueueTask(new SpellCheckerParams(3, null, 0, false));
        }

        public void getSentenceSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt)
        {
            processOrEnqueueTask(new SpellCheckerParams(4, paramArrayOfTextInfo, paramInt, false));
        }

        public void getSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean)
        {
            processOrEnqueueTask(new SpellCheckerParams(2, paramArrayOfTextInfo, paramInt, paramBoolean));
        }

        public boolean isDisconnected()
        {
            if ((this.mOpened) && (this.mISpellCheckerSession == null));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo)
        {
            this.mHandler.sendMessage(Message.obtain(this.mHandler, 2, paramArrayOfSentenceSuggestionsInfo));
        }

        public void onGetSuggestions(SuggestionsInfo[] paramArrayOfSuggestionsInfo)
        {
            try
            {
                if (this.mHandler != null)
                    this.mHandler.sendMessage(Message.obtain(this.mHandler, 1, paramArrayOfSuggestionsInfo));
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        // ERROR //
        public void onServiceConnected(ISpellCheckerSession paramISpellCheckerSession)
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: monitorenter
            //     4: aload_0
            //     5: aload_1
            //     6: putfield 57	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mISpellCheckerSession	Lcom/android/internal/textservice/ISpellCheckerSession;
            //     9: aload_1
            //     10: invokeinterface 175 1 0
            //     15: instanceof 177
            //     18: ifeq +51 -> 69
            //     21: aload_0
            //     22: getfield 80	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mThread	Landroid/os/HandlerThread;
            //     25: ifnonnull +44 -> 69
            //     28: aload_0
            //     29: new 82	android/os/HandlerThread
            //     32: dup
            //     33: ldc 179
            //     35: bipush 10
            //     37: invokespecial 182	android/os/HandlerThread:<init>	(Ljava/lang/String;I)V
            //     40: putfield 80	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mThread	Landroid/os/HandlerThread;
            //     43: aload_0
            //     44: getfield 80	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mThread	Landroid/os/HandlerThread;
            //     47: invokevirtual 185	android/os/HandlerThread:start	()V
            //     50: aload_0
            //     51: new 6	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$1
            //     54: dup
            //     55: aload_0
            //     56: aload_0
            //     57: getfield 80	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mThread	Landroid/os/HandlerThread;
            //     60: invokevirtual 189	android/os/HandlerThread:getLooper	()Landroid/os/Looper;
            //     63: invokespecial 192	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$1:<init>	(Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl;Landroid/os/Looper;)V
            //     66: putfield 78	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mAsyncHandler	Landroid/os/Handler;
            //     69: aload_0
            //     70: iconst_1
            //     71: putfield 45	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mOpened	Z
            //     74: aload_0
            //     75: monitorexit
            //     76: aload_0
            //     77: getfield 43	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mPendingTasks	Ljava/util/Queue;
            //     80: invokeinterface 66 1 0
            //     85: ifne +34 -> 119
            //     88: aload_0
            //     89: aload_1
            //     90: aload_0
            //     91: getfield 43	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:mPendingTasks	Ljava/util/Queue;
            //     94: invokeinterface 70 1 0
            //     99: checkcast 11	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$SpellCheckerParams
            //     102: iconst_0
            //     103: invokespecial 53	android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl:processTask	(Lcom/android/internal/textservice/ISpellCheckerSession;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$SpellCheckerParams;Z)V
            //     106: goto -30 -> 76
            //     109: astore_2
            //     110: aload_0
            //     111: monitorexit
            //     112: aload_2
            //     113: athrow
            //     114: astore_3
            //     115: aload_0
            //     116: monitorexit
            //     117: aload_3
            //     118: athrow
            //     119: aload_0
            //     120: monitorexit
            //     121: return
            //
            // Exception table:
            //     from	to	target	type
            //     2	4	109	finally
            //     76	106	109	finally
            //     117	119	109	finally
            //     4	76	114	finally
            //     115	117	114	finally
        }

        private static class SpellCheckerParams
        {
            public final boolean mSequentialWords;
            public ISpellCheckerSession mSession;
            public final int mSuggestionsLimit;
            public final TextInfo[] mTextInfos;
            public final int mWhat;

            public SpellCheckerParams(int paramInt1, TextInfo[] paramArrayOfTextInfo, int paramInt2, boolean paramBoolean)
            {
                this.mWhat = paramInt1;
                this.mTextInfos = paramArrayOfTextInfo;
                this.mSuggestionsLimit = paramInt2;
                this.mSequentialWords = paramBoolean;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.textservice.SpellCheckerSession
 * JD-Core Version:        0.6.2
 */