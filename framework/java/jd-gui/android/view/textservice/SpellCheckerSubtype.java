package android.view.textservice;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Slog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class SpellCheckerSubtype
    implements Parcelable
{
    public static final Parcelable.Creator<SpellCheckerSubtype> CREATOR = new Parcelable.Creator()
    {
        public SpellCheckerSubtype createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SpellCheckerSubtype(paramAnonymousParcel);
        }

        public SpellCheckerSubtype[] newArray(int paramAnonymousInt)
        {
            return new SpellCheckerSubtype[paramAnonymousInt];
        }
    };
    private static final String EXTRA_VALUE_KEY_VALUE_SEPARATOR = "=";
    private static final String EXTRA_VALUE_PAIR_SEPARATOR = ",";
    private static final String TAG = SpellCheckerSubtype.class.getSimpleName();
    private HashMap<String, String> mExtraValueHashMapCache;
    private final String mSubtypeExtraValue;
    private final int mSubtypeHashCode;
    private final String mSubtypeLocale;
    private final int mSubtypeNameResId;

    public SpellCheckerSubtype(int paramInt, String paramString1, String paramString2)
    {
        this.mSubtypeNameResId = paramInt;
        if (paramString1 != null)
        {
            this.mSubtypeLocale = paramString1;
            if (paramString2 == null)
                break label49;
        }
        while (true)
        {
            this.mSubtypeExtraValue = paramString2;
            this.mSubtypeHashCode = hashCodeInternal(this.mSubtypeLocale, this.mSubtypeExtraValue);
            return;
            paramString1 = "";
            break;
            label49: paramString2 = "";
        }
    }

    SpellCheckerSubtype(Parcel paramParcel)
    {
        this.mSubtypeNameResId = paramParcel.readInt();
        String str1 = paramParcel.readString();
        String str2;
        if (str1 != null)
        {
            this.mSubtypeLocale = str1;
            str2 = paramParcel.readString();
            if (str2 == null)
                break label62;
        }
        while (true)
        {
            this.mSubtypeExtraValue = str2;
            this.mSubtypeHashCode = hashCodeInternal(this.mSubtypeLocale, this.mSubtypeExtraValue);
            return;
            str1 = "";
            break;
            label62: str2 = "";
        }
    }

    public static Locale constructLocaleFromString(String paramString)
    {
        Locale localLocale = null;
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return localLocale;
            String[] arrayOfString = paramString.split("_", 3);
            if (arrayOfString.length == 1)
                localLocale = new Locale(arrayOfString[0]);
            else if (arrayOfString.length == 2)
                localLocale = new Locale(arrayOfString[0], arrayOfString[1]);
            else if (arrayOfString.length == 3)
                localLocale = new Locale(arrayOfString[0], arrayOfString[1], arrayOfString[2]);
        }
    }

    private HashMap<String, String> getExtraValueHashMap()
    {
        if (this.mExtraValueHashMapCache == null)
        {
            this.mExtraValueHashMapCache = new HashMap();
            String[] arrayOfString1 = this.mSubtypeExtraValue.split(",");
            int i = arrayOfString1.length;
            int j = 0;
            if (j < i)
            {
                String[] arrayOfString2 = arrayOfString1[j].split("=");
                if (arrayOfString2.length == 1)
                    this.mExtraValueHashMapCache.put(arrayOfString2[0], null);
                while (true)
                {
                    j++;
                    break;
                    if (arrayOfString2.length > 1)
                    {
                        if (arrayOfString2.length > 2)
                            Slog.w(TAG, "ExtraValue has two or more '='s");
                        this.mExtraValueHashMapCache.put(arrayOfString2[0], arrayOfString2[1]);
                    }
                }
            }
        }
        return this.mExtraValueHashMapCache;
    }

    private static int hashCodeInternal(String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        return Arrays.hashCode(arrayOfObject);
    }

    public static List<SpellCheckerSubtype> sort(Context paramContext, int paramInt, SpellCheckerInfo paramSpellCheckerInfo, List<SpellCheckerSubtype> paramList)
    {
        if (paramSpellCheckerInfo == null);
        while (true)
        {
            return paramList;
            HashSet localHashSet = new HashSet(paramList);
            ArrayList localArrayList = new ArrayList();
            int i = paramSpellCheckerInfo.getSubtypeCount();
            for (int j = 0; j < i; j++)
            {
                SpellCheckerSubtype localSpellCheckerSubtype = paramSpellCheckerInfo.getSubtypeAt(j);
                if (localHashSet.contains(localSpellCheckerSubtype))
                {
                    localArrayList.add(localSpellCheckerSubtype);
                    localHashSet.remove(localSpellCheckerSubtype);
                }
            }
            Iterator localIterator = localHashSet.iterator();
            while (localIterator.hasNext())
                localArrayList.add((SpellCheckerSubtype)localIterator.next());
            paramList = localArrayList;
        }
    }

    public boolean containsExtraValueKey(String paramString)
    {
        return getExtraValueHashMap().containsKey(paramString);
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof SpellCheckerSubtype))
        {
            SpellCheckerSubtype localSpellCheckerSubtype = (SpellCheckerSubtype)paramObject;
            if ((localSpellCheckerSubtype.hashCode() == hashCode()) && (localSpellCheckerSubtype.getNameResId() == getNameResId()) && (localSpellCheckerSubtype.getLocale().equals(getLocale())) && (localSpellCheckerSubtype.getExtraValue().equals(getExtraValue())))
                bool = true;
        }
        return bool;
    }

    public CharSequence getDisplayName(Context paramContext, String paramString, ApplicationInfo paramApplicationInfo)
    {
        Locale localLocale = constructLocaleFromString(this.mSubtypeLocale);
        String str1;
        if (localLocale != null)
        {
            str1 = localLocale.getDisplayName();
            if (this.mSubtypeNameResId != 0)
                break label40;
        }
        while (true)
        {
            return str1;
            str1 = this.mSubtypeLocale;
            break;
            label40: CharSequence localCharSequence = paramContext.getPackageManager().getText(paramString, this.mSubtypeNameResId, paramApplicationInfo);
            if (!TextUtils.isEmpty(localCharSequence))
            {
                String str2 = localCharSequence.toString();
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = str1;
                str1 = String.format(str2, arrayOfObject);
            }
        }
    }

    public String getExtraValue()
    {
        return this.mSubtypeExtraValue;
    }

    public String getExtraValueOf(String paramString)
    {
        return (String)getExtraValueHashMap().get(paramString);
    }

    public String getLocale()
    {
        return this.mSubtypeLocale;
    }

    public int getNameResId()
    {
        return this.mSubtypeNameResId;
    }

    public int hashCode()
    {
        return this.mSubtypeHashCode;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mSubtypeNameResId);
        paramParcel.writeString(this.mSubtypeLocale);
        paramParcel.writeString(this.mSubtypeExtraValue);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.textservice.SpellCheckerSubtype
 * JD-Core Version:        0.6.2
 */