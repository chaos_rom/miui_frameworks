package android.view.textservice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.android.internal.util.ArrayUtils;

public final class SuggestionsInfo
    implements Parcelable
{
    public static final Parcelable.Creator<SuggestionsInfo> CREATOR = new Parcelable.Creator()
    {
        public SuggestionsInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SuggestionsInfo(paramAnonymousParcel);
        }

        public SuggestionsInfo[] newArray(int paramAnonymousInt)
        {
            return new SuggestionsInfo[paramAnonymousInt];
        }
    };
    private static final String[] EMPTY = (String[])ArrayUtils.emptyArray(String.class);
    public static final int RESULT_ATTR_HAS_RECOMMENDED_SUGGESTIONS = 4;
    public static final int RESULT_ATTR_IN_THE_DICTIONARY = 1;
    public static final int RESULT_ATTR_LOOKS_LIKE_TYPO = 2;
    private int mCookie;
    private int mSequence;
    private final String[] mSuggestions;
    private final int mSuggestionsAttributes;
    private final boolean mSuggestionsAvailable;

    public SuggestionsInfo(int paramInt, String[] paramArrayOfString)
    {
        this(paramInt, paramArrayOfString, 0, 0);
    }

    public SuggestionsInfo(int paramInt1, String[] paramArrayOfString, int paramInt2, int paramInt3)
    {
        if (paramArrayOfString == null)
            this.mSuggestions = EMPTY;
        for (this.mSuggestionsAvailable = false; ; this.mSuggestionsAvailable = true)
        {
            this.mSuggestionsAttributes = paramInt1;
            this.mCookie = paramInt2;
            this.mSequence = paramInt3;
            return;
            this.mSuggestions = paramArrayOfString;
        }
    }

    public SuggestionsInfo(Parcel paramParcel)
    {
        this.mSuggestionsAttributes = paramParcel.readInt();
        this.mSuggestions = paramParcel.readStringArray();
        this.mCookie = paramParcel.readInt();
        this.mSequence = paramParcel.readInt();
        if (paramParcel.readInt() == i);
        while (true)
        {
            this.mSuggestionsAvailable = i;
            return;
            i = 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCookie()
    {
        return this.mCookie;
    }

    public int getSequence()
    {
        return this.mSequence;
    }

    public String getSuggestionAt(int paramInt)
    {
        return this.mSuggestions[paramInt];
    }

    public int getSuggestionsAttributes()
    {
        return this.mSuggestionsAttributes;
    }

    public int getSuggestionsCount()
    {
        if (!this.mSuggestionsAvailable);
        for (int i = -1; ; i = this.mSuggestions.length)
            return i;
    }

    public void setCookieAndSequence(int paramInt1, int paramInt2)
    {
        this.mCookie = paramInt1;
        this.mSequence = paramInt2;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mSuggestionsAttributes);
        paramParcel.writeStringArray(this.mSuggestions);
        paramParcel.writeInt(this.mCookie);
        paramParcel.writeInt(this.mSequence);
        if (this.mSuggestionsAvailable);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.textservice.SuggestionsInfo
 * JD-Core Version:        0.6.2
 */