package android.view.textservice;

import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.textservice.ITextServicesManager;
import com.android.internal.textservice.ITextServicesManager.Stub;
import java.util.Locale;

public final class TextServicesManager
{
    private static final boolean DBG;
    private static final String TAG = TextServicesManager.class.getSimpleName();
    private static TextServicesManager sInstance;
    private static ITextServicesManager sService;

    private TextServicesManager()
    {
        if (sService == null)
            sService = ITextServicesManager.Stub.asInterface(ServiceManager.getService("textservices"));
    }

    public static TextServicesManager getInstance()
    {
        TextServicesManager localTextServicesManager;
        try
        {
            if (sInstance != null)
            {
                localTextServicesManager = sInstance;
            }
            else
            {
                sInstance = new TextServicesManager();
                localTextServicesManager = sInstance;
            }
        }
        finally
        {
        }
        return localTextServicesManager;
    }

    public SpellCheckerInfo getCurrentSpellChecker()
    {
        Object localObject = null;
        try
        {
            SpellCheckerInfo localSpellCheckerInfo = sService.getCurrentSpellChecker(null);
            localObject = localSpellCheckerInfo;
            label14: return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public SpellCheckerSubtype getCurrentSpellCheckerSubtype(boolean paramBoolean)
    {
        Object localObject = null;
        try
        {
            SpellCheckerSubtype localSpellCheckerSubtype = sService.getCurrentSpellCheckerSubtype(null, paramBoolean);
            localObject = localSpellCheckerSubtype;
            return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Error in getCurrentSpellCheckerSubtype: " + localRemoteException);
        }
    }

    public SpellCheckerInfo[] getEnabledSpellCheckers()
    {
        try
        {
            SpellCheckerInfo[] arrayOfSpellCheckerInfo2 = sService.getEnabledSpellCheckers();
            arrayOfSpellCheckerInfo1 = arrayOfSpellCheckerInfo2;
            return arrayOfSpellCheckerInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Error in getEnabledSpellCheckers: " + localRemoteException);
                SpellCheckerInfo[] arrayOfSpellCheckerInfo1 = null;
            }
        }
    }

    public boolean isSpellCheckerEnabled()
    {
        try
        {
            boolean bool2 = sService.isSpellCheckerEnabled();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Error in isSpellCheckerEnabled:" + localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public SpellCheckerSession newSpellCheckerSession(Bundle paramBundle, Locale paramLocale, SpellCheckerSession.SpellCheckerSessionListener paramSpellCheckerSessionListener, boolean paramBoolean)
    {
        if (paramSpellCheckerSessionListener == null)
            throw new NullPointerException();
        if ((!paramBoolean) && (paramLocale == null))
            throw new IllegalArgumentException("Locale should not be null if you don't refer settings.");
        SpellCheckerSession localSpellCheckerSession;
        if ((paramBoolean) && (!isSpellCheckerEnabled()))
            localSpellCheckerSession = null;
        while (true)
        {
            return localSpellCheckerSession;
            SpellCheckerInfo localSpellCheckerInfo;
            try
            {
                localSpellCheckerInfo = sService.getCurrentSpellChecker(null);
                if (localSpellCheckerInfo != null)
                    break label79;
                localSpellCheckerSession = null;
            }
            catch (RemoteException localRemoteException1)
            {
                localSpellCheckerSession = null;
            }
            continue;
            label79: Object localObject = null;
            if (paramBoolean)
            {
                localObject = getCurrentSpellCheckerSubtype(true);
                if (localObject == null)
                {
                    localSpellCheckerSession = null;
                }
                else if (paramLocale != null)
                {
                    String str3 = ((SpellCheckerSubtype)localObject).getLocale();
                    String str4 = paramLocale.toString();
                    if ((str3.length() < 2) || (str4.length() < 2) || (!str3.substring(0, 2).equals(str4.substring(0, 2))))
                        localSpellCheckerSession = null;
                }
            }
            else
            {
                String str1 = paramLocale.toString();
                for (int i = 0; ; i++)
                {
                    SpellCheckerSubtype localSpellCheckerSubtype;
                    String str2;
                    if (i < localSpellCheckerInfo.getSubtypeCount())
                    {
                        localSpellCheckerSubtype = localSpellCheckerInfo.getSubtypeAt(i);
                        str2 = localSpellCheckerSubtype.getLocale();
                        if (str2.equals(str1))
                            localObject = localSpellCheckerSubtype;
                    }
                    else
                    {
                        if (localObject != null)
                            break label264;
                        localSpellCheckerSession = null;
                        break;
                    }
                    if ((str1.length() >= 2) && (str2.length() >= 2) && (str1.startsWith(str2)))
                        localObject = localSpellCheckerSubtype;
                }
                label264: localSpellCheckerSession = new SpellCheckerSession(localSpellCheckerInfo, sService, paramSpellCheckerSessionListener, (SpellCheckerSubtype)localObject);
                try
                {
                    sService.getSpellCheckerService(localSpellCheckerInfo.getId(), ((SpellCheckerSubtype)localObject).getLocale(), localSpellCheckerSession.getTextServicesSessionListener(), localSpellCheckerSession.getSpellCheckerSessionListener(), paramBundle);
                }
                catch (RemoteException localRemoteException2)
                {
                    localSpellCheckerSession = null;
                }
            }
        }
    }

    public void setCurrentSpellChecker(SpellCheckerInfo paramSpellCheckerInfo)
    {
        if (paramSpellCheckerInfo == null)
            try
            {
                throw new NullPointerException("SpellCheckerInfo is null.");
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Error in setCurrentSpellChecker: " + localRemoteException);
            }
        while (true)
        {
            return;
            sService.setCurrentSpellChecker(null, paramSpellCheckerInfo.getId());
        }
    }

    public void setSpellCheckerEnabled(boolean paramBoolean)
    {
        try
        {
            sService.setSpellCheckerEnabled(paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Error in setSpellCheckerEnabled:" + localRemoteException);
        }
    }

    public void setSpellCheckerSubtype(SpellCheckerSubtype paramSpellCheckerSubtype)
    {
        int j;
        if (paramSpellCheckerSubtype == null)
            j = 0;
        try
        {
            while (true)
            {
                sService.setCurrentSpellCheckerSubtype(null, j);
                break;
                int i = paramSpellCheckerSubtype.hashCode();
                j = i;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Error in setSpellCheckerSubtype:" + localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.textservice.TextServicesManager
 * JD-Core Version:        0.6.2
 */