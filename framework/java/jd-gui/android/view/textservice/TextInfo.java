package android.view.textservice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public final class TextInfo
    implements Parcelable
{
    public static final Parcelable.Creator<TextInfo> CREATOR = new Parcelable.Creator()
    {
        public TextInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new TextInfo(paramAnonymousParcel);
        }

        public TextInfo[] newArray(int paramAnonymousInt)
        {
            return new TextInfo[paramAnonymousInt];
        }
    };
    private final int mCookie;
    private final int mSequence;
    private final String mText;

    public TextInfo(Parcel paramParcel)
    {
        this.mText = paramParcel.readString();
        this.mCookie = paramParcel.readInt();
        this.mSequence = paramParcel.readInt();
    }

    public TextInfo(String paramString)
    {
        this(paramString, 0, 0);
    }

    public TextInfo(String paramString, int paramInt1, int paramInt2)
    {
        if (TextUtils.isEmpty(paramString))
            throw new IllegalArgumentException(paramString);
        this.mText = paramString;
        this.mCookie = paramInt1;
        this.mSequence = paramInt2;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getCookie()
    {
        return this.mCookie;
    }

    public int getSequence()
    {
        return this.mSequence;
    }

    public String getText()
    {
        return this.mText;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mText);
        paramParcel.writeInt(this.mCookie);
        paramParcel.writeInt(this.mSequence);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.textservice.TextInfo
 * JD-Core Version:        0.6.2
 */