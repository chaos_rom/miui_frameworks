package android.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.Resources.Theme;

public class ContextThemeWrapper extends ContextWrapper
{
    private Context mBase;
    private LayoutInflater mInflater;
    private Resources.Theme mTheme;
    private int mThemeResource;

    public ContextThemeWrapper()
    {
        super(null);
    }

    public ContextThemeWrapper(Context paramContext, int paramInt)
    {
        super(paramContext);
        this.mBase = paramContext;
        this.mThemeResource = paramInt;
    }

    private void initializeTheme()
    {
        if (this.mTheme == null);
        for (boolean bool = true; ; bool = false)
        {
            if (bool)
            {
                this.mTheme = getResources().newTheme();
                Resources.Theme localTheme = this.mBase.getTheme();
                if (localTheme != null)
                    this.mTheme.setTo(localTheme);
            }
            onApplyThemeResource(this.mTheme, this.mThemeResource, bool);
            return;
        }
    }

    protected void attachBaseContext(Context paramContext)
    {
        super.attachBaseContext(paramContext);
        this.mBase = paramContext;
    }

    public Object getSystemService(String paramString)
    {
        if ("layout_inflater".equals(paramString))
            if (this.mInflater == null)
                this.mInflater = LayoutInflater.from(this.mBase).cloneInContext(this);
        for (Object localObject = this.mInflater; ; localObject = this.mBase.getSystemService(paramString))
            return localObject;
    }

    public Resources.Theme getTheme()
    {
        if (this.mTheme != null);
        for (Resources.Theme localTheme = this.mTheme; ; localTheme = this.mTheme)
        {
            return localTheme;
            this.mThemeResource = Resources.selectDefaultTheme(this.mThemeResource, getApplicationInfo().targetSdkVersion);
            initializeTheme();
        }
    }

    public int getThemeResId()
    {
        return this.mThemeResource;
    }

    protected void onApplyThemeResource(Resources.Theme paramTheme, int paramInt, boolean paramBoolean)
    {
        paramTheme.applyStyle(paramInt, true);
    }

    public void setTheme(int paramInt)
    {
        this.mThemeResource = paramInt;
        initializeTheme();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ContextThemeWrapper
 * JD-Core Version:        0.6.2
 */