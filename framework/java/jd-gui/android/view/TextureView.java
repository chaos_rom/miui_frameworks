package android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;

public class TextureView extends View
{
    private static final String LOG_TAG = "TextureView";
    private Canvas mCanvas;
    private HardwareLayer mLayer;
    private SurfaceTextureListener mListener;
    private final Object[] mLock = new Object[0];
    private final Matrix mMatrix = new Matrix();
    private boolean mMatrixChanged;
    private int mNativeWindow;
    private final Object[] mNativeWindowLock = new Object[0];
    private boolean mOpaque = true;
    private int mSaveCount;
    private SurfaceTexture mSurface;
    private boolean mUpdateLayer;
    private SurfaceTexture.OnFrameAvailableListener mUpdateListener;
    private boolean mUpdateSurface;

    public TextureView(Context paramContext)
    {
        super(paramContext);
        init();
    }

    public TextureView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        init();
    }

    public TextureView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        init();
    }

    private void applyTransformMatrix()
    {
        if ((this.mMatrixChanged) && (this.mLayer != null))
        {
            this.mLayer.setTransform(this.mMatrix);
            this.mMatrixChanged = false;
        }
    }

    private void applyUpdate()
    {
        if (this.mLayer == null);
        while (true)
        {
            return;
            synchronized (this.mLock)
            {
                if (this.mUpdateLayer)
                {
                    this.mUpdateLayer = false;
                    this.mLayer.update(getWidth(), getHeight(), this.mOpaque);
                    if (this.mListener == null)
                        continue;
                    this.mListener.onSurfaceTextureUpdated(this.mSurface);
                    continue;
                }
            }
        }
    }

    private void destroySurface()
    {
        boolean bool;
        if (this.mLayer != null)
        {
            this.mSurface.detachFromGLContext();
            bool = true;
            if (this.mListener != null)
                bool = this.mListener.onSurfaceTextureDestroyed(this.mSurface);
        }
        synchronized (this.mNativeWindowLock)
        {
            nDestroyNativeWindow();
            this.mLayer.destroy();
            if (bool)
                this.mSurface.release();
            this.mSurface = null;
            this.mLayer = null;
            return;
        }
    }

    private void init()
    {
        this.mLayerPaint = new Paint();
    }

    private native void nCreateNativeWindow(SurfaceTexture paramSurfaceTexture);

    private native void nDestroyNativeWindow();

    private static native void nLockCanvas(int paramInt, Canvas paramCanvas, Rect paramRect);

    private static native void nSetDefaultBufferSize(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2);

    private static native void nUnlockCanvasAndPost(int paramInt, Canvas paramCanvas);

    private void updateLayer()
    {
        this.mUpdateLayer = true;
        invalidate();
    }

    public void buildLayer()
    {
    }

    protected void destroyHardwareResources()
    {
        super.destroyHardwareResources();
        destroySurface();
        invalidateParentCaches();
        invalidate(true);
    }

    boolean destroyLayer(boolean paramBoolean)
    {
        return false;
    }

    public final void draw(Canvas paramCanvas)
    {
        applyUpdate();
        applyTransformMatrix();
    }

    public Bitmap getBitmap()
    {
        return getBitmap(getWidth(), getHeight());
    }

    public Bitmap getBitmap(int paramInt1, int paramInt2)
    {
        if ((isAvailable()) && (paramInt1 > 0) && (paramInt2 > 0));
        for (Bitmap localBitmap = getBitmap(Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888)); ; localBitmap = null)
            return localBitmap;
    }

    public Bitmap getBitmap(Bitmap paramBitmap)
    {
        if ((paramBitmap != null) && (isAvailable()))
        {
            View.AttachInfo localAttachInfo = this.mAttachInfo;
            if ((localAttachInfo != null) && (localAttachInfo.mHardwareRenderer != null) && (localAttachInfo.mHardwareRenderer.isEnabled()) && (!localAttachInfo.mHardwareRenderer.validate()))
                throw new IllegalStateException("Could not acquire hardware rendering context");
            applyUpdate();
            applyTransformMatrix();
            if ((this.mLayer == null) && (this.mUpdateSurface))
                getHardwareLayer();
            if (this.mLayer != null)
                this.mLayer.copyInto(paramBitmap);
        }
        return paramBitmap;
    }

    HardwareLayer getHardwareLayer()
    {
        HardwareLayer localHardwareLayer;
        if (this.mLayer == null)
            if ((this.mAttachInfo == null) || (this.mAttachInfo.mHardwareRenderer == null))
                localHardwareLayer = null;
        while (true)
        {
            return localHardwareLayer;
            this.mLayer = this.mAttachInfo.mHardwareRenderer.createHardwareLayer(this.mOpaque);
            if (!this.mUpdateSurface)
                this.mSurface = this.mAttachInfo.mHardwareRenderer.createSurfaceTexture(this.mLayer);
            nSetDefaultBufferSize(this.mSurface, getWidth(), getHeight());
            nCreateNativeWindow(this.mSurface);
            this.mUpdateListener = new SurfaceTexture.OnFrameAvailableListener()
            {
                public void onFrameAvailable(SurfaceTexture paramAnonymousSurfaceTexture)
                {
                    while (true)
                    {
                        synchronized (TextureView.this.mLock)
                        {
                            TextureView.access$202(TextureView.this, true);
                            if (Looper.myLooper() == Looper.getMainLooper())
                            {
                                TextureView.this.invalidate();
                                return;
                            }
                        }
                        TextureView.this.postInvalidate();
                    }
                }
            };
            this.mSurface.setOnFrameAvailableListener(this.mUpdateListener);
            if ((this.mListener != null) && (!this.mUpdateSurface))
                this.mListener.onSurfaceTextureAvailable(this.mSurface, getWidth(), getHeight());
            if (this.mUpdateSurface)
                this.mUpdateSurface = false;
            synchronized (this.mLock)
            {
                this.mUpdateLayer = true;
                this.mMatrixChanged = true;
                this.mAttachInfo.mHardwareRenderer.setSurfaceTexture(this.mLayer, this.mSurface);
                nSetDefaultBufferSize(this.mSurface, getWidth(), getHeight());
                applyUpdate();
                applyTransformMatrix();
                localHardwareLayer = this.mLayer;
            }
        }
    }

    public int getLayerType()
    {
        return 2;
    }

    public SurfaceTexture getSurfaceTexture()
    {
        return this.mSurface;
    }

    public SurfaceTextureListener getSurfaceTextureListener()
    {
        return this.mListener;
    }

    public Matrix getTransform(Matrix paramMatrix)
    {
        if (paramMatrix == null)
            paramMatrix = new Matrix();
        paramMatrix.set(this.mMatrix);
        return paramMatrix;
    }

    boolean hasStaticLayer()
    {
        return true;
    }

    public boolean isAvailable()
    {
        if (this.mSurface != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isOpaque()
    {
        return this.mOpaque;
    }

    public Canvas lockCanvas()
    {
        return lockCanvas(null);
    }

    public Canvas lockCanvas(Rect paramRect)
    {
        Canvas localCanvas;
        if (!isAvailable())
            localCanvas = null;
        while (true)
        {
            return localCanvas;
            if (this.mCanvas == null)
                this.mCanvas = new Canvas();
            synchronized (this.mNativeWindowLock)
            {
                nLockCanvas(this.mNativeWindow, this.mCanvas, paramRect);
                this.mSaveCount = this.mCanvas.save();
                localCanvas = this.mCanvas;
            }
        }
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (!isHardwareAccelerated())
            Log.w("TextureView", "A TextureView or a subclass can only be used with hardware acceleration enabled.");
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if ((this.mLayer != null) && (this.mAttachInfo != null) && (this.mAttachInfo.mHardwareRenderer != null) && (!this.mAttachInfo.mHardwareRenderer.safelyRun(new Runnable()
        {
            public void run()
            {
                TextureView.this.destroySurface();
            }
        })))
            Log.w("TextureView", "TextureView was not able to destroy its surface: " + this);
    }

    protected final void onDraw(Canvas paramCanvas)
    {
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        if (this.mSurface != null)
        {
            nSetDefaultBufferSize(this.mSurface, getWidth(), getHeight());
            if (this.mListener != null)
                this.mListener.onSurfaceTextureSizeChanged(this.mSurface, getWidth(), getHeight());
        }
    }

    protected void onVisibilityChanged(View paramView, int paramInt)
    {
        super.onVisibilityChanged(paramView, paramInt);
        if (this.mSurface != null)
        {
            if (paramInt != 0)
                break label33;
            this.mSurface.setOnFrameAvailableListener(this.mUpdateListener);
            updateLayer();
        }
        while (true)
        {
            return;
            label33: this.mSurface.setOnFrameAvailableListener(null);
        }
    }

    public void setLayerType(int paramInt, Paint paramPaint)
    {
        if (paramPaint != this.mLayerPaint)
        {
            this.mLayerPaint = paramPaint;
            invalidate();
        }
    }

    public void setOpaque(boolean paramBoolean)
    {
        if (paramBoolean != this.mOpaque)
        {
            this.mOpaque = paramBoolean;
            if (this.mLayer != null)
                updateLayer();
        }
    }

    public void setSurfaceTexture(SurfaceTexture paramSurfaceTexture)
    {
        if (paramSurfaceTexture == null)
            throw new NullPointerException("surfaceTexture must not be null");
        if (this.mSurface != null)
            this.mSurface.release();
        this.mSurface = paramSurfaceTexture;
        this.mUpdateSurface = true;
        invalidateParentIfNeeded();
    }

    public void setSurfaceTextureListener(SurfaceTextureListener paramSurfaceTextureListener)
    {
        this.mListener = paramSurfaceTextureListener;
    }

    public void setTransform(Matrix paramMatrix)
    {
        this.mMatrix.set(paramMatrix);
        this.mMatrixChanged = true;
        invalidateParentIfNeeded();
    }

    public void unlockCanvasAndPost(Canvas paramCanvas)
    {
        if ((this.mCanvas != null) && (paramCanvas == this.mCanvas))
        {
            paramCanvas.restoreToCount(this.mSaveCount);
            this.mSaveCount = 0;
            synchronized (this.mNativeWindowLock)
            {
                nUnlockCanvasAndPost(this.mNativeWindow, this.mCanvas);
            }
        }
    }

    public static abstract interface SurfaceTextureListener
    {
        public abstract void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2);

        public abstract boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture);

        public abstract void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2);

        public abstract void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.TextureView
 * JD-Core Version:        0.6.2
 */