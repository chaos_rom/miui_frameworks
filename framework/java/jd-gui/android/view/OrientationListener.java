package android.view;

import android.content.Context;
import android.hardware.SensorListener;

@Deprecated
public abstract class OrientationListener
    implements SensorListener
{
    public static final int ORIENTATION_UNKNOWN = -1;
    private OrientationEventListener mOrientationEventLis;

    public OrientationListener(Context paramContext)
    {
        this.mOrientationEventLis = new OrientationEventListenerInternal(paramContext);
    }

    public OrientationListener(Context paramContext, int paramInt)
    {
        this.mOrientationEventLis = new OrientationEventListenerInternal(paramContext, paramInt);
    }

    public void disable()
    {
        this.mOrientationEventLis.disable();
    }

    public void enable()
    {
        this.mOrientationEventLis.enable();
    }

    public void onAccuracyChanged(int paramInt1, int paramInt2)
    {
    }

    public abstract void onOrientationChanged(int paramInt);

    public void onSensorChanged(int paramInt, float[] paramArrayOfFloat)
    {
    }

    class OrientationEventListenerInternal extends OrientationEventListener
    {
        OrientationEventListenerInternal(Context arg2)
        {
            super();
        }

        OrientationEventListenerInternal(Context paramInt, int arg3)
        {
            super(i);
            registerListener(OrientationListener.this);
        }

        public void onOrientationChanged(int paramInt)
        {
            OrientationListener.this.onOrientationChanged(paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.OrientationListener
 * JD-Core Version:        0.6.2
 */