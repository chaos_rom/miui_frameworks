package android.view;

import android.graphics.Rect;

public class Gravity
{
    public static final int AXIS_CLIP = 8;
    public static final int AXIS_PULL_AFTER = 4;
    public static final int AXIS_PULL_BEFORE = 2;
    public static final int AXIS_SPECIFIED = 1;
    public static final int AXIS_X_SHIFT = 0;
    public static final int AXIS_Y_SHIFT = 4;
    public static final int BOTTOM = 80;
    public static final int CENTER = 17;
    public static final int CENTER_HORIZONTAL = 1;
    public static final int CENTER_VERTICAL = 16;
    public static final int CLIP_HORIZONTAL = 8;
    public static final int CLIP_VERTICAL = 128;
    public static final int DISPLAY_CLIP_HORIZONTAL = 16777216;
    public static final int DISPLAY_CLIP_VERTICAL = 268435456;
    public static final int END = 8388613;
    public static final int FILL = 119;
    public static final int FILL_HORIZONTAL = 7;
    public static final int FILL_VERTICAL = 112;
    public static final int HORIZONTAL_GRAVITY_MASK = 7;
    public static final int LEFT = 3;
    public static final int NO_GRAVITY = 0;
    public static final int RELATIVE_HORIZONTAL_GRAVITY_MASK = 8388615;
    public static final int RELATIVE_LAYOUT_DIRECTION = 8388608;
    public static final int RIGHT = 5;
    public static final int START = 8388611;
    public static final int TOP = 48;
    public static final int VERTICAL_GRAVITY_MASK = 112;

    public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, int paramInt4, int paramInt5, Rect paramRect2)
    {
        switch (paramInt1 & 0x6)
        {
        case 1:
        case 3:
        default:
            paramRect2.left = (paramInt4 + paramRect1.left);
            paramRect2.right = (paramInt4 + paramRect1.right);
            switch (paramInt1 & 0x60)
            {
            default:
                paramRect2.top = (paramInt5 + paramRect1.top);
                paramRect2.bottom = (paramInt5 + paramRect1.bottom);
            case 0:
            case 32:
            case 64:
            }
            break;
        case 0:
        case 2:
        case 4:
        }
        while (true)
        {
            return;
            paramRect2.left = (paramInt4 + (paramRect1.left + (paramRect1.right - paramRect1.left - paramInt2) / 2));
            paramRect2.right = (paramInt2 + paramRect2.left);
            if ((paramInt1 & 0x8) != 8)
                break;
            if (paramRect2.left < paramRect1.left)
                paramRect2.left = paramRect1.left;
            if (paramRect2.right <= paramRect1.right)
                break;
            paramRect2.right = paramRect1.right;
            break;
            paramRect2.left = (paramInt4 + paramRect1.left);
            paramRect2.right = (paramInt2 + paramRect2.left);
            if (((paramInt1 & 0x8) != 8) || (paramRect2.right <= paramRect1.right))
                break;
            paramRect2.right = paramRect1.right;
            break;
            paramRect1.right -= paramInt4;
            paramRect2.left = (paramRect2.right - paramInt2);
            if (((paramInt1 & 0x8) != 8) || (paramRect2.left >= paramRect1.left))
                break;
            paramRect2.left = paramRect1.left;
            break;
            paramRect2.top = (paramInt5 + (paramRect1.top + (paramRect1.bottom - paramRect1.top - paramInt3) / 2));
            paramRect2.bottom = (paramInt3 + paramRect2.top);
            if ((paramInt1 & 0x80) == 128)
            {
                if (paramRect2.top < paramRect1.top)
                    paramRect2.top = paramRect1.top;
                if (paramRect2.bottom > paramRect1.bottom)
                {
                    paramRect2.bottom = paramRect1.bottom;
                    continue;
                    paramRect2.top = (paramInt5 + paramRect1.top);
                    paramRect2.bottom = (paramInt3 + paramRect2.top);
                    if (((paramInt1 & 0x80) == 128) && (paramRect2.bottom > paramRect1.bottom))
                    {
                        paramRect2.bottom = paramRect1.bottom;
                        continue;
                        paramRect1.bottom -= paramInt5;
                        paramRect2.top = (paramRect2.bottom - paramInt3);
                        if (((paramInt1 & 0x80) == 128) && (paramRect2.top < paramRect1.top))
                            paramRect2.top = paramRect1.top;
                    }
                }
            }
        }
    }

    public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, int paramInt4, int paramInt5, Rect paramRect2, int paramInt6)
    {
        apply(getAbsoluteGravity(paramInt1, paramInt6), paramInt2, paramInt3, paramRect1, paramInt4, paramInt5, paramRect2);
    }

    public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2)
    {
        apply(paramInt1, paramInt2, paramInt3, paramRect1, 0, 0, paramRect2);
    }

    public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, int paramInt4)
    {
        apply(getAbsoluteGravity(paramInt1, paramInt4), paramInt2, paramInt3, paramRect1, 0, 0, paramRect2);
    }

    public static void applyDisplay(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        if ((0x10000000 & paramInt) != 0)
        {
            if (paramRect2.top < paramRect1.top)
                paramRect2.top = paramRect1.top;
            if (paramRect2.bottom > paramRect1.bottom)
                paramRect2.bottom = paramRect1.bottom;
            if ((0x1000000 & paramInt) == 0)
                break label200;
            if (paramRect2.left < paramRect1.left)
                paramRect2.left = paramRect1.left;
            if (paramRect2.right > paramRect1.right)
                paramRect2.right = paramRect1.right;
        }
        while (true)
        {
            return;
            int i = 0;
            if (paramRect2.top < paramRect1.top)
                i = paramRect1.top - paramRect2.top;
            while (true)
            {
                if (i == 0)
                    break label175;
                if (paramRect2.height() <= paramRect1.bottom - paramRect1.top)
                    break label177;
                paramRect2.top = paramRect1.top;
                paramRect2.bottom = paramRect1.bottom;
                break;
                if (paramRect2.bottom > paramRect1.bottom)
                    i = paramRect1.bottom - paramRect2.bottom;
            }
            label175: break;
            label177: paramRect2.top = (i + paramRect2.top);
            paramRect2.bottom = (i + paramRect2.bottom);
            break;
            label200: int j = 0;
            if (paramRect2.left < paramRect1.left)
                j = paramRect1.left - paramRect2.left;
            while (true)
            {
                if (j == 0)
                    break label288;
                if (paramRect2.width() <= paramRect1.right - paramRect1.left)
                    break label290;
                paramRect2.left = paramRect1.left;
                paramRect2.right = paramRect1.right;
                break;
                if (paramRect2.right > paramRect1.right)
                    j = paramRect1.right - paramRect2.right;
            }
            label288: continue;
            label290: paramRect2.left = (j + paramRect2.left);
            paramRect2.right = (j + paramRect2.right);
        }
    }

    public static void applyDisplay(int paramInt1, Rect paramRect1, Rect paramRect2, int paramInt2)
    {
        applyDisplay(getAbsoluteGravity(paramInt1, paramInt2), paramRect1, paramRect2);
    }

    public static int getAbsoluteGravity(int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        int k;
        if ((0x800000 & i) > 0)
        {
            if ((i & 0x800003) != 8388611)
                break label49;
            k = i & 0xFF7FFFFC;
            if (paramInt2 != 1)
                break label41;
            i = k | 0x5;
        }
        while (true)
        {
            i &= -8388609;
            return i;
            label41: i = k | 0x3;
            continue;
            label49: if ((i & 0x800005) == 8388613)
            {
                int j = i & 0xFF7FFFFA;
                if (paramInt2 == 1)
                    i = j | 0x3;
                else
                    i = j | 0x5;
            }
        }
    }

    public static boolean isHorizontal(int paramInt)
    {
        if ((paramInt > 0) && ((0x800007 & paramInt) != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isVertical(int paramInt)
    {
        if ((paramInt > 0) && ((paramInt & 0x70) != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.Gravity
 * JD-Core Version:        0.6.2
 */