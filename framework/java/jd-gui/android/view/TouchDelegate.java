package android.view;

import android.graphics.Rect;

public class TouchDelegate
{
    public static final int ABOVE = 1;
    public static final int BELOW = 2;
    public static final int TO_LEFT = 4;
    public static final int TO_RIGHT = 8;
    private Rect mBounds;
    private boolean mDelegateTargeted;
    private View mDelegateView;
    private int mSlop;
    private Rect mSlopBounds;

    public TouchDelegate(Rect paramRect, View paramView)
    {
        this.mBounds = paramRect;
        this.mSlop = ViewConfiguration.get(paramView.getContext()).getScaledTouchSlop();
        this.mSlopBounds = new Rect(paramRect);
        this.mSlopBounds.inset(-this.mSlop, -this.mSlop);
        this.mDelegateView = paramView;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = (int)paramMotionEvent.getX();
        int j = (int)paramMotionEvent.getY();
        boolean bool1 = false;
        int k = 1;
        boolean bool2 = false;
        View localView;
        switch (paramMotionEvent.getAction())
        {
        default:
            if (bool1)
            {
                localView = this.mDelegateView;
                if (k == 0)
                    break label169;
                paramMotionEvent.setLocation(localView.getWidth() / 2, localView.getHeight() / 2);
            }
            break;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            bool2 = localView.dispatchTouchEvent(paramMotionEvent);
            return bool2;
            if (!this.mBounds.contains(i, j))
                break;
            this.mDelegateTargeted = true;
            bool1 = true;
            break;
            bool1 = this.mDelegateTargeted;
            if ((!bool1) || (this.mSlopBounds.contains(i, j)))
                break;
            k = 0;
            break;
            bool1 = this.mDelegateTargeted;
            this.mDelegateTargeted = false;
            break;
            label169: int m = this.mSlop;
            paramMotionEvent.setLocation(-(m * 2), -(m * 2));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.TouchDelegate
 * JD-Core Version:        0.6.2
 */