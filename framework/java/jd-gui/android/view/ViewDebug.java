package android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.os.Debug;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ViewDebug
{
    private static final int CAPTURE_TIMEOUT = 4000;
    public static final boolean DEBUG_DRAG = false;
    private static final String REMOTE_COMMAND_CAPTURE = "CAPTURE";
    private static final String REMOTE_COMMAND_CAPTURE_LAYERS = "CAPTURE_LAYERS";
    private static final String REMOTE_COMMAND_DUMP = "DUMP";
    private static final String REMOTE_COMMAND_INVALIDATE = "INVALIDATE";
    private static final String REMOTE_COMMAND_OUTPUT_DISPLAYLIST = "OUTPUT_DISPLAYLIST";
    private static final String REMOTE_COMMAND_REQUEST_LAYOUT = "REQUEST_LAYOUT";
    private static final String REMOTE_PROFILE = "PROFILE";

    @Deprecated
    public static final boolean TRACE_HIERARCHY;

    @Deprecated
    public static final boolean TRACE_RECYCLER;
    private static HashMap<Class<?>, Field[]> mCapturedViewFieldsForClasses = null;
    private static HashMap<Class<?>, Method[]> mCapturedViewMethodsForClasses = null;
    private static HashMap<AccessibleObject, ExportedProperty> sAnnotations;
    private static HashMap<Class<?>, Field[]> sFieldsForClasses;
    private static HashMap<Class<?>, Method[]> sMethodsForClasses;

    // ERROR //
    private static void capture(View paramView, OutputStream paramOutputStream, String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_2
        //     2: invokestatic 94	android/view/ViewDebug:findView	(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
        //     5: iconst_0
        //     6: invokestatic 98	android/view/ViewDebug:performViewCapture	(Landroid/view/View;Z)Landroid/graphics/Bitmap;
        //     9: astore_3
        //     10: aload_3
        //     11: ifnonnull +20 -> 31
        //     14: ldc 100
        //     16: ldc 102
        //     18: invokestatic 108	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     21: pop
        //     22: iconst_1
        //     23: iconst_1
        //     24: getstatic 114	android/graphics/Bitmap$Config:ARGB_8888	Landroid/graphics/Bitmap$Config;
        //     27: invokestatic 120	android/graphics/Bitmap:createBitmap	(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
        //     30: astore_3
        //     31: aconst_null
        //     32: astore 4
        //     34: new 122	java/io/BufferedOutputStream
        //     37: dup
        //     38: aload_1
        //     39: ldc 123
        //     41: invokespecial 126	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;I)V
        //     44: astore 5
        //     46: aload_3
        //     47: getstatic 132	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
        //     50: bipush 100
        //     52: aload 5
        //     54: invokevirtual 136	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
        //     57: pop
        //     58: aload 5
        //     60: invokevirtual 139	java/io/BufferedOutputStream:flush	()V
        //     63: aload 5
        //     65: ifnull +8 -> 73
        //     68: aload 5
        //     70: invokevirtual 142	java/io/BufferedOutputStream:close	()V
        //     73: aload_3
        //     74: invokevirtual 145	android/graphics/Bitmap:recycle	()V
        //     77: return
        //     78: astore 6
        //     80: aload 4
        //     82: ifnull +8 -> 90
        //     85: aload 4
        //     87: invokevirtual 142	java/io/BufferedOutputStream:close	()V
        //     90: aload_3
        //     91: invokevirtual 145	android/graphics/Bitmap:recycle	()V
        //     94: aload 6
        //     96: athrow
        //     97: astore 6
        //     99: aload 5
        //     101: astore 4
        //     103: goto -23 -> 80
        //
        // Exception table:
        //     from	to	target	type
        //     34	46	78	finally
        //     46	63	97	finally
    }

    // ERROR //
    private static void captureLayers(View paramView, DataOutputStream paramDataOutputStream)
        throws IOException
    {
        // Byte code:
        //     0: new 151	android/graphics/Rect
        //     3: dup
        //     4: invokespecial 152	android/graphics/Rect:<init>	()V
        //     7: astore_2
        //     8: aload_0
        //     9: getfield 158	android/view/View:mAttachInfo	Landroid/view/View$AttachInfo;
        //     12: getfield 164	android/view/View$AttachInfo:mSession	Landroid/view/IWindowSession;
        //     15: aload_0
        //     16: getfield 158	android/view/View:mAttachInfo	Landroid/view/View$AttachInfo;
        //     19: getfield 168	android/view/View$AttachInfo:mWindow	Landroid/view/IWindow;
        //     22: aload_2
        //     23: invokeinterface 174 3 0
        //     28: aload_1
        //     29: aload_2
        //     30: invokevirtual 178	android/graphics/Rect:width	()I
        //     33: invokevirtual 184	java/io/DataOutputStream:writeInt	(I)V
        //     36: aload_1
        //     37: aload_2
        //     38: invokevirtual 187	android/graphics/Rect:height	()I
        //     41: invokevirtual 184	java/io/DataOutputStream:writeInt	(I)V
        //     44: aload_0
        //     45: aload_1
        //     46: iconst_1
        //     47: invokestatic 191	android/view/ViewDebug:captureViewLayer	(Landroid/view/View;Ljava/io/DataOutputStream;Z)V
        //     50: aload_1
        //     51: iconst_2
        //     52: invokevirtual 194	java/io/DataOutputStream:write	(I)V
        //     55: aload_1
        //     56: invokevirtual 195	java/io/DataOutputStream:close	()V
        //     59: return
        //     60: astore 4
        //     62: aload_1
        //     63: invokevirtual 195	java/io/DataOutputStream:close	()V
        //     66: aload 4
        //     68: athrow
        //     69: astore_3
        //     70: goto -42 -> 28
        //
        // Exception table:
        //     from	to	target	type
        //     0	8	60	finally
        //     8	28	60	finally
        //     28	55	60	finally
        //     8	28	69	android/os/RemoteException
    }

    private static void captureViewLayer(View paramView, DataOutputStream paramDataOutputStream, boolean paramBoolean)
        throws IOException
    {
        boolean bool;
        if ((paramView.getVisibility() == 0) && (paramBoolean))
        {
            bool = true;
            if ((0x80 & paramView.mPrivateFlags) != 128)
            {
                int k = paramView.getId();
                String str = paramView.getClass().getSimpleName();
                if (k != -1)
                    str = resolveId(paramView.getContext(), k).toString();
                paramDataOutputStream.write(1);
                paramDataOutputStream.writeUTF(str);
                if (!bool)
                    break label237;
            }
        }
        label237: for (int m = 1; ; m = 0)
        {
            paramDataOutputStream.writeByte(m);
            int[] arrayOfInt = new int[2];
            paramView.getLocationInWindow(arrayOfInt);
            paramDataOutputStream.writeInt(arrayOfInt[0]);
            paramDataOutputStream.writeInt(arrayOfInt[1]);
            paramDataOutputStream.flush();
            Bitmap localBitmap = performViewCapture(paramView, true);
            if (localBitmap != null)
            {
                ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(2 * (localBitmap.getWidth() * localBitmap.getHeight()));
                localBitmap.compress(Bitmap.CompressFormat.PNG, 100, localByteArrayOutputStream);
                paramDataOutputStream.writeInt(localByteArrayOutputStream.size());
                localByteArrayOutputStream.writeTo(paramDataOutputStream);
            }
            paramDataOutputStream.flush();
            if (!(paramView instanceof ViewGroup))
                return;
            ViewGroup localViewGroup = (ViewGroup)paramView;
            int i = localViewGroup.getChildCount();
            for (int j = 0; j < i; j++)
                captureViewLayer(localViewGroup.getChildAt(j), paramDataOutputStream, bool);
            bool = false;
            break;
        }
    }

    private static String capturedViewExportFields(Object paramObject, Class<?> paramClass, String paramString)
    {
        String str;
        if (paramObject == null)
        {
            str = "null";
            return str;
        }
        StringBuilder localStringBuilder = new StringBuilder();
        Field[] arrayOfField = capturedViewGetPropertyFields(paramClass);
        int i = arrayOfField.length;
        for (int j = 0; ; j++)
            if (j < i)
            {
                Field localField = arrayOfField[j];
                try
                {
                    Object localObject = localField.get(paramObject);
                    localStringBuilder.append(paramString);
                    localStringBuilder.append(localField.getName());
                    localStringBuilder.append("=");
                    if (localObject != null)
                        localStringBuilder.append(localObject.toString().replace("\n", "\\n"));
                    while (true)
                    {
                        localStringBuilder.append(' ');
                        break;
                        localStringBuilder.append("null");
                    }
                }
                catch (IllegalAccessException localIllegalAccessException)
                {
                }
            }
            else
            {
                str = localStringBuilder.toString();
                break;
            }
    }

    private static String capturedViewExportMethods(Object paramObject, Class<?> paramClass, String paramString)
    {
        String str;
        if (paramObject == null)
            str = "null";
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder();
            Method[] arrayOfMethod = capturedViewGetPropertyMethods(paramClass);
            int i = arrayOfMethod.length;
            int j = 0;
            Method localMethod;
            if (j < i)
                localMethod = arrayOfMethod[j];
            try
            {
                Object localObject = localMethod.invoke(paramObject, (Object[])null);
                Class localClass = localMethod.getReturnType();
                if (((CapturedViewProperty)localMethod.getAnnotation(CapturedViewProperty.class)).retrieveReturn())
                {
                    localStringBuilder.append(capturedViewExportMethods(localObject, localClass, localMethod.getName() + "#"));
                }
                else
                {
                    localStringBuilder.append(paramString);
                    localStringBuilder.append(localMethod.getName());
                    localStringBuilder.append("()=");
                    if (localObject != null)
                        localStringBuilder.append(localObject.toString().replace("\n", "\\n"));
                    while (true)
                    {
                        localStringBuilder.append("; ");
                        break;
                        localStringBuilder.append("null");
                    }
                }
            }
            catch (InvocationTargetException localInvocationTargetException)
            {
                break label208;
                str = localStringBuilder.toString();
                continue;
                j++;
            }
            catch (IllegalAccessException localIllegalAccessException)
            {
                label208: break label208;
            }
        }
    }

    private static Field[] capturedViewGetPropertyFields(Class<?> paramClass)
    {
        if (mCapturedViewFieldsForClasses == null)
            mCapturedViewFieldsForClasses = new HashMap();
        HashMap localHashMap = mCapturedViewFieldsForClasses;
        Field[] arrayOfField1 = (Field[])localHashMap.get(paramClass);
        if (arrayOfField1 != null);
        Field[] arrayOfField3;
        for (Object localObject = arrayOfField1; ; localObject = arrayOfField3)
        {
            return localObject;
            ArrayList localArrayList = new ArrayList();
            for (Field localField : paramClass.getFields())
                if (localField.isAnnotationPresent(CapturedViewProperty.class))
                {
                    localField.setAccessible(true);
                    localArrayList.add(localField);
                }
            arrayOfField3 = (Field[])localArrayList.toArray(new Field[localArrayList.size()]);
            localHashMap.put(paramClass, arrayOfField3);
        }
    }

    private static Method[] capturedViewGetPropertyMethods(Class<?> paramClass)
    {
        if (mCapturedViewMethodsForClasses == null)
            mCapturedViewMethodsForClasses = new HashMap();
        HashMap localHashMap = mCapturedViewMethodsForClasses;
        Method[] arrayOfMethod1 = (Method[])localHashMap.get(paramClass);
        if (arrayOfMethod1 != null);
        Method[] arrayOfMethod3;
        for (Object localObject = arrayOfMethod1; ; localObject = arrayOfMethod3)
        {
            return localObject;
            ArrayList localArrayList = new ArrayList();
            for (Method localMethod : paramClass.getMethods())
                if ((localMethod.getParameterTypes().length == 0) && (localMethod.isAnnotationPresent(CapturedViewProperty.class)) && (localMethod.getReturnType() != Void.class))
                {
                    localMethod.setAccessible(true);
                    localArrayList.add(localMethod);
                }
            arrayOfMethod3 = (Method[])localArrayList.toArray(new Method[localArrayList.size()]);
            localHashMap.put(paramClass, arrayOfMethod3);
        }
    }

    static void dispatchCommand(View paramView, String paramString1, String paramString2, OutputStream paramOutputStream)
        throws IOException
    {
        View localView = paramView.getRootView();
        if ("DUMP".equalsIgnoreCase(paramString1))
            dump(localView, paramOutputStream);
        while (true)
        {
            return;
            if ("CAPTURE_LAYERS".equalsIgnoreCase(paramString1))
            {
                captureLayers(localView, new DataOutputStream(paramOutputStream));
            }
            else
            {
                String[] arrayOfString = paramString2.split(" ");
                if ("CAPTURE".equalsIgnoreCase(paramString1))
                    capture(localView, paramOutputStream, arrayOfString[0]);
                else if ("OUTPUT_DISPLAYLIST".equalsIgnoreCase(paramString1))
                    outputDisplayList(localView, arrayOfString[0]);
                else if ("INVALIDATE".equalsIgnoreCase(paramString1))
                    invalidate(localView, arrayOfString[0]);
                else if ("REQUEST_LAYOUT".equalsIgnoreCase(paramString1))
                    requestLayout(localView, arrayOfString[0]);
                else if ("PROFILE".equalsIgnoreCase(paramString1))
                    profile(localView, paramOutputStream, arrayOfString[0]);
            }
        }
    }

    // ERROR //
    private static void dump(View paramView, OutputStream paramOutputStream)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 431	java/io/BufferedWriter
        //     5: dup
        //     6: new 433	java/io/OutputStreamWriter
        //     9: dup
        //     10: aload_1
        //     11: ldc_w 435
        //     14: invokespecial 438	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/lang/String;)V
        //     17: ldc 123
        //     19: invokespecial 441	java/io/BufferedWriter:<init>	(Ljava/io/Writer;I)V
        //     22: astore_3
        //     23: aload_0
        //     24: invokevirtual 394	android/view/View:getRootView	()Landroid/view/View;
        //     27: astore 7
        //     29: aload 7
        //     31: instanceof 256
        //     34: ifeq +22 -> 56
        //     37: aload 7
        //     39: checkcast 256	android/view/ViewGroup
        //     42: astore 8
        //     44: aload 8
        //     46: invokevirtual 442	android/view/ViewGroup:getContext	()Landroid/content/Context;
        //     49: aload 8
        //     51: aload_3
        //     52: iconst_0
        //     53: invokestatic 446	android/view/ViewDebug:dumpViewHierarchyWithProperties	(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/io/BufferedWriter;I)V
        //     56: aload_3
        //     57: ldc_w 448
        //     60: invokevirtual 450	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     63: aload_3
        //     64: invokevirtual 453	java/io/BufferedWriter:newLine	()V
        //     67: aload_3
        //     68: ifnull +59 -> 127
        //     71: aload_3
        //     72: invokevirtual 454	java/io/BufferedWriter:close	()V
        //     75: return
        //     76: astore 4
        //     78: ldc 100
        //     80: ldc_w 456
        //     83: aload 4
        //     85: invokestatic 459	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     88: pop
        //     89: aload_2
        //     90: ifnull -15 -> 75
        //     93: aload_2
        //     94: invokevirtual 454	java/io/BufferedWriter:close	()V
        //     97: goto -22 -> 75
        //     100: astore 5
        //     102: aload_2
        //     103: ifnull +7 -> 110
        //     106: aload_2
        //     107: invokevirtual 454	java/io/BufferedWriter:close	()V
        //     110: aload 5
        //     112: athrow
        //     113: astore 5
        //     115: aload_3
        //     116: astore_2
        //     117: goto -15 -> 102
        //     120: astore 4
        //     122: aload_3
        //     123: astore_2
        //     124: goto -46 -> 78
        //     127: goto -52 -> 75
        //
        // Exception table:
        //     from	to	target	type
        //     2	23	76	java/lang/Exception
        //     2	23	100	finally
        //     78	89	100	finally
        //     23	67	113	finally
        //     23	67	120	java/lang/Exception
    }

    public static void dumpCapturedView(String paramString, Object paramObject)
    {
        Class localClass = paramObject.getClass();
        StringBuilder localStringBuilder = new StringBuilder(localClass.getName() + ": ");
        localStringBuilder.append(capturedViewExportFields(paramObject, localClass, ""));
        localStringBuilder.append(capturedViewExportMethods(paramObject, localClass, ""));
        Log.d(paramString, localStringBuilder.toString());
    }

    private static void dumpViewHierarchyWithProperties(Context paramContext, ViewGroup paramViewGroup, BufferedWriter paramBufferedWriter, int paramInt)
    {
        if (!dumpViewWithProperties(paramContext, paramViewGroup, paramBufferedWriter, paramInt))
            return;
        int i = paramViewGroup.getChildCount();
        int j = 0;
        label20: View localView;
        if (j < i)
        {
            localView = paramViewGroup.getChildAt(j);
            if (!(localView instanceof ViewGroup))
                break label62;
            dumpViewHierarchyWithProperties(paramContext, (ViewGroup)localView, paramBufferedWriter, paramInt + 1);
        }
        while (true)
        {
            j++;
            break label20;
            break;
            label62: dumpViewWithProperties(paramContext, localView, paramBufferedWriter, paramInt + 1);
        }
    }

    private static void dumpViewProperties(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter)
        throws IOException
    {
        dumpViewProperties(paramContext, paramObject, paramBufferedWriter, "");
    }

    private static void dumpViewProperties(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter, String paramString)
        throws IOException
    {
        Class localClass = paramObject.getClass();
        do
        {
            exportFields(paramContext, paramObject, paramBufferedWriter, localClass, paramString);
            exportMethods(paramContext, paramObject, paramBufferedWriter, localClass, paramString);
            localClass = localClass.getSuperclass();
        }
        while (localClass != Object.class);
    }

    private static boolean dumpViewWithProperties(Context paramContext, View paramView, BufferedWriter paramBufferedWriter, int paramInt)
    {
        int i = 0;
        while (true)
        {
            if (i < paramInt);
            try
            {
                paramBufferedWriter.write(32);
                i++;
                continue;
                paramBufferedWriter.write(paramView.getClass().getName());
                paramBufferedWriter.write(64);
                paramBufferedWriter.write(Integer.toHexString(paramView.hashCode()));
                paramBufferedWriter.write(32);
                dumpViewProperties(paramContext, paramView, paramBufferedWriter);
                paramBufferedWriter.newLine();
                bool = true;
                return bool;
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    Log.w("View", "Error while dumping hierarchy tree");
                    boolean bool = false;
                }
            }
        }
    }

    private static void exportFields(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter, Class<?> paramClass, String paramString)
        throws IOException
    {
        Field[] arrayOfField = getExportedPropertyFields(paramClass);
        int i = arrayOfField.length;
        int j = 0;
        if (j < i)
        {
            Field localField = arrayOfField[j];
            Object localObject = null;
            while (true)
            {
                int n;
                try
                {
                    Class localClass = localField.getType();
                    ExportedProperty localExportedProperty = (ExportedProperty)sAnnotations.get(localField);
                    if (localExportedProperty.category().length() == 0)
                        break label448;
                    str = localExportedProperty.category() + ":";
                    if (localClass == Integer.TYPE)
                    {
                        if ((localExportedProperty.resolveId()) && (paramContext != null))
                        {
                            localObject = resolveId(paramContext, localField.getInt(paramObject));
                            if (localObject == null)
                                localObject = localField.get(paramObject);
                            writeEntry(paramBufferedWriter, str + paramString, localField.getName(), "", localObject);
                        }
                        else
                        {
                            FlagToString[] arrayOfFlagToString = localExportedProperty.flagMapping();
                            if (arrayOfFlagToString.length > 0)
                                exportUnrolledFlags(paramBufferedWriter, arrayOfFlagToString, localField.getInt(paramObject), str + paramString + localField.getName() + '_');
                            IntToString[] arrayOfIntToString = localExportedProperty.mapping();
                            if (arrayOfIntToString.length <= 0)
                                continue;
                            int k = localField.getInt(paramObject);
                            int m = arrayOfIntToString.length;
                            n = 0;
                            if (n < m)
                            {
                                IntToString localIntToString = arrayOfIntToString[n];
                                if (localIntToString.from() != k)
                                    break label456;
                                localObject = localIntToString.to();
                            }
                            if (localObject != null)
                                continue;
                            localObject = Integer.valueOf(k);
                            continue;
                        }
                    }
                    else
                    {
                        if (localClass == [I.class)
                        {
                            exportUnrolledArray(paramContext, paramBufferedWriter, localExportedProperty, (int[])localField.get(paramObject), str + paramString + localField.getName() + '_', "");
                            return;
                        }
                        if ((localClass.isPrimitive()) || (!localExportedProperty.deepExport()))
                            continue;
                        dumpViewProperties(paramContext, localField.get(paramObject), paramBufferedWriter, paramString + localExportedProperty.prefix());
                    }
                }
                catch (IllegalAccessException localIllegalAccessException)
                {
                }
                j++;
                break;
                label448: String str = "";
                continue;
                label456: n++;
            }
        }
    }

    private static void exportMethods(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter, Class<?> paramClass, String paramString)
        throws IOException
    {
        Method[] arrayOfMethod = getExportedPropertyMethods(paramClass);
        int i = arrayOfMethod.length;
        int j = 0;
        if (j < i)
        {
            Method localMethod = arrayOfMethod[j];
            while (true)
            {
                int i1;
                try
                {
                    Object localObject = localMethod.invoke(paramObject, (Object[])null);
                    Class localClass = localMethod.getReturnType();
                    ExportedProperty localExportedProperty = (ExportedProperty)sAnnotations.get(localMethod);
                    if (localExportedProperty.category().length() == 0)
                        break label457;
                    str1 = localExportedProperty.category() + ":";
                    if (localClass == Integer.TYPE)
                    {
                        if ((localExportedProperty.resolveId()) && (paramContext != null))
                        {
                            localObject = resolveId(paramContext, ((Integer)localObject).intValue());
                            writeEntry(paramBufferedWriter, str1 + paramString, localMethod.getName(), "()", localObject);
                        }
                        else
                        {
                            FlagToString[] arrayOfFlagToString = localExportedProperty.flagMapping();
                            if (arrayOfFlagToString.length > 0)
                                exportUnrolledFlags(paramBufferedWriter, arrayOfFlagToString, ((Integer)localObject).intValue(), str1 + paramString + localMethod.getName() + '_');
                            IntToString[] arrayOfIntToString = localExportedProperty.mapping();
                            if (arrayOfIntToString.length <= 0)
                                continue;
                            int k = ((Integer)localObject).intValue();
                            int m = 0;
                            int n = arrayOfIntToString.length;
                            i1 = 0;
                            if (i1 < n)
                            {
                                IntToString localIntToString = arrayOfIntToString[i1];
                                if (localIntToString.from() != k)
                                    break label465;
                                localObject = localIntToString.to();
                                m = 1;
                            }
                            if (m != 0)
                                continue;
                            localObject = Integer.valueOf(k);
                            continue;
                        }
                    }
                    else
                    {
                        if (localClass == [I.class)
                        {
                            exportUnrolledArray(paramContext, paramBufferedWriter, localExportedProperty, (int[])localObject, str1 + paramString + localMethod.getName() + '_', "()");
                            return;
                        }
                        if ((localClass.isPrimitive()) || (!localExportedProperty.deepExport()))
                            continue;
                        String str2 = paramString + localExportedProperty.prefix();
                        dumpViewProperties(paramContext, localObject, paramBufferedWriter, str2);
                    }
                }
                catch (IllegalAccessException localIllegalAccessException)
                {
                }
                catch (InvocationTargetException localInvocationTargetException)
                {
                }
                j++;
                break;
                label457: String str1 = "";
                continue;
                label465: i1++;
            }
        }
    }

    private static void exportUnrolledArray(Context paramContext, BufferedWriter paramBufferedWriter, ExportedProperty paramExportedProperty, int[] paramArrayOfInt, String paramString1, String paramString2)
        throws IOException
    {
        IntToString[] arrayOfIntToString1 = paramExportedProperty.indexMapping();
        int i;
        int j;
        label34: int k;
        label50: int n;
        label57: int i1;
        String str2;
        int i5;
        label93: int i3;
        if (arrayOfIntToString1.length > 0)
        {
            i = 1;
            IntToString[] arrayOfIntToString2 = paramExportedProperty.mapping();
            if (arrayOfIntToString2.length <= 0)
                break label221;
            j = 1;
            if ((!paramExportedProperty.resolveId()) || (paramContext == null))
                break label227;
            k = 1;
            int m = paramArrayOfInt.length;
            n = 0;
            if (n >= m)
                return;
            str1 = null;
            i1 = paramArrayOfInt[n];
            str2 = String.valueOf(n);
            if (i != 0)
            {
                int i4 = arrayOfIntToString1.length;
                i5 = 0;
                if (i5 < i4)
                {
                    IntToString localIntToString2 = arrayOfIntToString1[i5];
                    if (localIntToString2.from() != n)
                        break label233;
                    str2 = localIntToString2.to();
                }
            }
            if (j != 0)
            {
                int i2 = arrayOfIntToString2.length;
                i3 = 0;
                label141: if (i3 < i2)
                {
                    IntToString localIntToString1 = arrayOfIntToString2[i3];
                    if (localIntToString1.from() != i1)
                        break label239;
                    str1 = localIntToString1.to();
                }
            }
            if (k == 0)
                break label245;
            if (str1 != null);
        }
        label221: label227: label233: label239: label245: for (String str1 = (String)resolveId(paramContext, i1); ; str1 = String.valueOf(i1))
        {
            writeEntry(paramBufferedWriter, paramString1, str2, paramString2, str1);
            n++;
            break label57;
            i = 0;
            break;
            j = 0;
            break label34;
            k = 0;
            break label50;
            i5++;
            break label93;
            i3++;
            break label141;
        }
    }

    private static void exportUnrolledFlags(BufferedWriter paramBufferedWriter, FlagToString[] paramArrayOfFlagToString, int paramInt, String paramString)
        throws IOException
    {
        int i = paramArrayOfFlagToString.length;
        int j = 0;
        if (j < i)
        {
            FlagToString localFlagToString = paramArrayOfFlagToString[j];
            boolean bool = localFlagToString.outputIf();
            int k = paramInt & localFlagToString.mask();
            if (k == localFlagToString.equals());
            for (int m = 1; ; m = 0)
            {
                if (((m != 0) && (bool)) || ((m == 0) && (!bool)))
                    writeEntry(paramBufferedWriter, paramString, localFlagToString.name(), "", "0x" + Integer.toHexString(k));
                j++;
                break;
            }
        }
    }

    private static View findView(View paramView, String paramString)
    {
        View localView1 = null;
        String str;
        int j;
        View localView2;
        if (paramString.indexOf('@') != -1)
        {
            String[] arrayOfString = paramString.split("@");
            str = arrayOfString[0];
            j = (int)Long.parseLong(arrayOfString[1], 16);
            localView2 = paramView.getRootView();
            if (!(localView2 instanceof ViewGroup));
        }
        int i;
        for (localView1 = findView((ViewGroup)localView2, str, j); ; localView1 = paramView.getRootView().findViewById(i))
        {
            return localView1;
            i = paramView.getResources().getIdentifier(paramString, null, null);
        }
    }

    private static View findView(ViewGroup paramViewGroup, String paramString, int paramInt)
    {
        if (isRequestedView(paramViewGroup, paramString, paramInt));
        while (true)
        {
            return paramViewGroup;
            int i = paramViewGroup.getChildCount();
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label86;
                View localView1 = paramViewGroup.getChildAt(j);
                if ((localView1 instanceof ViewGroup))
                {
                    View localView2 = findView((ViewGroup)localView1, paramString, paramInt);
                    if (localView2 == null)
                        continue;
                    paramViewGroup = localView2;
                    break;
                }
                if (isRequestedView(localView1, paramString, paramInt))
                {
                    paramViewGroup = localView1;
                    break;
                }
            }
            label86: paramViewGroup = null;
        }
    }

    private static Field[] getExportedPropertyFields(Class<?> paramClass)
    {
        if (sFieldsForClasses == null)
            sFieldsForClasses = new HashMap();
        if (sAnnotations == null)
            sAnnotations = new HashMap(512);
        HashMap localHashMap = sFieldsForClasses;
        Field[] arrayOfField1 = (Field[])localHashMap.get(paramClass);
        if (arrayOfField1 != null);
        Field[] arrayOfField3;
        for (Object localObject = arrayOfField1; ; localObject = arrayOfField3)
        {
            return localObject;
            ArrayList localArrayList = new ArrayList();
            for (Field localField : paramClass.getDeclaredFields())
                if (localField.isAnnotationPresent(ExportedProperty.class))
                {
                    localField.setAccessible(true);
                    localArrayList.add(localField);
                    sAnnotations.put(localField, localField.getAnnotation(ExportedProperty.class));
                }
            arrayOfField3 = (Field[])localArrayList.toArray(new Field[localArrayList.size()]);
            localHashMap.put(paramClass, arrayOfField3);
        }
    }

    private static Method[] getExportedPropertyMethods(Class<?> paramClass)
    {
        if (sMethodsForClasses == null)
            sMethodsForClasses = new HashMap(100);
        if (sAnnotations == null)
            sAnnotations = new HashMap(512);
        HashMap localHashMap = sMethodsForClasses;
        Method[] arrayOfMethod1 = (Method[])localHashMap.get(paramClass);
        if (arrayOfMethod1 != null);
        Method[] arrayOfMethod3;
        for (Object localObject = arrayOfMethod1; ; localObject = arrayOfMethod3)
        {
            return localObject;
            ArrayList localArrayList = new ArrayList();
            for (Method localMethod : paramClass.getDeclaredMethods())
                if ((localMethod.getParameterTypes().length == 0) && (localMethod.isAnnotationPresent(ExportedProperty.class)) && (localMethod.getReturnType() != Void.class))
                {
                    localMethod.setAccessible(true);
                    localArrayList.add(localMethod);
                    sAnnotations.put(localMethod, localMethod.getAnnotation(ExportedProperty.class));
                }
            arrayOfMethod3 = (Method[])localArrayList.toArray(new Method[localArrayList.size()]);
            localHashMap.put(paramClass, arrayOfMethod3);
        }
    }

    public static long getViewInstanceCount()
    {
        return Debug.countInstancesOfClass(View.class);
    }

    public static long getViewRootImplCount()
    {
        return Debug.countInstancesOfClass(ViewRootImpl.class);
    }

    private static void invalidate(View paramView, String paramString)
    {
        View localView = findView(paramView, paramString);
        if (localView != null)
            localView.postInvalidate();
    }

    private static boolean isRequestedView(View paramView, String paramString, int paramInt)
    {
        if ((paramView.getClass().getName().equals(paramString)) && (paramView.hashCode() == paramInt));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static void outputDisplayList(View paramView, String paramString)
        throws IOException
    {
        View localView = findView(paramView, paramString);
        localView.getViewRootImpl().outputDisplayList(localView);
    }

    private static Bitmap performViewCapture(final View paramView, final boolean paramBoolean)
    {
        final CountDownLatch localCountDownLatch;
        Bitmap[] arrayOfBitmap;
        if (paramView != null)
        {
            localCountDownLatch = new CountDownLatch(1);
            arrayOfBitmap = new Bitmap[1];
            paramView.post(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        ViewDebug.this[0] = paramView.createSnapshot(Bitmap.Config.ARGB_8888, 0, paramBoolean);
                        return;
                    }
                    catch (OutOfMemoryError localOutOfMemoryError)
                    {
                        while (true)
                        {
                            Log.w("View", "Out of memory for bitmap");
                            localCountDownLatch.countDown();
                        }
                    }
                    finally
                    {
                        localCountDownLatch.countDown();
                    }
                }
            });
        }
        while (true)
        {
            try
            {
                localCountDownLatch.await(4000L, TimeUnit.MILLISECONDS);
                localBitmap = arrayOfBitmap[0];
                return localBitmap;
            }
            catch (InterruptedException localInterruptedException)
            {
                Log.w("View", "Could not complete the capture of the view " + paramView);
                Thread.currentThread().interrupt();
            }
            Bitmap localBitmap = null;
        }
    }

    // ERROR //
    private static void profile(View paramView, OutputStream paramOutputStream, String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_2
        //     2: invokestatic 94	android/view/ViewDebug:findView	(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
        //     5: astore_3
        //     6: aconst_null
        //     7: astore 4
        //     9: new 431	java/io/BufferedWriter
        //     12: dup
        //     13: new 433	java/io/OutputStreamWriter
        //     16: dup
        //     17: aload_1
        //     18: invokespecial 706	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     21: ldc 123
        //     23: invokespecial 441	java/io/BufferedWriter:<init>	(Ljava/io/Writer;I)V
        //     26: astore 5
        //     28: aload_3
        //     29: ifnull +33 -> 62
        //     32: aload_3
        //     33: aload 5
        //     35: invokestatic 710	android/view/ViewDebug:profileViewAndChildren	(Landroid/view/View;Ljava/io/BufferedWriter;)V
        //     38: aload 5
        //     40: ldc_w 448
        //     43: invokevirtual 450	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     46: aload 5
        //     48: invokevirtual 453	java/io/BufferedWriter:newLine	()V
        //     51: aload 5
        //     53: ifnull +84 -> 137
        //     56: aload 5
        //     58: invokevirtual 454	java/io/BufferedWriter:close	()V
        //     61: return
        //     62: aload 5
        //     64: ldc_w 712
        //     67: invokevirtual 450	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     70: aload 5
        //     72: invokevirtual 453	java/io/BufferedWriter:newLine	()V
        //     75: goto -37 -> 38
        //     78: astore 7
        //     80: aload 5
        //     82: astore 4
        //     84: ldc 100
        //     86: ldc_w 714
        //     89: aload 7
        //     91: invokestatic 459	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     94: pop
        //     95: aload 4
        //     97: ifnull -36 -> 61
        //     100: aload 4
        //     102: invokevirtual 454	java/io/BufferedWriter:close	()V
        //     105: goto -44 -> 61
        //     108: astore 6
        //     110: aload 4
        //     112: ifnull +8 -> 120
        //     115: aload 4
        //     117: invokevirtual 454	java/io/BufferedWriter:close	()V
        //     120: aload 6
        //     122: athrow
        //     123: astore 6
        //     125: aload 5
        //     127: astore 4
        //     129: goto -19 -> 110
        //     132: astore 7
        //     134: goto -50 -> 84
        //     137: goto -76 -> 61
        //
        // Exception table:
        //     from	to	target	type
        //     32	51	78	java/lang/Exception
        //     62	75	78	java/lang/Exception
        //     9	28	108	finally
        //     84	95	108	finally
        //     32	51	123	finally
        //     62	75	123	finally
        //     9	28	132	java/lang/Exception
    }

    private static void profileViewAndChildren(View paramView, BufferedWriter paramBufferedWriter)
        throws IOException
    {
        profileViewAndChildren(paramView, paramBufferedWriter, true);
    }

    private static void profileViewAndChildren(View paramView, BufferedWriter paramBufferedWriter, boolean paramBoolean)
        throws IOException
    {
        long l1 = 0L;
        long l2;
        if ((paramBoolean) || ((0x800 & paramView.mPrivateFlags) != 0))
        {
            l2 = profileViewOperation(paramView, new ViewOperation()
            {
                private void forceLayout(View paramAnonymousView)
                {
                    paramAnonymousView.forceLayout();
                    if ((paramAnonymousView instanceof ViewGroup))
                    {
                        ViewGroup localViewGroup = (ViewGroup)paramAnonymousView;
                        int i = localViewGroup.getChildCount();
                        for (int j = 0; j < i; j++)
                            forceLayout(localViewGroup.getChildAt(j));
                    }
                }

                public void post(Void[] paramAnonymousArrayOfVoid)
                {
                }

                public Void[] pre()
                {
                    forceLayout(ViewDebug.this);
                    return null;
                }

                public void run(Void[] paramAnonymousArrayOfVoid)
                {
                    ViewDebug.this.measure(ViewDebug.this.mOldWidthMeasureSpec, ViewDebug.this.mOldHeightMeasureSpec);
                }
            });
            if ((!paramBoolean) && ((0x2000 & paramView.mPrivateFlags) == 0))
                break label190;
        }
        label190: for (long l3 = profileViewOperation(paramView, new ViewOperation()
        {
            public void post(Void[] paramAnonymousArrayOfVoid)
            {
            }

            public Void[] pre()
            {
                return null;
            }

            public void run(Void[] paramAnonymousArrayOfVoid)
            {
                ViewDebug.this.layout(ViewDebug.this.mLeft, ViewDebug.this.mTop, ViewDebug.this.mRight, ViewDebug.this.mBottom);
            }
        }); ; l3 = l1)
        {
            if ((paramBoolean) || (!paramView.willNotDraw()) || ((0x20 & paramView.mPrivateFlags) != 0))
                l1 = profileViewOperation(paramView, new ViewOperation()
                {
                    public void post(Object[] paramAnonymousArrayOfObject)
                    {
                        if (paramAnonymousArrayOfObject[1] != null)
                            ((Canvas)paramAnonymousArrayOfObject[1]).setBitmap(null);
                        if (paramAnonymousArrayOfObject[0] != null)
                            ((Bitmap)paramAnonymousArrayOfObject[0]).recycle();
                    }

                    public Object[] pre()
                    {
                        Canvas localCanvas = null;
                        DisplayMetrics localDisplayMetrics;
                        if ((ViewDebug.this != null) && (ViewDebug.this.getResources() != null))
                        {
                            localDisplayMetrics = ViewDebug.this.getResources().getDisplayMetrics();
                            if (localDisplayMetrics == null)
                                break label86;
                        }
                        label86: for (Bitmap localBitmap = Bitmap.createBitmap(localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels, Bitmap.Config.RGB_565); ; localBitmap = null)
                        {
                            if (localBitmap != null)
                                localCanvas = new Canvas(localBitmap);
                            Object[] arrayOfObject = new Object[2];
                            arrayOfObject[0] = localBitmap;
                            arrayOfObject[1] = localCanvas;
                            return arrayOfObject;
                            localDisplayMetrics = null;
                            break;
                        }
                    }

                    public void run(Object[] paramAnonymousArrayOfObject)
                    {
                        if (paramAnonymousArrayOfObject[1] != null)
                            ViewDebug.this.draw((Canvas)paramAnonymousArrayOfObject[1]);
                    }
                });
            paramBufferedWriter.write(String.valueOf(l2));
            paramBufferedWriter.write(32);
            paramBufferedWriter.write(String.valueOf(l3));
            paramBufferedWriter.write(32);
            paramBufferedWriter.write(String.valueOf(l1));
            paramBufferedWriter.newLine();
            if (!(paramView instanceof ViewGroup))
                return;
            ViewGroup localViewGroup = (ViewGroup)paramView;
            int i = localViewGroup.getChildCount();
            for (int j = 0; j < i; j++)
                profileViewAndChildren(localViewGroup.getChildAt(j), paramBufferedWriter, false);
            l2 = l1;
            break;
        }
    }

    private static <T> long profileViewOperation(View paramView, ViewOperation<T> paramViewOperation)
    {
        long l = -1L;
        final CountDownLatch localCountDownLatch = new CountDownLatch(1);
        final long[] arrayOfLong = new long[1];
        paramView.post(new Runnable()
        {
            public void run()
            {
                try
                {
                    Object[] arrayOfObject = ViewDebug.this.pre();
                    long l = Debug.threadCpuTimeNanos();
                    ViewDebug.this.run(arrayOfObject);
                    arrayOfLong[0] = (Debug.threadCpuTimeNanos() - l);
                    ViewDebug.this.post(arrayOfObject);
                    return;
                }
                finally
                {
                    localCountDownLatch.countDown();
                }
            }
        });
        try
        {
            if (!localCountDownLatch.await(4000L, TimeUnit.MILLISECONDS))
            {
                Log.w("View", "Could not complete the profiling of the view " + paramView);
                return l;
            }
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
            {
                Log.w("View", "Could not complete the profiling of the view " + paramView);
                Thread.currentThread().interrupt();
                continue;
                l = arrayOfLong[0];
            }
        }
    }

    private static void requestLayout(View paramView, String paramString)
    {
        View localView = findView(paramView, paramString);
        if (localView != null)
            paramView.post(new Runnable()
            {
                public void run()
                {
                    ViewDebug.this.requestLayout();
                }
            });
    }

    static Object resolveId(Context paramContext, int paramInt)
    {
        Resources localResources = paramContext.getResources();
        if (paramInt >= 0);
        while (true)
        {
            try
            {
                String str2 = localResources.getResourceTypeName(paramInt) + '/' + localResources.getResourceEntryName(paramInt);
                str1 = str2;
                return str1;
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                str1 = "id/0x" + Integer.toHexString(paramInt);
                continue;
            }
            String str1 = "NO_ID";
        }
    }

    @Deprecated
    public static void startHierarchyTracing(String paramString, View paramView)
    {
    }

    @Deprecated
    public static void startRecyclerTracing(String paramString, View paramView)
    {
    }

    @Deprecated
    public static void stopHierarchyTracing()
    {
    }

    @Deprecated
    public static void stopRecyclerTracing()
    {
    }

    @Deprecated
    public static void trace(View paramView, HierarchyTraceType paramHierarchyTraceType)
    {
    }

    @Deprecated
    public static void trace(View paramView, RecyclerTraceType paramRecyclerTraceType, int[] paramArrayOfInt)
    {
    }

    private static void writeEntry(BufferedWriter paramBufferedWriter, String paramString1, String paramString2, String paramString3, Object paramObject)
        throws IOException
    {
        paramBufferedWriter.write(paramString1);
        paramBufferedWriter.write(paramString2);
        paramBufferedWriter.write(paramString3);
        paramBufferedWriter.write("=");
        writeValue(paramBufferedWriter, paramObject);
        paramBufferedWriter.write(32);
    }

    private static void writeValue(BufferedWriter paramBufferedWriter, Object paramObject)
        throws IOException
    {
        if (paramObject != null)
        {
            String str = paramObject.toString().replace("\n", "\\n");
            paramBufferedWriter.write(String.valueOf(str.length()));
            paramBufferedWriter.write(",");
            paramBufferedWriter.write(str);
        }
        while (true)
        {
            return;
            paramBufferedWriter.write("4,null");
        }
    }

    static abstract interface ViewOperation<T>
    {
        public abstract void post(T[] paramArrayOfT);

        public abstract T[] pre();

        public abstract void run(T[] paramArrayOfT);
    }

    @Deprecated
    public static enum RecyclerTraceType
    {
        static
        {
            BIND_VIEW = new RecyclerTraceType("BIND_VIEW", 1);
            RECYCLE_FROM_ACTIVE_HEAP = new RecyclerTraceType("RECYCLE_FROM_ACTIVE_HEAP", 2);
            RECYCLE_FROM_SCRAP_HEAP = new RecyclerTraceType("RECYCLE_FROM_SCRAP_HEAP", 3);
            MOVE_TO_SCRAP_HEAP = new RecyclerTraceType("MOVE_TO_SCRAP_HEAP", 4);
            MOVE_FROM_ACTIVE_TO_SCRAP_HEAP = new RecyclerTraceType("MOVE_FROM_ACTIVE_TO_SCRAP_HEAP", 5);
            RecyclerTraceType[] arrayOfRecyclerTraceType = new RecyclerTraceType[6];
            arrayOfRecyclerTraceType[0] = NEW_VIEW;
            arrayOfRecyclerTraceType[1] = BIND_VIEW;
            arrayOfRecyclerTraceType[2] = RECYCLE_FROM_ACTIVE_HEAP;
            arrayOfRecyclerTraceType[3] = RECYCLE_FROM_SCRAP_HEAP;
            arrayOfRecyclerTraceType[4] = MOVE_TO_SCRAP_HEAP;
            arrayOfRecyclerTraceType[5] = MOVE_FROM_ACTIVE_TO_SCRAP_HEAP;
        }
    }

    @Deprecated
    public static enum HierarchyTraceType
    {
        static
        {
            ON_LAYOUT = new HierarchyTraceType("ON_LAYOUT", 4);
            ON_MEASURE = new HierarchyTraceType("ON_MEASURE", 5);
            DRAW = new HierarchyTraceType("DRAW", 6);
            BUILD_CACHE = new HierarchyTraceType("BUILD_CACHE", 7);
            HierarchyTraceType[] arrayOfHierarchyTraceType = new HierarchyTraceType[8];
            arrayOfHierarchyTraceType[0] = INVALIDATE;
            arrayOfHierarchyTraceType[1] = INVALIDATE_CHILD;
            arrayOfHierarchyTraceType[2] = INVALIDATE_CHILD_IN_PARENT;
            arrayOfHierarchyTraceType[3] = REQUEST_LAYOUT;
            arrayOfHierarchyTraceType[4] = ON_LAYOUT;
            arrayOfHierarchyTraceType[5] = ON_MEASURE;
            arrayOfHierarchyTraceType[6] = DRAW;
            arrayOfHierarchyTraceType[7] = BUILD_CACHE;
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({java.lang.annotation.ElementType.FIELD, java.lang.annotation.ElementType.METHOD})
    public static @interface CapturedViewProperty
    {
        public abstract boolean retrieveReturn();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({java.lang.annotation.ElementType.TYPE})
    public static @interface FlagToString
    {
        public abstract int equals();

        public abstract int mask();

        public abstract String name();

        public abstract boolean outputIf();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({java.lang.annotation.ElementType.TYPE})
    public static @interface IntToString
    {
        public abstract int from();

        public abstract String to();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({java.lang.annotation.ElementType.FIELD, java.lang.annotation.ElementType.METHOD})
    public static @interface ExportedProperty
    {
        public abstract String category();

        public abstract boolean deepExport();

        public abstract ViewDebug.FlagToString[] flagMapping();

        public abstract ViewDebug.IntToString[] indexMapping();

        public abstract ViewDebug.IntToString[] mapping();

        public abstract String prefix();

        public abstract boolean resolveId();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewDebug
 * JD-Core Version:        0.6.2
 */