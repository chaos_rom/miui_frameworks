package android.view.inputmethod;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Editable.Factory;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.MetaKeyKeyListener;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewRootImpl;

public class BaseInputConnection
    implements InputConnection
{
    static final Object COMPOSING = new ComposingText();
    private static final boolean DEBUG = false;
    private static final String TAG = "BaseInputConnection";
    private Object[] mDefaultComposingSpans;
    final boolean mDummyMode;
    Editable mEditable;
    protected final InputMethodManager mIMM;
    KeyCharacterMap mKeyCharacterMap;
    final View mTargetView;

    public BaseInputConnection(View paramView, boolean paramBoolean)
    {
        this.mIMM = ((InputMethodManager)paramView.getContext().getSystemService("input_method"));
        this.mTargetView = paramView;
        if (!paramBoolean);
        for (boolean bool = true; ; bool = false)
        {
            this.mDummyMode = bool;
            return;
        }
    }

    BaseInputConnection(InputMethodManager paramInputMethodManager, boolean paramBoolean)
    {
        this.mIMM = paramInputMethodManager;
        this.mTargetView = null;
        if (!paramBoolean);
        for (boolean bool = true; ; bool = false)
        {
            this.mDummyMode = bool;
            return;
        }
    }

    private void ensureDefaultComposingSpans()
    {
        Context localContext;
        if (this.mDefaultComposingSpans == null)
        {
            if (this.mTargetView == null)
                break label98;
            localContext = this.mTargetView.getContext();
        }
        while (true)
        {
            if (localContext != null)
            {
                Resources.Theme localTheme = localContext.getTheme();
                int[] arrayOfInt = new int[1];
                arrayOfInt[0] = 16843312;
                TypedArray localTypedArray = localTheme.obtainStyledAttributes(arrayOfInt);
                CharSequence localCharSequence = localTypedArray.getText(0);
                localTypedArray.recycle();
                if ((localCharSequence != null) && ((localCharSequence instanceof Spanned)))
                    this.mDefaultComposingSpans = ((Spanned)localCharSequence).getSpans(0, localCharSequence.length(), Object.class);
            }
            return;
            label98: if (this.mIMM.mServedView != null)
                localContext = this.mIMM.mServedView.getContext();
            else
                localContext = null;
        }
    }

    public static int getComposingSpanEnd(Spannable paramSpannable)
    {
        return paramSpannable.getSpanEnd(COMPOSING);
    }

    public static int getComposingSpanStart(Spannable paramSpannable)
    {
        return paramSpannable.getSpanStart(COMPOSING);
    }

    public static final void removeComposingSpans(Spannable paramSpannable)
    {
        paramSpannable.removeSpan(COMPOSING);
        Object[] arrayOfObject = paramSpannable.getSpans(0, paramSpannable.length(), Object.class);
        if (arrayOfObject != null)
            for (int i = -1 + arrayOfObject.length; i >= 0; i--)
            {
                Object localObject = arrayOfObject[i];
                if ((0x100 & paramSpannable.getSpanFlags(localObject)) != 0)
                    paramSpannable.removeSpan(localObject);
            }
    }

    private void replaceText(CharSequence paramCharSequence, int paramInt, boolean paramBoolean)
    {
        Editable localEditable = getEditable();
        if (localEditable == null)
            return;
        beginBatchEdit();
        int i = getComposingSpanStart(localEditable);
        int j = getComposingSpanEnd(localEditable);
        if (j < i)
        {
            int i1 = i;
            i = j;
            j = i1;
        }
        if ((i != -1) && (j != -1))
            removeComposingSpans(localEditable);
        while (paramBoolean)
        {
            Object localObject;
            if (!(paramCharSequence instanceof Spannable))
            {
                localObject = new SpannableStringBuilder(paramCharSequence);
                paramCharSequence = (CharSequence)localObject;
                ensureDefaultComposingSpans();
                if (this.mDefaultComposingSpans != null)
                {
                    for (int n = 0; n < this.mDefaultComposingSpans.length; n++)
                        ((Spannable)localObject).setSpan(this.mDefaultComposingSpans[n], 0, ((Spannable)localObject).length(), 289);
                    i = Selection.getSelectionStart(localEditable);
                    j = Selection.getSelectionEnd(localEditable);
                    if (i < 0)
                        i = 0;
                    if (j < 0)
                        j = 0;
                    if (j >= i)
                        continue;
                    int k = i;
                    i = j;
                    j = k;
                }
            }
            else
            {
                localObject = (Spannable)paramCharSequence;
                setComposingSpans((Spannable)localObject);
            }
        }
        if (paramInt > 0);
        for (int m = paramInt + (j - 1); ; m = paramInt + i)
        {
            if (m < 0)
                m = 0;
            if (m > localEditable.length())
                m = localEditable.length();
            Selection.setSelection(localEditable, m);
            localEditable.replace(i, j, paramCharSequence);
            endBatchEdit();
            break;
        }
    }

    private void sendCurrentText()
    {
        if (!this.mDummyMode);
        while (true)
        {
            return;
            Editable localEditable = getEditable();
            if (localEditable != null)
            {
                int i = localEditable.length();
                if (i != 0)
                    if (i == 1)
                    {
                        if (this.mKeyCharacterMap == null)
                            this.mKeyCharacterMap = KeyCharacterMap.load(-1);
                        char[] arrayOfChar = new char[1];
                        localEditable.getChars(0, 1, arrayOfChar, 0);
                        KeyEvent[] arrayOfKeyEvent = this.mKeyCharacterMap.getEvents(arrayOfChar);
                        if (arrayOfKeyEvent != null)
                        {
                            for (int j = 0; j < arrayOfKeyEvent.length; j++)
                                sendKeyEvent(arrayOfKeyEvent[j]);
                            localEditable.clear();
                        }
                    }
                    else
                    {
                        sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), localEditable.toString(), -1, 0));
                        localEditable.clear();
                    }
            }
        }
    }

    public static void setComposingSpans(Spannable paramSpannable)
    {
        setComposingSpans(paramSpannable, 0, paramSpannable.length());
    }

    public static void setComposingSpans(Spannable paramSpannable, int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = paramSpannable.getSpans(paramInt1, paramInt2, Object.class);
        if (arrayOfObject != null)
        {
            int i = -1 + arrayOfObject.length;
            if (i >= 0)
            {
                Object localObject = arrayOfObject[i];
                if (localObject == COMPOSING)
                    paramSpannable.removeSpan(localObject);
                while (true)
                {
                    i--;
                    break;
                    int j = paramSpannable.getSpanFlags(localObject);
                    if ((j & 0x133) != 289)
                        paramSpannable.setSpan(localObject, paramSpannable.getSpanStart(localObject), paramSpannable.getSpanEnd(localObject), 0x21 | (0x100 | j & 0xFFFFFFCC));
                }
            }
        }
        paramSpannable.setSpan(COMPOSING, paramInt1, paramInt2, 289);
    }

    public boolean beginBatchEdit()
    {
        return false;
    }

    public boolean clearMetaKeyStates(int paramInt)
    {
        Editable localEditable = getEditable();
        if (localEditable == null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            MetaKeyKeyListener.clearMetaKeyState(localEditable, paramInt);
        }
    }

    public boolean commitCompletion(CompletionInfo paramCompletionInfo)
    {
        return false;
    }

    public boolean commitCorrection(CorrectionInfo paramCorrectionInfo)
    {
        return false;
    }

    public boolean commitText(CharSequence paramCharSequence, int paramInt)
    {
        replaceText(paramCharSequence, paramInt, false);
        sendCurrentText();
        return true;
    }

    public boolean deleteSurroundingText(int paramInt1, int paramInt2)
    {
        Editable localEditable = getEditable();
        if (localEditable == null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            beginBatchEdit();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            if (i > j)
            {
                int i5 = i;
                i = j;
                j = i5;
            }
            int k = getComposingSpanStart(localEditable);
            int m = getComposingSpanEnd(localEditable);
            if (m < k)
            {
                int i4 = k;
                k = m;
                m = i4;
            }
            if ((k != -1) && (m != -1))
            {
                if (k < i)
                    i = k;
                if (m > j)
                    j = m;
            }
            int n = 0;
            if (paramInt1 > 0)
            {
                int i3 = i - paramInt1;
                if (i3 < 0)
                    i3 = 0;
                localEditable.delete(i3, i);
                n = i - i3;
            }
            if (paramInt2 > 0)
            {
                int i1 = j - n;
                int i2 = i1 + paramInt2;
                if (i2 > localEditable.length())
                    i2 = localEditable.length();
                localEditable.delete(i1, i2);
            }
            endBatchEdit();
        }
    }

    public boolean endBatchEdit()
    {
        return false;
    }

    public boolean finishComposingText()
    {
        Editable localEditable = getEditable();
        if (localEditable != null)
        {
            beginBatchEdit();
            removeComposingSpans(localEditable);
            endBatchEdit();
            sendCurrentText();
        }
        return true;
    }

    public int getCursorCapsMode(int paramInt)
    {
        int i = 0;
        if (this.mDummyMode);
        while (true)
        {
            return i;
            Editable localEditable = getEditable();
            if (localEditable != null)
            {
                int j = Selection.getSelectionStart(localEditable);
                int k = Selection.getSelectionEnd(localEditable);
                if (j > k)
                    j = k;
                i = TextUtils.getCapsMode(localEditable, j, paramInt);
            }
        }
    }

    public Editable getEditable()
    {
        if (this.mEditable == null)
        {
            this.mEditable = Editable.Factory.getInstance().newEditable("");
            Selection.setSelection(this.mEditable, 0);
        }
        return this.mEditable;
    }

    public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt)
    {
        return null;
    }

    public CharSequence getSelectedText(int paramInt)
    {
        Object localObject = null;
        Editable localEditable = getEditable();
        if (localEditable == null);
        while (true)
        {
            return localObject;
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            if (i > j)
            {
                int k = i;
                i = j;
                j = k;
            }
            if (i != j)
                if ((paramInt & 0x1) != 0)
                    localObject = localEditable.subSequence(i, j);
                else
                    localObject = TextUtils.substring(localEditable, i, j);
        }
    }

    public CharSequence getTextAfterCursor(int paramInt1, int paramInt2)
    {
        Editable localEditable = getEditable();
        Object localObject;
        if (localEditable == null)
            localObject = null;
        while (true)
        {
            return localObject;
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            if (i > j)
                j = i;
            if (j < 0)
                j = 0;
            if (j + paramInt1 > localEditable.length())
                paramInt1 = localEditable.length() - j;
            if ((paramInt2 & 0x1) != 0)
                localObject = localEditable.subSequence(j, j + paramInt1);
            else
                localObject = TextUtils.substring(localEditable, j, j + paramInt1);
        }
    }

    public CharSequence getTextBeforeCursor(int paramInt1, int paramInt2)
    {
        Editable localEditable = getEditable();
        Object localObject;
        if (localEditable == null)
            localObject = null;
        while (true)
        {
            return localObject;
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            if (i > j)
                i = j;
            if (i <= 0)
            {
                localObject = "";
            }
            else
            {
                if (paramInt1 > i)
                    paramInt1 = i;
                if ((paramInt2 & 0x1) != 0)
                    localObject = localEditable.subSequence(i - paramInt1, i);
                else
                    localObject = TextUtils.substring(localEditable, i - paramInt1, i);
            }
        }
    }

    public boolean performContextMenuAction(int paramInt)
    {
        return false;
    }

    public boolean performEditorAction(int paramInt)
    {
        long l = SystemClock.uptimeMillis();
        sendKeyEvent(new KeyEvent(l, l, 0, 66, 0, 0, -1, 0, 22));
        sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), l, 1, 66, 0, 0, -1, 0, 22));
        return true;
    }

    public boolean performPrivateCommand(String paramString, Bundle paramBundle)
    {
        return false;
    }

    protected void reportFinish()
    {
    }

    public boolean reportFullscreenMode(boolean paramBoolean)
    {
        this.mIMM.setFullscreenMode(paramBoolean);
        return true;
    }

    public boolean sendKeyEvent(KeyEvent paramKeyEvent)
    {
        while (true)
        {
            synchronized (this.mIMM.mH)
            {
                if (this.mTargetView != null)
                {
                    localViewRootImpl = this.mTargetView.getViewRootImpl();
                    if ((localViewRootImpl == null) && (this.mIMM.mServedView != null))
                        localViewRootImpl = this.mIMM.mServedView.getViewRootImpl();
                    if (localViewRootImpl != null)
                        localViewRootImpl.dispatchKeyFromIme(paramKeyEvent);
                    return false;
                }
            }
            ViewRootImpl localViewRootImpl = null;
        }
    }

    public boolean setComposingRegion(int paramInt1, int paramInt2)
    {
        Editable localEditable = getEditable();
        if (localEditable != null)
        {
            beginBatchEdit();
            removeComposingSpans(localEditable);
            int i = paramInt1;
            int j = paramInt2;
            if (i > j)
            {
                int n = i;
                i = j;
                j = n;
            }
            int k = localEditable.length();
            if (i < 0)
                i = 0;
            if (j < 0)
                j = 0;
            if (i > k)
                i = k;
            if (j > k)
                j = k;
            ensureDefaultComposingSpans();
            if (this.mDefaultComposingSpans != null)
                for (int m = 0; m < this.mDefaultComposingSpans.length; m++)
                    localEditable.setSpan(this.mDefaultComposingSpans[m], i, j, 289);
            localEditable.setSpan(COMPOSING, i, j, 289);
            endBatchEdit();
            sendCurrentText();
        }
        return true;
    }

    public boolean setComposingText(CharSequence paramCharSequence, int paramInt)
    {
        replaceText(paramCharSequence, paramInt, true);
        return true;
    }

    public boolean setSelection(int paramInt1, int paramInt2)
    {
        boolean bool = true;
        Editable localEditable = getEditable();
        if (localEditable == null)
            bool = false;
        while (true)
        {
            return bool;
            int i = localEditable.length();
            if ((paramInt1 <= i) && (paramInt2 <= i))
                if ((paramInt1 == paramInt2) && (MetaKeyKeyListener.getMetaState(localEditable, 2048) != 0))
                    Selection.extendSelection(localEditable, paramInt1);
                else
                    Selection.setSelection(localEditable, paramInt1, paramInt2);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.inputmethod.BaseInputConnection
 * JD-Core Version:        0.6.2
 */