package android.view.inputmethod;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.text.style.SuggestionSpan;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewRootImpl;
import com.android.internal.os.HandlerCaller.SomeArgs;
import com.android.internal.view.IInputConnectionWrapper;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodCallback;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodClient.Stub;
import com.android.internal.view.IInputMethodManager;
import com.android.internal.view.IInputMethodManager.Stub;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.InputBindResult;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class InputMethodManager
{
    public static final int CONTROL_START_INITIAL = 256;
    public static final int CONTROL_WINDOW_FIRST = 4;
    public static final int CONTROL_WINDOW_IS_TEXT_EDITOR = 2;
    public static final int CONTROL_WINDOW_VIEW_HAS_FOCUS = 1;
    static final boolean DEBUG = false;
    public static final int HIDE_IMPLICIT_ONLY = 1;
    public static final int HIDE_NOT_ALWAYS = 2;
    static final int MSG_BIND = 2;
    static final int MSG_DUMP = 1;
    static final int MSG_SET_ACTIVE = 4;
    static final int MSG_UNBIND = 3;
    public static final int RESULT_HIDDEN = 3;
    public static final int RESULT_SHOWN = 2;
    public static final int RESULT_UNCHANGED_HIDDEN = 1;
    public static final int RESULT_UNCHANGED_SHOWN = 0;
    public static final int SHOW_FORCED = 2;
    public static final int SHOW_IMPLICIT = 1;
    static final String TAG = "InputMethodManager";
    static InputMethodManager mInstance;
    static final Object mInstanceSync = new Object();
    boolean mActive = false;
    int mBindSequence = -1;
    final IInputMethodClient.Stub mClient = new IInputMethodClient.Stub()
    {
        protected void dump(FileDescriptor paramAnonymousFileDescriptor, PrintWriter paramAnonymousPrintWriter, String[] paramAnonymousArrayOfString)
        {
            CountDownLatch localCountDownLatch = new CountDownLatch(1);
            HandlerCaller.SomeArgs localSomeArgs = new HandlerCaller.SomeArgs();
            localSomeArgs.arg1 = paramAnonymousFileDescriptor;
            localSomeArgs.arg2 = paramAnonymousPrintWriter;
            localSomeArgs.arg3 = paramAnonymousArrayOfString;
            localSomeArgs.arg4 = localCountDownLatch;
            InputMethodManager.this.mH.sendMessage(InputMethodManager.this.mH.obtainMessage(1, localSomeArgs));
            try
            {
                if (!localCountDownLatch.await(5L, TimeUnit.SECONDS))
                    paramAnonymousPrintWriter.println("Timeout waiting for dump");
                return;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    paramAnonymousPrintWriter.println("Interrupted waiting for dump");
            }
        }

        public void onBindMethod(InputBindResult paramAnonymousInputBindResult)
        {
            InputMethodManager.this.mH.sendMessage(InputMethodManager.this.mH.obtainMessage(2, paramAnonymousInputBindResult));
        }

        public void onUnbindMethod(int paramAnonymousInt)
        {
            InputMethodManager.this.mH.sendMessage(InputMethodManager.this.mH.obtainMessage(3, paramAnonymousInt, 0));
        }

        public void setActive(boolean paramAnonymousBoolean)
        {
            InputMethodManager.H localH1 = InputMethodManager.this.mH;
            InputMethodManager.H localH2 = InputMethodManager.this.mH;
            if (paramAnonymousBoolean);
            for (int i = 1; ; i = 0)
            {
                localH1.sendMessage(localH2.obtainMessage(4, i, 0));
                return;
            }
        }

        public void setUsingInputMethod(boolean paramAnonymousBoolean)
        {
        }
    };
    CompletionInfo[] mCompletions;
    String mCurId;
    IInputMethodSession mCurMethod;
    View mCurRootView;
    EditorInfo mCurrentTextBoxAttribute;
    int mCursorCandEnd;
    int mCursorCandStart;
    Rect mCursorRect = new Rect();
    int mCursorSelEnd;
    int mCursorSelStart;
    final InputConnection mDummyInputConnection = new BaseInputConnection(this, false);
    boolean mFullscreenMode;
    final H mH;
    boolean mHasBeenInactive = true;
    final IInputContext mIInputContext;
    final Looper mMainLooper;
    View mNextServedView;
    boolean mServedConnecting;
    InputConnection mServedInputConnection;
    ControlledInputConnectionWrapper mServedInputConnectionWrapper;
    View mServedView;
    final IInputMethodManager mService;
    Rect mTmpCursorRect = new Rect();

    InputMethodManager(IInputMethodManager paramIInputMethodManager, Looper paramLooper)
    {
        this.mService = paramIInputMethodManager;
        this.mMainLooper = paramLooper;
        this.mH = new H(paramLooper);
        this.mIInputContext = new ControlledInputConnectionWrapper(paramLooper, this.mDummyInputConnection, this);
        if (mInstance == null)
            mInstance = this;
    }

    private void checkFocus(boolean paramBoolean)
    {
        if (checkFocusNoStartInput(paramBoolean))
            startInputInner(null, 0, 0, 0);
    }

    // ERROR //
    private boolean checkFocusNoStartInput(boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aload_0
        //     3: getfield 149	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
        //     6: aload_0
        //     7: getfield 151	android/view/inputmethod/InputMethodManager:mNextServedView	Landroid/view/View;
        //     10: if_acmpne +9 -> 19
        //     13: iload_1
        //     14: ifne +5 -> 19
        //     17: iload_2
        //     18: ireturn
        //     19: aload_0
        //     20: getfield 127	android/view/inputmethod/InputMethodManager:mH	Landroid/view/inputmethod/InputMethodManager$H;
        //     23: astore_3
        //     24: aload_3
        //     25: monitorenter
        //     26: aload_0
        //     27: getfield 149	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
        //     30: aload_0
        //     31: getfield 151	android/view/inputmethod/InputMethodManager:mNextServedView	Landroid/view/View;
        //     34: if_acmpne +19 -> 53
        //     37: iload_1
        //     38: ifne +15 -> 53
        //     41: aload_3
        //     42: monitorexit
        //     43: goto -26 -> 17
        //     46: astore 4
        //     48: aload_3
        //     49: monitorexit
        //     50: aload 4
        //     52: athrow
        //     53: aload_0
        //     54: getfield 151	android/view/inputmethod/InputMethodManager:mNextServedView	Landroid/view/View;
        //     57: ifnonnull +16 -> 73
        //     60: aload_0
        //     61: invokevirtual 154	android/view/inputmethod/InputMethodManager:finishInputLocked	()V
        //     64: aload_0
        //     65: invokevirtual 157	android/view/inputmethod/InputMethodManager:closeCurrentInput	()V
        //     68: aload_3
        //     69: monitorexit
        //     70: goto -53 -> 17
        //     73: aload_0
        //     74: getfield 159	android/view/inputmethod/InputMethodManager:mServedInputConnection	Landroid/view/inputmethod/InputConnection;
        //     77: astore 5
        //     79: aload_0
        //     80: aload_0
        //     81: getfield 151	android/view/inputmethod/InputMethodManager:mNextServedView	Landroid/view/View;
        //     84: putfield 149	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
        //     87: aload_0
        //     88: aconst_null
        //     89: putfield 161	android/view/inputmethod/InputMethodManager:mCurrentTextBoxAttribute	Landroid/view/inputmethod/EditorInfo;
        //     92: aload_0
        //     93: aconst_null
        //     94: putfield 163	android/view/inputmethod/InputMethodManager:mCompletions	[Landroid/view/inputmethod/CompletionInfo;
        //     97: aload_0
        //     98: iconst_1
        //     99: putfield 165	android/view/inputmethod/InputMethodManager:mServedConnecting	Z
        //     102: aload_3
        //     103: monitorexit
        //     104: aload 5
        //     106: ifnull +11 -> 117
        //     109: aload 5
        //     111: invokeinterface 171 1 0
        //     116: pop
        //     117: iconst_1
        //     118: istore_2
        //     119: goto -102 -> 17
        //
        // Exception table:
        //     from	to	target	type
        //     26	50	46	finally
        //     53	104	46	finally
    }

    public static InputMethodManager getInstance(Context paramContext)
    {
        return getInstance(paramContext.getMainLooper());
    }

    public static InputMethodManager getInstance(Looper paramLooper)
    {
        InputMethodManager localInputMethodManager;
        synchronized (mInstanceSync)
        {
            if (mInstance != null)
            {
                localInputMethodManager = mInstance;
            }
            else
            {
                mInstance = new InputMethodManager(IInputMethodManager.Stub.asInterface(ServiceManager.getService("input_method")), paramLooper);
                localInputMethodManager = mInstance;
            }
        }
        return localInputMethodManager;
    }

    private void notifyInputConnectionFinished()
    {
        if ((this.mServedView != null) && (this.mServedInputConnection != null))
        {
            ViewRootImpl localViewRootImpl = this.mServedView.getViewRootImpl();
            if (localViewRootImpl != null)
                localViewRootImpl.dispatchFinishInputConnection(this.mServedInputConnection);
        }
    }

    public static InputMethodManager peekInstance()
    {
        return mInstance;
    }

    static void scheduleCheckFocusLocked(View paramView)
    {
        ViewRootImpl localViewRootImpl = paramView.getViewRootImpl();
        if (localViewRootImpl != null)
            localViewRootImpl.dispatchCheckFocus();
    }

    public void checkFocus()
    {
        checkFocus(false);
    }

    void clearBindingLocked()
    {
        clearConnectionLocked();
        this.mBindSequence = -1;
        this.mCurId = null;
        this.mCurMethod = null;
    }

    void clearConnectionLocked()
    {
        this.mCurrentTextBoxAttribute = null;
        this.mServedInputConnection = null;
        if (this.mServedInputConnectionWrapper != null)
        {
            this.mServedInputConnectionWrapper.deactivate();
            this.mServedInputConnectionWrapper = null;
        }
    }

    void closeCurrentInput()
    {
        try
        {
            this.mService.hideSoftInput(this.mClient, 2, null);
            label16: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label16;
        }
    }

    public void dispatchKeyEvent(Context paramContext, int paramInt, KeyEvent paramKeyEvent, IInputMethodCallback paramIInputMethodCallback)
    {
        synchronized (this.mH)
        {
            IInputMethodSession localIInputMethodSession = this.mCurMethod;
            if (localIInputMethodSession != null);
        }
        try
        {
            paramIInputMethodCallback.finishedEvent(paramInt, false);
            return;
            if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getKeyCode() == 63))
                showInputMethodPicker();
        }
        catch (RemoteException localRemoteException4)
        {
            try
            {
                paramIInputMethodCallback.finishedEvent(paramInt, true);
                label64: return;
                localObject = finally;
                throw localObject;
                try
                {
                    this.mCurMethod.dispatchKeyEvent(paramInt, paramKeyEvent, paramIInputMethodCallback);
                }
                catch (RemoteException localRemoteException1)
                {
                    while (true)
                    {
                        Log.w("InputMethodManager", "IME died: " + this.mCurId + " dropping: " + paramKeyEvent, localRemoteException1);
                        try
                        {
                            paramIInputMethodCallback.finishedEvent(paramInt, false);
                        }
                        catch (RemoteException localRemoteException2)
                        {
                        }
                    }
                }
                localRemoteException4 = localRemoteException4;
            }
            catch (RemoteException localRemoteException3)
            {
                break label64;
            }
        }
    }

    void dispatchTrackballEvent(Context paramContext, int paramInt, MotionEvent paramMotionEvent, IInputMethodCallback paramIInputMethodCallback)
    {
        synchronized (this.mH)
        {
            if (this.mCurMethod != null)
            {
                EditorInfo localEditorInfo = this.mCurrentTextBoxAttribute;
                if (localEditorInfo != null)
                    break label40;
            }
        }
        try
        {
            paramIInputMethodCallback.finishedEvent(paramInt, false);
            while (true)
            {
                label36: return;
                try
                {
                    label40: this.mCurMethod.dispatchTrackballEvent(paramInt, paramMotionEvent, paramIInputMethodCallback);
                    continue;
                    localObject = finally;
                    throw localObject;
                }
                catch (RemoteException localRemoteException2)
                {
                    while (true)
                    {
                        Log.w("InputMethodManager", "IME died: " + this.mCurId + " dropping trackball: " + paramMotionEvent, localRemoteException2);
                        try
                        {
                            paramIInputMethodCallback.finishedEvent(paramInt, false);
                        }
                        catch (RemoteException localRemoteException3)
                        {
                        }
                    }
                }
            }
        }
        catch (RemoteException localRemoteException1)
        {
            break label36;
        }
    }

    public void displayCompletions(View paramView, CompletionInfo[] paramArrayOfCompletionInfo)
    {
        checkFocus();
        synchronized (this.mH)
        {
            if ((this.mServedView != paramView) && ((this.mServedView != null) && (!this.mServedView.checkInputConnectionProxy(paramView))))
                return;
            this.mCompletions = paramArrayOfCompletionInfo;
            IInputMethodSession localIInputMethodSession = this.mCurMethod;
            if (localIInputMethodSession == null);
        }
        try
        {
            this.mCurMethod.displayCompletions(this.mCompletions);
            label71: return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label71;
        }
    }

    void doDump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        PrintWriterPrinter localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
        localPrintWriterPrinter.println("Input method client state for " + this + ":");
        localPrintWriterPrinter.println("    mService=" + this.mService);
        localPrintWriterPrinter.println("    mMainLooper=" + this.mMainLooper);
        localPrintWriterPrinter.println("    mIInputContext=" + this.mIInputContext);
        localPrintWriterPrinter.println("    mActive=" + this.mActive + " mHasBeenInactive=" + this.mHasBeenInactive + " mBindSequence=" + this.mBindSequence + " mCurId=" + this.mCurId);
        localPrintWriterPrinter.println("    mCurMethod=" + this.mCurMethod);
        localPrintWriterPrinter.println("    mCurRootView=" + this.mCurRootView);
        localPrintWriterPrinter.println("    mServedView=" + this.mServedView);
        localPrintWriterPrinter.println("    mNextServedView=" + this.mNextServedView);
        localPrintWriterPrinter.println("    mServedConnecting=" + this.mServedConnecting);
        if (this.mCurrentTextBoxAttribute != null)
        {
            localPrintWriterPrinter.println("    mCurrentTextBoxAttribute:");
            this.mCurrentTextBoxAttribute.dump(localPrintWriterPrinter, "        ");
        }
        while (true)
        {
            localPrintWriterPrinter.println("    mServedInputConnection=" + this.mServedInputConnection);
            localPrintWriterPrinter.println("    mCompletions=" + this.mCompletions);
            localPrintWriterPrinter.println("    mCursorRect=" + this.mCursorRect);
            localPrintWriterPrinter.println("    mCursorSelStart=" + this.mCursorSelStart + " mCursorSelEnd=" + this.mCursorSelEnd + " mCursorCandStart=" + this.mCursorCandStart + " mCursorCandEnd=" + this.mCursorCandEnd);
            return;
            localPrintWriterPrinter.println("    mCurrentTextBoxAttribute: null");
        }
    }

    void finishInputLocked()
    {
        this.mCurRootView = null;
        this.mNextServedView = null;
        if ((this.mServedView == null) || (this.mCurrentTextBoxAttribute != null));
        try
        {
            this.mService.finishInput(this.mClient);
            label37: notifyInputConnectionFinished();
            this.mServedView = null;
            this.mCompletions = null;
            this.mServedConnecting = false;
            clearConnectionLocked();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            break label37;
        }
    }

    public void focusIn(View paramView)
    {
        synchronized (this.mH)
        {
            focusInLocked(paramView);
            return;
        }
    }

    void focusInLocked(View paramView)
    {
        if (this.mCurRootView != paramView.getRootView());
        while (true)
        {
            return;
            this.mNextServedView = paramView;
            scheduleCheckFocusLocked(paramView);
        }
    }

    public void focusOut(View paramView)
    {
        synchronized (this.mH)
        {
            if (this.mServedView != paramView);
            return;
        }
    }

    public IInputMethodClient getClient()
    {
        return this.mClient;
    }

    public InputMethodSubtype getCurrentInputMethodSubtype()
    {
        InputMethodSubtype localInputMethodSubtype1;
        synchronized (this.mH)
        {
            try
            {
                InputMethodSubtype localInputMethodSubtype2 = this.mService.getCurrentInputMethodSubtype();
                localInputMethodSubtype1 = localInputMethodSubtype2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
                localInputMethodSubtype1 = null;
            }
        }
        return localInputMethodSubtype1;
    }

    public List<InputMethodInfo> getEnabledInputMethodList()
    {
        try
        {
            List localList = this.mService.getEnabledInputMethodList();
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(InputMethodInfo paramInputMethodInfo, boolean paramBoolean)
    {
        try
        {
            List localList = this.mService.getEnabledInputMethodSubtypeList(paramInputMethodInfo, paramBoolean);
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public IInputContext getInputContext()
    {
        return this.mIInputContext;
    }

    public List<InputMethodInfo> getInputMethodList()
    {
        try
        {
            List localList = this.mService.getInputMethodList();
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public InputMethodSubtype getLastInputMethodSubtype()
    {
        InputMethodSubtype localInputMethodSubtype1;
        synchronized (this.mH)
        {
            try
            {
                InputMethodSubtype localInputMethodSubtype2 = this.mService.getLastInputMethodSubtype();
                localInputMethodSubtype1 = localInputMethodSubtype2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
                localInputMethodSubtype1 = null;
            }
        }
        return localInputMethodSubtype1;
    }

    public Map<InputMethodInfo, List<InputMethodSubtype>> getShortcutInputMethodsAndSubtypes()
    {
        while (true)
        {
            int j;
            synchronized (this.mH)
            {
                HashMap localHashMap = new HashMap();
                try
                {
                    List localList = this.mService.getShortcutInputMethodsAndSubtypes();
                    ArrayList localArrayList = null;
                    int i = localList.size();
                    Object localObject2;
                    if ((localList != null) && (i > 0))
                    {
                        j = 0;
                        if (j < i)
                        {
                            localObject2 = localList.get(j);
                            if (!(localObject2 instanceof InputMethodInfo))
                                continue;
                            if (!localHashMap.containsKey(localObject2))
                                continue;
                            Log.e("InputMethodManager", "IMI list already contains the same InputMethod.");
                        }
                    }
                    return localHashMap;
                    localArrayList = new ArrayList();
                    localHashMap.put((InputMethodInfo)localObject2, localArrayList);
                    break label191;
                    if ((localArrayList != null) && ((localObject2 instanceof InputMethodSubtype)))
                        localArrayList.add((InputMethodSubtype)localObject2);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
                    continue;
                }
            }
            label191: j++;
        }
    }

    public void hideSoftInputFromInputMethod(IBinder paramIBinder, int paramInt)
    {
        try
        {
            this.mService.hideMySoftInput(paramIBinder, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public boolean hideSoftInputFromWindow(IBinder paramIBinder, int paramInt)
    {
        return hideSoftInputFromWindow(paramIBinder, paramInt, null);
    }

    public boolean hideSoftInputFromWindow(IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver)
    {
        boolean bool1 = false;
        checkFocus();
        while (true)
        {
            synchronized (this.mH)
            {
                if ((this.mServedView == null) || (this.mServedView.getWindowToken() != paramIBinder))
                    return bool1;
            }
            try
            {
                boolean bool2 = this.mService.hideSoftInput(this.mClient, paramInt, paramResultReceiver);
                bool1 = bool2;
                continue;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public void hideStatusIcon(IBinder paramIBinder)
    {
        try
        {
            this.mService.updateStatusIcon(paramIBinder, null, 0);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public boolean isAcceptingText()
    {
        checkFocus();
        if (this.mServedInputConnection != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isActive()
    {
        checkFocus();
        while (true)
        {
            synchronized (this.mH)
            {
                if ((this.mServedView != null) && (this.mCurrentTextBoxAttribute != null))
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
    }

    public boolean isActive(View paramView)
    {
        checkFocus();
        while (true)
        {
            synchronized (this.mH)
            {
                if (((this.mServedView == paramView) || ((this.mServedView != null) && (this.mServedView.checkInputConnectionProxy(paramView)))) && (this.mCurrentTextBoxAttribute != null))
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
    }

    public boolean isFullscreenMode()
    {
        return this.mFullscreenMode;
    }

    public boolean isWatchingCursor(View paramView)
    {
        return false;
    }

    public void notifySuggestionPicked(SuggestionSpan paramSuggestionSpan, String paramString, int paramInt)
    {
        try
        {
            this.mService.notifySuggestionPicked(paramSuggestionSpan, paramString, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void onWindowFocus(View paramView1, View paramView2, int paramInt1, boolean paramBoolean, int paramInt2)
    {
        boolean bool = false;
        while (true)
        {
            View localView;
            int i;
            synchronized (this.mH)
            {
                if (!this.mHasBeenInactive)
                    break label168;
                this.mHasBeenInactive = false;
                bool = true;
                break label168;
                focusInLocked(localView);
                i = 0;
                if (paramView2 != null)
                {
                    i = 0x0 | 0x1;
                    if (paramView2.onCheckIsTextEditor())
                        i |= 2;
                }
                if (paramBoolean)
                    i |= 4;
                if ((checkFocusNoStartInput(bool)) && (startInputInner(paramView1.getWindowToken(), i, paramInt1, paramInt2)))
                {
                    return;
                    localView = paramView1;
                }
            }
            try
            {
                label149: synchronized (this.mH)
                {
                    this.mService.windowGainedFocus(this.mClient, paramView1.getWindowToken(), i, paramInt1, paramInt2, null, null);
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label149;
            }
            label168: if (paramView2 != null)
                localView = paramView2;
        }
    }

    public void registerSuggestionSpansForNotification(SuggestionSpan[] paramArrayOfSuggestionSpan)
    {
        try
        {
            this.mService.registerSuggestionSpansForNotification(paramArrayOfSuggestionSpan);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void reportFinishInputConnection(InputConnection paramInputConnection)
    {
        if (this.mServedInputConnection != paramInputConnection)
        {
            paramInputConnection.finishComposingText();
            if ((paramInputConnection instanceof BaseInputConnection))
                ((BaseInputConnection)paramInputConnection).reportFinish();
        }
    }

    public void restartInput(View paramView)
    {
        checkFocus();
        synchronized (this.mH)
        {
            if ((this.mServedView == paramView) || ((this.mServedView == null) || (this.mServedView.checkInputConnectionProxy(paramView))))
            {
                this.mServedConnecting = true;
                startInputInner(null, 0, 0, 0);
            }
        }
    }

    public void sendAppPrivateCommand(View paramView, String paramString, Bundle paramBundle)
    {
        checkFocus();
        while (true)
        {
            synchronized (this.mH)
            {
                if (((this.mServedView != paramView) && ((this.mServedView == null) || (!this.mServedView.checkInputConnectionProxy(paramView)))) || (this.mCurrentTextBoxAttribute == null) || (this.mCurMethod == null))
                    return;
            }
            try
            {
                this.mCurMethod.appPrivateCommand(paramString, paramBundle);
                continue;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
            }
        }
    }

    public void setAdditionalInputMethodSubtypes(String paramString, InputMethodSubtype[] paramArrayOfInputMethodSubtype)
    {
        synchronized (this.mH)
        {
            try
            {
                this.mService.setAdditionalInputMethodSubtypes(paramString, paramArrayOfInputMethodSubtype);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
            }
        }
    }

    public boolean setCurrentInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
    {
        boolean bool1;
        synchronized (this.mH)
        {
            try
            {
                boolean bool2 = this.mService.setCurrentInputMethodSubtype(paramInputMethodSubtype);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
                bool1 = false;
            }
        }
        return bool1;
    }

    public void setFullscreenMode(boolean paramBoolean)
    {
        this.mFullscreenMode = paramBoolean;
    }

    public void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
    {
        try
        {
            this.mService.setImeWindowStatus(paramIBinder, paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void setInputMethod(IBinder paramIBinder, String paramString)
    {
        try
        {
            this.mService.setInputMethod(paramIBinder, paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void setInputMethodAndSubtype(IBinder paramIBinder, String paramString, InputMethodSubtype paramInputMethodSubtype)
    {
        try
        {
            this.mService.setInputMethodAndSubtype(paramIBinder, paramString, paramInputMethodSubtype);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void showInputMethodAndSubtypeEnabler(String paramString)
    {
        synchronized (this.mH)
        {
            try
            {
                this.mService.showInputMethodAndSubtypeEnablerFromClient(this.mClient, paramString);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
            }
        }
    }

    public void showInputMethodPicker()
    {
        synchronized (this.mH)
        {
            try
            {
                this.mService.showInputMethodPickerFromClient(this.mClient);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
            }
        }
    }

    public boolean showSoftInput(View paramView, int paramInt)
    {
        return showSoftInput(paramView, paramInt, null);
    }

    public boolean showSoftInput(View paramView, int paramInt, ResultReceiver paramResultReceiver)
    {
        boolean bool1 = false;
        checkFocus();
        while (true)
        {
            synchronized (this.mH)
            {
                if ((this.mServedView != paramView) && ((this.mServedView == null) || (!this.mServedView.checkInputConnectionProxy(paramView))))
                    return bool1;
            }
            try
            {
                boolean bool2 = this.mService.showSoftInput(this.mClient, paramInt, paramResultReceiver);
                bool1 = bool2;
                continue;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public void showSoftInputFromInputMethod(IBinder paramIBinder, int paramInt)
    {
        try
        {
            this.mService.showMySoftInput(paramIBinder, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void showSoftInputUnchecked(int paramInt, ResultReceiver paramResultReceiver)
    {
        try
        {
            this.mService.showSoftInput(this.mClient, paramInt, paramResultReceiver);
            label16: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label16;
        }
    }

    public void showStatusIcon(IBinder paramIBinder, String paramString, int paramInt)
    {
        try
        {
            this.mService.updateStatusIcon(paramIBinder, paramString, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void startGettingWindowFocus(View paramView)
    {
        synchronized (this.mH)
        {
            this.mCurRootView = paramView;
            return;
        }
    }

    // ERROR //
    boolean startInputInner(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 127	android/view/inputmethod/InputMethodManager:mH	Landroid/view/inputmethod/InputMethodManager$H;
        //     4: astore 5
        //     6: aload 5
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 149	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
        //     13: astore 7
        //     15: aload 7
        //     17: ifnonnull +12 -> 29
        //     20: iconst_0
        //     21: istore 13
        //     23: aload 5
        //     25: monitorexit
        //     26: goto +448 -> 474
        //     29: aload 5
        //     31: monitorexit
        //     32: aload 7
        //     34: invokevirtual 576	android/view/View:getHandler	()Landroid/os/Handler;
        //     37: astore 8
        //     39: aload 8
        //     41: ifnonnull +17 -> 58
        //     44: iconst_0
        //     45: istore 13
        //     47: goto +427 -> 474
        //     50: astore 6
        //     52: aload 5
        //     54: monitorexit
        //     55: aload 6
        //     57: athrow
        //     58: aload 8
        //     60: invokevirtual 581	android/os/Handler:getLooper	()Landroid/os/Looper;
        //     63: invokestatic 586	android/os/Looper:myLooper	()Landroid/os/Looper;
        //     66: if_acmpeq +23 -> 89
        //     69: aload 8
        //     71: new 8	android/view/inputmethod/InputMethodManager$2
        //     74: dup
        //     75: aload_0
        //     76: invokespecial 587	android/view/inputmethod/InputMethodManager$2:<init>	(Landroid/view/inputmethod/InputMethodManager;)V
        //     79: invokevirtual 591	android/os/Handler:post	(Ljava/lang/Runnable;)Z
        //     82: pop
        //     83: iconst_0
        //     84: istore 13
        //     86: goto +388 -> 474
        //     89: new 361	android/view/inputmethod/EditorInfo
        //     92: dup
        //     93: invokespecial 592	android/view/inputmethod/EditorInfo:<init>	()V
        //     96: astore 9
        //     98: aload 9
        //     100: aload 7
        //     102: invokevirtual 596	android/view/View:getContext	()Landroid/content/Context;
        //     105: invokevirtual 599	android/content/Context:getPackageName	()Ljava/lang/String;
        //     108: putfield 602	android/view/inputmethod/EditorInfo:packageName	Ljava/lang/String;
        //     111: aload 9
        //     113: aload 7
        //     115: invokevirtual 605	android/view/View:getId	()I
        //     118: putfield 608	android/view/inputmethod/EditorInfo:fieldId	I
        //     121: aload 7
        //     123: aload 9
        //     125: invokevirtual 612	android/view/View:onCreateInputConnection	(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
        //     128: astore 10
        //     130: aload_0
        //     131: getfield 127	android/view/inputmethod/InputMethodManager:mH	Landroid/view/inputmethod/InputMethodManager$H;
        //     134: astore 11
        //     136: aload 11
        //     138: monitorenter
        //     139: aload_0
        //     140: getfield 149	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
        //     143: aload 7
        //     145: if_acmpne +10 -> 155
        //     148: aload_0
        //     149: getfield 165	android/view/inputmethod/InputMethodManager:mServedConnecting	Z
        //     152: ifne +20 -> 172
        //     155: iconst_0
        //     156: istore 13
        //     158: aload 11
        //     160: monitorexit
        //     161: goto +313 -> 474
        //     164: astore 12
        //     166: aload 11
        //     168: monitorexit
        //     169: aload 12
        //     171: athrow
        //     172: aload_0
        //     173: getfield 161	android/view/inputmethod/InputMethodManager:mCurrentTextBoxAttribute	Landroid/view/inputmethod/EditorInfo;
        //     176: ifnonnull +9 -> 185
        //     179: sipush 256
        //     182: iload_2
        //     183: ior
        //     184: istore_2
        //     185: aload_0
        //     186: aload 9
        //     188: putfield 161	android/view/inputmethod/InputMethodManager:mCurrentTextBoxAttribute	Landroid/view/inputmethod/EditorInfo;
        //     191: aload_0
        //     192: iconst_0
        //     193: putfield 165	android/view/inputmethod/InputMethodManager:mServedConnecting	Z
        //     196: aload_0
        //     197: invokespecial 395	android/view/inputmethod/InputMethodManager:notifyInputConnectionFinished	()V
        //     200: aload_0
        //     201: aload 10
        //     203: putfield 159	android/view/inputmethod/InputMethodManager:mServedInputConnection	Landroid/view/inputmethod/InputConnection;
        //     206: aload 10
        //     208: ifnull +176 -> 384
        //     211: aload_0
        //     212: aload 9
        //     214: getfield 615	android/view/inputmethod/EditorInfo:initialSelStart	I
        //     217: putfield 375	android/view/inputmethod/InputMethodManager:mCursorSelStart	I
        //     220: aload_0
        //     221: aload 9
        //     223: getfield 618	android/view/inputmethod/EditorInfo:initialSelEnd	I
        //     226: putfield 379	android/view/inputmethod/InputMethodManager:mCursorSelEnd	I
        //     229: aload_0
        //     230: bipush 255
        //     232: putfield 383	android/view/inputmethod/InputMethodManager:mCursorCandStart	I
        //     235: aload_0
        //     236: bipush 255
        //     238: putfield 387	android/view/inputmethod/InputMethodManager:mCursorCandEnd	I
        //     241: aload_0
        //     242: getfield 104	android/view/inputmethod/InputMethodManager:mCursorRect	Landroid/graphics/Rect;
        //     245: invokevirtual 621	android/graphics/Rect:setEmpty	()V
        //     248: new 10	android/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper
        //     251: dup
        //     252: aload 8
        //     254: invokevirtual 581	android/os/Handler:getLooper	()Landroid/os/Looper;
        //     257: aload 10
        //     259: aload_0
        //     260: invokespecial 130	android/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper:<init>	(Landroid/os/Looper;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/InputMethodManager;)V
        //     263: astore 14
        //     265: aload_0
        //     266: getfield 228	android/view/inputmethod/InputMethodManager:mServedInputConnectionWrapper	Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
        //     269: ifnull +10 -> 279
        //     272: aload_0
        //     273: getfield 228	android/view/inputmethod/InputMethodManager:mServedInputConnectionWrapper	Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
        //     276: invokevirtual 231	android/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper:deactivate	()V
        //     279: aload_0
        //     280: aload 14
        //     282: putfield 228	android/view/inputmethod/InputMethodManager:mServedInputConnectionWrapper	Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
        //     285: aload_1
        //     286: ifnull +104 -> 390
        //     289: aload_0
        //     290: getfield 120	android/view/inputmethod/InputMethodManager:mService	Lcom/android/internal/view/IInputMethodManager;
        //     293: aload_0
        //     294: getfield 111	android/view/inputmethod/InputMethodManager:mClient	Lcom/android/internal/view/IInputMethodClient$Stub;
        //     297: aload_1
        //     298: iload_2
        //     299: iload_3
        //     300: iload 4
        //     302: aload 9
        //     304: aload 14
        //     306: invokeinterface 513 8 0
        //     311: astore 17
        //     313: aload 17
        //     315: ifnull +29 -> 344
        //     318: aload 17
        //     320: getfield 626	com/android/internal/view/InputBindResult:id	Ljava/lang/String;
        //     323: ifnull +90 -> 413
        //     326: aload_0
        //     327: aload 17
        //     329: getfield 629	com/android/internal/view/InputBindResult:sequence	I
        //     332: putfield 106	android/view/inputmethod/InputMethodManager:mBindSequence	I
        //     335: aload_0
        //     336: aload 17
        //     338: getfield 632	com/android/internal/view/InputBindResult:method	Lcom/android/internal/view/IInputMethodSession;
        //     341: putfield 226	android/view/inputmethod/InputMethodManager:mCurMethod	Lcom/android/internal/view/IInputMethodSession;
        //     344: aload_0
        //     345: getfield 226	android/view/inputmethod/InputMethodManager:mCurMethod	Lcom/android/internal/view/IInputMethodSession;
        //     348: ifnull +27 -> 375
        //     351: aload_0
        //     352: getfield 163	android/view/inputmethod/InputMethodManager:mCompletions	[Landroid/view/inputmethod/CompletionInfo;
        //     355: astore 18
        //     357: aload 18
        //     359: ifnull +16 -> 375
        //     362: aload_0
        //     363: getfield 226	android/view/inputmethod/InputMethodManager:mCurMethod	Lcom/android/internal/view/IInputMethodSession;
        //     366: aload_0
        //     367: getfield 163	android/view/inputmethod/InputMethodManager:mCompletions	[Landroid/view/inputmethod/CompletionInfo;
        //     370: invokeinterface 306 2 0
        //     375: aload 11
        //     377: monitorexit
        //     378: iconst_1
        //     379: istore 13
        //     381: goto +93 -> 474
        //     384: aconst_null
        //     385: astore 14
        //     387: goto -122 -> 265
        //     390: aload_0
        //     391: getfield 120	android/view/inputmethod/InputMethodManager:mService	Lcom/android/internal/view/IInputMethodManager;
        //     394: aload_0
        //     395: getfield 111	android/view/inputmethod/InputMethodManager:mClient	Lcom/android/internal/view/IInputMethodClient$Stub;
        //     398: aload 14
        //     400: aload 9
        //     402: iload_2
        //     403: invokeinterface 636 5 0
        //     408: astore 17
        //     410: goto -97 -> 313
        //     413: aload_0
        //     414: getfield 226	android/view/inputmethod/InputMethodManager:mCurMethod	Lcom/android/internal/view/IInputMethodSession;
        //     417: astore 20
        //     419: aload 20
        //     421: ifnonnull -77 -> 344
        //     424: iconst_1
        //     425: istore 13
        //     427: aload 11
        //     429: monitorexit
        //     430: goto +44 -> 474
        //     433: astore 15
        //     435: ldc 43
        //     437: new 266	java/lang/StringBuilder
        //     440: dup
        //     441: invokespecial 267	java/lang/StringBuilder:<init>	()V
        //     444: ldc_w 269
        //     447: invokevirtual 273	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     450: aload_0
        //     451: getfield 224	android/view/inputmethod/InputMethodManager:mCurId	Ljava/lang/String;
        //     454: invokevirtual 273	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     457: invokevirtual 282	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     460: aload 15
        //     462: invokestatic 288	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     465: pop
        //     466: goto -91 -> 375
        //     469: astore 19
        //     471: goto -96 -> 375
        //     474: iload 13
        //     476: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	32	50	finally
        //     52	55	50	finally
        //     139	169	164	finally
        //     172	285	164	finally
        //     289	357	164	finally
        //     362	375	164	finally
        //     375	378	164	finally
        //     390	419	164	finally
        //     427	466	164	finally
        //     289	357	433	android/os/RemoteException
        //     390	419	433	android/os/RemoteException
        //     362	375	469	android/os/RemoteException
    }

    public boolean switchToLastInputMethod(IBinder paramIBinder)
    {
        boolean bool1;
        synchronized (this.mH)
        {
            try
            {
                boolean bool2 = this.mService.switchToLastInputMethod(paramIBinder);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
                bool1 = false;
            }
        }
        return bool1;
    }

    public boolean switchToNextInputMethod(IBinder paramIBinder, boolean paramBoolean)
    {
        boolean bool1;
        synchronized (this.mH)
        {
            try
            {
                boolean bool2 = this.mService.switchToNextInputMethod(paramIBinder, paramBoolean);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
                bool1 = false;
            }
        }
        return bool1;
    }

    public void toggleSoftInput(int paramInt1, int paramInt2)
    {
        if (this.mCurMethod != null);
        try
        {
            this.mCurMethod.toggleSoftInput(paramInt1, paramInt2);
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    public void toggleSoftInputFromWindow(IBinder paramIBinder, int paramInt1, int paramInt2)
    {
        synchronized (this.mH)
        {
            if ((this.mServedView != null) && (this.mServedView.getWindowToken() != paramIBinder))
                return;
            IInputMethodSession localIInputMethodSession = this.mCurMethod;
            if (localIInputMethodSession == null);
        }
        try
        {
            this.mCurMethod.toggleSoftInput(paramInt1, paramInt2);
            label55: return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label55;
        }
    }

    public void updateCursor(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        checkFocus();
        synchronized (this.mH)
        {
            if (((this.mServedView == paramView) || ((this.mServedView != null) && (this.mServedView.checkInputConnectionProxy(paramView)))) && (this.mCurrentTextBoxAttribute != null) && (this.mCurMethod == null))
                return;
            this.mTmpCursorRect.set(paramInt1, paramInt2, paramInt3, paramInt4);
            boolean bool = this.mCursorRect.equals(this.mTmpCursorRect);
            if (bool);
        }
        try
        {
            this.mCurMethod.updateCursor(this.mTmpCursorRect);
            this.mCursorRect.set(this.mTmpCursorRect);
            return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
        }
    }

    public void updateExtractedText(View paramView, int paramInt, ExtractedText paramExtractedText)
    {
        checkFocus();
        synchronized (this.mH)
        {
            if ((this.mServedView != paramView) && ((this.mServedView != null) && (!this.mServedView.checkInputConnectionProxy(paramView))))
                return;
            IInputMethodSession localIInputMethodSession = this.mCurMethod;
            if (localIInputMethodSession == null);
        }
        try
        {
            this.mCurMethod.updateExtractedText(paramInt, paramExtractedText);
            label67: return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label67;
        }
    }

    public void updateSelection(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        checkFocus();
        synchronized (this.mH)
        {
            if (((this.mServedView == paramView) || ((this.mServedView != null) && (this.mServedView.checkInputConnectionProxy(paramView)))) && (this.mCurrentTextBoxAttribute != null) && (this.mCurMethod == null))
                return;
            if ((this.mCursorSelStart == paramInt1) && (this.mCursorSelEnd == paramInt2) && (this.mCursorCandStart == paramInt3))
            {
                int i = this.mCursorCandEnd;
                if (i == paramInt4)
                    break label142;
            }
        }
        try
        {
            this.mCurMethod.updateSelection(this.mCursorSelStart, this.mCursorSelEnd, paramInt1, paramInt2, paramInt3, paramInt4);
            this.mCursorSelStart = paramInt1;
            this.mCursorSelEnd = paramInt2;
            this.mCursorCandStart = paramInt3;
            this.mCursorCandEnd = paramInt4;
            label142: return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
        }
    }

    public void viewClicked(View paramView)
    {
        boolean bool;
        if (this.mServedView != this.mNextServedView)
        {
            bool = true;
            checkFocus();
        }
        while (true)
        {
            synchronized (this.mH)
            {
                if (((this.mServedView != paramView) && ((this.mServedView == null) || (!this.mServedView.checkInputConnectionProxy(paramView)))) || (this.mCurrentTextBoxAttribute == null) || (this.mCurMethod == null))
                {
                    return;
                    bool = false;
                }
            }
            try
            {
                this.mCurMethod.viewClicked(bool);
                continue;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputMethodManager", "IME died: " + this.mCurId, localRemoteException);
            }
        }
    }

    public void windowDismissed(IBinder paramIBinder)
    {
        checkFocus();
        synchronized (this.mH)
        {
            if ((this.mServedView != null) && (this.mServedView.getWindowToken() == paramIBinder))
                finishInputLocked();
            return;
        }
    }

    private static class ControlledInputConnectionWrapper extends IInputConnectionWrapper
    {
        private boolean mActive;
        private final InputMethodManager mParentInputMethodManager;

        public ControlledInputConnectionWrapper(Looper paramLooper, InputConnection paramInputConnection, InputMethodManager paramInputMethodManager)
        {
            super(paramInputConnection);
            this.mParentInputMethodManager = paramInputMethodManager;
            this.mActive = true;
        }

        void deactivate()
        {
            this.mActive = false;
        }

        public boolean isActive()
        {
            if ((this.mParentInputMethodManager.mActive) && (this.mActive));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    class H extends Handler
    {
        H(Looper arg2)
        {
            super();
        }

        // ERROR //
        public void handleMessage(android.os.Message paramMessage)
        {
            // Byte code:
            //     0: iconst_1
            //     1: istore_2
            //     2: aload_1
            //     3: getfield 28	android/os/Message:what	I
            //     6: tableswitch	default:+30 -> 36, 1:+31->37, 2:+145->151, 3:+303->309, 4:+418->424
            //     37: aload_1
            //     38: getfield 32	android/os/Message:obj	Ljava/lang/Object;
            //     41: checkcast 34	com/android/internal/os/HandlerCaller$SomeArgs
            //     44: astore 16
            //     46: aload_0
            //     47: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     50: aload 16
            //     52: getfield 37	com/android/internal/os/HandlerCaller$SomeArgs:arg1	Ljava/lang/Object;
            //     55: checkcast 39	java/io/FileDescriptor
            //     58: aload 16
            //     60: getfield 42	com/android/internal/os/HandlerCaller$SomeArgs:arg2	Ljava/lang/Object;
            //     63: checkcast 44	java/io/PrintWriter
            //     66: aload 16
            //     68: getfield 47	com/android/internal/os/HandlerCaller$SomeArgs:arg3	Ljava/lang/Object;
            //     71: checkcast 49	[Ljava/lang/String;
            //     74: checkcast 49	[Ljava/lang/String;
            //     77: invokevirtual 53	android/view/inputmethod/InputMethodManager:doDump	(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
            //     80: aload 16
            //     82: getfield 56	com/android/internal/os/HandlerCaller$SomeArgs:arg4	Ljava/lang/Object;
            //     85: astore 18
            //     87: aload 18
            //     89: monitorenter
            //     90: aload 16
            //     92: getfield 56	com/android/internal/os/HandlerCaller$SomeArgs:arg4	Ljava/lang/Object;
            //     95: checkcast 58	java/util/concurrent/CountDownLatch
            //     98: invokevirtual 62	java/util/concurrent/CountDownLatch:countDown	()V
            //     101: aload 18
            //     103: monitorexit
            //     104: goto -68 -> 36
            //     107: astore 19
            //     109: aload 18
            //     111: monitorexit
            //     112: aload 19
            //     114: athrow
            //     115: astore 17
            //     117: aload 16
            //     119: getfield 42	com/android/internal/os/HandlerCaller$SomeArgs:arg2	Ljava/lang/Object;
            //     122: checkcast 44	java/io/PrintWriter
            //     125: new 64	java/lang/StringBuilder
            //     128: dup
            //     129: invokespecial 66	java/lang/StringBuilder:<init>	()V
            //     132: ldc 68
            //     134: invokevirtual 72	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     137: aload 17
            //     139: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     142: invokevirtual 79	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     145: invokevirtual 83	java/io/PrintWriter:println	(Ljava/lang/String;)V
            //     148: goto -68 -> 80
            //     151: aload_1
            //     152: getfield 32	android/os/Message:obj	Ljava/lang/Object;
            //     155: checkcast 85	com/android/internal/view/InputBindResult
            //     158: astore 11
            //     160: aload_0
            //     161: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     164: getfield 89	android/view/inputmethod/InputMethodManager:mH	Landroid/view/inputmethod/InputMethodManager$H;
            //     167: astore 12
            //     169: aload 12
            //     171: monitorenter
            //     172: aload_0
            //     173: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     176: getfield 92	android/view/inputmethod/InputMethodManager:mBindSequence	I
            //     179: iflt +18 -> 197
            //     182: aload_0
            //     183: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     186: getfield 92	android/view/inputmethod/InputMethodManager:mBindSequence	I
            //     189: aload 11
            //     191: getfield 95	com/android/internal/view/InputBindResult:sequence	I
            //     194: if_icmpeq +61 -> 255
            //     197: ldc 97
            //     199: new 64	java/lang/StringBuilder
            //     202: dup
            //     203: invokespecial 66	java/lang/StringBuilder:<init>	()V
            //     206: ldc 99
            //     208: invokevirtual 72	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     211: aload_0
            //     212: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     215: getfield 92	android/view/inputmethod/InputMethodManager:mBindSequence	I
            //     218: invokevirtual 102	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     221: ldc 104
            //     223: invokevirtual 72	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     226: aload 11
            //     228: getfield 95	com/android/internal/view/InputBindResult:sequence	I
            //     231: invokevirtual 102	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     234: invokevirtual 79	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     237: invokestatic 110	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     240: pop
            //     241: aload 12
            //     243: monitorexit
            //     244: goto -208 -> 36
            //     247: astore 13
            //     249: aload 12
            //     251: monitorexit
            //     252: aload 13
            //     254: athrow
            //     255: aload_0
            //     256: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     259: aload 11
            //     261: getfield 114	com/android/internal/view/InputBindResult:method	Lcom/android/internal/view/IInputMethodSession;
            //     264: putfield 117	android/view/inputmethod/InputMethodManager:mCurMethod	Lcom/android/internal/view/IInputMethodSession;
            //     267: aload_0
            //     268: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     271: aload 11
            //     273: getfield 121	com/android/internal/view/InputBindResult:id	Ljava/lang/String;
            //     276: putfield 124	android/view/inputmethod/InputMethodManager:mCurId	Ljava/lang/String;
            //     279: aload_0
            //     280: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     283: aload 11
            //     285: getfield 95	com/android/internal/view/InputBindResult:sequence	I
            //     288: putfield 92	android/view/inputmethod/InputMethodManager:mBindSequence	I
            //     291: aload 12
            //     293: monitorexit
            //     294: aload_0
            //     295: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     298: aconst_null
            //     299: iconst_0
            //     300: iconst_0
            //     301: iconst_0
            //     302: invokevirtual 128	android/view/inputmethod/InputMethodManager:startInputInner	(Landroid/os/IBinder;III)Z
            //     305: pop
            //     306: goto -270 -> 36
            //     309: aload_1
            //     310: getfield 130	android/os/Message:arg1	I
            //     313: istore 6
            //     315: iconst_0
            //     316: istore 7
            //     318: aload_0
            //     319: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     322: getfield 89	android/view/inputmethod/InputMethodManager:mH	Landroid/view/inputmethod/InputMethodManager$H;
            //     325: astore 8
            //     327: aload 8
            //     329: monitorenter
            //     330: aload_0
            //     331: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     334: getfield 92	android/view/inputmethod/InputMethodManager:mBindSequence	I
            //     337: iload 6
            //     339: if_icmpne +54 -> 393
            //     342: aload_0
            //     343: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     346: invokevirtual 133	android/view/inputmethod/InputMethodManager:clearBindingLocked	()V
            //     349: aload_0
            //     350: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     353: getfield 137	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
            //     356: ifnull +24 -> 380
            //     359: aload_0
            //     360: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     363: getfield 137	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
            //     366: invokevirtual 143	android/view/View:isFocused	()Z
            //     369: ifeq +11 -> 380
            //     372: aload_0
            //     373: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     376: iconst_1
            //     377: putfield 147	android/view/inputmethod/InputMethodManager:mServedConnecting	Z
            //     380: aload_0
            //     381: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     384: getfield 150	android/view/inputmethod/InputMethodManager:mActive	Z
            //     387: ifeq +6 -> 393
            //     390: iconst_1
            //     391: istore 7
            //     393: aload 8
            //     395: monitorexit
            //     396: iload 7
            //     398: ifeq -362 -> 36
            //     401: aload_0
            //     402: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     405: aconst_null
            //     406: iconst_0
            //     407: iconst_0
            //     408: iconst_0
            //     409: invokevirtual 128	android/view/inputmethod/InputMethodManager:startInputInner	(Landroid/os/IBinder;III)Z
            //     412: pop
            //     413: goto -377 -> 36
            //     416: astore 9
            //     418: aload 8
            //     420: monitorexit
            //     421: aload 9
            //     423: athrow
            //     424: aload_1
            //     425: getfield 130	android/os/Message:arg1	I
            //     428: ifeq +102 -> 530
            //     431: aload_0
            //     432: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     435: getfield 89	android/view/inputmethod/InputMethodManager:mH	Landroid/view/inputmethod/InputMethodManager$H;
            //     438: astore_3
            //     439: aload_3
            //     440: monitorenter
            //     441: aload_0
            //     442: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     445: iload_2
            //     446: putfield 150	android/view/inputmethod/InputMethodManager:mActive	Z
            //     449: aload_0
            //     450: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     453: iconst_0
            //     454: putfield 153	android/view/inputmethod/InputMethodManager:mFullscreenMode	Z
            //     457: iload_2
            //     458: ifne +60 -> 518
            //     461: aload_0
            //     462: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     465: iconst_1
            //     466: putfield 156	android/view/inputmethod/InputMethodManager:mHasBeenInactive	Z
            //     469: aload_0
            //     470: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     473: getfield 160	android/view/inputmethod/InputMethodManager:mIInputContext	Lcom/android/internal/view/IInputContext;
            //     476: invokeinterface 165 1 0
            //     481: aload_0
            //     482: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     485: getfield 137	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
            //     488: ifnull +30 -> 518
            //     491: aload_0
            //     492: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     495: getfield 137	android/view/inputmethod/InputMethodManager:mServedView	Landroid/view/View;
            //     498: invokevirtual 168	android/view/View:hasWindowFocus	()Z
            //     501: ifeq +17 -> 518
            //     504: aload_0
            //     505: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     508: aload_0
            //     509: getfield 13	android/view/inputmethod/InputMethodManager$H:this$0	Landroid/view/inputmethod/InputMethodManager;
            //     512: getfield 156	android/view/inputmethod/InputMethodManager:mHasBeenInactive	Z
            //     515: invokestatic 172	android/view/inputmethod/InputMethodManager:access$000	(Landroid/view/inputmethod/InputMethodManager;Z)V
            //     518: aload_3
            //     519: monitorexit
            //     520: goto -484 -> 36
            //     523: astore 4
            //     525: aload_3
            //     526: monitorexit
            //     527: aload 4
            //     529: athrow
            //     530: iconst_0
            //     531: istore_2
            //     532: goto -101 -> 431
            //     535: astore 5
            //     537: goto -56 -> 481
            //
            // Exception table:
            //     from	to	target	type
            //     90	112	107	finally
            //     46	80	115	java/lang/RuntimeException
            //     172	252	247	finally
            //     255	294	247	finally
            //     330	396	416	finally
            //     418	421	416	finally
            //     441	469	523	finally
            //     469	481	523	finally
            //     481	527	523	finally
            //     469	481	535	android/os/RemoteException
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.inputmethod.InputMethodManager
 * JD-Core Version:        0.6.2
 */