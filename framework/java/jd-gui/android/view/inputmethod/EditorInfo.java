package android.view.inputmethod;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Printer;

public class EditorInfo
    implements InputType, Parcelable
{
    public static final Parcelable.Creator<EditorInfo> CREATOR = new Parcelable.Creator()
    {
        public EditorInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            EditorInfo localEditorInfo = new EditorInfo();
            localEditorInfo.inputType = paramAnonymousParcel.readInt();
            localEditorInfo.imeOptions = paramAnonymousParcel.readInt();
            localEditorInfo.privateImeOptions = paramAnonymousParcel.readString();
            localEditorInfo.actionLabel = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramAnonymousParcel));
            localEditorInfo.actionId = paramAnonymousParcel.readInt();
            localEditorInfo.initialSelStart = paramAnonymousParcel.readInt();
            localEditorInfo.initialSelEnd = paramAnonymousParcel.readInt();
            localEditorInfo.initialCapsMode = paramAnonymousParcel.readInt();
            localEditorInfo.hintText = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramAnonymousParcel));
            localEditorInfo.label = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramAnonymousParcel));
            localEditorInfo.packageName = paramAnonymousParcel.readString();
            localEditorInfo.fieldId = paramAnonymousParcel.readInt();
            localEditorInfo.fieldName = paramAnonymousParcel.readString();
            localEditorInfo.extras = paramAnonymousParcel.readBundle();
            return localEditorInfo;
        }

        public EditorInfo[] newArray(int paramAnonymousInt)
        {
            return new EditorInfo[paramAnonymousInt];
        }
    };
    public static final int IME_ACTION_DONE = 6;
    public static final int IME_ACTION_GO = 2;
    public static final int IME_ACTION_NEXT = 5;
    public static final int IME_ACTION_NONE = 1;
    public static final int IME_ACTION_PREVIOUS = 7;
    public static final int IME_ACTION_SEARCH = 3;
    public static final int IME_ACTION_SEND = 4;
    public static final int IME_ACTION_UNSPECIFIED = 0;
    public static final int IME_FLAG_FORCE_ASCII = -2147483648;
    public static final int IME_FLAG_NAVIGATE_NEXT = 134217728;
    public static final int IME_FLAG_NAVIGATE_PREVIOUS = 67108864;
    public static final int IME_FLAG_NO_ACCESSORY_ACTION = 536870912;
    public static final int IME_FLAG_NO_ENTER_ACTION = 1073741824;
    public static final int IME_FLAG_NO_EXTRACT_UI = 268435456;
    public static final int IME_FLAG_NO_FULLSCREEN = 33554432;
    public static final int IME_MASK_ACTION = 255;
    public static final int IME_NULL;
    public int actionId = 0;
    public CharSequence actionLabel = null;
    public Bundle extras;
    public int fieldId;
    public String fieldName;
    public CharSequence hintText;
    public int imeOptions = 0;
    public int initialCapsMode = 0;
    public int initialSelEnd = -1;
    public int initialSelStart = -1;
    public int inputType = 0;
    public CharSequence label;
    public String packageName;
    public String privateImeOptions = null;

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + "inputType=0x" + Integer.toHexString(this.inputType) + " imeOptions=0x" + Integer.toHexString(this.imeOptions) + " privateImeOptions=" + this.privateImeOptions);
        paramPrinter.println(paramString + "actionLabel=" + this.actionLabel + " actionId=" + this.actionId);
        paramPrinter.println(paramString + "initialSelStart=" + this.initialSelStart + " initialSelEnd=" + this.initialSelEnd + " initialCapsMode=0x" + Integer.toHexString(this.initialCapsMode));
        paramPrinter.println(paramString + "hintText=" + this.hintText + " label=" + this.label);
        paramPrinter.println(paramString + "packageName=" + this.packageName + " fieldId=" + this.fieldId + " fieldName=" + this.fieldName);
        paramPrinter.println(paramString + "extras=" + this.extras);
    }

    public final void makeCompatible(int paramInt)
    {
        if (paramInt < 11)
            switch (0xFFF & this.inputType)
            {
            default:
            case 209:
            case 225:
            case 2:
            case 18:
            }
        while (true)
        {
            return;
            this.inputType = (0x21 | 0xFFF000 & this.inputType);
            continue;
            this.inputType = (0x81 | 0xFFF000 & this.inputType);
            continue;
            this.inputType = (0x2 | 0xFFF000 & this.inputType);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.inputType);
        paramParcel.writeInt(this.imeOptions);
        paramParcel.writeString(this.privateImeOptions);
        TextUtils.writeToParcel(this.actionLabel, paramParcel, paramInt);
        paramParcel.writeInt(this.actionId);
        paramParcel.writeInt(this.initialSelStart);
        paramParcel.writeInt(this.initialSelEnd);
        paramParcel.writeInt(this.initialCapsMode);
        TextUtils.writeToParcel(this.hintText, paramParcel, paramInt);
        TextUtils.writeToParcel(this.label, paramParcel, paramInt);
        paramParcel.writeString(this.packageName);
        paramParcel.writeInt(this.fieldId);
        paramParcel.writeString(this.fieldName);
        paramParcel.writeBundle(this.extras);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.inputmethod.EditorInfo
 * JD-Core Version:        0.6.2
 */