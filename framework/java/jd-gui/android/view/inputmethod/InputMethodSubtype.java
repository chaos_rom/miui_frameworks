package android.view.inputmethod;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Slog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class InputMethodSubtype
    implements Parcelable
{
    public static final Parcelable.Creator<InputMethodSubtype> CREATOR = new Parcelable.Creator()
    {
        public InputMethodSubtype createFromParcel(Parcel paramAnonymousParcel)
        {
            return new InputMethodSubtype(paramAnonymousParcel);
        }

        public InputMethodSubtype[] newArray(int paramAnonymousInt)
        {
            return new InputMethodSubtype[paramAnonymousInt];
        }
    };
    private static final String EXTRA_KEY_UNTRANSLATABLE_STRING_IN_SUBTYPE_NAME = "UntranslatableReplacementStringInSubtypeName";
    private static final String EXTRA_VALUE_KEY_VALUE_SEPARATOR = "=";
    private static final String EXTRA_VALUE_PAIR_SEPARATOR = ",";
    private static final String TAG = InputMethodSubtype.class.getSimpleName();
    private volatile HashMap<String, String> mExtraValueHashMapCache;
    private final boolean mIsAuxiliary;
    private final boolean mOverridesImplicitlyEnabledSubtype;
    private final String mSubtypeExtraValue;
    private final int mSubtypeHashCode;
    private final int mSubtypeIconResId;
    private final String mSubtypeLocale;
    private final String mSubtypeMode;
    private final int mSubtypeNameResId;

    public InputMethodSubtype(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
    {
        this(paramInt1, paramInt2, paramString1, paramString2, paramString3, paramBoolean, false);
    }

    public InputMethodSubtype(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mSubtypeNameResId = paramInt1;
        this.mSubtypeIconResId = paramInt2;
        if (paramString1 != null)
        {
            this.mSubtypeLocale = paramString1;
            if (paramString2 == null)
                break label91;
            label28: this.mSubtypeMode = paramString2;
            if (paramString3 == null)
                break label98;
        }
        while (true)
        {
            this.mSubtypeExtraValue = paramString3;
            this.mIsAuxiliary = paramBoolean1;
            this.mOverridesImplicitlyEnabledSubtype = paramBoolean2;
            this.mSubtypeHashCode = hashCodeInternal(this.mSubtypeLocale, this.mSubtypeMode, this.mSubtypeExtraValue, this.mIsAuxiliary, this.mOverridesImplicitlyEnabledSubtype);
            return;
            paramString1 = "";
            break;
            label91: paramString2 = "";
            break label28;
            label98: paramString3 = "";
        }
    }

    InputMethodSubtype(Parcel paramParcel)
    {
        this.mSubtypeNameResId = paramParcel.readInt();
        this.mSubtypeIconResId = paramParcel.readInt();
        String str1 = paramParcel.readString();
        String str2;
        label47: String str3;
        label64: int j;
        if (str1 != null)
        {
            this.mSubtypeLocale = str1;
            str2 = paramParcel.readString();
            if (str2 == null)
                break label134;
            this.mSubtypeMode = str2;
            str3 = paramParcel.readString();
            if (str3 == null)
                break label141;
            this.mSubtypeExtraValue = str3;
            if (paramParcel.readInt() != i)
                break label148;
            j = i;
            label81: this.mIsAuxiliary = j;
            if (paramParcel.readInt() != i)
                break label154;
        }
        while (true)
        {
            this.mOverridesImplicitlyEnabledSubtype = i;
            this.mSubtypeHashCode = hashCodeInternal(this.mSubtypeLocale, this.mSubtypeMode, this.mSubtypeExtraValue, this.mIsAuxiliary, this.mOverridesImplicitlyEnabledSubtype);
            return;
            str1 = "";
            break;
            label134: str2 = "";
            break label47;
            label141: str3 = "";
            break label64;
            label148: j = 0;
            break label81;
            label154: i = 0;
        }
    }

    private static Locale constructLocaleFromString(String paramString)
    {
        Locale localLocale = null;
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return localLocale;
            String[] arrayOfString = paramString.split("_", 3);
            if (arrayOfString.length == 1)
                localLocale = new Locale(arrayOfString[0]);
            else if (arrayOfString.length == 2)
                localLocale = new Locale(arrayOfString[0], arrayOfString[1]);
            else if (arrayOfString.length == 3)
                localLocale = new Locale(arrayOfString[0], arrayOfString[1], arrayOfString[2]);
        }
    }

    private HashMap<String, String> getExtraValueHashMap()
    {
        if (this.mExtraValueHashMapCache == null);
        while (true)
        {
            int j;
            try
            {
                if (this.mExtraValueHashMapCache == null)
                {
                    this.mExtraValueHashMapCache = new HashMap();
                    String[] arrayOfString1 = this.mSubtypeExtraValue.split(",");
                    int i = arrayOfString1.length;
                    j = 0;
                    if (j < i)
                    {
                        String[] arrayOfString2 = arrayOfString1[j].split("=");
                        if (arrayOfString2.length == 1)
                        {
                            this.mExtraValueHashMapCache.put(arrayOfString2[0], null);
                        }
                        else if (arrayOfString2.length > 1)
                        {
                            if (arrayOfString2.length > 2)
                                Slog.w(TAG, "ExtraValue has two or more '='s");
                            this.mExtraValueHashMapCache.put(arrayOfString2[0], arrayOfString2[1]);
                        }
                    }
                }
            }
            finally
            {
                throw localObject;
            }
        }
    }

    private static int hashCodeInternal(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        arrayOfObject[2] = paramString3;
        arrayOfObject[3] = Boolean.valueOf(paramBoolean1);
        arrayOfObject[4] = Boolean.valueOf(paramBoolean2);
        return Arrays.hashCode(arrayOfObject);
    }

    public static List<InputMethodSubtype> sort(Context paramContext, int paramInt, InputMethodInfo paramInputMethodInfo, List<InputMethodSubtype> paramList)
    {
        if (paramInputMethodInfo == null);
        while (true)
        {
            return paramList;
            HashSet localHashSet = new HashSet(paramList);
            ArrayList localArrayList = new ArrayList();
            int i = paramInputMethodInfo.getSubtypeCount();
            for (int j = 0; j < i; j++)
            {
                InputMethodSubtype localInputMethodSubtype = paramInputMethodInfo.getSubtypeAt(j);
                if (localHashSet.contains(localInputMethodSubtype))
                {
                    localArrayList.add(localInputMethodSubtype);
                    localHashSet.remove(localInputMethodSubtype);
                }
            }
            Iterator localIterator = localHashSet.iterator();
            while (localIterator.hasNext())
                localArrayList.add((InputMethodSubtype)localIterator.next());
            paramList = localArrayList;
        }
    }

    public boolean containsExtraValueKey(String paramString)
    {
        return getExtraValueHashMap().containsKey(paramString);
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof InputMethodSubtype))
        {
            InputMethodSubtype localInputMethodSubtype = (InputMethodSubtype)paramObject;
            if ((localInputMethodSubtype.hashCode() == hashCode()) && (localInputMethodSubtype.getNameResId() == getNameResId()) && (localInputMethodSubtype.getMode().equals(getMode())) && (localInputMethodSubtype.getIconResId() == getIconResId()) && (localInputMethodSubtype.getLocale().equals(getLocale())) && (localInputMethodSubtype.getExtraValue().equals(getExtraValue())) && (localInputMethodSubtype.isAuxiliary() == isAuxiliary()))
                bool = true;
        }
        return bool;
    }

    public CharSequence getDisplayName(Context paramContext, String paramString, ApplicationInfo paramApplicationInfo)
    {
        Locale localLocale = constructLocaleFromString(this.mSubtypeLocale);
        String str1;
        if (localLocale != null)
        {
            str1 = localLocale.getDisplayName();
            if (this.mSubtypeNameResId != 0)
                break label40;
        }
        label40: CharSequence localCharSequence;
        do
        {
            return str1;
            str1 = this.mSubtypeLocale;
            break;
            localCharSequence = paramContext.getPackageManager().getText(paramString, this.mSubtypeNameResId, paramApplicationInfo);
        }
        while (TextUtils.isEmpty(localCharSequence));
        if (containsExtraValueKey("UntranslatableReplacementStringInSubtypeName"));
        for (String str2 = getExtraValueOf("UntranslatableReplacementStringInSubtypeName"); ; str2 = str1)
        {
            try
            {
                String str3 = localCharSequence.toString();
                Object[] arrayOfObject = new Object[1];
                if (str2 != null);
                while (true)
                {
                    arrayOfObject[0] = str2;
                    str1 = String.format(str3, arrayOfObject);
                    break;
                    str2 = "";
                }
            }
            catch (IllegalFormatException localIllegalFormatException)
            {
                Slog.w(TAG, "Found illegal format in subtype name(" + localCharSequence + "): " + localIllegalFormatException);
                str1 = "";
            }
            break;
        }
    }

    public String getExtraValue()
    {
        return this.mSubtypeExtraValue;
    }

    public String getExtraValueOf(String paramString)
    {
        return (String)getExtraValueHashMap().get(paramString);
    }

    public int getIconResId()
    {
        return this.mSubtypeIconResId;
    }

    public String getLocale()
    {
        return this.mSubtypeLocale;
    }

    public String getMode()
    {
        return this.mSubtypeMode;
    }

    public int getNameResId()
    {
        return this.mSubtypeNameResId;
    }

    public int hashCode()
    {
        return this.mSubtypeHashCode;
    }

    public boolean isAuxiliary()
    {
        return this.mIsAuxiliary;
    }

    public boolean overridesImplicitlyEnabledSubtype()
    {
        return this.mOverridesImplicitlyEnabledSubtype;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(this.mSubtypeNameResId);
        paramParcel.writeInt(this.mSubtypeIconResId);
        paramParcel.writeString(this.mSubtypeLocale);
        paramParcel.writeString(this.mSubtypeMode);
        paramParcel.writeString(this.mSubtypeExtraValue);
        int j;
        if (this.mIsAuxiliary)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.mOverridesImplicitlyEnabledSubtype)
                break label77;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 0;
            break;
            label77: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.inputmethod.InputMethodSubtype
 * JD-Core Version:        0.6.2
 */