package android.view.inputmethod;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Printer;
import android.util.Xml;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

public final class InputMethodInfo
    implements Parcelable
{
    public static final Parcelable.Creator<InputMethodInfo> CREATOR = new Parcelable.Creator()
    {
        public InputMethodInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new InputMethodInfo(paramAnonymousParcel);
        }

        public InputMethodInfo[] newArray(int paramAnonymousInt)
        {
            return new InputMethodInfo[paramAnonymousInt];
        }
    };
    static final String TAG = "InputMethodInfo";
    final String mId;
    private boolean mIsAuxIme;
    final int mIsDefaultResId;
    final ResolveInfo mService;
    final String mSettingsActivityName;
    private final ArrayList<InputMethodSubtype> mSubtypes = new ArrayList();

    public InputMethodInfo(Context paramContext, ResolveInfo paramResolveInfo)
        throws XmlPullParserException, IOException
    {
        this(paramContext, paramResolveInfo, null);
    }

    public InputMethodInfo(Context paramContext, ResolveInfo paramResolveInfo, Map<String, List<InputMethodSubtype>> paramMap)
        throws XmlPullParserException, IOException
    {
        this.mService = paramResolveInfo;
        ServiceInfo localServiceInfo = paramResolveInfo.serviceInfo;
        this.mId = new ComponentName(localServiceInfo.packageName, localServiceInfo.name).flattenToShortString();
        this.mIsAuxIme = true;
        PackageManager localPackageManager = paramContext.getPackageManager();
        XmlResourceParser localXmlResourceParser = null;
        try
        {
            localXmlResourceParser = localServiceInfo.loadXmlMetaData(localPackageManager, "android.view.im");
            if (localXmlResourceParser == null)
                throw new XmlPullParserException("No android.view.im meta-data");
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new XmlPullParserException("Unable to create context for: " + localServiceInfo.packageName);
        }
        finally
        {
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
        Resources localResources = localPackageManager.getResourcesForApplication(localServiceInfo.applicationInfo);
        AttributeSet localAttributeSet = Xml.asAttributeSet(localXmlResourceParser);
        int i;
        do
            i = localXmlResourceParser.next();
        while ((i != 1) && (i != 2));
        if (!"input-method".equals(localXmlResourceParser.getName()))
            throw new XmlPullParserException("Meta-data does not start with input-method tag");
        TypedArray localTypedArray1 = localResources.obtainAttributes(localAttributeSet, R.styleable.InputMethod);
        String str = localTypedArray1.getString(1);
        int j = localTypedArray1.getResourceId(0, 0);
        localTypedArray1.recycle();
        int k = localXmlResourceParser.getDepth();
        while (true)
        {
            int m = localXmlResourceParser.next();
            if (((m == 3) && (localXmlResourceParser.getDepth() <= k)) || (m == 1))
                break;
            if (m == 2)
            {
                if (!"subtype".equals(localXmlResourceParser.getName()))
                    throw new XmlPullParserException("Meta-data in input-method does not start with subtype tag");
                TypedArray localTypedArray2 = localResources.obtainAttributes(localAttributeSet, R.styleable.InputMethod_Subtype);
                InputMethodSubtype localInputMethodSubtype2 = new InputMethodSubtype(localTypedArray2.getResourceId(0, 0), localTypedArray2.getResourceId(1, 0), localTypedArray2.getString(2), localTypedArray2.getString(3), localTypedArray2.getString(4), localTypedArray2.getBoolean(5, false), localTypedArray2.getBoolean(6, false));
                if (!localInputMethodSubtype2.isAuxiliary())
                    this.mIsAuxIme = false;
                this.mSubtypes.add(localInputMethodSubtype2);
            }
        }
        if (localXmlResourceParser != null)
            localXmlResourceParser.close();
        if (this.mSubtypes.size() == 0)
            this.mIsAuxIme = false;
        if ((paramMap != null) && (paramMap.containsKey(this.mId)))
        {
            List localList = (List)paramMap.get(this.mId);
            int n = localList.size();
            for (int i1 = 0; i1 < n; i1++)
            {
                InputMethodSubtype localInputMethodSubtype1 = (InputMethodSubtype)localList.get(i1);
                if (!this.mSubtypes.contains(localInputMethodSubtype1))
                    this.mSubtypes.add(localInputMethodSubtype1);
            }
        }
        this.mSettingsActivityName = str;
        this.mIsDefaultResId = j;
    }

    InputMethodInfo(Parcel paramParcel)
    {
        this.mId = paramParcel.readString();
        this.mSettingsActivityName = paramParcel.readString();
        this.mIsDefaultResId = paramParcel.readInt();
        if (paramParcel.readInt() == i);
        while (true)
        {
            this.mIsAuxIme = i;
            this.mService = ((ResolveInfo)ResolveInfo.CREATOR.createFromParcel(paramParcel));
            paramParcel.readTypedList(this.mSubtypes, InputMethodSubtype.CREATOR);
            return;
            i = 0;
        }
    }

    public InputMethodInfo(String paramString1, String paramString2, CharSequence paramCharSequence, String paramString3)
    {
        ResolveInfo localResolveInfo = new ResolveInfo();
        ServiceInfo localServiceInfo = new ServiceInfo();
        ApplicationInfo localApplicationInfo = new ApplicationInfo();
        localApplicationInfo.packageName = paramString1;
        localApplicationInfo.enabled = true;
        localServiceInfo.applicationInfo = localApplicationInfo;
        localServiceInfo.enabled = true;
        localServiceInfo.packageName = paramString1;
        localServiceInfo.name = paramString2;
        localServiceInfo.exported = true;
        localServiceInfo.nonLocalizedLabel = paramCharSequence;
        localResolveInfo.serviceInfo = localServiceInfo;
        this.mService = localResolveInfo;
        this.mId = new ComponentName(localServiceInfo.packageName, localServiceInfo.name).flattenToShortString();
        this.mSettingsActivityName = paramString3;
        this.mIsDefaultResId = 0;
        this.mIsAuxIme = false;
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + "mId=" + this.mId + " mSettingsActivityName=" + this.mSettingsActivityName);
        paramPrinter.println(paramString + "mIsDefaultResId=0x" + Integer.toHexString(this.mIsDefaultResId));
        paramPrinter.println(paramString + "Service:");
        this.mService.dump(paramPrinter, paramString + "    ");
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (paramObject == this);
        InputMethodInfo localInputMethodInfo;
        for (bool = true; ; bool = this.mId.equals(localInputMethodInfo.mId))
        {
            do
                return bool;
            while ((paramObject == null) || (!(paramObject instanceof InputMethodInfo)));
            localInputMethodInfo = (InputMethodInfo)paramObject;
        }
    }

    public ComponentName getComponent()
    {
        return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
    }

    public String getId()
    {
        return this.mId;
    }

    public int getIsDefaultResourceId()
    {
        return this.mIsDefaultResId;
    }

    public String getPackageName()
    {
        return this.mService.serviceInfo.packageName;
    }

    public ServiceInfo getServiceInfo()
    {
        return this.mService.serviceInfo;
    }

    public String getServiceName()
    {
        return this.mService.serviceInfo.name;
    }

    public String getSettingsActivity()
    {
        return this.mSettingsActivityName;
    }

    public InputMethodSubtype getSubtypeAt(int paramInt)
    {
        return (InputMethodSubtype)this.mSubtypes.get(paramInt);
    }

    public int getSubtypeCount()
    {
        return this.mSubtypes.size();
    }

    public int hashCode()
    {
        return this.mId.hashCode();
    }

    public boolean isAuxiliaryIme()
    {
        return this.mIsAuxIme;
    }

    public Drawable loadIcon(PackageManager paramPackageManager)
    {
        return this.mService.loadIcon(paramPackageManager);
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        return this.mService.loadLabel(paramPackageManager);
    }

    public String toString()
    {
        return "InputMethodInfo{" + this.mId + ", settings: " + this.mSettingsActivityName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mId);
        paramParcel.writeString(this.mSettingsActivityName);
        paramParcel.writeInt(this.mIsDefaultResId);
        if (this.mIsAuxIme);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            this.mService.writeToParcel(paramParcel, paramInt);
            paramParcel.writeTypedList(this.mSubtypes);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.inputmethod.InputMethodInfo
 * JD-Core Version:        0.6.2
 */