package android.view;

import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IRemoteCallback;
import android.os.IRemoteCallback.Stub;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.text.TextUtils;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputContext.Stub;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodClient.Stub;
import java.util.List;

public abstract interface IWindowManager extends IInterface
{
    public abstract void addAppToken(int paramInt1, IApplicationToken paramIApplicationToken, int paramInt2, int paramInt3, boolean paramBoolean)
        throws RemoteException;

    public abstract void addWindowToken(IBinder paramIBinder, int paramInt)
        throws RemoteException;

    public abstract void clearForcedDisplaySize()
        throws RemoteException;

    public abstract void closeSystemDialogs(String paramString)
        throws RemoteException;

    public abstract void disableKeyguard(IBinder paramIBinder, String paramString)
        throws RemoteException;

    public abstract void dismissKeyguard()
        throws RemoteException;

    public abstract void executeAppTransition()
        throws RemoteException;

    public abstract void exitKeyguardSecurely(IOnKeyguardExitResult paramIOnKeyguardExitResult)
        throws RemoteException;

    public abstract void freezeRotation(int paramInt)
        throws RemoteException;

    public abstract float getAnimationScale(int paramInt)
        throws RemoteException;

    public abstract float[] getAnimationScales()
        throws RemoteException;

    public abstract int getAppOrientation(IApplicationToken paramIApplicationToken)
        throws RemoteException;

    public abstract void getCurrentSizeRange(Point paramPoint1, Point paramPoint2)
        throws RemoteException;

    public abstract void getDisplaySize(Point paramPoint)
        throws RemoteException;

    public abstract int getMaximumSizeDimension()
        throws RemoteException;

    public abstract int getPendingAppTransition()
        throws RemoteException;

    public abstract int getPreferredOptionsPanelGravity()
        throws RemoteException;

    public abstract void getRealDisplaySize(Point paramPoint)
        throws RemoteException;

    public abstract int getRotation()
        throws RemoteException;

    public abstract boolean hasNavigationBar()
        throws RemoteException;

    public abstract boolean hasSystemNavBar()
        throws RemoteException;

    public abstract boolean inKeyguardRestrictedInputMode()
        throws RemoteException;

    public abstract boolean inputMethodClientHasFocus(IInputMethodClient paramIInputMethodClient)
        throws RemoteException;

    public abstract boolean isKeyguardLocked()
        throws RemoteException;

    public abstract boolean isKeyguardSecure()
        throws RemoteException;

    public abstract boolean isViewServerRunning()
        throws RemoteException;

    public abstract void lockNow()
        throws RemoteException;

    public abstract void moveAppToken(int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public abstract void moveAppTokensToBottom(List<IBinder> paramList)
        throws RemoteException;

    public abstract void moveAppTokensToTop(List<IBinder> paramList)
        throws RemoteException;

    public abstract IWindowSession openSession(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext)
        throws RemoteException;

    public abstract void overridePendingAppTransition(String paramString, int paramInt1, int paramInt2, IRemoteCallback paramIRemoteCallback)
        throws RemoteException;

    public abstract void overridePendingAppTransitionScaleUp(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        throws RemoteException;

    public abstract void overridePendingAppTransitionThumb(Bitmap paramBitmap, int paramInt1, int paramInt2, IRemoteCallback paramIRemoteCallback, boolean paramBoolean)
        throws RemoteException;

    public abstract void pauseKeyDispatching(IBinder paramIBinder)
        throws RemoteException;

    public abstract void prepareAppTransition(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void reenableKeyguard(IBinder paramIBinder)
        throws RemoteException;

    public abstract void removeAppToken(IBinder paramIBinder)
        throws RemoteException;

    public abstract void removeWindowToken(IBinder paramIBinder)
        throws RemoteException;

    public abstract void resumeKeyDispatching(IBinder paramIBinder)
        throws RemoteException;

    public abstract Bitmap screenshotApplications(IBinder paramIBinder, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setAnimationScale(int paramInt, float paramFloat)
        throws RemoteException;

    public abstract void setAnimationScales(float[] paramArrayOfFloat)
        throws RemoteException;

    public abstract void setAppGroupId(IBinder paramIBinder, int paramInt)
        throws RemoteException;

    public abstract void setAppOrientation(IApplicationToken paramIApplicationToken, int paramInt)
        throws RemoteException;

    public abstract void setAppStartingWindow(IBinder paramIBinder1, String paramString, int paramInt1, CompatibilityInfo paramCompatibilityInfo, CharSequence paramCharSequence, int paramInt2, int paramInt3, int paramInt4, IBinder paramIBinder2, boolean paramBoolean)
        throws RemoteException;

    public abstract void setAppVisibility(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException;

    public abstract void setAppWillBeHidden(IBinder paramIBinder)
        throws RemoteException;

    public abstract void setEventDispatching(boolean paramBoolean)
        throws RemoteException;

    public abstract void setFocusedApp(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException;

    public abstract void setForcedDisplaySize(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setInTouchMode(boolean paramBoolean)
        throws RemoteException;

    public abstract void setNewConfiguration(Configuration paramConfiguration)
        throws RemoteException;

    public abstract void setStrictModeVisualIndicatorPreference(String paramString)
        throws RemoteException;

    public abstract void showStrictModeViolation(boolean paramBoolean)
        throws RemoteException;

    public abstract void startAppFreezingScreen(IBinder paramIBinder, int paramInt)
        throws RemoteException;

    public abstract boolean startViewServer(int paramInt)
        throws RemoteException;

    public abstract void statusBarVisibilityChanged(int paramInt)
        throws RemoteException;

    public abstract void stopAppFreezingScreen(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean stopViewServer()
        throws RemoteException;

    public abstract void thawRotation()
        throws RemoteException;

    public abstract Configuration updateOrientationFromAppTokens(Configuration paramConfiguration, IBinder paramIBinder)
        throws RemoteException;

    public abstract void updateRotation(boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract void waitForWindowDrawn(IBinder paramIBinder, IRemoteCallback paramIRemoteCallback)
        throws RemoteException;

    public abstract int watchRotation(IRotationWatcher paramIRotationWatcher)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWindowManager
    {
        private static final String DESCRIPTOR = "android.view.IWindowManager";
        static final int TRANSACTION_addAppToken = 18;
        static final int TRANSACTION_addWindowToken = 16;
        static final int TRANSACTION_clearForcedDisplaySize = 11;
        static final int TRANSACTION_closeSystemDialogs = 47;
        static final int TRANSACTION_disableKeyguard = 40;
        static final int TRANSACTION_dismissKeyguard = 46;
        static final int TRANSACTION_executeAppTransition = 28;
        static final int TRANSACTION_exitKeyguardSecurely = 42;
        static final int TRANSACTION_freezeRotation = 59;
        static final int TRANSACTION_getAnimationScale = 48;
        static final int TRANSACTION_getAnimationScales = 49;
        static final int TRANSACTION_getAppOrientation = 21;
        static final int TRANSACTION_getCurrentSizeRange = 9;
        static final int TRANSACTION_getDisplaySize = 6;
        static final int TRANSACTION_getMaximumSizeDimension = 8;
        static final int TRANSACTION_getPendingAppTransition = 24;
        static final int TRANSACTION_getPreferredOptionsPanelGravity = 58;
        static final int TRANSACTION_getRealDisplaySize = 7;
        static final int TRANSACTION_getRotation = 56;
        static final int TRANSACTION_hasNavigationBar = 64;
        static final int TRANSACTION_hasSystemNavBar = 12;
        static final int TRANSACTION_inKeyguardRestrictedInputMode = 45;
        static final int TRANSACTION_inputMethodClientHasFocus = 5;
        static final int TRANSACTION_isKeyguardLocked = 43;
        static final int TRANSACTION_isKeyguardSecure = 44;
        static final int TRANSACTION_isViewServerRunning = 3;
        static final int TRANSACTION_lockNow = 65;
        static final int TRANSACTION_moveAppToken = 35;
        static final int TRANSACTION_moveAppTokensToBottom = 37;
        static final int TRANSACTION_moveAppTokensToTop = 36;
        static final int TRANSACTION_openSession = 4;
        static final int TRANSACTION_overridePendingAppTransition = 25;
        static final int TRANSACTION_overridePendingAppTransitionScaleUp = 26;
        static final int TRANSACTION_overridePendingAppTransitionThumb = 27;
        static final int TRANSACTION_pauseKeyDispatching = 13;
        static final int TRANSACTION_prepareAppTransition = 23;
        static final int TRANSACTION_reenableKeyguard = 41;
        static final int TRANSACTION_removeAppToken = 34;
        static final int TRANSACTION_removeWindowToken = 17;
        static final int TRANSACTION_resumeKeyDispatching = 14;
        static final int TRANSACTION_screenshotApplications = 61;
        static final int TRANSACTION_setAnimationScale = 50;
        static final int TRANSACTION_setAnimationScales = 51;
        static final int TRANSACTION_setAppGroupId = 19;
        static final int TRANSACTION_setAppOrientation = 20;
        static final int TRANSACTION_setAppStartingWindow = 29;
        static final int TRANSACTION_setAppVisibility = 31;
        static final int TRANSACTION_setAppWillBeHidden = 30;
        static final int TRANSACTION_setEventDispatching = 15;
        static final int TRANSACTION_setFocusedApp = 22;
        static final int TRANSACTION_setForcedDisplaySize = 10;
        static final int TRANSACTION_setInTouchMode = 52;
        static final int TRANSACTION_setNewConfiguration = 39;
        static final int TRANSACTION_setStrictModeVisualIndicatorPreference = 54;
        static final int TRANSACTION_showStrictModeViolation = 53;
        static final int TRANSACTION_startAppFreezingScreen = 32;
        static final int TRANSACTION_startViewServer = 1;
        static final int TRANSACTION_statusBarVisibilityChanged = 62;
        static final int TRANSACTION_stopAppFreezingScreen = 33;
        static final int TRANSACTION_stopViewServer = 2;
        static final int TRANSACTION_thawRotation = 60;
        static final int TRANSACTION_updateOrientationFromAppTokens = 38;
        static final int TRANSACTION_updateRotation = 55;
        static final int TRANSACTION_waitForWindowDrawn = 63;
        static final int TRANSACTION_watchRotation = 57;

        public Stub()
        {
            attachInterface(this, "android.view.IWindowManager");
        }

        public static IWindowManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.IWindowManager");
                if ((localIInterface != null) && ((localIInterface instanceof IWindowManager)))
                    localObject = (IWindowManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.view.IWindowManager");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool22 = startViewServer(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool22);
                for (int i20 = 1; ; i20 = 0)
                {
                    paramParcel2.writeInt(i20);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool21 = stopViewServer();
                paramParcel2.writeNoException();
                if (bool21);
                for (int i19 = 1; ; i19 = 0)
                {
                    paramParcel2.writeInt(i19);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool20 = isViewServerRunning();
                paramParcel2.writeNoException();
                if (bool20);
                for (int i18 = 1; ; i18 = 0)
                {
                    paramParcel2.writeInt(i18);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                IWindowSession localIWindowSession = openSession(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()), IInputContext.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                if (localIWindowSession != null);
                for (IBinder localIBinder6 = localIWindowSession.asBinder(); ; localIBinder6 = null)
                {
                    paramParcel2.writeStrongBinder(localIBinder6);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool19 = inputMethodClientHasFocus(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                if (bool19);
                for (int i17 = 1; ; i17 = 0)
                {
                    paramParcel2.writeInt(i17);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                Point localPoint4 = new Point();
                getDisplaySize(localPoint4);
                paramParcel2.writeNoException();
                if (localPoint4 != null)
                {
                    paramParcel2.writeInt(1);
                    localPoint4.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                Point localPoint3 = new Point();
                getRealDisplaySize(localPoint3);
                paramParcel2.writeNoException();
                if (localPoint3 != null)
                {
                    paramParcel2.writeInt(1);
                    localPoint3.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int i16 = getMaximumSizeDimension();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i16);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                Point localPoint1 = new Point();
                Point localPoint2 = new Point();
                getCurrentSizeRange(localPoint1, localPoint2);
                paramParcel2.writeNoException();
                if (localPoint1 != null)
                {
                    paramParcel2.writeInt(1);
                    localPoint1.writeToParcel(paramParcel2, 1);
                    label1000: if (localPoint2 == null)
                        break label1031;
                    paramParcel2.writeInt(1);
                    localPoint2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                    break label1000;
                    label1031: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setForcedDisplaySize(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                clearForcedDisplaySize();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool18 = hasSystemNavBar();
                paramParcel2.writeNoException();
                if (bool18);
                for (int i15 = 1; ; i15 = 0)
                {
                    paramParcel2.writeInt(i15);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                pauseKeyDispatching(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                resumeKeyDispatching(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                if (paramParcel1.readInt() != 0);
                for (boolean bool17 = true; ; bool17 = false)
                {
                    setEventDispatching(bool17);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                addWindowToken(paramParcel1.readStrongBinder(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                removeWindowToken(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int i12 = paramParcel1.readInt();
                IApplicationToken localIApplicationToken = IApplicationToken.Stub.asInterface(paramParcel1.readStrongBinder());
                int i13 = paramParcel1.readInt();
                int i14 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool16 = true; ; bool16 = false)
                {
                    addAppToken(i12, localIApplicationToken, i13, i14, bool16);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setAppGroupId(paramParcel1.readStrongBinder(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setAppOrientation(IApplicationToken.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int i11 = getAppOrientation(IApplicationToken.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i11);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                IBinder localIBinder5 = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (boolean bool15 = true; ; bool15 = false)
                {
                    setFocusedApp(localIBinder5, bool15);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int i10 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool14 = true; ; bool14 = false)
                {
                    prepareAppTransition(i10, bool14);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int i9 = getPendingAppTransition();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i9);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                overridePendingAppTransition(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt(), IRemoteCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                overridePendingAppTransitionScaleUp(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                Bitmap localBitmap2;
                label1656: int i7;
                int i8;
                IRemoteCallback localIRemoteCallback;
                if (paramParcel1.readInt() != 0)
                {
                    localBitmap2 = (Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel1);
                    i7 = paramParcel1.readInt();
                    i8 = paramParcel1.readInt();
                    localIRemoteCallback = IRemoteCallback.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() == 0)
                        break label1717;
                }
                label1717: for (boolean bool13 = true; ; bool13 = false)
                {
                    overridePendingAppTransitionThumb(localBitmap2, i7, i8, localIRemoteCallback, bool13);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    localBitmap2 = null;
                    break label1656;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                executeAppTransition();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                IBinder localIBinder3 = paramParcel1.readStrongBinder();
                String str = paramParcel1.readString();
                int i3 = paramParcel1.readInt();
                CompatibilityInfo localCompatibilityInfo;
                label1788: CharSequence localCharSequence;
                label1809: int i4;
                int i5;
                int i6;
                IBinder localIBinder4;
                if (paramParcel1.readInt() != 0)
                {
                    localCompatibilityInfo = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label1883;
                    localCharSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1);
                    i4 = paramParcel1.readInt();
                    i5 = paramParcel1.readInt();
                    i6 = paramParcel1.readInt();
                    localIBinder4 = paramParcel1.readStrongBinder();
                    if (paramParcel1.readInt() == 0)
                        break label1889;
                }
                label1883: label1889: for (boolean bool12 = true; ; bool12 = false)
                {
                    setAppStartingWindow(localIBinder3, str, i3, localCompatibilityInfo, localCharSequence, i4, i5, i6, localIBinder4, bool12);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    localCompatibilityInfo = null;
                    break label1788;
                    localCharSequence = null;
                    break label1809;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setAppWillBeHidden(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                IBinder localIBinder2 = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (boolean bool11 = true; ; bool11 = false)
                {
                    setAppVisibility(localIBinder2, bool11);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                startAppFreezingScreen(paramParcel1.readStrongBinder(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                IBinder localIBinder1 = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (boolean bool10 = true; ; bool10 = false)
                {
                    stopAppFreezingScreen(localIBinder1, bool10);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                removeAppToken(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                moveAppToken(paramParcel1.readInt(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                moveAppTokensToTop(paramParcel1.createBinderArrayList());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                moveAppTokensToBottom(paramParcel1.createBinderArrayList());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                Configuration localConfiguration2;
                if (paramParcel1.readInt() != 0)
                {
                    localConfiguration2 = (Configuration)Configuration.CREATOR.createFromParcel(paramParcel1);
                    label2166: Configuration localConfiguration3 = updateOrientationFromAppTokens(localConfiguration2, paramParcel1.readStrongBinder());
                    paramParcel2.writeNoException();
                    if (localConfiguration3 == null)
                        break label2211;
                    paramParcel2.writeInt(1);
                    localConfiguration3.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localConfiguration2 = null;
                    break label2166;
                    label2211: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                if (paramParcel1.readInt() != 0);
                for (Configuration localConfiguration1 = (Configuration)Configuration.CREATOR.createFromParcel(paramParcel1); ; localConfiguration1 = null)
                {
                    setNewConfiguration(localConfiguration1);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                disableKeyguard(paramParcel1.readStrongBinder(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                reenableKeyguard(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                exitKeyguardSecurely(IOnKeyguardExitResult.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool9 = isKeyguardLocked();
                paramParcel2.writeNoException();
                if (bool9);
                for (int i2 = 1; ; i2 = 0)
                {
                    paramParcel2.writeInt(i2);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool8 = isKeyguardSecure();
                paramParcel2.writeNoException();
                if (bool8);
                for (int i1 = 1; ; i1 = 0)
                {
                    paramParcel2.writeInt(i1);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool7 = inKeyguardRestrictedInputMode();
                paramParcel2.writeNoException();
                if (bool7);
                for (int n = 1; ; n = 0)
                {
                    paramParcel2.writeInt(n);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                dismissKeyguard();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                closeSystemDialogs(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                float f = getAnimationScale(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeFloat(f);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                float[] arrayOfFloat = getAnimationScales();
                paramParcel2.writeNoException();
                paramParcel2.writeFloatArray(arrayOfFloat);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setAnimationScale(paramParcel1.readInt(), paramParcel1.readFloat());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setAnimationScales(paramParcel1.createFloatArray());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                if (paramParcel1.readInt() != 0);
                for (boolean bool6 = true; ; bool6 = false)
                {
                    setInTouchMode(bool6);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                if (paramParcel1.readInt() != 0);
                for (boolean bool5 = true; ; bool5 = false)
                {
                    showStrictModeViolation(bool5);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                setStrictModeVisualIndicatorPreference(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool3;
                if (paramParcel1.readInt() != 0)
                {
                    bool3 = true;
                    label2745: if (paramParcel1.readInt() == 0)
                        break label2779;
                }
                label2779: for (boolean bool4 = true; ; bool4 = false)
                {
                    updateRotation(bool3, bool4);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    bool3 = false;
                    break label2745;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int m = getRotation();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int k = watchRotation(IRotationWatcher.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                int j = getPreferredOptionsPanelGravity();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                freezeRotation(paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                thawRotation();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                Bitmap localBitmap1 = screenshotApplications(paramParcel1.readStrongBinder(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localBitmap1 != null)
                {
                    paramParcel2.writeInt(1);
                    localBitmap1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                statusBarVisibilityChanged(paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                waitForWindowDrawn(paramParcel1.readStrongBinder(), IRemoteCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.view.IWindowManager");
                boolean bool2 = hasNavigationBar();
                paramParcel2.writeNoException();
                if (bool2);
                for (int i = 1; ; i = 0)
                {
                    paramParcel2.writeInt(i);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.view.IWindowManager");
                lockNow();
                paramParcel2.writeNoException();
                bool1 = true;
            }
        }

        private static class Proxy
            implements IWindowManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addAppToken(int paramInt1, IApplicationToken paramIApplicationToken, int paramInt2, int paramInt3, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt1);
                    if (paramIApplicationToken != null)
                    {
                        localIBinder = paramIApplicationToken.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt2);
                        localParcel1.writeInt(paramInt3);
                        if (paramBoolean)
                            i = 1;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(18, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void addWindowToken(IBinder paramIBinder, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearForcedDisplaySize()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void closeSystemDialogs(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(47, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void disableKeyguard(IBinder paramIBinder, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(40, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void dismissKeyguard()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(46, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void executeAppTransition()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(28, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void exitKeyguardSecurely(IOnKeyguardExitResult paramIOnKeyguardExitResult)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramIOnKeyguardExitResult != null)
                    {
                        localIBinder = paramIOnKeyguardExitResult.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(42, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void freezeRotation(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(59, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public float getAnimationScale(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(48, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    float f = localParcel2.readFloat();
                    return f;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public float[] getAnimationScales()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(49, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    float[] arrayOfFloat = localParcel2.createFloatArray();
                    return arrayOfFloat;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getAppOrientation(IApplicationToken paramIApplicationToken)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramIApplicationToken != null)
                    {
                        localIBinder = paramIApplicationToken.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(21, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getCurrentSizeRange(Point paramPoint1, Point paramPoint2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                        paramPoint1.readFromParcel(localParcel2);
                    if (localParcel2.readInt() != 0)
                        paramPoint2.readFromParcel(localParcel2);
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getDisplaySize(Point paramPoint)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                        paramPoint.readFromParcel(localParcel2);
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.IWindowManager";
            }

            public int getMaximumSizeDimension()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPendingAppTransition()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPreferredOptionsPanelGravity()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(58, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getRealDisplaySize(Point paramPoint)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                        paramPoint.readFromParcel(localParcel2);
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getRotation()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(56, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasNavigationBar()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(64, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasSystemNavBar()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean inKeyguardRestrictedInputMode()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(45, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean inputMethodClientHasFocus(IInputMethodClient paramIInputMethodClient)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        if (i != 0)
                            bool = true;
                        return bool;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isKeyguardLocked()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(43, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isKeyguardSecure()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(44, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isViewServerRunning()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void lockNow()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(65, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void moveAppToken(int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void moveAppTokensToBottom(List<IBinder> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeBinderList(paramList);
                    this.mRemote.transact(37, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void moveAppTokensToTop(List<IBinder> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeBinderList(paramList);
                    this.mRemote.transact(36, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IWindowSession openSession(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext)
                throws RemoteException
            {
                IBinder localIBinder1 = null;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder2 = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder2);
                        if (paramIInputContext != null)
                            localIBinder1 = paramIInputContext.asBinder();
                        localParcel1.writeStrongBinder(localIBinder1);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        IWindowSession localIWindowSession = IWindowSession.Stub.asInterface(localParcel2.readStrongBinder());
                        return localIWindowSession;
                    }
                    IBinder localIBinder2 = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void overridePendingAppTransition(String paramString, int paramInt1, int paramInt2, IRemoteCallback paramIRemoteCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    if (paramIRemoteCallback != null)
                    {
                        localIBinder = paramIRemoteCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(25, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void overridePendingAppTransitionScaleUp(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    localParcel1.writeInt(paramInt4);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void overridePendingAppTransitionThumb(Bitmap paramBitmap, int paramInt1, int paramInt2, IRemoteCallback paramIRemoteCallback, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.IWindowManager");
                        if (paramBitmap != null)
                        {
                            localParcel1.writeInt(1);
                            paramBitmap.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            if (paramIRemoteCallback != null)
                            {
                                localIBinder = paramIRemoteCallback.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                if (!paramBoolean)
                                    break label145;
                                localParcel1.writeInt(i);
                                this.mRemote.transact(27, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                    continue;
                    label145: i = 0;
                }
            }

            public void pauseKeyDispatching(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void prepareAppTransition(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reenableKeyguard(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(41, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeAppToken(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(34, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeWindowToken(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void resumeKeyDispatching(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Bitmap screenshotApplications(IBinder paramIBinder, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(61, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(localParcel2);
                        return localBitmap;
                    }
                    Bitmap localBitmap = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAnimationScale(int paramInt, float paramFloat)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeFloat(paramFloat);
                    this.mRemote.transact(50, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAnimationScales(float[] paramArrayOfFloat)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeFloatArray(paramArrayOfFloat);
                    this.mRemote.transact(51, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAppGroupId(IBinder paramIBinder, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAppOrientation(IApplicationToken paramIApplicationToken, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramIApplicationToken != null)
                    {
                        localIBinder = paramIApplicationToken.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAppStartingWindow(IBinder paramIBinder1, String paramString, int paramInt1, CompatibilityInfo paramCompatibilityInfo, CharSequence paramCharSequence, int paramInt2, int paramInt3, int paramInt4, IBinder paramIBinder2, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.IWindowManager");
                        localParcel1.writeStrongBinder(paramIBinder1);
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt1);
                        if (paramCompatibilityInfo != null)
                        {
                            localParcel1.writeInt(1);
                            paramCompatibilityInfo.writeToParcel(localParcel1, 0);
                            if (paramCharSequence != null)
                            {
                                localParcel1.writeInt(1);
                                TextUtils.writeToParcel(paramCharSequence, localParcel1, 0);
                                localParcel1.writeInt(paramInt2);
                                localParcel1.writeInt(paramInt3);
                                localParcel1.writeInt(paramInt4);
                                localParcel1.writeStrongBinder(paramIBinder2);
                                if (!paramBoolean)
                                    break label182;
                                localParcel1.writeInt(i);
                                this.mRemote.transact(29, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label182: i = 0;
                }
            }

            public void setAppVisibility(IBinder paramIBinder, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(31, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAppWillBeHidden(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setEventDispatching(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setFocusedApp(IBinder paramIBinder, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setForcedDisplaySize(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInTouchMode(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(52, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setNewConfiguration(Configuration paramConfiguration)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(39, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setStrictModeVisualIndicatorPreference(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(54, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void showStrictModeViolation(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(53, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startAppFreezingScreen(IBinder paramIBinder, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean startViewServer(int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        return bool;
                    bool = false;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void statusBarVisibilityChanged(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(62, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopAppFreezingScreen(IBinder paramIBinder, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean stopViewServer()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void thawRotation()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    this.mRemote.transact(60, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Configuration updateOrientationFromAppTokens(Configuration paramConfiguration, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.IWindowManager");
                        if (paramConfiguration != null)
                        {
                            localParcel1.writeInt(1);
                            paramConfiguration.writeToParcel(localParcel1, 0);
                            localParcel1.writeStrongBinder(paramIBinder);
                            this.mRemote.transact(38, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localConfiguration = (Configuration)Configuration.CREATOR.createFromParcel(localParcel2);
                                return localConfiguration;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    Configuration localConfiguration = null;
                }
            }

            public void updateRotation(boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramBoolean1);
                    for (int j = i; ; j = 0)
                    {
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(55, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void waitForWindowDrawn(IBinder paramIBinder, IRemoteCallback paramIRemoteCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramIRemoteCallback != null)
                    {
                        localIBinder = paramIRemoteCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(63, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int watchRotation(IRotationWatcher paramIRotationWatcher)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowManager");
                    if (paramIRotationWatcher != null)
                    {
                        localIBinder = paramIRotationWatcher.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(57, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.IWindowManager
 * JD-Core Version:        0.6.2
 */