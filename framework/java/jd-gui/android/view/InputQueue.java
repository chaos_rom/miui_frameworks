package android.view;

public final class InputQueue
{
    final InputChannel mChannel;

    public InputQueue(InputChannel paramInputChannel)
    {
        this.mChannel = paramInputChannel;
    }

    public InputChannel getInputChannel()
    {
        return this.mChannel;
    }

    public static abstract interface Callback
    {
        public abstract void onInputQueueCreated(InputQueue paramInputQueue);

        public abstract void onInputQueueDestroyed(InputQueue paramInputQueue);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InputQueue
 * JD-Core Version:        0.6.2
 */