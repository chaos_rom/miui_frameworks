package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class InputEvent
    implements Parcelable
{
    public static final Parcelable.Creator<InputEvent> CREATOR = new Parcelable.Creator()
    {
        public InputEvent createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = paramAnonymousParcel.readInt();
            if (i == 2);
            for (Object localObject = KeyEvent.createFromParcelBody(paramAnonymousParcel); ; localObject = MotionEvent.createFromParcelBody(paramAnonymousParcel))
            {
                return localObject;
                if (i != 1)
                    break;
            }
            throw new IllegalStateException("Unexpected input event type token in parcel.");
        }

        public InputEvent[] newArray(int paramAnonymousInt)
        {
            return new InputEvent[paramAnonymousInt];
        }
    };
    protected static final int PARCEL_TOKEN_KEY_EVENT = 2;
    protected static final int PARCEL_TOKEN_MOTION_EVENT = 1;
    private static final boolean TRACK_RECYCLED_LOCATION;
    private static final AtomicInteger mNextSeq = new AtomicInteger();
    protected boolean mRecycled;
    private RuntimeException mRecycledLocation;
    protected int mSeq = mNextSeq.getAndIncrement();

    public abstract InputEvent copy();

    public int describeContents()
    {
        return 0;
    }

    public final InputDevice getDevice()
    {
        return InputDevice.getDevice(getDeviceId());
    }

    public abstract int getDeviceId();

    public abstract long getEventTime();

    public abstract long getEventTimeNano();

    public int getSequenceNumber()
    {
        return this.mSeq;
    }

    public abstract int getSource();

    public abstract boolean isTainted();

    protected void prepareForReuse()
    {
        this.mRecycled = false;
        this.mRecycledLocation = null;
        this.mSeq = mNextSeq.getAndIncrement();
    }

    public void recycle()
    {
        if (this.mRecycled)
            throw new RuntimeException(toString() + " recycled twice!");
        this.mRecycled = true;
    }

    public void recycleIfNeededAfterDispatch()
    {
        recycle();
    }

    public abstract void setSource(int paramInt);

    public abstract void setTainted(boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InputEvent
 * JD-Core Version:        0.6.2
 */