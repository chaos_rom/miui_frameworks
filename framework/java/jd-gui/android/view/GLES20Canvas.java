package android.view;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas.EdgeType;
import android.graphics.Canvas.VertexMode;
import android.graphics.ColorFilter;
import android.graphics.DrawFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Region.Op;
import android.graphics.Shader;
import android.graphics.SurfaceTexture;
import android.graphics.TemporaryBuffer;
import android.text.GraphicsOperations;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;

class GLES20Canvas extends HardwareCanvas
{
    public static final int FLUSH_CACHES_FULL = 2;
    public static final int FLUSH_CACHES_LAYERS = 0;
    public static final int FLUSH_CACHES_MODERATE = 1;
    private static final int MODIFIER_COLOR_FILTER = 4;
    private static final int MODIFIER_NONE = 0;
    private static final int MODIFIER_SHADER = 2;
    private static final int MODIFIER_SHADOW = 1;
    private static boolean sIsAvailable = nIsAvailable();
    private final Rect mClipBounds = new Rect();
    private DrawFilter mFilter;
    private CanvasFinalizer mFinalizer;
    private int mHeight;
    private final float[] mLine = new float[4];
    private final boolean mOpaque;
    private final RectF mPathBounds = new RectF();
    private final float[] mPoint = new float[2];
    private int mRenderer;
    private int mWidth;

    GLES20Canvas(int paramInt, boolean paramBoolean)
    {
        if (!paramBoolean);
        for (boolean bool = true; ; bool = false)
        {
            this.mOpaque = bool;
            this.mRenderer = nCreateLayerRenderer(paramInt);
            setupFinalizer();
            return;
        }
    }

    GLES20Canvas(boolean paramBoolean)
    {
        this(false, paramBoolean);
    }

    protected GLES20Canvas(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool;
        if (!paramBoolean2)
        {
            bool = true;
            this.mOpaque = bool;
            if (!paramBoolean1)
                break label72;
        }
        label72: for (this.mRenderer = nCreateDisplayListRenderer(); ; this.mRenderer = nCreateRenderer())
        {
            setupFinalizer();
            return;
            bool = false;
            break;
        }
    }

    static void destroyDisplayList(int paramInt)
    {
        nDestroyDisplayList(paramInt);
    }

    public static void flushCaches(int paramInt)
    {
        nFlushCaches(paramInt);
    }

    static int getDisplayListSize(int paramInt)
    {
        return nGetDisplayListSize(paramInt);
    }

    public static int getStencilSize()
    {
        return nGetStencilSize();
    }

    public static void initCaches()
    {
        nInitCaches();
    }

    static boolean isAvailable()
    {
        return sIsAvailable;
    }

    private static native void nAttachFunctor(int paramInt1, int paramInt2);

    private static native int nCallDrawGLFunction(int paramInt1, int paramInt2);

    private static native boolean nClipRect(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native boolean nClipRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private static native void nConcatMatrix(int paramInt1, int paramInt2);

    static native boolean nCopyLayer(int paramInt1, int paramInt2);

    private static native int nCreateDisplayListRenderer();

    static native int nCreateLayer(int paramInt1, int paramInt2, boolean paramBoolean, int[] paramArrayOfInt);

    private static native int nCreateLayerRenderer(int paramInt);

    private static native int nCreateRenderer();

    static native int nCreateTextureLayer(boolean paramBoolean, int[] paramArrayOfInt);

    private static native void nDestroyDisplayList(int paramInt);

    static native void nDestroyLayer(int paramInt);

    static native void nDestroyLayerDeferred(int paramInt);

    private static native void nDestroyRenderer(int paramInt);

    private static native void nDetachFunctor(int paramInt1, int paramInt2);

    private static native void nDrawArc(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, boolean paramBoolean, int paramInt2);

    private static native void nDrawBitmap(int paramInt1, int paramInt2, byte[] paramArrayOfByte, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, int paramInt3);

    private static native void nDrawBitmap(int paramInt1, int paramInt2, byte[] paramArrayOfByte, float paramFloat1, float paramFloat2, int paramInt3);

    private static native void nDrawBitmap(int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3, int paramInt4);

    private static native void nDrawBitmap(int paramInt1, int[] paramArrayOfInt, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5, boolean paramBoolean, int paramInt6);

    private static native void nDrawBitmapMesh(int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3, int paramInt4, float[] paramArrayOfFloat, int paramInt5, int[] paramArrayOfInt, int paramInt6, int paramInt7);

    private static native void nDrawCircle(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt2);

    private static native void nDrawColor(int paramInt1, int paramInt2, int paramInt3);

    private static native int nDrawDisplayList(int paramInt1, int paramInt2, Rect paramRect, int paramInt3);

    private static native void nDrawLayer(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3);

    private static native void nDrawLines(int paramInt1, float[] paramArrayOfFloat, int paramInt2, int paramInt3, int paramInt4);

    private static native void nDrawOval(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native void nDrawPatch(int paramInt1, int paramInt2, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt3);

    private static native void nDrawPath(int paramInt1, int paramInt2, int paramInt3);

    private static native void nDrawPoints(int paramInt1, float[] paramArrayOfFloat, int paramInt2, int paramInt3, int paramInt4);

    private static native void nDrawPosText(int paramInt1, String paramString, int paramInt2, int paramInt3, float[] paramArrayOfFloat, int paramInt4);

    private static native void nDrawPosText(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, float[] paramArrayOfFloat, int paramInt4);

    private static native void nDrawRect(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native void nDrawRects(int paramInt1, int paramInt2, int paramInt3);

    private static native void nDrawRoundRect(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, int paramInt2);

    private static native void nDrawText(int paramInt1, String paramString, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5);

    private static native void nDrawText(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5);

    private static native void nDrawTextOnPath(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, int paramInt6);

    private static native void nDrawTextOnPath(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, int paramInt6);

    private static native void nDrawTextRun(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat1, float paramFloat2, int paramInt6, int paramInt7);

    private static native void nDrawTextRun(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat1, float paramFloat2, int paramInt6, int paramInt7);

    private static native void nFinish(int paramInt);

    private static native void nFlushCaches(int paramInt);

    static native void nFlushLayer(int paramInt);

    private static native boolean nGetClipBounds(int paramInt, Rect paramRect);

    private static native int nGetDisplayList(int paramInt1, int paramInt2);

    private static native int nGetDisplayListSize(int paramInt);

    private static native void nGetMatrix(int paramInt1, int paramInt2);

    private static native int nGetMaximumTextureHeight();

    private static native int nGetMaximumTextureWidth();

    private static native int nGetSaveCount(int paramInt);

    private static native int nGetStencilSize();

    private static native void nInitCaches();

    private static native void nInterrupt(int paramInt);

    private static native int nInvokeFunctors(int paramInt, Rect paramRect);

    private static native boolean nIsAvailable();

    private static native void nOutputDisplayList(int paramInt1, int paramInt2);

    private static native int nPrepare(int paramInt, boolean paramBoolean);

    private static native int nPrepareDirty(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean);

    private static native boolean nQuickReject(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native void nResetDisplayListRenderer(int paramInt);

    private static native void nResetModifiers(int paramInt1, int paramInt2);

    private static native void nResetPaintFilter(int paramInt);

    static native void nResizeLayer(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt);

    private static native void nRestore(int paramInt);

    private static native void nRestoreToCount(int paramInt1, int paramInt2);

    private static native void nResume(int paramInt);

    private static native void nRotate(int paramInt, float paramFloat);

    private static native int nSave(int paramInt1, int paramInt2);

    private static native int nSaveLayer(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, int paramInt3);

    private static native int nSaveLayer(int paramInt1, int paramInt2, int paramInt3);

    private static native int nSaveLayerAlpha(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, int paramInt3);

    private static native int nSaveLayerAlpha(int paramInt1, int paramInt2, int paramInt3);

    private static native void nScale(int paramInt, float paramFloat1, float paramFloat2);

    private static native void nSetDisplayListName(int paramInt, String paramString);

    private static native void nSetMatrix(int paramInt1, int paramInt2);

    static native void nSetTextureLayerTransform(int paramInt1, int paramInt2);

    private static native void nSetViewport(int paramInt1, int paramInt2, int paramInt3);

    private static native void nSetupColorFilter(int paramInt1, int paramInt2);

    private static native void nSetupPaintFilter(int paramInt1, int paramInt2, int paramInt3);

    private static native void nSetupShader(int paramInt1, int paramInt2);

    private static native void nSetupShadow(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt2);

    private static native void nSkew(int paramInt, float paramFloat1, float paramFloat2);

    private static native void nTerminateCaches();

    private static native void nTranslate(int paramInt, float paramFloat1, float paramFloat2);

    static native void nUpdateRenderLayer(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);

    static native void nUpdateTextureLayer(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, SurfaceTexture paramSurfaceTexture);

    static void setDisplayListName(int paramInt, String paramString)
    {
        nSetDisplayListName(paramInt, paramString);
    }

    private int setupColorFilter(Paint paramPaint)
    {
        ColorFilter localColorFilter = paramPaint.getColorFilter();
        if (localColorFilter != null)
            nSetupColorFilter(this.mRenderer, localColorFilter.nativeColorFilter);
        for (int i = 4; ; i = 0)
            return i;
    }

    private void setupFinalizer()
    {
        if (this.mRenderer == 0)
            throw new IllegalStateException("Could not create GLES20Canvas renderer");
        this.mFinalizer = new CanvasFinalizer(this.mRenderer);
    }

    private int setupModifiers(Bitmap paramBitmap, Paint paramPaint)
    {
        int i;
        if (paramBitmap.getConfig() != Bitmap.Config.ALPHA_8)
        {
            ColorFilter localColorFilter = paramPaint.getColorFilter();
            if (localColorFilter != null)
            {
                nSetupColorFilter(this.mRenderer, localColorFilter.nativeColorFilter);
                i = 4;
            }
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = setupModifiers(paramPaint);
        }
    }

    private int setupModifiers(Paint paramPaint)
    {
        int i = 0;
        if (paramPaint.hasShadow)
        {
            nSetupShadow(this.mRenderer, paramPaint.shadowRadius, paramPaint.shadowDx, paramPaint.shadowDy, paramPaint.shadowColor);
            i = 0x0 | 0x1;
        }
        Shader localShader = paramPaint.getShader();
        if (localShader != null)
        {
            nSetupShader(this.mRenderer, localShader.native_shader);
            i |= 2;
        }
        ColorFilter localColorFilter = paramPaint.getColorFilter();
        if (localColorFilter != null)
        {
            nSetupColorFilter(this.mRenderer, localColorFilter.nativeColorFilter);
            i |= 4;
        }
        return i;
    }

    private int setupModifiers(Paint paramPaint, int paramInt)
    {
        int i = 0;
        if ((paramPaint.hasShadow) && ((paramInt & 0x1) != 0))
        {
            nSetupShadow(this.mRenderer, paramPaint.shadowRadius, paramPaint.shadowDx, paramPaint.shadowDy, paramPaint.shadowColor);
            i = 0x0 | 0x1;
        }
        Shader localShader = paramPaint.getShader();
        if ((localShader != null) && ((paramInt & 0x2) != 0))
        {
            nSetupShader(this.mRenderer, localShader.native_shader);
            i |= 2;
        }
        ColorFilter localColorFilter = paramPaint.getColorFilter();
        if ((localColorFilter != null) && ((paramInt & 0x4) != 0))
        {
            nSetupColorFilter(this.mRenderer, localColorFilter.nativeColorFilter);
            i |= 4;
        }
        return i;
    }

    public static void terminateCaches()
    {
        nTerminateCaches();
    }

    public void attachFunctor(int paramInt)
    {
        nAttachFunctor(this.mRenderer, paramInt);
    }

    public int callDrawGLFunction(int paramInt)
    {
        return nCallDrawGLFunction(this.mRenderer, paramInt);
    }

    public boolean clipPath(Path paramPath)
    {
        paramPath.computeBounds(this.mPathBounds, true);
        return nClipRect(this.mRenderer, this.mPathBounds.left, this.mPathBounds.top, this.mPathBounds.right, this.mPathBounds.bottom, Region.Op.INTERSECT.nativeInt);
    }

    public boolean clipPath(Path paramPath, Region.Op paramOp)
    {
        paramPath.computeBounds(this.mPathBounds, true);
        return nClipRect(this.mRenderer, this.mPathBounds.left, this.mPathBounds.top, this.mPathBounds.right, this.mPathBounds.bottom, paramOp.nativeInt);
    }

    public boolean clipRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        return nClipRect(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramFloat4, Region.Op.INTERSECT.nativeInt);
    }

    public boolean clipRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Region.Op paramOp)
    {
        return nClipRect(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramOp.nativeInt);
    }

    public boolean clipRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return nClipRect(this.mRenderer, paramInt1, paramInt2, paramInt3, paramInt4, Region.Op.INTERSECT.nativeInt);
    }

    public boolean clipRect(Rect paramRect)
    {
        return nClipRect(this.mRenderer, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, Region.Op.INTERSECT.nativeInt);
    }

    public boolean clipRect(Rect paramRect, Region.Op paramOp)
    {
        return nClipRect(this.mRenderer, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramOp.nativeInt);
    }

    public boolean clipRect(RectF paramRectF)
    {
        return nClipRect(this.mRenderer, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, Region.Op.INTERSECT.nativeInt);
    }

    public boolean clipRect(RectF paramRectF, Region.Op paramOp)
    {
        return nClipRect(this.mRenderer, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramOp.nativeInt);
    }

    public boolean clipRegion(Region paramRegion)
    {
        paramRegion.getBounds(this.mClipBounds);
        return nClipRect(this.mRenderer, this.mClipBounds.left, this.mClipBounds.top, this.mClipBounds.right, this.mClipBounds.bottom, Region.Op.INTERSECT.nativeInt);
    }

    public boolean clipRegion(Region paramRegion, Region.Op paramOp)
    {
        paramRegion.getBounds(this.mClipBounds);
        return nClipRect(this.mRenderer, this.mClipBounds.left, this.mClipBounds.top, this.mClipBounds.right, this.mClipBounds.bottom, paramOp.nativeInt);
    }

    public void concat(Matrix paramMatrix)
    {
        nConcatMatrix(this.mRenderer, paramMatrix.native_instance);
    }

    public void detachFunctor(int paramInt)
    {
        nDetachFunctor(this.mRenderer, paramInt);
    }

    public void drawARGB(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        drawColor((paramInt1 & 0xFF) << 24 | (paramInt2 & 0xFF) << 16 | (paramInt3 & 0xFF) << 8 | paramInt4 & 0xFF);
    }

    public void drawArc(RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint, 6);
        try
        {
            nDrawArc(this.mRenderer, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, paramBoolean, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawBitmap(Bitmap paramBitmap, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot draw recycled bitmaps");
        int i;
        if (paramPaint != null)
            i = setupModifiers(paramBitmap, paramPaint);
        while (true)
        {
            int j;
            if (paramPaint == null)
                j = 0;
            try
            {
                while (true)
                {
                    nDrawBitmap(this.mRenderer, paramBitmap.mNativeBitmap, paramBitmap.mBuffer, paramFloat1, paramFloat2, j);
                    return;
                    i = 0;
                    break;
                    j = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawBitmap(Bitmap paramBitmap, Matrix paramMatrix, Paint paramPaint)
    {
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot draw recycled bitmaps");
        int i;
        if (paramPaint != null)
            i = setupModifiers(paramBitmap, paramPaint);
        while (true)
        {
            int j;
            if (paramPaint == null)
                j = 0;
            try
            {
                while (true)
                {
                    nDrawBitmap(this.mRenderer, paramBitmap.mNativeBitmap, paramBitmap.mBuffer, paramMatrix.native_instance, j);
                    return;
                    i = 0;
                    break;
                    j = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawBitmap(Bitmap paramBitmap, Rect paramRect1, Rect paramRect2, Paint paramPaint)
    {
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot draw recycled bitmaps");
        int i;
        if (paramPaint != null)
            i = setupModifiers(paramBitmap, paramPaint);
        while (true)
        {
            int j;
            label40: int n;
            int k;
            if (paramPaint == null)
            {
                j = 0;
                if (paramRect1 != null)
                    break label142;
                n = 0;
                k = 0;
            }
            try
            {
                int m = paramBitmap.getWidth();
                for (int i1 = paramBitmap.getHeight(); ; i1 = paramRect1.bottom)
                {
                    nDrawBitmap(this.mRenderer, paramBitmap.mNativeBitmap, paramBitmap.mBuffer, k, n, m, i1, paramRect2.left, paramRect2.top, paramRect2.right, paramRect2.bottom, j);
                    return;
                    i = 0;
                    break;
                    j = paramPaint.mNativePaint;
                    break label40;
                    label142: k = paramRect1.left;
                    m = paramRect1.right;
                    n = paramRect1.top;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawBitmap(Bitmap paramBitmap, Rect paramRect, RectF paramRectF, Paint paramPaint)
    {
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot draw recycled bitmaps");
        int i;
        if (paramPaint != null)
            i = setupModifiers(paramBitmap, paramPaint);
        while (true)
        {
            int j;
            label40: float f3;
            float f1;
            if (paramPaint == null)
            {
                j = 0;
                if (paramRect != null)
                    break label136;
                f3 = 0.0F;
                f1 = 0.0F;
            }
            try
            {
                float f2 = paramBitmap.getWidth();
                label136: int k;
                for (float f4 = paramBitmap.getHeight(); ; f4 = k)
                {
                    nDrawBitmap(this.mRenderer, paramBitmap.mNativeBitmap, paramBitmap.mBuffer, f1, f3, f2, f4, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, j);
                    return;
                    i = 0;
                    break;
                    j = paramPaint.mNativePaint;
                    break label40;
                    f1 = paramRect.left;
                    f2 = paramRect.right;
                    f3 = paramRect.top;
                    k = paramRect.bottom;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4, boolean paramBoolean, Paint paramPaint)
    {
        if (paramInt3 < 0)
            throw new IllegalArgumentException("width must be >= 0");
        if (paramInt4 < 0)
            throw new IllegalArgumentException("height must be >= 0");
        if (Math.abs(paramInt2) < paramInt3)
            throw new IllegalArgumentException("abs(stride) must be >= width");
        int i = paramInt1 + paramInt2 * (paramInt4 - 1);
        int j = paramArrayOfInt.length;
        if ((paramInt1 < 0) || (paramInt1 + paramInt3 > j) || (i < 0) || (i + paramInt3 > j))
            throw new ArrayIndexOutOfBoundsException();
        int k;
        if (paramPaint != null)
            k = setupColorFilter(paramPaint);
        while (true)
        {
            int m;
            if (paramPaint == null)
                m = 0;
            try
            {
                while (true)
                {
                    nDrawBitmap(this.mRenderer, paramArrayOfInt, paramInt1, paramInt2, paramFloat1, paramFloat2, paramInt3, paramInt4, paramBoolean, m);
                    return;
                    k = 0;
                    break;
                    m = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (k != 0)
                    nResetModifiers(this.mRenderer, k);
            }
        }
    }

    public void drawBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, Paint paramPaint)
    {
        drawBitmap(paramArrayOfInt, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBoolean, paramPaint);
    }

    public void drawBitmapMesh(Bitmap paramBitmap, int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3, int[] paramArrayOfInt, int paramInt4, Paint paramPaint)
    {
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot draw recycled bitmaps");
        if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt3 < 0) || (paramInt4 < 0))
            throw new ArrayIndexOutOfBoundsException();
        if ((paramInt1 == 0) || (paramInt2 == 0));
        while (true)
        {
            return;
            int i = (paramInt1 + 1) * (paramInt2 + 1);
            checkRange(paramArrayOfFloat.length, paramInt3, i * 2);
            int j;
            label88: int k;
            if (paramPaint != null)
            {
                j = setupModifiers(paramBitmap, paramPaint);
                if (paramPaint != null)
                    break label144;
                k = 0;
            }
            try
            {
                while (true)
                {
                    nDrawBitmapMesh(this.mRenderer, paramBitmap.mNativeBitmap, paramBitmap.mBuffer, paramInt1, paramInt2, paramArrayOfFloat, paramInt3, null, 0, k);
                    if (j == 0)
                        break;
                    nResetModifiers(this.mRenderer, j);
                    break;
                    j = 0;
                    break label88;
                    label144: k = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (j != 0)
                    nResetModifiers(this.mRenderer, j);
            }
        }
    }

    public void drawCircle(float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint, 6);
        try
        {
            nDrawCircle(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawColor(int paramInt)
    {
        drawColor(paramInt, PorterDuff.Mode.SRC_OVER);
    }

    public void drawColor(int paramInt, PorterDuff.Mode paramMode)
    {
        nDrawColor(this.mRenderer, paramInt, paramMode.nativeInt);
    }

    public int drawDisplayList(DisplayList paramDisplayList, Rect paramRect, int paramInt)
    {
        return nDrawDisplayList(this.mRenderer, ((GLES20DisplayList)paramDisplayList).getNativeDisplayList(), paramRect, paramInt);
    }

    void drawHardwareLayer(HardwareLayer paramHardwareLayer, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        GLES20Layer localGLES20Layer = (GLES20Layer)paramHardwareLayer;
        int i;
        if (paramPaint != null)
            i = setupColorFilter(paramPaint);
        while (true)
        {
            int j;
            if (paramPaint == null)
                j = 0;
            try
            {
                while (true)
                {
                    nDrawLayer(this.mRenderer, localGLES20Layer.getLayer(), paramFloat1, paramFloat2, j);
                    return;
                    i = 0;
                    break;
                    j = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawLine(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint)
    {
        this.mLine[0] = paramFloat1;
        this.mLine[1] = paramFloat2;
        this.mLine[2] = paramFloat3;
        this.mLine[3] = paramFloat4;
        drawLines(this.mLine, 0, 4, paramPaint);
    }

    public void drawLines(float[] paramArrayOfFloat, int paramInt1, int paramInt2, Paint paramPaint)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 + paramInt2 > paramArrayOfFloat.length))
            throw new IllegalArgumentException("The lines array must contain 4 elements per line.");
        int i = setupModifiers(paramPaint, 6);
        try
        {
            nDrawLines(this.mRenderer, paramArrayOfFloat, paramInt1, paramInt2, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawLines(float[] paramArrayOfFloat, Paint paramPaint)
    {
        drawLines(paramArrayOfFloat, 0, paramArrayOfFloat.length, paramPaint);
    }

    public void drawOval(RectF paramRectF, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint, 6);
        try
        {
            nDrawOval(this.mRenderer, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawPaint(Paint paramPaint)
    {
        Rect localRect = this.mClipBounds;
        nGetClipBounds(this.mRenderer, localRect);
        drawRect(localRect.left, localRect.top, localRect.right, localRect.bottom, paramPaint);
    }

    public void drawPatch(Bitmap paramBitmap, byte[] paramArrayOfByte, RectF paramRectF, Paint paramPaint)
    {
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot draw recycled bitmaps");
        int i;
        if (paramPaint != null)
            i = setupColorFilter(paramPaint);
        while (true)
        {
            int j;
            if (paramPaint == null)
                j = 0;
            try
            {
                while (true)
                {
                    nDrawPatch(this.mRenderer, paramBitmap.mNativeBitmap, paramBitmap.mBuffer, paramArrayOfByte, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, j);
                    return;
                    i = 0;
                    break;
                    j = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawPath(Path paramPath, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint, 6);
        try
        {
            if (paramPath.isSimplePath)
                if (paramPath.rects != null)
                    nDrawRects(this.mRenderer, paramPath.rects.mNativeRegion, paramPaint.mNativePaint);
            while (true)
            {
                return;
                nDrawPath(this.mRenderer, paramPath.mNativePath, paramPaint.mNativePaint);
            }
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawPicture(Picture paramPicture)
    {
        if (paramPicture.createdFromStream);
        while (true)
        {
            return;
            paramPicture.endRecording();
        }
    }

    public void drawPicture(Picture paramPicture, Rect paramRect)
    {
        if (paramPicture.createdFromStream);
        while (true)
        {
            return;
            save();
            translate(paramRect.left, paramRect.top);
            if ((paramPicture.getWidth() > 0) && (paramPicture.getHeight() > 0))
                scale(paramRect.width() / paramPicture.getWidth(), paramRect.height() / paramPicture.getHeight());
            drawPicture(paramPicture);
            restore();
        }
    }

    public void drawPicture(Picture paramPicture, RectF paramRectF)
    {
        if (paramPicture.createdFromStream);
        while (true)
        {
            return;
            save();
            translate(paramRectF.left, paramRectF.top);
            if ((paramPicture.getWidth() > 0) && (paramPicture.getHeight() > 0))
                scale(paramRectF.width() / paramPicture.getWidth(), paramRectF.height() / paramPicture.getHeight());
            drawPicture(paramPicture);
            restore();
        }
    }

    public void drawPoint(float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        this.mPoint[0] = paramFloat1;
        this.mPoint[1] = paramFloat2;
        drawPoints(this.mPoint, 0, 2, paramPaint);
    }

    public void drawPoints(float[] paramArrayOfFloat, int paramInt1, int paramInt2, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint, 6);
        try
        {
            nDrawPoints(this.mRenderer, paramArrayOfFloat, paramInt1, paramInt2, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawPoints(float[] paramArrayOfFloat, Paint paramPaint)
    {
        drawPoints(paramArrayOfFloat, 0, paramArrayOfFloat.length, paramPaint);
    }

    public void drawPosText(String paramString, float[] paramArrayOfFloat, Paint paramPaint)
    {
        if (2 * paramString.length() > paramArrayOfFloat.length)
            throw new ArrayIndexOutOfBoundsException();
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawPosText(this.mRenderer, paramString, 0, paramString.length(), paramArrayOfFloat, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawPosText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float[] paramArrayOfFloat, Paint paramPaint)
    {
        if ((paramInt1 < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length) || (paramInt2 * 2 > paramArrayOfFloat.length))
            throw new IndexOutOfBoundsException();
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawPosText(this.mRenderer, paramArrayOfChar, paramInt1, paramInt2, paramArrayOfFloat, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawRGB(int paramInt1, int paramInt2, int paramInt3)
    {
        drawColor(0xFF000000 | (paramInt1 & 0xFF) << 16 | (paramInt2 & 0xFF) << 8 | paramInt3 & 0xFF);
    }

    public void drawRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint)
    {
        if ((paramFloat1 == paramFloat3) || (paramFloat2 == paramFloat4));
        while (true)
        {
            return;
            int i = setupModifiers(paramPaint, 6);
            try
            {
                nDrawRect(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint.mNativePaint);
                if (i == 0)
                    continue;
                nResetModifiers(this.mRenderer, i);
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawRect(Rect paramRect, Paint paramPaint)
    {
        drawRect(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramPaint);
    }

    public void drawRect(RectF paramRectF, Paint paramPaint)
    {
        drawRect(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramPaint);
    }

    public void drawRoundRect(RectF paramRectF, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint, 6);
        try
        {
            nDrawRoundRect(this.mRenderer, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawText(CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint);
        while (true)
        {
            try
            {
                if (((paramCharSequence instanceof String)) || ((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
                {
                    nDrawText(this.mRenderer, paramCharSequence.toString(), paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
                    return;
                }
                if ((paramCharSequence instanceof GraphicsOperations))
                {
                    ((GraphicsOperations)paramCharSequence).drawText(this, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint);
                    continue;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
            int j = paramInt2 - paramInt1;
            char[] arrayOfChar = TemporaryBuffer.obtain(j);
            TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
            nDrawText(this.mRenderer, arrayOfChar, 0, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
            TemporaryBuffer.recycle(arrayOfChar);
        }
    }

    public void drawText(String paramString, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawText(this.mRenderer, paramString, 0, paramString.length(), paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawText(String paramString, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawText(this.mRenderer, paramString, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if ((paramInt1 | paramInt2 | paramInt1 + paramInt2 | paramArrayOfChar.length - paramInt1 - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawText(this.mRenderer, paramArrayOfChar, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawTextOnPath(String paramString, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if (paramString.length() == 0);
        while (true)
        {
            return;
            int i = setupModifiers(paramPaint);
            try
            {
                nDrawTextOnPath(this.mRenderer, paramString, 0, paramString.length(), paramPath.mNativePath, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
                if (i == 0)
                    continue;
                nResetModifiers(this.mRenderer, i);
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public void drawTextOnPath(char[] paramArrayOfChar, int paramInt1, int paramInt2, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if ((paramInt1 < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length))
            throw new ArrayIndexOutOfBoundsException();
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawTextOnPath(this.mRenderer, paramArrayOfChar, paramInt1, paramInt2, paramPath.mNativePath, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawTextRun(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramCharSequence.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        int i = setupModifiers(paramPaint);
        int j;
        if (paramInt5 == 0)
            j = 0;
        while (true)
        {
            try
            {
                if (((paramCharSequence instanceof String)) || ((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
                {
                    nDrawTextRun(this.mRenderer, paramCharSequence.toString(), paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, j, paramPaint.mNativePaint);
                    return;
                    j = 1;
                    continue;
                }
                if ((paramCharSequence instanceof GraphicsOperations))
                {
                    ((GraphicsOperations)paramCharSequence).drawTextRun(this, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, j, paramPaint);
                    continue;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
            int k = paramInt4 - paramInt3;
            int m = paramInt2 - paramInt1;
            char[] arrayOfChar = TemporaryBuffer.obtain(k);
            TextUtils.getChars(paramCharSequence, paramInt3, paramInt4, arrayOfChar, 0);
            nDrawTextRun(this.mRenderer, arrayOfChar, paramInt1 - paramInt3, m, 0, k, paramFloat1, paramFloat2, j, paramPaint.mNativePaint);
            TemporaryBuffer.recycle(arrayOfChar);
        }
    }

    public void drawTextRun(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        if ((paramInt1 | paramInt2 | paramArrayOfChar.length - paramInt1 - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        if ((paramInt5 != 0) && (paramInt5 != 1))
            throw new IllegalArgumentException("Unknown direction: " + paramInt5);
        int i = setupModifiers(paramPaint);
        try
        {
            nDrawTextRun(this.mRenderer, paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramInt5, paramPaint.mNativePaint);
            return;
        }
        finally
        {
            if (i != 0)
                nResetModifiers(this.mRenderer, i);
        }
    }

    public void drawVertices(Canvas.VertexMode paramVertexMode, int paramInt1, float[] paramArrayOfFloat1, int paramInt2, float[] paramArrayOfFloat2, int paramInt3, int[] paramArrayOfInt, int paramInt4, short[] paramArrayOfShort, int paramInt5, int paramInt6, Paint paramPaint)
    {
    }

    public boolean getClipBounds(Rect paramRect)
    {
        return nGetClipBounds(this.mRenderer, paramRect);
    }

    int getDisplayList(int paramInt)
    {
        return nGetDisplayList(this.mRenderer, paramInt);
    }

    public DrawFilter getDrawFilter()
    {
        return this.mFilter;
    }

    public int getHeight()
    {
        return this.mHeight;
    }

    public void getMatrix(Matrix paramMatrix)
    {
        nGetMatrix(this.mRenderer, paramMatrix.native_instance);
    }

    public int getMaximumBitmapHeight()
    {
        return nGetMaximumTextureHeight();
    }

    public int getMaximumBitmapWidth()
    {
        return nGetMaximumTextureWidth();
    }

    int getRenderer()
    {
        return this.mRenderer;
    }

    public int getSaveCount()
    {
        return nGetSaveCount(this.mRenderer);
    }

    public int getWidth()
    {
        return this.mWidth;
    }

    void interrupt()
    {
        nInterrupt(this.mRenderer);
    }

    public int invokeFunctors(Rect paramRect)
    {
        return nInvokeFunctors(this.mRenderer, paramRect);
    }

    public boolean isOpaque()
    {
        return this.mOpaque;
    }

    public void onPostDraw()
    {
        nFinish(this.mRenderer);
    }

    public int onPreDraw(Rect paramRect)
    {
        if (paramRect != null);
        for (int i = nPrepareDirty(this.mRenderer, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, this.mOpaque); ; i = nPrepare(this.mRenderer, this.mOpaque))
            return i;
    }

    void outputDisplayList(DisplayList paramDisplayList)
    {
        nOutputDisplayList(this.mRenderer, ((GLES20DisplayList)paramDisplayList).getNativeDisplayList());
    }

    public boolean quickReject(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Canvas.EdgeType paramEdgeType)
    {
        return nQuickReject(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramEdgeType.nativeInt);
    }

    public boolean quickReject(Path paramPath, Canvas.EdgeType paramEdgeType)
    {
        paramPath.computeBounds(this.mPathBounds, true);
        return nQuickReject(this.mRenderer, this.mPathBounds.left, this.mPathBounds.top, this.mPathBounds.right, this.mPathBounds.bottom, paramEdgeType.nativeInt);
    }

    public boolean quickReject(RectF paramRectF, Canvas.EdgeType paramEdgeType)
    {
        return nQuickReject(this.mRenderer, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramEdgeType.nativeInt);
    }

    protected void resetDisplayListRenderer()
    {
        nResetDisplayListRenderer(this.mRenderer);
    }

    public void restore()
    {
        nRestore(this.mRenderer);
    }

    public void restoreToCount(int paramInt)
    {
        nRestoreToCount(this.mRenderer, paramInt);
    }

    void resume()
    {
        nResume(this.mRenderer);
    }

    public void rotate(float paramFloat)
    {
        nRotate(this.mRenderer, paramFloat);
    }

    public int save()
    {
        return nSave(this.mRenderer, 3);
    }

    public int save(int paramInt)
    {
        return nSave(this.mRenderer, paramInt);
    }

    public int saveLayer(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint, int paramInt)
    {
        int j;
        int k;
        if ((paramFloat1 < paramFloat3) && (paramFloat2 < paramFloat4))
            if (paramPaint != null)
            {
                j = setupColorFilter(paramPaint);
                if (paramPaint != null)
                    break label79;
                k = 0;
            }
        while (true)
        {
            try
            {
                int m = nSaveLayer(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramFloat4, k, paramInt);
                i = m;
                return i;
                j = 0;
                break;
                label79: k = paramPaint.mNativePaint;
                continue;
            }
            finally
            {
                if (j != 0)
                    nResetModifiers(this.mRenderer, j);
            }
            int i = save(paramInt);
        }
    }

    public int saveLayer(RectF paramRectF, Paint paramPaint, int paramInt)
    {
        int m;
        if (paramRectF != null)
            m = saveLayer(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramPaint, paramInt);
        while (true)
        {
            return m;
            int i;
            label42: int j;
            if (paramPaint != null)
            {
                i = setupColorFilter(paramPaint);
                if (paramPaint != null)
                    break label88;
                j = 0;
            }
            try
            {
                while (true)
                {
                    int k = nSaveLayer(this.mRenderer, j, paramInt);
                    m = k;
                    if (i == 0)
                        break;
                    nResetModifiers(this.mRenderer, i);
                    break;
                    i = 0;
                    break label42;
                    label88: j = paramPaint.mNativePaint;
                }
            }
            finally
            {
                if (i != 0)
                    nResetModifiers(this.mRenderer, i);
            }
        }
    }

    public int saveLayerAlpha(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, int paramInt2)
    {
        if ((paramFloat1 < paramFloat3) && (paramFloat2 < paramFloat4));
        for (int i = nSaveLayerAlpha(this.mRenderer, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt1, paramInt2); ; i = save(paramInt2))
            return i;
    }

    public int saveLayerAlpha(RectF paramRectF, int paramInt1, int paramInt2)
    {
        if (paramRectF != null);
        for (int i = saveLayerAlpha(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramInt1, paramInt2); ; i = nSaveLayerAlpha(this.mRenderer, paramInt1, paramInt2))
            return i;
    }

    public void scale(float paramFloat1, float paramFloat2)
    {
        nScale(this.mRenderer, paramFloat1, paramFloat2);
    }

    public void setDrawFilter(DrawFilter paramDrawFilter)
    {
        this.mFilter = paramDrawFilter;
        if (paramDrawFilter == null)
            nResetPaintFilter(this.mRenderer);
        while (true)
        {
            return;
            if ((paramDrawFilter instanceof PaintFlagsDrawFilter))
            {
                PaintFlagsDrawFilter localPaintFlagsDrawFilter = (PaintFlagsDrawFilter)paramDrawFilter;
                nSetupPaintFilter(this.mRenderer, localPaintFlagsDrawFilter.clearBits, localPaintFlagsDrawFilter.setBits);
            }
        }
    }

    public void setMatrix(Matrix paramMatrix)
    {
        int i = this.mRenderer;
        if (paramMatrix == null);
        for (int j = 0; ; j = paramMatrix.native_instance)
        {
            nSetMatrix(i, j);
            return;
        }
    }

    public void setViewport(int paramInt1, int paramInt2)
    {
        this.mWidth = paramInt1;
        this.mHeight = paramInt2;
        nSetViewport(this.mRenderer, paramInt1, paramInt2);
    }

    public void skew(float paramFloat1, float paramFloat2)
    {
        nSkew(this.mRenderer, paramFloat1, paramFloat2);
    }

    public void translate(float paramFloat1, float paramFloat2)
    {
        if ((paramFloat1 != 0.0F) || (paramFloat2 != 0.0F))
            nTranslate(this.mRenderer, paramFloat1, paramFloat2);
    }

    private static final class CanvasFinalizer
    {
        private final int mRenderer;

        public CanvasFinalizer(int paramInt)
        {
            this.mRenderer = paramInt;
        }

        protected void finalize()
            throws Throwable
        {
            try
            {
                GLES20Canvas.nDestroyRenderer(this.mRenderer);
                return;
            }
            finally
            {
                super.finalize();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GLES20Canvas
 * JD-Core Version:        0.6.2
 */