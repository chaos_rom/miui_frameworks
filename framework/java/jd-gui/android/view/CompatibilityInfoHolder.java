package android.view;

import android.content.res.CompatibilityInfo;

public class CompatibilityInfoHolder
{
    private volatile CompatibilityInfo mCompatInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;

    public CompatibilityInfo get()
    {
        return this.mCompatInfo;
    }

    public CompatibilityInfo getIfNeeded()
    {
        CompatibilityInfo localCompatibilityInfo = this.mCompatInfo;
        if ((localCompatibilityInfo == null) || (localCompatibilityInfo == CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO))
            localCompatibilityInfo = null;
        return localCompatibilityInfo;
    }

    public void set(CompatibilityInfo paramCompatibilityInfo)
    {
        if ((paramCompatibilityInfo != null) && ((paramCompatibilityInfo.isScalingRequired()) || (!paramCompatibilityInfo.supportsScreen())));
        for (this.mCompatInfo = paramCompatibilityInfo; ; this.mCompatInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO)
            return;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.CompatibilityInfoHolder
 * JD-Core Version:        0.6.2
 */