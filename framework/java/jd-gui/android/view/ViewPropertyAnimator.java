package android.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class ViewPropertyAnimator
{
    private static final int ALPHA = 512;
    private static final int NONE = 0;
    private static final int ROTATION = 16;
    private static final int ROTATION_X = 32;
    private static final int ROTATION_Y = 64;
    private static final int SCALE_X = 4;
    private static final int SCALE_Y = 8;
    private static final int TRANSFORM_MASK = 511;
    private static final int TRANSLATION_X = 1;
    private static final int TRANSLATION_Y = 2;
    private static final int X = 128;
    private static final int Y = 256;
    private Runnable mAnimationStarter = new Runnable()
    {
        public void run()
        {
            ViewPropertyAnimator.this.startAnimation();
        }
    };
    private HashMap<Animator, Runnable> mAnimatorCleanupMap;
    private AnimatorEventListener mAnimatorEventListener = new AnimatorEventListener(null);
    private HashMap<Animator, PropertyBundle> mAnimatorMap = new HashMap();
    private HashMap<Animator, Runnable> mAnimatorOnEndMap;
    private HashMap<Animator, Runnable> mAnimatorOnStartMap;
    private HashMap<Animator, Runnable> mAnimatorSetupMap;
    private long mDuration;
    private boolean mDurationSet = false;
    private TimeInterpolator mInterpolator;
    private boolean mInterpolatorSet = false;
    private Animator.AnimatorListener mListener = null;
    ArrayList<NameValuesHolder> mPendingAnimations = new ArrayList();
    private Runnable mPendingCleanupAction;
    private Runnable mPendingOnEndAction;
    private Runnable mPendingOnStartAction;
    private Runnable mPendingSetupAction;
    private long mStartDelay = 0L;
    private boolean mStartDelaySet = false;
    private final View mView;

    ViewPropertyAnimator(View paramView)
    {
        this.mView = paramView;
        paramView.ensureTransformationInfo();
    }

    private void animateProperty(int paramInt, float paramFloat)
    {
        float f = getValue(paramInt);
        animatePropertyBy(paramInt, f, paramFloat - f);
    }

    private void animatePropertyBy(int paramInt, float paramFloat)
    {
        animatePropertyBy(paramInt, getValue(paramInt), paramFloat);
    }

    private void animatePropertyBy(int paramInt, float paramFloat1, float paramFloat2)
    {
        if (this.mAnimatorMap.size() > 0)
        {
            Object localObject = null;
            Iterator localIterator = this.mAnimatorMap.keySet().iterator();
            while (localIterator.hasNext())
            {
                Animator localAnimator = (Animator)localIterator.next();
                PropertyBundle localPropertyBundle = (PropertyBundle)this.mAnimatorMap.get(localAnimator);
                if ((localPropertyBundle.cancel(paramInt)) && (localPropertyBundle.mPropertyMask == 0))
                    localObject = localAnimator;
            }
            if (localObject != null)
                localObject.cancel();
        }
        NameValuesHolder localNameValuesHolder = new NameValuesHolder(paramInt, paramFloat1, paramFloat2);
        this.mPendingAnimations.add(localNameValuesHolder);
        this.mView.removeCallbacks(this.mAnimationStarter);
        this.mView.post(this.mAnimationStarter);
    }

    private float getValue(int paramInt)
    {
        View.TransformationInfo localTransformationInfo = this.mView.mTransformationInfo;
        float f;
        switch (paramInt)
        {
        default:
            f = 0.0F;
        case 1:
        case 2:
        case 16:
        case 32:
        case 64:
        case 4:
        case 8:
        case 128:
        case 256:
        case 512:
        }
        while (true)
        {
            return f;
            f = localTransformationInfo.mTranslationX;
            continue;
            f = localTransformationInfo.mTranslationY;
            continue;
            f = localTransformationInfo.mRotation;
            continue;
            f = localTransformationInfo.mRotationX;
            continue;
            f = localTransformationInfo.mRotationY;
            continue;
            f = localTransformationInfo.mScaleX;
            continue;
            f = localTransformationInfo.mScaleY;
            continue;
            f = this.mView.mLeft + localTransformationInfo.mTranslationX;
            continue;
            f = this.mView.mTop + localTransformationInfo.mTranslationY;
            continue;
            f = localTransformationInfo.mAlpha;
        }
    }

    private void setValue(int paramInt, float paramFloat)
    {
        View.TransformationInfo localTransformationInfo = this.mView.mTransformationInfo;
        DisplayList localDisplayList = this.mView.mDisplayList;
        switch (paramInt)
        {
        default:
        case 1:
        case 2:
        case 16:
        case 32:
        case 64:
        case 4:
        case 8:
        case 128:
        case 256:
        case 512:
        }
        while (true)
        {
            return;
            localTransformationInfo.mTranslationX = paramFloat;
            if (localDisplayList != null)
            {
                localDisplayList.setTranslationX(paramFloat);
                continue;
                localTransformationInfo.mTranslationY = paramFloat;
                if (localDisplayList != null)
                {
                    localDisplayList.setTranslationY(paramFloat);
                    continue;
                    localTransformationInfo.mRotation = paramFloat;
                    if (localDisplayList != null)
                    {
                        localDisplayList.setRotation(paramFloat);
                        continue;
                        localTransformationInfo.mRotationX = paramFloat;
                        if (localDisplayList != null)
                        {
                            localDisplayList.setRotationX(paramFloat);
                            continue;
                            localTransformationInfo.mRotationY = paramFloat;
                            if (localDisplayList != null)
                            {
                                localDisplayList.setRotationY(paramFloat);
                                continue;
                                localTransformationInfo.mScaleX = paramFloat;
                                if (localDisplayList != null)
                                {
                                    localDisplayList.setScaleX(paramFloat);
                                    continue;
                                    localTransformationInfo.mScaleY = paramFloat;
                                    if (localDisplayList != null)
                                    {
                                        localDisplayList.setScaleY(paramFloat);
                                        continue;
                                        localTransformationInfo.mTranslationX = (paramFloat - this.mView.mLeft);
                                        if (localDisplayList != null)
                                        {
                                            localDisplayList.setTranslationX(paramFloat - this.mView.mLeft);
                                            continue;
                                            localTransformationInfo.mTranslationY = (paramFloat - this.mView.mTop);
                                            if (localDisplayList != null)
                                            {
                                                localDisplayList.setTranslationY(paramFloat - this.mView.mTop);
                                                continue;
                                                localTransformationInfo.mAlpha = paramFloat;
                                                if (localDisplayList != null)
                                                    localDisplayList.setAlpha(paramFloat);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void startAnimation()
    {
        this.mView.setHasTransientState(true);
        float[] arrayOfFloat = new float[1];
        arrayOfFloat[0] = 1.0F;
        ValueAnimator localValueAnimator = ValueAnimator.ofFloat(arrayOfFloat);
        ArrayList localArrayList = (ArrayList)this.mPendingAnimations.clone();
        this.mPendingAnimations.clear();
        int i = 0;
        int j = localArrayList.size();
        for (int k = 0; k < j; k++)
            i |= ((NameValuesHolder)localArrayList.get(k)).mNameConstant;
        this.mAnimatorMap.put(localValueAnimator, new PropertyBundle(i, localArrayList));
        if (this.mPendingSetupAction != null)
        {
            this.mAnimatorSetupMap.put(localValueAnimator, this.mPendingSetupAction);
            this.mPendingSetupAction = null;
        }
        if (this.mPendingCleanupAction != null)
        {
            this.mAnimatorCleanupMap.put(localValueAnimator, this.mPendingCleanupAction);
            this.mPendingCleanupAction = null;
        }
        if (this.mPendingOnStartAction != null)
        {
            this.mAnimatorOnStartMap.put(localValueAnimator, this.mPendingOnStartAction);
            this.mPendingOnStartAction = null;
        }
        if (this.mPendingOnEndAction != null)
        {
            this.mAnimatorOnEndMap.put(localValueAnimator, this.mPendingOnEndAction);
            this.mPendingOnEndAction = null;
        }
        localValueAnimator.addUpdateListener(this.mAnimatorEventListener);
        localValueAnimator.addListener(this.mAnimatorEventListener);
        if (this.mStartDelaySet)
            localValueAnimator.setStartDelay(this.mStartDelay);
        if (this.mDurationSet)
            localValueAnimator.setDuration(this.mDuration);
        if (this.mInterpolatorSet)
            localValueAnimator.setInterpolator(this.mInterpolator);
        localValueAnimator.start();
    }

    public ViewPropertyAnimator alpha(float paramFloat)
    {
        animateProperty(512, paramFloat);
        return this;
    }

    public ViewPropertyAnimator alphaBy(float paramFloat)
    {
        animatePropertyBy(512, paramFloat);
        return this;
    }

    public void cancel()
    {
        if (this.mAnimatorMap.size() > 0)
        {
            Iterator localIterator = ((HashMap)this.mAnimatorMap.clone()).keySet().iterator();
            while (localIterator.hasNext())
                ((Animator)localIterator.next()).cancel();
        }
        this.mPendingAnimations.clear();
        this.mView.removeCallbacks(this.mAnimationStarter);
    }

    public long getDuration()
    {
        if (this.mDurationSet);
        for (long l = this.mDuration; ; l = new ValueAnimator().getDuration())
            return l;
    }

    public long getStartDelay()
    {
        if (this.mStartDelaySet);
        for (long l = this.mStartDelay; ; l = 0L)
            return l;
    }

    public ViewPropertyAnimator rotation(float paramFloat)
    {
        animateProperty(16, paramFloat);
        return this;
    }

    public ViewPropertyAnimator rotationBy(float paramFloat)
    {
        animatePropertyBy(16, paramFloat);
        return this;
    }

    public ViewPropertyAnimator rotationX(float paramFloat)
    {
        animateProperty(32, paramFloat);
        return this;
    }

    public ViewPropertyAnimator rotationXBy(float paramFloat)
    {
        animatePropertyBy(32, paramFloat);
        return this;
    }

    public ViewPropertyAnimator rotationY(float paramFloat)
    {
        animateProperty(64, paramFloat);
        return this;
    }

    public ViewPropertyAnimator rotationYBy(float paramFloat)
    {
        animatePropertyBy(64, paramFloat);
        return this;
    }

    public ViewPropertyAnimator scaleX(float paramFloat)
    {
        animateProperty(4, paramFloat);
        return this;
    }

    public ViewPropertyAnimator scaleXBy(float paramFloat)
    {
        animatePropertyBy(4, paramFloat);
        return this;
    }

    public ViewPropertyAnimator scaleY(float paramFloat)
    {
        animateProperty(8, paramFloat);
        return this;
    }

    public ViewPropertyAnimator scaleYBy(float paramFloat)
    {
        animatePropertyBy(8, paramFloat);
        return this;
    }

    public ViewPropertyAnimator setDuration(long paramLong)
    {
        if (paramLong < 0L)
            throw new IllegalArgumentException("Animators cannot have negative duration: " + paramLong);
        this.mDurationSet = true;
        this.mDuration = paramLong;
        return this;
    }

    public ViewPropertyAnimator setInterpolator(TimeInterpolator paramTimeInterpolator)
    {
        this.mInterpolatorSet = true;
        this.mInterpolator = paramTimeInterpolator;
        return this;
    }

    public ViewPropertyAnimator setListener(Animator.AnimatorListener paramAnimatorListener)
    {
        this.mListener = paramAnimatorListener;
        return this;
    }

    public ViewPropertyAnimator setStartDelay(long paramLong)
    {
        if (paramLong < 0L)
            throw new IllegalArgumentException("Animators cannot have negative duration: " + paramLong);
        this.mStartDelaySet = true;
        this.mStartDelay = paramLong;
        return this;
    }

    public void start()
    {
        this.mView.removeCallbacks(this.mAnimationStarter);
        startAnimation();
    }

    public ViewPropertyAnimator translationX(float paramFloat)
    {
        animateProperty(1, paramFloat);
        return this;
    }

    public ViewPropertyAnimator translationXBy(float paramFloat)
    {
        animatePropertyBy(1, paramFloat);
        return this;
    }

    public ViewPropertyAnimator translationY(float paramFloat)
    {
        animateProperty(2, paramFloat);
        return this;
    }

    public ViewPropertyAnimator translationYBy(float paramFloat)
    {
        animatePropertyBy(2, paramFloat);
        return this;
    }

    public ViewPropertyAnimator withEndAction(Runnable paramRunnable)
    {
        this.mPendingOnEndAction = paramRunnable;
        if ((paramRunnable != null) && (this.mAnimatorOnEndMap == null))
            this.mAnimatorOnEndMap = new HashMap();
        return this;
    }

    public ViewPropertyAnimator withLayer()
    {
        this.mPendingSetupAction = new Runnable()
        {
            public void run()
            {
                ViewPropertyAnimator.this.mView.setLayerType(2, null);
            }
        };
        this.mPendingCleanupAction = new Runnable()
        {
            public void run()
            {
                ViewPropertyAnimator.this.mView.setLayerType(this.val$currentLayerType, null);
            }
        };
        if (this.mAnimatorSetupMap == null)
            this.mAnimatorSetupMap = new HashMap();
        if (this.mAnimatorCleanupMap == null)
            this.mAnimatorCleanupMap = new HashMap();
        return this;
    }

    public ViewPropertyAnimator withStartAction(Runnable paramRunnable)
    {
        this.mPendingOnStartAction = paramRunnable;
        if ((paramRunnable != null) && (this.mAnimatorOnStartMap == null))
            this.mAnimatorOnStartMap = new HashMap();
        return this;
    }

    public ViewPropertyAnimator x(float paramFloat)
    {
        animateProperty(128, paramFloat);
        return this;
    }

    public ViewPropertyAnimator xBy(float paramFloat)
    {
        animatePropertyBy(128, paramFloat);
        return this;
    }

    public ViewPropertyAnimator y(float paramFloat)
    {
        animateProperty(256, paramFloat);
        return this;
    }

    public ViewPropertyAnimator yBy(float paramFloat)
    {
        animatePropertyBy(256, paramFloat);
        return this;
    }

    private class AnimatorEventListener
        implements Animator.AnimatorListener, ValueAnimator.AnimatorUpdateListener
    {
        private AnimatorEventListener()
        {
        }

        public void onAnimationCancel(Animator paramAnimator)
        {
            if (ViewPropertyAnimator.this.mListener != null)
                ViewPropertyAnimator.this.mListener.onAnimationCancel(paramAnimator);
            if (ViewPropertyAnimator.this.mAnimatorOnEndMap != null)
                ViewPropertyAnimator.this.mAnimatorOnEndMap.remove(paramAnimator);
        }

        public void onAnimationEnd(Animator paramAnimator)
        {
            ViewPropertyAnimator.this.mView.setHasTransientState(false);
            if (ViewPropertyAnimator.this.mListener != null)
                ViewPropertyAnimator.this.mListener.onAnimationEnd(paramAnimator);
            if (ViewPropertyAnimator.this.mAnimatorOnEndMap != null)
            {
                Runnable localRunnable2 = (Runnable)ViewPropertyAnimator.this.mAnimatorOnEndMap.get(paramAnimator);
                if (localRunnable2 != null)
                    localRunnable2.run();
                ViewPropertyAnimator.this.mAnimatorOnEndMap.remove(paramAnimator);
            }
            if (ViewPropertyAnimator.this.mAnimatorCleanupMap != null)
            {
                Runnable localRunnable1 = (Runnable)ViewPropertyAnimator.this.mAnimatorCleanupMap.get(paramAnimator);
                if (localRunnable1 != null)
                    localRunnable1.run();
                ViewPropertyAnimator.this.mAnimatorCleanupMap.remove(paramAnimator);
            }
            ViewPropertyAnimator.this.mAnimatorMap.remove(paramAnimator);
        }

        public void onAnimationRepeat(Animator paramAnimator)
        {
            if (ViewPropertyAnimator.this.mListener != null)
                ViewPropertyAnimator.this.mListener.onAnimationRepeat(paramAnimator);
        }

        public void onAnimationStart(Animator paramAnimator)
        {
            if (ViewPropertyAnimator.this.mAnimatorSetupMap != null)
            {
                Runnable localRunnable2 = (Runnable)ViewPropertyAnimator.this.mAnimatorSetupMap.get(paramAnimator);
                if (localRunnable2 != null)
                    localRunnable2.run();
                ViewPropertyAnimator.this.mAnimatorSetupMap.remove(paramAnimator);
            }
            if (ViewPropertyAnimator.this.mAnimatorOnStartMap != null)
            {
                Runnable localRunnable1 = (Runnable)ViewPropertyAnimator.this.mAnimatorOnStartMap.get(paramAnimator);
                if (localRunnable1 != null)
                    localRunnable1.run();
                ViewPropertyAnimator.this.mAnimatorOnStartMap.remove(paramAnimator);
            }
            if (ViewPropertyAnimator.this.mListener != null)
                ViewPropertyAnimator.this.mListener.onAnimationStart(paramAnimator);
        }

        public void onAnimationUpdate(ValueAnimator paramValueAnimator)
        {
            ViewPropertyAnimator.PropertyBundle localPropertyBundle = (ViewPropertyAnimator.PropertyBundle)ViewPropertyAnimator.this.mAnimatorMap.get(paramValueAnimator);
            if (localPropertyBundle == null);
            while (true)
            {
                return;
                int i;
                boolean bool;
                int j;
                int m;
                label106: ViewPropertyAnimator.NameValuesHolder localNameValuesHolder;
                float f2;
                if (ViewPropertyAnimator.this.mView.mDisplayList != null)
                {
                    i = 1;
                    bool = false;
                    if (i == 0)
                        ViewPropertyAnimator.this.mView.invalidateParentCaches();
                    float f1 = paramValueAnimator.getAnimatedFraction();
                    j = localPropertyBundle.mPropertyMask;
                    if ((j & 0x1FF) != 0)
                        ViewPropertyAnimator.this.mView.invalidateViewProperty(false, false);
                    ArrayList localArrayList = localPropertyBundle.mNameValuesHolder;
                    if (localArrayList == null)
                        break label194;
                    int k = localArrayList.size();
                    m = 0;
                    if (m >= k)
                        break label194;
                    localNameValuesHolder = (ViewPropertyAnimator.NameValuesHolder)localArrayList.get(m);
                    f2 = localNameValuesHolder.mFromValue + f1 * localNameValuesHolder.mDeltaValue;
                    if (localNameValuesHolder.mNameConstant != 512)
                        break label177;
                    bool = ViewPropertyAnimator.this.mView.setAlphaNoInvalidation(f2);
                }
                while (true)
                {
                    m++;
                    break label106;
                    i = 0;
                    break;
                    label177: ViewPropertyAnimator.this.setValue(localNameValuesHolder.mNameConstant, f2);
                }
                label194: if ((j & 0x1FF) != 0)
                {
                    ViewPropertyAnimator.this.mView.mTransformationInfo.mMatrixDirty = true;
                    if (i == 0)
                    {
                        View localView = ViewPropertyAnimator.this.mView;
                        localView.mPrivateFlags = (0x20 | localView.mPrivateFlags);
                    }
                }
                if (bool)
                    ViewPropertyAnimator.this.mView.invalidate(true);
                else
                    ViewPropertyAnimator.this.mView.invalidateViewProperty(false, false);
            }
        }
    }

    private static class NameValuesHolder
    {
        float mDeltaValue;
        float mFromValue;
        int mNameConstant;

        NameValuesHolder(int paramInt, float paramFloat1, float paramFloat2)
        {
            this.mNameConstant = paramInt;
            this.mFromValue = paramFloat1;
            this.mDeltaValue = paramFloat2;
        }
    }

    private static class PropertyBundle
    {
        ArrayList<ViewPropertyAnimator.NameValuesHolder> mNameValuesHolder;
        int mPropertyMask;

        PropertyBundle(int paramInt, ArrayList<ViewPropertyAnimator.NameValuesHolder> paramArrayList)
        {
            this.mPropertyMask = paramInt;
            this.mNameValuesHolder = paramArrayList;
        }

        boolean cancel(int paramInt)
        {
            int j;
            if (((paramInt & this.mPropertyMask) != 0) && (this.mNameValuesHolder != null))
            {
                int i = this.mNameValuesHolder.size();
                j = 0;
                if (j < i)
                    if (((ViewPropertyAnimator.NameValuesHolder)this.mNameValuesHolder.get(j)).mNameConstant == paramInt)
                    {
                        this.mNameValuesHolder.remove(j);
                        this.mPropertyMask &= (paramInt ^ 0xFFFFFFFF);
                    }
            }
            for (boolean bool = true; ; bool = false)
            {
                return bool;
                j++;
                break;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewPropertyAnimator
 * JD-Core Version:        0.6.2
 */