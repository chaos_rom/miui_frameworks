package android.view;

import android.content.ClipData;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IWindowSession extends IInterface
{
    public abstract int add(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect, InputChannel paramInputChannel)
        throws RemoteException;

    public abstract int addWithoutInputChannel(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect)
        throws RemoteException;

    public abstract void dragRecipientEntered(IWindow paramIWindow)
        throws RemoteException;

    public abstract void dragRecipientExited(IWindow paramIWindow)
        throws RemoteException;

    public abstract void finishDrawing(IWindow paramIWindow)
        throws RemoteException;

    public abstract void getDisplayFrame(IWindow paramIWindow, Rect paramRect)
        throws RemoteException;

    public abstract boolean getInTouchMode()
        throws RemoteException;

    public abstract boolean outOfMemory(IWindow paramIWindow)
        throws RemoteException;

    public abstract void performDeferredDestroy(IWindow paramIWindow)
        throws RemoteException;

    public abstract boolean performDrag(IWindow paramIWindow, IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, ClipData paramClipData)
        throws RemoteException;

    public abstract boolean performHapticFeedback(IWindow paramIWindow, int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract IBinder prepareDrag(IWindow paramIWindow, int paramInt1, int paramInt2, int paramInt3, Surface paramSurface)
        throws RemoteException;

    public abstract int relayout(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Rect paramRect1, Rect paramRect2, Rect paramRect3, Configuration paramConfiguration, Surface paramSurface)
        throws RemoteException;

    public abstract void remove(IWindow paramIWindow)
        throws RemoteException;

    public abstract void reportDropResult(IWindow paramIWindow, boolean paramBoolean)
        throws RemoteException;

    public abstract Bundle sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
        throws RemoteException;

    public abstract void setInTouchMode(boolean paramBoolean)
        throws RemoteException;

    public abstract void setInsets(IWindow paramIWindow, int paramInt, Rect paramRect1, Rect paramRect2, Region paramRegion)
        throws RemoteException;

    public abstract void setTransparentRegion(IWindow paramIWindow, Region paramRegion)
        throws RemoteException;

    public abstract void setWallpaperPosition(IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
        throws RemoteException;

    public abstract void wallpaperCommandComplete(IBinder paramIBinder, Bundle paramBundle)
        throws RemoteException;

    public abstract void wallpaperOffsetsComplete(IBinder paramIBinder)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWindowSession
    {
        private static final String DESCRIPTOR = "android.view.IWindowSession";
        static final int TRANSACTION_add = 1;
        static final int TRANSACTION_addWithoutInputChannel = 2;
        static final int TRANSACTION_dragRecipientEntered = 17;
        static final int TRANSACTION_dragRecipientExited = 18;
        static final int TRANSACTION_finishDrawing = 10;
        static final int TRANSACTION_getDisplayFrame = 9;
        static final int TRANSACTION_getInTouchMode = 12;
        static final int TRANSACTION_outOfMemory = 6;
        static final int TRANSACTION_performDeferredDestroy = 5;
        static final int TRANSACTION_performDrag = 15;
        static final int TRANSACTION_performHapticFeedback = 13;
        static final int TRANSACTION_prepareDrag = 14;
        static final int TRANSACTION_relayout = 4;
        static final int TRANSACTION_remove = 3;
        static final int TRANSACTION_reportDropResult = 16;
        static final int TRANSACTION_sendWallpaperCommand = 21;
        static final int TRANSACTION_setInTouchMode = 11;
        static final int TRANSACTION_setInsets = 8;
        static final int TRANSACTION_setTransparentRegion = 7;
        static final int TRANSACTION_setWallpaperPosition = 19;
        static final int TRANSACTION_wallpaperCommandComplete = 22;
        static final int TRANSACTION_wallpaperOffsetsComplete = 20;

        public Stub()
        {
            attachInterface(this, "android.view.IWindowSession");
        }

        public static IWindowSession asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.IWindowSession");
                if ((localIInterface != null) && ((localIInterface instanceof IWindowSession)))
                    localObject = (IWindowSession)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.view.IWindowSession");
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow10 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    int i17 = paramParcel1.readInt();
                    WindowManager.LayoutParams localLayoutParams3;
                    if (paramParcel1.readInt() != 0)
                    {
                        localLayoutParams3 = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel(paramParcel1);
                        int i18 = paramParcel1.readInt();
                        Rect localRect8 = new Rect();
                        InputChannel localInputChannel = new InputChannel();
                        int i19 = add(localIWindow10, i17, localLayoutParams3, i18, localRect8, localInputChannel);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i19);
                        if (localRect8 == null)
                            break label362;
                        paramParcel2.writeInt(1);
                        localRect8.writeToParcel(paramParcel2, 1);
                        if (localInputChannel == null)
                            break label370;
                        paramParcel2.writeInt(1);
                        localInputChannel.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool1 = true;
                        break;
                        localLayoutParams3 = null;
                        break label264;
                        paramParcel2.writeInt(0);
                        break label333;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow9 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    int i14 = paramParcel1.readInt();
                    WindowManager.LayoutParams localLayoutParams2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localLayoutParams2 = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel(paramParcel1);
                        int i15 = paramParcel1.readInt();
                        Rect localRect7 = new Rect();
                        int i16 = addWithoutInputChannel(localIWindow9, i14, localLayoutParams2, i15, localRect7);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i16);
                        if (localRect7 == null)
                            break label490;
                        paramParcel2.writeInt(1);
                        localRect7.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool1 = true;
                        break;
                        localLayoutParams2 = null;
                        break label420;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    remove(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow8 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    int i8 = paramParcel1.readInt();
                    WindowManager.LayoutParams localLayoutParams1;
                    if (paramParcel1.readInt() != 0)
                    {
                        localLayoutParams1 = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel(paramParcel1);
                        int i9 = paramParcel1.readInt();
                        int i10 = paramParcel1.readInt();
                        int i11 = paramParcel1.readInt();
                        int i12 = paramParcel1.readInt();
                        Rect localRect4 = new Rect();
                        Rect localRect5 = new Rect();
                        Rect localRect6 = new Rect();
                        Configuration localConfiguration = new Configuration();
                        Surface localSurface2 = new Surface();
                        int i13 = relayout(localIWindow8, i8, localLayoutParams1, i9, i10, i11, i12, localRect4, localRect5, localRect6, localConfiguration, localSurface2);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i13);
                        if (localRect4 == null)
                            break label773;
                        paramParcel2.writeInt(1);
                        localRect4.writeToParcel(paramParcel2, 1);
                        if (localRect5 == null)
                            break label781;
                        paramParcel2.writeInt(1);
                        localRect5.writeToParcel(paramParcel2, 1);
                        if (localRect6 == null)
                            break label789;
                        paramParcel2.writeInt(1);
                        localRect6.writeToParcel(paramParcel2, 1);
                        if (localConfiguration == null)
                            break label797;
                        paramParcel2.writeInt(1);
                        localConfiguration.writeToParcel(paramParcel2, 1);
                        if (localSurface2 == null)
                            break label805;
                        paramParcel2.writeInt(1);
                        localSurface2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool1 = true;
                        break;
                        localLayoutParams1 = null;
                        break label567;
                        paramParcel2.writeInt(0);
                        break label693;
                        paramParcel2.writeInt(0);
                        break label710;
                        paramParcel2.writeInt(0);
                        break label727;
                        paramParcel2.writeInt(0);
                        break label744;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    performDeferredDestroy(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    boolean bool9 = outOfMemory(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    if (bool9);
                    for (int i7 = 1; ; i7 = 0)
                    {
                        paramParcel2.writeInt(i7);
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow7 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() != 0);
                    for (Region localRegion2 = (Region)Region.CREATOR.createFromParcel(paramParcel1); ; localRegion2 = null)
                    {
                        setTransparentRegion(localIWindow7, localRegion2);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow6 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    int i6 = paramParcel1.readInt();
                    Rect localRect2;
                    Rect localRect3;
                    if (paramParcel1.readInt() != 0)
                    {
                        localRect2 = (Rect)Rect.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label1063;
                        localRect3 = (Rect)Rect.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label1069;
                    }
                    for (Region localRegion1 = (Region)Region.CREATOR.createFromParcel(paramParcel1); ; localRegion1 = null)
                    {
                        setInsets(localIWindow6, i6, localRect2, localRect3, localRegion1);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                        localRect2 = null;
                        break label991;
                        localRect3 = null;
                        break label1012;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow5 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    Rect localRect1 = new Rect();
                    getDisplayFrame(localIWindow5, localRect1);
                    paramParcel2.writeNoException();
                    if (localRect1 != null)
                    {
                        paramParcel2.writeInt(1);
                        localRect1.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool1 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    finishDrawing(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool8 = true; ; bool8 = false)
                    {
                        setInTouchMode(bool8);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    boolean bool7 = getInTouchMode();
                    paramParcel2.writeNoException();
                    if (bool7);
                    for (int i5 = 1; ; i5 = 0)
                    {
                        paramParcel2.writeInt(i5);
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow4 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    int i3 = paramParcel1.readInt();
                    boolean bool5;
                    if (paramParcel1.readInt() != 0)
                    {
                        bool5 = true;
                        boolean bool6 = performHapticFeedback(localIWindow4, i3, bool5);
                        paramParcel2.writeNoException();
                        if (!bool6)
                            break label1322;
                    }
                    for (int i4 = 1; ; i4 = 0)
                    {
                        paramParcel2.writeInt(i4);
                        bool1 = true;
                        break;
                        bool5 = false;
                        break label1280;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow3 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    int n = paramParcel1.readInt();
                    int i1 = paramParcel1.readInt();
                    int i2 = paramParcel1.readInt();
                    Surface localSurface1 = new Surface();
                    IBinder localIBinder4 = prepareDrag(localIWindow3, n, i1, i2, localSurface1);
                    paramParcel2.writeNoException();
                    paramParcel2.writeStrongBinder(localIBinder4);
                    if (localSurface1 != null)
                    {
                        paramParcel2.writeInt(1);
                        localSurface1.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool1 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow2 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    IBinder localIBinder3 = paramParcel1.readStrongBinder();
                    float f1 = paramParcel1.readFloat();
                    float f2 = paramParcel1.readFloat();
                    float f3 = paramParcel1.readFloat();
                    float f4 = paramParcel1.readFloat();
                    ClipData localClipData;
                    if (paramParcel1.readInt() != 0)
                    {
                        localClipData = (ClipData)ClipData.CREATOR.createFromParcel(paramParcel1);
                        boolean bool4 = performDrag(localIWindow2, localIBinder3, f1, f2, f3, f4, localClipData);
                        paramParcel2.writeNoException();
                        if (!bool4)
                            break label1543;
                    }
                    for (int m = 1; ; m = 0)
                    {
                        paramParcel2.writeInt(m);
                        bool1 = true;
                        break;
                        localClipData = null;
                        break label1493;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    IWindow localIWindow1 = IWindow.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool3 = true; ; bool3 = false)
                    {
                        reportDropResult(localIWindow1, bool3);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    dragRecipientEntered(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    dragRecipientExited(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    setWallpaperPosition(paramParcel1.readStrongBinder(), paramParcel1.readFloat(), paramParcel1.readFloat(), paramParcel1.readFloat(), paramParcel1.readFloat());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.IWindowSession");
                    wallpaperOffsetsComplete(paramParcel1.readStrongBinder());
                    paramParcel2.writeNoException();
                    bool1 = true;
                }
            case 21:
                label264: label333: label362: label370: label1012: paramParcel1.enforceInterface("android.view.IWindowSession");
                label420: label490: IBinder localIBinder2 = paramParcel1.readStrongBinder();
                label567: label710: label727: label744: String str = paramParcel1.readString();
                label693: label991: int i = paramParcel1.readInt();
                label773: label781: label789: label797: label805: label1063: label1069: label1493: int j = paramParcel1.readInt();
                label1280: label1322: int k = paramParcel1.readInt();
                label1543: Bundle localBundle2;
                label1773: boolean bool2;
                if (paramParcel1.readInt() != 0)
                {
                    localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label1836;
                    bool2 = true;
                    label1783: Bundle localBundle3 = sendWallpaperCommand(localIBinder2, str, i, j, k, localBundle2, bool2);
                    paramParcel2.writeNoException();
                    if (localBundle3 == null)
                        break label1842;
                    paramParcel2.writeInt(1);
                    localBundle3.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localBundle2 = null;
                    break label1773;
                    label1836: bool2 = false;
                    break label1783;
                    label1842: paramParcel2.writeInt(0);
                }
            case 22:
            }
            paramParcel1.enforceInterface("android.view.IWindowSession");
            IBinder localIBinder1 = paramParcel1.readStrongBinder();
            if (paramParcel1.readInt() != 0);
            for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
            {
                wallpaperCommandComplete(localIBinder1, localBundle1);
                paramParcel2.writeNoException();
                bool1 = true;
                break;
            }
        }

        private static class Proxy
            implements IWindowSession
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public int add(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect, InputChannel paramInputChannel)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    IBinder localIBinder;
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt1);
                        if (paramLayoutParams == null)
                            break label143;
                        localParcel1.writeInt(1);
                        paramLayoutParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        if (localParcel2.readInt() != 0)
                            paramRect.readFromParcel(localParcel2);
                        if (localParcel2.readInt() != 0)
                            paramInputChannel.readFromParcel(localParcel2);
                        return i;
                        localIBinder = null;
                        break;
                        label143: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int addWithoutInputChannel(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    IBinder localIBinder;
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt1);
                        if (paramLayoutParams == null)
                            break label128;
                        localParcel1.writeInt(1);
                        paramLayoutParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        if (localParcel2.readInt() != 0)
                            paramRect.readFromParcel(localParcel2);
                        return i;
                        localIBinder = null;
                        break;
                        label128: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dragRecipientEntered(IWindow paramIWindow)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(17, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void dragRecipientExited(IWindow paramIWindow)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(18, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void finishDrawing(IWindow paramIWindow)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getDisplayFrame(IWindow paramIWindow, Rect paramRect)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        if (localParcel2.readInt() != 0)
                            paramRect.readFromParcel(localParcel2);
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getInTouchMode()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.IWindowSession";
            }

            public boolean outOfMemory(IWindow paramIWindow)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        if (i != 0)
                            bool = true;
                        return bool;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void performDeferredDestroy(IWindow paramIWindow)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean performDrag(IWindow paramIWindow, IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, ClipData paramClipData)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.IWindowSession");
                        IBinder localIBinder;
                        if (paramIWindow != null)
                        {
                            localIBinder = paramIWindow.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            localParcel1.writeStrongBinder(paramIBinder);
                            localParcel1.writeFloat(paramFloat1);
                            localParcel1.writeFloat(paramFloat2);
                            localParcel1.writeFloat(paramFloat3);
                            localParcel1.writeFloat(paramFloat4);
                            if (paramClipData != null)
                            {
                                localParcel1.writeInt(1);
                                paramClipData.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(15, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label168;
                                return bool;
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label168: bool = false;
                }
            }

            public boolean performHapticFeedback(IWindow paramIWindow, int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    IBinder localIBinder;
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        if (!paramBoolean)
                            break label113;
                    }
                    label113: int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break label119;
                        return i;
                        localIBinder = null;
                        break;
                    }
                    label119: i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder prepareDrag(IWindow paramIWindow, int paramInt1, int paramInt2, int paramInt3, Surface paramSurface)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder1 = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder1);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        localParcel1.writeInt(paramInt3);
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        IBinder localIBinder2 = localParcel2.readStrongBinder();
                        if (localParcel2.readInt() != 0)
                            paramSurface.readFromParcel(localParcel2);
                        return localIBinder2;
                    }
                    IBinder localIBinder1 = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int relayout(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Rect paramRect1, Rect paramRect2, Rect paramRect3, Configuration paramConfiguration, Surface paramSurface)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    IBinder localIBinder;
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt1);
                        if (paramLayoutParams == null)
                            break label209;
                        localParcel1.writeInt(1);
                        paramLayoutParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt2);
                        localParcel1.writeInt(paramInt3);
                        localParcel1.writeInt(paramInt4);
                        localParcel1.writeInt(paramInt5);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        if (localParcel2.readInt() != 0)
                            paramRect1.readFromParcel(localParcel2);
                        if (localParcel2.readInt() != 0)
                            paramRect2.readFromParcel(localParcel2);
                        if (localParcel2.readInt() != 0)
                            paramRect3.readFromParcel(localParcel2);
                        if (localParcel2.readInt() != 0)
                            paramConfiguration.readFromParcel(localParcel2);
                        if (localParcel2.readInt() != 0)
                            paramSurface.readFromParcel(localParcel2);
                        return i;
                        localIBinder = null;
                        break;
                        label209: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void remove(IWindow paramIWindow)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reportDropResult(IWindow paramIWindow, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramBoolean)
                            i = 1;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(16, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Bundle sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    Bundle localBundle;
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.IWindowSession");
                        localParcel1.writeStrongBinder(paramIBinder);
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        localParcel1.writeInt(paramInt3);
                        if (paramBundle != null)
                        {
                            localParcel1.writeInt(1);
                            paramBundle.writeToParcel(localParcel1, 0);
                            break label175;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(21, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localBundle = (Bundle)Bundle.CREATOR.createFromParcel(localParcel2);
                                label126: return localBundle;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label175: 
                    do
                    {
                        i = 0;
                        break;
                        localBundle = null;
                        break label126;
                    }
                    while (!paramBoolean);
                }
            }

            public void setInTouchMode(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInsets(IWindow paramIWindow, int paramInt, Rect paramRect1, Rect paramRect2, Region paramRegion)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.IWindowSession");
                        IBinder localIBinder;
                        if (paramIWindow != null)
                        {
                            localIBinder = paramIWindow.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            localParcel1.writeInt(paramInt);
                            if (paramRect1 != null)
                            {
                                localParcel1.writeInt(1);
                                paramRect1.writeToParcel(localParcel1, 0);
                                if (paramRect2 == null)
                                    break label160;
                                localParcel1.writeInt(1);
                                paramRect2.writeToParcel(localParcel1, 0);
                                if (paramRegion == null)
                                    break label169;
                                localParcel1.writeInt(1);
                                paramRegion.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(8, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label160: localParcel1.writeInt(0);
                    continue;
                    label169: localParcel1.writeInt(0);
                }
            }

            public void setTransparentRegion(IWindow paramIWindow, Region paramRegion)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    IBinder localIBinder;
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramRegion == null)
                            break label85;
                        localParcel1.writeInt(1);
                        paramRegion.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localIBinder = null;
                        break;
                        label85: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setWallpaperPosition(IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeFloat(paramFloat1);
                    localParcel1.writeFloat(paramFloat2);
                    localParcel1.writeFloat(paramFloat3);
                    localParcel1.writeFloat(paramFloat4);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void wallpaperCommandComplete(IBinder paramIBinder, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(22, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void wallpaperOffsetsComplete(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IWindowSession");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.IWindowSession
 * JD-Core Version:        0.6.2
 */