package android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RemoteViews.RemoteView;
import com.android.internal.R.styleable;
import java.lang.ref.WeakReference;

@RemoteViews.RemoteView
public final class ViewStub extends View
{
    private OnInflateListener mInflateListener;
    private int mInflatedId;
    private WeakReference<View> mInflatedViewRef;
    private LayoutInflater mInflater;
    private int mLayoutResource = 0;

    public ViewStub(Context paramContext)
    {
        initialize(paramContext);
    }

    public ViewStub(Context paramContext, int paramInt)
    {
        this.mLayoutResource = paramInt;
        initialize(paramContext);
    }

    public ViewStub(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public ViewStub(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewStub, paramInt, 0);
        this.mInflatedId = localTypedArray1.getResourceId(1, -1);
        this.mLayoutResource = localTypedArray1.getResourceId(0, 0);
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.View, paramInt, 0);
        this.mID = localTypedArray2.getResourceId(8, -1);
        localTypedArray2.recycle();
        initialize(paramContext);
    }

    private void initialize(Context paramContext)
    {
        this.mContext = paramContext;
        setVisibility(8);
        setWillNotDraw(true);
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
    }

    public void draw(Canvas paramCanvas)
    {
    }

    public int getInflatedId()
    {
        return this.mInflatedId;
    }

    public LayoutInflater getLayoutInflater()
    {
        return this.mInflater;
    }

    public int getLayoutResource()
    {
        return this.mLayoutResource;
    }

    public View inflate()
    {
        ViewParent localViewParent = getParent();
        if ((localViewParent != null) && ((localViewParent instanceof ViewGroup)))
        {
            if (this.mLayoutResource != 0)
            {
                ViewGroup localViewGroup = (ViewGroup)localViewParent;
                LayoutInflater localLayoutInflater;
                View localView;
                int i;
                if (this.mInflater != null)
                {
                    localLayoutInflater = this.mInflater;
                    localView = localLayoutInflater.inflate(this.mLayoutResource, localViewGroup, false);
                    if (this.mInflatedId != -1)
                        localView.setId(this.mInflatedId);
                    i = localViewGroup.indexOfChild(this);
                    localViewGroup.removeViewInLayout(this);
                    ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
                    if (localLayoutParams == null)
                        break label149;
                    localViewGroup.addView(localView, i, localLayoutParams);
                }
                while (true)
                {
                    this.mInflatedViewRef = new WeakReference(localView);
                    if (this.mInflateListener != null)
                        this.mInflateListener.onInflate(this, localView);
                    return localView;
                    localLayoutInflater = LayoutInflater.from(this.mContext);
                    break;
                    label149: localViewGroup.addView(localView, i);
                }
            }
            throw new IllegalArgumentException("ViewStub must have a valid layoutResource");
        }
        throw new IllegalStateException("ViewStub must have a non-null ViewGroup viewParent");
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        setMeasuredDimension(0, 0);
    }

    @RemotableViewMethod
    public void setInflatedId(int paramInt)
    {
        this.mInflatedId = paramInt;
    }

    public void setLayoutInflater(LayoutInflater paramLayoutInflater)
    {
        this.mInflater = paramLayoutInflater;
    }

    @RemotableViewMethod
    public void setLayoutResource(int paramInt)
    {
        this.mLayoutResource = paramInt;
    }

    public void setOnInflateListener(OnInflateListener paramOnInflateListener)
    {
        this.mInflateListener = paramOnInflateListener;
    }

    @RemotableViewMethod
    public void setVisibility(int paramInt)
    {
        if (this.mInflatedViewRef != null)
        {
            View localView = (View)this.mInflatedViewRef.get();
            if (localView != null)
                localView.setVisibility(paramInt);
        }
        while (true)
        {
            return;
            throw new IllegalStateException("setVisibility called on un-referenced view");
            super.setVisibility(paramInt);
            if ((paramInt == 0) || (paramInt == 4))
                inflate();
        }
    }

    public static abstract interface OnInflateListener
    {
        public abstract void onInflate(ViewStub paramViewStub, View paramView);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewStub
 * JD-Core Version:        0.6.2
 */