package android.view;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.AudioService;
import android.media.AudioSystem;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import java.util.HashMap;

public class VolumePanel extends Handler
    implements SeekBar.OnSeekBarChangeListener, View.OnClickListener
{
    private static final int BEEP_DURATION = 150;
    private static final int FREE_DELAY = 10000;
    private static boolean LOGD = false;
    private static final int MAX_VOLUME = 100;
    private static final int MSG_FREE_RESOURCES = 1;
    private static final int MSG_MUTE_CHANGED = 7;
    private static final int MSG_PLAY_SOUND = 2;
    private static final int MSG_REMOTE_VOLUME_CHANGED = 8;
    private static final int MSG_REMOTE_VOLUME_UPDATE_IF_SHOWN = 9;
    private static final int MSG_RINGER_MODE_CHANGED = 6;
    private static final int MSG_SLIDER_VISIBILITY_CHANGED = 10;
    private static final int MSG_STOP_SOUNDS = 3;
    private static final int MSG_TIMEOUT = 5;
    private static final int MSG_VIBRATE = 4;
    private static final int MSG_VOLUME_CHANGED = 0;
    public static final int PLAY_SOUND_DELAY = 300;
    private static final StreamResources[] STREAMS = arrayOfStreamResources;
    private static final int STREAM_MASTER = -100;
    private static final String TAG = "VolumePanel";
    private static final int TIMEOUT_DELAY = 3000;
    public static final int VIBRATE_DELAY = 300;
    private static final int VIBRATE_DURATION = 300;
    private int mActiveStreamType = -1;
    private AudioManager mAudioManager;
    protected AudioService mAudioService;
    protected Context mContext;
    private final Dialog mDialog;
    private final View mDivider;
    private final View mMoreButton;
    private final ViewGroup mPanel;
    private boolean mRingIsSilent;
    private boolean mShowCombinedVolumes;
    private final ViewGroup mSliderGroup;
    private HashMap<Integer, StreamControl> mStreamControls;
    private ToneGenerator[] mToneGenerators;
    private Vibrator mVibrator;
    private final View mView;
    private boolean mVoiceCapable;

    static
    {
        StreamResources[] arrayOfStreamResources = new StreamResources[8];
        arrayOfStreamResources[0] = StreamResources.BluetoothSCOStream;
        arrayOfStreamResources[1] = StreamResources.RingerStream;
        arrayOfStreamResources[2] = StreamResources.VoiceStream;
        arrayOfStreamResources[3] = StreamResources.MediaStream;
        arrayOfStreamResources[4] = StreamResources.NotificationStream;
        arrayOfStreamResources[5] = StreamResources.AlarmStream;
        arrayOfStreamResources[6] = StreamResources.MasterStream;
        arrayOfStreamResources[7] = StreamResources.RemoteStream;
    }

    public VolumePanel(Context paramContext, AudioService paramAudioService)
    {
        this.mContext = paramContext;
        this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
        this.mAudioService = paramAudioService;
        boolean bool1 = paramContext.getResources().getBoolean(17891338);
        if (bool1)
        {
            int i = 0;
            if (i < STREAMS.length)
            {
                StreamResources localStreamResources = STREAMS[i];
                if (localStreamResources.streamType == -100);
                for (boolean bool3 = true; ; bool3 = false)
                {
                    localStreamResources.show = bool3;
                    i++;
                    break;
                }
            }
        }
        this.mView = ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(17367235, null);
        this.mView.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
            {
                VolumePanel.this.resetTimeout();
                return false;
            }
        });
        this.mPanel = ((ViewGroup)this.mView.findViewById(16909153));
        this.mSliderGroup = ((ViewGroup)this.mView.findViewById(16909154));
        this.mMoreButton = ((ImageView)this.mView.findViewById(16909022));
        this.mDivider = ((ImageView)this.mView.findViewById(16909155));
        this.mDialog = new Dialog(paramContext, 16974581)
        {
            public boolean onTouchEvent(MotionEvent paramAnonymousMotionEvent)
            {
                if ((isShowing()) && (paramAnonymousMotionEvent.getAction() == 4))
                    VolumePanel.this.forceTimeout();
                for (boolean bool = true; ; bool = false)
                    return bool;
            }
        };
        this.mDialog.setTitle("Volume control");
        this.mDialog.setContentView(this.mView);
        this.mDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            public void onDismiss(DialogInterface paramAnonymousDialogInterface)
            {
                VolumePanel.access$202(VolumePanel.this, -1);
                VolumePanel.this.mAudioManager.forceVolumeControlStream(VolumePanel.this.mActiveStreamType);
            }
        });
        Window localWindow = this.mDialog.getWindow();
        localWindow.setGravity(48);
        WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
        localLayoutParams.token = null;
        localLayoutParams.y = this.mContext.getResources().getDimensionPixelOffset(17104970);
        localLayoutParams.type = 2020;
        localLayoutParams.width = -2;
        localLayoutParams.height = -2;
        localWindow.setAttributes(localLayoutParams);
        localWindow.addFlags(262184);
        this.mToneGenerators = new ToneGenerator[AudioSystem.getNumStreamTypes()];
        this.mVibrator = ((Vibrator)paramContext.getSystemService("vibrator"));
        this.mVoiceCapable = paramContext.getResources().getBoolean(17891368);
        boolean bool2;
        if ((!this.mVoiceCapable) && (!bool1))
        {
            bool2 = true;
            this.mShowCombinedVolumes = bool2;
            if (this.mShowCombinedVolumes)
                break label424;
            this.mMoreButton.setVisibility(8);
            this.mDivider.setVisibility(8);
        }
        while (true)
        {
            listenToRingerMode();
            return;
            bool2 = false;
            break;
            label424: this.mMoreButton.setOnClickListener(this);
        }
    }

    private void addOtherVolumes()
    {
        if (!this.mShowCombinedVolumes)
            return;
        int i = 0;
        label10: int j;
        if (i < STREAMS.length)
        {
            j = STREAMS[i].streamType;
            if ((STREAMS[i].show) && (j != this.mActiveStreamType))
                break label52;
        }
        while (true)
        {
            i++;
            break label10;
            break;
            label52: StreamControl localStreamControl = (StreamControl)this.mStreamControls.get(Integer.valueOf(j));
            this.mSliderGroup.addView(localStreamControl.group);
            updateSlider(localStreamControl);
        }
    }

    private void collapse()
    {
        this.mMoreButton.setVisibility(0);
        this.mDivider.setVisibility(0);
        int i = this.mSliderGroup.getChildCount();
        for (int j = 1; j < i; j++)
            this.mSliderGroup.getChildAt(j).setVisibility(8);
    }

    private void createSliders()
    {
        LayoutInflater localLayoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
        this.mStreamControls = new HashMap(STREAMS.length);
        Resources localResources = this.mContext.getResources();
        int i = 0;
        if (i < STREAMS.length)
        {
            StreamResources localStreamResources = STREAMS[i];
            int j = localStreamResources.streamType;
            if ((this.mVoiceCapable) && (localStreamResources == StreamResources.NotificationStream))
                localStreamResources = StreamResources.RingerStream;
            StreamControl localStreamControl = new StreamControl(null);
            localStreamControl.streamType = j;
            localStreamControl.group = ((ViewGroup)localLayoutInflater.inflate(17367236, null));
            localStreamControl.group.setTag(localStreamControl);
            localStreamControl.icon = ((ImageView)localStreamControl.group.findViewById(16909156));
            localStreamControl.icon.setTag(localStreamControl);
            localStreamControl.icon.setContentDescription(localResources.getString(localStreamResources.descRes));
            localStreamControl.iconRes = localStreamResources.iconRes;
            localStreamControl.iconMuteRes = localStreamResources.iconMuteRes;
            localStreamControl.icon.setImageResource(localStreamControl.iconRes);
            localStreamControl.seekbarView = ((SeekBar)localStreamControl.group.findViewById(16909068));
            if ((j == 6) || (j == 0));
            for (int k = 1; ; k = 0)
            {
                localStreamControl.seekbarView.setMax(k + getStreamMaxVolume(j));
                localStreamControl.seekbarView.setOnSeekBarChangeListener(this);
                localStreamControl.seekbarView.setTag(localStreamControl);
                this.mStreamControls.put(Integer.valueOf(j), localStreamControl);
                i++;
                break;
            }
        }
    }

    private void expand()
    {
        int i = this.mSliderGroup.getChildCount();
        for (int j = 0; j < i; j++)
            this.mSliderGroup.getChildAt(j).setVisibility(0);
        this.mMoreButton.setVisibility(4);
        this.mDivider.setVisibility(4);
    }

    private void forceTimeout()
    {
        removeMessages(5);
        sendMessage(obtainMessage(5));
    }

    // ERROR //
    private ToneGenerator getOrCreateToneGenerator(int paramInt)
    {
        // Byte code:
        //     0: iload_1
        //     1: bipush 156
        //     3: if_icmpne +9 -> 12
        //     6: aconst_null
        //     7: astore 4
        //     9: aload 4
        //     11: areturn
        //     12: aload_0
        //     13: monitorenter
        //     14: aload_0
        //     15: getfield 283	android/view/VolumePanel:mToneGenerators	[Landroid/media/ToneGenerator;
        //     18: iload_1
        //     19: aaload
        //     20: astore_3
        //     21: aload_3
        //     22: ifnonnull +19 -> 41
        //     25: aload_0
        //     26: getfield 283	android/view/VolumePanel:mToneGenerators	[Landroid/media/ToneGenerator;
        //     29: iload_1
        //     30: new 281	android/media/ToneGenerator
        //     33: dup
        //     34: iload_1
        //     35: bipush 100
        //     37: invokespecial 431	android/media/ToneGenerator:<init>	(II)V
        //     40: aastore
        //     41: aload_0
        //     42: getfield 283	android/view/VolumePanel:mToneGenerators	[Landroid/media/ToneGenerator;
        //     45: iload_1
        //     46: aaload
        //     47: astore 4
        //     49: aload_0
        //     50: monitorexit
        //     51: goto -42 -> 9
        //     54: astore_2
        //     55: aload_0
        //     56: monitorexit
        //     57: aload_2
        //     58: athrow
        //     59: astore 5
        //     61: getstatic 97	android/view/VolumePanel:LOGD	Z
        //     64: ifeq -23 -> 41
        //     67: ldc 63
        //     69: new 433	java/lang/StringBuilder
        //     72: dup
        //     73: invokespecial 434	java/lang/StringBuilder:<init>	()V
        //     76: ldc_w 436
        //     79: invokevirtual 440	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     82: aload 5
        //     84: invokevirtual 443	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     87: invokevirtual 447	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     90: invokestatic 453	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     93: pop
        //     94: goto -53 -> 41
        //
        // Exception table:
        //     from	to	target	type
        //     14	21	54	finally
        //     25	41	54	finally
        //     41	57	54	finally
        //     61	94	54	finally
        //     25	41	59	java/lang/RuntimeException
    }

    private int getStreamMaxVolume(int paramInt)
    {
        int i;
        if (paramInt == -100)
            i = this.mAudioManager.getMasterMaxVolume();
        while (true)
        {
            return i;
            if (paramInt == -200)
                i = this.mAudioService.getRemoteStreamMaxVolume();
            else
                i = this.mAudioManager.getStreamMaxVolume(paramInt);
        }
    }

    private int getStreamVolume(int paramInt)
    {
        int i;
        if (paramInt == -100)
            i = this.mAudioManager.getMasterVolume();
        while (true)
        {
            return i;
            if (paramInt == -200)
                i = this.mAudioService.getRemoteStreamVolume();
            else
                i = this.mAudioManager.getStreamVolume(paramInt);
        }
    }

    private boolean isExpanded()
    {
        if (this.mMoreButton.getVisibility() != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isMuted(int paramInt)
    {
        boolean bool;
        if (paramInt == -100)
            bool = this.mAudioManager.isMasterMute();
        while (true)
        {
            return bool;
            if (paramInt == -200)
            {
                if (this.mAudioService.getRemoteStreamVolume() <= 0)
                    bool = true;
                else
                    bool = false;
            }
            else
                bool = this.mAudioManager.isStreamMute(paramInt);
        }
    }

    private void listenToRingerMode()
    {
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.media.RINGER_MODE_CHANGED");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if ("android.media.RINGER_MODE_CHANGED".equals(paramAnonymousIntent.getAction()))
                {
                    VolumePanel.this.removeMessages(6);
                    VolumePanel.this.sendMessage(VolumePanel.this.obtainMessage(6));
                }
            }
        }
        , localIntentFilter);
    }

    private void reorderSliders(int paramInt)
    {
        this.mSliderGroup.removeAllViews();
        StreamControl localStreamControl = (StreamControl)this.mStreamControls.get(Integer.valueOf(paramInt));
        if (localStreamControl == null)
        {
            Log.e("VolumePanel", "Missing stream type! - " + paramInt);
            this.mActiveStreamType = -1;
        }
        while (true)
        {
            addOtherVolumes();
            return;
            this.mSliderGroup.addView(localStreamControl.group);
            this.mActiveStreamType = paramInt;
            localStreamControl.group.setVisibility(0);
            updateSlider(localStreamControl);
        }
    }

    private void resetTimeout()
    {
        removeMessages(5);
        sendMessageDelayed(obtainMessage(5), 3000L);
    }

    private void setMusicIcon(int paramInt1, int paramInt2)
    {
        StreamControl localStreamControl = (StreamControl)this.mStreamControls.get(Integer.valueOf(3));
        ImageView localImageView;
        if (localStreamControl != null)
        {
            localStreamControl.iconRes = paramInt1;
            localStreamControl.iconMuteRes = paramInt2;
            localImageView = localStreamControl.icon;
            if (!isMuted(localStreamControl.streamType))
                break label60;
        }
        label60: for (int i = localStreamControl.iconMuteRes; ; i = localStreamControl.iconRes)
        {
            localImageView.setImageResource(i);
            return;
        }
    }

    private void setStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt1 == -100)
            this.mAudioManager.setMasterVolume(paramInt2, paramInt3);
        while (true)
        {
            return;
            if (paramInt1 == -200)
                this.mAudioService.setRemoteStreamVolume(paramInt2);
            else
                this.mAudioManager.setStreamVolume(paramInt1, paramInt2, paramInt3);
        }
    }

    private void updateSlider(StreamControl paramStreamControl)
    {
        paramStreamControl.seekbarView.setProgress(getStreamVolume(paramStreamControl.streamType));
        boolean bool = isMuted(paramStreamControl.streamType);
        ImageView localImageView = paramStreamControl.icon;
        int i;
        if (bool)
        {
            i = paramStreamControl.iconMuteRes;
            localImageView.setImageResource(i);
            if ((paramStreamControl.streamType == 2) && (this.mAudioManager.getRingerMode() == 1))
                paramStreamControl.icon.setImageResource(17302181);
            if (paramStreamControl.streamType != -200)
                break label102;
            paramStreamControl.seekbarView.setEnabled(true);
        }
        while (true)
        {
            return;
            i = paramStreamControl.iconRes;
            break;
            label102: if ((paramStreamControl.streamType != this.mAudioManager.getMasterStreamType()) && (bool))
                paramStreamControl.seekbarView.setEnabled(false);
            else
                paramStreamControl.seekbarView.setEnabled(true);
        }
    }

    private void updateStates()
    {
        int i = this.mSliderGroup.getChildCount();
        for (int j = 0; j < i; j++)
            updateSlider((StreamControl)this.mSliderGroup.getChildAt(j).getTag());
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 0:
        case 7:
        case 1:
        case 3:
        case 2:
        case 4:
        case 5:
        case 6:
        case 8:
        case 9:
        case 10:
        }
        while (true)
        {
            return;
            onVolumeChanged(paramMessage.arg1, paramMessage.arg2);
            continue;
            onMuteChanged(paramMessage.arg1, paramMessage.arg2);
            continue;
            onFreeResources();
            continue;
            onStopSounds();
            continue;
            onPlaySound(paramMessage.arg1, paramMessage.arg2);
            continue;
            onVibrate();
            continue;
            if (this.mDialog.isShowing())
            {
                this.mDialog.dismiss();
                this.mActiveStreamType = -1;
                continue;
                if (this.mDialog.isShowing())
                {
                    updateStates();
                    continue;
                    onRemoteVolumeChanged(paramMessage.arg1, paramMessage.arg2);
                    continue;
                    onRemoteVolumeUpdateIfShown();
                    continue;
                    onSliderVisibilityChanged(paramMessage.arg1, paramMessage.arg2);
                }
            }
        }
    }

    public void onClick(View paramView)
    {
        if (paramView == this.mMoreButton)
            expand();
        resetTimeout();
    }

    protected void onFreeResources()
    {
        try
        {
            for (int i = -1 + this.mToneGenerators.length; i >= 0; i--)
            {
                if (this.mToneGenerators[i] != null)
                    this.mToneGenerators[i].release();
                this.mToneGenerators[i] = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void onMuteChanged(int paramInt1, int paramInt2)
    {
        if (LOGD)
            Log.d("VolumePanel", "onMuteChanged(streamType: " + paramInt1 + ", flags: " + paramInt2 + ")");
        StreamControl localStreamControl = (StreamControl)this.mStreamControls.get(Integer.valueOf(paramInt1));
        ImageView localImageView;
        if (localStreamControl != null)
        {
            localImageView = localStreamControl.icon;
            if (!isMuted(localStreamControl.streamType))
                break label104;
        }
        label104: for (int i = localStreamControl.iconMuteRes; ; i = localStreamControl.iconRes)
        {
            localImageView.setImageResource(i);
            onVolumeChanged(paramInt1, paramInt2);
            return;
        }
    }

    protected void onPlaySound(int paramInt1, int paramInt2)
    {
        if (hasMessages(3))
        {
            removeMessages(3);
            onStopSounds();
        }
        try
        {
            ToneGenerator localToneGenerator = getOrCreateToneGenerator(paramInt1);
            if (localToneGenerator != null)
            {
                localToneGenerator.startTone(24);
                sendMessageDelayed(obtainMessage(3), 150L);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
    {
        Object localObject = paramSeekBar.getTag();
        if ((paramBoolean) && ((localObject instanceof StreamControl)))
        {
            StreamControl localStreamControl = (StreamControl)localObject;
            if (getStreamVolume(localStreamControl.streamType) != paramInt)
                setStreamVolume(localStreamControl.streamType, paramInt, 0);
        }
        resetTimeout();
    }

    protected void onRemoteVolumeChanged(int paramInt1, int paramInt2)
    {
        if (LOGD)
            Log.d("VolumePanel", "onRemoteVolumeChanged(stream:" + paramInt1 + ", flags: " + paramInt2 + ")");
        if (((paramInt2 & 0x1) != 0) || (this.mDialog.isShowing()));
        while (true)
        {
            try
            {
                if (this.mActiveStreamType != -200)
                    reorderSliders(-200);
                onShowVolumeChanged(-200, paramInt2);
                if (((paramInt2 & 0x4) != 0) && (!this.mRingIsSilent))
                {
                    removeMessages(2);
                    sendMessageDelayed(obtainMessage(2, paramInt1, paramInt2), 300L);
                }
                if ((paramInt2 & 0x8) != 0)
                {
                    removeMessages(2);
                    removeMessages(4);
                    onStopSounds();
                }
                removeMessages(1);
                sendMessageDelayed(obtainMessage(1), 10000L);
                resetTimeout();
                return;
            }
            finally
            {
            }
            if (LOGD)
                Log.d("VolumePanel", "not calling onShowVolumeChanged(), no FLAG_SHOW_UI or no UI");
        }
    }

    protected void onRemoteVolumeUpdateIfShown()
    {
        if (LOGD)
            Log.d("VolumePanel", "onRemoteVolumeUpdateIfShown()");
        if ((this.mDialog.isShowing()) && (this.mActiveStreamType == -200) && (this.mStreamControls != null))
            onShowVolumeChanged(-200, 0);
    }

    protected void onShowVolumeChanged(int paramInt1, int paramInt2)
    {
        int i = getStreamVolume(paramInt1);
        this.mRingIsSilent = false;
        if (LOGD)
            Log.d("VolumePanel", "onShowVolumeChanged(streamType: " + paramInt1 + ", flags: " + paramInt2 + "), index: " + i);
        int j = getStreamMaxVolume(paramInt1);
        StreamControl localStreamControl;
        switch (paramInt1)
        {
        case 4:
        default:
            localStreamControl = (StreamControl)this.mStreamControls.get(Integer.valueOf(paramInt1));
            if (localStreamControl != null)
            {
                if (localStreamControl.seekbarView.getMax() != j)
                    localStreamControl.seekbarView.setMax(j);
                localStreamControl.seekbarView.setProgress(i);
                if ((paramInt1 != this.mAudioManager.getMasterStreamType()) && (paramInt1 != -200) && (isMuted(paramInt1)))
                    localStreamControl.seekbarView.setEnabled(false);
            }
            else
            {
                label224: if (!this.mDialog.isShowing())
                    if (paramInt1 != -200)
                        break label488;
            }
            break;
        case 2:
        case 3:
        case 0:
        case 5:
        case 6:
        case -200:
        }
        label488: for (int k = -1; ; k = paramInt1)
        {
            this.mAudioManager.forceVolumeControlStream(k);
            this.mDialog.setContentView(this.mView);
            if (this.mShowCombinedVolumes)
                collapse();
            this.mDialog.show();
            if ((paramInt1 != -200) && ((paramInt2 & 0x10) != 0) && (this.mAudioService.isStreamAffectedByRingerMode(paramInt1)) && (this.mAudioManager.getRingerMode() == 1))
                sendMessageDelayed(obtainMessage(4), 300L);
            return;
            if (RingtoneManager.getActualDefaultRingtoneUri(this.mContext, 1) != null)
                break;
            this.mRingIsSilent = true;
            break;
            if ((0x380 & this.mAudioManager.getDevicesForStream(3)) != 0)
            {
                setMusicIcon(17302174, 17302175);
                break;
            }
            setMusicIcon(17302182, 17302183);
            break;
            i++;
            j++;
            break;
            if (RingtoneManager.getActualDefaultRingtoneUri(this.mContext, 2) != null)
                break;
            this.mRingIsSilent = true;
            break;
            i++;
            j++;
            break;
            if (!LOGD)
                break;
            Log.d("VolumePanel", "showing remote volume " + i + " over " + j);
            break;
            localStreamControl.seekbarView.setEnabled(true);
            break label224;
        }
    }

    /** @deprecated */
    protected void onSliderVisibilityChanged(int paramInt1, int paramInt2)
    {
        int i = 1;
        while (true)
            try
            {
                if (LOGD)
                {
                    Log.d("VolumePanel", "onSliderVisibilityChanged(stream=" + paramInt1 + ", visi=" + paramInt2 + ")");
                    continue;
                    int j = -1 + STREAMS.length;
                    if (j >= 0)
                    {
                        StreamResources localStreamResources = STREAMS[j];
                        if (localStreamResources.streamType != paramInt1)
                            continue;
                        localStreamResources.show = i;
                        if ((i == 0) && (this.mActiveStreamType == paramInt1))
                            this.mActiveStreamType = -1;
                    }
                    return;
                    i = 0;
                    continue;
                    j--;
                }
            }
            finally
            {
            }
    }

    public void onStartTrackingTouch(SeekBar paramSeekBar)
    {
    }

    protected void onStopSounds()
    {
        while (true)
        {
            int i;
            try
            {
                i = -1 + AudioSystem.getNumStreamTypes();
                if (i >= 0)
                {
                    ToneGenerator localToneGenerator = this.mToneGenerators[i];
                    if (localToneGenerator != null)
                        localToneGenerator.stopTone();
                }
                else
                {
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            i--;
        }
    }

    public void onStopTrackingTouch(SeekBar paramSeekBar)
    {
        Object localObject = paramSeekBar.getTag();
        if (((localObject instanceof StreamControl)) && (((StreamControl)localObject).streamType == -200))
            paramSeekBar.setProgress(getStreamVolume(-200));
    }

    protected void onVibrate()
    {
        if (this.mAudioManager.getRingerMode() != 1);
        while (true)
        {
            return;
            this.mVibrator.vibrate(300L);
        }
    }

    protected void onVolumeChanged(int paramInt1, int paramInt2)
    {
        if (LOGD)
            Log.d("VolumePanel", "onVolumeChanged(streamType: " + paramInt1 + ", flags: " + paramInt2 + ")");
        if ((paramInt2 & 0x1) != 0);
        try
        {
            if (this.mActiveStreamType != paramInt1)
                reorderSliders(paramInt1);
            onShowVolumeChanged(paramInt1, paramInt2);
            if (((paramInt2 & 0x4) != 0) && (!this.mRingIsSilent))
            {
                removeMessages(2);
                sendMessageDelayed(obtainMessage(2, paramInt1, paramInt2), 300L);
            }
            if ((paramInt2 & 0x8) != 0)
            {
                removeMessages(2);
                removeMessages(4);
                onStopSounds();
            }
            removeMessages(1);
            sendMessageDelayed(obtainMessage(1), 10000L);
            resetTimeout();
            return;
        }
        finally
        {
        }
    }

    public void postHasNewRemotePlaybackInfo()
    {
        if (hasMessages(9));
        while (true)
        {
            return;
            obtainMessage(9).sendToTarget();
        }
    }

    public void postMasterMuteChanged(int paramInt)
    {
        postMuteChanged(-100, paramInt);
    }

    public void postMasterVolumeChanged(int paramInt)
    {
        postVolumeChanged(-100, paramInt);
    }

    public void postMuteChanged(int paramInt1, int paramInt2)
    {
        if (hasMessages(0));
        while (true)
        {
            return;
            try
            {
                if (this.mStreamControls == null)
                    createSliders();
                removeMessages(1);
                obtainMessage(7, paramInt1, paramInt2).sendToTarget();
            }
            finally
            {
            }
        }
    }

    public void postRemoteSliderVisibility(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            obtainMessage(10, -200, i).sendToTarget();
            return;
        }
    }

    public void postRemoteVolumeChanged(int paramInt1, int paramInt2)
    {
        if (hasMessages(8));
        while (true)
        {
            return;
            try
            {
                if (this.mStreamControls == null)
                    createSliders();
                removeMessages(1);
                obtainMessage(8, paramInt1, paramInt2).sendToTarget();
            }
            finally
            {
            }
        }
    }

    public void postVolumeChanged(int paramInt1, int paramInt2)
    {
        if (hasMessages(0));
        while (true)
        {
            return;
            try
            {
                if (this.mStreamControls == null)
                    createSliders();
                removeMessages(1);
                obtainMessage(0, paramInt1, paramInt2).sendToTarget();
            }
            finally
            {
            }
        }
    }

    private class StreamControl
    {
        ViewGroup group;
        ImageView icon;
        int iconMuteRes;
        int iconRes;
        SeekBar seekbarView;
        int streamType;

        private StreamControl()
        {
        }
    }

    private static enum StreamResources
    {
        int descRes;
        int iconMuteRes;
        int iconRes;
        boolean show;
        int streamType;

        static
        {
            AlarmStream = new StreamResources("AlarmStream", 3, 4, 17040374, 17302172, 17302173, false);
            MediaStream = new StreamResources("MediaStream", 4, 3, 17040380, 17302182, 17302183, true);
            NotificationStream = new StreamResources("NotificationStream", 5, 5, 17040381, 17302176, 17302177, true);
            MasterStream = new StreamResources("MasterStream", 6, -100, 17040380, 17302182, 17302183, false);
            RemoteStream = new StreamResources("RemoteStream", 7, -200, 17040380, 17302293, 17302287, false);
            StreamResources[] arrayOfStreamResources = new StreamResources[8];
            arrayOfStreamResources[0] = BluetoothSCOStream;
            arrayOfStreamResources[1] = RingerStream;
            arrayOfStreamResources[2] = VoiceStream;
            arrayOfStreamResources[3] = AlarmStream;
            arrayOfStreamResources[4] = MediaStream;
            arrayOfStreamResources[5] = NotificationStream;
            arrayOfStreamResources[6] = MasterStream;
            arrayOfStreamResources[7] = RemoteStream;
        }

        private StreamResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
        {
            this.streamType = paramInt1;
            this.descRes = paramInt2;
            this.iconRes = paramInt3;
            this.iconMuteRes = paramInt4;
            this.show = paramBoolean;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.VolumePanel
 * JD-Core Version:        0.6.2
 */