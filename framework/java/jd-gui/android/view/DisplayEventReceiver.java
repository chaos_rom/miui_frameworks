package android.view;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import dalvik.system.CloseGuard;

public abstract class DisplayEventReceiver
{
    private static final String TAG = "DisplayEventReceiver";
    private final CloseGuard mCloseGuard = CloseGuard.get();
    private MessageQueue mMessageQueue;
    private int mReceiverPtr;

    public DisplayEventReceiver(Looper paramLooper)
    {
        if (paramLooper == null)
            throw new IllegalArgumentException("looper must not be null");
        this.mMessageQueue = paramLooper.getQueue();
        this.mReceiverPtr = nativeInit(this, this.mMessageQueue);
        this.mCloseGuard.open("dispose");
    }

    private void dispatchVsync(long paramLong, int paramInt)
    {
        onVsync(paramLong, paramInt);
    }

    private static native void nativeDispose(int paramInt);

    private static native int nativeInit(DisplayEventReceiver paramDisplayEventReceiver, MessageQueue paramMessageQueue);

    private static native void nativeScheduleVsync(int paramInt);

    public void dispose()
    {
        if (this.mCloseGuard != null)
            this.mCloseGuard.close();
        if (this.mReceiverPtr != 0)
        {
            nativeDispose(this.mReceiverPtr);
            this.mReceiverPtr = 0;
        }
        this.mMessageQueue = null;
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            dispose();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public void onVsync(long paramLong, int paramInt)
    {
    }

    public void scheduleVsync()
    {
        if (this.mReceiverPtr == 0)
            Log.w("DisplayEventReceiver", "Attempted to schedule a vertical sync pulse but the display event receiver has already been disposed.");
        while (true)
        {
            return;
            nativeScheduleVsync(this.mReceiverPtr);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.DisplayEventReceiver
 * JD-Core Version:        0.6.2
 */