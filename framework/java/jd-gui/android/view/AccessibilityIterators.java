package android.view;

import android.content.ComponentCallbacks;
import android.content.res.Configuration;
import java.text.BreakIterator;
import java.util.Locale;

public final class AccessibilityIterators
{
    static class ParagraphTextSegmentIterator extends AccessibilityIterators.AbstractTextSegmentIterator
    {
        private static ParagraphTextSegmentIterator sInstance;

        public static ParagraphTextSegmentIterator getInstance()
        {
            if (sInstance == null)
                sInstance = new ParagraphTextSegmentIterator();
            return sInstance;
        }

        private boolean isEndBoundary(int paramInt)
        {
            if ((paramInt > 0) && (this.mText.charAt(paramInt - 1) != '\n') && ((paramInt == this.mText.length()) || (this.mText.charAt(paramInt) == '\n')));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean isStartBoundary(int paramInt)
        {
            if ((this.mText.charAt(paramInt) != '\n') && ((paramInt == 0) || (this.mText.charAt(paramInt - 1) == '\n')));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public int[] following(int paramInt)
        {
            int[] arrayOfInt = null;
            int i = this.mText.length();
            if (i <= 0);
            while (true)
            {
                return arrayOfInt;
                if (paramInt < i)
                {
                    int j = paramInt;
                    if (j < 0);
                    for (j = 0; (j < i) && (this.mText.charAt(j) == '\n') && (!isStartBoundary(j)); j++);
                    if (j < i)
                    {
                        for (int k = j + 1; (k < i) && (!isEndBoundary(k)); k++);
                        arrayOfInt = getRange(j, k);
                    }
                }
            }
        }

        public int[] preceding(int paramInt)
        {
            int[] arrayOfInt = null;
            int i = this.mText.length();
            if (i <= 0);
            while (true)
            {
                return arrayOfInt;
                if (paramInt > 0)
                {
                    int j = paramInt;
                    if (j > i);
                    for (j = i; (j > 0) && (this.mText.charAt(j - 1) == '\n') && (!isEndBoundary(j)); j--);
                    if (j > 0)
                    {
                        for (int k = j - 1; (k > 0) && (!isStartBoundary(k)); k--);
                        arrayOfInt = getRange(k, j);
                    }
                }
            }
        }
    }

    static class WordTextSegmentIterator extends AccessibilityIterators.CharacterTextSegmentIterator
    {
        private static WordTextSegmentIterator sInstance;

        private WordTextSegmentIterator(Locale paramLocale)
        {
            super(null);
        }

        public static WordTextSegmentIterator getInstance(Locale paramLocale)
        {
            if (sInstance == null)
                sInstance = new WordTextSegmentIterator(paramLocale);
            return sInstance;
        }

        private boolean isEndBoundary(int paramInt)
        {
            if ((paramInt > 0) && (isLetterOrDigit(paramInt - 1)) && ((paramInt == this.mText.length()) || (!isLetterOrDigit(paramInt))));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean isLetterOrDigit(int paramInt)
        {
            if ((paramInt >= 0) && (paramInt < this.mText.length()));
            for (boolean bool = Character.isLetterOrDigit(this.mText.codePointAt(paramInt)); ; bool = false)
                return bool;
        }

        private boolean isStartBoundary(int paramInt)
        {
            if ((isLetterOrDigit(paramInt)) && ((paramInt == 0) || (!isLetterOrDigit(paramInt - 1))));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public int[] following(int paramInt)
        {
            int[] arrayOfInt = null;
            if (this.mText.length() <= 0);
            while (true)
            {
                return arrayOfInt;
                if (paramInt < this.mText.length())
                {
                    int i = paramInt;
                    if (i < 0)
                        i = 0;
                    while (true)
                        if ((!isLetterOrDigit(i)) && (!isStartBoundary(i)))
                        {
                            i = this.mImpl.following(i);
                            if (i == -1)
                                break;
                        }
                    int j = this.mImpl.following(i);
                    if ((j != -1) && (isEndBoundary(j)))
                        arrayOfInt = getRange(i, j);
                }
            }
        }

        protected void onLocaleChanged(Locale paramLocale)
        {
            this.mImpl = BreakIterator.getWordInstance(paramLocale);
        }

        public int[] preceding(int paramInt)
        {
            int[] arrayOfInt = null;
            int i = this.mText.length();
            if (i <= 0);
            while (true)
            {
                return arrayOfInt;
                if (paramInt > 0)
                {
                    int j = paramInt;
                    if (j > i)
                        j = i;
                    while (true)
                        if ((j > 0) && (!isLetterOrDigit(j - 1)) && (!isEndBoundary(j)))
                        {
                            j = this.mImpl.preceding(j);
                            if (j == -1)
                                break;
                        }
                    int k = this.mImpl.preceding(j);
                    if ((k != -1) && (isStartBoundary(k)))
                        arrayOfInt = getRange(k, j);
                }
            }
        }
    }

    static class CharacterTextSegmentIterator extends AccessibilityIterators.AbstractTextSegmentIterator
        implements ComponentCallbacks
    {
        private static CharacterTextSegmentIterator sInstance;
        protected BreakIterator mImpl;
        private Locale mLocale;

        private CharacterTextSegmentIterator(Locale paramLocale)
        {
            this.mLocale = paramLocale;
            onLocaleChanged(paramLocale);
            ViewRootImpl.addConfigCallback(this);
        }

        public static CharacterTextSegmentIterator getInstance(Locale paramLocale)
        {
            if (sInstance == null)
                sInstance = new CharacterTextSegmentIterator(paramLocale);
            return sInstance;
        }

        public int[] following(int paramInt)
        {
            int[] arrayOfInt = null;
            int i = this.mText.length();
            if (i <= 0);
            while (true)
            {
                return arrayOfInt;
                if (paramInt < i)
                {
                    int j = paramInt;
                    if (j < 0)
                        j = 0;
                    while (true)
                        if (!this.mImpl.isBoundary(j))
                        {
                            j = this.mImpl.following(j);
                            if (j == -1)
                                break;
                        }
                    int k = this.mImpl.following(j);
                    if (k != -1)
                        arrayOfInt = getRange(j, k);
                }
            }
        }

        public void initialize(String paramString)
        {
            super.initialize(paramString);
            this.mImpl.setText(paramString);
        }

        public void onConfigurationChanged(Configuration paramConfiguration)
        {
            Locale localLocale = paramConfiguration.locale;
            if (!this.mLocale.equals(localLocale))
            {
                this.mLocale = localLocale;
                onLocaleChanged(localLocale);
            }
        }

        protected void onLocaleChanged(Locale paramLocale)
        {
            this.mImpl = BreakIterator.getCharacterInstance(paramLocale);
        }

        public void onLowMemory()
        {
        }

        public int[] preceding(int paramInt)
        {
            int[] arrayOfInt = null;
            int i = this.mText.length();
            if (i <= 0);
            while (true)
            {
                return arrayOfInt;
                if (paramInt > 0)
                {
                    int j = paramInt;
                    if (j > i)
                        j = i;
                    while (true)
                        if (!this.mImpl.isBoundary(j))
                        {
                            j = this.mImpl.preceding(j);
                            if (j == -1)
                                break;
                        }
                    int k = this.mImpl.preceding(j);
                    if (k != -1)
                        arrayOfInt = getRange(k, j);
                }
            }
        }
    }

    public static abstract class AbstractTextSegmentIterator
        implements AccessibilityIterators.TextSegmentIterator
    {
        private final int[] mSegment = new int[2];
        protected String mText;

        protected int[] getRange(int paramInt1, int paramInt2)
        {
            if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 == paramInt2));
            for (int[] arrayOfInt = null; ; arrayOfInt = this.mSegment)
            {
                return arrayOfInt;
                this.mSegment[0] = paramInt1;
                this.mSegment[1] = paramInt2;
            }
        }

        public void initialize(String paramString)
        {
            this.mText = paramString;
        }
    }

    public static abstract interface TextSegmentIterator
    {
        public abstract int[] following(int paramInt);

        public abstract int[] preceding(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.AccessibilityIterators
 * JD-Core Version:        0.6.2
 */