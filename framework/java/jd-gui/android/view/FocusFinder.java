package android.view;

import android.graphics.Rect;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FocusFinder
{
    private static final ThreadLocal<FocusFinder> tlFocusFinder = new ThreadLocal()
    {
        protected FocusFinder initialValue()
        {
            return new FocusFinder(null);
        }
    };
    final Rect mBestCandidateRect = new Rect();
    final Rect mFocusedRect = new Rect();
    final Rect mOtherRect = new Rect();
    final SequentialFocusComparator mSequentialFocusComparator = new SequentialFocusComparator(null);
    private final ArrayList<View> mTempList = new ArrayList();

    private View findNextAccessibilityFocus(ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt)
    {
        ArrayList localArrayList = this.mTempList;
        try
        {
            localArrayList.clear();
            paramViewGroup.addFocusables(localArrayList, paramInt, 2);
            View localView = findNextFocus(paramViewGroup, paramView, paramRect, paramInt, localArrayList);
            return localView;
        }
        finally
        {
            localArrayList.clear();
        }
    }

    private View findNextFocus(ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt)
    {
        if ((paramInt & 0x1000) != 4096);
        for (View localView = findNextInputFocus(paramViewGroup, paramView, paramRect, paramInt); ; localView = findNextAccessibilityFocus(paramViewGroup, paramView, paramRect, paramInt))
            return localView;
    }

    private View findNextFocus(ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt, ArrayList<View> paramArrayList)
    {
        int i = paramInt & 0xFFFFEFFF;
        if (paramView != null)
        {
            if (paramRect == null)
                paramRect = this.mFocusedRect;
            paramView.getFocusedRect(paramRect);
            paramViewGroup.offsetDescendantRectToMyCoords(paramView, paramRect);
        }
        while (true)
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unknown direction: " + i);
                if (paramRect == null)
                {
                    paramRect = this.mFocusedRect;
                    switch (i)
                    {
                    default:
                        break;
                    case 1:
                        if (paramViewGroup.isLayoutRtl())
                            setFocusTopLeft(paramViewGroup, paramRect);
                        break;
                    case 66:
                    case 130:
                        setFocusTopLeft(paramViewGroup, paramRect);
                        break;
                    case 2:
                        if (paramViewGroup.isLayoutRtl())
                            setFocusBottomRight(paramViewGroup, paramRect);
                        else
                            setFocusTopLeft(paramViewGroup, paramRect);
                        break;
                    case 17:
                    case 33:
                        setFocusBottomRight(paramViewGroup, paramRect);
                        continue;
                        setFocusBottomRight(paramViewGroup, paramRect);
                    }
                }
                break;
            case 1:
            case 2:
            case 17:
            case 33:
            case 66:
            case 130:
            }
        for (View localView = findNextInputFocusInRelativeDirection(paramArrayList, paramViewGroup, paramView, paramRect, i); ; localView = findNextInputFocusInAbsoluteDirection(paramArrayList, paramViewGroup, paramView, paramRect, i))
            return localView;
    }

    private View findNextInputFocus(ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt)
    {
        Object localObject1 = null;
        if (paramView != null)
            localObject1 = findNextUserSpecifiedInputFocus(paramViewGroup, paramView, paramInt);
        Object localObject3;
        if (localObject1 != null)
            localObject3 = localObject1;
        while (true)
        {
            return localObject3;
            ArrayList localArrayList = this.mTempList;
            try
            {
                localArrayList.clear();
                paramViewGroup.addFocusables(localArrayList, paramInt);
                if (!localArrayList.isEmpty())
                {
                    View localView = findNextFocus(paramViewGroup, paramView, paramRect, paramInt, localArrayList);
                    localObject1 = localView;
                }
                localArrayList.clear();
                localObject3 = localObject1;
            }
            finally
            {
                localArrayList.clear();
            }
        }
    }

    private View findNextInputFocusInRelativeDirection(ArrayList<View> paramArrayList, ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt)
    {
        while (true)
        {
            int i;
            try
            {
                this.mSequentialFocusComparator.setRoot(paramViewGroup);
                Collections.sort(paramArrayList, this.mSequentialFocusComparator);
                this.mSequentialFocusComparator.recycle();
                i = paramArrayList.size();
                switch (paramInt)
                {
                default:
                    localView = (View)paramArrayList.get(i - 1);
                    return localView;
                case 2:
                case 1:
                }
            }
            finally
            {
                this.mSequentialFocusComparator.recycle();
            }
            View localView = getForwardFocusable(paramViewGroup, paramView, paramArrayList, i);
            continue;
            localView = getBackwardFocusable(paramViewGroup, paramView, paramArrayList, i);
        }
    }

    private View findNextUserSpecifiedInputFocus(ViewGroup paramViewGroup, View paramView, int paramInt)
    {
        View localView = paramView.findUserSetNextFocus(paramViewGroup, paramInt);
        if ((localView != null) && (localView.isFocusable()) && ((!localView.isInTouchMode()) || (localView.isFocusableInTouchMode())));
        while (true)
        {
            return localView;
            localView = null;
        }
    }

    private static View getBackwardFocusable(ViewGroup paramViewGroup, View paramView, ArrayList<View> paramArrayList, int paramInt)
    {
        if (paramViewGroup.isLayoutRtl());
        for (View localView = getNextFocusable(paramView, paramArrayList, paramInt); ; localView = getPreviousFocusable(paramView, paramArrayList, paramInt))
            return localView;
    }

    private static View getForwardFocusable(ViewGroup paramViewGroup, View paramView, ArrayList<View> paramArrayList, int paramInt)
    {
        if (paramViewGroup.isLayoutRtl());
        for (View localView = getPreviousFocusable(paramView, paramArrayList, paramInt); ; localView = getNextFocusable(paramView, paramArrayList, paramInt))
            return localView;
    }

    public static FocusFinder getInstance()
    {
        return (FocusFinder)tlFocusFinder.get();
    }

    private static View getNextFocusable(View paramView, ArrayList<View> paramArrayList, int paramInt)
    {
        View localView;
        if (paramView != null)
        {
            int i = paramArrayList.lastIndexOf(paramView);
            if ((i >= 0) && (i + 1 < paramInt))
                localView = (View)paramArrayList.get(i + 1);
        }
        while (true)
        {
            return localView;
            if (!paramArrayList.isEmpty())
                localView = (View)paramArrayList.get(0);
            else
                localView = null;
        }
    }

    private static View getPreviousFocusable(View paramView, ArrayList<View> paramArrayList, int paramInt)
    {
        View localView;
        if (paramView != null)
        {
            int i = paramArrayList.indexOf(paramView);
            if (i > 0)
                localView = (View)paramArrayList.get(i - 1);
        }
        while (true)
        {
            return localView;
            if (!paramArrayList.isEmpty())
                localView = (View)paramArrayList.get(paramInt - 1);
            else
                localView = null;
        }
    }

    private boolean isTouchCandidate(int paramInt1, int paramInt2, Rect paramRect, int paramInt3)
    {
        boolean bool = true;
        switch (paramInt3)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
            if ((paramRect.left > paramInt1) || (paramRect.top > paramInt2) || (paramInt2 > paramRect.bottom))
                break;
        case 66:
        case 33:
        case 130:
        }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if ((paramRect.left < paramInt1) || (paramRect.top > paramInt2) || (paramInt2 > paramRect.bottom))
            {
                bool = false;
                continue;
                if ((paramRect.top > paramInt2) || (paramRect.left > paramInt1) || (paramInt1 > paramRect.right))
                {
                    bool = false;
                    continue;
                    if ((paramRect.top < paramInt2) || (paramRect.left > paramInt1) || (paramInt1 > paramRect.right))
                        bool = false;
                }
            }
        }
    }

    static int majorAxisDistance(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        return Math.max(0, majorAxisDistanceRaw(paramInt, paramRect1, paramRect2));
    }

    static int majorAxisDistanceRaw(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        int i;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
            i = paramRect1.left - paramRect2.right;
        case 66:
        case 33:
        case 130:
        }
        while (true)
        {
            return i;
            i = paramRect2.left - paramRect1.right;
            continue;
            i = paramRect1.top - paramRect2.bottom;
            continue;
            i = paramRect2.top - paramRect1.bottom;
        }
    }

    static int majorAxisDistanceToFarEdge(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        return Math.max(1, majorAxisDistanceToFarEdgeRaw(paramInt, paramRect1, paramRect2));
    }

    static int majorAxisDistanceToFarEdgeRaw(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        int i;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
            i = paramRect1.left - paramRect2.left;
        case 66:
        case 33:
        case 130:
        }
        while (true)
        {
            return i;
            i = paramRect2.right - paramRect1.right;
            continue;
            i = paramRect1.top - paramRect2.top;
            continue;
            i = paramRect2.bottom - paramRect1.bottom;
        }
    }

    static int minorAxisDistance(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
        case 66:
        case 33:
        case 130:
        }
        for (int i = Math.abs(paramRect1.top + paramRect1.height() / 2 - (paramRect2.top + paramRect2.height() / 2)); ; i = Math.abs(paramRect1.left + paramRect1.width() / 2 - (paramRect2.left + paramRect2.width() / 2)))
            return i;
    }

    private void setFocusBottomRight(ViewGroup paramViewGroup, Rect paramRect)
    {
        int i = paramViewGroup.getScrollY() + paramViewGroup.getHeight();
        int j = paramViewGroup.getScrollX() + paramViewGroup.getWidth();
        paramRect.set(j, i, j, i);
    }

    private void setFocusTopLeft(ViewGroup paramViewGroup, Rect paramRect)
    {
        int i = paramViewGroup.getScrollY();
        int j = paramViewGroup.getScrollX();
        paramRect.set(j, i, j, i);
    }

    boolean beamBeats(int paramInt, Rect paramRect1, Rect paramRect2, Rect paramRect3)
    {
        boolean bool1 = true;
        boolean bool2 = beamsOverlap(paramInt, paramRect1, paramRect2);
        if ((beamsOverlap(paramInt, paramRect1, paramRect3)) || (!bool2));
        for (bool1 = false; ; bool1 = false)
            do
                return bool1;
            while ((!isToDirectionOf(paramInt, paramRect1, paramRect3)) || (paramInt == 17) || (paramInt == 66) || (majorAxisDistance(paramInt, paramRect1, paramRect2) < majorAxisDistanceToFarEdge(paramInt, paramRect1, paramRect3)));
    }

    boolean beamsOverlap(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        boolean bool = true;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
        case 66:
            if ((paramRect2.bottom < paramRect1.top) || (paramRect2.top > paramRect1.bottom))
                break;
        case 33:
        case 130:
        }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if ((paramRect2.right < paramRect1.left) || (paramRect2.left > paramRect1.right))
                bool = false;
        }
    }

    public View findNearestTouchable(ViewGroup paramViewGroup, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt)
    {
        ArrayList localArrayList = paramViewGroup.getTouchables();
        int i = 2147483647;
        Object localObject = null;
        int j = localArrayList.size();
        int k = ViewConfiguration.get(paramViewGroup.mContext).getScaledEdgeSlop();
        Rect localRect1 = new Rect();
        Rect localRect2 = this.mOtherRect;
        int m = 0;
        if (m < j)
        {
            View localView = (View)localArrayList.get(m);
            localView.getDrawingRect(localRect2);
            paramViewGroup.offsetRectBetweenParentAndChild(localView, localRect2, true, true);
            if (!isTouchCandidate(paramInt1, paramInt2, localRect2, paramInt3));
            while (true)
            {
                m++;
                break;
                int n = 2147483647;
                switch (paramInt3)
                {
                default:
                case 17:
                case 66:
                case 33:
                case 130:
                }
                while ((n < k) && ((localObject == null) || (localRect1.contains(localRect2)) || ((!localRect2.contains(localRect1)) && (n < i))))
                {
                    i = n;
                    localObject = localView;
                    localRect1.set(localRect2);
                    switch (paramInt3)
                    {
                    default:
                        break;
                    case 17:
                        paramArrayOfInt[0] = (-n);
                        break;
                        n = 1 + (paramInt1 - localRect2.right);
                        continue;
                        n = localRect2.left;
                        continue;
                        n = 1 + (paramInt2 - localRect2.bottom);
                        continue;
                        n = localRect2.top;
                    case 66:
                    case 33:
                    case 130:
                    }
                }
                paramArrayOfInt[0] = n;
                continue;
                paramArrayOfInt[1] = (-n);
                continue;
                paramArrayOfInt[1] = n;
            }
        }
        return localObject;
    }

    public final View findNextFocus(ViewGroup paramViewGroup, View paramView, int paramInt)
    {
        return findNextFocus(paramViewGroup, paramView, null, paramInt);
    }

    public View findNextFocusFromRect(ViewGroup paramViewGroup, Rect paramRect, int paramInt)
    {
        this.mFocusedRect.set(paramRect);
        return findNextFocus(paramViewGroup, null, this.mFocusedRect, paramInt);
    }

    View findNextInputFocusInAbsoluteDirection(ArrayList<View> paramArrayList, ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt)
    {
        this.mBestCandidateRect.set(paramRect);
        Object localObject;
        int j;
        label64: View localView;
        switch (paramInt)
        {
        default:
            localObject = null;
            int i = paramArrayList.size();
            j = 0;
            if (j >= i)
                break label230;
            localView = (View)paramArrayList.get(j);
            if ((localView != paramView) && (localView != paramViewGroup))
                break;
        case 17:
        case 66:
        case 33:
        case 130:
        }
        while (true)
        {
            j++;
            break label64;
            this.mBestCandidateRect.offset(1 + paramRect.width(), 0);
            break;
            this.mBestCandidateRect.offset(-(1 + paramRect.width()), 0);
            break;
            this.mBestCandidateRect.offset(0, 1 + paramRect.height());
            break;
            this.mBestCandidateRect.offset(0, -(1 + paramRect.height()));
            break;
            localView.getDrawingRect(this.mOtherRect);
            paramViewGroup.offsetDescendantRectToMyCoords(localView, this.mOtherRect);
            if (isBetterCandidate(paramInt, paramRect, this.mOtherRect, this.mBestCandidateRect))
            {
                this.mBestCandidateRect.set(this.mOtherRect);
                localObject = localView;
            }
        }
        label230: return localObject;
    }

    int getWeightedDistanceFor(int paramInt1, int paramInt2)
    {
        return paramInt1 * (paramInt1 * 13) + paramInt2 * paramInt2;
    }

    boolean isBetterCandidate(int paramInt, Rect paramRect1, Rect paramRect2, Rect paramRect3)
    {
        boolean bool = true;
        if (!isCandidate(paramRect1, paramRect2, paramInt))
            bool = false;
        while (true)
        {
            return bool;
            if ((isCandidate(paramRect1, paramRect3, paramInt)) && (!beamBeats(paramInt, paramRect1, paramRect2, paramRect3)))
                if (beamBeats(paramInt, paramRect1, paramRect3, paramRect2))
                    bool = false;
                else if (getWeightedDistanceFor(majorAxisDistance(paramInt, paramRect1, paramRect2), minorAxisDistance(paramInt, paramRect1, paramRect2)) >= getWeightedDistanceFor(majorAxisDistance(paramInt, paramRect1, paramRect3), minorAxisDistance(paramInt, paramRect1, paramRect3)))
                    bool = false;
        }
    }

    boolean isCandidate(Rect paramRect1, Rect paramRect2, int paramInt)
    {
        boolean bool = true;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
            if (((paramRect1.right <= paramRect2.right) && (paramRect1.left < paramRect2.right)) || (paramRect1.left <= paramRect2.left))
                break;
        case 66:
        case 33:
        case 130:
        }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if (((paramRect1.left >= paramRect2.left) && (paramRect1.right > paramRect2.left)) || (paramRect1.right >= paramRect2.right))
            {
                bool = false;
                continue;
                if (((paramRect1.bottom <= paramRect2.bottom) && (paramRect1.top < paramRect2.bottom)) || (paramRect1.top <= paramRect2.top))
                {
                    bool = false;
                    continue;
                    if (((paramRect1.top >= paramRect2.top) && (paramRect1.bottom > paramRect2.top)) || (paramRect1.bottom >= paramRect2.bottom))
                        bool = false;
                }
            }
        }
    }

    boolean isToDirectionOf(int paramInt, Rect paramRect1, Rect paramRect2)
    {
        boolean bool = true;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        case 17:
            if (paramRect1.left < paramRect2.right)
                break;
        case 66:
        case 33:
        case 130:
        }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if (paramRect1.right > paramRect2.left)
            {
                bool = false;
                continue;
                if (paramRect1.top < paramRect2.bottom)
                {
                    bool = false;
                    continue;
                    if (paramRect1.bottom > paramRect2.top)
                        bool = false;
                }
            }
        }
    }

    private static final class SequentialFocusComparator
        implements Comparator<View>
    {
        private final Rect mFirstRect = new Rect();
        private ViewGroup mRoot;
        private final Rect mSecondRect = new Rect();

        private void getRect(View paramView, Rect paramRect)
        {
            paramView.getDrawingRect(paramRect);
            this.mRoot.offsetDescendantRectToMyCoords(paramView, paramRect);
        }

        public int compare(View paramView1, View paramView2)
        {
            int i = 0;
            if (paramView1 == paramView2);
            while (true)
            {
                return i;
                getRect(paramView1, this.mFirstRect);
                getRect(paramView2, this.mSecondRect);
                if (this.mFirstRect.top < this.mSecondRect.top)
                    i = -1;
                else if (this.mFirstRect.top > this.mSecondRect.top)
                    i = 1;
                else if (this.mFirstRect.left < this.mSecondRect.left)
                    i = -1;
                else if (this.mFirstRect.left > this.mSecondRect.left)
                    i = 1;
                else if (this.mFirstRect.bottom < this.mSecondRect.bottom)
                    i = -1;
                else if (this.mFirstRect.bottom > this.mSecondRect.bottom)
                    i = 1;
                else if (this.mFirstRect.right < this.mSecondRect.right)
                    i = -1;
                else if (this.mFirstRect.right > this.mSecondRect.right)
                    i = 1;
            }
        }

        public void recycle()
        {
            this.mRoot = null;
        }

        public void setRoot(ViewGroup paramViewGroup)
        {
            this.mRoot = paramViewGroup;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.FocusFinder
 * JD-Core Version:        0.6.2
 */