package android.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

abstract class HardwareLayer
{
    static final int DIMENSION_UNDEFINED = -1;
    DisplayList mDisplayList;
    int mHeight;
    boolean mOpaque;
    int mWidth;

    HardwareLayer()
    {
        this(-1, -1, false);
    }

    HardwareLayer(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        this.mWidth = paramInt1;
        this.mHeight = paramInt2;
        this.mOpaque = paramBoolean;
    }

    abstract boolean copyInto(Bitmap paramBitmap);

    abstract void destroy();

    abstract void end(Canvas paramCanvas);

    abstract void flush();

    abstract HardwareCanvas getCanvas();

    DisplayList getDisplayList()
    {
        return this.mDisplayList;
    }

    int getHeight()
    {
        return this.mHeight;
    }

    int getWidth()
    {
        return this.mWidth;
    }

    boolean isOpaque()
    {
        return this.mOpaque;
    }

    abstract boolean isValid();

    abstract void redraw(DisplayList paramDisplayList, Rect paramRect);

    abstract void resize(int paramInt1, int paramInt2);

    void setDisplayList(DisplayList paramDisplayList)
    {
        this.mDisplayList = paramDisplayList;
    }

    abstract void setTransform(Matrix paramMatrix);

    abstract HardwareCanvas start(Canvas paramCanvas);

    void update(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        this.mWidth = paramInt1;
        this.mHeight = paramInt2;
        this.mOpaque = paramBoolean;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.HardwareLayer
 * JD-Core Version:        0.6.2
 */