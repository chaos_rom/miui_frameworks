package android.view;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;

public abstract interface WindowManager extends ViewManager
{
    public abstract Display getDefaultDisplay();

    public abstract boolean isHardwareAccelerated();

    public abstract void removeViewImmediate(View paramView);

    public static class LayoutParams extends ViewGroup.LayoutParams
        implements Parcelable
    {
        public static final int ALPHA_CHANGED = 128;
        public static final int ANIMATION_CHANGED = 16;
        public static final float BRIGHTNESS_OVERRIDE_FULL = 1.0F;
        public static final float BRIGHTNESS_OVERRIDE_NONE = -1.0F;
        public static final float BRIGHTNESS_OVERRIDE_OFF = 0.0F;
        public static final int BUTTON_BRIGHTNESS_CHANGED = 4096;
        public static final Parcelable.Creator<LayoutParams> CREATOR = new Parcelable.Creator()
        {
            public WindowManager.LayoutParams createFromParcel(Parcel paramAnonymousParcel)
            {
                return new WindowManager.LayoutParams(paramAnonymousParcel);
            }

            public WindowManager.LayoutParams[] newArray(int paramAnonymousInt)
            {
                return new WindowManager.LayoutParams[paramAnonymousInt];
            }
        };
        public static final int DIM_AMOUNT_CHANGED = 32;
        public static final int EVERYTHING_CHANGED = -1;
        public static final int FIRST_APPLICATION_WINDOW = 1;
        public static final int FIRST_SUB_WINDOW = 1000;
        public static final int FIRST_SYSTEM_WINDOW = 2000;
        public static final int FLAGS_CHANGED = 4;
        public static final int FLAG_ALLOW_LOCK_WHILE_SCREEN_ON = 1;
        public static final int FLAG_ALT_FOCUSABLE_IM = 131072;

        @Deprecated
        public static final int FLAG_BLUR_BEHIND = 4;
        public static final int FLAG_COMPATIBLE_WINDOW = 536870912;
        public static final int FLAG_DIM_BEHIND = 2;
        public static final int FLAG_DISMISS_KEYGUARD = 4194304;
        public static final int FLAG_DITHER = 4096;
        public static final int FLAG_FORCE_NOT_FULLSCREEN = 2048;
        public static final int FLAG_FULLSCREEN = 1024;
        public static final int FLAG_HARDWARE_ACCELERATED = 16777216;
        public static final int FLAG_IGNORE_CHEEK_PRESSES = 32768;
        public static final int FLAG_KEEP_SCREEN_ON = 128;
        public static final int FLAG_LAYOUT_INSET_DECOR = 65536;
        public static final int FLAG_LAYOUT_IN_SCREEN = 256;
        public static final int FLAG_LAYOUT_NO_LIMITS = 512;
        public static final int FLAG_NEEDS_MENU_KEY = 134217728;
        public static final int FLAG_NOT_FOCUSABLE = 8;
        public static final int FLAG_NOT_TOUCHABLE = 16;
        public static final int FLAG_NOT_TOUCH_MODAL = 32;
        public static final int FLAG_SCALED = 16384;
        public static final int FLAG_SECURE = 8192;
        public static final int FLAG_SHOW_WALLPAPER = 1048576;
        public static final int FLAG_SHOW_WHEN_LOCKED = 524288;
        public static final int FLAG_SLIPPERY = 67108864;
        public static final int FLAG_SPLIT_TOUCH = 8388608;
        public static final int FLAG_SYSTEM_ERROR = 1073741824;
        public static final int FLAG_TOUCHABLE_WHEN_WAKING = 64;
        public static final int FLAG_TURN_SCREEN_ON = 2097152;
        public static final int FLAG_WATCH_OUTSIDE_TOUCH = 262144;
        public static final int FORMAT_CHANGED = 8;
        public static final int INPUT_FEATURES_CHANGED = 32768;
        public static final int INPUT_FEATURE_DISABLE_POINTER_GESTURES = 1;
        public static final int INPUT_FEATURE_NO_INPUT_CHANNEL = 2;
        public static final int LAST_APPLICATION_WINDOW = 99;
        public static final int LAST_SUB_WINDOW = 1999;
        public static final int LAST_SYSTEM_WINDOW = 2999;
        public static final int LAYOUT_CHANGED = 1;
        public static final int MEMORY_TYPE_CHANGED = 256;

        @Deprecated
        public static final int MEMORY_TYPE_GPU = 2;

        @Deprecated
        public static final int MEMORY_TYPE_HARDWARE = 1;

        @Deprecated
        public static final int MEMORY_TYPE_NORMAL = 0;

        @Deprecated
        public static final int MEMORY_TYPE_PUSH_BUFFERS = 3;
        public static final int PRIVATE_FLAGS_CHANGED = 65536;
        public static final int PRIVATE_FLAG_FAKE_HARDWARE_ACCELERATED = 1;
        public static final int PRIVATE_FLAG_FORCE_HARDWARE_ACCELERATED = 2;
        public static final int PRIVATE_FLAG_SET_NEEDS_MENU_KEY = 8;
        public static final int PRIVATE_FLAG_WANTS_OFFSET_NOTIFICATIONS = 4;
        public static final int SCREEN_BRIGHTNESS_CHANGED = 2048;
        public static final int SCREEN_ORIENTATION_CHANGED = 1024;
        public static final int SOFT_INPUT_ADJUST_NOTHING = 48;
        public static final int SOFT_INPUT_ADJUST_PAN = 32;
        public static final int SOFT_INPUT_ADJUST_RESIZE = 16;
        public static final int SOFT_INPUT_ADJUST_UNSPECIFIED = 0;
        public static final int SOFT_INPUT_IS_FORWARD_NAVIGATION = 256;
        public static final int SOFT_INPUT_MASK_ADJUST = 240;
        public static final int SOFT_INPUT_MASK_STATE = 15;
        public static final int SOFT_INPUT_MODE_CHANGED = 512;
        public static final int SOFT_INPUT_STATE_ALWAYS_HIDDEN = 3;
        public static final int SOFT_INPUT_STATE_ALWAYS_VISIBLE = 5;
        public static final int SOFT_INPUT_STATE_HIDDEN = 2;
        public static final int SOFT_INPUT_STATE_UNCHANGED = 1;
        public static final int SOFT_INPUT_STATE_UNSPECIFIED = 0;
        public static final int SOFT_INPUT_STATE_VISIBLE = 4;
        public static final int SYSTEM_UI_LISTENER_CHANGED = 16384;
        public static final int SYSTEM_UI_VISIBILITY_CHANGED = 8192;
        public static final int TITLE_CHANGED = 64;
        public static final int TYPE_APPLICATION = 2;
        public static final int TYPE_APPLICATION_ATTACHED_DIALOG = 1003;
        public static final int TYPE_APPLICATION_MEDIA = 1001;
        public static final int TYPE_APPLICATION_MEDIA_OVERLAY = 1004;
        public static final int TYPE_APPLICATION_PANEL = 1000;
        public static final int TYPE_APPLICATION_STARTING = 3;
        public static final int TYPE_APPLICATION_SUB_PANEL = 1002;
        public static final int TYPE_BASE_APPLICATION = 1;
        public static final int TYPE_BOOT_PROGRESS = 2021;
        public static final int TYPE_CHANGED = 2;
        public static final int TYPE_DRAG = 2016;
        public static final int TYPE_DREAM = 2023;
        public static final int TYPE_HIDDEN_NAV_CONSUMER = 2022;
        public static final int TYPE_INPUT_METHOD = 2011;
        public static final int TYPE_INPUT_METHOD_DIALOG = 2012;
        public static final int TYPE_KEYGUARD = 2004;
        public static final int TYPE_KEYGUARD_DIALOG = 2009;
        public static final int TYPE_NAVIGATION_BAR = 2019;
        public static final int TYPE_NAVIGATION_BAR_PANEL = 2024;
        public static final int TYPE_PHONE = 2002;
        public static final int TYPE_POINTER = 2018;
        public static final int TYPE_PRIORITY_PHONE = 2007;
        public static final int TYPE_SEARCH_BAR = 2001;
        public static final int TYPE_SECURE_SYSTEM_OVERLAY = 2015;
        public static final int TYPE_STATUS_BAR = 2000;
        public static final int TYPE_STATUS_BAR_PANEL = 2014;
        public static final int TYPE_STATUS_BAR_SUB_PANEL = 2017;
        public static final int TYPE_SYSTEM_ALERT = 2003;
        public static final int TYPE_SYSTEM_DIALOG = 2008;
        public static final int TYPE_SYSTEM_ERROR = 2010;
        public static final int TYPE_SYSTEM_OVERLAY = 2006;
        public static final int TYPE_TOAST = 2005;
        public static final int TYPE_VOLUME_OVERLAY = 2020;
        public static final int TYPE_WALLPAPER = 2013;
        public float alpha = 1.0F;
        public float buttonBrightness = -1.0F;
        public float dimAmount = 1.0F;

        @ViewDebug.ExportedProperty(flagMapping={@ViewDebug.FlagToString(equals=1, mask=1, name="FLAG_ALLOW_LOCK_WHILE_SCREEN_ON"), @ViewDebug.FlagToString(equals=2, mask=2, name="FLAG_DIM_BEHIND"), @ViewDebug.FlagToString(equals=4, mask=4, name="FLAG_BLUR_BEHIND"), @ViewDebug.FlagToString(equals=8, mask=8, name="FLAG_NOT_FOCUSABLE"), @ViewDebug.FlagToString(equals=16, mask=16, name="FLAG_NOT_TOUCHABLE"), @ViewDebug.FlagToString(equals=32, mask=32, name="FLAG_NOT_TOUCH_MODAL"), @ViewDebug.FlagToString(equals=64, mask=64, name="FLAG_TOUCHABLE_WHEN_WAKING"), @ViewDebug.FlagToString(equals=128, mask=128, name="FLAG_KEEP_SCREEN_ON"), @ViewDebug.FlagToString(equals=256, mask=256, name="FLAG_LAYOUT_IN_SCREEN"), @ViewDebug.FlagToString(equals=512, mask=512, name="FLAG_LAYOUT_NO_LIMITS"), @ViewDebug.FlagToString(equals=1024, mask=1024, name="FLAG_FULLSCREEN"), @ViewDebug.FlagToString(equals=2048, mask=2048, name="FLAG_FORCE_NOT_FULLSCREEN"), @ViewDebug.FlagToString(equals=4096, mask=4096, name="FLAG_DITHER"), @ViewDebug.FlagToString(equals=8192, mask=8192, name="FLAG_SECURE"), @ViewDebug.FlagToString(equals=16384, mask=16384, name="FLAG_SCALED"), @ViewDebug.FlagToString(equals=32768, mask=32768, name="FLAG_IGNORE_CHEEK_PRESSES"), @ViewDebug.FlagToString(equals=65536, mask=65536, name="FLAG_LAYOUT_INSET_DECOR"), @ViewDebug.FlagToString(equals=131072, mask=131072, name="FLAG_ALT_FOCUSABLE_IM"), @ViewDebug.FlagToString(equals=262144, mask=262144, name="FLAG_WATCH_OUTSIDE_TOUCH"), @ViewDebug.FlagToString(equals=524288, mask=524288, name="FLAG_SHOW_WHEN_LOCKED"), @ViewDebug.FlagToString(equals=1048576, mask=1048576, name="FLAG_SHOW_WALLPAPER"), @ViewDebug.FlagToString(equals=2097152, mask=2097152, name="FLAG_TURN_SCREEN_ON"), @ViewDebug.FlagToString(equals=4194304, mask=4194304, name="FLAG_DISMISS_KEYGUARD"), @ViewDebug.FlagToString(equals=8388608, mask=8388608, name="FLAG_SPLIT_TOUCH"), @ViewDebug.FlagToString(equals=16777216, mask=16777216, name="FLAG_HARDWARE_ACCELERATED")})
        public int flags;
        public int format;
        public int gravity;
        public boolean hasSystemUiListeners;
        public float horizontalMargin;

        @ViewDebug.ExportedProperty
        public float horizontalWeight;
        public int inputFeatures;
        private int[] mCompatibilityParamsBackup = null;
        private CharSequence mTitle = "";

        @Deprecated
        public int memoryType;
        public String packageName = null;
        public int privateFlags;
        public float screenBrightness = -1.0F;
        public int screenOrientation = -1;
        public int softInputMode;
        public int subtreeSystemUiVisibility;
        public int systemUiVisibility;
        public IBinder token = null;

        @ViewDebug.ExportedProperty(mapping={@ViewDebug.IntToString(from=1, to="TYPE_BASE_APPLICATION"), @ViewDebug.IntToString(from=2, to="TYPE_APPLICATION"), @ViewDebug.IntToString(from=3, to="TYPE_APPLICATION_STARTING"), @ViewDebug.IntToString(from=1000, to="TYPE_APPLICATION_PANEL"), @ViewDebug.IntToString(from=1001, to="TYPE_APPLICATION_MEDIA"), @ViewDebug.IntToString(from=1002, to="TYPE_APPLICATION_SUB_PANEL"), @ViewDebug.IntToString(from=1003, to="TYPE_APPLICATION_ATTACHED_DIALOG"), @ViewDebug.IntToString(from=2000, to="TYPE_STATUS_BAR"), @ViewDebug.IntToString(from=2001, to="TYPE_SEARCH_BAR"), @ViewDebug.IntToString(from=2002, to="TYPE_PHONE"), @ViewDebug.IntToString(from=2003, to="TYPE_SYSTEM_ALERT"), @ViewDebug.IntToString(from=2004, to="TYPE_KEYGUARD"), @ViewDebug.IntToString(from=2005, to="TYPE_TOAST"), @ViewDebug.IntToString(from=2006, to="TYPE_SYSTEM_OVERLAY"), @ViewDebug.IntToString(from=2007, to="TYPE_PRIORITY_PHONE"), @ViewDebug.IntToString(from=2014, to="TYPE_STATUS_BAR_PANEL"), @ViewDebug.IntToString(from=2017, to="TYPE_STATUS_BAR_SUB_PANEL"), @ViewDebug.IntToString(from=2008, to="TYPE_SYSTEM_DIALOG"), @ViewDebug.IntToString(from=2009, to="TYPE_KEYGUARD_DIALOG"), @ViewDebug.IntToString(from=2010, to="TYPE_SYSTEM_ERROR"), @ViewDebug.IntToString(from=2011, to="TYPE_INPUT_METHOD"), @ViewDebug.IntToString(from=2012, to="TYPE_INPUT_METHOD_DIALOG"), @ViewDebug.IntToString(from=2013, to="TYPE_WALLPAPER"), @ViewDebug.IntToString(from=2014, to="TYPE_STATUS_BAR_PANEL"), @ViewDebug.IntToString(from=2015, to="TYPE_SECURE_SYSTEM_OVERLAY"), @ViewDebug.IntToString(from=2016, to="TYPE_DRAG"), @ViewDebug.IntToString(from=2017, to="TYPE_STATUS_BAR_SUB_PANEL"), @ViewDebug.IntToString(from=2018, to="TYPE_POINTER"), @ViewDebug.IntToString(from=2019, to="TYPE_NAVIGATION_BAR"), @ViewDebug.IntToString(from=2020, to="TYPE_VOLUME_OVERLAY"), @ViewDebug.IntToString(from=2021, to="TYPE_BOOT_PROGRESS")})
        public int type;
        public float verticalMargin;

        @ViewDebug.ExportedProperty
        public float verticalWeight;
        public int windowAnimations;

        @ViewDebug.ExportedProperty
        public int x;

        @ViewDebug.ExportedProperty
        public int y;

        public LayoutParams()
        {
            super(-1);
            this.type = 2;
            this.format = -1;
        }

        public LayoutParams(int paramInt)
        {
            super(-1);
            this.type = paramInt;
            this.format = -1;
        }

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(-1);
            this.type = paramInt1;
            this.flags = paramInt2;
            this.format = -1;
        }

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3)
        {
            super(-1);
            this.type = paramInt1;
            this.flags = paramInt2;
            this.format = paramInt3;
        }

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            super(paramInt2);
            this.type = paramInt3;
            this.flags = paramInt4;
            this.format = paramInt5;
        }

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
        {
            super(paramInt2);
            this.x = paramInt3;
            this.y = paramInt4;
            this.type = paramInt5;
            this.flags = paramInt6;
            this.format = paramInt7;
        }

        public LayoutParams(Parcel paramParcel)
        {
            this.width = paramParcel.readInt();
            this.height = paramParcel.readInt();
            this.x = paramParcel.readInt();
            this.y = paramParcel.readInt();
            this.type = paramParcel.readInt();
            this.flags = paramParcel.readInt();
            this.privateFlags = paramParcel.readInt();
            this.softInputMode = paramParcel.readInt();
            this.gravity = paramParcel.readInt();
            this.horizontalMargin = paramParcel.readFloat();
            this.verticalMargin = paramParcel.readFloat();
            this.format = paramParcel.readInt();
            this.windowAnimations = paramParcel.readInt();
            this.alpha = paramParcel.readFloat();
            this.dimAmount = paramParcel.readFloat();
            this.screenBrightness = paramParcel.readFloat();
            this.buttonBrightness = paramParcel.readFloat();
            this.token = paramParcel.readStrongBinder();
            this.packageName = paramParcel.readString();
            this.mTitle = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.screenOrientation = paramParcel.readInt();
            this.systemUiVisibility = paramParcel.readInt();
            this.subtreeSystemUiVisibility = paramParcel.readInt();
            if (paramParcel.readInt() != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.hasSystemUiListeners = bool;
                this.inputFeatures = paramParcel.readInt();
                return;
            }
        }

        public static boolean mayUseInputMethod(int paramInt)
        {
            switch (0x20008 & paramInt)
            {
            default:
            case 0:
            case 131080:
            }
            for (boolean bool = false; ; bool = true)
                return bool;
        }

        void backup()
        {
            int[] arrayOfInt = this.mCompatibilityParamsBackup;
            if (arrayOfInt == null)
            {
                arrayOfInt = new int[4];
                this.mCompatibilityParamsBackup = arrayOfInt;
            }
            arrayOfInt[0] = this.x;
            arrayOfInt[1] = this.y;
            arrayOfInt[2] = this.width;
            arrayOfInt[3] = this.height;
        }

        public final int copyFrom(LayoutParams paramLayoutParams)
        {
            int i = 0;
            if (this.width != paramLayoutParams.width)
            {
                this.width = paramLayoutParams.width;
                i = 0x0 | 0x1;
            }
            if (this.height != paramLayoutParams.height)
            {
                this.height = paramLayoutParams.height;
                i |= 1;
            }
            if (this.x != paramLayoutParams.x)
            {
                this.x = paramLayoutParams.x;
                i |= 1;
            }
            if (this.y != paramLayoutParams.y)
            {
                this.y = paramLayoutParams.y;
                i |= 1;
            }
            if (this.horizontalWeight != paramLayoutParams.horizontalWeight)
            {
                this.horizontalWeight = paramLayoutParams.horizontalWeight;
                i |= 1;
            }
            if (this.verticalWeight != paramLayoutParams.verticalWeight)
            {
                this.verticalWeight = paramLayoutParams.verticalWeight;
                i |= 1;
            }
            if (this.horizontalMargin != paramLayoutParams.horizontalMargin)
            {
                this.horizontalMargin = paramLayoutParams.horizontalMargin;
                i |= 1;
            }
            if (this.verticalMargin != paramLayoutParams.verticalMargin)
            {
                this.verticalMargin = paramLayoutParams.verticalMargin;
                i |= 1;
            }
            if (this.type != paramLayoutParams.type)
            {
                this.type = paramLayoutParams.type;
                i |= 2;
            }
            if (this.flags != paramLayoutParams.flags)
            {
                this.flags = paramLayoutParams.flags;
                i |= 4;
            }
            if (this.privateFlags != paramLayoutParams.privateFlags)
            {
                this.privateFlags = paramLayoutParams.privateFlags;
                i |= 65536;
            }
            if (this.softInputMode != paramLayoutParams.softInputMode)
            {
                this.softInputMode = paramLayoutParams.softInputMode;
                i |= 512;
            }
            if (this.gravity != paramLayoutParams.gravity)
            {
                this.gravity = paramLayoutParams.gravity;
                i |= 1;
            }
            if (this.format != paramLayoutParams.format)
            {
                this.format = paramLayoutParams.format;
                i |= 8;
            }
            if (this.windowAnimations != paramLayoutParams.windowAnimations)
            {
                this.windowAnimations = paramLayoutParams.windowAnimations;
                i |= 16;
            }
            if (this.token == null)
                this.token = paramLayoutParams.token;
            if (this.packageName == null)
                this.packageName = paramLayoutParams.packageName;
            if (!this.mTitle.equals(paramLayoutParams.mTitle))
            {
                this.mTitle = paramLayoutParams.mTitle;
                i |= 64;
            }
            if (this.alpha != paramLayoutParams.alpha)
            {
                this.alpha = paramLayoutParams.alpha;
                i |= 128;
            }
            if (this.dimAmount != paramLayoutParams.dimAmount)
            {
                this.dimAmount = paramLayoutParams.dimAmount;
                i |= 32;
            }
            if (this.screenBrightness != paramLayoutParams.screenBrightness)
            {
                this.screenBrightness = paramLayoutParams.screenBrightness;
                i |= 2048;
            }
            if (this.buttonBrightness != paramLayoutParams.buttonBrightness)
            {
                this.buttonBrightness = paramLayoutParams.buttonBrightness;
                i |= 4096;
            }
            if (this.screenOrientation != paramLayoutParams.screenOrientation)
            {
                this.screenOrientation = paramLayoutParams.screenOrientation;
                i |= 1024;
            }
            if ((this.systemUiVisibility != paramLayoutParams.systemUiVisibility) || (this.subtreeSystemUiVisibility != paramLayoutParams.subtreeSystemUiVisibility))
            {
                this.systemUiVisibility = paramLayoutParams.systemUiVisibility;
                this.subtreeSystemUiVisibility = paramLayoutParams.subtreeSystemUiVisibility;
                i |= 8192;
            }
            if (this.hasSystemUiListeners != paramLayoutParams.hasSystemUiListeners)
            {
                this.hasSystemUiListeners = paramLayoutParams.hasSystemUiListeners;
                i |= 16384;
            }
            if (this.inputFeatures != paramLayoutParams.inputFeatures)
            {
                this.inputFeatures = paramLayoutParams.inputFeatures;
                i |= 32768;
            }
            return i;
        }

        public String debug(String paramString)
        {
            Log.d("Debug", paramString + "Contents of " + this + ":");
            Log.d("Debug", super.debug(""));
            Log.d("Debug", "");
            Log.d("Debug", "WindowManager.LayoutParams={title=" + this.mTitle + "}");
            return "";
        }

        public int describeContents()
        {
            return 0;
        }

        public final CharSequence getTitle()
        {
            return this.mTitle;
        }

        void restore()
        {
            int[] arrayOfInt = this.mCompatibilityParamsBackup;
            if (arrayOfInt != null)
            {
                this.x = arrayOfInt[0];
                this.y = arrayOfInt[1];
                this.width = arrayOfInt[2];
                this.height = arrayOfInt[3];
            }
        }

        public void scale(float paramFloat)
        {
            this.x = ((int)(0.5F + paramFloat * this.x));
            this.y = ((int)(0.5F + paramFloat * this.y));
            if (this.width > 0)
                this.width = ((int)(0.5F + paramFloat * this.width));
            if (this.height > 0)
                this.height = ((int)(0.5F + paramFloat * this.height));
        }

        public final void setTitle(CharSequence paramCharSequence)
        {
            if (paramCharSequence == null)
                paramCharSequence = "";
            this.mTitle = TextUtils.stringOrSpannedString(paramCharSequence);
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder(256);
            localStringBuilder.append("WM.LayoutParams{");
            localStringBuilder.append("(");
            localStringBuilder.append(this.x);
            localStringBuilder.append(',');
            localStringBuilder.append(this.y);
            localStringBuilder.append(")(");
            Object localObject1;
            Object localObject2;
            if (this.width == -1)
            {
                localObject1 = "fill";
                localStringBuilder.append(localObject1);
                localStringBuilder.append('x');
                if (this.height != -1)
                    break label606;
                localObject2 = "fill";
            }
            while (true)
            {
                localStringBuilder.append(localObject2);
                localStringBuilder.append(")");
                if (this.horizontalMargin != 0.0F)
                {
                    localStringBuilder.append(" hm=");
                    localStringBuilder.append(this.horizontalMargin);
                }
                if (this.verticalMargin != 0.0F)
                {
                    localStringBuilder.append(" vm=");
                    localStringBuilder.append(this.verticalMargin);
                }
                if (this.gravity != 0)
                {
                    localStringBuilder.append(" gr=#");
                    localStringBuilder.append(Integer.toHexString(this.gravity));
                }
                if (this.softInputMode != 0)
                {
                    localStringBuilder.append(" sim=#");
                    localStringBuilder.append(Integer.toHexString(this.softInputMode));
                }
                localStringBuilder.append(" ty=");
                localStringBuilder.append(this.type);
                localStringBuilder.append(" fl=#");
                localStringBuilder.append(Integer.toHexString(this.flags));
                if (this.privateFlags != 0)
                    localStringBuilder.append(" pfl=0x").append(Integer.toHexString(this.privateFlags));
                if (this.format != -1)
                {
                    localStringBuilder.append(" fmt=");
                    localStringBuilder.append(this.format);
                }
                if (this.windowAnimations != 0)
                {
                    localStringBuilder.append(" wanim=0x");
                    localStringBuilder.append(Integer.toHexString(this.windowAnimations));
                }
                if (this.screenOrientation != -1)
                {
                    localStringBuilder.append(" or=");
                    localStringBuilder.append(this.screenOrientation);
                }
                if (this.alpha != 1.0F)
                {
                    localStringBuilder.append(" alpha=");
                    localStringBuilder.append(this.alpha);
                }
                if (this.screenBrightness != -1.0F)
                {
                    localStringBuilder.append(" sbrt=");
                    localStringBuilder.append(this.screenBrightness);
                }
                if (this.buttonBrightness != -1.0F)
                {
                    localStringBuilder.append(" bbrt=");
                    localStringBuilder.append(this.buttonBrightness);
                }
                if ((0x20000000 & this.flags) != 0)
                    localStringBuilder.append(" compatible=true");
                if (this.systemUiVisibility != 0)
                {
                    localStringBuilder.append(" sysui=0x");
                    localStringBuilder.append(Integer.toHexString(this.systemUiVisibility));
                }
                if (this.subtreeSystemUiVisibility != 0)
                {
                    localStringBuilder.append(" vsysui=0x");
                    localStringBuilder.append(Integer.toHexString(this.subtreeSystemUiVisibility));
                }
                if (this.hasSystemUiListeners)
                {
                    localStringBuilder.append(" sysuil=");
                    localStringBuilder.append(this.hasSystemUiListeners);
                }
                if (this.inputFeatures != 0)
                    localStringBuilder.append(" if=0x").append(Integer.toHexString(this.inputFeatures));
                localStringBuilder.append('}');
                return localStringBuilder.toString();
                if (this.width == -2)
                {
                    localObject1 = "wrap";
                    break;
                }
                localObject1 = Integer.valueOf(this.width);
                break;
                label606: if (this.height == -2)
                    localObject2 = "wrap";
                else
                    localObject2 = Integer.valueOf(this.height);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.width);
            paramParcel.writeInt(this.height);
            paramParcel.writeInt(this.x);
            paramParcel.writeInt(this.y);
            paramParcel.writeInt(this.type);
            paramParcel.writeInt(this.flags);
            paramParcel.writeInt(this.privateFlags);
            paramParcel.writeInt(this.softInputMode);
            paramParcel.writeInt(this.gravity);
            paramParcel.writeFloat(this.horizontalMargin);
            paramParcel.writeFloat(this.verticalMargin);
            paramParcel.writeInt(this.format);
            paramParcel.writeInt(this.windowAnimations);
            paramParcel.writeFloat(this.alpha);
            paramParcel.writeFloat(this.dimAmount);
            paramParcel.writeFloat(this.screenBrightness);
            paramParcel.writeFloat(this.buttonBrightness);
            paramParcel.writeStrongBinder(this.token);
            paramParcel.writeString(this.packageName);
            TextUtils.writeToParcel(this.mTitle, paramParcel, paramInt);
            paramParcel.writeInt(this.screenOrientation);
            paramParcel.writeInt(this.systemUiVisibility);
            paramParcel.writeInt(this.subtreeSystemUiVisibility);
            if (this.hasSystemUiListeners);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                paramParcel.writeInt(this.inputFeatures);
                return;
            }
        }
    }

    public static class BadTokenException extends RuntimeException
    {
        public BadTokenException()
        {
        }

        public BadTokenException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.WindowManager
 * JD-Core Version:        0.6.2
 */