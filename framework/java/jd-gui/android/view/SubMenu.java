package android.view;

import android.graphics.drawable.Drawable;

public abstract interface SubMenu extends Menu
{
    public abstract void clearHeader();

    public abstract MenuItem getItem();

    public abstract SubMenu setHeaderIcon(int paramInt);

    public abstract SubMenu setHeaderIcon(Drawable paramDrawable);

    public abstract SubMenu setHeaderTitle(int paramInt);

    public abstract SubMenu setHeaderTitle(CharSequence paramCharSequence);

    public abstract SubMenu setHeaderView(View paramView);

    public abstract SubMenu setIcon(int paramInt);

    public abstract SubMenu setIcon(Drawable paramDrawable);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.SubMenu
 * JD-Core Version:        0.6.2
 */