package android.view;

public class InflateException extends RuntimeException
{
    public InflateException()
    {
    }

    public InflateException(String paramString)
    {
        super(paramString);
    }

    public InflateException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public InflateException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InflateException
 * JD-Core Version:        0.6.2
 */