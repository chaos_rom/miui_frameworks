package android.view;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemProperties;
import android.util.FloatMath;
import android.util.Log;
import android.util.Slog;

public abstract class WindowOrientationListener
{
    private static final boolean LOG = false;
    private static final String TAG = "WindowOrientationListener";
    private static final boolean USE_GRAVITY_SENSOR;
    int mCurrentRotation = -1;
    private boolean mEnabled;
    private int mRate;
    private Sensor mSensor;
    private SensorEventListenerImpl mSensorEventListener;
    private SensorManager mSensorManager;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public WindowOrientationListener(Context paramContext)
    {
        this(paramContext, 3);
    }

    private WindowOrientationListener(Context paramContext, int paramInt)
    {
        this.mSensorManager = ((SensorManager)paramContext.getSystemService("sensor"));
        this.mRate = paramInt;
        this.mSensor = this.mSensorManager.getDefaultSensor(1);
        if (this.mSensor != null)
            this.mSensorEventListener = new SensorEventListenerImpl(this);
    }

    public boolean canDetectOrientation()
    {
        if (this.mSensor != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void disable()
    {
        if (this.mSensor == null)
            Log.w("WindowOrientationListener", "Cannot detect sensors. Invalid disable");
        while (true)
        {
            return;
            if (this.mEnabled == true)
            {
                if (LOG)
                    Log.d("WindowOrientationListener", "WindowOrientationListener disabled");
                this.mSensorManager.unregisterListener(this.mSensorEventListener);
                this.mEnabled = false;
            }
        }
    }

    public void enable()
    {
        if (this.mSensor == null)
            Log.w("WindowOrientationListener", "Cannot detect sensors. Not enabled");
        while (true)
        {
            return;
            if (!this.mEnabled)
            {
                if (LOG)
                    Log.d("WindowOrientationListener", "WindowOrientationListener enabled");
                this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensor, this.mRate);
                this.mEnabled = true;
            }
        }
    }

    public int getProposedRotation()
    {
        if (this.mEnabled);
        for (int i = this.mSensorEventListener.getProposedRotation(); ; i = -1)
            return i;
    }

    public abstract void onProposedRotationChanged(int paramInt);

    public void setCurrentRotation(int paramInt)
    {
        this.mCurrentRotation = paramInt;
    }

    static final class SensorEventListenerImpl
        implements SensorEventListener
    {
        private static final float ACCELERATION_TOLERANCE = 4.0F;
        private static final int ACCELEROMETER_DATA_X = 0;
        private static final int ACCELEROMETER_DATA_Y = 1;
        private static final int ACCELEROMETER_DATA_Z = 2;
        private static final int ADJACENT_ORIENTATION_ANGLE_GAP = 45;
        private static final float FILTER_TIME_CONSTANT_MS = 200.0F;
        private static final float FLAT_ANGLE = 75.0F;
        private static final long FLAT_TIME_NANOS = 1000000000L;
        private static final float MAX_ACCELERATION_MAGNITUDE = 13.80665F;
        private static final long MAX_FILTER_DELTA_TIME_NANOS = 1000000000L;
        private static final int MAX_TILT = 75;
        private static final float MIN_ACCELERATION_MAGNITUDE = 5.80665F;
        private static final long NANOS_PER_MS = 1000000L;
        private static final float NEAR_ZERO_MAGNITUDE = 1.0F;
        private static final long PROPOSAL_MIN_TIME_SINCE_ACCELERATION_ENDED_NANOS = 500000000L;
        private static final long PROPOSAL_MIN_TIME_SINCE_FLAT_ENDED_NANOS = 500000000L;
        private static final long PROPOSAL_MIN_TIME_SINCE_SWING_ENDED_NANOS = 300000000L;
        private static final long PROPOSAL_SETTLE_TIME_NANOS = 40000000L;
        private static final float RADIANS_TO_DEGREES = 57.29578F;
        private static final float SWING_AWAY_ANGLE_DELTA = 20.0F;
        private static final long SWING_TIME_NANOS = 300000000L;
        private static final int TILT_HISTORY_SIZE = 40;
        private static final int[][] TILT_TOLERANCE = arrayOfInt;
        private long mAccelerationTimestampNanos;
        private long mFlatTimestampNanos;
        private long mLastFilteredTimestampNanos;
        private float mLastFilteredX;
        private float mLastFilteredY;
        private float mLastFilteredZ;
        private final WindowOrientationListener mOrientationListener;
        private int mPredictedRotation;
        private long mPredictedRotationTimestampNanos;
        private int mProposedRotation;
        private long mSwingTimestampNanos;
        private float[] mTiltHistory = new float[40];
        private int mTiltHistoryIndex;
        private long[] mTiltHistoryTimestampNanos = new long[40];

        static
        {
            int[][] arrayOfInt = new int[4][];
            int[] arrayOfInt1 = new int[2];
            arrayOfInt1[0] = -25;
            arrayOfInt1[1] = 70;
            arrayOfInt[0] = arrayOfInt1;
            int[] arrayOfInt2 = new int[2];
            arrayOfInt2[0] = -25;
            arrayOfInt2[1] = 65;
            arrayOfInt[1] = arrayOfInt2;
            int[] arrayOfInt3 = new int[2];
            arrayOfInt3[0] = -25;
            arrayOfInt3[1] = 60;
            arrayOfInt[2] = arrayOfInt3;
            int[] arrayOfInt4 = new int[2];
            arrayOfInt4[0] = -25;
            arrayOfInt4[1] = 65;
            arrayOfInt[3] = arrayOfInt4;
        }

        public SensorEventListenerImpl(WindowOrientationListener paramWindowOrientationListener)
        {
            this.mOrientationListener = paramWindowOrientationListener;
            reset();
        }

        private void addTiltHistoryEntry(long paramLong, float paramFloat)
        {
            this.mTiltHistory[this.mTiltHistoryIndex] = paramFloat;
            this.mTiltHistoryTimestampNanos[this.mTiltHistoryIndex] = paramLong;
            this.mTiltHistoryIndex = ((1 + this.mTiltHistoryIndex) % 40);
            this.mTiltHistoryTimestampNanos[this.mTiltHistoryIndex] = -9223372036854775808L;
        }

        private void clearPredictedRotation()
        {
            this.mPredictedRotation = -1;
            this.mPredictedRotationTimestampNanos = -9223372036854775808L;
        }

        private void clearTiltHistory()
        {
            this.mTiltHistoryTimestampNanos[0] = -9223372036854775808L;
            this.mTiltHistoryIndex = 1;
        }

        private boolean isAccelerating(float paramFloat)
        {
            if ((paramFloat < 5.80665F) || (paramFloat > 13.80665F));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean isFlat(long paramLong)
        {
            int i = this.mTiltHistoryIndex;
            i = nextTiltHistoryIndex(i);
            if ((i < 0) || (this.mTiltHistory[i] < 75.0F));
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                if (1000000000L + this.mTiltHistoryTimestampNanos[i] > paramLong)
                    break;
            }
        }

        private boolean isOrientationAngleAcceptable(int paramInt1, int paramInt2)
        {
            boolean bool = false;
            int i = this.mOrientationListener.mCurrentRotation;
            int j;
            if (i >= 0)
                if ((paramInt1 == i) || (paramInt1 == (i + 1) % 4))
                {
                    j = 22 + (-45 + paramInt1 * 90);
                    if (paramInt1 == 0)
                        if ((paramInt2 < 315) || (paramInt2 >= j + 360))
                            break label73;
                }
            while (true)
            {
                return bool;
                if (paramInt2 >= j)
                {
                    label73: int k;
                    if ((paramInt1 == i) || (paramInt1 == (i + 3) % 4))
                    {
                        k = -22 + (45 + paramInt1 * 90);
                        if (paramInt1 == 0)
                            if ((paramInt2 <= 45) && (paramInt2 > k))
                                continue;
                    }
                    else
                    {
                        while (paramInt2 <= k)
                        {
                            bool = true;
                            break;
                        }
                    }
                }
            }
        }

        private boolean isPredictedRotationAcceptable(long paramLong)
        {
            boolean bool = false;
            if (paramLong < 40000000L + this.mPredictedRotationTimestampNanos);
            while (true)
            {
                return bool;
                if ((paramLong >= 500000000L + this.mFlatTimestampNanos) && (paramLong >= 300000000L + this.mSwingTimestampNanos) && (paramLong >= 500000000L + this.mAccelerationTimestampNanos))
                    bool = true;
            }
        }

        private boolean isSwinging(long paramLong, float paramFloat)
        {
            int i = this.mTiltHistoryIndex;
            i = nextTiltHistoryIndex(i);
            if ((i < 0) || (300000000L + this.mTiltHistoryTimestampNanos[i] < paramLong));
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                if (20.0F + this.mTiltHistory[i] > paramFloat)
                    break;
            }
        }

        private boolean isTiltAngleAcceptable(int paramInt1, int paramInt2)
        {
            boolean bool = true;
            if ((paramInt2 >= TILT_TOLERANCE[paramInt1][0]) && (paramInt2 <= TILT_TOLERANCE[paramInt1][bool]));
            while (true)
            {
                return bool;
                bool = false;
            }
        }

        private int nextTiltHistoryIndex(int paramInt)
        {
            if (paramInt == 0)
                paramInt = 40;
            int i = paramInt - 1;
            if (this.mTiltHistoryTimestampNanos[i] != -9223372036854775808L);
            while (true)
            {
                return i;
                i = -1;
            }
        }

        private static float remainingMS(long paramLong1, long paramLong2)
        {
            if (paramLong1 >= paramLong2);
            for (float f = 0.0F; ; f = 1.0E-06F * (float)(paramLong2 - paramLong1))
                return f;
        }

        private void reset()
        {
            this.mLastFilteredTimestampNanos = -9223372036854775808L;
            this.mProposedRotation = -1;
            this.mFlatTimestampNanos = -9223372036854775808L;
            this.mSwingTimestampNanos = -9223372036854775808L;
            this.mAccelerationTimestampNanos = -9223372036854775808L;
            clearPredictedRotation();
            clearTiltHistory();
        }

        private void updatePredictedRotation(long paramLong, int paramInt)
        {
            if (this.mPredictedRotation != paramInt)
            {
                this.mPredictedRotation = paramInt;
                this.mPredictedRotationTimestampNanos = paramLong;
            }
        }

        public int getProposedRotation()
        {
            return this.mProposedRotation;
        }

        public void onAccuracyChanged(Sensor paramSensor, int paramInt)
        {
        }

        public void onSensorChanged(SensorEvent paramSensorEvent)
        {
            float f1 = paramSensorEvent.values[0];
            float f2 = paramSensorEvent.values[1];
            float f3 = paramSensorEvent.values[2];
            if (WindowOrientationListener.LOG)
                Slog.v("WindowOrientationListener", "Raw acceleration vector: x=" + f1 + ", y=" + f2 + ", z=" + f3 + ", magnitude=" + FloatMath.sqrt(f1 * f1 + f2 * f2 + f3 * f3));
            long l1 = paramSensorEvent.timestamp;
            long l2 = this.mLastFilteredTimestampNanos;
            float f4 = 1.0E-06F * (float)(l1 - l2);
            int i;
            boolean bool1;
            boolean bool2;
            boolean bool3;
            float f5;
            if ((l1 < l2) || (l1 > 1000000000L + l2) || ((f1 == 0.0F) && (f2 == 0.0F) && (f3 == 0.0F)))
            {
                if (WindowOrientationListener.LOG)
                    Slog.v("WindowOrientationListener", "Resetting orientation listener.");
                reset();
                i = 1;
                this.mLastFilteredTimestampNanos = l1;
                this.mLastFilteredX = f1;
                this.mLastFilteredY = f2;
                this.mLastFilteredZ = f3;
                bool1 = false;
                bool2 = false;
                bool3 = false;
                if (i == 0)
                {
                    f5 = FloatMath.sqrt(f1 * f1 + f2 * f2 + f3 * f3);
                    if (f5 >= 1.0F)
                        break label682;
                    if (WindowOrientationListener.LOG)
                        Slog.v("WindowOrientationListener", "Ignoring sensor data, magnitude too close to zero.");
                    clearPredictedRotation();
                }
            }
            while (true)
            {
                int j = this.mProposedRotation;
                if ((this.mPredictedRotation < 0) || (isPredictedRotationAcceptable(l1)))
                    this.mProposedRotation = this.mPredictedRotation;
                if (WindowOrientationListener.LOG)
                    Slog.v("WindowOrientationListener", "Result: currentRotation=" + this.mOrientationListener.mCurrentRotation + ", proposedRotation=" + this.mProposedRotation + ", predictedRotation=" + this.mPredictedRotation + ", timeDeltaMS=" + f4 + ", isAccelerating=" + bool1 + ", isFlat=" + bool2 + ", isSwinging=" + bool3 + ", timeUntilSettledMS=" + remainingMS(l1, 40000000L + this.mPredictedRotationTimestampNanos) + ", timeUntilAccelerationDelayExpiredMS=" + remainingMS(l1, 500000000L + this.mAccelerationTimestampNanos) + ", timeUntilFlatDelayExpiredMS=" + remainingMS(l1, 500000000L + this.mFlatTimestampNanos) + ", timeUntilSwingDelayExpiredMS=" + remainingMS(l1, 300000000L + this.mSwingTimestampNanos));
                if ((this.mProposedRotation != j) && (this.mProposedRotation >= 0))
                {
                    if (WindowOrientationListener.LOG)
                        Slog.v("WindowOrientationListener", "Proposed rotation changed!    proposedRotation=" + this.mProposedRotation + ", oldProposedRotation=" + j);
                    this.mOrientationListener.onProposedRotationChanged(this.mProposedRotation);
                }
                return;
                float f6 = f4 / (200.0F + f4);
                f1 = f6 * (f1 - this.mLastFilteredX) + this.mLastFilteredX;
                f2 = f6 * (f2 - this.mLastFilteredY) + this.mLastFilteredY;
                f3 = f6 * (f3 - this.mLastFilteredZ) + this.mLastFilteredZ;
                if (WindowOrientationListener.LOG)
                    Slog.v("WindowOrientationListener", "Filtered acceleration vector: x=" + f1 + ", y=" + f2 + ", z=" + f3 + ", magnitude=" + FloatMath.sqrt(f1 * f1 + f2 * f2 + f3 * f3));
                i = 0;
                break;
                label682: if (isAccelerating(f5))
                {
                    bool1 = true;
                    this.mAccelerationTimestampNanos = l1;
                }
                int k = (int)Math.round(57.295780181884766D * Math.asin(f3 / f5));
                addTiltHistoryEntry(l1, k);
                if (isFlat(l1))
                {
                    bool2 = true;
                    this.mFlatTimestampNanos = l1;
                }
                if (isSwinging(l1, k))
                {
                    bool3 = true;
                    this.mSwingTimestampNanos = l1;
                }
                if (Math.abs(k) > 75)
                {
                    if (WindowOrientationListener.LOG)
                        Slog.v("WindowOrientationListener", "Ignoring sensor data, tilt angle too high: tiltAngle=" + k);
                    clearPredictedRotation();
                }
                else
                {
                    int m = (int)Math.round(57.295780181884766D * -Math.atan2(-f1, f2));
                    if (m < 0)
                        m += 360;
                    int n = (m + 45) / 90;
                    if (n == 4)
                        n = 0;
                    if ((isTiltAngleAcceptable(n, k)) && (isOrientationAngleAcceptable(n, m)))
                    {
                        updatePredictedRotation(l1, n);
                        if (WindowOrientationListener.LOG)
                            Slog.v("WindowOrientationListener", "Predicted: tiltAngle=" + k + ", orientationAngle=" + m + ", predictedRotation=" + this.mPredictedRotation + ", predictedRotationAgeMS=" + 1.0E-06F * (float)(l1 - this.mPredictedRotationTimestampNanos));
                    }
                    else
                    {
                        if (WindowOrientationListener.LOG)
                            Slog.v("WindowOrientationListener", "Ignoring sensor data, no predicted rotation: tiltAngle=" + k + ", orientationAngle=" + m);
                        clearPredictedRotation();
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.WindowOrientationListener
 * JD-Core Version:        0.6.2
 */