package android.view.accessibility;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IAccessibilityInteractionConnection extends IInterface
{
    public abstract void findAccessibilityNodeInfoByAccessibilityId(long paramLong1, int paramInt1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt4, int paramInt5, long paramLong2)
        throws RemoteException;

    public abstract void findAccessibilityNodeInfoByViewId(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
        throws RemoteException;

    public abstract void findAccessibilityNodeInfosByText(long paramLong1, String paramString, int paramInt1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt4, int paramInt5, long paramLong2)
        throws RemoteException;

    public abstract void findFocus(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
        throws RemoteException;

    public abstract void focusSearch(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
        throws RemoteException;

    public abstract void performAccessibilityAction(long paramLong1, int paramInt1, Bundle paramBundle, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccessibilityInteractionConnection
    {
        private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityInteractionConnection";
        static final int TRANSACTION_findAccessibilityNodeInfoByAccessibilityId = 1;
        static final int TRANSACTION_findAccessibilityNodeInfoByViewId = 2;
        static final int TRANSACTION_findAccessibilityNodeInfosByText = 3;
        static final int TRANSACTION_findFocus = 4;
        static final int TRANSACTION_focusSearch = 5;
        static final int TRANSACTION_performAccessibilityAction = 6;

        public Stub()
        {
            attachInterface(this, "android.view.accessibility.IAccessibilityInteractionConnection");
        }

        public static IAccessibilityInteractionConnection asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.accessibility.IAccessibilityInteractionConnection");
                if ((localIInterface != null) && ((localIInterface instanceof IAccessibilityInteractionConnection)))
                    localObject = (IAccessibilityInteractionConnection)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.view.accessibility.IAccessibilityInteractionConnection");
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
                    findAccessibilityNodeInfoByAccessibilityId(paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
                    findAccessibilityNodeInfoByViewId(paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
                    findAccessibilityNodeInfosByText(paramParcel1.readLong(), paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
                    findFocus(paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
                    focusSearch(paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                    bool = true;
                }
            case 6:
            }
            paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            long l = paramParcel1.readLong();
            int i = paramParcel1.readInt();
            if (paramParcel1.readInt() != 0);
            for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
            {
                performAccessibilityAction(l, i, localBundle, paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                bool = true;
                break;
            }
        }

        private static class Proxy
            implements IAccessibilityInteractionConnection
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void findAccessibilityNodeInfoByAccessibilityId(long paramLong1, int paramInt1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt4, int paramInt5, long paramLong2)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
                    localParcel.writeLong(paramLong1);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeInt(paramInt4);
                    localParcel.writeInt(paramInt5);
                    localParcel.writeLong(paramLong2);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void findAccessibilityNodeInfoByViewId(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
                    localParcel.writeLong(paramLong1);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        localParcel.writeInt(paramInt5);
                        localParcel.writeInt(paramInt6);
                        localParcel.writeLong(paramLong2);
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void findAccessibilityNodeInfosByText(long paramLong1, String paramString, int paramInt1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt4, int paramInt5, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
                    localParcel.writeLong(paramLong1);
                    localParcel.writeString(paramString);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        localParcel.writeInt(paramInt4);
                        localParcel.writeInt(paramInt5);
                        localParcel.writeLong(paramLong2);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void findFocus(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
                    localParcel.writeLong(paramLong1);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        localParcel.writeInt(paramInt5);
                        localParcel.writeInt(paramInt6);
                        localParcel.writeLong(paramLong2);
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void focusSearch(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt5, int paramInt6, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
                    localParcel.writeLong(paramLong1);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        localParcel.writeInt(paramInt5);
                        localParcel.writeInt(paramInt6);
                        localParcel.writeLong(paramLong2);
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.accessibility.IAccessibilityInteractionConnection";
            }

            public void performAccessibilityAction(long paramLong1, int paramInt1, Bundle paramBundle, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
                    localParcel.writeLong(paramLong1);
                    localParcel.writeInt(paramInt1);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt2);
                        if (paramIAccessibilityInteractionConnectionCallback != null)
                            localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        localParcel.writeInt(paramInt3);
                        localParcel.writeInt(paramInt4);
                        localParcel.writeLong(paramLong2);
                        this.mRemote.transact(6, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.IAccessibilityInteractionConnection
 * JD-Core Version:        0.6.2
 */