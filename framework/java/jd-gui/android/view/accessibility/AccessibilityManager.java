package android.view.accessibility;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.IWindow;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class AccessibilityManager
{
    private static final boolean DEBUG = false;
    private static final int DO_SET_STATE = 10;
    private static final String LOG_TAG = "AccessibilityManager";
    public static final int STATE_FLAG_ACCESSIBILITY_ENABLED = 1;
    public static final int STATE_FLAG_TOUCH_EXPLORATION_ENABLED = 2;
    private static AccessibilityManager sInstance;
    static final Object sInstanceSync = new Object();
    final CopyOnWriteArrayList<AccessibilityStateChangeListener> mAccessibilityStateChangeListeners = new CopyOnWriteArrayList();
    final IAccessibilityManagerClient.Stub mClient = new IAccessibilityManagerClient.Stub()
    {
        public void setState(int paramAnonymousInt)
        {
            AccessibilityManager.this.mHandler.obtainMessage(10, paramAnonymousInt, 0).sendToTarget();
        }
    };
    final Handler mHandler;
    boolean mIsEnabled;
    boolean mIsTouchExplorationEnabled;
    final IAccessibilityManager mService;

    public AccessibilityManager(Context paramContext, IAccessibilityManager paramIAccessibilityManager)
    {
        this.mHandler = new MyHandler(paramContext.getMainLooper());
        this.mService = paramIAccessibilityManager;
        try
        {
            setState(this.mService.addClient(this.mClient));
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("AccessibilityManager", "AccessibilityManagerService is dead", localRemoteException);
        }
    }

    public static AccessibilityManager getInstance(Context paramContext)
    {
        synchronized (sInstanceSync)
        {
            if (sInstance == null)
                sInstance = new AccessibilityManager(paramContext, IAccessibilityManager.Stub.asInterface(ServiceManager.getService("accessibility")));
            return sInstance;
        }
    }

    private void notifyAccessibilityStateChanged()
    {
        int i = this.mAccessibilityStateChangeListeners.size();
        for (int j = 0; j < i; j++)
            ((AccessibilityStateChangeListener)this.mAccessibilityStateChangeListeners.get(j)).onAccessibilityStateChanged(this.mIsEnabled);
    }

    private void setAccessibilityState(boolean paramBoolean)
    {
        synchronized (this.mHandler)
        {
            if (paramBoolean != this.mIsEnabled)
            {
                this.mIsEnabled = paramBoolean;
                notifyAccessibilityStateChanged();
            }
            return;
        }
    }

    private void setState(int paramInt)
    {
        boolean bool1 = true;
        boolean bool2;
        if ((paramInt & 0x1) != 0)
        {
            bool2 = bool1;
            setAccessibilityState(bool2);
            if ((paramInt & 0x2) == 0)
                break label32;
        }
        while (true)
        {
            this.mIsTouchExplorationEnabled = bool1;
            return;
            bool2 = false;
            break;
            label32: bool1 = false;
        }
    }

    public int addAccessibilityInteractionConnection(IWindow paramIWindow, IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection)
    {
        try
        {
            int j = this.mService.addAccessibilityInteractionConnection(paramIWindow, paramIAccessibilityInteractionConnection);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("AccessibilityManager", "Error while adding an accessibility interaction connection. ", localRemoteException);
                int i = -1;
            }
        }
    }

    public boolean addAccessibilityStateChangeListener(AccessibilityStateChangeListener paramAccessibilityStateChangeListener)
    {
        return this.mAccessibilityStateChangeListeners.add(paramAccessibilityStateChangeListener);
    }

    @Deprecated
    public List<ServiceInfo> getAccessibilityServiceList()
    {
        List localList = getInstalledAccessibilityServiceList();
        ArrayList localArrayList = new ArrayList();
        int i = localList.size();
        for (int j = 0; j < i; j++)
            localArrayList.add(((AccessibilityServiceInfo)localList.get(j)).getResolveInfo().serviceInfo);
        return Collections.unmodifiableList(localArrayList);
    }

    public IAccessibilityManagerClient getClient()
    {
        return (IAccessibilityManagerClient)this.mClient.asBinder();
    }

    public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int paramInt)
    {
        Object localObject = null;
        try
        {
            List localList = this.mService.getEnabledAccessibilityServiceList(paramInt);
            localObject = localList;
            return Collections.unmodifiableList(localObject);
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("AccessibilityManager", "Error while obtaining the installed AccessibilityServices. ", localRemoteException);
        }
    }

    public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList()
    {
        Object localObject = null;
        try
        {
            List localList = this.mService.getInstalledAccessibilityServiceList();
            localObject = localList;
            return Collections.unmodifiableList(localObject);
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("AccessibilityManager", "Error while obtaining the installed AccessibilityServices. ", localRemoteException);
        }
    }

    public void interrupt()
    {
        if (!this.mIsEnabled)
            throw new IllegalStateException("Accessibility off. Did you forget to check that?");
        try
        {
            this.mService.interrupt();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("AccessibilityManager", "Error while requesting interrupt from all services. ", localRemoteException);
        }
    }

    public boolean isEnabled()
    {
        synchronized (this.mHandler)
        {
            boolean bool = this.mIsEnabled;
            return bool;
        }
    }

    public boolean isTouchExplorationEnabled()
    {
        synchronized (this.mHandler)
        {
            boolean bool = this.mIsTouchExplorationEnabled;
            return bool;
        }
    }

    public void removeAccessibilityInteractionConnection(IWindow paramIWindow)
    {
        try
        {
            this.mService.removeAccessibilityInteractionConnection(paramIWindow);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("AccessibilityManager", "Error while removing an accessibility interaction connection. ", localRemoteException);
        }
    }

    public boolean removeAccessibilityStateChangeListener(AccessibilityStateChangeListener paramAccessibilityStateChangeListener)
    {
        return this.mAccessibilityStateChangeListeners.remove(paramAccessibilityStateChangeListener);
    }

    public void sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        if (!this.mIsEnabled)
            throw new IllegalStateException("Accessibility off. Did you forget to check that?");
        boolean bool = false;
        try
        {
            paramAccessibilityEvent.setEventTime(SystemClock.uptimeMillis());
            long l = Binder.clearCallingIdentity();
            bool = this.mService.sendAccessibilityEvent(paramAccessibilityEvent);
            Binder.restoreCallingIdentity(l);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("AccessibilityManager", "Error during sending " + paramAccessibilityEvent + " ", localRemoteException);
                if (!bool);
            }
        }
        finally
        {
            if (bool)
                paramAccessibilityEvent.recycle();
        }
    }

    class MyHandler extends Handler
    {
        MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.w("AccessibilityManager", "Unknown message type: " + paramMessage.what);
            case 10:
            }
            while (true)
            {
                return;
                AccessibilityManager.this.setState(paramMessage.arg1);
            }
        }
    }

    public static abstract interface AccessibilityStateChangeListener
    {
        public abstract void onAccessibilityStateChanged(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityManager
 * JD-Core Version:        0.6.2
 */