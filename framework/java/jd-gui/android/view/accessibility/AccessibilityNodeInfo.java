package android.view.accessibility;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.SparseLongArray;
import android.view.View;
import java.util.Collections;
import java.util.List;

public class AccessibilityNodeInfo
    implements Parcelable
{
    public static final int ACTION_ACCESSIBILITY_FOCUS = 64;
    public static final String ACTION_ARGUMENT_HTML_ELEMENT_STRING = "ACTION_ARGUMENT_HTML_ELEMENT_STRING";
    public static final String ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT";
    public static final int ACTION_CLEAR_ACCESSIBILITY_FOCUS = 128;
    public static final int ACTION_CLEAR_FOCUS = 2;
    public static final int ACTION_CLEAR_SELECTION = 8;
    public static final int ACTION_CLICK = 16;
    public static final int ACTION_FOCUS = 1;
    public static final int ACTION_LONG_CLICK = 32;
    public static final int ACTION_NEXT_AT_MOVEMENT_GRANULARITY = 256;
    public static final int ACTION_NEXT_HTML_ELEMENT = 1024;
    public static final int ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = 512;
    public static final int ACTION_PREVIOUS_HTML_ELEMENT = 2048;
    public static final int ACTION_SCROLL_BACKWARD = 8192;
    public static final int ACTION_SCROLL_FORWARD = 4096;
    public static final int ACTION_SELECT = 4;
    public static final int ACTIVE_WINDOW_ID = -1;
    public static final Parcelable.Creator<AccessibilityNodeInfo> CREATOR = new Parcelable.Creator()
    {
        public AccessibilityNodeInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo = AccessibilityNodeInfo.obtain();
            localAccessibilityNodeInfo.initFromParcel(paramAnonymousParcel);
            return localAccessibilityNodeInfo;
        }

        public AccessibilityNodeInfo[] newArray(int paramAnonymousInt)
        {
            return new AccessibilityNodeInfo[paramAnonymousInt];
        }
    };
    private static final boolean DEBUG = false;
    public static final int FLAG_PREFETCH_DESCENDANTS = 4;
    public static final int FLAG_PREFETCH_PREDECESSORS = 1;
    public static final int FLAG_PREFETCH_SIBLINGS = 2;
    public static final int FOCUS_ACCESSIBILITY = 2;
    public static final int FOCUS_INPUT = 1;
    public static final int INCLUDE_NOT_IMPORTANT_VIEWS = 8;
    private static final int MAX_POOL_SIZE = 50;
    public static final int MOVEMENT_GRANULARITY_CHARACTER = 1;
    public static final int MOVEMENT_GRANULARITY_LINE = 4;
    public static final int MOVEMENT_GRANULARITY_PAGE = 16;
    public static final int MOVEMENT_GRANULARITY_PARAGRAPH = 8;
    public static final int MOVEMENT_GRANULARITY_WORD = 2;
    private static final int PROPERTY_ACCESSIBILITY_FOCUSED = 1024;
    private static final int PROPERTY_CHECKABLE = 1;
    private static final int PROPERTY_CHECKED = 2;
    private static final int PROPERTY_CLICKABLE = 32;
    private static final int PROPERTY_ENABLED = 128;
    private static final int PROPERTY_FOCUSABLE = 4;
    private static final int PROPERTY_FOCUSED = 8;
    private static final int PROPERTY_LONG_CLICKABLE = 64;
    private static final int PROPERTY_PASSWORD = 256;
    private static final int PROPERTY_SCROLLABLE = 512;
    private static final int PROPERTY_SELECTED = 16;
    private static final int PROPERTY_VISIBLE_TO_USER = 2048;
    public static final long ROOT_NODE_ID = 0L;
    public static final int UNDEFINED = -1;
    private static final long VIRTUAL_DESCENDANT_ID_MASK = -4294967296L;
    private static final int VIRTUAL_DESCENDANT_ID_SHIFT = 32;
    private static AccessibilityNodeInfo sPool;
    private static final Object sPoolLock = new Object();
    private static int sPoolSize;
    private int mActions;
    private int mActualAndReportedWindowLeftDelta;
    private int mActualAndReportedWindowTopDelta;
    private int mBooleanProperties;
    private final Rect mBoundsInParent = new Rect();
    private final Rect mBoundsInScreen = new Rect();
    private final SparseLongArray mChildNodeIds = new SparseLongArray();
    private CharSequence mClassName;
    private int mConnectionId = -1;
    private CharSequence mContentDescription;
    private boolean mIsInPool;
    private int mMovementGranularities;
    private AccessibilityNodeInfo mNext;
    private CharSequence mPackageName;
    private long mParentNodeId = ROOT_NODE_ID;
    private boolean mSealed;
    private long mSourceNodeId = ROOT_NODE_ID;
    private CharSequence mText;
    private int mWindowId = -1;

    private boolean canPerformRequestOverConnection(long paramLong)
    {
        if ((this.mWindowId != -1) && (getAccessibilityViewId(paramLong) != -1) && (this.mConnectionId != -1));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void clear()
    {
        this.mSealed = false;
        this.mSourceNodeId = ROOT_NODE_ID;
        this.mParentNodeId = ROOT_NODE_ID;
        this.mWindowId = -1;
        this.mConnectionId = -1;
        this.mMovementGranularities = 0;
        this.mChildNodeIds.clear();
        this.mBoundsInParent.set(0, 0, 0, 0);
        this.mBoundsInScreen.set(0, 0, 0, 0);
        this.mBooleanProperties = 0;
        this.mPackageName = null;
        this.mClassName = null;
        this.mText = null;
        this.mContentDescription = null;
        this.mActions = 0;
    }

    private void enforceValidFocusDirection(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown direction: " + paramInt);
        case 1:
        case 2:
        case 17:
        case 33:
        case 66:
        case 130:
        case 4097:
        case 4098:
        case 4113:
        case 4129:
        case 4162:
        case 4226:
        }
    }

    private void enforceValidFocusType(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown focus type: " + paramInt);
        case 1:
        case 2:
        }
    }

    public static int getAccessibilityViewId(long paramLong)
    {
        return (int)paramLong;
    }

    private static String getActionSymbolicName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown action: " + paramInt);
        case 1:
            str = "ACTION_FOCUS";
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 64:
        case 128:
        case 256:
        case 512:
        case 1024:
        case 2048:
        case 4096:
        case 8192:
        }
        while (true)
        {
            return str;
            str = "ACTION_CLEAR_FOCUS";
            continue;
            str = "ACTION_SELECT";
            continue;
            str = "ACTION_CLEAR_SELECTION";
            continue;
            str = "ACTION_CLICK";
            continue;
            str = "ACTION_LONG_CLICK";
            continue;
            str = "ACTION_ACCESSIBILITY_FOCUS";
            continue;
            str = "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            continue;
            str = "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            continue;
            str = "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            continue;
            str = "ACTION_NEXT_HTML_ELEMENT";
            continue;
            str = "ACTION_PREVIOUS_HTML_ELEMENT";
            continue;
            str = "ACTION_SCROLL_FORWARD";
            continue;
            str = "ACTION_SCROLL_BACKWARD";
        }
    }

    private boolean getBooleanProperty(int paramInt)
    {
        if ((paramInt & this.mBooleanProperties) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static String getMovementGranularitySymbolicName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown movement granularity: " + paramInt);
        case 1:
            str = "MOVEMENT_GRANULARITY_CHARACTER";
        case 2:
        case 4:
        case 8:
        case 16:
        }
        while (true)
        {
            return str;
            str = "MOVEMENT_GRANULARITY_WORD";
            continue;
            str = "MOVEMENT_GRANULARITY_LINE";
            continue;
            str = "MOVEMENT_GRANULARITY_PARAGRAPH";
            continue;
            str = "MOVEMENT_GRANULARITY_PAGE";
        }
    }

    public static int getVirtualDescendantId(long paramLong)
    {
        return (int)((0x0 & paramLong) >> 32);
    }

    private void init(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        this.mSealed = paramAccessibilityNodeInfo.mSealed;
        this.mSourceNodeId = paramAccessibilityNodeInfo.mSourceNodeId;
        this.mParentNodeId = paramAccessibilityNodeInfo.mParentNodeId;
        this.mWindowId = paramAccessibilityNodeInfo.mWindowId;
        this.mConnectionId = paramAccessibilityNodeInfo.mConnectionId;
        this.mBoundsInParent.set(paramAccessibilityNodeInfo.mBoundsInParent);
        this.mBoundsInScreen.set(paramAccessibilityNodeInfo.mBoundsInScreen);
        this.mPackageName = paramAccessibilityNodeInfo.mPackageName;
        this.mClassName = paramAccessibilityNodeInfo.mClassName;
        this.mText = paramAccessibilityNodeInfo.mText;
        this.mContentDescription = paramAccessibilityNodeInfo.mContentDescription;
        this.mActions = paramAccessibilityNodeInfo.mActions;
        this.mBooleanProperties = paramAccessibilityNodeInfo.mBooleanProperties;
        this.mMovementGranularities = paramAccessibilityNodeInfo.mMovementGranularities;
        int i = paramAccessibilityNodeInfo.mChildNodeIds.size();
        for (int j = 0; j < i; j++)
            this.mChildNodeIds.put(j, paramAccessibilityNodeInfo.mChildNodeIds.valueAt(j));
    }

    private void initFromParcel(Parcel paramParcel)
    {
        int i = 1;
        if (paramParcel.readInt() == i);
        while (true)
        {
            this.mSealed = i;
            this.mSourceNodeId = paramParcel.readLong();
            this.mWindowId = paramParcel.readInt();
            this.mParentNodeId = paramParcel.readLong();
            this.mConnectionId = paramParcel.readInt();
            SparseLongArray localSparseLongArray = this.mChildNodeIds;
            int j = paramParcel.readInt();
            for (int k = 0; k < j; k++)
                localSparseLongArray.put(k, paramParcel.readLong());
            i = 0;
        }
        this.mBoundsInParent.top = paramParcel.readInt();
        this.mBoundsInParent.bottom = paramParcel.readInt();
        this.mBoundsInParent.left = paramParcel.readInt();
        this.mBoundsInParent.right = paramParcel.readInt();
        this.mBoundsInScreen.top = paramParcel.readInt();
        this.mBoundsInScreen.bottom = paramParcel.readInt();
        this.mBoundsInScreen.left = paramParcel.readInt();
        this.mBoundsInScreen.right = paramParcel.readInt();
        this.mActions = paramParcel.readInt();
        this.mMovementGranularities = paramParcel.readInt();
        this.mBooleanProperties = paramParcel.readInt();
        this.mPackageName = paramParcel.readCharSequence();
        this.mClassName = paramParcel.readCharSequence();
        this.mText = paramParcel.readCharSequence();
        this.mContentDescription = paramParcel.readCharSequence();
    }

    public static long makeNodeId(int paramInt1, int paramInt2)
    {
        return paramInt2 << 32 | paramInt1;
    }

    public static AccessibilityNodeInfo obtain()
    {
        AccessibilityNodeInfo localAccessibilityNodeInfo;
        synchronized (sPoolLock)
        {
            if (sPool != null)
            {
                localAccessibilityNodeInfo = sPool;
                sPool = sPool.mNext;
                sPoolSize = -1 + sPoolSize;
                localAccessibilityNodeInfo.mNext = null;
                localAccessibilityNodeInfo.mIsInPool = false;
            }
            else
            {
                localAccessibilityNodeInfo = new AccessibilityNodeInfo();
            }
        }
        return localAccessibilityNodeInfo;
    }

    public static AccessibilityNodeInfo obtain(View paramView)
    {
        AccessibilityNodeInfo localAccessibilityNodeInfo = obtain();
        localAccessibilityNodeInfo.setSource(paramView);
        return localAccessibilityNodeInfo;
    }

    public static AccessibilityNodeInfo obtain(View paramView, int paramInt)
    {
        AccessibilityNodeInfo localAccessibilityNodeInfo = obtain();
        localAccessibilityNodeInfo.setSource(paramView, paramInt);
        return localAccessibilityNodeInfo;
    }

    public static AccessibilityNodeInfo obtain(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        AccessibilityNodeInfo localAccessibilityNodeInfo = obtain();
        localAccessibilityNodeInfo.init(paramAccessibilityNodeInfo);
        return localAccessibilityNodeInfo;
    }

    private void setBooleanProperty(int paramInt, boolean paramBoolean)
    {
        enforceNotSealed();
        if (paramBoolean);
        for (this.mBooleanProperties = (paramInt | this.mBooleanProperties); ; this.mBooleanProperties &= (paramInt ^ 0xFFFFFFFF))
            return;
    }

    public void addAction(int paramInt)
    {
        enforceNotSealed();
        this.mActions = (paramInt | this.mActions);
    }

    public void addChild(View paramView)
    {
        addChild(paramView, -1);
    }

    public void addChild(View paramView, int paramInt)
    {
        enforceNotSealed();
        int i = this.mChildNodeIds.size();
        if (paramView != null);
        for (int j = paramView.getAccessibilityViewId(); ; j = -1)
        {
            long l = makeNodeId(j, paramInt);
            this.mChildNodeIds.put(i, l);
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    protected void enforceNotSealed()
    {
        if (isSealed())
            throw new IllegalStateException("Cannot perform this action on a sealed instance.");
    }

    protected void enforceSealed()
    {
        if (!isSealed())
            throw new IllegalStateException("Cannot perform this action on a not sealed instance.");
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (paramObject == null)
            {
                bool = false;
            }
            else if (getClass() != paramObject.getClass())
            {
                bool = false;
            }
            else
            {
                AccessibilityNodeInfo localAccessibilityNodeInfo = (AccessibilityNodeInfo)paramObject;
                if (this.mSourceNodeId != localAccessibilityNodeInfo.mSourceNodeId)
                    bool = false;
                else if (this.mWindowId != localAccessibilityNodeInfo.mWindowId)
                    bool = false;
            }
        }
    }

    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String paramString)
    {
        enforceSealed();
        if (!canPerformRequestOverConnection(this.mSourceNodeId));
        for (List localList = Collections.emptyList(); ; localList = AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfosByText(this.mConnectionId, this.mWindowId, this.mSourceNodeId, paramString))
            return localList;
    }

    public AccessibilityNodeInfo findFocus(int paramInt)
    {
        enforceSealed();
        enforceValidFocusType(paramInt);
        if (!canPerformRequestOverConnection(this.mSourceNodeId));
        for (AccessibilityNodeInfo localAccessibilityNodeInfo = null; ; localAccessibilityNodeInfo = AccessibilityInteractionClient.getInstance().findFocus(this.mConnectionId, this.mWindowId, this.mSourceNodeId, paramInt))
            return localAccessibilityNodeInfo;
    }

    public AccessibilityNodeInfo focusSearch(int paramInt)
    {
        enforceSealed();
        enforceValidFocusDirection(paramInt);
        if (!canPerformRequestOverConnection(this.mSourceNodeId));
        for (AccessibilityNodeInfo localAccessibilityNodeInfo = null; ; localAccessibilityNodeInfo = AccessibilityInteractionClient.getInstance().focusSearch(this.mConnectionId, this.mWindowId, this.mSourceNodeId, paramInt))
            return localAccessibilityNodeInfo;
    }

    public int getActions()
    {
        return this.mActions;
    }

    public void getBoundsInParent(Rect paramRect)
    {
        paramRect.set(this.mBoundsInParent.left, this.mBoundsInParent.top, this.mBoundsInParent.right, this.mBoundsInParent.bottom);
    }

    public void getBoundsInScreen(Rect paramRect)
    {
        paramRect.set(this.mBoundsInScreen.left, this.mBoundsInScreen.top, this.mBoundsInScreen.right, this.mBoundsInScreen.bottom);
    }

    public AccessibilityNodeInfo getChild(int paramInt)
    {
        enforceSealed();
        if (!canPerformRequestOverConnection(this.mSourceNodeId));
        long l;
        for (AccessibilityNodeInfo localAccessibilityNodeInfo = null; ; localAccessibilityNodeInfo = AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfoByAccessibilityId(this.mConnectionId, this.mWindowId, l, 4))
        {
            return localAccessibilityNodeInfo;
            l = this.mChildNodeIds.get(paramInt);
        }
    }

    public int getChildCount()
    {
        return this.mChildNodeIds.size();
    }

    public SparseLongArray getChildNodeIds()
    {
        return this.mChildNodeIds;
    }

    public CharSequence getClassName()
    {
        return this.mClassName;
    }

    public CharSequence getContentDescription()
    {
        return this.mContentDescription;
    }

    public int getMovementGranularities()
    {
        return this.mMovementGranularities;
    }

    public CharSequence getPackageName()
    {
        return this.mPackageName;
    }

    public AccessibilityNodeInfo getParent()
    {
        enforceSealed();
        if (!canPerformRequestOverConnection(this.mParentNodeId));
        for (AccessibilityNodeInfo localAccessibilityNodeInfo = null; ; localAccessibilityNodeInfo = AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfoByAccessibilityId(this.mConnectionId, this.mWindowId, this.mParentNodeId, 6))
            return localAccessibilityNodeInfo;
    }

    public long getParentNodeId()
    {
        return this.mParentNodeId;
    }

    public long getSourceNodeId()
    {
        return this.mSourceNodeId;
    }

    public CharSequence getText()
    {
        return this.mText;
    }

    public int getWindowId()
    {
        return this.mWindowId;
    }

    public int hashCode()
    {
        return 31 * (31 * (31 + getAccessibilityViewId(this.mSourceNodeId)) + getVirtualDescendantId(this.mSourceNodeId)) + this.mWindowId;
    }

    public boolean isAccessibilityFocused()
    {
        return getBooleanProperty(1024);
    }

    public boolean isCheckable()
    {
        return getBooleanProperty(1);
    }

    public boolean isChecked()
    {
        return getBooleanProperty(2);
    }

    public boolean isClickable()
    {
        return getBooleanProperty(32);
    }

    public boolean isEnabled()
    {
        return getBooleanProperty(128);
    }

    public boolean isFocusable()
    {
        return getBooleanProperty(4);
    }

    public boolean isFocused()
    {
        return getBooleanProperty(8);
    }

    public boolean isLongClickable()
    {
        return getBooleanProperty(64);
    }

    public boolean isPassword()
    {
        return getBooleanProperty(256);
    }

    public boolean isScrollable()
    {
        return getBooleanProperty(512);
    }

    public boolean isSealed()
    {
        return this.mSealed;
    }

    public boolean isSelected()
    {
        return getBooleanProperty(16);
    }

    public boolean isVisibleToUser()
    {
        return getBooleanProperty(2048);
    }

    public boolean performAction(int paramInt)
    {
        enforceSealed();
        if (!canPerformRequestOverConnection(this.mSourceNodeId));
        for (boolean bool = false; ; bool = AccessibilityInteractionClient.getInstance().performAccessibilityAction(this.mConnectionId, this.mWindowId, this.mSourceNodeId, paramInt, null))
            return bool;
    }

    public boolean performAction(int paramInt, Bundle paramBundle)
    {
        enforceSealed();
        if (!canPerformRequestOverConnection(this.mSourceNodeId));
        for (boolean bool = false; ; bool = AccessibilityInteractionClient.getInstance().performAccessibilityAction(this.mConnectionId, this.mWindowId, this.mSourceNodeId, paramInt, paramBundle))
            return bool;
    }

    public void recycle()
    {
        if (this.mIsInPool)
            throw new IllegalStateException("Info already recycled!");
        clear();
        synchronized (sPoolLock)
        {
            if (sPoolSize <= 50)
            {
                this.mNext = sPool;
                sPool = this;
                this.mIsInPool = true;
                sPoolSize = 1 + sPoolSize;
            }
            return;
        }
    }

    public void setAccessibilityFocused(boolean paramBoolean)
    {
        setBooleanProperty(1024, paramBoolean);
    }

    public void setBoundsInParent(Rect paramRect)
    {
        enforceNotSealed();
        this.mBoundsInParent.set(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public void setBoundsInScreen(Rect paramRect)
    {
        enforceNotSealed();
        this.mBoundsInScreen.set(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
        this.mBoundsInScreen.offset(this.mActualAndReportedWindowLeftDelta, this.mActualAndReportedWindowTopDelta);
    }

    public void setCheckable(boolean paramBoolean)
    {
        setBooleanProperty(1, paramBoolean);
    }

    public void setChecked(boolean paramBoolean)
    {
        setBooleanProperty(2, paramBoolean);
    }

    public void setClassName(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mClassName = paramCharSequence;
    }

    public void setClickable(boolean paramBoolean)
    {
        setBooleanProperty(32, paramBoolean);
    }

    public void setConnectionId(int paramInt)
    {
        enforceNotSealed();
        this.mConnectionId = paramInt;
    }

    public void setContentDescription(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mContentDescription = paramCharSequence;
    }

    public void setEnabled(boolean paramBoolean)
    {
        setBooleanProperty(128, paramBoolean);
    }

    public void setFocusable(boolean paramBoolean)
    {
        setBooleanProperty(4, paramBoolean);
    }

    public void setFocused(boolean paramBoolean)
    {
        setBooleanProperty(8, paramBoolean);
    }

    public void setLongClickable(boolean paramBoolean)
    {
        setBooleanProperty(64, paramBoolean);
    }

    public void setMovementGranularities(int paramInt)
    {
        enforceNotSealed();
        this.mMovementGranularities = paramInt;
    }

    public void setPackageName(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mPackageName = paramCharSequence;
    }

    public void setParent(View paramView)
    {
        setParent(paramView, -1);
    }

    public void setParent(View paramView, int paramInt)
    {
        enforceNotSealed();
        if (paramView != null);
        for (int i = paramView.getAccessibilityViewId(); ; i = -1)
        {
            this.mParentNodeId = makeNodeId(i, paramInt);
            return;
        }
    }

    public void setPassword(boolean paramBoolean)
    {
        setBooleanProperty(256, paramBoolean);
    }

    public void setScrollable(boolean paramBoolean)
    {
        enforceNotSealed();
        setBooleanProperty(512, paramBoolean);
    }

    public void setSealed(boolean paramBoolean)
    {
        this.mSealed = paramBoolean;
    }

    public void setSelected(boolean paramBoolean)
    {
        setBooleanProperty(16, paramBoolean);
    }

    public void setSource(View paramView)
    {
        setSource(paramView, -1);
    }

    public void setSource(View paramView, int paramInt)
    {
        enforceNotSealed();
        int i;
        if (paramView != null)
        {
            i = paramView.getAccessibilityWindowId();
            this.mWindowId = i;
            if (paramView == null)
                break label65;
        }
        label65: for (int j = paramView.getAccessibilityViewId(); ; j = -1)
        {
            this.mSourceNodeId = makeNodeId(j, paramInt);
            if (paramView != null)
            {
                this.mActualAndReportedWindowLeftDelta = paramView.getActualAndReportedWindowLeftDelta();
                this.mActualAndReportedWindowTopDelta = paramView.getActualAndReportedWindowTopDelta();
            }
            return;
            i = -1;
            break;
        }
    }

    public void setText(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mText = paramCharSequence;
    }

    public void setVisibleToUser(boolean paramBoolean)
    {
        setBooleanProperty(2048, paramBoolean);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(super.toString());
        localStringBuilder.append("; boundsInParent: " + this.mBoundsInParent);
        localStringBuilder.append("; boundsInScreen: " + this.mBoundsInScreen);
        localStringBuilder.append("; packageName: ").append(this.mPackageName);
        localStringBuilder.append("; className: ").append(this.mClassName);
        localStringBuilder.append("; text: ").append(this.mText);
        localStringBuilder.append("; contentDescription: ").append(this.mContentDescription);
        localStringBuilder.append("; checkable: ").append(isCheckable());
        localStringBuilder.append("; checked: ").append(isChecked());
        localStringBuilder.append("; focusable: ").append(isFocusable());
        localStringBuilder.append("; focused: ").append(isFocused());
        localStringBuilder.append("; selected: ").append(isSelected());
        localStringBuilder.append("; clickable: ").append(isClickable());
        localStringBuilder.append("; longClickable: ").append(isLongClickable());
        localStringBuilder.append("; enabled: ").append(isEnabled());
        localStringBuilder.append("; password: ").append(isPassword());
        localStringBuilder.append("; scrollable: " + isScrollable());
        localStringBuilder.append("; [");
        int i = this.mActions;
        while (i != 0)
        {
            int j = 1 << Integer.numberOfTrailingZeros(i);
            i &= (j ^ 0xFFFFFFFF);
            localStringBuilder.append(getActionSymbolicName(j));
            if (i != 0)
                localStringBuilder.append(", ");
        }
        localStringBuilder.append("]");
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (isSealed());
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            paramParcel.writeLong(this.mSourceNodeId);
            paramParcel.writeInt(this.mWindowId);
            paramParcel.writeLong(this.mParentNodeId);
            paramParcel.writeInt(this.mConnectionId);
            SparseLongArray localSparseLongArray = this.mChildNodeIds;
            int j = localSparseLongArray.size();
            paramParcel.writeInt(j);
            for (int k = 0; k < j; k++)
                paramParcel.writeLong(localSparseLongArray.valueAt(k));
        }
        paramParcel.writeInt(this.mBoundsInParent.top);
        paramParcel.writeInt(this.mBoundsInParent.bottom);
        paramParcel.writeInt(this.mBoundsInParent.left);
        paramParcel.writeInt(this.mBoundsInParent.right);
        paramParcel.writeInt(this.mBoundsInScreen.top);
        paramParcel.writeInt(this.mBoundsInScreen.bottom);
        paramParcel.writeInt(this.mBoundsInScreen.left);
        paramParcel.writeInt(this.mBoundsInScreen.right);
        paramParcel.writeInt(this.mActions);
        paramParcel.writeInt(this.mMovementGranularities);
        paramParcel.writeInt(this.mBooleanProperties);
        paramParcel.writeCharSequence(this.mPackageName);
        paramParcel.writeCharSequence(this.mClassName);
        paramParcel.writeCharSequence(this.mText);
        paramParcel.writeCharSequence(this.mContentDescription);
        recycle();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityNodeInfo
 * JD-Core Version:        0.6.2
 */