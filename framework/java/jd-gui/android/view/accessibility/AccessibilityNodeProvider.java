package android.view.accessibility;

import android.os.Bundle;
import java.util.List;

public abstract class AccessibilityNodeProvider
{
    public AccessibilityNodeInfo accessibilityFocusSearch(int paramInt1, int paramInt2)
    {
        return null;
    }

    public AccessibilityNodeInfo createAccessibilityNodeInfo(int paramInt)
    {
        return null;
    }

    public AccessibilityNodeInfo findAccessibilityFocus(int paramInt)
    {
        return null;
    }

    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String paramString, int paramInt)
    {
        return null;
    }

    public boolean performAction(int paramInt1, int paramInt2, Bundle paramBundle)
    {
        return false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityNodeProvider
 * JD-Core Version:        0.6.2
 */