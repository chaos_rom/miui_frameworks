package android.view.accessibility;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public final class AccessibilityEvent extends AccessibilityRecord
    implements Parcelable
{
    public static final Parcelable.Creator<AccessibilityEvent> CREATOR = new Parcelable.Creator()
    {
        public AccessibilityEvent createFromParcel(Parcel paramAnonymousParcel)
        {
            AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain();
            localAccessibilityEvent.initFromParcel(paramAnonymousParcel);
            return localAccessibilityEvent;
        }

        public AccessibilityEvent[] newArray(int paramAnonymousInt)
        {
            return new AccessibilityEvent[paramAnonymousInt];
        }
    };
    private static final boolean DEBUG = false;
    public static final int INVALID_POSITION = -1;
    private static final int MAX_POOL_SIZE = 10;

    @Deprecated
    public static final int MAX_TEXT_LENGTH = 500;
    public static final int TYPES_ALL_MASK = -1;
    public static final int TYPE_ANNOUNCEMENT = 16384;
    public static final int TYPE_NOTIFICATION_STATE_CHANGED = 64;
    public static final int TYPE_TOUCH_EXPLORATION_GESTURE_END = 1024;
    public static final int TYPE_TOUCH_EXPLORATION_GESTURE_START = 512;
    public static final int TYPE_VIEW_ACCESSIBILITY_FOCUSED = 32768;
    public static final int TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED = 65536;
    public static final int TYPE_VIEW_CLICKED = 1;
    public static final int TYPE_VIEW_FOCUSED = 8;
    public static final int TYPE_VIEW_HOVER_ENTER = 128;
    public static final int TYPE_VIEW_HOVER_EXIT = 256;
    public static final int TYPE_VIEW_LONG_CLICKED = 2;
    public static final int TYPE_VIEW_SCROLLED = 4096;
    public static final int TYPE_VIEW_SELECTED = 4;
    public static final int TYPE_VIEW_TEXT_CHANGED = 16;
    public static final int TYPE_VIEW_TEXT_SELECTION_CHANGED = 8192;
    public static final int TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY = 131072;
    public static final int TYPE_WINDOW_CONTENT_CHANGED = 2048;
    public static final int TYPE_WINDOW_STATE_CHANGED = 32;
    private static AccessibilityEvent sPool;
    private static final Object sPoolLock = new Object();
    private static int sPoolSize;
    int mAction;
    private long mEventTime;
    private int mEventType;
    private boolean mIsInPool;
    int mMovementGranularity;
    private AccessibilityEvent mNext;
    private CharSequence mPackageName;
    private final ArrayList<AccessibilityRecord> mRecords = new ArrayList();

    public static String eventTypeToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = null;
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 128:
        case 256:
        case 64:
        case 512:
        case 1024:
        case 2048:
        case 8192:
        case 4096:
        case 16384:
        case 32768:
        case 65536:
        case 131072:
        }
        while (true)
        {
            return str;
            str = "TYPE_VIEW_CLICKED";
            continue;
            str = "TYPE_VIEW_LONG_CLICKED";
            continue;
            str = "TYPE_VIEW_SELECTED";
            continue;
            str = "TYPE_VIEW_FOCUSED";
            continue;
            str = "TYPE_VIEW_TEXT_CHANGED";
            continue;
            str = "TYPE_WINDOW_STATE_CHANGED";
            continue;
            str = "TYPE_VIEW_HOVER_ENTER";
            continue;
            str = "TYPE_VIEW_HOVER_EXIT";
            continue;
            str = "TYPE_NOTIFICATION_STATE_CHANGED";
            continue;
            str = "TYPE_TOUCH_EXPLORATION_GESTURE_START";
            continue;
            str = "TYPE_TOUCH_EXPLORATION_GESTURE_END";
            continue;
            str = "TYPE_WINDOW_CONTENT_CHANGED";
            continue;
            str = "TYPE_VIEW_TEXT_SELECTION_CHANGED";
            continue;
            str = "TYPE_VIEW_SCROLLED";
            continue;
            str = "TYPE_ANNOUNCEMENT";
            continue;
            str = "TYPE_VIEW_ACCESSIBILITY_FOCUSED";
            continue;
            str = "TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED";
            continue;
            str = "TYPE_CURRENT_AT_GRANULARITY_MOVEMENT_CHANGED";
        }
    }

    public static AccessibilityEvent obtain()
    {
        AccessibilityEvent localAccessibilityEvent;
        synchronized (sPoolLock)
        {
            if (sPool != null)
            {
                localAccessibilityEvent = sPool;
                sPool = sPool.mNext;
                sPoolSize = -1 + sPoolSize;
                localAccessibilityEvent.mNext = null;
                localAccessibilityEvent.mIsInPool = false;
            }
            else
            {
                localAccessibilityEvent = new AccessibilityEvent();
            }
        }
        return localAccessibilityEvent;
    }

    public static AccessibilityEvent obtain(int paramInt)
    {
        AccessibilityEvent localAccessibilityEvent = obtain();
        localAccessibilityEvent.setEventType(paramInt);
        return localAccessibilityEvent;
    }

    public static AccessibilityEvent obtain(AccessibilityEvent paramAccessibilityEvent)
    {
        AccessibilityEvent localAccessibilityEvent = obtain();
        localAccessibilityEvent.init(paramAccessibilityEvent);
        int i = paramAccessibilityEvent.mRecords.size();
        for (int j = 0; j < i; j++)
        {
            AccessibilityRecord localAccessibilityRecord = AccessibilityRecord.obtain((AccessibilityRecord)paramAccessibilityEvent.mRecords.get(j));
            localAccessibilityEvent.mRecords.add(localAccessibilityRecord);
        }
        return localAccessibilityEvent;
    }

    private void readAccessibilityRecordFromParcel(AccessibilityRecord paramAccessibilityRecord, Parcel paramParcel)
    {
        paramAccessibilityRecord.mBooleanProperties = paramParcel.readInt();
        paramAccessibilityRecord.mCurrentItemIndex = paramParcel.readInt();
        paramAccessibilityRecord.mItemCount = paramParcel.readInt();
        paramAccessibilityRecord.mFromIndex = paramParcel.readInt();
        paramAccessibilityRecord.mToIndex = paramParcel.readInt();
        paramAccessibilityRecord.mScrollX = paramParcel.readInt();
        paramAccessibilityRecord.mScrollY = paramParcel.readInt();
        paramAccessibilityRecord.mMaxScrollX = paramParcel.readInt();
        paramAccessibilityRecord.mMaxScrollY = paramParcel.readInt();
        paramAccessibilityRecord.mAddedCount = paramParcel.readInt();
        paramAccessibilityRecord.mRemovedCount = paramParcel.readInt();
        paramAccessibilityRecord.mClassName = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        paramAccessibilityRecord.mContentDescription = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        paramAccessibilityRecord.mBeforeText = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        paramAccessibilityRecord.mParcelableData = paramParcel.readParcelable(null);
        paramParcel.readList(paramAccessibilityRecord.mText, null);
        paramAccessibilityRecord.mSourceWindowId = paramParcel.readInt();
        paramAccessibilityRecord.mSourceNodeId = paramParcel.readLong();
        if (paramParcel.readInt() == 1);
        for (boolean bool = true; ; bool = false)
        {
            paramAccessibilityRecord.mSealed = bool;
            return;
        }
    }

    private void writeAccessibilityRecordToParcel(AccessibilityRecord paramAccessibilityRecord, Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(paramAccessibilityRecord.mBooleanProperties);
        paramParcel.writeInt(paramAccessibilityRecord.mCurrentItemIndex);
        paramParcel.writeInt(paramAccessibilityRecord.mItemCount);
        paramParcel.writeInt(paramAccessibilityRecord.mFromIndex);
        paramParcel.writeInt(paramAccessibilityRecord.mToIndex);
        paramParcel.writeInt(paramAccessibilityRecord.mScrollX);
        paramParcel.writeInt(paramAccessibilityRecord.mScrollY);
        paramParcel.writeInt(paramAccessibilityRecord.mMaxScrollX);
        paramParcel.writeInt(paramAccessibilityRecord.mMaxScrollY);
        paramParcel.writeInt(paramAccessibilityRecord.mAddedCount);
        paramParcel.writeInt(paramAccessibilityRecord.mRemovedCount);
        TextUtils.writeToParcel(paramAccessibilityRecord.mClassName, paramParcel, paramInt);
        TextUtils.writeToParcel(paramAccessibilityRecord.mContentDescription, paramParcel, paramInt);
        TextUtils.writeToParcel(paramAccessibilityRecord.mBeforeText, paramParcel, paramInt);
        paramParcel.writeParcelable(paramAccessibilityRecord.mParcelableData, paramInt);
        paramParcel.writeList(paramAccessibilityRecord.mText);
        paramParcel.writeInt(paramAccessibilityRecord.mSourceWindowId);
        paramParcel.writeLong(paramAccessibilityRecord.mSourceNodeId);
        if (paramAccessibilityRecord.mSealed);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }

    public void appendRecord(AccessibilityRecord paramAccessibilityRecord)
    {
        enforceNotSealed();
        this.mRecords.add(paramAccessibilityRecord);
    }

    protected void clear()
    {
        super.clear();
        this.mEventType = 0;
        this.mMovementGranularity = 0;
        this.mAction = 0;
        this.mPackageName = null;
        this.mEventTime = 0L;
        while (!this.mRecords.isEmpty())
            ((AccessibilityRecord)this.mRecords.remove(0)).recycle();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getAction()
    {
        return this.mAction;
    }

    public long getEventTime()
    {
        return this.mEventTime;
    }

    public int getEventType()
    {
        return this.mEventType;
    }

    public int getMovementGranularity()
    {
        return this.mMovementGranularity;
    }

    public CharSequence getPackageName()
    {
        return this.mPackageName;
    }

    public AccessibilityRecord getRecord(int paramInt)
    {
        return (AccessibilityRecord)this.mRecords.get(paramInt);
    }

    public int getRecordCount()
    {
        return this.mRecords.size();
    }

    void init(AccessibilityEvent paramAccessibilityEvent)
    {
        super.init(paramAccessibilityEvent);
        this.mEventType = paramAccessibilityEvent.mEventType;
        this.mMovementGranularity = paramAccessibilityEvent.mMovementGranularity;
        this.mAction = paramAccessibilityEvent.mAction;
        this.mEventTime = paramAccessibilityEvent.mEventTime;
        this.mPackageName = paramAccessibilityEvent.mPackageName;
    }

    public void initFromParcel(Parcel paramParcel)
    {
        int i = 1;
        if (paramParcel.readInt() == i);
        while (true)
        {
            this.mSealed = i;
            this.mEventType = paramParcel.readInt();
            this.mMovementGranularity = paramParcel.readInt();
            this.mAction = paramParcel.readInt();
            this.mPackageName = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.mEventTime = paramParcel.readLong();
            this.mConnectionId = paramParcel.readInt();
            readAccessibilityRecordFromParcel(this, paramParcel);
            int j = paramParcel.readInt();
            for (int k = 0; k < j; k++)
            {
                AccessibilityRecord localAccessibilityRecord = AccessibilityRecord.obtain();
                readAccessibilityRecordFromParcel(localAccessibilityRecord, paramParcel);
                localAccessibilityRecord.mConnectionId = this.mConnectionId;
                this.mRecords.add(localAccessibilityRecord);
            }
            i = 0;
        }
    }

    public void recycle()
    {
        if (this.mIsInPool)
            throw new IllegalStateException("Event already recycled!");
        clear();
        synchronized (sPoolLock)
        {
            if (sPoolSize <= 10)
            {
                this.mNext = sPool;
                sPool = this;
                this.mIsInPool = true;
                sPoolSize = 1 + sPoolSize;
            }
            return;
        }
    }

    public void setAction(int paramInt)
    {
        enforceNotSealed();
        this.mAction = paramInt;
    }

    public void setEventTime(long paramLong)
    {
        enforceNotSealed();
        this.mEventTime = paramLong;
    }

    public void setEventType(int paramInt)
    {
        enforceNotSealed();
        this.mEventType = paramInt;
    }

    public void setMovementGranularity(int paramInt)
    {
        enforceNotSealed();
        this.mMovementGranularity = paramInt;
    }

    public void setPackageName(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mPackageName = paramCharSequence;
    }

    public void setSealed(boolean paramBoolean)
    {
        super.setSealed(paramBoolean);
        ArrayList localArrayList = this.mRecords;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            ((AccessibilityRecord)localArrayList.get(j)).setSealed(paramBoolean);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("EventType: ").append(eventTypeToString(this.mEventType));
        localStringBuilder.append("; EventTime: ").append(this.mEventTime);
        localStringBuilder.append("; PackageName: ").append(this.mPackageName);
        localStringBuilder.append("; MovementGranularity: ").append(this.mMovementGranularity);
        localStringBuilder.append("; Action: ").append(this.mAction);
        localStringBuilder.append(super.toString());
        localStringBuilder.append("; recordCount: ").append(getRecordCount());
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (isSealed());
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.mEventType);
            paramParcel.writeInt(this.mMovementGranularity);
            paramParcel.writeInt(this.mAction);
            TextUtils.writeToParcel(this.mPackageName, paramParcel, 0);
            paramParcel.writeLong(this.mEventTime);
            paramParcel.writeInt(this.mConnectionId);
            writeAccessibilityRecordToParcel(this, paramParcel, paramInt);
            int j = getRecordCount();
            paramParcel.writeInt(j);
            for (int k = 0; k < j; k++)
                writeAccessibilityRecordToParcel((AccessibilityRecord)this.mRecords.get(k), paramParcel, paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityEvent
 * JD-Core Version:        0.6.2
 */