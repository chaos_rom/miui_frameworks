package android.view.accessibility;

import android.os.Parcelable;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class AccessibilityRecord
{
    private static final int GET_SOURCE_PREFETCH_FLAGS = 7;
    private static final int MAX_POOL_SIZE = 10;
    private static final int PROPERTY_CHECKED = 1;
    private static final int PROPERTY_ENABLED = 2;
    private static final int PROPERTY_FULL_SCREEN = 128;
    private static final int PROPERTY_IMPORTANT_FOR_ACCESSIBILITY = 512;
    private static final int PROPERTY_PASSWORD = 4;
    private static final int PROPERTY_SCROLLABLE = 256;
    private static final int UNDEFINED = -1;
    private static AccessibilityRecord sPool;
    private static final Object sPoolLock = new Object();
    private static int sPoolSize;
    int mAddedCount = -1;
    CharSequence mBeforeText;
    int mBooleanProperties = 512;
    CharSequence mClassName;
    int mConnectionId = -1;
    CharSequence mContentDescription;
    int mCurrentItemIndex = -1;
    int mFromIndex = -1;
    private boolean mIsInPool;
    int mItemCount = -1;
    int mMaxScrollX = -1;
    int mMaxScrollY = -1;
    private AccessibilityRecord mNext;
    Parcelable mParcelableData;
    int mRemovedCount = -1;
    int mScrollX = -1;
    int mScrollY = -1;
    boolean mSealed;
    long mSourceNodeId = AccessibilityNodeInfo.makeNodeId(-1, -1);
    int mSourceWindowId = -1;
    final List<CharSequence> mText = new ArrayList();
    int mToIndex = -1;

    private boolean getBooleanProperty(int paramInt)
    {
        if ((paramInt & this.mBooleanProperties) == paramInt);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static AccessibilityRecord obtain()
    {
        AccessibilityRecord localAccessibilityRecord;
        synchronized (sPoolLock)
        {
            if (sPool != null)
            {
                localAccessibilityRecord = sPool;
                sPool = sPool.mNext;
                sPoolSize = -1 + sPoolSize;
                localAccessibilityRecord.mNext = null;
                localAccessibilityRecord.mIsInPool = false;
            }
            else
            {
                localAccessibilityRecord = new AccessibilityRecord();
            }
        }
        return localAccessibilityRecord;
    }

    public static AccessibilityRecord obtain(AccessibilityRecord paramAccessibilityRecord)
    {
        AccessibilityRecord localAccessibilityRecord = obtain();
        localAccessibilityRecord.init(paramAccessibilityRecord);
        return localAccessibilityRecord;
    }

    private void setBooleanProperty(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean);
        for (this.mBooleanProperties = (paramInt | this.mBooleanProperties); ; this.mBooleanProperties &= (paramInt ^ 0xFFFFFFFF))
            return;
    }

    void clear()
    {
        this.mSealed = false;
        this.mBooleanProperties = 512;
        this.mCurrentItemIndex = -1;
        this.mItemCount = -1;
        this.mFromIndex = -1;
        this.mToIndex = -1;
        this.mScrollX = -1;
        this.mScrollY = -1;
        this.mMaxScrollX = -1;
        this.mMaxScrollY = -1;
        this.mAddedCount = -1;
        this.mRemovedCount = -1;
        this.mClassName = null;
        this.mContentDescription = null;
        this.mBeforeText = null;
        this.mParcelableData = null;
        this.mText.clear();
        this.mSourceNodeId = AccessibilityNodeInfo.makeNodeId(-1, -1);
        this.mSourceWindowId = -1;
        this.mConnectionId = -1;
    }

    void enforceNotSealed()
    {
        if (isSealed())
            throw new IllegalStateException("Cannot perform this action on a sealed instance.");
    }

    void enforceSealed()
    {
        if (!isSealed())
            throw new IllegalStateException("Cannot perform this action on a not sealed instance.");
    }

    public int getAddedCount()
    {
        return this.mAddedCount;
    }

    public CharSequence getBeforeText()
    {
        return this.mBeforeText;
    }

    public CharSequence getClassName()
    {
        return this.mClassName;
    }

    public CharSequence getContentDescription()
    {
        return this.mContentDescription;
    }

    public int getCurrentItemIndex()
    {
        return this.mCurrentItemIndex;
    }

    public int getFromIndex()
    {
        return this.mFromIndex;
    }

    public int getItemCount()
    {
        return this.mItemCount;
    }

    public int getMaxScrollX()
    {
        return this.mMaxScrollX;
    }

    public int getMaxScrollY()
    {
        return this.mMaxScrollY;
    }

    public Parcelable getParcelableData()
    {
        return this.mParcelableData;
    }

    public int getRemovedCount()
    {
        return this.mRemovedCount;
    }

    public int getScrollX()
    {
        return this.mScrollX;
    }

    public int getScrollY()
    {
        return this.mScrollY;
    }

    public AccessibilityNodeInfo getSource()
    {
        enforceSealed();
        if ((this.mConnectionId == -1) || (this.mSourceWindowId == -1) || (AccessibilityNodeInfo.getAccessibilityViewId(this.mSourceNodeId) == -1));
        for (AccessibilityNodeInfo localAccessibilityNodeInfo = null; ; localAccessibilityNodeInfo = AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfoByAccessibilityId(this.mConnectionId, this.mSourceWindowId, this.mSourceNodeId, 7))
            return localAccessibilityNodeInfo;
    }

    public long getSourceNodeId()
    {
        return this.mSourceNodeId;
    }

    public List<CharSequence> getText()
    {
        return this.mText;
    }

    public int getToIndex()
    {
        return this.mToIndex;
    }

    public int getWindowId()
    {
        return this.mSourceWindowId;
    }

    void init(AccessibilityRecord paramAccessibilityRecord)
    {
        this.mSealed = paramAccessibilityRecord.mSealed;
        this.mBooleanProperties = paramAccessibilityRecord.mBooleanProperties;
        this.mCurrentItemIndex = paramAccessibilityRecord.mCurrentItemIndex;
        this.mItemCount = paramAccessibilityRecord.mItemCount;
        this.mFromIndex = paramAccessibilityRecord.mFromIndex;
        this.mToIndex = paramAccessibilityRecord.mToIndex;
        this.mScrollX = paramAccessibilityRecord.mScrollX;
        this.mScrollY = paramAccessibilityRecord.mScrollY;
        this.mMaxScrollX = paramAccessibilityRecord.mMaxScrollX;
        this.mMaxScrollY = paramAccessibilityRecord.mMaxScrollY;
        this.mAddedCount = paramAccessibilityRecord.mAddedCount;
        this.mRemovedCount = paramAccessibilityRecord.mRemovedCount;
        this.mClassName = paramAccessibilityRecord.mClassName;
        this.mContentDescription = paramAccessibilityRecord.mContentDescription;
        this.mBeforeText = paramAccessibilityRecord.mBeforeText;
        this.mParcelableData = paramAccessibilityRecord.mParcelableData;
        this.mText.addAll(paramAccessibilityRecord.mText);
        this.mSourceWindowId = paramAccessibilityRecord.mSourceWindowId;
        this.mSourceNodeId = paramAccessibilityRecord.mSourceNodeId;
        this.mConnectionId = paramAccessibilityRecord.mConnectionId;
    }

    public boolean isChecked()
    {
        return getBooleanProperty(1);
    }

    public boolean isEnabled()
    {
        return getBooleanProperty(2);
    }

    public boolean isFullScreen()
    {
        return getBooleanProperty(128);
    }

    public boolean isImportantForAccessibility()
    {
        return getBooleanProperty(512);
    }

    public boolean isPassword()
    {
        return getBooleanProperty(4);
    }

    public boolean isScrollable()
    {
        return getBooleanProperty(256);
    }

    boolean isSealed()
    {
        return this.mSealed;
    }

    public void recycle()
    {
        if (this.mIsInPool)
            throw new IllegalStateException("Record already recycled!");
        clear();
        synchronized (sPoolLock)
        {
            if (sPoolSize <= 10)
            {
                this.mNext = sPool;
                sPool = this;
                this.mIsInPool = true;
                sPoolSize = 1 + sPoolSize;
            }
            return;
        }
    }

    public void setAddedCount(int paramInt)
    {
        enforceNotSealed();
        this.mAddedCount = paramInt;
    }

    public void setBeforeText(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mBeforeText = paramCharSequence;
    }

    public void setChecked(boolean paramBoolean)
    {
        enforceNotSealed();
        setBooleanProperty(1, paramBoolean);
    }

    public void setClassName(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mClassName = paramCharSequence;
    }

    public void setConnectionId(int paramInt)
    {
        enforceNotSealed();
        this.mConnectionId = paramInt;
    }

    public void setContentDescription(CharSequence paramCharSequence)
    {
        enforceNotSealed();
        this.mContentDescription = paramCharSequence;
    }

    public void setCurrentItemIndex(int paramInt)
    {
        enforceNotSealed();
        this.mCurrentItemIndex = paramInt;
    }

    public void setEnabled(boolean paramBoolean)
    {
        enforceNotSealed();
        setBooleanProperty(2, paramBoolean);
    }

    public void setFromIndex(int paramInt)
    {
        enforceNotSealed();
        this.mFromIndex = paramInt;
    }

    public void setFullScreen(boolean paramBoolean)
    {
        enforceNotSealed();
        setBooleanProperty(128, paramBoolean);
    }

    public void setItemCount(int paramInt)
    {
        enforceNotSealed();
        this.mItemCount = paramInt;
    }

    public void setMaxScrollX(int paramInt)
    {
        enforceNotSealed();
        this.mMaxScrollX = paramInt;
    }

    public void setMaxScrollY(int paramInt)
    {
        enforceNotSealed();
        this.mMaxScrollY = paramInt;
    }

    public void setParcelableData(Parcelable paramParcelable)
    {
        enforceNotSealed();
        this.mParcelableData = paramParcelable;
    }

    public void setPassword(boolean paramBoolean)
    {
        enforceNotSealed();
        setBooleanProperty(4, paramBoolean);
    }

    public void setRemovedCount(int paramInt)
    {
        enforceNotSealed();
        this.mRemovedCount = paramInt;
    }

    public void setScrollX(int paramInt)
    {
        enforceNotSealed();
        this.mScrollX = paramInt;
    }

    public void setScrollY(int paramInt)
    {
        enforceNotSealed();
        this.mScrollY = paramInt;
    }

    public void setScrollable(boolean paramBoolean)
    {
        enforceNotSealed();
        setBooleanProperty(256, paramBoolean);
    }

    public void setSealed(boolean paramBoolean)
    {
        this.mSealed = paramBoolean;
    }

    public void setSource(View paramView)
    {
        setSource(paramView, -1);
    }

    public void setSource(View paramView, int paramInt)
    {
        enforceNotSealed();
        boolean bool;
        int i;
        if (paramInt == -1)
            if (paramView != null)
            {
                bool = paramView.isImportantForAccessibility();
                setBooleanProperty(512, bool);
                if (paramView == null)
                    break label74;
                i = paramView.getAccessibilityWindowId();
                label37: this.mSourceWindowId = i;
                if (paramView == null)
                    break label81;
            }
        label74: label81: for (int j = paramView.getAccessibilityViewId(); ; j = -1)
        {
            this.mSourceNodeId = AccessibilityNodeInfo.makeNodeId(j, paramInt);
            return;
            bool = true;
            break;
            bool = true;
            break;
            i = -1;
            break label37;
        }
    }

    public void setToIndex(int paramInt)
    {
        enforceNotSealed();
        this.mToIndex = paramInt;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(" [ ClassName: " + this.mClassName);
        localStringBuilder.append("; Text: " + this.mText);
        localStringBuilder.append("; ContentDescription: " + this.mContentDescription);
        localStringBuilder.append("; ItemCount: " + this.mItemCount);
        localStringBuilder.append("; CurrentItemIndex: " + this.mCurrentItemIndex);
        localStringBuilder.append("; IsEnabled: " + getBooleanProperty(2));
        localStringBuilder.append("; IsPassword: " + getBooleanProperty(4));
        localStringBuilder.append("; IsChecked: " + getBooleanProperty(1));
        localStringBuilder.append("; IsFullScreen: " + getBooleanProperty(128));
        localStringBuilder.append("; Scrollable: " + getBooleanProperty(256));
        localStringBuilder.append("; BeforeText: " + this.mBeforeText);
        localStringBuilder.append("; FromIndex: " + this.mFromIndex);
        localStringBuilder.append("; ToIndex: " + this.mToIndex);
        localStringBuilder.append("; ScrollX: " + this.mScrollX);
        localStringBuilder.append("; ScrollY: " + this.mScrollY);
        localStringBuilder.append("; MaxScrollX: " + this.mMaxScrollX);
        localStringBuilder.append("; MaxScrollY: " + this.mMaxScrollY);
        localStringBuilder.append("; AddedCount: " + this.mAddedCount);
        localStringBuilder.append("; RemovedCount: " + this.mRemovedCount);
        localStringBuilder.append("; ParcelableData: " + this.mParcelableData);
        localStringBuilder.append(" ]");
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityRecord
 * JD-Core Version:        0.6.2
 */