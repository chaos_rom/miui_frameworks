package android.view.accessibility;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IAccessibilityManagerClient extends IInterface
{
    public abstract void setState(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccessibilityManagerClient
    {
        private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityManagerClient";
        static final int TRANSACTION_setState = 1;

        public Stub()
        {
            attachInterface(this, "android.view.accessibility.IAccessibilityManagerClient");
        }

        public static IAccessibilityManagerClient asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.accessibility.IAccessibilityManagerClient");
                if ((localIInterface != null) && ((localIInterface instanceof IAccessibilityManagerClient)))
                    localObject = (IAccessibilityManagerClient)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.view.accessibility.IAccessibilityManagerClient");
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManagerClient");
                setState(paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IAccessibilityManagerClient
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.accessibility.IAccessibilityManagerClient";
            }

            public void setState(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManagerClient");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.IAccessibilityManagerClient
 * JD-Core Version:        0.6.2
 */