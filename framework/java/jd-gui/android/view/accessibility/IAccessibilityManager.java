package android.view.accessibility;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.IAccessibilityServiceClient;
import android.accessibilityservice.IAccessibilityServiceClient.Stub;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.IWindow;
import android.view.IWindow.Stub;
import java.util.ArrayList;
import java.util.List;

public abstract interface IAccessibilityManager extends IInterface
{
    public abstract int addAccessibilityInteractionConnection(IWindow paramIWindow, IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection)
        throws RemoteException;

    public abstract int addClient(IAccessibilityManagerClient paramIAccessibilityManagerClient)
        throws RemoteException;

    public abstract List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int paramInt)
        throws RemoteException;

    public abstract List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList()
        throws RemoteException;

    public abstract void interrupt()
        throws RemoteException;

    public abstract void registerUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient, AccessibilityServiceInfo paramAccessibilityServiceInfo)
        throws RemoteException;

    public abstract void removeAccessibilityInteractionConnection(IWindow paramIWindow)
        throws RemoteException;

    public abstract boolean sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
        throws RemoteException;

    public abstract void unregisterUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccessibilityManager
    {
        private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityManager";
        static final int TRANSACTION_addAccessibilityInteractionConnection = 6;
        static final int TRANSACTION_addClient = 1;
        static final int TRANSACTION_getEnabledAccessibilityServiceList = 4;
        static final int TRANSACTION_getInstalledAccessibilityServiceList = 3;
        static final int TRANSACTION_interrupt = 5;
        static final int TRANSACTION_registerUiTestAutomationService = 8;
        static final int TRANSACTION_removeAccessibilityInteractionConnection = 7;
        static final int TRANSACTION_sendAccessibilityEvent = 2;
        static final int TRANSACTION_unregisterUiTestAutomationService = 9;

        public Stub()
        {
            attachInterface(this, "android.view.accessibility.IAccessibilityManager");
        }

        public static IAccessibilityManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.accessibility.IAccessibilityManager");
                if ((localIInterface != null) && ((localIInterface instanceof IAccessibilityManager)))
                    localObject = (IAccessibilityManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.view.accessibility.IAccessibilityManager");
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                int n = addClient(IAccessibilityManagerClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                paramParcel2.writeInt(n);
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                AccessibilityEvent localAccessibilityEvent;
                if (paramParcel1.readInt() != 0)
                {
                    localAccessibilityEvent = (AccessibilityEvent)AccessibilityEvent.CREATOR.createFromParcel(paramParcel1);
                    label178: boolean bool = sendAccessibilityEvent(localAccessibilityEvent);
                    paramParcel2.writeNoException();
                    if (!bool)
                        break label214;
                }
                label214: int m;
                for (int k = i; ; m = 0)
                {
                    paramParcel2.writeInt(k);
                    break;
                    localAccessibilityEvent = null;
                    break label178;
                }
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                List localList2 = getInstalledAccessibilityServiceList();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList2);
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                List localList1 = getEnabledAccessibilityServiceList(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList1);
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                interrupt();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                int j = addAccessibilityInteractionConnection(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()), IAccessibilityInteractionConnection.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                removeAccessibilityInteractionConnection(IWindow.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                IAccessibilityServiceClient localIAccessibilityServiceClient = IAccessibilityServiceClient.Stub.asInterface(paramParcel1.readStrongBinder());
                if (paramParcel1.readInt() != 0);
                for (AccessibilityServiceInfo localAccessibilityServiceInfo = (AccessibilityServiceInfo)AccessibilityServiceInfo.CREATOR.createFromParcel(paramParcel1); ; localAccessibilityServiceInfo = null)
                {
                    registerUiTestAutomationService(localIAccessibilityServiceClient, localAccessibilityServiceInfo);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
                unregisterUiTestAutomationService(IAccessibilityServiceClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IAccessibilityManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public int addAccessibilityInteractionConnection(IWindow paramIWindow, IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection)
                throws RemoteException
            {
                IBinder localIBinder1 = null;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    if (paramIWindow != null)
                    {
                        localIBinder2 = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder2);
                        if (paramIAccessibilityInteractionConnection != null)
                            localIBinder1 = paramIAccessibilityInteractionConnection.asBinder();
                        localParcel1.writeStrongBinder(localIBinder1);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                    }
                    IBinder localIBinder2 = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int addClient(IAccessibilityManagerClient paramIAccessibilityManagerClient)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    if (paramIAccessibilityManagerClient != null)
                    {
                        localIBinder = paramIAccessibilityManagerClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(AccessibilityServiceInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(AccessibilityServiceInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.accessibility.IAccessibilityManager";
            }

            public void interrupt()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void registerUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient, AccessibilityServiceInfo paramAccessibilityServiceInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    IBinder localIBinder;
                    if (paramIAccessibilityServiceClient != null)
                    {
                        localIBinder = paramIAccessibilityServiceClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramAccessibilityServiceInfo == null)
                            break label85;
                        localParcel1.writeInt(1);
                        paramAccessibilityServiceInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localIBinder = null;
                        break;
                        label85: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeAccessibilityInteractionConnection(IWindow paramIWindow)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    if (paramIWindow != null)
                    {
                        localIBinder = paramIWindow.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                        if (paramAccessibilityEvent != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccessibilityEvent.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(2, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void unregisterUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
                    if (paramIAccessibilityServiceClient != null)
                    {
                        localIBinder = paramIAccessibilityServiceClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.IAccessibilityManager
 * JD-Core Version:        0.6.2
 */