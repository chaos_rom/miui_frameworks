package android.view.accessibility;

public abstract interface AccessibilityEventSource
{
    public abstract void sendAccessibilityEvent(int paramInt);

    public abstract void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityEventSource
 * JD-Core Version:        0.6.2
 */