package android.view.accessibility;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.List;

public abstract interface IAccessibilityInteractionConnectionCallback extends IInterface
{
    public abstract void setFindAccessibilityNodeInfoResult(AccessibilityNodeInfo paramAccessibilityNodeInfo, int paramInt)
        throws RemoteException;

    public abstract void setFindAccessibilityNodeInfosResult(List<AccessibilityNodeInfo> paramList, int paramInt)
        throws RemoteException;

    public abstract void setPerformAccessibilityActionResult(boolean paramBoolean, int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccessibilityInteractionConnectionCallback
    {
        private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityInteractionConnectionCallback";
        static final int TRANSACTION_setFindAccessibilityNodeInfoResult = 1;
        static final int TRANSACTION_setFindAccessibilityNodeInfosResult = 2;
        static final int TRANSACTION_setPerformAccessibilityActionResult = 3;

        public Stub()
        {
            attachInterface(this, "android.view.accessibility.IAccessibilityInteractionConnectionCallback");
        }

        public static IAccessibilityInteractionConnectionCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IAccessibilityInteractionConnectionCallback)))
                    localObject = (IAccessibilityInteractionConnectionCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                    continue;
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                    if (paramParcel1.readInt() != 0);
                    for (AccessibilityNodeInfo localAccessibilityNodeInfo = (AccessibilityNodeInfo)AccessibilityNodeInfo.CREATOR.createFromParcel(paramParcel1); ; localAccessibilityNodeInfo = null)
                    {
                        setFindAccessibilityNodeInfoResult(localAccessibilityNodeInfo, paramParcel1.readInt());
                        break;
                    }
                    paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                    setFindAccessibilityNodeInfosResult(paramParcel1.createTypedArrayList(AccessibilityNodeInfo.CREATOR), paramParcel1.readInt());
                }
            case 3:
            }
            paramParcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
            if (paramParcel1.readInt() != 0);
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                setPerformAccessibilityActionResult(bool2, paramParcel1.readInt());
                break;
            }
        }

        private static class Proxy
            implements IAccessibilityInteractionConnectionCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.accessibility.IAccessibilityInteractionConnectionCallback";
            }

            public void setFindAccessibilityNodeInfoResult(AccessibilityNodeInfo paramAccessibilityNodeInfo, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                    if (paramAccessibilityNodeInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramAccessibilityNodeInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setFindAccessibilityNodeInfosResult(List<AccessibilityNodeInfo> paramList, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                    localParcel.writeTypedList(paramList);
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setPerformAccessibilityActionResult(boolean paramBoolean, int paramInt)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.IAccessibilityInteractionConnectionCallback
 * JD-Core Version:        0.6.2
 */