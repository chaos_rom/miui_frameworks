package android.view.accessibility;

import android.os.Build;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseLongArray;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class AccessibilityNodeInfoCache
{
    private static final boolean CHECK_INTEGRITY = true;
    private static final boolean DEBUG = false;
    private static final boolean ENABLED = true;
    private static final String LOG_TAG = AccessibilityNodeInfoCache.class.getSimpleName();
    private final LongSparseArray<AccessibilityNodeInfo> mCacheImpl = new LongSparseArray();
    private final Object mLock = new Object();
    private int mWindowId;

    private void checkIntegrity()
    {
        while (true)
        {
            int i;
            HashSet localHashSet;
            LinkedList localLinkedList;
            AccessibilityNodeInfo localAccessibilityNodeInfo2;
            SparseLongArray localSparseLongArray;
            int m;
            int n;
            long l;
            AccessibilityNodeInfo localAccessibilityNodeInfo3;
            int j;
            int k;
            AccessibilityNodeInfo localAccessibilityNodeInfo1;
            synchronized (this.mLock)
            {
                if (this.mCacheImpl.size() > 0)
                {
                    Object localObject3 = (AccessibilityNodeInfo)this.mCacheImpl.valueAt(0);
                    Object localObject4 = localObject3;
                    if (localObject4 != null)
                    {
                        localObject3 = localObject4;
                        localObject4 = (AccessibilityNodeInfo)this.mCacheImpl.get(((AccessibilityNodeInfo)localObject4).getParentNodeId());
                        continue;
                    }
                    i = ((AccessibilityNodeInfo)localObject3).getWindowId();
                    localObject5 = null;
                    localObject6 = null;
                    localHashSet = new HashSet();
                    localLinkedList = new LinkedList();
                    localLinkedList.add(localObject3);
                    if (!localLinkedList.isEmpty())
                    {
                        localAccessibilityNodeInfo2 = (AccessibilityNodeInfo)localLinkedList.poll();
                        if (!localHashSet.add(localAccessibilityNodeInfo2))
                            Log.e(LOG_TAG, "Duplicate node: " + localAccessibilityNodeInfo2);
                    }
                }
            }
            n++;
            continue;
            Object localObject5 = localAccessibilityNodeInfo2;
            continue;
            Object localObject6 = localAccessibilityNodeInfo2;
            continue;
            k++;
        }
    }

    private void clearSubTreeLocked(long paramLong)
    {
        AccessibilityNodeInfo localAccessibilityNodeInfo = (AccessibilityNodeInfo)this.mCacheImpl.get(paramLong);
        if (localAccessibilityNodeInfo == null);
        while (true)
        {
            return;
            this.mCacheImpl.remove(paramLong);
            SparseLongArray localSparseLongArray = localAccessibilityNodeInfo.getChildNodeIds();
            int i = localSparseLongArray.size();
            for (int j = 0; j < i; j++)
                clearSubTreeLocked(localSparseLongArray.valueAt(j));
        }
    }

    private void clearSubtreeWithOldAccessibilityFocusLocked(long paramLong)
    {
        int i = this.mCacheImpl.size();
        for (int j = 0; ; j++)
            if (j < i)
            {
                AccessibilityNodeInfo localAccessibilityNodeInfo = (AccessibilityNodeInfo)this.mCacheImpl.valueAt(j);
                long l = localAccessibilityNodeInfo.getSourceNodeId();
                if ((l != paramLong) && (localAccessibilityNodeInfo.isAccessibilityFocused()))
                    clearSubTreeLocked(l);
            }
            else
            {
                return;
            }
    }

    private void clearSubtreeWithOldInputFocusLocked(long paramLong)
    {
        int i = this.mCacheImpl.size();
        for (int j = 0; ; j++)
            if (j < i)
            {
                AccessibilityNodeInfo localAccessibilityNodeInfo = (AccessibilityNodeInfo)this.mCacheImpl.valueAt(j);
                long l = localAccessibilityNodeInfo.getSourceNodeId();
                if ((l != paramLong) && (localAccessibilityNodeInfo.isFocused()))
                    clearSubTreeLocked(l);
            }
            else
            {
                return;
            }
    }

    public void add(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        while (true)
        {
            int j;
            synchronized (this.mLock)
            {
                long l1 = paramAccessibilityNodeInfo.getSourceNodeId();
                AccessibilityNodeInfo localAccessibilityNodeInfo1 = (AccessibilityNodeInfo)this.mCacheImpl.get(l1);
                if (localAccessibilityNodeInfo1 != null)
                {
                    SparseLongArray localSparseLongArray1 = localAccessibilityNodeInfo1.getChildNodeIds();
                    SparseLongArray localSparseLongArray2 = paramAccessibilityNodeInfo.getChildNodeIds();
                    int i = localSparseLongArray1.size();
                    j = 0;
                    if (j < i)
                    {
                        long l3 = localSparseLongArray1.valueAt(j);
                        if (localSparseLongArray2.indexOfValue(l3) >= 0)
                            break label138;
                        clearSubTreeLocked(l3);
                        break label138;
                    }
                    long l2 = localAccessibilityNodeInfo1.getParentNodeId();
                    if (paramAccessibilityNodeInfo.getParentNodeId() != l2)
                        clearSubTreeLocked(l2);
                }
                AccessibilityNodeInfo localAccessibilityNodeInfo2 = AccessibilityNodeInfo.obtain(paramAccessibilityNodeInfo);
                this.mCacheImpl.put(l1, localAccessibilityNodeInfo2);
                return;
            }
            label138: j++;
        }
    }

    public void clear()
    {
        synchronized (this.mLock)
        {
            int i = this.mCacheImpl.size();
            for (int j = 0; j < i; j++)
                ((AccessibilityNodeInfo)this.mCacheImpl.valueAt(j)).recycle();
            this.mCacheImpl.clear();
            return;
        }
    }

    public AccessibilityNodeInfo get(long paramLong)
    {
        synchronized (this.mLock)
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo = (AccessibilityNodeInfo)this.mCacheImpl.get(paramLong);
            if (localAccessibilityNodeInfo != null)
                localAccessibilityNodeInfo = AccessibilityNodeInfo.obtain(localAccessibilityNodeInfo);
            return localAccessibilityNodeInfo;
        }
    }

    public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        int i = paramAccessibilityEvent.getEventType();
        switch (i)
        {
        default:
        case 32:
        case 128:
        case 256:
        case 4:
        case 8:
        case 16:
        case 8192:
        case 32768:
        case 2048:
        case 4096:
        }
        while (true)
        {
            if (Build.IS_DEBUGGABLE)
                checkIntegrity();
            return;
            this.mWindowId = paramAccessibilityEvent.getWindowId();
            clear();
            continue;
            int j = paramAccessibilityEvent.getWindowId();
            if (this.mWindowId == j)
                continue;
            this.mWindowId = j;
            clear();
            continue;
            synchronized (this.mLock)
            {
                long l = paramAccessibilityEvent.getSourceNodeId();
                clearSubTreeLocked(l);
                if (i == 8)
                    clearSubtreeWithOldInputFocusLocked(l);
                if (i == 32768)
                    clearSubtreeWithOldAccessibilityFocusLocked(l);
            }
            synchronized (this.mLock)
            {
                clearSubTreeLocked(paramAccessibilityEvent.getSourceNodeId());
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.accessibility.AccessibilityNodeInfoCache
 * JD-Core Version:        0.6.2
 */