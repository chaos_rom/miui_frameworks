package android.view;

import android.app.Application;
import android.app.LoadedApk;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemProperties;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R.styleable;

public abstract class Window
{
    protected static final int DEFAULT_FEATURES = 65;
    public static final int FEATURE_ACTION_BAR = 8;
    public static final int FEATURE_ACTION_BAR_OVERLAY = 9;
    public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
    public static final int FEATURE_CONTEXT_MENU = 6;
    public static final int FEATURE_CUSTOM_TITLE = 7;
    public static final int FEATURE_INDETERMINATE_PROGRESS = 5;
    public static final int FEATURE_LEFT_ICON = 3;
    public static final int FEATURE_NO_TITLE = 1;
    public static final int FEATURE_OPTIONS_PANEL = 0;
    public static final int FEATURE_PROGRESS = 2;
    public static final int FEATURE_RIGHT_ICON = 4;
    public static final int ID_ANDROID_CONTENT = 16908290;
    public static final int PROGRESS_END = 10000;
    public static final int PROGRESS_INDETERMINATE_OFF = -4;
    public static final int PROGRESS_INDETERMINATE_ON = -3;
    public static final int PROGRESS_SECONDARY_END = 30000;
    public static final int PROGRESS_SECONDARY_START = 20000;
    public static final int PROGRESS_START = 0;
    public static final int PROGRESS_VISIBILITY_OFF = -2;
    public static final int PROGRESS_VISIBILITY_ON = -1;
    private Window mActiveChild;
    private String mAppName;
    private IBinder mAppToken;
    private Callback mCallback;
    private boolean mCloseOnTouchOutside = false;
    private Window mContainer;
    private final Context mContext;
    private int mDefaultWindowFormat = -1;
    private boolean mDestroyed;
    private int mFeatures = 65;
    private int mForcedWindowFlags = 0;
    private boolean mHasChildren = false;
    private boolean mHasSoftInputMode = false;
    private boolean mHaveDimAmount = false;
    private boolean mHaveWindowFormat = false;
    private boolean mIsActive = false;
    private int mLocalFeatures = 65;
    private boolean mSetCloseOnTouchOutside = false;
    private final WindowManager.LayoutParams mWindowAttributes = new WindowManager.LayoutParams();
    private WindowManager mWindowManager;
    private TypedArray mWindowStyle;

    public Window(Context paramContext)
    {
        this.mContext = paramContext;
    }

    static CompatibilityInfoHolder getCompatInfo(Context paramContext)
    {
        Application localApplication = (Application)paramContext.getApplicationContext();
        if (localApplication != null);
        for (CompatibilityInfoHolder localCompatibilityInfoHolder = localApplication.mLoadedApk.mCompatibilityInfo; ; localCompatibilityInfoHolder = new CompatibilityInfoHolder())
            return localCompatibilityInfoHolder;
    }

    private boolean isOutOfBounds(Context paramContext, MotionEvent paramMotionEvent)
    {
        int i = (int)paramMotionEvent.getX();
        int j = (int)paramMotionEvent.getY();
        int k = ViewConfiguration.get(paramContext).getScaledWindowTouchSlop();
        View localView = getDecorView();
        if ((i < -k) || (j < -k) || (i > k + localView.getWidth()) || (j > k + localView.getHeight()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams);

    public void addFlags(int paramInt)
    {
        setFlags(paramInt, paramInt);
    }

    public abstract void alwaysReadCloseOnTouchAttr();

    public void clearFlags(int paramInt)
    {
        setFlags(0, paramInt);
    }

    public abstract void closeAllPanels();

    public abstract void closePanel(int paramInt);

    public final void destroy()
    {
        this.mDestroyed = true;
    }

    public View findViewById(int paramInt)
    {
        return getDecorView().findViewById(paramInt);
    }

    public final WindowManager.LayoutParams getAttributes()
    {
        return this.mWindowAttributes;
    }

    public final Callback getCallback()
    {
        return this.mCallback;
    }

    public final Window getContainer()
    {
        return this.mContainer;
    }

    public final Context getContext()
    {
        return this.mContext;
    }

    public abstract View getCurrentFocus();

    public abstract View getDecorView();

    protected final int getFeatures()
    {
        return this.mFeatures;
    }

    protected final int getForcedWindowFlags()
    {
        return this.mForcedWindowFlags;
    }

    public abstract LayoutInflater getLayoutInflater();

    protected final int getLocalFeatures()
    {
        return this.mLocalFeatures;
    }

    public abstract int getVolumeControlStream();

    public WindowManager getWindowManager()
    {
        return this.mWindowManager;
    }

    public final TypedArray getWindowStyle()
    {
        try
        {
            if (this.mWindowStyle == null)
                this.mWindowStyle = this.mContext.obtainStyledAttributes(R.styleable.Window);
            TypedArray localTypedArray = this.mWindowStyle;
            return localTypedArray;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final boolean hasChildren()
    {
        return this.mHasChildren;
    }

    public boolean hasFeature(int paramInt)
    {
        int i = 1;
        if ((getFeatures() & i << paramInt) != 0);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    protected final boolean hasSoftInputMode()
    {
        return this.mHasSoftInputMode;
    }

    protected boolean haveDimAmount()
    {
        return this.mHaveDimAmount;
    }

    public abstract void invalidatePanelMenu(int paramInt);

    public final boolean isActive()
    {
        return this.mIsActive;
    }

    public final boolean isDestroyed()
    {
        return this.mDestroyed;
    }

    public abstract boolean isFloating();

    public abstract boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent);

    public final void makeActive()
    {
        if (this.mContainer != null)
        {
            if (this.mContainer.mActiveChild != null)
                this.mContainer.mActiveChild.mIsActive = false;
            this.mContainer.mActiveChild = this;
        }
        this.mIsActive = true;
        onActive();
    }

    protected abstract void onActive();

    public abstract void onConfigurationChanged(Configuration paramConfiguration);

    public abstract void openPanel(int paramInt, KeyEvent paramKeyEvent);

    public abstract View peekDecorView();

    public abstract boolean performContextMenuIdentifierAction(int paramInt1, int paramInt2);

    public abstract boolean performPanelIdentifierAction(int paramInt1, int paramInt2, int paramInt3);

    public abstract boolean performPanelShortcut(int paramInt1, int paramInt2, KeyEvent paramKeyEvent, int paramInt3);

    protected void removeFeature(int paramInt)
    {
        int i = 1 << paramInt;
        this.mFeatures &= (i ^ 0xFFFFFFFF);
        int j = this.mLocalFeatures;
        if (this.mContainer != null)
            i &= (0xFFFFFFFF ^ this.mContainer.mFeatures);
        this.mLocalFeatures = (j & (i ^ 0xFFFFFFFF));
    }

    public boolean requestFeature(int paramInt)
    {
        int i = 1 << paramInt;
        this.mFeatures = (i | this.mFeatures);
        int j = this.mLocalFeatures;
        int k;
        if (this.mContainer != null)
        {
            k = i & (0xFFFFFFFF ^ this.mContainer.mFeatures);
            this.mLocalFeatures = (k | j);
            if ((i & this.mFeatures) == 0)
                break label69;
        }
        label69: for (boolean bool = true; ; bool = false)
        {
            return bool;
            k = i;
            break;
        }
    }

    public abstract void restoreHierarchyState(Bundle paramBundle);

    public abstract Bundle saveHierarchyState();

    public void setAttributes(WindowManager.LayoutParams paramLayoutParams)
    {
        this.mWindowAttributes.copyFrom(paramLayoutParams);
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(this.mWindowAttributes);
    }

    public abstract void setBackgroundDrawable(Drawable paramDrawable);

    public void setBackgroundDrawableResource(int paramInt)
    {
        setBackgroundDrawable(this.mContext.getResources().getDrawable(paramInt));
    }

    public void setCallback(Callback paramCallback)
    {
        this.mCallback = paramCallback;
    }

    public abstract void setChildDrawable(int paramInt, Drawable paramDrawable);

    public abstract void setChildInt(int paramInt1, int paramInt2);

    public void setCloseOnTouchOutside(boolean paramBoolean)
    {
        this.mCloseOnTouchOutside = paramBoolean;
        this.mSetCloseOnTouchOutside = true;
    }

    public void setCloseOnTouchOutsideIfNotSet(boolean paramBoolean)
    {
        if (!this.mSetCloseOnTouchOutside)
        {
            this.mCloseOnTouchOutside = paramBoolean;
            this.mSetCloseOnTouchOutside = true;
        }
    }

    public void setContainer(Window paramWindow)
    {
        this.mContainer = paramWindow;
        if (paramWindow != null)
        {
            this.mFeatures = (0x2 | this.mFeatures);
            this.mLocalFeatures = (0x2 | this.mLocalFeatures);
            paramWindow.mHasChildren = true;
        }
    }

    public abstract void setContentView(int paramInt);

    public abstract void setContentView(View paramView);

    public abstract void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams);

    protected void setDefaultWindowFormat(int paramInt)
    {
        this.mDefaultWindowFormat = paramInt;
        if (!this.mHaveWindowFormat)
        {
            WindowManager.LayoutParams localLayoutParams = getAttributes();
            localLayoutParams.format = paramInt;
            if (this.mCallback != null)
                this.mCallback.onWindowAttributesChanged(localLayoutParams);
        }
    }

    public void setDimAmount(float paramFloat)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        localLayoutParams.dimAmount = paramFloat;
        this.mHaveDimAmount = true;
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(localLayoutParams);
    }

    public abstract void setFeatureDrawable(int paramInt, Drawable paramDrawable);

    public abstract void setFeatureDrawableAlpha(int paramInt1, int paramInt2);

    public abstract void setFeatureDrawableResource(int paramInt1, int paramInt2);

    public abstract void setFeatureDrawableUri(int paramInt, Uri paramUri);

    public abstract void setFeatureInt(int paramInt1, int paramInt2);

    public void setFlags(int paramInt1, int paramInt2)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        localLayoutParams.flags = (localLayoutParams.flags & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2);
        if ((0x8000000 & paramInt2) != 0)
            localLayoutParams.privateFlags = (0x8 | localLayoutParams.privateFlags);
        this.mForcedWindowFlags = (paramInt2 | this.mForcedWindowFlags);
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(localLayoutParams);
    }

    public void setFormat(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        if (paramInt != 0)
            localLayoutParams.format = paramInt;
        for (this.mHaveWindowFormat = true; ; this.mHaveWindowFormat = false)
        {
            if (this.mCallback != null)
                this.mCallback.onWindowAttributesChanged(localLayoutParams);
            return;
            localLayoutParams.format = this.mDefaultWindowFormat;
        }
    }

    public void setGravity(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        localLayoutParams.gravity = paramInt;
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(localLayoutParams);
    }

    public void setLayout(int paramInt1, int paramInt2)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        localLayoutParams.width = paramInt1;
        localLayoutParams.height = paramInt2;
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(localLayoutParams);
    }

    public void setSoftInputMode(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        if (paramInt != 0)
            localLayoutParams.softInputMode = paramInt;
        for (this.mHasSoftInputMode = true; ; this.mHasSoftInputMode = false)
        {
            if (this.mCallback != null)
                this.mCallback.onWindowAttributesChanged(localLayoutParams);
            return;
        }
    }

    public abstract void setTitle(CharSequence paramCharSequence);

    public abstract void setTitleColor(int paramInt);

    public void setType(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        localLayoutParams.type = paramInt;
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(localLayoutParams);
    }

    public void setUiOptions(int paramInt)
    {
    }

    public void setUiOptions(int paramInt1, int paramInt2)
    {
    }

    public abstract void setVolumeControlStream(int paramInt);

    public void setWindowAnimations(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getAttributes();
        localLayoutParams.windowAnimations = paramInt;
        if (this.mCallback != null)
            this.mCallback.onWindowAttributesChanged(localLayoutParams);
    }

    public void setWindowManager(WindowManager paramWindowManager, IBinder paramIBinder, String paramString)
    {
        setWindowManager(paramWindowManager, paramIBinder, paramString, false);
    }

    public void setWindowManager(WindowManager paramWindowManager, IBinder paramIBinder, String paramString, boolean paramBoolean)
    {
        this.mAppToken = paramIBinder;
        this.mAppName = paramString;
        if (paramWindowManager == null)
            paramWindowManager = WindowManagerImpl.getDefault();
        this.mWindowManager = new LocalWindowManager(paramWindowManager, paramBoolean);
    }

    public boolean shouldCloseOnTouch(Context paramContext, MotionEvent paramMotionEvent)
    {
        if ((this.mCloseOnTouchOutside) && (paramMotionEvent.getAction() == 0) && (isOutOfBounds(paramContext, paramMotionEvent)) && (peekDecorView() != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract boolean superDispatchGenericMotionEvent(MotionEvent paramMotionEvent);

    public abstract boolean superDispatchKeyEvent(KeyEvent paramKeyEvent);

    public abstract boolean superDispatchKeyShortcutEvent(KeyEvent paramKeyEvent);

    public abstract boolean superDispatchTouchEvent(MotionEvent paramMotionEvent);

    public abstract boolean superDispatchTrackballEvent(MotionEvent paramMotionEvent);

    public abstract void takeInputQueue(InputQueue.Callback paramCallback);

    public abstract void takeKeyEvents(boolean paramBoolean);

    public abstract void takeSurface(SurfaceHolder.Callback2 paramCallback2);

    public abstract void togglePanel(int paramInt, KeyEvent paramKeyEvent);

    private class LocalWindowManager extends WindowManagerImpl.CompatModeWrapper
    {
        private static final String PROPERTY_HARDWARE_UI = "persist.sys.ui.hw";
        private final boolean mHardwareAccelerated;

        LocalWindowManager(WindowManager paramBoolean, boolean arg3)
        {
            super(Window.getCompatInfo(Window.this.mContext));
            int i;
            if ((i != 0) || (SystemProperties.getBoolean("persist.sys.ui.hw", false)))
                bool = true;
            this.mHardwareAccelerated = bool;
        }

        public final void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
        {
            WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)paramLayoutParams;
            CharSequence localCharSequence = localLayoutParams.getTitle();
            if ((localLayoutParams.type >= 1000) && (localLayoutParams.type <= 1999))
            {
                if (localLayoutParams.token == null)
                {
                    View localView = Window.this.peekDecorView();
                    if (localView != null)
                        localLayoutParams.token = localView.getWindowToken();
                }
                String str;
                if ((localCharSequence == null) || (localCharSequence.length() == 0))
                {
                    if (localLayoutParams.type != 1001)
                        break label184;
                    str = "Media";
                }
                while (true)
                {
                    if (Window.this.mAppName != null)
                        str = str + ":" + Window.this.mAppName;
                    localLayoutParams.setTitle(str);
                    if (localLayoutParams.packageName == null)
                        localLayoutParams.packageName = Window.this.mContext.getPackageName();
                    if (this.mHardwareAccelerated)
                        localLayoutParams.flags = (0x1000000 | localLayoutParams.flags);
                    super.addView(paramView, paramLayoutParams);
                    return;
                    label184: if (localLayoutParams.type == 1004)
                        str = "MediaOvr";
                    else if (localLayoutParams.type == 1000)
                        str = "Panel";
                    else if (localLayoutParams.type == 1002)
                        str = "SubPanel";
                    else if (localLayoutParams.type == 1003)
                        str = "AtchDlg";
                    else
                        str = Integer.toString(localLayoutParams.type);
                }
            }
            if (localLayoutParams.token == null)
                if (Window.this.mContainer != null)
                    break label335;
            label335: for (IBinder localIBinder = Window.this.mAppToken; ; localIBinder = Window.access$200(Window.this).mAppToken)
            {
                localLayoutParams.token = localIBinder;
                if (((localCharSequence != null) && (localCharSequence.length() != 0)) || (Window.this.mAppName == null))
                    break;
                localLayoutParams.setTitle(Window.this.mAppName);
                break;
            }
        }

        public boolean isHardwareAccelerated()
        {
            return this.mHardwareAccelerated;
        }
    }

    public static abstract interface Callback
    {
        public abstract boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent);

        public abstract boolean dispatchKeyEvent(KeyEvent paramKeyEvent);

        public abstract boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent);

        public abstract boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent);

        public abstract boolean dispatchTouchEvent(MotionEvent paramMotionEvent);

        public abstract boolean dispatchTrackballEvent(MotionEvent paramMotionEvent);

        public abstract void onActionModeFinished(ActionMode paramActionMode);

        public abstract void onActionModeStarted(ActionMode paramActionMode);

        public abstract void onAttachedToWindow();

        public abstract void onContentChanged();

        public abstract boolean onCreatePanelMenu(int paramInt, Menu paramMenu);

        public abstract View onCreatePanelView(int paramInt);

        public abstract void onDetachedFromWindow();

        public abstract boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem);

        public abstract boolean onMenuOpened(int paramInt, Menu paramMenu);

        public abstract void onPanelClosed(int paramInt, Menu paramMenu);

        public abstract boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu);

        public abstract boolean onSearchRequested();

        public abstract void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams);

        public abstract void onWindowFocusChanged(boolean paramBoolean);

        public abstract ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.Window
 * JD-Core Version:        0.6.2
 */