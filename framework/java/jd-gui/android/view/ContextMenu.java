package android.view;

import android.graphics.drawable.Drawable;

public abstract interface ContextMenu extends Menu
{
    public abstract void clearHeader();

    public abstract ContextMenu setHeaderIcon(int paramInt);

    public abstract ContextMenu setHeaderIcon(Drawable paramDrawable);

    public abstract ContextMenu setHeaderTitle(int paramInt);

    public abstract ContextMenu setHeaderTitle(CharSequence paramCharSequence);

    public abstract ContextMenu setHeaderView(View paramView);

    public static abstract interface ContextMenuInfo
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ContextMenu
 * JD-Core Version:        0.6.2
 */