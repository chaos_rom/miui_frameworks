package android.view;

import android.content.res.CompatibilityInfo;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Slog;

public class Display
{
    static final boolean DEBUG_DISPLAY_SIZE = false;
    public static final int DEFAULT_DISPLAY = 0;
    static final String TAG = "Display";
    private static boolean sInitialized = false;
    private static final Object sStaticInit = new Object();
    private static IWindowManager sWindowManager;
    private final CompatibilityInfoHolder mCompatibilityInfo;
    float mDensity;
    private final int mDisplay;
    float mDpiX;
    float mDpiY;
    private float mLastGetTime;
    private int mPixelFormat;
    private float mRefreshRate;
    private final DisplayMetrics mTmpMetrics = new DisplayMetrics();
    private final Point mTmpPoint = new Point();

    Display(int paramInt, CompatibilityInfoHolder paramCompatibilityInfoHolder)
    {
        while (true)
        {
            synchronized (sStaticInit)
            {
                if (!sInitialized)
                {
                    nativeClassInit();
                    sInitialized = true;
                }
                if (paramCompatibilityInfoHolder != null)
                {
                    this.mCompatibilityInfo = paramCompatibilityInfoHolder;
                    this.mDisplay = paramInt;
                    init(paramInt);
                    return;
                }
            }
            paramCompatibilityInfoHolder = new CompatibilityInfoHolder();
        }
    }

    public static Display createCompatibleDisplay(int paramInt, CompatibilityInfoHolder paramCompatibilityInfoHolder)
    {
        return new Display(paramInt, paramCompatibilityInfoHolder);
    }

    static native int getDisplayCount();

    private native int getRawHeightNative();

    private native int getRawWidthNative();

    private void getSizeInternal(Point paramPoint, boolean paramBoolean)
    {
        try
        {
            IWindowManager localIWindowManager = getWindowManager();
            if (localIWindowManager != null)
            {
                localIWindowManager.getDisplaySize(paramPoint);
                if (!paramBoolean)
                    return;
                CompatibilityInfo localCompatibilityInfo = this.mCompatibilityInfo.getIfNeeded();
                if (localCompatibilityInfo == null)
                    return;
                synchronized (this.mTmpMetrics)
                {
                    this.mTmpMetrics.noncompatWidthPixels = paramPoint.x;
                    this.mTmpMetrics.noncompatHeightPixels = paramPoint.y;
                    this.mTmpMetrics.density = this.mDensity;
                    localCompatibilityInfo.applyToDisplayMetrics(this.mTmpMetrics);
                    paramPoint.x = this.mTmpMetrics.widthPixels;
                    paramPoint.y = this.mTmpMetrics.heightPixels;
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            Slog.w("Display", "Unable to get display size", localRemoteException);
            return;
            paramPoint.x = getRawWidth();
            paramPoint.y = getRawHeight();
        }
    }

    static IWindowManager getWindowManager()
    {
        synchronized (sStaticInit)
        {
            if (sWindowManager == null)
                sWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
            IWindowManager localIWindowManager = sWindowManager;
            return localIWindowManager;
        }
    }

    private native void init(int paramInt);

    private static native void nativeClassInit();

    public void getCurrentSizeRange(Point paramPoint1, Point paramPoint2)
    {
        try
        {
            getWindowManager().getCurrentSizeRange(paramPoint1, paramPoint2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.w("Display", "Unable to get display size range", localRemoteException);
                paramPoint1.x = 0;
                paramPoint1.y = 0;
                paramPoint2.x = 0;
                paramPoint2.y = 0;
            }
        }
    }

    public int getDisplayId()
    {
        return this.mDisplay;
    }

    public int getExternalRotation()
    {
        return 0;
    }

    @Deprecated
    public int getHeight()
    {
        synchronized (this.mTmpPoint)
        {
            long l = SystemClock.uptimeMillis();
            if ((float)l > 20.0F + this.mLastGetTime)
            {
                getSizeInternal(this.mTmpPoint, true);
                this.mLastGetTime = ((float)l);
            }
            int i = this.mTmpPoint.y;
            return i;
        }
    }

    public int getMaximumSizeDimension()
    {
        try
        {
            int j = getWindowManager().getMaximumSizeDimension();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.w("Display", "Unable to get display maximum size dimension", localRemoteException);
                int i = 0;
            }
        }
    }

    public void getMetrics(DisplayMetrics paramDisplayMetrics)
    {
        synchronized (this.mTmpPoint)
        {
            getSizeInternal(this.mTmpPoint, false);
            getMetricsWithSize(paramDisplayMetrics, this.mTmpPoint.x, this.mTmpPoint.y);
            CompatibilityInfo localCompatibilityInfo = this.mCompatibilityInfo.getIfNeeded();
            if (localCompatibilityInfo != null)
                localCompatibilityInfo.applyToDisplayMetrics(paramDisplayMetrics);
            return;
        }
    }

    public void getMetricsWithSize(DisplayMetrics paramDisplayMetrics, int paramInt1, int paramInt2)
    {
        paramDisplayMetrics.densityDpi = ((int)(0.5F + 160.0F * this.mDensity));
        paramDisplayMetrics.widthPixels = paramInt1;
        paramDisplayMetrics.noncompatWidthPixels = paramInt1;
        paramDisplayMetrics.heightPixels = paramInt2;
        paramDisplayMetrics.noncompatHeightPixels = paramInt2;
        float f1 = this.mDensity;
        paramDisplayMetrics.noncompatDensity = f1;
        paramDisplayMetrics.density = f1;
        float f2 = paramDisplayMetrics.density;
        paramDisplayMetrics.noncompatScaledDensity = f2;
        paramDisplayMetrics.scaledDensity = f2;
        float f3 = this.mDpiX;
        paramDisplayMetrics.noncompatXdpi = f3;
        paramDisplayMetrics.xdpi = f3;
        float f4 = this.mDpiY;
        paramDisplayMetrics.noncompatYdpi = f4;
        paramDisplayMetrics.ydpi = f4;
    }

    @Deprecated
    public native int getOrientation();

    public int getPixelFormat()
    {
        return this.mPixelFormat;
    }

    public int getRawExternalHeight()
    {
        return 720;
    }

    public int getRawExternalWidth()
    {
        return 1280;
    }

    public int getRawHeight()
    {
        return getRawHeightNative();
    }

    public int getRawWidth()
    {
        return getRawWidthNative();
    }

    public void getRealMetrics(DisplayMetrics paramDisplayMetrics)
    {
        synchronized (this.mTmpPoint)
        {
            getRealSize(this.mTmpPoint);
            getMetricsWithSize(paramDisplayMetrics, this.mTmpPoint.x, this.mTmpPoint.y);
            return;
        }
    }

    public void getRealSize(Point paramPoint)
    {
        try
        {
            IWindowManager localIWindowManager = getWindowManager();
            if (localIWindowManager != null)
            {
                localIWindowManager.getRealDisplaySize(paramPoint);
            }
            else
            {
                paramPoint.x = getRawWidth();
                paramPoint.y = getRawHeight();
            }
        }
        catch (RemoteException localRemoteException)
        {
            Slog.w("Display", "Unable to get real display size", localRemoteException);
        }
    }

    public void getRectSize(Rect paramRect)
    {
        synchronized (this.mTmpPoint)
        {
            getSizeInternal(this.mTmpPoint, true);
            paramRect.set(0, 0, this.mTmpPoint.x, this.mTmpPoint.y);
            return;
        }
    }

    public float getRefreshRate()
    {
        return this.mRefreshRate;
    }

    public int getRotation()
    {
        return getOrientation();
    }

    public void getSize(Point paramPoint)
    {
        getSizeInternal(paramPoint, true);
    }

    @Deprecated
    public int getWidth()
    {
        synchronized (this.mTmpPoint)
        {
            long l = SystemClock.uptimeMillis();
            if ((float)l > 20.0F + this.mLastGetTime)
            {
                getSizeInternal(this.mTmpPoint, true);
                this.mLastGetTime = ((float)l);
            }
            int i = this.mTmpPoint.x;
            return i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.Display
 * JD-Core Version:        0.6.2
 */