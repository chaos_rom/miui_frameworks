package android.view;

import android.content.Context;
import android.content.res.CompatibilityInfo.Translator;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Region.Op;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.view.BaseIWindow;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class SurfaceView extends View
{
    private static final boolean DEBUG = false;
    static final int GET_NEW_SURFACE_MSG = 2;
    static final int KEEP_SCREEN_ON_MSG = 1;
    private static final String TAG = "SurfaceView";
    static final int UPDATE_WINDOW_MSG = 3;
    final ArrayList<SurfaceHolder.Callback> mCallbacks = new ArrayList();
    final Configuration mConfiguration = new Configuration();
    final Rect mContentInsets = new Rect();
    private final ViewTreeObserver.OnPreDrawListener mDrawListener = new ViewTreeObserver.OnPreDrawListener()
    {
        public boolean onPreDraw()
        {
            SurfaceView localSurfaceView = SurfaceView.this;
            if ((SurfaceView.this.getWidth() > 0) && (SurfaceView.this.getHeight() > 0));
            for (boolean bool = true; ; bool = false)
            {
                localSurfaceView.mHaveFrame = bool;
                SurfaceView.this.updateWindow(false, false);
                return true;
            }
        }
    };
    boolean mDrawingStopped = true;
    int mFormat = -1;
    private boolean mGlobalListenersAdded;
    final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            boolean bool = false;
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                SurfaceView localSurfaceView = SurfaceView.this;
                if (paramAnonymousMessage.arg1 != 0)
                    bool = true;
                localSurfaceView.setKeepScreenOn(bool);
                continue;
                SurfaceView.this.handleGetNewSurface();
                continue;
                SurfaceView.this.updateWindow(false, false);
            }
        }
    };
    boolean mHaveFrame = false;
    int mHeight = -1;
    boolean mIsCreating = false;
    long mLastLockTime = 0L;
    int mLastSurfaceHeight = -1;
    int mLastSurfaceWidth = -1;
    final WindowManager.LayoutParams mLayout = new WindowManager.LayoutParams();
    int mLeft = -1;
    final int[] mLocation = new int[2];
    final Surface mNewSurface = new Surface();
    boolean mReportDrawNeeded;
    int mRequestedFormat = 4;
    int mRequestedHeight = -1;
    boolean mRequestedVisible = false;
    int mRequestedWidth = -1;
    final ViewTreeObserver.OnScrollChangedListener mScrollChangedListener = new ViewTreeObserver.OnScrollChangedListener()
    {
        public void onScrollChanged()
        {
            SurfaceView.this.updateWindow(false, false);
        }
    };
    IWindowSession mSession;
    final Surface mSurface = new Surface();
    boolean mSurfaceCreated = false;
    final Rect mSurfaceFrame = new Rect();
    private SurfaceHolder mSurfaceHolder = new SurfaceHolder()
    {
        private static final String LOG_TAG = "SurfaceHolder";

        private final Canvas internalLockCanvas(Rect paramAnonymousRect)
        {
            SurfaceView.this.mSurfaceLock.lock();
            Object localObject = null;
            if ((!SurfaceView.this.mDrawingStopped) && (SurfaceView.this.mWindow != null))
                if (paramAnonymousRect == null)
                {
                    if (SurfaceView.this.mTmpDirty == null)
                        SurfaceView.this.mTmpDirty = new Rect();
                    SurfaceView.this.mTmpDirty.set(SurfaceView.this.mSurfaceFrame);
                    paramAnonymousRect = SurfaceView.this.mTmpDirty;
                }
            while (true)
            {
                long l1;
                long l2;
                try
                {
                    Canvas localCanvas = SurfaceView.this.mSurface.lockCanvas(paramAnonymousRect);
                    localObject = localCanvas;
                    if (localObject != null)
                    {
                        SurfaceView.this.mLastLockTime = SystemClock.uptimeMillis();
                        return localObject;
                    }
                }
                catch (Exception localException)
                {
                    Log.e("SurfaceHolder", "Exception locking surface", localException);
                    continue;
                    l1 = SystemClock.uptimeMillis();
                    l2 = 100L + SurfaceView.this.mLastLockTime;
                    if (l2 <= l1)
                        break label171;
                }
                long l3 = l2 - l1;
                try
                {
                    Thread.sleep(l3);
                    label167: l1 = SystemClock.uptimeMillis();
                    label171: SurfaceView.this.mLastLockTime = l1;
                    SurfaceView.this.mSurfaceLock.unlock();
                    localObject = null;
                }
                catch (InterruptedException localInterruptedException)
                {
                    break label167;
                }
            }
        }

        public void addCallback(SurfaceHolder.Callback paramAnonymousCallback)
        {
            synchronized (SurfaceView.this.mCallbacks)
            {
                if (!SurfaceView.this.mCallbacks.contains(paramAnonymousCallback))
                    SurfaceView.this.mCallbacks.add(paramAnonymousCallback);
                return;
            }
        }

        public Surface getSurface()
        {
            return SurfaceView.this.mSurface;
        }

        public Rect getSurfaceFrame()
        {
            return SurfaceView.this.mSurfaceFrame;
        }

        public boolean isCreating()
        {
            return SurfaceView.this.mIsCreating;
        }

        public Canvas lockCanvas()
        {
            return internalLockCanvas(null);
        }

        public Canvas lockCanvas(Rect paramAnonymousRect)
        {
            return internalLockCanvas(paramAnonymousRect);
        }

        public void removeCallback(SurfaceHolder.Callback paramAnonymousCallback)
        {
            synchronized (SurfaceView.this.mCallbacks)
            {
                SurfaceView.this.mCallbacks.remove(paramAnonymousCallback);
                return;
            }
        }

        public void setFixedSize(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            if ((SurfaceView.this.mRequestedWidth != paramAnonymousInt1) || (SurfaceView.this.mRequestedHeight != paramAnonymousInt2))
            {
                SurfaceView.this.mRequestedWidth = paramAnonymousInt1;
                SurfaceView.this.mRequestedHeight = paramAnonymousInt2;
                SurfaceView.this.requestLayout();
            }
        }

        public void setFormat(int paramAnonymousInt)
        {
            if (paramAnonymousInt == -1)
                paramAnonymousInt = 4;
            SurfaceView.this.mRequestedFormat = paramAnonymousInt;
            if (SurfaceView.this.mWindow != null)
                SurfaceView.this.updateWindow(false, false);
        }

        public void setKeepScreenOn(boolean paramAnonymousBoolean)
        {
            int i = 1;
            Message localMessage = SurfaceView.this.mHandler.obtainMessage(i);
            if (paramAnonymousBoolean);
            while (true)
            {
                localMessage.arg1 = i;
                SurfaceView.this.mHandler.sendMessage(localMessage);
                return;
                i = 0;
            }
        }

        public void setSizeFromLayout()
        {
            if ((SurfaceView.this.mRequestedWidth != -1) || (SurfaceView.this.mRequestedHeight != -1))
            {
                SurfaceView localSurfaceView = SurfaceView.this;
                SurfaceView.this.mRequestedHeight = -1;
                localSurfaceView.mRequestedWidth = -1;
                SurfaceView.this.requestLayout();
            }
        }

        @Deprecated
        public void setType(int paramAnonymousInt)
        {
        }

        public void unlockCanvasAndPost(Canvas paramAnonymousCanvas)
        {
            SurfaceView.this.mSurface.unlockCanvasAndPost(paramAnonymousCanvas);
            SurfaceView.this.mSurfaceLock.unlock();
        }
    };
    final ReentrantLock mSurfaceLock = new ReentrantLock();
    Rect mTmpDirty;
    int mTop = -1;
    private CompatibilityInfo.Translator mTranslator;
    boolean mUpdateWindowNeeded;
    boolean mViewVisibility = false;
    boolean mVisible = false;
    final Rect mVisibleInsets = new Rect();
    int mWidth = -1;
    final Rect mWinFrame = new Rect();
    MyWindow mWindow;
    int mWindowType = 1001;
    boolean mWindowVisibility = false;

    public SurfaceView(Context paramContext)
    {
        super(paramContext);
        init();
    }

    public SurfaceView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        init();
    }

    public SurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        init();
    }

    private SurfaceHolder.Callback[] getSurfaceCallbacks()
    {
        synchronized (this.mCallbacks)
        {
            SurfaceHolder.Callback[] arrayOfCallback = new SurfaceHolder.Callback[this.mCallbacks.size()];
            this.mCallbacks.toArray(arrayOfCallback);
            return arrayOfCallback;
        }
    }

    private void init()
    {
        setWillNotDraw(true);
    }

    // ERROR //
    private void updateWindow(boolean paramBoolean1, boolean paramBoolean2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 159	android/view/SurfaceView:mHaveFrame	Z
        //     4: ifne +4 -> 8
        //     7: return
        //     8: aload_0
        //     9: invokevirtual 224	android/view/SurfaceView:getViewRootImpl	()Landroid/view/ViewRootImpl;
        //     12: astore_3
        //     13: aload_3
        //     14: ifnull +11 -> 25
        //     17: aload_0
        //     18: aload_3
        //     19: getfield 228	android/view/ViewRootImpl:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     22: putfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     25: aload_0
        //     26: getfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     29: ifnull +14 -> 43
        //     32: aload_0
        //     33: getfield 110	android/view/SurfaceView:mSurface	Landroid/view/Surface;
        //     36: aload_0
        //     37: getfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     40: invokevirtual 233	android/view/Surface:setCompatibilityTranslator	(Landroid/content/res/CompatibilityInfo$Translator;)V
        //     43: aload_0
        //     44: getfield 153	android/view/SurfaceView:mRequestedWidth	I
        //     47: istore 4
        //     49: iload 4
        //     51: ifgt +9 -> 60
        //     54: aload_0
        //     55: invokevirtual 236	android/view/SurfaceView:getWidth	()I
        //     58: istore 4
        //     60: aload_0
        //     61: getfield 155	android/view/SurfaceView:mRequestedHeight	I
        //     64: istore 5
        //     66: iload 5
        //     68: ifgt +9 -> 77
        //     71: aload_0
        //     72: invokevirtual 239	android/view/SurfaceView:getHeight	()I
        //     75: istore 5
        //     77: aload_0
        //     78: aload_0
        //     79: getfield 100	android/view/SurfaceView:mLocation	[I
        //     82: invokevirtual 243	android/view/SurfaceView:getLocationInWindow	([I)V
        //     85: aload_0
        //     86: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     89: ifnonnull +751 -> 840
        //     92: iconst_1
        //     93: istore 6
        //     95: aload_0
        //     96: getfield 175	android/view/SurfaceView:mFormat	I
        //     99: aload_0
        //     100: getfield 157	android/view/SurfaceView:mRequestedFormat	I
        //     103: if_icmpeq +743 -> 846
        //     106: iconst_1
        //     107: istore 7
        //     109: aload_0
        //     110: getfield 171	android/view/SurfaceView:mWidth	I
        //     113: iload 4
        //     115: if_icmpne +12 -> 127
        //     118: aload_0
        //     119: getfield 173	android/view/SurfaceView:mHeight	I
        //     122: iload 5
        //     124: if_icmpeq +728 -> 852
        //     127: iconst_1
        //     128: istore 8
        //     130: aload_0
        //     131: getfield 165	android/view/SurfaceView:mVisible	Z
        //     134: aload_0
        //     135: getfield 147	android/view/SurfaceView:mRequestedVisible	Z
        //     138: if_icmpeq +720 -> 858
        //     141: iconst_1
        //     142: istore 9
        //     144: iload_1
        //     145: ifne +67 -> 212
        //     148: iload 6
        //     150: ifne +62 -> 212
        //     153: iload 7
        //     155: ifne +57 -> 212
        //     158: iload 8
        //     160: ifne +52 -> 212
        //     163: iload 9
        //     165: ifne +47 -> 212
        //     168: aload_0
        //     169: getfield 167	android/view/SurfaceView:mLeft	I
        //     172: aload_0
        //     173: getfield 100	android/view/SurfaceView:mLocation	[I
        //     176: iconst_0
        //     177: iaload
        //     178: if_icmpne +34 -> 212
        //     181: aload_0
        //     182: getfield 169	android/view/SurfaceView:mTop	I
        //     185: aload_0
        //     186: getfield 100	android/view/SurfaceView:mLocation	[I
        //     189: iconst_1
        //     190: iaload
        //     191: if_icmpne +21 -> 212
        //     194: aload_0
        //     195: getfield 247	android/view/SurfaceView:mUpdateWindowNeeded	Z
        //     198: ifne +14 -> 212
        //     201: aload_0
        //     202: getfield 249	android/view/SurfaceView:mReportDrawNeeded	Z
        //     205: ifne +7 -> 212
        //     208: iload_2
        //     209: ifeq -202 -> 7
        //     212: aload_0
        //     213: getfield 147	android/view/SurfaceView:mRequestedVisible	Z
        //     216: istore 11
        //     218: aload_0
        //     219: iload 11
        //     221: putfield 165	android/view/SurfaceView:mVisible	Z
        //     224: aload_0
        //     225: aload_0
        //     226: getfield 100	android/view/SurfaceView:mLocation	[I
        //     229: iconst_0
        //     230: iaload
        //     231: putfield 167	android/view/SurfaceView:mLeft	I
        //     234: aload_0
        //     235: aload_0
        //     236: getfield 100	android/view/SurfaceView:mLocation	[I
        //     239: iconst_1
        //     240: iaload
        //     241: putfield 169	android/view/SurfaceView:mTop	I
        //     244: aload_0
        //     245: iload 4
        //     247: putfield 171	android/view/SurfaceView:mWidth	I
        //     250: aload_0
        //     251: iload 5
        //     253: putfield 173	android/view/SurfaceView:mHeight	I
        //     256: aload_0
        //     257: aload_0
        //     258: getfield 157	android/view/SurfaceView:mRequestedFormat	I
        //     261: putfield 175	android/view/SurfaceView:mFormat	I
        //     264: aload_0
        //     265: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     268: aload_0
        //     269: getfield 167	android/view/SurfaceView:mLeft	I
        //     272: putfield 252	android/view/WindowManager$LayoutParams:x	I
        //     275: aload_0
        //     276: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     279: aload_0
        //     280: getfield 169	android/view/SurfaceView:mTop	I
        //     283: putfield 255	android/view/WindowManager$LayoutParams:y	I
        //     286: aload_0
        //     287: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     290: aload_0
        //     291: invokevirtual 236	android/view/SurfaceView:getWidth	()I
        //     294: putfield 260	android/view/ViewGroup$LayoutParams:width	I
        //     297: aload_0
        //     298: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     301: aload_0
        //     302: invokevirtual 239	android/view/SurfaceView:getHeight	()I
        //     305: putfield 263	android/view/ViewGroup$LayoutParams:height	I
        //     308: aload_0
        //     309: getfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     312: ifnull +14 -> 326
        //     315: aload_0
        //     316: getfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     319: aload_0
        //     320: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     323: invokevirtual 269	android/content/res/CompatibilityInfo$Translator:translateLayoutParamsInAppWindowToScreen	(Landroid/view/WindowManager$LayoutParams;)V
        //     326: aload_0
        //     327: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     330: aload_0
        //     331: getfield 157	android/view/SurfaceView:mRequestedFormat	I
        //     334: putfield 272	android/view/WindowManager$LayoutParams:format	I
        //     337: aload_0
        //     338: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     341: astore 12
        //     343: aload 12
        //     345: sipush 16920
        //     348: aload 12
        //     350: getfield 275	android/view/WindowManager$LayoutParams:flags	I
        //     353: ior
        //     354: putfield 275	android/view/WindowManager$LayoutParams:flags	I
        //     357: aload_0
        //     358: invokevirtual 279	android/view/SurfaceView:getContext	()Landroid/content/Context;
        //     361: invokevirtual 285	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     364: invokevirtual 291	android/content/res/Resources:getCompatibilityInfo	()Landroid/content/res/CompatibilityInfo;
        //     367: invokevirtual 297	android/content/res/CompatibilityInfo:supportsScreen	()Z
        //     370: ifne +23 -> 393
        //     373: aload_0
        //     374: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     377: astore 51
        //     379: aload 51
        //     381: ldc_w 298
        //     384: aload 51
        //     386: getfield 275	android/view/WindowManager$LayoutParams:flags	I
        //     389: ior
        //     390: putfield 275	android/view/WindowManager$LayoutParams:flags	I
        //     393: aload_0
        //     394: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     397: ifnonnull +92 -> 489
        //     400: aload_0
        //     401: new 14	android/view/SurfaceView$MyWindow
        //     404: dup
        //     405: aload_0
        //     406: invokespecial 299	android/view/SurfaceView$MyWindow:<init>	(Landroid/view/SurfaceView;)V
        //     409: putfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     412: aload_0
        //     413: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     416: aload_0
        //     417: getfield 135	android/view/SurfaceView:mWindowType	I
        //     420: putfield 302	android/view/WindowManager$LayoutParams:type	I
        //     423: aload_0
        //     424: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     427: bipush 51
        //     429: putfield 305	android/view/WindowManager$LayoutParams:gravity	I
        //     432: aload_0
        //     433: getfield 307	android/view/SurfaceView:mSession	Landroid/view/IWindowSession;
        //     436: astore 45
        //     438: aload_0
        //     439: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     442: astore 46
        //     444: aload_0
        //     445: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     448: getfield 312	com/android/internal/view/BaseIWindow:mSeq	I
        //     451: istore 47
        //     453: aload_0
        //     454: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     457: astore 48
        //     459: aload_0
        //     460: getfield 165	android/view/SurfaceView:mVisible	Z
        //     463: ifeq +401 -> 864
        //     466: iconst_0
        //     467: istore 49
        //     469: aload 45
        //     471: aload 46
        //     473: iload 47
        //     475: aload 48
        //     477: iload 49
        //     479: aload_0
        //     480: getfield 128	android/view/SurfaceView:mContentInsets	Landroid/graphics/Rect;
        //     483: invokeinterface 318 6 0
        //     488: pop
        //     489: aload_0
        //     490: getfield 105	android/view/SurfaceView:mSurfaceLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     493: invokevirtual 321	java/util/concurrent/locks/ReentrantLock:lock	()V
        //     496: aload_0
        //     497: iconst_0
        //     498: putfield 247	android/view/SurfaceView:mUpdateWindowNeeded	Z
        //     501: aload_0
        //     502: getfield 249	android/view/SurfaceView:mReportDrawNeeded	Z
        //     505: istore 14
        //     507: aload_0
        //     508: iconst_0
        //     509: putfield 249	android/view/SurfaceView:mReportDrawNeeded	Z
        //     512: iload 11
        //     514: ifne +357 -> 871
        //     517: iconst_1
        //     518: istore 15
        //     520: aload_0
        //     521: iload 15
        //     523: putfield 114	android/view/SurfaceView:mDrawingStopped	Z
        //     526: aload_0
        //     527: getfield 307	android/view/SurfaceView:mSession	Landroid/view/IWindowSession;
        //     530: astore 16
        //     532: aload_0
        //     533: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     536: astore 17
        //     538: aload_0
        //     539: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     542: getfield 312	com/android/internal/view/BaseIWindow:mSeq	I
        //     545: istore 18
        //     547: aload_0
        //     548: getfield 119	android/view/SurfaceView:mLayout	Landroid/view/WindowManager$LayoutParams;
        //     551: astore 19
        //     553: aload_0
        //     554: getfield 171	android/view/SurfaceView:mWidth	I
        //     557: istore 20
        //     559: aload_0
        //     560: getfield 173	android/view/SurfaceView:mHeight	I
        //     563: istore 21
        //     565: iload 11
        //     567: ifeq +310 -> 877
        //     570: iconst_0
        //     571: istore 22
        //     573: aload 16
        //     575: aload 17
        //     577: iload 18
        //     579: aload 19
        //     581: iload 20
        //     583: iload 21
        //     585: iload 22
        //     587: iconst_2
        //     588: aload_0
        //     589: getfield 126	android/view/SurfaceView:mWinFrame	Landroid/graphics/Rect;
        //     592: aload_0
        //     593: getfield 128	android/view/SurfaceView:mContentInsets	Landroid/graphics/Rect;
        //     596: aload_0
        //     597: getfield 124	android/view/SurfaceView:mVisibleInsets	Landroid/graphics/Rect;
        //     600: aload_0
        //     601: getfield 133	android/view/SurfaceView:mConfiguration	Landroid/content/res/Configuration;
        //     604: aload_0
        //     605: getfield 112	android/view/SurfaceView:mNewSurface	Landroid/view/Surface;
        //     608: invokeinterface 325 13 0
        //     613: istore 23
        //     615: iload 23
        //     617: iconst_2
        //     618: iand
        //     619: ifeq +8 -> 627
        //     622: aload_0
        //     623: iconst_1
        //     624: putfield 249	android/view/SurfaceView:mReportDrawNeeded	Z
        //     627: aload_0
        //     628: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     631: iconst_0
        //     632: putfield 328	android/graphics/Rect:left	I
        //     635: aload_0
        //     636: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     639: iconst_0
        //     640: putfield 331	android/graphics/Rect:top	I
        //     643: aload_0
        //     644: getfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     647: ifnonnull +237 -> 884
        //     650: aload_0
        //     651: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     654: aload_0
        //     655: getfield 126	android/view/SurfaceView:mWinFrame	Landroid/graphics/Rect;
        //     658: invokevirtual 333	android/graphics/Rect:width	()I
        //     661: putfield 336	android/graphics/Rect:right	I
        //     664: aload_0
        //     665: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     668: aload_0
        //     669: getfield 126	android/view/SurfaceView:mWinFrame	Landroid/graphics/Rect;
        //     672: invokevirtual 338	android/graphics/Rect:height	()I
        //     675: putfield 341	android/graphics/Rect:bottom	I
        //     678: aload_0
        //     679: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     682: getfield 336	android/graphics/Rect:right	I
        //     685: istore 25
        //     687: aload_0
        //     688: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     691: getfield 341	android/graphics/Rect:bottom	I
        //     694: istore 26
        //     696: aload_0
        //     697: getfield 179	android/view/SurfaceView:mLastSurfaceWidth	I
        //     700: iload 25
        //     702: if_icmpne +570 -> 1272
        //     705: aload_0
        //     706: getfield 181	android/view/SurfaceView:mLastSurfaceHeight	I
        //     709: iload 26
        //     711: if_icmpeq +248 -> 959
        //     714: goto +558 -> 1272
        //     717: aload_0
        //     718: iload 25
        //     720: putfield 179	android/view/SurfaceView:mLastSurfaceWidth	I
        //     723: aload_0
        //     724: iload 26
        //     726: putfield 181	android/view/SurfaceView:mLastSurfaceHeight	I
        //     729: aload_0
        //     730: getfield 105	android/view/SurfaceView:mSurfaceLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     733: invokevirtual 344	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //     736: iload_2
        //     737: iload 6
        //     739: iload 14
        //     741: ior
        //     742: ior
        //     743: istore 28
        //     745: aconst_null
        //     746: astore 29
        //     748: iload 23
        //     750: iconst_4
        //     751: iand
        //     752: ifeq +213 -> 965
        //     755: iconst_1
        //     756: istore 30
        //     758: aload_0
        //     759: getfield 161	android/view/SurfaceView:mSurfaceCreated	Z
        //     762: ifeq +209 -> 971
        //     765: iload 30
        //     767: ifne +13 -> 780
        //     770: iload 11
        //     772: ifne +199 -> 971
        //     775: iload 9
        //     777: ifeq +194 -> 971
        //     780: aload_0
        //     781: iconst_0
        //     782: putfield 161	android/view/SurfaceView:mSurfaceCreated	Z
        //     785: aload_0
        //     786: getfield 110	android/view/SurfaceView:mSurface	Landroid/view/Surface;
        //     789: invokevirtual 347	android/view/Surface:isValid	()Z
        //     792: ifeq +179 -> 971
        //     795: aload_0
        //     796: invokespecial 349	android/view/SurfaceView:getSurfaceCallbacks	()[Landroid/view/SurfaceHolder$Callback;
        //     799: astore 29
        //     801: aload 29
        //     803: astore 42
        //     805: aload 42
        //     807: arraylength
        //     808: istore 43
        //     810: iconst_0
        //     811: istore 44
        //     813: iload 44
        //     815: iload 43
        //     817: if_icmpge +154 -> 971
        //     820: aload 42
        //     822: iload 44
        //     824: aaload
        //     825: aload_0
        //     826: getfield 187	android/view/SurfaceView:mSurfaceHolder	Landroid/view/SurfaceHolder;
        //     829: invokeinterface 353 2 0
        //     834: iinc 44 1
        //     837: goto -24 -> 813
        //     840: iconst_0
        //     841: istore 6
        //     843: goto -748 -> 95
        //     846: iconst_0
        //     847: istore 7
        //     849: goto -740 -> 109
        //     852: iconst_0
        //     853: istore 8
        //     855: goto -725 -> 130
        //     858: iconst_0
        //     859: istore 9
        //     861: goto -717 -> 144
        //     864: bipush 8
        //     866: istore 49
        //     868: goto -399 -> 469
        //     871: iconst_0
        //     872: istore 15
        //     874: goto -354 -> 520
        //     877: bipush 8
        //     879: istore 22
        //     881: goto -308 -> 573
        //     884: aload_0
        //     885: getfield 229	android/view/SurfaceView:mTranslator	Landroid/content/res/CompatibilityInfo$Translator;
        //     888: getfield 357	android/content/res/CompatibilityInfo$Translator:applicationInvertedScale	F
        //     891: fstore 24
        //     893: aload_0
        //     894: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     897: ldc_w 358
        //     900: fload 24
        //     902: aload_0
        //     903: getfield 126	android/view/SurfaceView:mWinFrame	Landroid/graphics/Rect;
        //     906: invokevirtual 333	android/graphics/Rect:width	()I
        //     909: i2f
        //     910: fmul
        //     911: fadd
        //     912: f2i
        //     913: putfield 336	android/graphics/Rect:right	I
        //     916: aload_0
        //     917: getfield 177	android/view/SurfaceView:mSurfaceFrame	Landroid/graphics/Rect;
        //     920: ldc_w 358
        //     923: fload 24
        //     925: aload_0
        //     926: getfield 126	android/view/SurfaceView:mWinFrame	Landroid/graphics/Rect;
        //     929: invokevirtual 338	android/graphics/Rect:height	()I
        //     932: i2f
        //     933: fmul
        //     934: fadd
        //     935: f2i
        //     936: putfield 341	android/graphics/Rect:bottom	I
        //     939: goto -261 -> 678
        //     942: astore 13
        //     944: aload_0
        //     945: getfield 105	android/view/SurfaceView:mSurfaceLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     948: invokevirtual 344	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //     951: aload 13
        //     953: athrow
        //     954: astore 10
        //     956: goto -949 -> 7
        //     959: iconst_0
        //     960: istore 27
        //     962: goto -245 -> 717
        //     965: iconst_0
        //     966: istore 30
        //     968: goto -210 -> 758
        //     971: aload_0
        //     972: getfield 110	android/view/SurfaceView:mSurface	Landroid/view/Surface;
        //     975: aload_0
        //     976: getfield 112	android/view/SurfaceView:mNewSurface	Landroid/view/Surface;
        //     979: invokevirtual 362	android/view/Surface:transferFrom	(Landroid/view/Surface;)V
        //     982: iload 11
        //     984: ifeq +249 -> 1233
        //     987: aload_0
        //     988: getfield 161	android/view/SurfaceView:mSurfaceCreated	Z
        //     991: ifne +287 -> 1278
        //     994: iload 30
        //     996: ifne +8 -> 1004
        //     999: iload 9
        //     1001: ifeq +277 -> 1278
        //     1004: aload_0
        //     1005: iconst_1
        //     1006: putfield 161	android/view/SurfaceView:mSurfaceCreated	Z
        //     1009: aload_0
        //     1010: iconst_1
        //     1011: putfield 137	android/view/SurfaceView:mIsCreating	Z
        //     1014: aload 29
        //     1016: ifnonnull +9 -> 1025
        //     1019: aload_0
        //     1020: invokespecial 349	android/view/SurfaceView:getSurfaceCallbacks	()[Landroid/view/SurfaceHolder$Callback;
        //     1023: astore 29
        //     1025: aload 29
        //     1027: astore 39
        //     1029: aload 39
        //     1031: arraylength
        //     1032: istore 40
        //     1034: iconst_0
        //     1035: istore 41
        //     1037: iload 41
        //     1039: iload 40
        //     1041: if_icmpge +237 -> 1278
        //     1044: aload 39
        //     1046: iload 41
        //     1048: aaload
        //     1049: aload_0
        //     1050: getfield 187	android/view/SurfaceView:mSurfaceHolder	Landroid/view/SurfaceHolder;
        //     1053: invokeinterface 365 2 0
        //     1058: iinc 41 1
        //     1061: goto -24 -> 1037
        //     1064: aload 29
        //     1066: ifnonnull +9 -> 1075
        //     1069: aload_0
        //     1070: invokespecial 349	android/view/SurfaceView:getSurfaceCallbacks	()[Landroid/view/SurfaceHolder$Callback;
        //     1073: astore 29
        //     1075: aload 29
        //     1077: astore 32
        //     1079: aload 32
        //     1081: arraylength
        //     1082: istore 33
        //     1084: iconst_0
        //     1085: istore 34
        //     1087: iload 34
        //     1089: iload 33
        //     1091: if_icmpge +31 -> 1122
        //     1094: aload 32
        //     1096: iload 34
        //     1098: aaload
        //     1099: aload_0
        //     1100: getfield 187	android/view/SurfaceView:mSurfaceHolder	Landroid/view/SurfaceHolder;
        //     1103: aload_0
        //     1104: getfield 175	android/view/SurfaceView:mFormat	I
        //     1107: iload 4
        //     1109: iload 5
        //     1111: invokeinterface 369 5 0
        //     1116: iinc 34 1
        //     1119: goto -32 -> 1087
        //     1122: iload 28
        //     1124: ifeq +109 -> 1233
        //     1127: aload 29
        //     1129: ifnonnull +9 -> 1138
        //     1132: aload_0
        //     1133: invokespecial 349	android/view/SurfaceView:getSurfaceCallbacks	()[Landroid/view/SurfaceHolder$Callback;
        //     1136: astore 29
        //     1138: aload 29
        //     1140: astore 35
        //     1142: aload 35
        //     1144: arraylength
        //     1145: istore 36
        //     1147: iconst_0
        //     1148: istore 37
        //     1150: iload 37
        //     1152: iload 36
        //     1154: if_icmpge +79 -> 1233
        //     1157: aload 35
        //     1159: iload 37
        //     1161: aaload
        //     1162: astore 38
        //     1164: aload 38
        //     1166: instanceof 371
        //     1169: ifeq +17 -> 1186
        //     1172: aload 38
        //     1174: checkcast 371	android/view/SurfaceHolder$Callback2
        //     1177: aload_0
        //     1178: getfield 187	android/view/SurfaceView:mSurfaceHolder	Landroid/view/SurfaceHolder;
        //     1181: invokeinterface 374 2 0
        //     1186: iinc 37 1
        //     1189: goto -39 -> 1150
        //     1192: astore 31
        //     1194: aload_0
        //     1195: iconst_0
        //     1196: putfield 137	android/view/SurfaceView:mIsCreating	Z
        //     1199: iload 28
        //     1201: ifeq +16 -> 1217
        //     1204: aload_0
        //     1205: getfield 307	android/view/SurfaceView:mSession	Landroid/view/IWindowSession;
        //     1208: aload_0
        //     1209: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     1212: invokeinterface 378 2 0
        //     1217: aload_0
        //     1218: getfield 307	android/view/SurfaceView:mSession	Landroid/view/IWindowSession;
        //     1221: aload_0
        //     1222: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     1225: invokeinterface 381 2 0
        //     1230: aload 31
        //     1232: athrow
        //     1233: aload_0
        //     1234: iconst_0
        //     1235: putfield 137	android/view/SurfaceView:mIsCreating	Z
        //     1238: iload 28
        //     1240: ifeq +16 -> 1256
        //     1243: aload_0
        //     1244: getfield 307	android/view/SurfaceView:mSession	Landroid/view/IWindowSession;
        //     1247: aload_0
        //     1248: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     1251: invokeinterface 378 2 0
        //     1256: aload_0
        //     1257: getfield 307	android/view/SurfaceView:mSession	Landroid/view/IWindowSession;
        //     1260: aload_0
        //     1261: getfield 245	android/view/SurfaceView:mWindow	Landroid/view/SurfaceView$MyWindow;
        //     1264: invokeinterface 381 2 0
        //     1269: goto -1262 -> 7
        //     1272: iconst_1
        //     1273: istore 27
        //     1275: goto -558 -> 717
        //     1278: iload 6
        //     1280: ifne -216 -> 1064
        //     1283: iload 7
        //     1285: ifne -221 -> 1064
        //     1288: iload 8
        //     1290: ifne -226 -> 1064
        //     1293: iload 9
        //     1295: ifne -231 -> 1064
        //     1298: iload 27
        //     1300: ifeq -178 -> 1122
        //     1303: goto -239 -> 1064
        //
        // Exception table:
        //     from	to	target	type
        //     496	729	942	finally
        //     884	939	942	finally
        //     212	496	954	android/os/RemoteException
        //     729	736	954	android/os/RemoteException
        //     944	954	954	android/os/RemoteException
        //     1194	1269	954	android/os/RemoteException
        //     758	834	1192	finally
        //     971	1186	1192	finally
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        if ((this.mWindowType != 1000) && ((0x80 & this.mPrivateFlags) == 128))
            paramCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        super.dispatchDraw(paramCanvas);
    }

    public void draw(Canvas paramCanvas)
    {
        if ((this.mWindowType != 1000) && ((0x80 & this.mPrivateFlags) == 0))
            paramCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        super.draw(paramCanvas);
    }

    public boolean gatherTransparentRegion(Region paramRegion)
    {
        boolean bool;
        if (this.mWindowType == 1000)
            bool = super.gatherTransparentRegion(paramRegion);
        label122: 
        while (true)
        {
            return bool;
            bool = true;
            if ((0x80 & this.mPrivateFlags) == 0)
                bool = super.gatherTransparentRegion(paramRegion);
            while (true)
            {
                if (!PixelFormat.formatHasAlpha(this.mRequestedFormat))
                    break label122;
                bool = false;
                break;
                if (paramRegion != null)
                {
                    int i = getWidth();
                    int j = getHeight();
                    if ((i > 0) && (j > 0))
                    {
                        getLocationInWindow(this.mLocation);
                        int k = this.mLocation[0];
                        int m = this.mLocation[1];
                        paramRegion.op(k, m, k + i, m + j, Region.Op.UNION);
                    }
                }
            }
        }
    }

    public SurfaceHolder getHolder()
    {
        return this.mSurfaceHolder;
    }

    void handleGetNewSurface()
    {
        updateWindow(false, false);
    }

    public boolean isFixedSize()
    {
        if ((this.mRequestedWidth != -1) || (this.mRequestedHeight != -1));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.mParent.requestTransparentRegion(this);
        this.mSession = getWindowSession();
        this.mLayout.token = getWindowToken();
        this.mLayout.setTitle("SurfaceView");
        if (getVisibility() == 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mViewVisibility = bool;
            if (!this.mGlobalListenersAdded)
            {
                ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
                localViewTreeObserver.addOnScrollChangedListener(this.mScrollChangedListener);
                localViewTreeObserver.addOnPreDrawListener(this.mDrawListener);
                this.mGlobalListenersAdded = true;
            }
            return;
        }
    }

    protected void onDetachedFromWindow()
    {
        if (this.mGlobalListenersAdded)
        {
            ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
            localViewTreeObserver.removeOnScrollChangedListener(this.mScrollChangedListener);
            localViewTreeObserver.removeOnPreDrawListener(this.mDrawListener);
            this.mGlobalListenersAdded = false;
        }
        this.mRequestedVisible = false;
        updateWindow(false, false);
        this.mHaveFrame = false;
        if (this.mWindow != null);
        try
        {
            this.mSession.remove(this.mWindow);
            label69: this.mWindow = null;
            this.mSession = null;
            this.mLayout.token = null;
            super.onDetachedFromWindow();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            break label69;
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i;
        if (this.mRequestedWidth >= 0)
        {
            i = resolveSizeAndState(this.mRequestedWidth, paramInt1, 0);
            if (this.mRequestedHeight < 0)
                break label52;
        }
        label52: for (int j = resolveSizeAndState(this.mRequestedHeight, paramInt2, 0); ; j = getDefaultSize(0, paramInt2))
        {
            setMeasuredDimension(i, j);
            return;
            i = getDefaultSize(0, paramInt1);
            break;
        }
    }

    protected void onWindowVisibilityChanged(int paramInt)
    {
        boolean bool1 = true;
        super.onWindowVisibilityChanged(paramInt);
        boolean bool2;
        if (paramInt == 0)
        {
            bool2 = bool1;
            this.mWindowVisibility = bool2;
            if ((!this.mWindowVisibility) || (!this.mViewVisibility))
                break label49;
        }
        while (true)
        {
            this.mRequestedVisible = bool1;
            updateWindow(false, false);
            return;
            bool2 = false;
            break;
            label49: bool1 = false;
        }
    }

    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        updateWindow(false, false);
        return bool;
    }

    public void setVisibility(int paramInt)
    {
        super.setVisibility(paramInt);
        boolean bool1;
        if (paramInt == 0)
        {
            bool1 = true;
            this.mViewVisibility = bool1;
            if ((!this.mWindowVisibility) || (!this.mViewVisibility))
                break label61;
        }
        label61: for (boolean bool2 = true; ; bool2 = false)
        {
            if (bool2 != this.mRequestedVisible)
                requestLayout();
            this.mRequestedVisible = bool2;
            updateWindow(false, false);
            return;
            bool1 = false;
            break;
        }
    }

    public void setWindowType(int paramInt)
    {
        this.mWindowType = paramInt;
    }

    public void setZOrderMediaOverlay(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1004; ; i = 1001)
        {
            this.mWindowType = i;
            return;
        }
    }

    public void setZOrderOnTop(boolean paramBoolean)
    {
        WindowManager.LayoutParams localLayoutParams2;
        if (paramBoolean)
        {
            this.mWindowType = 1000;
            localLayoutParams2 = this.mLayout;
        }
        WindowManager.LayoutParams localLayoutParams1;
        for (localLayoutParams2.flags = (0x20000 | localLayoutParams2.flags); ; localLayoutParams1.flags = (0xFFFDFFFF & localLayoutParams1.flags))
        {
            return;
            this.mWindowType = 1001;
            localLayoutParams1 = this.mLayout;
        }
    }

    private static class MyWindow extends BaseIWindow
    {
        int mCurHeight = -1;
        int mCurWidth = -1;
        private final WeakReference<SurfaceView> mSurfaceView;

        public MyWindow(SurfaceView paramSurfaceView)
        {
            this.mSurfaceView = new WeakReference(paramSurfaceView);
        }

        public void dispatchAppVisibility(boolean paramBoolean)
        {
        }

        public void dispatchGetNewSurface()
        {
            SurfaceView localSurfaceView = (SurfaceView)this.mSurfaceView.get();
            if (localSurfaceView != null)
            {
                Message localMessage = localSurfaceView.mHandler.obtainMessage(2);
                localSurfaceView.mHandler.sendMessage(localMessage);
            }
        }

        public void executeCommand(String paramString1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor)
        {
        }

        public void resized(int paramInt1, int paramInt2, Rect paramRect1, Rect paramRect2, boolean paramBoolean, Configuration paramConfiguration)
        {
            SurfaceView localSurfaceView = (SurfaceView)this.mSurfaceView.get();
            if (localSurfaceView != null)
            {
                localSurfaceView.mSurfaceLock.lock();
                if (!paramBoolean)
                    break label61;
            }
            try
            {
                localSurfaceView.mUpdateWindowNeeded = true;
                localSurfaceView.mReportDrawNeeded = true;
                localSurfaceView.mHandler.sendEmptyMessage(3);
                while (true)
                {
                    return;
                    label61: if ((localSurfaceView.mWinFrame.width() != paramInt1) || (localSurfaceView.mWinFrame.height() != paramInt2))
                    {
                        localSurfaceView.mUpdateWindowNeeded = true;
                        localSurfaceView.mHandler.sendEmptyMessage(3);
                    }
                }
            }
            finally
            {
                localSurfaceView.mSurfaceLock.unlock();
            }
        }

        public void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2)
        {
            Log.w("SurfaceView", "Unexpected focus in surface: focus=" + paramBoolean1 + ", touchEnabled=" + paramBoolean2);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.SurfaceView
 * JD-Core Version:        0.6.2
 */