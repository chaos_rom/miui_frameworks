package android.view;

import android.app.AppGlobals;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.util.SparseArray;

public class ViewConfiguration
{
    public static final float ALPHA_THRESHOLD = 0.02083333F;
    public static final float ALPHA_THRESHOLD_INT = 5.291667F;
    private static final int DEFAULT_LONG_PRESS_TIMEOUT = 500;
    private static final int DOUBLE_TAP_SLOP = 100;
    private static final int DOUBLE_TAP_TIMEOUT = 300;
    private static final int DOUBLE_TAP_TOUCH_SLOP = 8;
    private static final int EDGE_SLOP = 12;
    private static final int FADING_EDGE_LENGTH = 12;
    private static final int GLOBAL_ACTIONS_KEY_TIMEOUT = 500;
    private static final int HOVER_TAP_SLOP = 20;
    private static final int HOVER_TAP_TIMEOUT = 150;
    private static final int JUMP_TAP_TIMEOUT = 500;
    private static final int KEY_REPEAT_DELAY = 50;

    @Deprecated
    private static final int MAXIMUM_DRAWING_CACHE_SIZE = 1536000;
    private static final int MAXIMUM_FLING_VELOCITY = 8000;
    private static final int MINIMUM_FLING_VELOCITY = 50;
    private static final int OVERFLING_DISTANCE = 6;
    private static final int OVERSCROLL_DISTANCE = 0;
    private static final int PAGING_TOUCH_SLOP = 16;
    public static final float PANEL_BIT_DEPTH = 24.0F;
    private static final int PRESSED_STATE_DURATION = 64;
    private static final int SCROLL_BAR_DEFAULT_DELAY = 300;
    private static final int SCROLL_BAR_FADE_DURATION = 250;
    private static final int SCROLL_BAR_SIZE = 10;
    private static final float SCROLL_FRICTION = 0.015F;
    private static final long SEND_RECURRING_ACCESSIBILITY_EVENTS_INTERVAL_MILLIS = 100L;
    private static final int TAP_TIMEOUT = 180;
    private static final int TOUCH_SLOP = 8;
    private static final int WINDOW_TOUCH_SLOP = 16;
    private static final int ZOOM_CONTROLS_TIMEOUT = 3000;
    static final SparseArray<ViewConfiguration> sConfigurations = new SparseArray(2);
    private final int mDoubleTapSlop;
    private final int mDoubleTapTouchSlop;
    private final int mEdgeSlop;
    private final int mFadingEdgeLength;
    private final boolean mFadingMarqueeEnabled;
    private final int mMaximumDrawingCacheSize;
    private final int mMaximumFlingVelocity;
    private final int mMinimumFlingVelocity;
    private final int mOverflingDistance;
    private final int mOverscrollDistance;
    private final int mPagingTouchSlop;
    private final int mScrollbarSize;
    private final int mTouchSlop;
    private final int mWindowTouchSlop;
    private boolean sHasPermanentMenuKey;
    private boolean sHasPermanentMenuKeySet;

    @Deprecated
    public ViewConfiguration()
    {
        this.mEdgeSlop = 12;
        this.mFadingEdgeLength = 12;
        this.mMinimumFlingVelocity = 50;
        this.mMaximumFlingVelocity = 8000;
        this.mScrollbarSize = 10;
        this.mTouchSlop = 8;
        this.mDoubleTapTouchSlop = 8;
        this.mPagingTouchSlop = 16;
        this.mDoubleTapSlop = 100;
        this.mWindowTouchSlop = 16;
        this.mMaximumDrawingCacheSize = 1536000;
        this.mOverscrollDistance = 0;
        this.mOverflingDistance = 6;
        this.mFadingMarqueeEnabled = true;
    }

    private ViewConfiguration(Context paramContext)
    {
        Resources localResources = paramContext.getResources();
        DisplayMetrics localDisplayMetrics = localResources.getDisplayMetrics();
        Configuration localConfiguration = localResources.getConfiguration();
        float f1 = localDisplayMetrics.density;
        float f2;
        if (localConfiguration.isLayoutSizeAtLeast(4))
            f2 = f1 * 1.5F;
        while (true)
        {
            this.mEdgeSlop = ((int)(0.5F + f2 * 12.0F));
            this.mFadingEdgeLength = ((int)(0.5F + f2 * 12.0F));
            this.mMinimumFlingVelocity = ((int)(0.5F + 50.0F * f1));
            this.mMaximumFlingVelocity = ((int)(0.5F + 8000.0F * f1));
            this.mScrollbarSize = ((int)(0.5F + 10.0F * f1));
            this.mDoubleTapSlop = ((int)(0.5F + 100.0F * f2));
            this.mWindowTouchSlop = ((int)(0.5F + 16.0F * f2));
            Display localDisplay = WindowManagerImpl.getDefault().getDefaultDisplay();
            this.mMaximumDrawingCacheSize = (4 * localDisplay.getRawWidth() * localDisplay.getRawHeight());
            this.mOverscrollDistance = ((int)(0.5F + 0.0F * f2));
            this.mOverflingDistance = ((int)(0.5F + 6.0F * f2));
            IWindowManager localIWindowManager;
            if (!this.sHasPermanentMenuKeySet)
                localIWindowManager = Display.getWindowManager();
            try
            {
                if ((!localIWindowManager.hasSystemNavBar()) && (!localIWindowManager.hasNavigationBar()));
                while (true)
                {
                    this.sHasPermanentMenuKey = bool;
                    this.sHasPermanentMenuKeySet = true;
                    this.mFadingMarqueeEnabled = localResources.getBoolean(17891340);
                    this.mTouchSlop = localResources.getDimensionPixelSize(17104904);
                    this.mPagingTouchSlop = (2 * this.mTouchSlop);
                    this.mDoubleTapTouchSlop = this.mTouchSlop;
                    return;
                    f2 = f1;
                    break;
                    bool = false;
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    this.sHasPermanentMenuKey = false;
            }
        }
    }

    public static ViewConfiguration get(Context paramContext)
    {
        int i = (int)(100.0F * paramContext.getResources().getDisplayMetrics().density);
        ViewConfiguration localViewConfiguration = (ViewConfiguration)sConfigurations.get(i);
        if (localViewConfiguration == null)
        {
            localViewConfiguration = new ViewConfiguration(paramContext);
            sConfigurations.put(i, localViewConfiguration);
        }
        return localViewConfiguration;
    }

    @Deprecated
    public static int getDoubleTapSlop()
    {
        return 100;
    }

    public static int getDoubleTapTimeout()
    {
        return 300;
    }

    @Deprecated
    public static int getEdgeSlop()
    {
        return 12;
    }

    @Deprecated
    public static int getFadingEdgeLength()
    {
        return 12;
    }

    public static long getGlobalActionKeyTimeout()
    {
        return 500L;
    }

    public static int getHoverTapSlop()
    {
        return 20;
    }

    public static int getHoverTapTimeout()
    {
        return 150;
    }

    public static int getJumpTapTimeout()
    {
        return 500;
    }

    public static int getKeyRepeatDelay()
    {
        return 50;
    }

    public static int getKeyRepeatTimeout()
    {
        return getLongPressTimeout();
    }

    public static int getLongPressTimeout()
    {
        return AppGlobals.getIntCoreSetting("long_press_timeout", 500);
    }

    @Deprecated
    public static int getMaximumDrawingCacheSize()
    {
        return 1536000;
    }

    @Deprecated
    public static int getMaximumFlingVelocity()
    {
        return 8000;
    }

    @Deprecated
    public static int getMinimumFlingVelocity()
    {
        return 50;
    }

    public static int getPressedStateDuration()
    {
        return 64;
    }

    public static int getScrollBarFadeDuration()
    {
        return 250;
    }

    @Deprecated
    public static int getScrollBarSize()
    {
        return 10;
    }

    public static int getScrollDefaultDelay()
    {
        return 300;
    }

    public static float getScrollFriction()
    {
        return 0.015F;
    }

    public static long getSendRecurringAccessibilityEventsInterval()
    {
        return 100L;
    }

    public static int getTapTimeout()
    {
        return 180;
    }

    @Deprecated
    public static int getTouchSlop()
    {
        return 8;
    }

    @Deprecated
    public static int getWindowTouchSlop()
    {
        return 16;
    }

    public static long getZoomControlsTimeout()
    {
        return 3000L;
    }

    public int getScaledDoubleTapSlop()
    {
        return this.mDoubleTapSlop;
    }

    public int getScaledDoubleTapTouchSlop()
    {
        return this.mDoubleTapTouchSlop;
    }

    public int getScaledEdgeSlop()
    {
        return this.mEdgeSlop;
    }

    public int getScaledFadingEdgeLength()
    {
        return this.mFadingEdgeLength;
    }

    public int getScaledMaximumDrawingCacheSize()
    {
        return this.mMaximumDrawingCacheSize;
    }

    public int getScaledMaximumFlingVelocity()
    {
        return this.mMaximumFlingVelocity;
    }

    public int getScaledMinimumFlingVelocity()
    {
        return this.mMinimumFlingVelocity;
    }

    public int getScaledOverflingDistance()
    {
        return this.mOverflingDistance;
    }

    public int getScaledOverscrollDistance()
    {
        return this.mOverscrollDistance;
    }

    public int getScaledPagingTouchSlop()
    {
        return this.mPagingTouchSlop;
    }

    public int getScaledScrollBarSize()
    {
        return this.mScrollbarSize;
    }

    public int getScaledTouchSlop()
    {
        return this.mTouchSlop;
    }

    public int getScaledWindowTouchSlop()
    {
        return this.mWindowTouchSlop;
    }

    public boolean hasPermanentMenuKey()
    {
        return this.sHasPermanentMenuKey;
    }

    public boolean isFadingMarqueeEnabled()
    {
        return this.mFadingMarqueeEnabled;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.ViewConfiguration
 * JD-Core Version:        0.6.2
 */