package android.view;

import android.graphics.Bitmap;

abstract class GLES20Layer extends HardwareLayer
{
    Finalizer mFinalizer;
    int mLayer;

    GLES20Layer()
    {
    }

    GLES20Layer(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        super(paramInt1, paramInt2, paramBoolean);
    }

    boolean copyInto(Bitmap paramBitmap)
    {
        return GLES20Canvas.nCopyLayer(this.mLayer, paramBitmap.mNativeBitmap);
    }

    void destroy()
    {
        if (this.mFinalizer != null)
        {
            this.mFinalizer.destroy();
            this.mFinalizer = null;
        }
        this.mLayer = 0;
    }

    void flush()
    {
        if (this.mLayer != 0)
            GLES20Canvas.nFlushLayer(this.mLayer);
    }

    public int getLayer()
    {
        return this.mLayer;
    }

    void update(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        super.update(paramInt1, paramInt2, paramBoolean);
    }

    static class Finalizer
    {
        private int mLayerId;

        public Finalizer(int paramInt)
        {
            this.mLayerId = paramInt;
        }

        void destroy()
        {
            GLES20Canvas.nDestroyLayer(this.mLayerId);
            this.mLayerId = 0;
        }

        protected void finalize()
            throws Throwable
        {
            try
            {
                if (this.mLayerId != 0)
                    GLES20Canvas.nDestroyLayerDeferred(this.mLayerId);
                return;
            }
            finally
            {
                super.finalize();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.GLES20Layer
 * JD-Core Version:        0.6.2
 */