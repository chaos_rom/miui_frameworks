package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import android.util.SparseIntArray;

public class KeyEvent extends InputEvent
    implements Parcelable
{
    public static final int ACTION_DOWN = 0;
    public static final int ACTION_MULTIPLE = 2;
    public static final int ACTION_UP = 1;
    public static final Parcelable.Creator<KeyEvent> CREATOR = new Parcelable.Creator()
    {
        public KeyEvent createFromParcel(Parcel paramAnonymousParcel)
        {
            paramAnonymousParcel.readInt();
            return KeyEvent.createFromParcelBody(paramAnonymousParcel);
        }

        public KeyEvent[] newArray(int paramAnonymousInt)
        {
            return new KeyEvent[paramAnonymousInt];
        }
    };
    static final boolean DEBUG = false;
    public static final int FLAG_CANCELED = 32;
    public static final int FLAG_CANCELED_LONG_PRESS = 256;
    public static final int FLAG_EDITOR_ACTION = 16;
    public static final int FLAG_FALLBACK = 1024;
    public static final int FLAG_FROM_SYSTEM = 8;
    public static final int FLAG_KEEP_TOUCH_MODE = 4;
    public static final int FLAG_LONG_PRESS = 128;
    public static final int FLAG_SOFT_KEYBOARD = 2;
    public static final int FLAG_START_TRACKING = 1073741824;
    public static final int FLAG_TAINTED = -2147483648;
    public static final int FLAG_TRACKING = 512;
    public static final int FLAG_VIRTUAL_HARD_KEY = 64;
    public static final int FLAG_WOKE_HERE = 1;
    public static final int KEYCODE_0 = 7;
    public static final int KEYCODE_1 = 8;
    public static final int KEYCODE_2 = 9;
    public static final int KEYCODE_3 = 10;
    public static final int KEYCODE_3D_MODE = 206;
    public static final int KEYCODE_4 = 11;
    public static final int KEYCODE_5 = 12;
    public static final int KEYCODE_6 = 13;
    public static final int KEYCODE_7 = 14;
    public static final int KEYCODE_8 = 15;
    public static final int KEYCODE_9 = 16;
    public static final int KEYCODE_A = 29;
    public static final int KEYCODE_ALT_LEFT = 57;
    public static final int KEYCODE_ALT_RIGHT = 58;
    public static final int KEYCODE_APOSTROPHE = 75;
    public static final int KEYCODE_APP_SWITCH = 187;
    public static final int KEYCODE_ASSIST = 219;
    public static final int KEYCODE_AT = 77;
    public static final int KEYCODE_AVR_INPUT = 182;
    public static final int KEYCODE_AVR_POWER = 181;
    public static final int KEYCODE_B = 30;
    public static final int KEYCODE_BACK = 4;
    public static final int KEYCODE_BACKSLASH = 73;
    public static final int KEYCODE_BOOKMARK = 174;
    public static final int KEYCODE_BREAK = 121;
    public static final int KEYCODE_BUTTON_1 = 188;
    public static final int KEYCODE_BUTTON_10 = 197;
    public static final int KEYCODE_BUTTON_11 = 198;
    public static final int KEYCODE_BUTTON_12 = 199;
    public static final int KEYCODE_BUTTON_13 = 200;
    public static final int KEYCODE_BUTTON_14 = 201;
    public static final int KEYCODE_BUTTON_15 = 202;
    public static final int KEYCODE_BUTTON_16 = 203;
    public static final int KEYCODE_BUTTON_2 = 189;
    public static final int KEYCODE_BUTTON_3 = 190;
    public static final int KEYCODE_BUTTON_4 = 191;
    public static final int KEYCODE_BUTTON_5 = 192;
    public static final int KEYCODE_BUTTON_6 = 193;
    public static final int KEYCODE_BUTTON_7 = 194;
    public static final int KEYCODE_BUTTON_8 = 195;
    public static final int KEYCODE_BUTTON_9 = 196;
    public static final int KEYCODE_BUTTON_A = 96;
    public static final int KEYCODE_BUTTON_B = 97;
    public static final int KEYCODE_BUTTON_C = 98;
    public static final int KEYCODE_BUTTON_L1 = 102;
    public static final int KEYCODE_BUTTON_L2 = 104;
    public static final int KEYCODE_BUTTON_MODE = 110;
    public static final int KEYCODE_BUTTON_R1 = 103;
    public static final int KEYCODE_BUTTON_R2 = 105;
    public static final int KEYCODE_BUTTON_SELECT = 109;
    public static final int KEYCODE_BUTTON_START = 108;
    public static final int KEYCODE_BUTTON_THUMBL = 106;
    public static final int KEYCODE_BUTTON_THUMBR = 107;
    public static final int KEYCODE_BUTTON_X = 99;
    public static final int KEYCODE_BUTTON_Y = 100;
    public static final int KEYCODE_BUTTON_Z = 101;
    public static final int KEYCODE_C = 31;
    public static final int KEYCODE_CALCULATOR = 210;
    public static final int KEYCODE_CALENDAR = 208;
    public static final int KEYCODE_CALL = 5;
    public static final int KEYCODE_CAMERA = 27;
    public static final int KEYCODE_CAPS_LOCK = 115;
    public static final int KEYCODE_CAPTIONS = 175;
    public static final int KEYCODE_CHANNEL_DOWN = 167;
    public static final int KEYCODE_CHANNEL_UP = 166;
    public static final int KEYCODE_CLEAR = 28;
    public static final int KEYCODE_COMMA = 55;
    public static final int KEYCODE_CONTACTS = 207;
    public static final int KEYCODE_CTRL_LEFT = 113;
    public static final int KEYCODE_CTRL_RIGHT = 114;
    public static final int KEYCODE_D = 32;
    public static final int KEYCODE_DEL = 67;
    public static final int KEYCODE_DPAD_CENTER = 23;
    public static final int KEYCODE_DPAD_DOWN = 20;
    public static final int KEYCODE_DPAD_LEFT = 21;
    public static final int KEYCODE_DPAD_RIGHT = 22;
    public static final int KEYCODE_DPAD_UP = 19;
    public static final int KEYCODE_DVR = 173;
    public static final int KEYCODE_E = 33;
    public static final int KEYCODE_EISU = 212;
    public static final int KEYCODE_ENDCALL = 6;
    public static final int KEYCODE_ENTER = 66;
    public static final int KEYCODE_ENVELOPE = 65;
    public static final int KEYCODE_EQUALS = 70;
    public static final int KEYCODE_ESCAPE = 111;
    public static final int KEYCODE_EXPLORER = 64;
    public static final int KEYCODE_F = 34;
    public static final int KEYCODE_F1 = 131;
    public static final int KEYCODE_F10 = 140;
    public static final int KEYCODE_F11 = 141;
    public static final int KEYCODE_F12 = 142;
    public static final int KEYCODE_F2 = 132;
    public static final int KEYCODE_F3 = 133;
    public static final int KEYCODE_F4 = 134;
    public static final int KEYCODE_F5 = 135;
    public static final int KEYCODE_F6 = 136;
    public static final int KEYCODE_F7 = 137;
    public static final int KEYCODE_F8 = 138;
    public static final int KEYCODE_F9 = 139;
    public static final int KEYCODE_FOCUS = 80;
    public static final int KEYCODE_FORWARD = 125;
    public static final int KEYCODE_FORWARD_DEL = 112;
    public static final int KEYCODE_FUNCTION = 119;
    public static final int KEYCODE_G = 35;
    public static final int KEYCODE_GRAVE = 68;
    public static final int KEYCODE_GUIDE = 172;
    public static final int KEYCODE_H = 36;
    public static final int KEYCODE_HEADSETHOOK = 79;
    public static final int KEYCODE_HENKAN = 214;
    public static final int KEYCODE_HOME = 3;
    public static final int KEYCODE_I = 37;
    public static final int KEYCODE_INFO = 165;
    public static final int KEYCODE_INSERT = 124;
    public static final int KEYCODE_J = 38;
    public static final int KEYCODE_K = 39;
    public static final int KEYCODE_KANA = 218;
    public static final int KEYCODE_KATAKANA_HIRAGANA = 215;
    public static final int KEYCODE_L = 40;
    public static final int KEYCODE_LANGUAGE_SWITCH = 204;
    public static final int KEYCODE_LEFT_BRACKET = 71;
    public static final int KEYCODE_M = 41;
    public static final int KEYCODE_MANNER_MODE = 205;
    public static final int KEYCODE_MEDIA_CLOSE = 128;
    public static final int KEYCODE_MEDIA_EJECT = 129;
    public static final int KEYCODE_MEDIA_FAST_FORWARD = 90;
    public static final int KEYCODE_MEDIA_NEXT = 87;
    public static final int KEYCODE_MEDIA_PAUSE = 127;
    public static final int KEYCODE_MEDIA_PLAY = 126;
    public static final int KEYCODE_MEDIA_PLAY_PAUSE = 85;
    public static final int KEYCODE_MEDIA_PREVIOUS = 88;
    public static final int KEYCODE_MEDIA_RECORD = 130;
    public static final int KEYCODE_MEDIA_REWIND = 89;
    public static final int KEYCODE_MEDIA_STOP = 86;
    public static final int KEYCODE_MENU = 82;
    public static final int KEYCODE_META_LEFT = 117;
    public static final int KEYCODE_META_RIGHT = 118;
    public static final int KEYCODE_MINUS = 69;
    public static final int KEYCODE_MOVE_END = 123;
    public static final int KEYCODE_MOVE_HOME = 122;
    public static final int KEYCODE_MUHENKAN = 213;
    public static final int KEYCODE_MUSIC = 209;
    public static final int KEYCODE_MUTE = 91;
    public static final int KEYCODE_N = 42;
    public static final int KEYCODE_NOTIFICATION = 83;
    public static final int KEYCODE_NUM = 78;
    public static final int KEYCODE_NUMPAD_0 = 144;
    public static final int KEYCODE_NUMPAD_1 = 145;
    public static final int KEYCODE_NUMPAD_2 = 146;
    public static final int KEYCODE_NUMPAD_3 = 147;
    public static final int KEYCODE_NUMPAD_4 = 148;
    public static final int KEYCODE_NUMPAD_5 = 149;
    public static final int KEYCODE_NUMPAD_6 = 150;
    public static final int KEYCODE_NUMPAD_7 = 151;
    public static final int KEYCODE_NUMPAD_8 = 152;
    public static final int KEYCODE_NUMPAD_9 = 153;
    public static final int KEYCODE_NUMPAD_ADD = 157;
    public static final int KEYCODE_NUMPAD_COMMA = 159;
    public static final int KEYCODE_NUMPAD_DIVIDE = 154;
    public static final int KEYCODE_NUMPAD_DOT = 158;
    public static final int KEYCODE_NUMPAD_ENTER = 160;
    public static final int KEYCODE_NUMPAD_EQUALS = 161;
    public static final int KEYCODE_NUMPAD_LEFT_PAREN = 162;
    public static final int KEYCODE_NUMPAD_MULTIPLY = 155;
    public static final int KEYCODE_NUMPAD_RIGHT_PAREN = 163;
    public static final int KEYCODE_NUMPAD_SUBTRACT = 156;
    public static final int KEYCODE_NUM_LOCK = 143;
    public static final int KEYCODE_O = 43;
    public static final int KEYCODE_P = 44;
    public static final int KEYCODE_PAGE_DOWN = 93;
    public static final int KEYCODE_PAGE_UP = 92;
    public static final int KEYCODE_PERIOD = 56;
    public static final int KEYCODE_PICTSYMBOLS = 94;
    public static final int KEYCODE_PLUS = 81;
    public static final int KEYCODE_POUND = 18;
    public static final int KEYCODE_POWER = 26;
    public static final int KEYCODE_PROG_BLUE = 186;
    public static final int KEYCODE_PROG_GREEN = 184;
    public static final int KEYCODE_PROG_RED = 183;
    public static final int KEYCODE_PROG_YELLOW = 185;
    public static final int KEYCODE_Q = 45;
    public static final int KEYCODE_R = 46;
    public static final int KEYCODE_RIGHT_BRACKET = 72;
    public static final int KEYCODE_RO = 217;
    public static final int KEYCODE_S = 47;
    public static final int KEYCODE_SCROLL_LOCK = 116;
    public static final int KEYCODE_SEARCH = 84;
    public static final int KEYCODE_SEMICOLON = 74;
    public static final int KEYCODE_SETTINGS = 176;
    public static final int KEYCODE_SHIFT_LEFT = 59;
    public static final int KEYCODE_SHIFT_RIGHT = 60;
    public static final int KEYCODE_SLASH = 76;
    public static final int KEYCODE_SOFT_LEFT = 1;
    public static final int KEYCODE_SOFT_RIGHT = 2;
    public static final int KEYCODE_SPACE = 62;
    public static final int KEYCODE_STAR = 17;
    public static final int KEYCODE_STB_INPUT = 180;
    public static final int KEYCODE_STB_POWER = 179;
    public static final int KEYCODE_SWITCH_CHARSET = 95;
    public static final int KEYCODE_SYM = 63;
    private static final SparseArray<String> KEYCODE_SYMBOLIC_NAMES = new SparseArray();
    public static final int KEYCODE_SYSRQ = 120;
    public static final int KEYCODE_T = 48;
    public static final int KEYCODE_TAB = 61;
    public static final int KEYCODE_TV = 170;
    public static final int KEYCODE_TV_INPUT = 178;
    public static final int KEYCODE_TV_POWER = 177;
    public static final int KEYCODE_U = 49;
    public static final int KEYCODE_UNKNOWN = 0;
    public static final int KEYCODE_V = 50;
    public static final int KEYCODE_VOLUME_DOWN = 25;
    public static final int KEYCODE_VOLUME_MUTE = 164;
    public static final int KEYCODE_VOLUME_UP = 24;
    public static final int KEYCODE_W = 51;
    public static final int KEYCODE_WINDOW = 171;
    public static final int KEYCODE_X = 52;
    public static final int KEYCODE_Y = 53;
    public static final int KEYCODE_YEN = 216;
    public static final int KEYCODE_Z = 54;
    public static final int KEYCODE_ZENKAKU_HANKAKU = 211;
    public static final int KEYCODE_ZOOM_IN = 168;
    public static final int KEYCODE_ZOOM_OUT = 169;
    private static final int LAST_KEYCODE = 219;

    @Deprecated
    public static final int MAX_KEYCODE = 84;
    private static final int MAX_RECYCLED = 10;
    private static final int META_ALL_MASK = 7827711;
    public static final int META_ALT_LEFT_ON = 16;
    public static final int META_ALT_LOCKED = 512;
    public static final int META_ALT_MASK = 50;
    public static final int META_ALT_ON = 2;
    public static final int META_ALT_RIGHT_ON = 32;
    public static final int META_CAPS_LOCK_ON = 1048576;
    public static final int META_CAP_LOCKED = 256;
    public static final int META_CTRL_LEFT_ON = 8192;
    public static final int META_CTRL_MASK = 28672;
    public static final int META_CTRL_ON = 4096;
    public static final int META_CTRL_RIGHT_ON = 16384;
    public static final int META_FUNCTION_ON = 8;
    private static final int META_INVALID_MODIFIER_MASK = 7343872;
    private static final int META_LOCK_MASK = 7340032;
    public static final int META_META_LEFT_ON = 131072;
    public static final int META_META_MASK = 458752;
    public static final int META_META_ON = 65536;
    public static final int META_META_RIGHT_ON = 262144;
    private static final int META_MODIFIER_MASK = 487679;
    public static final int META_NUM_LOCK_ON = 2097152;
    public static final int META_SCROLL_LOCK_ON = 4194304;
    public static final int META_SELECTING = 2048;
    public static final int META_SHIFT_LEFT_ON = 64;
    public static final int META_SHIFT_MASK = 193;
    public static final int META_SHIFT_ON = 1;
    public static final int META_SHIFT_RIGHT_ON = 128;
    private static final String[] META_SYMBOLIC_NAMES;
    public static final int META_SYM_LOCKED = 1024;
    public static final int META_SYM_ON = 4;
    private static final int META_SYNTHETIC_MASK = 3840;
    static final String TAG = "KeyEvent";
    private static final Object gRecyclerLock;
    private static KeyEvent gRecyclerTop;
    private static int gRecyclerUsed;
    private int mAction;
    private String mCharacters;
    private int mDeviceId;
    private long mDownTime;
    private long mEventTime;
    private int mFlags;
    private int mKeyCode;
    private int mMetaState;
    private KeyEvent mNext;
    private int mRepeatCount;
    private int mScanCode;
    private int mSource;

    static
    {
        String[] arrayOfString = new String[32];
        arrayOfString[0] = "META_SHIFT_ON";
        arrayOfString[1] = "META_ALT_ON";
        arrayOfString[2] = "META_SYM_ON";
        arrayOfString[3] = "META_FUNCTION_ON";
        arrayOfString[4] = "META_ALT_LEFT_ON";
        arrayOfString[5] = "META_ALT_RIGHT_ON";
        arrayOfString[6] = "META_SHIFT_LEFT_ON";
        arrayOfString[7] = "META_SHIFT_RIGHT_ON";
        arrayOfString[8] = "META_CAP_LOCKED";
        arrayOfString[9] = "META_ALT_LOCKED";
        arrayOfString[10] = "META_SYM_LOCKED";
        arrayOfString[11] = "0x00000800";
        arrayOfString[12] = "META_CTRL_ON";
        arrayOfString[13] = "META_CTRL_LEFT_ON";
        arrayOfString[14] = "META_CTRL_RIGHT_ON";
        arrayOfString[15] = "0x00008000";
        arrayOfString[16] = "META_META_ON";
        arrayOfString[17] = "META_META_LEFT_ON";
        arrayOfString[18] = "META_META_RIGHT_ON";
        arrayOfString[19] = "0x00080000";
        arrayOfString[20] = "META_CAPS_LOCK_ON";
        arrayOfString[21] = "META_NUM_LOCK_ON";
        arrayOfString[22] = "META_SCROLL_LOCK_ON";
        arrayOfString[23] = "0x00800000";
        arrayOfString[24] = "0x01000000";
        arrayOfString[25] = "0x02000000";
        arrayOfString[26] = "0x04000000";
        arrayOfString[27] = "0x08000000";
        arrayOfString[28] = "0x10000000";
        arrayOfString[29] = "0x20000000";
        arrayOfString[30] = "0x40000000";
        arrayOfString[31] = "0x80000000";
        META_SYMBOLIC_NAMES = arrayOfString;
        gRecyclerLock = new Object();
        populateKeycodeSymbolicNames();
    }

    private KeyEvent()
    {
    }

    public KeyEvent(int paramInt1, int paramInt2)
    {
        this.mAction = paramInt1;
        this.mKeyCode = paramInt2;
        this.mRepeatCount = 0;
        this.mDeviceId = -1;
    }

    public KeyEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mDownTime = paramLong1;
        this.mEventTime = paramLong2;
        this.mAction = paramInt1;
        this.mKeyCode = paramInt2;
        this.mRepeatCount = paramInt3;
        this.mDeviceId = -1;
    }

    public KeyEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mDownTime = paramLong1;
        this.mEventTime = paramLong2;
        this.mAction = paramInt1;
        this.mKeyCode = paramInt2;
        this.mRepeatCount = paramInt3;
        this.mMetaState = paramInt4;
        this.mDeviceId = -1;
    }

    public KeyEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        this.mDownTime = paramLong1;
        this.mEventTime = paramLong2;
        this.mAction = paramInt1;
        this.mKeyCode = paramInt2;
        this.mRepeatCount = paramInt3;
        this.mMetaState = paramInt4;
        this.mDeviceId = paramInt5;
        this.mScanCode = paramInt6;
    }

    public KeyEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
    {
        this.mDownTime = paramLong1;
        this.mEventTime = paramLong2;
        this.mAction = paramInt1;
        this.mKeyCode = paramInt2;
        this.mRepeatCount = paramInt3;
        this.mMetaState = paramInt4;
        this.mDeviceId = paramInt5;
        this.mScanCode = paramInt6;
        this.mFlags = paramInt7;
    }

    public KeyEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        this.mDownTime = paramLong1;
        this.mEventTime = paramLong2;
        this.mAction = paramInt1;
        this.mKeyCode = paramInt2;
        this.mRepeatCount = paramInt3;
        this.mMetaState = paramInt4;
        this.mDeviceId = paramInt5;
        this.mScanCode = paramInt6;
        this.mFlags = paramInt7;
        this.mSource = paramInt8;
    }

    public KeyEvent(long paramLong, String paramString, int paramInt1, int paramInt2)
    {
        this.mDownTime = paramLong;
        this.mEventTime = paramLong;
        this.mCharacters = paramString;
        this.mAction = 2;
        this.mKeyCode = 0;
        this.mRepeatCount = 0;
        this.mDeviceId = paramInt1;
        this.mFlags = paramInt2;
        this.mSource = 257;
    }

    private KeyEvent(Parcel paramParcel)
    {
        this.mDeviceId = paramParcel.readInt();
        this.mSource = paramParcel.readInt();
        this.mAction = paramParcel.readInt();
        this.mKeyCode = paramParcel.readInt();
        this.mRepeatCount = paramParcel.readInt();
        this.mMetaState = paramParcel.readInt();
        this.mScanCode = paramParcel.readInt();
        this.mFlags = paramParcel.readInt();
        this.mDownTime = paramParcel.readLong();
        this.mEventTime = paramParcel.readLong();
    }

    public KeyEvent(KeyEvent paramKeyEvent)
    {
        this.mDownTime = paramKeyEvent.mDownTime;
        this.mEventTime = paramKeyEvent.mEventTime;
        this.mAction = paramKeyEvent.mAction;
        this.mKeyCode = paramKeyEvent.mKeyCode;
        this.mRepeatCount = paramKeyEvent.mRepeatCount;
        this.mMetaState = paramKeyEvent.mMetaState;
        this.mDeviceId = paramKeyEvent.mDeviceId;
        this.mSource = paramKeyEvent.mSource;
        this.mScanCode = paramKeyEvent.mScanCode;
        this.mFlags = paramKeyEvent.mFlags;
        this.mCharacters = paramKeyEvent.mCharacters;
    }

    private KeyEvent(KeyEvent paramKeyEvent, int paramInt)
    {
        this.mDownTime = paramKeyEvent.mDownTime;
        this.mEventTime = paramKeyEvent.mEventTime;
        this.mAction = paramInt;
        this.mKeyCode = paramKeyEvent.mKeyCode;
        this.mRepeatCount = paramKeyEvent.mRepeatCount;
        this.mMetaState = paramKeyEvent.mMetaState;
        this.mDeviceId = paramKeyEvent.mDeviceId;
        this.mSource = paramKeyEvent.mSource;
        this.mScanCode = paramKeyEvent.mScanCode;
        this.mFlags = paramKeyEvent.mFlags;
    }

    @Deprecated
    public KeyEvent(KeyEvent paramKeyEvent, long paramLong, int paramInt)
    {
        this.mDownTime = paramKeyEvent.mDownTime;
        this.mEventTime = paramLong;
        this.mAction = paramKeyEvent.mAction;
        this.mKeyCode = paramKeyEvent.mKeyCode;
        this.mRepeatCount = paramInt;
        this.mMetaState = paramKeyEvent.mMetaState;
        this.mDeviceId = paramKeyEvent.mDeviceId;
        this.mSource = paramKeyEvent.mSource;
        this.mScanCode = paramKeyEvent.mScanCode;
        this.mFlags = paramKeyEvent.mFlags;
        this.mCharacters = paramKeyEvent.mCharacters;
    }

    public static String actionToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = Integer.toString(paramInt);
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return str;
            str = "ACTION_DOWN";
            continue;
            str = "ACTION_UP";
            continue;
            str = "ACTION_MULTIPLE";
        }
    }

    public static KeyEvent changeAction(KeyEvent paramKeyEvent, int paramInt)
    {
        return new KeyEvent(paramKeyEvent, paramInt);
    }

    public static KeyEvent changeFlags(KeyEvent paramKeyEvent, int paramInt)
    {
        KeyEvent localKeyEvent = new KeyEvent(paramKeyEvent);
        localKeyEvent.mFlags = paramInt;
        return localKeyEvent;
    }

    public static KeyEvent changeTimeRepeat(KeyEvent paramKeyEvent, long paramLong, int paramInt)
    {
        return new KeyEvent(paramKeyEvent, paramLong, paramInt);
    }

    public static KeyEvent changeTimeRepeat(KeyEvent paramKeyEvent, long paramLong, int paramInt1, int paramInt2)
    {
        KeyEvent localKeyEvent = new KeyEvent(paramKeyEvent);
        localKeyEvent.mEventTime = paramLong;
        localKeyEvent.mRepeatCount = paramInt1;
        localKeyEvent.mFlags = paramInt2;
        return localKeyEvent;
    }

    public static KeyEvent createFromParcelBody(Parcel paramParcel)
    {
        return new KeyEvent(paramParcel);
    }

    public static int getDeadChar(int paramInt1, int paramInt2)
    {
        return KeyCharacterMap.getDeadChar(paramInt1, paramInt2);
    }

    public static int getMaxKeyCode()
    {
        return 219;
    }

    public static int getModifierMetaStateMask()
    {
        return 487679;
    }

    public static final boolean isGamepadButton(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 96:
        case 97:
        case 98:
        case 99:
        case 100:
        case 101:
        case 102:
        case 103:
        case 104:
        case 105:
        case 106:
        case 107:
        case 108:
        case 109:
        case 110:
        case 188:
        case 189:
        case 190:
        case 191:
        case 192:
        case 193:
        case 194:
        case 195:
        case 196:
        case 197:
        case 198:
        case 199:
        case 200:
        case 201:
        case 202:
        case 203:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public static boolean isModifierKey(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 57:
        case 58:
        case 59:
        case 60:
        case 63:
        case 78:
        case 113:
        case 114:
        case 117:
        case 118:
        case 119:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public static int keyCodeFromString(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("symbolicName must not be null");
        int i = KEYCODE_SYMBOLIC_NAMES.size();
        int j = 0;
        if (j < i)
            if (!paramString.equals(KEYCODE_SYMBOLIC_NAMES.valueAt(j)));
        while (true)
        {
            return j;
            j++;
            break;
            try
            {
                int k = Integer.parseInt(paramString, 10);
                j = k;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                j = 0;
            }
        }
    }

    public static String keyCodeToString(int paramInt)
    {
        String str = (String)KEYCODE_SYMBOLIC_NAMES.get(paramInt);
        if (str != null);
        while (true)
        {
            return str;
            str = Integer.toString(paramInt);
        }
    }

    private static int metaStateFilterDirectionalModifiers(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        int i = 1;
        int j;
        int k;
        if ((paramInt2 & paramInt3) != 0)
        {
            j = i;
            k = paramInt4 | paramInt5;
            if ((paramInt2 & k) == 0)
                break label100;
        }
        while (true)
            if (j != 0)
            {
                if (i != 0)
                {
                    throw new IllegalArgumentException("modifiers must not contain " + metaStateToString(paramInt3) + " combined with " + metaStateToString(paramInt4) + " or " + metaStateToString(paramInt5));
                    j = 0;
                    break;
                    label100: i = 0;
                    continue;
                }
                paramInt1 &= (k ^ 0xFFFFFFFF);
            }
        while (true)
        {
            return paramInt1;
            if (i != 0)
                paramInt1 &= (paramInt3 ^ 0xFFFFFFFF);
        }
    }

    public static boolean metaStateHasModifiers(int paramInt1, int paramInt2)
    {
        int i = 1;
        if ((0x700F00 & paramInt2) != 0)
            throw new IllegalArgumentException("modifiers must not contain META_CAPS_LOCK_ON, META_NUM_LOCK_ON, META_SCROLL_LOCK_ON, META_CAP_LOCKED, META_ALT_LOCKED, META_SYM_LOCKED, or META_SELECTING");
        if (metaStateFilterDirectionalModifiers(metaStateFilterDirectionalModifiers(metaStateFilterDirectionalModifiers(metaStateFilterDirectionalModifiers(0x770FF & normalizeMetaState(paramInt1), paramInt2, i, 64, 128), paramInt2, 2, 16, 32), paramInt2, 4096, 8192, 16384), paramInt2, 65536, 131072, 262144) == paramInt2);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean metaStateHasNoModifiers(int paramInt)
    {
        if ((0x770FF & normalizeMetaState(paramInt)) == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static String metaStateToString(int paramInt)
    {
        String str;
        if (paramInt == 0)
            str = "0";
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = null;
            int i = 0;
            if (paramInt != 0)
            {
                int j;
                if ((paramInt & 0x1) != 0)
                {
                    j = 1;
                    label27: paramInt >>>= 1;
                    if (j != 0)
                    {
                        str = META_SYMBOLIC_NAMES[i];
                        if (localStringBuilder != null)
                            break label71;
                        if (paramInt == 0)
                            continue;
                        localStringBuilder = new StringBuilder(str);
                    }
                }
                else
                {
                    while (true)
                    {
                        i++;
                        break;
                        j = 0;
                        break label27;
                        label71: localStringBuilder.append('|');
                        localStringBuilder.append(str);
                    }
                }
            }
            else
            {
                str = localStringBuilder.toString();
            }
        }
    }

    private native boolean native_hasDefaultAction(int paramInt);

    private native boolean native_isSystemKey(int paramInt);

    public static int normalizeMetaState(int paramInt)
    {
        if ((paramInt & 0xC0) != 0)
            paramInt |= 1;
        if ((paramInt & 0x30) != 0)
            paramInt |= 2;
        if ((paramInt & 0x6000) != 0)
            paramInt |= 4096;
        if ((0x60000 & paramInt) != 0)
            paramInt |= 65536;
        if ((paramInt & 0x100) != 0)
            paramInt |= 1048576;
        if ((paramInt & 0x200) != 0)
            paramInt |= 2;
        if ((paramInt & 0x400) != 0)
            paramInt |= 4;
        return 0x7770FF & paramInt;
    }

    private static KeyEvent obtain()
    {
        KeyEvent localKeyEvent;
        synchronized (gRecyclerLock)
        {
            localKeyEvent = gRecyclerTop;
            if (localKeyEvent == null)
            {
                localKeyEvent = new KeyEvent();
            }
            else
            {
                gRecyclerTop = localKeyEvent.mNext;
                gRecyclerUsed = -1 + gRecyclerUsed;
                localKeyEvent.mNext = null;
                localKeyEvent.prepareForReuse();
            }
        }
        return localKeyEvent;
    }

    public static KeyEvent obtain(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, String paramString)
    {
        KeyEvent localKeyEvent = obtain();
        localKeyEvent.mDownTime = paramLong1;
        localKeyEvent.mEventTime = paramLong2;
        localKeyEvent.mAction = paramInt1;
        localKeyEvent.mKeyCode = paramInt2;
        localKeyEvent.mRepeatCount = paramInt3;
        localKeyEvent.mMetaState = paramInt4;
        localKeyEvent.mDeviceId = paramInt5;
        localKeyEvent.mScanCode = paramInt6;
        localKeyEvent.mFlags = paramInt7;
        localKeyEvent.mSource = paramInt8;
        localKeyEvent.mCharacters = paramString;
        return localKeyEvent;
    }

    public static KeyEvent obtain(KeyEvent paramKeyEvent)
    {
        KeyEvent localKeyEvent = obtain();
        localKeyEvent.mDownTime = paramKeyEvent.mDownTime;
        localKeyEvent.mEventTime = paramKeyEvent.mEventTime;
        localKeyEvent.mAction = paramKeyEvent.mAction;
        localKeyEvent.mKeyCode = paramKeyEvent.mKeyCode;
        localKeyEvent.mRepeatCount = paramKeyEvent.mRepeatCount;
        localKeyEvent.mMetaState = paramKeyEvent.mMetaState;
        localKeyEvent.mDeviceId = paramKeyEvent.mDeviceId;
        localKeyEvent.mScanCode = paramKeyEvent.mScanCode;
        localKeyEvent.mFlags = paramKeyEvent.mFlags;
        localKeyEvent.mSource = paramKeyEvent.mSource;
        localKeyEvent.mCharacters = paramKeyEvent.mCharacters;
        return localKeyEvent;
    }

    private static void populateKeycodeSymbolicNames()
    {
        SparseArray localSparseArray = KEYCODE_SYMBOLIC_NAMES;
        localSparseArray.append(0, "KEYCODE_UNKNOWN");
        localSparseArray.append(1, "KEYCODE_SOFT_LEFT");
        localSparseArray.append(2, "KEYCODE_SOFT_RIGHT");
        localSparseArray.append(3, "KEYCODE_HOME");
        localSparseArray.append(4, "KEYCODE_BACK");
        localSparseArray.append(5, "KEYCODE_CALL");
        localSparseArray.append(6, "KEYCODE_ENDCALL");
        localSparseArray.append(7, "KEYCODE_0");
        localSparseArray.append(8, "KEYCODE_1");
        localSparseArray.append(9, "KEYCODE_2");
        localSparseArray.append(10, "KEYCODE_3");
        localSparseArray.append(11, "KEYCODE_4");
        localSparseArray.append(12, "KEYCODE_5");
        localSparseArray.append(13, "KEYCODE_6");
        localSparseArray.append(14, "KEYCODE_7");
        localSparseArray.append(15, "KEYCODE_8");
        localSparseArray.append(16, "KEYCODE_9");
        localSparseArray.append(17, "KEYCODE_STAR");
        localSparseArray.append(18, "KEYCODE_POUND");
        localSparseArray.append(19, "KEYCODE_DPAD_UP");
        localSparseArray.append(20, "KEYCODE_DPAD_DOWN");
        localSparseArray.append(21, "KEYCODE_DPAD_LEFT");
        localSparseArray.append(22, "KEYCODE_DPAD_RIGHT");
        localSparseArray.append(23, "KEYCODE_DPAD_CENTER");
        localSparseArray.append(24, "KEYCODE_VOLUME_UP");
        localSparseArray.append(25, "KEYCODE_VOLUME_DOWN");
        localSparseArray.append(26, "KEYCODE_POWER");
        localSparseArray.append(27, "KEYCODE_CAMERA");
        localSparseArray.append(28, "KEYCODE_CLEAR");
        localSparseArray.append(29, "KEYCODE_A");
        localSparseArray.append(30, "KEYCODE_B");
        localSparseArray.append(31, "KEYCODE_C");
        localSparseArray.append(32, "KEYCODE_D");
        localSparseArray.append(33, "KEYCODE_E");
        localSparseArray.append(34, "KEYCODE_F");
        localSparseArray.append(35, "KEYCODE_G");
        localSparseArray.append(36, "KEYCODE_H");
        localSparseArray.append(37, "KEYCODE_I");
        localSparseArray.append(38, "KEYCODE_J");
        localSparseArray.append(39, "KEYCODE_K");
        localSparseArray.append(40, "KEYCODE_L");
        localSparseArray.append(41, "KEYCODE_M");
        localSparseArray.append(42, "KEYCODE_N");
        localSparseArray.append(43, "KEYCODE_O");
        localSparseArray.append(44, "KEYCODE_P");
        localSparseArray.append(45, "KEYCODE_Q");
        localSparseArray.append(46, "KEYCODE_R");
        localSparseArray.append(47, "KEYCODE_S");
        localSparseArray.append(48, "KEYCODE_T");
        localSparseArray.append(49, "KEYCODE_U");
        localSparseArray.append(50, "KEYCODE_V");
        localSparseArray.append(51, "KEYCODE_W");
        localSparseArray.append(52, "KEYCODE_X");
        localSparseArray.append(53, "KEYCODE_Y");
        localSparseArray.append(54, "KEYCODE_Z");
        localSparseArray.append(55, "KEYCODE_COMMA");
        localSparseArray.append(56, "KEYCODE_PERIOD");
        localSparseArray.append(57, "KEYCODE_ALT_LEFT");
        localSparseArray.append(58, "KEYCODE_ALT_RIGHT");
        localSparseArray.append(59, "KEYCODE_SHIFT_LEFT");
        localSparseArray.append(60, "KEYCODE_SHIFT_RIGHT");
        localSparseArray.append(61, "KEYCODE_TAB");
        localSparseArray.append(62, "KEYCODE_SPACE");
        localSparseArray.append(63, "KEYCODE_SYM");
        localSparseArray.append(64, "KEYCODE_EXPLORER");
        localSparseArray.append(65, "KEYCODE_ENVELOPE");
        localSparseArray.append(66, "KEYCODE_ENTER");
        localSparseArray.append(67, "KEYCODE_DEL");
        localSparseArray.append(68, "KEYCODE_GRAVE");
        localSparseArray.append(69, "KEYCODE_MINUS");
        localSparseArray.append(70, "KEYCODE_EQUALS");
        localSparseArray.append(71, "KEYCODE_LEFT_BRACKET");
        localSparseArray.append(72, "KEYCODE_RIGHT_BRACKET");
        localSparseArray.append(73, "KEYCODE_BACKSLASH");
        localSparseArray.append(74, "KEYCODE_SEMICOLON");
        localSparseArray.append(75, "KEYCODE_APOSTROPHE");
        localSparseArray.append(76, "KEYCODE_SLASH");
        localSparseArray.append(77, "KEYCODE_AT");
        localSparseArray.append(78, "KEYCODE_NUM");
        localSparseArray.append(79, "KEYCODE_HEADSETHOOK");
        localSparseArray.append(80, "KEYCODE_FOCUS");
        localSparseArray.append(81, "KEYCODE_PLUS");
        localSparseArray.append(82, "KEYCODE_MENU");
        localSparseArray.append(83, "KEYCODE_NOTIFICATION");
        localSparseArray.append(84, "KEYCODE_SEARCH");
        localSparseArray.append(85, "KEYCODE_MEDIA_PLAY_PAUSE");
        localSparseArray.append(86, "KEYCODE_MEDIA_STOP");
        localSparseArray.append(87, "KEYCODE_MEDIA_NEXT");
        localSparseArray.append(88, "KEYCODE_MEDIA_PREVIOUS");
        localSparseArray.append(89, "KEYCODE_MEDIA_REWIND");
        localSparseArray.append(90, "KEYCODE_MEDIA_FAST_FORWARD");
        localSparseArray.append(91, "KEYCODE_MUTE");
        localSparseArray.append(92, "KEYCODE_PAGE_UP");
        localSparseArray.append(93, "KEYCODE_PAGE_DOWN");
        localSparseArray.append(94, "KEYCODE_PICTSYMBOLS");
        localSparseArray.append(95, "KEYCODE_SWITCH_CHARSET");
        localSparseArray.append(96, "KEYCODE_BUTTON_A");
        localSparseArray.append(97, "KEYCODE_BUTTON_B");
        localSparseArray.append(98, "KEYCODE_BUTTON_C");
        localSparseArray.append(99, "KEYCODE_BUTTON_X");
        localSparseArray.append(100, "KEYCODE_BUTTON_Y");
        localSparseArray.append(101, "KEYCODE_BUTTON_Z");
        localSparseArray.append(102, "KEYCODE_BUTTON_L1");
        localSparseArray.append(103, "KEYCODE_BUTTON_R1");
        localSparseArray.append(104, "KEYCODE_BUTTON_L2");
        localSparseArray.append(105, "KEYCODE_BUTTON_R2");
        localSparseArray.append(106, "KEYCODE_BUTTON_THUMBL");
        localSparseArray.append(107, "KEYCODE_BUTTON_THUMBR");
        localSparseArray.append(108, "KEYCODE_BUTTON_START");
        localSparseArray.append(109, "KEYCODE_BUTTON_SELECT");
        localSparseArray.append(110, "KEYCODE_BUTTON_MODE");
        localSparseArray.append(111, "KEYCODE_ESCAPE");
        localSparseArray.append(112, "KEYCODE_FORWARD_DEL");
        localSparseArray.append(113, "KEYCODE_CTRL_LEFT");
        localSparseArray.append(114, "KEYCODE_CTRL_RIGHT");
        localSparseArray.append(115, "KEYCODE_CAPS_LOCK");
        localSparseArray.append(116, "KEYCODE_SCROLL_LOCK");
        localSparseArray.append(117, "KEYCODE_META_LEFT");
        localSparseArray.append(118, "KEYCODE_META_RIGHT");
        localSparseArray.append(119, "KEYCODE_FUNCTION");
        localSparseArray.append(120, "KEYCODE_SYSRQ");
        localSparseArray.append(121, "KEYCODE_BREAK");
        localSparseArray.append(122, "KEYCODE_MOVE_HOME");
        localSparseArray.append(123, "KEYCODE_MOVE_END");
        localSparseArray.append(124, "KEYCODE_INSERT");
        localSparseArray.append(125, "KEYCODE_FORWARD");
        localSparseArray.append(126, "KEYCODE_MEDIA_PLAY");
        localSparseArray.append(127, "KEYCODE_MEDIA_PAUSE");
        localSparseArray.append(128, "KEYCODE_MEDIA_CLOSE");
        localSparseArray.append(129, "KEYCODE_MEDIA_EJECT");
        localSparseArray.append(130, "KEYCODE_MEDIA_RECORD");
        localSparseArray.append(131, "KEYCODE_F1");
        localSparseArray.append(132, "KEYCODE_F2");
        localSparseArray.append(133, "KEYCODE_F3");
        localSparseArray.append(134, "KEYCODE_F4");
        localSparseArray.append(135, "KEYCODE_F5");
        localSparseArray.append(136, "KEYCODE_F6");
        localSparseArray.append(137, "KEYCODE_F7");
        localSparseArray.append(138, "KEYCODE_F8");
        localSparseArray.append(139, "KEYCODE_F9");
        localSparseArray.append(140, "KEYCODE_F10");
        localSparseArray.append(141, "KEYCODE_F11");
        localSparseArray.append(142, "KEYCODE_F12");
        localSparseArray.append(143, "KEYCODE_NUM_LOCK");
        localSparseArray.append(144, "KEYCODE_NUMPAD_0");
        localSparseArray.append(145, "KEYCODE_NUMPAD_1");
        localSparseArray.append(146, "KEYCODE_NUMPAD_2");
        localSparseArray.append(147, "KEYCODE_NUMPAD_3");
        localSparseArray.append(148, "KEYCODE_NUMPAD_4");
        localSparseArray.append(149, "KEYCODE_NUMPAD_5");
        localSparseArray.append(150, "KEYCODE_NUMPAD_6");
        localSparseArray.append(151, "KEYCODE_NUMPAD_7");
        localSparseArray.append(152, "KEYCODE_NUMPAD_8");
        localSparseArray.append(153, "KEYCODE_NUMPAD_9");
        localSparseArray.append(154, "KEYCODE_NUMPAD_DIVIDE");
        localSparseArray.append(155, "KEYCODE_NUMPAD_MULTIPLY");
        localSparseArray.append(156, "KEYCODE_NUMPAD_SUBTRACT");
        localSparseArray.append(157, "KEYCODE_NUMPAD_ADD");
        localSparseArray.append(158, "KEYCODE_NUMPAD_DOT");
        localSparseArray.append(159, "KEYCODE_NUMPAD_COMMA");
        localSparseArray.append(160, "KEYCODE_NUMPAD_ENTER");
        localSparseArray.append(161, "KEYCODE_NUMPAD_EQUALS");
        localSparseArray.append(162, "KEYCODE_NUMPAD_LEFT_PAREN");
        localSparseArray.append(163, "KEYCODE_NUMPAD_RIGHT_PAREN");
        localSparseArray.append(164, "KEYCODE_VOLUME_MUTE");
        localSparseArray.append(165, "KEYCODE_INFO");
        localSparseArray.append(166, "KEYCODE_CHANNEL_UP");
        localSparseArray.append(167, "KEYCODE_CHANNEL_DOWN");
        localSparseArray.append(168, "KEYCODE_ZOOM_IN");
        localSparseArray.append(169, "KEYCODE_ZOOM_OUT");
        localSparseArray.append(170, "KEYCODE_TV");
        localSparseArray.append(171, "KEYCODE_WINDOW");
        localSparseArray.append(172, "KEYCODE_GUIDE");
        localSparseArray.append(173, "KEYCODE_DVR");
        localSparseArray.append(174, "KEYCODE_BOOKMARK");
        localSparseArray.append(175, "KEYCODE_CAPTIONS");
        localSparseArray.append(176, "KEYCODE_SETTINGS");
        localSparseArray.append(177, "KEYCODE_TV_POWER");
        localSparseArray.append(178, "KEYCODE_TV_INPUT");
        localSparseArray.append(180, "KEYCODE_STB_INPUT");
        localSparseArray.append(179, "KEYCODE_STB_POWER");
        localSparseArray.append(181, "KEYCODE_AVR_POWER");
        localSparseArray.append(182, "KEYCODE_AVR_INPUT");
        localSparseArray.append(183, "KEYCODE_PROG_RED");
        localSparseArray.append(184, "KEYCODE_PROG_GREEN");
        localSparseArray.append(185, "KEYCODE_PROG_YELLOW");
        localSparseArray.append(186, "KEYCODE_PROG_BLUE");
        localSparseArray.append(187, "KEYCODE_APP_SWITCH");
        localSparseArray.append(188, "KEYCODE_BUTTON_1");
        localSparseArray.append(189, "KEYCODE_BUTTON_2");
        localSparseArray.append(190, "KEYCODE_BUTTON_3");
        localSparseArray.append(191, "KEYCODE_BUTTON_4");
        localSparseArray.append(192, "KEYCODE_BUTTON_5");
        localSparseArray.append(193, "KEYCODE_BUTTON_6");
        localSparseArray.append(194, "KEYCODE_BUTTON_7");
        localSparseArray.append(195, "KEYCODE_BUTTON_8");
        localSparseArray.append(196, "KEYCODE_BUTTON_9");
        localSparseArray.append(197, "KEYCODE_BUTTON_10");
        localSparseArray.append(198, "KEYCODE_BUTTON_11");
        localSparseArray.append(199, "KEYCODE_BUTTON_12");
        localSparseArray.append(200, "KEYCODE_BUTTON_13");
        localSparseArray.append(201, "KEYCODE_BUTTON_14");
        localSparseArray.append(202, "KEYCODE_BUTTON_15");
        localSparseArray.append(203, "KEYCODE_BUTTON_16");
        localSparseArray.append(204, "KEYCODE_LANGUAGE_SWITCH");
        localSparseArray.append(205, "KEYCODE_MANNER_MODE");
        localSparseArray.append(206, "KEYCODE_3D_MODE");
        localSparseArray.append(207, "KEYCODE_CONTACTS");
        localSparseArray.append(208, "KEYCODE_CALENDAR");
        localSparseArray.append(209, "KEYCODE_MUSIC");
        localSparseArray.append(210, "KEYCODE_CALCULATOR");
        localSparseArray.append(211, "KEYCODE_ZENKAKU_HANKAKU");
        localSparseArray.append(212, "KEYCODE_EISU");
        localSparseArray.append(213, "KEYCODE_MUHENKAN");
        localSparseArray.append(214, "KEYCODE_HENKAN");
        localSparseArray.append(215, "KEYCODE_KATAKANA_HIRAGANA");
        localSparseArray.append(216, "KEYCODE_YEN");
        localSparseArray.append(217, "KEYCODE_RO");
        localSparseArray.append(218, "KEYCODE_KANA");
        localSparseArray.append(219, "KEYCODE_ASSIST");
    }

    public KeyEvent copy()
    {
        return obtain(this);
    }

    @Deprecated
    public final boolean dispatch(Callback paramCallback)
    {
        return dispatch(paramCallback, null, null);
    }

    public final boolean dispatch(Callback paramCallback, DispatcherState paramDispatcherState, Object paramObject)
    {
        int i = 1;
        switch (this.mAction)
        {
        default:
            i = 0;
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            this.mFlags = (0xBFFFFFFF & this.mFlags);
            i = paramCallback.onKeyDown(this.mKeyCode, this);
            if (paramDispatcherState != null)
                if ((i != 0) && (this.mRepeatCount == 0) && ((0x40000000 & this.mFlags) != 0))
                    paramDispatcherState.startTracking(this, paramObject);
                else if ((isLongPress()) && (paramDispatcherState.isTracking(this)))
                    try
                    {
                        if (paramCallback.onKeyLongPress(this.mKeyCode, this))
                        {
                            paramDispatcherState.performedLongPress(this);
                            i = 1;
                            continue;
                            if (paramDispatcherState != null)
                                paramDispatcherState.handleUpEvent(this);
                            i = paramCallback.onKeyUp(this.mKeyCode, this);
                            continue;
                            int j = this.mRepeatCount;
                            int k = this.mKeyCode;
                            if (!paramCallback.onKeyMultiple(k, j, this))
                                if (k != 0)
                                {
                                    this.mAction = 0;
                                    this.mRepeatCount = 0;
                                    int m = paramCallback.onKeyDown(k, this);
                                    if (m != 0)
                                    {
                                        this.mAction = i;
                                        paramCallback.onKeyUp(k, this);
                                    }
                                    this.mAction = 2;
                                    this.mRepeatCount = j;
                                    i = m;
                                }
                                else
                                {
                                    i = 0;
                                }
                        }
                    }
                    catch (AbstractMethodError localAbstractMethodError)
                    {
                    }
        }
    }

    public final int getAction()
    {
        return this.mAction;
    }

    public final String getCharacters()
    {
        return this.mCharacters;
    }

    public final int getDeviceId()
    {
        return this.mDeviceId;
    }

    public char getDisplayLabel()
    {
        return getKeyCharacterMap().getDisplayLabel(this.mKeyCode);
    }

    public final long getDownTime()
    {
        return this.mDownTime;
    }

    public final long getEventTime()
    {
        return this.mEventTime;
    }

    public final long getEventTimeNano()
    {
        return 1000000L * this.mEventTime;
    }

    public final int getFlags()
    {
        return this.mFlags;
    }

    public final KeyCharacterMap getKeyCharacterMap()
    {
        return KeyCharacterMap.load(this.mDeviceId);
    }

    public final int getKeyCode()
    {
        return this.mKeyCode;
    }

    @Deprecated
    public boolean getKeyData(KeyCharacterMap.KeyData paramKeyData)
    {
        return getKeyCharacterMap().getKeyData(this.mKeyCode, paramKeyData);
    }

    @Deprecated
    public final int getKeyboardDevice()
    {
        return this.mDeviceId;
    }

    public char getMatch(char[] paramArrayOfChar)
    {
        return getMatch(paramArrayOfChar, 0);
    }

    public char getMatch(char[] paramArrayOfChar, int paramInt)
    {
        return getKeyCharacterMap().getMatch(this.mKeyCode, paramArrayOfChar, paramInt);
    }

    public final int getMetaState()
    {
        return this.mMetaState;
    }

    public final int getModifiers()
    {
        return 0x770FF & normalizeMetaState(this.mMetaState);
    }

    public char getNumber()
    {
        return getKeyCharacterMap().getNumber(this.mKeyCode);
    }

    public final int getRepeatCount()
    {
        return this.mRepeatCount;
    }

    public final int getScanCode()
    {
        return this.mScanCode;
    }

    public final int getSource()
    {
        return this.mSource;
    }

    public int getUnicodeChar()
    {
        return getUnicodeChar(this.mMetaState);
    }

    public int getUnicodeChar(int paramInt)
    {
        return getKeyCharacterMap().get(this.mKeyCode, paramInt);
    }

    public final boolean hasDefaultAction()
    {
        return native_hasDefaultAction(this.mKeyCode);
    }

    public final boolean hasModifiers(int paramInt)
    {
        return metaStateHasModifiers(this.mMetaState, paramInt);
    }

    public final boolean hasNoModifiers()
    {
        return metaStateHasNoModifiers(this.mMetaState);
    }

    public final boolean isAltPressed()
    {
        if ((0x2 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isCanceled()
    {
        if ((0x20 & this.mFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isCapsLockOn()
    {
        if ((0x100000 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isCtrlPressed()
    {
        if ((0x1000 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @Deprecated
    public final boolean isDown()
    {
        if (this.mAction == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isFunctionPressed()
    {
        if ((0x8 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isLongPress()
    {
        if ((0x80 & this.mFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isMetaPressed()
    {
        if ((0x10000 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isNumLockOn()
    {
        if ((0x200000 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPrintingKey()
    {
        return getKeyCharacterMap().isPrintingKey(this.mKeyCode);
    }

    public final boolean isScrollLockOn()
    {
        if ((0x400000 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isShiftPressed()
    {
        if ((0x1 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isSymPressed()
    {
        if ((0x4 & this.mMetaState) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isSystem()
    {
        return native_isSystemKey(this.mKeyCode);
    }

    public final boolean isTainted()
    {
        if ((0x80000000 & this.mFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isTracking()
    {
        if ((0x200 & this.mFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final void recycle()
    {
        super.recycle();
        this.mCharacters = null;
        synchronized (gRecyclerLock)
        {
            if (gRecyclerUsed < 10)
            {
                gRecyclerUsed = 1 + gRecyclerUsed;
                this.mNext = gRecyclerTop;
                gRecyclerTop = this;
            }
            return;
        }
    }

    public final void recycleIfNeededAfterDispatch()
    {
    }

    public final void setSource(int paramInt)
    {
        this.mSource = paramInt;
    }

    public final void setTainted(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 0x80000000 | this.mFlags; ; i = 0x7FFFFFFF & this.mFlags)
        {
            this.mFlags = i;
            return;
        }
    }

    public final void startTracking()
    {
        this.mFlags = (0x40000000 | this.mFlags);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("KeyEvent { action=").append(actionToString(this.mAction));
        localStringBuilder.append(", keyCode=").append(keyCodeToString(this.mKeyCode));
        localStringBuilder.append(", scanCode=").append(this.mScanCode);
        if (this.mCharacters != null)
            localStringBuilder.append(", characters=\"").append(this.mCharacters).append("\"");
        localStringBuilder.append(", metaState=").append(metaStateToString(this.mMetaState));
        localStringBuilder.append(", flags=0x").append(Integer.toHexString(this.mFlags));
        localStringBuilder.append(", repeatCount=").append(this.mRepeatCount);
        localStringBuilder.append(", eventTime=").append(this.mEventTime);
        localStringBuilder.append(", downTime=").append(this.mDownTime);
        localStringBuilder.append(", deviceId=").append(this.mDeviceId);
        localStringBuilder.append(", source=0x").append(Integer.toHexString(this.mSource));
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(2);
        paramParcel.writeInt(this.mDeviceId);
        paramParcel.writeInt(this.mSource);
        paramParcel.writeInt(this.mAction);
        paramParcel.writeInt(this.mKeyCode);
        paramParcel.writeInt(this.mRepeatCount);
        paramParcel.writeInt(this.mMetaState);
        paramParcel.writeInt(this.mScanCode);
        paramParcel.writeInt(this.mFlags);
        paramParcel.writeLong(this.mDownTime);
        paramParcel.writeLong(this.mEventTime);
    }

    public static class DispatcherState
    {
        SparseIntArray mActiveLongPresses = new SparseIntArray();
        int mDownKeyCode;
        Object mDownTarget;

        public void handleUpEvent(KeyEvent paramKeyEvent)
        {
            int i = paramKeyEvent.getKeyCode();
            int j = this.mActiveLongPresses.indexOfKey(i);
            if (j >= 0)
            {
                KeyEvent.access$076(paramKeyEvent, 288);
                this.mActiveLongPresses.removeAt(j);
            }
            if (this.mDownKeyCode == i)
            {
                KeyEvent.access$076(paramKeyEvent, 512);
                this.mDownKeyCode = 0;
                this.mDownTarget = null;
            }
        }

        public boolean isTracking(KeyEvent paramKeyEvent)
        {
            if (this.mDownKeyCode == paramKeyEvent.getKeyCode());
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void performedLongPress(KeyEvent paramKeyEvent)
        {
            this.mActiveLongPresses.put(paramKeyEvent.getKeyCode(), 1);
        }

        public void reset()
        {
            this.mDownKeyCode = 0;
            this.mDownTarget = null;
            this.mActiveLongPresses.clear();
        }

        public void reset(Object paramObject)
        {
            if (this.mDownTarget == paramObject)
            {
                this.mDownKeyCode = 0;
                this.mDownTarget = null;
            }
        }

        public void startTracking(KeyEvent paramKeyEvent, Object paramObject)
        {
            if (paramKeyEvent.getAction() != 0)
                throw new IllegalArgumentException("Can only start tracking on a down event");
            this.mDownKeyCode = paramKeyEvent.getKeyCode();
            this.mDownTarget = paramObject;
        }
    }

    public static abstract interface Callback
    {
        public abstract boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent);

        public abstract boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent);

        public abstract boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent);

        public abstract boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.KeyEvent
 * JD-Core Version:        0.6.2
 */