package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IApplicationToken extends IInterface
{
    public abstract long getKeyDispatchingTimeout()
        throws RemoteException;

    public abstract boolean keyDispatchingTimedOut()
        throws RemoteException;

    public abstract void windowsDrawn()
        throws RemoteException;

    public abstract void windowsGone()
        throws RemoteException;

    public abstract void windowsVisible()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IApplicationToken
    {
        private static final String DESCRIPTOR = "android.view.IApplicationToken";
        static final int TRANSACTION_getKeyDispatchingTimeout = 5;
        static final int TRANSACTION_keyDispatchingTimedOut = 4;
        static final int TRANSACTION_windowsDrawn = 1;
        static final int TRANSACTION_windowsGone = 3;
        static final int TRANSACTION_windowsVisible = 2;

        public Stub()
        {
            attachInterface(this, "android.view.IApplicationToken");
        }

        public static IApplicationToken asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.view.IApplicationToken");
                if ((localIInterface != null) && ((localIInterface instanceof IApplicationToken)))
                    localObject = (IApplicationToken)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.view.IApplicationToken");
                continue;
                paramParcel1.enforceInterface("android.view.IApplicationToken");
                windowsDrawn();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.view.IApplicationToken");
                windowsVisible();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.view.IApplicationToken");
                windowsGone();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.view.IApplicationToken");
                boolean bool = keyDispatchingTimedOut();
                paramParcel2.writeNoException();
                if (bool);
                int k;
                for (int j = i; ; k = 0)
                {
                    paramParcel2.writeInt(j);
                    break;
                }
                paramParcel1.enforceInterface("android.view.IApplicationToken");
                long l = getKeyDispatchingTimeout();
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l);
            }
        }

        private static class Proxy
            implements IApplicationToken
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.view.IApplicationToken";
            }

            public long getKeyDispatchingTimeout()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IApplicationToken");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean keyDispatchingTimedOut()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IApplicationToken");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void windowsDrawn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IApplicationToken");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void windowsGone()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IApplicationToken");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void windowsVisible()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.view.IApplicationToken");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.IApplicationToken
 * JD-Core Version:        0.6.2
 */