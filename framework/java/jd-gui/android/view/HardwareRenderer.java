package android.view;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.opengl.ManagedEGLContext;
import android.os.Handler;
import android.os.SystemProperties;
import android.util.Log;
import java.io.File;
import java.io.PrintWriter;
import java.util.concurrent.locks.ReentrantLock;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

public abstract class HardwareRenderer
{
    private static final String CACHE_PATH_SHADERS = "com.android.opengl.shaders_cache";
    public static final String DEBUG_DIRTY_REGIONS_PROPERTY = "debug.hwui.show_dirty_regions";
    static final String DISABLE_VSYNC_PROPERTY = "debug.hwui.disable_vsync";
    static final String LOG_TAG = "HardwareRenderer";
    static final String PRINT_CONFIG_PROPERTY = "debug.hwui.print_config";
    private static final int PROFILE_FRAME_DATA_COUNT = 3;
    static final String PROFILE_MAXFRAMES_PROPERTY = "debug.hwui.profile.maxframes";
    private static final int PROFILE_MAX_FRAMES = 128;
    public static final String PROFILE_PROPERTY = "debug.hwui.profile";
    public static final boolean RENDER_DIRTY_REGIONS = true;
    static final String RENDER_DIRTY_REGIONS_PROPERTY = "debug.hwui.render_dirty_regions";
    public static boolean sRendererDisabled = false;
    public static boolean sSystemRendererDisabled = false;
    private boolean mEnabled;
    private boolean mRequested = true;

    private static void beginFrame(int[] paramArrayOfInt)
    {
        nBeginFrame(paramArrayOfInt);
    }

    static HardwareRenderer createGlRenderer(int paramInt, boolean paramBoolean)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown GL version: " + paramInt);
        case 2:
        }
        return Gl20Renderer.create(paramBoolean);
    }

    public static void disable(boolean paramBoolean)
    {
        sRendererDisabled = true;
        if (paramBoolean)
            sSystemRendererDisabled = true;
    }

    static void disableVsync()
    {
        nDisableVsync();
    }

    static void endTrimMemory()
    {
        Gl20Renderer.endTrimMemory();
    }

    public static boolean isAvailable()
    {
        return GLES20Canvas.isAvailable();
    }

    static boolean isBackBufferPreserved()
    {
        return nIsBackBufferPreserved();
    }

    private static native void nBeginFrame(int[] paramArrayOfInt);

    private static native void nDisableVsync();

    private static native boolean nIsBackBufferPreserved();

    private static native boolean nPreserveBackBuffer();

    private static native void nSetupShadersDiskCache(String paramString);

    static boolean preserveBackBuffer()
    {
        return nPreserveBackBuffer();
    }

    public static void setupDiskCache(File paramFile)
    {
        nSetupShadersDiskCache(new File(paramFile, "com.android.opengl.shaders_cache").getAbsolutePath());
    }

    static void startTrimMemory(int paramInt)
    {
        Gl20Renderer.startTrimMemory(paramInt);
    }

    static void trimMemory(int paramInt)
    {
        startTrimMemory(paramInt);
        endTrimMemory();
    }

    abstract boolean attachFunctor(View.AttachInfo paramAttachInfo, int paramInt);

    public abstract DisplayList createDisplayList(String paramString);

    abstract HardwareLayer createHardwareLayer(int paramInt1, int paramInt2, boolean paramBoolean);

    abstract HardwareLayer createHardwareLayer(boolean paramBoolean);

    abstract SurfaceTexture createSurfaceTexture(HardwareLayer paramHardwareLayer);

    abstract void destroy(boolean paramBoolean);

    abstract void destroyHardwareResources(View paramView);

    abstract void destroyLayers(View paramView);

    abstract void detachFunctor(int paramInt);

    abstract boolean draw(View paramView, View.AttachInfo paramAttachInfo, HardwareDrawCallbacks paramHardwareDrawCallbacks, Rect paramRect);

    abstract void dumpGfxInfo(PrintWriter paramPrintWriter);

    abstract HardwareCanvas getCanvas();

    abstract long getFrameCount();

    abstract int getHeight();

    abstract int getWidth();

    abstract boolean initialize(SurfaceHolder paramSurfaceHolder)
        throws Surface.OutOfResourcesException;

    void initializeIfNeeded(int paramInt1, int paramInt2, SurfaceHolder paramSurfaceHolder)
        throws Surface.OutOfResourcesException
    {
        if ((isRequested()) && (!isEnabled()) && (initialize(paramSurfaceHolder)))
            setup(paramInt1, paramInt2);
    }

    abstract void invalidate(SurfaceHolder paramSurfaceHolder);

    boolean isEnabled()
    {
        return this.mEnabled;
    }

    boolean isRequested()
    {
        return this.mRequested;
    }

    abstract boolean safelyRun(Runnable paramRunnable);

    void setEnabled(boolean paramBoolean)
    {
        this.mEnabled = paramBoolean;
    }

    void setRequested(boolean paramBoolean)
    {
        this.mRequested = paramBoolean;
    }

    abstract void setSurfaceTexture(HardwareLayer paramHardwareLayer, SurfaceTexture paramSurfaceTexture);

    abstract void setup(int paramInt1, int paramInt2);

    abstract void updateSurface(SurfaceHolder paramSurfaceHolder)
        throws Surface.OutOfResourcesException;

    abstract boolean validate();

    static class Gl20Renderer extends HardwareRenderer.GlRenderer
    {
        private static EGLSurface sPbuffer;
        private static final Object[] sPbufferLock = new Object[0];
        private GLES20Canvas mGlCanvas;

        Gl20Renderer(boolean paramBoolean)
        {
            super(paramBoolean);
        }

        static HardwareRenderer create(boolean paramBoolean)
        {
            if (GLES20Canvas.isAvailable());
            for (Gl20Renderer localGl20Renderer = new Gl20Renderer(paramBoolean); ; localGl20Renderer = null)
                return localGl20Renderer;
        }

        private static void destroyHardwareLayer(View paramView)
        {
            paramView.destroyLayer(true);
            if ((paramView instanceof ViewGroup))
            {
                ViewGroup localViewGroup = (ViewGroup)paramView;
                int i = localViewGroup.getChildCount();
                for (int j = 0; j < i; j++)
                    destroyHardwareLayer(localViewGroup.getChildAt(j));
            }
        }

        private static void destroyResources(View paramView)
        {
            paramView.destroyHardwareResources();
            if ((paramView instanceof ViewGroup))
            {
                ViewGroup localViewGroup = (ViewGroup)paramView;
                int i = localViewGroup.getChildCount();
                for (int j = 0; j < i; j++)
                    destroyResources(localViewGroup.getChildAt(j));
            }
        }

        static void endTrimMemory()
        {
            if ((sEgl != null) && (sEglDisplay != null))
                sEgl.eglMakeCurrent(sEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
        }

        static void startTrimMemory(int paramInt)
        {
            if ((sEgl == null) || (sEglConfig == null));
            while (true)
            {
                return;
                Gl20RendererEglContext localGl20RendererEglContext = (Gl20RendererEglContext)sEglContextStorage.get();
                if (localGl20RendererEglContext != null)
                {
                    usePbufferSurface(localGl20RendererEglContext.getContext());
                    if (paramInt >= 80)
                        GLES20Canvas.flushCaches(2);
                    else if (paramInt >= 20)
                        GLES20Canvas.flushCaches(1);
                }
            }
        }

        private static void usePbufferSurface(EGLContext paramEGLContext)
        {
            synchronized (sPbufferLock)
            {
                if (sPbuffer == null)
                {
                    EGL10 localEGL10 = sEgl;
                    EGLDisplay localEGLDisplay = sEglDisplay;
                    EGLConfig localEGLConfig = sEglConfig;
                    int[] arrayOfInt = new int[5];
                    arrayOfInt[0] = 12375;
                    arrayOfInt[1] = 1;
                    arrayOfInt[2] = 12374;
                    arrayOfInt[3] = 1;
                    arrayOfInt[4] = 12344;
                    sPbuffer = localEGL10.eglCreatePbufferSurface(localEGLDisplay, localEGLConfig, arrayOfInt);
                }
                sEgl.eglMakeCurrent(sEglDisplay, sPbuffer, sPbuffer, paramEGLContext);
                return;
            }
        }

        boolean canDraw()
        {
            if ((super.canDraw()) && (this.mGlCanvas != null));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        HardwareCanvas createCanvas()
        {
            GLES20Canvas localGLES20Canvas = new GLES20Canvas(this.mTranslucent);
            this.mGlCanvas = localGLES20Canvas;
            return localGLES20Canvas;
        }

        public DisplayList createDisplayList(String paramString)
        {
            return new GLES20DisplayList(paramString);
        }

        HardwareLayer createHardwareLayer(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            return new GLES20RenderLayer(paramInt1, paramInt2, paramBoolean);
        }

        HardwareLayer createHardwareLayer(boolean paramBoolean)
        {
            return new GLES20TextureLayer(paramBoolean);
        }

        ManagedEGLContext createManagedContext(EGLContext paramEGLContext)
        {
            return new Gl20RendererEglContext(this.mEglContext);
        }

        SurfaceTexture createSurfaceTexture(HardwareLayer paramHardwareLayer)
        {
            return ((GLES20TextureLayer)paramHardwareLayer).getSurfaceTexture();
        }

        void destroy(boolean paramBoolean)
        {
            try
            {
                super.destroy(paramBoolean);
                return;
            }
            finally
            {
                if ((paramBoolean) && (this.mGlCanvas != null))
                    this.mGlCanvas = null;
            }
        }

        void destroyHardwareResources(final View paramView)
        {
            if (paramView != null)
                safelyRun(new Runnable()
                {
                    public void run()
                    {
                        HardwareRenderer.Gl20Renderer.destroyResources(paramView);
                        GLES20Canvas.flushCaches(0);
                    }
                });
        }

        void destroyLayers(View paramView)
        {
            if ((paramView != null) && (isEnabled()) && (checkCurrent() != 0))
            {
                destroyHardwareLayer(paramView);
                GLES20Canvas.flushCaches(0);
            }
        }

        int[] getConfig(boolean paramBoolean)
        {
            int i = 0;
            int[] arrayOfInt = new int[17];
            arrayOfInt[i] = 12352;
            arrayOfInt[1] = 4;
            arrayOfInt[2] = 12324;
            arrayOfInt[3] = 8;
            arrayOfInt[4] = 12323;
            arrayOfInt[5] = 8;
            arrayOfInt[6] = 12322;
            arrayOfInt[7] = 8;
            arrayOfInt[8] = 12321;
            arrayOfInt[9] = 8;
            arrayOfInt[10] = 12325;
            arrayOfInt[11] = 0;
            arrayOfInt[12] = 12326;
            arrayOfInt[13] = GLES20Canvas.getStencilSize();
            arrayOfInt[14] = 12339;
            if (paramBoolean)
                i = 1024;
            arrayOfInt[15] = (i | 0x4);
            arrayOfInt[16] = 12344;
            return arrayOfInt;
        }

        void initCaches()
        {
            GLES20Canvas.initCaches();
        }

        void onPostDraw()
        {
            this.mGlCanvas.onPostDraw();
        }

        int onPreDraw(Rect paramRect)
        {
            return this.mGlCanvas.onPreDraw(paramRect);
        }

        boolean safelyRun(Runnable paramRunnable)
        {
            int i = 1;
            if ((isEnabled()) && (checkCurrent() != 0))
                i = 0;
            Gl20RendererEglContext localGl20RendererEglContext;
            boolean bool;
            if (i != 0)
            {
                localGl20RendererEglContext = (Gl20RendererEglContext)sEglContextStorage.get();
                if (localGl20RendererEglContext == null)
                    bool = false;
            }
            while (true)
            {
                return bool;
                usePbufferSurface(localGl20RendererEglContext.getContext());
                try
                {
                    paramRunnable.run();
                    if (i != 0)
                        sEgl.eglMakeCurrent(sEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                    bool = true;
                }
                finally
                {
                    if (i != 0)
                        sEgl.eglMakeCurrent(sEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                }
            }
        }

        void setSurfaceTexture(HardwareLayer paramHardwareLayer, SurfaceTexture paramSurfaceTexture)
        {
            ((GLES20TextureLayer)paramHardwareLayer).setSurfaceTexture(paramSurfaceTexture);
        }

        void setup(int paramInt1, int paramInt2)
        {
            super.setup(paramInt1, paramInt2);
            if (this.mVsyncDisabled)
                disableVsync();
        }

        static class Gl20RendererEglContext extends ManagedEGLContext
        {
            final Handler mHandler = new Handler();

            public Gl20RendererEglContext(EGLContext paramEGLContext)
            {
                super();
            }

            // ERROR //
            public void onTerminate(final EGLContext paramEGLContext)
            {
                // Byte code:
                //     0: aload_0
                //     1: getfield 22	android/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext:mHandler	Landroid/os/Handler;
                //     4: invokevirtual 27	android/os/Handler:getLooper	()Landroid/os/Looper;
                //     7: invokestatic 32	android/os/Looper:myLooper	()Landroid/os/Looper;
                //     10: if_acmpeq +21 -> 31
                //     13: aload_0
                //     14: getfield 22	android/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext:mHandler	Landroid/os/Handler;
                //     17: new 6	android/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext$1
                //     20: dup
                //     21: aload_0
                //     22: aload_1
                //     23: invokespecial 35	android/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext$1:<init>	(Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;Ljavax/microedition/khronos/egl/EGLContext;)V
                //     26: invokevirtual 39	android/os/Handler:post	(Ljava/lang/Runnable;)Z
                //     29: pop
                //     30: return
                //     31: getstatic 45	android/view/HardwareRenderer$GlRenderer:sEglLock	[Ljava/lang/Object;
                //     34: astore_2
                //     35: aload_2
                //     36: monitorenter
                //     37: getstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     40: ifnonnull +13 -> 53
                //     43: aload_2
                //     44: monitorexit
                //     45: goto -15 -> 30
                //     48: astore_3
                //     49: aload_2
                //     50: monitorexit
                //     51: aload_3
                //     52: athrow
                //     53: getstatic 53	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
                //     56: invokestatic 59	com/google/android/gles_jni/EGLImpl:getInitCount	(Ljavax/microedition/khronos/egl/EGLDisplay;)I
                //     59: iconst_1
                //     60: if_icmpne +110 -> 170
                //     63: aload_1
                //     64: invokestatic 62	android/view/HardwareRenderer$Gl20Renderer:access$300	(Ljavax/microedition/khronos/egl/EGLContext;)V
                //     67: invokestatic 67	android/view/GLES20Canvas:terminateCaches	()V
                //     70: getstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     73: getstatic 53	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
                //     76: aload_1
                //     77: invokeinterface 73 3 0
                //     82: pop
                //     83: getstatic 77	android/view/HardwareRenderer$GlRenderer:sEglContextStorage	Ljava/lang/ThreadLocal;
                //     86: aconst_null
                //     87: invokevirtual 83	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
                //     90: getstatic 77	android/view/HardwareRenderer$GlRenderer:sEglContextStorage	Ljava/lang/ThreadLocal;
                //     93: invokevirtual 86	java/lang/ThreadLocal:remove	()V
                //     96: getstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     99: getstatic 53	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
                //     102: invokestatic 90	android/view/HardwareRenderer$Gl20Renderer:access$400	()Ljavax/microedition/khronos/egl/EGLSurface;
                //     105: invokeinterface 94 3 0
                //     110: pop
                //     111: getstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     114: getstatic 53	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
                //     117: getstatic 98	javax/microedition/khronos/egl/EGL10:EGL_NO_SURFACE	Ljavax/microedition/khronos/egl/EGLSurface;
                //     120: getstatic 98	javax/microedition/khronos/egl/EGL10:EGL_NO_SURFACE	Ljavax/microedition/khronos/egl/EGLSurface;
                //     123: getstatic 102	javax/microedition/khronos/egl/EGL10:EGL_NO_CONTEXT	Ljavax/microedition/khronos/egl/EGLContext;
                //     126: invokeinterface 106 5 0
                //     131: pop
                //     132: getstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     135: invokeinterface 110 1 0
                //     140: pop
                //     141: getstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     144: getstatic 53	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
                //     147: invokeinterface 114 2 0
                //     152: pop
                //     153: aconst_null
                //     154: putstatic 49	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
                //     157: aconst_null
                //     158: putstatic 53	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
                //     161: aconst_null
                //     162: putstatic 118	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
                //     165: aconst_null
                //     166: invokestatic 122	android/view/HardwareRenderer$Gl20Renderer:access$402	(Ljavax/microedition/khronos/egl/EGLSurface;)Ljavax/microedition/khronos/egl/EGLSurface;
                //     169: pop
                //     170: aload_2
                //     171: monitorexit
                //     172: goto -142 -> 30
                //
                // Exception table:
                //     from	to	target	type
                //     37	51	48	finally
                //     53	172	48	finally
            }
        }
    }

    static abstract class GlRenderer extends HardwareRenderer
    {
        static final int EGL_CONTEXT_CLIENT_VERSION = 12440;
        static final int EGL_OPENGL_ES2_BIT = 4;
        static final int EGL_SURFACE_TYPE = 12339;
        static final int EGL_SWAP_BEHAVIOR_PRESERVED_BIT = 1024;
        static final int FUNCTOR_PROCESS_DELAY = 4;
        static final int SURFACE_STATE_ERROR = 0;
        static final int SURFACE_STATE_SUCCESS = 1;
        static final int SURFACE_STATE_UPDATED = 2;
        static boolean sDirtyRegions = "true".equalsIgnoreCase(SystemProperties.get("debug.hwui.render_dirty_regions", "true"));
        static final boolean sDirtyRegionsRequested = sDirtyRegions;
        static EGL10 sEgl;
        static EGLConfig sEglConfig;
        static final ThreadLocal<ManagedEGLContext> sEglContextStorage;
        static EGLDisplay sEglDisplay;
        static final Object[] sEglLock = new Object[0];
        HardwareCanvas mCanvas;
        final boolean mDebugDirtyRegions;
        Paint mDebugPaint;
        private boolean mDestroyed;
        boolean mDirtyRegionsEnabled;
        EGLContext mEglContext;
        EGLSurface mEglSurface;
        Thread mEglThread;
        long mFrameCount;
        private final FunctorsRunnable mFunctorsRunnable = new FunctorsRunnable();
        GL mGl;
        final int mGlVersion;
        int mHeight = -1;
        int mProfileCurrentFrame = -3;
        final float[] mProfileData;
        final boolean mProfileEnabled;
        final ReentrantLock mProfileLock;
        private final Rect mRedrawClip = new Rect();
        private final int[] mSurfaceSize = new int[2];
        final boolean mTranslucent;
        boolean mUpdateDirtyRegions;
        final boolean mVsyncDisabled;
        int mWidth = -1;

        static
        {
            sEglContextStorage = new ThreadLocal();
        }

        GlRenderer(int paramInt, boolean paramBoolean)
        {
            this.mGlVersion = paramInt;
            this.mTranslucent = paramBoolean;
            this.mVsyncDisabled = "true".equalsIgnoreCase(SystemProperties.get("debug.hwui.disable_vsync", "false"));
            if (this.mVsyncDisabled)
                Log.d("HardwareRenderer", "Disabling v-sync");
            this.mProfileEnabled = "true".equalsIgnoreCase(SystemProperties.get("debug.hwui.profile", "false"));
            if (this.mProfileEnabled)
                Log.d("HardwareRenderer", "Profiling hardware renderer");
            if (this.mProfileEnabled)
            {
                this.mProfileData = new float[3 * Integer.valueOf(SystemProperties.get("debug.hwui.profile.maxframes", Integer.toString(128))).intValue()];
                for (int i = 0; i < this.mProfileData.length; i += 3)
                {
                    float[] arrayOfFloat1 = this.mProfileData;
                    float[] arrayOfFloat2 = this.mProfileData;
                    int j = i + 1;
                    this.mProfileData[(i + 2)] = -1.0F;
                    arrayOfFloat2[j] = -1.0F;
                    arrayOfFloat1[i] = -1.0F;
                }
            }
            for (this.mProfileLock = new ReentrantLock(); ; this.mProfileLock = null)
            {
                this.mDebugDirtyRegions = "true".equalsIgnoreCase(SystemProperties.get("debug.hwui.show_dirty_regions", "false"));
                if (this.mDebugDirtyRegions)
                    Log.d("HardwareRenderer", "Debugging dirty regions");
                return;
                this.mProfileData = null;
            }
        }

        private EGLConfig chooseEglConfig()
        {
            EGLConfig[] arrayOfEGLConfig1 = new EGLConfig[1];
            int[] arrayOfInt1 = new int[1];
            int[] arrayOfInt2 = getConfig(sDirtyRegions);
            String str = SystemProperties.get("debug.hwui.print_config", "");
            if ("all".equalsIgnoreCase(str))
            {
                sEgl.eglChooseConfig(sEglDisplay, arrayOfInt2, null, 0, arrayOfInt1);
                EGLConfig[] arrayOfEGLConfig2 = new EGLConfig[arrayOfInt1[0]];
                sEgl.eglChooseConfig(sEglDisplay, arrayOfInt2, arrayOfEGLConfig2, arrayOfInt1[0], arrayOfInt1);
                int i = arrayOfEGLConfig2.length;
                for (int j = 0; j < i; j++)
                    printConfig(arrayOfEGLConfig2[j]);
            }
            if (!sEgl.eglChooseConfig(sEglDisplay, arrayOfInt2, arrayOfEGLConfig1, 1, arrayOfInt1))
                throw new IllegalArgumentException("eglChooseConfig failed " + GLUtils.getEGLErrorString(sEgl.eglGetError()));
            if (arrayOfInt1[0] > 0)
                if ("choice".equalsIgnoreCase(str))
                    printConfig(arrayOfEGLConfig1[0]);
            for (EGLConfig localEGLConfig = arrayOfEGLConfig1[0]; ; localEGLConfig = null)
                return localEGLConfig;
        }

        private boolean createSurface(SurfaceHolder paramSurfaceHolder)
        {
            this.mEglSurface = sEgl.eglCreateWindowSurface(sEglDisplay, sEglConfig, paramSurfaceHolder, null);
            int i;
            if ((this.mEglSurface == null) || (this.mEglSurface == EGL10.EGL_NO_SURFACE))
            {
                i = sEgl.eglGetError();
                if (i == 12299)
                    Log.e("HardwareRenderer", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
            }
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                throw new RuntimeException("createWindowSurface failed " + GLUtils.getEGLErrorString(i));
            }
        }

        private void enableDirtyRegions()
        {
            if (sDirtyRegions)
            {
                boolean bool = preserveBackBuffer();
                this.mDirtyRegionsEnabled = bool;
                if (!bool)
                    Log.w("HardwareRenderer", "Backbuffer cannot be preserved");
            }
            while (true)
            {
                return;
                if (sDirtyRegionsRequested)
                    this.mDirtyRegionsEnabled = isBackBufferPreserved();
            }
        }

        private void fallback(boolean paramBoolean)
        {
            destroy(true);
            if (paramBoolean)
            {
                setRequested(false);
                Log.w("HardwareRenderer", "Mountain View, we've had a problem here. Switching back to software rendering.");
            }
        }

        private void handleFunctorStatus(View.AttachInfo paramAttachInfo, int paramInt)
        {
            if ((paramInt & 0x1) != 0)
            {
                if (!this.mRedrawClip.isEmpty())
                    break label36;
                paramAttachInfo.mViewRootImpl.invalidate();
            }
            while (true)
            {
                if ((paramInt & 0x2) != 0)
                    scheduleFunctors(paramAttachInfo, true);
                return;
                label36: paramAttachInfo.mViewRootImpl.invalidateChildInParent(null, this.mRedrawClip);
                this.mRedrawClip.setEmpty();
            }
        }

        private static void printConfig(EGLConfig paramEGLConfig)
        {
            int[] arrayOfInt = new int[1];
            Log.d("HardwareRenderer", "EGL configuration " + paramEGLConfig + ":");
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12324, arrayOfInt);
            Log.d("HardwareRenderer", "    RED_SIZE = " + arrayOfInt[0]);
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12323, arrayOfInt);
            Log.d("HardwareRenderer", "    GREEN_SIZE = " + arrayOfInt[0]);
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12322, arrayOfInt);
            Log.d("HardwareRenderer", "    BLUE_SIZE = " + arrayOfInt[0]);
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12321, arrayOfInt);
            Log.d("HardwareRenderer", "    ALPHA_SIZE = " + arrayOfInt[0]);
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12325, arrayOfInt);
            Log.d("HardwareRenderer", "    DEPTH_SIZE = " + arrayOfInt[0]);
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12326, arrayOfInt);
            Log.d("HardwareRenderer", "    STENCIL_SIZE = " + arrayOfInt[0]);
            sEgl.eglGetConfigAttrib(sEglDisplay, paramEGLConfig, 12339, arrayOfInt);
            Log.d("HardwareRenderer", "    SURFACE_TYPE = 0x" + Integer.toHexString(arrayOfInt[0]));
        }

        private void scheduleFunctors(View.AttachInfo paramAttachInfo, boolean paramBoolean)
        {
            this.mFunctorsRunnable.attachInfo = paramAttachInfo;
            Handler localHandler;
            FunctorsRunnable localFunctorsRunnable;
            if (!paramAttachInfo.mHandler.hasCallbacks(this.mFunctorsRunnable))
            {
                localHandler = paramAttachInfo.mHandler;
                localFunctorsRunnable = this.mFunctorsRunnable;
                if (!paramBoolean)
                    break label52;
            }
            label52: for (long l = 4L; ; l = 0L)
            {
                localHandler.postDelayed(localFunctorsRunnable, l);
                return;
            }
        }

        boolean attachFunctor(View.AttachInfo paramAttachInfo, int paramInt)
        {
            boolean bool = false;
            if (this.mCanvas != null)
            {
                this.mCanvas.attachFunctor(paramInt);
                scheduleFunctors(paramAttachInfo, false);
                bool = true;
            }
            return bool;
        }

        boolean canDraw()
        {
            if ((this.mGl != null) && (this.mCanvas != null));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        int checkCurrent()
        {
            int i = 1;
            if (this.mEglThread != Thread.currentThread())
                throw new IllegalStateException("Hardware acceleration can only be used with a single UI thread.\nOriginal thread: " + this.mEglThread + "\n" + "Current thread: " + Thread.currentThread());
            if ((!this.mEglContext.equals(sEgl.eglGetCurrentContext())) || (!this.mEglSurface.equals(sEgl.eglGetCurrentSurface(12377))))
            {
                if (sEgl.eglMakeCurrent(sEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext))
                    break label171;
                Log.e("HardwareRenderer", "eglMakeCurrent failed " + GLUtils.getEGLErrorString(sEgl.eglGetError()));
                fallback(i);
            }
            label171: int j;
            for (i = 0; ; j = 2)
            {
                return i;
                if (this.mUpdateDirtyRegions)
                {
                    enableDirtyRegions();
                    this.mUpdateDirtyRegions = false;
                }
            }
        }

        void checkEglErrors()
        {
            if (isEnabled())
            {
                int i = sEgl.eglGetError();
                if (i != 12288)
                {
                    Log.w("HardwareRenderer", "EGL error: " + GLUtils.getEGLErrorString(i));
                    if (i == 12302)
                        break label67;
                }
            }
            label67: for (boolean bool = true; ; bool = false)
            {
                fallback(bool);
                return;
            }
        }

        abstract HardwareCanvas createCanvas();

        EGLContext createContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig)
        {
            int[] arrayOfInt = new int[3];
            arrayOfInt[0] = 12440;
            arrayOfInt[1] = this.mGlVersion;
            arrayOfInt[2] = 12344;
            EGLContext localEGLContext = EGL10.EGL_NO_CONTEXT;
            if (this.mGlVersion != 0);
            while (true)
            {
                return paramEGL10.eglCreateContext(paramEGLDisplay, paramEGLConfig, localEGLContext, arrayOfInt);
                arrayOfInt = null;
            }
        }

        GL createEglSurface(SurfaceHolder paramSurfaceHolder)
            throws Surface.OutOfResourcesException
        {
            if (sEgl == null)
                throw new RuntimeException("egl not initialized");
            if (sEglDisplay == null)
                throw new RuntimeException("eglDisplay not initialized");
            if (sEglConfig == null)
                throw new RuntimeException("eglConfig not initialized");
            if (Thread.currentThread() != this.mEglThread)
                throw new IllegalStateException("HardwareRenderer cannot be used from multiple threads");
            destroySurface();
            if (!createSurface(paramSurfaceHolder));
            for (GL localGL = null; ; localGL = this.mEglContext.getGL())
            {
                return localGL;
                if (!sEgl.eglMakeCurrent(sEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext))
                    throw new Surface.OutOfResourcesException("eglMakeCurrent failed " + GLUtils.getEGLErrorString(sEgl.eglGetError()));
                initCaches();
                enableDirtyRegions();
            }
        }

        abstract ManagedEGLContext createManagedContext(EGLContext paramEGLContext);

        void destroy(boolean paramBoolean)
        {
            if ((paramBoolean) && (this.mCanvas != null))
                this.mCanvas = null;
            if ((!isEnabled()) || (this.mDestroyed))
                setEnabled(false);
            while (true)
            {
                return;
                destroySurface();
                setEnabled(false);
                this.mDestroyed = true;
                this.mGl = null;
            }
        }

        void destroySurface()
        {
            if ((this.mEglSurface != null) && (this.mEglSurface != EGL10.EGL_NO_SURFACE))
            {
                sEgl.eglMakeCurrent(sEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                sEgl.eglDestroySurface(sEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
        }

        void detachFunctor(int paramInt)
        {
            if (this.mCanvas != null)
                this.mCanvas.detachFunctor(paramInt);
        }

        // ERROR //
        boolean draw(View paramView, View.AttachInfo paramAttachInfo, HardwareRenderer.HardwareDrawCallbacks paramHardwareDrawCallbacks, Rect paramRect)
        {
            // Byte code:
            //     0: aload_0
            //     1: invokevirtual 487	android/view/HardwareRenderer$GlRenderer:canDraw	()Z
            //     4: ifeq +752 -> 756
            //     7: aload_0
            //     8: invokevirtual 490	android/view/HardwareRenderer$GlRenderer:hasDirtyRegions	()Z
            //     11: ifne +6 -> 17
            //     14: aconst_null
            //     15: astore 4
            //     17: aload_2
            //     18: iconst_1
            //     19: putfield 493	android/view/View$AttachInfo:mIgnoreDirtyState	Z
            //     22: aload_2
            //     23: invokestatic 499	android/os/SystemClock:uptimeMillis	()J
            //     26: putfield 502	android/view/View$AttachInfo:mDrawingTime	J
            //     29: aload_1
            //     30: bipush 32
            //     32: aload_1
            //     33: getfield 507	android/view/View:mPrivateFlags	I
            //     36: ior
            //     37: putfield 507	android/view/View:mPrivateFlags	I
            //     40: aload_0
            //     41: invokevirtual 509	android/view/HardwareRenderer$GlRenderer:checkCurrent	()I
            //     44: istore 6
            //     46: iload 6
            //     48: ifeq +708 -> 756
            //     51: aload_0
            //     52: getfield 373	android/view/HardwareRenderer$GlRenderer:mCanvas	Landroid/view/HardwareCanvas;
            //     55: astore 7
            //     57: aload_2
            //     58: aload 7
            //     60: putfield 512	android/view/View$AttachInfo:mHardwareCanvas	Landroid/view/HardwareCanvas;
            //     63: aload_0
            //     64: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     67: ifeq +10 -> 77
            //     70: aload_0
            //     71: getfield 176	android/view/HardwareRenderer$GlRenderer:mProfileLock	Ljava/util/concurrent/locks/ReentrantLock;
            //     74: invokevirtual 515	java/util/concurrent/locks/ReentrantLock:lock	()V
            //     77: iload 6
            //     79: iconst_2
            //     80: if_icmpne +464 -> 544
            //     83: aconst_null
            //     84: astore 4
            //     86: aconst_null
            //     87: invokestatic 519	android/view/HardwareRenderer:access$200	([I)V
            //     90: aload_0
            //     91: aload 4
            //     93: invokevirtual 523	android/view/HardwareRenderer$GlRenderer:onPreDraw	(Landroid/graphics/Rect;)I
            //     96: istore 9
            //     98: aload 7
            //     100: invokevirtual 526	android/view/HardwareCanvas:save	()I
            //     103: istore 10
            //     105: aload_3
            //     106: aload 7
            //     108: invokeinterface 532 2 0
            //     113: ldc_w 533
            //     116: aload_1
            //     117: getfield 507	android/view/View:mPrivateFlags	I
            //     120: iand
            //     121: ldc_w 533
            //     124: if_icmpne +488 -> 612
            //     127: iconst_1
            //     128: istore 13
            //     130: aload_1
            //     131: iload 13
            //     133: putfield 536	android/view/View:mRecreateDisplayList	Z
            //     136: aload_1
            //     137: ldc_w 537
            //     140: aload_1
            //     141: getfield 507	android/view/View:mPrivateFlags	I
            //     144: iand
            //     145: putfield 507	android/view/View:mPrivateFlags	I
            //     148: lconst_0
            //     149: lstore 14
            //     151: aload_0
            //     152: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     155: ifeq +35 -> 190
            //     158: aload_0
            //     159: iconst_3
            //     160: aload_0
            //     161: getfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     164: iadd
            //     165: putfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     168: aload_0
            //     169: getfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     172: aload_0
            //     173: getfield 170	android/view/HardwareRenderer$GlRenderer:mProfileData	[F
            //     176: arraylength
            //     177: if_icmplt +8 -> 185
            //     180: aload_0
            //     181: iconst_0
            //     182: putfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     185: invokestatic 542	java/lang/System:nanoTime	()J
            //     188: lstore 14
            //     190: ldc2_w 543
            //     193: ldc_w 546
            //     196: invokestatic 552	android/os/Trace:traceBegin	(JLjava/lang/String;)V
            //     199: aload_1
            //     200: invokevirtual 555	android/view/View:getDisplayList	()Landroid/view/DisplayList;
            //     203: astore 17
            //     205: ldc2_w 543
            //     208: invokestatic 559	android/os/Trace:traceEnd	(J)V
            //     211: aload_0
            //     212: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     215: ifeq +27 -> 242
            //     218: ldc_w 560
            //     221: invokestatic 542	java/lang/System:nanoTime	()J
            //     224: lload 14
            //     226: lsub
            //     227: l2f
            //     228: fmul
            //     229: fstore 28
            //     231: aload_0
            //     232: getfield 170	android/view/HardwareRenderer$GlRenderer:mProfileData	[F
            //     235: aload_0
            //     236: getfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     239: fload 28
            //     241: fastore
            //     242: aload 17
            //     244: ifnull +497 -> 741
            //     247: lconst_0
            //     248: lstore 23
            //     250: aload_0
            //     251: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     254: ifeq +8 -> 262
            //     257: invokestatic 542	java/lang/System:nanoTime	()J
            //     260: lstore 23
            //     262: ldc2_w 543
            //     265: ldc_w 562
            //     268: invokestatic 552	android/os/Trace:traceBegin	(JLjava/lang/String;)V
            //     271: aload 7
            //     273: aload 17
            //     275: aload_0
            //     276: getfield 119	android/view/HardwareRenderer$GlRenderer:mRedrawClip	Landroid/graphics/Rect;
            //     279: iconst_1
            //     280: invokevirtual 565	android/view/HardwareCanvas:drawDisplayList	(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I
            //     283: istore 26
            //     285: iload 9
            //     287: iload 26
            //     289: ior
            //     290: istore 9
            //     292: ldc2_w 543
            //     295: invokestatic 559	android/os/Trace:traceEnd	(J)V
            //     298: aload_0
            //     299: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     302: ifeq +29 -> 331
            //     305: ldc_w 560
            //     308: invokestatic 542	java/lang/System:nanoTime	()J
            //     311: lload 23
            //     313: lsub
            //     314: l2f
            //     315: fmul
            //     316: fstore 27
            //     318: aload_0
            //     319: getfield 170	android/view/HardwareRenderer$GlRenderer:mProfileData	[F
            //     322: iconst_1
            //     323: aload_0
            //     324: getfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     327: iadd
            //     328: fload 27
            //     330: fastore
            //     331: aload_0
            //     332: aload_2
            //     333: iload 9
            //     335: invokespecial 190	android/view/HardwareRenderer$GlRenderer:handleFunctorStatus	(Landroid/view/View$AttachInfo;I)V
            //     338: aload_3
            //     339: aload 7
            //     341: invokeinterface 568 2 0
            //     346: aload 7
            //     348: iload 10
            //     350: invokevirtual 571	android/view/HardwareCanvas:restoreToCount	(I)V
            //     353: aload_1
            //     354: iconst_0
            //     355: putfield 536	android/view/View:mRecreateDisplayList	Z
            //     358: aload_0
            //     359: lconst_1
            //     360: aload_0
            //     361: getfield 573	android/view/HardwareRenderer$GlRenderer:mFrameCount	J
            //     364: ladd
            //     365: putfield 573	android/view/HardwareRenderer$GlRenderer:mFrameCount	J
            //     368: aload_0
            //     369: getfield 180	android/view/HardwareRenderer$GlRenderer:mDebugDirtyRegions	Z
            //     372: ifeq +62 -> 434
            //     375: aload_0
            //     376: getfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     379: ifnonnull +24 -> 403
            //     382: aload_0
            //     383: new 577	android/graphics/Paint
            //     386: dup
            //     387: invokespecial 578	android/graphics/Paint:<init>	()V
            //     390: putfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     393: aload_0
            //     394: getfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     397: ldc_w 579
            //     400: invokevirtual 582	android/graphics/Paint:setColor	(I)V
            //     403: aload 4
            //     405: ifnull +29 -> 434
            //     408: lconst_1
            //     409: aload_0
            //     410: getfield 573	android/view/HardwareRenderer$GlRenderer:mFrameCount	J
            //     413: land
            //     414: lconst_0
            //     415: lcmp
            //     416: ifne +18 -> 434
            //     419: aload_0
            //     420: getfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     423: astore 22
            //     425: aload 7
            //     427: aload 4
            //     429: aload 22
            //     431: invokevirtual 586	android/view/HardwareCanvas:drawRect	(Landroid/graphics/Rect;Landroid/graphics/Paint;)V
            //     434: aload_0
            //     435: invokevirtual 589	android/view/HardwareRenderer$GlRenderer:onPostDraw	()V
            //     438: aload_2
            //     439: iconst_0
            //     440: putfield 493	android/view/View$AttachInfo:mIgnoreDirtyState	Z
            //     443: iload 9
            //     445: iconst_4
            //     446: iand
            //     447: iconst_4
            //     448: if_icmpne +71 -> 519
            //     451: lconst_0
            //     452: lstore 18
            //     454: aload_0
            //     455: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     458: ifeq +8 -> 466
            //     461: invokestatic 542	java/lang/System:nanoTime	()J
            //     464: lstore 18
            //     466: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     469: getstatic 208	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
            //     472: aload_0
            //     473: getfield 255	android/view/HardwareRenderer$GlRenderer:mEglSurface	Ljavax/microedition/khronos/egl/EGLSurface;
            //     476: invokeinterface 592 3 0
            //     481: pop
            //     482: aload_0
            //     483: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     486: ifeq +29 -> 515
            //     489: ldc_w 560
            //     492: invokestatic 542	java/lang/System:nanoTime	()J
            //     495: lload 18
            //     497: lsub
            //     498: l2f
            //     499: fmul
            //     500: fstore 21
            //     502: aload_0
            //     503: getfield 170	android/view/HardwareRenderer$GlRenderer:mProfileData	[F
            //     506: iconst_2
            //     507: aload_0
            //     508: getfield 114	android/view/HardwareRenderer$GlRenderer:mProfileCurrentFrame	I
            //     511: iadd
            //     512: fload 21
            //     514: fastore
            //     515: aload_0
            //     516: invokevirtual 594	android/view/HardwareRenderer$GlRenderer:checkEglErrors	()V
            //     519: aload_0
            //     520: getfield 150	android/view/HardwareRenderer$GlRenderer:mProfileEnabled	Z
            //     523: ifeq +10 -> 533
            //     526: aload_0
            //     527: getfield 176	android/view/HardwareRenderer$GlRenderer:mProfileLock	Ljava/util/concurrent/locks/ReentrantLock;
            //     530: invokevirtual 597	java/util/concurrent/locks/ReentrantLock:unlock	()V
            //     533: aload 4
            //     535: ifnonnull +215 -> 750
            //     538: iconst_1
            //     539: istore 5
            //     541: iload 5
            //     543: ireturn
            //     544: aload_0
            //     545: getfield 121	android/view/HardwareRenderer$GlRenderer:mSurfaceSize	[I
            //     548: astore 8
            //     550: aload 8
            //     552: invokestatic 519	android/view/HardwareRenderer:access$200	([I)V
            //     555: aload 8
            //     557: iconst_1
            //     558: iaload
            //     559: aload_0
            //     560: getfield 112	android/view/HardwareRenderer$GlRenderer:mHeight	I
            //     563: if_icmpne +14 -> 577
            //     566: aload 8
            //     568: iconst_0
            //     569: iaload
            //     570: aload_0
            //     571: getfield 110	android/view/HardwareRenderer$GlRenderer:mWidth	I
            //     574: if_icmpeq -484 -> 90
            //     577: aload_0
            //     578: aload 8
            //     580: iconst_0
            //     581: iaload
            //     582: putfield 110	android/view/HardwareRenderer$GlRenderer:mWidth	I
            //     585: aload_0
            //     586: aload 8
            //     588: iconst_1
            //     589: iaload
            //     590: putfield 112	android/view/HardwareRenderer$GlRenderer:mHeight	I
            //     593: aload 7
            //     595: aload_0
            //     596: getfield 110	android/view/HardwareRenderer$GlRenderer:mWidth	I
            //     599: aload_0
            //     600: getfield 112	android/view/HardwareRenderer$GlRenderer:mHeight	I
            //     603: invokevirtual 601	android/view/HardwareCanvas:setViewport	(II)V
            //     606: aconst_null
            //     607: astore 4
            //     609: goto -519 -> 90
            //     612: iconst_0
            //     613: istore 13
            //     615: goto -485 -> 130
            //     618: astore 16
            //     620: ldc2_w 543
            //     623: invokestatic 559	android/os/Trace:traceEnd	(J)V
            //     626: aload 16
            //     628: athrow
            //     629: astore 11
            //     631: aload_3
            //     632: aload 7
            //     634: invokeinterface 568 2 0
            //     639: aload 7
            //     641: iload 10
            //     643: invokevirtual 571	android/view/HardwareCanvas:restoreToCount	(I)V
            //     646: aload_1
            //     647: iconst_0
            //     648: putfield 536	android/view/View:mRecreateDisplayList	Z
            //     651: aload_0
            //     652: lconst_1
            //     653: aload_0
            //     654: getfield 573	android/view/HardwareRenderer$GlRenderer:mFrameCount	J
            //     657: ladd
            //     658: putfield 573	android/view/HardwareRenderer$GlRenderer:mFrameCount	J
            //     661: aload_0
            //     662: getfield 180	android/view/HardwareRenderer$GlRenderer:mDebugDirtyRegions	Z
            //     665: ifeq +62 -> 727
            //     668: aload_0
            //     669: getfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     672: ifnonnull +24 -> 696
            //     675: aload_0
            //     676: new 577	android/graphics/Paint
            //     679: dup
            //     680: invokespecial 578	android/graphics/Paint:<init>	()V
            //     683: putfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     686: aload_0
            //     687: getfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     690: ldc_w 579
            //     693: invokevirtual 582	android/graphics/Paint:setColor	(I)V
            //     696: aload 4
            //     698: ifnull +29 -> 727
            //     701: lconst_1
            //     702: aload_0
            //     703: getfield 573	android/view/HardwareRenderer$GlRenderer:mFrameCount	J
            //     706: land
            //     707: lconst_0
            //     708: lcmp
            //     709: ifne +18 -> 727
            //     712: aload_0
            //     713: getfield 575	android/view/HardwareRenderer$GlRenderer:mDebugPaint	Landroid/graphics/Paint;
            //     716: astore 12
            //     718: aload 7
            //     720: aload 4
            //     722: aload 12
            //     724: invokevirtual 586	android/view/HardwareCanvas:drawRect	(Landroid/graphics/Rect;Landroid/graphics/Paint;)V
            //     727: aload 11
            //     729: athrow
            //     730: astore 25
            //     732: ldc2_w 543
            //     735: invokestatic 559	android/os/Trace:traceEnd	(J)V
            //     738: aload 25
            //     740: athrow
            //     741: aload_1
            //     742: aload 7
            //     744: invokevirtual 604	android/view/View:draw	(Landroid/graphics/Canvas;)V
            //     747: goto -409 -> 338
            //     750: iconst_0
            //     751: istore 5
            //     753: goto -212 -> 541
            //     756: iconst_0
            //     757: istore 5
            //     759: goto -218 -> 541
            //
            // Exception table:
            //     from	to	target	type
            //     199	205	618	finally
            //     113	199	629	finally
            //     205	271	629	finally
            //     292	338	629	finally
            //     620	629	629	finally
            //     732	747	629	finally
            //     271	285	730	finally
        }

        void dumpGfxInfo(PrintWriter paramPrintWriter)
        {
            int i;
            if (this.mProfileEnabled)
            {
                paramPrintWriter.printf("\n\tDraw\tProcess\tExecute\n", new Object[0]);
                this.mProfileLock.lock();
                i = 0;
            }
            try
            {
                while (true)
                {
                    if ((i >= this.mProfileData.length) || (this.mProfileData[i] < 0.0F))
                    {
                        this.mProfileCurrentFrame = this.mProfileData.length;
                        return;
                    }
                    Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = Float.valueOf(this.mProfileData[i]);
                    arrayOfObject[1] = Float.valueOf(this.mProfileData[(i + 1)]);
                    arrayOfObject[2] = Float.valueOf(this.mProfileData[(i + 2)]);
                    paramPrintWriter.printf("\t%3.2f\t%3.2f\t%3.2f\n", arrayOfObject);
                    float[] arrayOfFloat1 = this.mProfileData;
                    float[] arrayOfFloat2 = this.mProfileData;
                    int j = i + 1;
                    this.mProfileData[(i + 2)] = -1.0F;
                    arrayOfFloat2[j] = -1.0F;
                    arrayOfFloat1[i] = -1.0F;
                    i += 3;
                }
            }
            finally
            {
                this.mProfileLock.unlock();
            }
        }

        HardwareCanvas getCanvas()
        {
            return this.mCanvas;
        }

        abstract int[] getConfig(boolean paramBoolean);

        long getFrameCount()
        {
            return this.mFrameCount;
        }

        int getHeight()
        {
            return this.mHeight;
        }

        int getWidth()
        {
            return this.mWidth;
        }

        boolean hasDirtyRegions()
        {
            return this.mDirtyRegionsEnabled;
        }

        abstract void initCaches();

        boolean initialize(SurfaceHolder paramSurfaceHolder)
            throws Surface.OutOfResourcesException
        {
            boolean bool = true;
            if ((isRequested()) && (!isEnabled()))
            {
                initializeEgl();
                this.mGl = createEglSurface(paramSurfaceHolder);
                this.mDestroyed = false;
                if (this.mGl != null)
                    if (sEgl.eglGetError() != 12288)
                    {
                        destroy(bool);
                        setRequested(false);
                        if (this.mCanvas == null)
                            break label116;
                    }
            }
            while (true)
            {
                return bool;
                if (this.mCanvas == null)
                    this.mCanvas = createCanvas();
                if (this.mCanvas != null)
                {
                    setEnabled(bool);
                    break;
                }
                Log.w("HardwareRenderer", "Hardware accelerated Canvas could not be created");
                break;
                label116: bool = false;
                continue;
                bool = false;
            }
        }

        // ERROR //
        void initializeEgl()
        {
            // Byte code:
            //     0: getstatic 79	android/view/HardwareRenderer$GlRenderer:sEglLock	[Ljava/lang/Object;
            //     3: astore_1
            //     4: aload_1
            //     5: monitorenter
            //     6: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     9: ifnonnull +201 -> 210
            //     12: getstatic 249	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     15: ifnonnull +195 -> 210
            //     18: invokestatic 642	javax/microedition/khronos/egl/EGLContext:getEGL	()Ljavax/microedition/khronos/egl/EGL;
            //     21: checkcast 210	javax/microedition/khronos/egl/EGL10
            //     24: putstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     27: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     30: getstatic 646	javax/microedition/khronos/egl/EGL10:EGL_DEFAULT_DISPLAY	Ljava/lang/Object;
            //     33: invokeinterface 650 2 0
            //     38: putstatic 208	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
            //     41: getstatic 208	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
            //     44: getstatic 653	javax/microedition/khronos/egl/EGL10:EGL_NO_DISPLAY	Ljavax/microedition/khronos/egl/EGLDisplay;
            //     47: if_acmpne +46 -> 93
            //     50: new 265	java/lang/RuntimeException
            //     53: dup
            //     54: new 222	java/lang/StringBuilder
            //     57: dup
            //     58: invokespecial 223	java/lang/StringBuilder:<init>	()V
            //     61: ldc_w 655
            //     64: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     67: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     70: invokeinterface 232 1 0
            //     75: invokestatic 237	android/opengl/GLUtils:getEGLErrorString	(I)Ljava/lang/String;
            //     78: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     81: invokevirtual 240	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     84: invokespecial 268	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
            //     87: athrow
            //     88: astore_2
            //     89: aload_1
            //     90: monitorexit
            //     91: aload_2
            //     92: athrow
            //     93: iconst_2
            //     94: newarray int
            //     96: astore 5
            //     98: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     101: getstatic 208	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
            //     104: aload 5
            //     106: invokeinterface 659 3 0
            //     111: ifne +41 -> 152
            //     114: new 265	java/lang/RuntimeException
            //     117: dup
            //     118: new 222	java/lang/StringBuilder
            //     121: dup
            //     122: invokespecial 223	java/lang/StringBuilder:<init>	()V
            //     125: ldc_w 661
            //     128: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     131: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     134: invokeinterface 232 1 0
            //     139: invokestatic 237	android/opengl/GLUtils:getEGLErrorString	(I)Ljava/lang/String;
            //     142: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     145: invokevirtual 240	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     148: invokespecial 268	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
            //     151: athrow
            //     152: aload_0
            //     153: invokespecial 663	android/view/HardwareRenderer$GlRenderer:chooseEglConfig	()Ljavax/microedition/khronos/egl/EGLConfig;
            //     156: putstatic 249	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     159: getstatic 249	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     162: ifnonnull +48 -> 210
            //     165: getstatic 104	android/view/HardwareRenderer$GlRenderer:sDirtyRegions	Z
            //     168: ifeq +31 -> 199
            //     171: iconst_0
            //     172: putstatic 104	android/view/HardwareRenderer$GlRenderer:sDirtyRegions	Z
            //     175: aload_0
            //     176: invokespecial 663	android/view/HardwareRenderer$GlRenderer:chooseEglConfig	()Ljavax/microedition/khronos/egl/EGLConfig;
            //     179: putstatic 249	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     182: getstatic 249	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     185: ifnonnull +25 -> 210
            //     188: new 265	java/lang/RuntimeException
            //     191: dup
            //     192: ldc_w 452
            //     195: invokespecial 268	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
            //     198: athrow
            //     199: new 265	java/lang/RuntimeException
            //     202: dup
            //     203: ldc_w 452
            //     206: invokespecial 268	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
            //     209: athrow
            //     210: aload_1
            //     211: monitorexit
            //     212: getstatic 86	android/view/HardwareRenderer$GlRenderer:sEglContextStorage	Ljava/lang/ThreadLocal;
            //     215: invokevirtual 666	java/lang/ThreadLocal:get	()Ljava/lang/Object;
            //     218: checkcast 668	android/opengl/ManagedEGLContext
            //     221: astore_3
            //     222: aload_3
            //     223: ifnull +61 -> 284
            //     226: aload_3
            //     227: invokevirtual 671	android/opengl/ManagedEGLContext:getContext	()Ljavax/microedition/khronos/egl/EGLContext;
            //     230: astore 4
            //     232: aload_0
            //     233: aload 4
            //     235: putfield 401	android/view/HardwareRenderer$GlRenderer:mEglContext	Ljavax/microedition/khronos/egl/EGLContext;
            //     238: aload_0
            //     239: invokestatic 390	java/lang/Thread:currentThread	()Ljava/lang/Thread;
            //     242: putfield 384	android/view/HardwareRenderer$GlRenderer:mEglThread	Ljava/lang/Thread;
            //     245: aload_0
            //     246: getfield 401	android/view/HardwareRenderer$GlRenderer:mEglContext	Ljavax/microedition/khronos/egl/EGLContext;
            //     249: ifnonnull +34 -> 283
            //     252: aload_0
            //     253: aload_0
            //     254: getstatic 206	android/view/HardwareRenderer$GlRenderer:sEgl	Ljavax/microedition/khronos/egl/EGL10;
            //     257: getstatic 208	android/view/HardwareRenderer$GlRenderer:sEglDisplay	Ljavax/microedition/khronos/egl/EGLDisplay;
            //     260: getstatic 249	android/view/HardwareRenderer$GlRenderer:sEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     263: invokevirtual 673	android/view/HardwareRenderer$GlRenderer:createContext	(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;
            //     266: putfield 401	android/view/HardwareRenderer$GlRenderer:mEglContext	Ljavax/microedition/khronos/egl/EGLContext;
            //     269: getstatic 86	android/view/HardwareRenderer$GlRenderer:sEglContextStorage	Ljava/lang/ThreadLocal;
            //     272: aload_0
            //     273: aload_0
            //     274: getfield 401	android/view/HardwareRenderer$GlRenderer:mEglContext	Ljavax/microedition/khronos/egl/EGLContext;
            //     277: invokevirtual 675	android/view/HardwareRenderer$GlRenderer:createManagedContext	(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/opengl/ManagedEGLContext;
            //     280: invokevirtual 679	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
            //     283: return
            //     284: aconst_null
            //     285: astore 4
            //     287: goto -55 -> 232
            //
            // Exception table:
            //     from	to	target	type
            //     6	91	88	finally
            //     93	212	88	finally
        }

        void invalidate(SurfaceHolder paramSurfaceHolder)
        {
            sEgl.eglMakeCurrent(sEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            if ((this.mEglSurface != null) && (this.mEglSurface != EGL10.EGL_NO_SURFACE))
            {
                sEgl.eglDestroySurface(sEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
                setEnabled(false);
            }
            if ((!paramSurfaceHolder.getSurface().isValid()) || (!createSurface(paramSurfaceHolder)));
            while (true)
            {
                return;
                this.mUpdateDirtyRegions = true;
                if (this.mCanvas != null)
                    setEnabled(true);
            }
        }

        void onPostDraw()
        {
        }

        int onPreDraw(Rect paramRect)
        {
            return 0;
        }

        void setup(int paramInt1, int paramInt2)
        {
            if (validate())
            {
                this.mCanvas.setViewport(paramInt1, paramInt2);
                this.mWidth = paramInt1;
                this.mHeight = paramInt2;
            }
        }

        void updateSurface(SurfaceHolder paramSurfaceHolder)
            throws Surface.OutOfResourcesException
        {
            if ((isRequested()) && (isEnabled()))
                createEglSurface(paramSurfaceHolder);
        }

        boolean validate()
        {
            if (checkCurrent() != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        class FunctorsRunnable
            implements Runnable
        {
            View.AttachInfo attachInfo;

            FunctorsRunnable()
            {
            }

            public void run()
            {
                HardwareRenderer localHardwareRenderer = this.attachInfo.mHardwareRenderer;
                if ((localHardwareRenderer == null) || (!localHardwareRenderer.isEnabled()) || (localHardwareRenderer != HardwareRenderer.GlRenderer.this));
                while (true)
                {
                    return;
                    if (HardwareRenderer.GlRenderer.this.checkCurrent() != 0)
                    {
                        int i = HardwareRenderer.GlRenderer.this.mCanvas.invokeFunctors(HardwareRenderer.GlRenderer.this.mRedrawClip);
                        HardwareRenderer.GlRenderer.this.handleFunctorStatus(this.attachInfo, i);
                    }
                }
            }
        }
    }

    static abstract interface HardwareDrawCallbacks
    {
        public abstract void onHardwarePostDraw(HardwareCanvas paramHardwareCanvas);

        public abstract void onHardwarePreDraw(HardwareCanvas paramHardwareCanvas);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.HardwareRenderer
 * JD-Core Version:        0.6.2
 */