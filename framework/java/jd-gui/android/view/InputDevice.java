package android.view;

import android.hardware.input.InputManager;
import android.os.NullVibrator;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.Vibrator;
import java.util.ArrayList;
import java.util.List;

public final class InputDevice
    implements Parcelable
{
    public static final Parcelable.Creator<InputDevice> CREATOR = new Parcelable.Creator()
    {
        public InputDevice createFromParcel(Parcel paramAnonymousParcel)
        {
            return new InputDevice(paramAnonymousParcel, null);
        }

        public InputDevice[] newArray(int paramAnonymousInt)
        {
            return new InputDevice[paramAnonymousInt];
        }
    };
    public static final int KEYBOARD_TYPE_ALPHABETIC = 2;
    public static final int KEYBOARD_TYPE_NONE = 0;
    public static final int KEYBOARD_TYPE_NON_ALPHABETIC = 1;

    @Deprecated
    public static final int MOTION_RANGE_ORIENTATION = 8;

    @Deprecated
    public static final int MOTION_RANGE_PRESSURE = 2;

    @Deprecated
    public static final int MOTION_RANGE_SIZE = 3;

    @Deprecated
    public static final int MOTION_RANGE_TOOL_MAJOR = 6;

    @Deprecated
    public static final int MOTION_RANGE_TOOL_MINOR = 7;

    @Deprecated
    public static final int MOTION_RANGE_TOUCH_MAJOR = 4;

    @Deprecated
    public static final int MOTION_RANGE_TOUCH_MINOR = 5;

    @Deprecated
    public static final int MOTION_RANGE_X = 0;

    @Deprecated
    public static final int MOTION_RANGE_Y = 1;
    public static final int SOURCE_ANY = -256;
    public static final int SOURCE_CLASS_BUTTON = 1;
    public static final int SOURCE_CLASS_JOYSTICK = 16;
    public static final int SOURCE_CLASS_MASK = 255;
    public static final int SOURCE_CLASS_POINTER = 2;
    public static final int SOURCE_CLASS_POSITION = 8;
    public static final int SOURCE_CLASS_TRACKBALL = 4;
    public static final int SOURCE_DPAD = 513;
    public static final int SOURCE_GAMEPAD = 1025;
    public static final int SOURCE_JOYSTICK = 16777232;
    public static final int SOURCE_KEYBOARD = 257;
    public static final int SOURCE_MOUSE = 8194;
    public static final int SOURCE_STYLUS = 16386;
    public static final int SOURCE_TOUCHPAD = 1048584;
    public static final int SOURCE_TOUCHSCREEN = 4098;
    public static final int SOURCE_TRACKBALL = 65540;
    public static final int SOURCE_UNKNOWN;
    private final String mDescriptor;
    private final int mGeneration;
    private final boolean mHasVibrator;
    private final int mId;
    private final boolean mIsExternal;
    private final KeyCharacterMap mKeyCharacterMap;
    private final int mKeyboardType;
    private final ArrayList<MotionRange> mMotionRanges = new ArrayList();
    private final String mName;
    private final int mSources;
    private Vibrator mVibrator;

    private InputDevice(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean1, int paramInt3, int paramInt4, KeyCharacterMap paramKeyCharacterMap, boolean paramBoolean2)
    {
        this.mId = paramInt1;
        this.mGeneration = paramInt2;
        this.mName = paramString1;
        this.mDescriptor = paramString2;
        this.mIsExternal = paramBoolean1;
        this.mSources = paramInt3;
        this.mKeyboardType = paramInt4;
        this.mKeyCharacterMap = paramKeyCharacterMap;
        this.mHasVibrator = paramBoolean2;
    }

    private InputDevice(Parcel paramParcel)
    {
        this.mId = paramParcel.readInt();
        this.mGeneration = paramParcel.readInt();
        this.mName = paramParcel.readString();
        this.mDescriptor = paramParcel.readString();
        boolean bool2;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.mIsExternal = bool2;
            this.mSources = paramParcel.readInt();
            this.mKeyboardType = paramParcel.readInt();
            this.mKeyCharacterMap = ((KeyCharacterMap)KeyCharacterMap.CREATOR.createFromParcel(paramParcel));
            if (paramParcel.readInt() == 0)
                break label124;
            label102: this.mHasVibrator = bool1;
        }
        while (true)
        {
            int i = paramParcel.readInt();
            if (i < 0)
            {
                return;
                bool2 = false;
                break;
                label124: bool1 = false;
                break label102;
            }
            addMotionRange(i, paramParcel.readInt(), paramParcel.readFloat(), paramParcel.readFloat(), paramParcel.readFloat(), paramParcel.readFloat());
        }
    }

    private void addMotionRange(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.mMotionRanges.add(new MotionRange(paramInt1, paramInt2, paramFloat1, paramFloat2, paramFloat3, paramFloat4, null));
    }

    private void appendSourceDescriptionIfApplicable(StringBuilder paramStringBuilder, int paramInt, String paramString)
    {
        if ((paramInt & this.mSources) == paramInt)
        {
            paramStringBuilder.append(" ");
            paramStringBuilder.append(paramString);
        }
    }

    public static InputDevice getDevice(int paramInt)
    {
        return InputManager.getInstance().getInputDevice(paramInt);
    }

    public static int[] getDeviceIds()
    {
        return InputManager.getInstance().getInputDeviceIds();
    }

    public int describeContents()
    {
        return 0;
    }

    public String getDescriptor()
    {
        return this.mDescriptor;
    }

    public int getGeneration()
    {
        return this.mGeneration;
    }

    public int getId()
    {
        return this.mId;
    }

    public KeyCharacterMap getKeyCharacterMap()
    {
        return this.mKeyCharacterMap;
    }

    public int getKeyboardType()
    {
        return this.mKeyboardType;
    }

    public MotionRange getMotionRange(int paramInt)
    {
        int i = this.mMotionRanges.size();
        int j = 0;
        MotionRange localMotionRange;
        if (j < i)
        {
            localMotionRange = (MotionRange)this.mMotionRanges.get(j);
            if (localMotionRange.mAxis != paramInt);
        }
        while (true)
        {
            return localMotionRange;
            j++;
            break;
            localMotionRange = null;
        }
    }

    public MotionRange getMotionRange(int paramInt1, int paramInt2)
    {
        int i = this.mMotionRanges.size();
        int j = 0;
        MotionRange localMotionRange;
        if (j < i)
        {
            localMotionRange = (MotionRange)this.mMotionRanges.get(j);
            if ((localMotionRange.mAxis != paramInt1) || (localMotionRange.mSource != paramInt2));
        }
        while (true)
        {
            return localMotionRange;
            j++;
            break;
            localMotionRange = null;
        }
    }

    public List<MotionRange> getMotionRanges()
    {
        return this.mMotionRanges;
    }

    public String getName()
    {
        return this.mName;
    }

    public int getSources()
    {
        return this.mSources;
    }

    public Vibrator getVibrator()
    {
        synchronized (this.mMotionRanges)
        {
            if (this.mVibrator == null)
            {
                if (this.mHasVibrator)
                    this.mVibrator = InputManager.getInstance().getInputDeviceVibrator(this.mId);
            }
            else
            {
                Vibrator localVibrator = this.mVibrator;
                return localVibrator;
            }
            this.mVibrator = NullVibrator.getInstance();
        }
    }

    public boolean isExternal()
    {
        return this.mIsExternal;
    }

    public boolean isFullKeyboard()
    {
        if (((0x101 & this.mSources) == 257) && (this.mKeyboardType == 2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isVirtual()
    {
        if (this.mId < 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("Input Device ").append(this.mId).append(": ").append(this.mName).append("\n");
        localStringBuilder1.append("    Descriptor: ").append(this.mDescriptor).append("\n");
        localStringBuilder1.append("    Generation: ").append(this.mGeneration).append("\n");
        StringBuilder localStringBuilder2 = localStringBuilder1.append("    Location: ");
        String str;
        if (this.mIsExternal)
        {
            str = "external";
            localStringBuilder2.append(str).append("\n");
            localStringBuilder1.append("    Keyboard Type: ");
            switch (this.mKeyboardType)
            {
            default:
            case 0:
            case 1:
            case 2:
            }
        }
        while (true)
        {
            localStringBuilder1.append("\n");
            localStringBuilder1.append("    Has Vibrator: ").append(this.mHasVibrator).append("\n");
            localStringBuilder1.append("    Sources: 0x").append(Integer.toHexString(this.mSources)).append(" (");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 257, "keyboard");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 513, "dpad");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 4098, "touchscreen");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 8194, "mouse");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 16386, "stylus");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 65540, "trackball");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 1048584, "touchpad");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 16777232, "joystick");
            appendSourceDescriptionIfApplicable(localStringBuilder1, 1025, "gamepad");
            localStringBuilder1.append(" )\n");
            int i = this.mMotionRanges.size();
            for (int j = 0; j < i; j++)
            {
                MotionRange localMotionRange = (MotionRange)this.mMotionRanges.get(j);
                localStringBuilder1.append("        ").append(MotionEvent.axisToString(localMotionRange.mAxis));
                localStringBuilder1.append(": source=0x").append(Integer.toHexString(localMotionRange.mSource));
                localStringBuilder1.append(" min=").append(localMotionRange.mMin);
                localStringBuilder1.append(" max=").append(localMotionRange.mMax);
                localStringBuilder1.append(" flat=").append(localMotionRange.mFlat);
                localStringBuilder1.append(" fuzz=").append(localMotionRange.mFuzz);
                localStringBuilder1.append("\n");
            }
            str = "built-in";
            break;
            localStringBuilder1.append("none");
            continue;
            localStringBuilder1.append("non-alphabetic");
            continue;
            localStringBuilder1.append("alphabetic");
        }
        return localStringBuilder1.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(this.mId);
        paramParcel.writeInt(this.mGeneration);
        paramParcel.writeString(this.mName);
        paramParcel.writeString(this.mDescriptor);
        int j;
        if (this.mIsExternal)
        {
            j = i;
            paramParcel.writeInt(j);
            paramParcel.writeInt(this.mSources);
            paramParcel.writeInt(this.mKeyboardType);
            this.mKeyCharacterMap.writeToParcel(paramParcel, paramInt);
            if (!this.mHasVibrator)
                break label186;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            int k = this.mMotionRanges.size();
            for (int m = 0; m < k; m++)
            {
                MotionRange localMotionRange = (MotionRange)this.mMotionRanges.get(m);
                paramParcel.writeInt(localMotionRange.mAxis);
                paramParcel.writeInt(localMotionRange.mSource);
                paramParcel.writeFloat(localMotionRange.mMin);
                paramParcel.writeFloat(localMotionRange.mMax);
                paramParcel.writeFloat(localMotionRange.mFlat);
                paramParcel.writeFloat(localMotionRange.mFuzz);
            }
            j = 0;
            break;
            label186: i = 0;
        }
        paramParcel.writeInt(-1);
    }

    public static final class MotionRange
    {
        private int mAxis;
        private float mFlat;
        private float mFuzz;
        private float mMax;
        private float mMin;
        private int mSource;

        private MotionRange(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
        {
            this.mAxis = paramInt1;
            this.mSource = paramInt2;
            this.mMin = paramFloat1;
            this.mMax = paramFloat2;
            this.mFlat = paramFloat3;
            this.mFuzz = paramFloat4;
        }

        public int getAxis()
        {
            return this.mAxis;
        }

        public float getFlat()
        {
            return this.mFlat;
        }

        public float getFuzz()
        {
            return this.mFuzz;
        }

        public float getMax()
        {
            return this.mMax;
        }

        public float getMin()
        {
            return this.mMin;
        }

        public float getRange()
        {
            return this.mMax - this.mMin;
        }

        public int getSource()
        {
            return this.mSource;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.view.InputDevice
 * JD-Core Version:        0.6.2
 */