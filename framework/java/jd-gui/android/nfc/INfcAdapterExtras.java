package android.nfc;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INfcAdapterExtras extends IInterface
{
    public abstract void authenticate(String paramString, byte[] paramArrayOfByte)
        throws RemoteException;

    public abstract Bundle close(String paramString, IBinder paramIBinder)
        throws RemoteException;

    public abstract int getCardEmulationRoute(String paramString)
        throws RemoteException;

    public abstract Bundle open(String paramString, IBinder paramIBinder)
        throws RemoteException;

    public abstract void setCardEmulationRoute(String paramString, int paramInt)
        throws RemoteException;

    public abstract Bundle transceive(String paramString, byte[] paramArrayOfByte)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INfcAdapterExtras
    {
        private static final String DESCRIPTOR = "android.nfc.INfcAdapterExtras";
        static final int TRANSACTION_authenticate = 6;
        static final int TRANSACTION_close = 2;
        static final int TRANSACTION_getCardEmulationRoute = 4;
        static final int TRANSACTION_open = 1;
        static final int TRANSACTION_setCardEmulationRoute = 5;
        static final int TRANSACTION_transceive = 3;

        public Stub()
        {
            attachInterface(this, "android.nfc.INfcAdapterExtras");
        }

        public static INfcAdapterExtras asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.nfc.INfcAdapterExtras");
                if ((localIInterface != null) && ((localIInterface instanceof INfcAdapterExtras)))
                    localObject = (INfcAdapterExtras)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.nfc.INfcAdapterExtras");
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapterExtras");
                Bundle localBundle3 = open(paramParcel1.readString(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                if (localBundle3 != null)
                {
                    paramParcel2.writeInt(i);
                    localBundle3.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INfcAdapterExtras");
                    Bundle localBundle2 = close(paramParcel1.readString(), paramParcel1.readStrongBinder());
                    paramParcel2.writeNoException();
                    if (localBundle2 != null)
                    {
                        paramParcel2.writeInt(i);
                        localBundle2.writeToParcel(paramParcel2, i);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcAdapterExtras");
                        Bundle localBundle1 = transceive(paramParcel1.readString(), paramParcel1.createByteArray());
                        paramParcel2.writeNoException();
                        if (localBundle1 != null)
                        {
                            paramParcel2.writeInt(i);
                            localBundle1.writeToParcel(paramParcel2, i);
                        }
                        else
                        {
                            paramParcel2.writeInt(0);
                            continue;
                            paramParcel1.enforceInterface("android.nfc.INfcAdapterExtras");
                            int j = getCardEmulationRoute(paramParcel1.readString());
                            paramParcel2.writeNoException();
                            paramParcel2.writeInt(j);
                            continue;
                            paramParcel1.enforceInterface("android.nfc.INfcAdapterExtras");
                            setCardEmulationRoute(paramParcel1.readString(), paramParcel1.readInt());
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("android.nfc.INfcAdapterExtras");
                            authenticate(paramParcel1.readString(), paramParcel1.createByteArray());
                            paramParcel2.writeNoException();
                        }
                    }
                }
            }
        }

        private static class Proxy
            implements INfcAdapterExtras
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void authenticate(String paramString, byte[] paramArrayOfByte)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
                    localParcel1.writeString(paramString);
                    localParcel1.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Bundle close(String paramString, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
                    localParcel1.writeString(paramString);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBundle = (Bundle)Bundle.CREATOR.createFromParcel(localParcel2);
                        return localBundle;
                    }
                    Bundle localBundle = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getCardEmulationRoute(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.nfc.INfcAdapterExtras";
            }

            public Bundle open(String paramString, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
                    localParcel1.writeString(paramString);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBundle = (Bundle)Bundle.CREATOR.createFromParcel(localParcel2);
                        return localBundle;
                    }
                    Bundle localBundle = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCardEmulationRoute(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Bundle transceive(String paramString, byte[] paramArrayOfByte)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
                    localParcel1.writeString(paramString);
                    localParcel1.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBundle = (Bundle)Bundle.CREATOR.createFromParcel(localParcel2);
                        return localBundle;
                    }
                    Bundle localBundle = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.INfcAdapterExtras
 * JD-Core Version:        0.6.2
 */