package android.nfc;

import android.app.PendingIntent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INfcAdapter extends IInterface
{
    public abstract boolean disable(boolean paramBoolean)
        throws RemoteException;

    public abstract boolean disableNdefPush()
        throws RemoteException;

    public abstract void dispatch(Tag paramTag)
        throws RemoteException;

    public abstract boolean enable()
        throws RemoteException;

    public abstract boolean enableNdefPush()
        throws RemoteException;

    public abstract INfcAdapterExtras getNfcAdapterExtrasInterface(String paramString)
        throws RemoteException;

    public abstract INfcTag getNfcTagInterface()
        throws RemoteException;

    public abstract int getState()
        throws RemoteException;

    public abstract boolean isNdefPushEnabled()
        throws RemoteException;

    public abstract void setForegroundDispatch(PendingIntent paramPendingIntent, IntentFilter[] paramArrayOfIntentFilter, TechListParcel paramTechListParcel)
        throws RemoteException;

    public abstract void setNdefPushCallback(INdefPushCallback paramINdefPushCallback)
        throws RemoteException;

    public abstract void setP2pModes(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INfcAdapter
    {
        private static final String DESCRIPTOR = "android.nfc.INfcAdapter";
        static final int TRANSACTION_disable = 4;
        static final int TRANSACTION_disableNdefPush = 7;
        static final int TRANSACTION_dispatch = 11;
        static final int TRANSACTION_enable = 5;
        static final int TRANSACTION_enableNdefPush = 6;
        static final int TRANSACTION_getNfcAdapterExtrasInterface = 2;
        static final int TRANSACTION_getNfcTagInterface = 1;
        static final int TRANSACTION_getState = 3;
        static final int TRANSACTION_isNdefPushEnabled = 8;
        static final int TRANSACTION_setForegroundDispatch = 9;
        static final int TRANSACTION_setNdefPushCallback = 10;
        static final int TRANSACTION_setP2pModes = 12;

        public Stub()
        {
            attachInterface(this, "android.nfc.INfcAdapter");
        }

        public static INfcAdapter asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.nfc.INfcAdapter");
                if ((localIInterface != null) && ((localIInterface instanceof INfcAdapter)))
                    localObject = (INfcAdapter)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            IBinder localIBinder = null;
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.nfc.INfcAdapter");
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                INfcTag localINfcTag = getNfcTagInterface();
                paramParcel2.writeNoException();
                if (localINfcTag != null)
                    localIBinder = localINfcTag.asBinder();
                paramParcel2.writeStrongBinder(localIBinder);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                INfcAdapterExtras localINfcAdapterExtras = getNfcAdapterExtrasInterface(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localINfcAdapterExtras != null)
                    localIBinder = localINfcAdapterExtras.asBinder();
                paramParcel2.writeStrongBinder(localIBinder);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                int k = getState();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                if (paramParcel1.readInt() != 0);
                for (boolean bool5 = j; ; bool5 = false)
                {
                    boolean bool6 = disable(bool5);
                    paramParcel2.writeNoException();
                    if (bool6)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                boolean bool4 = enable();
                paramParcel2.writeNoException();
                if (bool4)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                boolean bool3 = enableNdefPush();
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                boolean bool2 = disableNdefPush();
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                boolean bool1 = isNdefPushEnabled();
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                PendingIntent localPendingIntent;
                label470: IntentFilter[] arrayOfIntentFilter;
                if (paramParcel1.readInt() != 0)
                {
                    localPendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1);
                    arrayOfIntentFilter = (IntentFilter[])paramParcel1.createTypedArray(IntentFilter.CREATOR);
                    if (paramParcel1.readInt() == 0)
                        break label526;
                }
                label526: for (TechListParcel localTechListParcel = (TechListParcel)TechListParcel.CREATOR.createFromParcel(paramParcel1); ; localTechListParcel = null)
                {
                    setForegroundDispatch(localPendingIntent, arrayOfIntentFilter, localTechListParcel);
                    paramParcel2.writeNoException();
                    break;
                    localPendingIntent = null;
                    break label470;
                }
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                setNdefPushCallback(INdefPushCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                if (paramParcel1.readInt() != 0);
                for (Tag localTag = (Tag)Tag.CREATOR.createFromParcel(paramParcel1); ; localTag = null)
                {
                    dispatch(localTag);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.nfc.INfcAdapter");
                setP2pModes(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements INfcAdapter
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean disable(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean disableNdefPush()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void dispatch(Tag paramTag)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    if (paramTag != null)
                    {
                        localParcel1.writeInt(1);
                        paramTag.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean enable()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean enableNdefPush()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.nfc.INfcAdapter";
            }

            public INfcAdapterExtras getNfcAdapterExtrasInterface(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    INfcAdapterExtras localINfcAdapterExtras = INfcAdapterExtras.Stub.asInterface(localParcel2.readStrongBinder());
                    return localINfcAdapterExtras;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public INfcTag getNfcTagInterface()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    INfcTag localINfcTag = INfcTag.Stub.asInterface(localParcel2.readStrongBinder());
                    return localINfcTag;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isNdefPushEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setForegroundDispatch(PendingIntent paramPendingIntent, IntentFilter[] paramArrayOfIntentFilter, TechListParcel paramTechListParcel)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                        if (paramPendingIntent != null)
                        {
                            localParcel1.writeInt(1);
                            paramPendingIntent.writeToParcel(localParcel1, 0);
                            localParcel1.writeTypedArray(paramArrayOfIntentFilter, 0);
                            if (paramTechListParcel != null)
                            {
                                localParcel1.writeInt(1);
                                paramTechListParcel.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(9, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void setNdefPushCallback(INdefPushCallback paramINdefPushCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    if (paramINdefPushCallback != null)
                    {
                        localIBinder = paramINdefPushCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setP2pModes(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcAdapter");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.INfcAdapter
 * JD-Core Version:        0.6.2
 */