package android.nfc;

import android.app.Activity;
import android.app.ActivityThread;
import android.app.OnActivityPausedListener;
import android.app.PendingIntent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.HashMap;

public final class NfcAdapter
{
    public static final String ACTION_ADAPTER_STATE_CHANGED = "android.nfc.action.ADAPTER_STATE_CHANGED";
    public static final String ACTION_HANDOVER_TRANSFER_DONE = "android.nfc.action.HANDOVER_TRANSFER_DONE";
    public static final String ACTION_HANDOVER_TRANSFER_STARTED = "android.nfc.action.HANDOVER_TRANSFER_STARTED";
    public static final String ACTION_NDEF_DISCOVERED = "android.nfc.action.NDEF_DISCOVERED";
    public static final String ACTION_TAG_DISCOVERED = "android.nfc.action.TAG_DISCOVERED";
    public static final String ACTION_TAG_LEFT_FIELD = "android.nfc.action.TAG_LOST";
    public static final String ACTION_TECH_DISCOVERED = "android.nfc.action.TECH_DISCOVERED";
    public static final String EXTRA_ADAPTER_STATE = "android.nfc.extra.ADAPTER_STATE";
    public static final String EXTRA_HANDOVER_TRANSFER_STATUS = "android.nfc.extra.HANDOVER_TRANSFER_STATUS";
    public static final String EXTRA_HANDOVER_TRANSFER_URI = "android.nfc.extra.HANDOVER_TRANSFER_URI";
    public static final String EXTRA_ID = "android.nfc.extra.ID";
    public static final String EXTRA_NDEF_MESSAGES = "android.nfc.extra.NDEF_MESSAGES";
    public static final String EXTRA_TAG = "android.nfc.extra.TAG";
    public static final int HANDOVER_TRANSFER_STATUS_FAILURE = 1;
    public static final int HANDOVER_TRANSFER_STATUS_SUCCESS = 0;
    public static final int STATE_OFF = 1;
    public static final int STATE_ON = 3;
    public static final int STATE_TURNING_OFF = 4;
    public static final int STATE_TURNING_ON = 2;
    static final String TAG = "NFC";
    static boolean sIsInitialized = false;
    static HashMap<Context, NfcAdapter> sNfcAdapters = new HashMap();
    static NfcAdapter sNullContextNfcAdapter;
    static INfcAdapter sService;
    static INfcTag sTagService;
    final Context mContext;
    OnActivityPausedListener mForegroundDispatchListener = new OnActivityPausedListener()
    {
        public void onPaused(Activity paramAnonymousActivity)
        {
            NfcAdapter.this.disableForegroundDispatchInternal(paramAnonymousActivity, true);
        }
    };
    final NfcActivityManager mNfcActivityManager;

    NfcAdapter(Context paramContext)
    {
        this.mContext = paramContext;
        this.mNfcActivityManager = new NfcActivityManager(this);
    }

    @Deprecated
    public static NfcAdapter getDefaultAdapter()
    {
        Log.w("NFC", "WARNING: NfcAdapter.getDefaultAdapter() is deprecated, use NfcAdapter.getDefaultAdapter(Context) instead", new Exception());
        return getNfcAdapter(null);
    }

    public static NfcAdapter getDefaultAdapter(Context paramContext)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context cannot be null");
        Context localContext = paramContext.getApplicationContext();
        if (localContext == null)
            throw new IllegalArgumentException("context not associated with any application (using a mock context?)");
        NfcManager localNfcManager = (NfcManager)localContext.getSystemService("nfc");
        if (localNfcManager == null);
        for (NfcAdapter localNfcAdapter = null; ; localNfcAdapter = localNfcManager.getDefaultAdapter())
            return localNfcAdapter;
    }

    /** @deprecated */
    public static NfcAdapter getNfcAdapter(Context paramContext)
    {
        try
        {
            if (sIsInitialized)
                break label80;
            if (!hasNfcFeature())
            {
                Log.v("NFC", "this device does not have NFC support");
                throw new UnsupportedOperationException();
            }
        }
        finally
        {
        }
        sService = getServiceInterface();
        if (sService == null)
        {
            Log.e("NFC", "could not retrieve NFC service");
            throw new UnsupportedOperationException();
        }
        while (true)
        {
            try
            {
                sTagService = sService.getNfcTagInterface();
                sIsInitialized = true;
                label80: if (paramContext == null)
                {
                    if (sNullContextNfcAdapter == null)
                        sNullContextNfcAdapter = new NfcAdapter(null);
                    localNfcAdapter = sNullContextNfcAdapter;
                    return localNfcAdapter;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("NFC", "could not retrieve NFC Tag service");
                throw new UnsupportedOperationException();
            }
            NfcAdapter localNfcAdapter = (NfcAdapter)sNfcAdapters.get(paramContext);
            if (localNfcAdapter == null)
            {
                localNfcAdapter = new NfcAdapter(paramContext);
                sNfcAdapters.put(paramContext, localNfcAdapter);
            }
        }
    }

    private static INfcAdapter getServiceInterface()
    {
        IBinder localIBinder = ServiceManager.getService("nfc");
        if (localIBinder == null);
        for (INfcAdapter localINfcAdapter = null; ; localINfcAdapter = INfcAdapter.Stub.asInterface(localIBinder))
            return localINfcAdapter;
    }

    private static boolean hasNfcFeature()
    {
        boolean bool1 = false;
        IPackageManager localIPackageManager = ActivityThread.getPackageManager();
        if (localIPackageManager == null)
            Log.e("NFC", "Cannot get package manager, assuming no NFC feature");
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = localIPackageManager.hasSystemFeature("android.hardware.nfc");
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("NFC", "Package manager query failed, assuming no NFC feature", localRemoteException);
            }
        }
    }

    public void attemptDeadServiceRecovery(Exception paramException)
    {
        Log.e("NFC", "NFC service dead - attempting to recover", paramException);
        INfcAdapter localINfcAdapter = getServiceInterface();
        if (localINfcAdapter == null)
            Log.e("NFC", "could not retrieve NFC service during service recovery");
        while (true)
        {
            return;
            sService = localINfcAdapter;
            try
            {
                sTagService = localINfcAdapter.getNfcTagInterface();
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("NFC", "could not retrieve NFC tag service during service recovery");
            }
        }
    }

    public boolean disable()
    {
        try
        {
            boolean bool2 = sService.disable(true);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void disableForegroundDispatch(Activity paramActivity)
    {
        ActivityThread.currentActivityThread().unregisterOnActivityPausedListener(paramActivity, this.mForegroundDispatchListener);
        disableForegroundDispatchInternal(paramActivity, false);
    }

    void disableForegroundDispatchInternal(Activity paramActivity, boolean paramBoolean)
    {
        try
        {
            sService.setForegroundDispatch(null, null, null);
            if ((!paramBoolean) && (!paramActivity.isResumed()))
                throw new IllegalStateException("You must disable foreground dispatching while your activity is still resumed");
        }
        catch (RemoteException localRemoteException)
        {
            attemptDeadServiceRecovery(localRemoteException);
        }
    }

    @Deprecated
    public void disableForegroundNdefPush(Activity paramActivity)
    {
        if (paramActivity == null)
            throw new NullPointerException();
        enforceResumed(paramActivity);
        this.mNfcActivityManager.setNdefPushMessage(paramActivity, null);
        this.mNfcActivityManager.setNdefPushMessageCallback(paramActivity, null);
        this.mNfcActivityManager.setOnNdefPushCompleteCallback(paramActivity, null);
    }

    public boolean disableNdefPush()
    {
        try
        {
            boolean bool2 = sService.disableNdefPush();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void dispatch(Tag paramTag)
    {
        if (paramTag == null)
            throw new NullPointerException("tag cannot be null");
        try
        {
            sService.dispatch(paramTag);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                attemptDeadServiceRecovery(localRemoteException);
        }
    }

    public boolean enable()
    {
        try
        {
            boolean bool2 = sService.enable();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void enableForegroundDispatch(Activity paramActivity, PendingIntent paramPendingIntent, IntentFilter[] paramArrayOfIntentFilter, String[][] paramArrayOfString)
    {
        if ((paramActivity == null) || (paramPendingIntent == null))
            throw new NullPointerException();
        if (!paramActivity.isResumed())
            throw new IllegalStateException("Foreground dispatch can only be enabled when your activity is resumed");
        TechListParcel localTechListParcel = null;
        if (paramArrayOfString != null);
        try
        {
            if (paramArrayOfString.length > 0)
                localTechListParcel = new TechListParcel(paramArrayOfString);
            ActivityThread.currentActivityThread().registerOnActivityPausedListener(paramActivity, this.mForegroundDispatchListener);
            sService.setForegroundDispatch(paramPendingIntent, paramArrayOfIntentFilter, localTechListParcel);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                attemptDeadServiceRecovery(localRemoteException);
        }
    }

    @Deprecated
    public void enableForegroundNdefPush(Activity paramActivity, NdefMessage paramNdefMessage)
    {
        if ((paramActivity == null) || (paramNdefMessage == null))
            throw new NullPointerException();
        enforceResumed(paramActivity);
        this.mNfcActivityManager.setNdefPushMessage(paramActivity, paramNdefMessage);
    }

    public boolean enableNdefPush()
    {
        try
        {
            boolean bool2 = sService.enableNdefPush();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    void enforceResumed(Activity paramActivity)
    {
        if (!paramActivity.isResumed())
            throw new IllegalStateException("API cannot be called while activity is paused");
    }

    public int getAdapterState()
    {
        try
        {
            int j = sService.getState();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                int i = 1;
            }
        }
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public INfcAdapterExtras getNfcAdapterExtrasInterface()
    {
        if (this.mContext == null)
            throw new UnsupportedOperationException("You need a context on NfcAdapter to use the    NFC extras APIs");
        try
        {
            INfcAdapterExtras localINfcAdapterExtras2 = sService.getNfcAdapterExtrasInterface(this.mContext.getPackageName());
            localINfcAdapterExtras1 = localINfcAdapterExtras2;
            return localINfcAdapterExtras1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                INfcAdapterExtras localINfcAdapterExtras1 = null;
            }
        }
    }

    int getSdkVersion()
    {
        if (this.mContext == null);
        for (int i = 9; ; i = this.mContext.getApplicationInfo().targetSdkVersion)
            return i;
    }

    public INfcAdapter getService()
    {
        isEnabled();
        return sService;
    }

    public INfcTag getTagService()
    {
        isEnabled();
        return sTagService;
    }

    public boolean isEnabled()
    {
        boolean bool = false;
        try
        {
            int i = sService.getState();
            if (i == 3)
                bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                attemptDeadServiceRecovery(localRemoteException);
        }
    }

    public boolean isNdefPushEnabled()
    {
        try
        {
            boolean bool2 = sService.isNdefPushEnabled();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                attemptDeadServiceRecovery(localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void setBeamPushUris(Uri[] paramArrayOfUri, Activity paramActivity)
    {
        if (paramActivity == null)
            throw new NullPointerException("activity cannot be null");
        if (paramArrayOfUri != null)
        {
            int i = paramArrayOfUri.length;
            for (int j = 0; j < i; j++)
            {
                Uri localUri = paramArrayOfUri[j];
                if (localUri == null)
                    throw new NullPointerException("Uri not allowed to be null");
                String str = localUri.getScheme();
                if ((str == null) || ((!str.equalsIgnoreCase("file")) && (!str.equalsIgnoreCase("content"))))
                    throw new IllegalArgumentException("URI needs to have either scheme file or scheme content");
            }
        }
        this.mNfcActivityManager.setNdefPushContentUri(paramActivity, paramArrayOfUri);
    }

    public void setBeamPushUrisCallback(CreateBeamUrisCallback paramCreateBeamUrisCallback, Activity paramActivity)
    {
        if (paramActivity == null)
            throw new NullPointerException("activity cannot be null");
        this.mNfcActivityManager.setNdefPushContentUriCallback(paramActivity, paramCreateBeamUrisCallback);
    }

    public void setNdefPushMessage(NdefMessage paramNdefMessage, Activity paramActivity, Activity[] paramArrayOfActivity)
    {
        int i = getSdkVersion();
        if (paramActivity == null)
        {
            try
            {
                throw new NullPointerException("activity cannot be null");
            }
            catch (IllegalStateException localIllegalStateException)
            {
                if (i >= 16)
                    break label103;
            }
            Log.e("NFC", "Cannot call API with Activity that has already been destroyed", localIllegalStateException);
        }
        else
        {
            while (true)
            {
                return;
                this.mNfcActivityManager.setNdefPushMessage(paramActivity, paramNdefMessage);
                int j = paramArrayOfActivity.length;
                for (int k = 0; k < j; k++)
                {
                    Activity localActivity = paramArrayOfActivity[k];
                    if (localActivity == null)
                        throw new NullPointerException("activities cannot contain null");
                    this.mNfcActivityManager.setNdefPushMessage(localActivity, paramNdefMessage);
                }
            }
        }
        label103: throw localIllegalStateException;
    }

    public void setNdefPushMessageCallback(CreateNdefMessageCallback paramCreateNdefMessageCallback, Activity paramActivity, Activity[] paramArrayOfActivity)
    {
        int i = getSdkVersion();
        if (paramActivity == null)
        {
            try
            {
                throw new NullPointerException("activity cannot be null");
            }
            catch (IllegalStateException localIllegalStateException)
            {
                if (i >= 16)
                    break label103;
            }
            Log.e("NFC", "Cannot call API with Activity that has already been destroyed", localIllegalStateException);
        }
        else
        {
            while (true)
            {
                return;
                this.mNfcActivityManager.setNdefPushMessageCallback(paramActivity, paramCreateNdefMessageCallback);
                int j = paramArrayOfActivity.length;
                for (int k = 0; k < j; k++)
                {
                    Activity localActivity = paramArrayOfActivity[k];
                    if (localActivity == null)
                        throw new NullPointerException("activities cannot contain null");
                    this.mNfcActivityManager.setNdefPushMessageCallback(localActivity, paramCreateNdefMessageCallback);
                }
            }
        }
        label103: throw localIllegalStateException;
    }

    public void setOnNdefPushCompleteCallback(OnNdefPushCompleteCallback paramOnNdefPushCompleteCallback, Activity paramActivity, Activity[] paramArrayOfActivity)
    {
        int i = getSdkVersion();
        if (paramActivity == null)
        {
            try
            {
                throw new NullPointerException("activity cannot be null");
            }
            catch (IllegalStateException localIllegalStateException)
            {
                if (i >= 16)
                    break label103;
            }
            Log.e("NFC", "Cannot call API with Activity that has already been destroyed", localIllegalStateException);
        }
        else
        {
            while (true)
            {
                return;
                this.mNfcActivityManager.setOnNdefPushCompleteCallback(paramActivity, paramOnNdefPushCompleteCallback);
                int j = paramArrayOfActivity.length;
                for (int k = 0; k < j; k++)
                {
                    Activity localActivity = paramArrayOfActivity[k];
                    if (localActivity == null)
                        throw new NullPointerException("activities cannot contain null");
                    this.mNfcActivityManager.setOnNdefPushCompleteCallback(localActivity, paramOnNdefPushCompleteCallback);
                }
            }
        }
        label103: throw localIllegalStateException;
    }

    public void setP2pModes(int paramInt1, int paramInt2)
    {
        try
        {
            sService.setP2pModes(paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                attemptDeadServiceRecovery(localRemoteException);
        }
    }

    public static abstract interface CreateBeamUrisCallback
    {
        public abstract Uri[] createBeamUris(NfcEvent paramNfcEvent);
    }

    public static abstract interface CreateNdefMessageCallback
    {
        public abstract NdefMessage createNdefMessage(NfcEvent paramNfcEvent);
    }

    public static abstract interface OnNdefPushCompleteCallback
    {
        public abstract void onNdefPushComplete(NfcEvent paramNfcEvent);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.NfcAdapter
 * JD-Core Version:        0.6.2
 */