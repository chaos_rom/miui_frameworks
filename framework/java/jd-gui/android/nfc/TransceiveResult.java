package android.nfc;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.IOException;

public final class TransceiveResult
    implements Parcelable
{
    public static final Parcelable.Creator<TransceiveResult> CREATOR = new Parcelable.Creator()
    {
        public TransceiveResult createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = paramAnonymousParcel.readInt();
            byte[] arrayOfByte;
            if (i == 0)
            {
                arrayOfByte = new byte[paramAnonymousParcel.readInt()];
                paramAnonymousParcel.readByteArray(arrayOfByte);
            }
            while (true)
            {
                return new TransceiveResult(i, arrayOfByte);
                arrayOfByte = null;
            }
        }

        public TransceiveResult[] newArray(int paramAnonymousInt)
        {
            return new TransceiveResult[paramAnonymousInt];
        }
    };
    public static final int RESULT_EXCEEDED_LENGTH = 3;
    public static final int RESULT_FAILURE = 1;
    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_TAGLOST = 2;
    final byte[] mResponseData;
    final int mResult;

    public TransceiveResult(int paramInt, byte[] paramArrayOfByte)
    {
        this.mResult = paramInt;
        this.mResponseData = paramArrayOfByte;
    }

    public int describeContents()
    {
        return 0;
    }

    public byte[] getResponseOrThrow()
        throws IOException
    {
        switch (this.mResult)
        {
        case 1:
        default:
            throw new IOException("Transceive failed");
        case 0:
            return this.mResponseData;
        case 2:
            throw new TagLostException("Tag was lost.");
        case 3:
        }
        throw new IOException("Transceive length exceeds supported maximum");
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mResult);
        if (this.mResult == 0)
        {
            paramParcel.writeInt(this.mResponseData.length);
            paramParcel.writeByteArray(this.mResponseData);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.TransceiveResult
 * JD-Core Version:        0.6.2
 */