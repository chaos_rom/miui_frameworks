package android.nfc;

public class ErrorCodes
{
    public static final int ERROR_BUFFER_TO_SMALL = -12;
    public static final int ERROR_BUSY = -4;
    public static final int ERROR_CANCELLED = -2;
    public static final int ERROR_CONNECT = -5;
    public static final int ERROR_DISCONNECT = -5;
    public static final int ERROR_INSUFFICIENT_RESOURCES = -9;
    public static final int ERROR_INVALID_PARAM = -8;
    public static final int ERROR_IO = -1;
    public static final int ERROR_NFC_ON = -16;
    public static final int ERROR_NOT_INITIALIZED = -17;
    public static final int ERROR_NOT_SUPPORTED = -21;
    public static final int ERROR_NO_SE_CONNECTED = -20;
    public static final int ERROR_READ = -6;
    public static final int ERROR_SAP_USED = -13;
    public static final int ERROR_SERVICE_NAME_USED = -14;
    public static final int ERROR_SE_ALREADY_SELECTED = -18;
    public static final int ERROR_SE_CONNECTED = -19;
    public static final int ERROR_SOCKET_CREATION = -10;
    public static final int ERROR_SOCKET_NOT_CONNECTED = -11;
    public static final int ERROR_SOCKET_OPTIONS = -15;
    public static final int ERROR_TIMEOUT = -3;
    public static final int ERROR_WRITE = -7;
    public static final int SUCCESS;

    public static String asString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "UNKNOWN ERROR";
        case 0:
        case -1:
        case -2:
        case -3:
        case -4:
        case -5:
        case -6:
        case -7:
        case -8:
        case -9:
        case -10:
        case -11:
        case -12:
        case -13:
        case -14:
        case -15:
        case -16:
        case -17:
        case -18:
        case -19:
        case -20:
        case -21:
        }
        while (true)
        {
            return str;
            str = "SUCCESS";
            continue;
            str = "IO";
            continue;
            str = "CANCELLED";
            continue;
            str = "TIMEOUT";
            continue;
            str = "BUSY";
            continue;
            str = "CONNECT/DISCONNECT";
            continue;
            str = "READ";
            continue;
            str = "WRITE";
            continue;
            str = "INVALID_PARAM";
            continue;
            str = "INSUFFICIENT_RESOURCES";
            continue;
            str = "SOCKET_CREATION";
            continue;
            str = "SOCKET_NOT_CONNECTED";
            continue;
            str = "BUFFER_TO_SMALL";
            continue;
            str = "SAP_USED";
            continue;
            str = "SERVICE_NAME_USED";
            continue;
            str = "SOCKET_OPTIONS";
            continue;
            str = "NFC_ON";
            continue;
            str = "NOT_INITIALIZED";
            continue;
            str = "SE_ALREADY_SELECTED";
            continue;
            str = "SE_CONNECTED";
            continue;
            str = "NO_SE_CONNECTED";
            continue;
            str = "NOT_SUPPORTED";
        }
    }

    public static boolean isError(int paramInt)
    {
        if (paramInt < 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.ErrorCodes
 * JD-Core Version:        0.6.2
 */