package android.nfc;

public final class NfcEvent
{
    public final NfcAdapter nfcAdapter;

    NfcEvent(NfcAdapter paramNfcAdapter)
    {
        this.nfcAdapter = paramNfcAdapter;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.NfcEvent
 * JD-Core Version:        0.6.2
 */