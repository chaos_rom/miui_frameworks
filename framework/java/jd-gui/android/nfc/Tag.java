package android.nfc;

import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.io.IOException;
import java.util.Arrays;

public final class Tag
    implements Parcelable
{
    public static final Parcelable.Creator<Tag> CREATOR = new Parcelable.Creator()
    {
        public Tag createFromParcel(Parcel paramAnonymousParcel)
        {
            byte[] arrayOfByte = Tag.readBytesWithNull(paramAnonymousParcel);
            int[] arrayOfInt = new int[paramAnonymousParcel.readInt()];
            paramAnonymousParcel.readIntArray(arrayOfInt);
            Bundle[] arrayOfBundle = (Bundle[])paramAnonymousParcel.createTypedArray(Bundle.CREATOR);
            int i = paramAnonymousParcel.readInt();
            if (paramAnonymousParcel.readInt() == 0);
            for (INfcTag localINfcTag = INfcTag.Stub.asInterface(paramAnonymousParcel.readStrongBinder()); ; localINfcTag = null)
                return new Tag(arrayOfByte, arrayOfInt, arrayOfBundle, i, localINfcTag);
        }

        public Tag[] newArray(int paramAnonymousInt)
        {
            return new Tag[paramAnonymousInt];
        }
    };
    int mConnectedTechnology;
    final byte[] mId;
    final int mServiceHandle;
    final INfcTag mTagService;
    final Bundle[] mTechExtras;
    final int[] mTechList;
    final String[] mTechStringList;

    public Tag(byte[] paramArrayOfByte, int[] paramArrayOfInt, Bundle[] paramArrayOfBundle, int paramInt, INfcTag paramINfcTag)
    {
        if (paramArrayOfInt == null)
            throw new IllegalArgumentException("rawTargets cannot be null");
        this.mId = paramArrayOfByte;
        this.mTechList = Arrays.copyOf(paramArrayOfInt, paramArrayOfInt.length);
        this.mTechStringList = generateTechStringList(paramArrayOfInt);
        this.mTechExtras = ((Bundle[])Arrays.copyOf(paramArrayOfBundle, paramArrayOfInt.length));
        this.mServiceHandle = paramInt;
        this.mTagService = paramINfcTag;
        this.mConnectedTechnology = -1;
    }

    public static Tag createMockTag(byte[] paramArrayOfByte, int[] paramArrayOfInt, Bundle[] paramArrayOfBundle)
    {
        return new Tag(paramArrayOfByte, paramArrayOfInt, paramArrayOfBundle, 0, null);
    }

    private String[] generateTechStringList(int[] paramArrayOfInt)
    {
        int i = paramArrayOfInt.length;
        String[] arrayOfString = new String[i];
        int j = 0;
        if (j < i)
        {
            switch (paramArrayOfInt[j])
            {
            default:
                throw new IllegalArgumentException("Unknown tech type " + paramArrayOfInt[j]);
            case 3:
                arrayOfString[j] = IsoDep.class.getName();
            case 8:
            case 9:
            case 6:
            case 7:
            case 1:
            case 2:
            case 4:
            case 5:
            }
            while (true)
            {
                j++;
                break;
                arrayOfString[j] = MifareClassic.class.getName();
                continue;
                arrayOfString[j] = MifareUltralight.class.getName();
                continue;
                arrayOfString[j] = Ndef.class.getName();
                continue;
                arrayOfString[j] = NdefFormatable.class.getName();
                continue;
                arrayOfString[j] = NfcA.class.getName();
                continue;
                arrayOfString[j] = NfcB.class.getName();
                continue;
                arrayOfString[j] = NfcF.class.getName();
                continue;
                arrayOfString[j] = NfcV.class.getName();
            }
        }
        return arrayOfString;
    }

    static byte[] readBytesWithNull(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        byte[] arrayOfByte = null;
        if (i >= 0)
        {
            arrayOfByte = new byte[i];
            paramParcel.readByteArray(arrayOfByte);
        }
        return arrayOfByte;
    }

    static void writeBytesWithNull(Parcel paramParcel, byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null)
            paramParcel.writeInt(-1);
        while (true)
        {
            return;
            paramParcel.writeInt(paramArrayOfByte.length);
            paramParcel.writeByteArray(paramArrayOfByte);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int getConnectedTechnology()
    {
        return this.mConnectedTechnology;
    }

    public byte[] getId()
    {
        return this.mId;
    }

    public int getServiceHandle()
    {
        return this.mServiceHandle;
    }

    public INfcTag getTagService()
    {
        return this.mTagService;
    }

    public Bundle getTechExtras(int paramInt)
    {
        int i = -1;
        int j = 0;
        if (j < this.mTechList.length)
        {
            if (this.mTechList[j] == paramInt)
                i = j;
        }
        else
            if (i >= 0)
                break label42;
        label42: for (Bundle localBundle = null; ; localBundle = this.mTechExtras[i])
        {
            return localBundle;
            j++;
            break;
        }
    }

    public String[] getTechList()
    {
        return this.mTechStringList;
    }

    public boolean hasTech(int paramInt)
    {
        int[] arrayOfInt = this.mTechList;
        int i = arrayOfInt.length;
        int j = 0;
        if (j < i)
            if (arrayOfInt[j] != paramInt);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public Tag rediscover()
        throws IOException
    {
        if (getConnectedTechnology() != -1)
            throw new IllegalStateException("Close connection to the technology first!");
        if (this.mTagService == null)
            throw new IOException("Mock tags don't support this operation.");
        try
        {
            Tag localTag = this.mTagService.rediscover(getServiceHandle());
            if (localTag != null)
                return localTag;
            throw new IOException("Failed to rediscover tag");
        }
        catch (RemoteException localRemoteException)
        {
        }
        throw new IOException("NFC service dead");
    }

    /** @deprecated */
    public void setConnectedTechnology(int paramInt)
    {
        try
        {
            if (this.mConnectedTechnology == -1)
            {
                this.mConnectedTechnology = paramInt;
                return;
            }
            throw new IllegalStateException("Close other technology first!");
        }
        finally
        {
        }
    }

    public void setTechnologyDisconnected()
    {
        this.mConnectedTechnology = -1;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("TAG: Tech [");
        String[] arrayOfString = getTechList();
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
        {
            localStringBuilder.append(arrayOfString[j]);
            if (j < i - 1)
                localStringBuilder.append(", ");
        }
        localStringBuilder.append("]");
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.mTagService == null);
        for (int i = 1; ; i = 0)
        {
            writeBytesWithNull(paramParcel, this.mId);
            paramParcel.writeInt(this.mTechList.length);
            paramParcel.writeIntArray(this.mTechList);
            paramParcel.writeTypedArray(this.mTechExtras, 0);
            paramParcel.writeInt(this.mServiceHandle);
            paramParcel.writeInt(i);
            if (i == 0)
                paramParcel.writeStrongBinder(this.mTagService.asBinder());
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.Tag
 * JD-Core Version:        0.6.2
 */