package android.nfc;

import android.content.Context;

public final class NfcManager
{
    private final NfcAdapter mAdapter;

    public NfcManager(Context paramContext)
    {
        Context localContext = paramContext.getApplicationContext();
        if (localContext == null)
            throw new IllegalArgumentException("context not associated with any application (using a mock context?)");
        try
        {
            NfcAdapter localNfcAdapter2 = NfcAdapter.getNfcAdapter(localContext);
            localNfcAdapter1 = localNfcAdapter2;
            this.mAdapter = localNfcAdapter1;
            return;
        }
        catch (UnsupportedOperationException localUnsupportedOperationException)
        {
            while (true)
                NfcAdapter localNfcAdapter1 = null;
        }
    }

    public NfcAdapter getDefaultAdapter()
    {
        return this.mAdapter;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.NfcManager
 * JD-Core Version:        0.6.2
 */