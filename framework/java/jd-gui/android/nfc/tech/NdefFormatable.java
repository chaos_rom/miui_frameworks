package android.nfc.tech;

import android.nfc.FormatException;
import android.nfc.INfcTag;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class NdefFormatable extends BasicTagTechnology
{
    private static final String TAG = "NFC";

    public NdefFormatable(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 7);
    }

    public static NdefFormatable get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(7));
        while (true)
        {
            return localObject;
            try
            {
                NdefFormatable localNdefFormatable = new NdefFormatable(paramTag);
                localObject = localNdefFormatable;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public void format(NdefMessage paramNdefMessage)
        throws IOException, FormatException
    {
        format(paramNdefMessage, false);
    }

    void format(NdefMessage paramNdefMessage, boolean paramBoolean)
        throws IOException, FormatException
    {
        checkConnected();
        int i;
        INfcTag localINfcTag;
        do
        {
            try
            {
                i = this.mTag.getServiceHandle();
                localINfcTag = this.mTag.getTagService();
                switch (localINfcTag.formatNdef(i, MifareClassic.KEY_DEFAULT))
                {
                default:
                    throw new IOException();
                case -1:
                case -8:
                case 0:
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                return;
            }
            throw new IOException();
            throw new FormatException();
            if (!localINfcTag.isNdef(i))
                throw new IOException();
            if (paramNdefMessage != null)
                switch (localINfcTag.ndefWrite(i, paramNdefMessage))
                {
                default:
                    throw new IOException();
                case -1:
                    throw new IOException();
                case -8:
                    throw new FormatException();
                case 0:
                }
        }
        while (!paramBoolean);
        switch (localINfcTag.ndefMakeReadOnly(i))
        {
        case 0:
        default:
            throw new IOException();
        case -1:
            throw new IOException();
        case -8:
        }
        throw new IOException();
    }

    public void formatReadOnly(NdefMessage paramNdefMessage)
        throws IOException, FormatException
    {
        format(paramNdefMessage, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.NdefFormatable
 * JD-Core Version:        0.6.2
 */