package android.nfc.tech;

import android.nfc.FormatException;
import android.nfc.INfcTag;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class Ndef extends BasicTagTechnology
{
    public static final String EXTRA_NDEF_CARDSTATE = "ndefcardstate";
    public static final String EXTRA_NDEF_MAXLENGTH = "ndefmaxlength";
    public static final String EXTRA_NDEF_MSG = "ndefmsg";
    public static final String EXTRA_NDEF_TYPE = "ndeftype";
    public static final String ICODE_SLI = "com.nxp.ndef.icodesli";
    public static final String MIFARE_CLASSIC = "com.nxp.ndef.mifareclassic";
    public static final int NDEF_MODE_READ_ONLY = 1;
    public static final int NDEF_MODE_READ_WRITE = 2;
    public static final int NDEF_MODE_UNKNOWN = 3;
    public static final String NFC_FORUM_TYPE_1 = "org.nfcforum.ndef.type1";
    public static final String NFC_FORUM_TYPE_2 = "org.nfcforum.ndef.type2";
    public static final String NFC_FORUM_TYPE_3 = "org.nfcforum.ndef.type3";
    public static final String NFC_FORUM_TYPE_4 = "org.nfcforum.ndef.type4";
    private static final String TAG = "NFC";
    public static final int TYPE_1 = 1;
    public static final int TYPE_2 = 2;
    public static final int TYPE_3 = 3;
    public static final int TYPE_4 = 4;
    public static final int TYPE_ICODE_SLI = 102;
    public static final int TYPE_MIFARE_CLASSIC = 101;
    public static final int TYPE_OTHER = -1;
    public static final String UNKNOWN = "android.ndef.unknown";
    private final int mCardState;
    private final int mMaxNdefSize;
    private final NdefMessage mNdefMsg;
    private final int mNdefType;

    public Ndef(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 6);
        Bundle localBundle = paramTag.getTechExtras(6);
        if (localBundle != null)
        {
            this.mMaxNdefSize = localBundle.getInt("ndefmaxlength");
            this.mCardState = localBundle.getInt("ndefcardstate");
            this.mNdefMsg = ((NdefMessage)localBundle.getParcelable("ndefmsg"));
            this.mNdefType = localBundle.getInt("ndeftype");
            return;
        }
        throw new NullPointerException("NDEF tech extras are null.");
    }

    public static Ndef get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(6));
        while (true)
        {
            return localObject;
            try
            {
                Ndef localNdef = new Ndef(paramTag);
                localObject = localNdef;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public boolean canMakeReadOnly()
    {
        boolean bool1 = false;
        INfcTag localINfcTag = this.mTag.getTagService();
        if (localINfcTag == null);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = localINfcTag.canMakeReadOnly(this.mNdefType);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
            }
        }
    }

    public NdefMessage getCachedNdefMessage()
    {
        return this.mNdefMsg;
    }

    public int getMaxSize()
    {
        return this.mMaxNdefSize;
    }

    public NdefMessage getNdefMessage()
        throws IOException, FormatException
    {
        checkConnected();
        INfcTag localINfcTag;
        NdefMessage localNdefMessage;
        try
        {
            localINfcTag = this.mTag.getTagService();
            if (localINfcTag == null)
                throw new IOException("Mock tags don't support this operation.");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
            localNdefMessage = null;
        }
        while (true)
        {
            return localNdefMessage;
            int i = this.mTag.getServiceHandle();
            if (localINfcTag.isNdef(i))
            {
                localNdefMessage = localINfcTag.ndefRead(i);
                if ((localNdefMessage == null) && (!localINfcTag.isPresent(i)))
                    throw new TagLostException();
            }
            else
            {
                localNdefMessage = null;
            }
        }
    }

    public String getType()
    {
        String str;
        switch (this.mNdefType)
        {
        default:
            str = "android.ndef.unknown";
        case 1:
        case 2:
        case 3:
        case 4:
        case 101:
        case 102:
        }
        while (true)
        {
            return str;
            str = "org.nfcforum.ndef.type1";
            continue;
            str = "org.nfcforum.ndef.type2";
            continue;
            str = "org.nfcforum.ndef.type3";
            continue;
            str = "org.nfcforum.ndef.type4";
            continue;
            str = "com.nxp.ndef.mifareclassic";
            continue;
            str = "com.nxp.ndef.icodesli";
        }
    }

    public boolean isWritable()
    {
        if (this.mCardState == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean makeReadOnly()
        throws IOException
    {
        boolean bool = false;
        checkConnected();
        try
        {
            INfcTag localINfcTag = this.mTag.getTagService();
            if (localINfcTag != null)
                if (localINfcTag.isNdef(this.mTag.getServiceHandle()))
                    switch (localINfcTag.ndefMakeReadOnly(this.mTag.getServiceHandle()))
                    {
                    default:
                        throw new IOException();
                    case 0:
                    case -1:
                    case -8:
                    }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
            break label132;
            bool = true;
            break label132;
            throw new IOException();
            throw new IOException("Tag is not ndef");
        }
        label132: return bool;
    }

    public void writeNdefMessage(NdefMessage paramNdefMessage)
        throws IOException, FormatException
    {
        checkConnected();
        INfcTag localINfcTag;
        try
        {
            localINfcTag = this.mTag.getTagService();
            if (localINfcTag == null)
                throw new IOException("Mock tags don't support this operation.");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
            return;
        }
        int i = this.mTag.getServiceHandle();
        if (localINfcTag.isNdef(i))
        {
            switch (localINfcTag.ndefWrite(i, paramNdefMessage))
            {
            case 0:
            default:
                throw new IOException();
            case -1:
                throw new IOException();
            case -8:
            }
            throw new FormatException();
        }
        throw new IOException("Tag is not ndef");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.Ndef
 * JD-Core Version:        0.6.2
 */