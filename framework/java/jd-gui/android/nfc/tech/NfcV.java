package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import java.io.IOException;

public final class NfcV extends BasicTagTechnology
{
    public static final String EXTRA_DSFID = "dsfid";
    public static final String EXTRA_RESP_FLAGS = "respflags";
    private byte mDsfId;
    private byte mRespFlags;

    public NfcV(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 5);
        Bundle localBundle = paramTag.getTechExtras(5);
        this.mRespFlags = localBundle.getByte("respflags");
        this.mDsfId = localBundle.getByte("dsfid");
    }

    public static NfcV get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(5));
        while (true)
        {
            return localObject;
            try
            {
                NfcV localNfcV = new NfcV(paramTag);
                localObject = localNfcV;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public byte getDsfId()
    {
        return this.mDsfId;
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public byte getResponseFlags()
    {
        return this.mRespFlags;
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.NfcV
 * JD-Core Version:        0.6.2
 */