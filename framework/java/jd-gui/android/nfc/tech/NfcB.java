package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import java.io.IOException;

public final class NfcB extends BasicTagTechnology
{
    public static final String EXTRA_APPDATA = "appdata";
    public static final String EXTRA_PROTINFO = "protinfo";
    private byte[] mAppData;
    private byte[] mProtInfo;

    public NfcB(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 2);
        Bundle localBundle = paramTag.getTechExtras(2);
        this.mAppData = localBundle.getByteArray("appdata");
        this.mProtInfo = localBundle.getByteArray("protinfo");
    }

    public static NfcB get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(2));
        while (true)
        {
            return localObject;
            try
            {
                NfcB localNfcB = new NfcB(paramTag);
                localObject = localNfcB;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public byte[] getApplicationData()
    {
        return this.mAppData;
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public byte[] getProtocolInfo()
    {
        return this.mProtInfo;
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.NfcB
 * JD-Core Version:        0.6.2
 */