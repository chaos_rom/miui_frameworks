package android.nfc.tech;

import android.nfc.INfcTag;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class MifareClassic extends BasicTagTechnology
{
    public static final int BLOCK_SIZE = 16;
    public static final byte[] KEY_DEFAULT;
    public static final byte[] KEY_MIFARE_APPLICATION_DIRECTORY;
    public static final byte[] KEY_NFC_FORUM = arrayOfByte3;
    private static final int MAX_BLOCK_COUNT = 256;
    private static final int MAX_SECTOR_COUNT = 40;
    public static final int SIZE_1K = 1024;
    public static final int SIZE_2K = 2048;
    public static final int SIZE_4K = 4096;
    public static final int SIZE_MINI = 320;
    private static final String TAG = "NFC";
    public static final int TYPE_CLASSIC = 0;
    public static final int TYPE_PLUS = 1;
    public static final int TYPE_PRO = 2;
    public static final int TYPE_UNKNOWN = -1;
    private boolean mIsEmulated;
    private int mSize;
    private int mType;

    static
    {
        byte[] arrayOfByte1 = new byte[6];
        arrayOfByte1[0] = -1;
        arrayOfByte1[1] = -1;
        arrayOfByte1[2] = -1;
        arrayOfByte1[3] = -1;
        arrayOfByte1[4] = -1;
        arrayOfByte1[5] = -1;
        KEY_DEFAULT = arrayOfByte1;
        byte[] arrayOfByte2 = new byte[6];
        arrayOfByte2[0] = -96;
        arrayOfByte2[1] = -95;
        arrayOfByte2[2] = -94;
        arrayOfByte2[3] = -93;
        arrayOfByte2[4] = -92;
        arrayOfByte2[5] = -91;
        KEY_MIFARE_APPLICATION_DIRECTORY = arrayOfByte2;
        byte[] arrayOfByte3 = new byte[6];
        arrayOfByte3[0] = -45;
        arrayOfByte3[1] = -9;
        arrayOfByte3[2] = -45;
        arrayOfByte3[3] = -9;
        arrayOfByte3[4] = -45;
        arrayOfByte3[5] = -9;
    }

    public MifareClassic(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 8);
        NfcA localNfcA = NfcA.get(paramTag);
        this.mIsEmulated = false;
        switch (localNfcA.getSak())
        {
        default:
            throw new RuntimeException("Tag incorrectly enumerated as MIFARE Classic, SAK = " + localNfcA.getSak());
        case 1:
        case 8:
            this.mType = 0;
            this.mSize = 1024;
        case 9:
        case 16:
        case 17:
        case 24:
        case 40:
        case 56:
        case 136:
        case 152:
        case 184:
        }
        while (true)
        {
            return;
            this.mType = 0;
            this.mSize = 320;
            continue;
            this.mType = 1;
            this.mSize = 2048;
            continue;
            this.mType = 1;
            this.mSize = 4096;
            continue;
            this.mType = 0;
            this.mSize = 4096;
            continue;
            this.mType = 0;
            this.mSize = 1024;
            this.mIsEmulated = true;
            continue;
            this.mType = 0;
            this.mSize = 4096;
            this.mIsEmulated = true;
            continue;
            this.mType = 0;
            this.mSize = 1024;
            continue;
            this.mType = 2;
            this.mSize = 4096;
        }
    }

    private boolean authenticate(int paramInt, byte[] paramArrayOfByte, boolean paramBoolean)
        throws IOException
    {
        boolean bool = true;
        validateSector(paramInt);
        checkConnected();
        byte[] arrayOfByte1 = new byte[12];
        if (paramBoolean)
            arrayOfByte1[0] = 96;
        while (true)
        {
            arrayOfByte1[bool] = ((byte)sectorToBlock(paramInt));
            byte[] arrayOfByte2 = getTag().getId();
            System.arraycopy(arrayOfByte2, -4 + arrayOfByte2.length, arrayOfByte1, 2, 4);
            System.arraycopy(paramArrayOfByte, 0, arrayOfByte1, 6, 6);
            try
            {
                byte[] arrayOfByte3 = transceive(arrayOfByte1, false);
                if (arrayOfByte3 != null)
                {
                    return bool;
                    arrayOfByte1[0] = 97;
                }
            }
            catch (TagLostException localTagLostException)
            {
                throw localTagLostException;
            }
            catch (IOException localIOException)
            {
                while (true)
                    bool = false;
            }
        }
    }

    public static MifareClassic get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(8));
        while (true)
        {
            return localObject;
            try
            {
                MifareClassic localMifareClassic = new MifareClassic(paramTag);
                localObject = localMifareClassic;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    private static void validateBlock(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= 256))
            throw new IndexOutOfBoundsException("block out of bounds: " + paramInt);
    }

    private static void validateSector(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= 40))
            throw new IndexOutOfBoundsException("sector out of bounds: " + paramInt);
    }

    private static void validateValueOperand(int paramInt)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException("value operand negative");
    }

    public boolean authenticateSectorWithKeyA(int paramInt, byte[] paramArrayOfByte)
        throws IOException
    {
        return authenticate(paramInt, paramArrayOfByte, true);
    }

    public boolean authenticateSectorWithKeyB(int paramInt, byte[] paramArrayOfByte)
        throws IOException
    {
        return authenticate(paramInt, paramArrayOfByte, false);
    }

    public int blockToSector(int paramInt)
    {
        validateBlock(paramInt);
        if (paramInt < 128);
        for (int i = paramInt / 4; ; i = 32 + (paramInt - 128) / 16)
            return i;
    }

    public void decrement(int paramInt1, int paramInt2)
        throws IOException
    {
        validateBlock(paramInt1);
        validateValueOperand(paramInt2);
        checkConnected();
        ByteBuffer localByteBuffer = ByteBuffer.allocate(6);
        localByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        localByteBuffer.put((byte)-64);
        localByteBuffer.put((byte)paramInt1);
        localByteBuffer.putInt(paramInt2);
        transceive(localByteBuffer.array(), false);
    }

    public int getBlockCount()
    {
        return this.mSize / 16;
    }

    public int getBlockCountInSector(int paramInt)
    {
        validateSector(paramInt);
        if (paramInt < 32);
        for (int i = 4; ; i = 16)
            return i;
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public int getSectorCount()
    {
        int i;
        switch (this.mSize)
        {
        default:
            i = 0;
        case 1024:
        case 2048:
        case 4096:
        case 320:
        }
        while (true)
        {
            return i;
            i = 16;
            continue;
            i = 32;
            continue;
            i = 40;
            continue;
            i = 5;
        }
    }

    public int getSize()
    {
        return this.mSize;
    }

    public int getTimeout()
    {
        try
        {
            int j = this.mTag.getTagService().getTimeout(8);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                int i = 0;
            }
        }
    }

    public int getType()
    {
        return this.mType;
    }

    public void increment(int paramInt1, int paramInt2)
        throws IOException
    {
        validateBlock(paramInt1);
        validateValueOperand(paramInt2);
        checkConnected();
        ByteBuffer localByteBuffer = ByteBuffer.allocate(6);
        localByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        localByteBuffer.put((byte)-63);
        localByteBuffer.put((byte)paramInt1);
        localByteBuffer.putInt(paramInt2);
        transceive(localByteBuffer.array(), false);
    }

    public boolean isEmulated()
    {
        return this.mIsEmulated;
    }

    public byte[] readBlock(int paramInt)
        throws IOException
    {
        validateBlock(paramInt);
        checkConnected();
        byte[] arrayOfByte = new byte[2];
        arrayOfByte[0] = 48;
        arrayOfByte[1] = ((byte)paramInt);
        return transceive(arrayOfByte, false);
    }

    public void restore(int paramInt)
        throws IOException
    {
        validateBlock(paramInt);
        checkConnected();
        byte[] arrayOfByte = new byte[2];
        arrayOfByte[0] = -62;
        arrayOfByte[1] = ((byte)paramInt);
        transceive(arrayOfByte, false);
    }

    public int sectorToBlock(int paramInt)
    {
        if (paramInt < 32);
        for (int i = paramInt * 4; ; i = 128 + 16 * (paramInt - 32))
            return i;
    }

    public void setTimeout(int paramInt)
    {
        try
        {
            if (this.mTag.getTagService().setTimeout(8, paramInt) != 0)
                throw new IllegalArgumentException("The supplied timeout is not valid");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
        }
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }

    public void transfer(int paramInt)
        throws IOException
    {
        validateBlock(paramInt);
        checkConnected();
        byte[] arrayOfByte = new byte[2];
        arrayOfByte[0] = -80;
        arrayOfByte[1] = ((byte)paramInt);
        transceive(arrayOfByte, false);
    }

    public void writeBlock(int paramInt, byte[] paramArrayOfByte)
        throws IOException
    {
        validateBlock(paramInt);
        checkConnected();
        if (paramArrayOfByte.length != 16)
            throw new IllegalArgumentException("must write 16-bytes");
        byte[] arrayOfByte = new byte[2 + paramArrayOfByte.length];
        arrayOfByte[0] = -96;
        arrayOfByte[1] = ((byte)paramInt);
        System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 2, paramArrayOfByte.length);
        transceive(arrayOfByte, false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.MifareClassic
 * JD-Core Version:        0.6.2
 */