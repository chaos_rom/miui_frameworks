package android.nfc.tech;

import android.nfc.INfcTag;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class MifareUltralight extends BasicTagTechnology
{
    public static final String EXTRA_IS_UL_C = "isulc";
    private static final int MAX_PAGE_COUNT = 256;
    private static final int NXP_MANUFACTURER_ID = 4;
    public static final int PAGE_SIZE = 4;
    private static final String TAG = "NFC";
    public static final int TYPE_ULTRALIGHT = 1;
    public static final int TYPE_ULTRALIGHT_C = 2;
    public static final int TYPE_UNKNOWN = -1;
    private int mType;

    public MifareUltralight(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 9);
        NfcA localNfcA = NfcA.get(paramTag);
        this.mType = -1;
        if ((localNfcA.getSak() == 0) && (paramTag.getId()[0] == 4))
            if (!paramTag.getTechExtras(9).getBoolean("isulc"))
                break label55;
        label55: for (this.mType = 2; ; this.mType = 1)
            return;
    }

    public static MifareUltralight get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(9));
        while (true)
        {
            return localObject;
            try
            {
                MifareUltralight localMifareUltralight = new MifareUltralight(paramTag);
                localObject = localMifareUltralight;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    private static void validatePageIndex(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= 256))
            throw new IndexOutOfBoundsException("page out of bounds: " + paramInt);
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public int getTimeout()
    {
        try
        {
            int j = this.mTag.getTagService().getTimeout(9);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                int i = 0;
            }
        }
    }

    public int getType()
    {
        return this.mType;
    }

    public byte[] readPages(int paramInt)
        throws IOException
    {
        validatePageIndex(paramInt);
        checkConnected();
        byte[] arrayOfByte = new byte[2];
        arrayOfByte[0] = 48;
        arrayOfByte[1] = ((byte)paramInt);
        return transceive(arrayOfByte, false);
    }

    public void setTimeout(int paramInt)
    {
        try
        {
            if (this.mTag.getTagService().setTimeout(9, paramInt) != 0)
                throw new IllegalArgumentException("The supplied timeout is not valid");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
        }
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }

    public void writePage(int paramInt, byte[] paramArrayOfByte)
        throws IOException
    {
        validatePageIndex(paramInt);
        checkConnected();
        byte[] arrayOfByte = new byte[2 + paramArrayOfByte.length];
        arrayOfByte[0] = -94;
        arrayOfByte[1] = ((byte)paramInt);
        System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 2, paramArrayOfByte.length);
        transceive(arrayOfByte, false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.MifareUltralight
 * JD-Core Version:        0.6.2
 */