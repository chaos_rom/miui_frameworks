package android.nfc.tech;

import android.nfc.INfcTag;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class NfcA extends BasicTagTechnology
{
    public static final String EXTRA_ATQA = "atqa";
    public static final String EXTRA_SAK = "sak";
    private static final String TAG = "NFC";
    private byte[] mAtqa;
    private short mSak;

    public NfcA(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 1);
        Bundle localBundle = paramTag.getTechExtras(1);
        this.mSak = localBundle.getShort("sak");
        this.mAtqa = localBundle.getByteArray("atqa");
    }

    public static NfcA get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(1));
        while (true)
        {
            return localObject;
            try
            {
                NfcA localNfcA = new NfcA(paramTag);
                localObject = localNfcA;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public byte[] getAtqa()
    {
        return this.mAtqa;
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public short getSak()
    {
        return this.mSak;
    }

    public int getTimeout()
    {
        try
        {
            int j = this.mTag.getTagService().getTimeout(1);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                int i = 0;
            }
        }
    }

    public void setTimeout(int paramInt)
    {
        try
        {
            if (this.mTag.getTagService().setTimeout(1, paramInt) != 0)
                throw new IllegalArgumentException("The supplied timeout is not valid");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
        }
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.NfcA
 * JD-Core Version:        0.6.2
 */