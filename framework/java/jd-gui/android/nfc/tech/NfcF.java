package android.nfc.tech;

import android.nfc.INfcTag;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class NfcF extends BasicTagTechnology
{
    public static final String EXTRA_PMM = "pmm";
    public static final String EXTRA_SC = "systemcode";
    private static final String TAG = "NFC";
    private byte[] mManufacturer = null;
    private byte[] mSystemCode = null;

    public NfcF(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 4);
        Bundle localBundle = paramTag.getTechExtras(4);
        if (localBundle != null)
        {
            this.mSystemCode = localBundle.getByteArray("systemcode");
            this.mManufacturer = localBundle.getByteArray("pmm");
        }
    }

    public static NfcF get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(4));
        while (true)
        {
            return localObject;
            try
            {
                NfcF localNfcF = new NfcF(paramTag);
                localObject = localNfcF;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public byte[] getManufacturer()
    {
        return this.mManufacturer;
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public byte[] getSystemCode()
    {
        return this.mSystemCode;
    }

    public int getTimeout()
    {
        try
        {
            int j = this.mTag.getTagService().getTimeout(4);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                int i = 0;
            }
        }
    }

    public void setTimeout(int paramInt)
    {
        try
        {
            if (this.mTag.getTagService().setTimeout(4, paramInt) != 0)
                throw new IllegalArgumentException("The supplied timeout is not valid");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
        }
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.NfcF
 * JD-Core Version:        0.6.2
 */