package android.nfc.tech;

import android.nfc.INfcTag;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class IsoDep extends BasicTagTechnology
{
    public static final String EXTRA_HIST_BYTES = "histbytes";
    public static final String EXTRA_HI_LAYER_RESP = "hiresp";
    private static final String TAG = "NFC";
    private byte[] mHiLayerResponse = null;
    private byte[] mHistBytes = null;

    public IsoDep(Tag paramTag)
        throws RemoteException
    {
        super(paramTag, 3);
        Bundle localBundle = paramTag.getTechExtras(3);
        if (localBundle != null)
        {
            this.mHiLayerResponse = localBundle.getByteArray("hiresp");
            this.mHistBytes = localBundle.getByteArray("histbytes");
        }
    }

    public static IsoDep get(Tag paramTag)
    {
        Object localObject = null;
        if (!paramTag.hasTech(3));
        while (true)
        {
            return localObject;
            try
            {
                IsoDep localIsoDep = new IsoDep(paramTag);
                localObject = localIsoDep;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public byte[] getHiLayerResponse()
    {
        return this.mHiLayerResponse;
    }

    public byte[] getHistoricalBytes()
    {
        return this.mHistBytes;
    }

    public int getMaxTransceiveLength()
    {
        return getMaxTransceiveLengthInternal();
    }

    public int getTimeout()
    {
        try
        {
            int j = this.mTag.getTagService().getTimeout(3);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                int i = 0;
            }
        }
    }

    public boolean isExtendedLengthApduSupported()
    {
        try
        {
            boolean bool2 = this.mTag.getTagService().getExtendedLengthApdusSupported();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("NFC", "NFC service dead", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void setTimeout(int paramInt)
    {
        try
        {
            if (this.mTag.getTagService().setTimeout(3, paramInt) != 0)
                throw new IllegalArgumentException("The supplied timeout is not valid");
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("NFC", "NFC service dead", localRemoteException);
        }
    }

    public byte[] transceive(byte[] paramArrayOfByte)
        throws IOException
    {
        return transceive(paramArrayOfByte, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.tech.IsoDep
 * JD-Core Version:        0.6.2
 */