package android.nfc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INfcTag extends IInterface
{
    public abstract boolean canMakeReadOnly(int paramInt)
        throws RemoteException;

    public abstract int close(int paramInt)
        throws RemoteException;

    public abstract int connect(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract int formatNdef(int paramInt, byte[] paramArrayOfByte)
        throws RemoteException;

    public abstract boolean getExtendedLengthApdusSupported()
        throws RemoteException;

    public abstract int getMaxTransceiveLength(int paramInt)
        throws RemoteException;

    public abstract int[] getTechList(int paramInt)
        throws RemoteException;

    public abstract int getTimeout(int paramInt)
        throws RemoteException;

    public abstract boolean isNdef(int paramInt)
        throws RemoteException;

    public abstract boolean isPresent(int paramInt)
        throws RemoteException;

    public abstract boolean ndefIsWritable(int paramInt)
        throws RemoteException;

    public abstract int ndefMakeReadOnly(int paramInt)
        throws RemoteException;

    public abstract NdefMessage ndefRead(int paramInt)
        throws RemoteException;

    public abstract int ndefWrite(int paramInt, NdefMessage paramNdefMessage)
        throws RemoteException;

    public abstract int reconnect(int paramInt)
        throws RemoteException;

    public abstract Tag rediscover(int paramInt)
        throws RemoteException;

    public abstract void resetTimeouts()
        throws RemoteException;

    public abstract int setTimeout(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract TransceiveResult transceive(int paramInt, byte[] paramArrayOfByte, boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INfcTag
    {
        private static final String DESCRIPTOR = "android.nfc.INfcTag";
        static final int TRANSACTION_canMakeReadOnly = 17;
        static final int TRANSACTION_close = 1;
        static final int TRANSACTION_connect = 2;
        static final int TRANSACTION_formatNdef = 12;
        static final int TRANSACTION_getExtendedLengthApdusSupported = 19;
        static final int TRANSACTION_getMaxTransceiveLength = 18;
        static final int TRANSACTION_getTechList = 4;
        static final int TRANSACTION_getTimeout = 15;
        static final int TRANSACTION_isNdef = 5;
        static final int TRANSACTION_isPresent = 6;
        static final int TRANSACTION_ndefIsWritable = 11;
        static final int TRANSACTION_ndefMakeReadOnly = 10;
        static final int TRANSACTION_ndefRead = 8;
        static final int TRANSACTION_ndefWrite = 9;
        static final int TRANSACTION_reconnect = 3;
        static final int TRANSACTION_rediscover = 13;
        static final int TRANSACTION_resetTimeouts = 16;
        static final int TRANSACTION_setTimeout = 14;
        static final int TRANSACTION_transceive = 7;

        public Stub()
        {
            attachInterface(this, "android.nfc.INfcTag");
        }

        public static INfcTag asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.nfc.INfcTag");
                if ((localIInterface != null) && ((localIInterface instanceof INfcTag)))
                    localObject = (INfcTag)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.nfc.INfcTag");
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                int i10 = close(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i10);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                int i9 = connect(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i9);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                int i8 = reconnect(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i8);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                int[] arrayOfInt = getTechList(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeIntArray(arrayOfInt);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                boolean bool5 = isNdef(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool5)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                boolean bool4 = isPresent(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool4)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                int i5 = paramParcel1.readInt();
                byte[] arrayOfByte = paramParcel1.createByteArray();
                if (paramParcel1.readInt() != 0);
                int i7;
                for (int i6 = j; ; i7 = 0)
                {
                    TransceiveResult localTransceiveResult = transceive(i5, arrayOfByte, i6);
                    paramParcel2.writeNoException();
                    if (localTransceiveResult == null)
                        break label468;
                    paramParcel2.writeInt(j);
                    localTransceiveResult.writeToParcel(paramParcel2, j);
                    break;
                }
                label468: paramParcel2.writeInt(0);
                continue;
                paramParcel1.enforceInterface("android.nfc.INfcTag");
                NdefMessage localNdefMessage2 = ndefRead(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localNdefMessage2 != null)
                {
                    paramParcel2.writeInt(j);
                    localNdefMessage2.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INfcTag");
                    int i3 = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    for (NdefMessage localNdefMessage1 = (NdefMessage)NdefMessage.CREATOR.createFromParcel(paramParcel1); ; localNdefMessage1 = null)
                    {
                        int i4 = ndefWrite(i3, localNdefMessage1);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i4);
                        break;
                    }
                    paramParcel1.enforceInterface("android.nfc.INfcTag");
                    int i2 = ndefMakeReadOnly(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i2);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INfcTag");
                    boolean bool3 = ndefIsWritable(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool3)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INfcTag");
                    int i1 = formatNdef(paramParcel1.readInt(), paramParcel1.createByteArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i1);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INfcTag");
                    Tag localTag = rediscover(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localTag != null)
                    {
                        paramParcel2.writeInt(j);
                        localTag.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcTag");
                        int n = setTimeout(paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(n);
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcTag");
                        int m = getTimeout(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(m);
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcTag");
                        resetTimeouts();
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcTag");
                        boolean bool2 = canMakeReadOnly(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (bool2)
                            i = j;
                        paramParcel2.writeInt(i);
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcTag");
                        int k = getMaxTransceiveLength(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(k);
                        continue;
                        paramParcel1.enforceInterface("android.nfc.INfcTag");
                        boolean bool1 = getExtendedLengthApdusSupported();
                        paramParcel2.writeNoException();
                        if (bool1)
                            i = j;
                        paramParcel2.writeInt(i);
                    }
                }
            }
        }

        private static class Proxy
            implements INfcTag
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean canMakeReadOnly(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int close(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int connect(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int formatNdef(int paramInt, byte[] paramArrayOfByte)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getExtendedLengthApdusSupported()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.nfc.INfcTag";
            }

            public int getMaxTransceiveLength(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getTechList(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int[] arrayOfInt = localParcel2.createIntArray();
                    return arrayOfInt;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getTimeout(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isNdef(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isPresent(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean ndefIsWritable(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int ndefMakeReadOnly(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NdefMessage ndefRead(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNdefMessage = (NdefMessage)NdefMessage.CREATOR.createFromParcel(localParcel2);
                        return localNdefMessage;
                    }
                    NdefMessage localNdefMessage = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int ndefWrite(int paramInt, NdefMessage paramNdefMessage)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    if (paramNdefMessage != null)
                    {
                        localParcel1.writeInt(1);
                        paramNdefMessage.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int reconnect(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Tag rediscover(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localTag = (Tag)Tag.CREATOR.createFromParcel(localParcel2);
                        return localTag;
                    }
                    Tag localTag = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void resetTimeouts()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int setTimeout(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public TransceiveResult transceive(int paramInt, byte[] paramArrayOfByte, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INfcTag");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeByteArray(paramArrayOfByte);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localTransceiveResult = (TransceiveResult)TransceiveResult.CREATOR.createFromParcel(localParcel2);
                        return localTransceiveResult;
                    }
                    TransceiveResult localTransceiveResult = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.INfcTag
 * JD-Core Version:        0.6.2
 */