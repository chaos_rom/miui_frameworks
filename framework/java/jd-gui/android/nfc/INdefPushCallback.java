package android.nfc;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INdefPushCallback extends IInterface
{
    public abstract NdefMessage createMessage()
        throws RemoteException;

    public abstract Uri[] getUris()
        throws RemoteException;

    public abstract void onNdefPushComplete()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INdefPushCallback
    {
        private static final String DESCRIPTOR = "android.nfc.INdefPushCallback";
        static final int TRANSACTION_createMessage = 1;
        static final int TRANSACTION_getUris = 2;
        static final int TRANSACTION_onNdefPushComplete = 3;

        public Stub()
        {
            attachInterface(this, "android.nfc.INdefPushCallback");
        }

        public static INdefPushCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.nfc.INdefPushCallback");
                if ((localIInterface != null) && ((localIInterface instanceof INdefPushCallback)))
                    localObject = (INdefPushCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.nfc.INdefPushCallback");
                continue;
                paramParcel1.enforceInterface("android.nfc.INdefPushCallback");
                NdefMessage localNdefMessage = createMessage();
                paramParcel2.writeNoException();
                if (localNdefMessage != null)
                {
                    paramParcel2.writeInt(i);
                    localNdefMessage.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INdefPushCallback");
                    Uri[] arrayOfUri = getUris();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfUri, i);
                    continue;
                    paramParcel1.enforceInterface("android.nfc.INdefPushCallback");
                    onNdefPushComplete();
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements INdefPushCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public NdefMessage createMessage()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INdefPushCallback");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNdefMessage = (NdefMessage)NdefMessage.CREATOR.createFromParcel(localParcel2);
                        return localNdefMessage;
                    }
                    NdefMessage localNdefMessage = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.nfc.INdefPushCallback";
            }

            public Uri[] getUris()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INdefPushCallback");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    Uri[] arrayOfUri = (Uri[])localParcel2.createTypedArray(Uri.CREATOR);
                    return arrayOfUri;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onNdefPushComplete()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.nfc.INdefPushCallback");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.INdefPushCallback
 * JD-Core Version:        0.6.2
 */