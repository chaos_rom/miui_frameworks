package android.nfc;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.nio.ByteBuffer;
import java.util.Arrays;

public final class NdefMessage
    implements Parcelable
{
    public static final Parcelable.Creator<NdefMessage> CREATOR = new Parcelable.Creator()
    {
        public NdefMessage createFromParcel(Parcel paramAnonymousParcel)
        {
            NdefRecord[] arrayOfNdefRecord = new NdefRecord[paramAnonymousParcel.readInt()];
            paramAnonymousParcel.readTypedArray(arrayOfNdefRecord, NdefRecord.CREATOR);
            return new NdefMessage(arrayOfNdefRecord);
        }

        public NdefMessage[] newArray(int paramAnonymousInt)
        {
            return new NdefMessage[paramAnonymousInt];
        }
    };
    private final NdefRecord[] mRecords;

    public NdefMessage(NdefRecord paramNdefRecord, NdefRecord[] paramArrayOfNdefRecord)
    {
        if (paramNdefRecord == null)
            throw new NullPointerException("record cannot be null");
        int i = paramArrayOfNdefRecord.length;
        for (int j = 0; j < i; j++)
            if (paramArrayOfNdefRecord[j] == null)
                throw new NullPointerException("record cannot be null");
        this.mRecords = new NdefRecord[1 + paramArrayOfNdefRecord.length];
        this.mRecords[0] = paramNdefRecord;
        System.arraycopy(paramArrayOfNdefRecord, 0, this.mRecords, 1, paramArrayOfNdefRecord.length);
    }

    public NdefMessage(byte[] paramArrayOfByte)
        throws FormatException
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("data is null");
        ByteBuffer localByteBuffer = ByteBuffer.wrap(paramArrayOfByte);
        this.mRecords = NdefRecord.parse(localByteBuffer, false);
        if (localByteBuffer.remaining() > 0)
            throw new FormatException("trailing data");
    }

    public NdefMessage(NdefRecord[] paramArrayOfNdefRecord)
    {
        if (paramArrayOfNdefRecord.length < 1)
            throw new IllegalArgumentException("must have at least one record");
        int i = paramArrayOfNdefRecord.length;
        for (int j = 0; j < i; j++)
            if (paramArrayOfNdefRecord[j] == null)
                throw new NullPointerException("records cannot contain null");
        this.mRecords = paramArrayOfNdefRecord;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (this == paramObject);
        NdefMessage localNdefMessage;
        for (bool = true; ; bool = Arrays.equals(this.mRecords, localNdefMessage.mRecords))
        {
            do
                return bool;
            while ((paramObject == null) || (getClass() != paramObject.getClass()));
            localNdefMessage = (NdefMessage)paramObject;
        }
    }

    public int getByteArrayLength()
    {
        int i = 0;
        NdefRecord[] arrayOfNdefRecord = this.mRecords;
        int j = arrayOfNdefRecord.length;
        for (int k = 0; k < j; k++)
            i += arrayOfNdefRecord[k].getByteLength();
        return i;
    }

    public NdefRecord[] getRecords()
    {
        return this.mRecords;
    }

    public int hashCode()
    {
        return Arrays.hashCode(this.mRecords);
    }

    public byte[] toByteArray()
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(getByteArrayLength());
        int i = 0;
        if (i < this.mRecords.length)
        {
            boolean bool1;
            if (i == 0)
            {
                bool1 = true;
                label25: if (i != -1 + this.mRecords.length)
                    break label64;
            }
            label64: for (boolean bool2 = true; ; bool2 = false)
            {
                this.mRecords[i].writeToByteBuffer(localByteBuffer, bool1, bool2);
                i++;
                break;
                bool1 = false;
                break label25;
            }
        }
        return localByteBuffer.array();
    }

    public String toString()
    {
        return "NdefMessage " + Arrays.toString(this.mRecords);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mRecords.length);
        paramParcel.writeTypedArray(this.mRecords, paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.NdefMessage
 * JD-Core Version:        0.6.2
 */