package android.nfc;

import java.io.IOException;

public class TagLostException extends IOException
{
    public TagLostException()
    {
    }

    public TagLostException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.TagLostException
 * JD-Core Version:        0.6.2
 */