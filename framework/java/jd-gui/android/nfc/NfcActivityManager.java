package android.nfc;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class NfcActivityManager extends INdefPushCallback.Stub
    implements Application.ActivityLifecycleCallbacks
{
    static final Boolean DBG = Boolean.valueOf(false);
    static final String TAG = "NFC";
    final List<NfcActivityState> mActivities;
    final NfcAdapter mAdapter;
    final List<NfcApplicationState> mApps;
    final NfcEvent mDefaultEvent;

    public NfcActivityManager(NfcAdapter paramNfcAdapter)
    {
        this.mAdapter = paramNfcAdapter;
        this.mActivities = new LinkedList();
        this.mApps = new ArrayList(1);
        this.mDefaultEvent = new NfcEvent(this.mAdapter);
    }

    public NdefMessage createMessage()
    {
        NdefMessage localNdefMessage;
        try
        {
            NfcActivityState localNfcActivityState = findResumedActivityState();
            if (localNfcActivityState == null)
            {
                localNdefMessage = null;
            }
            else
            {
                NfcAdapter.CreateNdefMessageCallback localCreateNdefMessageCallback = localNfcActivityState.ndefMessageCallback;
                localNdefMessage = localNfcActivityState.ndefMessage;
                if (localCreateNdefMessageCallback != null)
                    localNdefMessage = localCreateNdefMessageCallback.createNdefMessage(this.mDefaultEvent);
            }
        }
        finally
        {
        }
        return localNdefMessage;
    }

    /** @deprecated */
    void destroyActivityState(Activity paramActivity)
    {
        try
        {
            NfcActivityState localNfcActivityState = findActivityState(paramActivity);
            if (localNfcActivityState != null)
            {
                localNfcActivityState.destroy();
                this.mActivities.remove(localNfcActivityState);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    NfcActivityState findActivityState(Activity paramActivity)
    {
        try
        {
            Iterator localIterator = this.mActivities.iterator();
            while (localIterator.hasNext())
            {
                localNfcActivityState = (NfcActivityState)localIterator.next();
                Activity localActivity = localNfcActivityState.activity;
                if (localActivity == paramActivity)
                    return localNfcActivityState;
            }
            NfcActivityState localNfcActivityState = null;
        }
        finally
        {
        }
    }

    NfcApplicationState findAppState(Application paramApplication)
    {
        Iterator localIterator = this.mApps.iterator();
        NfcApplicationState localNfcApplicationState;
        do
        {
            if (!localIterator.hasNext())
                break;
            localNfcApplicationState = (NfcApplicationState)localIterator.next();
        }
        while (localNfcApplicationState.app != paramApplication);
        while (true)
        {
            return localNfcApplicationState;
            localNfcApplicationState = null;
        }
    }

    /** @deprecated */
    NfcActivityState findResumedActivityState()
    {
        try
        {
            Iterator localIterator = this.mActivities.iterator();
            while (localIterator.hasNext())
            {
                localNfcActivityState = (NfcActivityState)localIterator.next();
                boolean bool = localNfcActivityState.resumed;
                if (bool)
                    return localNfcActivityState;
            }
            NfcActivityState localNfcActivityState = null;
        }
        finally
        {
        }
    }

    /** @deprecated */
    NfcActivityState getActivityState(Activity paramActivity)
    {
        try
        {
            NfcActivityState localNfcActivityState = findActivityState(paramActivity);
            if (localNfcActivityState == null)
            {
                localNfcActivityState = new NfcActivityState(paramActivity);
                this.mActivities.add(localNfcActivityState);
            }
            return localNfcActivityState;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public Uri[] getUris()
    {
        Uri[] arrayOfUri1;
        while (true)
        {
            int j;
            Uri localUri;
            try
            {
                NfcActivityState localNfcActivityState = findResumedActivityState();
                if (localNfcActivityState == null)
                {
                    arrayOfUri1 = null;
                }
                else
                {
                    arrayOfUri1 = localNfcActivityState.uris;
                    NfcAdapter.CreateBeamUrisCallback localCreateBeamUrisCallback = localNfcActivityState.uriCallback;
                    if (localCreateBeamUrisCallback != null)
                    {
                        arrayOfUri1 = localCreateBeamUrisCallback.createBeamUris(this.mDefaultEvent);
                        if (arrayOfUri1 != null)
                        {
                            Uri[] arrayOfUri2 = arrayOfUri1;
                            int i = arrayOfUri2.length;
                            j = 0;
                            if (j < i)
                            {
                                localUri = arrayOfUri2[j];
                                if (localUri == null)
                                {
                                    Log.e("NFC", "Uri not allowed to be null.");
                                    arrayOfUri1 = null;
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
            }
            String str = localUri.getScheme();
            if ((str == null) || ((!str.equalsIgnoreCase("file")) && (!str.equalsIgnoreCase("content"))))
            {
                Log.e("NFC", "Uri needs to have either scheme file or scheme content");
                arrayOfUri1 = null;
                break;
            }
            j++;
        }
        return arrayOfUri1;
    }

    public void onActivityCreated(Activity paramActivity, Bundle paramBundle)
    {
    }

    public void onActivityDestroyed(Activity paramActivity)
    {
        try
        {
            NfcActivityState localNfcActivityState = findActivityState(paramActivity);
            if (DBG.booleanValue())
                Log.d("NFC", "onDestroy() for " + paramActivity + " " + localNfcActivityState);
            if (localNfcActivityState != null)
                destroyActivityState(paramActivity);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void onActivityPaused(Activity paramActivity)
    {
        try
        {
            NfcActivityState localNfcActivityState = findActivityState(paramActivity);
            if (DBG.booleanValue())
                Log.d("NFC", "onPause() for " + paramActivity + " " + localNfcActivityState);
            if (localNfcActivityState == null)
                return;
            localNfcActivityState.resumed = false;
            requestNfcServiceCallback(false);
        }
        finally
        {
        }
    }

    public void onActivityResumed(Activity paramActivity)
    {
        try
        {
            NfcActivityState localNfcActivityState = findActivityState(paramActivity);
            if (DBG.booleanValue())
                Log.d("NFC", "onResume() for " + paramActivity + " " + localNfcActivityState);
            if (localNfcActivityState == null)
                return;
            localNfcActivityState.resumed = true;
            requestNfcServiceCallback(true);
        }
        finally
        {
        }
    }

    public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle)
    {
    }

    public void onActivityStarted(Activity paramActivity)
    {
    }

    public void onActivityStopped(Activity paramActivity)
    {
    }

    public void onNdefPushComplete()
    {
        try
        {
            NfcActivityState localNfcActivityState = findResumedActivityState();
            if (localNfcActivityState == null)
                return;
            NfcAdapter.OnNdefPushCompleteCallback localOnNdefPushCompleteCallback = localNfcActivityState.onNdefPushCompleteCallback;
            if (localOnNdefPushCompleteCallback != null)
                localOnNdefPushCompleteCallback.onNdefPushComplete(this.mDefaultEvent);
        }
        finally
        {
        }
    }

    void registerApplication(Application paramApplication)
    {
        NfcApplicationState localNfcApplicationState = findAppState(paramApplication);
        if (localNfcApplicationState == null)
        {
            localNfcApplicationState = new NfcApplicationState(paramApplication);
            this.mApps.add(localNfcApplicationState);
        }
        localNfcApplicationState.register();
    }

    void requestNfcServiceCallback(boolean paramBoolean)
    {
        try
        {
            INfcAdapter localINfcAdapter = NfcAdapter.sService;
            if (paramBoolean);
            for (NfcActivityManager localNfcActivityManager = this; ; localNfcActivityManager = null)
            {
                localINfcAdapter.setNdefPushCallback(localNfcActivityManager);
                return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                this.mAdapter.attemptDeadServiceRecovery(localRemoteException);
        }
    }

    public void setNdefPushContentUri(Activity paramActivity, Uri[] paramArrayOfUri)
    {
        try
        {
            NfcActivityState localNfcActivityState = getActivityState(paramActivity);
            localNfcActivityState.uris = paramArrayOfUri;
            boolean bool = localNfcActivityState.resumed;
            if (bool)
                requestNfcServiceCallback(true);
            return;
        }
        finally
        {
        }
    }

    public void setNdefPushContentUriCallback(Activity paramActivity, NfcAdapter.CreateBeamUrisCallback paramCreateBeamUrisCallback)
    {
        try
        {
            NfcActivityState localNfcActivityState = getActivityState(paramActivity);
            localNfcActivityState.uriCallback = paramCreateBeamUrisCallback;
            boolean bool = localNfcActivityState.resumed;
            if (bool)
                requestNfcServiceCallback(true);
            return;
        }
        finally
        {
        }
    }

    public void setNdefPushMessage(Activity paramActivity, NdefMessage paramNdefMessage)
    {
        try
        {
            NfcActivityState localNfcActivityState = getActivityState(paramActivity);
            localNfcActivityState.ndefMessage = paramNdefMessage;
            boolean bool = localNfcActivityState.resumed;
            if (bool)
                requestNfcServiceCallback(true);
            return;
        }
        finally
        {
        }
    }

    public void setNdefPushMessageCallback(Activity paramActivity, NfcAdapter.CreateNdefMessageCallback paramCreateNdefMessageCallback)
    {
        try
        {
            NfcActivityState localNfcActivityState = getActivityState(paramActivity);
            localNfcActivityState.ndefMessageCallback = paramCreateNdefMessageCallback;
            boolean bool = localNfcActivityState.resumed;
            if (bool)
                requestNfcServiceCallback(true);
            return;
        }
        finally
        {
        }
    }

    public void setOnNdefPushCompleteCallback(Activity paramActivity, NfcAdapter.OnNdefPushCompleteCallback paramOnNdefPushCompleteCallback)
    {
        try
        {
            NfcActivityState localNfcActivityState = getActivityState(paramActivity);
            localNfcActivityState.onNdefPushCompleteCallback = paramOnNdefPushCompleteCallback;
            boolean bool = localNfcActivityState.resumed;
            if (bool)
                requestNfcServiceCallback(true);
            return;
        }
        finally
        {
        }
    }

    void unregisterApplication(Application paramApplication)
    {
        NfcApplicationState localNfcApplicationState = findAppState(paramApplication);
        if (localNfcApplicationState == null)
            Log.e("NFC", "app was not registered " + paramApplication);
        while (true)
        {
            return;
            localNfcApplicationState.unregister();
        }
    }

    class NfcActivityState
    {
        Activity activity;
        NdefMessage ndefMessage = null;
        NfcAdapter.CreateNdefMessageCallback ndefMessageCallback = null;
        NfcAdapter.OnNdefPushCompleteCallback onNdefPushCompleteCallback = null;
        boolean resumed = false;
        NfcAdapter.CreateBeamUrisCallback uriCallback = null;
        Uri[] uris = null;

        public NfcActivityState(Activity arg2)
        {
            Object localObject;
            if (localObject.getWindow().isDestroyed())
                throw new IllegalStateException("activity is already destroyed");
            this.resumed = localObject.isResumed();
            this.activity = localObject;
            NfcActivityManager.this.registerApplication(localObject.getApplication());
        }

        public void destroy()
        {
            NfcActivityManager.this.unregisterApplication(this.activity.getApplication());
            this.resumed = false;
            this.activity = null;
            this.ndefMessage = null;
            this.ndefMessageCallback = null;
            this.onNdefPushCompleteCallback = null;
            this.uriCallback = null;
            this.uris = null;
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder("[").append(" ");
            localStringBuilder.append(this.ndefMessage).append(" ").append(this.ndefMessageCallback).append(" ");
            localStringBuilder.append(this.uriCallback).append(" ");
            if (this.uris != null)
                for (Uri localUri : this.uris)
                    localStringBuilder.append(this.onNdefPushCompleteCallback).append(" ").append(localUri).append("]");
            return localStringBuilder.toString();
        }
    }

    class NfcApplicationState
    {
        final Application app;
        int refCount = 0;

        public NfcApplicationState(Application arg2)
        {
            Object localObject;
            this.app = localObject;
        }

        public void register()
        {
            this.refCount = (1 + this.refCount);
            if (this.refCount == 1)
                this.app.registerActivityLifecycleCallbacks(NfcActivityManager.this);
        }

        public void unregister()
        {
            this.refCount = (-1 + this.refCount);
            if (this.refCount == 0)
                this.app.unregisterActivityLifecycleCallbacks(NfcActivityManager.this);
            while (true)
            {
                return;
                if (this.refCount < 0)
                    Log.e("NFC", "-ve refcount for " + this.app);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.NfcActivityManager
 * JD-Core Version:        0.6.2
 */