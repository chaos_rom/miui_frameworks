package android.nfc;

public class FormatException extends Exception
{
    public FormatException()
    {
    }

    public FormatException(String paramString)
    {
        super(paramString);
    }

    public FormatException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.nfc.FormatException
 * JD-Core Version:        0.6.2
 */