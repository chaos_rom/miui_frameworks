package android.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.Downloads.Impl;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.util.Pair;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DownloadManager
{
    public static final String ACTION_DOWNLOAD_COMPLETE = "android.intent.action.DOWNLOAD_COMPLETE";
    public static final String ACTION_NOTIFICATION_CLICKED = "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED";
    public static final String ACTION_VIEW_DOWNLOADS = "android.intent.action.VIEW_DOWNLOADS";
    public static final String COLUMN_BYTES_DOWNLOADED_SO_FAR = "bytes_so_far";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LAST_MODIFIED_TIMESTAMP = "last_modified_timestamp";
    public static final String COLUMN_LOCAL_FILENAME = "local_filename";
    public static final String COLUMN_LOCAL_URI = "local_uri";
    public static final String COLUMN_MEDIAPROVIDER_URI = "mediaprovider_uri";
    public static final String COLUMN_MEDIA_TYPE = "media_type";
    public static final String COLUMN_REASON = "reason";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_TOTAL_SIZE_BYTES = "total_size";
    public static final String COLUMN_URI = "uri";
    public static final int ERROR_BLOCKED = 1010;
    public static final int ERROR_CANNOT_RESUME = 1008;
    public static final int ERROR_DEVICE_NOT_FOUND = 1007;
    public static final int ERROR_FILE_ALREADY_EXISTS = 1009;
    public static final int ERROR_FILE_ERROR = 1001;
    public static final int ERROR_HTTP_DATA_ERROR = 1004;
    public static final int ERROR_INSUFFICIENT_SPACE = 1006;
    public static final int ERROR_TOO_MANY_REDIRECTS = 1005;
    public static final int ERROR_UNHANDLED_HTTP_CODE = 1002;
    public static final int ERROR_UNKNOWN = 1000;
    public static final String EXTRA_DOWNLOAD_ID = "extra_download_id";
    public static final String EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS = "extra_click_download_ids";
    public static final String INTENT_EXTRAS_SORT_BY_SIZE = "android.app.DownloadManager.extra_sortBySize";
    private static final String NON_DOWNLOADMANAGER_DOWNLOAD = "non-dwnldmngr-download-dont-retry2download";
    public static final int PAUSED_QUEUED_FOR_WIFI = 3;
    public static final int PAUSED_UNKNOWN = 4;
    public static final int PAUSED_WAITING_FOR_NETWORK = 2;
    public static final int PAUSED_WAITING_TO_RETRY = 1;
    public static final int STATUS_FAILED = 16;
    public static final int STATUS_PAUSED = 4;
    public static final int STATUS_PENDING = 1;
    public static final int STATUS_RUNNING = 2;
    public static final int STATUS_SUCCESSFUL = 8;
    public static final String[] UNDERLYING_COLUMNS = arrayOfString;
    private Uri mBaseUri = Downloads.Impl.CONTENT_URI;
    private String mPackageName;
    private ContentResolver mResolver;

    static
    {
        String[] arrayOfString = new String[15];
        arrayOfString[0] = "_id";
        arrayOfString[1] = "_data AS local_filename";
        arrayOfString[2] = "mediaprovider_uri";
        arrayOfString[3] = "destination";
        arrayOfString[4] = "title";
        arrayOfString[5] = "description";
        arrayOfString[6] = "uri";
        arrayOfString[7] = "status";
        arrayOfString[8] = "hint";
        arrayOfString[9] = "mimetype AS media_type";
        arrayOfString[10] = "total_bytes AS total_size";
        arrayOfString[11] = "lastmod AS last_modified_timestamp";
        arrayOfString[12] = "current_bytes AS bytes_so_far";
        arrayOfString[13] = "'placeholder' AS local_uri";
        arrayOfString[14] = "'placeholder' AS reason";
    }

    public DownloadManager(ContentResolver paramContentResolver, String paramString)
    {
        this.mResolver = paramContentResolver;
        this.mPackageName = paramString;
    }

    public static long getActiveNetworkWarningBytes(Context paramContext)
    {
        return -1L;
    }

    public static Long getMaxBytesOverMobile(Context paramContext)
    {
        try
        {
            Long localLong2 = Long.valueOf(Settings.Secure.getLong(paramContext.getContentResolver(), "download_manager_max_bytes_over_mobile"));
            localLong1 = localLong2;
            return localLong1;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            while (true)
                Long localLong1 = null;
        }
    }

    public static Long getRecommendedMaxBytesOverMobile(Context paramContext)
    {
        try
        {
            Long localLong2 = Long.valueOf(Settings.Secure.getLong(paramContext.getContentResolver(), "download_manager_recommended_max_bytes_over_mobile"));
            localLong1 = localLong2;
            return localLong1;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            while (true)
                Long localLong1 = null;
        }
    }

    static String[] getWhereArgsForIds(long[] paramArrayOfLong)
    {
        String[] arrayOfString = new String[paramArrayOfLong.length];
        for (int i = 0; i < paramArrayOfLong.length; i++)
            arrayOfString[i] = Long.toString(paramArrayOfLong[i]);
        return arrayOfString;
    }

    static String getWhereClauseForIds(long[] paramArrayOfLong)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("(");
        for (int i = 0; i < paramArrayOfLong.length; i++)
        {
            if (i > 0)
                localStringBuilder.append("OR ");
            localStringBuilder.append("_id");
            localStringBuilder.append(" = ? ");
        }
        localStringBuilder.append(")");
        return localStringBuilder.toString();
    }

    public static boolean isActiveNetworkExpensive(Context paramContext)
    {
        return false;
    }

    private static void validateArgumentIsNonEmpty(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString2))
            throw new IllegalArgumentException(paramString1 + " can't be null");
    }

    public long addCompletedDownload(String paramString1, String paramString2, boolean paramBoolean1, String paramString3, String paramString4, long paramLong, boolean paramBoolean2)
    {
        int i = 2;
        validateArgumentIsNonEmpty("title", paramString1);
        validateArgumentIsNonEmpty("description", paramString2);
        validateArgumentIsNonEmpty("path", paramString4);
        validateArgumentIsNonEmpty("mimeType", paramString3);
        if (paramLong < 0L)
            throw new IllegalArgumentException(" invalid value for param: totalBytes");
        ContentValues localContentValues = new Request("non-dwnldmngr-download-dont-retry2download").setTitle(paramString1).setDescription(paramString2).setMimeType(paramString3).toContentValues(null);
        localContentValues.put("destination", Integer.valueOf(6));
        localContentValues.put("_data", paramString4);
        localContentValues.put("status", Integer.valueOf(200));
        localContentValues.put("total_bytes", Long.valueOf(paramLong));
        int j;
        Uri localUri;
        if (paramBoolean1)
        {
            j = 0;
            localContentValues.put("scanned", Integer.valueOf(j));
            if (paramBoolean2)
                i = 3;
            localContentValues.put("visibility", Integer.valueOf(i));
            localUri = this.mResolver.insert(Downloads.Impl.CONTENT_URI, localContentValues);
            if (localUri != null)
                break label197;
        }
        label197: for (long l = -1L; ; l = Long.parseLong(localUri.getLastPathSegment()))
        {
            return l;
            j = i;
            break;
        }
    }

    public long enqueue(Request paramRequest)
    {
        ContentValues localContentValues = paramRequest.toContentValues(this.mPackageName);
        return Long.parseLong(this.mResolver.insert(Downloads.Impl.CONTENT_URI, localContentValues).getLastPathSegment());
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Uri getBaseUri()
    {
        return this.mBaseUri;
    }

    Uri getDownloadUri(long paramLong)
    {
        return ContentUris.withAppendedId(this.mBaseUri, paramLong);
    }

    public String getMimeTypeForDownloadedFile(long paramLong)
    {
        Object localObject1 = null;
        Query localQuery1 = new Query();
        long[] arrayOfLong = new long[1];
        arrayOfLong[0] = paramLong;
        Query localQuery2 = localQuery1.setFilterById(arrayOfLong);
        Object localObject2 = null;
        while (true)
        {
            try
            {
                Cursor localCursor = query(localQuery2);
                localObject2 = localCursor;
                if (localObject2 == null)
                    return localObject1;
                if (localObject2.moveToFirst())
                {
                    String str = localObject2.getString(localObject2.getColumnIndexOrThrow("media_type"));
                    localObject1 = str;
                    if (localObject2 == null)
                        continue;
                    continue;
                }
            }
            finally
            {
                if (localObject2 != null)
                    localObject2.close();
            }
            if (localObject2 == null);
        }
    }

    public Uri getUriForDownloadedFile(long paramLong)
    {
        Object localObject1 = null;
        Query localQuery1 = new Query();
        long[] arrayOfLong = new long[1];
        arrayOfLong[0] = paramLong;
        Query localQuery2 = localQuery1.setFilterById(arrayOfLong);
        Object localObject2 = null;
        while (true)
        {
            try
            {
                Cursor localCursor = query(localQuery2);
                localObject2 = localCursor;
                if (localObject2 == null)
                    return localObject1;
                if ((localObject2.moveToFirst()) && (8 == localObject2.getInt(localObject2.getColumnIndexOrThrow("status"))))
                {
                    int i = localObject2.getInt(localObject2.getColumnIndexOrThrow("destination"));
                    if ((i == 1) || (i == 5) || (i == 3) || (i == 2))
                    {
                        localObject1 = ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI, paramLong);
                        if (localObject2 == null)
                            continue;
                        continue;
                    }
                    Uri localUri = Uri.fromFile(new File(localObject2.getString(localObject2.getColumnIndexOrThrow("local_filename"))));
                    localObject1 = localUri;
                    if (localObject2 == null)
                        continue;
                    continue;
                }
            }
            finally
            {
                if (localObject2 != null)
                    localObject2.close();
            }
            if (localObject2 == null);
        }
    }

    public int markRowDeleted(long[] paramArrayOfLong)
    {
        if ((paramArrayOfLong == null) || (paramArrayOfLong.length == 0))
            throw new IllegalArgumentException("input param 'ids' can't be null");
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("deleted", Integer.valueOf(1));
        if (paramArrayOfLong.length == 1);
        for (int i = this.mResolver.update(ContentUris.withAppendedId(this.mBaseUri, paramArrayOfLong[0]), localContentValues, null, null); ; i = this.mResolver.update(this.mBaseUri, localContentValues, getWhereClauseForIds(paramArrayOfLong), getWhereArgsForIds(paramArrayOfLong)))
            return i;
    }

    public ParcelFileDescriptor openDownloadedFile(long paramLong)
        throws FileNotFoundException
    {
        return this.mResolver.openFileDescriptor(getDownloadUri(paramLong), "r");
    }

    public Cursor query(Query paramQuery)
    {
        Cursor localCursor = paramQuery.runQuery(this.mResolver, UNDERLYING_COLUMNS, this.mBaseUri);
        if (localCursor == null);
        for (Object localObject = null; ; localObject = new CursorTranslator(localCursor, this.mBaseUri))
            return localObject;
    }

    public int remove(long[] paramArrayOfLong)
    {
        return markRowDeleted(paramArrayOfLong);
    }

    public void restartDownload(long[] paramArrayOfLong)
    {
        Cursor localCursor = query(new Query().setFilterById(paramArrayOfLong));
        while (true)
        {
            try
            {
                localCursor.moveToFirst();
                if (localCursor.isAfterLast())
                    break;
                int i = localCursor.getInt(localCursor.getColumnIndex("status"));
                if ((i != 8) && (i != 16))
                    throw new IllegalArgumentException("Cannot restart incomplete download: " + localCursor.getLong(localCursor.getColumnIndex("_id")));
            }
            finally
            {
                localCursor.close();
            }
            localCursor.moveToNext();
        }
        localCursor.close();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("current_bytes", Integer.valueOf(0));
        localContentValues.put("total_bytes", Integer.valueOf(-1));
        localContentValues.putNull("_data");
        localContentValues.put("status", Integer.valueOf(190));
        this.mResolver.update(this.mBaseUri, localContentValues, getWhereClauseForIds(paramArrayOfLong), getWhereArgsForIds(paramArrayOfLong));
    }

    public void setAccessAllDownloads(boolean paramBoolean)
    {
        if (paramBoolean);
        for (this.mBaseUri = Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI; ; this.mBaseUri = Downloads.Impl.CONTENT_URI)
            return;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    static class CursorTranslator extends CursorWrapper
    {
        private Uri mBaseUri;

        static
        {
            if (!DownloadManager.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        public CursorTranslator(Cursor paramCursor, Uri paramUri)
        {
            super();
            this.mBaseUri = paramUri;
        }

        private long getErrorCode(int paramInt)
        {
            long l;
            if (((400 <= paramInt) && (paramInt < 488)) || ((500 <= paramInt) && (paramInt < 600)))
                l = paramInt;
            while (true)
            {
                return l;
                switch (paramInt)
                {
                default:
                    l = 1000L;
                    break;
                case 492:
                    l = 1001L;
                    break;
                case 493:
                case 494:
                    l = 1002L;
                    break;
                case 495:
                    l = 1004L;
                    break;
                case 497:
                    l = 1005L;
                    break;
                case 198:
                    l = 1006L;
                    break;
                case 199:
                    l = 1007L;
                    break;
                case 489:
                    l = 1008L;
                    break;
                case 488:
                    l = 1009L;
                }
            }
        }

        private String getLocalUri()
        {
            long l1 = getLong(getColumnIndex("destination"));
            String str1;
            String str2;
            if ((l1 == 4L) || (l1 == 0L) || (l1 == 6L))
            {
                str1 = getString(getColumnIndex("local_filename"));
                if (str1 == null)
                    str2 = null;
            }
            while (true)
            {
                return str2;
                str2 = Uri.fromFile(new File(str1)).toString();
                continue;
                long l2 = getLong(getColumnIndex("_id"));
                str2 = ContentUris.withAppendedId(this.mBaseUri, l2).toString();
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        private long getPausedReason(int paramInt)
        {
            long l;
            switch (paramInt)
            {
            default:
                l = DownloadManager.Injector.getPausedReason(paramInt);
            case 194:
            case 195:
            case 196:
            }
            while (true)
            {
                return l;
                l = 1L;
                continue;
                l = 2L;
                continue;
                l = 3L;
            }
        }

        private long getReason(int paramInt)
        {
            long l;
            switch (translateStatus(paramInt))
            {
            default:
                l = 0L;
            case 16:
            case 4:
            }
            while (true)
            {
                return l;
                l = getErrorCode(paramInt);
                continue;
                l = getPausedReason(paramInt);
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
        static int translateStatus(int paramInt)
        {
            int i;
            switch (paramInt)
            {
            case 191:
            case 197:
            case 198:
            case 199:
            default:
                if ((!$assertionsDisabled) && (!Downloads.Impl.isStatusError(paramInt)))
                    throw new AssertionError();
                break;
            case 190:
                i = 1;
            case 192:
            case 193:
            case 194:
            case 195:
            case 196:
            case 200:
            }
            while (true)
            {
                return i;
                i = 2;
                continue;
                i = 4;
                continue;
                i = 8;
                continue;
                i = 16;
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        String callGetLocalUri()
        {
            return getLocalUri();
        }

        public int getInt(int paramInt)
        {
            return (int)getLong(paramInt);
        }

        public long getLong(int paramInt)
        {
            long l;
            if (getColumnName(paramInt).equals("reason"))
                l = getReason(super.getInt(getColumnIndex("status")));
            while (true)
            {
                return l;
                if (getColumnName(paramInt).equals("status"))
                    l = translateStatus(super.getInt(getColumnIndex("status")));
                else
                    l = super.getLong(paramInt);
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public String getString(int paramInt)
        {
            if (getColumnName(paramInt).equals("local_uri"));
            for (String str = DownloadManager.Injector.getLocalUri(this); ; str = super.getString(paramInt))
                return str;
        }
    }

    public static class Query
    {
        public static final int ORDER_ASCENDING = 1;
        public static final int ORDER_DESCENDING = 2;
        private long[] mIds = null;
        private boolean mOnlyIncludeVisibleInDownloadsUi = false;
        private String mOrderByColumn = "lastmod";
        private int mOrderDirection = 2;
        private Integer mStatusFlags = null;

        private String joinStrings(String paramString, Iterable<String> paramIterable)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            int i = 1;
            Iterator localIterator = paramIterable.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                if (i == 0)
                    localStringBuilder.append(paramString);
                localStringBuilder.append(str);
                i = 0;
            }
            return localStringBuilder.toString();
        }

        private String statusClause(String paramString, int paramInt)
        {
            return "status" + paramString + "'" + paramInt + "'";
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        void addExtraSelectionParts(List<String> paramList)
        {
        }

        public Query orderBy(String paramString, int paramInt)
        {
            if ((paramInt != 1) && (paramInt != 2))
                throw new IllegalArgumentException("Invalid direction: " + paramInt);
            if (paramString.equals("last_modified_timestamp"));
            for (this.mOrderByColumn = "lastmod"; ; this.mOrderByColumn = "total_bytes")
            {
                this.mOrderDirection = paramInt;
                return this;
                if (!paramString.equals("total_size"))
                    break;
            }
            throw new IllegalArgumentException("Cannot order by " + paramString);
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        Cursor runQuery(ContentResolver paramContentResolver, String[] paramArrayOfString, Uri paramUri)
        {
            ArrayList localArrayList1 = new ArrayList();
            String[] arrayOfString = null;
            if (this.mIds != null)
            {
                localArrayList1.add(DownloadManager.getWhereClauseForIds(this.mIds));
                arrayOfString = DownloadManager.getWhereArgsForIds(this.mIds);
            }
            if (this.mStatusFlags != null)
            {
                ArrayList localArrayList2 = new ArrayList();
                if ((0x1 & this.mStatusFlags.intValue()) != 0)
                    localArrayList2.add(statusClause("=", 190));
                if ((0x2 & this.mStatusFlags.intValue()) != 0)
                    localArrayList2.add(statusClause("=", 192));
                if ((0x4 & this.mStatusFlags.intValue()) != 0)
                {
                    localArrayList2.add(statusClause("=", 193));
                    localArrayList2.add(statusClause("=", 194));
                    localArrayList2.add(statusClause("=", 195));
                    localArrayList2.add(statusClause("=", 196));
                }
                if ((0x8 & this.mStatusFlags.intValue()) != 0)
                    localArrayList2.add(statusClause("=", 200));
                if ((0x10 & this.mStatusFlags.intValue()) != 0)
                    localArrayList2.add("(" + statusClause(">=", 400) + " AND " + statusClause("<", 600) + ")");
                localArrayList1.add(DownloadManager.Injector.addParens(joinStrings(" OR ", localArrayList2)));
            }
            if (this.mOnlyIncludeVisibleInDownloadsUi)
                localArrayList1.add("is_visible_in_downloads_ui != '0'");
            addExtraSelectionParts(localArrayList1);
            localArrayList1.add("deleted != '1'");
            String str1 = joinStrings(" AND ", localArrayList1);
            if (this.mOrderDirection == 1);
            for (String str2 = "ASC"; ; str2 = "DESC")
                return paramContentResolver.query(paramUri, paramArrayOfString, str1, arrayOfString, this.mOrderByColumn + " " + str2);
        }

        public Query setFilterById(long[] paramArrayOfLong)
        {
            this.mIds = paramArrayOfLong;
            return this;
        }

        public Query setFilterByStatus(int paramInt)
        {
            this.mStatusFlags = Integer.valueOf(paramInt);
            return this;
        }

        public Query setOnlyIncludeVisibleInDownloadsUi(boolean paramBoolean)
        {
            this.mOnlyIncludeVisibleInDownloadsUi = paramBoolean;
            return this;
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        void setOrderByColumn(String paramString)
        {
            this.mOrderByColumn = paramString;
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        void setOrderDirection(int paramInt)
        {
            this.mOrderDirection = paramInt;
        }
    }

    public static class Request
    {
        public static final int NETWORK_MOBILE = 1;
        public static final int NETWORK_WIFI = 2;
        private static final int SCANNABLE_VALUE_NO = 2;
        private static final int SCANNABLE_VALUE_YES = 0;
        public static final int VISIBILITY_HIDDEN = 2;
        public static final int VISIBILITY_VISIBLE = 0;
        public static final int VISIBILITY_VISIBLE_NOTIFY_COMPLETED = 1;
        public static final int VISIBILITY_VISIBLE_NOTIFY_ONLY_COMPLETION = 3;
        private int mAllowedNetworkTypes = -1;
        private CharSequence mDescription;
        private Uri mDestinationUri;
        private boolean mIsVisibleInDownloadsUi = true;
        private boolean mMeteredAllowed = true;
        private String mMimeType;
        private int mNotificationVisibility = 0;
        private List<Pair<String, String>> mRequestHeaders = new ArrayList();
        private boolean mRoamingAllowed = true;
        private boolean mScannable = false;
        private CharSequence mTitle;
        private Uri mUri;
        private boolean mUseSystemCache = false;

        static
        {
            if (!DownloadManager.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        public Request(Uri paramUri)
        {
            if (paramUri == null)
                throw new NullPointerException();
            String str = paramUri.getScheme();
            if ((str == null) || ((!str.equals("http")) && (!str.equals("https"))))
                throw new IllegalArgumentException("Can only download HTTP/HTTPS URIs: " + paramUri);
            this.mUri = paramUri;
        }

        Request(String paramString)
        {
            this.mUri = Uri.parse(paramString);
        }

        private void encodeHttpHeaders(ContentValues paramContentValues)
        {
            int i = 0;
            Iterator localIterator = this.mRequestHeaders.iterator();
            while (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                String str = (String)localPair.first + ": " + (String)localPair.second;
                paramContentValues.put("http_header_" + i, str);
                i++;
            }
        }

        private void putIfNonNull(ContentValues paramContentValues, String paramString, Object paramObject)
        {
            if (paramObject != null)
                paramContentValues.put(paramString, paramObject.toString());
        }

        private void setDestinationFromBase(File paramFile, String paramString)
        {
            if (paramString == null)
                throw new NullPointerException("subPath cannot be null");
            this.mDestinationUri = Uri.withAppendedPath(Uri.fromFile(paramFile), paramString);
        }

        public Request addRequestHeader(String paramString1, String paramString2)
        {
            if (paramString1 == null)
                throw new NullPointerException("header cannot be null");
            if (paramString1.contains(":"))
                throw new IllegalArgumentException("header may not contain ':'");
            if (paramString2 == null)
                paramString2 = "";
            this.mRequestHeaders.add(Pair.create(paramString1, paramString2));
            return this;
        }

        public void allowScanningByMediaScanner()
        {
            this.mScannable = true;
        }

        public Request setAllowedNetworkTypes(int paramInt)
        {
            this.mAllowedNetworkTypes = paramInt;
            return this;
        }

        public Request setAllowedOverMetered(boolean paramBoolean)
        {
            this.mMeteredAllowed = paramBoolean;
            return this;
        }

        public Request setAllowedOverRoaming(boolean paramBoolean)
        {
            this.mRoamingAllowed = paramBoolean;
            return this;
        }

        public Request setDescription(CharSequence paramCharSequence)
        {
            this.mDescription = paramCharSequence;
            return this;
        }

        public Request setDestinationInExternalFilesDir(Context paramContext, String paramString1, String paramString2)
        {
            setDestinationFromBase(paramContext.getExternalFilesDir(paramString1), paramString2);
            return this;
        }

        public Request setDestinationInExternalPublicDir(String paramString1, String paramString2)
        {
            File localFile = Environment.getExternalStoragePublicDirectory(paramString1);
            if (localFile.exists())
            {
                if (!localFile.isDirectory())
                    throw new IllegalStateException(localFile.getAbsolutePath() + " already exists and is not a directory");
            }
            else if (!localFile.mkdir())
                throw new IllegalStateException("Unable to create directory: " + localFile.getAbsolutePath());
            setDestinationFromBase(localFile, paramString2);
            return this;
        }

        public Request setDestinationToSystemCache()
        {
            this.mUseSystemCache = true;
            return this;
        }

        public Request setDestinationUri(Uri paramUri)
        {
            this.mDestinationUri = paramUri;
            return this;
        }

        public Request setMimeType(String paramString)
        {
            this.mMimeType = paramString;
            return this;
        }

        public Request setNotificationVisibility(int paramInt)
        {
            this.mNotificationVisibility = paramInt;
            return this;
        }

        @Deprecated
        public Request setShowRunningNotification(boolean paramBoolean)
        {
            if (paramBoolean);
            for (Request localRequest = setNotificationVisibility(0); ; localRequest = setNotificationVisibility(2))
                return localRequest;
        }

        public Request setTitle(CharSequence paramCharSequence)
        {
            this.mTitle = paramCharSequence;
            return this;
        }

        public Request setVisibleInDownloadsUi(boolean paramBoolean)
        {
            this.mIsVisibleInDownloadsUi = paramBoolean;
            return this;
        }

        ContentValues toContentValues(String paramString)
        {
            int i = 2;
            ContentValues localContentValues = new ContentValues();
            assert (this.mUri != null);
            localContentValues.put("uri", this.mUri.toString());
            localContentValues.put("is_public_api", Boolean.valueOf(true));
            localContentValues.put("notificationpackage", paramString);
            if (this.mDestinationUri != null)
            {
                localContentValues.put("destination", Integer.valueOf(4));
                localContentValues.put("hint", this.mDestinationUri.toString());
                if (this.mScannable)
                    i = 0;
                localContentValues.put("scanned", Integer.valueOf(i));
                if (!this.mRequestHeaders.isEmpty())
                    encodeHttpHeaders(localContentValues);
                putIfNonNull(localContentValues, "title", this.mTitle);
                putIfNonNull(localContentValues, "description", this.mDescription);
                putIfNonNull(localContentValues, "mimetype", this.mMimeType);
                localContentValues.put("visibility", Integer.valueOf(this.mNotificationVisibility));
                localContentValues.put("allowed_network_types", Integer.valueOf(this.mAllowedNetworkTypes));
                localContentValues.put("allow_roaming", Boolean.valueOf(this.mRoamingAllowed));
                localContentValues.put("allow_metered", Boolean.valueOf(this.mMeteredAllowed));
                localContentValues.put("is_visible_in_downloads_ui", Boolean.valueOf(this.mIsVisibleInDownloadsUi));
                return localContentValues;
            }
            if (this.mUseSystemCache);
            for (int j = 5; ; j = i)
            {
                localContentValues.put("destination", Integer.valueOf(j));
                break;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String addParens(String paramString)
        {
            return "(" + paramString + ")";
        }

        static String getLocalUri(DownloadManager.CursorTranslator paramCursorTranslator)
        {
            if (paramCursorTranslator.getLong(paramCursorTranslator.getColumnIndex("destination")) == 4L);
            for (String str = paramCursorTranslator.getString(paramCursorTranslator.getColumnIndex("hint")); ; str = paramCursorTranslator.callGetLocalUri())
                return str;
        }

        public static long getPausedReason(int paramInt)
        {
            if (paramInt == 193);
            for (long l = 5L; ; l = 4L)
                return l;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.DownloadManager
 * JD-Core Version:        0.6.2
 */