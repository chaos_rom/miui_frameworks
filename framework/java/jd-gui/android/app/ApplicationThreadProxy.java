package android.app;

import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.FileDescriptor;
import java.util.List;
import java.util.Map;

class ApplicationThreadProxy
    implements IApplicationThread
{
    private final IBinder mRemote;

    public ApplicationThreadProxy(IBinder paramIBinder)
    {
        this.mRemote = paramIBinder;
    }

    public final IBinder asBinder()
    {
        return this.mRemote;
    }

    public final void bindApplication(String paramString1, ApplicationInfo paramApplicationInfo, List<ProviderInfo> paramList, ComponentName paramComponentName, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor, boolean paramBoolean1, Bundle paramBundle1, IInstrumentationWatcher paramIInstrumentationWatcher, int paramInt, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, Map<String, IBinder> paramMap, Bundle paramBundle2)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeString(paramString1);
        paramApplicationInfo.writeToParcel(localParcel, 0);
        localParcel.writeTypedList(paramList);
        label68: int i;
        label76: int j;
        label112: int k;
        if (paramComponentName == null)
        {
            localParcel.writeInt(0);
            localParcel.writeString(paramString2);
            if (paramParcelFileDescriptor == null)
                break label218;
            localParcel.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel, 1);
            if (!paramBoolean1)
                break label227;
            i = 1;
            localParcel.writeInt(i);
            localParcel.writeBundle(paramBundle1);
            localParcel.writeStrongInterface(paramIInstrumentationWatcher);
            localParcel.writeInt(paramInt);
            if (!paramBoolean2)
                break label233;
            j = 1;
            localParcel.writeInt(j);
            if (!paramBoolean3)
                break label239;
            k = 1;
            label127: localParcel.writeInt(k);
            if (!paramBoolean4)
                break label245;
        }
        label218: label227: label233: label239: label245: for (int m = 1; ; m = 0)
        {
            localParcel.writeInt(m);
            paramConfiguration.writeToParcel(localParcel, 0);
            paramCompatibilityInfo.writeToParcel(localParcel, 0);
            localParcel.writeMap(paramMap);
            localParcel.writeBundle(paramBundle2);
            this.mRemote.transact(13, localParcel, null, 1);
            localParcel.recycle();
            return;
            localParcel.writeInt(1);
            paramComponentName.writeToParcel(localParcel, 0);
            break;
            localParcel.writeInt(0);
            break label68;
            i = 0;
            break label76;
            j = 0;
            break label112;
            k = 0;
            break label127;
        }
    }

    public void clearDnsCache()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(38, localParcel, null, 1);
        localParcel.recycle();
    }

    public void dispatchPackageBroadcast(int paramInt, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeInt(paramInt);
        localParcel.writeStringArray(paramArrayOfString);
        this.mRemote.transact(34, localParcel, null, 1);
        localParcel.recycle();
    }

    public void dumpActivity(FileDescriptor paramFileDescriptor, IBinder paramIBinder, String paramString, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeFileDescriptor(paramFileDescriptor);
        localParcel.writeStrongBinder(paramIBinder);
        localParcel.writeString(paramString);
        localParcel.writeStringArray(paramArrayOfString);
        this.mRemote.transact(37, localParcel, null, 1);
        localParcel.recycle();
    }

    public void dumpDbInfo(FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeFileDescriptor(paramFileDescriptor);
        localParcel.writeStringArray(paramArrayOfString);
        this.mRemote.transact(46, localParcel, null, 1);
        localParcel.recycle();
    }

    public void dumpGfxInfo(FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeFileDescriptor(paramFileDescriptor);
        localParcel.writeStringArray(paramArrayOfString);
        this.mRemote.transact(44, localParcel, null, 1);
        localParcel.recycle();
    }

    public void dumpHeap(boolean paramBoolean, String paramString, ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel.writeInt(i);
            localParcel.writeString(paramString);
            if (paramParcelFileDescriptor == null)
                break label77;
            localParcel.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel, 1);
        }
        while (true)
        {
            this.mRemote.transact(36, localParcel, null, 1);
            localParcel.recycle();
            return;
            i = 0;
            break;
            label77: localParcel.writeInt(0);
        }
    }

    public Debug.MemoryInfo dumpMemInfo(FileDescriptor paramFileDescriptor, boolean paramBoolean1, boolean paramBoolean2, String[] paramArrayOfString)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IApplicationThread");
        localParcel1.writeFileDescriptor(paramFileDescriptor);
        int j;
        if (paramBoolean1)
        {
            j = i;
            localParcel1.writeInt(j);
            if (!paramBoolean2)
                break label116;
        }
        while (true)
        {
            localParcel1.writeInt(i);
            localParcel1.writeStringArray(paramArrayOfString);
            this.mRemote.transact(43, localParcel1, localParcel2, 0);
            localParcel2.readException();
            Debug.MemoryInfo localMemoryInfo = new Debug.MemoryInfo();
            localMemoryInfo.readFromParcel(localParcel2);
            localParcel1.recycle();
            localParcel2.recycle();
            return localMemoryInfo;
            j = 0;
            break;
            label116: i = 0;
        }
    }

    public void dumpProvider(FileDescriptor paramFileDescriptor, IBinder paramIBinder, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeFileDescriptor(paramFileDescriptor);
        localParcel.writeStrongBinder(paramIBinder);
        localParcel.writeStringArray(paramArrayOfString);
        this.mRemote.transact(45, localParcel, null, 1);
        localParcel.recycle();
    }

    public void dumpService(FileDescriptor paramFileDescriptor, IBinder paramIBinder, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeFileDescriptor(paramFileDescriptor);
        localParcel.writeStrongBinder(paramIBinder);
        localParcel.writeStringArray(paramArrayOfString);
        this.mRemote.transact(22, localParcel, null, 1);
        localParcel.recycle();
    }

    public void getMemoryInfo(Debug.MemoryInfo paramMemoryInfo)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(32, localParcel1, localParcel2, 0);
        localParcel2.readException();
        paramMemoryInfo.readFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void processInBackground()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(19, localParcel, null, 1);
        localParcel.recycle();
    }

    public void profilerControl(boolean paramBoolean, String paramString, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel.writeInt(i);
            localParcel.writeInt(paramInt);
            localParcel.writeString(paramString);
            if (paramParcelFileDescriptor == null)
                break label84;
            localParcel.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel, 1);
        }
        while (true)
        {
            this.mRemote.transact(28, localParcel, null, 1);
            localParcel.recycle();
            return;
            i = 0;
            break;
            label84: localParcel.writeInt(0);
        }
    }

    public final void requestThumbnail(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        this.mRemote.transact(15, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleActivityConfigurationChanged(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        this.mRemote.transact(25, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleBindService(IBinder paramIBinder, Intent paramIntent, boolean paramBoolean)
        throws RemoteException
    {
        int i = 0;
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        paramIntent.writeToParcel(localParcel, 0);
        if (paramBoolean)
            i = 1;
        localParcel.writeInt(i);
        this.mRemote.transact(20, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleConfigurationChanged(Configuration paramConfiguration)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        paramConfiguration.writeToParcel(localParcel, 0);
        this.mRemote.transact(16, localParcel, null, 1);
        localParcel.recycle();
    }

    public void scheduleCrash(String paramString)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeString(paramString);
        this.mRemote.transact(35, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleCreateBackupAgent(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        paramApplicationInfo.writeToParcel(localParcel, 0);
        paramCompatibilityInfo.writeToParcel(localParcel, 0);
        localParcel.writeInt(paramInt);
        this.mRemote.transact(30, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleCreateService(IBinder paramIBinder, ServiceInfo paramServiceInfo, CompatibilityInfo paramCompatibilityInfo)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        paramServiceInfo.writeToParcel(localParcel, 0);
        paramCompatibilityInfo.writeToParcel(localParcel, 0);
        this.mRemote.transact(11, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleDestroyActivity(IBinder paramIBinder, boolean paramBoolean, int paramInt)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            localParcel.writeInt(paramInt);
            this.mRemote.transact(9, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public final void scheduleDestroyBackupAgent(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        paramApplicationInfo.writeToParcel(localParcel, 0);
        paramCompatibilityInfo.writeToParcel(localParcel, 0);
        this.mRemote.transact(31, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleExit()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(14, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleLaunchActivity(Intent paramIntent, IBinder paramIBinder, int paramInt, ActivityInfo paramActivityInfo, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, Bundle paramBundle, List<ResultInfo> paramList, List<Intent> paramList1, boolean paramBoolean1, boolean paramBoolean2, String paramString, ParcelFileDescriptor paramParcelFileDescriptor, boolean paramBoolean3)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        paramIntent.writeToParcel(localParcel, 0);
        localParcel.writeStrongBinder(paramIBinder);
        localParcel.writeInt(paramInt);
        paramActivityInfo.writeToParcel(localParcel, 0);
        paramConfiguration.writeToParcel(localParcel, 0);
        paramCompatibilityInfo.writeToParcel(localParcel, 0);
        localParcel.writeBundle(paramBundle);
        localParcel.writeTypedList(paramList);
        localParcel.writeTypedList(paramList1);
        int i;
        int j;
        if (paramBoolean1)
        {
            i = 1;
            localParcel.writeInt(i);
            if (!paramBoolean2)
                break label175;
            j = 1;
            label99: localParcel.writeInt(j);
            localParcel.writeString(paramString);
            if (paramParcelFileDescriptor == null)
                break label181;
            localParcel.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel, 1);
            label132: if (!paramBoolean3)
                break label190;
        }
        label175: label181: label190: for (int k = 1; ; k = 0)
        {
            localParcel.writeInt(k);
            this.mRemote.transact(7, localParcel, null, 1);
            localParcel.recycle();
            return;
            i = 0;
            break;
            j = 0;
            break label99;
            localParcel.writeInt(0);
            break label132;
        }
    }

    public final void scheduleLowMemory()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(24, localParcel, null, 1);
        localParcel.recycle();
    }

    public void scheduleNewIntent(List<Intent> paramList, IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeTypedList(paramList);
        localParcel.writeStrongBinder(paramIBinder);
        this.mRemote.transact(8, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void schedulePauseActivity(IBinder paramIBinder, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
        throws RemoteException
    {
        int i = 0;
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        if (paramBoolean1);
        for (int j = 1; ; j = 0)
        {
            localParcel.writeInt(j);
            if (paramBoolean2)
                i = 1;
            localParcel.writeInt(i);
            localParcel.writeInt(paramInt);
            this.mRemote.transact(1, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public final void scheduleReceiver(Intent paramIntent, ActivityInfo paramActivityInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean)
        throws RemoteException
    {
        int i = 0;
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        paramIntent.writeToParcel(localParcel, 0);
        paramActivityInfo.writeToParcel(localParcel, 0);
        paramCompatibilityInfo.writeToParcel(localParcel, 0);
        localParcel.writeInt(paramInt);
        localParcel.writeString(paramString);
        localParcel.writeBundle(paramBundle);
        if (paramBoolean)
            i = 1;
        localParcel.writeInt(i);
        this.mRemote.transact(10, localParcel, null, 1);
        localParcel.recycle();
    }

    public void scheduleRegisteredReceiver(IIntentReceiver paramIIntentReceiver, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException
    {
        int i = 0;
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIIntentReceiver.asBinder());
        paramIntent.writeToParcel(localParcel, 0);
        localParcel.writeInt(paramInt);
        localParcel.writeString(paramString);
        localParcel.writeBundle(paramBundle);
        if (paramBoolean1);
        for (int j = 1; ; j = 0)
        {
            localParcel.writeInt(j);
            if (paramBoolean2)
                i = 1;
            localParcel.writeInt(i);
            this.mRemote.transact(23, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public final void scheduleRelaunchActivity(IBinder paramIBinder, List<ResultInfo> paramList, List<Intent> paramList1, int paramInt, boolean paramBoolean, Configuration paramConfiguration)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        localParcel.writeTypedList(paramList);
        localParcel.writeTypedList(paramList1);
        localParcel.writeInt(paramInt);
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel.writeInt(i);
            if (paramConfiguration == null)
                break label99;
            localParcel.writeInt(1);
            paramConfiguration.writeToParcel(localParcel, 0);
        }
        while (true)
        {
            this.mRemote.transact(26, localParcel, null, 1);
            localParcel.recycle();
            return;
            i = 0;
            break;
            label99: localParcel.writeInt(0);
        }
    }

    public final void scheduleResumeActivity(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            this.mRemote.transact(5, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public final void scheduleSendResult(IBinder paramIBinder, List<ResultInfo> paramList)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        localParcel.writeTypedList(paramList);
        this.mRemote.transact(6, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleServiceArgs(IBinder paramIBinder, boolean paramBoolean, int paramInt1, int paramInt2, Intent paramIntent)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel.writeInt(i);
            localParcel.writeInt(paramInt1);
            localParcel.writeInt(paramInt2);
            if (paramIntent == null)
                break label92;
            localParcel.writeInt(1);
            paramIntent.writeToParcel(localParcel, 0);
        }
        while (true)
        {
            this.mRemote.transact(17, localParcel, null, 1);
            localParcel.recycle();
            return;
            i = 0;
            break;
            label92: localParcel.writeInt(0);
        }
    }

    public final void scheduleSleeping(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            this.mRemote.transact(27, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public final void scheduleStopActivity(IBinder paramIBinder, boolean paramBoolean, int paramInt)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            localParcel.writeInt(paramInt);
            this.mRemote.transact(3, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public final void scheduleStopService(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        this.mRemote.transact(12, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleSuicide()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(33, localParcel, null, 1);
        localParcel.recycle();
    }

    public void scheduleTrimMemory(int paramInt)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeInt(paramInt);
        this.mRemote.transact(42, localParcel, null, 1);
    }

    public final void scheduleUnbindService(IBinder paramIBinder, Intent paramIntent)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        paramIntent.writeToParcel(localParcel, 0);
        this.mRemote.transact(21, localParcel, null, 1);
        localParcel.recycle();
    }

    public final void scheduleWindowVisibility(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            this.mRemote.transact(4, localParcel, null, 1);
            localParcel.recycle();
            return;
        }
    }

    public void setCoreSettings(Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeBundle(paramBundle);
        this.mRemote.transact(40, localParcel, null, 1);
    }

    public void setHttpProxy(String paramString1, String paramString2, String paramString3)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeString(paramString1);
        localParcel.writeString(paramString2);
        localParcel.writeString(paramString3);
        this.mRemote.transact(39, localParcel, null, 1);
        localParcel.recycle();
    }

    public void setSchedulingGroup(int paramInt)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeInt(paramInt);
        this.mRemote.transact(29, localParcel, null, 1);
        localParcel.recycle();
    }

    public void unstableProviderDied(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeStrongBinder(paramIBinder);
        this.mRemote.transact(47, localParcel, null, 1);
        localParcel.recycle();
    }

    public void updatePackageCompatibilityInfo(String paramString, CompatibilityInfo paramCompatibilityInfo)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        localParcel.writeString(paramString);
        paramCompatibilityInfo.writeToParcel(localParcel, 0);
        this.mRemote.transact(41, localParcel, null, 1);
    }

    public void updateTimeZone()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IApplicationThread");
        this.mRemote.transact(18, localParcel, null, 1);
        localParcel.recycle();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ApplicationThreadProxy
 * JD-Core Version:        0.6.2
 */