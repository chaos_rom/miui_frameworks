package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IAlarmManager extends IInterface
{
    public abstract void remove(PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void set(int paramInt, long paramLong, PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void setInexactRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void setRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void setTime(long paramLong)
        throws RemoteException;

    public abstract void setTimeZone(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAlarmManager
    {
        private static final String DESCRIPTOR = "android.app.IAlarmManager";
        static final int TRANSACTION_remove = 6;
        static final int TRANSACTION_set = 1;
        static final int TRANSACTION_setInexactRepeating = 3;
        static final int TRANSACTION_setRepeating = 2;
        static final int TRANSACTION_setTime = 4;
        static final int TRANSACTION_setTimeZone = 5;

        public Stub()
        {
            attachInterface(this, "android.app.IAlarmManager");
        }

        public static IAlarmManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IAlarmManager");
                if ((localIInterface != null) && ((localIInterface instanceof IAlarmManager)))
                    localObject = (IAlarmManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.app.IAlarmManager");
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.app.IAlarmManager");
                    int k = paramParcel1.readInt();
                    long l5 = paramParcel1.readLong();
                    if (paramParcel1.readInt() != 0);
                    for (PendingIntent localPendingIntent4 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent4 = null)
                    {
                        set(k, l5, localPendingIntent4);
                        paramParcel2.writeNoException();
                        bool = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.app.IAlarmManager");
                    int j = paramParcel1.readInt();
                    long l3 = paramParcel1.readLong();
                    long l4 = paramParcel1.readLong();
                    if (paramParcel1.readInt() != 0);
                    for (PendingIntent localPendingIntent3 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent3 = null)
                    {
                        setRepeating(j, l3, l4, localPendingIntent3);
                        paramParcel2.writeNoException();
                        bool = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.app.IAlarmManager");
                    int i = paramParcel1.readInt();
                    long l1 = paramParcel1.readLong();
                    long l2 = paramParcel1.readLong();
                    if (paramParcel1.readInt() != 0);
                    for (PendingIntent localPendingIntent2 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent2 = null)
                    {
                        setInexactRepeating(i, l1, l2, localPendingIntent2);
                        paramParcel2.writeNoException();
                        bool = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.app.IAlarmManager");
                    setTime(paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.app.IAlarmManager");
                    setTimeZone(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool = true;
                }
            case 6:
            }
            paramParcel1.enforceInterface("android.app.IAlarmManager");
            if (paramParcel1.readInt() != 0);
            for (PendingIntent localPendingIntent1 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent1 = null)
            {
                remove(localPendingIntent1);
                paramParcel2.writeNoException();
                bool = true;
                break;
            }
        }

        private static class Proxy
            implements IAlarmManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IAlarmManager";
            }

            public void remove(PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IAlarmManager");
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void set(int paramInt, long paramLong, PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IAlarmManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeLong(paramLong);
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInexactRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IAlarmManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeLong(paramLong2);
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IAlarmManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeLong(paramLong2);
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setTime(long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IAlarmManager");
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setTimeZone(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IAlarmManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IAlarmManager
 * JD-Core Version:        0.6.2
 */