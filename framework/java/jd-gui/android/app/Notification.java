package android.app;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.text.TextUtils;
import android.widget.RemoteViews;
import java.text.NumberFormat;
import java.util.ArrayList;

public class Notification
    implements Parcelable
{
    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator()
    {
        public Notification createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Notification(paramAnonymousParcel);
        }

        public Notification[] newArray(int paramAnonymousInt)
        {
            return new Notification[paramAnonymousInt];
        }
    };
    public static final int DEFAULT_ALL = -1;
    public static final int DEFAULT_LIGHTS = 4;
    public static final int DEFAULT_SOUND = 1;
    public static final int DEFAULT_VIBRATE = 2;
    public static final String EXTRA_PEOPLE = "android.people";
    public static final int FLAG_AUTO_CANCEL = 16;
    public static final int FLAG_FOREGROUND_SERVICE = 64;
    public static final int FLAG_HIGH_PRIORITY = 128;
    public static final int FLAG_INSISTENT = 4;
    public static final int FLAG_NO_CLEAR = 32;
    public static final int FLAG_ONGOING_EVENT = 2;
    public static final int FLAG_ONLY_ALERT_ONCE = 8;
    public static final int FLAG_SHOW_LIGHTS = 1;
    public static final String KIND_CALL = "android.call";
    public static final String KIND_EMAIL = "android.email";
    public static final String KIND_EVENT = "android.event";
    public static final String KIND_MESSAGE = "android.message";
    public static final String KIND_PROMO = "android.promo";
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = -1;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = -2;
    public static final int STREAM_DEFAULT = -1;
    private Action[] actions;
    public int audioStreamType = -1;
    public RemoteViews bigContentView;
    public PendingIntent contentIntent;
    public RemoteViews contentView;
    public int defaults;
    public PendingIntent deleteIntent;
    private Bundle extras;
    public int flags;
    public PendingIntent fullScreenIntent;
    public int icon;
    public int iconLevel;
    public String[] kind;
    public Bitmap largeIcon;
    public int ledARGB;
    public int ledOffMS;
    public int ledOnMS;
    public int number;
    public int priority;
    public Uri sound;
    public CharSequence tickerText;
    public RemoteViews tickerView;
    public long[] vibrate;
    public long when;

    public Notification()
    {
        this.when = System.currentTimeMillis();
        this.priority = 0;
    }

    @Deprecated
    public Notification(int paramInt, CharSequence paramCharSequence, long paramLong)
    {
        this.icon = paramInt;
        this.tickerText = paramCharSequence;
        this.when = paramLong;
    }

    public Notification(Context paramContext, int paramInt, CharSequence paramCharSequence1, long paramLong, CharSequence paramCharSequence2, CharSequence paramCharSequence3, Intent paramIntent)
    {
        this.when = paramLong;
        this.icon = paramInt;
        this.tickerText = paramCharSequence1;
        setLatestEventInfo(paramContext, paramCharSequence2, paramCharSequence3, PendingIntent.getActivity(paramContext, 0, paramIntent, 0));
    }

    public Notification(Parcel paramParcel)
    {
        paramParcel.readInt();
        this.when = paramParcel.readLong();
        this.icon = paramParcel.readInt();
        this.number = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
            this.contentIntent = ((PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel));
        if (paramParcel.readInt() != 0)
            this.deleteIntent = ((PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel));
        if (paramParcel.readInt() != 0)
            this.tickerText = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        if (paramParcel.readInt() != 0)
            this.tickerView = ((RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel));
        if (paramParcel.readInt() != 0)
            this.contentView = ((RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel));
        if (paramParcel.readInt() != 0)
            this.largeIcon = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel));
        this.defaults = paramParcel.readInt();
        this.flags = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
            this.sound = ((Uri)Uri.CREATOR.createFromParcel(paramParcel));
        this.audioStreamType = paramParcel.readInt();
        this.vibrate = paramParcel.createLongArray();
        this.ledARGB = paramParcel.readInt();
        this.ledOnMS = paramParcel.readInt();
        this.ledOffMS = paramParcel.readInt();
        this.iconLevel = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
            this.fullScreenIntent = ((PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel));
        this.priority = paramParcel.readInt();
        this.kind = paramParcel.createStringArray();
        if (paramParcel.readInt() != 0)
            this.extras = paramParcel.readBundle();
        this.actions = ((Action[])paramParcel.createTypedArray(Action.CREATOR));
        if (paramParcel.readInt() != 0)
            this.bigContentView = ((RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel));
    }

    public Notification clone()
    {
        Notification localNotification = new Notification();
        localNotification.when = this.when;
        localNotification.icon = this.icon;
        localNotification.number = this.number;
        localNotification.contentIntent = this.contentIntent;
        localNotification.deleteIntent = this.deleteIntent;
        localNotification.fullScreenIntent = this.fullScreenIntent;
        if (this.tickerText != null)
            localNotification.tickerText = this.tickerText.toString();
        if (this.tickerView != null)
            localNotification.tickerView = this.tickerView.clone();
        if (this.contentView != null)
            localNotification.contentView = this.contentView.clone();
        if (this.largeIcon != null)
            localNotification.largeIcon = Bitmap.createBitmap(this.largeIcon);
        localNotification.iconLevel = this.iconLevel;
        localNotification.sound = this.sound;
        localNotification.audioStreamType = this.audioStreamType;
        long[] arrayOfLong1 = this.vibrate;
        if (arrayOfLong1 != null)
        {
            int k = arrayOfLong1.length;
            long[] arrayOfLong2 = new long[k];
            localNotification.vibrate = arrayOfLong2;
            System.arraycopy(arrayOfLong1, 0, arrayOfLong2, 0, k);
        }
        localNotification.ledARGB = this.ledARGB;
        localNotification.ledOnMS = this.ledOnMS;
        localNotification.ledOffMS = this.ledOffMS;
        localNotification.defaults = this.defaults;
        localNotification.flags = this.flags;
        localNotification.priority = this.priority;
        String[] arrayOfString1 = this.kind;
        if (arrayOfString1 != null)
        {
            int j = arrayOfString1.length;
            String[] arrayOfString2 = new String[j];
            localNotification.kind = arrayOfString2;
            System.arraycopy(arrayOfString1, 0, arrayOfString2, 0, j);
        }
        if (this.extras != null)
            localNotification.extras = new Bundle(this.extras);
        localNotification.actions = new Action[this.actions.length];
        for (int i = 0; i < this.actions.length; i++)
            localNotification.actions[i] = this.actions[i].clone();
        if (this.bigContentView != null)
            localNotification.bigContentView = this.bigContentView.clone();
        return localNotification;
    }

    public int describeContents()
    {
        return 0;
    }

    @Deprecated
    public void setLatestEventInfo(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, PendingIntent paramPendingIntent)
    {
        RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 17367153);
        if (this.icon != 0)
            localRemoteViews.setImageViewResource(16908294, this.icon);
        if (this.priority < -1)
        {
            localRemoteViews.setInt(16908294, "setBackgroundResource", 17303051);
            localRemoteViews.setInt(16909029, "setBackgroundResource", 17302539);
        }
        if (paramCharSequence1 != null)
            localRemoteViews.setTextViewText(16908310, paramCharSequence1);
        if (paramCharSequence2 != null)
            localRemoteViews.setTextViewText(16908358, paramCharSequence2);
        if (this.when != 0L)
        {
            localRemoteViews.setViewVisibility(16908388, 0);
            localRemoteViews.setLong(16908388, "setTime", this.when);
        }
        if (this.number != 0)
            localRemoteViews.setTextViewText(16909033, NumberFormat.getIntegerInstance().format(this.number));
        this.contentView = localRemoteViews;
        this.contentIntent = paramPendingIntent;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Notification(pri=");
        localStringBuilder.append(this.priority);
        localStringBuilder.append(" contentView=");
        if (this.contentView != null)
        {
            localStringBuilder.append(this.contentView.getPackage());
            localStringBuilder.append("/0x");
            localStringBuilder.append(Integer.toHexString(this.contentView.getLayoutId()));
        }
        int j;
        while (true)
        {
            localStringBuilder.append(" vibrate=");
            if (this.vibrate == null)
                break label342;
            j = -1 + this.vibrate.length;
            localStringBuilder.append("[");
            for (int k = 0; k < j; k++)
            {
                localStringBuilder.append(this.vibrate[k]);
                localStringBuilder.append(',');
            }
            localStringBuilder.append("null");
        }
        if (j != -1)
            localStringBuilder.append(this.vibrate[j]);
        localStringBuilder.append("]");
        localStringBuilder.append(" sound=");
        if (this.sound != null)
        {
            localStringBuilder.append(this.sound.toString());
            label208: localStringBuilder.append(" defaults=0x");
            localStringBuilder.append(Integer.toHexString(this.defaults));
            localStringBuilder.append(" flags=0x");
            localStringBuilder.append(Integer.toHexString(this.flags));
            localStringBuilder.append(" kind=[");
            if (this.kind != null)
                break label404;
            localStringBuilder.append("null");
        }
        while (true)
        {
            localStringBuilder.append("]");
            if (this.actions != null)
            {
                localStringBuilder.append(" ");
                localStringBuilder.append(this.actions.length);
                localStringBuilder.append(" action");
                if (this.actions.length > 1)
                    localStringBuilder.append("s");
            }
            localStringBuilder.append(")");
            return localStringBuilder.toString();
            label342: if ((0x2 & this.defaults) != 0)
            {
                localStringBuilder.append("default");
                break;
            }
            localStringBuilder.append("null");
            break;
            if ((0x1 & this.defaults) != 0)
            {
                localStringBuilder.append("default");
                break label208;
            }
            localStringBuilder.append("null");
            break label208;
            label404: for (int i = 0; i < this.kind.length; i++)
            {
                if (i > 0)
                    localStringBuilder.append(",");
                localStringBuilder.append(this.kind[i]);
            }
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(1);
        paramParcel.writeLong(this.when);
        paramParcel.writeInt(this.icon);
        paramParcel.writeInt(this.number);
        if (this.contentIntent != null)
        {
            paramParcel.writeInt(1);
            this.contentIntent.writeToParcel(paramParcel, 0);
            if (this.deleteIntent == null)
                break label337;
            paramParcel.writeInt(1);
            this.deleteIntent.writeToParcel(paramParcel, 0);
            label71: if (this.tickerText == null)
                break label345;
            paramParcel.writeInt(1);
            TextUtils.writeToParcel(this.tickerText, paramParcel, paramInt);
            label92: if (this.tickerView == null)
                break label353;
            paramParcel.writeInt(1);
            this.tickerView.writeToParcel(paramParcel, 0);
            label113: if (this.contentView == null)
                break label361;
            paramParcel.writeInt(1);
            this.contentView.writeToParcel(paramParcel, 0);
            label134: if (this.largeIcon == null)
                break label369;
            paramParcel.writeInt(1);
            this.largeIcon.writeToParcel(paramParcel, 0);
            label155: paramParcel.writeInt(this.defaults);
            paramParcel.writeInt(this.flags);
            if (this.sound == null)
                break label377;
            paramParcel.writeInt(1);
            this.sound.writeToParcel(paramParcel, 0);
            label192: paramParcel.writeInt(this.audioStreamType);
            paramParcel.writeLongArray(this.vibrate);
            paramParcel.writeInt(this.ledARGB);
            paramParcel.writeInt(this.ledOnMS);
            paramParcel.writeInt(this.ledOffMS);
            paramParcel.writeInt(this.iconLevel);
            if (this.fullScreenIntent == null)
                break label385;
            paramParcel.writeInt(1);
            this.fullScreenIntent.writeToParcel(paramParcel, 0);
            label261: paramParcel.writeInt(this.priority);
            paramParcel.writeStringArray(this.kind);
            if (this.extras == null)
                break label393;
            paramParcel.writeInt(1);
            this.extras.writeToParcel(paramParcel, 0);
            label298: paramParcel.writeTypedArray(this.actions, 0);
            if (this.bigContentView == null)
                break label401;
            paramParcel.writeInt(1);
            this.bigContentView.writeToParcel(paramParcel, 0);
        }
        while (true)
        {
            return;
            paramParcel.writeInt(0);
            break;
            label337: paramParcel.writeInt(0);
            break label71;
            label345: paramParcel.writeInt(0);
            break label92;
            label353: paramParcel.writeInt(0);
            break label113;
            label361: paramParcel.writeInt(0);
            break label134;
            label369: paramParcel.writeInt(0);
            break label155;
            label377: paramParcel.writeInt(0);
            break label192;
            label385: paramParcel.writeInt(0);
            break label261;
            label393: paramParcel.writeInt(0);
            break label298;
            label401: paramParcel.writeInt(0);
        }
    }

    public static class InboxStyle extends Notification.Style
    {
        private ArrayList<CharSequence> mTexts = new ArrayList(5);

        public InboxStyle()
        {
        }

        public InboxStyle(Notification.Builder paramBuilder)
        {
            setBuilder(paramBuilder);
        }

        private RemoteViews makeBigContentView()
        {
            Notification.Builder.access$702(this.mBuilder, null);
            RemoteViews localRemoteViews = getStandardView(17367157);
            localRemoteViews.setViewVisibility(16908309, 8);
            int[] arrayOfInt = new int[7];
            arrayOfInt[0] = 16909043;
            arrayOfInt[1] = 16909044;
            arrayOfInt[2] = 16909045;
            arrayOfInt[3] = 16909046;
            arrayOfInt[4] = 16909047;
            arrayOfInt[5] = 16909048;
            arrayOfInt[6] = 16909049;
            int i = arrayOfInt.length;
            for (int j = 0; j < i; j++)
                localRemoteViews.setViewVisibility(arrayOfInt[j], 8);
            for (int k = 0; (k < this.mTexts.size()) && (k < arrayOfInt.length); k++)
            {
                CharSequence localCharSequence = (CharSequence)this.mTexts.get(k);
                if ((localCharSequence != null) && (!localCharSequence.equals("")))
                {
                    localRemoteViews.setViewVisibility(arrayOfInt[k], 0);
                    localRemoteViews.setTextViewText(arrayOfInt[k], localCharSequence);
                }
            }
            if (this.mTexts.size() > arrayOfInt.length)
                localRemoteViews.setViewVisibility(16909050, 0);
            while (true)
            {
                return localRemoteViews;
                localRemoteViews.setViewVisibility(16909050, 8);
            }
        }

        public InboxStyle addLine(CharSequence paramCharSequence)
        {
            this.mTexts.add(paramCharSequence);
            return this;
        }

        public Notification build()
        {
            checkBuilder();
            Notification localNotification = Notification.Builder.access$500(this.mBuilder);
            localNotification.bigContentView = makeBigContentView();
            return localNotification;
        }

        public InboxStyle setBigContentTitle(CharSequence paramCharSequence)
        {
            internalSetBigContentTitle(paramCharSequence);
            return this;
        }

        public InboxStyle setSummaryText(CharSequence paramCharSequence)
        {
            internalSetSummaryText(paramCharSequence);
            return this;
        }
    }

    public static class BigTextStyle extends Notification.Style
    {
        private CharSequence mBigText;

        public BigTextStyle()
        {
        }

        public BigTextStyle(Notification.Builder paramBuilder)
        {
            setBuilder(paramBuilder);
        }

        private RemoteViews makeBigContentView()
        {
            if ((Notification.Builder.access$700(this.mBuilder) != null) && (Notification.Builder.access$400(this.mBuilder) != null));
            for (int i = 1; ; i = 0)
            {
                Notification.Builder.access$702(this.mBuilder, null);
                RemoteViews localRemoteViews = getStandardView(17367156);
                if (i != 0)
                    localRemoteViews.setViewPadding(16909030, 0, 0, 0, 0);
                localRemoteViews.setTextViewText(16909035, this.mBigText);
                localRemoteViews.setViewVisibility(16909035, 0);
                localRemoteViews.setViewVisibility(16908309, 8);
                return localRemoteViews;
            }
        }

        public BigTextStyle bigText(CharSequence paramCharSequence)
        {
            this.mBigText = paramCharSequence;
            return this;
        }

        public Notification build()
        {
            checkBuilder();
            Notification localNotification = Notification.Builder.access$500(this.mBuilder);
            localNotification.bigContentView = makeBigContentView();
            return localNotification;
        }

        public BigTextStyle setBigContentTitle(CharSequence paramCharSequence)
        {
            internalSetBigContentTitle(paramCharSequence);
            return this;
        }

        public BigTextStyle setSummaryText(CharSequence paramCharSequence)
        {
            internalSetSummaryText(paramCharSequence);
            return this;
        }
    }

    public static class BigPictureStyle extends Notification.Style
    {
        private Bitmap mBigLargeIcon;
        private boolean mBigLargeIconSet = false;
        private Bitmap mPicture;

        public BigPictureStyle()
        {
        }

        public BigPictureStyle(Notification.Builder paramBuilder)
        {
            setBuilder(paramBuilder);
        }

        private RemoteViews makeBigContentView()
        {
            RemoteViews localRemoteViews = getStandardView(17367155);
            localRemoteViews.setImageViewBitmap(16909037, this.mPicture);
            return localRemoteViews;
        }

        public BigPictureStyle bigLargeIcon(Bitmap paramBitmap)
        {
            this.mBigLargeIconSet = true;
            this.mBigLargeIcon = paramBitmap;
            return this;
        }

        public BigPictureStyle bigPicture(Bitmap paramBitmap)
        {
            this.mPicture = paramBitmap;
            return this;
        }

        public Notification build()
        {
            checkBuilder();
            Notification localNotification = Notification.Builder.access$500(this.mBuilder);
            if (this.mBigLargeIconSet)
                Notification.Builder.access$602(this.mBuilder, this.mBigLargeIcon);
            localNotification.bigContentView = makeBigContentView();
            return localNotification;
        }

        public BigPictureStyle setBigContentTitle(CharSequence paramCharSequence)
        {
            internalSetBigContentTitle(paramCharSequence);
            return this;
        }

        public BigPictureStyle setSummaryText(CharSequence paramCharSequence)
        {
            internalSetSummaryText(paramCharSequence);
            return this;
        }
    }

    public static abstract class Style
    {
        private CharSequence mBigContentTitle;
        protected Notification.Builder mBuilder;
        private CharSequence mSummaryText = null;
        private boolean mSummaryTextSet = false;

        public abstract Notification build();

        protected void checkBuilder()
        {
            if (this.mBuilder == null)
                throw new IllegalArgumentException("Style requires a valid Builder object");
        }

        protected RemoteViews getStandardView(int paramInt)
        {
            checkBuilder();
            if (this.mBigContentTitle != null)
                this.mBuilder.setContentTitle(this.mBigContentTitle);
            RemoteViews localRemoteViews = Notification.Builder.access$300(this.mBuilder, paramInt);
            CharSequence localCharSequence;
            if ((this.mBigContentTitle != null) && (this.mBigContentTitle.equals("")))
            {
                localRemoteViews.setViewVisibility(16909030, 8);
                if (!this.mSummaryTextSet)
                    break label108;
                localCharSequence = this.mSummaryText;
                label71: if (localCharSequence == null)
                    break label119;
                localRemoteViews.setTextViewText(16908358, localCharSequence);
                localRemoteViews.setViewVisibility(16909041, 0);
                localRemoteViews.setViewVisibility(16909032, 0);
            }
            while (true)
            {
                return localRemoteViews;
                localRemoteViews.setViewVisibility(16909030, 0);
                break;
                label108: localCharSequence = Notification.Builder.access$400(this.mBuilder);
                break label71;
                label119: localRemoteViews.setViewVisibility(16909041, 8);
                localRemoteViews.setViewVisibility(16909032, 8);
            }
        }

        protected void internalSetBigContentTitle(CharSequence paramCharSequence)
        {
            this.mBigContentTitle = paramCharSequence;
        }

        protected void internalSetSummaryText(CharSequence paramCharSequence)
        {
            this.mSummaryText = paramCharSequence;
            this.mSummaryTextSet = true;
        }

        public void setBuilder(Notification.Builder paramBuilder)
        {
            if (this.mBuilder != paramBuilder)
            {
                this.mBuilder = paramBuilder;
                if (this.mBuilder != null)
                    this.mBuilder.setStyle(this);
            }
        }
    }

    public static class Builder
    {
        private static final int MAX_ACTION_BUTTONS = 3;
        private ArrayList<Notification.Action> mActions = new ArrayList(3);
        private int mAudioStreamType;
        private CharSequence mContentInfo;
        private PendingIntent mContentIntent;
        private CharSequence mContentText;
        private CharSequence mContentTitle;
        private RemoteViews mContentView;
        private Context mContext;
        private int mDefaults;
        private PendingIntent mDeleteIntent;
        private Bundle mExtras;
        private int mFlags;
        private PendingIntent mFullScreenIntent;
        private ArrayList<String> mKindList = new ArrayList(1);
        private Bitmap mLargeIcon;
        private int mLedArgb;
        private int mLedOffMs;
        private int mLedOnMs;
        private int mNumber;
        private int mPriority;
        private int mProgress;
        private boolean mProgressIndeterminate;
        private int mProgressMax;
        private int mSmallIcon;
        private int mSmallIconLevel;
        private Uri mSound;
        private Notification.Style mStyle;
        private CharSequence mSubText;
        private CharSequence mTickerText;
        private RemoteViews mTickerView;
        private boolean mUseChronometer;
        private long[] mVibrate;
        private long mWhen;

        public Builder(Context paramContext)
        {
            this.mContext = paramContext;
            this.mWhen = System.currentTimeMillis();
            this.mAudioStreamType = -1;
            this.mPriority = 0;
        }

        private RemoteViews applyStandardTemplate(int paramInt, boolean paramBoolean)
        {
            RemoteViews localRemoteViews = new RemoteViews(this.mContext.getPackageName(), paramInt);
            int i = 0;
            int j = 0;
            int k = 16908294;
            if (this.mLargeIcon != null)
            {
                localRemoteViews.setImageViewBitmap(16908294, this.mLargeIcon);
                k = 16908852;
            }
            if (this.mPriority < -1)
            {
                localRemoteViews.setInt(16908294, "setBackgroundResource", 17303051);
                localRemoteViews.setInt(16909029, "setBackgroundResource", 17302539);
            }
            label164: label208: int m;
            if (this.mSmallIcon != 0)
            {
                localRemoteViews.setImageViewResource(k, this.mSmallIcon);
                localRemoteViews.setViewVisibility(k, 0);
                if (this.mContentTitle != null)
                    localRemoteViews.setTextViewText(16908310, this.mContentTitle);
                if (this.mContentText != null)
                {
                    localRemoteViews.setTextViewText(16908358, this.mContentText);
                    i = 1;
                }
                if (this.mContentInfo == null)
                    break label344;
                localRemoteViews.setTextViewText(16909033, this.mContentInfo);
                localRemoteViews.setViewVisibility(16909033, 0);
                i = 1;
                if (this.mSubText == null)
                    break label447;
                localRemoteViews.setTextViewText(16908358, this.mSubText);
                if (this.mContentText == null)
                    break label436;
                localRemoteViews.setTextViewText(16908309, this.mContentText);
                localRemoteViews.setViewVisibility(16908309, 0);
                j = 1;
                if (j != 0)
                {
                    if (paramBoolean)
                        localRemoteViews.setTextViewTextSize(16908358, 0, this.mContext.getResources().getDimensionPixelSize(17104980));
                    localRemoteViews.setViewPadding(16909030, 0, 0, 0, 0);
                }
                if (this.mWhen != 0L)
                {
                    if (!this.mUseChronometer)
                        break label511;
                    localRemoteViews.setViewVisibility(16909031, 0);
                    localRemoteViews.setLong(16909031, "setBase", this.mWhen + (SystemClock.elapsedRealtime() - System.currentTimeMillis()));
                    localRemoteViews.setBoolean(16909031, "setStarted", true);
                }
                label299: if (i == 0)
                    break label533;
                m = 0;
                label307: localRemoteViews.setViewVisibility(16909032, m);
                if (i == 0)
                    break label540;
            }
            label533: label540: for (int n = 0; ; n = 8)
            {
                localRemoteViews.setViewVisibility(16909041, n);
                return localRemoteViews;
                localRemoteViews.setViewVisibility(k, 8);
                break;
                label344: if (this.mNumber > 0)
                {
                    int i1 = this.mContext.getResources().getInteger(17694723);
                    if (this.mNumber > i1)
                        localRemoteViews.setTextViewText(16909033, this.mContext.getResources().getString(17039383));
                    while (true)
                    {
                        localRemoteViews.setViewVisibility(16909033, 0);
                        i = 1;
                        break;
                        localRemoteViews.setTextViewText(16909033, NumberFormat.getIntegerInstance().format(this.mNumber));
                    }
                }
                localRemoteViews.setViewVisibility(16909033, 8);
                break label164;
                label436: localRemoteViews.setViewVisibility(16908309, 8);
                break label208;
                label447: localRemoteViews.setViewVisibility(16908309, 8);
                if ((this.mProgressMax != 0) || (this.mProgressIndeterminate))
                {
                    localRemoteViews.setProgressBar(16908301, this.mProgressMax, this.mProgress, this.mProgressIndeterminate);
                    localRemoteViews.setViewVisibility(16908301, 0);
                    j = 1;
                    break label208;
                }
                localRemoteViews.setViewVisibility(16908301, 8);
                break label208;
                label511: localRemoteViews.setViewVisibility(16908388, 0);
                localRemoteViews.setLong(16908388, "setTime", this.mWhen);
                break label299;
                m = 8;
                break label307;
            }
        }

        private RemoteViews applyStandardTemplateWithActions(int paramInt)
        {
            RemoteViews localRemoteViews = applyStandardTemplate(paramInt, false);
            int i = this.mActions.size();
            if (i > 0)
            {
                localRemoteViews.setViewVisibility(16909026, 0);
                localRemoteViews.setViewVisibility(16909036, 0);
                if (i > 3)
                    i = 3;
                localRemoteViews.removeAllViews(16909026);
                for (int j = 0; j < i; j++)
                    localRemoteViews.addView(16909026, generateActionButton((Notification.Action)this.mActions.get(j)));
            }
            return localRemoteViews;
        }

        private Notification buildUnstyled()
        {
            Bundle localBundle = null;
            Notification localNotification = new Notification();
            localNotification.when = this.mWhen;
            localNotification.icon = this.mSmallIcon;
            localNotification.iconLevel = this.mSmallIconLevel;
            localNotification.number = this.mNumber;
            localNotification.contentView = makeContentView();
            localNotification.contentIntent = this.mContentIntent;
            localNotification.deleteIntent = this.mDeleteIntent;
            localNotification.fullScreenIntent = this.mFullScreenIntent;
            localNotification.tickerText = this.mTickerText;
            localNotification.tickerView = makeTickerView();
            localNotification.largeIcon = this.mLargeIcon;
            localNotification.sound = this.mSound;
            localNotification.audioStreamType = this.mAudioStreamType;
            localNotification.vibrate = this.mVibrate;
            localNotification.ledARGB = this.mLedArgb;
            localNotification.ledOnMS = this.mLedOnMs;
            localNotification.ledOffMS = this.mLedOffMs;
            localNotification.defaults = this.mDefaults;
            localNotification.flags = this.mFlags;
            localNotification.bigContentView = makeBigContentView();
            if ((this.mLedOnMs != 0) && (this.mLedOffMs != 0))
                localNotification.flags = (0x1 | localNotification.flags);
            if ((0x4 & this.mDefaults) != 0)
                localNotification.flags = (0x1 | localNotification.flags);
            if (this.mKindList.size() > 0)
            {
                localNotification.kind = new String[this.mKindList.size()];
                this.mKindList.toArray(localNotification.kind);
            }
            while (true)
            {
                localNotification.priority = this.mPriority;
                if (this.mExtras != null)
                    localBundle = new Bundle(this.mExtras);
                Notification.access$102(localNotification, localBundle);
                if (this.mActions.size() > 0)
                {
                    Notification.access$202(localNotification, new Notification.Action[this.mActions.size()]);
                    this.mActions.toArray(localNotification.actions);
                }
                return localNotification;
                localNotification.kind = null;
            }
        }

        private RemoteViews generateActionButton(Notification.Action paramAction)
        {
            int i;
            String str;
            if (paramAction.actionIntent == null)
            {
                i = 1;
                str = this.mContext.getPackageName();
                if (i == 0)
                    break label101;
            }
            label101: for (int j = 17367151; ; j = 17367149)
            {
                RemoteViews localRemoteViews = new RemoteViews(str, j);
                localRemoteViews.setTextViewCompoundDrawables(16909025, paramAction.icon, 0, 0, 0);
                localRemoteViews.setTextViewText(16909025, paramAction.title);
                if (i == 0)
                    localRemoteViews.setOnClickPendingIntent(16909025, paramAction.actionIntent);
                localRemoteViews.setContentDescription(16909025, paramAction.title);
                return localRemoteViews;
                i = 0;
                break;
            }
        }

        private RemoteViews makeBigContentView()
        {
            if (this.mActions.size() == 0);
            for (RemoteViews localRemoteViews = null; ; localRemoteViews = applyStandardTemplateWithActions(17367154))
                return localRemoteViews;
        }

        private RemoteViews makeContentView()
        {
            if (this.mContentView != null);
            for (RemoteViews localRemoteViews = this.mContentView; ; localRemoteViews = applyStandardTemplate(17367153, true))
                return localRemoteViews;
        }

        private RemoteViews makeTickerView()
        {
            RemoteViews localRemoteViews;
            if (this.mTickerView != null)
                localRemoteViews = this.mTickerView;
            while (true)
            {
                return localRemoteViews;
                if (this.mContentView == null)
                {
                    if (this.mLargeIcon == null);
                    for (int i = 17367214; ; i = 17367215)
                    {
                        localRemoteViews = applyStandardTemplate(i, true);
                        break;
                    }
                }
                localRemoteViews = null;
            }
        }

        private void setFlag(int paramInt, boolean paramBoolean)
        {
            if (paramBoolean);
            for (this.mFlags = (paramInt | this.mFlags); ; this.mFlags &= (paramInt ^ 0xFFFFFFFF))
                return;
        }

        public Builder addAction(int paramInt, CharSequence paramCharSequence, PendingIntent paramPendingIntent)
        {
            this.mActions.add(new Notification.Action(paramInt, paramCharSequence, paramPendingIntent));
            return this;
        }

        public Builder addKind(String paramString)
        {
            this.mKindList.add(paramString);
            return this;
        }

        public Notification build()
        {
            if (this.mStyle != null);
            for (Notification localNotification = this.mStyle.build(); ; localNotification = buildUnstyled())
                return localNotification;
        }

        @Deprecated
        public Notification getNotification()
        {
            return build();
        }

        public Builder setAutoCancel(boolean paramBoolean)
        {
            setFlag(16, paramBoolean);
            return this;
        }

        public Builder setContent(RemoteViews paramRemoteViews)
        {
            this.mContentView = paramRemoteViews;
            return this;
        }

        public Builder setContentInfo(CharSequence paramCharSequence)
        {
            this.mContentInfo = paramCharSequence;
            return this;
        }

        public Builder setContentIntent(PendingIntent paramPendingIntent)
        {
            this.mContentIntent = paramPendingIntent;
            return this;
        }

        public Builder setContentText(CharSequence paramCharSequence)
        {
            this.mContentText = paramCharSequence;
            return this;
        }

        public Builder setContentTitle(CharSequence paramCharSequence)
        {
            this.mContentTitle = paramCharSequence;
            return this;
        }

        public Builder setDefaults(int paramInt)
        {
            this.mDefaults = paramInt;
            return this;
        }

        public Builder setDeleteIntent(PendingIntent paramPendingIntent)
        {
            this.mDeleteIntent = paramPendingIntent;
            return this;
        }

        public Builder setExtras(Bundle paramBundle)
        {
            this.mExtras = paramBundle;
            return this;
        }

        public Builder setFullScreenIntent(PendingIntent paramPendingIntent, boolean paramBoolean)
        {
            this.mFullScreenIntent = paramPendingIntent;
            setFlag(128, paramBoolean);
            return this;
        }

        public Builder setLargeIcon(Bitmap paramBitmap)
        {
            this.mLargeIcon = paramBitmap;
            return this;
        }

        public Builder setLights(int paramInt1, int paramInt2, int paramInt3)
        {
            this.mLedArgb = paramInt1;
            this.mLedOnMs = paramInt2;
            this.mLedOffMs = paramInt3;
            return this;
        }

        public Builder setNumber(int paramInt)
        {
            this.mNumber = paramInt;
            return this;
        }

        public Builder setOngoing(boolean paramBoolean)
        {
            setFlag(2, paramBoolean);
            return this;
        }

        public Builder setOnlyAlertOnce(boolean paramBoolean)
        {
            setFlag(8, paramBoolean);
            return this;
        }

        public Builder setPriority(int paramInt)
        {
            this.mPriority = paramInt;
            return this;
        }

        public Builder setProgress(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            this.mProgressMax = paramInt1;
            this.mProgress = paramInt2;
            this.mProgressIndeterminate = paramBoolean;
            return this;
        }

        public Builder setSmallIcon(int paramInt)
        {
            this.mSmallIcon = paramInt;
            return this;
        }

        public Builder setSmallIcon(int paramInt1, int paramInt2)
        {
            this.mSmallIcon = paramInt1;
            this.mSmallIconLevel = paramInt2;
            return this;
        }

        public Builder setSound(Uri paramUri)
        {
            this.mSound = paramUri;
            this.mAudioStreamType = -1;
            return this;
        }

        public Builder setSound(Uri paramUri, int paramInt)
        {
            this.mSound = paramUri;
            this.mAudioStreamType = paramInt;
            return this;
        }

        public Builder setStyle(Notification.Style paramStyle)
        {
            if (this.mStyle != paramStyle)
            {
                this.mStyle = paramStyle;
                if (this.mStyle != null)
                    this.mStyle.setBuilder(this);
            }
            return this;
        }

        public Builder setSubText(CharSequence paramCharSequence)
        {
            this.mSubText = paramCharSequence;
            return this;
        }

        public Builder setTicker(CharSequence paramCharSequence)
        {
            this.mTickerText = paramCharSequence;
            return this;
        }

        public Builder setTicker(CharSequence paramCharSequence, RemoteViews paramRemoteViews)
        {
            this.mTickerText = paramCharSequence;
            this.mTickerView = paramRemoteViews;
            return this;
        }

        public Builder setUsesChronometer(boolean paramBoolean)
        {
            this.mUseChronometer = paramBoolean;
            return this;
        }

        public Builder setVibrate(long[] paramArrayOfLong)
        {
            this.mVibrate = paramArrayOfLong;
            return this;
        }

        public Builder setWhen(long paramLong)
        {
            this.mWhen = paramLong;
            return this;
        }
    }

    private static class Action
        implements Parcelable
    {
        public static final Parcelable.Creator<Action> CREATOR = new Parcelable.Creator()
        {
            public Notification.Action createFromParcel(Parcel paramAnonymousParcel)
            {
                return new Notification.Action(paramAnonymousParcel, null);
            }

            public Notification.Action[] newArray(int paramAnonymousInt)
            {
                return new Notification.Action[paramAnonymousInt];
            }
        };
        public PendingIntent actionIntent;
        public int icon;
        public CharSequence title;

        public Action()
        {
        }

        public Action(int paramInt, CharSequence paramCharSequence, PendingIntent paramPendingIntent)
        {
            this.icon = paramInt;
            this.title = paramCharSequence;
            this.actionIntent = paramPendingIntent;
        }

        private Action(Parcel paramParcel)
        {
            this.icon = paramParcel.readInt();
            this.title = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            if (paramParcel.readInt() == 1)
                this.actionIntent = ((PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel));
        }

        public Action clone()
        {
            return new Action(this.icon, this.title.toString(), this.actionIntent);
        }

        public int describeContents()
        {
            return 0;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.icon);
            TextUtils.writeToParcel(this.title, paramParcel, paramInt);
            if (this.actionIntent != null)
            {
                paramParcel.writeInt(1);
                this.actionIntent.writeToParcel(paramParcel, paramInt);
            }
            while (true)
            {
                return;
                paramParcel.writeInt(0);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Notification
 * JD-Core Version:        0.6.2
 */