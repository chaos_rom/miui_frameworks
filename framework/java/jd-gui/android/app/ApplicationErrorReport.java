package android.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.Printer;
import java.io.PrintWriter;
import java.io.StringWriter;

public class ApplicationErrorReport
    implements Parcelable
{
    public static final Parcelable.Creator<ApplicationErrorReport> CREATOR = new Parcelable.Creator()
    {
        public ApplicationErrorReport createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ApplicationErrorReport(paramAnonymousParcel);
        }

        public ApplicationErrorReport[] newArray(int paramAnonymousInt)
        {
            return new ApplicationErrorReport[paramAnonymousInt];
        }
    };
    static final String DEFAULT_ERROR_RECEIVER_PROPERTY = "ro.error.receiver.default";
    static final String SYSTEM_APPS_ERROR_RECEIVER_PROPERTY = "ro.error.receiver.system.apps";
    public static final int TYPE_ANR = 2;
    public static final int TYPE_BATTERY = 3;
    public static final int TYPE_CRASH = 1;
    public static final int TYPE_NONE = 0;
    public static final int TYPE_RUNNING_SERVICE = 5;
    public AnrInfo anrInfo;
    public BatteryInfo batteryInfo;
    public CrashInfo crashInfo;
    public String installerPackageName;
    public String packageName;
    public String processName;
    public RunningServiceInfo runningServiceInfo;
    public boolean systemApp;
    public long time;
    public int type;

    public ApplicationErrorReport()
    {
    }

    ApplicationErrorReport(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    public static ComponentName getErrorReportReceiver(Context paramContext, String paramString, int paramInt)
    {
        ComponentName localComponentName;
        if (Settings.Secure.getInt(paramContext.getContentResolver(), "send_action_app_error", 0) == 0)
            localComponentName = null;
        while (true)
        {
            return localComponentName;
            PackageManager localPackageManager = paramContext.getPackageManager();
            localComponentName = getErrorReportReceiver(localPackageManager, paramString, localPackageManager.getInstallerPackageName(paramString));
            if (localComponentName == null)
                if ((paramInt & 0x1) != 0)
                {
                    localComponentName = getErrorReportReceiver(localPackageManager, paramString, SystemProperties.get("ro.error.receiver.system.apps"));
                    if (localComponentName != null);
                }
                else
                {
                    localComponentName = getErrorReportReceiver(localPackageManager, paramString, SystemProperties.get("ro.error.receiver.default"));
                }
        }
    }

    static ComponentName getErrorReportReceiver(PackageManager paramPackageManager, String paramString1, String paramString2)
    {
        ComponentName localComponentName = null;
        if ((paramString2 == null) || (paramString2.length() == 0));
        while (true)
        {
            return localComponentName;
            if (!paramString2.equals(paramString1))
            {
                Intent localIntent = new Intent("android.intent.action.APP_ERROR");
                localIntent.setPackage(paramString2);
                ResolveInfo localResolveInfo = paramPackageManager.resolveActivity(localIntent, 0);
                if ((localResolveInfo != null) && (localResolveInfo.activityInfo != null))
                    localComponentName = new ComponentName(paramString2, localResolveInfo.activityInfo.name);
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + "type: " + this.type);
        paramPrinter.println(paramString + "packageName: " + this.packageName);
        paramPrinter.println(paramString + "installerPackageName: " + this.installerPackageName);
        paramPrinter.println(paramString + "processName: " + this.processName);
        paramPrinter.println(paramString + "time: " + this.time);
        paramPrinter.println(paramString + "systemApp: " + this.systemApp);
        switch (this.type)
        {
        case 4:
        default:
        case 1:
        case 2:
        case 3:
        case 5:
        }
        while (true)
        {
            return;
            this.crashInfo.dump(paramPrinter, paramString);
            continue;
            this.anrInfo.dump(paramPrinter, paramString);
            continue;
            this.batteryInfo.dump(paramPrinter, paramString);
            continue;
            this.runningServiceInfo.dump(paramPrinter, paramString);
        }
    }

    public void readFromParcel(Parcel paramParcel)
    {
        int i = 1;
        this.type = paramParcel.readInt();
        this.packageName = paramParcel.readString();
        this.installerPackageName = paramParcel.readString();
        this.processName = paramParcel.readString();
        this.time = paramParcel.readLong();
        if (paramParcel.readInt() == i)
        {
            this.systemApp = i;
            switch (this.type)
            {
            case 4:
            default:
            case 1:
            case 2:
            case 3:
            case 5:
            }
        }
        while (true)
        {
            return;
            i = 0;
            break;
            this.crashInfo = new CrashInfo(paramParcel);
            this.anrInfo = null;
            this.batteryInfo = null;
            this.runningServiceInfo = null;
            continue;
            this.anrInfo = new AnrInfo(paramParcel);
            this.crashInfo = null;
            this.batteryInfo = null;
            this.runningServiceInfo = null;
            continue;
            this.batteryInfo = new BatteryInfo(paramParcel);
            this.anrInfo = null;
            this.crashInfo = null;
            this.runningServiceInfo = null;
            continue;
            this.batteryInfo = null;
            this.anrInfo = null;
            this.crashInfo = null;
            this.runningServiceInfo = new RunningServiceInfo(paramParcel);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.type);
        paramParcel.writeString(this.packageName);
        paramParcel.writeString(this.installerPackageName);
        paramParcel.writeString(this.processName);
        paramParcel.writeLong(this.time);
        int i;
        if (this.systemApp)
        {
            i = 1;
            paramParcel.writeInt(i);
            switch (this.type)
            {
            case 4:
            default:
            case 1:
            case 2:
            case 3:
            case 5:
            }
        }
        while (true)
        {
            return;
            i = 0;
            break;
            this.crashInfo.writeToParcel(paramParcel, paramInt);
            continue;
            this.anrInfo.writeToParcel(paramParcel, paramInt);
            continue;
            this.batteryInfo.writeToParcel(paramParcel, paramInt);
            continue;
            this.runningServiceInfo.writeToParcel(paramParcel, paramInt);
        }
    }

    public static class RunningServiceInfo
    {
        public long durationMillis;
        public String serviceDetails;

        public RunningServiceInfo()
        {
        }

        public RunningServiceInfo(Parcel paramParcel)
        {
            this.durationMillis = paramParcel.readLong();
            this.serviceDetails = paramParcel.readString();
        }

        public void dump(Printer paramPrinter, String paramString)
        {
            paramPrinter.println(paramString + "durationMillis: " + this.durationMillis);
            paramPrinter.println(paramString + "serviceDetails: " + this.serviceDetails);
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeLong(this.durationMillis);
            paramParcel.writeString(this.serviceDetails);
        }
    }

    public static class BatteryInfo
    {
        public String checkinDetails;
        public long durationMicros;
        public String usageDetails;
        public int usagePercent;

        public BatteryInfo()
        {
        }

        public BatteryInfo(Parcel paramParcel)
        {
            this.usagePercent = paramParcel.readInt();
            this.durationMicros = paramParcel.readLong();
            this.usageDetails = paramParcel.readString();
            this.checkinDetails = paramParcel.readString();
        }

        public void dump(Printer paramPrinter, String paramString)
        {
            paramPrinter.println(paramString + "usagePercent: " + this.usagePercent);
            paramPrinter.println(paramString + "durationMicros: " + this.durationMicros);
            paramPrinter.println(paramString + "usageDetails: " + this.usageDetails);
            paramPrinter.println(paramString + "checkinDetails: " + this.checkinDetails);
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.usagePercent);
            paramParcel.writeLong(this.durationMicros);
            paramParcel.writeString(this.usageDetails);
            paramParcel.writeString(this.checkinDetails);
        }
    }

    public static class AnrInfo
    {
        public String activity;
        public String cause;
        public String info;

        public AnrInfo()
        {
        }

        public AnrInfo(Parcel paramParcel)
        {
            this.activity = paramParcel.readString();
            this.cause = paramParcel.readString();
            this.info = paramParcel.readString();
        }

        public void dump(Printer paramPrinter, String paramString)
        {
            paramPrinter.println(paramString + "activity: " + this.activity);
            paramPrinter.println(paramString + "cause: " + this.cause);
            paramPrinter.println(paramString + "info: " + this.info);
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeString(this.activity);
            paramParcel.writeString(this.cause);
            paramParcel.writeString(this.info);
        }
    }

    public static class CrashInfo
    {
        public String exceptionClassName;
        public String exceptionMessage;
        public String stackTrace;
        public String throwClassName;
        public String throwFileName;
        public int throwLineNumber;
        public String throwMethodName;

        public CrashInfo()
        {
        }

        public CrashInfo(Parcel paramParcel)
        {
            this.exceptionClassName = paramParcel.readString();
            this.exceptionMessage = paramParcel.readString();
            this.throwFileName = paramParcel.readString();
            this.throwClassName = paramParcel.readString();
            this.throwMethodName = paramParcel.readString();
            this.throwLineNumber = paramParcel.readInt();
            this.stackTrace = paramParcel.readString();
        }

        public CrashInfo(Throwable paramThrowable)
        {
            StringWriter localStringWriter = new StringWriter();
            paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
            this.stackTrace = localStringWriter.toString();
            this.exceptionMessage = paramThrowable.getMessage();
            Throwable localThrowable = paramThrowable;
            while (paramThrowable.getCause() != null)
            {
                paramThrowable = paramThrowable.getCause();
                if ((paramThrowable.getStackTrace() != null) && (paramThrowable.getStackTrace().length > 0))
                    localThrowable = paramThrowable;
                String str = paramThrowable.getMessage();
                if ((str != null) && (str.length() > 0))
                    this.exceptionMessage = str;
            }
            this.exceptionClassName = localThrowable.getClass().getName();
            StackTraceElement localStackTraceElement;
            if (localThrowable.getStackTrace().length > 0)
            {
                localStackTraceElement = localThrowable.getStackTrace()[0];
                this.throwFileName = localStackTraceElement.getFileName();
                this.throwClassName = localStackTraceElement.getClassName();
                this.throwMethodName = localStackTraceElement.getMethodName();
            }
            for (this.throwLineNumber = localStackTraceElement.getLineNumber(); ; this.throwLineNumber = 0)
            {
                return;
                this.throwFileName = "unknown";
                this.throwClassName = "unknown";
                this.throwMethodName = "unknown";
            }
        }

        public void dump(Printer paramPrinter, String paramString)
        {
            paramPrinter.println(paramString + "exceptionClassName: " + this.exceptionClassName);
            paramPrinter.println(paramString + "exceptionMessage: " + this.exceptionMessage);
            paramPrinter.println(paramString + "throwFileName: " + this.throwFileName);
            paramPrinter.println(paramString + "throwClassName: " + this.throwClassName);
            paramPrinter.println(paramString + "throwMethodName: " + this.throwMethodName);
            paramPrinter.println(paramString + "throwLineNumber: " + this.throwLineNumber);
            paramPrinter.println(paramString + "stackTrace: " + this.stackTrace);
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeString(this.exceptionClassName);
            paramParcel.writeString(this.exceptionMessage);
            paramParcel.writeString(this.throwFileName);
            paramParcel.writeString(this.throwClassName);
            paramParcel.writeString(this.throwMethodName);
            paramParcel.writeInt(this.throwLineNumber);
            paramParcel.writeString(this.stackTrace);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ApplicationErrorReport
 * JD-Core Version:        0.6.2
 */