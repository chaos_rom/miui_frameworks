package android.app;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.FileUtils;
import android.os.FileUtils.FileStatus;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.android.internal.util.XmlUtils;
import com.google.android.collect.Maps;
import dalvik.system.BlockGuard;
import dalvik.system.BlockGuard.Policy;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import org.xmlpull.v1.XmlPullParserException;

final class SharedPreferencesImpl
    implements SharedPreferences
{
    private static final boolean DEBUG = false;
    private static final String TAG = "SharedPreferencesImpl";
    private static final Object mContent = new Object();
    private final File mBackupFile;
    private int mDiskWritesInFlight = 0;
    private final File mFile;
    private final WeakHashMap<SharedPreferences.OnSharedPreferenceChangeListener, Object> mListeners = new WeakHashMap();
    private boolean mLoaded = false;
    private Map<String, Object> mMap;
    private final int mMode;
    private long mStatSize;
    private long mStatTimestamp;
    private final Object mWritingToDiskLock = new Object();

    SharedPreferencesImpl(File paramFile, int paramInt)
    {
        this.mFile = paramFile;
        this.mBackupFile = makeBackupFile(paramFile);
        this.mMode = paramInt;
        this.mLoaded = false;
        this.mMap = null;
        startLoadFromDisk();
    }

    private void awaitLoadedLocked()
    {
        if (!this.mLoaded)
            BlockGuard.getThreadPolicy().onReadFromDisk();
        while (!this.mLoaded)
            try
            {
                wait();
            }
            catch (InterruptedException localInterruptedException)
            {
            }
    }

    private static FileOutputStream createFileOutputStream(File paramFile)
    {
        Object localObject1 = null;
        try
        {
            FileOutputStream localFileOutputStream1 = new FileOutputStream(paramFile);
            localObject1 = localFileOutputStream1;
            localObject2 = localObject1;
            return localObject2;
        }
        catch (FileNotFoundException localFileNotFoundException1)
        {
            while (true)
            {
                Object localObject2;
                File localFile = paramFile.getParentFile();
                if (!localFile.mkdir())
                {
                    Log.e("SharedPreferencesImpl", "Couldn't create directory for SharedPreferences file " + paramFile);
                    localObject2 = null;
                }
                else
                {
                    FileUtils.setPermissions(localFile.getPath(), 505, -1, -1);
                    try
                    {
                        FileOutputStream localFileOutputStream2 = new FileOutputStream(paramFile);
                        localObject1 = localFileOutputStream2;
                    }
                    catch (FileNotFoundException localFileNotFoundException2)
                    {
                        Log.e("SharedPreferencesImpl", "Couldn't create SharedPreferences file " + paramFile, localFileNotFoundException2);
                    }
                }
            }
        }
    }

    private void enqueueDiskWrite(final MemoryCommitResult paramMemoryCommitResult, final Runnable paramRunnable)
    {
        Runnable local2 = new Runnable()
        {
            public void run()
            {
                synchronized (SharedPreferencesImpl.this.mWritingToDiskLock)
                {
                    SharedPreferencesImpl.this.writeToFile(paramMemoryCommitResult);
                }
                synchronized (SharedPreferencesImpl.this)
                {
                    SharedPreferencesImpl.access$310(SharedPreferencesImpl.this);
                    if (paramRunnable != null)
                        paramRunnable.run();
                    return;
                    localObject2 = finally;
                    throw localObject2;
                }
            }
        };
        int i;
        if (paramRunnable == null)
        {
            i = 1;
            if (i == 0)
                break label69;
        }
        while (true)
        {
            try
            {
                if (this.mDiskWritesInFlight == 1)
                {
                    j = 1;
                    if (j == 0)
                        break label69;
                    local2.run();
                    return;
                    i = 0;
                    break;
                }
                int j = 0;
                continue;
            }
            finally
            {
            }
            label69: QueuedWork.singleThreadExecutor().execute(local2);
        }
    }

    private boolean hasFileChangedUnexpectedly()
    {
        boolean bool = false;
        FileUtils.FileStatus localFileStatus;
        try
        {
            if (this.mDiskWritesInFlight > 0)
                break label91;
            localFileStatus = new FileUtils.FileStatus();
            if (!FileUtils.getFileStatus(this.mFile.getPath(), localFileStatus))
                bool = true;
        }
        finally
        {
        }
        while (true)
        {
            try
            {
                if ((this.mStatTimestamp != localFileStatus.mtime) || (this.mStatSize != localFileStatus.size))
                    break label93;
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
            label91: return bool;
            label93: bool = true;
        }
    }

    private void loadFromDiskLocked()
    {
        if (this.mLoaded);
        while (true)
        {
            return;
            if (this.mBackupFile.exists())
            {
                this.mFile.delete();
                this.mBackupFile.renameTo(this.mFile);
            }
            if ((this.mFile.exists()) && (!this.mFile.canRead()))
                Log.w("SharedPreferencesImpl", "Attempt to read preferences file " + this.mFile + " without permission");
            HashMap localHashMap = null;
            FileUtils.FileStatus localFileStatus = new FileUtils.FileStatus();
            if ((FileUtils.getFileStatus(this.mFile.getPath(), localFileStatus)) && (this.mFile.canRead()));
            try
            {
                BufferedInputStream localBufferedInputStream = new BufferedInputStream(new FileInputStream(this.mFile), 16384);
                localHashMap = XmlUtils.readMapXml(localBufferedInputStream);
                localBufferedInputStream.close();
                this.mLoaded = true;
                if (localHashMap != null)
                {
                    this.mMap = localHashMap;
                    this.mStatTimestamp = localFileStatus.mtime;
                    this.mStatSize = localFileStatus.size;
                    notifyAll();
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                    Log.w("SharedPreferencesImpl", "getSharedPreferences", localXmlPullParserException);
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                while (true)
                    Log.w("SharedPreferencesImpl", "getSharedPreferences", localFileNotFoundException);
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    Log.w("SharedPreferencesImpl", "getSharedPreferences", localIOException);
                    continue;
                    this.mMap = new HashMap();
                }
            }
        }
    }

    private static File makeBackupFile(File paramFile)
    {
        return new File(paramFile.getPath() + ".bak");
    }

    private void startLoadFromDisk()
    {
        try
        {
            this.mLoaded = false;
            new Thread("SharedPreferencesImpl-load")
            {
                public void run()
                {
                    synchronized (SharedPreferencesImpl.this)
                    {
                        SharedPreferencesImpl.this.loadFromDiskLocked();
                        return;
                    }
                }
            }
            .start();
            return;
        }
        finally
        {
        }
    }

    private void writeToFile(MemoryCommitResult paramMemoryCommitResult)
    {
        if (this.mFile.exists())
            if (!paramMemoryCommitResult.changesMade)
                paramMemoryCommitResult.setDiskWriteResult(true);
        while (true)
        {
            return;
            if (!this.mBackupFile.exists())
            {
                if (this.mFile.renameTo(this.mBackupFile))
                    break label105;
                Log.e("SharedPreferencesImpl", "Couldn't rename file " + this.mFile + " to backup file " + this.mBackupFile);
                paramMemoryCommitResult.setDiskWriteResult(false);
                continue;
            }
            this.mFile.delete();
            try
            {
                label105: localFileOutputStream = createFileOutputStream(this.mFile);
                if (localFileOutputStream == null)
                    paramMemoryCommitResult.setDiskWriteResult(false);
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                FileOutputStream localFileOutputStream;
                Log.w("SharedPreferencesImpl", "writeToFile: Got exception:", localXmlPullParserException);
                if ((this.mFile.exists()) && (!this.mFile.delete()))
                    Log.e("SharedPreferencesImpl", "Couldn't clean up partially-written file " + this.mFile);
                paramMemoryCommitResult.setDiskWriteResult(false);
                continue;
                XmlUtils.writeMapXml(paramMemoryCommitResult.mapToWriteToDisk, localFileOutputStream);
                FileUtils.sync(localFileOutputStream);
                localFileOutputStream.close();
                ContextImpl.setFilePermissionsFromMode(this.mFile.getPath(), this.mMode, 0);
                localFileStatus = new FileUtils.FileStatus();
                if (!FileUtils.getFileStatus(this.mFile.getPath(), localFileStatus));
            }
            catch (IOException localIOException)
            {
                try
                {
                    while (true)
                    {
                        FileUtils.FileStatus localFileStatus;
                        this.mStatTimestamp = localFileStatus.mtime;
                        this.mStatSize = localFileStatus.size;
                        this.mBackupFile.delete();
                        paramMemoryCommitResult.setDiskWriteResult(true);
                        break;
                        localIOException = localIOException;
                        Log.w("SharedPreferencesImpl", "writeToFile: Got exception:", localIOException);
                    }
                }
                finally
                {
                }
            }
        }
    }

    public boolean contains(String paramString)
    {
        try
        {
            awaitLoadedLocked();
            boolean bool = this.mMap.containsKey(paramString);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public SharedPreferences.Editor edit()
    {
        try
        {
            awaitLoadedLocked();
            return new EditorImpl();
        }
        finally
        {
        }
    }

    public Map<String, ?> getAll()
    {
        try
        {
            awaitLoadedLocked();
            HashMap localHashMap = new HashMap(this.mMap);
            return localHashMap;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean getBoolean(String paramString, boolean paramBoolean)
    {
        try
        {
            awaitLoadedLocked();
            Boolean localBoolean = (Boolean)this.mMap.get(paramString);
            if (localBoolean != null)
                paramBoolean = localBoolean.booleanValue();
            return paramBoolean;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public float getFloat(String paramString, float paramFloat)
    {
        try
        {
            awaitLoadedLocked();
            Float localFloat = (Float)this.mMap.get(paramString);
            if (localFloat != null)
                paramFloat = localFloat.floatValue();
            return paramFloat;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getInt(String paramString, int paramInt)
    {
        try
        {
            awaitLoadedLocked();
            Integer localInteger = (Integer)this.mMap.get(paramString);
            if (localInteger != null)
                paramInt = localInteger.intValue();
            return paramInt;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public long getLong(String paramString, long paramLong)
    {
        try
        {
            awaitLoadedLocked();
            Long localLong = (Long)this.mMap.get(paramString);
            if (localLong != null)
                paramLong = localLong.longValue();
            return paramLong;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getString(String paramString1, String paramString2)
    {
        while (true)
        {
            try
            {
                awaitLoadedLocked();
                str = (String)this.mMap.get(paramString1);
                if (str != null)
                    return str;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            String str = paramString2;
        }
    }

    public Set<String> getStringSet(String paramString, Set<String> paramSet)
    {
        while (true)
        {
            try
            {
                awaitLoadedLocked();
                localObject2 = (Set)this.mMap.get(paramString);
                if (localObject2 != null)
                    return localObject2;
            }
            finally
            {
                localObject1 = finally;
                throw localObject1;
            }
            Object localObject2 = paramSet;
        }
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener paramOnSharedPreferenceChangeListener)
    {
        try
        {
            this.mListeners.put(paramOnSharedPreferenceChangeListener, mContent);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void startReloadIfChangedUnexpectedly()
    {
        try
        {
            if (!hasFileChangedUnexpectedly())
                return;
            startLoadFromDisk();
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener paramOnSharedPreferenceChangeListener)
    {
        try
        {
            this.mListeners.remove(paramOnSharedPreferenceChangeListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final class EditorImpl
        implements SharedPreferences.Editor
    {
        private boolean mClear = false;
        private final Map<String, Object> mModified = Maps.newHashMap();

        public EditorImpl()
        {
        }

        // ERROR //
        private SharedPreferencesImpl.MemoryCommitResult commitToMemory()
        {
            // Byte code:
            //     0: iconst_1
            //     1: istore_1
            //     2: new 49	android/app/SharedPreferencesImpl$MemoryCommitResult
            //     5: dup
            //     6: aconst_null
            //     7: invokespecial 52	android/app/SharedPreferencesImpl$MemoryCommitResult:<init>	(Landroid/app/SharedPreferencesImpl$1;)V
            //     10: astore_2
            //     11: aload_0
            //     12: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     15: astore_3
            //     16: aload_3
            //     17: monitorenter
            //     18: aload_0
            //     19: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     22: invokestatic 56	android/app/SharedPreferencesImpl:access$300	(Landroid/app/SharedPreferencesImpl;)I
            //     25: ifle +25 -> 50
            //     28: aload_0
            //     29: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     32: new 58	java/util/HashMap
            //     35: dup
            //     36: aload_0
            //     37: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     40: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     43: invokespecial 65	java/util/HashMap:<init>	(Ljava/util/Map;)V
            //     46: invokestatic 69	android/app/SharedPreferencesImpl:access$402	(Landroid/app/SharedPreferencesImpl;Ljava/util/Map;)Ljava/util/Map;
            //     49: pop
            //     50: aload_2
            //     51: aload_0
            //     52: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     55: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     58: putfield 72	android/app/SharedPreferencesImpl$MemoryCommitResult:mapToWriteToDisk	Ljava/util/Map;
            //     61: aload_0
            //     62: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     65: invokestatic 75	android/app/SharedPreferencesImpl:access$308	(Landroid/app/SharedPreferencesImpl;)I
            //     68: pop
            //     69: aload_0
            //     70: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     73: invokestatic 79	android/app/SharedPreferencesImpl:access$500	(Landroid/app/SharedPreferencesImpl;)Ljava/util/WeakHashMap;
            //     76: invokevirtual 85	java/util/WeakHashMap:size	()I
            //     79: ifle +220 -> 299
            //     82: iload_1
            //     83: ifeq +35 -> 118
            //     86: aload_2
            //     87: new 87	java/util/ArrayList
            //     90: dup
            //     91: invokespecial 88	java/util/ArrayList:<init>	()V
            //     94: putfield 92	android/app/SharedPreferencesImpl$MemoryCommitResult:keysModified	Ljava/util/List;
            //     97: aload_2
            //     98: new 94	java/util/HashSet
            //     101: dup
            //     102: aload_0
            //     103: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     106: invokestatic 79	android/app/SharedPreferencesImpl:access$500	(Landroid/app/SharedPreferencesImpl;)Ljava/util/WeakHashMap;
            //     109: invokevirtual 98	java/util/WeakHashMap:keySet	()Ljava/util/Set;
            //     112: invokespecial 101	java/util/HashSet:<init>	(Ljava/util/Collection;)V
            //     115: putfield 105	android/app/SharedPreferencesImpl$MemoryCommitResult:listeners	Ljava/util/Set;
            //     118: aload_0
            //     119: monitorenter
            //     120: aload_0
            //     121: getfield 39	android/app/SharedPreferencesImpl$EditorImpl:mClear	Z
            //     124: ifeq +40 -> 164
            //     127: aload_0
            //     128: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     131: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     134: invokeinterface 111 1 0
            //     139: ifne +20 -> 159
            //     142: aload_2
            //     143: iconst_1
            //     144: putfield 114	android/app/SharedPreferencesImpl$MemoryCommitResult:changesMade	Z
            //     147: aload_0
            //     148: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     151: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     154: invokeinterface 117 1 0
            //     159: aload_0
            //     160: iconst_0
            //     161: putfield 39	android/app/SharedPreferencesImpl$EditorImpl:mClear	Z
            //     164: aload_0
            //     165: getfield 37	android/app/SharedPreferencesImpl$EditorImpl:mModified	Ljava/util/Map;
            //     168: invokeinterface 120 1 0
            //     173: invokeinterface 126 1 0
            //     178: astore 7
            //     180: aload 7
            //     182: invokeinterface 131 1 0
            //     187: ifeq +185 -> 372
            //     190: aload 7
            //     192: invokeinterface 135 1 0
            //     197: checkcast 137	java/util/Map$Entry
            //     200: astore 8
            //     202: aload 8
            //     204: invokeinterface 140 1 0
            //     209: checkcast 142	java/lang/String
            //     212: astore 9
            //     214: aload 8
            //     216: invokeinterface 145 1 0
            //     221: astore 10
            //     223: aload 10
            //     225: aload_0
            //     226: if_acmpne +78 -> 304
            //     229: aload_0
            //     230: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     233: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     236: aload 9
            //     238: invokeinterface 149 2 0
            //     243: ifeq -63 -> 180
            //     246: aload_0
            //     247: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     250: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     253: aload 9
            //     255: invokeinterface 153 2 0
            //     260: pop
            //     261: aload_2
            //     262: iconst_1
            //     263: putfield 114	android/app/SharedPreferencesImpl$MemoryCommitResult:changesMade	Z
            //     266: iload_1
            //     267: ifeq -87 -> 180
            //     270: aload_2
            //     271: getfield 92	android/app/SharedPreferencesImpl$MemoryCommitResult:keysModified	Ljava/util/List;
            //     274: aload 9
            //     276: invokeinterface 158 2 0
            //     281: pop
            //     282: goto -102 -> 180
            //     285: astore 6
            //     287: aload_0
            //     288: monitorexit
            //     289: aload 6
            //     291: athrow
            //     292: astore 4
            //     294: aload_3
            //     295: monitorexit
            //     296: aload 4
            //     298: athrow
            //     299: iconst_0
            //     300: istore_1
            //     301: goto -219 -> 82
            //     304: aload_0
            //     305: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     308: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     311: aload 9
            //     313: invokeinterface 149 2 0
            //     318: ifeq +34 -> 352
            //     321: aload_0
            //     322: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     325: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     328: aload 9
            //     330: invokeinterface 161 2 0
            //     335: astore 13
            //     337: aload 13
            //     339: ifnull +13 -> 352
            //     342: aload 13
            //     344: aload 10
            //     346: invokevirtual 164	java/lang/Object:equals	(Ljava/lang/Object;)Z
            //     349: ifne -169 -> 180
            //     352: aload_0
            //     353: getfield 26	android/app/SharedPreferencesImpl$EditorImpl:this$0	Landroid/app/SharedPreferencesImpl;
            //     356: invokestatic 62	android/app/SharedPreferencesImpl:access$400	(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
            //     359: aload 9
            //     361: aload 10
            //     363: invokeinterface 168 3 0
            //     368: pop
            //     369: goto -108 -> 261
            //     372: aload_0
            //     373: getfield 37	android/app/SharedPreferencesImpl$EditorImpl:mModified	Ljava/util/Map;
            //     376: invokeinterface 117 1 0
            //     381: aload_0
            //     382: monitorexit
            //     383: aload_3
            //     384: monitorexit
            //     385: aload_2
            //     386: areturn
            //
            // Exception table:
            //     from	to	target	type
            //     120	289	285	finally
            //     304	383	285	finally
            //     18	120	292	finally
            //     289	296	292	finally
            //     383	385	292	finally
        }

        private void notifyListeners(final SharedPreferencesImpl.MemoryCommitResult paramMemoryCommitResult)
        {
            if ((paramMemoryCommitResult.listeners == null) || (paramMemoryCommitResult.keysModified == null) || (paramMemoryCommitResult.keysModified.size() == 0));
            while (true)
            {
                return;
                if (Looper.myLooper() == Looper.getMainLooper())
                    for (int i = -1 + paramMemoryCommitResult.keysModified.size(); i >= 0; i--)
                    {
                        String str = (String)paramMemoryCommitResult.keysModified.get(i);
                        Iterator localIterator = paramMemoryCommitResult.listeners.iterator();
                        while (localIterator.hasNext())
                        {
                            SharedPreferences.OnSharedPreferenceChangeListener localOnSharedPreferenceChangeListener = (SharedPreferences.OnSharedPreferenceChangeListener)localIterator.next();
                            if (localOnSharedPreferenceChangeListener != null)
                                localOnSharedPreferenceChangeListener.onSharedPreferenceChanged(SharedPreferencesImpl.this, str);
                        }
                    }
                else
                    ActivityThread.sMainThreadHandler.post(new Runnable()
                    {
                        public void run()
                        {
                            SharedPreferencesImpl.EditorImpl.this.notifyListeners(paramMemoryCommitResult);
                        }
                    });
            }
        }

        public void apply()
        {
            final SharedPreferencesImpl.MemoryCommitResult localMemoryCommitResult = commitToMemory();
            final Runnable local1 = new Runnable()
            {
                public void run()
                {
                    try
                    {
                        localMemoryCommitResult.writtenToDiskLatch.await();
                        label10: return;
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                        break label10;
                    }
                }
            };
            QueuedWork.add(local1);
            Runnable local2 = new Runnable()
            {
                public void run()
                {
                    local1.run();
                    QueuedWork.remove(local1);
                }
            };
            SharedPreferencesImpl.this.enqueueDiskWrite(localMemoryCommitResult, local2);
            notifyListeners(localMemoryCommitResult);
        }

        public SharedPreferences.Editor clear()
        {
            try
            {
                this.mClear = true;
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public boolean commit()
        {
            SharedPreferencesImpl.MemoryCommitResult localMemoryCommitResult = commitToMemory();
            SharedPreferencesImpl.this.enqueueDiskWrite(localMemoryCommitResult, null);
            try
            {
                localMemoryCommitResult.writtenToDiskLatch.await();
                notifyListeners(localMemoryCommitResult);
                bool = localMemoryCommitResult.writeToDiskResult;
                return bool;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    boolean bool = false;
            }
        }

        public SharedPreferences.Editor putBoolean(String paramString, boolean paramBoolean)
        {
            try
            {
                this.mModified.put(paramString, Boolean.valueOf(paramBoolean));
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public SharedPreferences.Editor putFloat(String paramString, float paramFloat)
        {
            try
            {
                this.mModified.put(paramString, Float.valueOf(paramFloat));
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public SharedPreferences.Editor putInt(String paramString, int paramInt)
        {
            try
            {
                this.mModified.put(paramString, Integer.valueOf(paramInt));
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public SharedPreferences.Editor putLong(String paramString, long paramLong)
        {
            try
            {
                this.mModified.put(paramString, Long.valueOf(paramLong));
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public SharedPreferences.Editor putString(String paramString1, String paramString2)
        {
            try
            {
                this.mModified.put(paramString1, paramString2);
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public SharedPreferences.Editor putStringSet(String paramString, Set<String> paramSet)
        {
            try
            {
                this.mModified.put(paramString, paramSet);
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public SharedPreferences.Editor remove(String paramString)
        {
            try
            {
                this.mModified.put(paramString, this);
                return this;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    private static class MemoryCommitResult
    {
        public boolean changesMade;
        public List<String> keysModified;
        public Set<SharedPreferences.OnSharedPreferenceChangeListener> listeners;
        public Map<?, ?> mapToWriteToDisk;
        public volatile boolean writeToDiskResult = false;
        public final CountDownLatch writtenToDiskLatch = new CountDownLatch(1);

        public void setDiskWriteResult(boolean paramBoolean)
        {
            this.writeToDiskResult = paramBoolean;
            this.writtenToDiskLatch.countDown();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.SharedPreferencesImpl
 * JD-Core Version:        0.6.2
 */