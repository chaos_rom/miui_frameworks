package android.app;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageItemInfo;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Deprecated
public class LocalActivityManager
{
    static final int CREATED = 2;
    static final int DESTROYED = 5;
    static final int INITIALIZING = 1;
    static final int RESTORED = 0;
    static final int RESUMED = 4;
    static final int STARTED = 3;
    private static final String TAG = "LocalActivityManager";
    private static final boolean localLOGV;
    private final Map<String, LocalActivityRecord> mActivities = new HashMap();
    private final ArrayList<LocalActivityRecord> mActivityArray = new ArrayList();
    private final ActivityThread mActivityThread = ActivityThread.currentActivityThread();
    private int mCurState = 1;
    private boolean mFinishing;
    private final Activity mParent;
    private LocalActivityRecord mResumed;
    private boolean mSingleMode;

    public LocalActivityManager(Activity paramActivity, boolean paramBoolean)
    {
        this.mParent = paramActivity;
        this.mSingleMode = paramBoolean;
    }

    private void moveToState(LocalActivityRecord paramLocalActivityRecord, int paramInt)
    {
        if ((paramLocalActivityRecord.curState == 0) || (paramLocalActivityRecord.curState == 5));
        while (true)
        {
            return;
            if (paramLocalActivityRecord.curState == 1)
            {
                HashMap localHashMap = this.mParent.getLastNonConfigurationChildInstances();
                Object localObject = null;
                if (localHashMap != null)
                    localObject = localHashMap.get(paramLocalActivityRecord.id);
                Activity.NonConfigurationInstances localNonConfigurationInstances = null;
                if (localObject != null)
                {
                    localNonConfigurationInstances = new Activity.NonConfigurationInstances();
                    localNonConfigurationInstances.activity = localObject;
                }
                if (paramLocalActivityRecord.activityInfo == null)
                    paramLocalActivityRecord.activityInfo = this.mActivityThread.resolveActivityInfo(paramLocalActivityRecord.intent);
                paramLocalActivityRecord.activity = this.mActivityThread.startActivityNow(this.mParent, paramLocalActivityRecord.id, paramLocalActivityRecord.intent, paramLocalActivityRecord.activityInfo, paramLocalActivityRecord, paramLocalActivityRecord.instanceState, localNonConfigurationInstances);
                if (paramLocalActivityRecord.activity != null)
                {
                    paramLocalActivityRecord.window = paramLocalActivityRecord.activity.getWindow();
                    paramLocalActivityRecord.instanceState = null;
                    paramLocalActivityRecord.curState = 3;
                    if (paramInt == 4)
                    {
                        this.mActivityThread.performResumeActivity(paramLocalActivityRecord, true);
                        paramLocalActivityRecord.curState = 4;
                    }
                }
            }
            else
            {
                switch (paramLocalActivityRecord.curState)
                {
                default:
                    break;
                case 2:
                    if (paramInt == 3)
                    {
                        this.mActivityThread.performRestartActivity(paramLocalActivityRecord);
                        paramLocalActivityRecord.curState = 3;
                    }
                    if (paramInt == 4)
                    {
                        this.mActivityThread.performRestartActivity(paramLocalActivityRecord);
                        this.mActivityThread.performResumeActivity(paramLocalActivityRecord, true);
                        paramLocalActivityRecord.curState = 4;
                    }
                    break;
                case 3:
                    if (paramInt == 4)
                    {
                        this.mActivityThread.performResumeActivity(paramLocalActivityRecord, true);
                        paramLocalActivityRecord.instanceState = null;
                        paramLocalActivityRecord.curState = 4;
                    }
                    if (paramInt == 2)
                    {
                        this.mActivityThread.performStopActivity(paramLocalActivityRecord, false);
                        paramLocalActivityRecord.curState = 2;
                    }
                    break;
                case 4:
                    if (paramInt == 3)
                    {
                        performPause(paramLocalActivityRecord, this.mFinishing);
                        paramLocalActivityRecord.curState = 3;
                    }
                    if (paramInt == 2)
                    {
                        performPause(paramLocalActivityRecord, this.mFinishing);
                        this.mActivityThread.performStopActivity(paramLocalActivityRecord, false);
                        paramLocalActivityRecord.curState = 2;
                    }
                    break;
                }
            }
        }
    }

    private Window performDestroy(LocalActivityRecord paramLocalActivityRecord, boolean paramBoolean)
    {
        Window localWindow = paramLocalActivityRecord.window;
        if ((paramLocalActivityRecord.curState == 4) && (!paramBoolean))
            performPause(paramLocalActivityRecord, paramBoolean);
        this.mActivityThread.performDestroyActivity(paramLocalActivityRecord, paramBoolean);
        paramLocalActivityRecord.activity = null;
        paramLocalActivityRecord.window = null;
        if (paramBoolean)
            paramLocalActivityRecord.instanceState = null;
        paramLocalActivityRecord.curState = 5;
        return localWindow;
    }

    private void performPause(LocalActivityRecord paramLocalActivityRecord, boolean paramBoolean)
    {
        if (paramLocalActivityRecord.instanceState == null);
        for (boolean bool = true; ; bool = false)
        {
            Bundle localBundle = this.mActivityThread.performPauseActivity(paramLocalActivityRecord, paramBoolean, bool);
            if (bool)
                paramLocalActivityRecord.instanceState = localBundle;
            return;
        }
    }

    public Window destroyActivity(String paramString, boolean paramBoolean)
    {
        LocalActivityRecord localLocalActivityRecord = (LocalActivityRecord)this.mActivities.get(paramString);
        Window localWindow = null;
        if (localLocalActivityRecord != null)
        {
            localWindow = performDestroy(localLocalActivityRecord, paramBoolean);
            if (paramBoolean)
            {
                this.mActivities.remove(paramString);
                this.mActivityArray.remove(localLocalActivityRecord);
            }
        }
        return localWindow;
    }

    public void dispatchCreate(Bundle paramBundle)
    {
        if (paramBundle != null)
        {
            Iterator localIterator = paramBundle.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                Bundle localBundle;
                try
                {
                    localBundle = paramBundle.getBundle(str);
                    LocalActivityRecord localLocalActivityRecord1 = (LocalActivityRecord)this.mActivities.get(str);
                    if (localLocalActivityRecord1 == null)
                        break label85;
                    localLocalActivityRecord1.instanceState = localBundle;
                }
                catch (Exception localException)
                {
                    Log.e("LocalActivityManager", "Exception thrown when restoring LocalActivityManager state", localException);
                }
                continue;
                label85: LocalActivityRecord localLocalActivityRecord2 = new LocalActivityRecord(str, null);
                localLocalActivityRecord2.instanceState = localBundle;
                this.mActivities.put(str, localLocalActivityRecord2);
                this.mActivityArray.add(localLocalActivityRecord2);
            }
        }
        this.mCurState = 2;
    }

    public void dispatchDestroy(boolean paramBoolean)
    {
        int i = this.mActivityArray.size();
        for (int j = 0; j < i; j++)
        {
            LocalActivityRecord localLocalActivityRecord = (LocalActivityRecord)this.mActivityArray.get(j);
            this.mActivityThread.performDestroyActivity(localLocalActivityRecord, paramBoolean);
        }
        this.mActivities.clear();
        this.mActivityArray.clear();
    }

    public void dispatchPause(boolean paramBoolean)
    {
        if (paramBoolean)
            this.mFinishing = true;
        this.mCurState = 3;
        if (this.mSingleMode)
            if (this.mResumed != null)
                moveToState(this.mResumed, 3);
        while (true)
        {
            return;
            int i = this.mActivityArray.size();
            for (int j = 0; j < i; j++)
            {
                LocalActivityRecord localLocalActivityRecord = (LocalActivityRecord)this.mActivityArray.get(j);
                if (localLocalActivityRecord.curState == 4)
                    moveToState(localLocalActivityRecord, 3);
            }
        }
    }

    public void dispatchResume()
    {
        this.mCurState = 4;
        if (this.mSingleMode)
            if (this.mResumed != null)
                moveToState(this.mResumed, 4);
        while (true)
        {
            return;
            int i = this.mActivityArray.size();
            for (int j = 0; j < i; j++)
                moveToState((LocalActivityRecord)this.mActivityArray.get(j), 4);
        }
    }

    public HashMap<String, Object> dispatchRetainNonConfigurationInstance()
    {
        HashMap localHashMap = null;
        int i = this.mActivityArray.size();
        for (int j = 0; j < i; j++)
        {
            LocalActivityRecord localLocalActivityRecord = (LocalActivityRecord)this.mActivityArray.get(j);
            if ((localLocalActivityRecord != null) && (localLocalActivityRecord.activity != null))
            {
                Object localObject = localLocalActivityRecord.activity.onRetainNonConfigurationInstance();
                if (localObject != null)
                {
                    if (localHashMap == null)
                        localHashMap = new HashMap();
                    localHashMap.put(localLocalActivityRecord.id, localObject);
                }
            }
        }
        return localHashMap;
    }

    public void dispatchStop()
    {
        this.mCurState = 2;
        int i = this.mActivityArray.size();
        for (int j = 0; j < i; j++)
            moveToState((LocalActivityRecord)this.mActivityArray.get(j), 2);
    }

    public Activity getActivity(String paramString)
    {
        LocalActivityRecord localLocalActivityRecord = (LocalActivityRecord)this.mActivities.get(paramString);
        if (localLocalActivityRecord != null);
        for (Activity localActivity = localLocalActivityRecord.activity; ; localActivity = null)
            return localActivity;
    }

    public Activity getCurrentActivity()
    {
        if (this.mResumed != null);
        for (Activity localActivity = this.mResumed.activity; ; localActivity = null)
            return localActivity;
    }

    public String getCurrentId()
    {
        if (this.mResumed != null);
        for (String str = this.mResumed.id; ; str = null)
            return str;
    }

    public void removeAllActivities()
    {
        dispatchDestroy(true);
    }

    public Bundle saveInstanceState()
    {
        Bundle localBundle1 = null;
        int i = this.mActivityArray.size();
        for (int j = 0; j < i; j++)
        {
            LocalActivityRecord localLocalActivityRecord = (LocalActivityRecord)this.mActivityArray.get(j);
            if (localBundle1 == null)
                localBundle1 = new Bundle();
            if (((localLocalActivityRecord.instanceState != null) || (localLocalActivityRecord.curState == 4)) && (localLocalActivityRecord.activity != null))
            {
                Bundle localBundle2 = new Bundle();
                localLocalActivityRecord.activity.performSaveInstanceState(localBundle2);
                localLocalActivityRecord.instanceState = localBundle2;
            }
            if (localLocalActivityRecord.instanceState != null)
                localBundle1.putBundle(localLocalActivityRecord.id, localLocalActivityRecord.instanceState);
        }
        return localBundle1;
    }

    public Window startActivity(String paramString, Intent paramIntent)
    {
        if (this.mCurState == 1)
            throw new IllegalStateException("Activities can't be added until the containing group has been created.");
        int i = 0;
        boolean bool = false;
        ActivityInfo localActivityInfo = null;
        LocalActivityRecord localLocalActivityRecord1 = (LocalActivityRecord)this.mActivities.get(paramString);
        if (localLocalActivityRecord1 == null)
        {
            localLocalActivityRecord1 = new LocalActivityRecord(paramString, paramIntent);
            i = 1;
            if (localActivityInfo == null)
                localActivityInfo = this.mActivityThread.resolveActivityInfo(paramIntent);
            if (this.mSingleMode)
            {
                LocalActivityRecord localLocalActivityRecord2 = this.mResumed;
                if ((localLocalActivityRecord2 != null) && (localLocalActivityRecord2 != localLocalActivityRecord1) && (this.mCurState == 4))
                    moveToState(localLocalActivityRecord2, 3);
            }
            if (i == 0)
                break label228;
            this.mActivities.put(paramString, localLocalActivityRecord1);
            this.mActivityArray.add(localLocalActivityRecord1);
        }
        while (true)
        {
            label142: localLocalActivityRecord1.intent = paramIntent;
            localLocalActivityRecord1.curState = 1;
            localLocalActivityRecord1.activityInfo = localActivityInfo;
            moveToState(localLocalActivityRecord1, this.mCurState);
            if (this.mSingleMode)
                this.mResumed = localLocalActivityRecord1;
            Window localWindow = localLocalActivityRecord1.window;
            while (true)
            {
                return localWindow;
                if (localLocalActivityRecord1.intent == null)
                    break;
                bool = localLocalActivityRecord1.intent.filterEquals(paramIntent);
                if (!bool)
                    break;
                localActivityInfo = localLocalActivityRecord1.activityInfo;
                break;
                label228: if (localLocalActivityRecord1.activityInfo == null)
                    break label142;
                if ((localActivityInfo != localLocalActivityRecord1.activityInfo) && ((!localActivityInfo.name.equals(localLocalActivityRecord1.activityInfo.name)) || (!localActivityInfo.packageName.equals(localLocalActivityRecord1.activityInfo.packageName))))
                    break label425;
                if ((localActivityInfo.launchMode != 0) || ((0x20000000 & paramIntent.getFlags()) != 0))
                {
                    ArrayList localArrayList = new ArrayList(1);
                    localArrayList.add(paramIntent);
                    this.mActivityThread.performNewIntents(localLocalActivityRecord1, localArrayList);
                    localLocalActivityRecord1.intent = paramIntent;
                    moveToState(localLocalActivityRecord1, this.mCurState);
                    if (this.mSingleMode)
                        this.mResumed = localLocalActivityRecord1;
                    localWindow = localLocalActivityRecord1.window;
                }
                else
                {
                    if ((!bool) || ((0x4000000 & paramIntent.getFlags()) != 0))
                        break label425;
                    localLocalActivityRecord1.intent = paramIntent;
                    moveToState(localLocalActivityRecord1, this.mCurState);
                    if (this.mSingleMode)
                        this.mResumed = localLocalActivityRecord1;
                    localWindow = localLocalActivityRecord1.window;
                }
            }
            label425: performDestroy(localLocalActivityRecord1, true);
        }
    }

    private static class LocalActivityRecord extends Binder
    {
        Activity activity;
        ActivityInfo activityInfo;
        int curState = 0;
        final String id;
        Bundle instanceState;
        Intent intent;
        Window window;

        LocalActivityRecord(String paramString, Intent paramIntent)
        {
            this.id = paramString;
            this.intent = paramIntent;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.LocalActivityManager
 * JD-Core Version:        0.6.2
 */