package android.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R.styleable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public final class SearchableInfo
    implements Parcelable
{
    public static final Parcelable.Creator<SearchableInfo> CREATOR = new Parcelable.Creator()
    {
        public SearchableInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SearchableInfo(paramAnonymousParcel);
        }

        public SearchableInfo[] newArray(int paramAnonymousInt)
        {
            return new SearchableInfo[paramAnonymousInt];
        }
    };
    private static final boolean DBG = false;
    private static final String LOG_TAG = "SearchableInfo";
    private static final String MD_LABEL_SEARCHABLE = "android.app.searchable";
    private static final String MD_XML_ELEMENT_SEARCHABLE = "searchable";
    private static final String MD_XML_ELEMENT_SEARCHABLE_ACTION_KEY = "actionkey";
    private static final int SEARCH_MODE_BADGE_ICON = 8;
    private static final int SEARCH_MODE_BADGE_LABEL = 4;
    private static final int SEARCH_MODE_QUERY_REWRITE_FROM_DATA = 16;
    private static final int SEARCH_MODE_QUERY_REWRITE_FROM_TEXT = 32;
    private static final int VOICE_SEARCH_LAUNCH_RECOGNIZER = 4;
    private static final int VOICE_SEARCH_LAUNCH_WEB_SEARCH = 2;
    private static final int VOICE_SEARCH_SHOW_BUTTON = 1;
    private HashMap<Integer, ActionKeyInfo> mActionKeys = null;
    private final boolean mAutoUrlDetect;
    private final int mHintId;
    private final int mIconId;
    private final boolean mIncludeInGlobalSearch;
    private final int mLabelId;
    private final boolean mQueryAfterZeroResults;
    private final ComponentName mSearchActivity;
    private final int mSearchButtonText;
    private final int mSearchImeOptions;
    private final int mSearchInputType;
    private final int mSearchMode;
    private final int mSettingsDescriptionId;
    private final String mSuggestAuthority;
    private final String mSuggestIntentAction;
    private final String mSuggestIntentData;
    private final String mSuggestPath;
    private final String mSuggestProviderPackage;
    private final String mSuggestSelection;
    private final int mSuggestThreshold;
    private final int mVoiceLanguageId;
    private final int mVoiceLanguageModeId;
    private final int mVoiceMaxResults;
    private final int mVoicePromptTextId;
    private final int mVoiceSearchMode;

    private SearchableInfo(Context paramContext, AttributeSet paramAttributeSet, ComponentName paramComponentName)
    {
        this.mSearchActivity = paramComponentName;
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Searchable);
        this.mSearchMode = localTypedArray.getInt(3, 0);
        this.mLabelId = localTypedArray.getResourceId(0, 0);
        this.mHintId = localTypedArray.getResourceId(2, 0);
        this.mIconId = localTypedArray.getResourceId(1, 0);
        this.mSearchButtonText = localTypedArray.getResourceId(9, 0);
        this.mSearchInputType = localTypedArray.getInt(10, 1);
        this.mSearchImeOptions = localTypedArray.getInt(16, 2);
        this.mIncludeInGlobalSearch = localTypedArray.getBoolean(18, false);
        this.mQueryAfterZeroResults = localTypedArray.getBoolean(19, false);
        this.mAutoUrlDetect = localTypedArray.getBoolean(21, false);
        this.mSettingsDescriptionId = localTypedArray.getResourceId(20, 0);
        this.mSuggestAuthority = localTypedArray.getString(4);
        this.mSuggestPath = localTypedArray.getString(5);
        this.mSuggestSelection = localTypedArray.getString(6);
        this.mSuggestIntentAction = localTypedArray.getString(7);
        this.mSuggestIntentData = localTypedArray.getString(8);
        this.mSuggestThreshold = localTypedArray.getInt(17, 0);
        this.mVoiceSearchMode = localTypedArray.getInt(11, 0);
        this.mVoiceLanguageModeId = localTypedArray.getResourceId(12, 0);
        this.mVoicePromptTextId = localTypedArray.getResourceId(13, 0);
        this.mVoiceLanguageId = localTypedArray.getResourceId(14, 0);
        this.mVoiceMaxResults = localTypedArray.getInt(15, 0);
        localTypedArray.recycle();
        String str = null;
        if (this.mSuggestAuthority != null)
        {
            ProviderInfo localProviderInfo = paramContext.getPackageManager().resolveContentProvider(this.mSuggestAuthority, 0);
            if (localProviderInfo != null)
                str = localProviderInfo.packageName;
        }
        this.mSuggestProviderPackage = str;
        if (this.mLabelId == 0)
            throw new IllegalArgumentException("Search label must be a resource reference.");
    }

    SearchableInfo(Parcel paramParcel)
    {
        this.mLabelId = paramParcel.readInt();
        this.mSearchActivity = ComponentName.readFromParcel(paramParcel);
        this.mHintId = paramParcel.readInt();
        this.mSearchMode = paramParcel.readInt();
        this.mIconId = paramParcel.readInt();
        this.mSearchButtonText = paramParcel.readInt();
        this.mSearchInputType = paramParcel.readInt();
        this.mSearchImeOptions = paramParcel.readInt();
        boolean bool2;
        boolean bool3;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.mIncludeInGlobalSearch = bool2;
            if (paramParcel.readInt() == 0)
                break label208;
            bool3 = bool1;
            label99: this.mQueryAfterZeroResults = bool3;
            if (paramParcel.readInt() == 0)
                break label214;
        }
        while (true)
        {
            this.mAutoUrlDetect = bool1;
            this.mSettingsDescriptionId = paramParcel.readInt();
            this.mSuggestAuthority = paramParcel.readString();
            this.mSuggestPath = paramParcel.readString();
            this.mSuggestSelection = paramParcel.readString();
            this.mSuggestIntentAction = paramParcel.readString();
            this.mSuggestIntentData = paramParcel.readString();
            this.mSuggestThreshold = paramParcel.readInt();
            for (int i = paramParcel.readInt(); i > 0; i--)
                addActionKey(new ActionKeyInfo(paramParcel, null));
            bool2 = false;
            break;
            label208: bool3 = false;
            break label99;
            label214: bool1 = false;
        }
        this.mSuggestProviderPackage = paramParcel.readString();
        this.mVoiceSearchMode = paramParcel.readInt();
        this.mVoiceLanguageModeId = paramParcel.readInt();
        this.mVoicePromptTextId = paramParcel.readInt();
        this.mVoiceLanguageId = paramParcel.readInt();
        this.mVoiceMaxResults = paramParcel.readInt();
    }

    private void addActionKey(ActionKeyInfo paramActionKeyInfo)
    {
        if (this.mActionKeys == null)
            this.mActionKeys = new HashMap();
        this.mActionKeys.put(Integer.valueOf(paramActionKeyInfo.getKeyCode()), paramActionKeyInfo);
    }

    private static Context createActivityContext(Context paramContext, ComponentName paramComponentName)
    {
        Object localObject = null;
        try
        {
            Context localContext = paramContext.createPackageContext(paramComponentName.getPackageName(), 0);
            localObject = localContext;
            return localObject;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
                Log.e("SearchableInfo", "Package not found " + paramComponentName.getPackageName());
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
                Log.e("SearchableInfo", "Can't make context for " + paramComponentName.getPackageName(), localSecurityException);
        }
    }

    public static SearchableInfo getActivityMetaData(Context paramContext, ActivityInfo paramActivityInfo)
    {
        XmlResourceParser localXmlResourceParser = paramActivityInfo.loadXmlMetaData(paramContext.getPackageManager(), "android.app.searchable");
        SearchableInfo localSearchableInfo;
        if (localXmlResourceParser == null)
            localSearchableInfo = null;
        while (true)
        {
            return localSearchableInfo;
            localSearchableInfo = getActivityMetaData(paramContext, localXmlResourceParser, new ComponentName(paramActivityInfo.packageName, paramActivityInfo.name));
            localXmlResourceParser.close();
        }
    }

    // ERROR //
    private static SearchableInfo getActivityMetaData(Context paramContext, org.xmlpull.v1.XmlPullParser paramXmlPullParser, ComponentName paramComponentName)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_2
        //     2: invokestatic 289	android/app/SearchableInfo:createActivityContext	(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Context;
        //     5: astore_3
        //     6: aload_3
        //     7: ifnonnull +9 -> 16
        //     10: aconst_null
        //     11: astore 6
        //     13: aload 6
        //     15: areturn
        //     16: aload_1
        //     17: invokeinterface 294 1 0
        //     22: istore 9
        //     24: iload 9
        //     26: istore 10
        //     28: aconst_null
        //     29: astore 6
        //     31: iload 10
        //     33: iconst_1
        //     34: if_icmpeq +313 -> 347
        //     37: iload 10
        //     39: iconst_2
        //     40: if_icmpne +329 -> 369
        //     43: aload_1
        //     44: invokeinterface 297 1 0
        //     49: ldc 27
        //     51: invokevirtual 303	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     54: ifeq +100 -> 154
        //     57: aload_1
        //     58: invokestatic 309	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     61: astore 21
        //     63: aload 21
        //     65: ifnull +304 -> 369
        //     68: new 2	android/app/SearchableInfo
        //     71: dup
        //     72: aload_3
        //     73: aload 21
        //     75: aload_2
        //     76: invokespecial 311	android/app/SearchableInfo:<init>	(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/ComponentName;)V
        //     79: astore 12
        //     81: aload_1
        //     82: invokeinterface 294 1 0
        //     87: istore 13
        //     89: iload 13
        //     91: istore 10
        //     93: aload 12
        //     95: astore 6
        //     97: goto -66 -> 31
        //     100: astore 22
        //     102: ldc 21
        //     104: new 240	java/lang/StringBuilder
        //     107: dup
        //     108: invokespecial 241	java/lang/StringBuilder:<init>	()V
        //     111: ldc_w 313
        //     114: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     117: aload_2
        //     118: invokevirtual 316	android/content/ComponentName:flattenToShortString	()Ljava/lang/String;
        //     121: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     124: ldc_w 318
        //     127: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     130: aload 22
        //     132: invokevirtual 321	java/lang/IllegalArgumentException:getMessage	()Ljava/lang/String;
        //     135: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: invokevirtual 250	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     141: invokestatic 324	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     144: pop
        //     145: aload 6
        //     147: pop
        //     148: aconst_null
        //     149: astore 6
        //     151: goto -138 -> 13
        //     154: aload_1
        //     155: invokeinterface 297 1 0
        //     160: ldc 30
        //     162: invokevirtual 303	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     165: ifeq +204 -> 369
        //     168: aload 6
        //     170: ifnonnull +12 -> 182
        //     173: aload 6
        //     175: pop
        //     176: aconst_null
        //     177: astore 6
        //     179: goto -166 -> 13
        //     182: aload_1
        //     183: invokestatic 309	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     186: astore 16
        //     188: aload 16
        //     190: ifnull +179 -> 369
        //     193: aload 6
        //     195: new 10	android/app/SearchableInfo$ActionKeyInfo
        //     198: dup
        //     199: aload_3
        //     200: aload 16
        //     202: invokespecial 327	android/app/SearchableInfo$ActionKeyInfo:<init>	(Landroid/content/Context;Landroid/util/AttributeSet;)V
        //     205: invokespecial 209	android/app/SearchableInfo:addActionKey	(Landroid/app/SearchableInfo$ActionKeyInfo;)V
        //     208: aload 6
        //     210: astore 12
        //     212: goto -131 -> 81
        //     215: astore 17
        //     217: ldc 21
        //     219: new 240	java/lang/StringBuilder
        //     222: dup
        //     223: invokespecial 241	java/lang/StringBuilder:<init>	()V
        //     226: ldc_w 329
        //     229: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     232: aload_2
        //     233: invokevirtual 316	android/content/ComponentName:flattenToShortString	()Ljava/lang/String;
        //     236: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     239: ldc_w 318
        //     242: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     245: aload 17
        //     247: invokevirtual 321	java/lang/IllegalArgumentException:getMessage	()Ljava/lang/String;
        //     250: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     253: invokevirtual 250	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     256: invokestatic 324	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     259: pop
        //     260: aload 6
        //     262: pop
        //     263: aconst_null
        //     264: astore 6
        //     266: goto -253 -> 13
        //     269: astore 7
        //     271: ldc 21
        //     273: new 240	java/lang/StringBuilder
        //     276: dup
        //     277: invokespecial 241	java/lang/StringBuilder:<init>	()V
        //     280: ldc_w 331
        //     283: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     286: aload_2
        //     287: invokevirtual 316	android/content/ComponentName:flattenToShortString	()Ljava/lang/String;
        //     290: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     293: invokevirtual 250	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     296: aload 7
        //     298: invokestatic 333	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     301: pop
        //     302: aconst_null
        //     303: astore 6
        //     305: goto -292 -> 13
        //     308: astore 4
        //     310: ldc 21
        //     312: new 240	java/lang/StringBuilder
        //     315: dup
        //     316: invokespecial 241	java/lang/StringBuilder:<init>	()V
        //     319: ldc_w 331
        //     322: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     325: aload_2
        //     326: invokevirtual 316	android/content/ComponentName:flattenToShortString	()Ljava/lang/String;
        //     329: invokevirtual 247	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     332: invokevirtual 250	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     335: aload 4
        //     337: invokestatic 333	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     340: pop
        //     341: aconst_null
        //     342: astore 6
        //     344: goto -331 -> 13
        //     347: aload 6
        //     349: pop
        //     350: goto -337 -> 13
        //     353: astore 4
        //     355: aload 6
        //     357: pop
        //     358: goto -48 -> 310
        //     361: astore 7
        //     363: aload 6
        //     365: pop
        //     366: goto -95 -> 271
        //     369: aload 6
        //     371: astore 12
        //     373: goto -292 -> 81
        //
        // Exception table:
        //     from	to	target	type
        //     68	81	100	java/lang/IllegalArgumentException
        //     193	208	215	java/lang/IllegalArgumentException
        //     16	24	269	org/xmlpull/v1/XmlPullParserException
        //     81	89	269	org/xmlpull/v1/XmlPullParserException
        //     16	24	308	java/io/IOException
        //     81	89	308	java/io/IOException
        //     43	63	353	java/io/IOException
        //     68	81	353	java/io/IOException
        //     102	188	353	java/io/IOException
        //     193	208	353	java/io/IOException
        //     217	260	353	java/io/IOException
        //     43	63	361	org/xmlpull/v1/XmlPullParserException
        //     68	81	361	org/xmlpull/v1/XmlPullParserException
        //     102	188	361	org/xmlpull/v1/XmlPullParserException
        //     193	208	361	org/xmlpull/v1/XmlPullParserException
        //     217	260	361	org/xmlpull/v1/XmlPullParserException
    }

    public boolean autoUrlDetect()
    {
        return this.mAutoUrlDetect;
    }

    public int describeContents()
    {
        return 0;
    }

    public ActionKeyInfo findActionKey(int paramInt)
    {
        if (this.mActionKeys == null);
        for (ActionKeyInfo localActionKeyInfo = null; ; localActionKeyInfo = (ActionKeyInfo)this.mActionKeys.get(Integer.valueOf(paramInt)))
            return localActionKeyInfo;
    }

    public Context getActivityContext(Context paramContext)
    {
        return createActivityContext(paramContext, this.mSearchActivity);
    }

    public int getHintId()
    {
        return this.mHintId;
    }

    public int getIconId()
    {
        return this.mIconId;
    }

    public int getImeOptions()
    {
        return this.mSearchImeOptions;
    }

    public int getInputType()
    {
        return this.mSearchInputType;
    }

    public int getLabelId()
    {
        return this.mLabelId;
    }

    public Context getProviderContext(Context paramContext1, Context paramContext2)
    {
        Object localObject = null;
        if (this.mSearchActivity.getPackageName().equals(this.mSuggestProviderPackage));
        while (true)
        {
            return paramContext2;
            if (this.mSuggestProviderPackage != null);
            try
            {
                Context localContext = paramContext1.createPackageContext(this.mSuggestProviderPackage, 0);
                localObject = localContext;
                label42: paramContext2 = localObject;
            }
            catch (SecurityException localSecurityException)
            {
                break label42;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                break label42;
            }
        }
    }

    public ComponentName getSearchActivity()
    {
        return this.mSearchActivity;
    }

    public int getSearchButtonText()
    {
        return this.mSearchButtonText;
    }

    public int getSettingsDescriptionId()
    {
        return this.mSettingsDescriptionId;
    }

    public String getSuggestAuthority()
    {
        return this.mSuggestAuthority;
    }

    public String getSuggestIntentAction()
    {
        return this.mSuggestIntentAction;
    }

    public String getSuggestIntentData()
    {
        return this.mSuggestIntentData;
    }

    public String getSuggestPackage()
    {
        return this.mSuggestProviderPackage;
    }

    public String getSuggestPath()
    {
        return this.mSuggestPath;
    }

    public String getSuggestSelection()
    {
        return this.mSuggestSelection;
    }

    public int getSuggestThreshold()
    {
        return this.mSuggestThreshold;
    }

    public int getVoiceLanguageId()
    {
        return this.mVoiceLanguageId;
    }

    public int getVoiceLanguageModeId()
    {
        return this.mVoiceLanguageModeId;
    }

    public int getVoiceMaxResults()
    {
        return this.mVoiceMaxResults;
    }

    public int getVoicePromptTextId()
    {
        return this.mVoicePromptTextId;
    }

    public boolean getVoiceSearchEnabled()
    {
        if ((0x1 & this.mVoiceSearchMode) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean getVoiceSearchLaunchRecognizer()
    {
        if ((0x4 & this.mVoiceSearchMode) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean getVoiceSearchLaunchWebSearch()
    {
        if ((0x2 & this.mVoiceSearchMode) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean queryAfterZeroResults()
    {
        return this.mQueryAfterZeroResults;
    }

    public boolean shouldIncludeInGlobalSearch()
    {
        return this.mIncludeInGlobalSearch;
    }

    public boolean shouldRewriteQueryFromData()
    {
        if ((0x10 & this.mSearchMode) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean shouldRewriteQueryFromText()
    {
        if ((0x20 & this.mSearchMode) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean useBadgeIcon()
    {
        if (((0x8 & this.mSearchMode) != 0) && (this.mIconId != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean useBadgeLabel()
    {
        if ((0x4 & this.mSearchMode) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(this.mLabelId);
        this.mSearchActivity.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.mHintId);
        paramParcel.writeInt(this.mSearchMode);
        paramParcel.writeInt(this.mIconId);
        paramParcel.writeInt(this.mSearchButtonText);
        paramParcel.writeInt(this.mSearchInputType);
        paramParcel.writeInt(this.mSearchImeOptions);
        int j;
        int k;
        if (this.mIncludeInGlobalSearch)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.mQueryAfterZeroResults)
                break label234;
            k = i;
            label93: paramParcel.writeInt(k);
            if (!this.mAutoUrlDetect)
                break label240;
            label106: paramParcel.writeInt(i);
            paramParcel.writeInt(this.mSettingsDescriptionId);
            paramParcel.writeString(this.mSuggestAuthority);
            paramParcel.writeString(this.mSuggestPath);
            paramParcel.writeString(this.mSuggestSelection);
            paramParcel.writeString(this.mSuggestIntentAction);
            paramParcel.writeString(this.mSuggestIntentData);
            paramParcel.writeInt(this.mSuggestThreshold);
            if (this.mActionKeys != null)
                break label245;
            paramParcel.writeInt(0);
        }
        while (true)
        {
            paramParcel.writeString(this.mSuggestProviderPackage);
            paramParcel.writeInt(this.mVoiceSearchMode);
            paramParcel.writeInt(this.mVoiceLanguageModeId);
            paramParcel.writeInt(this.mVoicePromptTextId);
            paramParcel.writeInt(this.mVoiceLanguageId);
            paramParcel.writeInt(this.mVoiceMaxResults);
            return;
            j = 0;
            break;
            label234: k = 0;
            break label93;
            label240: i = 0;
            break label106;
            label245: paramParcel.writeInt(this.mActionKeys.size());
            Iterator localIterator = this.mActionKeys.values().iterator();
            while (localIterator.hasNext())
                ((ActionKeyInfo)localIterator.next()).writeToParcel(paramParcel, paramInt);
        }
    }

    public static class ActionKeyInfo
        implements Parcelable
    {
        private final int mKeyCode;
        private final String mQueryActionMsg;
        private final String mSuggestActionMsg;
        private final String mSuggestActionMsgColumn;

        ActionKeyInfo(Context paramContext, AttributeSet paramAttributeSet)
        {
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SearchableActionKey);
            this.mKeyCode = localTypedArray.getInt(0, 0);
            this.mQueryActionMsg = localTypedArray.getString(1);
            this.mSuggestActionMsg = localTypedArray.getString(2);
            this.mSuggestActionMsgColumn = localTypedArray.getString(3);
            localTypedArray.recycle();
            if (this.mKeyCode == 0)
                throw new IllegalArgumentException("No keycode.");
            if ((this.mQueryActionMsg == null) && (this.mSuggestActionMsg == null) && (this.mSuggestActionMsgColumn == null))
                throw new IllegalArgumentException("No message information.");
        }

        private ActionKeyInfo(Parcel paramParcel)
        {
            this.mKeyCode = paramParcel.readInt();
            this.mQueryActionMsg = paramParcel.readString();
            this.mSuggestActionMsg = paramParcel.readString();
            this.mSuggestActionMsgColumn = paramParcel.readString();
        }

        public int describeContents()
        {
            return 0;
        }

        public int getKeyCode()
        {
            return this.mKeyCode;
        }

        public String getQueryActionMsg()
        {
            return this.mQueryActionMsg;
        }

        public String getSuggestActionMsg()
        {
            return this.mSuggestActionMsg;
        }

        public String getSuggestActionMsgColumn()
        {
            return this.mSuggestActionMsgColumn;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.mKeyCode);
            paramParcel.writeString(this.mQueryActionMsg);
            paramParcel.writeString(this.mSuggestActionMsg);
            paramParcel.writeString(this.mSuggestActionMsgColumn);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.SearchableInfo
 * JD-Core Version:        0.6.2
 */