package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INotificationManager extends IInterface
{
    public abstract boolean areNotificationsEnabledForPackage(String paramString)
        throws RemoteException;

    public abstract void cancelAllNotifications(String paramString)
        throws RemoteException;

    public abstract void cancelNotification(String paramString, int paramInt)
        throws RemoteException;

    public abstract void cancelNotificationWithTag(String paramString1, String paramString2, int paramInt)
        throws RemoteException;

    public abstract void cancelToast(String paramString, ITransientNotification paramITransientNotification)
        throws RemoteException;

    public abstract void enqueueNotification(String paramString, int paramInt, Notification paramNotification, int[] paramArrayOfInt)
        throws RemoteException;

    public abstract void enqueueNotificationWithTag(String paramString1, String paramString2, int paramInt, Notification paramNotification, int[] paramArrayOfInt)
        throws RemoteException;

    public abstract void enqueueToast(String paramString, ITransientNotification paramITransientNotification, int paramInt)
        throws RemoteException;

    public abstract void setNotificationsEnabledForPackage(String paramString, boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INotificationManager
    {
        private static final String DESCRIPTOR = "android.app.INotificationManager";
        static final int TRANSACTION_areNotificationsEnabledForPackage = 9;
        static final int TRANSACTION_cancelAllNotifications = 3;
        static final int TRANSACTION_cancelNotification = 2;
        static final int TRANSACTION_cancelNotificationWithTag = 7;
        static final int TRANSACTION_cancelToast = 5;
        static final int TRANSACTION_enqueueNotification = 1;
        static final int TRANSACTION_enqueueNotificationWithTag = 6;
        static final int TRANSACTION_enqueueToast = 4;
        static final int TRANSACTION_setNotificationsEnabledForPackage = 8;

        public Stub()
        {
            attachInterface(this, "android.app.INotificationManager");
        }

        public static INotificationManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.INotificationManager");
                if ((localIInterface != null) && ((localIInterface instanceof INotificationManager)))
                    localObject = (INotificationManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.app.INotificationManager");
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                String str4 = paramParcel1.readString();
                int m = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Notification localNotification2 = (Notification)Notification.CREATOR.createFromParcel(paramParcel1); ; localNotification2 = null)
                {
                    int[] arrayOfInt2 = paramParcel1.createIntArray();
                    enqueueNotification(str4, m, localNotification2, arrayOfInt2);
                    paramParcel2.writeNoException();
                    paramParcel2.writeIntArray(arrayOfInt2);
                    break;
                }
                paramParcel1.enforceInterface("android.app.INotificationManager");
                cancelNotification(paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                cancelAllNotifications(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                enqueueToast(paramParcel1.readString(), ITransientNotification.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                cancelToast(paramParcel1.readString(), ITransientNotification.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                String str2 = paramParcel1.readString();
                String str3 = paramParcel1.readString();
                int k = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Notification localNotification1 = (Notification)Notification.CREATOR.createFromParcel(paramParcel1); ; localNotification1 = null)
                {
                    int[] arrayOfInt1 = paramParcel1.createIntArray();
                    enqueueNotificationWithTag(str2, str3, k, localNotification1, arrayOfInt1);
                    paramParcel2.writeNoException();
                    paramParcel2.writeIntArray(arrayOfInt1);
                    break;
                }
                paramParcel1.enforceInterface("android.app.INotificationManager");
                cancelNotificationWithTag(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                String str1 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0)
                    i = j;
                setNotificationsEnabledForPackage(str1, i);
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.INotificationManager");
                boolean bool = areNotificationsEnabledForPackage(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool)
                    i = j;
                paramParcel2.writeInt(i);
            }
        }

        private static class Proxy
            implements INotificationManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean areNotificationsEnabledForPackage(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelAllNotifications(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void cancelNotification(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void cancelNotificationWithTag(String paramString1, String paramString2, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void cancelToast(String paramString, ITransientNotification paramITransientNotification)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    if (paramITransientNotification != null)
                    {
                        localIBinder = paramITransientNotification.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enqueueNotification(String paramString, int paramInt, Notification paramNotification, int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    if (paramNotification != null)
                    {
                        localParcel1.writeInt(1);
                        paramNotification.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeIntArray(paramArrayOfInt);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        localParcel2.readIntArray(paramArrayOfInt);
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enqueueNotificationWithTag(String paramString1, String paramString2, int paramInt, Notification paramNotification, int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    if (paramNotification != null)
                    {
                        localParcel1.writeInt(1);
                        paramNotification.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeIntArray(paramArrayOfInt);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        localParcel2.readIntArray(paramArrayOfInt);
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enqueueToast(String paramString, ITransientNotification paramITransientNotification, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    if (paramITransientNotification != null)
                    {
                        localIBinder = paramITransientNotification.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.INotificationManager";
            }

            public void setNotificationsEnabledForPackage(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.INotificationManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.INotificationManager
 * JD-Core Version:        0.6.2
 */