package android.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

public class TimePickerDialog extends AlertDialog
    implements DialogInterface.OnClickListener, TimePicker.OnTimeChangedListener
{
    private static final String HOUR = "hour";
    private static final String IS_24_HOUR = "is24hour";
    private static final String MINUTE = "minute";
    private final OnTimeSetListener mCallback;
    int mInitialHourOfDay;
    int mInitialMinute;
    boolean mIs24HourView;
    private final TimePicker mTimePicker;

    public TimePickerDialog(Context paramContext, int paramInt1, OnTimeSetListener paramOnTimeSetListener, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        super(paramContext, paramInt1);
        this.mCallback = paramOnTimeSetListener;
        this.mInitialHourOfDay = paramInt2;
        this.mInitialMinute = paramInt3;
        this.mIs24HourView = paramBoolean;
        setIcon(0);
        setTitle(17040416);
        Context localContext = getContext();
        setButton(-1, localContext.getText(17040419), this);
        View localView = ((LayoutInflater)localContext.getSystemService("layout_inflater")).inflate(17367229, null);
        setView(localView);
        this.mTimePicker = ((TimePicker)localView.findViewById(16909137));
        this.mTimePicker.setIs24HourView(Boolean.valueOf(this.mIs24HourView));
        this.mTimePicker.setCurrentHour(Integer.valueOf(this.mInitialHourOfDay));
        this.mTimePicker.setCurrentMinute(Integer.valueOf(this.mInitialMinute));
        this.mTimePicker.setOnTimeChangedListener(this);
    }

    public TimePickerDialog(Context paramContext, OnTimeSetListener paramOnTimeSetListener, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        this(paramContext, 0, paramOnTimeSetListener, paramInt1, paramInt2, paramBoolean);
    }

    private void tryNotifyTimeSet()
    {
        if (this.mCallback != null)
        {
            this.mTimePicker.clearFocus();
            this.mCallback.onTimeSet(this.mTimePicker, this.mTimePicker.getCurrentHour().intValue(), this.mTimePicker.getCurrentMinute().intValue());
        }
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        tryNotifyTimeSet();
    }

    public void onRestoreInstanceState(Bundle paramBundle)
    {
        super.onRestoreInstanceState(paramBundle);
        int i = paramBundle.getInt("hour");
        int j = paramBundle.getInt("minute");
        this.mTimePicker.setIs24HourView(Boolean.valueOf(paramBundle.getBoolean("is24hour")));
        this.mTimePicker.setCurrentHour(Integer.valueOf(i));
        this.mTimePicker.setCurrentMinute(Integer.valueOf(j));
    }

    public Bundle onSaveInstanceState()
    {
        Bundle localBundle = super.onSaveInstanceState();
        localBundle.putInt("hour", this.mTimePicker.getCurrentHour().intValue());
        localBundle.putInt("minute", this.mTimePicker.getCurrentMinute().intValue());
        localBundle.putBoolean("is24hour", this.mTimePicker.is24HourView());
        return localBundle;
    }

    protected void onStop()
    {
        tryNotifyTimeSet();
        super.onStop();
    }

    public void onTimeChanged(TimePicker paramTimePicker, int paramInt1, int paramInt2)
    {
    }

    public void updateTime(int paramInt1, int paramInt2)
    {
        this.mTimePicker.setCurrentHour(Integer.valueOf(paramInt1));
        this.mTimePicker.setCurrentMinute(Integer.valueOf(paramInt2));
    }

    public static abstract interface OnTimeSetListener
    {
        public abstract void onTimeSet(TimePicker paramTimePicker, int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.TimePickerDialog
 * JD-Core Version:        0.6.2
 */