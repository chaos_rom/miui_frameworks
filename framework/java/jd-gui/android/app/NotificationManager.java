package android.app;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class NotificationManager
{
    private static String TAG = "NotificationManager";
    private static boolean localLOGV = false;
    private static INotificationManager sService;
    private Context mContext;

    NotificationManager(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
    }

    public static INotificationManager getService()
    {
        if (sService != null);
        for (INotificationManager localINotificationManager = sService; ; localINotificationManager = sService)
        {
            return localINotificationManager;
            sService = INotificationManager.Stub.asInterface(ServiceManager.getService("notification"));
        }
    }

    public void cancel(int paramInt)
    {
        cancel(null, paramInt);
    }

    public void cancel(String paramString, int paramInt)
    {
        INotificationManager localINotificationManager = getService();
        String str = this.mContext.getPackageName();
        if (localLOGV)
            Log.v(TAG, str + ": cancel(" + paramInt + ")");
        try
        {
            localINotificationManager.cancelNotificationWithTag(str, paramString, paramInt);
            label65: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label65;
        }
    }

    public void cancelAll()
    {
        INotificationManager localINotificationManager = getService();
        String str = this.mContext.getPackageName();
        if (localLOGV)
            Log.v(TAG, str + ": cancelAll()");
        try
        {
            localINotificationManager.cancelAllNotifications(str);
            label51: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label51;
        }
    }

    public void notify(int paramInt, Notification paramNotification)
    {
        notify(null, paramInt, paramNotification);
    }

    public void notify(String paramString, int paramInt, Notification paramNotification)
    {
        int[] arrayOfInt = new int[1];
        INotificationManager localINotificationManager = getService();
        String str = this.mContext.getPackageName();
        if (localLOGV)
            Log.v(TAG, str + ": notify(" + paramInt + ", " + paramNotification + ")");
        try
        {
            localINotificationManager.enqueueNotificationWithTag(str, paramString, paramInt, paramNotification, arrayOfInt);
            if (paramInt != arrayOfInt[0])
                Log.w(TAG, "notify: id corrupted: sent " + paramInt + ", got back " + arrayOfInt[0]);
            label130: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label130;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.NotificationManager
 * JD-Core Version:        0.6.2
 */