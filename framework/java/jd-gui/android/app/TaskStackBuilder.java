package android.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;

public class TaskStackBuilder
{
    private static final String TAG = "TaskStackBuilder";
    private final ArrayList<Intent> mIntents = new ArrayList();
    private final Context mSourceContext;

    private TaskStackBuilder(Context paramContext)
    {
        this.mSourceContext = paramContext;
    }

    public static TaskStackBuilder create(Context paramContext)
    {
        return new TaskStackBuilder(paramContext);
    }

    public TaskStackBuilder addNextIntent(Intent paramIntent)
    {
        this.mIntents.add(paramIntent);
        return this;
    }

    public TaskStackBuilder addNextIntentWithParentStack(Intent paramIntent)
    {
        ComponentName localComponentName = paramIntent.getComponent();
        if (localComponentName == null)
            localComponentName = paramIntent.resolveActivity(this.mSourceContext.getPackageManager());
        if (localComponentName != null)
            addParentStack(localComponentName);
        addNextIntent(paramIntent);
        return this;
    }

    public TaskStackBuilder addParentStack(Activity paramActivity)
    {
        int i = this.mIntents.size();
        Object localObject = paramActivity.getParentActivityIntent();
        PackageManager localPackageManager = paramActivity.getPackageManager();
        while (localObject != null)
        {
            this.mIntents.add(i, localObject);
            try
            {
                String str = localPackageManager.getActivityInfo(((Intent)localObject).getComponent(), 0).parentActivityName;
                if (str != null)
                {
                    Intent localIntent = new Intent().setComponent(new ComponentName(this.mSourceContext, str));
                    localObject = localIntent;
                }
                else
                {
                    localObject = null;
                }
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
                throw new IllegalArgumentException(localNameNotFoundException);
            }
        }
        return this;
    }

    public TaskStackBuilder addParentStack(ComponentName paramComponentName)
    {
        int i = this.mIntents.size();
        PackageManager localPackageManager = this.mSourceContext.getPackageManager();
        try
        {
            ActivityInfo localActivityInfo = localPackageManager.getActivityInfo(paramComponentName, 0);
            for (String str = localActivityInfo.parentActivityName; str != null; str = localActivityInfo.parentActivityName)
            {
                Intent localIntent = new Intent().setComponent(new ComponentName(localActivityInfo.packageName, str));
                this.mIntents.add(i, localIntent);
                localActivityInfo = localPackageManager.getActivityInfo(localIntent.getComponent(), 0);
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(localNameNotFoundException);
        }
        return this;
    }

    public TaskStackBuilder addParentStack(Class<?> paramClass)
    {
        int i = this.mIntents.size();
        PackageManager localPackageManager = this.mSourceContext.getPackageManager();
        try
        {
            Intent localIntent;
            for (String str = localPackageManager.getActivityInfo(new ComponentName(this.mSourceContext, paramClass), 0).parentActivityName; str != null; str = localPackageManager.getActivityInfo(localIntent.getComponent(), 0).parentActivityName)
            {
                localIntent = new Intent().setComponent(new ComponentName(this.mSourceContext, str));
                this.mIntents.add(i, localIntent);
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(localNameNotFoundException);
        }
        return this;
    }

    public Intent editIntentAt(int paramInt)
    {
        return (Intent)this.mIntents.get(paramInt);
    }

    public int getIntentCount()
    {
        return this.mIntents.size();
    }

    public Intent[] getIntents()
    {
        return (Intent[])this.mIntents.toArray(new Intent[this.mIntents.size()]);
    }

    public PendingIntent getPendingIntent(int paramInt1, int paramInt2)
    {
        return getPendingIntent(paramInt1, paramInt2, null);
    }

    public PendingIntent getPendingIntent(int paramInt1, int paramInt2, Bundle paramBundle)
    {
        if (this.mIntents.isEmpty())
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot getPendingIntent");
        Intent[] arrayOfIntent = (Intent[])this.mIntents.toArray(new Intent[this.mIntents.size()]);
        arrayOfIntent[0].addFlags(268484608);
        return PendingIntent.getActivities(this.mSourceContext, paramInt1, arrayOfIntent, paramInt2, paramBundle);
    }

    public void startActivities()
    {
        startActivities(null);
    }

    public void startActivities(Bundle paramBundle)
    {
        if (this.mIntents.isEmpty())
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        Intent[] arrayOfIntent = (Intent[])this.mIntents.toArray(new Intent[this.mIntents.size()]);
        arrayOfIntent[0].addFlags(268484608);
        this.mSourceContext.startActivities(arrayOfIntent, paramBundle);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.TaskStackBuilder
 * JD-Core Version:        0.6.2
 */