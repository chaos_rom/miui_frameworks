package android.app.backup;

import android.app.IBackupAgent.Stub;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.ApplicationInfo;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import libcore.io.ErrnoException;
import libcore.io.Libcore;
import libcore.io.Os;
import libcore.io.OsConstants;
import libcore.io.StructStat;

public abstract class BackupAgent extends ContextWrapper
{
    private static final boolean DEBUG = true;
    private static final String TAG = "BackupAgent";
    public static final int TYPE_DIRECTORY = 2;
    public static final int TYPE_EOF = 0;
    public static final int TYPE_FILE = 1;
    public static final int TYPE_SYMLINK = 3;
    private final IBinder mBinder = new BackupServiceBinder(null).asBinder();

    public BackupAgent()
    {
        super(null);
    }

    public void attach(Context paramContext)
    {
        attachBaseContext(paramContext);
    }

    public final void fullBackupFile(File paramFile, FullBackupDataOutput paramFullBackupDataOutput)
    {
        ApplicationInfo localApplicationInfo = getApplicationInfo();
        try
        {
            str1 = new File(localApplicationInfo.dataDir).getCanonicalPath();
            str2 = getFilesDir().getCanonicalPath();
            str3 = getDatabasePath("foo").getParentFile().getCanonicalPath();
            str4 = getSharedPrefsFile("foo").getParentFile().getCanonicalPath();
            String str5 = getCacheDir().getCanonicalPath();
            if (localApplicationInfo.nativeLibraryDir == null);
            String str6;
            for (Object localObject = null; ; localObject = str6)
            {
                str7 = paramFile.getCanonicalPath();
                if ((!str7.startsWith(str5)) && (!str7.startsWith((String)localObject)))
                    break;
                Log.w("BackupAgent", "lib and cache files are not backed up");
                return;
                str6 = new File(localApplicationInfo.nativeLibraryDir).getCanonicalPath();
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                String str1;
                String str2;
                String str3;
                String str4;
                String str7;
                Log.w("BackupAgent", "Unable to obtain canonical paths");
                continue;
                String str8;
                String str9;
                if (str7.startsWith(str3))
                {
                    str8 = "db";
                    str9 = str3;
                }
                while (true)
                {
                    Log.i("BackupAgent", "backupFile() of " + str7 + " => domain=" + str8 + " rootpath=" + str9);
                    FullBackup.backupToTar(getPackageName(), str8, null, str9, str7, paramFullBackupDataOutput.getData());
                    break;
                    if (str7.startsWith(str4))
                    {
                        str8 = "sp";
                        str9 = str4;
                    }
                    else if (str7.startsWith(str2))
                    {
                        str8 = "f";
                        str9 = str2;
                    }
                    else
                    {
                        if (!str7.startsWith(str1))
                            break label297;
                        str8 = "r";
                        str9 = str1;
                    }
                }
                label297: Log.w("BackupAgent", "File " + str7 + " is in an unsupported location; skipping");
            }
        }
    }

    protected final void fullBackupFileTree(String paramString1, String paramString2, String paramString3, HashSet<String> paramHashSet, FullBackupDataOutput paramFullBackupDataOutput)
    {
        File localFile1 = new File(paramString3);
        if (localFile1.exists())
        {
            LinkedList localLinkedList = new LinkedList();
            localLinkedList.add(localFile1);
            while (localLinkedList.size() > 0)
            {
                File localFile2 = (File)localLinkedList.remove(0);
                String str;
                try
                {
                    str = localFile2.getCanonicalPath();
                    if ((paramHashSet != null) && (paramHashSet.contains(str)))
                        continue;
                    localStructStat = Libcore.os.lstat(str);
                    if (OsConstants.S_ISLNK(localStructStat.st_mode))
                        Log.i("BackupAgent", "Symlink (skipping)!: " + localFile2);
                }
                catch (IOException localIOException)
                {
                    StructStat localStructStat;
                    Log.w("BackupAgent", "Error canonicalizing path of " + localFile2);
                    continue;
                    if (!OsConstants.S_ISDIR(localStructStat.st_mode))
                        break label255;
                    File[] arrayOfFile = localFile2.listFiles();
                    if (arrayOfFile == null)
                        break label255;
                    int i = arrayOfFile.length;
                    for (int j = 0; j < i; j++)
                        localLinkedList.add(0, arrayOfFile[j]);
                }
                catch (ErrnoException localErrnoException)
                {
                    Log.w("BackupAgent", "Error scanning file " + localFile2 + " : " + localErrnoException);
                }
                continue;
                label255: FullBackup.backupToTar(paramString1, paramString2, null, paramString3, str, paramFullBackupDataOutput.getData());
            }
        }
    }

    public abstract void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
        throws IOException;

    public final IBinder onBind()
    {
        return this.mBinder;
    }

    public void onCreate()
    {
    }

    public void onDestroy()
    {
    }

    public void onFullBackup(FullBackupDataOutput paramFullBackupDataOutput)
        throws IOException
    {
        ApplicationInfo localApplicationInfo = getApplicationInfo();
        String str1 = new File(localApplicationInfo.dataDir).getCanonicalPath();
        String str2 = getFilesDir().getCanonicalPath();
        String str3 = getDatabasePath("foo").getParentFile().getCanonicalPath();
        String str4 = getSharedPrefsFile("foo").getParentFile().getCanonicalPath();
        String str5 = getCacheDir().getCanonicalPath();
        if (localApplicationInfo.nativeLibraryDir != null);
        for (String str6 = new File(localApplicationInfo.nativeLibraryDir).getCanonicalPath(); ; str6 = null)
        {
            HashSet localHashSet = new HashSet();
            String str7 = getPackageName();
            if (str6 != null)
                localHashSet.add(str6);
            localHashSet.add(str5);
            localHashSet.add(str3);
            localHashSet.add(str4);
            localHashSet.add(str2);
            fullBackupFileTree(str7, "r", str1, localHashSet, paramFullBackupDataOutput);
            localHashSet.add(str1);
            localHashSet.remove(str2);
            fullBackupFileTree(str7, "f", str2, localHashSet, paramFullBackupDataOutput);
            localHashSet.add(str2);
            localHashSet.remove(str3);
            fullBackupFileTree(str7, "db", str3, localHashSet, paramFullBackupDataOutput);
            localHashSet.add(str3);
            localHashSet.remove(str4);
            fullBackupFileTree(str7, "sp", str4, localHashSet, paramFullBackupDataOutput);
            return;
        }
    }

    public abstract void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor)
        throws IOException;

    protected void onRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt, String paramString1, String paramString2, long paramLong2, long paramLong3)
        throws IOException
    {
        String str = null;
        Log.d("BackupAgent", "onRestoreFile() size=" + paramLong1 + " type=" + paramInt + " domain=" + paramString1 + " relpath=" + paramString2 + " mode=" + paramLong2 + " mtime=" + paramLong3);
        if (paramString1.equals("f"))
        {
            str = getFilesDir().getCanonicalPath();
            if (str == null)
                break label328;
            File localFile = new File(str, paramString2);
            Log.i("BackupAgent", "[" + paramString1 + " : " + paramString2 + "] mapped to " + localFile.getPath());
            onRestoreFile(paramParcelFileDescriptor, paramLong1, localFile, paramInt, paramLong2, paramLong3);
        }
        while (true)
        {
            return;
            if (paramString1.equals("db"))
            {
                str = getDatabasePath("foo").getParentFile().getCanonicalPath();
                break;
            }
            if (paramString1.equals("r"))
            {
                str = new File(getApplicationInfo().dataDir).getCanonicalPath();
                break;
            }
            if (paramString1.equals("sp"))
            {
                str = getSharedPrefsFile("foo").getParentFile().getCanonicalPath();
                break;
            }
            if (paramString1.equals("c"))
            {
                str = getCacheDir().getCanonicalPath();
                break;
            }
            Log.i("BackupAgent", "Data restored from non-app domain " + paramString1 + ", ignoring");
            break;
            label328: Log.i("BackupAgent", "[ skipping data from unsupported domain " + paramString1 + "]");
            FullBackup.restoreFile(paramParcelFileDescriptor, paramLong1, paramInt, paramLong2, paramLong3, null);
        }
    }

    public void onRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, File paramFile, int paramInt, long paramLong2, long paramLong3)
        throws IOException
    {
        FullBackup.restoreFile(paramParcelFileDescriptor, paramLong1, paramInt, paramLong2, paramLong3, paramFile);
    }

    private class BackupServiceBinder extends IBackupAgent.Stub
    {
        private static final String TAG = "BackupServiceBinder";

        private BackupServiceBinder()
        {
        }

        // ERROR //
        public void doBackup(ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, ParcelFileDescriptor paramParcelFileDescriptor3, int paramInt, IBackupManager paramIBackupManager)
            throws android.os.RemoteException
        {
            // Byte code:
            //     0: invokestatic 36	android/os/Binder:clearCallingIdentity	()J
            //     3: lstore 6
            //     5: ldc 10
            //     7: ldc 38
            //     9: invokestatic 44	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     12: pop
            //     13: new 46	android/app/backup/BackupDataOutput
            //     16: dup
            //     17: aload_2
            //     18: invokevirtual 52	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     21: invokespecial 55	android/app/backup/BackupDataOutput:<init>	(Ljava/io/FileDescriptor;)V
            //     24: astore 9
            //     26: aload_0
            //     27: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     30: aload_1
            //     31: aload 9
            //     33: aload_3
            //     34: invokevirtual 59	android/app/backup/BackupAgent:onBackup	(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
            //     37: lload 6
            //     39: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     42: aload 5
            //     44: iload 4
            //     46: invokeinterface 69 2 0
            //     51: return
            //     52: astore 14
            //     54: ldc 10
            //     56: new 71	java/lang/StringBuilder
            //     59: dup
            //     60: invokespecial 72	java/lang/StringBuilder:<init>	()V
            //     63: ldc 74
            //     65: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     68: aload_0
            //     69: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     72: invokevirtual 84	java/lang/Object:getClass	()Ljava/lang/Class;
            //     75: invokevirtual 90	java/lang/Class:getName	()Ljava/lang/String;
            //     78: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     81: ldc 92
            //     83: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     86: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     89: aload 14
            //     91: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     94: pop
            //     95: new 30	java/lang/RuntimeException
            //     98: dup
            //     99: aload 14
            //     101: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
            //     104: athrow
            //     105: astore 12
            //     107: lload 6
            //     109: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     112: aload 5
            //     114: iload 4
            //     116: invokeinterface 69 2 0
            //     121: aload 12
            //     123: athrow
            //     124: astore 10
            //     126: ldc 10
            //     128: new 71	java/lang/StringBuilder
            //     131: dup
            //     132: invokespecial 72	java/lang/StringBuilder:<init>	()V
            //     135: ldc 74
            //     137: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     140: aload_0
            //     141: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     144: invokevirtual 84	java/lang/Object:getClass	()Ljava/lang/Class;
            //     147: invokevirtual 90	java/lang/Class:getName	()Ljava/lang/String;
            //     150: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     153: ldc 92
            //     155: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     158: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     161: aload 10
            //     163: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     166: pop
            //     167: aload 10
            //     169: athrow
            //     170: astore 13
            //     172: goto -51 -> 121
            //     175: astore 16
            //     177: goto -126 -> 51
            //
            // Exception table:
            //     from	to	target	type
            //     26	37	52	java/io/IOException
            //     26	37	105	finally
            //     54	105	105	finally
            //     126	170	105	finally
            //     26	37	124	java/lang/RuntimeException
            //     112	121	170	android/os/RemoteException
            //     42	51	175	android/os/RemoteException
        }

        // ERROR //
        public void doFullBackup(ParcelFileDescriptor paramParcelFileDescriptor, int paramInt, IBackupManager paramIBackupManager)
        {
            // Byte code:
            //     0: invokestatic 36	android/os/Binder:clearCallingIdentity	()J
            //     3: lstore 4
            //     5: ldc 10
            //     7: ldc 106
            //     9: invokestatic 44	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     12: pop
            //     13: aload_0
            //     14: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     17: new 108	android/app/backup/FullBackupDataOutput
            //     20: dup
            //     21: aload_1
            //     22: invokespecial 111	android/app/backup/FullBackupDataOutput:<init>	(Landroid/os/ParcelFileDescriptor;)V
            //     25: invokevirtual 115	android/app/backup/BackupAgent:onFullBackup	(Landroid/app/backup/FullBackupDataOutput;)V
            //     28: new 117	java/io/FileOutputStream
            //     31: dup
            //     32: aload_1
            //     33: invokevirtual 52	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     36: invokespecial 118	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
            //     39: iconst_4
            //     40: newarray byte
            //     42: invokevirtual 122	java/io/FileOutputStream:write	([B)V
            //     45: lload 4
            //     47: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     50: aload_3
            //     51: iload_2
            //     52: invokeinterface 69 2 0
            //     57: return
            //     58: astore 13
            //     60: ldc 10
            //     62: new 71	java/lang/StringBuilder
            //     65: dup
            //     66: invokespecial 72	java/lang/StringBuilder:<init>	()V
            //     69: ldc 74
            //     71: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     74: aload_0
            //     75: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     78: invokevirtual 84	java/lang/Object:getClass	()Ljava/lang/Class;
            //     81: invokevirtual 90	java/lang/Class:getName	()Ljava/lang/String;
            //     84: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     87: ldc 92
            //     89: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     92: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     95: aload 13
            //     97: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     100: pop
            //     101: new 30	java/lang/RuntimeException
            //     104: dup
            //     105: aload 13
            //     107: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
            //     110: athrow
            //     111: astore 9
            //     113: new 117	java/io/FileOutputStream
            //     116: dup
            //     117: aload_1
            //     118: invokevirtual 52	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     121: invokespecial 118	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
            //     124: iconst_4
            //     125: newarray byte
            //     127: invokevirtual 122	java/io/FileOutputStream:write	([B)V
            //     130: lload 4
            //     132: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     135: aload_3
            //     136: iload_2
            //     137: invokeinterface 69 2 0
            //     142: aload 9
            //     144: athrow
            //     145: astore 7
            //     147: ldc 10
            //     149: new 71	java/lang/StringBuilder
            //     152: dup
            //     153: invokespecial 72	java/lang/StringBuilder:<init>	()V
            //     156: ldc 74
            //     158: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     161: aload_0
            //     162: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     165: invokevirtual 84	java/lang/Object:getClass	()Ljava/lang/Class;
            //     168: invokevirtual 90	java/lang/Class:getName	()Ljava/lang/String;
            //     171: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     174: ldc 92
            //     176: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     179: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     182: aload 7
            //     184: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     187: pop
            //     188: aload 7
            //     190: athrow
            //     191: astore 10
            //     193: ldc 10
            //     195: ldc 124
            //     197: invokestatic 127	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     200: pop
            //     201: goto -71 -> 130
            //     204: astore 12
            //     206: goto -64 -> 142
            //     209: astore 15
            //     211: ldc 10
            //     213: ldc 124
            //     215: invokestatic 127	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     218: pop
            //     219: goto -174 -> 45
            //     222: astore 17
            //     224: goto -167 -> 57
            //
            // Exception table:
            //     from	to	target	type
            //     13	28	58	java/io/IOException
            //     13	28	111	finally
            //     60	111	111	finally
            //     147	191	111	finally
            //     13	28	145	java/lang/RuntimeException
            //     113	130	191	java/io/IOException
            //     135	142	204	android/os/RemoteException
            //     28	45	209	java/io/IOException
            //     50	57	222	android/os/RemoteException
        }

        // ERROR //
        public void doRestore(ParcelFileDescriptor paramParcelFileDescriptor1, int paramInt1, ParcelFileDescriptor paramParcelFileDescriptor2, int paramInt2, IBackupManager paramIBackupManager)
            throws android.os.RemoteException
        {
            // Byte code:
            //     0: invokestatic 36	android/os/Binder:clearCallingIdentity	()J
            //     3: lstore 6
            //     5: ldc 10
            //     7: ldc 131
            //     9: invokestatic 44	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     12: pop
            //     13: new 133	android/app/backup/BackupDataInput
            //     16: dup
            //     17: aload_1
            //     18: invokevirtual 52	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     21: invokespecial 134	android/app/backup/BackupDataInput:<init>	(Ljava/io/FileDescriptor;)V
            //     24: astore 9
            //     26: aload_0
            //     27: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     30: aload 9
            //     32: iload_2
            //     33: aload_3
            //     34: invokevirtual 138	android/app/backup/BackupAgent:onRestore	(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
            //     37: lload 6
            //     39: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     42: aload 5
            //     44: iload 4
            //     46: invokeinterface 69 2 0
            //     51: return
            //     52: astore 14
            //     54: ldc 10
            //     56: new 71	java/lang/StringBuilder
            //     59: dup
            //     60: invokespecial 72	java/lang/StringBuilder:<init>	()V
            //     63: ldc 140
            //     65: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     68: aload_0
            //     69: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     72: invokevirtual 84	java/lang/Object:getClass	()Ljava/lang/Class;
            //     75: invokevirtual 90	java/lang/Class:getName	()Ljava/lang/String;
            //     78: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     81: ldc 92
            //     83: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     86: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     89: aload 14
            //     91: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     94: pop
            //     95: new 30	java/lang/RuntimeException
            //     98: dup
            //     99: aload 14
            //     101: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
            //     104: athrow
            //     105: astore 12
            //     107: lload 6
            //     109: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     112: aload 5
            //     114: iload 4
            //     116: invokeinterface 69 2 0
            //     121: aload 12
            //     123: athrow
            //     124: astore 10
            //     126: ldc 10
            //     128: new 71	java/lang/StringBuilder
            //     131: dup
            //     132: invokespecial 72	java/lang/StringBuilder:<init>	()V
            //     135: ldc 140
            //     137: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     140: aload_0
            //     141: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     144: invokevirtual 84	java/lang/Object:getClass	()Ljava/lang/Class;
            //     147: invokevirtual 90	java/lang/Class:getName	()Ljava/lang/String;
            //     150: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     153: ldc 92
            //     155: invokevirtual 78	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     158: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     161: aload 10
            //     163: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     166: pop
            //     167: aload 10
            //     169: athrow
            //     170: astore 13
            //     172: goto -51 -> 121
            //     175: astore 16
            //     177: goto -126 -> 51
            //
            // Exception table:
            //     from	to	target	type
            //     26	37	52	java/io/IOException
            //     26	37	105	finally
            //     54	105	105	finally
            //     126	170	105	finally
            //     26	37	124	java/lang/RuntimeException
            //     112	121	170	android/os/RemoteException
            //     42	51	175	android/os/RemoteException
        }

        // ERROR //
        public void doRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt1, String paramString1, String paramString2, long paramLong2, long paramLong3, int paramInt2, IBackupManager paramIBackupManager)
            throws android.os.RemoteException
        {
            // Byte code:
            //     0: invokestatic 36	android/os/Binder:clearCallingIdentity	()J
            //     3: lstore 13
            //     5: aload_0
            //     6: getfield 16	android/app/backup/BackupAgent$BackupServiceBinder:this$0	Landroid/app/backup/BackupAgent;
            //     9: aload_1
            //     10: lload_2
            //     11: iload 4
            //     13: aload 5
            //     15: aload 6
            //     17: lload 7
            //     19: lload 9
            //     21: invokevirtual 146	android/app/backup/BackupAgent:onRestoreFile	(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJ)V
            //     24: lload 13
            //     26: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     29: aload 12
            //     31: iload 11
            //     33: invokeinterface 69 2 0
            //     38: return
            //     39: astore 17
            //     41: new 30	java/lang/RuntimeException
            //     44: dup
            //     45: aload 17
            //     47: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
            //     50: athrow
            //     51: astore 15
            //     53: lload 13
            //     55: invokestatic 63	android/os/Binder:restoreCallingIdentity	(J)V
            //     58: aload 12
            //     60: iload 11
            //     62: invokeinterface 69 2 0
            //     67: aload 15
            //     69: athrow
            //     70: astore 16
            //     72: goto -5 -> 67
            //     75: astore 18
            //     77: goto -39 -> 38
            //
            // Exception table:
            //     from	to	target	type
            //     5	24	39	java/io/IOException
            //     5	24	51	finally
            //     41	51	51	finally
            //     58	67	70	android/os/RemoteException
            //     29	38	75	android/os/RemoteException
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.BackupAgent
 * JD-Core Version:        0.6.2
 */