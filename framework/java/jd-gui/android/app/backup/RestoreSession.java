package android.app.backup;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

public class RestoreSession
{
    static final String TAG = "RestoreSession";
    IRestoreSession mBinder;
    final Context mContext;
    RestoreObserverWrapper mObserver = null;

    RestoreSession(Context paramContext, IRestoreSession paramIRestoreSession)
    {
        this.mContext = paramContext;
        this.mBinder = paramIRestoreSession;
    }

    public void endRestoreSession()
    {
        try
        {
            this.mBinder.endRestoreSession();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.d("RestoreSession", "Can't contact server to get available sets");
        }
        finally
        {
            this.mBinder = null;
        }
    }

    public int getAvailableRestoreSets(RestoreObserver paramRestoreObserver)
    {
        int i = -1;
        RestoreObserverWrapper localRestoreObserverWrapper = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
        try
        {
            int j = this.mBinder.getAvailableRestoreSets(localRestoreObserverWrapper);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.d("RestoreSession", "Can't contact server to get available sets");
        }
    }

    public int restoreAll(long paramLong, RestoreObserver paramRestoreObserver)
    {
        int i = -1;
        int j;
        if (this.mObserver != null)
        {
            Log.d("RestoreSession", "restoreAll() called during active restore");
            j = -1;
        }
        while (true)
        {
            return j;
            this.mObserver = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
            try
            {
                int k = this.mBinder.restoreAll(paramLong, this.mObserver);
                i = k;
                j = i;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.d("RestoreSession", "Can't contact server to restore");
            }
        }
    }

    public int restorePackage(String paramString, RestoreObserver paramRestoreObserver)
    {
        int i = -1;
        int j;
        if (this.mObserver != null)
        {
            Log.d("RestoreSession", "restorePackage() called during active restore");
            j = -1;
        }
        while (true)
        {
            return j;
            this.mObserver = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
            try
            {
                int k = this.mBinder.restorePackage(paramString, this.mObserver);
                i = k;
                j = i;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.d("RestoreSession", "Can't contact server to restore package");
            }
        }
    }

    public int restoreSome(long paramLong, RestoreObserver paramRestoreObserver, String[] paramArrayOfString)
    {
        int i = -1;
        int j;
        if (this.mObserver != null)
        {
            Log.d("RestoreSession", "restoreAll() called during active restore");
            j = -1;
        }
        while (true)
        {
            return j;
            this.mObserver = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
            try
            {
                int k = this.mBinder.restoreSome(paramLong, this.mObserver, paramArrayOfString);
                i = k;
                j = i;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.d("RestoreSession", "Can't contact server to restore packages");
            }
        }
    }

    private class RestoreObserverWrapper extends IRestoreObserver.Stub
    {
        static final int MSG_RESTORE_FINISHED = 3;
        static final int MSG_RESTORE_SETS_AVAILABLE = 4;
        static final int MSG_RESTORE_STARTING = 1;
        static final int MSG_UPDATE = 2;
        final RestoreObserver mAppObserver;
        final Handler mHandler;

        RestoreObserverWrapper(Context paramRestoreObserver, RestoreObserver arg3)
        {
            this.mHandler = new Handler(paramRestoreObserver.getMainLooper())
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    switch (paramAnonymousMessage.what)
                    {
                    default:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    }
                    while (true)
                    {
                        return;
                        RestoreSession.RestoreObserverWrapper.this.mAppObserver.restoreStarting(paramAnonymousMessage.arg1);
                        continue;
                        RestoreSession.RestoreObserverWrapper.this.mAppObserver.onUpdate(paramAnonymousMessage.arg1, (String)paramAnonymousMessage.obj);
                        continue;
                        RestoreSession.RestoreObserverWrapper.this.mAppObserver.restoreFinished(paramAnonymousMessage.arg1);
                        continue;
                        RestoreSession.RestoreObserverWrapper.this.mAppObserver.restoreSetsAvailable((RestoreSet[])paramAnonymousMessage.obj);
                    }
                }
            };
            Object localObject;
            this.mAppObserver = localObject;
        }

        public void onUpdate(int paramInt, String paramString)
        {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(2, paramInt, 0, paramString));
        }

        public void restoreFinished(int paramInt)
        {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, paramInt, 0));
        }

        public void restoreSetsAvailable(RestoreSet[] paramArrayOfRestoreSet)
        {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(4, paramArrayOfRestoreSet));
        }

        public void restoreStarting(int paramInt)
        {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, paramInt, 0));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.RestoreSession
 * JD-Core Version:        0.6.2
 */