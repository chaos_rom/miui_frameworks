package android.app.backup;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import libcore.io.ErrnoException;
import libcore.io.Libcore;
import libcore.io.Os;

public class FullBackup
{
    public static final String APK_TREE_TOKEN = "a";
    public static final String APPS_PREFIX = "apps/";
    public static final String CACHE_TREE_TOKEN = "c";
    public static final String CONF_TOKEN_INTENT_EXTRA = "conftoken";
    public static final String DATABASE_TREE_TOKEN = "db";
    public static final String DATA_TREE_TOKEN = "f";
    public static final String FULL_BACKUP_INTENT_ACTION = "fullback";
    public static final String FULL_RESTORE_INTENT_ACTION = "fullrest";
    public static final String OBB_TREE_TOKEN = "obb";
    public static final String ROOT_TREE_TOKEN = "r";
    public static final String SHAREDPREFS_TREE_TOKEN = "sp";
    public static final String SHARED_PREFIX = "shared/";
    public static final String SHARED_STORAGE_TOKEN = "shared";
    static final String TAG = "FullBackup";

    public static native int backupToTar(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, BackupDataOutput paramBackupDataOutput);

    public static void restoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt, long paramLong2, long paramLong3, File paramFile)
        throws IOException
    {
        if (paramInt == 2)
            if (paramFile != null)
                paramFile.mkdirs();
        while (true)
        {
            long l2;
            if ((paramLong2 >= 0L) && (paramFile != null))
                l2 = paramLong2 & 0x1C0;
            try
            {
                Libcore.os.chmod(paramFile.getPath(), (int)l2);
                paramFile.setLastModified(paramLong3);
                return;
                Object localObject = null;
                if (paramFile != null);
                try
                {
                    File localFile = paramFile.getParentFile();
                    if (!localFile.exists())
                        localFile.mkdirs();
                    FileOutputStream localFileOutputStream = new FileOutputStream(paramFile);
                    localObject = localFileOutputStream;
                    arrayOfByte = new byte[32768];
                    long l1 = paramLong1;
                    FileInputStream localFileInputStream = new FileInputStream(paramParcelFileDescriptor.getFileDescriptor());
                    if (paramLong1 > 0L)
                    {
                        if (paramLong1 > arrayOfByte.length)
                        {
                            i = arrayOfByte.length;
                            j = localFileInputStream.read(arrayOfByte, 0, i);
                            if (j > 0)
                                break label257;
                            Log.w("FullBackup", "Incomplete read: expected " + paramLong1 + " but got " + (l1 - paramLong1));
                        }
                    }
                    else
                    {
                        if (localObject == null)
                            continue;
                        localObject.close();
                    }
                }
                catch (IOException localIOException2)
                {
                    while (true)
                    {
                        byte[] arrayOfByte;
                        int j;
                        Log.e("FullBackup", "Unable to create/open file " + paramFile.getPath(), localIOException2);
                        continue;
                        int i = (int)paramLong1;
                        continue;
                        label257: if (localObject != null);
                        try
                        {
                            localObject.write(arrayOfByte, 0, j);
                            paramLong1 -= j;
                        }
                        catch (IOException localIOException1)
                        {
                            while (true)
                            {
                                Log.e("FullBackup", "Unable to write to file " + paramFile.getPath(), localIOException1);
                                localObject.close();
                                localObject = null;
                                paramFile.delete();
                            }
                        }
                    }
                }
            }
            catch (ErrnoException localErrnoException)
            {
                while (true)
                    localErrnoException.rethrowAsIOException();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.FullBackup
 * JD-Core Version:        0.6.2
 */