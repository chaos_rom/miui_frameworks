package android.app.backup;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class BackupManager
{
    private static final String TAG = "BackupManager";
    private static IBackupManager sService;
    private Context mContext;

    public BackupManager(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private static void checkServiceBinder()
    {
        if (sService == null)
            sService = IBackupManager.Stub.asInterface(ServiceManager.getService("backup"));
    }

    public static void dataChanged(String paramString)
    {
        checkServiceBinder();
        if (sService != null);
        try
        {
            sService.dataChanged(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.d("BackupManager", "dataChanged(pkg) couldn't connect");
        }
    }

    public RestoreSession beginRestoreSession()
    {
        Object localObject = null;
        checkServiceBinder();
        if (sService != null);
        try
        {
            IRestoreSession localIRestoreSession = sService.beginRestoreSession(null, null);
            if (localIRestoreSession != null)
            {
                RestoreSession localRestoreSession = new RestoreSession(this.mContext, localIRestoreSession);
                localObject = localRestoreSession;
            }
            return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("BackupManager", "beginRestoreSession() couldn't connect");
        }
    }

    public void dataChanged()
    {
        checkServiceBinder();
        if (sService != null);
        try
        {
            sService.dataChanged(this.mContext.getPackageName());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.d("BackupManager", "dataChanged() couldn't connect");
        }
    }

    // ERROR //
    public int requestRestore(RestoreObserver paramRestoreObserver)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_2
        //     3: invokestatic 42	android/app/backup/BackupManager:checkServiceBinder	()V
        //     6: getstatic 22	android/app/backup/BackupManager:sService	Landroid/app/backup/IBackupManager;
        //     9: ifnull +72 -> 81
        //     12: aconst_null
        //     13: astore_3
        //     14: getstatic 22	android/app/backup/BackupManager:sService	Landroid/app/backup/IBackupManager;
        //     17: aload_0
        //     18: getfield 19	android/app/backup/BackupManager:mContext	Landroid/content/Context;
        //     21: invokevirtual 75	android/content/Context:getPackageName	()Ljava/lang/String;
        //     24: aconst_null
        //     25: invokeinterface 59 3 0
        //     30: astore 7
        //     32: aload 7
        //     34: ifnull +39 -> 73
        //     37: new 61	android/app/backup/RestoreSession
        //     40: dup
        //     41: aload_0
        //     42: getfield 19	android/app/backup/BackupManager:mContext	Landroid/content/Context;
        //     45: aload 7
        //     47: invokespecial 64	android/app/backup/RestoreSession:<init>	(Landroid/content/Context;Landroid/app/backup/IRestoreSession;)V
        //     50: astore 8
        //     52: aload 8
        //     54: aload_0
        //     55: getfield 19	android/app/backup/BackupManager:mContext	Landroid/content/Context;
        //     58: invokevirtual 75	android/content/Context:getPackageName	()Ljava/lang/String;
        //     61: aload_1
        //     62: invokevirtual 83	android/app/backup/RestoreSession:restorePackage	(Ljava/lang/String;Landroid/app/backup/RestoreObserver;)I
        //     65: istore 10
        //     67: iload 10
        //     69: istore_2
        //     70: aload 8
        //     72: astore_3
        //     73: aload_3
        //     74: ifnull +7 -> 81
        //     77: aload_3
        //     78: invokevirtual 86	android/app/backup/RestoreSession:endRestoreSession	()V
        //     81: iload_2
        //     82: ireturn
        //     83: astore 5
        //     85: ldc 8
        //     87: ldc 88
        //     89: invokestatic 69	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     92: pop
        //     93: aload_3
        //     94: ifnull -13 -> 81
        //     97: aload_3
        //     98: invokevirtual 86	android/app/backup/RestoreSession:endRestoreSession	()V
        //     101: goto -20 -> 81
        //     104: astore 4
        //     106: aload_3
        //     107: ifnull +7 -> 114
        //     110: aload_3
        //     111: invokevirtual 86	android/app/backup/RestoreSession:endRestoreSession	()V
        //     114: aload 4
        //     116: athrow
        //     117: astore 4
        //     119: aload 8
        //     121: astore_3
        //     122: goto -16 -> 106
        //     125: astore 9
        //     127: aload 8
        //     129: astore_3
        //     130: goto -45 -> 85
        //
        // Exception table:
        //     from	to	target	type
        //     14	52	83	android/os/RemoteException
        //     14	52	104	finally
        //     85	93	104	finally
        //     52	67	117	finally
        //     52	67	125	android/os/RemoteException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.BackupManager
 * JD-Core Version:        0.6.2
 */