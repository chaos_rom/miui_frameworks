package android.app.backup;

import android.app.QueuedWork;
import android.content.Context;
import android.os.ParcelFileDescriptor;
import java.io.File;

public class SharedPreferencesBackupHelper extends FileBackupHelperBase
    implements BackupHelper
{
    private static final boolean DEBUG = false;
    private static final String TAG = "SharedPreferencesBackupHelper";
    private Context mContext;
    private String[] mPrefGroups;

    public SharedPreferencesBackupHelper(Context paramContext, String[] paramArrayOfString)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mPrefGroups = paramArrayOfString;
    }

    public void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
    {
        Context localContext = this.mContext;
        QueuedWork.waitToFinish();
        String[] arrayOfString1 = this.mPrefGroups;
        int i = arrayOfString1.length;
        String[] arrayOfString2 = new String[i];
        for (int j = 0; j < i; j++)
            arrayOfString2[j] = localContext.getSharedPrefsFile(arrayOfString1[j]).getAbsolutePath();
        performBackup_checked(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2, arrayOfString2, arrayOfString1);
    }

    public void restoreEntity(BackupDataInputStream paramBackupDataInputStream)
    {
        Context localContext = this.mContext;
        String str = paramBackupDataInputStream.getKey();
        if (isKeyInList(str, this.mPrefGroups))
            writeFile(localContext.getSharedPrefsFile(str).getAbsoluteFile(), paramBackupDataInputStream);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.SharedPreferencesBackupHelper
 * JD-Core Version:        0.6.2
 */