package android.app.backup;

import android.os.ParcelFileDescriptor;

public class FullBackupDataOutput
{
    private BackupDataOutput mData;

    public FullBackupDataOutput(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        this.mData = new BackupDataOutput(paramParcelFileDescriptor.getFileDescriptor());
    }

    public BackupDataOutput getData()
    {
        return this.mData;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.FullBackupDataOutput
 * JD-Core Version:        0.6.2
 */