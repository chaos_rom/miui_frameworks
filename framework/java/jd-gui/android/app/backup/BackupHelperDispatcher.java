package android.app.backup;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class BackupHelperDispatcher
{
    private static final String TAG = "BackupHelperDispatcher";
    TreeMap<String, BackupHelper> mHelpers = new TreeMap();

    private static native int allocateHeader_native(Header paramHeader, FileDescriptor paramFileDescriptor);

    private void doOneBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2, Header paramHeader, BackupHelper paramBackupHelper)
        throws IOException
    {
        FileDescriptor localFileDescriptor = paramParcelFileDescriptor2.getFileDescriptor();
        int i = allocateHeader_native(paramHeader, localFileDescriptor);
        if (i < 0)
            throw new IOException("allocateHeader_native failed (error " + i + ")");
        paramBackupDataOutput.setKeyPrefix(paramHeader.keyPrefix);
        paramBackupHelper.performBackup(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2);
        int j = writeHeader_native(paramHeader, localFileDescriptor, i);
        if (j != 0)
            throw new IOException("writeHeader_native failed (error " + j + ")");
    }

    private static native int readHeader_native(Header paramHeader, FileDescriptor paramFileDescriptor);

    private static native int skipChunk_native(FileDescriptor paramFileDescriptor, int paramInt);

    private static native int writeHeader_native(Header paramHeader, FileDescriptor paramFileDescriptor, int paramInt);

    public void addHelper(String paramString, BackupHelper paramBackupHelper)
    {
        this.mHelpers.put(paramString, paramBackupHelper);
    }

    public void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
        throws IOException
    {
        Header localHeader = new Header(null);
        TreeMap localTreeMap = (TreeMap)this.mHelpers.clone();
        paramParcelFileDescriptor2.getFileDescriptor();
        if (paramParcelFileDescriptor1 != null)
        {
            FileDescriptor localFileDescriptor = paramParcelFileDescriptor1.getFileDescriptor();
            while (true)
            {
                int i = readHeader_native(localHeader, localFileDescriptor);
                if (i < 0)
                    break;
                if (i == 0)
                {
                    BackupHelper localBackupHelper = (BackupHelper)localTreeMap.get(localHeader.keyPrefix);
                    Log.d("BackupHelperDispatcher", "handling existing helper '" + localHeader.keyPrefix + "' " + localBackupHelper);
                    if (localBackupHelper != null)
                    {
                        doOneBackup(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2, localHeader, localBackupHelper);
                        localTreeMap.remove(localHeader.keyPrefix);
                    }
                    else
                    {
                        skipChunk_native(localFileDescriptor, localHeader.chunkSize);
                    }
                }
            }
        }
        Iterator localIterator = localTreeMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            localHeader.keyPrefix = ((String)localEntry.getKey());
            Log.d("BackupHelperDispatcher", "handling new helper '" + localHeader.keyPrefix + "'");
            doOneBackup(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2, localHeader, (BackupHelper)localEntry.getValue());
        }
    }

    public void performRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor)
        throws IOException
    {
        int i = 0;
        BackupDataInputStream localBackupDataInputStream = new BackupDataInputStream(paramBackupDataInput);
        if (paramBackupDataInput.readNextHeader())
        {
            String str1 = paramBackupDataInput.getKey();
            int j = str1.indexOf(':');
            if (j > 0)
            {
                String str2 = str1.substring(0, j);
                BackupHelper localBackupHelper = (BackupHelper)this.mHelpers.get(str2);
                if (localBackupHelper != null)
                {
                    localBackupDataInputStream.dataSize = paramBackupDataInput.getDataSize();
                    localBackupDataInputStream.key = str1.substring(j + 1);
                    localBackupHelper.restoreEntity(localBackupDataInputStream);
                }
            }
            while (true)
            {
                paramBackupDataInput.skipEntityData();
                break;
                if (i == 0)
                {
                    Log.w("BackupHelperDispatcher", "Couldn't find helper for: '" + str1 + "'");
                    i = 1;
                    continue;
                    if (i == 0)
                    {
                        Log.w("BackupHelperDispatcher", "Entity with no prefix: '" + str1 + "'");
                        i = 1;
                    }
                }
            }
        }
        Iterator localIterator = this.mHelpers.values().iterator();
        while (localIterator.hasNext())
            ((BackupHelper)localIterator.next()).writeNewStateDescription(paramParcelFileDescriptor);
    }

    private static class Header
    {
        int chunkSize;
        String keyPrefix;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.BackupHelperDispatcher
 * JD-Core Version:        0.6.2
 */