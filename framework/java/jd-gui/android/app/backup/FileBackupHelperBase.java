package android.app.backup;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;

class FileBackupHelperBase
{
    private static final String TAG = "FileBackupHelperBase";
    Context mContext;
    boolean mExceptionLogged;
    int mPtr = ctor();

    FileBackupHelperBase(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private static native int ctor();

    private static native void dtor(int paramInt);

    static void performBackup_checked(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2, String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        if (paramArrayOfString1.length == 0);
        int k;
        do
        {
            return;
            int i = paramArrayOfString1.length;
            for (int j = 0; j < i; j++)
            {
                String str = paramArrayOfString1[j];
                if (str.charAt(0) != '/')
                    throw new RuntimeException("files must have all absolute paths: " + str);
            }
            if (paramArrayOfString1.length != paramArrayOfString2.length)
                throw new RuntimeException("files.length=" + paramArrayOfString1.length + " keys.length=" + paramArrayOfString2.length);
            if (paramParcelFileDescriptor1 != null);
            FileDescriptor localFileDescriptor2;
            for (FileDescriptor localFileDescriptor1 = paramParcelFileDescriptor1.getFileDescriptor(); ; localFileDescriptor1 = null)
            {
                localFileDescriptor2 = paramParcelFileDescriptor2.getFileDescriptor();
                if (localFileDescriptor2 != null)
                    break;
                throw new NullPointerException();
            }
            k = performBackup_native(localFileDescriptor1, paramBackupDataOutput.mBackupWriter, localFileDescriptor2, paramArrayOfString1, paramArrayOfString2);
        }
        while (k == 0);
        throw new RuntimeException("Backup failed 0x" + Integer.toHexString(k));
    }

    private static native int performBackup_native(FileDescriptor paramFileDescriptor1, int paramInt, FileDescriptor paramFileDescriptor2, String[] paramArrayOfString1, String[] paramArrayOfString2);

    private static native int writeFile_native(int paramInt1, String paramString, int paramInt2);

    private static native int writeSnapshot_native(int paramInt, FileDescriptor paramFileDescriptor);

    protected void finalize()
        throws Throwable
    {
        try
        {
            dtor(this.mPtr);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    boolean isKeyInList(String paramString, String[] paramArrayOfString)
    {
        int i = paramArrayOfString.length;
        int j = 0;
        if (j < i)
            if (!paramArrayOfString[j].equals(paramString));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    boolean writeFile(File paramFile, BackupDataInputStream paramBackupDataInputStream)
    {
        boolean bool = true;
        paramFile.getParentFile().mkdirs();
        int i = writeFile_native(this.mPtr, paramFile.getAbsolutePath(), paramBackupDataInputStream.mData.mBackupReader);
        if ((i != 0) && (!this.mExceptionLogged))
        {
            Log.e("FileBackupHelperBase", "Failed restoring file '" + paramFile + "' for app '" + this.mContext.getPackageName() + "' result=0x" + Integer.toHexString(i));
            this.mExceptionLogged = bool;
        }
        if (i == 0);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public void writeNewStateDescription(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        writeSnapshot_native(this.mPtr, paramParcelFileDescriptor.getFileDescriptor());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.FileBackupHelperBase
 * JD-Core Version:        0.6.2
 */