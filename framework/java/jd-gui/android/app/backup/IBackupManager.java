package android.app.backup;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IBackupManager extends IInterface
{
    public abstract void acknowledgeFullBackupOrRestore(int paramInt, boolean paramBoolean, String paramString1, String paramString2, IFullBackupRestoreObserver paramIFullBackupRestoreObserver)
        throws RemoteException;

    public abstract void agentConnected(String paramString, IBinder paramIBinder)
        throws RemoteException;

    public abstract void agentDisconnected(String paramString)
        throws RemoteException;

    public abstract void backupNow()
        throws RemoteException;

    public abstract IRestoreSession beginRestoreSession(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void clearBackupData(String paramString)
        throws RemoteException;

    public abstract void dataChanged(String paramString)
        throws RemoteException;

    public abstract void fullBackup(ParcelFileDescriptor paramParcelFileDescriptor, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String[] paramArrayOfString)
        throws RemoteException;

    public abstract void fullRestore(ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException;

    public abstract Intent getConfigurationIntent(String paramString)
        throws RemoteException;

    public abstract String getCurrentTransport()
        throws RemoteException;

    public abstract String getDestinationString(String paramString)
        throws RemoteException;

    public abstract boolean hasBackupPassword()
        throws RemoteException;

    public abstract boolean isBackupEnabled()
        throws RemoteException;

    public abstract String[] listAllTransports()
        throws RemoteException;

    public abstract void opComplete(int paramInt)
        throws RemoteException;

    public abstract void restoreAtInstall(String paramString, int paramInt)
        throws RemoteException;

    public abstract String selectBackupTransport(String paramString)
        throws RemoteException;

    public abstract void setAutoRestore(boolean paramBoolean)
        throws RemoteException;

    public abstract void setBackupEnabled(boolean paramBoolean)
        throws RemoteException;

    public abstract boolean setBackupPassword(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setBackupProvisioned(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBackupManager
    {
        private static final String DESCRIPTOR = "android.app.backup.IBackupManager";
        static final int TRANSACTION_acknowledgeFullBackupOrRestore = 15;
        static final int TRANSACTION_agentConnected = 3;
        static final int TRANSACTION_agentDisconnected = 4;
        static final int TRANSACTION_backupNow = 12;
        static final int TRANSACTION_beginRestoreSession = 21;
        static final int TRANSACTION_clearBackupData = 2;
        static final int TRANSACTION_dataChanged = 1;
        static final int TRANSACTION_fullBackup = 13;
        static final int TRANSACTION_fullRestore = 14;
        static final int TRANSACTION_getConfigurationIntent = 19;
        static final int TRANSACTION_getCurrentTransport = 16;
        static final int TRANSACTION_getDestinationString = 20;
        static final int TRANSACTION_hasBackupPassword = 11;
        static final int TRANSACTION_isBackupEnabled = 9;
        static final int TRANSACTION_listAllTransports = 17;
        static final int TRANSACTION_opComplete = 22;
        static final int TRANSACTION_restoreAtInstall = 5;
        static final int TRANSACTION_selectBackupTransport = 18;
        static final int TRANSACTION_setAutoRestore = 7;
        static final int TRANSACTION_setBackupEnabled = 6;
        static final int TRANSACTION_setBackupPassword = 10;
        static final int TRANSACTION_setBackupProvisioned = 8;

        public Stub()
        {
            attachInterface(this, "android.app.backup.IBackupManager");
        }

        public static IBackupManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.backup.IBackupManager");
                if ((localIInterface != null) && ((localIInterface instanceof IBackupManager)))
                    localObject = (IBackupManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.app.backup.IBackupManager");
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                dataChanged(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                clearBackupData(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                agentConnected(paramParcel1.readString(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                agentDisconnected(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                restoreAtInstall(paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                if (paramParcel1.readInt() != 0);
                int i14;
                for (int i13 = j; ; i14 = 0)
                {
                    setBackupEnabled(i13);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                if (paramParcel1.readInt() != 0);
                int i12;
                for (int i11 = j; ; i12 = 0)
                {
                    setAutoRestore(i11);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                if (paramParcel1.readInt() != 0);
                int i10;
                for (int i9 = j; ; i10 = 0)
                {
                    setBackupProvisioned(i9);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                boolean bool3 = isBackupEnabled();
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                boolean bool2 = setBackupPassword(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                boolean bool1 = hasBackupPassword();
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                backupNow();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                ParcelFileDescriptor localParcelFileDescriptor2;
                label598: int i1;
                label609: int i3;
                label620: int i5;
                if (paramParcel1.readInt() != 0)
                {
                    localParcelFileDescriptor2 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label673;
                    i1 = j;
                    if (paramParcel1.readInt() == 0)
                        break label679;
                    i3 = j;
                    if (paramParcel1.readInt() == 0)
                        break label685;
                    i5 = j;
                    label631: if (paramParcel1.readInt() == 0)
                        break label691;
                }
                label673: label679: label685: label691: int i8;
                for (int i7 = j; ; i8 = 0)
                {
                    fullBackup(localParcelFileDescriptor2, i1, i3, i5, i7, paramParcel1.createStringArray());
                    paramParcel2.writeNoException();
                    break;
                    localParcelFileDescriptor2 = null;
                    break label598;
                    int i2 = 0;
                    break label609;
                    int i4 = 0;
                    break label620;
                    int i6 = 0;
                    break label631;
                }
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                if (paramParcel1.readInt() != 0);
                for (ParcelFileDescriptor localParcelFileDescriptor1 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor1 = null)
                {
                    fullRestore(localParcelFileDescriptor1);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                int k = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                int n;
                for (int m = j; ; n = 0)
                {
                    acknowledgeFullBackupOrRestore(k, m, paramParcel1.readString(), paramParcel1.readString(), IFullBackupRestoreObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                String str3 = getCurrentTransport();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str3);
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                String[] arrayOfString = listAllTransports();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString);
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                String str2 = selectBackupTransport(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str2);
                continue;
                paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                Intent localIntent = getConfigurationIntent(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localIntent != null)
                {
                    paramParcel2.writeInt(j);
                    localIntent.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                    String str1 = getDestinationString(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str1);
                    continue;
                    paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                    IRestoreSession localIRestoreSession = beginRestoreSession(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localIRestoreSession != null);
                    for (IBinder localIBinder = localIRestoreSession.asBinder(); ; localIBinder = null)
                    {
                        paramParcel2.writeStrongBinder(localIBinder);
                        break;
                    }
                    paramParcel1.enforceInterface("android.app.backup.IBackupManager");
                    opComplete(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements IBackupManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void acknowledgeFullBackupOrRestore(int paramInt, boolean paramBoolean, String paramString1, String paramString2, IFullBackupRestoreObserver paramIFullBackupRestoreObserver)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    if (paramIFullBackupRestoreObserver != null)
                    {
                        localIBinder = paramIFullBackupRestoreObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(15, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void agentConnected(String paramString, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void agentDisconnected(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void backupNow()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IRestoreSession beginRestoreSession(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    IRestoreSession localIRestoreSession = IRestoreSession.Stub.asInterface(localParcel2.readStrongBinder());
                    return localIRestoreSession;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void clearBackupData(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void dataChanged(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void fullBackup(ParcelFileDescriptor paramParcelFileDescriptor, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String[] paramArrayOfString)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    int k;
                    label55: int m;
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                        if (paramParcelFileDescriptor != null)
                        {
                            localParcel1.writeInt(1);
                            paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                            break label178;
                            localParcel1.writeInt(j);
                            if (paramBoolean2)
                            {
                                k = i;
                                localParcel1.writeInt(k);
                                if (!paramBoolean3)
                                    break label166;
                                m = i;
                                label71: localParcel1.writeInt(m);
                                if (!paramBoolean4)
                                    break label172;
                                label83: localParcel1.writeInt(i);
                                localParcel1.writeStringArray(paramArrayOfString);
                                this.mRemote.transact(13, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label166: label172: label178: 
                    do
                    {
                        j = 0;
                        break;
                        k = 0;
                        break label55;
                        m = 0;
                        break label71;
                        i = 0;
                        break label83;
                    }
                    while (!paramBoolean1);
                    int j = i;
                }
            }

            public void fullRestore(ParcelFileDescriptor paramParcelFileDescriptor)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    if (paramParcelFileDescriptor != null)
                    {
                        localParcel1.writeInt(1);
                        paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Intent getConfigurationIntent(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localIntent = (Intent)Intent.CREATOR.createFromParcel(localParcel2);
                        return localIntent;
                    }
                    Intent localIntent = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getCurrentTransport()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getDestinationString(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.backup.IBackupManager";
            }

            public boolean hasBackupPassword()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isBackupEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] listAllTransports()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void opComplete(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void restoreAtInstall(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String selectBackupTransport(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAutoRestore(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBackupEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setBackupPassword(String paramString1, String paramString2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBackupProvisioned(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.backup.IBackupManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.IBackupManager
 * JD-Core Version:        0.6.2
 */