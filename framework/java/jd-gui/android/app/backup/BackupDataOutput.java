package android.app.backup;

import java.io.FileDescriptor;
import java.io.IOException;

public class BackupDataOutput
{
    int mBackupWriter;

    public BackupDataOutput(FileDescriptor paramFileDescriptor)
    {
        if (paramFileDescriptor == null)
            throw new NullPointerException();
        this.mBackupWriter = ctor(paramFileDescriptor);
        if (this.mBackupWriter == 0)
            throw new RuntimeException("Native initialization failed with fd=" + paramFileDescriptor);
    }

    private static native int ctor(FileDescriptor paramFileDescriptor);

    private static native void dtor(int paramInt);

    private static native void setKeyPrefix_native(int paramInt, String paramString);

    private static native int writeEntityData_native(int paramInt1, byte[] paramArrayOfByte, int paramInt2);

    private static native int writeEntityHeader_native(int paramInt1, String paramString, int paramInt2);

    protected void finalize()
        throws Throwable
    {
        try
        {
            dtor(this.mBackupWriter);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public void setKeyPrefix(String paramString)
    {
        setKeyPrefix_native(this.mBackupWriter, paramString);
    }

    public int writeEntityData(byte[] paramArrayOfByte, int paramInt)
        throws IOException
    {
        int i = writeEntityData_native(this.mBackupWriter, paramArrayOfByte, paramInt);
        if (i >= 0)
            return i;
        throw new IOException("result=0x" + Integer.toHexString(i));
    }

    public int writeEntityHeader(String paramString, int paramInt)
        throws IOException
    {
        int i = writeEntityHeader_native(this.mBackupWriter, paramString, paramInt);
        if (i >= 0)
            return i;
        throw new IOException("result=0x" + Integer.toHexString(i));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.BackupDataOutput
 * JD-Core Version:        0.6.2
 */