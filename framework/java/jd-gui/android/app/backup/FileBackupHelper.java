package android.app.backup;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import java.io.File;

public class FileBackupHelper extends FileBackupHelperBase
    implements BackupHelper
{
    private static final boolean DEBUG = false;
    private static final String TAG = "FileBackupHelper";
    Context mContext;
    String[] mFiles;
    File mFilesDir;

    public FileBackupHelper(Context paramContext, String[] paramArrayOfString)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mFilesDir = paramContext.getFilesDir();
        this.mFiles = paramArrayOfString;
    }

    public void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
    {
        String[] arrayOfString1 = this.mFiles;
        File localFile = this.mContext.getFilesDir();
        int i = arrayOfString1.length;
        String[] arrayOfString2 = new String[i];
        for (int j = 0; j < i; j++)
            arrayOfString2[j] = new File(localFile, arrayOfString1[j]).getAbsolutePath();
        performBackup_checked(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2, arrayOfString2, arrayOfString1);
    }

    public void restoreEntity(BackupDataInputStream paramBackupDataInputStream)
    {
        String str = paramBackupDataInputStream.getKey();
        if (isKeyInList(str, this.mFiles))
            writeFile(new File(this.mFilesDir, str), paramBackupDataInputStream);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.FileBackupHelper
 * JD-Core Version:        0.6.2
 */