package android.app.backup;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Point;
import android.os.ParcelFileDescriptor;
import android.view.Display;
import android.view.WindowManager;
import java.io.File;

public class WallpaperBackupHelper extends FileBackupHelperBase
    implements BackupHelper
{
    private static final boolean DEBUG = false;
    private static final String STAGE_FILE = "/data/system/users/0/wallpaper-tmp";
    private static final String TAG = "WallpaperBackupHelper";
    public static final String WALLPAPER_IMAGE = "/data/system/users/0/wallpaper";
    public static final String WALLPAPER_IMAGE_KEY = "/data/data/com.android.settings/files/wallpaper";
    public static final String WALLPAPER_INFO = "/data/system/users/0/wallpaper_info.xml";
    public static final String WALLPAPER_INFO_KEY = "/data/system/wallpaper_info.xml";
    Context mContext;
    double mDesiredMinHeight;
    double mDesiredMinWidth;
    String[] mFiles;
    String[] mKeys;

    public WallpaperBackupHelper(Context paramContext, String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mFiles = paramArrayOfString1;
        this.mKeys = paramArrayOfString2;
        WallpaperManager localWallpaperManager = (WallpaperManager)paramContext.getSystemService("wallpaper");
        this.mDesiredMinWidth = localWallpaperManager.getDesiredMinimumWidth();
        this.mDesiredMinHeight = localWallpaperManager.getDesiredMinimumHeight();
        if ((this.mDesiredMinWidth <= 0.0D) || (this.mDesiredMinHeight <= 0.0D))
        {
            Display localDisplay = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
            Point localPoint = new Point();
            localDisplay.getSize(localPoint);
            this.mDesiredMinWidth = localPoint.x;
            this.mDesiredMinHeight = localPoint.y;
        }
    }

    public void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
    {
        performBackup_checked(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2, this.mFiles, this.mKeys);
    }

    public void restoreEntity(BackupDataInputStream paramBackupDataInputStream)
    {
        String str = paramBackupDataInputStream.getKey();
        File localFile;
        if (isKeyInList(str, this.mKeys))
        {
            if (!str.equals("/data/data/com.android.settings/files/wallpaper"))
                break label149;
            localFile = new File("/data/system/users/0/wallpaper-tmp");
            if (writeFile(localFile, paramBackupDataInputStream))
            {
                BitmapFactory.Options localOptions = new BitmapFactory.Options();
                localOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile("/data/system/users/0/wallpaper-tmp", localOptions);
                double d1 = this.mDesiredMinWidth / localOptions.outWidth;
                double d2 = this.mDesiredMinHeight / localOptions.outHeight;
                if ((d1 <= 0.0D) || (d1 >= 1.33D) || (d2 <= 0.0D) || (d2 >= 1.33D))
                    break label141;
                localFile.renameTo(new File("/data/system/users/0/wallpaper"));
            }
        }
        while (true)
        {
            return;
            label141: localFile.delete();
            continue;
            label149: if (str.equals("/data/system/wallpaper_info.xml"))
                writeFile(new File("/data/system/users/0/wallpaper_info.xml"), paramBackupDataInputStream);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.WallpaperBackupHelper
 * JD-Core Version:        0.6.2
 */