package android.app.backup;

public abstract class RestoreObserver
{
    public void onUpdate(int paramInt, String paramString)
    {
    }

    public void restoreFinished(int paramInt)
    {
    }

    public void restoreSetsAvailable(RestoreSet[] paramArrayOfRestoreSet)
    {
    }

    public void restoreStarting(int paramInt)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.RestoreObserver
 * JD-Core Version:        0.6.2
 */