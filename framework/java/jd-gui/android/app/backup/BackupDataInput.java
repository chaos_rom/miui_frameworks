package android.app.backup;

import java.io.FileDescriptor;
import java.io.IOException;

public class BackupDataInput
{
    int mBackupReader;
    private EntityHeader mHeader = new EntityHeader(null);
    private boolean mHeaderReady;

    public BackupDataInput(FileDescriptor paramFileDescriptor)
    {
        if (paramFileDescriptor == null)
            throw new NullPointerException();
        this.mBackupReader = ctor(paramFileDescriptor);
        if (this.mBackupReader == 0)
            throw new RuntimeException("Native initialization failed with fd=" + paramFileDescriptor);
    }

    private static native int ctor(FileDescriptor paramFileDescriptor);

    private static native void dtor(int paramInt);

    private native int readEntityData_native(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3);

    private native int readNextHeader_native(int paramInt, EntityHeader paramEntityHeader);

    private native int skipEntityData_native(int paramInt);

    protected void finalize()
        throws Throwable
    {
        try
        {
            dtor(this.mBackupReader);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getDataSize()
    {
        if (this.mHeaderReady)
            return this.mHeader.dataSize;
        throw new IllegalStateException("Entity header not read");
    }

    public String getKey()
    {
        if (this.mHeaderReady)
            return this.mHeader.key;
        throw new IllegalStateException("Entity header not read");
    }

    public int readEntityData(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        if (this.mHeaderReady)
        {
            int i = readEntityData_native(this.mBackupReader, paramArrayOfByte, paramInt1, paramInt2);
            if (i >= 0)
                return i;
            throw new IOException("result=0x" + Integer.toHexString(i));
        }
        throw new IllegalStateException("Entity header not read");
    }

    public boolean readNextHeader()
        throws IOException
    {
        boolean bool = true;
        int i = readNextHeader_native(this.mBackupReader, this.mHeader);
        if (i == 0)
            this.mHeaderReady = bool;
        while (true)
        {
            return bool;
            if (i <= 0)
                break;
            this.mHeaderReady = false;
            bool = false;
        }
        this.mHeaderReady = false;
        throw new IOException("failed: 0x" + Integer.toHexString(i));
    }

    public void skipEntityData()
        throws IOException
    {
        if (this.mHeaderReady)
        {
            skipEntityData_native(this.mBackupReader);
            return;
        }
        throw new IllegalStateException("Entity header not read");
    }

    private static class EntityHeader
    {
        int dataSize;
        String key;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.BackupDataInput
 * JD-Core Version:        0.6.2
 */