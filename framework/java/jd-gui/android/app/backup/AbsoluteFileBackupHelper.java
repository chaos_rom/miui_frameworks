package android.app.backup;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import java.io.File;

public class AbsoluteFileBackupHelper extends FileBackupHelperBase
    implements BackupHelper
{
    private static final boolean DEBUG = false;
    private static final String TAG = "AbsoluteFileBackupHelper";
    Context mContext;
    String[] mFiles;

    public AbsoluteFileBackupHelper(Context paramContext, String[] paramArrayOfString)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mFiles = paramArrayOfString;
    }

    public void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
    {
        performBackup_checked(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2, this.mFiles, this.mFiles);
    }

    public void restoreEntity(BackupDataInputStream paramBackupDataInputStream)
    {
        String str = paramBackupDataInputStream.getKey();
        if (isKeyInList(str, this.mFiles))
            writeFile(new File(str), paramBackupDataInputStream);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.backup.AbsoluteFileBackupHelper
 * JD-Core Version:        0.6.2
 */