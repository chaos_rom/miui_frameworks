package android.app;

import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ResolveInfo.DisplayNameComparator;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class LauncherActivity extends ListActivity
{
    IconResizer mIconResizer;
    Intent mIntent;
    PackageManager mPackageManager;

    private void updateAlertTitle()
    {
        TextView localTextView = (TextView)findViewById(16908877);
        if (localTextView != null)
            localTextView.setText(getTitle());
    }

    private void updateButtonText()
    {
        Button localButton = (Button)findViewById(16908313);
        if (localButton != null)
            localButton.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    LauncherActivity.this.finish();
                }
            });
    }

    protected Intent getTargetIntent()
    {
        return new Intent();
    }

    protected Intent intentForPosition(int paramInt)
    {
        return ((ActivityAdapter)this.mAdapter).intentForPosition(paramInt);
    }

    protected ListItem itemForPosition(int paramInt)
    {
        return ((ActivityAdapter)this.mAdapter).itemForPosition(paramInt);
    }

    public List<ListItem> makeListItems()
    {
        List localList = onQueryPackageManager(this.mIntent);
        Collections.sort(localList, new ResolveInfo.DisplayNameComparator(this.mPackageManager));
        ArrayList localArrayList = new ArrayList(localList.size());
        int i = localList.size();
        for (int j = 0; j < i; j++)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)localList.get(j);
            localArrayList.add(new ListItem(this.mPackageManager, localResolveInfo, null));
        }
        return localArrayList;
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        this.mPackageManager = getPackageManager();
        requestWindowFeature(5);
        setProgressBarIndeterminateVisibility(true);
        onSetContentView();
        this.mIconResizer = new IconResizer();
        this.mIntent = new Intent(getTargetIntent());
        this.mIntent.setComponent(null);
        this.mAdapter = new ActivityAdapter(this.mIconResizer);
        setListAdapter(this.mAdapter);
        getListView().setTextFilterEnabled(true);
        updateAlertTitle();
        updateButtonText();
        setProgressBarIndeterminateVisibility(false);
    }

    protected boolean onEvaluateShowIcons()
    {
        return true;
    }

    protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
    {
        startActivity(intentForPosition(paramInt));
    }

    protected List<ResolveInfo> onQueryPackageManager(Intent paramIntent)
    {
        return this.mPackageManager.queryIntentActivities(paramIntent, 0);
    }

    protected void onSetContentView()
    {
        setContentView(17367072);
    }

    public void setTitle(int paramInt)
    {
        super.setTitle(paramInt);
        updateAlertTitle();
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        super.setTitle(paramCharSequence);
        updateAlertTitle();
    }

    public class IconResizer
    {
        private Canvas mCanvas = new Canvas();
        private int mIconHeight = -1;
        private int mIconWidth = -1;
        private final Rect mOldBounds = new Rect();

        public IconResizer()
        {
            this.mCanvas.setDrawFilter(new PaintFlagsDrawFilter(4, 2));
            int i = (int)LauncherActivity.this.getResources().getDimension(17104896);
            this.mIconHeight = i;
            this.mIconWidth = i;
        }

        public Drawable createIconThumbnail(Drawable paramDrawable)
        {
            int i = this.mIconWidth;
            int j = this.mIconHeight;
            int k = paramDrawable.getIntrinsicWidth();
            int m = paramDrawable.getIntrinsicHeight();
            if ((paramDrawable instanceof PaintDrawable))
            {
                PaintDrawable localPaintDrawable = (PaintDrawable)paramDrawable;
                localPaintDrawable.setIntrinsicWidth(i);
                localPaintDrawable.setIntrinsicHeight(j);
            }
            float f;
            Bitmap.Config localConfig1;
            if ((i > 0) && (j > 0))
            {
                if ((i >= k) && (j >= m))
                    break label243;
                f = k / m;
                if (k <= m)
                    break label218;
                j = (int)(i / f);
                if (paramDrawable.getOpacity() == -1)
                    break label235;
                localConfig1 = Bitmap.Config.ARGB_8888;
                label104: Bitmap localBitmap1 = Bitmap.createBitmap(this.mIconWidth, this.mIconHeight, localConfig1);
                Canvas localCanvas1 = this.mCanvas;
                localCanvas1.setBitmap(localBitmap1);
                this.mOldBounds.set(paramDrawable.getBounds());
                int n = (this.mIconWidth - i) / 2;
                int i1 = (this.mIconHeight - j) / 2;
                paramDrawable.setBounds(n, i1, n + i, i1 + j);
                paramDrawable.draw(localCanvas1);
                paramDrawable.setBounds(this.mOldBounds);
                paramDrawable = new BitmapDrawable(LauncherActivity.this.getResources(), localBitmap1);
                localCanvas1.setBitmap(null);
            }
            while (true)
            {
                return paramDrawable;
                label218: if (m <= k)
                    break;
                i = (int)(f * j);
                break;
                label235: localConfig1 = Bitmap.Config.RGB_565;
                break label104;
                label243: if ((k < i) && (m < j))
                {
                    Bitmap.Config localConfig2 = Bitmap.Config.ARGB_8888;
                    Bitmap localBitmap2 = Bitmap.createBitmap(this.mIconWidth, this.mIconHeight, localConfig2);
                    Canvas localCanvas2 = this.mCanvas;
                    localCanvas2.setBitmap(localBitmap2);
                    this.mOldBounds.set(paramDrawable.getBounds());
                    int i2 = (i - k) / 2;
                    int i3 = (j - m) / 2;
                    paramDrawable.setBounds(i2, i3, i2 + k, i3 + m);
                    paramDrawable.draw(localCanvas2);
                    paramDrawable.setBounds(this.mOldBounds);
                    paramDrawable = new BitmapDrawable(LauncherActivity.this.getResources(), localBitmap2);
                    localCanvas2.setBitmap(null);
                }
            }
        }
    }

    private class ActivityAdapter extends BaseAdapter
        implements Filterable
    {
        private final Object lock = new Object();
        protected List<LauncherActivity.ListItem> mActivitiesList;
        private Filter mFilter;
        protected final LauncherActivity.IconResizer mIconResizer;
        protected final LayoutInflater mInflater;
        private ArrayList<LauncherActivity.ListItem> mOriginalValues;
        private final boolean mShowIcons;

        public ActivityAdapter(LauncherActivity.IconResizer arg2)
        {
            Object localObject;
            this.mIconResizer = localObject;
            this.mInflater = ((LayoutInflater)LauncherActivity.this.getSystemService("layout_inflater"));
            this.mShowIcons = LauncherActivity.this.onEvaluateShowIcons();
            this.mActivitiesList = LauncherActivity.this.makeListItems();
        }

        private void bindView(View paramView, LauncherActivity.ListItem paramListItem)
        {
            TextView localTextView = (TextView)paramView;
            localTextView.setText(paramListItem.label);
            if (this.mShowIcons)
            {
                if (paramListItem.icon == null)
                    paramListItem.icon = this.mIconResizer.createIconThumbnail(paramListItem.resolveInfo.loadIcon(LauncherActivity.this.getPackageManager()));
                localTextView.setCompoundDrawablesWithIntrinsicBounds(paramListItem.icon, null, null, null);
            }
        }

        public int getCount()
        {
            if (this.mActivitiesList != null);
            for (int i = this.mActivitiesList.size(); ; i = 0)
                return i;
        }

        public Filter getFilter()
        {
            if (this.mFilter == null)
                this.mFilter = new ArrayFilter(null);
            return this.mFilter;
        }

        public Object getItem(int paramInt)
        {
            return Integer.valueOf(paramInt);
        }

        public long getItemId(int paramInt)
        {
            return paramInt;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            if (paramView == null);
            for (View localView = this.mInflater.inflate(17367073, paramViewGroup, false); ; localView = paramView)
            {
                bindView(localView, (LauncherActivity.ListItem)this.mActivitiesList.get(paramInt));
                return localView;
            }
        }

        public Intent intentForPosition(int paramInt)
        {
            Intent localIntent;
            if (this.mActivitiesList == null)
                localIntent = null;
            while (true)
            {
                return localIntent;
                localIntent = new Intent(LauncherActivity.this.mIntent);
                LauncherActivity.ListItem localListItem = (LauncherActivity.ListItem)this.mActivitiesList.get(paramInt);
                localIntent.setClassName(localListItem.packageName, localListItem.className);
                if (localListItem.extras != null)
                    localIntent.putExtras(localListItem.extras);
            }
        }

        public LauncherActivity.ListItem itemForPosition(int paramInt)
        {
            if (this.mActivitiesList == null);
            for (LauncherActivity.ListItem localListItem = null; ; localListItem = (LauncherActivity.ListItem)this.mActivitiesList.get(paramInt))
                return localListItem;
        }

        private class ArrayFilter extends Filter
        {
            private ArrayFilter()
            {
            }

            protected Filter.FilterResults performFiltering(CharSequence paramCharSequence)
            {
                Filter.FilterResults localFilterResults = new Filter.FilterResults();
                if (LauncherActivity.ActivityAdapter.this.mOriginalValues == null);
                synchronized (LauncherActivity.ActivityAdapter.this.lock)
                {
                    LauncherActivity.ActivityAdapter.access$102(LauncherActivity.ActivityAdapter.this, new ArrayList(LauncherActivity.ActivityAdapter.this.mActivitiesList));
                    if (paramCharSequence != null)
                        if (paramCharSequence.length() != 0)
                            break label128;
                }
                while (true)
                {
                    synchronized (LauncherActivity.ActivityAdapter.this.lock)
                    {
                        ArrayList localArrayList1 = new ArrayList(LauncherActivity.ActivityAdapter.this.mOriginalValues);
                        localFilterResults.values = localArrayList1;
                        localFilterResults.count = localArrayList1.size();
                        return localFilterResults;
                        localObject4 = finally;
                        throw localObject4;
                    }
                    label128: String str = paramCharSequence.toString().toLowerCase();
                    ArrayList localArrayList2 = LauncherActivity.ActivityAdapter.this.mOriginalValues;
                    int i = localArrayList2.size();
                    ArrayList localArrayList3 = new ArrayList(i);
                    int j = 0;
                    if (j < i)
                    {
                        LauncherActivity.ListItem localListItem = (LauncherActivity.ListItem)localArrayList2.get(j);
                        String[] arrayOfString = localListItem.label.toString().toLowerCase().split(" ");
                        int k = arrayOfString.length;
                        for (int m = 0; ; m++)
                            if (m < k)
                            {
                                if (arrayOfString[m].startsWith(str))
                                    localArrayList3.add(localListItem);
                            }
                            else
                            {
                                j++;
                                break;
                            }
                    }
                    localFilterResults.values = localArrayList3;
                    localFilterResults.count = localArrayList3.size();
                }
            }

            protected void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
            {
                LauncherActivity.ActivityAdapter.this.mActivitiesList = ((List)paramFilterResults.values);
                if (paramFilterResults.count > 0)
                    LauncherActivity.ActivityAdapter.this.notifyDataSetChanged();
                while (true)
                {
                    return;
                    LauncherActivity.ActivityAdapter.this.notifyDataSetInvalidated();
                }
            }
        }
    }

    public static class ListItem
    {
        public String className;
        public Bundle extras;
        public Drawable icon;
        public CharSequence label;
        public String packageName;
        public ResolveInfo resolveInfo;

        public ListItem()
        {
        }

        ListItem(PackageManager paramPackageManager, ResolveInfo paramResolveInfo, LauncherActivity.IconResizer paramIconResizer)
        {
            this.resolveInfo = paramResolveInfo;
            this.label = paramResolveInfo.loadLabel(paramPackageManager);
            Object localObject = paramResolveInfo.activityInfo;
            if (localObject == null)
                localObject = paramResolveInfo.serviceInfo;
            if ((this.label == null) && (localObject != null))
                this.label = paramResolveInfo.activityInfo.name;
            if (paramIconResizer != null)
                this.icon = paramIconResizer.createIconThumbnail(paramResolveInfo.loadIcon(paramPackageManager));
            this.packageName = ((ComponentInfo)localObject).applicationInfo.packageName;
            this.className = ((PackageItemInfo)localObject).name;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.LauncherActivity
 * JD-Core Version:        0.6.2
 */