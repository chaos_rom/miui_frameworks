package android.app;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IActivityPendingResult extends IInterface
{
    public abstract boolean sendResult(int paramInt, String paramString, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IActivityPendingResult
    {
        private static final String DESCRIPTOR = "android.app.IActivityPendingResult";
        static final int TRANSACTION_sendResult = 1;

        public Stub()
        {
            attachInterface(this, "android.app.IActivityPendingResult");
        }

        public static IActivityPendingResult asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IActivityPendingResult");
                if ((localIInterface != null) && ((localIInterface instanceof IActivityPendingResult)))
                    localObject = (IActivityPendingResult)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return i;
                    paramParcel2.writeString("android.app.IActivityPendingResult");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.app.IActivityPendingResult");
            int j = paramParcel1.readInt();
            String str = paramParcel1.readString();
            Bundle localBundle;
            if (paramParcel1.readInt() != 0)
            {
                localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                label94: boolean bool = sendResult(j, str, localBundle);
                paramParcel2.writeNoException();
                if (!bool)
                    break label134;
            }
            label134: int m;
            for (int k = i; ; m = 0)
            {
                paramParcel2.writeInt(k);
                break;
                localBundle = null;
                break label94;
            }
        }

        private static class Proxy
            implements IActivityPendingResult
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IActivityPendingResult";
            }

            public boolean sendResult(int paramInt, String paramString, Bundle paramBundle)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.IActivityPendingResult");
                        localParcel1.writeInt(paramInt);
                        localParcel1.writeString(paramString);
                        if (paramBundle != null)
                        {
                            localParcel1.writeInt(1);
                            paramBundle.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(1, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IActivityPendingResult
 * JD-Core Version:        0.6.2
 */