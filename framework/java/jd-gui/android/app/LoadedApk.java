package android.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.BroadcastReceiver;
import android.content.BroadcastReceiver.PendingResult;
import android.content.ComponentName;
import android.content.Context;
import android.content.IIntentReceiver;
import android.content.IIntentReceiver.Stub;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.Trace;
import android.os.UserId;
import android.util.Slog;
import android.view.CompatibilityInfoHolder;
import com.android.internal.util.ArrayUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

public final class LoadedApk
{
    private static final String TAG = "LoadedApk";
    private final ActivityThread mActivityThread;
    private final String mAppDir;
    private Application mApplication;
    private final ApplicationInfo mApplicationInfo;
    private final ClassLoader mBaseClassLoader;
    private ClassLoader mClassLoader;
    int mClientCount = 0;
    public final CompatibilityInfoHolder mCompatibilityInfo = new CompatibilityInfoHolder();
    private final String mDataDir;
    private final File mDataDirFile;
    private final boolean mIncludeCode;
    private final String mLibDir;
    final String mPackageName;
    private final HashMap<Context, HashMap<BroadcastReceiver, ReceiverDispatcher>> mReceivers = new HashMap();
    private final String mResDir;
    Resources mResources;
    private final boolean mSecurityViolation;
    private final HashMap<Context, HashMap<ServiceConnection, ServiceDispatcher>> mServices = new HashMap();
    private final String[] mSharedLibraries;
    private final HashMap<Context, HashMap<ServiceConnection, ServiceDispatcher>> mUnboundServices = new HashMap();
    private final HashMap<Context, HashMap<BroadcastReceiver, ReceiverDispatcher>> mUnregisteredReceivers = new HashMap();

    public LoadedApk(ActivityThread paramActivityThread1, ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, ActivityThread paramActivityThread2, ClassLoader paramClassLoader, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mActivityThread = paramActivityThread1;
        this.mApplicationInfo = paramApplicationInfo;
        this.mPackageName = paramApplicationInfo.packageName;
        this.mAppDir = paramApplicationInfo.sourceDir;
        int i = Process.myUid();
        String str;
        if (paramApplicationInfo.uid == i)
        {
            str = paramApplicationInfo.sourceDir;
            this.mResDir = str;
            if ((!UserId.isSameUser(paramApplicationInfo.uid, i)) && (!Process.isIsolated()))
                paramApplicationInfo.dataDir = PackageManager.getDataDirForUser(UserId.getUserId(i), this.mPackageName);
            this.mSharedLibraries = paramApplicationInfo.sharedLibraryFiles;
            this.mDataDir = paramApplicationInfo.dataDir;
            if (this.mDataDir == null)
                break label299;
        }
        label299: for (File localFile = new File(this.mDataDir); ; localFile = null)
        {
            this.mDataDirFile = localFile;
            this.mLibDir = paramApplicationInfo.nativeLibraryDir;
            this.mBaseClassLoader = paramClassLoader;
            this.mSecurityViolation = paramBoolean1;
            this.mIncludeCode = paramBoolean2;
            this.mCompatibilityInfo.set(paramCompatibilityInfo);
            if (this.mAppDir == null)
            {
                if (ActivityThread.mSystemContext == null)
                {
                    ActivityThread.mSystemContext = ContextImpl.createSystemContext(paramActivityThread2);
                    ActivityThread.mSystemContext.getResources().updateConfiguration(paramActivityThread2.getConfiguration(), paramActivityThread2.getDisplayMetricsLocked(paramCompatibilityInfo, false), paramCompatibilityInfo);
                }
                this.mClassLoader = ActivityThread.mSystemContext.getClassLoader();
                this.mResources = ActivityThread.mSystemContext.getResources();
            }
            return;
            str = paramApplicationInfo.publicSourceDir;
            break;
        }
    }

    public LoadedApk(ActivityThread paramActivityThread, String paramString, Context paramContext, ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo)
    {
        this.mActivityThread = paramActivityThread;
        if (paramApplicationInfo != null);
        while (true)
        {
            this.mApplicationInfo = paramApplicationInfo;
            this.mApplicationInfo.packageName = paramString;
            this.mPackageName = paramString;
            this.mAppDir = null;
            this.mResDir = null;
            this.mSharedLibraries = null;
            this.mDataDir = null;
            this.mDataDirFile = null;
            this.mLibDir = null;
            this.mBaseClassLoader = null;
            this.mSecurityViolation = false;
            this.mIncludeCode = true;
            this.mClassLoader = paramContext.getClassLoader();
            this.mResources = paramContext.getResources();
            this.mCompatibilityInfo.set(paramCompatibilityInfo);
            return;
            paramApplicationInfo = new ApplicationInfo();
        }
    }

    private static String combineLibs(String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        StringBuilder localStringBuilder = new StringBuilder(300);
        int i = 1;
        if (paramArrayOfString1 != null)
        {
            int n = paramArrayOfString1.length;
            int i1 = 0;
            if (i1 < n)
            {
                String str2 = paramArrayOfString1[i1];
                if (i != 0)
                    i = 0;
                while (true)
                {
                    localStringBuilder.append(str2);
                    i1++;
                    break;
                    localStringBuilder.append(':');
                }
            }
        }
        int j;
        if (i == 0)
            j = 1;
        while (paramArrayOfString2 != null)
        {
            int k = paramArrayOfString2.length;
            int m = 0;
            while (true)
                if (m < k)
                {
                    String str1 = paramArrayOfString2[m];
                    if ((j != 0) && (ArrayUtils.contains(paramArrayOfString1, str1)))
                    {
                        m++;
                        continue;
                        j = 0;
                        break;
                    }
                    if (i != 0)
                        i = 0;
                    while (true)
                    {
                        localStringBuilder.append(str1);
                        break;
                        localStringBuilder.append(':');
                    }
                }
        }
        return localStringBuilder.toString();
    }

    private static String[] getLibrariesFor(String paramString)
    {
        while (true)
        {
            ApplicationInfo localApplicationInfo;
            try
            {
                localApplicationInfo = ActivityThread.getPackageManager().getApplicationInfo(paramString, 1024, UserId.myUserId());
                if (localApplicationInfo == null)
                {
                    arrayOfString = null;
                    return arrayOfString;
                }
            }
            catch (RemoteException localRemoteException)
            {
                throw new AssertionError(localRemoteException);
            }
            String[] arrayOfString = localApplicationInfo.sharedLibraryFiles;
        }
    }

    private void initializeJavaContextClassLoader()
    {
        IPackageManager localIPackageManager = ActivityThread.getPackageManager();
        while (true)
        {
            try
            {
                PackageInfo localPackageInfo = localIPackageManager.getPackageInfo(this.mPackageName, 0, UserId.myUserId());
                if (localPackageInfo.sharedUserId != null)
                {
                    i = 1;
                    if ((localPackageInfo.applicationInfo == null) || (this.mPackageName.equals(localPackageInfo.applicationInfo.processName)))
                        break label109;
                    j = 1;
                    if ((i == 0) && (j == 0))
                        break label115;
                    k = 1;
                    if (k == 0)
                        break label121;
                    localObject = new WarningContextClassLoader(null);
                    Thread.currentThread().setContextClassLoader((ClassLoader)localObject);
                    return;
                }
            }
            catch (RemoteException localRemoteException)
            {
                throw new AssertionError(localRemoteException);
            }
            int i = 0;
            continue;
            label109: int j = 0;
            continue;
            label115: int k = 0;
            continue;
            label121: Object localObject = this.mClassLoader;
        }
    }

    // ERROR //
    public IIntentReceiver forgetReceiverDispatcher(Context paramContext, BroadcastReceiver paramBroadcastReceiver)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 68	android/app/LoadedApk:mReceivers	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 68	android/app/LoadedApk:mReceivers	Ljava/util/HashMap;
        //     11: aload_1
        //     12: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     15: checkcast 65	java/util/HashMap
        //     18: astore 5
        //     20: aload 5
        //     22: ifnull +140 -> 162
        //     25: aload 5
        //     27: aload_2
        //     28: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     31: checkcast 11	android/app/LoadedApk$ReceiverDispatcher
        //     34: astore 9
        //     36: aload 9
        //     38: ifnull +124 -> 162
        //     41: aload 5
        //     43: aload_2
        //     44: invokevirtual 294	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     47: pop
        //     48: aload 5
        //     50: invokevirtual 297	java/util/HashMap:size	()I
        //     53: ifne +12 -> 65
        //     56: aload_0
        //     57: getfield 68	android/app/LoadedApk:mReceivers	Ljava/util/HashMap;
        //     60: aload_1
        //     61: invokevirtual 294	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     64: pop
        //     65: aload_2
        //     66: invokevirtual 302	android/content/BroadcastReceiver:getDebugUnregister	()Z
        //     69: ifeq +75 -> 144
        //     72: aload_0
        //     73: getfield 70	android/app/LoadedApk:mUnregisteredReceivers	Ljava/util/HashMap;
        //     76: aload_1
        //     77: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     80: checkcast 65	java/util/HashMap
        //     83: astore 12
        //     85: aload 12
        //     87: ifnonnull +23 -> 110
        //     90: new 65	java/util/HashMap
        //     93: dup
        //     94: invokespecial 66	java/util/HashMap:<init>	()V
        //     97: astore 12
        //     99: aload_0
        //     100: getfield 70	android/app/LoadedApk:mUnregisteredReceivers	Ljava/util/HashMap;
        //     103: aload_1
        //     104: aload 12
        //     106: invokevirtual 306	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     109: pop
        //     110: new 308	java/lang/IllegalArgumentException
        //     113: dup
        //     114: ldc_w 310
        //     117: invokespecial 311	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     120: astore 14
        //     122: aload 14
        //     124: invokevirtual 317	java/lang/RuntimeException:fillInStackTrace	()Ljava/lang/Throwable;
        //     127: pop
        //     128: aload 9
        //     130: aload 14
        //     132: invokevirtual 321	android/app/LoadedApk$ReceiverDispatcher:setUnregisterLocation	(Ljava/lang/RuntimeException;)V
        //     135: aload 12
        //     137: aload_2
        //     138: aload 9
        //     140: invokevirtual 306	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     143: pop
        //     144: aload 9
        //     146: iconst_1
        //     147: putfield 324	android/app/LoadedApk$ReceiverDispatcher:mForgotten	Z
        //     150: aload 9
        //     152: invokevirtual 328	android/app/LoadedApk$ReceiverDispatcher:getIIntentReceiver	()Landroid/content/IIntentReceiver;
        //     155: astore 11
        //     157: aload_3
        //     158: monitorexit
        //     159: aload 11
        //     161: areturn
        //     162: aload_0
        //     163: getfield 70	android/app/LoadedApk:mUnregisteredReceivers	Ljava/util/HashMap;
        //     166: aload_1
        //     167: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     170: checkcast 65	java/util/HashMap
        //     173: astore 6
        //     175: aload 6
        //     177: ifnull +69 -> 246
        //     180: aload 6
        //     182: aload_2
        //     183: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     186: checkcast 11	android/app/LoadedApk$ReceiverDispatcher
        //     189: astore 7
        //     191: aload 7
        //     193: ifnull +53 -> 246
        //     196: aload 7
        //     198: invokevirtual 332	android/app/LoadedApk$ReceiverDispatcher:getUnregisterLocation	()Ljava/lang/RuntimeException;
        //     201: astore 8
        //     203: new 308	java/lang/IllegalArgumentException
        //     206: dup
        //     207: new 208	java/lang/StringBuilder
        //     210: dup
        //     211: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     214: ldc_w 335
        //     217: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     220: aload_2
        //     221: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     224: ldc_w 340
        //     227: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     230: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     233: aload 8
        //     235: invokespecial 343	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     238: athrow
        //     239: astore 4
        //     241: aload_3
        //     242: monitorexit
        //     243: aload 4
        //     245: athrow
        //     246: aload_1
        //     247: ifnonnull +41 -> 288
        //     250: new 345	java/lang/IllegalStateException
        //     253: dup
        //     254: new 208	java/lang/StringBuilder
        //     257: dup
        //     258: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     261: ldc_w 347
        //     264: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     267: aload_2
        //     268: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     271: ldc_w 349
        //     274: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     277: aload_1
        //     278: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     281: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     284: invokespecial 350	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     287: athrow
        //     288: new 308	java/lang/IllegalArgumentException
        //     291: dup
        //     292: new 208	java/lang/StringBuilder
        //     295: dup
        //     296: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     299: ldc_w 352
        //     302: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     305: aload_2
        //     306: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     309: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     312: invokespecial 311	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     315: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     7	243	239	finally
        //     250	316	239	finally
    }

    // ERROR //
    public final IServiceConnection forgetServiceDispatcher(Context paramContext, ServiceConnection paramServiceConnection)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 72	android/app/LoadedApk:mServices	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 72	android/app/LoadedApk:mServices	Ljava/util/HashMap;
        //     11: aload_1
        //     12: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     15: checkcast 65	java/util/HashMap
        //     18: astore 5
        //     20: aload 5
        //     22: ifnull +142 -> 164
        //     25: aload 5
        //     27: aload_2
        //     28: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     31: checkcast 8	android/app/LoadedApk$ServiceDispatcher
        //     34: astore 9
        //     36: aload 9
        //     38: ifnull +126 -> 164
        //     41: aload 5
        //     43: aload_2
        //     44: invokevirtual 294	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     47: pop
        //     48: aload 9
        //     50: invokevirtual 357	android/app/LoadedApk$ServiceDispatcher:doForget	()V
        //     53: aload 5
        //     55: invokevirtual 297	java/util/HashMap:size	()I
        //     58: ifne +12 -> 70
        //     61: aload_0
        //     62: getfield 72	android/app/LoadedApk:mServices	Ljava/util/HashMap;
        //     65: aload_1
        //     66: invokevirtual 294	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     69: pop
        //     70: iconst_2
        //     71: aload 9
        //     73: invokevirtual 360	android/app/LoadedApk$ServiceDispatcher:getFlags	()I
        //     76: iand
        //     77: ifeq +75 -> 152
        //     80: aload_0
        //     81: getfield 74	android/app/LoadedApk:mUnboundServices	Ljava/util/HashMap;
        //     84: aload_1
        //     85: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     88: checkcast 65	java/util/HashMap
        //     91: astore 12
        //     93: aload 12
        //     95: ifnonnull +23 -> 118
        //     98: new 65	java/util/HashMap
        //     101: dup
        //     102: invokespecial 66	java/util/HashMap:<init>	()V
        //     105: astore 12
        //     107: aload_0
        //     108: getfield 74	android/app/LoadedApk:mUnboundServices	Ljava/util/HashMap;
        //     111: aload_1
        //     112: aload 12
        //     114: invokevirtual 306	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     117: pop
        //     118: new 308	java/lang/IllegalArgumentException
        //     121: dup
        //     122: ldc_w 362
        //     125: invokespecial 311	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     128: astore 14
        //     130: aload 14
        //     132: invokevirtual 317	java/lang/RuntimeException:fillInStackTrace	()Ljava/lang/Throwable;
        //     135: pop
        //     136: aload 9
        //     138: aload 14
        //     140: invokevirtual 365	android/app/LoadedApk$ServiceDispatcher:setUnbindLocation	(Ljava/lang/RuntimeException;)V
        //     143: aload 12
        //     145: aload_2
        //     146: aload 9
        //     148: invokevirtual 306	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     151: pop
        //     152: aload 9
        //     154: invokevirtual 369	android/app/LoadedApk$ServiceDispatcher:getIServiceConnection	()Landroid/app/IServiceConnection;
        //     157: astore 11
        //     159: aload_3
        //     160: monitorexit
        //     161: aload 11
        //     163: areturn
        //     164: aload_0
        //     165: getfield 74	android/app/LoadedApk:mUnboundServices	Ljava/util/HashMap;
        //     168: aload_1
        //     169: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     172: checkcast 65	java/util/HashMap
        //     175: astore 6
        //     177: aload 6
        //     179: ifnull +69 -> 248
        //     182: aload 6
        //     184: aload_2
        //     185: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     188: checkcast 8	android/app/LoadedApk$ServiceDispatcher
        //     191: astore 7
        //     193: aload 7
        //     195: ifnull +53 -> 248
        //     198: aload 7
        //     200: invokevirtual 372	android/app/LoadedApk$ServiceDispatcher:getUnbindLocation	()Ljava/lang/RuntimeException;
        //     203: astore 8
        //     205: new 308	java/lang/IllegalArgumentException
        //     208: dup
        //     209: new 208	java/lang/StringBuilder
        //     212: dup
        //     213: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     216: ldc_w 374
        //     219: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: aload_2
        //     223: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     226: ldc_w 376
        //     229: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     232: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     235: aload 8
        //     237: invokespecial 343	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     240: athrow
        //     241: astore 4
        //     243: aload_3
        //     244: monitorexit
        //     245: aload 4
        //     247: athrow
        //     248: aload_1
        //     249: ifnonnull +41 -> 290
        //     252: new 345	java/lang/IllegalStateException
        //     255: dup
        //     256: new 208	java/lang/StringBuilder
        //     259: dup
        //     260: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     263: ldc_w 374
        //     266: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     269: aload_2
        //     270: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     273: ldc_w 349
        //     276: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     279: aload_1
        //     280: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     283: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     286: invokespecial 350	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     289: athrow
        //     290: new 308	java/lang/IllegalArgumentException
        //     293: dup
        //     294: new 208	java/lang/StringBuilder
        //     297: dup
        //     298: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     301: ldc_w 378
        //     304: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     307: aload_2
        //     308: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     311: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     314: invokespecial 311	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     317: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     7	245	241	finally
        //     252	318	241	finally
    }

    public String getAppDir()
    {
        return this.mAppDir;
    }

    Application getApplication()
    {
        return this.mApplication;
    }

    public ApplicationInfo getApplicationInfo()
    {
        return this.mApplicationInfo;
    }

    public AssetManager getAssets(ActivityThread paramActivityThread)
    {
        return getResources(paramActivityThread).getAssets();
    }

    // ERROR //
    public ClassLoader getClassLoader()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 193	android/app/LoadedApk:mClassLoader	Ljava/lang/ClassLoader;
        //     6: ifnull +13 -> 19
        //     9: aload_0
        //     10: getfield 193	android/app/LoadedApk:mClassLoader	Ljava/lang/ClassLoader;
        //     13: astore_2
        //     14: aload_0
        //     15: monitorexit
        //     16: goto +296 -> 312
        //     19: aload_0
        //     20: getfield 153	android/app/LoadedApk:mIncludeCode	Z
        //     23: ifeq +261 -> 284
        //     26: aload_0
        //     27: getfield 87	android/app/LoadedApk:mPackageName	Ljava/lang/String;
        //     30: ldc_w 394
        //     33: invokevirtual 272	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     36: ifne +248 -> 284
        //     39: aload_0
        //     40: getfield 94	android/app/LoadedApk:mAppDir	Ljava/lang/String;
        //     43: astore_3
        //     44: aload_0
        //     45: getfield 147	android/app/LoadedApk:mLibDir	Ljava/lang/String;
        //     48: astore 4
        //     50: aload_0
        //     51: getfield 78	android/app/LoadedApk:mActivityThread	Landroid/app/ActivityThread;
        //     54: getfield 397	android/app/ActivityThread:mInstrumentationAppDir	Ljava/lang/String;
        //     57: astore 5
        //     59: aload_0
        //     60: getfield 78	android/app/LoadedApk:mActivityThread	Landroid/app/ActivityThread;
        //     63: getfield 400	android/app/ActivityThread:mInstrumentationAppLibraryDir	Ljava/lang/String;
        //     66: astore 6
        //     68: aload_0
        //     69: getfield 78	android/app/LoadedApk:mActivityThread	Landroid/app/ActivityThread;
        //     72: getfield 403	android/app/ActivityThread:mInstrumentationAppPackage	Ljava/lang/String;
        //     75: astore 7
        //     77: aload_0
        //     78: getfield 78	android/app/LoadedApk:mActivityThread	Landroid/app/ActivityThread;
        //     81: getfield 406	android/app/ActivityThread:mInstrumentedAppDir	Ljava/lang/String;
        //     84: astore 8
        //     86: aload_0
        //     87: getfield 78	android/app/LoadedApk:mActivityThread	Landroid/app/ActivityThread;
        //     90: getfield 409	android/app/ActivityThread:mInstrumentedAppLibraryDir	Ljava/lang/String;
        //     93: astore 9
        //     95: aconst_null
        //     96: astore 10
        //     98: aload_0
        //     99: getfield 94	android/app/LoadedApk:mAppDir	Ljava/lang/String;
        //     102: aload 5
        //     104: invokevirtual 272	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     107: ifne +15 -> 122
        //     110: aload_0
        //     111: getfield 94	android/app/LoadedApk:mAppDir	Ljava/lang/String;
        //     114: aload 8
        //     116: invokevirtual 272	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     119: ifeq +75 -> 194
        //     122: new 208	java/lang/StringBuilder
        //     125: dup
        //     126: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     129: aload 5
        //     131: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     134: ldc_w 411
        //     137: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     140: aload 8
        //     142: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     145: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     148: astore_3
        //     149: new 208	java/lang/StringBuilder
        //     152: dup
        //     153: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     156: aload 6
        //     158: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     161: ldc_w 411
        //     164: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: aload 9
        //     169: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     172: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     175: astore 4
        //     177: aload 8
        //     179: aload 5
        //     181: invokevirtual 272	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     184: ifne +10 -> 194
        //     187: aload 7
        //     189: invokestatic 413	android/app/LoadedApk:getLibrariesFor	(Ljava/lang/String;)[Ljava/lang/String;
        //     192: astore 10
        //     194: aload_0
        //     195: getfield 133	android/app/LoadedApk:mSharedLibraries	[Ljava/lang/String;
        //     198: ifnonnull +8 -> 206
        //     201: aload 10
        //     203: ifnull +35 -> 238
        //     206: new 208	java/lang/StringBuilder
        //     209: dup
        //     210: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     213: aload_0
        //     214: getfield 133	android/app/LoadedApk:mSharedLibraries	[Ljava/lang/String;
        //     217: aload 10
        //     219: invokestatic 415	android/app/LoadedApk:combineLibs	([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
        //     222: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     225: bipush 58
        //     227: invokevirtual 218	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     230: aload_3
        //     231: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     234: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     237: astore_3
        //     238: invokestatic 421	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
        //     241: astore 11
        //     243: aload_0
        //     244: invokestatic 427	android/app/ApplicationLoaders:getDefault	()Landroid/app/ApplicationLoaders;
        //     247: aload_3
        //     248: aload 4
        //     250: aload_0
        //     251: getfield 149	android/app/LoadedApk:mBaseClassLoader	Ljava/lang/ClassLoader;
        //     254: invokevirtual 430	android/app/ApplicationLoaders:getClassLoader	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;
        //     257: putfield 193	android/app/LoadedApk:mClassLoader	Ljava/lang/ClassLoader;
        //     260: aload_0
        //     261: invokespecial 432	android/app/LoadedApk:initializeJavaContextClassLoader	()V
        //     264: aload 11
        //     266: invokestatic 436	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     269: aload_0
        //     270: getfield 193	android/app/LoadedApk:mClassLoader	Ljava/lang/ClassLoader;
        //     273: astore_2
        //     274: aload_0
        //     275: monitorexit
        //     276: goto +36 -> 312
        //     279: astore_1
        //     280: aload_0
        //     281: monitorexit
        //     282: aload_1
        //     283: athrow
        //     284: aload_0
        //     285: getfield 149	android/app/LoadedApk:mBaseClassLoader	Ljava/lang/ClassLoader;
        //     288: ifnonnull +13 -> 301
        //     291: aload_0
        //     292: invokestatic 441	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
        //     295: putfield 193	android/app/LoadedApk:mClassLoader	Ljava/lang/ClassLoader;
        //     298: goto -29 -> 269
        //     301: aload_0
        //     302: aload_0
        //     303: getfield 149	android/app/LoadedApk:mBaseClassLoader	Ljava/lang/ClassLoader;
        //     306: putfield 193	android/app/LoadedApk:mClassLoader	Ljava/lang/ClassLoader;
        //     309: goto -40 -> 269
        //     312: aload_2
        //     313: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	282	279	finally
        //     284	309	279	finally
    }

    public String getDataDir()
    {
        return this.mDataDir;
    }

    public File getDataDirFile()
    {
        return this.mDataDirFile;
    }

    public String getLibDir()
    {
        return this.mLibDir;
    }

    public String getPackageName()
    {
        return this.mPackageName;
    }

    // ERROR //
    public IIntentReceiver getReceiverDispatcher(BroadcastReceiver paramBroadcastReceiver, Context paramContext, Handler paramHandler, Instrumentation paramInstrumentation, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 68	android/app/LoadedApk:mReceivers	Ljava/util/HashMap;
        //     4: astore 6
        //     6: aload 6
        //     8: monitorenter
        //     9: aconst_null
        //     10: astore 7
        //     12: iload 5
        //     14: ifeq +176 -> 190
        //     17: aload_0
        //     18: getfield 68	android/app/LoadedApk:mReceivers	Ljava/util/HashMap;
        //     21: aload_2
        //     22: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     25: checkcast 65	java/util/HashMap
        //     28: astore 7
        //     30: aload 7
        //     32: ifnull +158 -> 190
        //     35: aload 7
        //     37: aload_1
        //     38: invokevirtual 291	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     41: checkcast 11	android/app/LoadedApk$ReceiverDispatcher
        //     44: astore 21
        //     46: aload 7
        //     48: astore 8
        //     50: aload 21
        //     52: astore 9
        //     54: aload 9
        //     56: ifnonnull +77 -> 133
        //     59: new 11	android/app/LoadedApk$ReceiverDispatcher
        //     62: dup
        //     63: aload_1
        //     64: aload_2
        //     65: aload_3
        //     66: aload 4
        //     68: iload 5
        //     70: invokespecial 451	android/app/LoadedApk$ReceiverDispatcher:<init>	(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V
        //     73: astore 14
        //     75: iload 5
        //     77: ifeq +107 -> 184
        //     80: aload 8
        //     82: ifnonnull +95 -> 177
        //     85: new 65	java/util/HashMap
        //     88: dup
        //     89: invokespecial 66	java/util/HashMap:<init>	()V
        //     92: astore 17
        //     94: aload_0
        //     95: getfield 68	android/app/LoadedApk:mReceivers	Ljava/util/HashMap;
        //     98: aload_2
        //     99: aload 17
        //     101: invokevirtual 306	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     104: pop
        //     105: aload 17
        //     107: aload_1
        //     108: aload 14
        //     110: invokevirtual 306	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     113: pop
        //     114: aload 14
        //     116: iconst_0
        //     117: putfield 324	android/app/LoadedApk$ReceiverDispatcher:mForgotten	Z
        //     120: aload 14
        //     122: invokevirtual 328	android/app/LoadedApk$ReceiverDispatcher:getIIntentReceiver	()Landroid/content/IIntentReceiver;
        //     125: astore 15
        //     127: aload 6
        //     129: monitorexit
        //     130: aload 15
        //     132: areturn
        //     133: aload 9
        //     135: aload_2
        //     136: aload_3
        //     137: invokevirtual 455	android/app/LoadedApk$ReceiverDispatcher:validate	(Landroid/content/Context;Landroid/os/Handler;)V
        //     140: aload 8
        //     142: pop
        //     143: aload 9
        //     145: astore 14
        //     147: goto -33 -> 114
        //     150: astore 10
        //     152: aload 6
        //     154: monitorexit
        //     155: aload 10
        //     157: athrow
        //     158: astore 10
        //     160: aload 8
        //     162: pop
        //     163: aload 9
        //     165: pop
        //     166: goto -14 -> 152
        //     169: astore 10
        //     171: aload 8
        //     173: pop
        //     174: goto -22 -> 152
        //     177: aload 8
        //     179: astore 17
        //     181: goto -76 -> 105
        //     184: aload 8
        //     186: pop
        //     187: goto -73 -> 114
        //     190: aload 7
        //     192: astore 8
        //     194: aconst_null
        //     195: astore 9
        //     197: goto -143 -> 54
        //
        // Exception table:
        //     from	to	target	type
        //     17	46	150	finally
        //     94	130	150	finally
        //     152	155	150	finally
        //     59	75	158	finally
        //     133	140	158	finally
        //     85	94	169	finally
    }

    public String getResDir()
    {
        return this.mResDir;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Resources getResources(ActivityThread paramActivityThread)
    {
        if (this.mResources == null)
            this.mResources = paramActivityThread.getTopLevelResources(this.mPackageName, this.mResDir, this);
        return this.mResources;
    }

    public final IServiceConnection getServiceDispatcher(ServiceConnection paramServiceConnection, Context paramContext, Handler paramHandler, int paramInt)
    {
        while (true)
        {
            HashMap localHashMap2;
            synchronized (this.mServices)
            {
                localHashMap2 = (HashMap)this.mServices.get(paramContext);
                if (localHashMap2 == null)
                    break label138;
                ServiceDispatcher localServiceDispatcher3 = (ServiceDispatcher)localHashMap2.get(paramServiceConnection);
                localServiceDispatcher1 = localServiceDispatcher3;
                if (localServiceDispatcher1 != null);
            }
            try
            {
                ServiceDispatcher localServiceDispatcher2 = new ServiceDispatcher(paramServiceConnection, paramContext, paramHandler, paramInt);
                if (localHashMap2 == null)
                {
                    localHashMap2 = new HashMap();
                    this.mServices.put(paramContext, localHashMap2);
                }
                localHashMap2.put(paramServiceConnection, localServiceDispatcher2);
                while (true)
                {
                    IServiceConnection localIServiceConnection = localServiceDispatcher2.getIServiceConnection();
                    return localIServiceConnection;
                    localServiceDispatcher1.validate(paramContext, paramHandler);
                    localServiceDispatcher2 = localServiceDispatcher1;
                }
                localObject1 = finally;
                label124: throw localObject1;
            }
            finally
            {
                break label124;
            }
            label138: ServiceDispatcher localServiceDispatcher1 = null;
        }
    }

    public boolean isSecurityViolation()
    {
        return this.mSecurityViolation;
    }

    public Application makeApplication(boolean paramBoolean, Instrumentation paramInstrumentation)
    {
        Application localApplication;
        if (this.mApplication != null)
            localApplication = this.mApplication;
        while (true)
        {
            return localApplication;
            localApplication = null;
            String str = this.mApplicationInfo.className;
            if ((paramBoolean) || (str == null))
                str = "android.app.Application";
            try
            {
                ClassLoader localClassLoader = getClassLoader();
                ContextImpl localContextImpl = new ContextImpl();
                localContextImpl.init(this, null, this.mActivityThread);
                localApplication = this.mActivityThread.mInstrumentation.newApplication(localClassLoader, str, localContextImpl);
                localContextImpl.setOuterContext(localApplication);
                this.mActivityThread.mAllApplications.add(localApplication);
                this.mApplication = localApplication;
                if (paramInstrumentation == null)
                    continue;
                try
                {
                    paramInstrumentation.callApplicationOnCreate(localApplication);
                }
                catch (Exception localException2)
                {
                }
                if (paramInstrumentation.onException(localApplication, localException2))
                    continue;
                throw new RuntimeException("Unable to create application " + localApplication.getClass().getName() + ": " + localException2.toString(), localException2);
            }
            catch (Exception localException1)
            {
                while (this.mActivityThread.mInstrumentation.onException(localApplication, localException1));
                throw new RuntimeException("Unable to instantiate application " + str + ": " + localException1.toString(), localException1);
            }
        }
    }

    public void removeContextRegistrations(Context paramContext, String paramString1, String paramString2)
    {
        boolean bool = StrictMode.vmRegistrationLeaksEnabled();
        HashMap localHashMap1 = (HashMap)this.mReceivers.remove(paramContext);
        if (localHashMap1 != null)
        {
            Iterator localIterator2 = localHashMap1.values().iterator();
            while (localIterator2.hasNext())
            {
                ReceiverDispatcher localReceiverDispatcher = (ReceiverDispatcher)localIterator2.next();
                IntentReceiverLeaked localIntentReceiverLeaked = new IntentReceiverLeaked(paramString2 + " " + paramString1 + " has leaked IntentReceiver " + localReceiverDispatcher.getIntentReceiver() + " that was " + "originally registered here. Are you missing a " + "call to unregisterReceiver()?");
                localIntentReceiverLeaked.setStackTrace(localReceiverDispatcher.getLocation().getStackTrace());
                Slog.e("ActivityThread", localIntentReceiverLeaked.getMessage(), localIntentReceiverLeaked);
                if (bool)
                    StrictMode.onIntentReceiverLeaked(localIntentReceiverLeaked);
                try
                {
                    ActivityManagerNative.getDefault().unregisterReceiver(localReceiverDispatcher.getIIntentReceiver());
                }
                catch (RemoteException localRemoteException2)
                {
                }
            }
        }
        this.mUnregisteredReceivers.remove(paramContext);
        HashMap localHashMap2 = (HashMap)this.mServices.remove(paramContext);
        Iterator localIterator1;
        if (localHashMap2 != null)
            localIterator1 = localHashMap2.values().iterator();
        while (true)
        {
            ServiceDispatcher localServiceDispatcher;
            if (localIterator1.hasNext())
            {
                localServiceDispatcher = (ServiceDispatcher)localIterator1.next();
                ServiceConnectionLeaked localServiceConnectionLeaked = new ServiceConnectionLeaked(paramString2 + " " + paramString1 + " has leaked ServiceConnection " + localServiceDispatcher.getServiceConnection() + " that was originally bound here");
                localServiceConnectionLeaked.setStackTrace(localServiceDispatcher.getLocation().getStackTrace());
                Slog.e("ActivityThread", localServiceConnectionLeaked.getMessage(), localServiceConnectionLeaked);
                if (bool)
                    StrictMode.onServiceConnectionLeaked(localServiceConnectionLeaked);
            }
            try
            {
                ActivityManagerNative.getDefault().unbindService(localServiceDispatcher.getIServiceConnection());
                label345: localServiceDispatcher.doForget();
                continue;
                this.mUnboundServices.remove(paramContext);
                return;
            }
            catch (RemoteException localRemoteException1)
            {
                break label345;
            }
        }
    }

    static final class ServiceDispatcher
    {
        private final HashMap<ComponentName, ConnectionInfo> mActiveConnections = new HashMap();
        private final Handler mActivityThread;
        private final ServiceConnection mConnection;
        private final Context mContext;
        private boolean mDied;
        private final int mFlags;
        private boolean mForgotten;
        private final InnerConnection mIServiceConnection = new InnerConnection(this);
        private final ServiceConnectionLeaked mLocation;
        private RuntimeException mUnbindLocation;

        ServiceDispatcher(ServiceConnection paramServiceConnection, Context paramContext, Handler paramHandler, int paramInt)
        {
            this.mConnection = paramServiceConnection;
            this.mContext = paramContext;
            this.mActivityThread = paramHandler;
            this.mLocation = new ServiceConnectionLeaked(null);
            this.mLocation.fillInStackTrace();
            this.mFlags = paramInt;
        }

        public void connected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            if (this.mActivityThread != null)
                this.mActivityThread.post(new RunConnection(paramComponentName, paramIBinder, 0));
            while (true)
            {
                return;
                doConnected(paramComponentName, paramIBinder);
            }
        }

        public void death(ComponentName paramComponentName, IBinder paramIBinder)
        {
            try
            {
                this.mDied = true;
                ConnectionInfo localConnectionInfo = (ConnectionInfo)this.mActiveConnections.remove(paramComponentName);
                if ((localConnectionInfo == null) || (localConnectionInfo.binder != paramIBinder))
                    return;
                localConnectionInfo.binder.unlinkToDeath(localConnectionInfo.deathMonitor, 0);
                if (this.mActivityThread != null)
                    this.mActivityThread.post(new RunConnection(paramComponentName, paramIBinder, 1));
            }
            finally
            {
            }
            doDeath(paramComponentName, paramIBinder);
        }

        // ERROR //
        public void doConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 115	android/app/LoadedApk$ServiceDispatcher:mForgotten	Z
            //     6: ifeq +8 -> 14
            //     9: aload_0
            //     10: monitorexit
            //     11: goto +188 -> 199
            //     14: aload_0
            //     15: getfield 49	android/app/LoadedApk$ServiceDispatcher:mActiveConnections	Ljava/util/HashMap;
            //     18: aload_1
            //     19: invokevirtual 118	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     22: checkcast 18	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
            //     25: astore 4
            //     27: aload 4
            //     29: ifnull +22 -> 51
            //     32: aload 4
            //     34: getfield 98	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:binder	Landroid/os/IBinder;
            //     37: aload_2
            //     38: if_acmpne +13 -> 51
            //     41: aload_0
            //     42: monitorexit
            //     43: goto +156 -> 199
            //     46: astore_3
            //     47: aload_0
            //     48: monitorexit
            //     49: aload_3
            //     50: athrow
            //     51: aload_2
            //     52: ifnull +135 -> 187
            //     55: aload_0
            //     56: iconst_0
            //     57: putfield 90	android/app/LoadedApk$ServiceDispatcher:mDied	Z
            //     60: new 18	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
            //     63: dup
            //     64: aconst_null
            //     65: invokespecial 121	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:<init>	(Landroid/app/LoadedApk$1;)V
            //     68: astore 7
            //     70: aload 7
            //     72: aload_2
            //     73: putfield 98	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:binder	Landroid/os/IBinder;
            //     76: aload 7
            //     78: new 9	android/app/LoadedApk$ServiceDispatcher$DeathMonitor
            //     81: dup
            //     82: aload_0
            //     83: aload_1
            //     84: aload_2
            //     85: invokespecial 124	android/app/LoadedApk$ServiceDispatcher$DeathMonitor:<init>	(Landroid/app/LoadedApk$ServiceDispatcher;Landroid/content/ComponentName;Landroid/os/IBinder;)V
            //     88: putfield 102	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:deathMonitor	Landroid/os/IBinder$DeathRecipient;
            //     91: aload_2
            //     92: aload 7
            //     94: getfield 102	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:deathMonitor	Landroid/os/IBinder$DeathRecipient;
            //     97: iconst_0
            //     98: invokeinterface 128 3 0
            //     103: aload_0
            //     104: getfield 49	android/app/LoadedApk$ServiceDispatcher:mActiveConnections	Ljava/util/HashMap;
            //     107: aload_1
            //     108: aload 7
            //     110: invokevirtual 132	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     113: pop
            //     114: aload 4
            //     116: ifnull +20 -> 136
            //     119: aload 4
            //     121: getfield 98	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:binder	Landroid/os/IBinder;
            //     124: aload 4
            //     126: getfield 102	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:deathMonitor	Landroid/os/IBinder$DeathRecipient;
            //     129: iconst_0
            //     130: invokeinterface 108 3 0
            //     135: pop
            //     136: aload_0
            //     137: monitorexit
            //     138: aload 4
            //     140: ifnull +13 -> 153
            //     143: aload_0
            //     144: getfield 56	android/app/LoadedApk$ServiceDispatcher:mConnection	Landroid/content/ServiceConnection;
            //     147: aload_1
            //     148: invokeinterface 138 2 0
            //     153: aload_2
            //     154: ifnull +45 -> 199
            //     157: aload_0
            //     158: getfield 56	android/app/LoadedApk$ServiceDispatcher:mConnection	Landroid/content/ServiceConnection;
            //     161: aload_1
            //     162: aload_2
            //     163: invokeinterface 141 3 0
            //     168: goto +31 -> 199
            //     171: astore 8
            //     173: aload_0
            //     174: getfield 49	android/app/LoadedApk$ServiceDispatcher:mActiveConnections	Ljava/util/HashMap;
            //     177: aload_1
            //     178: invokevirtual 94	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
            //     181: pop
            //     182: aload_0
            //     183: monitorexit
            //     184: goto +15 -> 199
            //     187: aload_0
            //     188: getfield 49	android/app/LoadedApk$ServiceDispatcher:mActiveConnections	Ljava/util/HashMap;
            //     191: aload_1
            //     192: invokevirtual 94	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
            //     195: pop
            //     196: goto -82 -> 114
            //     199: return
            //
            // Exception table:
            //     from	to	target	type
            //     2	49	46	finally
            //     55	91	46	finally
            //     91	114	46	finally
            //     119	138	46	finally
            //     173	196	46	finally
            //     91	114	171	android/os/RemoteException
        }

        public void doDeath(ComponentName paramComponentName, IBinder paramIBinder)
        {
            this.mConnection.onServiceDisconnected(paramComponentName);
        }

        // ERROR //
        void doForget()
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 49	android/app/LoadedApk$ServiceDispatcher:mActiveConnections	Ljava/util/HashMap;
            //     6: invokevirtual 146	java/util/HashMap:values	()Ljava/util/Collection;
            //     9: invokeinterface 152 1 0
            //     14: astore_2
            //     15: aload_2
            //     16: invokeinterface 158 1 0
            //     21: ifeq +36 -> 57
            //     24: aload_2
            //     25: invokeinterface 162 1 0
            //     30: checkcast 18	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
            //     33: astore_3
            //     34: aload_3
            //     35: getfield 98	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:binder	Landroid/os/IBinder;
            //     38: aload_3
            //     39: getfield 102	android/app/LoadedApk$ServiceDispatcher$ConnectionInfo:deathMonitor	Landroid/os/IBinder$DeathRecipient;
            //     42: iconst_0
            //     43: invokeinterface 108 3 0
            //     48: pop
            //     49: goto -34 -> 15
            //     52: astore_1
            //     53: aload_0
            //     54: monitorexit
            //     55: aload_1
            //     56: athrow
            //     57: aload_0
            //     58: getfield 49	android/app/LoadedApk$ServiceDispatcher:mActiveConnections	Ljava/util/HashMap;
            //     61: invokevirtual 165	java/util/HashMap:clear	()V
            //     64: aload_0
            //     65: iconst_1
            //     66: putfield 115	android/app/LoadedApk$ServiceDispatcher:mForgotten	Z
            //     69: aload_0
            //     70: monitorexit
            //     71: return
            //
            // Exception table:
            //     from	to	target	type
            //     2	55	52	finally
            //     57	71	52	finally
        }

        int getFlags()
        {
            return this.mFlags;
        }

        IServiceConnection getIServiceConnection()
        {
            return this.mIServiceConnection;
        }

        ServiceConnectionLeaked getLocation()
        {
            return this.mLocation;
        }

        ServiceConnection getServiceConnection()
        {
            return this.mConnection;
        }

        RuntimeException getUnbindLocation()
        {
            return this.mUnbindLocation;
        }

        void setUnbindLocation(RuntimeException paramRuntimeException)
        {
            this.mUnbindLocation = paramRuntimeException;
        }

        void validate(Context paramContext, Handler paramHandler)
        {
            if (this.mContext != paramContext)
                throw new RuntimeException("ServiceConnection " + this.mConnection + " registered with differing Context (was " + this.mContext + " now " + paramContext + ")");
            if (this.mActivityThread != paramHandler)
                throw new RuntimeException("ServiceConnection " + this.mConnection + " registered with differing handler (was " + this.mActivityThread + " now " + paramHandler + ")");
        }

        private final class DeathMonitor
            implements IBinder.DeathRecipient
        {
            final ComponentName mName;
            final IBinder mService;

            DeathMonitor(ComponentName paramIBinder, IBinder arg3)
            {
                this.mName = paramIBinder;
                Object localObject;
                this.mService = localObject;
            }

            public void binderDied()
            {
                LoadedApk.ServiceDispatcher.this.death(this.mName, this.mService);
            }
        }

        private final class RunConnection
            implements Runnable
        {
            final int mCommand;
            final ComponentName mName;
            final IBinder mService;

            RunConnection(ComponentName paramIBinder, IBinder paramInt, int arg4)
            {
                this.mName = paramIBinder;
                this.mService = paramInt;
                int i;
                this.mCommand = i;
            }

            public void run()
            {
                if (this.mCommand == 0)
                    LoadedApk.ServiceDispatcher.this.doConnected(this.mName, this.mService);
                while (true)
                {
                    return;
                    if (this.mCommand == 1)
                        LoadedApk.ServiceDispatcher.this.doDeath(this.mName, this.mService);
                }
            }
        }

        private static class InnerConnection extends IServiceConnection.Stub
        {
            final WeakReference<LoadedApk.ServiceDispatcher> mDispatcher;

            InnerConnection(LoadedApk.ServiceDispatcher paramServiceDispatcher)
            {
                this.mDispatcher = new WeakReference(paramServiceDispatcher);
            }

            public void connected(ComponentName paramComponentName, IBinder paramIBinder)
                throws RemoteException
            {
                LoadedApk.ServiceDispatcher localServiceDispatcher = (LoadedApk.ServiceDispatcher)this.mDispatcher.get();
                if (localServiceDispatcher != null)
                    localServiceDispatcher.connected(paramComponentName, paramIBinder);
            }
        }

        private static class ConnectionInfo
        {
            IBinder binder;
            IBinder.DeathRecipient deathMonitor;
        }
    }

    static final class ReceiverDispatcher
    {
        final Handler mActivityThread;
        final Context mContext;
        boolean mForgotten;
        final IIntentReceiver.Stub mIIntentReceiver;
        final Instrumentation mInstrumentation;
        final IntentReceiverLeaked mLocation;
        final BroadcastReceiver mReceiver;
        final boolean mRegistered;
        RuntimeException mUnregisterLocation;

        ReceiverDispatcher(BroadcastReceiver paramBroadcastReceiver, Context paramContext, Handler paramHandler, Instrumentation paramInstrumentation, boolean paramBoolean)
        {
            if (paramHandler == null)
                throw new NullPointerException("Handler must not be null");
            if (!paramBoolean);
            for (boolean bool = true; ; bool = false)
            {
                this.mIIntentReceiver = new InnerReceiver(this, bool);
                this.mReceiver = paramBroadcastReceiver;
                this.mContext = paramContext;
                this.mActivityThread = paramHandler;
                this.mInstrumentation = paramInstrumentation;
                this.mRegistered = paramBoolean;
                this.mLocation = new IntentReceiverLeaked(null);
                this.mLocation.fillInStackTrace();
                return;
            }
        }

        IIntentReceiver getIIntentReceiver()
        {
            return this.mIIntentReceiver;
        }

        BroadcastReceiver getIntentReceiver()
        {
            return this.mReceiver;
        }

        IntentReceiverLeaked getLocation()
        {
            return this.mLocation;
        }

        RuntimeException getUnregisterLocation()
        {
            return this.mUnregisterLocation;
        }

        public void performReceive(Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
        {
            Args localArgs = new Args(paramIntent, paramInt, paramString, paramBundle, paramBoolean1, paramBoolean2);
            if ((!this.mActivityThread.post(localArgs)) && (this.mRegistered) && (paramBoolean1))
                localArgs.sendFinished(ActivityManagerNative.getDefault());
        }

        void setUnregisterLocation(RuntimeException paramRuntimeException)
        {
            this.mUnregisterLocation = paramRuntimeException;
        }

        void validate(Context paramContext, Handler paramHandler)
        {
            if (this.mContext != paramContext)
                throw new IllegalStateException("Receiver " + this.mReceiver + " registered with differing Context (was " + this.mContext + " now " + paramContext + ")");
            if (this.mActivityThread != paramHandler)
                throw new IllegalStateException("Receiver " + this.mReceiver + " registered with differing handler (was " + this.mActivityThread + " now " + paramHandler + ")");
        }

        final class Args extends BroadcastReceiver.PendingResult
            implements Runnable
        {
            private Intent mCurIntent;
            private final boolean mOrdered;

            public Args(Intent paramInt, int paramString, String paramBundle, Bundle paramBoolean1, boolean paramBoolean2, boolean arg7)
            {
            }

            public void run()
            {
                BroadcastReceiver localBroadcastReceiver = LoadedApk.ReceiverDispatcher.this.mReceiver;
                boolean bool = this.mOrdered;
                IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
                Intent localIntent = this.mCurIntent;
                this.mCurIntent = null;
                if ((localBroadcastReceiver == null) || (LoadedApk.ReceiverDispatcher.this.mForgotten))
                    if ((LoadedApk.ReceiverDispatcher.this.mRegistered) && (bool))
                        sendFinished(localIActivityManager);
                while (true)
                {
                    return;
                    Trace.traceBegin(64L, "broadcastReceiveReg");
                    try
                    {
                        ClassLoader localClassLoader = LoadedApk.ReceiverDispatcher.this.mReceiver.getClass().getClassLoader();
                        localIntent.setExtrasClassLoader(localClassLoader);
                        setExtrasClassLoader(localClassLoader);
                        localBroadcastReceiver.setPendingResult(this);
                        localBroadcastReceiver.onReceive(LoadedApk.ReceiverDispatcher.this.mContext, localIntent);
                        if (localBroadcastReceiver.getPendingResult() != null)
                            finish();
                        Trace.traceEnd(64L);
                    }
                    catch (Exception localException)
                    {
                        do
                            if ((LoadedApk.ReceiverDispatcher.this.mRegistered) && (bool))
                                sendFinished(localIActivityManager);
                        while ((LoadedApk.ReceiverDispatcher.this.mInstrumentation != null) && (LoadedApk.ReceiverDispatcher.this.mInstrumentation.onException(LoadedApk.ReceiverDispatcher.this.mReceiver, localException)));
                        Trace.traceEnd(64L);
                        throw new RuntimeException("Error receiving broadcast " + localIntent + " in " + LoadedApk.ReceiverDispatcher.this.mReceiver, localException);
                    }
                }
            }
        }

        static final class InnerReceiver extends IIntentReceiver.Stub
        {
            final WeakReference<LoadedApk.ReceiverDispatcher> mDispatcher;
            final LoadedApk.ReceiverDispatcher mStrongRef;

            InnerReceiver(LoadedApk.ReceiverDispatcher paramReceiverDispatcher, boolean paramBoolean)
            {
                this.mDispatcher = new WeakReference(paramReceiverDispatcher);
                if (paramBoolean);
                while (true)
                {
                    this.mStrongRef = paramReceiverDispatcher;
                    return;
                    paramReceiverDispatcher = null;
                }
            }

            public void performReceive(Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
            {
                LoadedApk.ReceiverDispatcher localReceiverDispatcher = (LoadedApk.ReceiverDispatcher)this.mDispatcher.get();
                if (localReceiverDispatcher != null)
                    localReceiverDispatcher.performReceive(paramIntent, paramInt, paramString, paramBundle, paramBoolean1, paramBoolean2);
                while (true)
                {
                    return;
                    IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
                    if (paramBundle != null);
                    try
                    {
                        paramBundle.setAllowFds(false);
                        localIActivityManager.finishReceiver(this, paramInt, paramString, paramBundle, false);
                    }
                    catch (RemoteException localRemoteException)
                    {
                        Slog.w("ActivityThread", "Couldn't finish broadcast to unregistered receiver");
                    }
                }
            }
        }
    }

    private static class WarningContextClassLoader extends ClassLoader
    {
        private static boolean warned = false;

        private void warn(String paramString)
        {
            if (warned);
            while (true)
            {
                return;
                warned = true;
                Thread.currentThread().setContextClassLoader(getParent());
                Slog.w("ActivityThread", "ClassLoader." + paramString + ": " + "The class loader returned by " + "Thread.getContextClassLoader() may fail for processes " + "that host multiple applications. You should explicitly " + "specify a context class loader. For example: " + "Thread.setContextClassLoader(getClass().getClassLoader());");
            }
        }

        public void clearAssertionStatus()
        {
            warn("clearAssertionStatus");
            getParent().clearAssertionStatus();
        }

        public URL getResource(String paramString)
        {
            warn("getResource");
            return getParent().getResource(paramString);
        }

        public InputStream getResourceAsStream(String paramString)
        {
            warn("getResourceAsStream");
            return getParent().getResourceAsStream(paramString);
        }

        public Enumeration<URL> getResources(String paramString)
            throws IOException
        {
            warn("getResources");
            return getParent().getResources(paramString);
        }

        public Class<?> loadClass(String paramString)
            throws ClassNotFoundException
        {
            warn("loadClass");
            return getParent().loadClass(paramString);
        }

        public void setClassAssertionStatus(String paramString, boolean paramBoolean)
        {
            warn("setClassAssertionStatus");
            getParent().setClassAssertionStatus(paramString, paramBoolean);
        }

        public void setDefaultAssertionStatus(boolean paramBoolean)
        {
            warn("setDefaultAssertionStatus");
            getParent().setDefaultAssertionStatus(paramBoolean);
        }

        public void setPackageAssertionStatus(String paramString, boolean paramBoolean)
        {
            warn("setPackageAssertionStatus");
            getParent().setPackageAssertionStatus(paramString, paramBoolean);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.LoadedApk
 * JD-Core Version:        0.6.2
 */