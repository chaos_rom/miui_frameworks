package android.app;

import android.graphics.Bitmap;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IThumbnailRetriever extends IInterface
{
    public abstract Bitmap getThumbnail(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IThumbnailRetriever
    {
        private static final String DESCRIPTOR = "android.app.IThumbnailRetriever";
        static final int TRANSACTION_getThumbnail = 1;

        public Stub()
        {
            attachInterface(this, "android.app.IThumbnailRetriever");
        }

        public static IThumbnailRetriever asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IThumbnailRetriever");
                if ((localIInterface != null) && ((localIInterface instanceof IThumbnailRetriever)))
                    localObject = (IThumbnailRetriever)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.app.IThumbnailRetriever");
                continue;
                paramParcel1.enforceInterface("android.app.IThumbnailRetriever");
                Bitmap localBitmap = getThumbnail(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localBitmap != null)
                {
                    paramParcel2.writeInt(i);
                    localBitmap.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                }
            }
        }

        private static class Proxy
            implements IThumbnailRetriever
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IThumbnailRetriever";
            }

            public Bitmap getThumbnail(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IThumbnailRetriever");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(localParcel2);
                        return localBitmap;
                    }
                    Bitmap localBitmap = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IThumbnailRetriever
 * JD-Core Version:        0.6.2
 */