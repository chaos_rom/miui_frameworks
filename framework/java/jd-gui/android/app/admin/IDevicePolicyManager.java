package android.app.admin;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteCallback;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IDevicePolicyManager extends IInterface
{
    public abstract List<ComponentName> getActiveAdmins()
        throws RemoteException;

    public abstract boolean getCameraDisabled(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getCurrentFailedPasswordAttempts()
        throws RemoteException;

    public abstract ComponentName getGlobalProxyAdmin()
        throws RemoteException;

    public abstract int getMaximumFailedPasswordsForWipe(ComponentName paramComponentName)
        throws RemoteException;

    public abstract long getMaximumTimeToLock(ComponentName paramComponentName)
        throws RemoteException;

    public abstract long getPasswordExpiration(ComponentName paramComponentName)
        throws RemoteException;

    public abstract long getPasswordExpirationTimeout(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordHistoryLength(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumLength(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumLetters(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumLowerCase(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumNonLetter(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumNumeric(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumSymbols(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordMinimumUpperCase(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getPasswordQuality(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void getRemoveWarning(ComponentName paramComponentName, RemoteCallback paramRemoteCallback)
        throws RemoteException;

    public abstract boolean getStorageEncryption(ComponentName paramComponentName)
        throws RemoteException;

    public abstract int getStorageEncryptionStatus()
        throws RemoteException;

    public abstract boolean hasGrantedPolicy(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract boolean isActivePasswordSufficient()
        throws RemoteException;

    public abstract boolean isAdminActive(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void lockNow()
        throws RemoteException;

    public abstract boolean packageHasActiveAdmins(String paramString)
        throws RemoteException;

    public abstract void removeActiveAdmin(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void reportFailedPasswordAttempt()
        throws RemoteException;

    public abstract void reportSuccessfulPasswordAttempt()
        throws RemoteException;

    public abstract boolean resetPassword(String paramString, int paramInt)
        throws RemoteException;

    public abstract void setActiveAdmin(ComponentName paramComponentName, boolean paramBoolean)
        throws RemoteException;

    public abstract void setActivePasswordState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
        throws RemoteException;

    public abstract void setCameraDisabled(ComponentName paramComponentName, boolean paramBoolean)
        throws RemoteException;

    public abstract ComponentName setGlobalProxy(ComponentName paramComponentName, String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setMaximumFailedPasswordsForWipe(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setMaximumTimeToLock(ComponentName paramComponentName, long paramLong)
        throws RemoteException;

    public abstract void setPasswordExpirationTimeout(ComponentName paramComponentName, long paramLong)
        throws RemoteException;

    public abstract void setPasswordHistoryLength(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumLength(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumLetters(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumLowerCase(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumNonLetter(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumNumeric(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumSymbols(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordMinimumUpperCase(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void setPasswordQuality(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract int setStorageEncryption(ComponentName paramComponentName, boolean paramBoolean)
        throws RemoteException;

    public abstract void wipeData(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IDevicePolicyManager
    {
        private static final String DESCRIPTOR = "android.app.admin.IDevicePolicyManager";
        static final int TRANSACTION_getActiveAdmins = 40;
        static final int TRANSACTION_getCameraDisabled = 37;
        static final int TRANSACTION_getCurrentFailedPasswordAttempts = 23;
        static final int TRANSACTION_getGlobalProxyAdmin = 32;
        static final int TRANSACTION_getMaximumFailedPasswordsForWipe = 25;
        static final int TRANSACTION_getMaximumTimeToLock = 28;
        static final int TRANSACTION_getPasswordExpiration = 21;
        static final int TRANSACTION_getPasswordExpirationTimeout = 20;
        static final int TRANSACTION_getPasswordHistoryLength = 18;
        static final int TRANSACTION_getPasswordMinimumLength = 4;
        static final int TRANSACTION_getPasswordMinimumLetters = 10;
        static final int TRANSACTION_getPasswordMinimumLowerCase = 8;
        static final int TRANSACTION_getPasswordMinimumNonLetter = 16;
        static final int TRANSACTION_getPasswordMinimumNumeric = 12;
        static final int TRANSACTION_getPasswordMinimumSymbols = 14;
        static final int TRANSACTION_getPasswordMinimumUpperCase = 6;
        static final int TRANSACTION_getPasswordQuality = 2;
        static final int TRANSACTION_getRemoveWarning = 42;
        static final int TRANSACTION_getStorageEncryption = 34;
        static final int TRANSACTION_getStorageEncryptionStatus = 35;
        static final int TRANSACTION_hasGrantedPolicy = 44;
        static final int TRANSACTION_isActivePasswordSufficient = 22;
        static final int TRANSACTION_isAdminActive = 39;
        static final int TRANSACTION_lockNow = 29;
        static final int TRANSACTION_packageHasActiveAdmins = 41;
        static final int TRANSACTION_removeActiveAdmin = 43;
        static final int TRANSACTION_reportFailedPasswordAttempt = 46;
        static final int TRANSACTION_reportSuccessfulPasswordAttempt = 47;
        static final int TRANSACTION_resetPassword = 26;
        static final int TRANSACTION_setActiveAdmin = 38;
        static final int TRANSACTION_setActivePasswordState = 45;
        static final int TRANSACTION_setCameraDisabled = 36;
        static final int TRANSACTION_setGlobalProxy = 31;
        static final int TRANSACTION_setMaximumFailedPasswordsForWipe = 24;
        static final int TRANSACTION_setMaximumTimeToLock = 27;
        static final int TRANSACTION_setPasswordExpirationTimeout = 19;
        static final int TRANSACTION_setPasswordHistoryLength = 17;
        static final int TRANSACTION_setPasswordMinimumLength = 3;
        static final int TRANSACTION_setPasswordMinimumLetters = 9;
        static final int TRANSACTION_setPasswordMinimumLowerCase = 7;
        static final int TRANSACTION_setPasswordMinimumNonLetter = 15;
        static final int TRANSACTION_setPasswordMinimumNumeric = 11;
        static final int TRANSACTION_setPasswordMinimumSymbols = 13;
        static final int TRANSACTION_setPasswordMinimumUpperCase = 5;
        static final int TRANSACTION_setPasswordQuality = 1;
        static final int TRANSACTION_setStorageEncryption = 33;
        static final int TRANSACTION_wipeData = 30;

        public Stub()
        {
            attachInterface(this, "android.app.admin.IDevicePolicyManager");
        }

        public static IDevicePolicyManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.admin.IDevicePolicyManager");
                if ((localIInterface != null) && ((localIInterface instanceof IDevicePolicyManager)))
                    localObject = (IDevicePolicyManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.app.admin.IDevicePolicyManager");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName37 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName37 = null)
                {
                    setPasswordQuality(localComponentName37, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName36 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName36 = null)
                {
                    int i15 = getPasswordQuality(localComponentName36);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i15);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName35 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName35 = null)
                {
                    setPasswordMinimumLength(localComponentName35, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName34 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName34 = null)
                {
                    int i14 = getPasswordMinimumLength(localComponentName34);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i14);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName33 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName33 = null)
                {
                    setPasswordMinimumUpperCase(localComponentName33, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName32 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName32 = null)
                {
                    int i13 = getPasswordMinimumUpperCase(localComponentName32);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i13);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName31 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName31 = null)
                {
                    setPasswordMinimumLowerCase(localComponentName31, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName30 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName30 = null)
                {
                    int i12 = getPasswordMinimumLowerCase(localComponentName30);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i12);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName29 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName29 = null)
                {
                    setPasswordMinimumLetters(localComponentName29, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName28 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName28 = null)
                {
                    int i11 = getPasswordMinimumLetters(localComponentName28);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i11);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName27 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName27 = null)
                {
                    setPasswordMinimumNumeric(localComponentName27, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName26 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName26 = null)
                {
                    int i10 = getPasswordMinimumNumeric(localComponentName26);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i10);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName25 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName25 = null)
                {
                    setPasswordMinimumSymbols(localComponentName25, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName24 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName24 = null)
                {
                    int i9 = getPasswordMinimumSymbols(localComponentName24);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i9);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName23 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName23 = null)
                {
                    setPasswordMinimumNonLetter(localComponentName23, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName22 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName22 = null)
                {
                    int i8 = getPasswordMinimumNonLetter(localComponentName22);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i8);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName21 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName21 = null)
                {
                    setPasswordHistoryLength(localComponentName21, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName20 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName20 = null)
                {
                    int i7 = getPasswordHistoryLength(localComponentName20);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i7);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName19 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName19 = null)
                {
                    setPasswordExpirationTimeout(localComponentName19, paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName18 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName18 = null)
                {
                    long l3 = getPasswordExpirationTimeout(localComponentName18);
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l3);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName17 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName17 = null)
                {
                    long l2 = getPasswordExpiration(localComponentName17);
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l2);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                boolean bool11 = isActivePasswordSufficient();
                paramParcel2.writeNoException();
                if (bool11);
                for (int i6 = 1; ; i6 = 0)
                {
                    paramParcel2.writeInt(i6);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                int i5 = getCurrentFailedPasswordAttempts();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i5);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName16 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName16 = null)
                {
                    setMaximumFailedPasswordsForWipe(localComponentName16, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName15 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName15 = null)
                {
                    int i4 = getMaximumFailedPasswordsForWipe(localComponentName15);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i4);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                boolean bool10 = resetPassword(paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool10);
                for (int i3 = 1; ; i3 = 0)
                {
                    paramParcel2.writeInt(i3);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName14 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName14 = null)
                {
                    setMaximumTimeToLock(localComponentName14, paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName13 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName13 = null)
                {
                    long l1 = getMaximumTimeToLock(localComponentName13);
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l1);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                lockNow();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                wipeData(paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName11;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName11 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label1990: ComponentName localComponentName12 = setGlobalProxy(localComponentName11, paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localComponentName12 == null)
                        break label2039;
                    paramParcel2.writeInt(1);
                    localComponentName12.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localComponentName11 = null;
                    break label1990;
                    label2039: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName10 = getGlobalProxyAdmin();
                paramParcel2.writeNoException();
                if (localComponentName10 != null)
                {
                    paramParcel2.writeInt(1);
                    localComponentName10.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName9;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName9 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2121: if (paramParcel1.readInt() == 0)
                        break label2163;
                }
                label2163: for (boolean bool9 = true; ; bool9 = false)
                {
                    int i2 = setStorageEncryption(localComponentName9, bool9);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i2);
                    bool1 = true;
                    break;
                    localComponentName9 = null;
                    break label2121;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName8;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName8 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2196: boolean bool8 = getStorageEncryption(localComponentName8);
                    paramParcel2.writeNoException();
                    if (!bool8)
                        break label2234;
                }
                label2234: for (int i1 = 1; ; i1 = 0)
                {
                    paramParcel2.writeInt(i1);
                    bool1 = true;
                    break;
                    localComponentName8 = null;
                    break label2196;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                int n = getStorageEncryptionStatus();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(n);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName7;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName7 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2295: if (paramParcel1.readInt() == 0)
                        break label2329;
                }
                label2329: for (boolean bool7 = true; ; bool7 = false)
                {
                    setCameraDisabled(localComponentName7, bool7);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    localComponentName7 = null;
                    break label2295;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName6;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName6 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2362: boolean bool6 = getCameraDisabled(localComponentName6);
                    paramParcel2.writeNoException();
                    if (!bool6)
                        break label2400;
                }
                label2400: for (int m = 1; ; m = 0)
                {
                    paramParcel2.writeInt(m);
                    bool1 = true;
                    break;
                    localComponentName6 = null;
                    break label2362;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName5;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName5 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2433: if (paramParcel1.readInt() == 0)
                        break label2467;
                }
                label2467: for (boolean bool5 = true; ; bool5 = false)
                {
                    setActiveAdmin(localComponentName5, bool5);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    localComponentName5 = null;
                    break label2433;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName4;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName4 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2500: boolean bool4 = isAdminActive(localComponentName4);
                    paramParcel2.writeNoException();
                    if (!bool4)
                        break label2538;
                }
                label2538: for (int k = 1; ; k = 0)
                {
                    paramParcel2.writeInt(k);
                    bool1 = true;
                    break;
                    localComponentName4 = null;
                    break label2500;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                List localList = getActiveAdmins();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                boolean bool3 = packageHasActiveAdmins(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool3);
                for (int j = 1; ; j = 0)
                {
                    paramParcel2.writeInt(j);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName3;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2645: if (paramParcel1.readInt() == 0)
                        break label2690;
                }
                label2690: for (RemoteCallback localRemoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(paramParcel1); ; localRemoteCallback = null)
                {
                    getRemoveWarning(localComponentName3, localRemoteCallback);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    localComponentName3 = null;
                    break label2645;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName2 = null)
                {
                    removeActiveAdmin(localComponentName2);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                ComponentName localComponentName1;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    label2772: boolean bool2 = hasGrantedPolicy(localComponentName1, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (!bool2)
                        break label2814;
                }
                label2814: for (int i = 1; ; i = 0)
                {
                    paramParcel2.writeInt(i);
                    bool1 = true;
                    break;
                    localComponentName1 = null;
                    break label2772;
                }
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                setActivePasswordState(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                reportFailedPasswordAttempt();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.app.admin.IDevicePolicyManager");
                reportSuccessfulPasswordAttempt();
                paramParcel2.writeNoException();
                bool1 = true;
            }
        }

        private static class Proxy
            implements IDevicePolicyManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public List<ComponentName> getActiveAdmins()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(40, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(ComponentName.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getCameraDisabled(ComponentName paramComponentName)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(37, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public int getCurrentFailedPasswordAttempts()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ComponentName getGlobalProxyAdmin()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(localParcel2);
                        return localComponentName;
                    }
                    ComponentName localComponentName = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.admin.IDevicePolicyManager";
            }

            public int getMaximumFailedPasswordsForWipe(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(25, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getMaximumTimeToLock(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(28, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        long l = localParcel2.readLong();
                        return l;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getPasswordExpiration(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(21, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        long l = localParcel2.readLong();
                        return l;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getPasswordExpirationTimeout(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        long l = localParcel2.readLong();
                        return l;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordHistoryLength(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(18, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumLength(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumLetters(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumLowerCase(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumNonLetter(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(16, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumNumeric(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(12, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumSymbols(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordMinimumUpperCase(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPasswordQuality(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getRemoveWarning(ComponentName paramComponentName, RemoteCallback paramRemoteCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            if (paramRemoteCallback != null)
                            {
                                localParcel1.writeInt(1);
                                paramRemoteCallback.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(42, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public boolean getStorageEncryption(ComponentName paramComponentName)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(34, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public int getStorageEncryptionStatus()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasGrantedPolicy(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(44, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean isActivePasswordSufficient()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isAdminActive(ComponentName paramComponentName)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(39, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void lockNow()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean packageHasActiveAdmins(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(41, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeActiveAdmin(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(43, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reportFailedPasswordAttempt()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(46, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reportSuccessfulPasswordAttempt()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    this.mRemote.transact(47, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean resetPassword(String paramString, int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setActiveAdmin(ComponentName paramComponentName, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            break label107;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(38, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label107: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void setActivePasswordState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    localParcel1.writeInt(paramInt4);
                    localParcel1.writeInt(paramInt5);
                    localParcel1.writeInt(paramInt6);
                    localParcel1.writeInt(paramInt7);
                    localParcel1.writeInt(paramInt8);
                    this.mRemote.transact(45, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCameraDisabled(ComponentName paramComponentName, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            break label107;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(36, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label107: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public ComponentName setGlobalProxy(ComponentName paramComponentName, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString1);
                            localParcel1.writeString(paramString2);
                            this.mRemote.transact(31, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(localParcel2);
                                return localComponentName;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ComponentName localComponentName = null;
                }
            }

            public void setMaximumFailedPasswordsForWipe(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(24, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMaximumTimeToLock(ComponentName paramComponentName, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeLong(paramLong);
                        this.mRemote.transact(27, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordExpirationTimeout(ComponentName paramComponentName, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeLong(paramLong);
                        this.mRemote.transact(19, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordHistoryLength(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(17, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumLength(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumLetters(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumLowerCase(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumNonLetter(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(15, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumNumeric(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumSymbols(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordMinimumUpperCase(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPasswordQuality(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int setStorageEncryption(ComponentName paramComponentName, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            break label116;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(33, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int j = localParcel2.readInt();
                            return j;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label116: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void wipeData(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.admin.IDevicePolicyManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.admin.IDevicePolicyManager
 * JD-Core Version:        0.6.2
 */