package android.app.admin;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy.Type;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

public class DevicePolicyManager
{
    public static final String ACTION_ADD_DEVICE_ADMIN = "android.app.action.ADD_DEVICE_ADMIN";
    public static final String ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED = "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED";
    public static final String ACTION_SET_NEW_PASSWORD = "android.app.action.SET_NEW_PASSWORD";
    public static final String ACTION_START_ENCRYPTION = "android.app.action.START_ENCRYPTION";
    public static final int ENCRYPTION_STATUS_ACTIVATING = 2;
    public static final int ENCRYPTION_STATUS_ACTIVE = 3;
    public static final int ENCRYPTION_STATUS_INACTIVE = 1;
    public static final int ENCRYPTION_STATUS_UNSUPPORTED = 0;
    public static final String EXTRA_ADD_EXPLANATION = "android.app.extra.ADD_EXPLANATION";
    public static final String EXTRA_DEVICE_ADMIN = "android.app.extra.DEVICE_ADMIN";
    public static final int PASSWORD_QUALITY_ALPHABETIC = 262144;
    public static final int PASSWORD_QUALITY_ALPHANUMERIC = 327680;
    public static final int PASSWORD_QUALITY_BIOMETRIC_WEAK = 32768;
    public static final int PASSWORD_QUALITY_COMPLEX = 393216;
    public static final int PASSWORD_QUALITY_NUMERIC = 131072;
    public static final int PASSWORD_QUALITY_SOMETHING = 65536;
    public static final int PASSWORD_QUALITY_UNSPECIFIED = 0;
    public static final int RESET_PASSWORD_REQUIRE_ENTRY = 1;
    private static String TAG = "DevicePolicyManager";
    public static final int WIPE_EXTERNAL_STORAGE = 1;
    private final Context mContext;
    private final IDevicePolicyManager mService;

    private DevicePolicyManager(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mService = IDevicePolicyManager.Stub.asInterface(ServiceManager.getService("device_policy"));
    }

    public static DevicePolicyManager create(Context paramContext, Handler paramHandler)
    {
        DevicePolicyManager localDevicePolicyManager = new DevicePolicyManager(paramContext, paramHandler);
        if (localDevicePolicyManager.mService != null);
        while (true)
        {
            return localDevicePolicyManager;
            localDevicePolicyManager = null;
        }
    }

    public List<ComponentName> getActiveAdmins()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                List localList2 = this.mService.getActiveAdmins();
                localList1 = localList2;
                return localList1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            List localList1 = null;
        }
    }

    public DeviceAdminInfo getAdminInfo(ComponentName paramComponentName)
    {
        try
        {
            ActivityInfo localActivityInfo = this.mContext.getPackageManager().getReceiverInfo(paramComponentName, 128);
            localResolveInfo = new ResolveInfo();
            localResolveInfo.activityInfo = localActivityInfo;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            try
            {
                ResolveInfo localResolveInfo;
                for (localDeviceAdminInfo = new DeviceAdminInfo(this.mContext, localResolveInfo); ; localDeviceAdminInfo = null)
                {
                    return localDeviceAdminInfo;
                    localNameNotFoundException = localNameNotFoundException;
                    Log.w(TAG, "Unable to retrieve device policy " + paramComponentName, localNameNotFoundException);
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                {
                    Log.w(TAG, "Unable to parse device policy " + paramComponentName, localXmlPullParserException);
                    localDeviceAdminInfo = null;
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    Log.w(TAG, "Unable to parse device policy " + paramComponentName, localIOException);
                    DeviceAdminInfo localDeviceAdminInfo = null;
                }
            }
        }
    }

    public boolean getCameraDisabled(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.getCameraDisabled(paramComponentName);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public int getCurrentFailedPasswordAttempts()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getCurrentFailedPasswordAttempts();
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = -1;
        }
    }

    public ComponentName getGlobalProxyAdmin()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                ComponentName localComponentName2 = this.mService.getGlobalProxyAdmin();
                localComponentName1 = localComponentName2;
                return localComponentName1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            ComponentName localComponentName1 = null;
        }
    }

    public int getMaximumFailedPasswordsForWipe(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getMaximumFailedPasswordsForWipe(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public long getMaximumTimeToLock(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                long l2 = this.mService.getMaximumTimeToLock(paramComponentName);
                l1 = l2;
                return l1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            long l1 = 0L;
        }
    }

    public long getPasswordExpiration(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                long l2 = this.mService.getPasswordExpiration(paramComponentName);
                l1 = l2;
                return l1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            long l1 = 0L;
        }
    }

    public long getPasswordExpirationTimeout(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                long l2 = this.mService.getPasswordExpirationTimeout(paramComponentName);
                l1 = l2;
                return l1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            long l1 = 0L;
        }
    }

    public int getPasswordHistoryLength(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordHistoryLength(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMaximumLength(int paramInt)
    {
        return 16;
    }

    public int getPasswordMinimumLength(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumLength(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMinimumLetters(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumLetters(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMinimumLowerCase(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumLowerCase(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMinimumNonLetter(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumNonLetter(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMinimumNumeric(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumNumeric(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMinimumSymbols(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumSymbols(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordMinimumUpperCase(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordMinimumUpperCase(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public int getPasswordQuality(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getPasswordQuality(paramComponentName);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public void getRemoveWarning(ComponentName paramComponentName, RemoteCallback paramRemoteCallback)
    {
        if (this.mService != null);
        try
        {
            this.mService.getRemoveWarning(paramComponentName, paramRemoteCallback);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public boolean getStorageEncryption(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.getStorageEncryption(paramComponentName);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public int getStorageEncryptionStatus()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getStorageEncryptionStatus();
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public boolean hasGrantedPolicy(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.hasGrantedPolicy(paramComponentName, paramInt);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public boolean isActivePasswordSufficient()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.isActivePasswordSufficient();
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public boolean isAdminActive(ComponentName paramComponentName)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.isAdminActive(paramComponentName);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public void lockNow()
    {
        if (this.mService != null);
        try
        {
            this.mService.lockNow();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public boolean packageHasActiveAdmins(String paramString)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.packageHasActiveAdmins(paramString);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public void removeActiveAdmin(ComponentName paramComponentName)
    {
        if (this.mService != null);
        try
        {
            this.mService.removeActiveAdmin(paramComponentName);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void reportFailedPasswordAttempt()
    {
        if (this.mService != null);
        try
        {
            this.mService.reportFailedPasswordAttempt();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void reportSuccessfulPasswordAttempt()
    {
        if (this.mService != null);
        try
        {
            this.mService.reportSuccessfulPasswordAttempt();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public boolean resetPassword(String paramString, int paramInt)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.resetPassword(paramString, paramInt);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public void setActiveAdmin(ComponentName paramComponentName, boolean paramBoolean)
    {
        if (this.mService != null);
        try
        {
            this.mService.setActiveAdmin(paramComponentName, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setActivePasswordState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        if (this.mService != null);
        try
        {
            this.mService.setActivePasswordState(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setCameraDisabled(ComponentName paramComponentName, boolean paramBoolean)
    {
        if (this.mService != null);
        try
        {
            this.mService.setCameraDisabled(paramComponentName, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public ComponentName setGlobalProxy(ComponentName paramComponentName, java.net.Proxy paramProxy, List<String> paramList)
    {
        if (paramProxy == null)
            throw new NullPointerException();
        if (this.mService != null);
        Object localObject;
        ComponentName localComponentName;
        while (true)
        {
            try
            {
                if (paramProxy.equals(java.net.Proxy.NO_PROXY))
                {
                    str2 = null;
                    localObject = null;
                    localComponentName = this.mService.setGlobalProxy(paramComponentName, str2, (String)localObject);
                    break label258;
                }
                if (paramProxy.type().equals(Proxy.Type.HTTP))
                    break label94;
                throw new IllegalArgumentException();
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            localComponentName = null;
            break label258;
            label94: InetSocketAddress localInetSocketAddress = (InetSocketAddress)paramProxy.address();
            String str1 = localInetSocketAddress.getHostName();
            int i = localInetSocketAddress.getPort();
            String str2 = str1 + ":" + Integer.toString(i);
            if (paramList != null)
                break;
            localObject = "";
            android.net.Proxy.validate(str1, Integer.toString(i), (String)localObject);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        int j = 1;
        Iterator localIterator = paramList.iterator();
        label192: String str4;
        if (localIterator.hasNext())
        {
            str4 = (String)localIterator.next();
            if (j != 0)
                break label261;
            localStringBuilder = localStringBuilder.append(",");
        }
        while (true)
        {
            localStringBuilder = localStringBuilder.append(str4.trim());
            break label192;
            String str3 = localStringBuilder.toString();
            localObject = str3;
            break;
            label258: return localComponentName;
            label261: j = 0;
        }
    }

    public void setMaximumFailedPasswordsForWipe(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setMaximumFailedPasswordsForWipe(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setMaximumTimeToLock(ComponentName paramComponentName, long paramLong)
    {
        if (this.mService != null);
        try
        {
            this.mService.setMaximumTimeToLock(paramComponentName, paramLong);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordExpirationTimeout(ComponentName paramComponentName, long paramLong)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordExpirationTimeout(paramComponentName, paramLong);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordHistoryLength(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordHistoryLength(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumLength(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumLength(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumLetters(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumLetters(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumLowerCase(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumLowerCase(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumNonLetter(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumNonLetter(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumNumeric(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumNumeric(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumSymbols(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumSymbols(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordMinimumUpperCase(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordMinimumUpperCase(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public void setPasswordQuality(ComponentName paramComponentName, int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setPasswordQuality(paramComponentName, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }

    public int setStorageEncryption(ComponentName paramComponentName, boolean paramBoolean)
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.setStorageEncryption(paramComponentName, paramBoolean);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
            }
            int i = 0;
        }
    }

    public void wipeData(int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.wipeData(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w(TAG, "Failed talking with device policy service", localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.admin.DevicePolicyManager
 * JD-Core Version:        0.6.2
 */