package android.app.admin;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class DeviceAdminReceiver extends BroadcastReceiver
{
    public static final String ACTION_DEVICE_ADMIN_DISABLED = "android.app.action.DEVICE_ADMIN_DISABLED";
    public static final String ACTION_DEVICE_ADMIN_DISABLE_REQUESTED = "android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED";
    public static final String ACTION_DEVICE_ADMIN_ENABLED = "android.app.action.DEVICE_ADMIN_ENABLED";
    public static final String ACTION_PASSWORD_CHANGED = "android.app.action.ACTION_PASSWORD_CHANGED";
    public static final String ACTION_PASSWORD_EXPIRING = "android.app.action.ACTION_PASSWORD_EXPIRING";
    public static final String ACTION_PASSWORD_FAILED = "android.app.action.ACTION_PASSWORD_FAILED";
    public static final String ACTION_PASSWORD_SUCCEEDED = "android.app.action.ACTION_PASSWORD_SUCCEEDED";
    public static final String DEVICE_ADMIN_META_DATA = "android.app.device_admin";
    public static final String EXTRA_DISABLE_WARNING = "android.app.extra.DISABLE_WARNING";
    private static String TAG = "DevicePolicy";
    private static boolean localLOGV = false;
    private DevicePolicyManager mManager;
    private ComponentName mWho;

    public DevicePolicyManager getManager(Context paramContext)
    {
        if (this.mManager != null);
        for (DevicePolicyManager localDevicePolicyManager = this.mManager; ; localDevicePolicyManager = this.mManager)
        {
            return localDevicePolicyManager;
            this.mManager = ((DevicePolicyManager)paramContext.getSystemService("device_policy"));
        }
    }

    public ComponentName getWho(Context paramContext)
    {
        if (this.mWho != null);
        for (ComponentName localComponentName = this.mWho; ; localComponentName = this.mWho)
        {
            return localComponentName;
            this.mWho = new ComponentName(paramContext, getClass());
        }
    }

    public CharSequence onDisableRequested(Context paramContext, Intent paramIntent)
    {
        return null;
    }

    public void onDisabled(Context paramContext, Intent paramIntent)
    {
    }

    public void onEnabled(Context paramContext, Intent paramIntent)
    {
    }

    public void onPasswordChanged(Context paramContext, Intent paramIntent)
    {
    }

    public void onPasswordExpiring(Context paramContext, Intent paramIntent)
    {
    }

    public void onPasswordFailed(Context paramContext, Intent paramIntent)
    {
    }

    public void onPasswordSucceeded(Context paramContext, Intent paramIntent)
    {
    }

    public void onReceive(Context paramContext, Intent paramIntent)
    {
        String str = paramIntent.getAction();
        if ("android.app.action.ACTION_PASSWORD_CHANGED".equals(str))
            onPasswordChanged(paramContext, paramIntent);
        while (true)
        {
            return;
            if ("android.app.action.ACTION_PASSWORD_FAILED".equals(str))
            {
                onPasswordFailed(paramContext, paramIntent);
            }
            else if ("android.app.action.ACTION_PASSWORD_SUCCEEDED".equals(str))
            {
                onPasswordSucceeded(paramContext, paramIntent);
            }
            else if ("android.app.action.DEVICE_ADMIN_ENABLED".equals(str))
            {
                onEnabled(paramContext, paramIntent);
            }
            else if ("android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED".equals(str))
            {
                CharSequence localCharSequence = onDisableRequested(paramContext, paramIntent);
                if (localCharSequence != null)
                    getResultExtras(true).putCharSequence("android.app.extra.DISABLE_WARNING", localCharSequence);
            }
            else if ("android.app.action.DEVICE_ADMIN_DISABLED".equals(str))
            {
                onDisabled(paramContext, paramIntent);
            }
            else if ("android.app.action.ACTION_PASSWORD_EXPIRING".equals(str))
            {
                onPasswordExpiring(paramContext, paramIntent);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.admin.DeviceAdminReceiver
 * JD-Core Version:        0.6.2
 */