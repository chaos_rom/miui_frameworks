package android.app.admin;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Printer;
import android.util.SparseArray;
import android.util.Xml;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public final class DeviceAdminInfo
    implements Parcelable
{
    public static final Parcelable.Creator<DeviceAdminInfo> CREATOR = new Parcelable.Creator()
    {
        public DeviceAdminInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new DeviceAdminInfo(paramAnonymousParcel);
        }

        public DeviceAdminInfo[] newArray(int paramAnonymousInt)
        {
            return new DeviceAdminInfo[paramAnonymousInt];
        }
    };
    static final String TAG = "DeviceAdminInfo";
    public static final int USES_ENCRYPTED_STORAGE = 7;
    public static final int USES_POLICY_DISABLE_CAMERA = 8;
    public static final int USES_POLICY_EXPIRE_PASSWORD = 6;
    public static final int USES_POLICY_FORCE_LOCK = 3;
    public static final int USES_POLICY_LIMIT_PASSWORD = 0;
    public static final int USES_POLICY_RESET_PASSWORD = 2;
    public static final int USES_POLICY_SETS_GLOBAL_PROXY = 5;
    public static final int USES_POLICY_WATCH_LOGIN = 1;
    public static final int USES_POLICY_WIPE_DATA = 4;
    static HashMap<String, Integer> sKnownPolicies;
    static ArrayList<PolicyInfo> sPoliciesDisplayOrder = new ArrayList();
    static SparseArray<PolicyInfo> sRevKnownPolicies;
    final ResolveInfo mReceiver;
    int mUsesPolicies;
    boolean mVisible;

    static
    {
        sKnownPolicies = new HashMap();
        sRevKnownPolicies = new SparseArray();
        sPoliciesDisplayOrder.add(new PolicyInfo(4, "wipe-data", 17040028, 17040029));
        sPoliciesDisplayOrder.add(new PolicyInfo(2, "reset-password", 17040024, 17040025));
        sPoliciesDisplayOrder.add(new PolicyInfo(0, "limit-password", 17040020, 17040021));
        sPoliciesDisplayOrder.add(new PolicyInfo(1, "watch-login", 17040022, 17040023));
        sPoliciesDisplayOrder.add(new PolicyInfo(3, "force-lock", 17040026, 17040027));
        sPoliciesDisplayOrder.add(new PolicyInfo(5, "set-global-proxy", 17040030, 17040031));
        sPoliciesDisplayOrder.add(new PolicyInfo(6, "expire-password", 17040032, 17040033));
        sPoliciesDisplayOrder.add(new PolicyInfo(7, "encrypted-storage", 17040034, 17040035));
        sPoliciesDisplayOrder.add(new PolicyInfo(8, "disable-camera", 17040036, 17040037));
        for (int i = 0; i < sPoliciesDisplayOrder.size(); i++)
        {
            PolicyInfo localPolicyInfo = (PolicyInfo)sPoliciesDisplayOrder.get(i);
            sRevKnownPolicies.put(localPolicyInfo.ident, localPolicyInfo);
            sKnownPolicies.put(localPolicyInfo.tag, Integer.valueOf(localPolicyInfo.ident));
        }
    }

    public DeviceAdminInfo(Context paramContext, ResolveInfo paramResolveInfo)
        throws XmlPullParserException, IOException
    {
        this.mReceiver = paramResolveInfo;
        ActivityInfo localActivityInfo = paramResolveInfo.activityInfo;
        PackageManager localPackageManager = paramContext.getPackageManager();
        XmlResourceParser localXmlResourceParser = null;
        try
        {
            localXmlResourceParser = localActivityInfo.loadXmlMetaData(localPackageManager, "android.app.device_admin");
            if (localXmlResourceParser == null)
                throw new XmlPullParserException("No android.app.device_admin meta-data");
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new XmlPullParserException("Unable to create context for: " + localActivityInfo.packageName);
        }
        finally
        {
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
        Resources localResources = localPackageManager.getResourcesForApplication(localActivityInfo.applicationInfo);
        AttributeSet localAttributeSet = Xml.asAttributeSet(localXmlResourceParser);
        int i;
        do
            i = localXmlResourceParser.next();
        while ((i != 1) && (i != 2));
        if (!"device-admin".equals(localXmlResourceParser.getName()))
            throw new XmlPullParserException("Meta-data does not start with device-admin tag");
        TypedArray localTypedArray = localResources.obtainAttributes(localAttributeSet, R.styleable.DeviceAdmin);
        this.mVisible = localTypedArray.getBoolean(0, true);
        localTypedArray.recycle();
        int j = localXmlResourceParser.getDepth();
        int k;
        do
        {
            k = localXmlResourceParser.next();
            if ((k == 1) || ((k == 3) && (localXmlResourceParser.getDepth() <= j)))
                break;
        }
        while ((k == 3) || (k == 4) || (!localXmlResourceParser.getName().equals("uses-policies")));
        int m = localXmlResourceParser.getDepth();
        while (true)
        {
            int n = localXmlResourceParser.next();
            if ((n == 1) || ((n == 3) && (localXmlResourceParser.getDepth() <= m)))
                break;
            if ((n != 3) && (n != 4))
            {
                String str = localXmlResourceParser.getName();
                Integer localInteger = (Integer)sKnownPolicies.get(str);
                if (localInteger != null)
                    this.mUsesPolicies |= 1 << localInteger.intValue();
                else
                    Log.w("DeviceAdminInfo", "Unknown tag under uses-policies of " + getComponent() + ": " + str);
            }
        }
        if (localXmlResourceParser != null)
            localXmlResourceParser.close();
    }

    DeviceAdminInfo(Parcel paramParcel)
    {
        this.mReceiver = ((ResolveInfo)ResolveInfo.CREATOR.createFromParcel(paramParcel));
        this.mUsesPolicies = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + "Receiver:");
        this.mReceiver.dump(paramPrinter, paramString + "    ");
    }

    public ActivityInfo getActivityInfo()
    {
        return this.mReceiver.activityInfo;
    }

    public ComponentName getComponent()
    {
        return new ComponentName(this.mReceiver.activityInfo.packageName, this.mReceiver.activityInfo.name);
    }

    public String getPackageName()
    {
        return this.mReceiver.activityInfo.packageName;
    }

    public String getReceiverName()
    {
        return this.mReceiver.activityInfo.name;
    }

    public String getTagForPolicy(int paramInt)
    {
        return ((PolicyInfo)sRevKnownPolicies.get(paramInt)).tag;
    }

    public ArrayList<PolicyInfo> getUsedPolicies()
    {
        ArrayList localArrayList = new ArrayList();
        for (int i = 0; i < sPoliciesDisplayOrder.size(); i++)
        {
            PolicyInfo localPolicyInfo = (PolicyInfo)sPoliciesDisplayOrder.get(i);
            if (usesPolicy(localPolicyInfo.ident))
                localArrayList.add(localPolicyInfo);
        }
        return localArrayList;
    }

    public boolean isVisible()
    {
        return this.mVisible;
    }

    public CharSequence loadDescription(PackageManager paramPackageManager)
        throws Resources.NotFoundException
    {
        if (this.mReceiver.activityInfo.descriptionRes != 0)
        {
            String str = this.mReceiver.resolvePackageName;
            ApplicationInfo localApplicationInfo = null;
            if (str == null)
            {
                str = this.mReceiver.activityInfo.packageName;
                localApplicationInfo = this.mReceiver.activityInfo.applicationInfo;
            }
            return paramPackageManager.getText(str, this.mReceiver.activityInfo.descriptionRes, localApplicationInfo);
        }
        throw new Resources.NotFoundException();
    }

    public Drawable loadIcon(PackageManager paramPackageManager)
    {
        return this.mReceiver.loadIcon(paramPackageManager);
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        return this.mReceiver.loadLabel(paramPackageManager);
    }

    public void readPoliciesFromXml(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        this.mUsesPolicies = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "flags"));
    }

    public String toString()
    {
        return "DeviceAdminInfo{" + this.mReceiver.activityInfo.name + "}";
    }

    public boolean usesPolicy(int paramInt)
    {
        int i = 1;
        if ((this.mUsesPolicies & i << paramInt) != 0);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public void writePoliciesToXml(XmlSerializer paramXmlSerializer)
        throws IllegalArgumentException, IllegalStateException, IOException
    {
        paramXmlSerializer.attribute(null, "flags", Integer.toString(this.mUsesPolicies));
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        this.mReceiver.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.mUsesPolicies);
    }

    public static class PolicyInfo
    {
        public final int description;
        public final int ident;
        public final int label;
        public final String tag;

        public PolicyInfo(int paramInt1, String paramString, int paramInt2, int paramInt3)
        {
            this.ident = paramInt1;
            this.tag = paramString;
            this.label = paramInt2;
            this.description = paramInt3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.admin.DeviceAdminInfo
 * JD-Core Version:        0.6.2
 */