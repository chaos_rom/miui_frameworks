package android.app;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IActivityController extends IInterface
{
    public abstract boolean activityResuming(String paramString)
        throws RemoteException;

    public abstract boolean activityStarting(Intent paramIntent, String paramString)
        throws RemoteException;

    public abstract boolean appCrashed(String paramString1, int paramInt, String paramString2, String paramString3, long paramLong, String paramString4)
        throws RemoteException;

    public abstract int appEarlyNotResponding(String paramString1, int paramInt, String paramString2)
        throws RemoteException;

    public abstract int appNotResponding(String paramString1, int paramInt, String paramString2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IActivityController
    {
        private static final String DESCRIPTOR = "android.app.IActivityController";
        static final int TRANSACTION_activityResuming = 2;
        static final int TRANSACTION_activityStarting = 1;
        static final int TRANSACTION_appCrashed = 3;
        static final int TRANSACTION_appEarlyNotResponding = 4;
        static final int TRANSACTION_appNotResponding = 5;

        public Stub()
        {
            attachInterface(this, "android.app.IActivityController");
        }

        public static IActivityController asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IActivityController");
                if ((localIInterface != null) && ((localIInterface instanceof IActivityController)))
                    localObject = (IActivityController)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.app.IActivityController");
                continue;
                paramParcel1.enforceInterface("android.app.IActivityController");
                Intent localIntent;
                if (paramParcel1.readInt() != 0)
                {
                    localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel1);
                    label114: boolean bool3 = activityStarting(localIntent, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (!bool3)
                        break label154;
                }
                label154: int i1;
                for (int n = j; ; i1 = 0)
                {
                    paramParcel2.writeInt(n);
                    break;
                    localIntent = null;
                    break label114;
                }
                paramParcel1.enforceInterface("android.app.IActivityController");
                boolean bool2 = activityResuming(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.app.IActivityController");
                boolean bool1 = appCrashed(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readLong(), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.app.IActivityController");
                int m = appEarlyNotResponding(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                continue;
                paramParcel1.enforceInterface("android.app.IActivityController");
                int k = appNotResponding(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
            }
        }

        private static class Proxy
            implements IActivityController
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean activityResuming(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IActivityController");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean activityStarting(Intent paramIntent, String paramString)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.IActivityController");
                        if (paramIntent != null)
                        {
                            localParcel1.writeInt(1);
                            paramIntent.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            this.mRemote.transact(1, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean appCrashed(String paramString1, int paramInt, String paramString2, String paramString3, long paramLong, String paramString4)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IActivityController");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeString(paramString4);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int appEarlyNotResponding(String paramString1, int paramInt, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IActivityController");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int appNotResponding(String paramString1, int paramInt, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IActivityController");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IActivityController";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IActivityController
 * JD-Core Version:        0.6.2
 */