package android.app;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.res.Configuration;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.MessageQueue;
import android.os.MessageQueue.IdleHandler;
import android.os.PerformanceCollector;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Instrumentation
{
    public static final String REPORT_KEY_IDENTIFIER = "id";
    public static final String REPORT_KEY_STREAMRESULT = "stream";
    private static final String TAG = "Instrumentation";
    private List<ActivityMonitor> mActivityMonitors;
    private Context mAppContext;
    private boolean mAutomaticPerformanceSnapshots = false;
    private ComponentName mComponent;
    private Context mInstrContext;
    private MessageQueue mMessageQueue = null;
    private Bundle mPerfMetrics = new Bundle();
    private PerformanceCollector mPerformanceCollector;
    private Thread mRunner;
    private final Object mSync = new Object();
    private ActivityThread mThread = null;
    private List<ActivityWaiter> mWaitingActivities;
    private IInstrumentationWatcher mWatcher;

    private void addValue(String paramString, int paramInt, Bundle paramBundle)
    {
        if (paramBundle.containsKey(paramString))
        {
            ArrayList localArrayList2 = paramBundle.getIntegerArrayList(paramString);
            if (localArrayList2 != null)
                localArrayList2.add(Integer.valueOf(paramInt));
        }
        while (true)
        {
            return;
            ArrayList localArrayList1 = new ArrayList();
            localArrayList1.add(Integer.valueOf(paramInt));
            paramBundle.putIntegerArrayList(paramString, localArrayList1);
        }
    }

    static void checkStartActivityResult(int paramInt, Object paramObject)
    {
        if (paramInt >= 0)
            return;
        switch (paramInt)
        {
        default:
            throw new AndroidRuntimeException("Unknown error code " + paramInt + " when starting " + paramObject);
        case -2:
        case -1:
            if (((paramObject instanceof Intent)) && (((Intent)paramObject).getComponent() != null))
                throw new ActivityNotFoundException("Unable to find explicit activity class " + ((Intent)paramObject).getComponent().toShortString() + "; have you declared this activity in your AndroidManifest.xml?");
            throw new ActivityNotFoundException("No Activity found to handle " + paramObject);
        case -4:
            throw new SecurityException("Not allowed to start activity " + paramObject);
        case -3:
            throw new AndroidRuntimeException("FORWARD_RESULT_FLAG used while also requesting a result");
        case -5:
        }
        throw new IllegalArgumentException("PendingIntent is not an activity");
    }

    public static Application newApplication(Class<?> paramClass, Context paramContext)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        Application localApplication = (Application)paramClass.newInstance();
        localApplication.attach(paramContext);
        return localApplication;
    }

    private final void validateNotAppThread()
    {
        if (ActivityThread.currentActivityThread() != null)
            throw new RuntimeException("This method can not be called from the main application thread");
    }

    public ActivityMonitor addMonitor(IntentFilter paramIntentFilter, ActivityResult paramActivityResult, boolean paramBoolean)
    {
        ActivityMonitor localActivityMonitor = new ActivityMonitor(paramIntentFilter, paramActivityResult, paramBoolean);
        addMonitor(localActivityMonitor);
        return localActivityMonitor;
    }

    public ActivityMonitor addMonitor(String paramString, ActivityResult paramActivityResult, boolean paramBoolean)
    {
        ActivityMonitor localActivityMonitor = new ActivityMonitor(paramString, paramActivityResult, paramBoolean);
        addMonitor(localActivityMonitor);
        return localActivityMonitor;
    }

    public void addMonitor(ActivityMonitor paramActivityMonitor)
    {
        synchronized (this.mSync)
        {
            if (this.mActivityMonitors == null)
                this.mActivityMonitors = new ArrayList();
            this.mActivityMonitors.add(paramActivityMonitor);
            return;
        }
    }

    public void callActivityOnCreate(Activity paramActivity, Bundle paramBundle)
    {
        if (this.mWaitingActivities != null);
        while (true)
        {
            int m;
            synchronized (this.mSync)
            {
                int k = this.mWaitingActivities.size();
                m = 0;
                if (m < k)
                {
                    ActivityWaiter localActivityWaiter = (ActivityWaiter)this.mWaitingActivities.get(m);
                    if (!localActivityWaiter.intent.filterEquals(paramActivity.getIntent()))
                        break label187;
                    localActivityWaiter.activity = paramActivity;
                    this.mMessageQueue.addIdleHandler(new ActivityGoing(localActivityWaiter));
                    break label187;
                }
                paramActivity.performCreate(paramBundle);
                if (this.mActivityMonitors == null)
                    break;
            }
            synchronized (this.mSync)
            {
                int i = this.mActivityMonitors.size();
                int j = 0;
                while (j < i)
                {
                    ((ActivityMonitor)this.mActivityMonitors.get(j)).match(paramActivity, paramActivity, paramActivity.getIntent());
                    j++;
                    continue;
                    localObject4 = finally;
                    throw localObject4;
                }
            }
            label187: m++;
        }
    }

    public void callActivityOnDestroy(Activity paramActivity)
    {
        paramActivity.performDestroy();
        if (this.mActivityMonitors != null)
            synchronized (this.mSync)
            {
                int i = this.mActivityMonitors.size();
                for (int j = 0; j < i; j++)
                    ((ActivityMonitor)this.mActivityMonitors.get(j)).match(paramActivity, paramActivity, paramActivity.getIntent());
            }
    }

    public void callActivityOnNewIntent(Activity paramActivity, Intent paramIntent)
    {
        paramActivity.onNewIntent(paramIntent);
    }

    public void callActivityOnPause(Activity paramActivity)
    {
        paramActivity.performPause();
    }

    public void callActivityOnPostCreate(Activity paramActivity, Bundle paramBundle)
    {
        paramActivity.onPostCreate(paramBundle);
    }

    public void callActivityOnRestart(Activity paramActivity)
    {
        paramActivity.onRestart();
    }

    public void callActivityOnRestoreInstanceState(Activity paramActivity, Bundle paramBundle)
    {
        paramActivity.performRestoreInstanceState(paramBundle);
    }

    public void callActivityOnResume(Activity paramActivity)
    {
        paramActivity.mResumed = true;
        paramActivity.onResume();
        if (this.mActivityMonitors != null)
            synchronized (this.mSync)
            {
                int i = this.mActivityMonitors.size();
                for (int j = 0; j < i; j++)
                    ((ActivityMonitor)this.mActivityMonitors.get(j)).match(paramActivity, paramActivity, paramActivity.getIntent());
            }
    }

    public void callActivityOnSaveInstanceState(Activity paramActivity, Bundle paramBundle)
    {
        paramActivity.performSaveInstanceState(paramBundle);
    }

    public void callActivityOnStart(Activity paramActivity)
    {
        paramActivity.onStart();
    }

    public void callActivityOnStop(Activity paramActivity)
    {
        paramActivity.onStop();
    }

    public void callActivityOnUserLeaving(Activity paramActivity)
    {
        paramActivity.performUserLeaving();
    }

    public void callApplicationOnCreate(Application paramApplication)
    {
        paramApplication.onCreate();
    }

    public boolean checkMonitorHit(ActivityMonitor paramActivityMonitor, int paramInt)
    {
        waitForIdleSync();
        boolean bool;
        synchronized (this.mSync)
        {
            if (paramActivityMonitor.getHits() < paramInt)
            {
                bool = false;
            }
            else
            {
                this.mActivityMonitors.remove(paramActivityMonitor);
                bool = true;
            }
        }
        return bool;
    }

    public void endPerformanceSnapshot()
    {
        if (!isProfiling())
            this.mPerfMetrics = this.mPerformanceCollector.endSnapshot();
    }

    public void execStartActivities(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Activity paramActivity, Intent[] paramArrayOfIntent, Bundle paramBundle)
    {
        IApplicationThread localIApplicationThread = (IApplicationThread)paramIBinder1;
        if (this.mActivityMonitors != null);
        while (true)
        {
            int k;
            synchronized (this.mSync)
            {
                int j = this.mActivityMonitors.size();
                k = 0;
                if (k < j)
                {
                    ActivityMonitor localActivityMonitor = (ActivityMonitor)this.mActivityMonitors.get(k);
                    if (!localActivityMonitor.match(paramContext, null, paramArrayOfIntent[0]))
                        break label191;
                    localActivityMonitor.mHits = (1 + localActivityMonitor.mHits);
                    if (localActivityMonitor.isBlocking())
                        break label190;
                }
            }
            try
            {
                String[] arrayOfString = new String[paramArrayOfIntent.length];
                int i = 0;
                while (i < paramArrayOfIntent.length)
                {
                    paramArrayOfIntent[i].setAllowFds(false);
                    arrayOfString[i] = paramArrayOfIntent[i].resolveTypeIfNeeded(paramContext.getContentResolver());
                    i++;
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                checkStartActivityResult(ActivityManagerNative.getDefault().startActivities(localIApplicationThread, paramArrayOfIntent, arrayOfString, paramIBinder2, paramBundle), paramArrayOfIntent[0]);
            }
            catch (RemoteException localRemoteException)
            {
            }
            label190: return;
            label191: k++;
        }
    }

    public ActivityResult execStartActivity(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Activity paramActivity, Intent paramIntent, int paramInt, Bundle paramBundle)
    {
        IApplicationThread localIApplicationThread = (IApplicationThread)paramIBinder1;
        if (this.mActivityMonitors != null);
        while (true)
        {
            int j;
            synchronized (this.mSync)
            {
                int i = this.mActivityMonitors.size();
                j = 0;
                if (j < i)
                {
                    ActivityMonitor localActivityMonitor = (ActivityMonitor)this.mActivityMonitors.get(j);
                    if (!localActivityMonitor.match(paramContext, null, paramIntent))
                        break label214;
                    localActivityMonitor.mHits = (1 + localActivityMonitor.mHits);
                    if (localActivityMonitor.isBlocking())
                    {
                        if (paramInt < 0)
                            break label208;
                        localActivityResult = localActivityMonitor.getResult();
                        break label205;
                    }
                }
            }
            try
            {
                paramIntent.setAllowFds(false);
                paramIntent.migrateExtraStreamToClipData();
                IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
                String str1 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
                if (paramActivity != null);
                for (String str2 = paramActivity.mEmbeddedID; ; str2 = null)
                {
                    checkStartActivityResult(localIActivityManager.startActivity(localIApplicationThread, paramIntent, str1, paramIBinder2, str2, paramInt, 0, null, null, paramBundle), paramIntent);
                    label180: localActivityResult = null;
                    break;
                    localObject2 = finally;
                    throw localObject2;
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label180;
            }
            label205: return localActivityResult;
            label208: ActivityResult localActivityResult = null;
            continue;
            label214: j++;
        }
    }

    public ActivityResult execStartActivity(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Fragment paramFragment, Intent paramIntent, int paramInt, Bundle paramBundle)
    {
        IApplicationThread localIApplicationThread = (IApplicationThread)paramIBinder1;
        if (this.mActivityMonitors != null);
        while (true)
        {
            int j;
            synchronized (this.mSync)
            {
                int i = this.mActivityMonitors.size();
                j = 0;
                if (j < i)
                {
                    ActivityMonitor localActivityMonitor = (ActivityMonitor)this.mActivityMonitors.get(j);
                    if (!localActivityMonitor.match(paramContext, null, paramIntent))
                        break label214;
                    localActivityMonitor.mHits = (1 + localActivityMonitor.mHits);
                    if (localActivityMonitor.isBlocking())
                    {
                        if (paramInt < 0)
                            break label208;
                        localActivityResult = localActivityMonitor.getResult();
                        break label205;
                    }
                }
            }
            try
            {
                paramIntent.setAllowFds(false);
                paramIntent.migrateExtraStreamToClipData();
                IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
                String str1 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
                if (paramFragment != null);
                for (String str2 = paramFragment.mWho; ; str2 = null)
                {
                    checkStartActivityResult(localIActivityManager.startActivity(localIApplicationThread, paramIntent, str1, paramIBinder2, str2, paramInt, 0, null, null, paramBundle), paramIntent);
                    label180: localActivityResult = null;
                    break;
                    localObject2 = finally;
                    throw localObject2;
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label180;
            }
            label205: return localActivityResult;
            label208: ActivityResult localActivityResult = null;
            continue;
            label214: j++;
        }
    }

    public void finish(int paramInt, Bundle paramBundle)
    {
        if (this.mAutomaticPerformanceSnapshots)
            endPerformanceSnapshot();
        if (this.mPerfMetrics != null)
            paramBundle.putAll(this.mPerfMetrics);
        this.mThread.finishInstrumentation(paramInt, paramBundle);
    }

    public Bundle getAllocCounts()
    {
        Bundle localBundle = new Bundle();
        localBundle.putLong("global_alloc_count", Debug.getGlobalAllocCount());
        localBundle.putLong("global_alloc_size", Debug.getGlobalAllocSize());
        localBundle.putLong("global_freed_count", Debug.getGlobalFreedCount());
        localBundle.putLong("global_freed_size", Debug.getGlobalFreedSize());
        localBundle.putLong("gc_invocation_count", Debug.getGlobalGcInvocationCount());
        return localBundle;
    }

    public Bundle getBinderCounts()
    {
        Bundle localBundle = new Bundle();
        localBundle.putLong("sent_transactions", Debug.getBinderSentTransactions());
        localBundle.putLong("received_transactions", Debug.getBinderReceivedTransactions());
        return localBundle;
    }

    public ComponentName getComponentName()
    {
        return this.mComponent;
    }

    public Context getContext()
    {
        return this.mInstrContext;
    }

    public Context getTargetContext()
    {
        return this.mAppContext;
    }

    final void init(ActivityThread paramActivityThread, Context paramContext1, Context paramContext2, ComponentName paramComponentName, IInstrumentationWatcher paramIInstrumentationWatcher)
    {
        this.mThread = paramActivityThread;
        this.mThread.getLooper();
        this.mMessageQueue = Looper.myQueue();
        this.mInstrContext = paramContext1;
        this.mAppContext = paramContext2;
        this.mComponent = paramComponentName;
        this.mWatcher = paramIInstrumentationWatcher;
    }

    public boolean invokeContextMenuAction(Activity paramActivity, int paramInt1, int paramInt2)
    {
        boolean bool = false;
        validateNotAppThread();
        sendKeySync(new KeyEvent(0, 23));
        waitForIdleSync();
        try
        {
            Thread.sleep(ViewConfiguration.getLongPressTimeout());
            sendKeySync(new KeyEvent(1, 23));
            waitForIdleSync();
            Instrumentation.1ContextMenuRunnable local1ContextMenuRunnable = new Instrumentation.1ContextMenuRunnable(this, paramActivity, paramInt1, paramInt2);
            runOnMainSync(local1ContextMenuRunnable);
            bool = local1ContextMenuRunnable.returnValue;
            return bool;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                Log.e("Instrumentation", "Could not sleep for long press timeout", localInterruptedException);
        }
    }

    public boolean invokeMenuActionSync(Activity paramActivity, int paramInt1, int paramInt2)
    {
        Instrumentation.1MenuRunnable local1MenuRunnable = new Instrumentation.1MenuRunnable(this, paramActivity, paramInt1, paramInt2);
        runOnMainSync(local1MenuRunnable);
        return local1MenuRunnable.returnValue;
    }

    public boolean isProfiling()
    {
        return this.mThread.isProfiling();
    }

    public Activity newActivity(Class<?> paramClass, Context paramContext, IBinder paramIBinder, Application paramApplication, Intent paramIntent, ActivityInfo paramActivityInfo, CharSequence paramCharSequence, Activity paramActivity, String paramString, Object paramObject)
        throws InstantiationException, IllegalAccessException
    {
        Activity localActivity = (Activity)paramClass.newInstance();
        localActivity.attach(paramContext, null, this, paramIBinder, paramApplication, paramIntent, paramActivityInfo, paramCharSequence, paramActivity, paramString, (Activity.NonConfigurationInstances)paramObject, new Configuration());
        return localActivity;
    }

    public Activity newActivity(ClassLoader paramClassLoader, String paramString, Intent paramIntent)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        return (Activity)paramClassLoader.loadClass(paramString).newInstance();
    }

    public Application newApplication(ClassLoader paramClassLoader, String paramString, Context paramContext)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        return newApplication(paramClassLoader.loadClass(paramString), paramContext);
    }

    public void onCreate(Bundle paramBundle)
    {
    }

    public void onDestroy()
    {
    }

    public boolean onException(Object paramObject, Throwable paramThrowable)
    {
        return false;
    }

    public void onStart()
    {
    }

    public void removeMonitor(ActivityMonitor paramActivityMonitor)
    {
        synchronized (this.mSync)
        {
            this.mActivityMonitors.remove(paramActivityMonitor);
            return;
        }
    }

    public void runOnMainSync(Runnable paramRunnable)
    {
        validateNotAppThread();
        SyncRunnable localSyncRunnable = new SyncRunnable(paramRunnable);
        this.mThread.getHandler().post(localSyncRunnable);
        localSyncRunnable.waitForComplete();
    }

    public void sendCharacterSync(int paramInt)
    {
        sendKeySync(new KeyEvent(0, paramInt));
        sendKeySync(new KeyEvent(1, paramInt));
    }

    public void sendKeyDownUpSync(int paramInt)
    {
        sendKeySync(new KeyEvent(0, paramInt));
        sendKeySync(new KeyEvent(1, paramInt));
    }

    public void sendKeySync(KeyEvent paramKeyEvent)
    {
        validateNotAppThread();
        long l1 = paramKeyEvent.getDownTime();
        long l2 = paramKeyEvent.getEventTime();
        int i = paramKeyEvent.getAction();
        int j = paramKeyEvent.getKeyCode();
        int k = paramKeyEvent.getRepeatCount();
        int m = paramKeyEvent.getMetaState();
        int n = paramKeyEvent.getDeviceId();
        int i1 = paramKeyEvent.getScanCode();
        int i2 = paramKeyEvent.getSource();
        int i3 = paramKeyEvent.getFlags();
        if (i2 == 0)
            i2 = 257;
        if (l2 == 0L)
            l2 = SystemClock.uptimeMillis();
        if (l1 == 0L)
            l1 = l2;
        KeyEvent localKeyEvent = new KeyEvent(l1, l2, i, j, k, m, n, i1, i3 | 0x8, i2);
        InputManager.getInstance().injectInputEvent(localKeyEvent, 2);
    }

    public void sendPointerSync(MotionEvent paramMotionEvent)
    {
        validateNotAppThread();
        if ((0x2 & paramMotionEvent.getSource()) == 0)
            paramMotionEvent.setSource(4098);
        InputManager.getInstance().injectInputEvent(paramMotionEvent, 2);
    }

    public void sendStatus(int paramInt, Bundle paramBundle)
    {
        if (this.mWatcher != null);
        try
        {
            this.mWatcher.instrumentationStatus(this.mComponent, paramInt, paramBundle);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                this.mWatcher = null;
        }
    }

    public void sendStringSync(String paramString)
    {
        if (paramString == null);
        while (true)
        {
            return;
            KeyEvent[] arrayOfKeyEvent = KeyCharacterMap.load(-1).getEvents(paramString.toCharArray());
            if (arrayOfKeyEvent != null)
                for (int i = 0; i < arrayOfKeyEvent.length; i++)
                    sendKeySync(KeyEvent.changeTimeRepeat(arrayOfKeyEvent[i], SystemClock.uptimeMillis(), 0));
        }
    }

    public void sendTrackballEventSync(MotionEvent paramMotionEvent)
    {
        validateNotAppThread();
        if ((0x4 & paramMotionEvent.getSource()) == 0)
            paramMotionEvent.setSource(65540);
        InputManager.getInstance().injectInputEvent(paramMotionEvent, 2);
    }

    public void setAutomaticPerformanceSnapshots()
    {
        this.mAutomaticPerformanceSnapshots = true;
        this.mPerformanceCollector = new PerformanceCollector();
    }

    public void setInTouchMode(boolean paramBoolean)
    {
        try
        {
            IWindowManager.Stub.asInterface(ServiceManager.getService("window")).setInTouchMode(paramBoolean);
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void start()
    {
        if (this.mRunner != null)
            throw new RuntimeException("Instrumentation already started");
        this.mRunner = new InstrumentationThread("Instr: " + getClass().getName());
        this.mRunner.start();
    }

    public Activity startActivitySync(Intent paramIntent)
    {
        validateNotAppThread();
        label74: ActivityWaiter localActivityWaiter;
        Activity localActivity;
        synchronized (this.mSync)
        {
            Intent localIntent = new Intent(paramIntent);
            ActivityInfo localActivityInfo;
            try
            {
                localActivityInfo = localIntent.resolveActivityInfo(getTargetContext().getPackageManager(), 0);
                if (localActivityInfo != null)
                    break label74;
                throw new RuntimeException("Unable to resolve activity for: " + localIntent);
            }
            finally
            {
            }
            throw localObject2;
            String str = this.mThread.getProcessName();
            if (!localActivityInfo.processName.equals(str))
                throw new RuntimeException("Intent in process " + str + " resolved to different process " + localActivityInfo.processName + ": " + localIntent);
            localIntent.setComponent(new ComponentName(localActivityInfo.applicationInfo.packageName, localActivityInfo.name));
            localActivityWaiter = new ActivityWaiter(localIntent);
            if (this.mWaitingActivities == null)
                this.mWaitingActivities = new ArrayList();
            this.mWaitingActivities.add(localActivityWaiter);
            getTargetContext().startActivity(localIntent);
        }
    }

    public void startAllocCounting()
    {
        Runtime.getRuntime().gc();
        Runtime.getRuntime().runFinalization();
        Runtime.getRuntime().gc();
        Debug.resetAllCounts();
        Debug.startAllocCounting();
    }

    public void startPerformanceSnapshot()
    {
        if (!isProfiling())
            this.mPerformanceCollector.beginSnapshot(null);
    }

    public void startProfiling()
    {
        if (this.mThread.isProfiling())
        {
            File localFile = new File(this.mThread.getProfileFilePath());
            localFile.getParentFile().mkdirs();
            Debug.startMethodTracing(localFile.toString(), 8388608);
        }
    }

    public void stopAllocCounting()
    {
        Runtime.getRuntime().gc();
        Runtime.getRuntime().runFinalization();
        Runtime.getRuntime().gc();
        Debug.stopAllocCounting();
    }

    public void stopProfiling()
    {
        if (this.mThread.isProfiling())
            Debug.stopMethodTracing();
    }

    public void waitForIdle(Runnable paramRunnable)
    {
        this.mMessageQueue.addIdleHandler(new Idler(paramRunnable));
        this.mThread.getHandler().post(new EmptyRunnable(null));
    }

    public void waitForIdleSync()
    {
        validateNotAppThread();
        Idler localIdler = new Idler(null);
        this.mMessageQueue.addIdleHandler(localIdler);
        this.mThread.getHandler().post(new EmptyRunnable(null));
        localIdler.waitForIdle();
    }

    public Activity waitForMonitor(ActivityMonitor paramActivityMonitor)
    {
        Activity localActivity = paramActivityMonitor.waitForActivity();
        synchronized (this.mSync)
        {
            this.mActivityMonitors.remove(paramActivityMonitor);
            return localActivity;
        }
    }

    public Activity waitForMonitorWithTimeout(ActivityMonitor paramActivityMonitor, long paramLong)
    {
        Activity localActivity = paramActivityMonitor.waitForActivityWithTimeout(paramLong);
        synchronized (this.mSync)
        {
            this.mActivityMonitors.remove(paramActivityMonitor);
            return localActivity;
        }
    }

    private static final class Idler
        implements MessageQueue.IdleHandler
    {
        private final Runnable mCallback;
        private boolean mIdle;

        public Idler(Runnable paramRunnable)
        {
            this.mCallback = paramRunnable;
            this.mIdle = false;
        }

        public final boolean queueIdle()
        {
            if (this.mCallback != null)
                this.mCallback.run();
            try
            {
                this.mIdle = true;
                notifyAll();
                return false;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void waitForIdle()
        {
            try
            {
                while (true)
                {
                    boolean bool = this.mIdle;
                    if (bool)
                        break;
                    try
                    {
                        wait();
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                    }
                }
                return;
            }
            finally
            {
            }
        }
    }

    private final class ActivityGoing
        implements MessageQueue.IdleHandler
    {
        private final Instrumentation.ActivityWaiter mWaiter;

        public ActivityGoing(Instrumentation.ActivityWaiter arg2)
        {
            Object localObject;
            this.mWaiter = localObject;
        }

        public final boolean queueIdle()
        {
            synchronized (Instrumentation.this.mSync)
            {
                Instrumentation.this.mWaitingActivities.remove(this.mWaiter);
                Instrumentation.this.mSync.notifyAll();
                return false;
            }
        }
    }

    private static final class ActivityWaiter
    {
        public Activity activity;
        public final Intent intent;

        public ActivityWaiter(Intent paramIntent)
        {
            this.intent = paramIntent;
        }
    }

    private static final class SyncRunnable
        implements Runnable
    {
        private boolean mComplete;
        private final Runnable mTarget;

        public SyncRunnable(Runnable paramRunnable)
        {
            this.mTarget = paramRunnable;
        }

        public void run()
        {
            this.mTarget.run();
            try
            {
                this.mComplete = true;
                notifyAll();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void waitForComplete()
        {
            try
            {
                while (true)
                {
                    boolean bool = this.mComplete;
                    if (bool)
                        break;
                    try
                    {
                        wait();
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                    }
                }
                return;
            }
            finally
            {
            }
        }
    }

    private static final class EmptyRunnable
        implements Runnable
    {
        public void run()
        {
        }
    }

    private final class InstrumentationThread extends Thread
    {
        public InstrumentationThread(String arg2)
        {
            super();
        }

        public void run()
        {
            ActivityManagerNative.getDefault();
            try
            {
                Process.setThreadPriority(-8);
                if (Instrumentation.this.mAutomaticPerformanceSnapshots)
                    Instrumentation.this.startPerformanceSnapshot();
                Instrumentation.this.onStart();
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                    Log.w("Instrumentation", "Exception setting priority of instrumentation thread " + Process.myTid(), localRuntimeException);
            }
        }
    }

    public static final class ActivityResult
    {
        private final int mResultCode;
        private final Intent mResultData;

        public ActivityResult(int paramInt, Intent paramIntent)
        {
            this.mResultCode = paramInt;
            this.mResultData = paramIntent;
        }

        public int getResultCode()
        {
            return this.mResultCode;
        }

        public Intent getResultData()
        {
            return this.mResultData;
        }
    }

    public static class ActivityMonitor
    {
        private final boolean mBlock;
        private final String mClass;
        int mHits = 0;
        Activity mLastActivity = null;
        private final Instrumentation.ActivityResult mResult;
        private final IntentFilter mWhich;

        public ActivityMonitor(IntentFilter paramIntentFilter, Instrumentation.ActivityResult paramActivityResult, boolean paramBoolean)
        {
            this.mWhich = paramIntentFilter;
            this.mClass = null;
            this.mResult = paramActivityResult;
            this.mBlock = paramBoolean;
        }

        public ActivityMonitor(String paramString, Instrumentation.ActivityResult paramActivityResult, boolean paramBoolean)
        {
            this.mWhich = null;
            this.mClass = paramString;
            this.mResult = paramActivityResult;
            this.mBlock = paramBoolean;
        }

        public final IntentFilter getFilter()
        {
            return this.mWhich;
        }

        public final int getHits()
        {
            return this.mHits;
        }

        public final Activity getLastActivity()
        {
            return this.mLastActivity;
        }

        public final Instrumentation.ActivityResult getResult()
        {
            return this.mResult;
        }

        public final boolean isBlocking()
        {
            return this.mBlock;
        }

        // ERROR //
        final boolean match(Context paramContext, Activity paramActivity, Intent paramIntent)
        {
            // Byte code:
            //     0: iconst_0
            //     1: istore 4
            //     3: aload_0
            //     4: monitorenter
            //     5: aload_0
            //     6: getfield 30	android/app/Instrumentation$ActivityMonitor:mWhich	Landroid/content/IntentFilter;
            //     9: ifnull +26 -> 35
            //     12: aload_0
            //     13: getfield 30	android/app/Instrumentation$ActivityMonitor:mWhich	Landroid/content/IntentFilter;
            //     16: aload_1
            //     17: invokevirtual 55	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     20: aload_3
            //     21: iconst_1
            //     22: ldc 57
            //     24: invokevirtual 62	android/content/IntentFilter:match	(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I
            //     27: ifge +8 -> 35
            //     30: aload_0
            //     31: monitorexit
            //     32: goto +92 -> 124
            //     35: aload_0
            //     36: getfield 32	android/app/Instrumentation$ActivityMonitor:mClass	Ljava/lang/String;
            //     39: ifnull +67 -> 106
            //     42: aconst_null
            //     43: astore 6
            //     45: aload_2
            //     46: ifnull +41 -> 87
            //     49: aload_2
            //     50: invokevirtual 66	java/lang/Object:getClass	()Ljava/lang/Class;
            //     53: invokevirtual 72	java/lang/Class:getName	()Ljava/lang/String;
            //     56: astore 6
            //     58: aload 6
            //     60: ifnull +15 -> 75
            //     63: aload_0
            //     64: getfield 32	android/app/Instrumentation$ActivityMonitor:mClass	Ljava/lang/String;
            //     67: aload 6
            //     69: invokevirtual 78	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     72: ifne +34 -> 106
            //     75: aload_0
            //     76: monitorexit
            //     77: goto +47 -> 124
            //     80: astore 5
            //     82: aload_0
            //     83: monitorexit
            //     84: aload 5
            //     86: athrow
            //     87: aload_3
            //     88: invokevirtual 84	android/content/Intent:getComponent	()Landroid/content/ComponentName;
            //     91: ifnull -33 -> 58
            //     94: aload_3
            //     95: invokevirtual 84	android/content/Intent:getComponent	()Landroid/content/ComponentName;
            //     98: invokevirtual 89	android/content/ComponentName:getClassName	()Ljava/lang/String;
            //     101: astore 6
            //     103: goto -45 -> 58
            //     106: aload_2
            //     107: ifnull +12 -> 119
            //     110: aload_0
            //     111: aload_2
            //     112: putfield 28	android/app/Instrumentation$ActivityMonitor:mLastActivity	Landroid/app/Activity;
            //     115: aload_0
            //     116: invokevirtual 92	java/lang/Object:notifyAll	()V
            //     119: aload_0
            //     120: monitorexit
            //     121: iconst_1
            //     122: istore 4
            //     124: iload 4
            //     126: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     5	84	80	finally
            //     87	121	80	finally
        }

        public final Activity waitForActivity()
        {
            try
            {
                while (true)
                {
                    Activity localActivity1 = this.mLastActivity;
                    if (localActivity1 != null)
                        break;
                    try
                    {
                        wait();
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                    }
                }
                Activity localActivity2 = this.mLastActivity;
                this.mLastActivity = null;
                return localActivity2;
            }
            finally
            {
            }
        }

        // ERROR //
        public final Activity waitForActivityWithTimeout(long paramLong)
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_3
            //     2: aload_0
            //     3: monitorenter
            //     4: aload_0
            //     5: getfield 28	android/app/Instrumentation$ActivityMonitor:mLastActivity	Landroid/app/Activity;
            //     8: astore 5
            //     10: aload 5
            //     12: ifnonnull +8 -> 20
            //     15: aload_0
            //     16: lload_1
            //     17: invokevirtual 103	java/lang/Object:wait	(J)V
            //     20: aload_0
            //     21: getfield 28	android/app/Instrumentation$ActivityMonitor:mLastActivity	Landroid/app/Activity;
            //     24: ifnonnull +8 -> 32
            //     27: aload_0
            //     28: monitorexit
            //     29: goto +30 -> 59
            //     32: aload_0
            //     33: getfield 28	android/app/Instrumentation$ActivityMonitor:mLastActivity	Landroid/app/Activity;
            //     36: astore_3
            //     37: aload_0
            //     38: aconst_null
            //     39: putfield 28	android/app/Instrumentation$ActivityMonitor:mLastActivity	Landroid/app/Activity;
            //     42: aload_0
            //     43: monitorexit
            //     44: goto +15 -> 59
            //     47: astore 4
            //     49: aload_0
            //     50: monitorexit
            //     51: aload 4
            //     53: athrow
            //     54: astore 6
            //     56: goto -36 -> 20
            //     59: aload_3
            //     60: areturn
            //
            // Exception table:
            //     from	to	target	type
            //     4	10	47	finally
            //     15	20	47	finally
            //     20	51	47	finally
            //     15	20	54	java/lang/InterruptedException
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Instrumentation
 * JD-Core Version:        0.6.2
 */