package android.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.media.MediaRouter;
import android.media.MediaRouter.RouteInfo;
import android.media.MediaRouter.SimpleCallback;
import android.util.Log;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import com.android.internal.app.MediaRouteChooserDialogFragment;
import java.lang.ref.WeakReference;

public class MediaRouteActionProvider extends ActionProvider
{
    private static final String TAG = "MediaRouteActionProvider";
    private RouterCallback mCallback;
    private Context mContext;
    private View.OnClickListener mExtendedSettingsListener;
    private MenuItem mMenuItem;
    private int mRouteTypes;
    private MediaRouter mRouter;
    private MediaRouteButton mView;

    public MediaRouteActionProvider(Context paramContext)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mRouter = ((MediaRouter)paramContext.getSystemService("media_router"));
        this.mCallback = new RouterCallback(this);
        setRouteTypes(1);
    }

    private Activity getActivity()
    {
        for (Context localContext = this.mContext; ((localContext instanceof ContextWrapper)) && (!(localContext instanceof Activity)); localContext = ((ContextWrapper)localContext).getBaseContext());
        if (!(localContext instanceof Activity))
            throw new IllegalStateException("The MediaRouteActionProvider's Context is not an Activity.");
        return (Activity)localContext;
    }

    public boolean isVisible()
    {
        int i = 1;
        if (this.mRouter.getRouteCount() > i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public View onCreateActionView()
    {
        throw new UnsupportedOperationException("Use onCreateActionView(MenuItem) instead.");
    }

    public View onCreateActionView(MenuItem paramMenuItem)
    {
        if ((this.mMenuItem != null) || (this.mView != null))
            Log.e("MediaRouteActionProvider", "onCreateActionView: this ActionProvider is already associated with a menu item. Don't reuse MediaRouteActionProvider instances! Abandoning the old one...");
        this.mMenuItem = paramMenuItem;
        this.mView = new MediaRouteButton(this.mContext);
        this.mView.setRouteTypes(this.mRouteTypes);
        this.mView.setExtendedSettingsClickListener(this.mExtendedSettingsListener);
        return this.mView;
    }

    public boolean onPerformDefaultAction()
    {
        FragmentManager localFragmentManager = getActivity().getFragmentManager();
        if ((MediaRouteChooserDialogFragment)localFragmentManager.findFragmentByTag("android:MediaRouteChooserDialogFragment") != null)
            Log.w("MediaRouteActionProvider", "onPerformDefaultAction(): Chooser dialog already showing!");
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            MediaRouteChooserDialogFragment localMediaRouteChooserDialogFragment = new MediaRouteChooserDialogFragment();
            localMediaRouteChooserDialogFragment.setExtendedSettingsClickListener(this.mExtendedSettingsListener);
            localMediaRouteChooserDialogFragment.setRouteTypes(this.mRouteTypes);
            localMediaRouteChooserDialogFragment.show(localFragmentManager, "android:MediaRouteChooserDialogFragment");
        }
    }

    public boolean overridesItemVisibility()
    {
        return true;
    }

    public void setExtendedSettingsClickListener(View.OnClickListener paramOnClickListener)
    {
        this.mExtendedSettingsListener = paramOnClickListener;
        if (this.mView != null)
            this.mView.setExtendedSettingsClickListener(paramOnClickListener);
    }

    public void setRouteTypes(int paramInt)
    {
        if (this.mRouteTypes == paramInt);
        while (true)
        {
            return;
            if (this.mRouteTypes != 0)
                this.mRouter.removeCallback(this.mCallback);
            this.mRouteTypes = paramInt;
            if (paramInt != 0)
                this.mRouter.addCallback(paramInt, this.mCallback);
            if (this.mView != null)
                this.mView.setRouteTypes(this.mRouteTypes);
        }
    }

    private static class RouterCallback extends MediaRouter.SimpleCallback
    {
        private WeakReference<MediaRouteActionProvider> mAp;

        RouterCallback(MediaRouteActionProvider paramMediaRouteActionProvider)
        {
            this.mAp = new WeakReference(paramMediaRouteActionProvider);
        }

        public void onRouteAdded(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteActionProvider localMediaRouteActionProvider = (MediaRouteActionProvider)this.mAp.get();
            if (localMediaRouteActionProvider == null)
                paramMediaRouter.removeCallback(this);
            while (true)
            {
                return;
                localMediaRouteActionProvider.refreshVisibility();
            }
        }

        public void onRouteRemoved(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteActionProvider localMediaRouteActionProvider = (MediaRouteActionProvider)this.mAp.get();
            if (localMediaRouteActionProvider == null)
                paramMediaRouter.removeCallback(this);
            while (true)
            {
                return;
                localMediaRouteActionProvider.refreshVisibility();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.MediaRouteActionProvider
 * JD-Core Version:        0.6.2
 */