package android.app;

import android.util.AndroidRuntimeException;

final class RemoteServiceException extends AndroidRuntimeException
{
    public RemoteServiceException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.RemoteServiceException
 * JD-Core Version:        0.6.2
 */