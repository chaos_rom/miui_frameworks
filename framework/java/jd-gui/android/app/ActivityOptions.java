package android.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IRemoteCallback;
import android.os.IRemoteCallback.Stub;
import android.os.RemoteException;
import android.view.View;

public class ActivityOptions
{
    public static final int ANIM_CUSTOM = 1;
    public static final int ANIM_NONE = 0;
    public static final int ANIM_SCALE_UP = 2;
    public static final int ANIM_THUMBNAIL = 3;
    public static final int ANIM_THUMBNAIL_DELAYED = 4;
    public static final String KEY_ANIM_ENTER_RES_ID = "android:animEnterRes";
    public static final String KEY_ANIM_EXIT_RES_ID = "android:animExitRes";
    public static final String KEY_ANIM_START_HEIGHT = "android:animStartHeight";
    public static final String KEY_ANIM_START_LISTENER = "android:animStartListener";
    public static final String KEY_ANIM_START_WIDTH = "android:animStartWidth";
    public static final String KEY_ANIM_START_X = "android:animStartX";
    public static final String KEY_ANIM_START_Y = "android:animStartY";
    public static final String KEY_ANIM_THUMBNAIL = "android:animThumbnail";
    public static final String KEY_ANIM_TYPE = "android:animType";
    public static final String KEY_PACKAGE_NAME = "android:packageName";
    private IRemoteCallback mAnimationStartedListener;
    private int mAnimationType = 0;
    private int mCustomEnterResId;
    private int mCustomExitResId;
    private String mPackageName;
    private int mStartHeight;
    private int mStartWidth;
    private int mStartX;
    private int mStartY;
    private Bitmap mThumbnail;

    private ActivityOptions()
    {
    }

    public ActivityOptions(Bundle paramBundle)
    {
        this.mPackageName = paramBundle.getString("android:packageName");
        this.mAnimationType = paramBundle.getInt("android:animType");
        if (this.mAnimationType == 1)
        {
            this.mCustomEnterResId = paramBundle.getInt("android:animEnterRes", 0);
            this.mCustomExitResId = paramBundle.getInt("android:animExitRes", 0);
            this.mAnimationStartedListener = IRemoteCallback.Stub.asInterface(paramBundle.getIBinder("android:animStartListener"));
        }
        while (true)
        {
            return;
            if (this.mAnimationType == 2)
            {
                this.mStartX = paramBundle.getInt("android:animStartX", 0);
                this.mStartY = paramBundle.getInt("android:animStartY", 0);
                this.mStartWidth = paramBundle.getInt("android:animStartWidth", 0);
                this.mStartHeight = paramBundle.getInt("android:animStartHeight", 0);
            }
            else if ((this.mAnimationType == 3) || (this.mAnimationType == 4))
            {
                this.mThumbnail = ((Bitmap)paramBundle.getParcelable("android:animThumbnail"));
                this.mStartX = paramBundle.getInt("android:animStartX", 0);
                this.mStartY = paramBundle.getInt("android:animStartY", 0);
                this.mAnimationStartedListener = IRemoteCallback.Stub.asInterface(paramBundle.getIBinder("android:animStartListener"));
            }
        }
    }

    public static void abort(Bundle paramBundle)
    {
        if (paramBundle != null)
            new ActivityOptions(paramBundle).abort();
    }

    public static ActivityOptions makeCustomAnimation(Context paramContext, int paramInt1, int paramInt2)
    {
        return makeCustomAnimation(paramContext, paramInt1, paramInt2, null, null);
    }

    public static ActivityOptions makeCustomAnimation(Context paramContext, int paramInt1, int paramInt2, Handler paramHandler, OnAnimationStartedListener paramOnAnimationStartedListener)
    {
        ActivityOptions localActivityOptions = new ActivityOptions();
        localActivityOptions.mPackageName = paramContext.getPackageName();
        localActivityOptions.mAnimationType = 1;
        localActivityOptions.mCustomEnterResId = paramInt1;
        localActivityOptions.mCustomExitResId = paramInt2;
        localActivityOptions.setListener(paramHandler, paramOnAnimationStartedListener);
        return localActivityOptions;
    }

    public static ActivityOptions makeDelayedThumbnailScaleUpAnimation(View paramView, Bitmap paramBitmap, int paramInt1, int paramInt2, OnAnimationStartedListener paramOnAnimationStartedListener)
    {
        return makeThumbnailScaleUpAnimation(paramView, paramBitmap, paramInt1, paramInt2, paramOnAnimationStartedListener, true);
    }

    public static ActivityOptions makeScaleUpAnimation(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        ActivityOptions localActivityOptions = new ActivityOptions();
        localActivityOptions.mPackageName = paramView.getContext().getPackageName();
        localActivityOptions.mAnimationType = 2;
        int[] arrayOfInt = new int[2];
        paramView.getLocationOnScreen(arrayOfInt);
        localActivityOptions.mStartX = (paramInt1 + arrayOfInt[0]);
        localActivityOptions.mStartY = (paramInt2 + arrayOfInt[1]);
        localActivityOptions.mStartWidth = paramInt3;
        localActivityOptions.mStartHeight = paramInt4;
        return localActivityOptions;
    }

    public static ActivityOptions makeThumbnailScaleUpAnimation(View paramView, Bitmap paramBitmap, int paramInt1, int paramInt2)
    {
        return makeThumbnailScaleUpAnimation(paramView, paramBitmap, paramInt1, paramInt2, null);
    }

    public static ActivityOptions makeThumbnailScaleUpAnimation(View paramView, Bitmap paramBitmap, int paramInt1, int paramInt2, OnAnimationStartedListener paramOnAnimationStartedListener)
    {
        return makeThumbnailScaleUpAnimation(paramView, paramBitmap, paramInt1, paramInt2, paramOnAnimationStartedListener, false);
    }

    private static ActivityOptions makeThumbnailScaleUpAnimation(View paramView, Bitmap paramBitmap, int paramInt1, int paramInt2, OnAnimationStartedListener paramOnAnimationStartedListener, boolean paramBoolean)
    {
        ActivityOptions localActivityOptions = new ActivityOptions();
        localActivityOptions.mPackageName = paramView.getContext().getPackageName();
        if (paramBoolean);
        for (int i = 4; ; i = 3)
        {
            localActivityOptions.mAnimationType = i;
            localActivityOptions.mThumbnail = paramBitmap;
            int[] arrayOfInt = new int[2];
            paramView.getLocationOnScreen(arrayOfInt);
            localActivityOptions.mStartX = (paramInt1 + arrayOfInt[0]);
            localActivityOptions.mStartY = (paramInt2 + arrayOfInt[1]);
            localActivityOptions.setListener(paramView.getHandler(), paramOnAnimationStartedListener);
            return localActivityOptions;
        }
    }

    private void setListener(final Handler paramHandler, final OnAnimationStartedListener paramOnAnimationStartedListener)
    {
        if (paramOnAnimationStartedListener != null)
            this.mAnimationStartedListener = new IRemoteCallback.Stub()
            {
                public void sendResult(Bundle paramAnonymousBundle)
                    throws RemoteException
                {
                    paramHandler.post(new Runnable()
                    {
                        public void run()
                        {
                            ActivityOptions.1.this.val$finalListener.onAnimationStarted();
                        }
                    });
                }
            };
    }

    public void abort()
    {
        if (this.mAnimationStartedListener != null);
        try
        {
            this.mAnimationStartedListener.sendResult(null);
            label17: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label17;
        }
    }

    public int getAnimationType()
    {
        return this.mAnimationType;
    }

    public int getCustomEnterResId()
    {
        return this.mCustomEnterResId;
    }

    public int getCustomExitResId()
    {
        return this.mCustomExitResId;
    }

    public IRemoteCallback getOnAnimationStartListener()
    {
        return this.mAnimationStartedListener;
    }

    public String getPackageName()
    {
        return this.mPackageName;
    }

    public int getStartHeight()
    {
        return this.mStartHeight;
    }

    public int getStartWidth()
    {
        return this.mStartWidth;
    }

    public int getStartX()
    {
        return this.mStartX;
    }

    public int getStartY()
    {
        return this.mStartY;
    }

    public Bitmap getThumbnail()
    {
        return this.mThumbnail;
    }

    public Bundle toBundle()
    {
        IBinder localIBinder = null;
        Bundle localBundle = new Bundle();
        if (this.mPackageName != null)
            localBundle.putString("android:packageName", this.mPackageName);
        switch (this.mAnimationType)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return localBundle;
            localBundle.putInt("android:animType", this.mAnimationType);
            localBundle.putInt("android:animEnterRes", this.mCustomEnterResId);
            localBundle.putInt("android:animExitRes", this.mCustomExitResId);
            if (this.mAnimationStartedListener != null)
                localIBinder = this.mAnimationStartedListener.asBinder();
            localBundle.putIBinder("android:animStartListener", localIBinder);
            continue;
            localBundle.putInt("android:animType", this.mAnimationType);
            localBundle.putInt("android:animStartX", this.mStartX);
            localBundle.putInt("android:animStartY", this.mStartY);
            localBundle.putInt("android:animStartWidth", this.mStartWidth);
            localBundle.putInt("android:animStartHeight", this.mStartHeight);
            continue;
            localBundle.putInt("android:animType", this.mAnimationType);
            localBundle.putParcelable("android:animThumbnail", this.mThumbnail);
            localBundle.putInt("android:animStartX", this.mStartX);
            localBundle.putInt("android:animStartY", this.mStartY);
            if (this.mAnimationStartedListener != null)
                localIBinder = this.mAnimationStartedListener.asBinder();
            localBundle.putIBinder("android:animStartListener", localIBinder);
        }
    }

    public void update(ActivityOptions paramActivityOptions)
    {
        if (paramActivityOptions.mPackageName != null)
            this.mPackageName = paramActivityOptions.mPackageName;
        switch (paramActivityOptions.mAnimationType)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return;
            this.mAnimationType = paramActivityOptions.mAnimationType;
            this.mCustomEnterResId = paramActivityOptions.mCustomEnterResId;
            this.mCustomExitResId = paramActivityOptions.mCustomExitResId;
            this.mThumbnail = null;
            if (paramActivityOptions.mAnimationStartedListener != null);
            try
            {
                paramActivityOptions.mAnimationStartedListener.sendResult(null);
                label95: this.mAnimationStartedListener = paramActivityOptions.mAnimationStartedListener;
                continue;
                this.mAnimationType = paramActivityOptions.mAnimationType;
                this.mStartX = paramActivityOptions.mStartX;
                this.mStartY = paramActivityOptions.mStartY;
                this.mStartWidth = paramActivityOptions.mStartWidth;
                this.mStartHeight = paramActivityOptions.mStartHeight;
                if (paramActivityOptions.mAnimationStartedListener != null);
                try
                {
                    paramActivityOptions.mAnimationStartedListener.sendResult(null);
                    label163: this.mAnimationStartedListener = null;
                    continue;
                    this.mAnimationType = paramActivityOptions.mAnimationType;
                    this.mThumbnail = paramActivityOptions.mThumbnail;
                    this.mStartX = paramActivityOptions.mStartX;
                    this.mStartY = paramActivityOptions.mStartY;
                    if (paramActivityOptions.mAnimationStartedListener != null);
                    try
                    {
                        paramActivityOptions.mAnimationStartedListener.sendResult(null);
                        label220: this.mAnimationStartedListener = paramActivityOptions.mAnimationStartedListener;
                    }
                    catch (RemoteException localRemoteException1)
                    {
                        break label220;
                    }
                }
                catch (RemoteException localRemoteException2)
                {
                    break label163;
                }
            }
            catch (RemoteException localRemoteException3)
            {
                break label95;
            }
        }
    }

    public static abstract interface OnAnimationStartedListener
    {
        public abstract void onAnimationStarted();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ActivityOptions
 * JD-Core Version:        0.6.2
 */