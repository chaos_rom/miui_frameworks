package android.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.MediaRouter;
import android.media.MediaRouter.RouteGroup;
import android.media.MediaRouter.RouteInfo;
import android.media.MediaRouter.SimpleCallback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import com.android.internal.R.styleable;
import com.android.internal.app.MediaRouteChooserDialogFragment;
import com.android.internal.app.MediaRouteChooserDialogFragment.LauncherListener;

public class MediaRouteButton extends View
{
    private static final int[] ACTIVATED_STATE_SET = arrayOfInt;
    private static final String TAG = "MediaRouteButton";
    private boolean mAttachedToWindow;
    private MediaRouteChooserDialogFragment mDialogFragment;
    private View.OnClickListener mExtendedSettingsClickListener;
    private int mMinHeight;
    private int mMinWidth;
    private boolean mRemoteActive;
    private Drawable mRemoteIndicator;
    private int mRouteTypes;
    private MediaRouter mRouter;
    private final MediaRouteCallback mRouterCallback = new MediaRouteCallback(null);
    private boolean mToggleMode;

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16843518;
    }

    public MediaRouteButton(Context paramContext)
    {
        this(paramContext, null);
    }

    public MediaRouteButton(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843693);
    }

    public MediaRouteButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        this.mRouter = ((MediaRouter)paramContext.getSystemService("media_router"));
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MediaRouteButton, paramInt, 0);
        setRemoteIndicatorDrawable(localTypedArray.getDrawable(3));
        this.mMinWidth = localTypedArray.getDimensionPixelSize(0, 0);
        this.mMinHeight = localTypedArray.getDimensionPixelSize(1, 0);
        int i = localTypedArray.getInteger(2, 1);
        localTypedArray.recycle();
        setClickable(true);
        setRouteTypes(i);
    }

    private Activity getActivity()
    {
        for (Context localContext = getContext(); ((localContext instanceof ContextWrapper)) && (!(localContext instanceof Activity)); localContext = ((ContextWrapper)localContext).getBaseContext());
        if (!(localContext instanceof Activity))
            throw new IllegalStateException("The MediaRouteButton's Context is not an Activity.");
        return (Activity)localContext;
    }

    private void setRemoteIndicatorDrawable(Drawable paramDrawable)
    {
        if (this.mRemoteIndicator != null)
        {
            this.mRemoteIndicator.setCallback(null);
            unscheduleDrawable(this.mRemoteIndicator);
        }
        this.mRemoteIndicator = paramDrawable;
        if (paramDrawable != null)
        {
            paramDrawable.setCallback(this);
            paramDrawable.setState(getDrawableState());
            if (getVisibility() != 0)
                break label67;
        }
        label67: for (boolean bool = true; ; bool = false)
        {
            paramDrawable.setVisible(bool, false);
            refreshDrawableState();
            return;
        }
    }

    private void updateRouteInfo()
    {
        updateRemoteIndicator();
        updateRouteCount();
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.mRemoteIndicator != null)
        {
            int[] arrayOfInt = getDrawableState();
            this.mRemoteIndicator.setState(arrayOfInt);
            invalidate();
        }
    }

    public int getRouteTypes()
    {
        return this.mRouteTypes;
    }

    public void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        if (this.mRemoteIndicator != null)
            this.mRemoteIndicator.jumpToCurrentState();
    }

    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.mAttachedToWindow = true;
        if (this.mRouteTypes != 0)
        {
            this.mRouter.addCallback(this.mRouteTypes, this.mRouterCallback);
            updateRouteInfo();
        }
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
        if (this.mRemoteActive)
            mergeDrawableStates(arrayOfInt, ACTIVATED_STATE_SET);
        return arrayOfInt;
    }

    public void onDetachedFromWindow()
    {
        if (this.mRouteTypes != 0)
            this.mRouter.removeCallback(this.mRouterCallback);
        this.mAttachedToWindow = false;
        super.onDetachedFromWindow();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        if (this.mRemoteIndicator == null);
        while (true)
        {
            return;
            int i = getPaddingLeft();
            int j = getWidth() - getPaddingRight();
            int k = getPaddingTop();
            int m = getHeight() - getPaddingBottom();
            int n = this.mRemoteIndicator.getIntrinsicWidth();
            int i1 = this.mRemoteIndicator.getIntrinsicHeight();
            int i2 = i + (j - i - n) / 2;
            int i3 = k + (m - k - i1) / 2;
            this.mRemoteIndicator.setBounds(i2, i3, i2 + n, i3 + i1);
            this.mRemoteIndicator.draw(paramCanvas);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = 0;
        int j = View.MeasureSpec.getSize(paramInt1);
        int k = View.MeasureSpec.getSize(paramInt2);
        int m = View.MeasureSpec.getMode(paramInt1);
        int n = View.MeasureSpec.getMode(paramInt2);
        int i1 = this.mMinWidth;
        int i2;
        int i3;
        int i5;
        int i6;
        label130: int i7;
        if (this.mRemoteIndicator != null)
        {
            i2 = this.mRemoteIndicator.getIntrinsicWidth();
            i3 = Math.max(i1, i2);
            int i4 = this.mMinHeight;
            if (this.mRemoteIndicator != null)
                i = this.mRemoteIndicator.getIntrinsicHeight();
            i5 = Math.max(i4, i);
            switch (m)
            {
            default:
                i6 = i3 + getPaddingLeft() + getPaddingRight();
                switch (n)
                {
                default:
                    i7 = i5 + getPaddingTop() + getPaddingBottom();
                case 1073741824:
                case -2147483648:
                }
                break;
            case 1073741824:
            case -2147483648:
            }
        }
        while (true)
        {
            setMeasuredDimension(i6, i7);
            return;
            i2 = 0;
            break;
            i6 = j;
            break label130;
            i6 = Math.min(j, i3 + getPaddingLeft() + getPaddingRight());
            break label130;
            i7 = k;
            continue;
            i7 = Math.min(k, i5 + getPaddingTop() + getPaddingBottom());
        }
    }

    public boolean performClick()
    {
        boolean bool = super.performClick();
        if (!bool)
            playSoundEffect(0);
        if (this.mToggleMode)
            if (this.mRemoteActive)
                this.mRouter.selectRouteInt(this.mRouteTypes, this.mRouter.getSystemAudioRoute());
        while (true)
        {
            return bool;
            int i = this.mRouter.getRouteCount();
            for (int j = 0; j < i; j++)
            {
                MediaRouter.RouteInfo localRouteInfo = this.mRouter.getRouteAt(j);
                if (((localRouteInfo.getSupportedTypes() & this.mRouteTypes) != 0) && (localRouteInfo != this.mRouter.getSystemAudioRoute()))
                    this.mRouter.selectRouteInt(this.mRouteTypes, localRouteInfo);
            }
            continue;
            showDialog();
        }
    }

    public void setExtendedSettingsClickListener(View.OnClickListener paramOnClickListener)
    {
        this.mExtendedSettingsClickListener = paramOnClickListener;
        if (this.mDialogFragment != null)
            this.mDialogFragment.setExtendedSettingsClickListener(paramOnClickListener);
    }

    public void setRouteTypes(int paramInt)
    {
        if (paramInt == this.mRouteTypes);
        while (true)
        {
            return;
            if ((this.mAttachedToWindow) && (this.mRouteTypes != 0))
                this.mRouter.removeCallback(this.mRouterCallback);
            this.mRouteTypes = paramInt;
            if (this.mAttachedToWindow)
            {
                updateRouteInfo();
                this.mRouter.addCallback(paramInt, this.mRouterCallback);
            }
        }
    }

    public void setVisibility(int paramInt)
    {
        super.setVisibility(paramInt);
        Drawable localDrawable;
        if (this.mRemoteIndicator != null)
        {
            localDrawable = this.mRemoteIndicator;
            if (getVisibility() != 0)
                break label34;
        }
        label34: for (boolean bool = true; ; bool = false)
        {
            localDrawable.setVisible(bool, false);
            return;
        }
    }

    public void showDialog()
    {
        FragmentManager localFragmentManager = getActivity().getFragmentManager();
        if (this.mDialogFragment == null)
            this.mDialogFragment = ((MediaRouteChooserDialogFragment)localFragmentManager.findFragmentByTag("android:MediaRouteChooserDialogFragment"));
        if (this.mDialogFragment != null)
            Log.w("MediaRouteButton", "showDialog(): Already showing!");
        while (true)
        {
            return;
            this.mDialogFragment = new MediaRouteChooserDialogFragment();
            this.mDialogFragment.setExtendedSettingsClickListener(this.mExtendedSettingsClickListener);
            this.mDialogFragment.setLauncherListener(new MediaRouteChooserDialogFragment.LauncherListener()
            {
                public void onDetached(MediaRouteChooserDialogFragment paramAnonymousMediaRouteChooserDialogFragment)
                {
                    MediaRouteButton.access$102(MediaRouteButton.this, null);
                }
            });
            this.mDialogFragment.setRouteTypes(this.mRouteTypes);
            this.mDialogFragment.show(localFragmentManager, "android:MediaRouteChooserDialogFragment");
        }
    }

    void updateRemoteIndicator()
    {
        if (this.mRouter.getSelectedRoute(this.mRouteTypes) != this.mRouter.getSystemAudioRoute());
        for (boolean bool = true; ; bool = false)
        {
            if (this.mRemoteActive != bool)
            {
                this.mRemoteActive = bool;
                refreshDrawableState();
            }
            return;
        }
    }

    void updateRouteCount()
    {
        boolean bool1 = true;
        int i = this.mRouter.getRouteCount();
        int j = 0;
        int k = 0;
        if (k < i)
        {
            MediaRouter.RouteInfo localRouteInfo = this.mRouter.getRouteAt(k);
            if ((localRouteInfo.getSupportedTypes() & this.mRouteTypes) != 0)
            {
                if (!(localRouteInfo instanceof MediaRouter.RouteGroup))
                    break label70;
                j += ((MediaRouter.RouteGroup)localRouteInfo).getRouteCount();
            }
            while (true)
            {
                k++;
                break;
                label70: j++;
            }
        }
        boolean bool2;
        if (j != 0)
        {
            bool2 = bool1;
            setEnabled(bool2);
            if ((j != 2) || ((0x1 & this.mRouteTypes) == 0))
                break label115;
        }
        while (true)
        {
            this.mToggleMode = bool1;
            return;
            bool2 = false;
            break;
            label115: bool1 = false;
        }
    }

    protected boolean verifyDrawable(Drawable paramDrawable)
    {
        if ((super.verifyDrawable(paramDrawable)) || (paramDrawable == this.mRemoteIndicator));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private class MediaRouteCallback extends MediaRouter.SimpleCallback
    {
        private MediaRouteCallback()
        {
        }

        public void onRouteAdded(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteButton.this.updateRouteCount();
        }

        public void onRouteGrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup, int paramInt)
        {
            MediaRouteButton.this.updateRouteCount();
        }

        public void onRouteRemoved(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteButton.this.updateRouteCount();
        }

        public void onRouteSelected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteButton.this.updateRemoteIndicator();
        }

        public void onRouteUngrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup)
        {
            MediaRouteButton.this.updateRouteCount();
        }

        public void onRouteUnselected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteButton.this.updateRemoteIndicator();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.MediaRouteButton
 * JD-Core Version:        0.6.2
 */