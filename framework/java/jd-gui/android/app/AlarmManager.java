package android.app;

import android.os.RemoteException;

public class AlarmManager
{
    public static final int ELAPSED_REALTIME = 3;
    public static final int ELAPSED_REALTIME_WAKEUP = 2;
    public static final long INTERVAL_DAY = 86400000L;
    public static final long INTERVAL_FIFTEEN_MINUTES = 900000L;
    public static final long INTERVAL_HALF_DAY = 43200000L;
    public static final long INTERVAL_HALF_HOUR = 1800000L;
    public static final long INTERVAL_HOUR = 3600000L;
    public static final int RTC = 1;
    public static final int RTC_WAKEUP;
    private final IAlarmManager mService;

    AlarmManager(IAlarmManager paramIAlarmManager)
    {
        this.mService = paramIAlarmManager;
    }

    public void cancel(PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.remove(paramPendingIntent);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void set(int paramInt, long paramLong, PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.set(paramInt, paramLong, paramPendingIntent);
            label13: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label13;
        }
    }

    public void setInexactRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.setInexactRepeating(paramInt, paramLong1, paramLong2, paramPendingIntent);
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void setRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.setRepeating(paramInt, paramLong1, paramLong2, paramPendingIntent);
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void setTime(long paramLong)
    {
        try
        {
            this.mService.setTime(paramLong);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setTimeZone(String paramString)
    {
        try
        {
            this.mService.setTimeZone(paramString);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.AlarmManager
 * JD-Core Version:        0.6.2
 */