package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IWallpaperManagerCallback extends IInterface
{
    public abstract void onWallpaperChanged()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWallpaperManagerCallback
    {
        private static final String DESCRIPTOR = "android.app.IWallpaperManagerCallback";
        static final int TRANSACTION_onWallpaperChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.app.IWallpaperManagerCallback");
        }

        public static IWallpaperManagerCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IWallpaperManagerCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IWallpaperManagerCallback)))
                    localObject = (IWallpaperManagerCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.app.IWallpaperManagerCallback");
                continue;
                paramParcel1.enforceInterface("android.app.IWallpaperManagerCallback");
                onWallpaperChanged();
            }
        }

        private static class Proxy
            implements IWallpaperManagerCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IWallpaperManagerCallback";
            }

            public void onWallpaperChanged()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IWallpaperManagerCallback");
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IWallpaperManagerCallback
 * JD-Core Version:        0.6.2
 */