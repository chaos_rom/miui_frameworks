package android.app;

import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface ISearchManager extends IInterface
{
    public abstract List<ResolveInfo> getGlobalSearchActivities()
        throws RemoteException;

    public abstract ComponentName getGlobalSearchActivity()
        throws RemoteException;

    public abstract SearchableInfo getSearchableInfo(ComponentName paramComponentName)
        throws RemoteException;

    public abstract List<SearchableInfo> getSearchablesInGlobalSearch()
        throws RemoteException;

    public abstract ComponentName getWebSearchActivity()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISearchManager
    {
        private static final String DESCRIPTOR = "android.app.ISearchManager";
        static final int TRANSACTION_getGlobalSearchActivities = 3;
        static final int TRANSACTION_getGlobalSearchActivity = 4;
        static final int TRANSACTION_getSearchableInfo = 1;
        static final int TRANSACTION_getSearchablesInGlobalSearch = 2;
        static final int TRANSACTION_getWebSearchActivity = 5;

        public Stub()
        {
            attachInterface(this, "android.app.ISearchManager");
        }

        public static ISearchManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.ISearchManager");
                if ((localIInterface != null) && ((localIInterface instanceof ISearchManager)))
                    localObject = (ISearchManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.app.ISearchManager");
                continue;
                paramParcel1.enforceInterface("android.app.ISearchManager");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName3 = null)
                {
                    SearchableInfo localSearchableInfo = getSearchableInfo(localComponentName3);
                    paramParcel2.writeNoException();
                    if (localSearchableInfo == null)
                        break label154;
                    paramParcel2.writeInt(i);
                    localSearchableInfo.writeToParcel(paramParcel2, i);
                    break;
                }
                label154: paramParcel2.writeInt(0);
                continue;
                paramParcel1.enforceInterface("android.app.ISearchManager");
                List localList2 = getSearchablesInGlobalSearch();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList2);
                continue;
                paramParcel1.enforceInterface("android.app.ISearchManager");
                List localList1 = getGlobalSearchActivities();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList1);
                continue;
                paramParcel1.enforceInterface("android.app.ISearchManager");
                ComponentName localComponentName2 = getGlobalSearchActivity();
                paramParcel2.writeNoException();
                if (localComponentName2 != null)
                {
                    paramParcel2.writeInt(i);
                    localComponentName2.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.app.ISearchManager");
                    ComponentName localComponentName1 = getWebSearchActivity();
                    paramParcel2.writeNoException();
                    if (localComponentName1 != null)
                    {
                        paramParcel2.writeInt(i);
                        localComponentName1.writeToParcel(paramParcel2, i);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                    }
                }
            }
        }

        private static class Proxy
            implements ISearchManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public List<ResolveInfo> getGlobalSearchActivities()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.ISearchManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(ResolveInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ComponentName getGlobalSearchActivity()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.ISearchManager");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(localParcel2);
                        return localComponentName;
                    }
                    ComponentName localComponentName = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.ISearchManager";
            }

            public SearchableInfo getSearchableInfo(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.app.ISearchManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(1, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localSearchableInfo = (SearchableInfo)SearchableInfo.CREATOR.createFromParcel(localParcel2);
                                return localSearchableInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    SearchableInfo localSearchableInfo = null;
                }
            }

            public List<SearchableInfo> getSearchablesInGlobalSearch()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.ISearchManager");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(SearchableInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ComponentName getWebSearchActivity()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.ISearchManager");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(localParcel2);
                        return localComponentName;
                    }
                    ComponentName localComponentName = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ISearchManager
 * JD-Core Version:        0.6.2
 */