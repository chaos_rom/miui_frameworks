package android.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserId;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Slog;
import android.view.Display;
import com.android.internal.app.IUsageStats;
import com.android.internal.app.IUsageStats.Stub;
import com.android.internal.os.PkgUsageStats;
import com.android.internal.util.MemInfoReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityManager
{
    public static final int BROADCAST_STICKY_CANT_HAVE_PERMISSION = -1;
    public static final int BROADCAST_SUCCESS = 0;
    public static final int COMPAT_MODE_ALWAYS = -1;
    public static final int COMPAT_MODE_DISABLED = 0;
    public static final int COMPAT_MODE_ENABLED = 1;
    public static final int COMPAT_MODE_NEVER = -2;
    public static final int COMPAT_MODE_TOGGLE = 2;
    public static final int COMPAT_MODE_UNKNOWN = -3;
    public static final int INTENT_SENDER_ACTIVITY = 2;
    public static final int INTENT_SENDER_ACTIVITY_RESULT = 3;
    public static final int INTENT_SENDER_BROADCAST = 1;
    public static final int INTENT_SENDER_SERVICE = 4;
    public static final int MOVE_TASK_NO_USER_ACTION = 2;
    public static final int MOVE_TASK_WITH_HOME = 1;
    public static final int RECENT_IGNORE_UNAVAILABLE = 2;
    public static final int RECENT_WITH_EXCLUDED = 1;
    public static final int REMOVE_TASK_KILL_PROCESS = 1;
    public static final int START_CANCELED = -6;
    public static final int START_CLASS_NOT_FOUND = -2;
    public static final int START_DELIVERED_TO_TOP = 3;
    public static final int START_FLAG_AUTO_STOP_PROFILER = 8;
    public static final int START_FLAG_DEBUG = 2;
    public static final int START_FLAG_ONLY_IF_NEEDED = 1;
    public static final int START_FLAG_OPENGL_TRACES = 4;
    public static final int START_FORWARD_AND_REQUEST_CONFLICT = -3;
    public static final int START_INTENT_NOT_RESOLVED = -1;
    public static final int START_NOT_ACTIVITY = -5;
    public static final int START_PERMISSION_DENIED = -4;
    public static final int START_RETURN_INTENT_TO_CALLER = 1;
    public static final int START_SUCCESS = 0;
    public static final int START_SWITCHES_CANCELED = 4;
    public static final int START_TASK_TO_FRONT = 2;
    private static String TAG = "ActivityManager";
    private static boolean localLOGV = false;
    private final Context mContext;
    private final Handler mHandler;

    ActivityManager(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mHandler = paramHandler;
    }

    public static int checkComponentPermission(String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        int i = -1;
        if ((paramInt1 == 0) || (paramInt1 == 1000))
            i = 0;
        while (true)
        {
            return i;
            if (!UserId.isIsolated(paramInt1))
                if ((paramInt2 >= 0) && (UserId.isSameApp(paramInt1, paramInt2)))
                    i = 0;
                else if (!paramBoolean)
                    Slog.w(TAG, "Permission denied: checkComponentPermission() owningUid=" + paramInt2);
                else if (paramString == null)
                    i = 0;
                else
                    try
                    {
                        int j = AppGlobals.getPackageManager().checkUidPermission(paramString, paramInt1);
                        i = j;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        Slog.e(TAG, "PackageManager is dead?!?", localRemoteException);
                    }
        }
    }

    public static void getMyMemoryState(RunningAppProcessInfo paramRunningAppProcessInfo)
    {
        try
        {
            ActivityManagerNative.getDefault().getMyMemoryState(paramRunningAppProcessInfo);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static boolean isHighEndGfx(Display paramDisplay)
    {
        boolean bool = true;
        MemInfoReader localMemInfoReader = new MemInfoReader();
        localMemInfoReader.readMemInfo();
        if (localMemInfoReader.getTotalSize() >= 536870912L);
        while (true)
        {
            return bool;
            Point localPoint = new Point();
            paramDisplay.getRealSize(localPoint);
            if (localPoint.x * localPoint.y < 614400)
                bool = ExtraActivityManager.isHighEndGfx(paramDisplay);
        }
    }

    public static boolean isLargeRAM()
    {
        MemInfoReader localMemInfoReader = new MemInfoReader();
        localMemInfoReader.readMemInfo();
        if (localMemInfoReader.getTotalSize() >= 671088640L);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isRunningInTestHarness()
    {
        return SystemProperties.getBoolean("ro.test_harness", false);
    }

    public static boolean isUserAMonkey()
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().isUserAMonkey();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public static int staticGetLargeMemoryClass()
    {
        String str = SystemProperties.get("dalvik.vm.heapsize", "16m");
        return Integer.parseInt(str.substring(0, -1 + str.length()));
    }

    public static int staticGetMemoryClass()
    {
        String str = SystemProperties.get("dalvik.vm.heapgrowthlimit", "");
        if ((str != null) && (!"".equals(str)));
        for (int i = Integer.parseInt(str.substring(0, -1 + str.length())); ; i = staticGetLargeMemoryClass())
            return i;
    }

    public boolean clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver)
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().clearApplicationUserData(paramString, paramIPackageDataObserver, Binder.getOrigCallingUser());
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void forceStopPackage(String paramString)
    {
        try
        {
            ActivityManagerNative.getDefault().forceStopPackage(paramString);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    public Map<String, Integer> getAllPackageLaunchCounts()
    {
        PkgUsageStats[] arrayOfPkgUsageStats;
        try
        {
            IUsageStats localIUsageStats = IUsageStats.Stub.asInterface(ServiceManager.getService("usagestats"));
            if (localIUsageStats == null)
            {
                localHashMap = new HashMap();
            }
            else
            {
                arrayOfPkgUsageStats = localIUsageStats.getAllPkgUsageStats();
                if (arrayOfPkgUsageStats == null)
                    localHashMap = new HashMap();
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.w(TAG, "Could not query launch counts", localRemoteException);
            localHashMap = new HashMap();
        }
        HashMap localHashMap = new HashMap();
        int i = arrayOfPkgUsageStats.length;
        for (int j = 0; j < i; j++)
        {
            PkgUsageStats localPkgUsageStats = arrayOfPkgUsageStats[j];
            localHashMap.put(localPkgUsageStats.packageName, Integer.valueOf(localPkgUsageStats.launchCount));
        }
        return localHashMap;
    }

    public PkgUsageStats[] getAllPackageUsageStats()
    {
        try
        {
            IUsageStats localIUsageStats = IUsageStats.Stub.asInterface(ServiceManager.getService("usagestats"));
            if (localIUsageStats != null)
            {
                PkgUsageStats[] arrayOfPkgUsageStats2 = localIUsageStats.getAllPkgUsageStats();
                arrayOfPkgUsageStats1 = arrayOfPkgUsageStats2;
                return arrayOfPkgUsageStats1;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w(TAG, "Could not query usage stats", localRemoteException);
                PkgUsageStats[] arrayOfPkgUsageStats1 = new PkgUsageStats[0];
            }
        }
    }

    public ConfigurationInfo getDeviceConfigurationInfo()
    {
        try
        {
            ConfigurationInfo localConfigurationInfo2 = ActivityManagerNative.getDefault().getDeviceConfigurationInfo();
            localConfigurationInfo1 = localConfigurationInfo2;
            return localConfigurationInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ConfigurationInfo localConfigurationInfo1 = null;
        }
    }

    public int getFrontActivityScreenCompatMode()
    {
        try
        {
            int j = ActivityManagerNative.getDefault().getFrontActivityScreenCompatMode();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public int getLargeMemoryClass()
    {
        return staticGetLargeMemoryClass();
    }

    public int getLauncherLargeIconDensity()
    {
        Resources localResources = this.mContext.getResources();
        int i = localResources.getDisplayMetrics().densityDpi;
        if (localResources.getConfiguration().smallestScreenWidthDp < 600);
        while (true)
        {
            return i;
            switch (i)
            {
            default:
                i = (int)(0.5F + 1.5F * i);
                break;
            case 120:
                i = 160;
                break;
            case 160:
                i = 240;
                break;
            case 213:
                i = 320;
                break;
            case 240:
                i = 320;
                break;
            case 320:
                i = 480;
                break;
            case 480:
                i = 640;
            }
        }
    }

    public int getLauncherLargeIconSize()
    {
        Resources localResources = this.mContext.getResources();
        int i = localResources.getDimensionPixelSize(17104896);
        if (localResources.getConfiguration().smallestScreenWidthDp < 600);
        while (true)
        {
            return i;
            switch (localResources.getDisplayMetrics().densityDpi)
            {
            default:
                i = (int)(0.5F + 1.5F * i);
                break;
            case 120:
                i = i * 160 / 120;
                break;
            case 160:
                i = i * 240 / 160;
                break;
            case 213:
                i = i * 320 / 240;
                break;
            case 240:
                i = i * 320 / 240;
                break;
            case 320:
                i = i * 480 / 320;
                break;
            case 480:
                i = 2 * (i * 320) / 480;
            }
        }
    }

    public int getMemoryClass()
    {
        return staticGetMemoryClass();
    }

    public void getMemoryInfo(MemoryInfo paramMemoryInfo)
    {
        try
        {
            ActivityManagerNative.getDefault().getMemoryInfo(paramMemoryInfo);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    public boolean getPackageAskScreenCompat(String paramString)
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().getPackageAskScreenCompat(paramString);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public int getPackageScreenCompatMode(String paramString)
    {
        try
        {
            int j = ActivityManagerNative.getDefault().getPackageScreenCompatMode(paramString);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public Debug.MemoryInfo[] getProcessMemoryInfo(int[] paramArrayOfInt)
    {
        try
        {
            Debug.MemoryInfo[] arrayOfMemoryInfo2 = ActivityManagerNative.getDefault().getProcessMemoryInfo(paramArrayOfInt);
            arrayOfMemoryInfo1 = arrayOfMemoryInfo2;
            return arrayOfMemoryInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Debug.MemoryInfo[] arrayOfMemoryInfo1 = null;
        }
    }

    public List<ProcessErrorStateInfo> getProcessesInErrorState()
    {
        try
        {
            List localList2 = ActivityManagerNative.getDefault().getProcessesInErrorState();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public List<RecentTaskInfo> getRecentTasks(int paramInt1, int paramInt2)
        throws SecurityException
    {
        try
        {
            List localList2 = ActivityManagerNative.getDefault().getRecentTasks(paramInt1, paramInt2);
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public List<RunningAppProcessInfo> getRunningAppProcesses()
    {
        try
        {
            List localList2 = ActivityManagerNative.getDefault().getRunningAppProcesses();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public List<ApplicationInfo> getRunningExternalApplications()
    {
        try
        {
            List localList2 = ActivityManagerNative.getDefault().getRunningExternalApplications();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public PendingIntent getRunningServiceControlPanel(ComponentName paramComponentName)
        throws SecurityException
    {
        try
        {
            PendingIntent localPendingIntent2 = ActivityManagerNative.getDefault().getRunningServiceControlPanel(paramComponentName);
            localPendingIntent1 = localPendingIntent2;
            return localPendingIntent1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                PendingIntent localPendingIntent1 = null;
        }
    }

    public List<RunningServiceInfo> getRunningServices(int paramInt)
        throws SecurityException
    {
        try
        {
            List localList2 = ActivityManagerNative.getDefault().getServices(paramInt, 0);
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public List<RunningTaskInfo> getRunningTasks(int paramInt)
        throws SecurityException
    {
        return getRunningTasks(paramInt, 0, null);
    }

    public List<RunningTaskInfo> getRunningTasks(int paramInt1, int paramInt2, IThumbnailReceiver paramIThumbnailReceiver)
        throws SecurityException
    {
        try
        {
            List localList2 = ActivityManagerNative.getDefault().getTasks(paramInt1, paramInt2, paramIThumbnailReceiver);
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public TaskThumbnails getTaskThumbnails(int paramInt)
        throws SecurityException
    {
        try
        {
            TaskThumbnails localTaskThumbnails2 = ActivityManagerNative.getDefault().getTaskThumbnails(paramInt);
            localTaskThumbnails1 = localTaskThumbnails2;
            return localTaskThumbnails1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                TaskThumbnails localTaskThumbnails1 = null;
        }
    }

    public void killBackgroundProcesses(String paramString)
    {
        try
        {
            ActivityManagerNative.getDefault().killBackgroundProcesses(paramString);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    public void moveTaskToFront(int paramInt1, int paramInt2)
    {
        moveTaskToFront(paramInt1, paramInt2, null);
    }

    public void moveTaskToFront(int paramInt1, int paramInt2, Bundle paramBundle)
    {
        try
        {
            ActivityManagerNative.getDefault().moveTaskToFront(paramInt1, paramInt2, paramBundle);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public boolean removeSubTask(int paramInt1, int paramInt2)
        throws SecurityException
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().removeSubTask(paramInt1, paramInt2);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean removeTask(int paramInt1, int paramInt2)
        throws SecurityException
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().removeTask(paramInt1, paramInt2);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    @Deprecated
    public void restartPackage(String paramString)
    {
        killBackgroundProcesses(paramString);
    }

    public void setFrontActivityScreenCompatMode(int paramInt)
    {
        try
        {
            ActivityManagerNative.getDefault().setFrontActivityScreenCompatMode(paramInt);
            label9: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label9;
        }
    }

    public void setPackageAskScreenCompat(String paramString, boolean paramBoolean)
    {
        try
        {
            ActivityManagerNative.getDefault().setPackageAskScreenCompat(paramString, paramBoolean);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setPackageScreenCompatMode(String paramString, int paramInt)
    {
        try
        {
            ActivityManagerNative.getDefault().setPackageScreenCompatMode(paramString, paramInt);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public boolean switchUser(int paramInt)
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().switchUser(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public static class RunningAppProcessInfo
        implements Parcelable
    {
        public static final Parcelable.Creator<RunningAppProcessInfo> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.RunningAppProcessInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.RunningAppProcessInfo(paramAnonymousParcel, null);
            }

            public ActivityManager.RunningAppProcessInfo[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.RunningAppProcessInfo[paramAnonymousInt];
            }
        };
        public static final int FLAG_CANT_SAVE_STATE = 1;
        public static final int FLAG_PERSISTENT = 2;
        public static final int IMPORTANCE_BACKGROUND = 400;
        public static final int IMPORTANCE_CANT_SAVE_STATE = 170;
        public static final int IMPORTANCE_EMPTY = 500;
        public static final int IMPORTANCE_FOREGROUND = 100;
        public static final int IMPORTANCE_PERCEPTIBLE = 130;
        public static final int IMPORTANCE_PERSISTENT = 50;
        public static final int IMPORTANCE_SERVICE = 300;
        public static final int IMPORTANCE_VISIBLE = 200;
        public static final int REASON_PROVIDER_IN_USE = 1;
        public static final int REASON_SERVICE_IN_USE = 2;
        public static final int REASON_UNKNOWN;
        public int flags;
        public int importance;
        public int importanceReasonCode;
        public ComponentName importanceReasonComponent;
        public int importanceReasonImportance;
        public int importanceReasonPid;
        public int lastTrimLevel;
        public int lru;
        public int pid;
        public String[] pkgList;
        public String processName;
        public int uid;

        public RunningAppProcessInfo()
        {
            this.importance = 100;
            this.importanceReasonCode = 0;
        }

        private RunningAppProcessInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public RunningAppProcessInfo(String paramString, int paramInt, String[] paramArrayOfString)
        {
            this.processName = paramString;
            this.pid = paramInt;
            this.pkgList = paramArrayOfString;
        }

        public int describeContents()
        {
            return 0;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.processName = paramParcel.readString();
            this.pid = paramParcel.readInt();
            this.uid = paramParcel.readInt();
            this.pkgList = paramParcel.readStringArray();
            this.flags = paramParcel.readInt();
            this.lastTrimLevel = paramParcel.readInt();
            this.importance = paramParcel.readInt();
            this.lru = paramParcel.readInt();
            this.importanceReasonCode = paramParcel.readInt();
            this.importanceReasonPid = paramParcel.readInt();
            this.importanceReasonComponent = ComponentName.readFromParcel(paramParcel);
            this.importanceReasonImportance = paramParcel.readInt();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeString(this.processName);
            paramParcel.writeInt(this.pid);
            paramParcel.writeInt(this.uid);
            paramParcel.writeStringArray(this.pkgList);
            paramParcel.writeInt(this.flags);
            paramParcel.writeInt(this.lastTrimLevel);
            paramParcel.writeInt(this.importance);
            paramParcel.writeInt(this.lru);
            paramParcel.writeInt(this.importanceReasonCode);
            paramParcel.writeInt(this.importanceReasonPid);
            ComponentName.writeToParcel(this.importanceReasonComponent, paramParcel);
            paramParcel.writeInt(this.importanceReasonImportance);
        }
    }

    public static class ProcessErrorStateInfo
        implements Parcelable
    {
        public static final int CRASHED = 1;
        public static final Parcelable.Creator<ProcessErrorStateInfo> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.ProcessErrorStateInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.ProcessErrorStateInfo(paramAnonymousParcel, null);
            }

            public ActivityManager.ProcessErrorStateInfo[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.ProcessErrorStateInfo[paramAnonymousInt];
            }
        };
        public static final int NOT_RESPONDING = 2;
        public static final int NO_ERROR;
        public int condition;
        public byte[] crashData = null;
        public String longMsg;
        public int pid;
        public String processName;
        public String shortMsg;
        public String stackTrace;
        public String tag;
        public int uid;

        public ProcessErrorStateInfo()
        {
        }

        private ProcessErrorStateInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.condition = paramParcel.readInt();
            this.processName = paramParcel.readString();
            this.pid = paramParcel.readInt();
            this.uid = paramParcel.readInt();
            this.tag = paramParcel.readString();
            this.shortMsg = paramParcel.readString();
            this.longMsg = paramParcel.readString();
            this.stackTrace = paramParcel.readString();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.condition);
            paramParcel.writeString(this.processName);
            paramParcel.writeInt(this.pid);
            paramParcel.writeInt(this.uid);
            paramParcel.writeString(this.tag);
            paramParcel.writeString(this.shortMsg);
            paramParcel.writeString(this.longMsg);
            paramParcel.writeString(this.stackTrace);
        }
    }

    public static class MemoryInfo
        implements Parcelable
    {
        public static final Parcelable.Creator<MemoryInfo> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.MemoryInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.MemoryInfo(paramAnonymousParcel, null);
            }

            public ActivityManager.MemoryInfo[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.MemoryInfo[paramAnonymousInt];
            }
        };
        public long availMem;
        public long foregroundAppThreshold;
        public long hiddenAppThreshold;
        public boolean lowMemory;
        public long secondaryServerThreshold;
        public long threshold;
        public long totalMem;
        public long visibleAppThreshold;

        public MemoryInfo()
        {
        }

        private MemoryInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.availMem = paramParcel.readLong();
            this.totalMem = paramParcel.readLong();
            this.threshold = paramParcel.readLong();
            if (paramParcel.readInt() != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.lowMemory = bool;
                this.hiddenAppThreshold = paramParcel.readLong();
                this.secondaryServerThreshold = paramParcel.readLong();
                this.visibleAppThreshold = paramParcel.readLong();
                this.foregroundAppThreshold = paramParcel.readLong();
                return;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeLong(this.availMem);
            paramParcel.writeLong(this.totalMem);
            paramParcel.writeLong(this.threshold);
            if (this.lowMemory);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                paramParcel.writeLong(this.hiddenAppThreshold);
                paramParcel.writeLong(this.secondaryServerThreshold);
                paramParcel.writeLong(this.visibleAppThreshold);
                paramParcel.writeLong(this.foregroundAppThreshold);
                return;
            }
        }
    }

    public static class RunningServiceInfo
        implements Parcelable
    {
        public static final Parcelable.Creator<RunningServiceInfo> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.RunningServiceInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.RunningServiceInfo(paramAnonymousParcel, null);
            }

            public ActivityManager.RunningServiceInfo[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.RunningServiceInfo[paramAnonymousInt];
            }
        };
        public static final int FLAG_FOREGROUND = 2;
        public static final int FLAG_PERSISTENT_PROCESS = 8;
        public static final int FLAG_STARTED = 1;
        public static final int FLAG_SYSTEM_PROCESS = 4;
        public long activeSince;
        public int clientCount;
        public int clientLabel;
        public String clientPackage;
        public int crashCount;
        public int flags;
        public boolean foreground;
        public long lastActivityTime;
        public int pid;
        public String process;
        public long restarting;
        public ComponentName service;
        public boolean started;
        public int uid;

        public RunningServiceInfo()
        {
        }

        private RunningServiceInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            boolean bool1 = true;
            this.service = ComponentName.readFromParcel(paramParcel);
            this.pid = paramParcel.readInt();
            this.uid = paramParcel.readInt();
            this.process = paramParcel.readString();
            boolean bool2;
            if (paramParcel.readInt() != 0)
            {
                bool2 = bool1;
                this.foreground = bool2;
                this.activeSince = paramParcel.readLong();
                if (paramParcel.readInt() == 0)
                    break label130;
            }
            while (true)
            {
                this.started = bool1;
                this.clientCount = paramParcel.readInt();
                this.crashCount = paramParcel.readInt();
                this.lastActivityTime = paramParcel.readLong();
                this.restarting = paramParcel.readLong();
                this.flags = paramParcel.readInt();
                this.clientPackage = paramParcel.readString();
                this.clientLabel = paramParcel.readInt();
                return;
                bool2 = false;
                break;
                label130: bool1 = false;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            int i = 1;
            ComponentName.writeToParcel(this.service, paramParcel);
            paramParcel.writeInt(this.pid);
            paramParcel.writeInt(this.uid);
            paramParcel.writeString(this.process);
            int j;
            if (this.foreground)
            {
                j = i;
                paramParcel.writeInt(j);
                paramParcel.writeLong(this.activeSince);
                if (!this.started)
                    break label133;
            }
            while (true)
            {
                paramParcel.writeInt(i);
                paramParcel.writeInt(this.clientCount);
                paramParcel.writeInt(this.crashCount);
                paramParcel.writeLong(this.lastActivityTime);
                paramParcel.writeLong(this.restarting);
                paramParcel.writeInt(this.flags);
                paramParcel.writeString(this.clientPackage);
                paramParcel.writeInt(this.clientLabel);
                return;
                j = 0;
                break;
                label133: i = 0;
            }
        }
    }

    public static class TaskThumbnails
        implements Parcelable
    {
        public static final Parcelable.Creator<TaskThumbnails> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.TaskThumbnails createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.TaskThumbnails(paramAnonymousParcel, null);
            }

            public ActivityManager.TaskThumbnails[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.TaskThumbnails[paramAnonymousInt];
            }
        };
        public Bitmap mainThumbnail;
        public int numSubThumbbails;
        public IThumbnailRetriever retriever;

        public TaskThumbnails()
        {
        }

        private TaskThumbnails(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public Bitmap getSubThumbnail(int paramInt)
        {
            try
            {
                Bitmap localBitmap2 = this.retriever.getThumbnail(paramInt);
                localBitmap1 = localBitmap2;
                return localBitmap1;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Bitmap localBitmap1 = null;
            }
        }

        public void readFromParcel(Parcel paramParcel)
        {
            if (paramParcel.readInt() != 0);
            for (this.mainThumbnail = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel)); ; this.mainThumbnail = null)
            {
                this.numSubThumbbails = paramParcel.readInt();
                this.retriever = IThumbnailRetriever.Stub.asInterface(paramParcel.readStrongBinder());
                return;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            if (this.mainThumbnail != null)
            {
                paramParcel.writeInt(1);
                this.mainThumbnail.writeToParcel(paramParcel, 0);
            }
            while (true)
            {
                paramParcel.writeInt(this.numSubThumbbails);
                paramParcel.writeStrongInterface(this.retriever);
                return;
                paramParcel.writeInt(0);
            }
        }
    }

    public static class RunningTaskInfo
        implements Parcelable
    {
        public static final Parcelable.Creator<RunningTaskInfo> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.RunningTaskInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.RunningTaskInfo(paramAnonymousParcel, null);
            }

            public ActivityManager.RunningTaskInfo[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.RunningTaskInfo[paramAnonymousInt];
            }
        };
        public ComponentName baseActivity;
        public CharSequence description;
        public int id;
        public int numActivities;
        public int numRunning;
        public Bitmap thumbnail;
        public ComponentName topActivity;

        public RunningTaskInfo()
        {
        }

        private RunningTaskInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.id = paramParcel.readInt();
            this.baseActivity = ComponentName.readFromParcel(paramParcel);
            this.topActivity = ComponentName.readFromParcel(paramParcel);
            if (paramParcel.readInt() != 0);
            for (this.thumbnail = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel)); ; this.thumbnail = null)
            {
                this.description = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
                this.numActivities = paramParcel.readInt();
                this.numRunning = paramParcel.readInt();
                return;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.id);
            ComponentName.writeToParcel(this.baseActivity, paramParcel);
            ComponentName.writeToParcel(this.topActivity, paramParcel);
            if (this.thumbnail != null)
            {
                paramParcel.writeInt(1);
                this.thumbnail.writeToParcel(paramParcel, 0);
            }
            while (true)
            {
                TextUtils.writeToParcel(this.description, paramParcel, 1);
                paramParcel.writeInt(this.numActivities);
                paramParcel.writeInt(this.numRunning);
                return;
                paramParcel.writeInt(0);
            }
        }
    }

    public static class RecentTaskInfo
        implements Parcelable
    {
        public static final Parcelable.Creator<RecentTaskInfo> CREATOR = new Parcelable.Creator()
        {
            public ActivityManager.RecentTaskInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new ActivityManager.RecentTaskInfo(paramAnonymousParcel, null);
            }

            public ActivityManager.RecentTaskInfo[] newArray(int paramAnonymousInt)
            {
                return new ActivityManager.RecentTaskInfo[paramAnonymousInt];
            }
        };
        public Intent baseIntent;
        public CharSequence description;
        public int id;
        public ComponentName origActivity;
        public int persistentId;

        public RecentTaskInfo()
        {
        }

        private RecentTaskInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public int describeContents()
        {
            return 0;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.id = paramParcel.readInt();
            this.persistentId = paramParcel.readInt();
            if (paramParcel.readInt() != 0);
            for (this.baseIntent = ((Intent)Intent.CREATOR.createFromParcel(paramParcel)); ; this.baseIntent = null)
            {
                this.origActivity = ComponentName.readFromParcel(paramParcel);
                this.description = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
                return;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.id);
            paramParcel.writeInt(this.persistentId);
            if (this.baseIntent != null)
            {
                paramParcel.writeInt(1);
                this.baseIntent.writeToParcel(paramParcel, 0);
            }
            while (true)
            {
                ComponentName.writeToParcel(this.origActivity, paramParcel);
                TextUtils.writeToParcel(this.description, paramParcel, 1);
                return;
                paramParcel.writeInt(0);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ActivityManager
 * JD-Core Version:        0.6.2
 */