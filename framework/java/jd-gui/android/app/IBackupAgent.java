package android.app;

import android.app.backup.IBackupManager;
import android.app.backup.IBackupManager.Stub;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IBackupAgent extends IInterface
{
    public abstract void doBackup(ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, ParcelFileDescriptor paramParcelFileDescriptor3, int paramInt, IBackupManager paramIBackupManager)
        throws RemoteException;

    public abstract void doFullBackup(ParcelFileDescriptor paramParcelFileDescriptor, int paramInt, IBackupManager paramIBackupManager)
        throws RemoteException;

    public abstract void doRestore(ParcelFileDescriptor paramParcelFileDescriptor1, int paramInt1, ParcelFileDescriptor paramParcelFileDescriptor2, int paramInt2, IBackupManager paramIBackupManager)
        throws RemoteException;

    public abstract void doRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt1, String paramString1, String paramString2, long paramLong2, long paramLong3, int paramInt2, IBackupManager paramIBackupManager)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBackupAgent
    {
        private static final String DESCRIPTOR = "android.app.IBackupAgent";
        static final int TRANSACTION_doBackup = 1;
        static final int TRANSACTION_doFullBackup = 3;
        static final int TRANSACTION_doRestore = 2;
        static final int TRANSACTION_doRestoreFile = 4;

        public Stub()
        {
            attachInterface(this, "android.app.IBackupAgent");
        }

        public static IBackupAgent asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IBackupAgent");
                if ((localIInterface != null) && ((localIInterface instanceof IBackupAgent)))
                    localObject = (IBackupAgent)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
            case 1598968902:
                for (bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2); ; bool = true)
                {
                    return bool;
                    paramParcel2.writeString("android.app.IBackupAgent");
                }
            case 1:
                paramParcel1.enforceInterface("android.app.IBackupAgent");
                ParcelFileDescriptor localParcelFileDescriptor5;
                ParcelFileDescriptor localParcelFileDescriptor6;
                if (paramParcel1.readInt() != 0)
                {
                    localParcelFileDescriptor5 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label180;
                    localParcelFileDescriptor6 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label186;
                }
                for (ParcelFileDescriptor localParcelFileDescriptor7 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor7 = null)
                {
                    doBackup(localParcelFileDescriptor5, localParcelFileDescriptor6, localParcelFileDescriptor7, paramParcel1.readInt(), IBackupManager.Stub.asInterface(paramParcel1.readStrongBinder()));
                    bool = true;
                    break;
                    localParcelFileDescriptor5 = null;
                    break label105;
                    localParcelFileDescriptor6 = null;
                    break label126;
                }
            case 2:
                paramParcel1.enforceInterface("android.app.IBackupAgent");
                ParcelFileDescriptor localParcelFileDescriptor3;
                int k;
                if (paramParcel1.readInt() != 0)
                {
                    localParcelFileDescriptor3 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1);
                    k = paramParcel1.readInt();
                    if (paramParcel1.readInt() == 0)
                        break label279;
                }
                for (ParcelFileDescriptor localParcelFileDescriptor4 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor4 = null)
                {
                    doRestore(localParcelFileDescriptor3, k, localParcelFileDescriptor4, paramParcel1.readInt(), IBackupManager.Stub.asInterface(paramParcel1.readStrongBinder()));
                    bool = true;
                    break;
                    localParcelFileDescriptor3 = null;
                    break label219;
                }
            case 3:
                label105: label126: paramParcel1.enforceInterface("android.app.IBackupAgent");
                label180: label186: label219: if (paramParcel1.readInt() != 0);
                label279: for (ParcelFileDescriptor localParcelFileDescriptor2 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor2 = null)
                {
                    doFullBackup(localParcelFileDescriptor2, paramParcel1.readInt(), IBackupManager.Stub.asInterface(paramParcel1.readStrongBinder()));
                    bool = true;
                    break;
                }
            case 4:
            }
            paramParcel1.enforceInterface("android.app.IBackupAgent");
            if (paramParcel1.readInt() != 0);
            for (ParcelFileDescriptor localParcelFileDescriptor1 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor1 = null)
            {
                long l1 = paramParcel1.readLong();
                int i = paramParcel1.readInt();
                String str1 = paramParcel1.readString();
                String str2 = paramParcel1.readString();
                long l2 = paramParcel1.readLong();
                long l3 = paramParcel1.readLong();
                int j = paramParcel1.readInt();
                IBackupManager localIBackupManager = IBackupManager.Stub.asInterface(paramParcel1.readStrongBinder());
                doRestoreFile(localParcelFileDescriptor1, l1, i, str1, str2, l2, l3, j, localIBackupManager);
                bool = true;
                break;
            }
        }

        private static class Proxy
            implements IBackupAgent
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void doBackup(ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, ParcelFileDescriptor paramParcelFileDescriptor3, int paramInt, IBackupManager paramIBackupManager)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.app.IBackupAgent");
                        if (paramParcelFileDescriptor1 != null)
                        {
                            localParcel.writeInt(1);
                            paramParcelFileDescriptor1.writeToParcel(localParcel, 0);
                            if (paramParcelFileDescriptor2 != null)
                            {
                                localParcel.writeInt(1);
                                paramParcelFileDescriptor2.writeToParcel(localParcel, 0);
                                if (paramParcelFileDescriptor3 == null)
                                    break label143;
                                localParcel.writeInt(1);
                                paramParcelFileDescriptor3.writeToParcel(localParcel, 0);
                                localParcel.writeInt(paramInt);
                                if (paramIBackupManager != null)
                                    localIBinder = paramIBackupManager.asBinder();
                                localParcel.writeStrongBinder(localIBinder);
                                this.mRemote.transact(1, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                    continue;
                    label143: localParcel.writeInt(0);
                }
            }

            public void doFullBackup(ParcelFileDescriptor paramParcelFileDescriptor, int paramInt, IBackupManager paramIBackupManager)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IBackupAgent");
                    if (paramParcelFileDescriptor != null)
                    {
                        localParcel.writeInt(1);
                        paramParcelFileDescriptor.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        if (paramIBackupManager != null)
                            localIBinder = paramIBackupManager.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void doRestore(ParcelFileDescriptor paramParcelFileDescriptor1, int paramInt1, ParcelFileDescriptor paramParcelFileDescriptor2, int paramInt2, IBackupManager paramIBackupManager)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.app.IBackupAgent");
                        if (paramParcelFileDescriptor1 != null)
                        {
                            localParcel.writeInt(1);
                            paramParcelFileDescriptor1.writeToParcel(localParcel, 0);
                            localParcel.writeInt(paramInt1);
                            if (paramParcelFileDescriptor2 != null)
                            {
                                localParcel.writeInt(1);
                                paramParcelFileDescriptor2.writeToParcel(localParcel, 0);
                                localParcel.writeInt(paramInt2);
                                if (paramIBackupManager != null)
                                    localIBinder = paramIBackupManager.asBinder();
                                localParcel.writeStrongBinder(localIBinder);
                                this.mRemote.transact(2, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public void doRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt1, String paramString1, String paramString2, long paramLong2, long paramLong3, int paramInt2, IBackupManager paramIBackupManager)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.app.IBackupAgent");
                        if (paramParcelFileDescriptor != null)
                        {
                            localParcel.writeInt(1);
                            paramParcelFileDescriptor.writeToParcel(localParcel, 0);
                            localParcel.writeLong(paramLong1);
                            localParcel.writeInt(paramInt1);
                            localParcel.writeString(paramString1);
                            localParcel.writeString(paramString2);
                            localParcel.writeLong(paramLong2);
                            localParcel.writeLong(paramLong3);
                            localParcel.writeInt(paramInt2);
                            if (paramIBackupManager != null)
                            {
                                localIBinder = paramIBackupManager.asBinder();
                                localParcel.writeStrongBinder(localIBinder);
                                this.mRemote.transact(4, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    IBinder localIBinder = null;
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IBackupAgent";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IBackupAgent
 * JD-Core Version:        0.6.2
 */