package android.app;

import android.graphics.Bitmap;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.text.TextUtils;

public abstract interface IThumbnailReceiver extends IInterface
{
    public abstract void finished()
        throws RemoteException;

    public abstract void newThumbnail(int paramInt, Bitmap paramBitmap, CharSequence paramCharSequence)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IThumbnailReceiver
    {
        private static final String DESCRIPTOR = "android.app.IThumbnailReceiver";
        static final int TRANSACTION_finished = 2;
        static final int TRANSACTION_newThumbnail = 1;

        public Stub()
        {
            attachInterface(this, "android.app.IThumbnailReceiver");
        }

        public static IThumbnailReceiver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IThumbnailReceiver");
                if ((localIInterface != null) && ((localIInterface instanceof IThumbnailReceiver)))
                    localObject = (IThumbnailReceiver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.app.IThumbnailReceiver");
                continue;
                paramParcel1.enforceInterface("android.app.IThumbnailReceiver");
                int i = paramParcel1.readInt();
                Bitmap localBitmap;
                if (paramParcel1.readInt() != 0)
                {
                    localBitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel1);
                    label96: if (paramParcel1.readInt() == 0)
                        break label136;
                }
                label136: for (CharSequence localCharSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence = null)
                {
                    newThumbnail(i, localBitmap, localCharSequence);
                    break;
                    localBitmap = null;
                    break label96;
                }
                paramParcel1.enforceInterface("android.app.IThumbnailReceiver");
                finished();
            }
        }

        private static class Proxy
            implements IThumbnailReceiver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void finished()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IThumbnailReceiver");
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IThumbnailReceiver";
            }

            public void newThumbnail(int paramInt, Bitmap paramBitmap, CharSequence paramCharSequence)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.app.IThumbnailReceiver");
                        localParcel.writeInt(paramInt);
                        if (paramBitmap != null)
                        {
                            localParcel.writeInt(1);
                            paramBitmap.writeToParcel(localParcel, 0);
                            if (paramCharSequence != null)
                            {
                                localParcel.writeInt(1);
                                TextUtils.writeToParcel(paramCharSequence, localParcel, 0);
                                this.mRemote.transact(1, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IThumbnailReceiver
 * JD-Core Version:        0.6.2
 */