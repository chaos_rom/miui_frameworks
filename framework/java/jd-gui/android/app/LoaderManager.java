package android.app;

import android.content.Loader;
import android.os.Bundle;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class LoaderManager
{
    public static void enableDebugLogging(boolean paramBoolean)
    {
        LoaderManagerImpl.DEBUG = paramBoolean;
    }

    public abstract void destroyLoader(int paramInt);

    public abstract void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString);

    public abstract <D> Loader<D> getLoader(int paramInt);

    public abstract <D> Loader<D> initLoader(int paramInt, Bundle paramBundle, LoaderCallbacks<D> paramLoaderCallbacks);

    public abstract <D> Loader<D> restartLoader(int paramInt, Bundle paramBundle, LoaderCallbacks<D> paramLoaderCallbacks);

    public static abstract interface LoaderCallbacks<D>
    {
        public abstract Loader<D> onCreateLoader(int paramInt, Bundle paramBundle);

        public abstract void onLoadFinished(Loader<D> paramLoader, D paramD);

        public abstract void onLoaderReset(Loader<D> paramLoader);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.LoaderManager
 * JD-Core Version:        0.6.2
 */