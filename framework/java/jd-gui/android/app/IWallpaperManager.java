package android.app;

import android.content.ComponentName;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IWallpaperManager extends IInterface
{
    public abstract void clearWallpaper()
        throws RemoteException;

    public abstract int getHeightHint()
        throws RemoteException;

    public abstract ParcelFileDescriptor getWallpaper(IWallpaperManagerCallback paramIWallpaperManagerCallback, Bundle paramBundle)
        throws RemoteException;

    public abstract WallpaperInfo getWallpaperInfo()
        throws RemoteException;

    public abstract int getWidthHint()
        throws RemoteException;

    public abstract void setDimensionHints(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract ParcelFileDescriptor setWallpaper(String paramString)
        throws RemoteException;

    public abstract void setWallpaperComponent(ComponentName paramComponentName)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWallpaperManager
    {
        private static final String DESCRIPTOR = "android.app.IWallpaperManager";
        static final int TRANSACTION_clearWallpaper = 5;
        static final int TRANSACTION_getHeightHint = 8;
        static final int TRANSACTION_getWallpaper = 3;
        static final int TRANSACTION_getWallpaperInfo = 4;
        static final int TRANSACTION_getWidthHint = 7;
        static final int TRANSACTION_setDimensionHints = 6;
        static final int TRANSACTION_setWallpaper = 1;
        static final int TRANSACTION_setWallpaperComponent = 2;

        public Stub()
        {
            attachInterface(this, "android.app.IWallpaperManager");
        }

        public static IWallpaperManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IWallpaperManager");
                if ((localIInterface != null) && ((localIInterface instanceof IWallpaperManager)))
                    localObject = (IWallpaperManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.app.IWallpaperManager");
                continue;
                paramParcel1.enforceInterface("android.app.IWallpaperManager");
                ParcelFileDescriptor localParcelFileDescriptor2 = setWallpaper(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localParcelFileDescriptor2 != null)
                {
                    paramParcel2.writeInt(i);
                    localParcelFileDescriptor2.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.app.IWallpaperManager");
                    if (paramParcel1.readInt() != 0);
                    for (ComponentName localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName = null)
                    {
                        setWallpaperComponent(localComponentName);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.app.IWallpaperManager");
                    IWallpaperManagerCallback localIWallpaperManagerCallback = IWallpaperManagerCallback.Stub.asInterface(paramParcel1.readStrongBinder());
                    Bundle localBundle = new Bundle();
                    ParcelFileDescriptor localParcelFileDescriptor1 = getWallpaper(localIWallpaperManagerCallback, localBundle);
                    paramParcel2.writeNoException();
                    if (localParcelFileDescriptor1 != null)
                    {
                        paramParcel2.writeInt(i);
                        localParcelFileDescriptor1.writeToParcel(paramParcel2, i);
                    }
                    while (true)
                    {
                        if (localBundle == null)
                            break label294;
                        paramParcel2.writeInt(i);
                        localBundle.writeToParcel(paramParcel2, i);
                        break;
                        paramParcel2.writeInt(0);
                    }
                    label294: paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.app.IWallpaperManager");
                    WallpaperInfo localWallpaperInfo = getWallpaperInfo();
                    paramParcel2.writeNoException();
                    if (localWallpaperInfo != null)
                    {
                        paramParcel2.writeInt(i);
                        localWallpaperInfo.writeToParcel(paramParcel2, i);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.app.IWallpaperManager");
                        clearWallpaper();
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.app.IWallpaperManager");
                        setDimensionHints(paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.app.IWallpaperManager");
                        int k = getWidthHint();
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(k);
                        continue;
                        paramParcel1.enforceInterface("android.app.IWallpaperManager");
                        int j = getHeightHint();
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(j);
                    }
                }
            }
        }

        private static class Proxy
            implements IWallpaperManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearWallpaper()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getHeightHint()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IWallpaperManager";
            }

            public ParcelFileDescriptor getWallpaper(IWallpaperManagerCallback paramIWallpaperManagerCallback, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    if (paramIWallpaperManagerCallback != null);
                    for (IBinder localIBinder = paramIWallpaperManagerCallback.asBinder(); ; localIBinder = null)
                    {
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        if (localParcel2.readInt() == 0)
                            break;
                        localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                        if (localParcel2.readInt() != 0)
                            paramBundle.readFromParcel(localParcel2);
                        return localParcelFileDescriptor;
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public WallpaperInfo getWallpaperInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localWallpaperInfo = (WallpaperInfo)WallpaperInfo.CREATOR.createFromParcel(localParcel2);
                        return localWallpaperInfo;
                    }
                    WallpaperInfo localWallpaperInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getWidthHint()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setDimensionHints(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ParcelFileDescriptor setWallpaper(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                        return localParcelFileDescriptor;
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setWallpaperComponent(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IWallpaperManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IWallpaperManager
 * JD-Core Version:        0.6.2
 */