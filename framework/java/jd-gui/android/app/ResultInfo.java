package android.app;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ResultInfo
    implements Parcelable
{
    public static final Parcelable.Creator<ResultInfo> CREATOR = new Parcelable.Creator()
    {
        public ResultInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ResultInfo(paramAnonymousParcel);
        }

        public ResultInfo[] newArray(int paramAnonymousInt)
        {
            return new ResultInfo[paramAnonymousInt];
        }
    };
    public final Intent mData;
    public final int mRequestCode;
    public final int mResultCode;
    public final String mResultWho;

    public ResultInfo(Parcel paramParcel)
    {
        this.mResultWho = paramParcel.readString();
        this.mRequestCode = paramParcel.readInt();
        this.mResultCode = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        for (this.mData = ((Intent)Intent.CREATOR.createFromParcel(paramParcel)); ; this.mData = null)
            return;
    }

    public ResultInfo(String paramString, int paramInt1, int paramInt2, Intent paramIntent)
    {
        this.mResultWho = paramString;
        this.mRequestCode = paramInt1;
        this.mResultCode = paramInt2;
        this.mData = paramIntent;
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "ResultInfo{who=" + this.mResultWho + ", request=" + this.mRequestCode + ", result=" + this.mResultCode + ", data=" + this.mData + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mResultWho);
        paramParcel.writeInt(this.mRequestCode);
        paramParcel.writeInt(this.mResultCode);
        if (this.mData != null)
        {
            paramParcel.writeInt(1);
            this.mData.writeToParcel(paramParcel, 0);
        }
        while (true)
        {
            return;
            paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ResultInfo
 * JD-Core Version:        0.6.2
 */