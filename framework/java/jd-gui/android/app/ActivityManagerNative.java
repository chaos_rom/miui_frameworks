package android.app;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Singleton;

public abstract class ActivityManagerNative extends Binder
    implements IActivityManager
{
    private static final Singleton<IActivityManager> gDefault = new Singleton()
    {
        protected IActivityManager create()
        {
            return ActivityManagerNative.asInterface(ServiceManager.getService("activity"));
        }
    };
    static boolean sSystemReady = false;

    public ActivityManagerNative()
    {
        attachInterface(this, "android.app.IActivityManager");
    }

    public static IActivityManager asInterface(IBinder paramIBinder)
    {
        Object localObject;
        if (paramIBinder == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = (IActivityManager)paramIBinder.queryLocalInterface("android.app.IActivityManager");
            if (localObject == null)
                localObject = new ActivityManagerProxy(paramIBinder);
        }
    }

    public static void broadcastStickyIntent(Intent paramIntent, String paramString)
    {
        try
        {
            getDefault().broadcastIntent(null, paramIntent, null, null, -1, null, null, null, false, true, Binder.getOrigCallingUser());
            label23: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label23;
        }
    }

    public static IActivityManager getDefault()
    {
        return (IActivityManager)gDefault.get();
    }

    public static boolean isSystemReady()
    {
        if (!sSystemReady)
            sSystemReady = getDefault().testIsSystemReady();
        return sSystemReady;
    }

    public static void noteWakeupAlarm(PendingIntent paramPendingIntent)
    {
        try
        {
            getDefault().noteWakeupAlarm(paramPendingIntent.getTarget());
            label12: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label12;
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    // ERROR //
    public boolean onTransact(int paramInt1, android.os.Parcel paramParcel1, android.os.Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        // Byte code:
        //     0: iload_1
        //     1: tableswitch	default:+623 -> 624, 1:+5604->5605, 2:+5640->5641, 3:+637->638, 4:+5158->5159, 5:+5178->5179, 6:+623->624, 7:+623->624, 8:+623->624, 9:+623->624, 10:+623->624, 11:+1539->1540, 12:+1747->1748, 13:+1878->1879, 14:+1920->1921, 15:+2100->2101, 16:+2162->2163, 17:+2237->2238, 18:+2273->2274, 19:+2350->2351, 20:+2374->2375, 21:+2503->2504, 22:+2550->2551, 23:+2582->2583, 24:+2967->2968, 25:+3032->3033, 26:+3124->3125, 27:+3148->3149, 28:+3214->3215, 29:+3287->3288, 30:+3426->3427, 31:+3460->3461, 32:+1623->1624, 33:+3612->3613, 34:+3652->3653, 35:+3703->3704, 36:+3896->3897, 37:+3994->3995, 38:+4043->4044, 39:+623->624, 40:+5232->5233, 41:+5252->5253, 42:+5310->5311, 43:+5374->5375, 44:+4181->4182, 45:+4246->4247, 46:+4281->4282, 47:+4310->4311, 48:+3754->3755, 49:+4402->4403, 50:+4434->4435, 51:+4726->4727, 52:+4750->4751, 53:+4832->4833, 54:+4872->4873, 55:+4981->4982, 56:+5028->5029, 57:+5412->5413, 58:+5071->5072, 59:+5798->5799, 60:+2706->2707, 61:+4145->4146, 62:+2479->2480, 63:+4466->4467, 64:+4629->4630, 65:+4656->4657, 66:+5435->5436, 67:+1428->1429, 68:+5455->5456, 69:+3538->3539, 70:+4342->4343, 71:+4370->4371, 72:+4083->4084, 73:+4778->4779, 74:+3808->3809, 75:+3056->3057, 76:+5120->5121, 77:+2883->2884, 78:+4924->4925, 79:+5866->5867, 80:+5482->5483, 81:+2793->2794, 82:+2742->2743, 83:+2911->2912, 84:+5928->5929, 85:+6148->6149, 86:+5957->5958, 87:+6062->6063, 88:+6108->6109, 89:+6128->6129, 90:+6192->6193, 91:+6250->6251, 92:+6278->6279, 93:+4691->4692, 94:+623->624, 95:+6310->6311, 96:+6431->6432, 97:+6459->6460, 98:+6483->6484, 99:+6516->6517, 100:+1222->1223, 101:+6544->6545, 102:+5680->5681, 103:+5822->5823, 104:+6580->6581, 105:+838->839, 106:+1701->1702, 107:+1040->1041, 108:+2939->2940, 109:+6622->6623, 110:+5750->5751, 111:+6642->6643, 112:+6688->6689, 113:+6735->6736, 114:+6777->6778, 115:+6813->6814, 116:+6853->6854, 117:+6885->6886, 118:+6933->6934, 119:+6983->6984, 120:+7035->7036, 121:+7227->7228, 122:+7132->7133, 123:+2455->2456, 124:+7325->7326, 125:+7353->7354, 126:+7387->7388, 127:+7419->7420, 128:+7668->7669, 129:+7714->7715, 130:+7447->7448, 131:+7522->7523, 132:+7572->7573, 133:+7622->7623, 134:+7645->7646, 135:+7760->7761, 136:+7858->7859, 137:+7890->7891, 138:+7922->7923, 139:+7976->7977, 140:+5846->5847, 141:+3371->3372, 142:+3584->3585, 143:+5890->5891, 144:+5558->5559, 145:+7493->7494, 146:+7996->7997, 147:+8046->8047, 148:+5272->5273, 149:+1655->1656, 150:+8146->8147, 151:+3514->3515, 152:+7809->7810
        //     625: iload_1
        //     626: aload_2
        //     627: aload_3
        //     628: iload 4
        //     630: invokespecial 86	android/os/Binder:onTransact	(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //     633: istore 6
        //     635: iload 6
        //     637: ireturn
        //     638: aload_2
        //     639: ldc 25
        //     641: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     644: aload_2
        //     645: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     648: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     651: wide
        //     655: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     658: aload_2
        //     659: invokeinterface 112 2 0
        //     664: checkcast 102	android/content/Intent
        //     667: wide
        //     671: aload_2
        //     672: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     675: wide
        //     679: aload_2
        //     680: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     683: wide
        //     687: aload_2
        //     688: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     691: wide
        //     695: aload_2
        //     696: invokevirtual 119	android/os/Parcel:readInt	()I
        //     699: wide
        //     703: aload_2
        //     704: invokevirtual 119	android/os/Parcel:readInt	()I
        //     707: wide
        //     711: aload_2
        //     712: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     715: wide
        //     719: aload_2
        //     720: invokevirtual 119	android/os/Parcel:readInt	()I
        //     723: ifeq +100 -> 823
        //     726: aload_2
        //     727: invokevirtual 123	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     730: wide
        //     734: aload_2
        //     735: invokevirtual 119	android/os/Parcel:readInt	()I
        //     738: ifeq +93 -> 831
        //     741: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     744: aload_2
        //     745: invokeinterface 112 2 0
        //     750: checkcast 125	android/os/Bundle
        //     753: wide
        //     757: aload_0
        //     758: wide
        //     762: wide
        //     766: wide
        //     770: wide
        //     774: wide
        //     778: wide
        //     782: wide
        //     786: wide
        //     790: wide
        //     794: wide
        //     798: invokevirtual 130	android/app/ActivityManagerNative:startActivity	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)I
        //     801: wide
        //     805: aload_3
        //     806: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     809: aload_3
        //     810: wide
        //     814: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     817: iconst_1
        //     818: istore 6
        //     820: goto -185 -> 635
        //     823: aconst_null
        //     824: wide
        //     828: goto -94 -> 734
        //     831: aconst_null
        //     832: wide
        //     836: goto -79 -> 757
        //     839: aload_2
        //     840: ldc 25
        //     842: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     845: aload_2
        //     846: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     849: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     852: wide
        //     856: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     859: aload_2
        //     860: invokeinterface 112 2 0
        //     865: checkcast 102	android/content/Intent
        //     868: wide
        //     872: aload_2
        //     873: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     876: wide
        //     880: aload_2
        //     881: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     884: wide
        //     888: aload_2
        //     889: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     892: wide
        //     896: aload_2
        //     897: invokevirtual 119	android/os/Parcel:readInt	()I
        //     900: wide
        //     904: aload_2
        //     905: invokevirtual 119	android/os/Parcel:readInt	()I
        //     908: wide
        //     912: aload_2
        //     913: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     916: wide
        //     920: aload_2
        //     921: invokevirtual 119	android/os/Parcel:readInt	()I
        //     924: ifeq +101 -> 1025
        //     927: aload_2
        //     928: invokevirtual 123	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     931: wide
        //     935: aload_2
        //     936: invokevirtual 119	android/os/Parcel:readInt	()I
        //     939: ifeq +94 -> 1033
        //     942: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     945: aload_2
        //     946: invokeinterface 112 2 0
        //     951: checkcast 125	android/os/Bundle
        //     954: wide
        //     958: aload_0
        //     959: wide
        //     963: wide
        //     967: wide
        //     971: wide
        //     975: wide
        //     979: wide
        //     983: wide
        //     987: wide
        //     991: wide
        //     995: wide
        //     999: invokevirtual 141	android/app/ActivityManagerNative:startActivityAndWait	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)Landroid/app/IActivityManager$WaitResult;
        //     1002: wide
        //     1006: aload_3
        //     1007: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1010: wide
        //     1014: aload_3
        //     1015: iconst_0
        //     1016: invokevirtual 147	android/app/IActivityManager$WaitResult:writeToParcel	(Landroid/os/Parcel;I)V
        //     1019: iconst_1
        //     1020: istore 6
        //     1022: goto -387 -> 635
        //     1025: aconst_null
        //     1026: wide
        //     1030: goto -95 -> 935
        //     1033: aconst_null
        //     1034: wide
        //     1038: goto -80 -> 958
        //     1041: aload_2
        //     1042: ldc 25
        //     1044: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1047: aload_2
        //     1048: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1051: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     1054: wide
        //     1058: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1061: aload_2
        //     1062: invokeinterface 112 2 0
        //     1067: checkcast 102	android/content/Intent
        //     1070: wide
        //     1074: aload_2
        //     1075: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1078: wide
        //     1082: aload_2
        //     1083: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1086: wide
        //     1090: aload_2
        //     1091: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1094: wide
        //     1098: aload_2
        //     1099: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1102: wide
        //     1106: aload_2
        //     1107: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1110: wide
        //     1114: getstatic 150	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     1117: aload_2
        //     1118: invokeinterface 112 2 0
        //     1123: checkcast 149	android/content/res/Configuration
        //     1126: wide
        //     1130: aload_2
        //     1131: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1134: ifeq +81 -> 1215
        //     1137: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     1140: aload_2
        //     1141: invokeinterface 112 2 0
        //     1146: checkcast 125	android/os/Bundle
        //     1149: wide
        //     1153: aload_0
        //     1154: wide
        //     1158: wide
        //     1162: wide
        //     1166: wide
        //     1170: wide
        //     1174: wide
        //     1178: wide
        //     1182: wide
        //     1186: wide
        //     1190: invokevirtual 154	android/app/ActivityManagerNative:startActivityWithConfig	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/res/Configuration;Landroid/os/Bundle;)I
        //     1193: wide
        //     1197: aload_3
        //     1198: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1201: aload_3
        //     1202: wide
        //     1206: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1209: iconst_1
        //     1210: istore 6
        //     1212: goto -577 -> 635
        //     1215: aconst_null
        //     1216: wide
        //     1220: goto -67 -> 1153
        //     1223: aload_2
        //     1224: ldc 25
        //     1226: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1229: aload_2
        //     1230: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1233: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     1236: wide
        //     1240: getstatic 157	android/content/IntentSender:CREATOR	Landroid/os/Parcelable$Creator;
        //     1243: aload_2
        //     1244: invokeinterface 112 2 0
        //     1249: checkcast 156	android/content/IntentSender
        //     1252: wide
        //     1256: aconst_null
        //     1257: wide
        //     1261: aload_2
        //     1262: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1265: ifeq +19 -> 1284
        //     1268: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1271: aload_2
        //     1272: invokeinterface 112 2 0
        //     1277: checkcast 102	android/content/Intent
        //     1280: wide
        //     1284: aload_2
        //     1285: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1288: wide
        //     1292: aload_2
        //     1293: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1296: wide
        //     1300: aload_2
        //     1301: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1304: wide
        //     1308: aload_2
        //     1309: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1312: wide
        //     1316: aload_2
        //     1317: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1320: wide
        //     1324: aload_2
        //     1325: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1328: wide
        //     1332: aload_2
        //     1333: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1336: ifeq +85 -> 1421
        //     1339: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     1342: aload_2
        //     1343: invokeinterface 112 2 0
        //     1348: checkcast 125	android/os/Bundle
        //     1351: wide
        //     1355: aload_0
        //     1356: wide
        //     1360: wide
        //     1364: wide
        //     1368: wide
        //     1372: wide
        //     1376: wide
        //     1380: wide
        //     1384: wide
        //     1388: wide
        //     1392: wide
        //     1396: invokevirtual 161	android/app/ActivityManagerNative:startActivityIntentSender	(Landroid/app/IApplicationThread;Landroid/content/IntentSender;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I
        //     1399: wide
        //     1403: aload_3
        //     1404: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1407: aload_3
        //     1408: wide
        //     1412: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1415: iconst_1
        //     1416: istore 6
        //     1418: goto -783 -> 635
        //     1421: aconst_null
        //     1422: wide
        //     1426: goto -71 -> 1355
        //     1429: aload_2
        //     1430: ldc 25
        //     1432: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1435: aload_2
        //     1436: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1439: astore 254
        //     1441: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1444: aload_2
        //     1445: invokeinterface 112 2 0
        //     1450: checkcast 102	android/content/Intent
        //     1453: astore 255
        //     1455: aload_2
        //     1456: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1459: ifeq +65 -> 1524
        //     1462: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     1465: aload_2
        //     1466: invokeinterface 112 2 0
        //     1471: checkcast 125	android/os/Bundle
        //     1474: wide
        //     1478: aload_0
        //     1479: aload 254
        //     1481: aload 255
        //     1483: wide
        //     1487: invokevirtual 165	android/app/ActivityManagerNative:startNextMatchingActivity	(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)Z
        //     1490: wide
        //     1494: aload_3
        //     1495: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1498: wide
        //     1502: ifeq +30 -> 1532
        //     1505: iconst_1
        //     1506: wide
        //     1510: aload_3
        //     1511: wide
        //     1515: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1518: iconst_1
        //     1519: istore 6
        //     1521: goto -886 -> 635
        //     1524: aconst_null
        //     1525: wide
        //     1529: goto -51 -> 1478
        //     1532: iconst_0
        //     1533: wide
        //     1537: goto -27 -> 1510
        //     1540: aload_2
        //     1541: ldc 25
        //     1543: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1546: aload_2
        //     1547: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1550: astore 249
        //     1552: aconst_null
        //     1553: astore 250
        //     1555: aload_2
        //     1556: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1559: istore 251
        //     1561: aload_2
        //     1562: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1565: ifeq +17 -> 1582
        //     1568: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1571: aload_2
        //     1572: invokeinterface 112 2 0
        //     1577: checkcast 102	android/content/Intent
        //     1580: astore 250
        //     1582: aload_0
        //     1583: aload 249
        //     1585: iload 251
        //     1587: aload 250
        //     1589: invokevirtual 169	android/app/ActivityManagerNative:finishActivity	(Landroid/os/IBinder;ILandroid/content/Intent;)Z
        //     1592: istore 252
        //     1594: aload_3
        //     1595: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1598: iload 252
        //     1600: ifeq +18 -> 1618
        //     1603: iconst_1
        //     1604: istore 253
        //     1606: aload_3
        //     1607: iload 253
        //     1609: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1612: iconst_1
        //     1613: istore 6
        //     1615: goto -980 -> 635
        //     1618: iconst_0
        //     1619: istore 253
        //     1621: goto -15 -> 1606
        //     1624: aload_2
        //     1625: ldc 25
        //     1627: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1630: aload_0
        //     1631: aload_2
        //     1632: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1635: aload_2
        //     1636: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1639: aload_2
        //     1640: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1643: invokevirtual 173	android/app/ActivityManagerNative:finishSubActivity	(Landroid/os/IBinder;Ljava/lang/String;I)V
        //     1646: aload_3
        //     1647: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1650: iconst_1
        //     1651: istore 6
        //     1653: goto -1018 -> 635
        //     1656: aload_2
        //     1657: ldc 25
        //     1659: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1662: aload_0
        //     1663: aload_2
        //     1664: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1667: invokevirtual 177	android/app/ActivityManagerNative:finishActivityAffinity	(Landroid/os/IBinder;)Z
        //     1670: istore 247
        //     1672: aload_3
        //     1673: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1676: iload 247
        //     1678: ifeq +18 -> 1696
        //     1681: iconst_1
        //     1682: istore 248
        //     1684: aload_3
        //     1685: iload 248
        //     1687: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1690: iconst_1
        //     1691: istore 6
        //     1693: goto -1058 -> 635
        //     1696: iconst_0
        //     1697: istore 248
        //     1699: goto -15 -> 1684
        //     1702: aload_2
        //     1703: ldc 25
        //     1705: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1708: aload_0
        //     1709: aload_2
        //     1710: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1713: invokevirtual 180	android/app/ActivityManagerNative:willActivityBeVisible	(Landroid/os/IBinder;)Z
        //     1716: istore 245
        //     1718: aload_3
        //     1719: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1722: iload 245
        //     1724: ifeq +18 -> 1742
        //     1727: iconst_1
        //     1728: istore 246
        //     1730: aload_3
        //     1731: iload 246
        //     1733: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1736: iconst_1
        //     1737: istore 6
        //     1739: goto -1104 -> 635
        //     1742: iconst_0
        //     1743: istore 246
        //     1745: goto -15 -> 1730
        //     1748: aload_2
        //     1749: ldc 25
        //     1751: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1754: aload_2
        //     1755: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1758: astore 237
        //     1760: aload 237
        //     1762: ifnull +97 -> 1859
        //     1765: aload 237
        //     1767: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     1770: astore 238
        //     1772: aload_2
        //     1773: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1776: astore 239
        //     1778: aload_2
        //     1779: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1782: astore 240
        //     1784: aload 240
        //     1786: ifnull +79 -> 1865
        //     1789: aload 240
        //     1791: invokestatic 185	android/content/IIntentReceiver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;
        //     1794: astore 241
        //     1796: getstatic 188	android/content/IntentFilter:CREATOR	Landroid/os/Parcelable$Creator;
        //     1799: aload_2
        //     1800: invokeinterface 112 2 0
        //     1805: checkcast 187	android/content/IntentFilter
        //     1808: astore 242
        //     1810: aload_2
        //     1811: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1814: astore 243
        //     1816: aload_0
        //     1817: aload 238
        //     1819: aload 239
        //     1821: aload 241
        //     1823: aload 242
        //     1825: aload 243
        //     1827: invokevirtual 192	android/app/ActivityManagerNative:registerReceiver	(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/IIntentReceiver;Landroid/content/IntentFilter;Ljava/lang/String;)Landroid/content/Intent;
        //     1830: astore 244
        //     1832: aload_3
        //     1833: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1836: aload 244
        //     1838: ifnull +33 -> 1871
        //     1841: aload_3
        //     1842: iconst_1
        //     1843: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1846: aload 244
        //     1848: aload_3
        //     1849: iconst_0
        //     1850: invokevirtual 193	android/content/Intent:writeToParcel	(Landroid/os/Parcel;I)V
        //     1853: iconst_1
        //     1854: istore 6
        //     1856: goto -1221 -> 635
        //     1859: aconst_null
        //     1860: astore 238
        //     1862: goto -90 -> 1772
        //     1865: aconst_null
        //     1866: astore 241
        //     1868: goto -72 -> 1796
        //     1871: aload_3
        //     1872: iconst_0
        //     1873: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     1876: goto -23 -> 1853
        //     1879: aload_2
        //     1880: ldc 25
        //     1882: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1885: aload_2
        //     1886: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1889: astore 236
        //     1891: aload 236
        //     1893: ifnonnull +9 -> 1902
        //     1896: iconst_1
        //     1897: istore 6
        //     1899: goto -1264 -> 635
        //     1902: aload_0
        //     1903: aload 236
        //     1905: invokestatic 185	android/content/IIntentReceiver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;
        //     1908: invokevirtual 197	android/app/ActivityManagerNative:unregisterReceiver	(Landroid/content/IIntentReceiver;)V
        //     1911: aload_3
        //     1912: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     1915: iconst_1
        //     1916: istore 6
        //     1918: goto -1283 -> 635
        //     1921: aload_2
        //     1922: ldc 25
        //     1924: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1927: aload_2
        //     1928: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1931: astore 222
        //     1933: aload 222
        //     1935: ifnull +142 -> 2077
        //     1938: aload 222
        //     1940: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     1943: astore 223
        //     1945: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1948: aload_2
        //     1949: invokeinterface 112 2 0
        //     1954: checkcast 102	android/content/Intent
        //     1957: astore 224
        //     1959: aload_2
        //     1960: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1963: astore 225
        //     1965: aload_2
        //     1966: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1969: astore 226
        //     1971: aload 226
        //     1973: ifnull +110 -> 2083
        //     1976: aload 226
        //     1978: invokestatic 185	android/content/IIntentReceiver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;
        //     1981: astore 227
        //     1983: aload_2
        //     1984: invokevirtual 119	android/os/Parcel:readInt	()I
        //     1987: istore 228
        //     1989: aload_2
        //     1990: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     1993: astore 229
        //     1995: aload_2
        //     1996: invokevirtual 201	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     1999: astore 230
        //     2001: aload_2
        //     2002: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     2005: astore 231
        //     2007: aload_2
        //     2008: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2011: ifeq +78 -> 2089
        //     2014: iconst_1
        //     2015: istore 232
        //     2017: aload_2
        //     2018: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2021: ifeq +74 -> 2095
        //     2024: iconst_1
        //     2025: istore 233
        //     2027: aload_2
        //     2028: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2031: istore 234
        //     2033: aload_0
        //     2034: aload 223
        //     2036: aload 224
        //     2038: aload 225
        //     2040: aload 227
        //     2042: iload 228
        //     2044: aload 229
        //     2046: aload 230
        //     2048: aload 231
        //     2050: iload 232
        //     2052: iload 233
        //     2054: iload 234
        //     2056: invokevirtual 202	android/app/ActivityManagerNative:broadcastIntent	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
        //     2059: istore 235
        //     2061: aload_3
        //     2062: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2065: aload_3
        //     2066: iload 235
        //     2068: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     2071: iconst_1
        //     2072: istore 6
        //     2074: goto -1439 -> 635
        //     2077: aconst_null
        //     2078: astore 223
        //     2080: goto -135 -> 1945
        //     2083: aconst_null
        //     2084: astore 227
        //     2086: goto -103 -> 1983
        //     2089: iconst_0
        //     2090: istore 232
        //     2092: goto -75 -> 2017
        //     2095: iconst_0
        //     2096: istore 233
        //     2098: goto -71 -> 2027
        //     2101: aload_2
        //     2102: ldc 25
        //     2104: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2107: aload_2
        //     2108: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2111: astore 220
        //     2113: aload 220
        //     2115: ifnull +42 -> 2157
        //     2118: aload 220
        //     2120: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     2123: astore 221
        //     2125: aload_0
        //     2126: aload 221
        //     2128: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     2131: aload_2
        //     2132: invokeinterface 112 2 0
        //     2137: checkcast 102	android/content/Intent
        //     2140: aload_2
        //     2141: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2144: invokevirtual 206	android/app/ActivityManagerNative:unbroadcastIntent	(Landroid/app/IApplicationThread;Landroid/content/Intent;I)V
        //     2147: aload_3
        //     2148: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2151: iconst_1
        //     2152: istore 6
        //     2154: goto -1519 -> 635
        //     2157: aconst_null
        //     2158: astore 221
        //     2160: goto -35 -> 2125
        //     2163: aload_2
        //     2164: ldc 25
        //     2166: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2169: aload_2
        //     2170: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2173: astore 215
        //     2175: aload_2
        //     2176: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2179: istore 216
        //     2181: aload_2
        //     2182: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     2185: astore 217
        //     2187: aload_2
        //     2188: invokevirtual 201	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     2191: astore 218
        //     2193: aload_2
        //     2194: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2197: ifeq +35 -> 2232
        //     2200: iconst_1
        //     2201: istore 219
        //     2203: aload 215
        //     2205: ifnull +17 -> 2222
        //     2208: aload_0
        //     2209: aload 215
        //     2211: iload 216
        //     2213: aload 217
        //     2215: aload 218
        //     2217: iload 219
        //     2219: invokevirtual 210	android/app/ActivityManagerNative:finishReceiver	(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;Z)V
        //     2222: aload_3
        //     2223: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2226: iconst_1
        //     2227: istore 6
        //     2229: goto -1594 -> 635
        //     2232: iconst_0
        //     2233: istore 219
        //     2235: goto -32 -> 2203
        //     2238: aload_2
        //     2239: ldc 25
        //     2241: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2244: aload_2
        //     2245: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2248: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     2251: astore 214
        //     2253: aload 214
        //     2255: ifnull +9 -> 2264
        //     2258: aload_0
        //     2259: aload 214
        //     2261: invokevirtual 214	android/app/ActivityManagerNative:attachApplication	(Landroid/app/IApplicationThread;)V
        //     2264: aload_3
        //     2265: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2268: iconst_1
        //     2269: istore 6
        //     2271: goto -1636 -> 635
        //     2274: aload_2
        //     2275: ldc 25
        //     2277: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2280: aload_2
        //     2281: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2284: astore 211
        //     2286: aconst_null
        //     2287: astore 212
        //     2289: aload_2
        //     2290: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2293: ifeq +17 -> 2310
        //     2296: getstatic 150	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     2299: aload_2
        //     2300: invokeinterface 112 2 0
        //     2305: checkcast 149	android/content/res/Configuration
        //     2308: astore 212
        //     2310: aload_2
        //     2311: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2314: ifeq +31 -> 2345
        //     2317: iconst_1
        //     2318: istore 213
        //     2320: aload 211
        //     2322: ifnull +13 -> 2335
        //     2325: aload_0
        //     2326: aload 211
        //     2328: aload 212
        //     2330: iload 213
        //     2332: invokevirtual 218	android/app/ActivityManagerNative:activityIdle	(Landroid/os/IBinder;Landroid/content/res/Configuration;Z)V
        //     2335: aload_3
        //     2336: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2339: iconst_1
        //     2340: istore 6
        //     2342: goto -1707 -> 635
        //     2345: iconst_0
        //     2346: istore 213
        //     2348: goto -28 -> 2320
        //     2351: aload_2
        //     2352: ldc 25
        //     2354: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2357: aload_0
        //     2358: aload_2
        //     2359: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2362: invokevirtual 221	android/app/ActivityManagerNative:activityPaused	(Landroid/os/IBinder;)V
        //     2365: aload_3
        //     2366: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2369: iconst_1
        //     2370: istore 6
        //     2372: goto -1737 -> 635
        //     2375: aload_2
        //     2376: ldc 25
        //     2378: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2381: aload_2
        //     2382: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2385: astore 207
        //     2387: aload_2
        //     2388: invokevirtual 201	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     2391: astore 208
        //     2393: aload_2
        //     2394: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2397: ifeq +53 -> 2450
        //     2400: getstatic 224	android/graphics/Bitmap:CREATOR	Landroid/os/Parcelable$Creator;
        //     2403: aload_2
        //     2404: invokeinterface 112 2 0
        //     2409: checkcast 223	android/graphics/Bitmap
        //     2412: astore 209
        //     2414: getstatic 229	android/text/TextUtils:CHAR_SEQUENCE_CREATOR	Landroid/os/Parcelable$Creator;
        //     2417: aload_2
        //     2418: invokeinterface 112 2 0
        //     2423: checkcast 231	java/lang/CharSequence
        //     2426: astore 210
        //     2428: aload_0
        //     2429: aload 207
        //     2431: aload 208
        //     2433: aload 209
        //     2435: aload 210
        //     2437: invokevirtual 235	android/app/ActivityManagerNative:activityStopped	(Landroid/os/IBinder;Landroid/os/Bundle;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
        //     2440: aload_3
        //     2441: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2444: iconst_1
        //     2445: istore 6
        //     2447: goto -1812 -> 635
        //     2450: aconst_null
        //     2451: astore 209
        //     2453: goto -39 -> 2414
        //     2456: aload_2
        //     2457: ldc 25
        //     2459: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2462: aload_0
        //     2463: aload_2
        //     2464: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2467: invokevirtual 238	android/app/ActivityManagerNative:activitySlept	(Landroid/os/IBinder;)V
        //     2470: aload_3
        //     2471: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2474: iconst_1
        //     2475: istore 6
        //     2477: goto -1842 -> 635
        //     2480: aload_2
        //     2481: ldc 25
        //     2483: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2486: aload_0
        //     2487: aload_2
        //     2488: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2491: invokevirtual 241	android/app/ActivityManagerNative:activityDestroyed	(Landroid/os/IBinder;)V
        //     2494: aload_3
        //     2495: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2498: iconst_1
        //     2499: istore 6
        //     2501: goto -1866 -> 635
        //     2504: aload_2
        //     2505: ldc 25
        //     2507: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2510: aload_2
        //     2511: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2514: astore 205
        //     2516: aload 205
        //     2518: ifnull +27 -> 2545
        //     2521: aload_0
        //     2522: aload 205
        //     2524: invokevirtual 245	android/app/ActivityManagerNative:getCallingPackage	(Landroid/os/IBinder;)Ljava/lang/String;
        //     2527: astore 206
        //     2529: aload_3
        //     2530: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2533: aload_3
        //     2534: aload 206
        //     2536: invokevirtual 248	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     2539: iconst_1
        //     2540: istore 6
        //     2542: goto -1907 -> 635
        //     2545: aconst_null
        //     2546: astore 206
        //     2548: goto -19 -> 2529
        //     2551: aload_2
        //     2552: ldc 25
        //     2554: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2557: aload_0
        //     2558: aload_2
        //     2559: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2562: invokevirtual 252	android/app/ActivityManagerNative:getCallingActivity	(Landroid/os/IBinder;)Landroid/content/ComponentName;
        //     2565: astore 204
        //     2567: aload_3
        //     2568: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2571: aload 204
        //     2573: aload_3
        //     2574: invokestatic 257	android/content/ComponentName:writeToParcel	(Landroid/content/ComponentName;Landroid/os/Parcel;)V
        //     2577: iconst_1
        //     2578: istore 6
        //     2580: goto -1945 -> 635
        //     2583: aload_2
        //     2584: ldc 25
        //     2586: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2589: aload_2
        //     2590: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2593: istore 197
        //     2595: aload_2
        //     2596: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2599: istore 198
        //     2601: aload_2
        //     2602: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2605: astore 199
        //     2607: aload 199
        //     2609: ifnull +79 -> 2688
        //     2612: aload 199
        //     2614: invokestatic 262	android/app/IThumbnailReceiver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IThumbnailReceiver;
        //     2617: astore 200
        //     2619: aload_0
        //     2620: iload 197
        //     2622: iload 198
        //     2624: aload 200
        //     2626: invokevirtual 266	android/app/ActivityManagerNative:getTasks	(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;
        //     2629: astore 201
        //     2631: aload_3
        //     2632: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2635: aload 201
        //     2637: ifnull +57 -> 2694
        //     2640: aload 201
        //     2642: invokeinterface 271 1 0
        //     2647: istore 202
        //     2649: aload_3
        //     2650: iload 202
        //     2652: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     2655: iconst_0
        //     2656: istore 203
        //     2658: iload 203
        //     2660: iload 202
        //     2662: if_icmpge +39 -> 2701
        //     2665: aload 201
        //     2667: iload 203
        //     2669: invokeinterface 274 2 0
        //     2674: checkcast 276	android/app/ActivityManager$RunningTaskInfo
        //     2677: aload_3
        //     2678: iconst_0
        //     2679: invokevirtual 277	android/app/ActivityManager$RunningTaskInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     2682: iinc 203 1
        //     2685: goto -27 -> 2658
        //     2688: aconst_null
        //     2689: astore 200
        //     2691: goto -72 -> 2619
        //     2694: bipush 255
        //     2696: istore 202
        //     2698: goto -49 -> 2649
        //     2701: iconst_1
        //     2702: istore 6
        //     2704: goto -2069 -> 635
        //     2707: aload_2
        //     2708: ldc 25
        //     2710: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2713: aload_0
        //     2714: aload_2
        //     2715: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2718: aload_2
        //     2719: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2722: invokevirtual 281	android/app/ActivityManagerNative:getRecentTasks	(II)Ljava/util/List;
        //     2725: astore 196
        //     2727: aload_3
        //     2728: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2731: aload_3
        //     2732: aload 196
        //     2734: invokevirtual 285	android/os/Parcel:writeTypedList	(Ljava/util/List;)V
        //     2737: iconst_1
        //     2738: istore 6
        //     2740: goto -2105 -> 635
        //     2743: aload_2
        //     2744: ldc 25
        //     2746: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2749: aload_0
        //     2750: aload_2
        //     2751: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2754: invokevirtual 289	android/app/ActivityManagerNative:getTaskThumbnails	(I)Landroid/app/ActivityManager$TaskThumbnails;
        //     2757: astore 195
        //     2759: aload_3
        //     2760: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2763: aload 195
        //     2765: ifnull +21 -> 2786
        //     2768: aload_3
        //     2769: iconst_1
        //     2770: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     2773: aload 195
        //     2775: aload_3
        //     2776: iconst_0
        //     2777: invokevirtual 292	android/app/ActivityManager$TaskThumbnails:writeToParcel	(Landroid/os/Parcel;I)V
        //     2780: iconst_1
        //     2781: istore 6
        //     2783: goto -2148 -> 635
        //     2786: aload_3
        //     2787: iconst_0
        //     2788: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     2791: goto -11 -> 2780
        //     2794: aload_2
        //     2795: ldc 25
        //     2797: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2800: aload_0
        //     2801: aload_2
        //     2802: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2805: aload_2
        //     2806: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2809: invokevirtual 295	android/app/ActivityManagerNative:getServices	(II)Ljava/util/List;
        //     2812: astore 192
        //     2814: aload_3
        //     2815: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2818: aload 192
        //     2820: ifnull +51 -> 2871
        //     2823: aload 192
        //     2825: invokeinterface 271 1 0
        //     2830: istore 193
        //     2832: aload_3
        //     2833: iload 193
        //     2835: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     2838: iconst_0
        //     2839: istore 194
        //     2841: iload 194
        //     2843: iload 193
        //     2845: if_icmpge +33 -> 2878
        //     2848: aload 192
        //     2850: iload 194
        //     2852: invokeinterface 274 2 0
        //     2857: checkcast 297	android/app/ActivityManager$RunningServiceInfo
        //     2860: aload_3
        //     2861: iconst_0
        //     2862: invokevirtual 298	android/app/ActivityManager$RunningServiceInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     2865: iinc 194 1
        //     2868: goto -27 -> 2841
        //     2871: bipush 255
        //     2873: istore 193
        //     2875: goto -43 -> 2832
        //     2878: iconst_1
        //     2879: istore 6
        //     2881: goto -2246 -> 635
        //     2884: aload_2
        //     2885: ldc 25
        //     2887: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2890: aload_0
        //     2891: invokevirtual 302	android/app/ActivityManagerNative:getProcessesInErrorState	()Ljava/util/List;
        //     2894: astore 191
        //     2896: aload_3
        //     2897: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2900: aload_3
        //     2901: aload 191
        //     2903: invokevirtual 285	android/os/Parcel:writeTypedList	(Ljava/util/List;)V
        //     2906: iconst_1
        //     2907: istore 6
        //     2909: goto -2274 -> 635
        //     2912: aload_2
        //     2913: ldc 25
        //     2915: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2918: aload_0
        //     2919: invokevirtual 305	android/app/ActivityManagerNative:getRunningAppProcesses	()Ljava/util/List;
        //     2922: astore 190
        //     2924: aload_3
        //     2925: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2928: aload_3
        //     2929: aload 190
        //     2931: invokevirtual 285	android/os/Parcel:writeTypedList	(Ljava/util/List;)V
        //     2934: iconst_1
        //     2935: istore 6
        //     2937: goto -2302 -> 635
        //     2940: aload_2
        //     2941: ldc 25
        //     2943: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2946: aload_0
        //     2947: invokevirtual 308	android/app/ActivityManagerNative:getRunningExternalApplications	()Ljava/util/List;
        //     2950: astore 189
        //     2952: aload_3
        //     2953: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     2956: aload_3
        //     2957: aload 189
        //     2959: invokevirtual 285	android/os/Parcel:writeTypedList	(Ljava/util/List;)V
        //     2962: iconst_1
        //     2963: istore 6
        //     2965: goto -2330 -> 635
        //     2968: aload_2
        //     2969: ldc 25
        //     2971: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2974: aload_2
        //     2975: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2978: istore 186
        //     2980: aload_2
        //     2981: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2984: istore 187
        //     2986: aload_2
        //     2987: invokevirtual 119	android/os/Parcel:readInt	()I
        //     2990: ifeq +37 -> 3027
        //     2993: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     2996: aload_2
        //     2997: invokeinterface 112 2 0
        //     3002: checkcast 125	android/os/Bundle
        //     3005: astore 188
        //     3007: aload_0
        //     3008: iload 186
        //     3010: iload 187
        //     3012: aload 188
        //     3014: invokevirtual 312	android/app/ActivityManagerNative:moveTaskToFront	(IILandroid/os/Bundle;)V
        //     3017: aload_3
        //     3018: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3021: iconst_1
        //     3022: istore 6
        //     3024: goto -2389 -> 635
        //     3027: aconst_null
        //     3028: astore 188
        //     3030: goto -23 -> 3007
        //     3033: aload_2
        //     3034: ldc 25
        //     3036: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3039: aload_0
        //     3040: aload_2
        //     3041: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3044: invokevirtual 315	android/app/ActivityManagerNative:moveTaskToBack	(I)V
        //     3047: aload_3
        //     3048: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3051: iconst_1
        //     3052: istore 6
        //     3054: goto -2419 -> 635
        //     3057: aload_2
        //     3058: ldc 25
        //     3060: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3063: aload_2
        //     3064: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3067: astore 182
        //     3069: aload_2
        //     3070: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3073: ifeq +40 -> 3113
        //     3076: iconst_1
        //     3077: istore 183
        //     3079: aload_0
        //     3080: aload 182
        //     3082: iload 183
        //     3084: invokevirtual 319	android/app/ActivityManagerNative:moveActivityTaskToBack	(Landroid/os/IBinder;Z)Z
        //     3087: istore 184
        //     3089: aload_3
        //     3090: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3093: iload 184
        //     3095: ifeq +24 -> 3119
        //     3098: iconst_1
        //     3099: istore 185
        //     3101: aload_3
        //     3102: iload 185
        //     3104: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3107: iconst_1
        //     3108: istore 6
        //     3110: goto -2475 -> 635
        //     3113: iconst_0
        //     3114: istore 183
        //     3116: goto -37 -> 3079
        //     3119: iconst_0
        //     3120: istore 185
        //     3122: goto -21 -> 3101
        //     3125: aload_2
        //     3126: ldc 25
        //     3128: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3131: aload_0
        //     3132: aload_2
        //     3133: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3136: invokevirtual 322	android/app/ActivityManagerNative:moveTaskBackwards	(I)V
        //     3139: aload_3
        //     3140: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3143: iconst_1
        //     3144: istore 6
        //     3146: goto -2511 -> 635
        //     3149: aload_2
        //     3150: ldc 25
        //     3152: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3155: aload_2
        //     3156: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3159: astore 179
        //     3161: aload_2
        //     3162: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3165: ifeq +37 -> 3202
        //     3168: iconst_1
        //     3169: istore 180
        //     3171: aload 179
        //     3173: ifnull +35 -> 3208
        //     3176: aload_0
        //     3177: aload 179
        //     3179: iload 180
        //     3181: invokevirtual 326	android/app/ActivityManagerNative:getTaskForActivity	(Landroid/os/IBinder;Z)I
        //     3184: istore 181
        //     3186: aload_3
        //     3187: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3190: aload_3
        //     3191: iload 181
        //     3193: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3196: iconst_1
        //     3197: istore 6
        //     3199: goto -2564 -> 635
        //     3202: iconst_0
        //     3203: istore 180
        //     3205: goto -34 -> 3171
        //     3208: bipush 255
        //     3210: istore 181
        //     3212: goto -26 -> 3186
        //     3215: aload_2
        //     3216: ldc 25
        //     3218: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3221: aload_2
        //     3222: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3225: astore 176
        //     3227: aload_2
        //     3228: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3231: ifeq +51 -> 3282
        //     3234: getstatic 224	android/graphics/Bitmap:CREATOR	Landroid/os/Parcelable$Creator;
        //     3237: aload_2
        //     3238: invokeinterface 112 2 0
        //     3243: checkcast 223	android/graphics/Bitmap
        //     3246: astore 177
        //     3248: getstatic 229	android/text/TextUtils:CHAR_SEQUENCE_CREATOR	Landroid/os/Parcelable$Creator;
        //     3251: aload_2
        //     3252: invokeinterface 112 2 0
        //     3257: checkcast 231	java/lang/CharSequence
        //     3260: astore 178
        //     3262: aload_0
        //     3263: aload 176
        //     3265: aload 177
        //     3267: aload 178
        //     3269: invokevirtual 330	android/app/ActivityManagerNative:reportThumbnail	(Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
        //     3272: aload_3
        //     3273: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3276: iconst_1
        //     3277: istore 6
        //     3279: goto -2644 -> 635
        //     3282: aconst_null
        //     3283: astore 177
        //     3285: goto -37 -> 3248
        //     3288: aload_2
        //     3289: ldc 25
        //     3291: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3294: aload_2
        //     3295: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3298: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     3301: astore 172
        //     3303: aload_2
        //     3304: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     3307: astore 173
        //     3309: aload_2
        //     3310: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3313: ifeq +45 -> 3358
        //     3316: iconst_1
        //     3317: istore 174
        //     3319: aload_0
        //     3320: aload 172
        //     3322: aload 173
        //     3324: iload 174
        //     3326: invokevirtual 334	android/app/ActivityManagerNative:getContentProvider	(Landroid/app/IApplicationThread;Ljava/lang/String;Z)Landroid/app/IActivityManager$ContentProviderHolder;
        //     3329: astore 175
        //     3331: aload_3
        //     3332: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3335: aload 175
        //     3337: ifnull +27 -> 3364
        //     3340: aload_3
        //     3341: iconst_1
        //     3342: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3345: aload 175
        //     3347: aload_3
        //     3348: iconst_0
        //     3349: invokevirtual 337	android/app/IActivityManager$ContentProviderHolder:writeToParcel	(Landroid/os/Parcel;I)V
        //     3352: iconst_1
        //     3353: istore 6
        //     3355: goto -2720 -> 635
        //     3358: iconst_0
        //     3359: istore 174
        //     3361: goto -42 -> 3319
        //     3364: aload_3
        //     3365: iconst_0
        //     3366: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3369: goto -17 -> 3352
        //     3372: aload_2
        //     3373: ldc 25
        //     3375: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3378: aload_0
        //     3379: aload_2
        //     3380: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     3383: aload_2
        //     3384: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3387: invokevirtual 341	android/app/ActivityManagerNative:getContentProviderExternal	(Ljava/lang/String;Landroid/os/IBinder;)Landroid/app/IActivityManager$ContentProviderHolder;
        //     3390: astore 171
        //     3392: aload_3
        //     3393: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3396: aload 171
        //     3398: ifnull +21 -> 3419
        //     3401: aload_3
        //     3402: iconst_1
        //     3403: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3406: aload 171
        //     3408: aload_3
        //     3409: iconst_0
        //     3410: invokevirtual 337	android/app/IActivityManager$ContentProviderHolder:writeToParcel	(Landroid/os/Parcel;I)V
        //     3413: iconst_1
        //     3414: istore 6
        //     3416: goto -2781 -> 635
        //     3419: aload_3
        //     3420: iconst_0
        //     3421: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3424: goto -11 -> 3413
        //     3427: aload_2
        //     3428: ldc 25
        //     3430: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3433: aload_0
        //     3434: aload_2
        //     3435: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3438: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     3441: aload_2
        //     3442: getstatic 342	android/app/IActivityManager$ContentProviderHolder:CREATOR	Landroid/os/Parcelable$Creator;
        //     3445: invokevirtual 346	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     3448: invokevirtual 350	android/app/ActivityManagerNative:publishContentProviders	(Landroid/app/IApplicationThread;Ljava/util/List;)V
        //     3451: aload_3
        //     3452: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3455: iconst_1
        //     3456: istore 6
        //     3458: goto -2823 -> 635
        //     3461: aload_2
        //     3462: ldc 25
        //     3464: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3467: aload_0
        //     3468: aload_2
        //     3469: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3472: aload_2
        //     3473: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3476: aload_2
        //     3477: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3480: invokevirtual 354	android/app/ActivityManagerNative:refContentProvider	(Landroid/os/IBinder;II)Z
        //     3483: istore 169
        //     3485: aload_3
        //     3486: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3489: iload 169
        //     3491: ifeq +18 -> 3509
        //     3494: iconst_1
        //     3495: istore 170
        //     3497: aload_3
        //     3498: iload 170
        //     3500: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3503: iconst_1
        //     3504: istore 6
        //     3506: goto -2871 -> 635
        //     3509: iconst_0
        //     3510: istore 170
        //     3512: goto -15 -> 3497
        //     3515: aload_2
        //     3516: ldc 25
        //     3518: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3521: aload_0
        //     3522: aload_2
        //     3523: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3526: invokevirtual 357	android/app/ActivityManagerNative:unstableProviderDied	(Landroid/os/IBinder;)V
        //     3529: aload_3
        //     3530: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3533: iconst_1
        //     3534: istore 6
        //     3536: goto -2901 -> 635
        //     3539: aload_2
        //     3540: ldc 25
        //     3542: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3545: aload_2
        //     3546: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3549: astore 167
        //     3551: aload_2
        //     3552: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3555: ifeq +24 -> 3579
        //     3558: iconst_1
        //     3559: istore 168
        //     3561: aload_0
        //     3562: aload 167
        //     3564: iload 168
        //     3566: invokevirtual 361	android/app/ActivityManagerNative:removeContentProvider	(Landroid/os/IBinder;Z)V
        //     3569: aload_3
        //     3570: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3573: iconst_1
        //     3574: istore 6
        //     3576: goto -2941 -> 635
        //     3579: iconst_0
        //     3580: istore 168
        //     3582: goto -21 -> 3561
        //     3585: aload_2
        //     3586: ldc 25
        //     3588: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3591: aload_0
        //     3592: aload_2
        //     3593: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     3596: aload_2
        //     3597: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3600: invokevirtual 365	android/app/ActivityManagerNative:removeContentProviderExternal	(Ljava/lang/String;Landroid/os/IBinder;)V
        //     3603: aload_3
        //     3604: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3607: iconst_1
        //     3608: istore 6
        //     3610: goto -2975 -> 635
        //     3613: aload_2
        //     3614: ldc 25
        //     3616: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3619: aload_0
        //     3620: getstatic 366	android/content/ComponentName:CREATOR	Landroid/os/Parcelable$Creator;
        //     3623: aload_2
        //     3624: invokeinterface 112 2 0
        //     3629: checkcast 254	android/content/ComponentName
        //     3632: invokevirtual 370	android/app/ActivityManagerNative:getRunningServiceControlPanel	(Landroid/content/ComponentName;)Landroid/app/PendingIntent;
        //     3635: astore 166
        //     3637: aload_3
        //     3638: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3641: aload 166
        //     3643: aload_3
        //     3644: invokestatic 374	android/app/PendingIntent:writePendingIntentOrNullToParcel	(Landroid/app/PendingIntent;Landroid/os/Parcel;)V
        //     3647: iconst_1
        //     3648: istore 6
        //     3650: goto -3015 -> 635
        //     3653: aload_2
        //     3654: ldc 25
        //     3656: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3659: aload_0
        //     3660: aload_2
        //     3661: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3664: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     3667: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     3670: aload_2
        //     3671: invokeinterface 112 2 0
        //     3676: checkcast 102	android/content/Intent
        //     3679: aload_2
        //     3680: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     3683: invokevirtual 378	android/app/ActivityManagerNative:startService	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/ComponentName;
        //     3686: astore 165
        //     3688: aload_3
        //     3689: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3692: aload 165
        //     3694: aload_3
        //     3695: invokestatic 257	android/content/ComponentName:writeToParcel	(Landroid/content/ComponentName;Landroid/os/Parcel;)V
        //     3698: iconst_1
        //     3699: istore 6
        //     3701: goto -3066 -> 635
        //     3704: aload_2
        //     3705: ldc 25
        //     3707: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3710: aload_0
        //     3711: aload_2
        //     3712: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3715: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     3718: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     3721: aload_2
        //     3722: invokeinterface 112 2 0
        //     3727: checkcast 102	android/content/Intent
        //     3730: aload_2
        //     3731: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     3734: invokevirtual 382	android/app/ActivityManagerNative:stopService	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;)I
        //     3737: istore 164
        //     3739: aload_3
        //     3740: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3743: aload_3
        //     3744: iload 164
        //     3746: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3749: iconst_1
        //     3750: istore 6
        //     3752: goto -3117 -> 635
        //     3755: aload_2
        //     3756: ldc 25
        //     3758: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3761: aload_0
        //     3762: aload_2
        //     3763: invokestatic 386	android/content/ComponentName:readFromParcel	(Landroid/os/Parcel;)Landroid/content/ComponentName;
        //     3766: aload_2
        //     3767: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3770: aload_2
        //     3771: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3774: invokevirtual 390	android/app/ActivityManagerNative:stopServiceToken	(Landroid/content/ComponentName;Landroid/os/IBinder;I)Z
        //     3777: istore 162
        //     3779: aload_3
        //     3780: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3783: iload 162
        //     3785: ifeq +18 -> 3803
        //     3788: iconst_1
        //     3789: istore 163
        //     3791: aload_3
        //     3792: iload 163
        //     3794: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3797: iconst_1
        //     3798: istore 6
        //     3800: goto -3165 -> 635
        //     3803: iconst_0
        //     3804: istore 163
        //     3806: goto -15 -> 3791
        //     3809: aload_2
        //     3810: ldc 25
        //     3812: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3815: aload_2
        //     3816: invokestatic 386	android/content/ComponentName:readFromParcel	(Landroid/os/Parcel;)Landroid/content/ComponentName;
        //     3819: astore 157
        //     3821: aload_2
        //     3822: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3825: astore 158
        //     3827: aload_2
        //     3828: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3831: istore 159
        //     3833: aconst_null
        //     3834: astore 160
        //     3836: aload_2
        //     3837: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3840: ifeq +17 -> 3857
        //     3843: getstatic 393	android/app/Notification:CREATOR	Landroid/os/Parcelable$Creator;
        //     3846: aload_2
        //     3847: invokeinterface 112 2 0
        //     3852: checkcast 392	android/app/Notification
        //     3855: astore 160
        //     3857: aload_2
        //     3858: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3861: ifeq +30 -> 3891
        //     3864: iconst_1
        //     3865: istore 161
        //     3867: aload_0
        //     3868: aload 157
        //     3870: aload 158
        //     3872: iload 159
        //     3874: aload 160
        //     3876: iload 161
        //     3878: invokevirtual 397	android/app/ActivityManagerNative:setServiceForeground	(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V
        //     3881: aload_3
        //     3882: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3885: iconst_1
        //     3886: istore 6
        //     3888: goto -3253 -> 635
        //     3891: iconst_0
        //     3892: istore 161
        //     3894: goto -27 -> 3867
        //     3897: aload_2
        //     3898: ldc 25
        //     3900: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     3903: aload_2
        //     3904: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3907: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     3910: astore 149
        //     3912: aload_2
        //     3913: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3916: astore 150
        //     3918: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     3921: aload_2
        //     3922: invokeinterface 112 2 0
        //     3927: checkcast 102	android/content/Intent
        //     3930: astore 151
        //     3932: aload_2
        //     3933: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     3936: astore 152
        //     3938: aload_2
        //     3939: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     3942: astore 153
        //     3944: aload_2
        //     3945: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3948: istore 154
        //     3950: aload_2
        //     3951: invokevirtual 119	android/os/Parcel:readInt	()I
        //     3954: istore 155
        //     3956: aload_0
        //     3957: aload 149
        //     3959: aload 150
        //     3961: aload 151
        //     3963: aload 152
        //     3965: aload 153
        //     3967: invokestatic 402	android/app/IServiceConnection$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IServiceConnection;
        //     3970: iload 154
        //     3972: iload 155
        //     3974: invokevirtual 406	android/app/ActivityManagerNative:bindService	(Landroid/app/IApplicationThread;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/app/IServiceConnection;II)I
        //     3977: istore 156
        //     3979: aload_3
        //     3980: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     3983: aload_3
        //     3984: iload 156
        //     3986: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     3989: iconst_1
        //     3990: istore 6
        //     3992: goto -3357 -> 635
        //     3995: aload_2
        //     3996: ldc 25
        //     3998: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4001: aload_0
        //     4002: aload_2
        //     4003: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4006: invokestatic 402	android/app/IServiceConnection$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IServiceConnection;
        //     4009: invokevirtual 410	android/app/ActivityManagerNative:unbindService	(Landroid/app/IServiceConnection;)Z
        //     4012: istore 147
        //     4014: aload_3
        //     4015: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4018: iload 147
        //     4020: ifeq +18 -> 4038
        //     4023: iconst_1
        //     4024: istore 148
        //     4026: aload_3
        //     4027: iload 148
        //     4029: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4032: iconst_1
        //     4033: istore 6
        //     4035: goto -3400 -> 635
        //     4038: iconst_0
        //     4039: istore 148
        //     4041: goto -15 -> 4026
        //     4044: aload_2
        //     4045: ldc 25
        //     4047: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4050: aload_0
        //     4051: aload_2
        //     4052: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4055: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     4058: aload_2
        //     4059: invokeinterface 112 2 0
        //     4064: checkcast 102	android/content/Intent
        //     4067: aload_2
        //     4068: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4071: invokevirtual 414	android/app/ActivityManagerNative:publishService	(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/IBinder;)V
        //     4074: aload_3
        //     4075: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4078: iconst_1
        //     4079: istore 6
        //     4081: goto -3446 -> 635
        //     4084: aload_2
        //     4085: ldc 25
        //     4087: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4090: aload_2
        //     4091: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4094: astore 144
        //     4096: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     4099: aload_2
        //     4100: invokeinterface 112 2 0
        //     4105: checkcast 102	android/content/Intent
        //     4108: astore 145
        //     4110: aload_2
        //     4111: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4114: ifeq +26 -> 4140
        //     4117: iconst_1
        //     4118: istore 146
        //     4120: aload_0
        //     4121: aload 144
        //     4123: aload 145
        //     4125: iload 146
        //     4127: invokevirtual 418	android/app/ActivityManagerNative:unbindFinished	(Landroid/os/IBinder;Landroid/content/Intent;Z)V
        //     4130: aload_3
        //     4131: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4134: iconst_1
        //     4135: istore 6
        //     4137: goto -3502 -> 635
        //     4140: iconst_0
        //     4141: istore 146
        //     4143: goto -23 -> 4120
        //     4146: aload_2
        //     4147: ldc 25
        //     4149: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4152: aload_0
        //     4153: aload_2
        //     4154: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4157: aload_2
        //     4158: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4161: aload_2
        //     4162: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4165: aload_2
        //     4166: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4169: invokevirtual 422	android/app/ActivityManagerNative:serviceDoneExecuting	(Landroid/os/IBinder;III)V
        //     4172: aload_3
        //     4173: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4176: iconst_1
        //     4177: istore 6
        //     4179: goto -3544 -> 635
        //     4182: aload_2
        //     4183: ldc 25
        //     4185: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4188: aload_0
        //     4189: aload_2
        //     4190: invokestatic 386	android/content/ComponentName:readFromParcel	(Landroid/os/Parcel;)Landroid/content/ComponentName;
        //     4193: aload_2
        //     4194: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     4197: aload_2
        //     4198: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4201: aload_2
        //     4202: invokevirtual 201	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     4205: aload_2
        //     4206: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4209: invokestatic 427	android/app/IInstrumentationWatcher$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IInstrumentationWatcher;
        //     4212: invokevirtual 431	android/app/ActivityManagerNative:startInstrumentation	(Landroid/content/ComponentName;Ljava/lang/String;ILandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;)Z
        //     4215: istore 142
        //     4217: aload_3
        //     4218: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4221: iload 142
        //     4223: ifeq +18 -> 4241
        //     4226: iconst_1
        //     4227: istore 143
        //     4229: aload_3
        //     4230: iload 143
        //     4232: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4235: iconst_1
        //     4236: istore 6
        //     4238: goto -3603 -> 635
        //     4241: iconst_0
        //     4242: istore 143
        //     4244: goto -15 -> 4229
        //     4247: aload_2
        //     4248: ldc 25
        //     4250: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4253: aload_0
        //     4254: aload_2
        //     4255: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4258: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     4261: aload_2
        //     4262: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4265: aload_2
        //     4266: invokevirtual 201	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     4269: invokevirtual 435	android/app/ActivityManagerNative:finishInstrumentation	(Landroid/app/IApplicationThread;ILandroid/os/Bundle;)V
        //     4272: aload_3
        //     4273: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4276: iconst_1
        //     4277: istore 6
        //     4279: goto -3644 -> 635
        //     4282: aload_2
        //     4283: ldc 25
        //     4285: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4288: aload_0
        //     4289: invokevirtual 439	android/app/ActivityManagerNative:getConfiguration	()Landroid/content/res/Configuration;
        //     4292: astore 141
        //     4294: aload_3
        //     4295: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4298: aload 141
        //     4300: aload_3
        //     4301: iconst_0
        //     4302: invokevirtual 440	android/content/res/Configuration:writeToParcel	(Landroid/os/Parcel;I)V
        //     4305: iconst_1
        //     4306: istore 6
        //     4308: goto -3673 -> 635
        //     4311: aload_2
        //     4312: ldc 25
        //     4314: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4317: aload_0
        //     4318: getstatic 150	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     4321: aload_2
        //     4322: invokeinterface 112 2 0
        //     4327: checkcast 149	android/content/res/Configuration
        //     4330: invokevirtual 444	android/app/ActivityManagerNative:updateConfiguration	(Landroid/content/res/Configuration;)V
        //     4333: aload_3
        //     4334: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4337: iconst_1
        //     4338: istore 6
        //     4340: goto -3705 -> 635
        //     4343: aload_2
        //     4344: ldc 25
        //     4346: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4349: aload_0
        //     4350: aload_2
        //     4351: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4354: aload_2
        //     4355: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4358: invokevirtual 448	android/app/ActivityManagerNative:setRequestedOrientation	(Landroid/os/IBinder;I)V
        //     4361: aload_3
        //     4362: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4365: iconst_1
        //     4366: istore 6
        //     4368: goto -3733 -> 635
        //     4371: aload_2
        //     4372: ldc 25
        //     4374: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4377: aload_0
        //     4378: aload_2
        //     4379: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4382: invokevirtual 452	android/app/ActivityManagerNative:getRequestedOrientation	(Landroid/os/IBinder;)I
        //     4385: istore 140
        //     4387: aload_3
        //     4388: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4391: aload_3
        //     4392: iload 140
        //     4394: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4397: iconst_1
        //     4398: istore 6
        //     4400: goto -3765 -> 635
        //     4403: aload_2
        //     4404: ldc 25
        //     4406: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4409: aload_0
        //     4410: aload_2
        //     4411: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4414: invokevirtual 455	android/app/ActivityManagerNative:getActivityClassForToken	(Landroid/os/IBinder;)Landroid/content/ComponentName;
        //     4417: astore 139
        //     4419: aload_3
        //     4420: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4423: aload 139
        //     4425: aload_3
        //     4426: invokestatic 257	android/content/ComponentName:writeToParcel	(Landroid/content/ComponentName;Landroid/os/Parcel;)V
        //     4429: iconst_1
        //     4430: istore 6
        //     4432: goto -3797 -> 635
        //     4435: aload_2
        //     4436: ldc 25
        //     4438: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4441: aload_2
        //     4442: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4445: astore 138
        //     4447: aload_3
        //     4448: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4451: aload_3
        //     4452: aload_0
        //     4453: aload 138
        //     4455: invokevirtual 458	android/app/ActivityManagerNative:getPackageForToken	(Landroid/os/IBinder;)Ljava/lang/String;
        //     4458: invokevirtual 248	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     4461: iconst_1
        //     4462: istore 6
        //     4464: goto -3829 -> 635
        //     4467: aload_2
        //     4468: ldc 25
        //     4470: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4473: aload_2
        //     4474: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4477: istore 127
        //     4479: aload_2
        //     4480: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     4483: astore 128
        //     4485: aload_2
        //     4486: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4489: astore 129
        //     4491: aload_2
        //     4492: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     4495: astore 130
        //     4497: aload_2
        //     4498: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4501: istore 131
        //     4503: aload_2
        //     4504: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4507: ifeq +102 -> 4609
        //     4510: aload_2
        //     4511: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     4514: invokevirtual 462	android/os/Parcel:createTypedArray	(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
        //     4517: checkcast 464	[Landroid/content/Intent;
        //     4520: astore 132
        //     4522: aload_2
        //     4523: invokevirtual 468	android/os/Parcel:createStringArray	()[Ljava/lang/String;
        //     4526: astore 133
        //     4528: aload_2
        //     4529: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4532: istore 134
        //     4534: aload_2
        //     4535: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4538: ifeq +80 -> 4618
        //     4541: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     4544: aload_2
        //     4545: invokeinterface 112 2 0
        //     4550: checkcast 125	android/os/Bundle
        //     4553: astore 135
        //     4555: aload_0
        //     4556: iload 127
        //     4558: aload 128
        //     4560: aload 129
        //     4562: aload 130
        //     4564: iload 131
        //     4566: aload 132
        //     4568: aload 133
        //     4570: iload 134
        //     4572: aload 135
        //     4574: invokevirtual 472	android/app/ActivityManagerNative:getIntentSender	(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;)Landroid/content/IIntentSender;
        //     4577: astore 136
        //     4579: aload_3
        //     4580: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4583: aload 136
        //     4585: ifnull +39 -> 4624
        //     4588: aload 136
        //     4590: invokeinterface 476 1 0
        //     4595: astore 137
        //     4597: aload_3
        //     4598: aload 137
        //     4600: invokevirtual 479	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     4603: iconst_1
        //     4604: istore 6
        //     4606: goto -3971 -> 635
        //     4609: aconst_null
        //     4610: astore 132
        //     4612: aconst_null
        //     4613: astore 133
        //     4615: goto -87 -> 4528
        //     4618: aconst_null
        //     4619: astore 135
        //     4621: goto -66 -> 4555
        //     4624: aconst_null
        //     4625: astore 137
        //     4627: goto -30 -> 4597
        //     4630: aload_2
        //     4631: ldc 25
        //     4633: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4636: aload_0
        //     4637: aload_2
        //     4638: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4641: invokestatic 484	android/content/IIntentSender$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentSender;
        //     4644: invokevirtual 487	android/app/ActivityManagerNative:cancelIntentSender	(Landroid/content/IIntentSender;)V
        //     4647: aload_3
        //     4648: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4651: iconst_1
        //     4652: istore 6
        //     4654: goto -4019 -> 635
        //     4657: aload_2
        //     4658: ldc 25
        //     4660: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4663: aload_0
        //     4664: aload_2
        //     4665: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4668: invokestatic 484	android/content/IIntentSender$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentSender;
        //     4671: invokevirtual 491	android/app/ActivityManagerNative:getPackageForIntentSender	(Landroid/content/IIntentSender;)Ljava/lang/String;
        //     4674: astore 126
        //     4676: aload_3
        //     4677: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4680: aload_3
        //     4681: aload 126
        //     4683: invokevirtual 248	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     4686: iconst_1
        //     4687: istore 6
        //     4689: goto -4054 -> 635
        //     4692: aload_2
        //     4693: ldc 25
        //     4695: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4698: aload_0
        //     4699: aload_2
        //     4700: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4703: invokestatic 484	android/content/IIntentSender$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentSender;
        //     4706: invokevirtual 495	android/app/ActivityManagerNative:getUidForIntentSender	(Landroid/content/IIntentSender;)I
        //     4709: istore 125
        //     4711: aload_3
        //     4712: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4715: aload_3
        //     4716: iload 125
        //     4718: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4721: iconst_1
        //     4722: istore 6
        //     4724: goto -4089 -> 635
        //     4727: aload_2
        //     4728: ldc 25
        //     4730: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4733: aload_0
        //     4734: aload_2
        //     4735: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4738: invokevirtual 498	android/app/ActivityManagerNative:setProcessLimit	(I)V
        //     4741: aload_3
        //     4742: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4745: iconst_1
        //     4746: istore 6
        //     4748: goto -4113 -> 635
        //     4751: aload_2
        //     4752: ldc 25
        //     4754: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4757: aload_0
        //     4758: invokevirtual 501	android/app/ActivityManagerNative:getProcessLimit	()I
        //     4761: istore 124
        //     4763: aload_3
        //     4764: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4767: aload_3
        //     4768: iload 124
        //     4770: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4773: iconst_1
        //     4774: istore 6
        //     4776: goto -4141 -> 635
        //     4779: aload_2
        //     4780: ldc 25
        //     4782: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4785: aload_2
        //     4786: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4789: astore 121
        //     4791: aload_2
        //     4792: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4795: istore 122
        //     4797: aload_2
        //     4798: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4801: ifeq +26 -> 4827
        //     4804: iconst_1
        //     4805: istore 123
        //     4807: aload_0
        //     4808: aload 121
        //     4810: iload 122
        //     4812: iload 123
        //     4814: invokevirtual 505	android/app/ActivityManagerNative:setProcessForeground	(Landroid/os/IBinder;IZ)V
        //     4817: aload_3
        //     4818: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4821: iconst_1
        //     4822: istore 6
        //     4824: goto -4189 -> 635
        //     4827: iconst_0
        //     4828: istore 123
        //     4830: goto -23 -> 4807
        //     4833: aload_2
        //     4834: ldc 25
        //     4836: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4839: aload_0
        //     4840: aload_2
        //     4841: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     4844: aload_2
        //     4845: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4848: aload_2
        //     4849: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4852: invokevirtual 509	android/app/ActivityManagerNative:checkPermission	(Ljava/lang/String;II)I
        //     4855: istore 120
        //     4857: aload_3
        //     4858: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4861: aload_3
        //     4862: iload 120
        //     4864: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4867: iconst_1
        //     4868: istore 6
        //     4870: goto -4235 -> 635
        //     4873: aload_2
        //     4874: ldc 25
        //     4876: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4879: aload_0
        //     4880: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     4883: aload_2
        //     4884: invokeinterface 112 2 0
        //     4889: checkcast 511	android/net/Uri
        //     4892: aload_2
        //     4893: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4896: aload_2
        //     4897: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4900: aload_2
        //     4901: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4904: invokevirtual 516	android/app/ActivityManagerNative:checkUriPermission	(Landroid/net/Uri;III)I
        //     4907: istore 119
        //     4909: aload_3
        //     4910: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4913: aload_3
        //     4914: iload 119
        //     4916: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4919: iconst_1
        //     4920: istore 6
        //     4922: goto -4287 -> 635
        //     4925: aload_2
        //     4926: ldc 25
        //     4928: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4931: aload_0
        //     4932: aload_2
        //     4933: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     4936: aload_2
        //     4937: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4940: invokestatic 521	android/content/pm/IPackageDataObserver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/pm/IPackageDataObserver;
        //     4943: aload_2
        //     4944: invokevirtual 119	android/os/Parcel:readInt	()I
        //     4947: invokevirtual 525	android/app/ActivityManagerNative:clearApplicationUserData	(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z
        //     4950: istore 117
        //     4952: aload_3
        //     4953: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     4956: iload 117
        //     4958: ifeq +18 -> 4976
        //     4961: iconst_1
        //     4962: istore 118
        //     4964: aload_3
        //     4965: iload 118
        //     4967: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     4970: iconst_1
        //     4971: istore 6
        //     4973: goto -4338 -> 635
        //     4976: iconst_0
        //     4977: istore 118
        //     4979: goto -15 -> 4964
        //     4982: aload_2
        //     4983: ldc 25
        //     4985: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     4988: aload_0
        //     4989: aload_2
        //     4990: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     4993: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     4996: aload_2
        //     4997: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5000: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     5003: aload_2
        //     5004: invokeinterface 112 2 0
        //     5009: checkcast 511	android/net/Uri
        //     5012: aload_2
        //     5013: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5016: invokevirtual 529	android/app/ActivityManagerNative:grantUriPermission	(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/net/Uri;I)V
        //     5019: aload_3
        //     5020: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5023: iconst_1
        //     5024: istore 6
        //     5026: goto -4391 -> 635
        //     5029: aload_2
        //     5030: ldc 25
        //     5032: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5035: aload_0
        //     5036: aload_2
        //     5037: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5040: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     5043: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     5046: aload_2
        //     5047: invokeinterface 112 2 0
        //     5052: checkcast 511	android/net/Uri
        //     5055: aload_2
        //     5056: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5059: invokevirtual 533	android/app/ActivityManagerNative:revokeUriPermission	(Landroid/app/IApplicationThread;Landroid/net/Uri;I)V
        //     5062: aload_3
        //     5063: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5066: iconst_1
        //     5067: istore 6
        //     5069: goto -4434 -> 635
        //     5072: aload_2
        //     5073: ldc 25
        //     5075: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5078: aload_2
        //     5079: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5082: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     5085: astore 115
        //     5087: aload_2
        //     5088: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5091: ifeq +24 -> 5115
        //     5094: iconst_1
        //     5095: istore 116
        //     5097: aload_0
        //     5098: aload 115
        //     5100: iload 116
        //     5102: invokevirtual 537	android/app/ActivityManagerNative:showWaitingForDebugger	(Landroid/app/IApplicationThread;Z)V
        //     5105: aload_3
        //     5106: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5109: iconst_1
        //     5110: istore 6
        //     5112: goto -4477 -> 635
        //     5115: iconst_0
        //     5116: istore 116
        //     5118: goto -21 -> 5097
        //     5121: aload_2
        //     5122: ldc 25
        //     5124: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5127: new 539	android/app/ActivityManager$MemoryInfo
        //     5130: dup
        //     5131: invokespecial 540	android/app/ActivityManager$MemoryInfo:<init>	()V
        //     5134: astore 114
        //     5136: aload_0
        //     5137: aload 114
        //     5139: invokevirtual 544	android/app/ActivityManagerNative:getMemoryInfo	(Landroid/app/ActivityManager$MemoryInfo;)V
        //     5142: aload_3
        //     5143: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5146: aload 114
        //     5148: aload_3
        //     5149: iconst_0
        //     5150: invokevirtual 545	android/app/ActivityManager$MemoryInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     5153: iconst_1
        //     5154: istore 6
        //     5156: goto -4521 -> 635
        //     5159: aload_2
        //     5160: ldc 25
        //     5162: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5165: aload_0
        //     5166: invokevirtual 548	android/app/ActivityManagerNative:unhandledBack	()V
        //     5169: aload_3
        //     5170: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5173: iconst_1
        //     5174: istore 6
        //     5176: goto -4541 -> 635
        //     5179: aload_2
        //     5180: ldc 25
        //     5182: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5185: aload_0
        //     5186: aload_2
        //     5187: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5190: invokestatic 552	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
        //     5193: invokevirtual 556	android/app/ActivityManagerNative:openContentUri	(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
        //     5196: astore 113
        //     5198: aload_3
        //     5199: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5202: aload 113
        //     5204: ifnull +21 -> 5225
        //     5207: aload_3
        //     5208: iconst_1
        //     5209: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     5212: aload 113
        //     5214: aload_3
        //     5215: iconst_1
        //     5216: invokevirtual 559	android/os/ParcelFileDescriptor:writeToParcel	(Landroid/os/Parcel;I)V
        //     5219: iconst_1
        //     5220: istore 6
        //     5222: goto -4587 -> 635
        //     5225: aload_3
        //     5226: iconst_0
        //     5227: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     5230: goto -11 -> 5219
        //     5233: aload_2
        //     5234: ldc 25
        //     5236: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5239: aload_0
        //     5240: invokevirtual 562	android/app/ActivityManagerNative:goingToSleep	()V
        //     5243: aload_3
        //     5244: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5247: iconst_1
        //     5248: istore 6
        //     5250: goto -4615 -> 635
        //     5253: aload_2
        //     5254: ldc 25
        //     5256: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5259: aload_0
        //     5260: invokevirtual 565	android/app/ActivityManagerNative:wakingUp	()V
        //     5263: aload_3
        //     5264: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5267: iconst_1
        //     5268: istore 6
        //     5270: goto -4635 -> 635
        //     5273: aload_2
        //     5274: ldc 25
        //     5276: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5279: aload_2
        //     5280: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5283: ifeq +22 -> 5305
        //     5286: iconst_1
        //     5287: istore 112
        //     5289: aload_0
        //     5290: iload 112
        //     5292: invokevirtual 569	android/app/ActivityManagerNative:setLockScreenShown	(Z)V
        //     5295: aload_3
        //     5296: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5299: iconst_1
        //     5300: istore 6
        //     5302: goto -4667 -> 635
        //     5305: iconst_0
        //     5306: istore 112
        //     5308: goto -19 -> 5289
        //     5311: aload_2
        //     5312: ldc 25
        //     5314: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5317: aload_2
        //     5318: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5321: astore 109
        //     5323: aload_2
        //     5324: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5327: ifeq +36 -> 5363
        //     5330: iconst_1
        //     5331: istore 110
        //     5333: aload_2
        //     5334: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5337: ifeq +32 -> 5369
        //     5340: iconst_1
        //     5341: istore 111
        //     5343: aload_0
        //     5344: aload 109
        //     5346: iload 110
        //     5348: iload 111
        //     5350: invokevirtual 573	android/app/ActivityManagerNative:setDebugApp	(Ljava/lang/String;ZZ)V
        //     5353: aload_3
        //     5354: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5357: iconst_1
        //     5358: istore 6
        //     5360: goto -4725 -> 635
        //     5363: iconst_0
        //     5364: istore 110
        //     5366: goto -33 -> 5333
        //     5369: iconst_0
        //     5370: istore 111
        //     5372: goto -29 -> 5343
        //     5375: aload_2
        //     5376: ldc 25
        //     5378: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5381: aload_2
        //     5382: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5385: ifeq +22 -> 5407
        //     5388: iconst_1
        //     5389: istore 108
        //     5391: aload_0
        //     5392: iload 108
        //     5394: invokevirtual 576	android/app/ActivityManagerNative:setAlwaysFinish	(Z)V
        //     5397: aload_3
        //     5398: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5401: iconst_1
        //     5402: istore 6
        //     5404: goto -4769 -> 635
        //     5407: iconst_0
        //     5408: istore 108
        //     5410: goto -19 -> 5391
        //     5413: aload_2
        //     5414: ldc 25
        //     5416: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5419: aload_0
        //     5420: aload_2
        //     5421: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5424: invokestatic 581	android/app/IActivityController$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IActivityController;
        //     5427: invokevirtual 585	android/app/ActivityManagerNative:setActivityController	(Landroid/app/IActivityController;)V
        //     5430: iconst_1
        //     5431: istore 6
        //     5433: goto -4798 -> 635
        //     5436: aload_2
        //     5437: ldc 25
        //     5439: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5442: aload_0
        //     5443: invokevirtual 588	android/app/ActivityManagerNative:enterSafeMode	()V
        //     5446: aload_3
        //     5447: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5450: iconst_1
        //     5451: istore 6
        //     5453: goto -4818 -> 635
        //     5456: aload_2
        //     5457: ldc 25
        //     5459: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5462: aload_0
        //     5463: aload_2
        //     5464: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5467: invokestatic 484	android/content/IIntentSender$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentSender;
        //     5470: invokevirtual 589	android/app/ActivityManagerNative:noteWakeupAlarm	(Landroid/content/IIntentSender;)V
        //     5473: aload_3
        //     5474: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5477: iconst_1
        //     5478: istore 6
        //     5480: goto -4845 -> 635
        //     5483: aload_2
        //     5484: ldc 25
        //     5486: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5489: aload_2
        //     5490: invokevirtual 593	android/os/Parcel:createIntArray	()[I
        //     5493: astore 103
        //     5495: aload_2
        //     5496: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5499: astore 104
        //     5501: aload_2
        //     5502: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5505: ifeq +42 -> 5547
        //     5508: iconst_1
        //     5509: istore 105
        //     5511: aload_0
        //     5512: aload 103
        //     5514: aload 104
        //     5516: iload 105
        //     5518: invokevirtual 597	android/app/ActivityManagerNative:killPids	([ILjava/lang/String;Z)Z
        //     5521: istore 106
        //     5523: aload_3
        //     5524: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5527: iload 106
        //     5529: ifeq +24 -> 5553
        //     5532: iconst_1
        //     5533: istore 107
        //     5535: aload_3
        //     5536: iload 107
        //     5538: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     5541: iconst_1
        //     5542: istore 6
        //     5544: goto -4909 -> 635
        //     5547: iconst_0
        //     5548: istore 105
        //     5550: goto -39 -> 5511
        //     5553: iconst_0
        //     5554: istore 107
        //     5556: goto -21 -> 5535
        //     5559: aload_2
        //     5560: ldc 25
        //     5562: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5565: aload_0
        //     5566: aload_2
        //     5567: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5570: invokevirtual 601	android/app/ActivityManagerNative:killProcessesBelowForeground	(Ljava/lang/String;)Z
        //     5573: istore 101
        //     5575: aload_3
        //     5576: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5579: iload 101
        //     5581: ifeq +18 -> 5599
        //     5584: iconst_1
        //     5585: istore 102
        //     5587: aload_3
        //     5588: iload 102
        //     5590: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     5593: iconst_1
        //     5594: istore 6
        //     5596: goto -4961 -> 635
        //     5599: iconst_0
        //     5600: istore 102
        //     5602: goto -15 -> 5587
        //     5605: aload_2
        //     5606: ldc 25
        //     5608: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5611: aload_0
        //     5612: aload_2
        //     5613: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5616: aload_2
        //     5617: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5620: aload_2
        //     5621: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5624: aload_2
        //     5625: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5628: invokevirtual 605	android/app/ActivityManagerNative:startRunning	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //     5631: aload_3
        //     5632: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5635: iconst_1
        //     5636: istore 6
        //     5638: goto -5003 -> 635
        //     5641: aload_2
        //     5642: ldc 25
        //     5644: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5647: aload_2
        //     5648: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5651: astore 99
        //     5653: new 607	android/app/ApplicationErrorReport$CrashInfo
        //     5656: dup
        //     5657: aload_2
        //     5658: invokespecial 610	android/app/ApplicationErrorReport$CrashInfo:<init>	(Landroid/os/Parcel;)V
        //     5661: astore 100
        //     5663: aload_0
        //     5664: aload 99
        //     5666: aload 100
        //     5668: invokevirtual 614	android/app/ActivityManagerNative:handleApplicationCrash	(Landroid/os/IBinder;Landroid/app/ApplicationErrorReport$CrashInfo;)V
        //     5671: aload_3
        //     5672: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5675: iconst_1
        //     5676: istore 6
        //     5678: goto -5043 -> 635
        //     5681: aload_2
        //     5682: ldc 25
        //     5684: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5687: aload_2
        //     5688: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5691: astore 94
        //     5693: aload_2
        //     5694: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5697: astore 95
        //     5699: new 607	android/app/ApplicationErrorReport$CrashInfo
        //     5702: dup
        //     5703: aload_2
        //     5704: invokespecial 610	android/app/ApplicationErrorReport$CrashInfo:<init>	(Landroid/os/Parcel;)V
        //     5707: astore 96
        //     5709: aload_0
        //     5710: aload 94
        //     5712: aload 95
        //     5714: aload 96
        //     5716: invokevirtual 618	android/app/ActivityManagerNative:handleApplicationWtf	(Landroid/os/IBinder;Ljava/lang/String;Landroid/app/ApplicationErrorReport$CrashInfo;)Z
        //     5719: istore 97
        //     5721: aload_3
        //     5722: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5725: iload 97
        //     5727: ifeq +18 -> 5745
        //     5730: iconst_1
        //     5731: istore 98
        //     5733: aload_3
        //     5734: iload 98
        //     5736: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     5739: iconst_1
        //     5740: istore 6
        //     5742: goto -5107 -> 635
        //     5745: iconst_0
        //     5746: istore 98
        //     5748: goto -15 -> 5733
        //     5751: aload_2
        //     5752: ldc 25
        //     5754: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5757: aload_2
        //     5758: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     5761: astore 91
        //     5763: aload_2
        //     5764: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5767: istore 92
        //     5769: new 620	android/os/StrictMode$ViolationInfo
        //     5772: dup
        //     5773: aload_2
        //     5774: invokespecial 621	android/os/StrictMode$ViolationInfo:<init>	(Landroid/os/Parcel;)V
        //     5777: astore 93
        //     5779: aload_0
        //     5780: aload 91
        //     5782: iload 92
        //     5784: aload 93
        //     5786: invokevirtual 625	android/app/ActivityManagerNative:handleApplicationStrictModeViolation	(Landroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V
        //     5789: aload_3
        //     5790: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5793: iconst_1
        //     5794: istore 6
        //     5796: goto -5161 -> 635
        //     5799: aload_2
        //     5800: ldc 25
        //     5802: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5805: aload_0
        //     5806: aload_2
        //     5807: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5810: invokevirtual 628	android/app/ActivityManagerNative:signalPersistentProcesses	(I)V
        //     5813: aload_3
        //     5814: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5817: iconst_1
        //     5818: istore 6
        //     5820: goto -5185 -> 635
        //     5823: aload_2
        //     5824: ldc 25
        //     5826: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5829: aload_0
        //     5830: aload_2
        //     5831: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5834: invokevirtual 631	android/app/ActivityManagerNative:killBackgroundProcesses	(Ljava/lang/String;)V
        //     5837: aload_3
        //     5838: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5841: iconst_1
        //     5842: istore 6
        //     5844: goto -5209 -> 635
        //     5847: aload_2
        //     5848: ldc 25
        //     5850: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5853: aload_0
        //     5854: invokevirtual 634	android/app/ActivityManagerNative:killAllBackgroundProcesses	()V
        //     5857: aload_3
        //     5858: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5861: iconst_1
        //     5862: istore 6
        //     5864: goto -5229 -> 635
        //     5867: aload_2
        //     5868: ldc 25
        //     5870: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5873: aload_0
        //     5874: aload_2
        //     5875: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5878: invokevirtual 637	android/app/ActivityManagerNative:forceStopPackage	(Ljava/lang/String;)V
        //     5881: aload_3
        //     5882: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5885: iconst_1
        //     5886: istore 6
        //     5888: goto -5253 -> 635
        //     5891: aload_2
        //     5892: ldc 25
        //     5894: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5897: new 639	android/app/ActivityManager$RunningAppProcessInfo
        //     5900: dup
        //     5901: invokespecial 640	android/app/ActivityManager$RunningAppProcessInfo:<init>	()V
        //     5904: astore 90
        //     5906: aload_0
        //     5907: aload 90
        //     5909: invokevirtual 644	android/app/ActivityManagerNative:getMyMemoryState	(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
        //     5912: aload_3
        //     5913: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5916: aload 90
        //     5918: aload_3
        //     5919: iconst_0
        //     5920: invokevirtual 645	android/app/ActivityManager$RunningAppProcessInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     5923: iconst_1
        //     5924: istore 6
        //     5926: goto -5291 -> 635
        //     5929: aload_2
        //     5930: ldc 25
        //     5932: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5935: aload_0
        //     5936: invokevirtual 649	android/app/ActivityManagerNative:getDeviceConfigurationInfo	()Landroid/content/pm/ConfigurationInfo;
        //     5939: astore 89
        //     5941: aload_3
        //     5942: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     5945: aload 89
        //     5947: aload_3
        //     5948: iconst_0
        //     5949: invokevirtual 652	android/content/pm/ConfigurationInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     5952: iconst_1
        //     5953: istore 6
        //     5955: goto -5320 -> 635
        //     5958: aload_2
        //     5959: ldc 25
        //     5961: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     5964: aload_2
        //     5965: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5968: astore 82
        //     5970: aload_2
        //     5971: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5974: ifeq +71 -> 6045
        //     5977: iconst_1
        //     5978: istore 83
        //     5980: aload_2
        //     5981: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5984: istore 84
        //     5986: aload_2
        //     5987: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     5990: astore 85
        //     5992: aload_2
        //     5993: invokevirtual 119	android/os/Parcel:readInt	()I
        //     5996: ifeq +55 -> 6051
        //     5999: aload_2
        //     6000: invokevirtual 123	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     6003: astore 86
        //     6005: aload_0
        //     6006: aload 82
        //     6008: iload 83
        //     6010: aload 85
        //     6012: aload 86
        //     6014: iload 84
        //     6016: invokevirtual 656	android/app/ActivityManagerNative:profileControl	(Ljava/lang/String;ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Z
        //     6019: istore 87
        //     6021: aload_3
        //     6022: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6025: iload 87
        //     6027: ifeq +30 -> 6057
        //     6030: iconst_1
        //     6031: istore 88
        //     6033: aload_3
        //     6034: iload 88
        //     6036: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6039: iconst_1
        //     6040: istore 6
        //     6042: goto -5407 -> 635
        //     6045: iconst_0
        //     6046: istore 83
        //     6048: goto -68 -> 5980
        //     6051: aconst_null
        //     6052: astore 86
        //     6054: goto -49 -> 6005
        //     6057: iconst_0
        //     6058: istore 88
        //     6060: goto -27 -> 6033
        //     6063: aload_2
        //     6064: ldc 25
        //     6066: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6069: aload_0
        //     6070: aload_2
        //     6071: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6074: invokevirtual 660	android/app/ActivityManagerNative:shutdown	(I)Z
        //     6077: istore 80
        //     6079: aload_3
        //     6080: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6083: iload 80
        //     6085: ifeq +18 -> 6103
        //     6088: iconst_1
        //     6089: istore 81
        //     6091: aload_3
        //     6092: iload 81
        //     6094: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6097: iconst_1
        //     6098: istore 6
        //     6100: goto -5465 -> 635
        //     6103: iconst_0
        //     6104: istore 81
        //     6106: goto -15 -> 6091
        //     6109: aload_2
        //     6110: ldc 25
        //     6112: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6115: aload_0
        //     6116: invokevirtual 663	android/app/ActivityManagerNative:stopAppSwitches	()V
        //     6119: aload_3
        //     6120: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6123: iconst_1
        //     6124: istore 6
        //     6126: goto -5491 -> 635
        //     6129: aload_2
        //     6130: ldc 25
        //     6132: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6135: aload_0
        //     6136: invokevirtual 666	android/app/ActivityManagerNative:resumeAppSwitches	()V
        //     6139: aload_3
        //     6140: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6143: iconst_1
        //     6144: istore 6
        //     6146: goto -5511 -> 635
        //     6149: aload_2
        //     6150: ldc 25
        //     6152: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6155: aload_0
        //     6156: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     6159: aload_2
        //     6160: invokeinterface 112 2 0
        //     6165: checkcast 102	android/content/Intent
        //     6168: aload_2
        //     6169: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6172: invokevirtual 670	android/app/ActivityManagerNative:peekService	(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;
        //     6175: astore 79
        //     6177: aload_3
        //     6178: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6181: aload_3
        //     6182: aload 79
        //     6184: invokevirtual 479	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     6187: iconst_1
        //     6188: istore 6
        //     6190: goto -5555 -> 635
        //     6193: aload_2
        //     6194: ldc 25
        //     6196: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6199: aload_0
        //     6200: getstatic 673	android/content/pm/ApplicationInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     6203: aload_2
        //     6204: invokeinterface 112 2 0
        //     6209: checkcast 672	android/content/pm/ApplicationInfo
        //     6212: aload_2
        //     6213: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6216: invokevirtual 677	android/app/ActivityManagerNative:bindBackupAgent	(Landroid/content/pm/ApplicationInfo;I)Z
        //     6219: istore 77
        //     6221: aload_3
        //     6222: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6225: iload 77
        //     6227: ifeq +18 -> 6245
        //     6230: iconst_1
        //     6231: istore 78
        //     6233: aload_3
        //     6234: iload 78
        //     6236: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6239: iconst_1
        //     6240: istore 6
        //     6242: goto -5607 -> 635
        //     6245: iconst_0
        //     6246: istore 78
        //     6248: goto -15 -> 6233
        //     6251: aload_2
        //     6252: ldc 25
        //     6254: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6257: aload_0
        //     6258: aload_2
        //     6259: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6262: aload_2
        //     6263: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6266: invokevirtual 680	android/app/ActivityManagerNative:backupAgentCreated	(Ljava/lang/String;Landroid/os/IBinder;)V
        //     6269: aload_3
        //     6270: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6273: iconst_1
        //     6274: istore 6
        //     6276: goto -5641 -> 635
        //     6279: aload_2
        //     6280: ldc 25
        //     6282: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6285: aload_0
        //     6286: getstatic 673	android/content/pm/ApplicationInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     6289: aload_2
        //     6290: invokeinterface 112 2 0
        //     6295: checkcast 672	android/content/pm/ApplicationInfo
        //     6298: invokevirtual 684	android/app/ActivityManagerNative:unbindBackupAgent	(Landroid/content/pm/ApplicationInfo;)V
        //     6301: aload_3
        //     6302: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6305: iconst_1
        //     6306: istore 6
        //     6308: goto -5673 -> 635
        //     6311: aload_2
        //     6312: ldc 25
        //     6314: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6317: aload_2
        //     6318: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6321: istore 68
        //     6323: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     6326: aload_2
        //     6327: invokeinterface 112 2 0
        //     6332: checkcast 102	android/content/Intent
        //     6335: astore 69
        //     6337: aload_2
        //     6338: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6341: astore 70
        //     6343: aload_2
        //     6344: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6347: astore 71
        //     6349: aload_2
        //     6350: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6353: astore 72
        //     6355: aload_2
        //     6356: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6359: istore 73
        //     6361: aload_2
        //     6362: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6365: istore 74
        //     6367: aload_2
        //     6368: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6371: ifeq +55 -> 6426
        //     6374: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     6377: aload_2
        //     6378: invokeinterface 112 2 0
        //     6383: checkcast 125	android/os/Bundle
        //     6386: astore 75
        //     6388: aload_0
        //     6389: iload 68
        //     6391: aload 69
        //     6393: aload 70
        //     6395: aload 71
        //     6397: aload 72
        //     6399: iload 73
        //     6401: iload 74
        //     6403: aload 75
        //     6405: invokevirtual 688	android/app/ActivityManagerNative:startActivityInPackage	(ILandroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/os/Bundle;)I
        //     6408: istore 76
        //     6410: aload_3
        //     6411: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6414: aload_3
        //     6415: iload 76
        //     6417: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6420: iconst_1
        //     6421: istore 6
        //     6423: goto -5788 -> 635
        //     6426: aconst_null
        //     6427: astore 75
        //     6429: goto -41 -> 6388
        //     6432: aload_2
        //     6433: ldc 25
        //     6435: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6438: aload_0
        //     6439: aload_2
        //     6440: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6443: aload_2
        //     6444: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6447: invokevirtual 692	android/app/ActivityManagerNative:killApplicationWithUid	(Ljava/lang/String;I)V
        //     6450: aload_3
        //     6451: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6454: iconst_1
        //     6455: istore 6
        //     6457: goto -5822 -> 635
        //     6460: aload_2
        //     6461: ldc 25
        //     6463: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6466: aload_0
        //     6467: aload_2
        //     6468: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6471: invokevirtual 695	android/app/ActivityManagerNative:closeSystemDialogs	(Ljava/lang/String;)V
        //     6474: aload_3
        //     6475: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6478: iconst_1
        //     6479: istore 6
        //     6481: goto -5846 -> 635
        //     6484: aload_2
        //     6485: ldc 25
        //     6487: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6490: aload_0
        //     6491: aload_2
        //     6492: invokevirtual 593	android/os/Parcel:createIntArray	()[I
        //     6495: invokevirtual 699	android/app/ActivityManagerNative:getProcessMemoryInfo	([I)[Landroid/os/Debug$MemoryInfo;
        //     6498: astore 67
        //     6500: aload_3
        //     6501: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6504: aload_3
        //     6505: aload 67
        //     6507: iconst_1
        //     6508: invokevirtual 703	android/os/Parcel:writeTypedArray	([Landroid/os/Parcelable;I)V
        //     6511: iconst_1
        //     6512: istore 6
        //     6514: goto -5879 -> 635
        //     6517: aload_2
        //     6518: ldc 25
        //     6520: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6523: aload_0
        //     6524: aload_2
        //     6525: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6528: aload_2
        //     6529: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6532: invokevirtual 706	android/app/ActivityManagerNative:killApplicationProcess	(Ljava/lang/String;I)V
        //     6535: aload_3
        //     6536: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6539: iconst_1
        //     6540: istore 6
        //     6542: goto -5907 -> 635
        //     6545: aload_2
        //     6546: ldc 25
        //     6548: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6551: aload_0
        //     6552: aload_2
        //     6553: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6556: aload_2
        //     6557: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6560: aload_2
        //     6561: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6564: aload_2
        //     6565: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6568: invokevirtual 710	android/app/ActivityManagerNative:overridePendingTransition	(Landroid/os/IBinder;Ljava/lang/String;II)V
        //     6571: aload_3
        //     6572: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6575: iconst_1
        //     6576: istore 6
        //     6578: goto -5943 -> 635
        //     6581: aload_2
        //     6582: ldc 25
        //     6584: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6587: aload_0
        //     6588: invokevirtual 713	android/app/ActivityManagerNative:isUserAMonkey	()Z
        //     6591: istore 65
        //     6593: aload_3
        //     6594: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6597: iload 65
        //     6599: ifeq +18 -> 6617
        //     6602: iconst_1
        //     6603: istore 66
        //     6605: aload_3
        //     6606: iload 66
        //     6608: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6611: iconst_1
        //     6612: istore 6
        //     6614: goto -5979 -> 635
        //     6617: iconst_0
        //     6618: istore 66
        //     6620: goto -15 -> 6605
        //     6623: aload_2
        //     6624: ldc 25
        //     6626: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6629: aload_0
        //     6630: invokevirtual 716	android/app/ActivityManagerNative:finishHeavyWeightApp	()V
        //     6633: aload_3
        //     6634: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6637: iconst_1
        //     6638: istore 6
        //     6640: goto -6005 -> 635
        //     6643: aload_2
        //     6644: ldc 25
        //     6646: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6649: aload_0
        //     6650: aload_2
        //     6651: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6654: invokevirtual 719	android/app/ActivityManagerNative:isImmersive	(Landroid/os/IBinder;)Z
        //     6657: istore 63
        //     6659: aload_3
        //     6660: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6663: iload 63
        //     6665: ifeq +18 -> 6683
        //     6668: iconst_1
        //     6669: istore 64
        //     6671: aload_3
        //     6672: iload 64
        //     6674: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6677: iconst_1
        //     6678: istore 6
        //     6680: goto -6045 -> 635
        //     6683: iconst_0
        //     6684: istore 64
        //     6686: goto -15 -> 6671
        //     6689: aload_2
        //     6690: ldc 25
        //     6692: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6695: aload_2
        //     6696: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6699: astore 61
        //     6701: aload_2
        //     6702: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6705: iconst_1
        //     6706: if_icmpne +24 -> 6730
        //     6709: iconst_1
        //     6710: istore 62
        //     6712: aload_0
        //     6713: aload 61
        //     6715: iload 62
        //     6717: invokevirtual 722	android/app/ActivityManagerNative:setImmersive	(Landroid/os/IBinder;Z)V
        //     6720: aload_3
        //     6721: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6724: iconst_1
        //     6725: istore 6
        //     6727: goto -6092 -> 635
        //     6730: iconst_0
        //     6731: istore 62
        //     6733: goto -21 -> 6712
        //     6736: aload_2
        //     6737: ldc 25
        //     6739: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6742: aload_0
        //     6743: invokevirtual 725	android/app/ActivityManagerNative:isTopActivityImmersive	()Z
        //     6746: istore 59
        //     6748: aload_3
        //     6749: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6752: iload 59
        //     6754: ifeq +18 -> 6772
        //     6757: iconst_1
        //     6758: istore 60
        //     6760: aload_3
        //     6761: iload 60
        //     6763: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     6766: iconst_1
        //     6767: istore 6
        //     6769: goto -6134 -> 635
        //     6772: iconst_0
        //     6773: istore 60
        //     6775: goto -15 -> 6760
        //     6778: aload_2
        //     6779: ldc 25
        //     6781: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6784: aload_0
        //     6785: aload_2
        //     6786: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6789: aload_2
        //     6790: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6793: aload_2
        //     6794: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6797: aload_2
        //     6798: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6801: invokevirtual 729	android/app/ActivityManagerNative:crashApplication	(IILjava/lang/String;Ljava/lang/String;)V
        //     6804: aload_3
        //     6805: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6808: iconst_1
        //     6809: istore 6
        //     6811: goto -6176 -> 635
        //     6814: aload_2
        //     6815: ldc 25
        //     6817: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6820: aload_0
        //     6821: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     6824: aload_2
        //     6825: invokeinterface 112 2 0
        //     6830: checkcast 511	android/net/Uri
        //     6833: invokevirtual 733	android/app/ActivityManagerNative:getProviderMimeType	(Landroid/net/Uri;)Ljava/lang/String;
        //     6836: astore 58
        //     6838: aload_3
        //     6839: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6842: aload_3
        //     6843: aload 58
        //     6845: invokevirtual 248	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     6848: iconst_1
        //     6849: istore 6
        //     6851: goto -6216 -> 635
        //     6854: aload_2
        //     6855: ldc 25
        //     6857: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6860: aload_0
        //     6861: aload_2
        //     6862: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6865: invokevirtual 737	android/app/ActivityManagerNative:newUriPermissionOwner	(Ljava/lang/String;)Landroid/os/IBinder;
        //     6868: astore 57
        //     6870: aload_3
        //     6871: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6874: aload_3
        //     6875: aload 57
        //     6877: invokevirtual 479	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     6880: iconst_1
        //     6881: istore 6
        //     6883: goto -6248 -> 635
        //     6886: aload_2
        //     6887: ldc 25
        //     6889: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6892: aload_0
        //     6893: aload_2
        //     6894: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6897: aload_2
        //     6898: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6901: aload_2
        //     6902: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6905: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     6908: aload_2
        //     6909: invokeinterface 112 2 0
        //     6914: checkcast 511	android/net/Uri
        //     6917: aload_2
        //     6918: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6921: invokevirtual 741	android/app/ActivityManagerNative:grantUriPermissionFromOwner	(Landroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;I)V
        //     6924: aload_3
        //     6925: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6928: iconst_1
        //     6929: istore 6
        //     6931: goto -6296 -> 635
        //     6934: aload_2
        //     6935: ldc 25
        //     6937: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6940: aload_2
        //     6941: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     6944: astore 55
        //     6946: aload_2
        //     6947: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6950: ifeq +13 -> 6963
        //     6953: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     6956: aload_2
        //     6957: invokeinterface 112 2 0
        //     6962: pop
        //     6963: aload_0
        //     6964: aload 55
        //     6966: aconst_null
        //     6967: aload_2
        //     6968: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6971: invokevirtual 745	android/app/ActivityManagerNative:revokeUriPermissionFromOwner	(Landroid/os/IBinder;Landroid/net/Uri;I)V
        //     6974: aload_3
        //     6975: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     6978: iconst_1
        //     6979: istore 6
        //     6981: goto -6346 -> 635
        //     6984: aload_2
        //     6985: ldc 25
        //     6987: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     6990: aload_0
        //     6991: aload_2
        //     6992: invokevirtual 119	android/os/Parcel:readInt	()I
        //     6995: aload_2
        //     6996: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     6999: getstatic 512	android/net/Uri:CREATOR	Landroid/os/Parcelable$Creator;
        //     7002: aload_2
        //     7003: invokeinterface 112 2 0
        //     7008: checkcast 511	android/net/Uri
        //     7011: aload_2
        //     7012: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7015: invokevirtual 749	android/app/ActivityManagerNative:checkGrantUriPermission	(ILjava/lang/String;Landroid/net/Uri;I)I
        //     7018: istore 54
        //     7020: aload_3
        //     7021: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7024: aload_3
        //     7025: iload 54
        //     7027: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7030: iconst_1
        //     7031: istore 6
        //     7033: goto -6398 -> 635
        //     7036: aload_2
        //     7037: ldc 25
        //     7039: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7042: aload_2
        //     7043: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     7046: astore 48
        //     7048: aload_2
        //     7049: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7052: ifeq +63 -> 7115
        //     7055: iconst_1
        //     7056: istore 49
        //     7058: aload_2
        //     7059: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     7062: astore 50
        //     7064: aload_2
        //     7065: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7068: ifeq +53 -> 7121
        //     7071: aload_2
        //     7072: invokevirtual 123	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     7075: astore 51
        //     7077: aload_0
        //     7078: aload 48
        //     7080: iload 49
        //     7082: aload 50
        //     7084: aload 51
        //     7086: invokevirtual 753	android/app/ActivityManagerNative:dumpHeap	(Ljava/lang/String;ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z
        //     7089: istore 52
        //     7091: aload_3
        //     7092: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7095: iload 52
        //     7097: ifeq +30 -> 7127
        //     7100: iconst_1
        //     7101: istore 53
        //     7103: aload_3
        //     7104: iload 53
        //     7106: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7109: iconst_1
        //     7110: istore 6
        //     7112: goto -6477 -> 635
        //     7115: iconst_0
        //     7116: istore 49
        //     7118: goto -60 -> 7058
        //     7121: aconst_null
        //     7122: astore 51
        //     7124: goto -47 -> 7077
        //     7127: iconst_0
        //     7128: istore 53
        //     7130: goto -27 -> 7103
        //     7133: aload_2
        //     7134: ldc 25
        //     7136: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7139: aload_2
        //     7140: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7143: istore 42
        //     7145: aload_2
        //     7146: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     7149: invokevirtual 462	android/os/Parcel:createTypedArray	(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
        //     7152: checkcast 464	[Landroid/content/Intent;
        //     7155: astore 43
        //     7157: aload_2
        //     7158: invokevirtual 468	android/os/Parcel:createStringArray	()[Ljava/lang/String;
        //     7161: astore 44
        //     7163: aload_2
        //     7164: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7167: astore 45
        //     7169: aload_2
        //     7170: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7173: ifeq +49 -> 7222
        //     7176: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     7179: aload_2
        //     7180: invokeinterface 112 2 0
        //     7185: checkcast 125	android/os/Bundle
        //     7188: astore 46
        //     7190: aload_0
        //     7191: iload 42
        //     7193: aload 43
        //     7195: aload 44
        //     7197: aload 45
        //     7199: aload 46
        //     7201: invokevirtual 757	android/app/ActivityManagerNative:startActivitiesInPackage	(I[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)I
        //     7204: istore 47
        //     7206: aload_3
        //     7207: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7210: aload_3
        //     7211: iload 47
        //     7213: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7216: iconst_1
        //     7217: istore 6
        //     7219: goto -6584 -> 635
        //     7222: aconst_null
        //     7223: astore 46
        //     7225: goto -35 -> 7190
        //     7228: aload_2
        //     7229: ldc 25
        //     7231: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7234: aload_2
        //     7235: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7238: invokestatic 100	android/app/ApplicationThreadNative:asInterface	(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
        //     7241: astore 36
        //     7243: aload_2
        //     7244: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     7247: invokevirtual 462	android/os/Parcel:createTypedArray	(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
        //     7250: checkcast 464	[Landroid/content/Intent;
        //     7253: astore 37
        //     7255: aload_2
        //     7256: invokevirtual 468	android/os/Parcel:createStringArray	()[Ljava/lang/String;
        //     7259: astore 38
        //     7261: aload_2
        //     7262: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7265: astore 39
        //     7267: aload_2
        //     7268: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7271: ifeq +49 -> 7320
        //     7274: getstatic 126	android/os/Bundle:CREATOR	Landroid/os/Parcelable$Creator;
        //     7277: aload_2
        //     7278: invokeinterface 112 2 0
        //     7283: checkcast 125	android/os/Bundle
        //     7286: astore 40
        //     7288: aload_0
        //     7289: aload 36
        //     7291: aload 37
        //     7293: aload 38
        //     7295: aload 39
        //     7297: aload 40
        //     7299: invokevirtual 761	android/app/ActivityManagerNative:startActivities	(Landroid/app/IApplicationThread;[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)I
        //     7302: istore 41
        //     7304: aload_3
        //     7305: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7308: aload_3
        //     7309: iload 41
        //     7311: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7314: iconst_1
        //     7315: istore 6
        //     7317: goto -6682 -> 635
        //     7320: aconst_null
        //     7321: astore 40
        //     7323: goto -35 -> 7288
        //     7326: aload_2
        //     7327: ldc 25
        //     7329: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7332: aload_0
        //     7333: invokevirtual 764	android/app/ActivityManagerNative:getFrontActivityScreenCompatMode	()I
        //     7336: istore 35
        //     7338: aload_3
        //     7339: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7342: aload_3
        //     7343: iload 35
        //     7345: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7348: iconst_1
        //     7349: istore 6
        //     7351: goto -6716 -> 635
        //     7354: aload_2
        //     7355: ldc 25
        //     7357: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7360: aload_2
        //     7361: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7364: istore 34
        //     7366: aload_0
        //     7367: iload 34
        //     7369: invokevirtual 767	android/app/ActivityManagerNative:setFrontActivityScreenCompatMode	(I)V
        //     7372: aload_3
        //     7373: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7376: aload_3
        //     7377: iload 34
        //     7379: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7382: iconst_1
        //     7383: istore 6
        //     7385: goto -6750 -> 635
        //     7388: aload_2
        //     7389: ldc 25
        //     7391: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7394: aload_0
        //     7395: aload_2
        //     7396: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     7399: invokevirtual 771	android/app/ActivityManagerNative:getPackageScreenCompatMode	(Ljava/lang/String;)I
        //     7402: istore 33
        //     7404: aload_3
        //     7405: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7408: aload_3
        //     7409: iload 33
        //     7411: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7414: iconst_1
        //     7415: istore 6
        //     7417: goto -6782 -> 635
        //     7420: aload_2
        //     7421: ldc 25
        //     7423: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7426: aload_0
        //     7427: aload_2
        //     7428: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     7431: aload_2
        //     7432: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7435: invokevirtual 774	android/app/ActivityManagerNative:setPackageScreenCompatMode	(Ljava/lang/String;I)V
        //     7438: aload_3
        //     7439: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7442: iconst_1
        //     7443: istore 6
        //     7445: goto -6810 -> 635
        //     7448: aload_2
        //     7449: ldc 25
        //     7451: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7454: aload_0
        //     7455: aload_2
        //     7456: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7459: invokevirtual 777	android/app/ActivityManagerNative:switchUser	(I)Z
        //     7462: istore 31
        //     7464: aload_3
        //     7465: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7468: iload 31
        //     7470: ifeq +18 -> 7488
        //     7473: iconst_1
        //     7474: istore 32
        //     7476: aload_3
        //     7477: iload 32
        //     7479: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7482: iconst_1
        //     7483: istore 6
        //     7485: goto -6850 -> 635
        //     7488: iconst_0
        //     7489: istore 32
        //     7491: goto -15 -> 7476
        //     7494: aload_2
        //     7495: ldc 25
        //     7497: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7500: aload_0
        //     7501: invokevirtual 781	android/app/ActivityManagerNative:getCurrentUser	()Landroid/content/pm/UserInfo;
        //     7504: astore 30
        //     7506: aload_3
        //     7507: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7510: aload 30
        //     7512: aload_3
        //     7513: iconst_0
        //     7514: invokevirtual 784	android/content/pm/UserInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     7517: iconst_1
        //     7518: istore 6
        //     7520: goto -6885 -> 635
        //     7523: aload_2
        //     7524: ldc 25
        //     7526: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7529: aload_0
        //     7530: aload_2
        //     7531: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7534: aload_2
        //     7535: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7538: invokevirtual 788	android/app/ActivityManagerNative:removeSubTask	(II)Z
        //     7541: istore 28
        //     7543: aload_3
        //     7544: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7547: iload 28
        //     7549: ifeq +18 -> 7567
        //     7552: iconst_1
        //     7553: istore 29
        //     7555: aload_3
        //     7556: iload 29
        //     7558: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7561: iconst_1
        //     7562: istore 6
        //     7564: goto -6929 -> 635
        //     7567: iconst_0
        //     7568: istore 29
        //     7570: goto -15 -> 7555
        //     7573: aload_2
        //     7574: ldc 25
        //     7576: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7579: aload_0
        //     7580: aload_2
        //     7581: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7584: aload_2
        //     7585: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7588: invokevirtual 791	android/app/ActivityManagerNative:removeTask	(II)Z
        //     7591: istore 26
        //     7593: aload_3
        //     7594: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7597: iload 26
        //     7599: ifeq +18 -> 7617
        //     7602: iconst_1
        //     7603: istore 27
        //     7605: aload_3
        //     7606: iload 27
        //     7608: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7611: iconst_1
        //     7612: istore 6
        //     7614: goto -6979 -> 635
        //     7617: iconst_0
        //     7618: istore 27
        //     7620: goto -15 -> 7605
        //     7623: aload_2
        //     7624: ldc 25
        //     7626: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7629: aload_0
        //     7630: aload_2
        //     7631: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7634: invokestatic 796	android/app/IProcessObserver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IProcessObserver;
        //     7637: invokevirtual 800	android/app/ActivityManagerNative:registerProcessObserver	(Landroid/app/IProcessObserver;)V
        //     7640: iconst_1
        //     7641: istore 6
        //     7643: goto -7008 -> 635
        //     7646: aload_2
        //     7647: ldc 25
        //     7649: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7652: aload_0
        //     7653: aload_2
        //     7654: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7657: invokestatic 796	android/app/IProcessObserver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IProcessObserver;
        //     7660: invokevirtual 803	android/app/ActivityManagerNative:unregisterProcessObserver	(Landroid/app/IProcessObserver;)V
        //     7663: iconst_1
        //     7664: istore 6
        //     7666: goto -7031 -> 635
        //     7669: aload_2
        //     7670: ldc 25
        //     7672: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7675: aload_0
        //     7676: aload_2
        //     7677: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     7680: invokevirtual 806	android/app/ActivityManagerNative:getPackageAskScreenCompat	(Ljava/lang/String;)Z
        //     7683: istore 24
        //     7685: aload_3
        //     7686: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7689: iload 24
        //     7691: ifeq +18 -> 7709
        //     7694: iconst_1
        //     7695: istore 25
        //     7697: aload_3
        //     7698: iload 25
        //     7700: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7703: iconst_1
        //     7704: istore 6
        //     7706: goto -7071 -> 635
        //     7709: iconst_0
        //     7710: istore 25
        //     7712: goto -15 -> 7697
        //     7715: aload_2
        //     7716: ldc 25
        //     7718: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7721: aload_2
        //     7722: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     7725: astore 22
        //     7727: aload_2
        //     7728: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7731: ifeq +24 -> 7755
        //     7734: iconst_1
        //     7735: istore 23
        //     7737: aload_0
        //     7738: aload 22
        //     7740: iload 23
        //     7742: invokevirtual 810	android/app/ActivityManagerNative:setPackageAskScreenCompat	(Ljava/lang/String;Z)V
        //     7745: aload_3
        //     7746: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7749: iconst_1
        //     7750: istore 6
        //     7752: goto -7117 -> 635
        //     7755: iconst_0
        //     7756: istore 23
        //     7758: goto -21 -> 7737
        //     7761: aload_2
        //     7762: ldc 25
        //     7764: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7767: aload_0
        //     7768: aload_2
        //     7769: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7772: invokestatic 484	android/content/IIntentSender$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentSender;
        //     7775: invokevirtual 814	android/app/ActivityManagerNative:isIntentSenderTargetedToPackage	(Landroid/content/IIntentSender;)Z
        //     7778: istore 20
        //     7780: aload_3
        //     7781: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7784: iload 20
        //     7786: ifeq +18 -> 7804
        //     7789: iconst_1
        //     7790: istore 21
        //     7792: aload_3
        //     7793: iload 21
        //     7795: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7798: iconst_1
        //     7799: istore 6
        //     7801: goto -7166 -> 635
        //     7804: iconst_0
        //     7805: istore 21
        //     7807: goto -15 -> 7792
        //     7810: aload_2
        //     7811: ldc 25
        //     7813: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7816: aload_0
        //     7817: aload_2
        //     7818: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     7821: invokestatic 484	android/content/IIntentSender$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentSender;
        //     7824: invokevirtual 817	android/app/ActivityManagerNative:isIntentSenderAnActivity	(Landroid/content/IIntentSender;)Z
        //     7827: istore 18
        //     7829: aload_3
        //     7830: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7833: iload 18
        //     7835: ifeq +18 -> 7853
        //     7838: iconst_1
        //     7839: istore 19
        //     7841: aload_3
        //     7842: iload 19
        //     7844: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     7847: iconst_1
        //     7848: istore 6
        //     7850: goto -7215 -> 635
        //     7853: iconst_0
        //     7854: istore 19
        //     7856: goto -15 -> 7841
        //     7859: aload_2
        //     7860: ldc 25
        //     7862: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7865: aload_0
        //     7866: getstatic 150	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     7869: aload_2
        //     7870: invokeinterface 112 2 0
        //     7875: checkcast 149	android/content/res/Configuration
        //     7878: invokevirtual 820	android/app/ActivityManagerNative:updatePersistentConfiguration	(Landroid/content/res/Configuration;)V
        //     7881: aload_3
        //     7882: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7885: iconst_1
        //     7886: istore 6
        //     7888: goto -7253 -> 635
        //     7891: aload_2
        //     7892: ldc 25
        //     7894: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7897: aload_0
        //     7898: aload_2
        //     7899: invokevirtual 593	android/os/Parcel:createIntArray	()[I
        //     7902: invokevirtual 824	android/app/ActivityManagerNative:getProcessPss	([I)[J
        //     7905: astore 17
        //     7907: aload_3
        //     7908: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7911: aload_3
        //     7912: aload 17
        //     7914: invokevirtual 828	android/os/Parcel:writeLongArray	([J)V
        //     7917: iconst_1
        //     7918: istore 6
        //     7920: goto -7285 -> 635
        //     7923: aload_2
        //     7924: ldc 25
        //     7926: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7929: getstatic 229	android/text/TextUtils:CHAR_SEQUENCE_CREATOR	Landroid/os/Parcelable$Creator;
        //     7932: aload_2
        //     7933: invokeinterface 112 2 0
        //     7938: checkcast 231	java/lang/CharSequence
        //     7941: astore 15
        //     7943: aload_2
        //     7944: invokevirtual 119	android/os/Parcel:readInt	()I
        //     7947: ifeq +24 -> 7971
        //     7950: iconst_1
        //     7951: istore 16
        //     7953: aload_0
        //     7954: aload 15
        //     7956: iload 16
        //     7958: invokevirtual 832	android/app/ActivityManagerNative:showBootMessage	(Ljava/lang/CharSequence;Z)V
        //     7961: aload_3
        //     7962: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7965: iconst_1
        //     7966: istore 6
        //     7968: goto -7333 -> 635
        //     7971: iconst_0
        //     7972: istore 16
        //     7974: goto -21 -> 7953
        //     7977: aload_2
        //     7978: ldc 25
        //     7980: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     7983: aload_0
        //     7984: invokevirtual 835	android/app/ActivityManagerNative:dismissKeyguardOnNextActivity	()V
        //     7987: aload_3
        //     7988: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     7991: iconst_1
        //     7992: istore 6
        //     7994: goto -7359 -> 635
        //     7997: aload_2
        //     7998: ldc 25
        //     8000: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     8003: aload_0
        //     8004: aload_2
        //     8005: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     8008: aload_2
        //     8009: invokevirtual 116	android/os/Parcel:readString	()Ljava/lang/String;
        //     8012: invokevirtual 839	android/app/ActivityManagerNative:targetTaskAffinityMatchesActivity	(Landroid/os/IBinder;Ljava/lang/String;)Z
        //     8015: istore 13
        //     8017: aload_3
        //     8018: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     8021: iload 13
        //     8023: ifeq +18 -> 8041
        //     8026: iconst_1
        //     8027: istore 14
        //     8029: aload_3
        //     8030: iload 14
        //     8032: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     8035: iconst_1
        //     8036: istore 6
        //     8038: goto -7403 -> 635
        //     8041: iconst_0
        //     8042: istore 14
        //     8044: goto -15 -> 8029
        //     8047: aload_2
        //     8048: ldc 25
        //     8050: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     8053: aload_2
        //     8054: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     8057: astore 7
        //     8059: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     8062: aload_2
        //     8063: invokeinterface 112 2 0
        //     8068: checkcast 102	android/content/Intent
        //     8071: astore 8
        //     8073: aload_2
        //     8074: invokevirtual 119	android/os/Parcel:readInt	()I
        //     8077: istore 9
        //     8079: aconst_null
        //     8080: astore 10
        //     8082: aload_2
        //     8083: invokevirtual 119	android/os/Parcel:readInt	()I
        //     8086: ifeq +17 -> 8103
        //     8089: getstatic 106	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     8092: aload_2
        //     8093: invokeinterface 112 2 0
        //     8098: checkcast 102	android/content/Intent
        //     8101: astore 10
        //     8103: aload_0
        //     8104: aload 7
        //     8106: aload 8
        //     8108: iload 9
        //     8110: aload 10
        //     8112: invokevirtual 843	android/app/ActivityManagerNative:navigateUpTo	(Landroid/os/IBinder;Landroid/content/Intent;ILandroid/content/Intent;)Z
        //     8115: istore 11
        //     8117: aload_3
        //     8118: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     8121: iload 11
        //     8123: ifeq +18 -> 8141
        //     8126: iconst_1
        //     8127: istore 12
        //     8129: aload_3
        //     8130: iload 12
        //     8132: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     8135: iconst_1
        //     8136: istore 6
        //     8138: goto -7503 -> 635
        //     8141: iconst_0
        //     8142: istore 12
        //     8144: goto -15 -> 8129
        //     8147: aload_2
        //     8148: ldc 25
        //     8150: invokevirtual 92	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     8153: aload_0
        //     8154: aload_2
        //     8155: invokevirtual 95	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     8158: invokevirtual 846	android/app/ActivityManagerNative:getLaunchedFromUid	(Landroid/os/IBinder;)I
        //     8161: istore 5
        //     8163: aload_3
        //     8164: invokevirtual 133	android/os/Parcel:writeNoException	()V
        //     8167: aload_3
        //     8168: iload 5
        //     8170: invokevirtual 137	android/os/Parcel:writeInt	(I)V
        //     8173: iconst_1
        //     8174: istore 6
        //     8176: goto -7541 -> 635
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ActivityManagerNative
 * JD-Core Version:        0.6.2
 */