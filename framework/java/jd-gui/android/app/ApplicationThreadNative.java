package android.app;

import android.os.Binder;
import android.os.IBinder;

public abstract class ApplicationThreadNative extends Binder
    implements IApplicationThread
{
    public ApplicationThreadNative()
    {
        attachInterface(this, "android.app.IApplicationThread");
    }

    public static IApplicationThread asInterface(IBinder paramIBinder)
    {
        Object localObject;
        if (paramIBinder == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = (IApplicationThread)paramIBinder.queryLocalInterface("android.app.IApplicationThread");
            if (localObject == null)
                localObject = new ApplicationThreadProxy(paramIBinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    // ERROR //
    public boolean onTransact(int paramInt1, android.os.Parcel paramParcel1, android.os.Parcel paramParcel2, int paramInt2)
        throws android.os.RemoteException
    {
        // Byte code:
        //     0: iload_1
        //     1: tableswitch	default:+203 -> 204, 1:+217->218, 2:+203->204, 3:+285->286, 4:+335->336, 5:+419->420, 6:+461->462, 7:+488->489, 8:+795->796, 9:+822->823, 10:+872->873, 11:+978->979, 12:+1199->1200, 13:+1219->1220, 14:+1472->1473, 15:+1504->1505, 16:+1524->1525, 17:+1112->1113, 18:+1552->1553, 19:+1612->1613, 20:+1022->1023, 21:+1080->1081, 22:+1628->1629, 23:+1734->1735, 24:+1837->1838, 25:+1847->1848, 26:+697->698, 27:+377->378, 28:+1867->1868, 29:+1938->1939, 30:+1958->1959, 31:+2002->2003, 32:+2042->2043, 33:+1488->1489, 34:+2080->2081, 35:+2104->2105, 36:+2124->2125, 37:+2187->2188, 38:+1568->1569, 39:+1584->1585, 40:+2248->2249, 41:+2268->2269, 42:+2300->2301, 43:+2320->2321, 44:+2431->2432, 45:+1681->1682, 46:+2490->2491, 47:+2549->2550
        //     205: iload_1
        //     206: aload_2
        //     207: aload_3
        //     208: iload 4
        //     210: invokespecial 39	android/os/Binder:onTransact	(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //     213: istore 5
        //     215: iload 5
        //     217: ireturn
        //     218: aload_2
        //     219: ldc 12
        //     221: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     224: aload_2
        //     225: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     228: astore 117
        //     230: aload_2
        //     231: invokevirtual 52	android/os/Parcel:readInt	()I
        //     234: ifeq +40 -> 274
        //     237: iconst_1
        //     238: istore 118
        //     240: aload_2
        //     241: invokevirtual 52	android/os/Parcel:readInt	()I
        //     244: ifeq +36 -> 280
        //     247: iconst_1
        //     248: istore 119
        //     250: aload_2
        //     251: invokevirtual 52	android/os/Parcel:readInt	()I
        //     254: istore 120
        //     256: aload_0
        //     257: aload 117
        //     259: iload 118
        //     261: iload 119
        //     263: iload 120
        //     265: invokevirtual 56	android/app/ApplicationThreadNative:schedulePauseActivity	(Landroid/os/IBinder;ZZI)V
        //     268: iconst_1
        //     269: istore 5
        //     271: goto -56 -> 215
        //     274: iconst_0
        //     275: istore 118
        //     277: goto -37 -> 240
        //     280: iconst_0
        //     281: istore 119
        //     283: goto -33 -> 250
        //     286: aload_2
        //     287: ldc 12
        //     289: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     292: aload_2
        //     293: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     296: astore 114
        //     298: aload_2
        //     299: invokevirtual 52	android/os/Parcel:readInt	()I
        //     302: ifeq +28 -> 330
        //     305: iconst_1
        //     306: istore 115
        //     308: aload_2
        //     309: invokevirtual 52	android/os/Parcel:readInt	()I
        //     312: istore 116
        //     314: aload_0
        //     315: aload 114
        //     317: iload 115
        //     319: iload 116
        //     321: invokevirtual 60	android/app/ApplicationThreadNative:scheduleStopActivity	(Landroid/os/IBinder;ZI)V
        //     324: iconst_1
        //     325: istore 5
        //     327: goto -112 -> 215
        //     330: iconst_0
        //     331: istore 115
        //     333: goto -25 -> 308
        //     336: aload_2
        //     337: ldc 12
        //     339: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     342: aload_2
        //     343: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     346: astore 112
        //     348: aload_2
        //     349: invokevirtual 52	android/os/Parcel:readInt	()I
        //     352: ifeq +20 -> 372
        //     355: iconst_1
        //     356: istore 113
        //     358: aload_0
        //     359: aload 112
        //     361: iload 113
        //     363: invokevirtual 64	android/app/ApplicationThreadNative:scheduleWindowVisibility	(Landroid/os/IBinder;Z)V
        //     366: iconst_1
        //     367: istore 5
        //     369: goto -154 -> 215
        //     372: iconst_0
        //     373: istore 113
        //     375: goto -17 -> 358
        //     378: aload_2
        //     379: ldc 12
        //     381: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     384: aload_2
        //     385: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     388: astore 110
        //     390: aload_2
        //     391: invokevirtual 52	android/os/Parcel:readInt	()I
        //     394: ifeq +20 -> 414
        //     397: iconst_1
        //     398: istore 111
        //     400: aload_0
        //     401: aload 110
        //     403: iload 111
        //     405: invokevirtual 67	android/app/ApplicationThreadNative:scheduleSleeping	(Landroid/os/IBinder;Z)V
        //     408: iconst_1
        //     409: istore 5
        //     411: goto -196 -> 215
        //     414: iconst_0
        //     415: istore 111
        //     417: goto -17 -> 400
        //     420: aload_2
        //     421: ldc 12
        //     423: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     426: aload_2
        //     427: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     430: astore 108
        //     432: aload_2
        //     433: invokevirtual 52	android/os/Parcel:readInt	()I
        //     436: ifeq +20 -> 456
        //     439: iconst_1
        //     440: istore 109
        //     442: aload_0
        //     443: aload 108
        //     445: iload 109
        //     447: invokevirtual 70	android/app/ApplicationThreadNative:scheduleResumeActivity	(Landroid/os/IBinder;Z)V
        //     450: iconst_1
        //     451: istore 5
        //     453: goto -238 -> 215
        //     456: iconst_0
        //     457: istore 109
        //     459: goto -17 -> 442
        //     462: aload_2
        //     463: ldc 12
        //     465: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     468: aload_0
        //     469: aload_2
        //     470: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     473: aload_2
        //     474: getstatic 76	android/app/ResultInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     477: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     480: invokevirtual 84	android/app/ApplicationThreadNative:scheduleSendResult	(Landroid/os/IBinder;Ljava/util/List;)V
        //     483: iconst_1
        //     484: istore 5
        //     486: goto -271 -> 215
        //     489: aload_2
        //     490: ldc 12
        //     492: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     495: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     498: aload_2
        //     499: invokeinterface 93 2 0
        //     504: checkcast 86	android/content/Intent
        //     507: astore 94
        //     509: aload_2
        //     510: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     513: astore 95
        //     515: aload_2
        //     516: invokevirtual 52	android/os/Parcel:readInt	()I
        //     519: istore 96
        //     521: getstatic 96	android/content/pm/ActivityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     524: aload_2
        //     525: invokeinterface 93 2 0
        //     530: checkcast 95	android/content/pm/ActivityInfo
        //     533: astore 97
        //     535: getstatic 99	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     538: aload_2
        //     539: invokeinterface 93 2 0
        //     544: checkcast 98	android/content/res/Configuration
        //     547: astore 98
        //     549: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     552: aload_2
        //     553: invokeinterface 93 2 0
        //     558: checkcast 101	android/content/res/CompatibilityInfo
        //     561: astore 99
        //     563: aload_2
        //     564: invokevirtual 106	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     567: astore 100
        //     569: aload_2
        //     570: getstatic 76	android/app/ResultInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     573: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     576: astore 101
        //     578: aload_2
        //     579: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     582: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     585: astore 102
        //     587: aload_2
        //     588: invokevirtual 52	android/os/Parcel:readInt	()I
        //     591: ifeq +83 -> 674
        //     594: iconst_1
        //     595: istore 103
        //     597: aload_2
        //     598: invokevirtual 52	android/os/Parcel:readInt	()I
        //     601: ifeq +79 -> 680
        //     604: iconst_1
        //     605: istore 104
        //     607: aload_2
        //     608: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     611: astore 105
        //     613: aload_2
        //     614: invokevirtual 52	android/os/Parcel:readInt	()I
        //     617: ifeq +69 -> 686
        //     620: aload_2
        //     621: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     624: astore 106
        //     626: aload_2
        //     627: invokevirtual 52	android/os/Parcel:readInt	()I
        //     630: ifeq +62 -> 692
        //     633: iconst_1
        //     634: istore 107
        //     636: aload_0
        //     637: aload 94
        //     639: aload 95
        //     641: iload 96
        //     643: aload 97
        //     645: aload 98
        //     647: aload 99
        //     649: aload 100
        //     651: aload 101
        //     653: aload 102
        //     655: iload 103
        //     657: iload 104
        //     659: aload 105
        //     661: aload 106
        //     663: iload 107
        //     665: invokevirtual 118	android/app/ApplicationThreadNative:scheduleLaunchActivity	(Landroid/content/Intent;Landroid/os/IBinder;ILandroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Landroid/os/Bundle;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Landroid/os/ParcelFileDescriptor;Z)V
        //     668: iconst_1
        //     669: istore 5
        //     671: goto -456 -> 215
        //     674: iconst_0
        //     675: istore 103
        //     677: goto -80 -> 597
        //     680: iconst_0
        //     681: istore 104
        //     683: goto -76 -> 607
        //     686: aconst_null
        //     687: astore 106
        //     689: goto -63 -> 626
        //     692: iconst_0
        //     693: istore 107
        //     695: goto -59 -> 636
        //     698: aload_2
        //     699: ldc 12
        //     701: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     704: aload_2
        //     705: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     708: astore 88
        //     710: aload_2
        //     711: getstatic 76	android/app/ResultInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     714: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     717: astore 89
        //     719: aload_2
        //     720: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     723: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     726: astore 90
        //     728: aload_2
        //     729: invokevirtual 52	android/os/Parcel:readInt	()I
        //     732: istore 91
        //     734: aload_2
        //     735: invokevirtual 52	android/os/Parcel:readInt	()I
        //     738: ifeq +52 -> 790
        //     741: iconst_1
        //     742: istore 92
        //     744: aconst_null
        //     745: astore 93
        //     747: aload_2
        //     748: invokevirtual 52	android/os/Parcel:readInt	()I
        //     751: ifeq +17 -> 768
        //     754: getstatic 99	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     757: aload_2
        //     758: invokeinterface 93 2 0
        //     763: checkcast 98	android/content/res/Configuration
        //     766: astore 93
        //     768: aload_0
        //     769: aload 88
        //     771: aload 89
        //     773: aload 90
        //     775: iload 91
        //     777: iload 92
        //     779: aload 93
        //     781: invokevirtual 122	android/app/ApplicationThreadNative:scheduleRelaunchActivity	(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;)V
        //     784: iconst_1
        //     785: istore 5
        //     787: goto -572 -> 215
        //     790: iconst_0
        //     791: istore 92
        //     793: goto -49 -> 744
        //     796: aload_2
        //     797: ldc 12
        //     799: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     802: aload_0
        //     803: aload_2
        //     804: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     807: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     810: aload_2
        //     811: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     814: invokevirtual 126	android/app/ApplicationThreadNative:scheduleNewIntent	(Ljava/util/List;Landroid/os/IBinder;)V
        //     817: iconst_1
        //     818: istore 5
        //     820: goto -605 -> 215
        //     823: aload_2
        //     824: ldc 12
        //     826: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     829: aload_2
        //     830: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     833: astore 85
        //     835: aload_2
        //     836: invokevirtual 52	android/os/Parcel:readInt	()I
        //     839: ifeq +28 -> 867
        //     842: iconst_1
        //     843: istore 86
        //     845: aload_2
        //     846: invokevirtual 52	android/os/Parcel:readInt	()I
        //     849: istore 87
        //     851: aload_0
        //     852: aload 85
        //     854: iload 86
        //     856: iload 87
        //     858: invokevirtual 129	android/app/ApplicationThreadNative:scheduleDestroyActivity	(Landroid/os/IBinder;ZI)V
        //     861: iconst_1
        //     862: istore 5
        //     864: goto -649 -> 215
        //     867: iconst_0
        //     868: istore 86
        //     870: goto -25 -> 845
        //     873: aload_2
        //     874: ldc 12
        //     876: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     879: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     882: aload_2
        //     883: invokeinterface 93 2 0
        //     888: checkcast 86	android/content/Intent
        //     891: astore 78
        //     893: getstatic 96	android/content/pm/ActivityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     896: aload_2
        //     897: invokeinterface 93 2 0
        //     902: checkcast 95	android/content/pm/ActivityInfo
        //     905: astore 79
        //     907: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     910: aload_2
        //     911: invokeinterface 93 2 0
        //     916: checkcast 101	android/content/res/CompatibilityInfo
        //     919: astore 80
        //     921: aload_2
        //     922: invokevirtual 52	android/os/Parcel:readInt	()I
        //     925: istore 81
        //     927: aload_2
        //     928: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     931: astore 82
        //     933: aload_2
        //     934: invokevirtual 106	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     937: astore 83
        //     939: aload_2
        //     940: invokevirtual 52	android/os/Parcel:readInt	()I
        //     943: ifeq +30 -> 973
        //     946: iconst_1
        //     947: istore 84
        //     949: aload_0
        //     950: aload 78
        //     952: aload 79
        //     954: aload 80
        //     956: iload 81
        //     958: aload 82
        //     960: aload 83
        //     962: iload 84
        //     964: invokevirtual 133	android/app/ApplicationThreadNative:scheduleReceiver	(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/content/res/CompatibilityInfo;ILjava/lang/String;Landroid/os/Bundle;Z)V
        //     967: iconst_1
        //     968: istore 5
        //     970: goto -755 -> 215
        //     973: iconst_0
        //     974: istore 84
        //     976: goto -27 -> 949
        //     979: aload_2
        //     980: ldc 12
        //     982: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     985: aload_0
        //     986: aload_2
        //     987: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     990: getstatic 136	android/content/pm/ServiceInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     993: aload_2
        //     994: invokeinterface 93 2 0
        //     999: checkcast 135	android/content/pm/ServiceInfo
        //     1002: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     1005: aload_2
        //     1006: invokeinterface 93 2 0
        //     1011: checkcast 101	android/content/res/CompatibilityInfo
        //     1014: invokevirtual 140	android/app/ApplicationThreadNative:scheduleCreateService	(Landroid/os/IBinder;Landroid/content/pm/ServiceInfo;Landroid/content/res/CompatibilityInfo;)V
        //     1017: iconst_1
        //     1018: istore 5
        //     1020: goto -805 -> 215
        //     1023: aload_2
        //     1024: ldc 12
        //     1026: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1029: aload_2
        //     1030: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1033: astore 75
        //     1035: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1038: aload_2
        //     1039: invokeinterface 93 2 0
        //     1044: checkcast 86	android/content/Intent
        //     1047: astore 76
        //     1049: aload_2
        //     1050: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1053: ifeq +22 -> 1075
        //     1056: iconst_1
        //     1057: istore 77
        //     1059: aload_0
        //     1060: aload 75
        //     1062: aload 76
        //     1064: iload 77
        //     1066: invokevirtual 144	android/app/ApplicationThreadNative:scheduleBindService	(Landroid/os/IBinder;Landroid/content/Intent;Z)V
        //     1069: iconst_1
        //     1070: istore 5
        //     1072: goto -857 -> 215
        //     1075: iconst_0
        //     1076: istore 77
        //     1078: goto -19 -> 1059
        //     1081: aload_2
        //     1082: ldc 12
        //     1084: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1087: aload_0
        //     1088: aload_2
        //     1089: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1092: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1095: aload_2
        //     1096: invokeinterface 93 2 0
        //     1101: checkcast 86	android/content/Intent
        //     1104: invokevirtual 148	android/app/ApplicationThreadNative:scheduleUnbindService	(Landroid/os/IBinder;Landroid/content/Intent;)V
        //     1107: iconst_1
        //     1108: istore 5
        //     1110: goto -895 -> 215
        //     1113: aload_2
        //     1114: ldc 12
        //     1116: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1119: aload_2
        //     1120: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1123: astore 70
        //     1125: aload_2
        //     1126: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1129: ifeq +59 -> 1188
        //     1132: iconst_1
        //     1133: istore 71
        //     1135: aload_2
        //     1136: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1139: istore 72
        //     1141: aload_2
        //     1142: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1145: istore 73
        //     1147: aload_2
        //     1148: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1151: ifeq +43 -> 1194
        //     1154: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1157: aload_2
        //     1158: invokeinterface 93 2 0
        //     1163: checkcast 86	android/content/Intent
        //     1166: astore 74
        //     1168: aload_0
        //     1169: aload 70
        //     1171: iload 71
        //     1173: iload 72
        //     1175: iload 73
        //     1177: aload 74
        //     1179: invokevirtual 152	android/app/ApplicationThreadNative:scheduleServiceArgs	(Landroid/os/IBinder;ZIILandroid/content/Intent;)V
        //     1182: iconst_1
        //     1183: istore 5
        //     1185: goto -970 -> 215
        //     1188: iconst_0
        //     1189: istore 71
        //     1191: goto -56 -> 1135
        //     1194: aconst_null
        //     1195: astore 74
        //     1197: goto -29 -> 1168
        //     1200: aload_2
        //     1201: ldc 12
        //     1203: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1206: aload_0
        //     1207: aload_2
        //     1208: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1211: invokevirtual 155	android/app/ApplicationThreadNative:scheduleStopService	(Landroid/os/IBinder;)V
        //     1214: iconst_1
        //     1215: istore 5
        //     1217: goto -1002 -> 215
        //     1220: aload_2
        //     1221: ldc 12
        //     1223: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1226: aload_2
        //     1227: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1230: astore 53
        //     1232: getstatic 158	android/content/pm/ApplicationInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     1235: aload_2
        //     1236: invokeinterface 93 2 0
        //     1241: checkcast 157	android/content/pm/ApplicationInfo
        //     1244: astore 54
        //     1246: aload_2
        //     1247: getstatic 161	android/content/pm/ProviderInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     1250: invokevirtual 80	android/os/Parcel:createTypedArrayList	(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
        //     1253: astore 55
        //     1255: aload_2
        //     1256: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1259: ifeq +178 -> 1437
        //     1262: new 163	android/content/ComponentName
        //     1265: dup
        //     1266: aload_2
        //     1267: invokespecial 166	android/content/ComponentName:<init>	(Landroid/os/Parcel;)V
        //     1270: astore 56
        //     1272: aload_2
        //     1273: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1276: astore 57
        //     1278: aload_2
        //     1279: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1282: ifeq +161 -> 1443
        //     1285: aload_2
        //     1286: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     1289: astore 58
        //     1291: aload_2
        //     1292: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1295: ifeq +154 -> 1449
        //     1298: iconst_1
        //     1299: istore 59
        //     1301: aload_2
        //     1302: invokevirtual 106	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     1305: astore 60
        //     1307: aload_2
        //     1308: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1311: invokestatic 171	android/app/IInstrumentationWatcher$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/IInstrumentationWatcher;
        //     1314: astore 61
        //     1316: aload_2
        //     1317: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1320: istore 62
        //     1322: aload_2
        //     1323: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1326: ifeq +129 -> 1455
        //     1329: iconst_1
        //     1330: istore 63
        //     1332: aload_2
        //     1333: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1336: ifeq +125 -> 1461
        //     1339: iconst_1
        //     1340: istore 64
        //     1342: aload_2
        //     1343: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1346: ifeq +121 -> 1467
        //     1349: iconst_1
        //     1350: istore 65
        //     1352: getstatic 99	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     1355: aload_2
        //     1356: invokeinterface 93 2 0
        //     1361: checkcast 98	android/content/res/Configuration
        //     1364: astore 66
        //     1366: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     1369: aload_2
        //     1370: invokeinterface 93 2 0
        //     1375: checkcast 101	android/content/res/CompatibilityInfo
        //     1378: astore 67
        //     1380: aload_2
        //     1381: aconst_null
        //     1382: invokevirtual 175	android/os/Parcel:readHashMap	(Ljava/lang/ClassLoader;)Ljava/util/HashMap;
        //     1385: astore 68
        //     1387: aload_2
        //     1388: invokevirtual 106	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     1391: astore 69
        //     1393: aload_0
        //     1394: aload 53
        //     1396: aload 54
        //     1398: aload 55
        //     1400: aload 56
        //     1402: aload 57
        //     1404: aload 58
        //     1406: iload 59
        //     1408: aload 60
        //     1410: aload 61
        //     1412: iload 62
        //     1414: iload 63
        //     1416: iload 64
        //     1418: iload 65
        //     1420: aload 66
        //     1422: aload 67
        //     1424: aload 68
        //     1426: aload 69
        //     1428: invokevirtual 179	android/app/ApplicationThreadNative:bindApplication	(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/util/List;Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ZLandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;IZZZLandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Ljava/util/Map;Landroid/os/Bundle;)V
        //     1431: iconst_1
        //     1432: istore 5
        //     1434: goto -1219 -> 215
        //     1437: aconst_null
        //     1438: astore 56
        //     1440: goto -168 -> 1272
        //     1443: aconst_null
        //     1444: astore 58
        //     1446: goto -155 -> 1291
        //     1449: iconst_0
        //     1450: istore 59
        //     1452: goto -151 -> 1301
        //     1455: iconst_0
        //     1456: istore 63
        //     1458: goto -126 -> 1332
        //     1461: iconst_0
        //     1462: istore 64
        //     1464: goto -122 -> 1342
        //     1467: iconst_0
        //     1468: istore 65
        //     1470: goto -118 -> 1352
        //     1473: aload_2
        //     1474: ldc 12
        //     1476: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1479: aload_0
        //     1480: invokevirtual 182	android/app/ApplicationThreadNative:scheduleExit	()V
        //     1483: iconst_1
        //     1484: istore 5
        //     1486: goto -1271 -> 215
        //     1489: aload_2
        //     1490: ldc 12
        //     1492: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1495: aload_0
        //     1496: invokevirtual 185	android/app/ApplicationThreadNative:scheduleSuicide	()V
        //     1499: iconst_1
        //     1500: istore 5
        //     1502: goto -1287 -> 215
        //     1505: aload_2
        //     1506: ldc 12
        //     1508: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1511: aload_0
        //     1512: aload_2
        //     1513: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1516: invokevirtual 188	android/app/ApplicationThreadNative:requestThumbnail	(Landroid/os/IBinder;)V
        //     1519: iconst_1
        //     1520: istore 5
        //     1522: goto -1307 -> 215
        //     1525: aload_2
        //     1526: ldc 12
        //     1528: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1531: aload_0
        //     1532: getstatic 99	android/content/res/Configuration:CREATOR	Landroid/os/Parcelable$Creator;
        //     1535: aload_2
        //     1536: invokeinterface 93 2 0
        //     1541: checkcast 98	android/content/res/Configuration
        //     1544: invokevirtual 192	android/app/ApplicationThreadNative:scheduleConfigurationChanged	(Landroid/content/res/Configuration;)V
        //     1547: iconst_1
        //     1548: istore 5
        //     1550: goto -1335 -> 215
        //     1553: aload_2
        //     1554: ldc 12
        //     1556: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1559: aload_0
        //     1560: invokevirtual 195	android/app/ApplicationThreadNative:updateTimeZone	()V
        //     1563: iconst_1
        //     1564: istore 5
        //     1566: goto -1351 -> 215
        //     1569: aload_2
        //     1570: ldc 12
        //     1572: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1575: aload_0
        //     1576: invokevirtual 198	android/app/ApplicationThreadNative:clearDnsCache	()V
        //     1579: iconst_1
        //     1580: istore 5
        //     1582: goto -1367 -> 215
        //     1585: aload_2
        //     1586: ldc 12
        //     1588: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1591: aload_0
        //     1592: aload_2
        //     1593: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1596: aload_2
        //     1597: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1600: aload_2
        //     1601: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1604: invokevirtual 202	android/app/ApplicationThreadNative:setHttpProxy	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //     1607: iconst_1
        //     1608: istore 5
        //     1610: goto -1395 -> 215
        //     1613: aload_2
        //     1614: ldc 12
        //     1616: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1619: aload_0
        //     1620: invokevirtual 205	android/app/ApplicationThreadNative:processInBackground	()V
        //     1623: iconst_1
        //     1624: istore 5
        //     1626: goto -1411 -> 215
        //     1629: aload_2
        //     1630: ldc 12
        //     1632: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1635: aload_2
        //     1636: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     1639: astore 49
        //     1641: aload_2
        //     1642: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1645: astore 50
        //     1647: aload_2
        //     1648: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     1651: astore 51
        //     1653: aload 49
        //     1655: ifnull +21 -> 1676
        //     1658: aload_0
        //     1659: aload 49
        //     1661: invokevirtual 215	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     1664: aload 50
        //     1666: aload 51
        //     1668: invokevirtual 219	android/app/ApplicationThreadNative:dumpService	(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V
        //     1671: aload 49
        //     1673: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     1676: iconst_1
        //     1677: istore 5
        //     1679: goto -1464 -> 215
        //     1682: aload_2
        //     1683: ldc 12
        //     1685: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1688: aload_2
        //     1689: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     1692: astore 45
        //     1694: aload_2
        //     1695: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1698: astore 46
        //     1700: aload_2
        //     1701: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     1704: astore 47
        //     1706: aload 45
        //     1708: ifnull +21 -> 1729
        //     1711: aload_0
        //     1712: aload 45
        //     1714: invokevirtual 215	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     1717: aload 46
        //     1719: aload 47
        //     1721: invokevirtual 225	android/app/ApplicationThreadNative:dumpProvider	(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V
        //     1724: aload 45
        //     1726: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     1729: iconst_1
        //     1730: istore 5
        //     1732: goto -1517 -> 215
        //     1735: aload_2
        //     1736: ldc 12
        //     1738: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1741: aload_2
        //     1742: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1745: invokestatic 230	android/content/IIntentReceiver$Stub:asInterface	(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;
        //     1748: astore 38
        //     1750: getstatic 87	android/content/Intent:CREATOR	Landroid/os/Parcelable$Creator;
        //     1753: aload_2
        //     1754: invokeinterface 93 2 0
        //     1759: checkcast 86	android/content/Intent
        //     1762: astore 39
        //     1764: aload_2
        //     1765: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1768: istore 40
        //     1770: aload_2
        //     1771: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1774: astore 41
        //     1776: aload_2
        //     1777: invokevirtual 106	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     1780: astore 42
        //     1782: aload_2
        //     1783: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1786: ifeq +40 -> 1826
        //     1789: iconst_1
        //     1790: istore 43
        //     1792: aload_2
        //     1793: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1796: ifeq +36 -> 1832
        //     1799: iconst_1
        //     1800: istore 44
        //     1802: aload_0
        //     1803: aload 38
        //     1805: aload 39
        //     1807: iload 40
        //     1809: aload 41
        //     1811: aload 42
        //     1813: iload 43
        //     1815: iload 44
        //     1817: invokevirtual 234	android/app/ApplicationThreadNative:scheduleRegisteredReceiver	(Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZ)V
        //     1820: iconst_1
        //     1821: istore 5
        //     1823: goto -1608 -> 215
        //     1826: iconst_0
        //     1827: istore 43
        //     1829: goto -37 -> 1792
        //     1832: iconst_0
        //     1833: istore 44
        //     1835: goto -33 -> 1802
        //     1838: aload_0
        //     1839: invokevirtual 237	android/app/ApplicationThreadNative:scheduleLowMemory	()V
        //     1842: iconst_1
        //     1843: istore 5
        //     1845: goto -1630 -> 215
        //     1848: aload_2
        //     1849: ldc 12
        //     1851: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1854: aload_0
        //     1855: aload_2
        //     1856: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     1859: invokevirtual 240	android/app/ApplicationThreadNative:scheduleActivityConfigurationChanged	(Landroid/os/IBinder;)V
        //     1862: iconst_1
        //     1863: istore 5
        //     1865: goto -1650 -> 215
        //     1868: aload_2
        //     1869: ldc 12
        //     1871: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1874: aload_2
        //     1875: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1878: ifeq +49 -> 1927
        //     1881: iconst_1
        //     1882: istore 34
        //     1884: aload_2
        //     1885: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1888: istore 35
        //     1890: aload_2
        //     1891: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     1894: astore 36
        //     1896: aload_2
        //     1897: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1900: ifeq +33 -> 1933
        //     1903: aload_2
        //     1904: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     1907: astore 37
        //     1909: aload_0
        //     1910: iload 34
        //     1912: aload 36
        //     1914: aload 37
        //     1916: iload 35
        //     1918: invokevirtual 244	android/app/ApplicationThreadNative:profilerControl	(ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;I)V
        //     1921: iconst_1
        //     1922: istore 5
        //     1924: goto -1709 -> 215
        //     1927: iconst_0
        //     1928: istore 34
        //     1930: goto -46 -> 1884
        //     1933: aconst_null
        //     1934: astore 37
        //     1936: goto -27 -> 1909
        //     1939: aload_2
        //     1940: ldc 12
        //     1942: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1945: aload_0
        //     1946: aload_2
        //     1947: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1950: invokevirtual 248	android/app/ApplicationThreadNative:setSchedulingGroup	(I)V
        //     1953: iconst_1
        //     1954: istore 5
        //     1956: goto -1741 -> 215
        //     1959: aload_2
        //     1960: ldc 12
        //     1962: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     1965: aload_0
        //     1966: getstatic 158	android/content/pm/ApplicationInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     1969: aload_2
        //     1970: invokeinterface 93 2 0
        //     1975: checkcast 157	android/content/pm/ApplicationInfo
        //     1978: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     1981: aload_2
        //     1982: invokeinterface 93 2 0
        //     1987: checkcast 101	android/content/res/CompatibilityInfo
        //     1990: aload_2
        //     1991: invokevirtual 52	android/os/Parcel:readInt	()I
        //     1994: invokevirtual 252	android/app/ApplicationThreadNative:scheduleCreateBackupAgent	(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)V
        //     1997: iconst_1
        //     1998: istore 5
        //     2000: goto -1785 -> 215
        //     2003: aload_2
        //     2004: ldc 12
        //     2006: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2009: aload_0
        //     2010: getstatic 158	android/content/pm/ApplicationInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     2013: aload_2
        //     2014: invokeinterface 93 2 0
        //     2019: checkcast 157	android/content/pm/ApplicationInfo
        //     2022: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     2025: aload_2
        //     2026: invokeinterface 93 2 0
        //     2031: checkcast 101	android/content/res/CompatibilityInfo
        //     2034: invokevirtual 256	android/app/ApplicationThreadNative:scheduleDestroyBackupAgent	(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)V
        //     2037: iconst_1
        //     2038: istore 5
        //     2040: goto -1825 -> 215
        //     2043: aload_2
        //     2044: ldc 12
        //     2046: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2049: new 258	android/os/Debug$MemoryInfo
        //     2052: dup
        //     2053: invokespecial 259	android/os/Debug$MemoryInfo:<init>	()V
        //     2056: astore 33
        //     2058: aload_0
        //     2059: aload 33
        //     2061: invokevirtual 263	android/app/ApplicationThreadNative:getMemoryInfo	(Landroid/os/Debug$MemoryInfo;)V
        //     2064: aload_3
        //     2065: invokevirtual 266	android/os/Parcel:writeNoException	()V
        //     2068: aload 33
        //     2070: aload_3
        //     2071: iconst_0
        //     2072: invokevirtual 270	android/os/Debug$MemoryInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     2075: iconst_1
        //     2076: istore 5
        //     2078: goto -1863 -> 215
        //     2081: aload_2
        //     2082: ldc 12
        //     2084: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2087: aload_0
        //     2088: aload_2
        //     2089: invokevirtual 52	android/os/Parcel:readInt	()I
        //     2092: aload_2
        //     2093: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     2096: invokevirtual 274	android/app/ApplicationThreadNative:dispatchPackageBroadcast	(I[Ljava/lang/String;)V
        //     2099: iconst_1
        //     2100: istore 5
        //     2102: goto -1887 -> 215
        //     2105: aload_2
        //     2106: ldc 12
        //     2108: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2111: aload_0
        //     2112: aload_2
        //     2113: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     2116: invokevirtual 277	android/app/ApplicationThreadNative:scheduleCrash	(Ljava/lang/String;)V
        //     2119: iconst_1
        //     2120: istore 5
        //     2122: goto -1907 -> 215
        //     2125: aload_2
        //     2126: ldc 12
        //     2128: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2131: aload_2
        //     2132: invokevirtual 52	android/os/Parcel:readInt	()I
        //     2135: ifeq +41 -> 2176
        //     2138: iconst_1
        //     2139: istore 30
        //     2141: aload_2
        //     2142: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     2145: astore 31
        //     2147: aload_2
        //     2148: invokevirtual 52	android/os/Parcel:readInt	()I
        //     2151: ifeq +31 -> 2182
        //     2154: aload_2
        //     2155: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     2158: astore 32
        //     2160: aload_0
        //     2161: iload 30
        //     2163: aload 31
        //     2165: aload 32
        //     2167: invokevirtual 281	android/app/ApplicationThreadNative:dumpHeap	(ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)V
        //     2170: iconst_1
        //     2171: istore 5
        //     2173: goto -1958 -> 215
        //     2176: iconst_0
        //     2177: istore 30
        //     2179: goto -38 -> 2141
        //     2182: aconst_null
        //     2183: astore 32
        //     2185: goto -25 -> 2160
        //     2188: aload_2
        //     2189: ldc 12
        //     2191: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2194: aload_2
        //     2195: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     2198: astore 25
        //     2200: aload_2
        //     2201: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2204: astore 26
        //     2206: aload_2
        //     2207: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     2210: astore 27
        //     2212: aload_2
        //     2213: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     2216: astore 28
        //     2218: aload 25
        //     2220: ifnull +23 -> 2243
        //     2223: aload_0
        //     2224: aload 25
        //     2226: invokevirtual 215	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     2229: aload 26
        //     2231: aload 27
        //     2233: aload 28
        //     2235: invokevirtual 285	android/app/ApplicationThreadNative:dumpActivity	(Ljava/io/FileDescriptor;Landroid/os/IBinder;Ljava/lang/String;[Ljava/lang/String;)V
        //     2238: aload 25
        //     2240: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2243: iconst_1
        //     2244: istore 5
        //     2246: goto -2031 -> 215
        //     2249: aload_2
        //     2250: ldc 12
        //     2252: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2255: aload_0
        //     2256: aload_2
        //     2257: invokevirtual 106	android/os/Parcel:readBundle	()Landroid/os/Bundle;
        //     2260: invokevirtual 289	android/app/ApplicationThreadNative:setCoreSettings	(Landroid/os/Bundle;)V
        //     2263: iconst_1
        //     2264: istore 5
        //     2266: goto -2051 -> 215
        //     2269: aload_2
        //     2270: ldc 12
        //     2272: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2275: aload_0
        //     2276: aload_2
        //     2277: invokevirtual 110	android/os/Parcel:readString	()Ljava/lang/String;
        //     2280: getstatic 102	android/content/res/CompatibilityInfo:CREATOR	Landroid/os/Parcelable$Creator;
        //     2283: aload_2
        //     2284: invokeinterface 93 2 0
        //     2289: checkcast 101	android/content/res/CompatibilityInfo
        //     2292: invokevirtual 293	android/app/ApplicationThreadNative:updatePackageCompatibilityInfo	(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;)V
        //     2295: iconst_1
        //     2296: istore 5
        //     2298: goto -2083 -> 215
        //     2301: aload_2
        //     2302: ldc 12
        //     2304: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2307: aload_0
        //     2308: aload_2
        //     2309: invokevirtual 52	android/os/Parcel:readInt	()I
        //     2312: invokevirtual 296	android/app/ApplicationThreadNative:scheduleTrimMemory	(I)V
        //     2315: iconst_1
        //     2316: istore 5
        //     2318: goto -2103 -> 215
        //     2321: aload_2
        //     2322: ldc 12
        //     2324: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2327: aload_2
        //     2328: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     2331: astore 16
        //     2333: aload_2
        //     2334: invokevirtual 52	android/os/Parcel:readInt	()I
        //     2337: ifeq +73 -> 2410
        //     2340: iconst_1
        //     2341: istore 17
        //     2343: aload_2
        //     2344: invokevirtual 52	android/os/Parcel:readInt	()I
        //     2347: ifeq +69 -> 2416
        //     2350: iconst_1
        //     2351: istore 18
        //     2353: aload_2
        //     2354: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     2357: astore 19
        //     2359: aconst_null
        //     2360: astore 20
        //     2362: aload 16
        //     2364: ifnull +29 -> 2393
        //     2367: aload_0
        //     2368: aload 16
        //     2370: invokevirtual 215	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     2373: iload 17
        //     2375: iload 18
        //     2377: aload 19
        //     2379: invokevirtual 300	android/app/ApplicationThreadNative:dumpMemInfo	(Ljava/io/FileDescriptor;ZZ[Ljava/lang/String;)Landroid/os/Debug$MemoryInfo;
        //     2382: astore 23
        //     2384: aload 23
        //     2386: astore 20
        //     2388: aload 16
        //     2390: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2393: aload_3
        //     2394: invokevirtual 266	android/os/Parcel:writeNoException	()V
        //     2397: aload 20
        //     2399: aload_3
        //     2400: iconst_0
        //     2401: invokevirtual 270	android/os/Debug$MemoryInfo:writeToParcel	(Landroid/os/Parcel;I)V
        //     2404: iconst_1
        //     2405: istore 5
        //     2407: goto -2192 -> 215
        //     2410: iconst_0
        //     2411: istore 17
        //     2413: goto -70 -> 2343
        //     2416: iconst_0
        //     2417: istore 18
        //     2419: goto -66 -> 2353
        //     2422: astore 21
        //     2424: aload 16
        //     2426: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2429: aload 21
        //     2431: athrow
        //     2432: aload_2
        //     2433: ldc 12
        //     2435: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2438: aload_2
        //     2439: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     2442: astore 11
        //     2444: aload_2
        //     2445: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     2448: astore 12
        //     2450: aload 11
        //     2452: ifnull +19 -> 2471
        //     2455: aload_0
        //     2456: aload 11
        //     2458: invokevirtual 215	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     2461: aload 12
        //     2463: invokevirtual 304	android/app/ApplicationThreadNative:dumpGfxInfo	(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
        //     2466: aload 11
        //     2468: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2471: aload_3
        //     2472: invokevirtual 266	android/os/Parcel:writeNoException	()V
        //     2475: iconst_1
        //     2476: istore 5
        //     2478: goto -2263 -> 215
        //     2481: astore 13
        //     2483: aload 11
        //     2485: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2488: aload 13
        //     2490: athrow
        //     2491: aload_2
        //     2492: ldc 12
        //     2494: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2497: aload_2
        //     2498: invokevirtual 114	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     2501: astore 6
        //     2503: aload_2
        //     2504: invokevirtual 209	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     2507: astore 7
        //     2509: aload 6
        //     2511: ifnull +19 -> 2530
        //     2514: aload_0
        //     2515: aload 6
        //     2517: invokevirtual 215	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     2520: aload 7
        //     2522: invokevirtual 307	android/app/ApplicationThreadNative:dumpDbInfo	(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
        //     2525: aload 6
        //     2527: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2530: aload_3
        //     2531: invokevirtual 266	android/os/Parcel:writeNoException	()V
        //     2534: iconst_1
        //     2535: istore 5
        //     2537: goto -2322 -> 215
        //     2540: astore 8
        //     2542: aload 6
        //     2544: invokevirtual 222	android/os/ParcelFileDescriptor:close	()V
        //     2547: aload 8
        //     2549: athrow
        //     2550: aload_2
        //     2551: ldc 12
        //     2553: invokevirtual 45	android/os/Parcel:enforceInterface	(Ljava/lang/String;)V
        //     2556: aload_0
        //     2557: aload_2
        //     2558: invokevirtual 48	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //     2561: invokevirtual 310	android/app/ApplicationThreadNative:unstableProviderDied	(Landroid/os/IBinder;)V
        //     2564: aload_3
        //     2565: invokevirtual 266	android/os/Parcel:writeNoException	()V
        //     2568: iconst_1
        //     2569: istore 5
        //     2571: goto -2356 -> 215
        //     2574: astore 52
        //     2576: goto -900 -> 1676
        //     2579: astore 48
        //     2581: goto -852 -> 1729
        //     2584: astore 29
        //     2586: goto -343 -> 2243
        //     2589: astore 24
        //     2591: goto -198 -> 2393
        //     2594: astore 22
        //     2596: goto -167 -> 2429
        //     2599: astore 15
        //     2601: goto -130 -> 2471
        //     2604: astore 14
        //     2606: goto -118 -> 2488
        //     2609: astore 10
        //     2611: goto -81 -> 2530
        //     2614: astore 9
        //     2616: goto -69 -> 2547
        //
        // Exception table:
        //     from	to	target	type
        //     2367	2384	2422	finally
        //     2455	2466	2481	finally
        //     2514	2525	2540	finally
        //     1671	1676	2574	java/io/IOException
        //     1724	1729	2579	java/io/IOException
        //     2238	2243	2584	java/io/IOException
        //     2388	2393	2589	java/io/IOException
        //     2424	2429	2594	java/io/IOException
        //     2466	2471	2599	java/io/IOException
        //     2483	2488	2604	java/io/IOException
        //     2525	2530	2609	java/io/IOException
        //     2542	2547	2614	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ApplicationThreadNative
 * JD-Core Version:        0.6.2
 */