package android.app;

import android.content.Intent;
import android.util.AttributeSet;
import android.util.Xml;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AliasActivity extends Activity
{
    public final String ALIAS_META_DATA = "android.app.alias";

    private Intent parseAlias(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        AttributeSet localAttributeSet = Xml.asAttributeSet(paramXmlPullParser);
        Object localObject = null;
        int i;
        do
            i = paramXmlPullParser.next();
        while ((i != 1) && (i != 2));
        String str = paramXmlPullParser.getName();
        if (!"alias".equals(str))
            throw new RuntimeException("Alias meta-data must start with <alias> tag; found" + str + " at " + paramXmlPullParser.getPositionDescription());
        int j = paramXmlPullParser.getDepth();
        while (true)
        {
            int k = paramXmlPullParser.next();
            if ((k == 1) || ((k == 3) && (paramXmlPullParser.getDepth() <= j)))
                break;
            if ((k != 3) && (k != 4))
                if ("intent".equals(paramXmlPullParser.getName()))
                {
                    Intent localIntent = Intent.parseIntent(getResources(), paramXmlPullParser, localAttributeSet);
                    if (localObject == null)
                        localObject = localIntent;
                }
                else
                {
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
        }
        return localObject;
    }

    // ERROR //
    protected void onCreate(android.os.Bundle paramBundle)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokespecial 93	android/app/Activity:onCreate	(Landroid/os/Bundle;)V
        //     5: aconst_null
        //     6: astore_2
        //     7: aload_0
        //     8: invokevirtual 97	android/app/AliasActivity:getPackageManager	()Landroid/content/pm/PackageManager;
        //     11: aload_0
        //     12: invokevirtual 101	android/app/AliasActivity:getComponentName	()Landroid/content/ComponentName;
        //     15: sipush 128
        //     18: invokevirtual 107	android/content/pm/PackageManager:getActivityInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
        //     21: aload_0
        //     22: invokevirtual 97	android/app/AliasActivity:getPackageManager	()Landroid/content/pm/PackageManager;
        //     25: ldc 12
        //     27: invokevirtual 113	android/content/pm/ActivityInfo:loadXmlMetaData	(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
        //     30: astore_2
        //     31: aload_2
        //     32: ifnonnull +42 -> 74
        //     35: new 46	java/lang/RuntimeException
        //     38: dup
        //     39: ldc 115
        //     41: invokespecial 66	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     44: athrow
        //     45: astore 6
        //     47: new 46	java/lang/RuntimeException
        //     50: dup
        //     51: ldc 117
        //     53: aload 6
        //     55: invokespecial 120	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     58: athrow
        //     59: astore 4
        //     61: aload_2
        //     62: ifnull +9 -> 71
        //     65: aload_2
        //     66: invokeinterface 125 1 0
        //     71: aload 4
        //     73: athrow
        //     74: aload_0
        //     75: aload_2
        //     76: invokespecial 127	android/app/AliasActivity:parseAlias	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/Intent;
        //     79: astore 7
        //     81: aload 7
        //     83: ifnonnull +27 -> 110
        //     86: new 46	java/lang/RuntimeException
        //     89: dup
        //     90: ldc 129
        //     92: invokespecial 66	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     95: athrow
        //     96: astore 5
        //     98: new 46	java/lang/RuntimeException
        //     101: dup
        //     102: ldc 117
        //     104: aload 5
        //     106: invokespecial 120	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     109: athrow
        //     110: aload_0
        //     111: aload 7
        //     113: invokevirtual 133	android/app/AliasActivity:startActivity	(Landroid/content/Intent;)V
        //     116: aload_0
        //     117: invokevirtual 136	android/app/AliasActivity:finish	()V
        //     120: aload_2
        //     121: ifnull +9 -> 130
        //     124: aload_2
        //     125: invokeinterface 125 1 0
        //     130: return
        //     131: astore_3
        //     132: new 46	java/lang/RuntimeException
        //     135: dup
        //     136: ldc 117
        //     138: aload_3
        //     139: invokespecial 120	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     142: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     7	45	45	android/content/pm/PackageManager$NameNotFoundException
        //     74	96	45	android/content/pm/PackageManager$NameNotFoundException
        //     110	120	45	android/content/pm/PackageManager$NameNotFoundException
        //     7	45	59	finally
        //     47	59	59	finally
        //     74	96	59	finally
        //     98	110	59	finally
        //     110	120	59	finally
        //     132	143	59	finally
        //     7	45	96	org/xmlpull/v1/XmlPullParserException
        //     74	96	96	org/xmlpull/v1/XmlPullParserException
        //     110	120	96	org/xmlpull/v1/XmlPullParserException
        //     7	45	131	java/io/IOException
        //     74	96	131	java/io/IOException
        //     110	120	131	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.AliasActivity
 * JD-Core Version:        0.6.2
 */