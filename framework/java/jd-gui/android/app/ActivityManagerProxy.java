package android.app;

import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.IIntentSender;
import android.content.IIntentSender.Stub;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.UserInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.StrictMode.ViolationInfo;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

class ActivityManagerProxy
    implements IActivityManager
{
    private IBinder mRemote;

    public ActivityManagerProxy(IBinder paramIBinder)
    {
        this.mRemote = paramIBinder;
    }

    public void activityDestroyed(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(62, localParcel1, localParcel2, 1);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void activityIdle(IBinder paramIBinder, Configuration paramConfiguration, boolean paramBoolean)
        throws RemoteException
    {
        int i = 0;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramConfiguration != null)
        {
            localParcel1.writeInt(1);
            paramConfiguration.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            if (paramBoolean)
                i = 1;
            localParcel1.writeInt(i);
            this.mRemote.transact(18, localParcel1, localParcel2, 1);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            localParcel1.writeInt(0);
        }
    }

    public void activityPaused(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(19, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void activitySlept(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(123, localParcel1, localParcel2, 1);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void activityStopped(IBinder paramIBinder, Bundle paramBundle, Bitmap paramBitmap, CharSequence paramCharSequence)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeBundle(paramBundle);
        if (paramBitmap != null)
        {
            localParcel1.writeInt(1);
            paramBitmap.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            TextUtils.writeToParcel(paramCharSequence, localParcel1, 0);
            this.mRemote.transact(20, localParcel1, localParcel2, 1);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            localParcel1.writeInt(0);
        }
    }

    public IBinder asBinder()
    {
        return this.mRemote;
    }

    public void attachApplication(IApplicationThread paramIApplicationThread)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIApplicationThread.asBinder());
        this.mRemote.transact(17, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void backupAgentCreated(String paramString, IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(91, localParcel1, localParcel2, 0);
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public boolean bindBackupAgent(ApplicationInfo paramApplicationInfo, int paramInt)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramApplicationInfo.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(90, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public int bindService(IApplicationThread paramIApplicationThread, IBinder paramIBinder, Intent paramIntent, String paramString, IServiceConnection paramIServiceConnection, int paramInt1, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder = paramIApplicationThread.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            localParcel1.writeStrongBinder(paramIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            localParcel1.writeStrongBinder(paramIServiceConnection.asBinder());
            localParcel1.writeInt(paramInt1);
            localParcel1.writeInt(paramInt2);
            this.mRemote.transact(36, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel1.recycle();
            localParcel2.recycle();
            return i;
        }
    }

    public int broadcastIntent(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, int paramInt1, String paramString2, Bundle paramBundle, String paramString3, boolean paramBoolean1, boolean paramBoolean2, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder1;
        IBinder localIBinder2;
        label63: int i;
        if (paramIApplicationThread != null)
        {
            localIBinder1 = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder1);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString1);
            if (paramIIntentReceiver == null)
                break label183;
            localIBinder2 = paramIIntentReceiver.asBinder();
            localParcel1.writeStrongBinder(localIBinder2);
            localParcel1.writeInt(paramInt1);
            localParcel1.writeString(paramString2);
            localParcel1.writeBundle(paramBundle);
            localParcel1.writeString(paramString3);
            if (!paramBoolean1)
                break label189;
            i = 1;
            label106: localParcel1.writeInt(i);
            if (!paramBoolean2)
                break label195;
        }
        label183: label189: label195: for (int j = 1; ; j = 0)
        {
            localParcel1.writeInt(j);
            localParcel1.writeInt(paramInt2);
            this.mRemote.transact(14, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int k = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return k;
            localIBinder1 = null;
            break;
            localIBinder2 = null;
            break label63;
            i = 0;
            break label106;
        }
    }

    public void cancelIntentSender(IIntentSender paramIIntentSender)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIIntentSender.asBinder());
        this.mRemote.transact(64, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public int checkGrantUriPermission(int paramInt1, String paramString, Uri paramUri, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeString(paramString);
        paramUri.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(119, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public int checkPermission(String paramString, int paramInt1, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(53, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramUri.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        localParcel1.writeInt(paramInt3);
        this.mRemote.transact(54, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public boolean clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver, int paramInt)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeStrongBinder(paramIPackageDataObserver.asBinder());
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(78, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public void closeSystemDialogs(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(97, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void crashApplication(int paramInt1, int paramInt2, String paramString1, String paramString2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        localParcel1.writeString(paramString1);
        localParcel1.writeString(paramString2);
        this.mRemote.transact(114, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void dismissKeyguardOnNextActivity()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(139, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public boolean dumpHeap(String paramString1, boolean paramBoolean, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString1);
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel1.writeInt(i);
            localParcel1.writeString(paramString2);
            if (paramParcelFileDescriptor == null)
                break label114;
            localParcel1.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel1, 1);
            label62: this.mRemote.transact(120, localParcel1, localParcel2, 0);
            localParcel2.readException();
            if (localParcel2.readInt() == 0)
                break label123;
        }
        label114: label123: for (boolean bool = true; ; bool = false)
        {
            localParcel2.recycle();
            localParcel1.recycle();
            return bool;
            i = 0;
            break;
            localParcel1.writeInt(0);
            break label62;
        }
    }

    public void enterSafeMode()
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(66, localParcel, null, 0);
        localParcel.recycle();
    }

    public boolean finishActivity(IBinder paramIBinder, int paramInt, Intent paramIntent)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        if (paramIntent != null)
        {
            localParcel1.writeInt(i);
            paramIntent.writeToParcel(localParcel1, 0);
            this.mRemote.transact(11, localParcel1, localParcel2, 0);
            localParcel2.readException();
            if (localParcel2.readInt() == 0)
                break label102;
        }
        while (true)
        {
            localParcel1.recycle();
            localParcel2.recycle();
            return i;
            localParcel1.writeInt(0);
            break;
            label102: int j = 0;
        }
    }

    public boolean finishActivityAffinity(IBinder paramIBinder)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(149, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public void finishHeavyWeightApp()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(109, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void finishInstrumentation(IApplicationThread paramIApplicationThread, int paramInt, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder = paramIApplicationThread.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            localParcel1.writeInt(paramInt);
            localParcel1.writeBundle(paramBundle);
            this.mRemote.transact(45, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void finishReceiver(IBinder paramIBinder, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        localParcel1.writeString(paramString);
        localParcel1.writeBundle(paramBundle);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(16, localParcel1, localParcel2, 1);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void finishSubActivity(IBinder paramIBinder, String paramString, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(32, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void forceStopPackage(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(79, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public ComponentName getActivityClassForToken(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(49, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ComponentName localComponentName = ComponentName.readFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
        return localComponentName;
    }

    public ComponentName getCallingActivity(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(22, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ComponentName localComponentName = ComponentName.readFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
        return localComponentName;
    }

    public String getCallingPackage(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(21, localParcel1, localParcel2, 0);
        localParcel2.readException();
        String str = localParcel2.readString();
        localParcel1.recycle();
        localParcel2.recycle();
        return str;
    }

    public Configuration getConfiguration()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(46, localParcel1, localParcel2, 0);
        localParcel2.readException();
        Configuration localConfiguration = (Configuration)Configuration.CREATOR.createFromParcel(localParcel2);
        localParcel2.recycle();
        localParcel1.recycle();
        return localConfiguration;
    }

    public IActivityManager.ContentProviderHolder getContentProvider(IApplicationThread paramIApplicationThread, String paramString, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder;
        if (paramIApplicationThread != null)
        {
            localIBinder = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder);
            localParcel1.writeString(paramString);
            if (!paramBoolean)
                break label127;
        }
        label127: for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(29, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int j = localParcel2.readInt();
            IActivityManager.ContentProviderHolder localContentProviderHolder = null;
            if (j != 0)
                localContentProviderHolder = (IActivityManager.ContentProviderHolder)IActivityManager.ContentProviderHolder.CREATOR.createFromParcel(localParcel2);
            localParcel1.recycle();
            localParcel2.recycle();
            return localContentProviderHolder;
            localIBinder = null;
            break;
        }
    }

    public IActivityManager.ContentProviderHolder getContentProviderExternal(String paramString, IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(141, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        IActivityManager.ContentProviderHolder localContentProviderHolder = null;
        if (i != 0)
            localContentProviderHolder = (IActivityManager.ContentProviderHolder)IActivityManager.ContentProviderHolder.CREATOR.createFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
        return localContentProviderHolder;
    }

    public UserInfo getCurrentUser()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(130, localParcel1, localParcel2, 0);
        localParcel2.readException();
        UserInfo localUserInfo = (UserInfo)UserInfo.CREATOR.createFromParcel(localParcel2);
        localParcel2.recycle();
        localParcel1.recycle();
        return localUserInfo;
    }

    public ConfigurationInfo getDeviceConfigurationInfo()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(84, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ConfigurationInfo localConfigurationInfo = (ConfigurationInfo)ConfigurationInfo.CREATOR.createFromParcel(localParcel2);
        localParcel2.recycle();
        localParcel1.recycle();
        return localConfigurationInfo;
    }

    public int getFrontActivityScreenCompatMode()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(124, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel2.recycle();
        localParcel1.recycle();
        return i;
    }

    public IIntentSender getIntentSender(int paramInt1, String paramString1, IBinder paramIBinder, String paramString2, int paramInt2, Intent[] paramArrayOfIntent, String[] paramArrayOfString, int paramInt3, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeString(paramString1);
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeString(paramString2);
        localParcel1.writeInt(paramInt2);
        if (paramArrayOfIntent != null)
        {
            localParcel1.writeInt(1);
            localParcel1.writeTypedArray(paramArrayOfIntent, 0);
            localParcel1.writeStringArray(paramArrayOfString);
            localParcel1.writeInt(paramInt3);
            if (paramBundle == null)
                break label155;
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(63, localParcel1, localParcel2, 0);
            localParcel2.readException();
            IIntentSender localIIntentSender = IIntentSender.Stub.asInterface(localParcel2.readStrongBinder());
            localParcel1.recycle();
            localParcel2.recycle();
            return localIIntentSender;
            localParcel1.writeInt(0);
            break;
            label155: localParcel1.writeInt(0);
        }
    }

    public int getLaunchedFromUid(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(150, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public void getMemoryInfo(ActivityManager.MemoryInfo paramMemoryInfo)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(76, localParcel1, localParcel2, 0);
        localParcel2.readException();
        paramMemoryInfo.readFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void getMyMemoryState(ActivityManager.RunningAppProcessInfo paramRunningAppProcessInfo)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(143, localParcel1, localParcel2, 0);
        localParcel2.readException();
        paramRunningAppProcessInfo.readFromParcel(localParcel2);
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public boolean getPackageAskScreenCompat(String paramString)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(128, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public String getPackageForIntentSender(IIntentSender paramIIntentSender)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIIntentSender.asBinder());
        this.mRemote.transact(65, localParcel1, localParcel2, 0);
        localParcel2.readException();
        String str = localParcel2.readString();
        localParcel1.recycle();
        localParcel2.recycle();
        return str;
    }

    public String getPackageForToken(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(50, localParcel1, localParcel2, 0);
        localParcel2.readException();
        String str = localParcel2.readString();
        localParcel1.recycle();
        localParcel2.recycle();
        return str;
    }

    public int getPackageScreenCompatMode(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(126, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel2.recycle();
        localParcel1.recycle();
        return i;
    }

    public int getProcessLimit()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(52, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public Debug.MemoryInfo[] getProcessMemoryInfo(int[] paramArrayOfInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeIntArray(paramArrayOfInt);
        this.mRemote.transact(98, localParcel1, localParcel2, 0);
        localParcel2.readException();
        Debug.MemoryInfo[] arrayOfMemoryInfo = (Debug.MemoryInfo[])localParcel2.createTypedArray(Debug.MemoryInfo.CREATOR);
        localParcel1.recycle();
        localParcel2.recycle();
        return arrayOfMemoryInfo;
    }

    public long[] getProcessPss(int[] paramArrayOfInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeIntArray(paramArrayOfInt);
        this.mRemote.transact(137, localParcel1, localParcel2, 0);
        localParcel2.readException();
        long[] arrayOfLong = localParcel2.createLongArray();
        localParcel1.recycle();
        localParcel2.recycle();
        return arrayOfLong;
    }

    public List<ActivityManager.ProcessErrorStateInfo> getProcessesInErrorState()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(77, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ArrayList localArrayList = localParcel2.createTypedArrayList(ActivityManager.ProcessErrorStateInfo.CREATOR);
        localParcel1.recycle();
        localParcel2.recycle();
        return localArrayList;
    }

    public String getProviderMimeType(Uri paramUri)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramUri.writeToParcel(localParcel1, 0);
        this.mRemote.transact(115, localParcel1, localParcel2, 0);
        localParcel2.readException();
        String str = localParcel2.readString();
        localParcel1.recycle();
        localParcel2.recycle();
        return str;
    }

    public List<ActivityManager.RecentTaskInfo> getRecentTasks(int paramInt1, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(60, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ArrayList localArrayList = localParcel2.createTypedArrayList(ActivityManager.RecentTaskInfo.CREATOR);
        localParcel1.recycle();
        localParcel2.recycle();
        return localArrayList;
    }

    public int getRequestedOrientation(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(71, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public List<ActivityManager.RunningAppProcessInfo> getRunningAppProcesses()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(83, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ArrayList localArrayList = localParcel2.createTypedArrayList(ActivityManager.RunningAppProcessInfo.CREATOR);
        localParcel1.recycle();
        localParcel2.recycle();
        return localArrayList;
    }

    public List<ApplicationInfo> getRunningExternalApplications()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(108, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ArrayList localArrayList = localParcel2.createTypedArrayList(ApplicationInfo.CREATOR);
        localParcel1.recycle();
        localParcel2.recycle();
        return localArrayList;
    }

    public PendingIntent getRunningServiceControlPanel(ComponentName paramComponentName)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramComponentName.writeToParcel(localParcel1, 0);
        this.mRemote.transact(33, localParcel1, localParcel2, 0);
        localParcel2.readException();
        PendingIntent localPendingIntent = PendingIntent.readPendingIntentOrNullFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
        return localPendingIntent;
    }

    public List getServices(int paramInt1, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(81, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ArrayList localArrayList = null;
        int i = localParcel2.readInt();
        if (i >= 0)
        {
            localArrayList = new ArrayList();
            while (i > 0)
            {
                localArrayList.add((ActivityManager.RunningServiceInfo)ActivityManager.RunningServiceInfo.CREATOR.createFromParcel(localParcel2));
                i--;
            }
        }
        localParcel1.recycle();
        localParcel2.recycle();
        return localArrayList;
    }

    public int getTaskForActivity(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(27, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int j = localParcel2.readInt();
            localParcel1.recycle();
            localParcel2.recycle();
            return j;
        }
    }

    public ActivityManager.TaskThumbnails getTaskThumbnails(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(82, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ActivityManager.TaskThumbnails localTaskThumbnails = null;
        if (localParcel2.readInt() != 0)
            localTaskThumbnails = (ActivityManager.TaskThumbnails)ActivityManager.TaskThumbnails.CREATOR.createFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
        return localTaskThumbnails;
    }

    public List getTasks(int paramInt1, int paramInt2, IThumbnailReceiver paramIThumbnailReceiver)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        if (paramIThumbnailReceiver != null);
        ArrayList localArrayList;
        for (IBinder localIBinder = paramIThumbnailReceiver.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(23, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localArrayList = null;
            int i = localParcel2.readInt();
            if (i < 0)
                break;
            localArrayList = new ArrayList();
            while (i > 0)
            {
                localArrayList.add((ActivityManager.RunningTaskInfo)ActivityManager.RunningTaskInfo.CREATOR.createFromParcel(localParcel2));
                i--;
            }
        }
        localParcel1.recycle();
        localParcel2.recycle();
        return localArrayList;
    }

    public int getUidForIntentSender(IIntentSender paramIIntentSender)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIIntentSender.asBinder());
        this.mRemote.transact(93, localParcel1, localParcel2, 0);
        localParcel2.readException();
        int i = localParcel2.readInt();
        localParcel1.recycle();
        localParcel2.recycle();
        return i;
    }

    public void goingToSleep()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(40, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void grantUriPermission(IApplicationThread paramIApplicationThread, String paramString, Uri paramUri, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIApplicationThread.asBinder());
        localParcel1.writeString(paramString);
        paramUri.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(55, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void grantUriPermissionFromOwner(IBinder paramIBinder, int paramInt1, String paramString, Uri paramUri, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt1);
        localParcel1.writeString(paramString);
        paramUri.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(55, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void handleApplicationCrash(IBinder paramIBinder, ApplicationErrorReport.CrashInfo paramCrashInfo)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        paramCrashInfo.writeToParcel(localParcel1, 0);
        this.mRemote.transact(2, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public void handleApplicationStrictModeViolation(IBinder paramIBinder, int paramInt, StrictMode.ViolationInfo paramViolationInfo)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        paramViolationInfo.writeToParcel(localParcel1, 0);
        this.mRemote.transact(110, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public boolean handleApplicationWtf(IBinder paramIBinder, String paramString, ApplicationErrorReport.CrashInfo paramCrashInfo)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeString(paramString);
        paramCrashInfo.writeToParcel(localParcel1, 0);
        this.mRemote.transact(102, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public boolean isImmersive(IBinder paramIBinder)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(111, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() == i);
        while (true)
        {
            localParcel1.recycle();
            localParcel2.recycle();
            return i;
            int j = 0;
        }
    }

    public boolean isIntentSenderAnActivity(IIntentSender paramIIntentSender)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIIntentSender.asBinder());
        this.mRemote.transact(152, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public boolean isIntentSenderTargetedToPackage(IIntentSender paramIIntentSender)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIIntentSender.asBinder());
        this.mRemote.transact(135, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public boolean isTopActivityImmersive()
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(113, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() == i);
        while (true)
        {
            localParcel1.recycle();
            localParcel2.recycle();
            return i;
            int j = 0;
        }
    }

    public boolean isUserAMonkey()
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(104, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public void killAllBackgroundProcesses()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(140, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void killApplicationProcess(String paramString, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(99, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void killApplicationWithUid(String paramString, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(96, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void killBackgroundProcesses(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(103, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public boolean killPids(int[] paramArrayOfInt, String paramString, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeIntArray(paramArrayOfInt);
        localParcel1.writeString(paramString);
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel1.writeInt(i);
            this.mRemote.transact(80, localParcel1, localParcel2, 0);
            if (localParcel2.readInt() == 0)
                break label90;
        }
        label90: for (boolean bool = true; ; bool = false)
        {
            localParcel1.recycle();
            localParcel2.recycle();
            return bool;
            i = 0;
            break;
        }
    }

    public boolean killProcessesBelowForeground(String paramString)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(144, localParcel1, localParcel2, 0);
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public boolean moveActivityTaskToBack(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel1.writeInt(i);
            this.mRemote.transact(75, localParcel1, localParcel2, 0);
            localParcel2.readException();
            if (localParcel2.readInt() == 0)
                break label83;
        }
        label83: for (boolean bool = true; ; bool = false)
        {
            localParcel1.recycle();
            localParcel2.recycle();
            return bool;
            i = 0;
            break;
        }
    }

    public void moveTaskBackwards(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(26, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void moveTaskToBack(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(25, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void moveTaskToFront(int paramInt1, int paramInt2, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        if (paramBundle != null)
        {
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(24, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            localParcel1.writeInt(0);
        }
    }

    public boolean navigateUpTo(IBinder paramIBinder, Intent paramIntent1, int paramInt, Intent paramIntent2)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        paramIntent1.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt);
        if (paramIntent2 != null)
        {
            localParcel1.writeInt(i);
            paramIntent2.writeToParcel(localParcel1, 0);
            this.mRemote.transact(147, localParcel1, localParcel2, 0);
            localParcel2.readException();
            if (localParcel2.readInt() == 0)
                break label112;
        }
        while (true)
        {
            localParcel1.recycle();
            localParcel2.recycle();
            return i;
            localParcel1.writeInt(0);
            break;
            label112: int j = 0;
        }
    }

    public IBinder newUriPermissionOwner(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(116, localParcel1, localParcel2, 0);
        localParcel2.readException();
        IBinder localIBinder = localParcel2.readStrongBinder();
        localParcel1.recycle();
        localParcel2.recycle();
        return localIBinder;
    }

    public void noteWakeupAlarm(IIntentSender paramIIntentSender)
        throws RemoteException
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeStrongBinder(paramIIntentSender.asBinder());
        localParcel.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(68, localParcel, null, 0);
        localParcel.recycle();
    }

    public ParcelFileDescriptor openContentUri(Uri paramUri)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(5, localParcel1, localParcel2, 0);
        localParcel2.readException();
        ParcelFileDescriptor localParcelFileDescriptor = null;
        if (localParcel2.readInt() != 0)
            localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
        localParcel1.recycle();
        localParcel2.recycle();
        return localParcelFileDescriptor;
    }

    public void overridePendingTransition(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(101, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public IBinder peekService(Intent paramIntent, String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramIntent.writeToParcel(localParcel1, 0);
        localParcel1.writeString(paramString);
        this.mRemote.transact(85, localParcel1, localParcel2, 0);
        localParcel2.readException();
        IBinder localIBinder = localParcel2.readStrongBinder();
        localParcel2.recycle();
        localParcel1.recycle();
        return localIBinder;
    }

    public boolean profileControl(String paramString1, boolean paramBoolean, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString1);
        int i;
        if (paramBoolean)
        {
            i = 1;
            localParcel1.writeInt(i);
            localParcel1.writeInt(paramInt);
            localParcel1.writeString(paramString2);
            if (paramParcelFileDescriptor == null)
                break label121;
            localParcel1.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel1, 1);
            label69: this.mRemote.transact(86, localParcel1, localParcel2, 0);
            localParcel2.readException();
            if (localParcel2.readInt() == 0)
                break label130;
        }
        label130: for (boolean bool = true; ; bool = false)
        {
            localParcel2.recycle();
            localParcel1.recycle();
            return bool;
            i = 0;
            break;
            label121: localParcel1.writeInt(0);
            break label69;
        }
    }

    public void publishContentProviders(IApplicationThread paramIApplicationThread, List<IActivityManager.ContentProviderHolder> paramList)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder = paramIApplicationThread.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            localParcel1.writeTypedList(paramList);
            this.mRemote.transact(30, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void publishService(IBinder paramIBinder1, Intent paramIntent, IBinder paramIBinder2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder1);
        paramIntent.writeToParcel(localParcel1, 0);
        localParcel1.writeStrongBinder(paramIBinder2);
        this.mRemote.transact(38, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public boolean refContentProvider(IBinder paramIBinder, int paramInt1, int paramInt2)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(31, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public void registerProcessObserver(IProcessObserver paramIProcessObserver)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIProcessObserver != null);
        for (IBinder localIBinder = paramIProcessObserver.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(133, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public Intent registerReceiver(IApplicationThread paramIApplicationThread, String paramString1, IIntentReceiver paramIIntentReceiver, IntentFilter paramIntentFilter, String paramString2)
        throws RemoteException
    {
        IBinder localIBinder1 = null;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder2 = paramIApplicationThread.asBinder(); ; localIBinder2 = null)
        {
            localParcel1.writeStrongBinder(localIBinder2);
            localParcel1.writeString(paramString1);
            if (paramIIntentReceiver != null)
                localIBinder1 = paramIIntentReceiver.asBinder();
            localParcel1.writeStrongBinder(localIBinder1);
            paramIntentFilter.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString2);
            this.mRemote.transact(12, localParcel1, localParcel2, 0);
            localParcel2.readException();
            Intent localIntent = null;
            if (localParcel2.readInt() != 0)
                localIntent = (Intent)Intent.CREATOR.createFromParcel(localParcel2);
            localParcel2.recycle();
            localParcel1.recycle();
            return localIntent;
        }
    }

    public void removeContentProvider(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(69, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void removeContentProviderExternal(String paramString, IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(142, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public boolean removeSubTask(int paramInt1, int paramInt2)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(131, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public boolean removeTask(int paramInt1, int paramInt2)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        this.mRemote.transact(132, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public void reportThumbnail(IBinder paramIBinder, Bitmap paramBitmap, CharSequence paramCharSequence)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramBitmap != null)
        {
            localParcel1.writeInt(1);
            paramBitmap.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            TextUtils.writeToParcel(paramCharSequence, localParcel1, 0);
            this.mRemote.transact(28, localParcel1, localParcel2, 1);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            localParcel1.writeInt(0);
        }
    }

    public void resumeAppSwitches()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(89, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public void revokeUriPermission(IApplicationThread paramIApplicationThread, Uri paramUri, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIApplicationThread.asBinder());
        paramUri.writeToParcel(localParcel1, 0);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(56, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void revokeUriPermissionFromOwner(IBinder paramIBinder, Uri paramUri, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramUri != null)
        {
            localParcel1.writeInt(1);
            paramUri.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            localParcel1.writeInt(paramInt);
            this.mRemote.transact(56, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            localParcel1.writeInt(0);
        }
    }

    public void serviceDoneExecuting(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt1);
        localParcel1.writeInt(paramInt2);
        localParcel1.writeInt(paramInt3);
        this.mRemote.transact(61, localParcel1, localParcel2, 1);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void setActivityController(IActivityController paramIActivityController)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIActivityController != null);
        for (IBinder localIBinder = paramIActivityController.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(57, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void setAlwaysFinish(boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(43, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void setDebugApp(String paramString, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        int j;
        if (paramBoolean1)
        {
            j = i;
            localParcel1.writeInt(j);
            if (!paramBoolean2)
                break label91;
        }
        while (true)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(42, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            j = 0;
            break;
            label91: i = 0;
        }
    }

    public void setFrontActivityScreenCompatMode(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(125, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public void setImmersive(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(112, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void setLockScreenShown(boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(148, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void setPackageAskScreenCompat(String paramString, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(129, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel2.recycle();
            localParcel1.recycle();
            return;
        }
    }

    public void setPackageScreenCompatMode(String paramString, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(127, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public void setProcessForeground(IBinder paramIBinder, int paramInt, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(73, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void setProcessLimit(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(51, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void setRequestedOrientation(IBinder paramIBinder, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(70, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void setServiceForeground(ComponentName paramComponentName, IBinder paramIBinder, int paramInt, Notification paramNotification, boolean paramBoolean)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        ComponentName.writeToParcel(paramComponentName, localParcel1);
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        if (paramNotification != null)
        {
            localParcel1.writeInt(i);
            paramNotification.writeToParcel(localParcel1, 0);
            if (!paramBoolean)
                break label112;
        }
        while (true)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(74, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
            localParcel1.writeInt(0);
            break;
            label112: i = 0;
        }
    }

    public void showBootMessage(CharSequence paramCharSequence, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        TextUtils.writeToParcel(paramCharSequence, localParcel1, 0);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(138, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void showWaitingForDebugger(IApplicationThread paramIApplicationThread, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIApplicationThread.asBinder());
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(58, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public boolean shutdown(int paramInt)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(87, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public void signalPersistentProcesses(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(59, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public int startActivities(IApplicationThread paramIApplicationThread, Intent[] paramArrayOfIntent, String[] paramArrayOfString, IBinder paramIBinder, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder;
        if (paramIApplicationThread != null)
        {
            localIBinder = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder);
            localParcel1.writeTypedArray(paramArrayOfIntent, 0);
            localParcel1.writeStringArray(paramArrayOfString);
            localParcel1.writeStrongBinder(paramIBinder);
            if (paramBundle == null)
                break label123;
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(121, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
            localIBinder = null;
            break;
            label123: localParcel1.writeInt(0);
        }
    }

    public int startActivitiesInPackage(int paramInt, Intent[] paramArrayOfIntent, String[] paramArrayOfString, IBinder paramIBinder, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        localParcel1.writeTypedArray(paramArrayOfIntent, 0);
        localParcel1.writeStringArray(paramArrayOfString);
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramBundle != null)
        {
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(122, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
            localParcel1.writeInt(0);
        }
    }

    public int startActivity(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, IBinder paramIBinder, String paramString2, int paramInt1, int paramInt2, String paramString3, ParcelFileDescriptor paramParcelFileDescriptor, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder;
        if (paramIApplicationThread != null)
        {
            localIBinder = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString1);
            localParcel1.writeStrongBinder(paramIBinder);
            localParcel1.writeString(paramString2);
            localParcel1.writeInt(paramInt1);
            localParcel1.writeInt(paramInt2);
            localParcel1.writeString(paramString3);
            if (paramParcelFileDescriptor == null)
                break label169;
            localParcel1.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel1, 1);
            label103: if (paramBundle == null)
                break label178;
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(3, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
            localIBinder = null;
            break;
            label169: localParcel1.writeInt(0);
            break label103;
            label178: localParcel1.writeInt(0);
        }
    }

    public IActivityManager.WaitResult startActivityAndWait(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, IBinder paramIBinder, String paramString2, int paramInt1, int paramInt2, String paramString3, ParcelFileDescriptor paramParcelFileDescriptor, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder;
        if (paramIApplicationThread != null)
        {
            localIBinder = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString1);
            localParcel1.writeStrongBinder(paramIBinder);
            localParcel1.writeString(paramString2);
            localParcel1.writeInt(paramInt1);
            localParcel1.writeInt(paramInt2);
            localParcel1.writeString(paramString3);
            if (paramParcelFileDescriptor == null)
                break label178;
            localParcel1.writeInt(1);
            paramParcelFileDescriptor.writeToParcel(localParcel1, 1);
            label103: if (paramBundle == null)
                break label187;
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(105, localParcel1, localParcel2, 0);
            localParcel2.readException();
            IActivityManager.WaitResult localWaitResult = (IActivityManager.WaitResult)IActivityManager.WaitResult.CREATOR.createFromParcel(localParcel2);
            localParcel2.recycle();
            localParcel1.recycle();
            return localWaitResult;
            localIBinder = null;
            break;
            label178: localParcel1.writeInt(0);
            break label103;
            label187: localParcel1.writeInt(0);
        }
    }

    public int startActivityInPackage(int paramInt1, Intent paramIntent, String paramString1, IBinder paramIBinder, String paramString2, int paramInt2, int paramInt3, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt1);
        paramIntent.writeToParcel(localParcel1, 0);
        localParcel1.writeString(paramString1);
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeString(paramString2);
        localParcel1.writeInt(paramInt2);
        localParcel1.writeInt(paramInt3);
        if (paramBundle != null)
        {
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(95, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
            localParcel1.writeInt(0);
        }
    }

    public int startActivityIntentSender(IApplicationThread paramIApplicationThread, IntentSender paramIntentSender, Intent paramIntent, String paramString1, IBinder paramIBinder, String paramString2, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder;
        if (paramIApplicationThread != null)
        {
            localIBinder = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder);
            paramIntentSender.writeToParcel(localParcel1, 0);
            if (paramIntent == null)
                break label169;
            localParcel1.writeInt(1);
            paramIntent.writeToParcel(localParcel1, 0);
            label60: localParcel1.writeString(paramString1);
            localParcel1.writeStrongBinder(paramIBinder);
            localParcel1.writeString(paramString2);
            localParcel1.writeInt(paramInt1);
            localParcel1.writeInt(paramInt2);
            localParcel1.writeInt(paramInt3);
            if (paramBundle == null)
                break label178;
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(100, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
            localIBinder = null;
            break;
            label169: localParcel1.writeInt(0);
            break label60;
            label178: localParcel1.writeInt(0);
        }
    }

    public int startActivityWithConfig(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, IBinder paramIBinder, String paramString2, int paramInt1, int paramInt2, Configuration paramConfiguration, Bundle paramBundle)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        IBinder localIBinder;
        if (paramIApplicationThread != null)
        {
            localIBinder = paramIApplicationThread.asBinder();
            localParcel1.writeStrongBinder(localIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString1);
            localParcel1.writeStrongBinder(paramIBinder);
            localParcel1.writeString(paramString2);
            localParcel1.writeInt(paramInt1);
            localParcel1.writeInt(paramInt2);
            paramConfiguration.writeToParcel(localParcel1, 0);
            if (paramBundle == null)
                break label151;
            localParcel1.writeInt(1);
            paramBundle.writeToParcel(localParcel1, 0);
        }
        while (true)
        {
            this.mRemote.transact(3, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
            localIBinder = null;
            break;
            label151: localParcel1.writeInt(0);
        }
    }

    public boolean startInstrumentation(ComponentName paramComponentName, String paramString, int paramInt, Bundle paramBundle, IInstrumentationWatcher paramIInstrumentationWatcher)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        ComponentName.writeToParcel(paramComponentName, localParcel1);
        localParcel1.writeString(paramString);
        localParcel1.writeInt(paramInt);
        localParcel1.writeBundle(paramBundle);
        if (paramIInstrumentationWatcher != null);
        for (IBinder localIBinder = paramIInstrumentationWatcher.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(44, localParcel1, localParcel2, 0);
            localParcel2.readException();
            if (localParcel2.readInt() != 0)
                bool = true;
            localParcel2.recycle();
            localParcel1.recycle();
            return bool;
        }
    }

    public boolean startNextMatchingActivity(IBinder paramIBinder, Intent paramIntent, Bundle paramBundle)
        throws RemoteException
    {
        int i = 1;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        paramIntent.writeToParcel(localParcel1, 0);
        if (paramBundle != null)
        {
            localParcel1.writeInt(i);
            paramBundle.writeToParcel(localParcel1, 0);
            this.mRemote.transact(67, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int k = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            if (k == 0)
                break label107;
        }
        while (true)
        {
            return i;
            localParcel1.writeInt(0);
            break;
            label107: int j = 0;
        }
    }

    public void startRunning(String paramString1, String paramString2, String paramString3, String paramString4)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeString(paramString1);
        localParcel1.writeString(paramString2);
        localParcel1.writeString(paramString3);
        localParcel1.writeString(paramString4);
        this.mRemote.transact(1, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public ComponentName startService(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder = paramIApplicationThread.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            this.mRemote.transact(34, localParcel1, localParcel2, 0);
            localParcel2.readException();
            ComponentName localComponentName = ComponentName.readFromParcel(localParcel2);
            localParcel1.recycle();
            localParcel2.recycle();
            return localComponentName;
        }
    }

    public void stopAppSwitches()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(88, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public int stopService(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder = paramIApplicationThread.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeString(paramString);
            this.mRemote.transact(35, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            localParcel2.recycle();
            localParcel1.recycle();
            return i;
        }
    }

    public boolean stopServiceToken(ComponentName paramComponentName, IBinder paramIBinder, int paramInt)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        ComponentName.writeToParcel(paramComponentName, localParcel1);
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(48, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public boolean switchUser(int paramInt)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeInt(paramInt);
        this.mRemote.transact(130, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel2.recycle();
        localParcel1.recycle();
        return bool;
    }

    public boolean targetTaskAffinityMatchesActivity(IBinder paramIBinder, String paramString)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        localParcel1.writeString(paramString);
        this.mRemote.transact(146, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public boolean testIsSystemReady()
    {
        return true;
    }

    public void unbindBackupAgent(ApplicationInfo paramApplicationInfo)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramApplicationInfo.writeToParcel(localParcel1, 0);
        this.mRemote.transact(92, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel2.recycle();
        localParcel1.recycle();
    }

    public void unbindFinished(IBinder paramIBinder, Intent paramIntent, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        paramIntent.writeToParcel(localParcel1, 0);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(72, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public boolean unbindService(IServiceConnection paramIServiceConnection)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIServiceConnection.asBinder());
        this.mRemote.transact(37, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }

    public void unbroadcastIntent(IApplicationThread paramIApplicationThread, Intent paramIntent, int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIApplicationThread != null);
        for (IBinder localIBinder = paramIApplicationThread.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            paramIntent.writeToParcel(localParcel1, 0);
            localParcel1.writeInt(paramInt);
            this.mRemote.transact(15, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void unhandledBack()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(4, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void unregisterProcessObserver(IProcessObserver paramIProcessObserver)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        if (paramIProcessObserver != null);
        for (IBinder localIBinder = paramIProcessObserver.asBinder(); ; localIBinder = null)
        {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(134, localParcel1, localParcel2, 0);
            localParcel2.readException();
            localParcel1.recycle();
            localParcel2.recycle();
            return;
        }
    }

    public void unregisterReceiver(IIntentReceiver paramIIntentReceiver)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIIntentReceiver.asBinder());
        this.mRemote.transact(13, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void unstableProviderDied(IBinder paramIBinder)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(151, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void updateConfiguration(Configuration paramConfiguration)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramConfiguration.writeToParcel(localParcel1, 0);
        this.mRemote.transact(47, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void updatePersistentConfiguration(Configuration paramConfiguration)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        paramConfiguration.writeToParcel(localParcel1, 0);
        this.mRemote.transact(136, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public void wakingUp()
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        this.mRemote.transact(41, localParcel1, localParcel2, 0);
        localParcel2.readException();
        localParcel1.recycle();
        localParcel2.recycle();
    }

    public boolean willActivityBeVisible(IBinder paramIBinder)
        throws RemoteException
    {
        boolean bool = false;
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.app.IActivityManager");
        localParcel1.writeStrongBinder(paramIBinder);
        this.mRemote.transact(106, localParcel1, localParcel2, 0);
        localParcel2.readException();
        if (localParcel2.readInt() != 0)
            bool = true;
        localParcel1.recycle();
        localParcel2.recycle();
        return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ActivityManagerProxy
 * JD-Core Version:        0.6.2
 */