package android.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.MessageQueue;
import android.util.AttributeSet;
import android.view.InputChannel;
import android.view.InputQueue;
import android.view.InputQueue.Callback;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback2;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.view.IInputMethodCallback.Stub;
import com.android.internal.view.IInputMethodSession;
import java.io.File;
import java.lang.ref.WeakReference;

public class NativeActivity extends Activity
    implements SurfaceHolder.Callback2, InputQueue.Callback, ViewTreeObserver.OnGlobalLayoutListener
{
    private static final String KEY_NATIVE_SAVED_STATE = "android:native_state";
    public static final String META_DATA_FUNC_NAME = "android.app.func_name";
    public static final String META_DATA_LIB_NAME = "android.app.lib_name";
    private InputQueue mCurInputQueue;
    private SurfaceHolder mCurSurfaceHolder;
    private boolean mDestroyed;
    private boolean mDispatchingUnhandledKey;
    private InputMethodManager mIMM;
    private InputMethodCallback mInputMethodCallback;
    int mLastContentHeight;
    int mLastContentWidth;
    int mLastContentX;
    int mLastContentY;
    final int[] mLocation = new int[2];
    private NativeContentView mNativeContentView;
    private int mNativeHandle;

    private native void dispatchKeyEventNative(int paramInt, KeyEvent paramKeyEvent);

    private native void finishPreDispatchKeyEventNative(int paramInt1, int paramInt2, boolean paramBoolean);

    private native int loadNativeCode(String paramString1, String paramString2, MessageQueue paramMessageQueue, String paramString3, String paramString4, String paramString5, int paramInt, AssetManager paramAssetManager, byte[] paramArrayOfByte);

    private native void onConfigurationChangedNative(int paramInt);

    private native void onContentRectChangedNative(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private native void onInputChannelCreatedNative(int paramInt, InputChannel paramInputChannel);

    private native void onInputChannelDestroyedNative(int paramInt, InputChannel paramInputChannel);

    private native void onLowMemoryNative(int paramInt);

    private native void onPauseNative(int paramInt);

    private native void onResumeNative(int paramInt);

    private native byte[] onSaveInstanceStateNative(int paramInt);

    private native void onStartNative(int paramInt);

    private native void onStopNative(int paramInt);

    private native void onSurfaceChangedNative(int paramInt1, Surface paramSurface, int paramInt2, int paramInt3, int paramInt4);

    private native void onSurfaceCreatedNative(int paramInt, Surface paramSurface);

    private native void onSurfaceDestroyedNative(int paramInt);

    private native void onSurfaceRedrawNeededNative(int paramInt, Surface paramSurface);

    private native void onWindowFocusChangedNative(int paramInt, boolean paramBoolean);

    private native void unloadNativeCode(int paramInt);

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if (this.mDispatchingUnhandledKey);
        for (boolean bool = super.dispatchKeyEvent(paramKeyEvent); ; bool = true)
        {
            return bool;
            dispatchKeyEventNative(this.mNativeHandle, paramKeyEvent);
        }
    }

    boolean dispatchUnhandledKeyEvent(KeyEvent paramKeyEvent)
    {
        try
        {
            this.mDispatchingUnhandledKey = true;
            View localView = getWindow().getDecorView();
            if (localView != null)
            {
                boolean bool2 = localView.dispatchKeyEvent(paramKeyEvent);
                bool1 = bool2;
                return bool1;
            }
            this.mDispatchingUnhandledKey = false;
            boolean bool1 = false;
        }
        finally
        {
            this.mDispatchingUnhandledKey = false;
        }
    }

    void hideIme(int paramInt)
    {
        this.mIMM.hideSoftInputFromWindow(this.mNativeContentView.getWindowToken(), paramInt);
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        if (!this.mDestroyed)
            onConfigurationChangedNative(this.mNativeHandle);
    }

    protected void onCreate(Bundle paramBundle)
    {
        Object localObject1 = "main";
        Object localObject2 = "ANativeActivity_onCreate";
        this.mIMM = ((InputMethodManager)getSystemService("input_method"));
        this.mInputMethodCallback = new InputMethodCallback(this);
        getWindow().takeSurface(this);
        getWindow().takeInputQueue(this);
        getWindow().setFormat(4);
        getWindow().setSoftInputMode(16);
        this.mNativeContentView = new NativeContentView(this);
        this.mNativeContentView.mActivity = this;
        setContentView(this.mNativeContentView);
        this.mNativeContentView.requestFocus();
        this.mNativeContentView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        ActivityInfo localActivityInfo;
        String str1;
        try
        {
            localActivityInfo = getPackageManager().getActivityInfo(getIntent().getComponent(), 128);
            if (localActivityInfo.metaData != null)
            {
                String str2 = localActivityInfo.metaData.getString("android.app.lib_name");
                if (str2 != null)
                    localObject1 = str2;
                String str3 = localActivityInfo.metaData.getString("android.app.func_name");
                if (str3 != null)
                    localObject2 = str3;
            }
            str1 = null;
            File localFile = new File(localActivityInfo.applicationInfo.nativeLibraryDir, System.mapLibraryName((String)localObject1));
            if (localFile.exists())
                str1 = localFile.getPath();
            if (str1 == null)
                throw new IllegalArgumentException("Unable to find native library: " + (String)localObject1);
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new RuntimeException("Error getting activity info", localNameNotFoundException);
        }
        if (paramBundle != null);
        for (byte[] arrayOfByte = paramBundle.getByteArray("android:native_state"); ; arrayOfByte = null)
        {
            this.mNativeHandle = loadNativeCode(str1, (String)localObject2, Looper.myQueue(), getFilesDir().toString(), getObbDir().toString(), Environment.getExternalStorageAppFilesDirectory(localActivityInfo.packageName).toString(), Build.VERSION.SDK_INT, getAssets(), arrayOfByte);
            if (this.mNativeHandle != 0)
                break;
            throw new IllegalArgumentException("Unable to load native library: " + str1);
        }
        super.onCreate(paramBundle);
    }

    protected void onDestroy()
    {
        this.mDestroyed = true;
        if (this.mCurSurfaceHolder != null)
        {
            onSurfaceDestroyedNative(this.mNativeHandle);
            this.mCurSurfaceHolder = null;
        }
        if (this.mCurInputQueue != null)
        {
            onInputChannelDestroyedNative(this.mNativeHandle, this.mCurInputQueue.getInputChannel());
            this.mCurInputQueue = null;
        }
        unloadNativeCode(this.mNativeHandle);
        super.onDestroy();
    }

    public void onGlobalLayout()
    {
        this.mNativeContentView.getLocationInWindow(this.mLocation);
        int i = this.mNativeContentView.getWidth();
        int j = this.mNativeContentView.getHeight();
        if ((this.mLocation[0] != this.mLastContentX) || (this.mLocation[1] != this.mLastContentY) || (i != this.mLastContentWidth) || (j != this.mLastContentHeight))
        {
            this.mLastContentX = this.mLocation[0];
            this.mLastContentY = this.mLocation[1];
            this.mLastContentWidth = i;
            this.mLastContentHeight = j;
            if (!this.mDestroyed)
                onContentRectChangedNative(this.mNativeHandle, this.mLastContentX, this.mLastContentY, this.mLastContentWidth, this.mLastContentHeight);
        }
    }

    public void onInputQueueCreated(InputQueue paramInputQueue)
    {
        if (!this.mDestroyed)
        {
            this.mCurInputQueue = paramInputQueue;
            onInputChannelCreatedNative(this.mNativeHandle, paramInputQueue.getInputChannel());
        }
    }

    public void onInputQueueDestroyed(InputQueue paramInputQueue)
    {
        this.mCurInputQueue = null;
        if (!this.mDestroyed)
            onInputChannelDestroyedNative(this.mNativeHandle, paramInputQueue.getInputChannel());
    }

    public void onLowMemory()
    {
        super.onLowMemory();
        if (!this.mDestroyed)
            onLowMemoryNative(this.mNativeHandle);
    }

    protected void onPause()
    {
        super.onPause();
        onPauseNative(this.mNativeHandle);
    }

    protected void onResume()
    {
        super.onResume();
        onResumeNative(this.mNativeHandle);
    }

    protected void onSaveInstanceState(Bundle paramBundle)
    {
        super.onSaveInstanceState(paramBundle);
        byte[] arrayOfByte = onSaveInstanceStateNative(this.mNativeHandle);
        if (arrayOfByte != null)
            paramBundle.putByteArray("android:native_state", arrayOfByte);
    }

    protected void onStart()
    {
        super.onStart();
        onStartNative(this.mNativeHandle);
    }

    protected void onStop()
    {
        super.onStop();
        onStopNative(this.mNativeHandle);
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        super.onWindowFocusChanged(paramBoolean);
        if (!this.mDestroyed)
            onWindowFocusChangedNative(this.mNativeHandle, paramBoolean);
    }

    void preDispatchKeyEvent(KeyEvent paramKeyEvent, int paramInt)
    {
        this.mIMM.dispatchKeyEvent(this, paramInt, paramKeyEvent, this.mInputMethodCallback);
    }

    void setWindowFlags(int paramInt1, int paramInt2)
    {
        getWindow().setFlags(paramInt1, paramInt2);
    }

    void setWindowFormat(int paramInt)
    {
        getWindow().setFormat(paramInt);
    }

    void showIme(int paramInt)
    {
        this.mIMM.showSoftInput(this.mNativeContentView, paramInt);
    }

    public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
    {
        if (!this.mDestroyed)
        {
            this.mCurSurfaceHolder = paramSurfaceHolder;
            onSurfaceChangedNative(this.mNativeHandle, paramSurfaceHolder.getSurface(), paramInt1, paramInt2, paramInt3);
        }
    }

    public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
    {
        if (!this.mDestroyed)
        {
            this.mCurSurfaceHolder = paramSurfaceHolder;
            onSurfaceCreatedNative(this.mNativeHandle, paramSurfaceHolder.getSurface());
        }
    }

    public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
    {
        this.mCurSurfaceHolder = null;
        if (!this.mDestroyed)
            onSurfaceDestroyedNative(this.mNativeHandle);
    }

    public void surfaceRedrawNeeded(SurfaceHolder paramSurfaceHolder)
    {
        if (!this.mDestroyed)
        {
            this.mCurSurfaceHolder = paramSurfaceHolder;
            onSurfaceRedrawNeededNative(this.mNativeHandle, paramSurfaceHolder.getSurface());
        }
    }

    static class InputMethodCallback extends IInputMethodCallback.Stub
    {
        WeakReference<NativeActivity> mNa;

        InputMethodCallback(NativeActivity paramNativeActivity)
        {
            this.mNa = new WeakReference(paramNativeActivity);
        }

        public void finishedEvent(int paramInt, boolean paramBoolean)
        {
            NativeActivity localNativeActivity = (NativeActivity)this.mNa.get();
            if (localNativeActivity != null)
                localNativeActivity.finishPreDispatchKeyEventNative(localNativeActivity.mNativeHandle, paramInt, paramBoolean);
        }

        public void sessionCreated(IInputMethodSession paramIInputMethodSession)
        {
        }
    }

    static class NativeContentView extends View
    {
        NativeActivity mActivity;

        public NativeContentView(Context paramContext)
        {
            super();
        }

        public NativeContentView(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.NativeActivity
 * JD-Core Version:        0.6.2
 */