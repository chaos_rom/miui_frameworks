package android.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.content.ServiceConnection;
import android.os.Handler;

class ReceiverRestrictedContext extends ContextWrapper
{
    ReceiverRestrictedContext(Context paramContext)
    {
        super(paramContext);
    }

    public boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt)
    {
        throw new ReceiverCallNotAllowedException("IntentReceiver components are not allowed to bind to services");
    }

    public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter)
    {
        return registerReceiver(paramBroadcastReceiver, paramIntentFilter, null, null);
    }

    public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler)
    {
        throw new ReceiverCallNotAllowedException("IntentReceiver components are not allowed to register to receive intents");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ReceiverRestrictedContext
 * JD-Core Version:        0.6.2
 */