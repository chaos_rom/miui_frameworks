package android.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.StrictMode;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.util.AttributeSet;
import android.util.EventLog;
import android.util.Log;
import android.util.SparseArray;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.KeyEvent.Callback;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory2;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewManager;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerImpl;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R.styleable;
import com.android.internal.app.ActionBarImpl;
import com.android.internal.policy.PolicyManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import miui.net.FirewallManager;

public class Activity extends ContextThemeWrapper
    implements LayoutInflater.Factory2, Window.Callback, KeyEvent.Callback, View.OnCreateContextMenuListener, ComponentCallbacks2
{
    private static final boolean DEBUG_LIFECYCLE = false;
    public static final int DEFAULT_KEYS_DIALER = 1;
    public static final int DEFAULT_KEYS_DISABLE = 0;
    public static final int DEFAULT_KEYS_SEARCH_GLOBAL = 4;
    public static final int DEFAULT_KEYS_SEARCH_LOCAL = 3;
    public static final int DEFAULT_KEYS_SHORTCUT = 2;
    protected static final int[] FOCUSED_STATE_SET = arrayOfInt;
    private static final String FRAGMENTS_TAG = "android:fragments";
    public static final int RESULT_CANCELED = 0;
    public static final int RESULT_FIRST_USER = 1;
    public static final int RESULT_OK = -1;
    private static final String SAVED_DIALOGS_TAG = "android:savedDialogs";
    private static final String SAVED_DIALOG_ARGS_KEY_PREFIX = "android:dialog_args_";
    private static final String SAVED_DIALOG_IDS_KEY = "android:savedDialogIds";
    private static final String SAVED_DIALOG_KEY_PREFIX = "android:dialog_";
    private static final String TAG = "Activity";
    private static final String WINDOW_HIERARCHY_TAG = "android:viewHierarchyState";
    ActionBarImpl mActionBar = null;
    ActivityInfo mActivityInfo;
    SparseArray<LoaderManagerImpl> mAllLoaderManagers;
    private Application mApplication;
    boolean mCalled;
    boolean mChangingConfigurations = false;
    boolean mCheckedForLoaderManager;
    private ComponentName mComponent;
    int mConfigChangeFlags;
    Configuration mCurrentConfig;
    View mDecor = null;
    private int mDefaultKeyMode = 0;
    private SpannableStringBuilder mDefaultKeySsb = null;
    String mEmbeddedID;
    private boolean mEnableDefaultActionBarUp;
    boolean mFinished;
    final FragmentManagerImpl mFragments = new FragmentManagerImpl();
    final Handler mHandler = new Handler();
    private int mIdent;
    private final Object mInstanceTracker = StrictMode.trackActivity(this);
    private Instrumentation mInstrumentation;
    Intent mIntent;
    NonConfigurationInstances mLastNonConfigurationInstances;
    LoaderManagerImpl mLoaderManager;
    boolean mLoadersStarted;
    ActivityThread mMainThread;
    private final ArrayList<ManagedCursor> mManagedCursors = new ArrayList();
    private SparseArray<ManagedDialog> mManagedDialogs;
    private MenuInflater mMenuInflater;
    Activity mParent;
    int mResultCode = 0;
    Intent mResultData = null;
    boolean mResumed;
    private SearchManager mSearchManager;
    boolean mStartedActivity;
    private boolean mStopped;
    boolean mTemporaryPause = false;
    private CharSequence mTitle;
    private int mTitleColor = 0;
    private boolean mTitleReady = false;
    private IBinder mToken;
    private Thread mUiThread;
    boolean mVisibleFromClient = true;
    boolean mVisibleFromServer = false;
    private Window mWindow;
    boolean mWindowAdded = false;
    private WindowManager mWindowManager;

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842908;
    }

    private Dialog createDialog(Integer paramInteger, Bundle paramBundle1, Bundle paramBundle2)
    {
        Dialog localDialog = onCreateDialog(paramInteger.intValue(), paramBundle2);
        if (localDialog == null)
            localDialog = null;
        while (true)
        {
            return localDialog;
            localDialog.dispatchOnCreate(paramBundle1);
        }
    }

    private void ensureSearchManager()
    {
        if (this.mSearchManager != null);
        while (true)
        {
            return;
            this.mSearchManager = new SearchManager(this, null);
        }
    }

    private void initActionBar()
    {
        Window localWindow = getWindow();
        localWindow.getDecorView();
        if ((isChild()) || (!localWindow.hasFeature(8)) || (this.mActionBar != null));
        while (true)
        {
            return;
            this.mActionBar = new ActionBarImpl(this);
            this.mActionBar.setDefaultDisplayHomeAsUpEnabled(this.mEnableDefaultActionBarUp);
        }
    }

    private IllegalArgumentException missingDialog(int paramInt)
    {
        return new IllegalArgumentException("no dialog with id " + paramInt + " was ever " + "shown via Activity#showDialog");
    }

    private void restoreManagedDialogs(Bundle paramBundle)
    {
        Bundle localBundle1 = paramBundle.getBundle("android:savedDialogs");
        if (localBundle1 == null);
        while (true)
        {
            return;
            int[] arrayOfInt = localBundle1.getIntArray("android:savedDialogIds");
            int i = arrayOfInt.length;
            this.mManagedDialogs = new SparseArray(i);
            for (int j = 0; j < i; j++)
            {
                Integer localInteger = Integer.valueOf(arrayOfInt[j]);
                Bundle localBundle2 = localBundle1.getBundle(savedDialogKeyFor(localInteger.intValue()));
                if (localBundle2 != null)
                {
                    ManagedDialog localManagedDialog = new ManagedDialog(null);
                    localManagedDialog.mArgs = localBundle1.getBundle(savedDialogArgsKeyFor(localInteger.intValue()));
                    localManagedDialog.mDialog = createDialog(localInteger, localBundle2, localManagedDialog.mArgs);
                    if (localManagedDialog.mDialog != null)
                    {
                        this.mManagedDialogs.put(localInteger.intValue(), localManagedDialog);
                        onPrepareDialog(localInteger.intValue(), localManagedDialog.mDialog, localManagedDialog.mArgs);
                        localManagedDialog.mDialog.onRestoreInstanceState(localBundle2);
                    }
                }
            }
        }
    }

    private void saveManagedDialogs(Bundle paramBundle)
    {
        if (this.mManagedDialogs == null);
        while (true)
        {
            return;
            int i = this.mManagedDialogs.size();
            if (i != 0)
            {
                Bundle localBundle = new Bundle();
                int[] arrayOfInt = new int[this.mManagedDialogs.size()];
                for (int j = 0; j < i; j++)
                {
                    int k = this.mManagedDialogs.keyAt(j);
                    arrayOfInt[j] = k;
                    ManagedDialog localManagedDialog = (ManagedDialog)this.mManagedDialogs.valueAt(j);
                    localBundle.putBundle(savedDialogKeyFor(k), localManagedDialog.mDialog.onSaveInstanceState());
                    if (localManagedDialog.mArgs != null)
                        localBundle.putBundle(savedDialogArgsKeyFor(k), localManagedDialog.mArgs);
                }
                localBundle.putIntArray("android:savedDialogIds", arrayOfInt);
                paramBundle.putBundle("android:savedDialogs", localBundle);
            }
        }
    }

    private static String savedDialogArgsKeyFor(int paramInt)
    {
        return "android:dialog_args_" + paramInt;
    }

    private static String savedDialogKeyFor(int paramInt)
    {
        return "android:dialog_" + paramInt;
    }

    private void startIntentSenderForResultInner(IntentSender paramIntentSender, int paramInt1, Intent paramIntent, int paramInt2, int paramInt3, Activity paramActivity, Bundle paramBundle)
        throws IntentSender.SendIntentException
    {
        String str = null;
        if (paramIntent != null);
        int i;
        try
        {
            paramIntent.setAllowFds(false);
            str = paramIntent.resolveTypeIfNeeded(getContentResolver());
            i = ActivityManagerNative.getDefault().startActivityIntentSender(this.mMainThread.getApplicationThread(), paramIntentSender, paramIntent, str, this.mToken, paramActivity.mEmbeddedID, paramInt1, paramInt2, paramInt3, paramBundle);
            if (i == -6)
                throw new IntentSender.SendIntentException();
        }
        catch (RemoteException localRemoteException)
        {
        }
        while (true)
        {
            if (paramInt1 >= 0)
                this.mStartedActivity = true;
            return;
            Instrumentation.checkStartActivityResult(i, null);
        }
    }

    public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        getWindow().addContentView(paramView, paramLayoutParams);
        initActionBar();
    }

    final void attach(Context paramContext, ActivityThread paramActivityThread, Instrumentation paramInstrumentation, IBinder paramIBinder, int paramInt, Application paramApplication, Intent paramIntent, ActivityInfo paramActivityInfo, CharSequence paramCharSequence, Activity paramActivity, String paramString, NonConfigurationInstances paramNonConfigurationInstances, Configuration paramConfiguration)
    {
        attachBaseContext(paramContext);
        this.mFragments.attachActivity(this);
        this.mWindow = PolicyManager.makeNewWindow(this);
        this.mWindow.setCallback(this);
        this.mWindow.getLayoutInflater().setPrivateFactory(this);
        if (paramActivityInfo.softInputMode != 0)
            this.mWindow.setSoftInputMode(paramActivityInfo.softInputMode);
        if (paramActivityInfo.uiOptions != 0)
            this.mWindow.setUiOptions(paramActivityInfo.uiOptions);
        this.mUiThread = Thread.currentThread();
        this.mMainThread = paramActivityThread;
        this.mInstrumentation = paramInstrumentation;
        this.mToken = paramIBinder;
        this.mIdent = paramInt;
        this.mApplication = paramApplication;
        this.mIntent = paramIntent;
        this.mComponent = paramIntent.getComponent();
        this.mActivityInfo = paramActivityInfo;
        this.mTitle = paramCharSequence;
        this.mParent = paramActivity;
        this.mEmbeddedID = paramString;
        this.mLastNonConfigurationInstances = paramNonConfigurationInstances;
        Window localWindow = this.mWindow;
        IBinder localIBinder = this.mToken;
        String str = this.mComponent.flattenToString();
        if ((0x200 & paramActivityInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
        {
            localWindow.setWindowManager(null, localIBinder, str, bool);
            if (this.mParent != null)
                this.mWindow.setContainer(this.mParent.getWindow());
            this.mWindowManager = this.mWindow.getWindowManager();
            this.mCurrentConfig = paramConfiguration;
            return;
        }
    }

    final void attach(Context paramContext, ActivityThread paramActivityThread, Instrumentation paramInstrumentation, IBinder paramIBinder, Application paramApplication, Intent paramIntent, ActivityInfo paramActivityInfo, CharSequence paramCharSequence, Activity paramActivity, String paramString, NonConfigurationInstances paramNonConfigurationInstances, Configuration paramConfiguration)
    {
        attach(paramContext, paramActivityThread, paramInstrumentation, paramIBinder, 0, paramApplication, paramIntent, paramActivityInfo, paramCharSequence, paramActivity, paramString, paramNonConfigurationInstances, paramConfiguration);
    }

    public void closeContextMenu()
    {
        this.mWindow.closePanel(6);
    }

    public void closeOptionsMenu()
    {
        this.mWindow.closePanel(0);
    }

    public PendingIntent createPendingResult(int paramInt1, Intent paramIntent, int paramInt2)
    {
        String str1 = getPackageName();
        PendingIntent localPendingIntent;
        try
        {
            paramIntent.setAllowFds(false);
            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
            if (this.mParent == null);
            for (IBinder localIBinder = this.mToken; ; localIBinder = this.mParent.mToken)
            {
                String str2 = this.mEmbeddedID;
                Intent[] arrayOfIntent = new Intent[1];
                arrayOfIntent[0] = paramIntent;
                IIntentSender localIIntentSender = localIActivityManager.getIntentSender(3, str1, localIBinder, str2, paramInt1, arrayOfIntent, null, paramInt2, null);
                if (localIIntentSender == null)
                    break;
                localPendingIntent = new PendingIntent(localIIntentSender);
                break label110;
            }
            localPendingIntent = null;
        }
        catch (RemoteException localRemoteException)
        {
            localPendingIntent = null;
        }
        label110: return localPendingIntent;
    }

    @Deprecated
    public final void dismissDialog(int paramInt)
    {
        if (this.mManagedDialogs == null)
            throw missingDialog(paramInt);
        ManagedDialog localManagedDialog = (ManagedDialog)this.mManagedDialogs.get(paramInt);
        if (localManagedDialog == null)
            throw missingDialog(paramInt);
        localManagedDialog.mDialog.dismiss();
    }

    void dispatchActivityResult(String paramString, int paramInt1, int paramInt2, Intent paramIntent)
    {
        this.mFragments.noteStateNotSaved();
        if (paramString == null)
            onActivityResult(paramInt1, paramInt2, paramIntent);
        while (true)
        {
            return;
            Fragment localFragment = this.mFragments.findFragmentByWho(paramString);
            if (localFragment != null)
                localFragment.onActivityResult(paramInt1, paramInt2, paramIntent);
        }
    }

    public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        onUserInteraction();
        if (getWindow().superDispatchGenericMotionEvent(paramMotionEvent));
        for (boolean bool = true; ; bool = onGenericMotionEvent(paramMotionEvent))
            return bool;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        onUserInteraction();
        Window localWindow = getWindow();
        boolean bool;
        if (localWindow.superDispatchKeyEvent(paramKeyEvent))
        {
            bool = true;
            return bool;
        }
        View localView = this.mDecor;
        if (localView == null)
            localView = localWindow.getDecorView();
        if (localView != null);
        for (KeyEvent.DispatcherState localDispatcherState = localView.getKeyDispatcherState(); ; localDispatcherState = null)
        {
            bool = paramKeyEvent.dispatch(this, localDispatcherState, this);
            break;
        }
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
    {
        onUserInteraction();
        if (getWindow().superDispatchKeyShortcutEvent(paramKeyEvent));
        for (boolean bool = true; ; bool = onKeyShortcut(paramKeyEvent.getKeyCode(), paramKeyEvent))
            return bool;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        paramAccessibilityEvent.setClassName(getClass().getName());
        paramAccessibilityEvent.setPackageName(getPackageName());
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        if ((localLayoutParams.width == -1) && (localLayoutParams.height == -1));
        for (boolean bool = true; ; bool = false)
        {
            paramAccessibilityEvent.setFullScreen(bool);
            CharSequence localCharSequence = getTitle();
            if (!TextUtils.isEmpty(localCharSequence))
                paramAccessibilityEvent.getText().add(localCharSequence);
            return true;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        if (paramMotionEvent.getAction() == 0)
            onUserInteraction();
        if (getWindow().superDispatchTouchEvent(paramMotionEvent));
        for (boolean bool = true; ; bool = onTouchEvent(paramMotionEvent))
            return bool;
    }

    public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
    {
        onUserInteraction();
        if (getWindow().superDispatchTrackballEvent(paramMotionEvent));
        for (boolean bool = true; ; bool = onTrackballEvent(paramMotionEvent))
            return bool;
    }

    public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("Local Activity ");
        paramPrintWriter.print(Integer.toHexString(System.identityHashCode(this)));
        paramPrintWriter.println(" State:");
        String str = paramString + "    ";
        paramPrintWriter.print(str);
        paramPrintWriter.print("mResumed=");
        paramPrintWriter.print(this.mResumed);
        paramPrintWriter.print(" mStopped=");
        paramPrintWriter.print(this.mStopped);
        paramPrintWriter.print(" mFinished=");
        paramPrintWriter.println(this.mFinished);
        paramPrintWriter.print(str);
        paramPrintWriter.print("mLoadersStarted=");
        paramPrintWriter.println(this.mLoadersStarted);
        paramPrintWriter.print(str);
        paramPrintWriter.print("mChangingConfigurations=");
        paramPrintWriter.println(this.mChangingConfigurations);
        paramPrintWriter.print(str);
        paramPrintWriter.print("mCurrentConfig=");
        paramPrintWriter.println(this.mCurrentConfig);
        if (this.mLoaderManager != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("Loader Manager ");
            paramPrintWriter.print(Integer.toHexString(System.identityHashCode(this.mLoaderManager)));
            paramPrintWriter.println(":");
            this.mLoaderManager.dump(paramString + "    ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        }
        this.mFragments.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }

    public View findViewById(int paramInt)
    {
        return getWindow().findViewById(paramInt);
    }

    // ERROR //
    public void finish()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 492	android/app/Activity:mParent	Landroid/app/Activity;
        //     4: ifnonnull +54 -> 58
        //     7: aload_0
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 180	android/app/Activity:mResultCode	I
        //     13: istore_2
        //     14: aload_0
        //     15: getfield 182	android/app/Activity:mResultData	Landroid/content/Intent;
        //     18: astore_3
        //     19: aload_0
        //     20: monitorexit
        //     21: aload_3
        //     22: ifnull +8 -> 30
        //     25: aload_3
        //     26: iconst_0
        //     27: invokevirtual 373	android/content/Intent:setAllowFds	(Z)V
        //     30: invokestatic 387	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     33: aload_0
        //     34: getfield 397	android/app/Activity:mToken	Landroid/os/IBinder;
        //     37: iload_2
        //     38: aload_3
        //     39: invokeinterface 753 4 0
        //     44: ifeq +8 -> 52
        //     47: aload_0
        //     48: iconst_1
        //     49: putfield 720	android/app/Activity:mFinished	Z
        //     52: return
        //     53: astore_1
        //     54: aload_0
        //     55: monitorexit
        //     56: aload_1
        //     57: athrow
        //     58: aload_0
        //     59: getfield 492	android/app/Activity:mParent	Landroid/app/Activity;
        //     62: aload_0
        //     63: invokevirtual 756	android/app/Activity:finishFromChild	(Landroid/app/Activity;)V
        //     66: goto -14 -> 52
        //     69: astore 4
        //     71: goto -19 -> 52
        //
        // Exception table:
        //     from	to	target	type
        //     9	21	53	finally
        //     54	56	53	finally
        //     25	52	69	android/os/RemoteException
    }

    public void finishActivity(int paramInt)
    {
        if (this.mParent == null);
        try
        {
            ActivityManagerNative.getDefault().finishSubActivity(this.mToken, this.mEmbeddedID, paramInt);
            while (true)
            {
                label24: return;
                this.mParent.finishActivityFromChild(this, paramInt);
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label24;
        }
    }

    public void finishActivityFromChild(Activity paramActivity, int paramInt)
    {
        try
        {
            ActivityManagerNative.getDefault().finishSubActivity(this.mToken, paramActivity.mEmbeddedID, paramInt);
            label17: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label17;
        }
    }

    public void finishAffinity()
    {
        if (this.mParent != null)
            throw new IllegalStateException("Can not be called from an embedded activity");
        if ((this.mResultCode != 0) || (this.mResultData != null))
            throw new IllegalStateException("Can not be called to deliver a result");
        try
        {
            if (ActivityManagerNative.getDefault().finishActivityAffinity(this.mToken))
                this.mFinished = true;
            label63: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label63;
        }
    }

    public void finishFromChild(Activity paramActivity)
    {
        finish();
    }

    public ActionBar getActionBar()
    {
        initActionBar();
        return this.mActionBar;
    }

    public final IBinder getActivityToken()
    {
        if (this.mParent != null);
        for (IBinder localIBinder = this.mParent.getActivityToken(); ; localIBinder = this.mToken)
            return localIBinder;
    }

    public final Application getApplication()
    {
        return this.mApplication;
    }

    public ComponentName getCallingActivity()
    {
        try
        {
            ComponentName localComponentName2 = ActivityManagerNative.getDefault().getCallingActivity(this.mToken);
            localComponentName1 = localComponentName2;
            return localComponentName1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ComponentName localComponentName1 = null;
        }
    }

    public String getCallingPackage()
    {
        try
        {
            String str2 = ActivityManagerNative.getDefault().getCallingPackage(this.mToken);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String str1 = null;
        }
    }

    public int getChangingConfigurations()
    {
        return this.mConfigChangeFlags;
    }

    public ComponentName getComponentName()
    {
        return this.mComponent;
    }

    public View getCurrentFocus()
    {
        if (this.mWindow != null);
        for (View localView = this.mWindow.getCurrentFocus(); ; localView = null)
            return localView;
    }

    public FragmentManager getFragmentManager()
    {
        return this.mFragments;
    }

    public Intent getIntent()
    {
        return this.mIntent;
    }

    HashMap<String, Object> getLastNonConfigurationChildInstances()
    {
        if (this.mLastNonConfigurationInstances != null);
        for (HashMap localHashMap = this.mLastNonConfigurationInstances.children; ; localHashMap = null)
            return localHashMap;
    }

    @Deprecated
    public Object getLastNonConfigurationInstance()
    {
        if (this.mLastNonConfigurationInstances != null);
        for (Object localObject = this.mLastNonConfigurationInstances.activity; ; localObject = null)
            return localObject;
    }

    public LayoutInflater getLayoutInflater()
    {
        return getWindow().getLayoutInflater();
    }

    public LoaderManager getLoaderManager()
    {
        if (this.mLoaderManager != null);
        for (LoaderManagerImpl localLoaderManagerImpl = this.mLoaderManager; ; localLoaderManagerImpl = this.mLoaderManager)
        {
            return localLoaderManagerImpl;
            this.mCheckedForLoaderManager = true;
            this.mLoaderManager = getLoaderManager(-1, this.mLoadersStarted, true);
        }
    }

    LoaderManagerImpl getLoaderManager(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mAllLoaderManagers == null)
            this.mAllLoaderManagers = new SparseArray();
        LoaderManagerImpl localLoaderManagerImpl = (LoaderManagerImpl)this.mAllLoaderManagers.get(paramInt);
        if (localLoaderManagerImpl == null)
            if (paramBoolean2)
            {
                localLoaderManagerImpl = new LoaderManagerImpl(this, paramBoolean1);
                this.mAllLoaderManagers.put(paramInt, localLoaderManagerImpl);
            }
        while (true)
        {
            return localLoaderManagerImpl;
            localLoaderManagerImpl.updateActivity(this);
        }
    }

    public String getLocalClassName()
    {
        String str1 = getPackageName();
        String str2 = this.mComponent.getClassName();
        int i = str1.length();
        if ((!str2.startsWith(str1)) || (str2.length() <= i) || (str2.charAt(i) != '.'));
        while (true)
        {
            return str2;
            str2 = str2.substring(i + 1);
        }
    }

    public MenuInflater getMenuInflater()
    {
        if (this.mMenuInflater == null)
        {
            initActionBar();
            if (this.mActionBar == null)
                break label42;
        }
        label42: for (this.mMenuInflater = new MenuInflater(this.mActionBar.getThemedContext(), this); ; this.mMenuInflater = new MenuInflater(this))
            return this.mMenuInflater;
    }

    public final Activity getParent()
    {
        return this.mParent;
    }

    public Intent getParentActivityIntent()
    {
        String str = this.mActivityInfo.parentActivityName;
        if (TextUtils.isEmpty(str));
        for (Intent localIntent = null; ; localIntent = new Intent().setClassName(this, str))
            return localIntent;
    }

    public SharedPreferences getPreferences(int paramInt)
    {
        return getSharedPreferences(getLocalClassName(), paramInt);
    }

    public int getRequestedOrientation()
    {
        if (this.mParent == null);
        try
        {
            int j = ActivityManagerNative.getDefault().getRequestedOrientation(this.mToken);
            for (i = j; ; i = this.mParent.getRequestedOrientation())
                return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public Object getSystemService(String paramString)
    {
        if (getBaseContext() == null)
            throw new IllegalStateException("System services not available to Activities before onCreate()");
        Object localObject;
        if ("window".equals(paramString))
            localObject = this.mWindowManager;
        while (true)
        {
            return localObject;
            if ("search".equals(paramString))
            {
                ensureSearchManager();
                localObject = this.mSearchManager;
            }
            else
            {
                localObject = super.getSystemService(paramString);
            }
        }
    }

    public int getTaskId()
    {
        try
        {
            int j = ActivityManagerNative.getDefault().getTaskForActivity(this.mToken, false);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public final CharSequence getTitle()
    {
        return this.mTitle;
    }

    public final int getTitleColor()
    {
        return this.mTitleColor;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    IBinder getToken()
    {
        return this.mToken;
    }

    public final int getVolumeControlStream()
    {
        return getWindow().getVolumeControlStream();
    }

    public Window getWindow()
    {
        return this.mWindow;
    }

    public WindowManager getWindowManager()
    {
        return this.mWindowManager;
    }

    public boolean hasWindowFocus()
    {
        Window localWindow = getWindow();
        View localView;
        if (localWindow != null)
        {
            localView = localWindow.getDecorView();
            if (localView == null);
        }
        for (boolean bool = localView.hasWindowFocus(); ; bool = false)
            return bool;
    }

    void invalidateFragmentIndex(int paramInt)
    {
        if (this.mAllLoaderManagers != null)
        {
            LoaderManagerImpl localLoaderManagerImpl = (LoaderManagerImpl)this.mAllLoaderManagers.get(paramInt);
            if ((localLoaderManagerImpl != null) && (!localLoaderManagerImpl.mRetaining))
            {
                localLoaderManagerImpl.doDestroy();
                this.mAllLoaderManagers.remove(paramInt);
            }
        }
    }

    public void invalidateOptionsMenu()
    {
        this.mWindow.invalidatePanelMenu(0);
    }

    public boolean isChangingConfigurations()
    {
        return this.mChangingConfigurations;
    }

    public final boolean isChild()
    {
        if (this.mParent != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isFinishing()
    {
        return this.mFinished;
    }

    public boolean isImmersive()
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().isImmersive(this.mToken);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public final boolean isResumed()
    {
        return this.mResumed;
    }

    public boolean isTaskRoot()
    {
        boolean bool = true;
        try
        {
            int i = ActivityManagerNative.getDefault().getTaskForActivity(this.mToken, true);
            if (i >= 0);
            while (true)
            {
                return bool;
                bool = false;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                bool = false;
        }
    }

    void makeVisible()
    {
        if (!this.mWindowAdded)
        {
            getWindowManager().addView(this.mDecor, getWindow().getAttributes());
            this.mWindowAdded = true;
        }
        this.mDecor.setVisibility(0);
    }

    @Deprecated
    public final Cursor managedQuery(Uri paramUri, String[] paramArrayOfString, String paramString1, String paramString2)
    {
        Cursor localCursor = getContentResolver().query(paramUri, paramArrayOfString, paramString1, null, paramString2);
        if (localCursor != null)
            startManagingCursor(localCursor);
        return localCursor;
    }

    @Deprecated
    public final Cursor managedQuery(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        Cursor localCursor = getContentResolver().query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2);
        if (localCursor != null)
            startManagingCursor(localCursor);
        return localCursor;
    }

    public boolean moveTaskToBack(boolean paramBoolean)
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().moveActivityTaskToBack(this.mToken, paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    // ERROR //
    public boolean navigateUpTo(Intent paramIntent)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aload_0
        //     3: getfield 492	android/app/Activity:mParent	Landroid/app/Activity;
        //     6: ifnonnull +110 -> 116
        //     9: aload_1
        //     10: invokevirtual 484	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     13: ifnonnull +41 -> 54
        //     16: aload_1
        //     17: aload_0
        //     18: invokevirtual 981	android/app/Activity:getPackageManager	()Landroid/content/pm/PackageManager;
        //     21: invokevirtual 985	android/content/Intent:resolveActivity	(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
        //     24: astore 8
        //     26: aload 8
        //     28: ifnonnull +5 -> 33
        //     31: iload_2
        //     32: ireturn
        //     33: new 370	android/content/Intent
        //     36: dup
        //     37: aload_1
        //     38: invokespecial 988	android/content/Intent:<init>	(Landroid/content/Intent;)V
        //     41: astore 9
        //     43: aload 9
        //     45: aload 8
        //     47: invokevirtual 992	android/content/Intent:setComponent	(Landroid/content/ComponentName;)Landroid/content/Intent;
        //     50: pop
        //     51: aload 9
        //     53: astore_1
        //     54: aload_0
        //     55: monitorenter
        //     56: aload_0
        //     57: getfield 180	android/app/Activity:mResultCode	I
        //     60: istore 4
        //     62: aload_0
        //     63: getfield 182	android/app/Activity:mResultData	Landroid/content/Intent;
        //     66: astore 5
        //     68: aload_0
        //     69: monitorexit
        //     70: aload 5
        //     72: ifnull +9 -> 81
        //     75: aload 5
        //     77: iconst_0
        //     78: invokevirtual 373	android/content/Intent:setAllowFds	(Z)V
        //     81: invokestatic 387	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     84: aload_0
        //     85: getfield 397	android/app/Activity:mToken	Landroid/os/IBinder;
        //     88: aload_1
        //     89: iload 4
        //     91: aload 5
        //     93: invokeinterface 995 5 0
        //     98: istore 7
        //     100: iload 7
        //     102: istore_2
        //     103: goto -72 -> 31
        //     106: astore_3
        //     107: aload_0
        //     108: monitorexit
        //     109: aload_3
        //     110: athrow
        //     111: astore 6
        //     113: goto -82 -> 31
        //     116: aload_0
        //     117: getfield 492	android/app/Activity:mParent	Landroid/app/Activity;
        //     120: aload_0
        //     121: aload_1
        //     122: invokevirtual 999	android/app/Activity:navigateUpToFromChild	(Landroid/app/Activity;Landroid/content/Intent;)Z
        //     125: istore_2
        //     126: goto -95 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     56	70	106	finally
        //     107	109	106	finally
        //     81	100	111	android/os/RemoteException
    }

    public boolean navigateUpToFromChild(Activity paramActivity, Intent paramIntent)
    {
        return navigateUpTo(paramIntent);
    }

    public void onActionModeFinished(ActionMode paramActionMode)
    {
    }

    public void onActionModeStarted(ActionMode paramActionMode)
    {
    }

    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
    }

    protected void onApplyThemeResource(Resources.Theme paramTheme, int paramInt, boolean paramBoolean)
    {
        if (this.mParent == null)
            super.onApplyThemeResource(paramTheme, paramInt, paramBoolean);
        while (true)
        {
            return;
            try
            {
                paramTheme.setTo(this.mParent.getTheme());
                label26: paramTheme.applyStyle(paramInt, false);
            }
            catch (Exception localException)
            {
                break label26;
            }
        }
    }

    public void onAttachFragment(Fragment paramFragment)
    {
    }

    public void onAttachedToWindow()
    {
    }

    public void onBackPressed()
    {
        if (!this.mFragments.popBackStackImmediate())
            finish();
    }

    protected void onChildTitleChanged(Activity paramActivity, CharSequence paramCharSequence)
    {
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        this.mCalled = true;
        this.mFragments.dispatchConfigurationChanged(paramConfiguration);
        if (this.mWindow != null)
            this.mWindow.onConfigurationChanged(paramConfiguration);
        if (this.mActionBar != null)
            this.mActionBar.onConfigurationChanged(paramConfiguration);
    }

    public void onContentChanged()
    {
    }

    public boolean onContextItemSelected(MenuItem paramMenuItem)
    {
        if (this.mParent != null);
        for (boolean bool = this.mParent.onContextItemSelected(paramMenuItem); ; bool = false)
            return bool;
    }

    public void onContextMenuClosed(Menu paramMenu)
    {
        if (this.mParent != null)
            this.mParent.onContextMenuClosed(paramMenu);
    }

    protected void onCreate(Bundle paramBundle)
    {
        if (this.mLastNonConfigurationInstances != null)
            this.mAllLoaderManagers = this.mLastNonConfigurationInstances.loaders;
        Parcelable localParcelable;
        FragmentManagerImpl localFragmentManagerImpl;
        if (this.mActivityInfo.parentActivityName != null)
        {
            if (this.mActionBar == null)
                this.mEnableDefaultActionBarUp = true;
        }
        else if (paramBundle != null)
        {
            localParcelable = paramBundle.getParcelable("android:fragments");
            localFragmentManagerImpl = this.mFragments;
            if (this.mLastNonConfigurationInstances == null)
                break label112;
        }
        label112: for (ArrayList localArrayList = this.mLastNonConfigurationInstances.fragments; ; localArrayList = null)
        {
            localFragmentManagerImpl.restoreAllState(localParcelable, localArrayList);
            this.mFragments.dispatchCreate();
            getApplication().dispatchActivityCreated(this, paramBundle);
            this.mCalled = true;
            return;
            this.mActionBar.setDefaultDisplayHomeAsUpEnabled(true);
            break;
        }
    }

    public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
    {
    }

    public CharSequence onCreateDescription()
    {
        return null;
    }

    @Deprecated
    protected Dialog onCreateDialog(int paramInt)
    {
        return null;
    }

    @Deprecated
    protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
    {
        return onCreateDialog(paramInt);
    }

    public void onCreateNavigateUpTaskStack(TaskStackBuilder paramTaskStackBuilder)
    {
        paramTaskStackBuilder.addParentStack(this);
    }

    public boolean onCreateOptionsMenu(Menu paramMenu)
    {
        if (this.mParent != null);
        for (boolean bool = this.mParent.onCreateOptionsMenu(paramMenu); ; bool = true)
            return bool;
    }

    public boolean onCreatePanelMenu(int paramInt, Menu paramMenu)
    {
        if (paramInt == 0);
        for (boolean bool = onCreateOptionsMenu(paramMenu) | this.mFragments.dispatchCreateOptionsMenu(paramMenu, getMenuInflater()); ; bool = false)
            return bool;
    }

    public View onCreatePanelView(int paramInt)
    {
        return null;
    }

    public boolean onCreateThumbnail(Bitmap paramBitmap, Canvas paramCanvas)
    {
        return false;
    }

    public View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
    {
        Fragment localFragment = null;
        int i = 0;
        if (!"fragment".equals(paramString));
        for (View localView = onCreateView(paramString, paramContext, paramAttributeSet); ; localView = localFragment.mView)
        {
            return localView;
            String str1 = paramAttributeSet.getAttributeValue(null, "class");
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Fragment);
            if (str1 == null)
                str1 = localTypedArray.getString(0);
            int j = localTypedArray.getResourceId(1, -1);
            String str2 = localTypedArray.getString(2);
            localTypedArray.recycle();
            if (paramView != null)
                i = paramView.getId();
            if ((i == -1) && (j == -1) && (str2 == null))
                throw new IllegalArgumentException(paramAttributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str1);
            if (j != -1)
                localFragment = this.mFragments.findFragmentById(j);
            if ((localFragment == null) && (str2 != null))
                localFragment = this.mFragments.findFragmentByTag(str2);
            if ((localFragment == null) && (i != -1))
                localFragment = this.mFragments.findFragmentById(i);
            if (FragmentManagerImpl.DEBUG)
                Log.v("Activity", "onCreateView: id=0x" + Integer.toHexString(j) + " fname=" + str1 + " existing=" + localFragment);
            int k;
            if (localFragment == null)
            {
                localFragment = Fragment.instantiate(this, str1);
                localFragment.mFromLayout = true;
                if (j != 0)
                {
                    k = j;
                    localFragment.mFragmentId = k;
                    localFragment.mContainerId = i;
                    localFragment.mTag = str2;
                    localFragment.mInLayout = true;
                    localFragment.mFragmentManager = this.mFragments;
                    localFragment.onInflate(this, paramAttributeSet, localFragment.mSavedFragmentState);
                    this.mFragments.addFragment(localFragment, true);
                }
            }
            while (true)
            {
                if (localFragment.mView != null)
                    break label539;
                throw new IllegalStateException("Fragment " + str1 + " did not create a view.");
                k = i;
                break;
                if (localFragment.mInLayout)
                    throw new IllegalArgumentException(paramAttributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(j) + ", tag " + str2 + ", or parent id 0x" + Integer.toHexString(i) + " with another fragment for " + str1);
                localFragment.mInLayout = true;
                if (!localFragment.mRetaining)
                    localFragment.onInflate(this, paramAttributeSet, localFragment.mSavedFragmentState);
                this.mFragments.moveToState(localFragment);
            }
            label539: if (j != 0)
                localFragment.mView.setId(j);
            if (localFragment.mView.getTag() == null)
                localFragment.mView.setTag(str2);
        }
    }

    public View onCreateView(String paramString, Context paramContext, AttributeSet paramAttributeSet)
    {
        return null;
    }

    protected void onDestroy()
    {
        this.mCalled = true;
        if (this.mManagedDialogs != null)
        {
            int k = this.mManagedDialogs.size();
            for (int m = 0; m < k; m++)
            {
                ManagedDialog localManagedDialog = (ManagedDialog)this.mManagedDialogs.valueAt(m);
                if (localManagedDialog.mDialog.isShowing())
                    localManagedDialog.mDialog.dismiss();
            }
            this.mManagedDialogs = null;
        }
        while (true)
        {
            int j;
            synchronized (this.mManagedCursors)
            {
                int i = this.mManagedCursors.size();
                j = 0;
                if (j < i)
                {
                    ManagedCursor localManagedCursor = (ManagedCursor)this.mManagedCursors.get(j);
                    if (localManagedCursor != null)
                        localManagedCursor.mCursor.close();
                }
                else
                {
                    this.mManagedCursors.clear();
                    if (this.mSearchManager != null)
                        this.mSearchManager.stopSearch();
                    getApplication().dispatchActivityDestroyed(this);
                    return;
                }
            }
            j++;
        }
    }

    public void onDetachedFromWindow()
    {
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool;
        if (paramInt == 4)
            if (getApplicationInfo().targetSdkVersion >= 5)
            {
                paramKeyEvent.startTracking();
                bool = true;
            }
        label283: 
        while (true)
        {
            return bool;
            onBackPressed();
            break;
            if (this.mDefaultKeyMode == 0)
            {
                bool = false;
            }
            else if (this.mDefaultKeyMode == 2)
            {
                if (getWindow().performPanelShortcut(0, paramInt, paramKeyEvent, 2))
                    bool = true;
                else
                    bool = false;
            }
            else
            {
                int i = 0;
                if ((paramKeyEvent.getRepeatCount() != 0) || (paramKeyEvent.isSystem()))
                {
                    i = 1;
                    bool = false;
                }
                while (true)
                {
                    if (i == 0)
                        break label283;
                    this.mDefaultKeySsb.clear();
                    this.mDefaultKeySsb.clearSpans();
                    Selection.setSelection(this.mDefaultKeySsb, 0);
                    break;
                    bool = TextKeyListener.getInstance().onKeyDown(null, this.mDefaultKeySsb, paramInt, paramKeyEvent);
                    if ((bool) && (this.mDefaultKeySsb.length() > 0))
                    {
                        String str = this.mDefaultKeySsb.toString();
                        i = 1;
                        switch (this.mDefaultKeyMode)
                        {
                        case 2:
                        default:
                            break;
                        case 1:
                            Intent localIntent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str));
                            localIntent.addFlags(268435456);
                            startActivity(localIntent);
                            break;
                        case 3:
                            startSearch(str, false, null, false);
                            break;
                        case 4:
                            startSearch(str, false, null, true);
                        }
                    }
                }
            }
        }
    }

    public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((getApplicationInfo().targetSdkVersion >= 5) && (paramInt == 4) && (paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
            onBackPressed();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onLowMemory()
    {
        this.mCalled = true;
        this.mFragments.dispatchLowMemory();
    }

    public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
            i = 0;
        case 0:
        case 6:
        }
        while (true)
        {
            return i;
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = Integer.valueOf(0);
            arrayOfObject2[i] = paramMenuItem.getTitleCondensed();
            EventLog.writeEvent(50000, arrayOfObject2);
            if ((!onOptionsItemSelected(paramMenuItem)) && (!this.mFragments.dispatchOptionsItemSelected(paramMenuItem)))
                if ((paramMenuItem.getItemId() == 16908332) && (this.mActionBar != null) && ((0x4 & this.mActionBar.getDisplayOptions()) != 0))
                {
                    if (this.mParent == null)
                        i = onNavigateUp();
                    else
                        i = this.mParent.onNavigateUpFromChild(this);
                }
                else
                {
                    i = 0;
                    continue;
                    Object[] arrayOfObject1 = new Object[2];
                    arrayOfObject1[0] = Integer.valueOf(i);
                    arrayOfObject1[i] = paramMenuItem.getTitleCondensed();
                    EventLog.writeEvent(50000, arrayOfObject1);
                    if (!onContextItemSelected(paramMenuItem))
                        boolean bool = this.mFragments.dispatchContextItemSelected(paramMenuItem);
                }
        }
    }

    public boolean onMenuOpened(int paramInt, Menu paramMenu)
    {
        if (paramInt == 8)
        {
            initActionBar();
            if (this.mActionBar == null)
                break label27;
            this.mActionBar.dispatchMenuVisibilityChanged(true);
        }
        while (true)
        {
            return true;
            label27: Log.e("Activity", "Tried to open action bar menu with no action bar");
        }
    }

    public boolean onNavigateUp()
    {
        Intent localIntent = getParentActivityIntent();
        if (localIntent != null)
            if (shouldUpRecreateTask(localIntent))
            {
                TaskStackBuilder localTaskStackBuilder = TaskStackBuilder.create(this);
                onCreateNavigateUpTaskStack(localTaskStackBuilder);
                onPrepareNavigateUpTaskStack(localTaskStackBuilder);
                localTaskStackBuilder.startActivities();
                if ((this.mResultCode != 0) || (this.mResultData != null))
                {
                    Log.i("Activity", "onNavigateUp only finishing topmost activity to return a result");
                    finish();
                }
            }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            finishAffinity();
            break;
            navigateUpTo(localIntent);
            break;
        }
    }

    public boolean onNavigateUpFromChild(Activity paramActivity)
    {
        return onNavigateUp();
    }

    protected void onNewIntent(Intent paramIntent)
    {
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem)
    {
        if (this.mParent != null);
        for (boolean bool = this.mParent.onOptionsItemSelected(paramMenuItem); ; bool = false)
            return bool;
    }

    public void onOptionsMenuClosed(Menu paramMenu)
    {
        if (this.mParent != null)
            this.mParent.onOptionsMenuClosed(paramMenu);
    }

    public void onPanelClosed(int paramInt, Menu paramMenu)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 6:
        case 8:
        }
        while (true)
        {
            return;
            this.mFragments.dispatchOptionsMenuClosed(paramMenu);
            onOptionsMenuClosed(paramMenu);
            continue;
            onContextMenuClosed(paramMenu);
            continue;
            initActionBar();
            this.mActionBar.dispatchMenuVisibilityChanged(false);
        }
    }

    protected void onPause()
    {
        getApplication().dispatchActivityPaused(this);
        this.mCalled = true;
    }

    protected void onPostCreate(Bundle paramBundle)
    {
        if (!isChild())
        {
            this.mTitleReady = true;
            onTitleChanged(getTitle(), getTitleColor());
        }
        this.mCalled = true;
    }

    protected void onPostResume()
    {
        Window localWindow = getWindow();
        if (localWindow != null)
            localWindow.makeActive();
        if (this.mActionBar != null)
            this.mActionBar.setShowHideAnimationEnabled(true);
        this.mCalled = true;
    }

    @Deprecated
    protected void onPrepareDialog(int paramInt, Dialog paramDialog)
    {
        paramDialog.setOwnerActivity(this);
    }

    @Deprecated
    protected void onPrepareDialog(int paramInt, Dialog paramDialog, Bundle paramBundle)
    {
        onPrepareDialog(paramInt, paramDialog);
    }

    public void onPrepareNavigateUpTaskStack(TaskStackBuilder paramTaskStackBuilder)
    {
    }

    public boolean onPrepareOptionsMenu(Menu paramMenu)
    {
        if (this.mParent != null);
        for (boolean bool = this.mParent.onPrepareOptionsMenu(paramMenu); ; bool = true)
            return bool;
    }

    public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu)
    {
        if ((paramInt == 0) && (paramMenu != null));
        for (boolean bool = onPrepareOptionsMenu(paramMenu) | this.mFragments.dispatchPrepareOptionsMenu(paramMenu); ; bool = true)
            return bool;
    }

    protected void onRestart()
    {
        this.mCalled = true;
    }

    protected void onRestoreInstanceState(Bundle paramBundle)
    {
        if (this.mWindow != null)
        {
            Bundle localBundle = paramBundle.getBundle("android:viewHierarchyState");
            if (localBundle != null)
                this.mWindow.restoreHierarchyState(localBundle);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onResume()
    {
        getApplication().dispatchActivityResumed(this);
        this.mCalled = true;
        Injector.checkAccessControl(this);
    }

    HashMap<String, Object> onRetainNonConfigurationChildInstances()
    {
        return null;
    }

    public Object onRetainNonConfigurationInstance()
    {
        return null;
    }

    protected void onSaveInstanceState(Bundle paramBundle)
    {
        paramBundle.putBundle("android:viewHierarchyState", this.mWindow.saveHierarchyState());
        Parcelable localParcelable = this.mFragments.saveAllState();
        if (localParcelable != null)
            paramBundle.putParcelable("android:fragments", localParcelable);
        getApplication().dispatchActivitySaveInstanceState(this, paramBundle);
    }

    public boolean onSearchRequested()
    {
        startSearch(null, false, null, false);
        return true;
    }

    protected void onStart()
    {
        this.mCalled = true;
        if (!this.mLoadersStarted)
        {
            this.mLoadersStarted = true;
            if (this.mLoaderManager == null)
                break label45;
            this.mLoaderManager.doStart();
        }
        while (true)
        {
            this.mCheckedForLoaderManager = true;
            getApplication().dispatchActivityStarted(this);
            return;
            label45: if (!this.mCheckedForLoaderManager)
                this.mLoaderManager = getLoaderManager(-1, this.mLoadersStarted, false);
        }
    }

    protected void onStop()
    {
        if (this.mActionBar != null)
            this.mActionBar.setShowHideAnimationEnabled(false);
        getApplication().dispatchActivityStopped(this);
        this.mCalled = true;
    }

    protected void onTitleChanged(CharSequence paramCharSequence, int paramInt)
    {
        if (this.mTitleReady)
        {
            Window localWindow = getWindow();
            if (localWindow != null)
            {
                localWindow.setTitle(paramCharSequence);
                if (paramInt != 0)
                    localWindow.setTitleColor(paramInt);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mWindow.shouldCloseOnTouch(this, paramMotionEvent))
            finish();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    public void onTrimMemory(int paramInt)
    {
        this.mCalled = true;
        this.mFragments.dispatchTrimMemory(paramInt);
    }

    public void onUserInteraction()
    {
    }

    protected void onUserLeaveHint()
    {
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams)
    {
        if (this.mParent == null)
        {
            View localView = this.mDecor;
            if ((localView != null) && (localView.getParent() != null))
                getWindowManager().updateViewLayout(localView, paramLayoutParams);
        }
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
    }

    public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback)
    {
        initActionBar();
        if (this.mActionBar != null);
        for (ActionMode localActionMode = this.mActionBar.startActionMode(paramCallback); ; localActionMode = null)
            return localActionMode;
    }

    public void openContextMenu(View paramView)
    {
        paramView.showContextMenu();
    }

    public void openOptionsMenu()
    {
        this.mWindow.openPanel(0, null);
    }

    public void overridePendingTransition(int paramInt1, int paramInt2)
    {
        try
        {
            ActivityManagerNative.getDefault().overridePendingTransition(this.mToken, getPackageName(), paramInt1, paramInt2);
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    final void performCreate(Bundle paramBundle)
    {
        boolean bool = false;
        onCreate(paramBundle);
        if (!this.mWindow.getWindowStyle().getBoolean(10, false))
            bool = true;
        this.mVisibleFromClient = bool;
        this.mFragments.dispatchActivityCreated();
    }

    final void performDestroy()
    {
        this.mWindow.destroy();
        this.mFragments.dispatchDestroy();
        onDestroy();
        if (this.mLoaderManager != null)
            this.mLoaderManager.doDestroy();
    }

    final void performPause()
    {
        this.mFragments.dispatchPause();
        this.mCalled = false;
        onPause();
        this.mResumed = false;
        if ((!this.mCalled) && (getApplicationInfo().targetSdkVersion >= 9))
            throw new SuperNotCalledException("Activity " + this.mComponent.toShortString() + " did not call through to super.onPause()");
        this.mResumed = false;
    }

    // ERROR //
    final void performRestart()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 173	android/app/Activity:mFragments	Landroid/app/FragmentManagerImpl;
        //     4: invokevirtual 555	android/app/FragmentManagerImpl:noteStateNotSaved	()V
        //     7: aload_0
        //     8: getfield 716	android/app/Activity:mStopped	Z
        //     11: ifeq +232 -> 243
        //     14: aload_0
        //     15: iconst_0
        //     16: putfield 716	android/app/Activity:mStopped	Z
        //     19: aload_0
        //     20: getfield 397	android/app/Activity:mToken	Landroid/os/IBinder;
        //     23: ifnull +21 -> 44
        //     26: aload_0
        //     27: getfield 492	android/app/Activity:mParent	Landroid/app/Activity;
        //     30: ifnonnull +14 -> 44
        //     33: invokestatic 1592	android/view/WindowManagerImpl:getDefault	()Landroid/view/WindowManagerImpl;
        //     36: aload_0
        //     37: getfield 397	android/app/Activity:mToken	Landroid/os/IBinder;
        //     40: iconst_0
        //     41: invokevirtual 1596	android/view/WindowManagerImpl:setStoppedState	(Landroid/os/IBinder;Z)V
        //     44: aload_0
        //     45: getfield 178	android/app/Activity:mManagedCursors	Ljava/util/ArrayList;
        //     48: astore_1
        //     49: aload_1
        //     50: monitorenter
        //     51: aload_0
        //     52: getfield 178	android/app/Activity:mManagedCursors	Ljava/util/ArrayList;
        //     55: invokevirtual 1244	java/util/ArrayList:size	()I
        //     58: istore_3
        //     59: iconst_0
        //     60: istore 4
        //     62: iload 4
        //     64: iload_3
        //     65: if_icmpge +112 -> 177
        //     68: aload_0
        //     69: getfield 178	android/app/Activity:mManagedCursors	Ljava/util/ArrayList;
        //     72: iload 4
        //     74: invokevirtual 1245	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     77: checkcast 18	android/app/Activity$ManagedCursor
        //     80: astore 5
        //     82: aload 5
        //     84: invokestatic 1600	android/app/Activity$ManagedCursor:access$200	(Landroid/app/Activity$ManagedCursor;)Z
        //     87: ifne +11 -> 98
        //     90: aload 5
        //     92: invokestatic 1603	android/app/Activity$ManagedCursor:access$300	(Landroid/app/Activity$ManagedCursor;)Z
        //     95: ifeq +149 -> 244
        //     98: aload 5
        //     100: invokestatic 1249	android/app/Activity$ManagedCursor:access$100	(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;
        //     103: invokeinterface 1606 1 0
        //     108: ifne +52 -> 160
        //     111: aload_0
        //     112: invokevirtual 1269	android/app/Activity:getApplicationInfo	()Landroid/content/pm/ApplicationInfo;
        //     115: getfield 1274	android/content/pm/ApplicationInfo:targetSdkVersion	I
        //     118: bipush 14
        //     120: if_icmplt +40 -> 160
        //     123: new 767	java/lang/IllegalStateException
        //     126: dup
        //     127: new 263	java/lang/StringBuilder
        //     130: dup
        //     131: invokespecial 264	java/lang/StringBuilder:<init>	()V
        //     134: ldc_w 1608
        //     137: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     140: aload 5
        //     142: invokestatic 1249	android/app/Activity$ManagedCursor:access$100	(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;
        //     145: invokevirtual 1172	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     148: invokevirtual 281	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     151: invokespecial 770	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     154: athrow
        //     155: astore_2
        //     156: aload_1
        //     157: monitorexit
        //     158: aload_2
        //     159: athrow
        //     160: aload 5
        //     162: iconst_0
        //     163: invokestatic 1612	android/app/Activity$ManagedCursor:access$202	(Landroid/app/Activity$ManagedCursor;Z)Z
        //     166: pop
        //     167: aload 5
        //     169: iconst_0
        //     170: invokestatic 1615	android/app/Activity$ManagedCursor:access$302	(Landroid/app/Activity$ManagedCursor;Z)Z
        //     173: pop
        //     174: goto +70 -> 244
        //     177: aload_1
        //     178: monitorexit
        //     179: aload_0
        //     180: iconst_0
        //     181: putfield 1037	android/app/Activity:mCalled	Z
        //     184: aload_0
        //     185: getfield 474	android/app/Activity:mInstrumentation	Landroid/app/Instrumentation;
        //     188: aload_0
        //     189: invokevirtual 1618	android/app/Instrumentation:callActivityOnRestart	(Landroid/app/Activity;)V
        //     192: aload_0
        //     193: getfield 1037	android/app/Activity:mCalled	Z
        //     196: ifne +43 -> 239
        //     199: new 1578	android/app/SuperNotCalledException
        //     202: dup
        //     203: new 263	java/lang/StringBuilder
        //     206: dup
        //     207: invokespecial 264	java/lang/StringBuilder:<init>	()V
        //     210: ldc_w 1580
        //     213: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     216: aload_0
        //     217: getfield 486	android/app/Activity:mComponent	Landroid/content/ComponentName;
        //     220: invokevirtual 1583	android/content/ComponentName:toShortString	()Ljava/lang/String;
        //     223: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: ldc_w 1620
        //     229: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     232: invokevirtual 281	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     235: invokespecial 1586	android/app/SuperNotCalledException:<init>	(Ljava/lang/String;)V
        //     238: athrow
        //     239: aload_0
        //     240: invokevirtual 1623	android/app/Activity:performStart	()V
        //     243: return
        //     244: iinc 4 1
        //     247: goto -185 -> 62
        //
        // Exception table:
        //     from	to	target	type
        //     51	158	155	finally
        //     160	179	155	finally
    }

    final void performRestoreInstanceState(Bundle paramBundle)
    {
        onRestoreInstanceState(paramBundle);
        restoreManagedDialogs(paramBundle);
    }

    final void performResume()
    {
        performRestart();
        this.mFragments.execPendingActions();
        this.mLastNonConfigurationInstances = null;
        this.mCalled = false;
        this.mInstrumentation.callActivityOnResume(this);
        if (!this.mCalled)
            throw new SuperNotCalledException("Activity " + this.mComponent.toShortString() + " did not call through to super.onResume()");
        this.mCalled = false;
        this.mFragments.dispatchResume();
        this.mFragments.execPendingActions();
        onPostResume();
        if (!this.mCalled)
            throw new SuperNotCalledException("Activity " + this.mComponent.toShortString() + " did not call through to super.onPostResume()");
    }

    final void performSaveInstanceState(Bundle paramBundle)
    {
        onSaveInstanceState(paramBundle);
        saveManagedDialogs(paramBundle);
    }

    final void performStart()
    {
        this.mFragments.noteStateNotSaved();
        this.mCalled = false;
        this.mFragments.execPendingActions();
        this.mInstrumentation.callActivityOnStart(this);
        if (!this.mCalled)
            throw new SuperNotCalledException("Activity " + this.mComponent.toShortString() + " did not call through to super.onStart()");
        this.mFragments.dispatchStart();
        if (this.mAllLoaderManagers != null)
            for (int i = -1 + this.mAllLoaderManagers.size(); i >= 0; i--)
            {
                LoaderManagerImpl localLoaderManagerImpl = (LoaderManagerImpl)this.mAllLoaderManagers.valueAt(i);
                localLoaderManagerImpl.finishRetain();
                localLoaderManagerImpl.doReportStart();
            }
    }

    final void performStop()
    {
        if (this.mLoadersStarted)
        {
            this.mLoadersStarted = false;
            if (this.mLoaderManager != null)
            {
                if (this.mChangingConfigurations)
                    break label146;
                this.mLoaderManager.doStop();
            }
        }
        while (true)
        {
            if (this.mStopped)
                break label229;
            if (this.mWindow != null)
                this.mWindow.closeAllPanels();
            if ((this.mToken != null) && (this.mParent == null))
                WindowManagerImpl.getDefault().setStoppedState(this.mToken, true);
            this.mFragments.dispatchStop();
            this.mCalled = false;
            this.mInstrumentation.callActivityOnStop(this);
            if (this.mCalled)
                break;
            throw new SuperNotCalledException("Activity " + this.mComponent.toShortString() + " did not call through to super.onStop()");
            label146: this.mLoaderManager.doRetain();
        }
        while (true)
        {
            int j;
            synchronized (this.mManagedCursors)
            {
                int i = this.mManagedCursors.size();
                j = 0;
                if (j < i)
                {
                    ManagedCursor localManagedCursor = (ManagedCursor)this.mManagedCursors.get(j);
                    if (!localManagedCursor.mReleased)
                    {
                        localManagedCursor.mCursor.deactivate();
                        ManagedCursor.access$202(localManagedCursor, true);
                    }
                }
                else
                {
                    this.mStopped = true;
                    label229: this.mResumed = false;
                    return;
                }
            }
            j++;
        }
    }

    final void performUserLeaving()
    {
        onUserInteraction();
        onUserLeaveHint();
    }

    public void recreate()
    {
        if (this.mParent != null)
            throw new IllegalStateException("Can only be called on top-level activity");
        if (Looper.myLooper() != this.mMainThread.getLooper())
            throw new IllegalStateException("Must be called from main thread");
        this.mMainThread.requestRelaunchActivity(this.mToken, null, null, 0, false, null, false);
    }

    public void registerForContextMenu(View paramView)
    {
        paramView.setOnCreateContextMenuListener(this);
    }

    @Deprecated
    public final void removeDialog(int paramInt)
    {
        if (this.mManagedDialogs != null)
        {
            ManagedDialog localManagedDialog = (ManagedDialog)this.mManagedDialogs.get(paramInt);
            if (localManagedDialog != null)
            {
                localManagedDialog.mDialog.dismiss();
                this.mManagedDialogs.remove(paramInt);
            }
        }
    }

    public final boolean requestWindowFeature(int paramInt)
    {
        return getWindow().requestFeature(paramInt);
    }

    NonConfigurationInstances retainNonConfigurationInstances()
    {
        Object localObject = onRetainNonConfigurationInstance();
        HashMap localHashMap = onRetainNonConfigurationChildInstances();
        ArrayList localArrayList = this.mFragments.retainNonConfig();
        int i = 0;
        if (this.mAllLoaderManagers != null)
        {
            int j = -1 + this.mAllLoaderManagers.size();
            if (j >= 0)
            {
                LoaderManagerImpl localLoaderManagerImpl = (LoaderManagerImpl)this.mAllLoaderManagers.valueAt(j);
                if (localLoaderManagerImpl.mRetaining)
                    i = 1;
                while (true)
                {
                    j--;
                    break;
                    localLoaderManagerImpl.doDestroy();
                    this.mAllLoaderManagers.removeAt(j);
                }
            }
        }
        NonConfigurationInstances localNonConfigurationInstances;
        if ((localObject == null) && (localHashMap == null) && (localArrayList == null) && (i == 0))
            localNonConfigurationInstances = null;
        while (true)
        {
            return localNonConfigurationInstances;
            localNonConfigurationInstances = new NonConfigurationInstances();
            localNonConfigurationInstances.activity = localObject;
            localNonConfigurationInstances.children = localHashMap;
            localNonConfigurationInstances.fragments = localArrayList;
            localNonConfigurationInstances.loaders = this.mAllLoaderManagers;
        }
    }

    public final void runOnUiThread(Runnable paramRunnable)
    {
        if (Thread.currentThread() != this.mUiThread)
            this.mHandler.post(paramRunnable);
        while (true)
        {
            return;
            paramRunnable.run();
        }
    }

    public void setContentView(int paramInt)
    {
        getWindow().setContentView(paramInt);
        initActionBar();
    }

    public void setContentView(View paramView)
    {
        getWindow().setContentView(paramView);
        initActionBar();
    }

    public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        getWindow().setContentView(paramView, paramLayoutParams);
        initActionBar();
    }

    public final void setDefaultKeyMode(int paramInt)
    {
        this.mDefaultKeyMode = paramInt;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException();
        case 0:
        case 2:
            this.mDefaultKeySsb = null;
        case 1:
        case 3:
        case 4:
        }
        while (true)
        {
            return;
            this.mDefaultKeySsb = new SpannableStringBuilder();
            Selection.setSelection(this.mDefaultKeySsb, 0);
        }
    }

    public final void setFeatureDrawable(int paramInt, Drawable paramDrawable)
    {
        getWindow().setFeatureDrawable(paramInt, paramDrawable);
    }

    public final void setFeatureDrawableAlpha(int paramInt1, int paramInt2)
    {
        getWindow().setFeatureDrawableAlpha(paramInt1, paramInt2);
    }

    public final void setFeatureDrawableResource(int paramInt1, int paramInt2)
    {
        getWindow().setFeatureDrawableResource(paramInt1, paramInt2);
    }

    public final void setFeatureDrawableUri(int paramInt, Uri paramUri)
    {
        getWindow().setFeatureDrawableUri(paramInt, paramUri);
    }

    public void setFinishOnTouchOutside(boolean paramBoolean)
    {
        this.mWindow.setCloseOnTouchOutside(paramBoolean);
    }

    public void setImmersive(boolean paramBoolean)
    {
        try
        {
            ActivityManagerNative.getDefault().setImmersive(this.mToken, paramBoolean);
            label13: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label13;
        }
    }

    public void setIntent(Intent paramIntent)
    {
        this.mIntent = paramIntent;
    }

    final void setParent(Activity paramActivity)
    {
        this.mParent = paramActivity;
    }

    @Deprecated
    public void setPersistent(boolean paramBoolean)
    {
    }

    public final void setProgress(int paramInt)
    {
        getWindow().setFeatureInt(2, paramInt + 0);
    }

    public final void setProgressBarIndeterminate(boolean paramBoolean)
    {
        Window localWindow = getWindow();
        if (paramBoolean);
        for (int i = -3; ; i = -4)
        {
            localWindow.setFeatureInt(2, i);
            return;
        }
    }

    public final void setProgressBarIndeterminateVisibility(boolean paramBoolean)
    {
        Window localWindow = getWindow();
        if (paramBoolean);
        for (int i = -1; ; i = -2)
        {
            localWindow.setFeatureInt(5, i);
            return;
        }
    }

    public final void setProgressBarVisibility(boolean paramBoolean)
    {
        Window localWindow = getWindow();
        if (paramBoolean);
        for (int i = -1; ; i = -2)
        {
            localWindow.setFeatureInt(2, i);
            return;
        }
    }

    public void setRequestedOrientation(int paramInt)
    {
        if (this.mParent == null);
        try
        {
            ActivityManagerNative.getDefault().setRequestedOrientation(this.mToken, paramInt);
            while (true)
            {
                label20: return;
                this.mParent.setRequestedOrientation(paramInt);
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    public final void setResult(int paramInt)
    {
        try
        {
            this.mResultCode = paramInt;
            this.mResultData = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final void setResult(int paramInt, Intent paramIntent)
    {
        try
        {
            this.mResultCode = paramInt;
            this.mResultData = paramIntent;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final void setSecondaryProgress(int paramInt)
    {
        getWindow().setFeatureInt(2, paramInt + 20000);
    }

    public void setTitle(int paramInt)
    {
        setTitle(getText(paramInt));
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        this.mTitle = paramCharSequence;
        onTitleChanged(paramCharSequence, this.mTitleColor);
        if (this.mParent != null)
            this.mParent.onChildTitleChanged(this, paramCharSequence);
    }

    public void setTitleColor(int paramInt)
    {
        this.mTitleColor = paramInt;
        onTitleChanged(this.mTitle, paramInt);
    }

    public void setVisible(boolean paramBoolean)
    {
        if (this.mVisibleFromClient != paramBoolean)
        {
            this.mVisibleFromClient = paramBoolean;
            if (this.mVisibleFromServer)
            {
                if (!paramBoolean)
                    break label29;
                makeVisible();
            }
        }
        while (true)
        {
            return;
            label29: this.mDecor.setVisibility(4);
        }
    }

    public final void setVolumeControlStream(int paramInt)
    {
        getWindow().setVolumeControlStream(paramInt);
    }

    public boolean shouldUpRecreateTask(Intent paramIntent)
    {
        boolean bool1 = false;
        try
        {
            PackageManager localPackageManager = getPackageManager();
            ComponentName localComponentName = paramIntent.getComponent();
            if (localComponentName == null)
                localComponentName = paramIntent.resolveActivity(localPackageManager);
            ActivityInfo localActivityInfo = localPackageManager.getActivityInfo(localComponentName, 0);
            if (localActivityInfo.taskAffinity != null)
            {
                boolean bool2 = ActivityManagerNative.getDefault().targetTaskAffinityMatchesActivity(this.mToken, localActivityInfo.taskAffinity);
                if (!bool2)
                    bool1 = true;
            }
        }
        catch (RemoteException localRemoteException)
        {
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
        }
        return bool1;
    }

    @Deprecated
    public final void showDialog(int paramInt)
    {
        showDialog(paramInt, null);
    }

    @Deprecated
    public final boolean showDialog(int paramInt, Bundle paramBundle)
    {
        if (this.mManagedDialogs == null)
            this.mManagedDialogs = new SparseArray();
        ManagedDialog localManagedDialog = (ManagedDialog)this.mManagedDialogs.get(paramInt);
        if (localManagedDialog == null)
        {
            localManagedDialog = new ManagedDialog(null);
            localManagedDialog.mDialog = createDialog(Integer.valueOf(paramInt), null, paramBundle);
            if (localManagedDialog.mDialog != null);
        }
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mManagedDialogs.put(paramInt, localManagedDialog);
            localManagedDialog.mArgs = paramBundle;
            onPrepareDialog(paramInt, localManagedDialog.mDialog, paramBundle);
            localManagedDialog.mDialog.show();
        }
    }

    public ActionMode startActionMode(ActionMode.Callback paramCallback)
    {
        return this.mWindow.getDecorView().startActionMode(paramCallback);
    }

    public void startActivities(Intent[] paramArrayOfIntent)
    {
        startActivities(paramArrayOfIntent, null);
    }

    public void startActivities(Intent[] paramArrayOfIntent, Bundle paramBundle)
    {
        this.mInstrumentation.execStartActivities(this, this.mMainThread.getApplicationThread(), this.mToken, this, paramArrayOfIntent, paramBundle);
    }

    public void startActivity(Intent paramIntent)
    {
        startActivity(paramIntent, null);
    }

    public void startActivity(Intent paramIntent, Bundle paramBundle)
    {
        if (paramBundle != null)
            startActivityForResult(paramIntent, -1, paramBundle);
        while (true)
        {
            return;
            startActivityForResult(paramIntent, -1);
        }
    }

    public void startActivityForResult(Intent paramIntent, int paramInt)
    {
        startActivityForResult(paramIntent, paramInt, null);
    }

    public void startActivityForResult(Intent paramIntent, int paramInt, Bundle paramBundle)
    {
        if (this.mParent == null)
        {
            Instrumentation.ActivityResult localActivityResult = this.mInstrumentation.execStartActivity(this, this.mMainThread.getApplicationThread(), this.mToken, this, paramIntent, paramInt, paramBundle);
            if (localActivityResult != null)
                this.mMainThread.sendActivityResult(this.mToken, this.mEmbeddedID, paramInt, localActivityResult.getResultCode(), localActivityResult.getResultData());
            if (paramInt >= 0)
                this.mStartedActivity = true;
        }
        while (true)
        {
            return;
            if (paramBundle != null)
                this.mParent.startActivityFromChild(this, paramIntent, paramInt, paramBundle);
            else
                this.mParent.startActivityFromChild(this, paramIntent, paramInt);
        }
    }

    public void startActivityFromChild(Activity paramActivity, Intent paramIntent, int paramInt)
    {
        startActivityFromChild(paramActivity, paramIntent, paramInt, null);
    }

    public void startActivityFromChild(Activity paramActivity, Intent paramIntent, int paramInt, Bundle paramBundle)
    {
        Instrumentation.ActivityResult localActivityResult = this.mInstrumentation.execStartActivity(this, this.mMainThread.getApplicationThread(), this.mToken, paramActivity, paramIntent, paramInt, paramBundle);
        if (localActivityResult != null)
            this.mMainThread.sendActivityResult(this.mToken, paramActivity.mEmbeddedID, paramInt, localActivityResult.getResultCode(), localActivityResult.getResultData());
    }

    public void startActivityFromFragment(Fragment paramFragment, Intent paramIntent, int paramInt)
    {
        startActivityFromFragment(paramFragment, paramIntent, paramInt, null);
    }

    public void startActivityFromFragment(Fragment paramFragment, Intent paramIntent, int paramInt, Bundle paramBundle)
    {
        Instrumentation.ActivityResult localActivityResult = this.mInstrumentation.execStartActivity(this, this.mMainThread.getApplicationThread(), this.mToken, paramFragment, paramIntent, paramInt, paramBundle);
        if (localActivityResult != null)
            this.mMainThread.sendActivityResult(this.mToken, paramFragment.mWho, paramInt, localActivityResult.getResultCode(), localActivityResult.getResultData());
    }

    public boolean startActivityIfNeeded(Intent paramIntent, int paramInt)
    {
        return startActivityIfNeeded(paramIntent, paramInt, null);
    }

    public boolean startActivityIfNeeded(Intent paramIntent, int paramInt, Bundle paramBundle)
    {
        int i;
        if (this.mParent == null)
            i = 1;
        try
        {
            paramIntent.setAllowFds(false);
            int j = ActivityManagerNative.getDefault().startActivity(this.mMainThread.getApplicationThread(), paramIntent, paramIntent.resolveTypeIfNeeded(getContentResolver()), this.mToken, this.mEmbeddedID, paramInt, 1, null, null, paramBundle);
            i = j;
            label58: Instrumentation.checkStartActivityResult(i, paramIntent);
            if (paramInt >= 0)
                this.mStartedActivity = true;
            if (i != 1);
            for (boolean bool = true; ; bool = false)
                return bool;
            throw new UnsupportedOperationException("startActivityIfNeeded can only be called from a top-level activity");
        }
        catch (RemoteException localRemoteException)
        {
            break label58;
        }
    }

    public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3)
        throws IntentSender.SendIntentException
    {
        startIntentSender(paramIntentSender, paramIntent, paramInt1, paramInt2, paramInt3, null);
    }

    public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
        throws IntentSender.SendIntentException
    {
        if (paramBundle != null)
            startIntentSenderForResult(paramIntentSender, -1, paramIntent, paramInt1, paramInt2, paramInt3, paramBundle);
        while (true)
        {
            return;
            startIntentSenderForResult(paramIntentSender, -1, paramIntent, paramInt1, paramInt2, paramInt3);
        }
    }

    public void startIntentSenderForResult(IntentSender paramIntentSender, int paramInt1, Intent paramIntent, int paramInt2, int paramInt3, int paramInt4)
        throws IntentSender.SendIntentException
    {
        startIntentSenderForResult(paramIntentSender, paramInt1, paramIntent, paramInt2, paramInt3, paramInt4, null);
    }

    public void startIntentSenderForResult(IntentSender paramIntentSender, int paramInt1, Intent paramIntent, int paramInt2, int paramInt3, int paramInt4, Bundle paramBundle)
        throws IntentSender.SendIntentException
    {
        if (this.mParent == null)
            startIntentSenderForResultInner(paramIntentSender, paramInt1, paramIntent, paramInt2, paramInt3, this, paramBundle);
        while (true)
        {
            return;
            if (paramBundle != null)
                this.mParent.startIntentSenderFromChild(this, paramIntentSender, paramInt1, paramIntent, paramInt2, paramInt3, paramInt4, paramBundle);
            else
                this.mParent.startIntentSenderFromChild(this, paramIntentSender, paramInt1, paramIntent, paramInt2, paramInt3, paramInt4);
        }
    }

    public void startIntentSenderFromChild(Activity paramActivity, IntentSender paramIntentSender, int paramInt1, Intent paramIntent, int paramInt2, int paramInt3, int paramInt4)
        throws IntentSender.SendIntentException
    {
        startIntentSenderFromChild(paramActivity, paramIntentSender, paramInt1, paramIntent, paramInt2, paramInt3, paramInt4, null);
    }

    public void startIntentSenderFromChild(Activity paramActivity, IntentSender paramIntentSender, int paramInt1, Intent paramIntent, int paramInt2, int paramInt3, int paramInt4, Bundle paramBundle)
        throws IntentSender.SendIntentException
    {
        startIntentSenderForResultInner(paramIntentSender, paramInt1, paramIntent, paramInt2, paramInt3, paramActivity, paramBundle);
    }

    @Deprecated
    public void startManagingCursor(Cursor paramCursor)
    {
        synchronized (this.mManagedCursors)
        {
            this.mManagedCursors.add(new ManagedCursor(paramCursor));
            return;
        }
    }

    public boolean startNextMatchingActivity(Intent paramIntent)
    {
        return startNextMatchingActivity(paramIntent, null);
    }

    public boolean startNextMatchingActivity(Intent paramIntent, Bundle paramBundle)
    {
        boolean bool1 = false;
        if (this.mParent == null);
        try
        {
            paramIntent.setAllowFds(false);
            boolean bool2 = ActivityManagerNative.getDefault().startNextMatchingActivity(this.mToken, paramIntent, paramBundle);
            bool1 = bool2;
            label33: return bool1;
            throw new UnsupportedOperationException("startNextMatchingActivity can only be called from a top-level activity");
        }
        catch (RemoteException localRemoteException)
        {
            break label33;
        }
    }

    public void startSearch(String paramString, boolean paramBoolean1, Bundle paramBundle, boolean paramBoolean2)
    {
        ensureSearchManager();
        this.mSearchManager.startSearch(paramString, paramBoolean1, getComponentName(), paramBundle, paramBoolean2);
    }

    @Deprecated
    public void stopManagingCursor(Cursor paramCursor)
    {
        while (true)
        {
            int j;
            synchronized (this.mManagedCursors)
            {
                int i = this.mManagedCursors.size();
                j = 0;
                if (j < i)
                {
                    if (((ManagedCursor)this.mManagedCursors.get(j)).mCursor == paramCursor)
                        this.mManagedCursors.remove(j);
                }
                else
                    return;
            }
            j++;
        }
    }

    public void takeKeyEvents(boolean paramBoolean)
    {
        getWindow().takeKeyEvents(paramBoolean);
    }

    public void triggerSearch(String paramString, Bundle paramBundle)
    {
        ensureSearchManager();
        this.mSearchManager.triggerSearch(paramString, getComponentName(), paramBundle);
    }

    public void unregisterForContextMenu(View paramView)
    {
        paramView.setOnCreateContextMenuListener(null);
    }

    private static final class ManagedCursor
    {
        private final Cursor mCursor;
        private boolean mReleased;
        private boolean mUpdated;

        ManagedCursor(Cursor paramCursor)
        {
            this.mCursor = paramCursor;
            this.mReleased = false;
            this.mUpdated = false;
        }
    }

    static final class NonConfigurationInstances
    {
        Object activity;
        HashMap<String, Object> children;
        ArrayList<Fragment> fragments;
        SparseArray<LoaderManagerImpl> loaders;
    }

    private static class ManagedDialog
    {
        Bundle mArgs;
        Dialog mDialog;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void checkAccessControl(Activity paramActivity)
        {
            FirewallManager.checkAccessControl(paramActivity.mParent, paramActivity.getContentResolver(), paramActivity.getPackageName(), paramActivity.getPackageManager(), paramActivity.mMainThread.getApplicationThread(), paramActivity.getToken(), paramActivity.mEmbeddedID);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Activity
 * JD-Core Version:        0.6.2
 */