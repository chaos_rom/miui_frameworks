package android.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.IWindowSession;
import android.view.ViewRootImpl;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class WallpaperManager
{
    public static final String ACTION_CHANGE_LIVE_WALLPAPER = "android.service.wallpaper.CHANGE_LIVE_WALLPAPER";
    public static final String ACTION_LIVE_WALLPAPER_CHOOSER = "android.service.wallpaper.LIVE_WALLPAPER_CHOOSER";
    public static final String COMMAND_DROP = "android.home.drop";
    public static final String COMMAND_SECONDARY_TAP = "android.wallpaper.secondaryTap";
    public static final String COMMAND_TAP = "android.wallpaper.tap";
    private static boolean DEBUG = false;
    public static final String EXTRA_LIVE_WALLPAPER_COMPONENT = "android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT";
    private static String TAG = "WallpaperManager";
    public static final String WALLPAPER_PREVIEW_META_DATA = "android.wallpaper.preview";
    private static Globals sGlobals;
    private static final Object sSync = new Object[0];
    private final Context mContext;
    private float mWallpaperXStep = -1.0F;
    private float mWallpaperYStep = -1.0F;

    WallpaperManager(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
        initGlobals(paramContext.getMainLooper());
    }

    static Bitmap generateBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2)
    {
        if (paramBitmap == null)
            paramBitmap = null;
        while (true)
        {
            return paramBitmap;
            paramBitmap.setDensity(DisplayMetrics.DENSITY_DEVICE);
            if ((paramInt1 > 0) && (paramInt2 > 0) && ((paramBitmap.getWidth() != paramInt1) || (paramBitmap.getHeight() != paramInt2)))
                try
                {
                    Bitmap localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
                    localBitmap.setDensity(DisplayMetrics.DENSITY_DEVICE);
                    Canvas localCanvas = new Canvas(localBitmap);
                    Rect localRect = new Rect();
                    localRect.right = paramBitmap.getWidth();
                    localRect.bottom = paramBitmap.getHeight();
                    int i = paramInt1 - localRect.right;
                    int j = paramInt2 - localRect.bottom;
                    if ((i > 0) || (j > 0))
                        if (i <= j)
                            break label260;
                    label260: float f1;
                    int k;
                    for (float f2 = paramInt1 / localRect.right; ; f2 = f1 / k)
                    {
                        localRect.right = ((int)(f2 * localRect.right));
                        localRect.bottom = ((int)(f2 * localRect.bottom));
                        i = paramInt1 - localRect.right;
                        j = paramInt2 - localRect.bottom;
                        localRect.offset(i / 2, j / 2);
                        Paint localPaint = new Paint();
                        localPaint.setFilterBitmap(true);
                        localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
                        localCanvas.drawBitmap(paramBitmap, null, localRect, localPaint);
                        paramBitmap.recycle();
                        localCanvas.setBitmap(null);
                        paramBitmap = localBitmap;
                        break;
                        f1 = paramInt2;
                        k = localRect.bottom;
                    }
                }
                catch (OutOfMemoryError localOutOfMemoryError)
                {
                    Log.w(TAG, "Can't generate default bitmap", localOutOfMemoryError);
                }
        }
    }

    public static WallpaperManager getInstance(Context paramContext)
    {
        return (WallpaperManager)paramContext.getSystemService("wallpaper");
    }

    static void initGlobals(Looper paramLooper)
    {
        synchronized (sSync)
        {
            if (sGlobals == null)
                sGlobals = new Globals(paramLooper);
            return;
        }
    }

    private void setWallpaper(InputStream paramInputStream, FileOutputStream paramFileOutputStream)
        throws IOException
    {
        byte[] arrayOfByte = new byte[32768];
        while (true)
        {
            int i = paramInputStream.read(arrayOfByte);
            if (i <= 0)
                break;
            paramFileOutputStream.write(arrayOfByte, 0, i);
        }
    }

    public void clear()
        throws IOException
    {
        setResource(17302048);
    }

    public void clearWallpaperOffsets(IBinder paramIBinder)
    {
        try
        {
            ViewRootImpl.getWindowSession(this.mContext.getMainLooper()).setWallpaperPosition(paramIBinder, -1.0F, -1.0F, -1.0F, -1.0F);
            label24: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label24;
        }
    }

    public void forgetLoadedWallpaper()
    {
        sGlobals.forgetLoadedWallpaper();
    }

    public Bitmap getBitmap()
    {
        return sGlobals.peekWallpaperBitmap(this.mContext, true);
    }

    public int getDesiredMinimumHeight()
    {
        int i = 0;
        if (sGlobals.mService == null)
            Log.w(TAG, "WallpaperService not running");
        while (true)
        {
            return i;
            try
            {
                int j = sGlobals.mService.getHeightHint();
                i = j;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public int getDesiredMinimumWidth()
    {
        int i = 0;
        if (sGlobals.mService == null)
            Log.w(TAG, "WallpaperService not running");
        while (true)
        {
            return i;
            try
            {
                int j = sGlobals.mService.getWidthHint();
                i = j;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public Drawable getDrawable()
    {
        Bitmap localBitmap = sGlobals.peekWallpaperBitmap(this.mContext, true);
        BitmapDrawable localBitmapDrawable;
        if (localBitmap != null)
        {
            localBitmapDrawable = new BitmapDrawable(this.mContext.getResources(), localBitmap);
            localBitmapDrawable.setDither(false);
        }
        while (true)
        {
            return localBitmapDrawable;
            localBitmapDrawable = null;
        }
    }

    public Drawable getFastDrawable()
    {
        Bitmap localBitmap = sGlobals.peekWallpaperBitmap(this.mContext, true);
        if (localBitmap != null);
        for (FastBitmapDrawable localFastBitmapDrawable = new FastBitmapDrawable(localBitmap, null); ; localFastBitmapDrawable = null)
            return localFastBitmapDrawable;
    }

    public IWallpaperManager getIWallpaperManager()
    {
        return sGlobals.mService;
    }

    public WallpaperInfo getWallpaperInfo()
    {
        Object localObject = null;
        try
        {
            if (sGlobals.mService == null)
            {
                Log.w(TAG, "WallpaperService not running");
            }
            else
            {
                WallpaperInfo localWallpaperInfo = sGlobals.mService.getWallpaperInfo();
                localObject = localWallpaperInfo;
            }
        }
        catch (RemoteException localRemoteException)
        {
        }
        return localObject;
    }

    public Drawable peekDrawable()
    {
        Bitmap localBitmap = sGlobals.peekWallpaperBitmap(this.mContext, false);
        BitmapDrawable localBitmapDrawable;
        if (localBitmap != null)
        {
            localBitmapDrawable = new BitmapDrawable(this.mContext.getResources(), localBitmap);
            localBitmapDrawable.setDither(false);
        }
        while (true)
        {
            return localBitmapDrawable;
            localBitmapDrawable = null;
        }
    }

    public Drawable peekFastDrawable()
    {
        Bitmap localBitmap = sGlobals.peekWallpaperBitmap(this.mContext, false);
        if (localBitmap != null);
        for (FastBitmapDrawable localFastBitmapDrawable = new FastBitmapDrawable(localBitmap, null); ; localFastBitmapDrawable = null)
            return localFastBitmapDrawable;
    }

    public void sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
    {
        try
        {
            ViewRootImpl.getWindowSession(this.mContext.getMainLooper()).sendWallpaperCommand(paramIBinder, paramString, paramInt1, paramInt2, paramInt3, paramBundle, false);
            label26: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label26;
        }
    }

    // ERROR //
    public void setBitmap(Bitmap paramBitmap)
        throws IOException
    {
        // Byte code:
        //     0: getstatic 181	android/app/WallpaperManager:sGlobals	Landroid/app/WallpaperManager$Globals;
        //     3: invokestatic 235	android/app/WallpaperManager$Globals:access$300	(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
        //     6: ifnonnull +13 -> 19
        //     9: getstatic 53	android/app/WallpaperManager:TAG	Ljava/lang/String;
        //     12: ldc 237
        //     14: invokestatic 240	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     17: pop
        //     18: return
        //     19: getstatic 181	android/app/WallpaperManager:sGlobals	Landroid/app/WallpaperManager$Globals;
        //     22: invokestatic 235	android/app/WallpaperManager$Globals:access$300	(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
        //     25: aconst_null
        //     26: invokeinterface 283 2 0
        //     31: astore_3
        //     32: aload_3
        //     33: ifnull -15 -> 18
        //     36: aconst_null
        //     37: astore 4
        //     39: new 285	android/os/ParcelFileDescriptor$AutoCloseOutputStream
        //     42: dup
        //     43: aload_3
        //     44: invokespecial 288	android/os/ParcelFileDescriptor$AutoCloseOutputStream:<init>	(Landroid/os/ParcelFileDescriptor;)V
        //     47: astore 5
        //     49: aload_1
        //     50: getstatic 294	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
        //     53: bipush 90
        //     55: aload 5
        //     57: invokevirtual 298	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
        //     60: pop
        //     61: aload 5
        //     63: ifnull -45 -> 18
        //     66: aload 5
        //     68: invokevirtual 301	android/os/ParcelFileDescriptor$AutoCloseOutputStream:close	()V
        //     71: goto -53 -> 18
        //     74: aload 4
        //     76: ifnull +8 -> 84
        //     79: aload 4
        //     81: invokevirtual 301	android/os/ParcelFileDescriptor$AutoCloseOutputStream:close	()V
        //     84: aload 6
        //     86: athrow
        //     87: astore 6
        //     89: aload 5
        //     91: astore 4
        //     93: goto -19 -> 74
        //     96: astore_2
        //     97: goto -79 -> 18
        //     100: astore 6
        //     102: goto -28 -> 74
        //
        // Exception table:
        //     from	to	target	type
        //     49	61	87	finally
        //     19	32	96	android/os/RemoteException
        //     66	87	96	android/os/RemoteException
        //     39	49	100	finally
    }

    // ERROR //
    public void setResource(int paramInt)
        throws IOException
    {
        // Byte code:
        //     0: getstatic 181	android/app/WallpaperManager:sGlobals	Landroid/app/WallpaperManager$Globals;
        //     3: invokestatic 235	android/app/WallpaperManager$Globals:access$300	(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
        //     6: ifnonnull +13 -> 19
        //     9: getstatic 53	android/app/WallpaperManager:TAG	Ljava/lang/String;
        //     12: ldc 237
        //     14: invokestatic 240	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     17: pop
        //     18: return
        //     19: aload_0
        //     20: getfield 68	android/app/WallpaperManager:mContext	Landroid/content/Context;
        //     23: invokevirtual 257	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     26: astore_3
        //     27: getstatic 181	android/app/WallpaperManager:sGlobals	Landroid/app/WallpaperManager$Globals;
        //     30: invokestatic 235	android/app/WallpaperManager$Globals:access$300	(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
        //     33: new 303	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 304	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 306
        //     43: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_3
        //     47: iload_1
        //     48: invokevirtual 316	android/content/res/Resources:getResourceName	(I)Ljava/lang/String;
        //     51: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     54: invokevirtual 319	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     57: invokeinterface 283 2 0
        //     62: astore 4
        //     64: aload 4
        //     66: ifnull -48 -> 18
        //     69: aconst_null
        //     70: astore 5
        //     72: new 285	android/os/ParcelFileDescriptor$AutoCloseOutputStream
        //     75: dup
        //     76: aload 4
        //     78: invokespecial 288	android/os/ParcelFileDescriptor$AutoCloseOutputStream:<init>	(Landroid/os/ParcelFileDescriptor;)V
        //     81: astore 6
        //     83: aload_0
        //     84: aload_3
        //     85: iload_1
        //     86: invokevirtual 323	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
        //     89: aload 6
        //     91: invokespecial 325	android/app/WallpaperManager:setWallpaper	(Ljava/io/InputStream;Ljava/io/FileOutputStream;)V
        //     94: aload 6
        //     96: ifnull -78 -> 18
        //     99: aload 6
        //     101: invokevirtual 301	android/os/ParcelFileDescriptor$AutoCloseOutputStream:close	()V
        //     104: goto -86 -> 18
        //     107: aload 5
        //     109: ifnull +8 -> 117
        //     112: aload 5
        //     114: invokevirtual 301	android/os/ParcelFileDescriptor$AutoCloseOutputStream:close	()V
        //     117: aload 7
        //     119: athrow
        //     120: astore 7
        //     122: aload 6
        //     124: astore 5
        //     126: goto -19 -> 107
        //     129: astore_2
        //     130: goto -112 -> 18
        //     133: astore 7
        //     135: goto -28 -> 107
        //
        // Exception table:
        //     from	to	target	type
        //     83	94	120	finally
        //     19	64	129	android/os/RemoteException
        //     99	120	129	android/os/RemoteException
        //     72	83	133	finally
    }

    // ERROR //
    public void setStream(InputStream paramInputStream)
        throws IOException
    {
        // Byte code:
        //     0: getstatic 181	android/app/WallpaperManager:sGlobals	Landroid/app/WallpaperManager$Globals;
        //     3: invokestatic 235	android/app/WallpaperManager$Globals:access$300	(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
        //     6: ifnonnull +13 -> 19
        //     9: getstatic 53	android/app/WallpaperManager:TAG	Ljava/lang/String;
        //     12: ldc 237
        //     14: invokestatic 240	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     17: pop
        //     18: return
        //     19: getstatic 181	android/app/WallpaperManager:sGlobals	Landroid/app/WallpaperManager$Globals;
        //     22: invokestatic 235	android/app/WallpaperManager$Globals:access$300	(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
        //     25: aconst_null
        //     26: invokeinterface 283 2 0
        //     31: astore_3
        //     32: aload_3
        //     33: ifnull -15 -> 18
        //     36: aconst_null
        //     37: astore 4
        //     39: new 285	android/os/ParcelFileDescriptor$AutoCloseOutputStream
        //     42: dup
        //     43: aload_3
        //     44: invokespecial 288	android/os/ParcelFileDescriptor$AutoCloseOutputStream:<init>	(Landroid/os/ParcelFileDescriptor;)V
        //     47: astore 5
        //     49: aload_0
        //     50: aload_1
        //     51: aload 5
        //     53: invokespecial 325	android/app/WallpaperManager:setWallpaper	(Ljava/io/InputStream;Ljava/io/FileOutputStream;)V
        //     56: aload 5
        //     58: ifnull -40 -> 18
        //     61: aload 5
        //     63: invokevirtual 301	android/os/ParcelFileDescriptor$AutoCloseOutputStream:close	()V
        //     66: goto -48 -> 18
        //     69: aload 4
        //     71: ifnull +8 -> 79
        //     74: aload 4
        //     76: invokevirtual 301	android/os/ParcelFileDescriptor$AutoCloseOutputStream:close	()V
        //     79: aload 6
        //     81: athrow
        //     82: astore 6
        //     84: aload 5
        //     86: astore 4
        //     88: goto -19 -> 69
        //     91: astore_2
        //     92: goto -74 -> 18
        //     95: astore 6
        //     97: goto -28 -> 69
        //
        // Exception table:
        //     from	to	target	type
        //     49	56	82	finally
        //     19	32	91	android/os/RemoteException
        //     61	82	91	android/os/RemoteException
        //     39	49	95	finally
    }

    public void setWallpaperOffsetSteps(float paramFloat1, float paramFloat2)
    {
        this.mWallpaperXStep = paramFloat1;
        this.mWallpaperYStep = paramFloat2;
    }

    public void setWallpaperOffsets(IBinder paramIBinder, float paramFloat1, float paramFloat2)
    {
        try
        {
            ViewRootImpl.getWindowSession(this.mContext.getMainLooper()).setWallpaperPosition(paramIBinder, paramFloat1, paramFloat2, this.mWallpaperXStep, this.mWallpaperYStep);
            label26: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label26;
        }
    }

    public void suggestDesiredDimensions(int paramInt1, int paramInt2)
    {
        try
        {
            if (sGlobals.mService == null)
                Log.w(TAG, "WallpaperService not running");
            else
                sGlobals.mService.setDimensionHints(paramInt1, paramInt2);
        }
        catch (RemoteException localRemoteException)
        {
        }
    }

    static class Globals extends IWallpaperManagerCallback.Stub
    {
        private static final int MSG_CLEAR_WALLPAPER = 1;
        private Bitmap mDefaultWallpaper;
        private final Handler mHandler;
        private IWallpaperManager mService = IWallpaperManager.Stub.asInterface(ServiceManager.getService("wallpaper"));
        private Bitmap mWallpaper;

        Globals(Looper paramLooper)
        {
            this.mHandler = new Handler(paramLooper)
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    switch (paramAnonymousMessage.what)
                    {
                    default:
                    case 1:
                    }
                    while (true)
                    {
                        return;
                        try
                        {
                            WallpaperManager.Globals.access$002(WallpaperManager.Globals.this, null);
                            WallpaperManager.Globals.access$102(WallpaperManager.Globals.this, null);
                        }
                        finally
                        {
                            localObject = finally;
                            throw localObject;
                        }
                    }
                }
            };
        }

        // ERROR //
        private Bitmap getCurrentWallpaperLocked()
        {
            // Byte code:
            //     0: new 64	android/os/Bundle
            //     3: dup
            //     4: invokespecial 65	android/os/Bundle:<init>	()V
            //     7: astore_1
            //     8: aload_0
            //     9: getfield 40	android/app/WallpaperManager$Globals:mService	Landroid/app/IWallpaperManager;
            //     12: aload_0
            //     13: aload_1
            //     14: invokeinterface 71 3 0
            //     19: astore 4
            //     21: aload 4
            //     23: ifnull +78 -> 101
            //     26: aload_1
            //     27: ldc 73
            //     29: iconst_0
            //     30: invokevirtual 77	android/os/Bundle:getInt	(Ljava/lang/String;I)I
            //     33: istore 5
            //     35: aload_1
            //     36: ldc 79
            //     38: iconst_0
            //     39: invokevirtual 77	android/os/Bundle:getInt	(Ljava/lang/String;I)I
            //     42: istore 6
            //     44: new 81	android/graphics/BitmapFactory$Options
            //     47: dup
            //     48: invokespecial 82	android/graphics/BitmapFactory$Options:<init>	()V
            //     51: astore 7
            //     53: aload 4
            //     55: invokevirtual 88	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     58: aconst_null
            //     59: aload 7
            //     61: invokestatic 94	android/graphics/BitmapFactory:decodeFileDescriptor	(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
            //     64: iload 5
            //     66: iload 6
            //     68: invokestatic 98	android/app/WallpaperManager:generateBitmap	(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
            //     71: astore 13
            //     73: aload 13
            //     75: astore_3
            //     76: aload 4
            //     78: invokevirtual 101	android/os/ParcelFileDescriptor:close	()V
            //     81: aload_3
            //     82: areturn
            //     83: astore 10
            //     85: invokestatic 105	android/app/WallpaperManager:access$200	()Ljava/lang/String;
            //     88: ldc 107
            //     90: aload 10
            //     92: invokestatic 113	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     95: pop
            //     96: aload 4
            //     98: invokevirtual 101	android/os/ParcelFileDescriptor:close	()V
            //     101: aconst_null
            //     102: astore_3
            //     103: goto -22 -> 81
            //     106: astore 8
            //     108: aload 4
            //     110: invokevirtual 101	android/os/ParcelFileDescriptor:close	()V
            //     113: aload 8
            //     115: athrow
            //     116: astore_2
            //     117: goto -16 -> 101
            //     120: astore 9
            //     122: goto -9 -> 113
            //     125: astore 12
            //     127: goto -26 -> 101
            //     130: astore 14
            //     132: goto -51 -> 81
            //
            // Exception table:
            //     from	to	target	type
            //     44	73	83	java/lang/OutOfMemoryError
            //     44	73	106	finally
            //     85	96	106	finally
            //     0	44	116	android/os/RemoteException
            //     76	81	116	android/os/RemoteException
            //     96	101	116	android/os/RemoteException
            //     108	113	116	android/os/RemoteException
            //     113	116	116	android/os/RemoteException
            //     108	113	120	java/io/IOException
            //     96	101	125	java/io/IOException
            //     76	81	130	java/io/IOException
        }

        // ERROR //
        private Bitmap getDefaultWallpaperLocked(Context paramContext)
        {
            // Byte code:
            //     0: aload_1
            //     1: invokevirtual 121	android/content/Context:getResources	()Landroid/content/res/Resources;
            //     4: ldc 122
            //     6: invokevirtual 128	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
            //     9: astore 4
            //     11: aload 4
            //     13: ifnull +75 -> 88
            //     16: aload_0
            //     17: getfield 40	android/app/WallpaperManager$Globals:mService	Landroid/app/IWallpaperManager;
            //     20: invokeinterface 132 1 0
            //     25: istore 5
            //     27: aload_0
            //     28: getfield 40	android/app/WallpaperManager$Globals:mService	Landroid/app/IWallpaperManager;
            //     31: invokeinterface 135 1 0
            //     36: istore 6
            //     38: aload 4
            //     40: aconst_null
            //     41: new 81	android/graphics/BitmapFactory$Options
            //     44: dup
            //     45: invokespecial 82	android/graphics/BitmapFactory$Options:<init>	()V
            //     48: invokestatic 139	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
            //     51: iload 5
            //     53: iload 6
            //     55: invokestatic 98	android/app/WallpaperManager:generateBitmap	(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
            //     58: astore 12
            //     60: aload 12
            //     62: astore_3
            //     63: aload 4
            //     65: invokevirtual 142	java/io/InputStream:close	()V
            //     68: aload_3
            //     69: areturn
            //     70: astore 9
            //     72: invokestatic 105	android/app/WallpaperManager:access$200	()Ljava/lang/String;
            //     75: ldc 144
            //     77: aload 9
            //     79: invokestatic 113	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     82: pop
            //     83: aload 4
            //     85: invokevirtual 142	java/io/InputStream:close	()V
            //     88: aconst_null
            //     89: astore_3
            //     90: goto -22 -> 68
            //     93: astore 7
            //     95: aload 4
            //     97: invokevirtual 142	java/io/InputStream:close	()V
            //     100: aload 7
            //     102: athrow
            //     103: astore_2
            //     104: goto -16 -> 88
            //     107: astore 8
            //     109: goto -9 -> 100
            //     112: astore 11
            //     114: goto -26 -> 88
            //     117: astore 13
            //     119: goto -51 -> 68
            //
            // Exception table:
            //     from	to	target	type
            //     38	60	70	java/lang/OutOfMemoryError
            //     38	60	93	finally
            //     72	83	93	finally
            //     0	38	103	android/os/RemoteException
            //     63	68	103	android/os/RemoteException
            //     83	88	103	android/os/RemoteException
            //     95	100	103	android/os/RemoteException
            //     100	103	103	android/os/RemoteException
            //     95	100	107	java/io/IOException
            //     83	88	112	java/io/IOException
            //     63	68	117	java/io/IOException
        }

        public void forgetLoadedWallpaper()
        {
            try
            {
                this.mWallpaper = null;
                this.mDefaultWallpaper = null;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onWallpaperChanged()
        {
            this.mHandler.sendEmptyMessage(1);
        }

        // ERROR //
        public Bitmap peekWallpaperBitmap(Context paramContext, boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 49	android/app/WallpaperManager$Globals:mWallpaper	Landroid/graphics/Bitmap;
            //     6: ifnull +14 -> 20
            //     9: aload_0
            //     10: getfield 49	android/app/WallpaperManager$Globals:mWallpaper	Landroid/graphics/Bitmap;
            //     13: astore 6
            //     15: aload_0
            //     16: monitorexit
            //     17: goto +99 -> 116
            //     20: aload_0
            //     21: getfield 52	android/app/WallpaperManager$Globals:mDefaultWallpaper	Landroid/graphics/Bitmap;
            //     24: ifnull +19 -> 43
            //     27: aload_0
            //     28: getfield 52	android/app/WallpaperManager$Globals:mDefaultWallpaper	Landroid/graphics/Bitmap;
            //     31: astore 6
            //     33: aload_0
            //     34: monitorexit
            //     35: goto +81 -> 116
            //     38: astore_3
            //     39: aload_0
            //     40: monitorexit
            //     41: aload_3
            //     42: athrow
            //     43: aload_0
            //     44: aconst_null
            //     45: putfield 49	android/app/WallpaperManager$Globals:mWallpaper	Landroid/graphics/Bitmap;
            //     48: aload_0
            //     49: aload_0
            //     50: invokespecial 156	android/app/WallpaperManager$Globals:getCurrentWallpaperLocked	()Landroid/graphics/Bitmap;
            //     53: putfield 49	android/app/WallpaperManager$Globals:mWallpaper	Landroid/graphics/Bitmap;
            //     56: iload_2
            //     57: ifeq +51 -> 108
            //     60: aload_0
            //     61: getfield 49	android/app/WallpaperManager$Globals:mWallpaper	Landroid/graphics/Bitmap;
            //     64: ifnonnull +39 -> 103
            //     67: aload_0
            //     68: aload_0
            //     69: aload_1
            //     70: invokespecial 158	android/app/WallpaperManager$Globals:getDefaultWallpaperLocked	(Landroid/content/Context;)Landroid/graphics/Bitmap;
            //     73: putfield 52	android/app/WallpaperManager$Globals:mDefaultWallpaper	Landroid/graphics/Bitmap;
            //     76: aload_0
            //     77: getfield 52	android/app/WallpaperManager$Globals:mDefaultWallpaper	Landroid/graphics/Bitmap;
            //     80: astore 6
            //     82: aload_0
            //     83: monitorexit
            //     84: goto +32 -> 116
            //     87: astore 4
            //     89: invokestatic 105	android/app/WallpaperManager:access$200	()Ljava/lang/String;
            //     92: ldc 160
            //     94: aload 4
            //     96: invokestatic 113	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     99: pop
            //     100: goto -44 -> 56
            //     103: aload_0
            //     104: aconst_null
            //     105: putfield 52	android/app/WallpaperManager$Globals:mDefaultWallpaper	Landroid/graphics/Bitmap;
            //     108: aload_0
            //     109: getfield 49	android/app/WallpaperManager$Globals:mWallpaper	Landroid/graphics/Bitmap;
            //     112: astore 6
            //     114: aload_0
            //     115: monitorexit
            //     116: aload 6
            //     118: areturn
            //
            // Exception table:
            //     from	to	target	type
            //     2	41	38	finally
            //     43	48	38	finally
            //     48	56	38	finally
            //     60	116	38	finally
            //     48	56	87	java/lang/OutOfMemoryError
        }
    }

    static class FastBitmapDrawable extends Drawable
    {
        private final Bitmap mBitmap;
        private int mDrawLeft;
        private int mDrawTop;
        private final int mHeight;
        private final Paint mPaint;
        private final int mWidth;

        private FastBitmapDrawable(Bitmap paramBitmap)
        {
            this.mBitmap = paramBitmap;
            this.mWidth = paramBitmap.getWidth();
            this.mHeight = paramBitmap.getHeight();
            setBounds(0, 0, this.mWidth, this.mHeight);
            this.mPaint = new Paint();
            this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        }

        public void draw(Canvas paramCanvas)
        {
            paramCanvas.drawBitmap(this.mBitmap, this.mDrawLeft, this.mDrawTop, this.mPaint);
        }

        public int getIntrinsicHeight()
        {
            return this.mHeight;
        }

        public int getIntrinsicWidth()
        {
            return this.mWidth;
        }

        public int getMinimumHeight()
        {
            return this.mHeight;
        }

        public int getMinimumWidth()
        {
            return this.mWidth;
        }

        public int getOpacity()
        {
            return -1;
        }

        public void setAlpha(int paramInt)
        {
            throw new UnsupportedOperationException("Not supported with this drawable");
        }

        public void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.mDrawLeft = (paramInt1 + (paramInt3 - paramInt1 - this.mWidth) / 2);
            this.mDrawTop = (paramInt2 + (paramInt4 - paramInt2 - this.mHeight) / 2);
        }

        public void setColorFilter(ColorFilter paramColorFilter)
        {
            throw new UnsupportedOperationException("Not supported with this drawable");
        }

        public void setDither(boolean paramBoolean)
        {
            throw new UnsupportedOperationException("Not supported with this drawable");
        }

        public void setFilterBitmap(boolean paramBoolean)
        {
            throw new UnsupportedOperationException("Not supported with this drawable");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.WallpaperManager
 * JD-Core Version:        0.6.2
 */