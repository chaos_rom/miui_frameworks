package android.app;

import android.view.Window;

class Instrumentation$1MenuRunnable
    implements Runnable
{
    private final Activity activity;
    private final int flags;
    private final int identifier;
    boolean returnValue;

    public Instrumentation$1MenuRunnable(Instrumentation paramInstrumentation, Activity paramActivity, int paramInt1, int paramInt2)
    {
        this.activity = paramActivity;
        this.identifier = paramInt1;
        this.flags = paramInt2;
    }

    public void run()
    {
        this.returnValue = this.activity.getWindow().performPanelIdentifierAction(0, this.identifier, this.flags);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Instrumentation.1MenuRunnable
 * JD-Core Version:        0.6.2
 */