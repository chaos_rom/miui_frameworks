package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IUiModeManager extends IInterface
{
    public abstract void disableCarMode(int paramInt)
        throws RemoteException;

    public abstract void enableCarMode(int paramInt)
        throws RemoteException;

    public abstract int getCurrentModeType()
        throws RemoteException;

    public abstract int getNightMode()
        throws RemoteException;

    public abstract void setNightMode(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IUiModeManager
    {
        private static final String DESCRIPTOR = "android.app.IUiModeManager";
        static final int TRANSACTION_disableCarMode = 2;
        static final int TRANSACTION_enableCarMode = 1;
        static final int TRANSACTION_getCurrentModeType = 3;
        static final int TRANSACTION_getNightMode = 5;
        static final int TRANSACTION_setNightMode = 4;

        public Stub()
        {
            attachInterface(this, "android.app.IUiModeManager");
        }

        public static IUiModeManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IUiModeManager");
                if ((localIInterface != null) && ((localIInterface instanceof IUiModeManager)))
                    localObject = (IUiModeManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.app.IUiModeManager");
                continue;
                paramParcel1.enforceInterface("android.app.IUiModeManager");
                enableCarMode(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.IUiModeManager");
                disableCarMode(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.IUiModeManager");
                int j = getCurrentModeType();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                continue;
                paramParcel1.enforceInterface("android.app.IUiModeManager");
                setNightMode(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.app.IUiModeManager");
                int i = getNightMode();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i);
            }
        }

        private static class Proxy
            implements IUiModeManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void disableCarMode(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IUiModeManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enableCarMode(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IUiModeManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getCurrentModeType()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IUiModeManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IUiModeManager";
            }

            public int getNightMode()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IUiModeManager");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setNightMode(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.app.IUiModeManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IUiModeManager
 * JD-Core Version:        0.6.2
 */