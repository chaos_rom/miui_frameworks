package android.app;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import java.lang.reflect.Array;
import java.util.List;
import miui.provider.ExtraSettings.Secure;

public class MiuiDownloadManager extends DownloadManager
{
    public static final String ACTION_DOWNLOAD_DELETED = "android.intent.action.DOWNLOAD_DELETED";
    public static final String ACTION_DOWNLOAD_UPDATED = "android.intent.action.DOWNLOAD_UPDATED";
    public static final String ACTION_OPERATE_DOWNLOADSET_UPDATE_PROGRESS = "android.intent.action.OPERATE_DOWNLOADSET_UPDATE_PROGRESS";
    private static final ComponentName DOWNLOAD_UPDATE_RECEIVER_COMPONENT = new ComponentName("com.android.providers.downloads", "com.android.providers.downloads.DownloadUpdateReceiver");
    public static final String EXTRA_DOWNLOAD_CURRENT_BYTES = "extra_download_current_bytes";
    public static final String EXTRA_DOWNLOAD_STATUS = "extra_download_status";
    public static final String EXTRA_DOWNLOAD_TOTAL_BYTES = "extra_download_total_bytes";
    public static final String INTENT_EXTRA_APPLICATION_PACKAGENAME = "intent_extra_application_packagename";
    public static final String INTENT_EXTRA_REGISTER_DOWNLOADS_UPDATE_PROGRESS = "intent_extra_register_downloads_update_progress";
    public static final String INTENT_EXTRA_UNREGISTER_DOWNLOADS_UPDATE_PROGRESS = "intent_extra_unregister_downloads_update_progress";
    public static final String[] MIUI_UNDERLYING_COLUMNS = (String[])concatArrays(arrayOfString1, arrayOfString2, String.class);
    public static final int PAUSED_BY_APP = 5;
    private ContentResolver mResolver;

    static
    {
        String[] arrayOfString1 = UNDERLYING_COLUMNS;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "bypass_recommended_size_limit";
        arrayOfString2[1] = "allowed_network_types";
    }

    public MiuiDownloadManager(ContentResolver paramContentResolver, String paramString)
    {
        super(paramContentResolver, paramString);
        this.mResolver = paramContentResolver;
    }

    public static void addRunningStatusAndControlRun(ContentValues paramContentValues)
    {
        if (paramContentValues != null)
        {
            paramContentValues.put("status", Integer.valueOf(192));
            paramContentValues.put("control", Integer.valueOf(0));
        }
    }

    private static <T> T[] concatArrays(T[] paramArrayOfT1, T[] paramArrayOfT2, Class<T> paramClass)
    {
        Object[] arrayOfObject = (Object[])Array.newInstance(paramClass, paramArrayOfT1.length + paramArrayOfT2.length);
        System.arraycopy(paramArrayOfT1, 0, arrayOfObject, 0, paramArrayOfT1.length);
        System.arraycopy(paramArrayOfT2, 0, arrayOfObject, paramArrayOfT1.length, paramArrayOfT2.length);
        return arrayOfObject;
    }

    public static Integer getMobileFileSizePromptEnabled(Context paramContext)
    {
        try
        {
            Integer localInteger2 = Integer.valueOf(Settings.Secure.getInt(paramContext.getContentResolver(), ExtraSettings.Secure.MOBILE_DOWNLOAD_FILE_SIZE_PROMPT_POPUP_ENABLED));
            localInteger1 = localInteger2;
            return localInteger1;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            while (true)
                Integer localInteger1 = null;
        }
    }

    private static String[] getWhereArgsForStatuses(int[] paramArrayOfInt)
    {
        String[] arrayOfString = new String[paramArrayOfInt.length];
        for (int i = 0; i < paramArrayOfInt.length; i++)
            arrayOfString[i] = Integer.toString(paramArrayOfInt[i]);
        return arrayOfString;
    }

    private static String getWhereClauseForStatuses(String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("(");
        for (int i = 0; i < paramArrayOfString1.length; i++)
        {
            if (i > 0)
                localStringBuilder.append(paramArrayOfString2[(i - 1)] + " ");
            localStringBuilder.append("status");
            localStringBuilder.append(" " + paramArrayOfString1[i] + " ? ");
        }
        localStringBuilder.append(")");
        return localStringBuilder.toString();
    }

    public static boolean isDownloadSuccess(Cursor paramCursor)
    {
        DownloadManager.CursorTranslator localCursorTranslator = (DownloadManager.CursorTranslator)paramCursor;
        if (localCursorTranslator.getInt(localCursorTranslator.getColumnIndex("status")) == 8);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isDownloading(Cursor paramCursor)
    {
        boolean bool = false;
        DownloadManager.CursorTranslator localCursorTranslator = (DownloadManager.CursorTranslator)paramCursor;
        switch (localCursorTranslator.getInt(localCursorTranslator.getColumnIndex("status")))
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            return bool;
            bool = true;
        }
    }

    public static void operateDownloadsNeedToUpdateProgress(Context paramContext, long[] paramArrayOfLong1, long[] paramArrayOfLong2)
    {
        Intent localIntent = new Intent("android.intent.action.OPERATE_DOWNLOADSET_UPDATE_PROGRESS");
        localIntent.setComponent(DOWNLOAD_UPDATE_RECEIVER_COMPONENT);
        localIntent.putExtra("intent_extra_register_downloads_update_progress", paramArrayOfLong1);
        localIntent.putExtra("intent_extra_unregister_downloads_update_progress", paramArrayOfLong2);
        paramContext.sendBroadcast(localIntent);
    }

    public static boolean setMobileFileSizePromptEnabled(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        String str = ExtraSettings.Secure.MOBILE_DOWNLOAD_FILE_SIZE_PROMPT_POPUP_ENABLED;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
            return Settings.Secure.putInt(localContentResolver, str, i);
    }

    public static boolean setRecommendedMaxBytesOverMobile(Context paramContext, long paramLong)
    {
        return Settings.Secure.putLong(paramContext.getContentResolver(), "download_manager_recommended_max_bytes_over_mobile", paramLong);
    }

    public static int translateStatus(int paramInt)
    {
        return DownloadManager.CursorTranslator.translateStatus(paramInt);
    }

    public void pauseDownload(long[] paramArrayOfLong)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("status", Integer.valueOf(193));
        localContentValues.put("control", Integer.valueOf(1));
        StringBuilder localStringBuilder = new StringBuilder().append("( ").append(getWhereClauseForIds(paramArrayOfLong)).append(" AND ");
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "!=";
        arrayOfString1[1] = "!=";
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = "AND";
        String str = getWhereClauseForStatuses(arrayOfString1, arrayOfString2) + ")";
        String[] arrayOfString3 = getWhereArgsForIds(paramArrayOfLong);
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 1;
        arrayOfInt[1] = 2;
        String[] arrayOfString4 = (String[])concatArrays(arrayOfString3, getWhereArgsForStatuses(arrayOfInt), String.class);
        this.mResolver.update(getBaseUri(), localContentValues, str, arrayOfString4);
    }

    public Cursor query(DownloadManager.Query paramQuery)
    {
        Cursor localCursor = paramQuery.runQuery(this.mResolver, MIUI_UNDERLYING_COLUMNS, getBaseUri());
        if (localCursor == null);
        for (Object localObject = null; ; localObject = new DownloadManager.CursorTranslator(localCursor, getBaseUri()))
            return localObject;
    }

    public int removeRecordOnly(long[] paramArrayOfLong)
    {
        if ((paramArrayOfLong == null) || (paramArrayOfLong.length == 0))
            throw new IllegalArgumentException("input param 'ids' can't be null");
        return this.mResolver.delete(getBaseUri(), getWhereClauseForIds(paramArrayOfLong), getWhereArgsForIds(paramArrayOfLong));
    }

    public void resumeDownload(long[] paramArrayOfLong)
    {
        ContentValues localContentValues = new ContentValues();
        addRunningStatusAndControlRun(localContentValues);
        StringBuilder localStringBuilder = new StringBuilder().append("( ").append(getWhereClauseForIds(paramArrayOfLong)).append(" AND ");
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "!=";
        String str = getWhereClauseForStatuses(arrayOfString1, null) + ")";
        String[] arrayOfString2 = getWhereArgsForIds(paramArrayOfLong);
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 4;
        String[] arrayOfString3 = (String[])concatArrays(arrayOfString2, getWhereArgsForStatuses(arrayOfInt), String.class);
        this.mResolver.update(getBaseUri(), localContentValues, str, arrayOfString3);
    }

    public static class Query extends DownloadManager.Query
    {
        private String mAppendedClause;
        private String mColumnAppData;
        private String mColumnNotificationPackage;

        void addExtraSelectionParts(List<String> paramList)
        {
            super.addExtraSelectionParts(paramList);
            if (!TextUtils.isEmpty(this.mColumnAppData))
            {
                Object[] arrayOfObject2 = new Object[2];
                arrayOfObject2[0] = "entity";
                arrayOfObject2[1] = this.mColumnAppData;
                paramList.add(String.format("%s='%s'", arrayOfObject2));
            }
            if (!TextUtils.isEmpty(this.mColumnNotificationPackage))
            {
                Object[] arrayOfObject1 = new Object[2];
                arrayOfObject1[0] = "notificationpackage";
                arrayOfObject1[1] = this.mColumnNotificationPackage;
                paramList.add(String.format("%s='%s'", arrayOfObject1));
            }
            if (!TextUtils.isEmpty(this.mAppendedClause))
                paramList.add(this.mAppendedClause);
        }

        public Query orderBy(String paramString, int paramInt)
        {
            if ((paramInt != 1) && (paramInt != 2))
                throw new IllegalArgumentException("Invalid direction: " + paramInt);
            if (paramString.equals("_id"))
            {
                setOrderByColumn("_id");
                setOrderDirection(paramInt);
            }
            while (true)
            {
                return this;
                super.orderBy(paramString, paramInt);
            }
        }

        public Query setFilterByAppData(String paramString)
        {
            this.mColumnAppData = paramString;
            return this;
        }

        public Query setFilterByAppendedClause(String paramString)
        {
            this.mAppendedClause = paramString;
            return this;
        }

        public Query setFilterByNotificationPackage(String paramString)
        {
            this.mColumnNotificationPackage = paramString;
            return this;
        }
    }

    public static class Request extends DownloadManager.Request
    {
        private String mAppointName;
        private boolean mBypassRecommendedSizeLimit;
        private String mColumnAppData;
        private String mNotificationClass;
        private String mUserAgent;

        public Request(Uri paramUri)
        {
            super();
        }

        private void putIfNonNull(ContentValues paramContentValues, String paramString, Object paramObject)
        {
            if (paramObject != null)
                paramContentValues.put(paramString, paramObject.toString());
        }

        public Request setAppData(String paramString)
        {
            this.mColumnAppData = paramString;
            return this;
        }

        public Request setAppointName(String paramString)
        {
            this.mAppointName = paramString;
            return this;
        }

        public void setBypassRecommendedSizeLimit(boolean paramBoolean)
        {
            this.mBypassRecommendedSizeLimit = paramBoolean;
        }

        public Request setNotificationClass(String paramString)
        {
            this.mNotificationClass = paramString;
            return this;
        }

        public void setUserAgent(String paramString)
        {
            this.mUserAgent = paramString;
        }

        protected ContentValues toContentValues(String paramString)
        {
            ContentValues localContentValues = super.toContentValues(paramString);
            putIfNonNull(localContentValues, "entity", this.mColumnAppData);
            putIfNonNull(localContentValues, "appointname", this.mAppointName);
            putIfNonNull(localContentValues, "notificationclass", this.mNotificationClass);
            putIfNonNull(localContentValues, "useragent", this.mUserAgent);
            localContentValues.put("bypass_recommended_size_limit", Boolean.valueOf(this.mBypassRecommendedSizeLimit));
            return localContentValues;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.MiuiDownloadManager
 * JD-Core Version:        0.6.2
 */