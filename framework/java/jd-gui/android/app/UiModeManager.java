package android.app;

import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class UiModeManager
{
    public static String ACTION_ENTER_CAR_MODE = "android.app.action.ENTER_CAR_MODE";
    public static String ACTION_ENTER_DESK_MODE = "android.app.action.ENTER_DESK_MODE";
    public static String ACTION_EXIT_CAR_MODE = "android.app.action.EXIT_CAR_MODE";
    public static String ACTION_EXIT_DESK_MODE = "android.app.action.EXIT_DESK_MODE";
    public static final int DISABLE_CAR_MODE_GO_HOME = 1;
    public static final int ENABLE_CAR_MODE_GO_CAR_HOME = 1;
    public static final int MODE_NIGHT_AUTO = 0;
    public static final int MODE_NIGHT_NO = 1;
    public static final int MODE_NIGHT_YES = 2;
    private static final String TAG = "UiModeManager";
    private IUiModeManager mService = IUiModeManager.Stub.asInterface(ServiceManager.getService("uimode"));

    public void disableCarMode(int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.disableCarMode(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UiModeManager", "disableCarMode: RemoteException", localRemoteException);
        }
    }

    public void enableCarMode(int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.enableCarMode(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UiModeManager", "disableCarMode: RemoteException", localRemoteException);
        }
    }

    public int getCurrentModeType()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getCurrentModeType();
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("UiModeManager", "getCurrentModeType: RemoteException", localRemoteException);
            }
            int i = 1;
        }
    }

    public int getNightMode()
    {
        if (this.mService != null);
        while (true)
        {
            try
            {
                int j = this.mService.getNightMode();
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("UiModeManager", "getNightMode: RemoteException", localRemoteException);
            }
            int i = -1;
        }
    }

    public void setNightMode(int paramInt)
    {
        if (this.mService != null);
        try
        {
            this.mService.setNightMode(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UiModeManager", "setNightMode: RemoteException", localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.UiModeManager
 * JD-Core Version:        0.6.2
 */