package android.app;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.List;

public class SearchManager
    implements DialogInterface.OnDismissListener, DialogInterface.OnCancelListener
{
    public static final String ACTION_KEY = "action_key";
    public static final String ACTION_MSG = "action_msg";
    public static final String APP_DATA = "app_data";
    public static final String CONTEXT_IS_VOICE = "android.search.CONTEXT_IS_VOICE";
    public static final String CURSOR_EXTRA_KEY_IN_PROGRESS = "in_progress";
    private static final boolean DBG = false;
    public static final String DISABLE_VOICE_SEARCH = "android.search.DISABLE_VOICE_SEARCH";
    public static final String EXTRA_DATA_KEY = "intent_extra_data_key";
    public static final String EXTRA_NEW_SEARCH = "new_search";
    public static final String EXTRA_SELECT_QUERY = "select_query";
    public static final String EXTRA_WEB_SEARCH_PENDINGINTENT = "web_search_pendingintent";
    public static final int FLAG_QUERY_REFINEMENT = 1;
    public static final String INTENT_ACTION_GLOBAL_SEARCH = "android.search.action.GLOBAL_SEARCH";
    public static final String INTENT_ACTION_SEARCHABLES_CHANGED = "android.search.action.SEARCHABLES_CHANGED";
    public static final String INTENT_ACTION_SEARCH_SETTINGS = "android.search.action.SEARCH_SETTINGS";
    public static final String INTENT_ACTION_SEARCH_SETTINGS_CHANGED = "android.search.action.SETTINGS_CHANGED";
    public static final String INTENT_ACTION_WEB_SEARCH_SETTINGS = "android.search.action.WEB_SEARCH_SETTINGS";
    public static final String INTENT_GLOBAL_SEARCH_ACTIVITY_CHANGED = "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED";
    public static final char MENU_KEY = 's';
    public static final int MENU_KEYCODE = 47;
    public static final String QUERY = "query";
    public static final String SEARCH_MODE = "search_mode";
    public static final String SHORTCUT_MIME_TYPE = "vnd.android.cursor.item/vnd.android.search.suggest";
    public static final String SUGGEST_COLUMN_FLAGS = "suggest_flags";
    public static final String SUGGEST_COLUMN_FORMAT = "suggest_format";
    public static final String SUGGEST_COLUMN_ICON_1 = "suggest_icon_1";
    public static final String SUGGEST_COLUMN_ICON_2 = "suggest_icon_2";
    public static final String SUGGEST_COLUMN_INTENT_ACTION = "suggest_intent_action";
    public static final String SUGGEST_COLUMN_INTENT_DATA = "suggest_intent_data";
    public static final String SUGGEST_COLUMN_INTENT_DATA_ID = "suggest_intent_data_id";
    public static final String SUGGEST_COLUMN_INTENT_EXTRA_DATA = "suggest_intent_extra_data";
    public static final String SUGGEST_COLUMN_LAST_ACCESS_HINT = "suggest_last_access_hint";
    public static final String SUGGEST_COLUMN_QUERY = "suggest_intent_query";
    public static final String SUGGEST_COLUMN_SHORTCUT_ID = "suggest_shortcut_id";
    public static final String SUGGEST_COLUMN_SPINNER_WHILE_REFRESHING = "suggest_spinner_while_refreshing";
    public static final String SUGGEST_COLUMN_TEXT_1 = "suggest_text_1";
    public static final String SUGGEST_COLUMN_TEXT_2 = "suggest_text_2";
    public static final String SUGGEST_COLUMN_TEXT_2_URL = "suggest_text_2_url";
    public static final String SUGGEST_MIME_TYPE = "vnd.android.cursor.dir/vnd.android.search.suggest";
    public static final String SUGGEST_NEVER_MAKE_SHORTCUT = "_-1";
    public static final String SUGGEST_PARAMETER_LIMIT = "limit";
    public static final String SUGGEST_URI_PATH_QUERY = "search_suggest_query";
    public static final String SUGGEST_URI_PATH_SHORTCUT = "search_suggest_shortcut";
    private static final String TAG = "SearchManager";
    public static final String USER_QUERY = "user_query";
    private static ISearchManager mService;
    private String mAssociatedPackage;
    OnCancelListener mCancelListener = null;
    private final Context mContext;
    OnDismissListener mDismissListener = null;
    final Handler mHandler;
    private SearchDialog mSearchDialog;

    SearchManager(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mHandler = paramHandler;
        mService = ISearchManager.Stub.asInterface(ServiceManager.getService("search"));
    }

    private void ensureSearchDialog()
    {
        if (this.mSearchDialog == null)
        {
            this.mSearchDialog = new SearchDialog(this.mContext, this);
            this.mSearchDialog.setOnCancelListener(this);
            this.mSearchDialog.setOnDismissListener(this);
        }
    }

    public static final Intent getAssistIntent(Context paramContext)
    {
        PackageManager localPackageManager = paramContext.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.ASSIST");
        ComponentName localComponentName = localIntent.resolveActivity(localPackageManager);
        if (localComponentName != null)
            localIntent.setComponent(localComponentName);
        while (true)
        {
            return localIntent;
            localIntent = null;
        }
    }

    public List<ResolveInfo> getGlobalSearchActivities()
    {
        try
        {
            List localList2 = mService.getGlobalSearchActivities();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SearchManager", "getGlobalSearchActivities() failed: " + localRemoteException);
                List localList1 = null;
            }
        }
    }

    public ComponentName getGlobalSearchActivity()
    {
        try
        {
            ComponentName localComponentName2 = mService.getGlobalSearchActivity();
            localComponentName1 = localComponentName2;
            return localComponentName1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SearchManager", "getGlobalSearchActivity() failed: " + localRemoteException);
                ComponentName localComponentName1 = null;
            }
        }
    }

    public SearchableInfo getSearchableInfo(ComponentName paramComponentName)
    {
        try
        {
            SearchableInfo localSearchableInfo2 = mService.getSearchableInfo(paramComponentName);
            localSearchableInfo1 = localSearchableInfo2;
            return localSearchableInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SearchManager", "getSearchableInfo() failed: " + localRemoteException);
                SearchableInfo localSearchableInfo1 = null;
            }
        }
    }

    public List<SearchableInfo> getSearchablesInGlobalSearch()
    {
        try
        {
            List localList2 = mService.getSearchablesInGlobalSearch();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SearchManager", "getSearchablesInGlobalSearch() failed: " + localRemoteException);
                List localList1 = null;
            }
        }
    }

    public Cursor getSuggestions(SearchableInfo paramSearchableInfo, String paramString)
    {
        return getSuggestions(paramSearchableInfo, paramString, -1);
    }

    public Cursor getSuggestions(SearchableInfo paramSearchableInfo, String paramString, int paramInt)
    {
        Cursor localCursor = null;
        if (paramSearchableInfo == null);
        String str1;
        do
        {
            return localCursor;
            str1 = paramSearchableInfo.getSuggestAuthority();
        }
        while (str1 == null);
        Uri.Builder localBuilder = new Uri.Builder().scheme("content").authority(str1).query("").fragment("");
        String str2 = paramSearchableInfo.getSuggestPath();
        if (str2 != null)
            localBuilder.appendEncodedPath(str2);
        localBuilder.appendPath("search_suggest_query");
        String str3 = paramSearchableInfo.getSuggestSelection();
        String[] arrayOfString = null;
        if (str3 != null)
        {
            arrayOfString = new String[1];
            arrayOfString[0] = paramString;
        }
        while (true)
        {
            if (paramInt > 0)
                localBuilder.appendQueryParameter("limit", String.valueOf(paramInt));
            Uri localUri = localBuilder.build();
            localCursor = this.mContext.getContentResolver().query(localUri, null, str3, arrayOfString, null);
            break;
            localBuilder.appendPath(paramString);
        }
    }

    public ComponentName getWebSearchActivity()
    {
        try
        {
            ComponentName localComponentName2 = mService.getWebSearchActivity();
            localComponentName1 = localComponentName2;
            return localComponentName1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SearchManager", "getWebSearchActivity() failed: " + localRemoteException);
                ComponentName localComponentName1 = null;
            }
        }
    }

    public boolean isVisible()
    {
        if (this.mSearchDialog == null);
        for (boolean bool = false; ; bool = this.mSearchDialog.isShowing())
            return bool;
    }

    @Deprecated
    public void onCancel(DialogInterface paramDialogInterface)
    {
        if (this.mCancelListener != null)
            this.mCancelListener.onCancel();
    }

    @Deprecated
    public void onDismiss(DialogInterface paramDialogInterface)
    {
        if (this.mDismissListener != null)
            this.mDismissListener.onDismiss();
    }

    public void setOnCancelListener(OnCancelListener paramOnCancelListener)
    {
        this.mCancelListener = paramOnCancelListener;
    }

    public void setOnDismissListener(OnDismissListener paramOnDismissListener)
    {
        this.mDismissListener = paramOnDismissListener;
    }

    void startGlobalSearch(String paramString, boolean paramBoolean, Bundle paramBundle, Rect paramRect)
    {
        ComponentName localComponentName = getGlobalSearchActivity();
        if (localComponentName == null)
        {
            Log.w("SearchManager", "No global search activity found.");
            return;
        }
        Intent localIntent = new Intent("android.search.action.GLOBAL_SEARCH");
        localIntent.addFlags(268435456);
        localIntent.setComponent(localComponentName);
        if (paramBundle == null);
        for (Bundle localBundle = new Bundle(); ; localBundle = new Bundle(paramBundle))
        {
            while (true)
            {
                if (!localBundle.containsKey("source"))
                    localBundle.putString("source", this.mContext.getPackageName());
                localIntent.putExtra("app_data", localBundle);
                if (!TextUtils.isEmpty(paramString))
                    localIntent.putExtra("query", paramString);
                if (paramBoolean)
                    localIntent.putExtra("select_query", paramBoolean);
                localIntent.setSourceBounds(paramRect);
                try
                {
                    this.mContext.startActivity(localIntent);
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                    Log.e("SearchManager", "Global search activity not found: " + localComponentName);
                }
            }
            break;
        }
    }

    public void startSearch(String paramString, boolean paramBoolean1, ComponentName paramComponentName, Bundle paramBundle, boolean paramBoolean2)
    {
        startSearch(paramString, paramBoolean1, paramComponentName, paramBundle, paramBoolean2, null);
    }

    public void startSearch(String paramString, boolean paramBoolean1, ComponentName paramComponentName, Bundle paramBundle, boolean paramBoolean2, Rect paramRect)
    {
        if (paramBoolean2)
            startGlobalSearch(paramString, paramBoolean1, paramBundle, paramRect);
        while (true)
        {
            return;
            ensureSearchDialog();
            this.mSearchDialog.show(paramString, paramBoolean1, paramComponentName, paramBundle);
        }
    }

    public void stopSearch()
    {
        if (this.mSearchDialog != null)
            this.mSearchDialog.cancel();
    }

    public void triggerSearch(String paramString, ComponentName paramComponentName, Bundle paramBundle)
    {
        if (!this.mAssociatedPackage.equals(paramComponentName.getPackageName()))
            throw new IllegalArgumentException("invoking app search on a different package not associated with this search manager");
        if ((paramString == null) || (TextUtils.getTrimmedLength(paramString) == 0))
            Log.w("SearchManager", "triggerSearch called with empty query, ignoring.");
        while (true)
        {
            return;
            startSearch(paramString, false, paramComponentName, paramBundle, false);
            this.mSearchDialog.launchQuerySearch();
        }
    }

    public static abstract interface OnCancelListener
    {
        public abstract void onCancel();
    }

    public static abstract interface OnDismissListener
    {
        public abstract void onDismiss();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.SearchManager
 * JD-Core Version:        0.6.2
 */