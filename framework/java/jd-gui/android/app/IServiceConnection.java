package android.app;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IServiceConnection extends IInterface
{
    public abstract void connected(ComponentName paramComponentName, IBinder paramIBinder)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IServiceConnection
    {
        private static final String DESCRIPTOR = "android.app.IServiceConnection";
        static final int TRANSACTION_connected = 1;

        public Stub()
        {
            attachInterface(this, "android.app.IServiceConnection");
        }

        public static IServiceConnection asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IServiceConnection");
                if ((localIInterface != null) && ((localIInterface instanceof IServiceConnection)))
                    localObject = (IServiceConnection)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.app.IServiceConnection");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.app.IServiceConnection");
            if (paramParcel1.readInt() != 0);
            for (ComponentName localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName = null)
            {
                connected(localComponentName, paramParcel1.readStrongBinder());
                break;
            }
        }

        private static class Proxy
            implements IServiceConnection
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void connected(ComponentName paramComponentName, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IServiceConnection");
                    if (paramComponentName != null)
                    {
                        localParcel.writeInt(1);
                        paramComponentName.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeStrongBinder(paramIBinder);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IServiceConnection";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IServiceConnection
 * JD-Core Version:        0.6.2
 */