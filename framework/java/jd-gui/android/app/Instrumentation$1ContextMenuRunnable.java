package android.app;

import android.view.Window;

class Instrumentation$1ContextMenuRunnable
    implements Runnable
{
    private final Activity activity;
    private final int flags;
    private final int identifier;
    boolean returnValue;

    public Instrumentation$1ContextMenuRunnable(Instrumentation paramInstrumentation, Activity paramActivity, int paramInt1, int paramInt2)
    {
        this.activity = paramActivity;
        this.identifier = paramInt1;
        this.flags = paramInt2;
    }

    public void run()
    {
        this.returnValue = this.activity.getWindow().performContextMenuIdentifierAction(this.identifier, this.flags);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Instrumentation.1ContextMenuRunnable
 * JD-Core Version:        0.6.2
 */