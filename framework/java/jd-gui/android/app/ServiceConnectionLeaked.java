package android.app;

import android.util.AndroidRuntimeException;

final class ServiceConnectionLeaked extends AndroidRuntimeException
{
    public ServiceConnectionLeaked(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ServiceConnectionLeaked
 * JD-Core Version:        0.6.2
 */