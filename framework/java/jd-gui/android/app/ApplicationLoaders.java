package android.app;

import dalvik.system.PathClassLoader;
import java.util.HashMap;
import java.util.Map;

class ApplicationLoaders
{
    private static final ApplicationLoaders gApplicationLoaders = new ApplicationLoaders();
    private final Map<String, ClassLoader> mLoaders = new HashMap();

    public static ApplicationLoaders getDefault()
    {
        return gApplicationLoaders;
    }

    public ClassLoader getClassLoader(String paramString1, String paramString2, ClassLoader paramClassLoader)
    {
        ClassLoader localClassLoader = ClassLoader.getSystemClassLoader().getParent();
        Map localMap = this.mLoaders;
        if (paramClassLoader == null)
            paramClassLoader = localClassLoader;
        if (paramClassLoader == localClassLoader);
        Object localObject1;
        try
        {
            localObject1 = (ClassLoader)this.mLoaders.get(paramString1);
            if (localObject1 != null)
                break label116;
            PathClassLoader localPathClassLoader = new PathClassLoader(paramString1, paramString2, paramClassLoader);
            this.mLoaders.put(paramString1, localPathClassLoader);
            localObject1 = localPathClassLoader;
            break label116;
            localObject1 = new PathClassLoader(paramString1, paramClassLoader);
        }
        finally
        {
            localObject2 = finally;
            throw localObject2;
        }
        label116: return localObject1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ApplicationLoaders
 * JD-Core Version:        0.6.2
 */