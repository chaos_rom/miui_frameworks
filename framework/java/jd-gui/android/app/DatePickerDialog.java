package android.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import java.util.Calendar;

public class DatePickerDialog extends AlertDialog
    implements DialogInterface.OnClickListener, DatePicker.OnDateChangedListener
{
    private static final String DAY = "day";
    private static final String MONTH = "month";
    private static final String YEAR = "year";
    private final Calendar mCalendar;
    private final OnDateSetListener mCallBack;
    private final DatePicker mDatePicker;
    private boolean mTitleNeedsUpdate = true;

    public DatePickerDialog(Context paramContext, int paramInt1, OnDateSetListener paramOnDateSetListener, int paramInt2, int paramInt3, int paramInt4)
    {
        super(paramContext, paramInt1);
        this.mCallBack = paramOnDateSetListener;
        this.mCalendar = Calendar.getInstance();
        Context localContext = getContext();
        setButton(-1, localContext.getText(17040419), this);
        setIcon(0);
        View localView = ((LayoutInflater)localContext.getSystemService("layout_inflater")).inflate(17367094, null);
        setView(localView);
        this.mDatePicker = ((DatePicker)localView.findViewById(16908915));
        this.mDatePicker.init(paramInt2, paramInt3, paramInt4, this);
        updateTitle(paramInt2, paramInt3, paramInt4);
    }

    public DatePickerDialog(Context paramContext, OnDateSetListener paramOnDateSetListener, int paramInt1, int paramInt2, int paramInt3)
    {
        this(paramContext, 0, paramOnDateSetListener, paramInt1, paramInt2, paramInt3);
    }

    private void tryNotifyDateSet()
    {
        if (this.mCallBack != null)
        {
            this.mDatePicker.clearFocus();
            this.mCallBack.onDateSet(this.mDatePicker, this.mDatePicker.getYear(), this.mDatePicker.getMonth(), this.mDatePicker.getDayOfMonth());
        }
    }

    private void updateTitle(int paramInt1, int paramInt2, int paramInt3)
    {
        if (!this.mDatePicker.getCalendarViewShown())
        {
            this.mCalendar.set(1, paramInt1);
            this.mCalendar.set(2, paramInt2);
            this.mCalendar.set(5, paramInt3);
            setTitle(DateUtils.formatDateTime(this.mContext, this.mCalendar.getTimeInMillis(), 98326));
            this.mTitleNeedsUpdate = true;
        }
        while (true)
        {
            return;
            if (this.mTitleNeedsUpdate)
            {
                this.mTitleNeedsUpdate = false;
                setTitle(17040417);
            }
        }
    }

    public DatePicker getDatePicker()
    {
        return this.mDatePicker;
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        tryNotifyDateSet();
    }

    public void onDateChanged(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mDatePicker.init(paramInt1, paramInt2, paramInt3, this);
        updateTitle(paramInt1, paramInt2, paramInt3);
    }

    public void onRestoreInstanceState(Bundle paramBundle)
    {
        super.onRestoreInstanceState(paramBundle);
        int i = paramBundle.getInt("year");
        int j = paramBundle.getInt("month");
        int k = paramBundle.getInt("day");
        this.mDatePicker.init(i, j, k, this);
    }

    public Bundle onSaveInstanceState()
    {
        Bundle localBundle = super.onSaveInstanceState();
        localBundle.putInt("year", this.mDatePicker.getYear());
        localBundle.putInt("month", this.mDatePicker.getMonth());
        localBundle.putInt("day", this.mDatePicker.getDayOfMonth());
        return localBundle;
    }

    protected void onStop()
    {
        tryNotifyDateSet();
        super.onStop();
    }

    public void updateDate(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mDatePicker.updateDate(paramInt1, paramInt2, paramInt3);
    }

    public static abstract interface OnDateSetListener
    {
        public abstract void onDateSet(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.DatePickerDialog
 * JD-Core Version:        0.6.2
 */