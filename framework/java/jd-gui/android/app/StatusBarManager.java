package android.app;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Slog;
import com.android.internal.statusbar.IStatusBarService;
import com.android.internal.statusbar.IStatusBarService.Stub;

public class StatusBarManager
{
    public static final int DISABLE_BACK = 4194304;
    public static final int DISABLE_CLOCK = 8388608;
    public static final int DISABLE_EXPAND = 65536;
    public static final int DISABLE_HOME = 2097152;
    public static final int DISABLE_MASK = 33488896;

    @Deprecated
    public static final int DISABLE_NAVIGATION = 18874368;
    public static final int DISABLE_NONE = 0;
    public static final int DISABLE_NOTIFICATION_ALERTS = 262144;
    public static final int DISABLE_NOTIFICATION_ICONS = 131072;
    public static final int DISABLE_NOTIFICATION_TICKER = 524288;
    public static final int DISABLE_RECENT = 16777216;
    public static final int DISABLE_SYSTEM_INFO = 1048576;
    public static final int NAVIGATION_HINT_BACK_ALT = 8;
    public static final int NAVIGATION_HINT_BACK_NOP = 1;
    public static final int NAVIGATION_HINT_HOME_NOP = 2;
    public static final int NAVIGATION_HINT_RECENT_NOP = 4;
    private Context mContext;
    private IStatusBarService mService;
    private IBinder mToken = new Binder();

    StatusBarManager(Context paramContext)
    {
        this.mContext = paramContext;
    }

    /** @deprecated */
    private IStatusBarService getService()
    {
        try
        {
            if (this.mService == null)
            {
                this.mService = IStatusBarService.Stub.asInterface(ServiceManager.getService("statusbar"));
                if (this.mService == null)
                    Slog.w("StatusBarManager", "warning: no STATUS_BAR_SERVICE");
            }
            IStatusBarService localIStatusBarService = this.mService;
            return localIStatusBarService;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void collapse()
    {
        try
        {
            IStatusBarService localIStatusBarService = getService();
            if (localIStatusBarService != null)
                localIStatusBarService.collapse();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void disable(int paramInt)
    {
        try
        {
            IStatusBarService localIStatusBarService = getService();
            if (localIStatusBarService != null)
                localIStatusBarService.disable(paramInt, this.mToken, this.mContext.getPackageName());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void expand()
    {
        try
        {
            IStatusBarService localIStatusBarService = getService();
            if (localIStatusBarService != null)
                localIStatusBarService.expand();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void removeIcon(String paramString)
    {
        try
        {
            IStatusBarService localIStatusBarService = getService();
            if (localIStatusBarService != null)
                localIStatusBarService.removeIcon(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void setIcon(String paramString1, int paramInt1, int paramInt2, String paramString2)
    {
        try
        {
            IStatusBarService localIStatusBarService = getService();
            if (localIStatusBarService != null)
                localIStatusBarService.setIcon(paramString1, this.mContext.getPackageName(), paramInt1, paramInt2, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void setIconVisibility(String paramString, boolean paramBoolean)
    {
        try
        {
            IStatusBarService localIStatusBarService = getService();
            if (localIStatusBarService != null)
                localIStatusBarService.setIconVisibility(paramString, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.StatusBarManager
 * JD-Core Version:        0.6.2
 */