package android.app;

import android.content.Context;
import android.content.IIntentReceiver.Stub;
import android.content.IIntentSender;
import android.content.IIntentSender.Stub;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.util.AndroidException;

public final class PendingIntent
    implements Parcelable
{
    public static final Parcelable.Creator<PendingIntent> CREATOR = new Parcelable.Creator()
    {
        public PendingIntent createFromParcel(Parcel paramAnonymousParcel)
        {
            IBinder localIBinder = paramAnonymousParcel.readStrongBinder();
            if (localIBinder != null);
            for (PendingIntent localPendingIntent = new PendingIntent(localIBinder); ; localPendingIntent = null)
                return localPendingIntent;
        }

        public PendingIntent[] newArray(int paramAnonymousInt)
        {
            return new PendingIntent[paramAnonymousInt];
        }
    };
    public static final int FLAG_CANCEL_CURRENT = 268435456;
    public static final int FLAG_NO_CREATE = 536870912;
    public static final int FLAG_ONE_SHOT = 1073741824;
    public static final int FLAG_UPDATE_CURRENT = 134217728;
    private final IIntentSender mTarget;

    PendingIntent(IIntentSender paramIIntentSender)
    {
        this.mTarget = paramIIntentSender;
    }

    PendingIntent(IBinder paramIBinder)
    {
        this.mTarget = IIntentSender.Stub.asInterface(paramIBinder);
    }

    public static PendingIntent getActivities(Context paramContext, int paramInt1, Intent[] paramArrayOfIntent, int paramInt2)
    {
        return getActivities(paramContext, paramInt1, paramArrayOfIntent, paramInt2, null);
    }

    public static PendingIntent getActivities(Context paramContext, int paramInt1, Intent[] paramArrayOfIntent, int paramInt2, Bundle paramBundle)
    {
        String str = paramContext.getPackageName();
        String[] arrayOfString = new String[paramArrayOfIntent.length];
        for (int i = 0; i < paramArrayOfIntent.length; i++)
        {
            paramArrayOfIntent[i].setAllowFds(false);
            arrayOfString[i] = paramArrayOfIntent[i].resolveTypeIfNeeded(paramContext.getContentResolver());
        }
        try
        {
            IIntentSender localIIntentSender = ActivityManagerNative.getDefault().getIntentSender(2, str, null, null, paramInt1, paramArrayOfIntent, arrayOfString, paramInt2, paramBundle);
            if (localIIntentSender != null);
            for (localPendingIntent = new PendingIntent(localIIntentSender); ; localPendingIntent = null)
                return localPendingIntent;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                PendingIntent localPendingIntent = null;
        }
    }

    public static PendingIntent getActivity(Context paramContext, int paramInt1, Intent paramIntent, int paramInt2)
    {
        return getActivity(paramContext, paramInt1, paramIntent, paramInt2, null);
    }

    public static PendingIntent getActivity(Context paramContext, int paramInt1, Intent paramIntent, int paramInt2, Bundle paramBundle)
    {
        String str1 = paramContext.getPackageName();
        String str2;
        if (paramIntent != null)
            str2 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
            Intent[] arrayOfIntent = new Intent[1];
            arrayOfIntent[0] = paramIntent;
            String[] arrayOfString;
            label58: IIntentSender localIIntentSender;
            if (str2 != null)
            {
                arrayOfString = new String[1];
                arrayOfString[0] = str2;
                localIIntentSender = localIActivityManager.getIntentSender(2, str1, null, null, paramInt1, arrayOfIntent, arrayOfString, paramInt2, paramBundle);
                if (localIIntentSender == null)
                    break label111;
            }
            label111: for (localPendingIntent = new PendingIntent(localIIntentSender); ; localPendingIntent = null)
            {
                return localPendingIntent;
                str2 = null;
                break;
                arrayOfString = null;
                break label58;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                PendingIntent localPendingIntent = null;
        }
    }

    public static PendingIntent getBroadcast(Context paramContext, int paramInt1, Intent paramIntent, int paramInt2)
    {
        String str1 = paramContext.getPackageName();
        String str2;
        if (paramIntent != null)
            str2 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
            Intent[] arrayOfIntent = new Intent[1];
            arrayOfIntent[0] = paramIntent;
            String[] arrayOfString;
            label58: IIntentSender localIIntentSender;
            if (str2 != null)
            {
                arrayOfString = new String[1];
                arrayOfString[0] = str2;
                localIIntentSender = localIActivityManager.getIntentSender(1, str1, null, null, paramInt1, arrayOfIntent, arrayOfString, paramInt2, null);
                if (localIIntentSender == null)
                    break label110;
            }
            label110: for (localPendingIntent = new PendingIntent(localIIntentSender); ; localPendingIntent = null)
            {
                return localPendingIntent;
                str2 = null;
                break;
                arrayOfString = null;
                break label58;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                PendingIntent localPendingIntent = null;
        }
    }

    public static PendingIntent getService(Context paramContext, int paramInt1, Intent paramIntent, int paramInt2)
    {
        String str1 = paramContext.getPackageName();
        String str2;
        if (paramIntent != null)
            str2 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
            Intent[] arrayOfIntent = new Intent[1];
            arrayOfIntent[0] = paramIntent;
            String[] arrayOfString;
            label58: IIntentSender localIIntentSender;
            if (str2 != null)
            {
                arrayOfString = new String[1];
                arrayOfString[0] = str2;
                localIIntentSender = localIActivityManager.getIntentSender(4, str1, null, null, paramInt1, arrayOfIntent, arrayOfString, paramInt2, null);
                if (localIIntentSender == null)
                    break label110;
            }
            label110: for (localPendingIntent = new PendingIntent(localIIntentSender); ; localPendingIntent = null)
            {
                return localPendingIntent;
                str2 = null;
                break;
                arrayOfString = null;
                break label58;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                PendingIntent localPendingIntent = null;
        }
    }

    public static PendingIntent readPendingIntentOrNullFromParcel(Parcel paramParcel)
    {
        IBinder localIBinder = paramParcel.readStrongBinder();
        if (localIBinder != null);
        for (PendingIntent localPendingIntent = new PendingIntent(localIBinder); ; localPendingIntent = null)
            return localPendingIntent;
    }

    public static void writePendingIntentOrNullToParcel(PendingIntent paramPendingIntent, Parcel paramParcel)
    {
        if (paramPendingIntent != null);
        for (IBinder localIBinder = paramPendingIntent.mTarget.asBinder(); ; localIBinder = null)
        {
            paramParcel.writeStrongBinder(localIBinder);
            return;
        }
    }

    public void cancel()
    {
        try
        {
            ActivityManagerNative.getDefault().cancelIntentSender(this.mTarget);
            label12: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label12;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        if ((paramObject instanceof PendingIntent));
        for (boolean bool = this.mTarget.asBinder().equals(((PendingIntent)paramObject).mTarget.asBinder()); ; bool = false)
            return bool;
    }

    public IntentSender getIntentSender()
    {
        return new IntentSender(this.mTarget);
    }

    public IIntentSender getTarget()
    {
        return this.mTarget;
    }

    public String getTargetPackage()
    {
        try
        {
            String str2 = ActivityManagerNative.getDefault().getPackageForIntentSender(this.mTarget);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String str1 = null;
        }
    }

    public int hashCode()
    {
        return this.mTarget.asBinder().hashCode();
    }

    public boolean isActivity()
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().isIntentSenderAnActivity(this.mTarget);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isTargetedToPackage()
    {
        try
        {
            boolean bool2 = ActivityManagerNative.getDefault().isIntentSenderTargetedToPackage(this.mTarget);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void send()
        throws PendingIntent.CanceledException
    {
        send(null, 0, null, null, null, null);
    }

    public void send(int paramInt)
        throws PendingIntent.CanceledException
    {
        send(null, paramInt, null, null, null, null);
    }

    public void send(int paramInt, OnFinished paramOnFinished, Handler paramHandler)
        throws PendingIntent.CanceledException
    {
        send(null, paramInt, null, paramOnFinished, paramHandler, null);
    }

    public void send(Context paramContext, int paramInt, Intent paramIntent)
        throws PendingIntent.CanceledException
    {
        send(paramContext, paramInt, paramIntent, null, null, null);
    }

    public void send(Context paramContext, int paramInt, Intent paramIntent, OnFinished paramOnFinished, Handler paramHandler)
        throws PendingIntent.CanceledException
    {
        send(paramContext, paramInt, paramIntent, paramOnFinished, paramHandler, null);
    }

    public void send(Context paramContext, int paramInt, Intent paramIntent, OnFinished paramOnFinished, Handler paramHandler, String paramString)
        throws PendingIntent.CanceledException
    {
        FinishedDispatcher localFinishedDispatcher = null;
        if (paramIntent != null);
        while (true)
        {
            try
            {
                str = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
                IIntentSender localIIntentSender = this.mTarget;
                if (paramOnFinished != null)
                    localFinishedDispatcher = new FinishedDispatcher(this, paramOnFinished, paramHandler);
                if (localIIntentSender.send(paramInt, paramIntent, str, localFinishedDispatcher, paramString) >= 0)
                    break;
                throw new CanceledException();
            }
            catch (RemoteException localRemoteException)
            {
                throw new CanceledException(localRemoteException);
            }
            String str = null;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("PendingIntent{");
        localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuilder.append(": ");
        if (this.mTarget != null);
        for (IBinder localIBinder = this.mTarget.asBinder(); ; localIBinder = null)
        {
            localStringBuilder.append(localIBinder);
            localStringBuilder.append('}');
            return localStringBuilder.toString();
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongBinder(this.mTarget.asBinder());
    }

    private static class FinishedDispatcher extends IIntentReceiver.Stub
        implements Runnable
    {
        private final Handler mHandler;
        private Intent mIntent;
        private final PendingIntent mPendingIntent;
        private int mResultCode;
        private String mResultData;
        private Bundle mResultExtras;
        private final PendingIntent.OnFinished mWho;

        FinishedDispatcher(PendingIntent paramPendingIntent, PendingIntent.OnFinished paramOnFinished, Handler paramHandler)
        {
            this.mPendingIntent = paramPendingIntent;
            this.mWho = paramOnFinished;
            this.mHandler = paramHandler;
        }

        public void performReceive(Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
        {
            this.mIntent = paramIntent;
            this.mResultCode = paramInt;
            this.mResultData = paramString;
            this.mResultExtras = paramBundle;
            if (this.mHandler == null)
                run();
            while (true)
            {
                return;
                this.mHandler.post(this);
            }
        }

        public void run()
        {
            this.mWho.onSendFinished(this.mPendingIntent, this.mIntent, this.mResultCode, this.mResultData, this.mResultExtras);
        }
    }

    public static abstract interface OnFinished
    {
        public abstract void onSendFinished(PendingIntent paramPendingIntent, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle);
    }

    public static class CanceledException extends AndroidException
    {
        public CanceledException()
        {
        }

        public CanceledException(Exception paramException)
        {
            super();
        }

        public CanceledException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.PendingIntent
 * JD-Core Version:        0.6.2
 */