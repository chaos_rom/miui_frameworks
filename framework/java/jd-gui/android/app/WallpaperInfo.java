package android.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Printer;
import android.util.Xml;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public final class WallpaperInfo
    implements Parcelable
{
    public static final Parcelable.Creator<WallpaperInfo> CREATOR = new Parcelable.Creator()
    {
        public WallpaperInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new WallpaperInfo(paramAnonymousParcel);
        }

        public WallpaperInfo[] newArray(int paramAnonymousInt)
        {
            return new WallpaperInfo[paramAnonymousInt];
        }
    };
    static final String TAG = "WallpaperInfo";
    final int mAuthorResource;
    final int mDescriptionResource;
    final ResolveInfo mService;
    final String mSettingsActivityName;
    final int mThumbnailResource;

    public WallpaperInfo(Context paramContext, ResolveInfo paramResolveInfo)
        throws XmlPullParserException, IOException
    {
        this.mService = paramResolveInfo;
        ServiceInfo localServiceInfo = paramResolveInfo.serviceInfo;
        PackageManager localPackageManager = paramContext.getPackageManager();
        XmlResourceParser localXmlResourceParser = null;
        try
        {
            localXmlResourceParser = localServiceInfo.loadXmlMetaData(localPackageManager, "android.service.wallpaper");
            if (localXmlResourceParser == null)
                throw new XmlPullParserException("No android.service.wallpaper meta-data");
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new XmlPullParserException("Unable to create context for: " + localServiceInfo.packageName);
        }
        finally
        {
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
        Resources localResources = localPackageManager.getResourcesForApplication(localServiceInfo.applicationInfo);
        AttributeSet localAttributeSet = Xml.asAttributeSet(localXmlResourceParser);
        int i;
        do
            i = localXmlResourceParser.next();
        while ((i != 1) && (i != 2));
        if (!"wallpaper".equals(localXmlResourceParser.getName()))
            throw new XmlPullParserException("Meta-data does not start with wallpaper tag");
        TypedArray localTypedArray = localResources.obtainAttributes(localAttributeSet, R.styleable.Wallpaper);
        String str = localTypedArray.getString(1);
        int j = localTypedArray.getResourceId(2, -1);
        int k = localTypedArray.getResourceId(3, -1);
        int m = localTypedArray.getResourceId(0, -1);
        localTypedArray.recycle();
        if (localXmlResourceParser != null)
            localXmlResourceParser.close();
        this.mSettingsActivityName = str;
        this.mThumbnailResource = j;
        this.mAuthorResource = k;
        this.mDescriptionResource = m;
    }

    WallpaperInfo(Parcel paramParcel)
    {
        this.mSettingsActivityName = paramParcel.readString();
        this.mThumbnailResource = paramParcel.readInt();
        this.mAuthorResource = paramParcel.readInt();
        this.mDescriptionResource = paramParcel.readInt();
        this.mService = ((ResolveInfo)ResolveInfo.CREATOR.createFromParcel(paramParcel));
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + "Service:");
        this.mService.dump(paramPrinter, paramString + "    ");
        paramPrinter.println(paramString + "mSettingsActivityName=" + this.mSettingsActivityName);
    }

    public ComponentName getComponent()
    {
        return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
    }

    public String getPackageName()
    {
        return this.mService.serviceInfo.packageName;
    }

    public ServiceInfo getServiceInfo()
    {
        return this.mService.serviceInfo;
    }

    public String getServiceName()
    {
        return this.mService.serviceInfo.name;
    }

    public String getSettingsActivity()
    {
        return this.mSettingsActivityName;
    }

    public CharSequence loadAuthor(PackageManager paramPackageManager)
        throws Resources.NotFoundException
    {
        if (this.mAuthorResource <= 0)
            throw new Resources.NotFoundException();
        String str = this.mService.resolvePackageName;
        ApplicationInfo localApplicationInfo = null;
        if (str == null)
        {
            str = this.mService.serviceInfo.packageName;
            localApplicationInfo = this.mService.serviceInfo.applicationInfo;
        }
        return paramPackageManager.getText(str, this.mAuthorResource, localApplicationInfo);
    }

    public CharSequence loadDescription(PackageManager paramPackageManager)
        throws Resources.NotFoundException
    {
        String str = this.mService.resolvePackageName;
        ApplicationInfo localApplicationInfo = null;
        if (str == null)
        {
            str = this.mService.serviceInfo.packageName;
            localApplicationInfo = this.mService.serviceInfo.applicationInfo;
        }
        if (this.mService.serviceInfo.descriptionRes != 0);
        for (CharSequence localCharSequence = paramPackageManager.getText(str, this.mService.serviceInfo.descriptionRes, localApplicationInfo); ; localCharSequence = paramPackageManager.getText(str, this.mDescriptionResource, this.mService.serviceInfo.applicationInfo))
        {
            return localCharSequence;
            if (this.mDescriptionResource <= 0)
                throw new Resources.NotFoundException();
        }
    }

    public Drawable loadIcon(PackageManager paramPackageManager)
    {
        return this.mService.loadIcon(paramPackageManager);
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        return this.mService.loadLabel(paramPackageManager);
    }

    public Drawable loadThumbnail(PackageManager paramPackageManager)
    {
        if (this.mThumbnailResource < 0);
        for (Drawable localDrawable = null; ; localDrawable = paramPackageManager.getDrawable(this.mService.serviceInfo.packageName, this.mThumbnailResource, this.mService.serviceInfo.applicationInfo))
            return localDrawable;
    }

    public String toString()
    {
        return "WallpaperInfo{" + this.mService.serviceInfo.name + ", settings: " + this.mSettingsActivityName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mSettingsActivityName);
        paramParcel.writeInt(this.mThumbnailResource);
        paramParcel.writeInt(this.mAuthorResource);
        paramParcel.writeInt(this.mDescriptionResource);
        this.mService.writeToParcel(paramParcel, paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.WallpaperInfo
 * JD-Core Version:        0.6.2
 */