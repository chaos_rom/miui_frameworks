package android.app;

import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.internal.R.styleable;
import java.text.NumberFormat;

public class ProgressDialog extends AlertDialog
{
    public static final int STYLE_HORIZONTAL = 1;
    public static final int STYLE_SPINNER;
    private boolean mHasStarted;
    private int mIncrementBy;
    private int mIncrementSecondaryBy;
    private boolean mIndeterminate;
    private Drawable mIndeterminateDrawable;
    private int mMax;
    private CharSequence mMessage;
    private TextView mMessageView;
    private ProgressBar mProgress;
    private Drawable mProgressDrawable;
    private TextView mProgressNumber;
    private String mProgressNumberFormat;
    private TextView mProgressPercent;
    private NumberFormat mProgressPercentFormat;
    private int mProgressStyle = 0;
    private int mProgressVal;
    private int mSecondaryProgressVal;
    private Handler mViewUpdateHandler;

    public ProgressDialog(Context paramContext)
    {
        super(paramContext);
        initFormats();
    }

    public ProgressDialog(Context paramContext, int paramInt)
    {
        super(paramContext, paramInt);
        initFormats();
    }

    private void initFormats()
    {
        this.mProgressNumberFormat = "%1d/%2d";
        this.mProgressPercentFormat = NumberFormat.getPercentInstance();
        this.mProgressPercentFormat.setMaximumFractionDigits(0);
    }

    private void onProgressChanged()
    {
        if ((this.mProgressStyle == 1) && (this.mViewUpdateHandler != null) && (!this.mViewUpdateHandler.hasMessages(0)))
            this.mViewUpdateHandler.sendEmptyMessage(0);
    }

    public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        return show(paramContext, paramCharSequence1, paramCharSequence2, false);
    }

    public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
    {
        return show(paramContext, paramCharSequence1, paramCharSequence2, paramBoolean, false, null);
    }

    public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean1, boolean paramBoolean2)
    {
        return show(paramContext, paramCharSequence1, paramCharSequence2, paramBoolean1, paramBoolean2, null);
    }

    public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean1, boolean paramBoolean2, DialogInterface.OnCancelListener paramOnCancelListener)
    {
        ProgressDialog localProgressDialog = new ProgressDialog(paramContext);
        localProgressDialog.setTitle(paramCharSequence1);
        localProgressDialog.setMessage(paramCharSequence2);
        localProgressDialog.setIndeterminate(paramBoolean1);
        localProgressDialog.setCancelable(paramBoolean2);
        localProgressDialog.setOnCancelListener(paramOnCancelListener);
        localProgressDialog.show();
        return localProgressDialog;
    }

    public int getMax()
    {
        if (this.mProgress != null);
        for (int i = this.mProgress.getMax(); ; i = this.mMax)
            return i;
    }

    public int getProgress()
    {
        if (this.mProgress != null);
        for (int i = this.mProgress.getProgress(); ; i = this.mProgressVal)
            return i;
    }

    public int getSecondaryProgress()
    {
        if (this.mProgress != null);
        for (int i = this.mProgress.getSecondaryProgress(); ; i = this.mSecondaryProgressVal)
            return i;
    }

    public void incrementProgressBy(int paramInt)
    {
        if (this.mProgress != null)
        {
            this.mProgress.incrementProgressBy(paramInt);
            onProgressChanged();
        }
        while (true)
        {
            return;
            this.mIncrementBy = (paramInt + this.mIncrementBy);
        }
    }

    public void incrementSecondaryProgressBy(int paramInt)
    {
        if (this.mProgress != null)
        {
            this.mProgress.incrementSecondaryProgressBy(paramInt);
            onProgressChanged();
        }
        while (true)
        {
            return;
            this.mIncrementSecondaryBy = (paramInt + this.mIncrementSecondaryBy);
        }
    }

    public boolean isIndeterminate()
    {
        if (this.mProgress != null);
        for (boolean bool = this.mProgress.isIndeterminate(); ; bool = this.mIndeterminate)
            return bool;
    }

    protected void onCreate(Bundle paramBundle)
    {
        LayoutInflater localLayoutInflater = LayoutInflater.from(this.mContext);
        TypedArray localTypedArray = this.mContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 0);
        if (this.mProgressStyle == 1)
        {
            this.mViewUpdateHandler = new Handler()
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    super.handleMessage(paramAnonymousMessage);
                    int i = ProgressDialog.this.mProgress.getProgress();
                    int j = ProgressDialog.this.mProgress.getMax();
                    if (ProgressDialog.this.mProgressNumberFormat != null)
                    {
                        String str = ProgressDialog.this.mProgressNumberFormat;
                        TextView localTextView = ProgressDialog.this.mProgressNumber;
                        Object[] arrayOfObject = new Object[2];
                        arrayOfObject[0] = Integer.valueOf(i);
                        arrayOfObject[1] = Integer.valueOf(j);
                        localTextView.setText(String.format(str, arrayOfObject));
                        if (ProgressDialog.this.mProgressPercentFormat == null)
                            break label176;
                        double d = i / j;
                        SpannableString localSpannableString = new SpannableString(ProgressDialog.this.mProgressPercentFormat.format(d));
                        localSpannableString.setSpan(new StyleSpan(1), 0, localSpannableString.length(), 33);
                        ProgressDialog.this.mProgressPercent.setText(localSpannableString);
                    }
                    while (true)
                    {
                        return;
                        ProgressDialog.this.mProgressNumber.setText("");
                        break;
                        label176: ProgressDialog.this.mProgressPercent.setText("");
                    }
                }
            };
            View localView2 = localLayoutInflater.inflate(localTypedArray.getResourceId(16, 17367077), null);
            this.mProgress = ((ProgressBar)localView2.findViewById(16908301));
            this.mProgressNumber = ((TextView)localView2.findViewById(16908887));
            this.mProgressPercent = ((TextView)localView2.findViewById(16908886));
            setView(localView2);
        }
        while (true)
        {
            localTypedArray.recycle();
            if (this.mMax > 0)
                setMax(this.mMax);
            if (this.mProgressVal > 0)
                setProgress(this.mProgressVal);
            if (this.mSecondaryProgressVal > 0)
                setSecondaryProgress(this.mSecondaryProgressVal);
            if (this.mIncrementBy > 0)
                incrementProgressBy(this.mIncrementBy);
            if (this.mIncrementSecondaryBy > 0)
                incrementSecondaryProgressBy(this.mIncrementSecondaryBy);
            if (this.mProgressDrawable != null)
                setProgressDrawable(this.mProgressDrawable);
            if (this.mIndeterminateDrawable != null)
                setIndeterminateDrawable(this.mIndeterminateDrawable);
            if (this.mMessage != null)
                setMessage(this.mMessage);
            setIndeterminate(this.mIndeterminate);
            onProgressChanged();
            super.onCreate(paramBundle);
            return;
            View localView1 = localLayoutInflater.inflate(localTypedArray.getResourceId(15, 17367182), null);
            this.mProgress = ((ProgressBar)localView1.findViewById(16908301));
            this.mMessageView = ((TextView)localView1.findViewById(16908299));
            setView(localView1);
        }
    }

    public void onStart()
    {
        super.onStart();
        this.mHasStarted = true;
    }

    protected void onStop()
    {
        super.onStop();
        this.mHasStarted = false;
    }

    public void setIndeterminate(boolean paramBoolean)
    {
        if (this.mProgress != null)
            this.mProgress.setIndeterminate(paramBoolean);
        while (true)
        {
            return;
            this.mIndeterminate = paramBoolean;
        }
    }

    public void setIndeterminateDrawable(Drawable paramDrawable)
    {
        if (this.mProgress != null)
            this.mProgress.setIndeterminateDrawable(paramDrawable);
        while (true)
        {
            return;
            this.mIndeterminateDrawable = paramDrawable;
        }
    }

    public void setMax(int paramInt)
    {
        if (this.mProgress != null)
        {
            this.mProgress.setMax(paramInt);
            onProgressChanged();
        }
        while (true)
        {
            return;
            this.mMax = paramInt;
        }
    }

    public void setMessage(CharSequence paramCharSequence)
    {
        if (this.mProgress != null)
            if (this.mProgressStyle == 1)
                super.setMessage(paramCharSequence);
        while (true)
        {
            return;
            this.mMessageView.setText(paramCharSequence);
            continue;
            this.mMessage = paramCharSequence;
        }
    }

    public void setProgress(int paramInt)
    {
        if (this.mHasStarted)
        {
            this.mProgress.setProgress(paramInt);
            onProgressChanged();
        }
        while (true)
        {
            return;
            this.mProgressVal = paramInt;
        }
    }

    public void setProgressDrawable(Drawable paramDrawable)
    {
        if (this.mProgress != null)
            this.mProgress.setProgressDrawable(paramDrawable);
        while (true)
        {
            return;
            this.mProgressDrawable = paramDrawable;
        }
    }

    public void setProgressNumberFormat(String paramString)
    {
        this.mProgressNumberFormat = paramString;
        onProgressChanged();
    }

    public void setProgressPercentFormat(NumberFormat paramNumberFormat)
    {
        this.mProgressPercentFormat = paramNumberFormat;
        onProgressChanged();
    }

    public void setProgressStyle(int paramInt)
    {
        this.mProgressStyle = paramInt;
    }

    public void setSecondaryProgress(int paramInt)
    {
        if (this.mProgress != null)
        {
            this.mProgress.setSecondaryProgress(paramInt);
            onProgressChanged();
        }
        while (true)
        {
            return;
            this.mSecondaryProgressVal = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ProgressDialog
 * JD-Core Version:        0.6.2
 */