package android.app;

import android.content.ComponentCallbacks;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;

public class Application extends ContextWrapper
    implements ComponentCallbacks2
{
    private ArrayList<ActivityLifecycleCallbacks> mActivityLifecycleCallbacks = new ArrayList();
    private ArrayList<ComponentCallbacks> mComponentCallbacks = new ArrayList();
    public LoadedApk mLoadedApk;

    public Application()
    {
        super(null);
    }

    private Object[] collectActivityLifecycleCallbacks()
    {
        Object[] arrayOfObject = null;
        synchronized (this.mActivityLifecycleCallbacks)
        {
            if (this.mActivityLifecycleCallbacks.size() > 0)
                arrayOfObject = this.mActivityLifecycleCallbacks.toArray();
            return arrayOfObject;
        }
    }

    private Object[] collectComponentCallbacks()
    {
        Object[] arrayOfObject = null;
        synchronized (this.mComponentCallbacks)
        {
            if (this.mComponentCallbacks.size() > 0)
                arrayOfObject = this.mComponentCallbacks.toArray();
            return arrayOfObject;
        }
    }

    final void attach(Context paramContext)
    {
        attachBaseContext(paramContext);
        this.mLoadedApk = ContextImpl.getImpl(paramContext).mPackageInfo;
    }

    void dispatchActivityCreated(Activity paramActivity, Bundle paramBundle)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivityCreated(paramActivity, paramBundle);
    }

    void dispatchActivityDestroyed(Activity paramActivity)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivityDestroyed(paramActivity);
    }

    void dispatchActivityPaused(Activity paramActivity)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivityPaused(paramActivity);
    }

    void dispatchActivityResumed(Activity paramActivity)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivityResumed(paramActivity);
    }

    void dispatchActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivitySaveInstanceState(paramActivity, paramBundle);
    }

    void dispatchActivityStarted(Activity paramActivity)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivityStarted(paramActivity);
    }

    void dispatchActivityStopped(Activity paramActivity)
    {
        Object[] arrayOfObject = collectActivityLifecycleCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ActivityLifecycleCallbacks)arrayOfObject[i]).onActivityStopped(paramActivity);
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        Object[] arrayOfObject = collectComponentCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ComponentCallbacks)arrayOfObject[i]).onConfigurationChanged(paramConfiguration);
    }

    public void onCreate()
    {
    }

    public void onLowMemory()
    {
        Object[] arrayOfObject = collectComponentCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
                ((ComponentCallbacks)arrayOfObject[i]).onLowMemory();
    }

    public void onTerminate()
    {
    }

    public void onTrimMemory(int paramInt)
    {
        Object[] arrayOfObject = collectComponentCallbacks();
        if (arrayOfObject != null)
            for (int i = 0; i < arrayOfObject.length; i++)
            {
                Object localObject = arrayOfObject[i];
                if ((localObject instanceof ComponentCallbacks2))
                    ((ComponentCallbacks2)localObject).onTrimMemory(paramInt);
            }
    }

    public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks paramActivityLifecycleCallbacks)
    {
        synchronized (this.mActivityLifecycleCallbacks)
        {
            this.mActivityLifecycleCallbacks.add(paramActivityLifecycleCallbacks);
            return;
        }
    }

    public void registerComponentCallbacks(ComponentCallbacks paramComponentCallbacks)
    {
        synchronized (this.mComponentCallbacks)
        {
            this.mComponentCallbacks.add(paramComponentCallbacks);
            return;
        }
    }

    public void unregisterActivityLifecycleCallbacks(ActivityLifecycleCallbacks paramActivityLifecycleCallbacks)
    {
        synchronized (this.mActivityLifecycleCallbacks)
        {
            this.mActivityLifecycleCallbacks.remove(paramActivityLifecycleCallbacks);
            return;
        }
    }

    public void unregisterComponentCallbacks(ComponentCallbacks paramComponentCallbacks)
    {
        synchronized (this.mComponentCallbacks)
        {
            this.mComponentCallbacks.remove(paramComponentCallbacks);
            return;
        }
    }

    public static abstract interface ActivityLifecycleCallbacks
    {
        public abstract void onActivityCreated(Activity paramActivity, Bundle paramBundle);

        public abstract void onActivityDestroyed(Activity paramActivity);

        public abstract void onActivityPaused(Activity paramActivity);

        public abstract void onActivityResumed(Activity paramActivity);

        public abstract void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle);

        public abstract void onActivityStarted(Activity paramActivity);

        public abstract void onActivityStopped(Activity paramActivity);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Application
 * JD-Core Version:        0.6.2
 */