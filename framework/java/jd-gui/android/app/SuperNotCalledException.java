package android.app;

import android.util.AndroidRuntimeException;

final class SuperNotCalledException extends AndroidRuntimeException
{
    public SuperNotCalledException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.SuperNotCalledException
 * JD-Core Version:        0.6.2
 */