package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IProcessObserver extends IInterface
{
    public abstract void onForegroundActivitiesChanged(int paramInt1, int paramInt2, boolean paramBoolean)
        throws RemoteException;

    public abstract void onImportanceChanged(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void onProcessDied(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IProcessObserver
    {
        private static final String DESCRIPTOR = "android.app.IProcessObserver";
        static final int TRANSACTION_onForegroundActivitiesChanged = 1;
        static final int TRANSACTION_onImportanceChanged = 2;
        static final int TRANSACTION_onProcessDied = 3;

        public Stub()
        {
            attachInterface(this, "android.app.IProcessObserver");
        }

        public static IProcessObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IProcessObserver");
                if ((localIInterface != null) && ((localIInterface instanceof IProcessObserver)))
                    localObject = (IProcessObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.app.IProcessObserver");
                continue;
                paramParcel1.enforceInterface("android.app.IProcessObserver");
                int i = paramParcel1.readInt();
                int j = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    onForegroundActivitiesChanged(i, j, bool2);
                    break;
                }
                paramParcel1.enforceInterface("android.app.IProcessObserver");
                onImportanceChanged(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.app.IProcessObserver");
                onProcessDied(paramParcel1.readInt(), paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IProcessObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IProcessObserver";
            }

            public void onForegroundActivitiesChanged(int paramInt1, int paramInt2, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IProcessObserver");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onImportanceChanged(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IProcessObserver");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onProcessDied(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.app.IProcessObserver");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IProcessObserver
 * JD-Core Version:        0.6.2
 */