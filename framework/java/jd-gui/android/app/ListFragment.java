package android.app;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListFragment extends Fragment
{
    ListAdapter mAdapter;
    CharSequence mEmptyText;
    View mEmptyView;
    private final Handler mHandler = new Handler();
    ListView mList;
    View mListContainer;
    boolean mListShown;
    private final AdapterView.OnItemClickListener mOnClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
            ListFragment.this.onListItemClick((ListView)paramAnonymousAdapterView, paramAnonymousView, paramAnonymousInt, paramAnonymousLong);
        }
    };
    View mProgressContainer;
    private final Runnable mRequestFocus = new Runnable()
    {
        public void run()
        {
            ListFragment.this.mList.focusableViewAvailable(ListFragment.this.mList);
        }
    };
    TextView mStandardEmptyView;

    private void ensureList()
    {
        if (this.mList != null)
            return;
        View localView1 = getView();
        if (localView1 == null)
            throw new IllegalStateException("Content view not yet created");
        if ((localView1 instanceof ListView))
        {
            this.mList = ((ListView)localView1);
            label42: this.mListShown = true;
            this.mList.setOnItemClickListener(this.mOnClickListener);
            if (this.mAdapter == null)
                break label261;
            ListAdapter localListAdapter = this.mAdapter;
            this.mAdapter = null;
            setListAdapter(localListAdapter);
        }
        while (true)
        {
            this.mHandler.post(this.mRequestFocus);
            break;
            this.mStandardEmptyView = ((TextView)localView1.findViewById(16909009));
            if (this.mStandardEmptyView == null)
                this.mEmptyView = localView1.findViewById(16908292);
            View localView2;
            while (true)
            {
                this.mProgressContainer = localView1.findViewById(16909007);
                this.mListContainer = localView1.findViewById(16909008);
                localView2 = localView1.findViewById(16908298);
                if ((localView2 instanceof ListView))
                    break;
                throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                this.mStandardEmptyView.setVisibility(8);
            }
            this.mList = ((ListView)localView2);
            if (this.mList == null)
                throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
            if (this.mEmptyView != null)
            {
                this.mList.setEmptyView(this.mEmptyView);
                break label42;
            }
            if (this.mEmptyText == null)
                break label42;
            this.mStandardEmptyView.setText(this.mEmptyText);
            this.mList.setEmptyView(this.mStandardEmptyView);
            break label42;
            label261: if (this.mProgressContainer != null)
                setListShown(false, false);
        }
    }

    private void setListShown(boolean paramBoolean1, boolean paramBoolean2)
    {
        ensureList();
        if (this.mProgressContainer == null)
            throw new IllegalStateException("Can't be used with a custom content view");
        if (this.mListShown == paramBoolean1)
            return;
        this.mListShown = paramBoolean1;
        if (paramBoolean1)
        {
            if (paramBoolean2)
            {
                this.mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432577));
                this.mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432576));
            }
            while (true)
            {
                this.mProgressContainer.setVisibility(8);
                this.mListContainer.setVisibility(0);
                break;
                this.mProgressContainer.clearAnimation();
                this.mListContainer.clearAnimation();
            }
        }
        if (paramBoolean2)
        {
            this.mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432576));
            this.mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432577));
        }
        while (true)
        {
            this.mProgressContainer.setVisibility(0);
            this.mListContainer.setVisibility(8);
            break;
            this.mProgressContainer.clearAnimation();
            this.mListContainer.clearAnimation();
        }
    }

    public ListAdapter getListAdapter()
    {
        return this.mAdapter;
    }

    public ListView getListView()
    {
        ensureList();
        return this.mList;
    }

    public long getSelectedItemId()
    {
        ensureList();
        return this.mList.getSelectedItemId();
    }

    public int getSelectedItemPosition()
    {
        ensureList();
        return this.mList.getSelectedItemPosition();
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
        return paramLayoutInflater.inflate(17367060, paramViewGroup, false);
    }

    public void onDestroyView()
    {
        this.mHandler.removeCallbacks(this.mRequestFocus);
        this.mList = null;
        this.mListShown = false;
        this.mListContainer = null;
        this.mProgressContainer = null;
        this.mEmptyView = null;
        this.mStandardEmptyView = null;
        super.onDestroyView();
    }

    public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
    {
    }

    public void onViewCreated(View paramView, Bundle paramBundle)
    {
        super.onViewCreated(paramView, paramBundle);
        ensureList();
    }

    public void setEmptyText(CharSequence paramCharSequence)
    {
        ensureList();
        if (this.mStandardEmptyView == null)
            throw new IllegalStateException("Can't be used with a custom content view");
        this.mStandardEmptyView.setText(paramCharSequence);
        if (this.mEmptyText == null)
            this.mList.setEmptyView(this.mStandardEmptyView);
        this.mEmptyText = paramCharSequence;
    }

    public void setListAdapter(ListAdapter paramListAdapter)
    {
        boolean bool = false;
        if (this.mAdapter != null);
        for (int i = 1; ; i = 0)
        {
            this.mAdapter = paramListAdapter;
            if (this.mList != null)
            {
                this.mList.setAdapter(paramListAdapter);
                if ((!this.mListShown) && (i == 0))
                {
                    if (getView().getWindowToken() != null)
                        bool = true;
                    setListShown(true, bool);
                }
            }
            return;
        }
    }

    public void setListShown(boolean paramBoolean)
    {
        setListShown(paramBoolean, true);
    }

    public void setListShownNoAnimation(boolean paramBoolean)
    {
        setListShown(paramBoolean, false);
    }

    public void setSelection(int paramInt)
    {
        ensureList();
        this.mList.setSelection(paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ListFragment
 * JD-Core Version:        0.6.2
 */