package android.app;

import android.animation.LayoutTransition;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentBreadCrumbs extends ViewGroup
    implements FragmentManager.OnBackStackChangedListener
{
    Activity mActivity;
    LinearLayout mContainer;
    LayoutInflater mInflater;
    int mMaxVisible = -1;
    private OnBreadCrumbClickListener mOnBreadCrumbClickListener;
    private View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            FragmentManager.BackStackEntry localBackStackEntry1;
            if ((paramAnonymousView.getTag() instanceof FragmentManager.BackStackEntry))
            {
                localBackStackEntry1 = (FragmentManager.BackStackEntry)paramAnonymousView.getTag();
                if (localBackStackEntry1 != FragmentBreadCrumbs.this.mParentEntry)
                    break label53;
                if (FragmentBreadCrumbs.this.mParentClickListener != null)
                    FragmentBreadCrumbs.this.mParentClickListener.onClick(paramAnonymousView);
            }
            while (true)
            {
                return;
                label53: FragmentBreadCrumbs.OnBreadCrumbClickListener localOnBreadCrumbClickListener;
                if (FragmentBreadCrumbs.this.mOnBreadCrumbClickListener != null)
                {
                    localOnBreadCrumbClickListener = FragmentBreadCrumbs.this.mOnBreadCrumbClickListener;
                    if (localBackStackEntry1 != FragmentBreadCrumbs.this.mTopEntry)
                        break label124;
                }
                label124: for (FragmentManager.BackStackEntry localBackStackEntry2 = null; ; localBackStackEntry2 = localBackStackEntry1)
                {
                    if (localOnBreadCrumbClickListener.onBreadCrumbClick(localBackStackEntry2, 0))
                        break label128;
                    if (localBackStackEntry1 != FragmentBreadCrumbs.this.mTopEntry)
                        break label130;
                    FragmentBreadCrumbs.this.mActivity.getFragmentManager().popBackStack();
                    break;
                }
                label128: continue;
                label130: FragmentBreadCrumbs.this.mActivity.getFragmentManager().popBackStack(localBackStackEntry1.getId(), 0);
            }
        }
    };
    private View.OnClickListener mParentClickListener;
    BackStackRecord mParentEntry;
    BackStackRecord mTopEntry;

    public FragmentBreadCrumbs(Context paramContext)
    {
        this(paramContext, null);
    }

    public FragmentBreadCrumbs(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16973961);
    }

    public FragmentBreadCrumbs(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    private BackStackRecord createBackStackEntry(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        BackStackRecord localBackStackRecord;
        if (paramCharSequence1 == null)
            localBackStackRecord = null;
        while (true)
        {
            return localBackStackRecord;
            localBackStackRecord = new BackStackRecord((FragmentManagerImpl)this.mActivity.getFragmentManager());
            localBackStackRecord.setBreadCrumbTitle(paramCharSequence1);
            localBackStackRecord.setBreadCrumbShortTitle(paramCharSequence2);
        }
    }

    private FragmentManager.BackStackEntry getPreEntry(int paramInt)
    {
        BackStackRecord localBackStackRecord;
        if (this.mParentEntry != null)
            if (paramInt == 0)
                localBackStackRecord = this.mParentEntry;
        while (true)
        {
            return localBackStackRecord;
            localBackStackRecord = this.mTopEntry;
            continue;
            localBackStackRecord = this.mTopEntry;
        }
    }

    private int getPreEntryCount()
    {
        int i = 1;
        int j;
        if (this.mTopEntry != null)
        {
            j = i;
            if (this.mParentEntry == null)
                break label27;
        }
        while (true)
        {
            return j + i;
            j = 0;
            break;
            label27: i = 0;
        }
    }

    public void onBackStackChanged()
    {
        updateCrumbs();
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = getChildCount();
        for (int j = 0; j < i; j++)
        {
            View localView = getChildAt(j);
            int k = this.mPaddingLeft + localView.getMeasuredWidth() - this.mPaddingRight;
            int m = this.mPaddingTop + localView.getMeasuredHeight() - this.mPaddingBottom;
            localView.layout(this.mPaddingLeft, this.mPaddingTop, k, m);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getChildCount();
        int j = 0;
        int k = 0;
        int m = 0;
        for (int n = 0; n < i; n++)
        {
            View localView = getChildAt(n);
            if (localView.getVisibility() != 8)
            {
                measureChild(localView, paramInt1, paramInt2);
                k = Math.max(k, localView.getMeasuredWidth());
                j = Math.max(j, localView.getMeasuredHeight());
                m = combineMeasuredStates(m, localView.getMeasuredState());
            }
        }
        int i1 = k + (this.mPaddingLeft + this.mPaddingRight);
        int i2 = Math.max(j + (this.mPaddingTop + this.mPaddingBottom), getSuggestedMinimumHeight());
        setMeasuredDimension(resolveSizeAndState(Math.max(i1, getSuggestedMinimumWidth()), paramInt1, m), resolveSizeAndState(i2, paramInt2, m << 16));
    }

    public void setActivity(Activity paramActivity)
    {
        this.mActivity = paramActivity;
        this.mInflater = ((LayoutInflater)paramActivity.getSystemService("layout_inflater"));
        this.mContainer = ((LinearLayout)this.mInflater.inflate(17367104, this, false));
        addView(this.mContainer);
        paramActivity.getFragmentManager().addOnBackStackChangedListener(this);
        updateCrumbs();
        setLayoutTransition(new LayoutTransition());
    }

    public void setMaxVisible(int paramInt)
    {
        if (paramInt < 1)
            throw new IllegalArgumentException("visibleCrumbs must be greater than zero");
        this.mMaxVisible = paramInt;
    }

    public void setOnBreadCrumbClickListener(OnBreadCrumbClickListener paramOnBreadCrumbClickListener)
    {
        this.mOnBreadCrumbClickListener = paramOnBreadCrumbClickListener;
    }

    public void setParentTitle(CharSequence paramCharSequence1, CharSequence paramCharSequence2, View.OnClickListener paramOnClickListener)
    {
        this.mParentEntry = createBackStackEntry(paramCharSequence1, paramCharSequence2);
        this.mParentClickListener = paramOnClickListener;
        updateCrumbs();
    }

    public void setTitle(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        this.mTopEntry = createBackStackEntry(paramCharSequence1, paramCharSequence2);
        updateCrumbs();
    }

    void updateCrumbs()
    {
        FragmentManager localFragmentManager = this.mActivity.getFragmentManager();
        int i = localFragmentManager.getBackStackEntryCount();
        int j = getPreEntryCount();
        int k = this.mContainer.getChildCount();
        for (int m = 0; m < i + j; m++)
        {
            FragmentManager.BackStackEntry localBackStackEntry;
            if (m < j)
                localBackStackEntry = getPreEntry(m);
            while ((m < k) && (this.mContainer.getChildAt(m).getTag() != localBackStackEntry))
            {
                int i5 = m;
                while (true)
                    if (i5 < k)
                    {
                        this.mContainer.removeViewAt(m);
                        i5++;
                        continue;
                        localBackStackEntry = localFragmentManager.getBackStackEntryAt(m - j);
                        break;
                    }
                k = m;
            }
            if (m >= k)
            {
                View localView4 = this.mInflater.inflate(17367103, this, false);
                TextView localTextView = (TextView)localView4.findViewById(16908310);
                localTextView.setText(localBackStackEntry.getBreadCrumbTitle());
                localTextView.setTag(localBackStackEntry);
                if (m == 0)
                    localView4.findViewById(16908850).setVisibility(8);
                this.mContainer.addView(localView4);
                localTextView.setOnClickListener(this.mOnClickListener);
            }
        }
        int n = i + j;
        for (int i1 = this.mContainer.getChildCount(); i1 > n; i1--)
            this.mContainer.removeViewAt(i1 - 1);
        int i2 = 0;
        if (i2 < i1)
        {
            View localView1 = this.mContainer.getChildAt(i2);
            View localView2 = localView1.findViewById(16908310);
            boolean bool;
            label291: int i3;
            label321: View localView3;
            if (i2 < i1 - 1)
            {
                bool = true;
                localView2.setEnabled(bool);
                if (this.mMaxVisible > 0)
                {
                    if (i2 >= i1 - this.mMaxVisible)
                        break label376;
                    i3 = 8;
                    localView1.setVisibility(i3);
                    localView3 = localView1.findViewById(16908850);
                    if ((i2 <= i1 - this.mMaxVisible) || (i2 == 0))
                        break label382;
                }
            }
            label376: label382: for (int i4 = 0; ; i4 = 8)
            {
                localView3.setVisibility(i4);
                i2++;
                break;
                bool = false;
                break label291;
                i3 = 0;
                break label321;
            }
        }
    }

    public static abstract interface OnBreadCrumbClickListener
    {
        public abstract boolean onBreadCrumbClick(FragmentManager.BackStackEntry paramBackStackEntry, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.FragmentBreadCrumbs
 * JD-Core Version:        0.6.2
 */