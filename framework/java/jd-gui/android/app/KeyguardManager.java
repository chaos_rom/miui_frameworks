package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.view.IOnKeyguardExitResult.Stub;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;

public class KeyguardManager
{
    private IWindowManager mWM = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));

    @Deprecated
    public void exitKeyguardSecurely(final OnKeyguardExitResult paramOnKeyguardExitResult)
    {
        try
        {
            this.mWM.exitKeyguardSecurely(new IOnKeyguardExitResult.Stub()
            {
                public void onKeyguardExitResult(boolean paramAnonymousBoolean)
                    throws RemoteException
                {
                    paramOnKeyguardExitResult.onKeyguardExitResult(paramAnonymousBoolean);
                }
            });
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    public boolean inKeyguardRestrictedInputMode()
    {
        try
        {
            boolean bool2 = this.mWM.inKeyguardRestrictedInputMode();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isKeyguardLocked()
    {
        try
        {
            boolean bool2 = this.mWM.isKeyguardLocked();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isKeyguardSecure()
    {
        try
        {
            boolean bool2 = this.mWM.isKeyguardSecure();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    @Deprecated
    public KeyguardLock newKeyguardLock(String paramString)
    {
        return new KeyguardLock(paramString);
    }

    public static abstract interface OnKeyguardExitResult
    {
        public abstract void onKeyguardExitResult(boolean paramBoolean);
    }

    public class KeyguardLock
    {
        private String mTag;
        private IBinder mToken = new Binder();

        KeyguardLock(String arg2)
        {
            Object localObject;
            this.mTag = localObject;
        }

        public void disableKeyguard()
        {
            try
            {
                KeyguardManager.this.mWM.disableKeyguard(this.mToken, this.mTag);
                label20: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label20;
            }
        }

        public void reenableKeyguard()
        {
            try
            {
                KeyguardManager.this.mWM.reenableKeyguard(this.mToken);
                label16: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label16;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.KeyguardManager
 * JD-Core Version:        0.6.2
 */