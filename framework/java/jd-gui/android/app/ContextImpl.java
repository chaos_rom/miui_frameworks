package android.app;

import android.accounts.AccountManager;
import android.accounts.IAccountManager.Stub;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.IContentProvider;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.ISerialManager.Stub;
import android.hardware.SerialManager;
import android.hardware.SystemSensorManager;
import android.hardware.input.InputManager;
import android.hardware.usb.IUsbManager.Stub;
import android.hardware.usb.UsbManager;
import android.location.CountryDetector;
import android.location.ICountryDetector.Stub;
import android.location.ILocationManager.Stub;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaRouter;
import android.net.ConnectivityManager;
import android.net.IConnectivityManager.Stub;
import android.net.INetworkPolicyManager.Stub;
import android.net.IThrottleManager.Stub;
import android.net.NetworkPolicyManager;
import android.net.ThrottleManager;
import android.net.Uri;
import android.net.nsd.INsdManager;
import android.net.nsd.INsdManager.Stub;
import android.net.nsd.NsdManager;
import android.net.wifi.IWifiManager.Stub;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.IWifiP2pManager.Stub;
import android.net.wifi.p2p.WifiP2pManager;
import android.nfc.NfcManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.DropBoxManager;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.IPowerManager.Stub;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemVibrator;
import android.os.UserId;
import android.os.storage.StorageManager;
import android.telephony.TelephonyManager;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.WindowManagerImpl;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import android.view.textservice.TextServicesManager;
import com.android.internal.os.IDropBoxManagerService;
import com.android.internal.os.IDropBoxManagerService.Stub;
import com.android.internal.policy.PolicyManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

class ContextImpl extends Context
{
    private static final boolean DEBUG = false;
    private static final String[] EMPTY_FILE_LIST;
    private static final HashMap<String, ServiceFetcher> SYSTEM_SERVICE_MAP;
    private static final String TAG = "ApplicationContext";
    private static ServiceFetcher WALLPAPER_FETCHER;
    private static int sNextPerContextServiceCacheIndex;
    private static final HashMap<String, SharedPreferencesImpl> sSharedPrefs = new HashMap();
    private IBinder mActivityToken = null;
    private String mBasePackageName;
    private File mCacheDir;
    private ApplicationContentResolver mContentResolver;
    private File mDatabasesDir;
    private File mExternalCacheDir;
    private File mExternalFilesDir;
    private File mFilesDir;
    ActivityThread mMainThread;
    private File mObbDir;
    private Context mOuterContext;
    LoadedApk mPackageInfo;
    private PackageManager mPackageManager;
    private File mPreferencesDir;
    private Context mReceiverRestrictedContext = null;
    private Resources mResources;
    private boolean mRestricted;
    final ArrayList<Object> mServiceCache = new ArrayList();
    private final Object mSync = new Object();
    private Resources.Theme mTheme = null;
    private int mThemeResource = 0;

    static
    {
        EMPTY_FILE_LIST = new String[0];
        SYSTEM_SERVICE_MAP = new HashMap();
        sNextPerContextServiceCacheIndex = 0;
        WALLPAPER_FETCHER = new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new WallpaperManager(paramAnonymousContextImpl.getOuterContext(), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        };
        registerService("accessibility", new ServiceFetcher()
        {
            public Object getService(ContextImpl paramAnonymousContextImpl)
            {
                return AccessibilityManager.getInstance(paramAnonymousContextImpl);
            }
        });
        registerService("account", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new AccountManager(paramAnonymousContextImpl, IAccountManager.Stub.asInterface(ServiceManager.getService("account")));
            }
        });
        registerService("activity", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new ActivityManager(paramAnonymousContextImpl.getOuterContext(), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("alarm", new StaticServiceFetcher()
        {
            public Object createStaticService()
            {
                return new AlarmManager(IAlarmManager.Stub.asInterface(ServiceManager.getService("alarm")));
            }
        });
        registerService("audio", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new AudioManager(paramAnonymousContextImpl);
            }
        });
        registerService("media_router", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new MediaRouter(paramAnonymousContextImpl);
            }
        });
        registerService("clipboard", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new ClipboardManager(paramAnonymousContextImpl.getOuterContext(), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("connectivity", new StaticServiceFetcher()
        {
            public Object createStaticService()
            {
                return new ConnectivityManager(IConnectivityManager.Stub.asInterface(ServiceManager.getService("connectivity")));
            }
        });
        registerService("country_detector", new StaticServiceFetcher()
        {
            public Object createStaticService()
            {
                return new CountryDetector(ICountryDetector.Stub.asInterface(ServiceManager.getService("country_detector")));
            }
        });
        registerService("device_policy", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return DevicePolicyManager.create(paramAnonymousContextImpl, paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("download", new ServiceFetcher()
        {
            @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new MiuiDownloadManager(paramAnonymousContextImpl.getContentResolver(), paramAnonymousContextImpl.getPackageName());
            }
        });
        registerService("nfc", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new NfcManager(paramAnonymousContextImpl);
            }
        });
        registerService("dropbox", new StaticServiceFetcher()
        {
            public Object createStaticService()
            {
                return ContextImpl.createDropBoxManager();
            }
        });
        registerService("input", new StaticServiceFetcher()
        {
            public Object createStaticService()
            {
                return InputManager.getInstance();
            }
        });
        registerService("input_method", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return InputMethodManager.getInstance(paramAnonymousContextImpl);
            }
        });
        registerService("textservices", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return TextServicesManager.getInstance();
            }
        });
        registerService("keyguard", new ServiceFetcher()
        {
            public Object getService(ContextImpl paramAnonymousContextImpl)
            {
                return new KeyguardManager();
            }
        });
        registerService("layout_inflater", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return PolicyManager.makeNewLayoutInflater(paramAnonymousContextImpl.getOuterContext());
            }
        });
        registerService("location", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new LocationManager(paramAnonymousContextImpl, ILocationManager.Stub.asInterface(ServiceManager.getService("location")));
            }
        });
        registerService("netpolicy", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new NetworkPolicyManager(INetworkPolicyManager.Stub.asInterface(ServiceManager.getService("netpolicy")));
            }
        });
        registerService("notification", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                Context localContext = paramAnonymousContextImpl.getOuterContext();
                return new NotificationManager(new ContextThemeWrapper(localContext, Resources.selectSystemTheme(0, localContext.getApplicationInfo().targetSdkVersion, 16973835, 16973935, 16974126)), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("servicediscovery", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                INsdManager localINsdManager = INsdManager.Stub.asInterface(ServiceManager.getService("servicediscovery"));
                return new NsdManager(paramAnonymousContextImpl.getOuterContext(), localINsdManager);
            }
        });
        registerService("power", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new PowerManager(IPowerManager.Stub.asInterface(ServiceManager.getService("power")), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("search", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new SearchManager(paramAnonymousContextImpl.getOuterContext(), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("sensor", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new SystemSensorManager(paramAnonymousContextImpl.mMainThread.getHandler().getLooper());
            }
        });
        registerService("statusbar", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new StatusBarManager(paramAnonymousContextImpl.getOuterContext());
            }
        });
        registerService("storage", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                try
                {
                    localStorageManager = new StorageManager(paramAnonymousContextImpl.mMainThread.getHandler().getLooper());
                    return localStorageManager;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        Log.e("ApplicationContext", "Failed to create StorageManager", localRemoteException);
                        StorageManager localStorageManager = null;
                    }
                }
            }
        });
        registerService("phone", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new TelephonyManager(paramAnonymousContextImpl.getOuterContext());
            }
        });
        registerService("throttle", new StaticServiceFetcher()
        {
            public Object createStaticService()
            {
                return new ThrottleManager(IThrottleManager.Stub.asInterface(ServiceManager.getService("throttle")));
            }
        });
        registerService("uimode", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new UiModeManager();
            }
        });
        registerService("usb", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new UsbManager(paramAnonymousContextImpl, IUsbManager.Stub.asInterface(ServiceManager.getService("usb")));
            }
        });
        registerService("serial", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new SerialManager(paramAnonymousContextImpl, ISerialManager.Stub.asInterface(ServiceManager.getService("serial")));
            }
        });
        registerService("vibrator", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new SystemVibrator();
            }
        });
        registerService("wallpaper", WALLPAPER_FETCHER);
        registerService("wifi", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new WifiManager(IWifiManager.Stub.asInterface(ServiceManager.getService("wifi")), paramAnonymousContextImpl.mMainThread.getHandler());
            }
        });
        registerService("wifip2p", new ServiceFetcher()
        {
            public Object createService(ContextImpl paramAnonymousContextImpl)
            {
                return new WifiP2pManager(IWifiP2pManager.Stub.asInterface(ServiceManager.getService("wifip2p")));
            }
        });
        registerService("window", new ServiceFetcher()
        {
            public Object getService(ContextImpl paramAnonymousContextImpl)
            {
                return WindowManagerImpl.getDefault(paramAnonymousContextImpl.mPackageInfo.mCompatibilityInfo);
            }
        });
    }

    ContextImpl()
    {
        this.mOuterContext = this;
    }

    public ContextImpl(ContextImpl paramContextImpl)
    {
        this.mPackageInfo = paramContextImpl.mPackageInfo;
        this.mBasePackageName = paramContextImpl.mBasePackageName;
        this.mResources = paramContextImpl.mResources;
        this.mMainThread = paramContextImpl.mMainThread;
        this.mContentResolver = paramContextImpl.mContentResolver;
        this.mOuterContext = this;
    }

    static DropBoxManager createDropBoxManager()
    {
        IDropBoxManagerService localIDropBoxManagerService = IDropBoxManagerService.Stub.asInterface(ServiceManager.getService("dropbox"));
        if (localIDropBoxManagerService == null);
        for (DropBoxManager localDropBoxManager = null; ; localDropBoxManager = new DropBoxManager(localIDropBoxManagerService))
            return localDropBoxManager;
    }

    static ContextImpl createSystemContext(ActivityThread paramActivityThread)
    {
        ContextImpl localContextImpl = new ContextImpl();
        localContextImpl.init(Resources.getSystem(), paramActivityThread);
        return localContextImpl;
    }

    private void enforce(String paramString1, int paramInt1, boolean paramBoolean, int paramInt2, String paramString2)
    {
        if (paramInt1 != 0)
        {
            StringBuilder localStringBuilder1 = new StringBuilder();
            String str1;
            StringBuilder localStringBuilder2;
            if (paramString2 != null)
            {
                str1 = paramString2 + ": ";
                localStringBuilder2 = localStringBuilder1.append(str1);
                if (!paramBoolean)
                    break label119;
            }
            label119: for (String str2 = "Neither user " + paramInt2 + " nor current process has "; ; str2 = "User " + paramInt2 + " does not have ")
            {
                throw new SecurityException(str2 + paramString1 + ".");
                str1 = "";
                break;
            }
        }
    }

    private void enforceForUri(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, Uri paramUri, String paramString)
    {
        if (paramInt2 != 0)
        {
            StringBuilder localStringBuilder1 = new StringBuilder();
            String str1;
            StringBuilder localStringBuilder2;
            if (paramString != null)
            {
                str1 = paramString + ": ";
                localStringBuilder2 = localStringBuilder1.append(str1);
                if (!paramBoolean)
                    break label134;
            }
            label134: for (String str2 = "Neither user " + paramInt3 + " nor current process has "; ; str2 = "User " + paramInt3 + " does not have ")
            {
                throw new SecurityException(str2 + uriModeFlagToString(paramInt1) + " permission on " + paramUri + ".");
                str1 = "";
                break;
            }
        }
    }

    private File getDataDirFile()
    {
        if (this.mPackageInfo != null)
            return this.mPackageInfo.getDataDirFile();
        throw new RuntimeException("Not supported in system context");
    }

    private File getDatabasesDir()
    {
        synchronized (this.mSync)
        {
            if (this.mDatabasesDir == null)
                this.mDatabasesDir = new File(getDataDirFile(), "databases");
            if (this.mDatabasesDir.getPath().equals("databases"))
                this.mDatabasesDir = new File("/data/system");
            File localFile = this.mDatabasesDir;
            return localFile;
        }
    }

    static ContextImpl getImpl(Context paramContext)
    {
        while ((paramContext instanceof ContextWrapper))
        {
            Context localContext = ((ContextWrapper)paramContext).getBaseContext();
            if (localContext == null)
                break;
            paramContext = localContext;
        }
        return (ContextImpl)paramContext;
    }

    private File getPreferencesDir()
    {
        synchronized (this.mSync)
        {
            if (this.mPreferencesDir == null)
                this.mPreferencesDir = new File(getDataDirFile(), "shared_prefs");
            File localFile = this.mPreferencesDir;
            return localFile;
        }
    }

    private WallpaperManager getWallpaperManager()
    {
        return (WallpaperManager)WALLPAPER_FETCHER.getService(this);
    }

    private File makeFilename(File paramFile, String paramString)
    {
        if (paramString.indexOf(File.separatorChar) < 0)
            return new File(paramFile, paramString);
        throw new IllegalArgumentException("File " + paramString + " contains a path separator");
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private Intent registerReceiverInternal(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler, Context paramContext)
    {
        IIntentReceiver localIIntentReceiver = null;
        if (paramBroadcastReceiver != null)
        {
            if ((this.mPackageInfo == null) || (paramContext == null))
                break label100;
            if (paramHandler == null)
                paramHandler = this.mMainThread.getHandler();
            LoadedApk localLoadedApk = this.mPackageInfo;
            Instrumentation localInstrumentation = this.mMainThread.getInstrumentation();
            localIIntentReceiver = localLoadedApk.getReceiverDispatcher(paramBroadcastReceiver, paramContext, paramHandler, localInstrumentation, true);
        }
        while (true)
        {
            Injector.checkPriority(this, paramIntentFilter);
            try
            {
                Intent localIntent2 = ActivityManagerNative.getDefault().registerReceiver(this.mMainThread.getApplicationThread(), this.mBasePackageName, localIIntentReceiver, paramIntentFilter, paramString);
                localIntent1 = localIntent2;
                return localIntent1;
                label100: if (paramHandler == null)
                    paramHandler = this.mMainThread.getHandler();
                localIIntentReceiver = new LoadedApk.ReceiverDispatcher(paramBroadcastReceiver, paramContext, paramHandler, null, true).getIIntentReceiver();
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Intent localIntent1 = null;
            }
        }
    }

    private static void registerService(String paramString, ServiceFetcher paramServiceFetcher)
    {
        if (!(paramServiceFetcher instanceof StaticServiceFetcher))
        {
            int i = sNextPerContextServiceCacheIndex;
            sNextPerContextServiceCacheIndex = i + 1;
            paramServiceFetcher.mContextCacheIndex = i;
        }
        SYSTEM_SERVICE_MAP.put(paramString, paramServiceFetcher);
    }

    static void setFilePermissionsFromMode(String paramString, int paramInt1, int paramInt2)
    {
        int i = paramInt2 | 0x1B0;
        if ((paramInt1 & 0x1) != 0)
            i |= 4;
        if ((paramInt1 & 0x2) != 0)
            i |= 2;
        FileUtils.setPermissions(paramString, i, -1, -1);
    }

    private String uriModeFlagToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown permission mode flags: " + paramInt);
        case 3:
            str = "read and write";
        case 1:
        case 2:
        }
        while (true)
        {
            return str;
            str = "read";
            continue;
            str = "write";
        }
    }

    private File validateFilePath(String paramString, boolean paramBoolean)
    {
        File localFile1;
        if (paramString.charAt(0) == File.separatorChar)
            localFile1 = new File(paramString.substring(0, paramString.lastIndexOf(File.separatorChar)));
        for (File localFile2 = new File(localFile1, paramString.substring(paramString.lastIndexOf(File.separatorChar))); ; localFile2 = makeFilename(localFile1, paramString))
        {
            if ((paramBoolean) && (!localFile1.isDirectory()) && (localFile1.mkdir()))
                FileUtils.setPermissions(localFile1.getPath(), 505, -1, -1);
            return localFile2;
            localFile1 = getDatabasesDir();
        }
    }

    public boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt)
    {
        return bindService(paramIntent, paramServiceConnection, paramInt, UserId.getUserId(Process.myUid()));
    }

    public boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt1, int paramInt2)
    {
        int i;
        boolean bool;
        if (this.mPackageInfo != null)
        {
            IServiceConnection localIServiceConnection = this.mPackageInfo.getServiceDispatcher(paramServiceConnection, getOuterContext(), this.mMainThread.getHandler(), paramInt1);
            try
            {
                if ((getActivityToken() == null) && ((paramInt1 & 0x1) == 0) && (this.mPackageInfo != null) && (this.mPackageInfo.getApplicationInfo().targetSdkVersion < 14))
                    paramInt1 |= 32;
                paramIntent.setAllowFds(false);
                i = ActivityManagerNative.getDefault().bindService(this.mMainThread.getApplicationThread(), getActivityToken(), paramIntent, paramIntent.resolveTypeIfNeeded(getContentResolver()), localIServiceConnection, paramInt1, paramInt2);
                if (i < 0)
                    throw new SecurityException("Not allowed to bind to service " + paramIntent);
            }
            catch (RemoteException localRemoteException)
            {
                bool = false;
            }
        }
        while (true)
        {
            return bool;
            throw new RuntimeException("Not supported in system context");
            if (i != 0)
                bool = true;
            else
                bool = false;
        }
    }

    public int checkCallingOrSelfPermission(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("permission is null");
        return checkPermission(paramString, Binder.getCallingPid(), Binder.getCallingUid());
    }

    public int checkCallingOrSelfUriPermission(Uri paramUri, int paramInt)
    {
        return checkUriPermission(paramUri, Binder.getCallingPid(), Binder.getCallingUid(), paramInt);
    }

    public int checkCallingPermission(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("permission is null");
        int i = Binder.getCallingPid();
        if (i != Process.myPid());
        for (int j = checkPermission(paramString, i, Binder.getCallingUid()); ; j = -1)
            return j;
    }

    public int checkCallingUriPermission(Uri paramUri, int paramInt)
    {
        int i = Binder.getCallingPid();
        if (i != Process.myPid());
        for (int j = checkUriPermission(paramUri, i, Binder.getCallingUid(), paramInt); ; j = -1)
            return j;
    }

    public int checkPermission(String paramString, int paramInt1, int paramInt2)
    {
        if (paramString == null)
            throw new IllegalArgumentException("permission is null");
        try
        {
            int j = ActivityManagerNative.getDefault().checkPermission(paramString, paramInt1, paramInt2);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            int j = ActivityManagerNative.getDefault().checkUriPermission(paramUri, paramInt1, paramInt2, paramInt3);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public int checkUriPermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        if (((paramInt3 & 0x1) != 0) && ((paramString1 == null) || (checkPermission(paramString1, paramInt1, paramInt2) == 0)));
        while (true)
        {
            return i;
            if (((paramInt3 & 0x2) == 0) || ((paramString2 != null) && (checkPermission(paramString2, paramInt1, paramInt2) != 0)))
                if (paramUri != null)
                    i = checkUriPermission(paramUri, paramInt1, paramInt2, paramInt3);
                else
                    i = -1;
        }
    }

    public void clearWallpaper()
        throws IOException
    {
        getWallpaperManager().clear();
    }

    public Context createPackageContext(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        ContextImpl localContextImpl2;
        if ((paramString.equals("system")) || (paramString.equals("android")))
        {
            ContextImpl localContextImpl1 = new ContextImpl(this.mMainThread.getSystemContext());
            localContextImpl1.mBasePackageName = this.mBasePackageName;
            localContextImpl2 = localContextImpl1;
            return localContextImpl2;
        }
        LoadedApk localLoadedApk = this.mMainThread.getPackageInfo(paramString, this.mResources.getCompatibilityInfo(), paramInt);
        if (localLoadedApk != null)
        {
            localContextImpl2 = new ContextImpl();
            if ((paramInt & 0x4) != 4)
                break label160;
        }
        label160: for (boolean bool = true; ; bool = false)
        {
            localContextImpl2.mRestricted = bool;
            localContextImpl2.init(localLoadedApk, null, this.mMainThread, this.mResources, this.mBasePackageName);
            if (localContextImpl2.mResources != null)
                break;
            throw new PackageManager.NameNotFoundException("Application package " + paramString + " not found");
        }
    }

    public String[] databaseList()
    {
        String[] arrayOfString = getDatabasesDir().list();
        if (arrayOfString != null);
        while (true)
        {
            return arrayOfString;
            arrayOfString = EMPTY_FILE_LIST;
        }
    }

    public boolean deleteDatabase(String paramString)
    {
        boolean bool1 = false;
        try
        {
            boolean bool2 = SQLiteDatabase.deleteDatabase(validateFilePath(paramString, false));
            bool1 = bool2;
            label16: return bool1;
        }
        catch (Exception localException)
        {
            break label16;
        }
    }

    public boolean deleteFile(String paramString)
    {
        return makeFilename(getFilesDir(), paramString).delete();
    }

    public void enforceCallingOrSelfPermission(String paramString1, String paramString2)
    {
        enforce(paramString1, checkCallingOrSelfPermission(paramString1), true, Binder.getCallingUid(), paramString2);
    }

    public void enforceCallingOrSelfUriPermission(Uri paramUri, int paramInt, String paramString)
    {
        enforceForUri(paramInt, checkCallingOrSelfUriPermission(paramUri, paramInt), true, Binder.getCallingUid(), paramUri, paramString);
    }

    public void enforceCallingPermission(String paramString1, String paramString2)
    {
        enforce(paramString1, checkCallingPermission(paramString1), false, Binder.getCallingUid(), paramString2);
    }

    public void enforceCallingUriPermission(Uri paramUri, int paramInt, String paramString)
    {
        enforceForUri(paramInt, checkCallingUriPermission(paramUri, paramInt), false, Binder.getCallingUid(), paramUri, paramString);
    }

    public void enforcePermission(String paramString1, int paramInt1, int paramInt2, String paramString2)
    {
        enforce(paramString1, checkPermission(paramString1, paramInt1, paramInt2), false, paramInt2, paramString2);
    }

    public void enforceUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, String paramString)
    {
        enforceForUri(paramInt3, checkUriPermission(paramUri, paramInt1, paramInt2, paramInt3), false, paramInt2, paramUri, paramString);
    }

    public void enforceUriPermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
    {
        enforceForUri(paramInt3, checkUriPermission(paramUri, paramString1, paramString2, paramInt1, paramInt2, paramInt3), false, paramInt2, paramUri, paramString3);
    }

    public String[] fileList()
    {
        String[] arrayOfString = getFilesDir().list();
        if (arrayOfString != null);
        while (true)
        {
            return arrayOfString;
            arrayOfString = EMPTY_FILE_LIST;
        }
    }

    final IBinder getActivityToken()
    {
        return this.mActivityToken;
    }

    public Context getApplicationContext()
    {
        if (this.mPackageInfo != null);
        for (Application localApplication = this.mPackageInfo.getApplication(); ; localApplication = this.mMainThread.getApplication())
            return localApplication;
    }

    public ApplicationInfo getApplicationInfo()
    {
        if (this.mPackageInfo != null)
            return this.mPackageInfo.getApplicationInfo();
        throw new RuntimeException("Not supported in system context");
    }

    public AssetManager getAssets()
    {
        return this.mResources.getAssets();
    }

    public File getCacheDir()
    {
        File localFile;
        synchronized (this.mSync)
        {
            if (this.mCacheDir == null)
                this.mCacheDir = new File(getDataDirFile(), "cache");
            if (!this.mCacheDir.exists())
            {
                if (!this.mCacheDir.mkdirs())
                {
                    Log.w("ApplicationContext", "Unable to create cache directory");
                    localFile = null;
                }
                else
                {
                    FileUtils.setPermissions(this.mCacheDir.getPath(), 505, -1, -1);
                }
            }
            else
                localFile = this.mCacheDir;
        }
        return localFile;
    }

    public ClassLoader getClassLoader()
    {
        if (this.mPackageInfo != null);
        for (ClassLoader localClassLoader = this.mPackageInfo.getClassLoader(); ; localClassLoader = ClassLoader.getSystemClassLoader())
            return localClassLoader;
    }

    public ContentResolver getContentResolver()
    {
        return this.mContentResolver;
    }

    public File getDatabasePath(String paramString)
    {
        return validateFilePath(paramString, false);
    }

    public File getDir(String paramString, int paramInt)
    {
        String str = "app_" + paramString;
        File localFile = makeFilename(getDataDirFile(), str);
        if (!localFile.exists())
        {
            localFile.mkdir();
            setFilePermissionsFromMode(localFile.getPath(), paramInt, 505);
        }
        return localFile;
    }

    public File getExternalCacheDir()
    {
        synchronized (this.mSync)
        {
            if (this.mExternalCacheDir == null)
                this.mExternalCacheDir = Environment.getExternalStorageAppCacheDirectory(getPackageName());
            boolean bool = this.mExternalCacheDir.exists();
            if (bool);
        }
        label54: File localFile;
        try
        {
            new File(Environment.getExternalStorageAndroidDataDir(), ".nomedia").createNewFile();
            if (!this.mExternalCacheDir.mkdirs())
            {
                Log.w("ApplicationContext", "Unable to create external cache directory");
                localFile = null;
            }
            else
            {
                localFile = this.mExternalCacheDir;
                break label102;
                localObject2 = finally;
                throw localObject2;
            }
        }
        catch (IOException localIOException)
        {
            break label54;
        }
        label102: return localFile;
    }

    public File getExternalFilesDir(String paramString)
    {
        synchronized (this.mSync)
        {
            if (this.mExternalFilesDir == null)
                this.mExternalFilesDir = Environment.getExternalStorageAppFilesDirectory(getPackageName());
            boolean bool = this.mExternalFilesDir.exists();
            if (bool);
        }
        label56: File localFile;
        try
        {
            new File(Environment.getExternalStorageAndroidDataDir(), ".nomedia").createNewFile();
            if (!this.mExternalFilesDir.mkdirs())
            {
                Log.w("ApplicationContext", "Unable to create external files directory");
                localFile = null;
            }
            else if (paramString == null)
            {
                localFile = this.mExternalFilesDir;
                break label178;
                localObject2 = finally;
                throw localObject2;
            }
            else
            {
                localFile = new File(this.mExternalFilesDir, paramString);
                if ((!localFile.exists()) && (!localFile.mkdirs()))
                {
                    Log.w("ApplicationContext", "Unable to create external media directory " + localFile);
                    localFile = null;
                }
            }
        }
        catch (IOException localIOException)
        {
            break label56;
        }
        label178: return localFile;
    }

    public File getFileStreamPath(String paramString)
    {
        return makeFilename(getFilesDir(), paramString);
    }

    public File getFilesDir()
    {
        File localFile;
        synchronized (this.mSync)
        {
            if (this.mFilesDir == null)
                this.mFilesDir = new File(getDataDirFile(), "files");
            if (!this.mFilesDir.exists())
            {
                if (!this.mFilesDir.mkdirs())
                {
                    Log.w("ApplicationContext", "Unable to create files directory " + this.mFilesDir.getPath());
                    localFile = null;
                }
                else
                {
                    FileUtils.setPermissions(this.mFilesDir.getPath(), 505, -1, -1);
                }
            }
            else
                localFile = this.mFilesDir;
        }
        return localFile;
    }

    public Looper getMainLooper()
    {
        return this.mMainThread.getLooper();
    }

    public File getObbDir()
    {
        synchronized (this.mSync)
        {
            if (this.mObbDir == null)
                this.mObbDir = Environment.getExternalStorageAppObbDirectory(getPackageName());
            File localFile = this.mObbDir;
            return localFile;
        }
    }

    final Context getOuterContext()
    {
        return this.mOuterContext;
    }

    public String getPackageCodePath()
    {
        if (this.mPackageInfo != null)
            return this.mPackageInfo.getAppDir();
        throw new RuntimeException("Not supported in system context");
    }

    public PackageManager getPackageManager()
    {
        Object localObject;
        if (this.mPackageManager != null)
            localObject = this.mPackageManager;
        while (true)
        {
            return localObject;
            IPackageManager localIPackageManager = ActivityThread.getPackageManager();
            if (localIPackageManager != null)
            {
                localObject = new ApplicationPackageManager(this, localIPackageManager);
                this.mPackageManager = ((PackageManager)localObject);
            }
            else
            {
                localObject = null;
            }
        }
    }

    public String getPackageName()
    {
        if (this.mPackageInfo != null)
            return this.mPackageInfo.getPackageName();
        throw new RuntimeException("Not supported in system context");
    }

    public String getPackageResourcePath()
    {
        if (this.mPackageInfo != null)
            return this.mPackageInfo.getResDir();
        throw new RuntimeException("Not supported in system context");
    }

    final Context getReceiverRestrictedContext()
    {
        Object localObject;
        if (this.mReceiverRestrictedContext != null)
            localObject = this.mReceiverRestrictedContext;
        while (true)
        {
            return localObject;
            localObject = new ReceiverRestrictedContext(getOuterContext());
            this.mReceiverRestrictedContext = ((Context)localObject);
        }
    }

    public Resources getResources()
    {
        return this.mResources;
    }

    public SharedPreferences getSharedPreferences(String paramString, int paramInt)
    {
        SharedPreferencesImpl localSharedPreferencesImpl3;
        synchronized (sSharedPrefs)
        {
            SharedPreferencesImpl localSharedPreferencesImpl1 = (SharedPreferencesImpl)sSharedPrefs.get(paramString);
            if (localSharedPreferencesImpl1 == null)
            {
                SharedPreferencesImpl localSharedPreferencesImpl2 = new SharedPreferencesImpl(getSharedPrefsFile(paramString), paramInt);
                sSharedPrefs.put(paramString, localSharedPreferencesImpl2);
                localSharedPreferencesImpl3 = localSharedPreferencesImpl2;
            }
            else
            {
                if (((paramInt & 0x4) != 0) || (getApplicationInfo().targetSdkVersion < 11))
                    localSharedPreferencesImpl1.startReloadIfChangedUnexpectedly();
                localSharedPreferencesImpl3 = localSharedPreferencesImpl1;
            }
        }
        return localSharedPreferencesImpl3;
    }

    public File getSharedPrefsFile(String paramString)
    {
        return makeFilename(getPreferencesDir(), paramString + ".xml");
    }

    public Object getSystemService(String paramString)
    {
        ServiceFetcher localServiceFetcher = (ServiceFetcher)SYSTEM_SERVICE_MAP.get(paramString);
        if (localServiceFetcher == null);
        for (Object localObject = null; ; localObject = localServiceFetcher.getService(this))
            return localObject;
    }

    public Resources.Theme getTheme()
    {
        if (this.mTheme == null)
        {
            this.mThemeResource = Resources.selectDefaultTheme(this.mThemeResource, getOuterContext().getApplicationInfo().targetSdkVersion);
            this.mTheme = this.mResources.newTheme();
            this.mTheme.applyStyle(this.mThemeResource, true);
        }
        return this.mTheme;
    }

    public int getThemeResId()
    {
        return this.mThemeResource;
    }

    public Drawable getWallpaper()
    {
        return getWallpaperManager().getDrawable();
    }

    public int getWallpaperDesiredMinimumHeight()
    {
        return getWallpaperManager().getDesiredMinimumHeight();
    }

    public int getWallpaperDesiredMinimumWidth()
    {
        return getWallpaperManager().getDesiredMinimumWidth();
    }

    public void grantUriPermission(String paramString, Uri paramUri, int paramInt)
    {
        try
        {
            ActivityManagerNative.getDefault().grantUriPermission(this.mMainThread.getApplicationThread(), paramString, paramUri, paramInt);
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    final void init(LoadedApk paramLoadedApk, IBinder paramIBinder, ActivityThread paramActivityThread)
    {
        init(paramLoadedApk, paramIBinder, paramActivityThread, null, null);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    final void init(LoadedApk paramLoadedApk, IBinder paramIBinder, ActivityThread paramActivityThread, Resources paramResources, String paramString)
    {
        this.mPackageInfo = paramLoadedApk;
        if (paramString != null);
        while (true)
        {
            this.mBasePackageName = paramString;
            this.mResources = this.mPackageInfo.getResources(paramActivityThread);
            if ((this.mResources != null) && (paramResources != null) && (paramResources.getCompatibilityInfo().applicationScale != this.mResources.getCompatibilityInfo().applicationScale))
                this.mResources = paramActivityThread.getTopLevelResources(this.mPackageInfo.mPackageName, this.mPackageInfo.getResDir(), paramResources.getCompatibilityInfo());
            this.mMainThread = paramActivityThread;
            this.mContentResolver = new ApplicationContentResolver(this, paramActivityThread);
            setActivityToken(paramIBinder);
            return;
            paramString = paramLoadedApk.mPackageName;
        }
    }

    final void init(Resources paramResources, ActivityThread paramActivityThread)
    {
        this.mPackageInfo = null;
        this.mBasePackageName = null;
        this.mResources = paramResources;
        this.mMainThread = paramActivityThread;
        this.mContentResolver = new ApplicationContentResolver(this, paramActivityThread);
    }

    public boolean isRestricted()
    {
        return this.mRestricted;
    }

    public FileInputStream openFileInput(String paramString)
        throws FileNotFoundException
    {
        return new FileInputStream(makeFilename(getFilesDir(), paramString));
    }

    public FileOutputStream openFileOutput(String paramString, int paramInt)
        throws FileNotFoundException
    {
        boolean bool;
        if ((0x8000 & paramInt) != 0)
            bool = true;
        while (true)
        {
            File localFile1 = makeFilename(getFilesDir(), paramString);
            try
            {
                localFileOutputStream = new FileOutputStream(localFile1, bool);
                setFilePermissionsFromMode(localFile1.getPath(), paramInt, 0);
                return localFileOutputStream;
                bool = false;
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                while (true)
                {
                    File localFile2 = localFile1.getParentFile();
                    localFile2.mkdir();
                    FileUtils.setPermissions(localFile2.getPath(), 505, -1, -1);
                    FileOutputStream localFileOutputStream = new FileOutputStream(localFile1, bool);
                    setFilePermissionsFromMode(localFile1.getPath(), paramInt, 0);
                }
            }
        }
    }

    public SQLiteDatabase openOrCreateDatabase(String paramString, int paramInt, SQLiteDatabase.CursorFactory paramCursorFactory)
    {
        return openOrCreateDatabase(paramString, paramInt, paramCursorFactory, null);
    }

    public SQLiteDatabase openOrCreateDatabase(String paramString, int paramInt, SQLiteDatabase.CursorFactory paramCursorFactory, DatabaseErrorHandler paramDatabaseErrorHandler)
    {
        File localFile = validateFilePath(paramString, true);
        int i = 268435456;
        if ((paramInt & 0x8) != 0)
            i |= 536870912;
        SQLiteDatabase localSQLiteDatabase = SQLiteDatabase.openDatabase(localFile.getPath(), paramCursorFactory, i, paramDatabaseErrorHandler);
        setFilePermissionsFromMode(localFile.getPath(), paramInt, 0);
        return localSQLiteDatabase;
    }

    public Drawable peekWallpaper()
    {
        return getWallpaperManager().peekDrawable();
    }

    final void performFinalCleanup(String paramString1, String paramString2)
    {
        this.mPackageInfo.removeContextRegistrations(getOuterContext(), paramString1, paramString2);
    }

    public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter)
    {
        return registerReceiver(paramBroadcastReceiver, paramIntentFilter, null, null);
    }

    public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler)
    {
        return registerReceiverInternal(paramBroadcastReceiver, paramIntentFilter, paramString, paramHandler, getOuterContext());
    }

    public void removeStickyBroadcast(Intent paramIntent)
    {
        String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
        if (str != null)
        {
            Intent localIntent = new Intent(paramIntent);
            localIntent.setDataAndType(localIntent.getData(), str);
            paramIntent = localIntent;
        }
        try
        {
            paramIntent.setAllowFds(false);
            ActivityManagerNative.getDefault().unbroadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, Binder.getOrigCallingUser());
            label58: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label58;
        }
    }

    public void revokeUriPermission(Uri paramUri, int paramInt)
    {
        try
        {
            ActivityManagerNative.getDefault().revokeUriPermission(this.mMainThread.getApplicationThread(), paramUri, paramInt);
            label17: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label17;
        }
    }

    final void scheduleFinalCleanup(String paramString1, String paramString2)
    {
        this.mMainThread.scheduleContextCleanup(this, paramString1, paramString2);
    }

    public void sendBroadcast(Intent paramIntent)
    {
        String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, null, -1, null, null, null, false, false, Binder.getOrigCallingUser());
            label43: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label43;
        }
    }

    public void sendBroadcast(Intent paramIntent, int paramInt)
    {
        String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, null, -1, null, null, null, false, false, paramInt);
            label41: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label41;
        }
    }

    public void sendBroadcast(Intent paramIntent, String paramString)
    {
        String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, null, -1, null, null, paramString, false, false, Binder.getOrigCallingUser());
            label43: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label43;
        }
    }

    public void sendOrderedBroadcast(Intent paramIntent, String paramString)
    {
        String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, null, -1, null, null, paramString, true, false, Binder.getOrigCallingUser());
            label43: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label43;
        }
    }

    public void sendOrderedBroadcast(Intent paramIntent, String paramString1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString2, Bundle paramBundle)
    {
        IIntentReceiver localIIntentReceiver = null;
        if (paramBroadcastReceiver != null)
        {
            if (this.mPackageInfo == null)
                break label113;
            if (paramHandler == null)
                paramHandler = this.mMainThread.getHandler();
            LoadedApk localLoadedApk = this.mPackageInfo;
            Context localContext = getOuterContext();
            Instrumentation localInstrumentation = this.mMainThread.getInstrumentation();
            localIIntentReceiver = localLoadedApk.getReceiverDispatcher(paramBroadcastReceiver, localContext, paramHandler, localInstrumentation, false);
        }
        while (true)
        {
            String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
            try
            {
                paramIntent.setAllowFds(false);
                ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, localIIntentReceiver, paramInt, paramString2, paramBundle, paramString1, true, false, Binder.getOrigCallingUser());
                label112: return;
                label113: if (paramHandler == null)
                    paramHandler = this.mMainThread.getHandler();
                localIIntentReceiver = new LoadedApk.ReceiverDispatcher(paramBroadcastReceiver, getOuterContext(), paramHandler, null, false).getIIntentReceiver();
            }
            catch (RemoteException localRemoteException)
            {
                break label112;
            }
        }
    }

    public void sendStickyBroadcast(Intent paramIntent)
    {
        String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
        try
        {
            paramIntent.setAllowFds(false);
            ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, null, -1, null, null, null, false, true, Binder.getOrigCallingUser());
            label43: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label43;
        }
    }

    public void sendStickyOrderedBroadcast(Intent paramIntent, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString, Bundle paramBundle)
    {
        IIntentReceiver localIIntentReceiver = null;
        if (paramBroadcastReceiver != null)
        {
            if (this.mPackageInfo == null)
                break label110;
            if (paramHandler == null)
                paramHandler = this.mMainThread.getHandler();
            LoadedApk localLoadedApk = this.mPackageInfo;
            Context localContext = getOuterContext();
            Instrumentation localInstrumentation = this.mMainThread.getInstrumentation();
            localIIntentReceiver = localLoadedApk.getReceiverDispatcher(paramBroadcastReceiver, localContext, paramHandler, localInstrumentation, false);
        }
        while (true)
        {
            String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
            try
            {
                paramIntent.setAllowFds(false);
                ActivityManagerNative.getDefault().broadcastIntent(this.mMainThread.getApplicationThread(), paramIntent, str, localIIntentReceiver, paramInt, paramString, paramBundle, null, true, true, Binder.getOrigCallingUser());
                label109: return;
                label110: if (paramHandler == null)
                    paramHandler = this.mMainThread.getHandler();
                localIIntentReceiver = new LoadedApk.ReceiverDispatcher(paramBroadcastReceiver, getOuterContext(), paramHandler, null, false).getIIntentReceiver();
            }
            catch (RemoteException localRemoteException)
            {
                break label109;
            }
        }
    }

    final void setActivityToken(IBinder paramIBinder)
    {
        this.mActivityToken = paramIBinder;
    }

    final void setOuterContext(Context paramContext)
    {
        this.mOuterContext = paramContext;
    }

    public void setTheme(int paramInt)
    {
        this.mThemeResource = paramInt;
    }

    public void setWallpaper(Bitmap paramBitmap)
        throws IOException
    {
        getWallpaperManager().setBitmap(paramBitmap);
    }

    public void setWallpaper(InputStream paramInputStream)
        throws IOException
    {
        getWallpaperManager().setStream(paramInputStream);
    }

    public void startActivities(Intent[] paramArrayOfIntent)
    {
        startActivities(paramArrayOfIntent, null);
    }

    public void startActivities(Intent[] paramArrayOfIntent, Bundle paramBundle)
    {
        if ((0x10000000 & paramArrayOfIntent[0].getFlags()) == 0)
            throw new AndroidRuntimeException("Calling startActivities() from outside of an Activity    context requires the FLAG_ACTIVITY_NEW_TASK flag on first Intent. Is this really what you want?");
        this.mMainThread.getInstrumentation().execStartActivities(getOuterContext(), this.mMainThread.getApplicationThread(), null, (Activity)null, paramArrayOfIntent, paramBundle);
    }

    public void startActivity(Intent paramIntent)
    {
        startActivity(paramIntent, null);
    }

    public void startActivity(Intent paramIntent, Bundle paramBundle)
    {
        if ((0x10000000 & paramIntent.getFlags()) == 0)
            throw new AndroidRuntimeException("Calling startActivity() from outside of an Activity    context requires the FLAG_ACTIVITY_NEW_TASK flag. Is this really what you want?");
        this.mMainThread.getInstrumentation().execStartActivity(getOuterContext(), this.mMainThread.getApplicationThread(), null, (Activity)null, paramIntent, -1, paramBundle);
    }

    public boolean startInstrumentation(ComponentName paramComponentName, String paramString, Bundle paramBundle)
    {
        if (paramBundle != null);
        try
        {
            paramBundle.setAllowFds(false);
            boolean bool2 = ActivityManagerNative.getDefault().startInstrumentation(paramComponentName, paramString, 0, paramBundle, null);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3)
        throws IntentSender.SendIntentException
    {
        startIntentSender(paramIntentSender, paramIntent, paramInt1, paramInt2, paramInt3, null);
    }

    public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
        throws IntentSender.SendIntentException
    {
        String str = null;
        if (paramIntent != null);
        try
        {
            paramIntent.setAllowFds(false);
            str = paramIntent.resolveTypeIfNeeded(getContentResolver());
            int i = ActivityManagerNative.getDefault().startActivityIntentSender(this.mMainThread.getApplicationThread(), paramIntentSender, paramIntent, str, null, null, 0, paramInt1, paramInt2, paramBundle);
            if (i == -6)
                throw new IntentSender.SendIntentException();
            Instrumentation.checkStartActivityResult(i, null);
        }
        catch (RemoteException localRemoteException)
        {
        }
    }

    public ComponentName startService(Intent paramIntent)
    {
        ComponentName localComponentName;
        try
        {
            paramIntent.setAllowFds(false);
            localComponentName = ActivityManagerNative.getDefault().startService(this.mMainThread.getApplicationThread(), paramIntent, paramIntent.resolveTypeIfNeeded(getContentResolver()));
            if ((localComponentName != null) && (localComponentName.getPackageName().equals("!")))
                throw new SecurityException("Not allowed to start service " + paramIntent + " without permission " + localComponentName.getClassName());
        }
        catch (RemoteException localRemoteException)
        {
            localComponentName = null;
        }
        return localComponentName;
    }

    public boolean stopService(Intent paramIntent)
    {
        boolean bool = false;
        int i;
        try
        {
            paramIntent.setAllowFds(false);
            i = ActivityManagerNative.getDefault().stopService(this.mMainThread.getApplicationThread(), paramIntent, paramIntent.resolveTypeIfNeeded(getContentResolver()));
            if (i < 0)
                throw new SecurityException("Not allowed to stop service " + paramIntent);
        }
        catch (RemoteException localRemoteException)
        {
        }
        while (true)
        {
            return bool;
            if (i != 0)
                bool = true;
        }
    }

    public void unbindService(ServiceConnection paramServiceConnection)
    {
        IServiceConnection localIServiceConnection;
        if (this.mPackageInfo != null)
            localIServiceConnection = this.mPackageInfo.forgetServiceDispatcher(getOuterContext(), paramServiceConnection);
        try
        {
            ActivityManagerNative.getDefault().unbindService(localIServiceConnection);
            label30: return;
            throw new RuntimeException("Not supported in system context");
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    public void unregisterReceiver(BroadcastReceiver paramBroadcastReceiver)
    {
        IIntentReceiver localIIntentReceiver;
        if (this.mPackageInfo != null)
            localIIntentReceiver = this.mPackageInfo.forgetReceiverDispatcher(getOuterContext(), paramBroadcastReceiver);
        try
        {
            ActivityManagerNative.getDefault().unregisterReceiver(localIIntentReceiver);
            label29: return;
            throw new RuntimeException("Not supported in system context");
        }
        catch (RemoteException localRemoteException)
        {
            break label29;
        }
    }

    private static final class ApplicationContentResolver extends ContentResolver
    {
        private final ActivityThread mMainThread;

        public ApplicationContentResolver(Context paramContext, ActivityThread paramActivityThread)
        {
            super();
            this.mMainThread = paramActivityThread;
        }

        protected IContentProvider acquireExistingProvider(Context paramContext, String paramString)
        {
            return this.mMainThread.acquireExistingProvider(paramContext, paramString, true);
        }

        protected IContentProvider acquireProvider(Context paramContext, String paramString)
        {
            return this.mMainThread.acquireProvider(paramContext, paramString, true);
        }

        protected IContentProvider acquireUnstableProvider(Context paramContext, String paramString)
        {
            return this.mMainThread.acquireProvider(paramContext, paramString, false);
        }

        public boolean releaseProvider(IContentProvider paramIContentProvider)
        {
            return this.mMainThread.releaseProvider(paramIContentProvider, true);
        }

        public boolean releaseUnstableProvider(IContentProvider paramIContentProvider)
        {
            return this.mMainThread.releaseProvider(paramIContentProvider, false);
        }

        public void unstableProviderDied(IContentProvider paramIContentProvider)
        {
            this.mMainThread.handleUnstableProviderDied(paramIContentProvider.asBinder(), true);
        }
    }

    static abstract class StaticServiceFetcher extends ContextImpl.ServiceFetcher
    {
        private Object mCachedInstance;

        public abstract Object createStaticService();

        public final Object getService(ContextImpl paramContextImpl)
        {
            Object localObject2;
            try
            {
                localObject2 = this.mCachedInstance;
                if (localObject2 != null)
                    break label36;
                localObject2 = createStaticService();
                this.mCachedInstance = localObject2;
            }
            finally
            {
                localObject1 = finally;
                throw localObject1;
            }
            label36: return localObject2;
        }
    }

    static class ServiceFetcher
    {
        int mContextCacheIndex = -1;

        public Object createService(ContextImpl paramContextImpl)
        {
            throw new RuntimeException("Not implemented");
        }

        public Object getService(ContextImpl paramContextImpl)
        {
            Object localObject4;
            synchronized (paramContextImpl.mServiceCache)
            {
                if (???.size() == 0)
                    for (int i = 0; i < ContextImpl.sNextPerContextServiceCacheIndex; i++)
                        ???.add(null);
                Object localObject2 = ???.get(this.mContextCacheIndex);
                if (localObject2 != null)
                {
                    localObject4 = localObject2;
                }
                else
                {
                    Object localObject3 = createService(paramContextImpl);
                    ???.set(this.mContextCacheIndex, localObject3);
                    localObject4 = localObject3;
                }
            }
            return localObject4;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void checkPriority(ContextImpl paramContextImpl, IntentFilter paramIntentFilter)
        {
            if (paramContextImpl.mPackageInfo != null)
            {
                ApplicationInfo localApplicationInfo = paramContextImpl.mPackageInfo.getApplicationInfo();
                if ((localApplicationInfo != null) && ((0x1 & localApplicationInfo.flags) == 0))
                {
                    if (paramIntentFilter.getPriority() < 1000)
                        break label46;
                    paramIntentFilter.setPriority(999);
                }
            }
            while (true)
            {
                return;
                label46: if (paramIntentFilter.getPriority() <= -1000)
                    paramIntentFilter.setPriority(-999);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ContextImpl
 * JD-Core Version:        0.6.2
 */