package android.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.ContainerEncryptionParams;
import android.content.pm.FeatureInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageInstallObserver;
import android.content.pm.IPackageManager;
import android.content.pm.IPackageMoveObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.InstrumentationInfo;
import android.content.pm.ManifestDigest;
import android.content.pm.PackageInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ParceledListSlice;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.UserInfo;
import android.content.pm.VerifierDeviceIdentity;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.net.Uri;
import android.os.Process;
import android.os.RemoteException;
import android.os.UserId;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class ApplicationPackageManager extends PackageManager
{
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_ICONS = false;
    private static final String TAG = "ApplicationPackageManager";
    private static HashMap<ResourceName, WeakReference<Drawable.ConstantState>> sIconCache = new HashMap();
    private static HashMap<ResourceName, WeakReference<CharSequence>> sStringCache = new HashMap();
    private static final Object sSync = new Object();
    int mCachedSafeMode = -1;
    private final ContextImpl mContext;
    private final IPackageManager mPM;

    ApplicationPackageManager(ContextImpl paramContextImpl, IPackageManager paramIPackageManager)
    {
        this.mContext = paramContextImpl;
        this.mPM = paramIPackageManager;
    }

    static void configurationChanged()
    {
        synchronized (sSync)
        {
            sIconCache.clear();
            sStringCache.clear();
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    static Drawable getCachedIcon(ResourceName paramResourceName)
    {
        Drawable localDrawable;
        synchronized (sSync)
        {
            WeakReference localWeakReference = (WeakReference)sIconCache.get(paramResourceName);
            if (localWeakReference != null)
            {
                Drawable.ConstantState localConstantState = (Drawable.ConstantState)localWeakReference.get();
                if (localConstantState != null)
                    localDrawable = localConstantState.newDrawable();
                else
                    sIconCache.remove(paramResourceName);
            }
            else
            {
                localDrawable = null;
            }
        }
        return localDrawable;
    }

    private CharSequence getCachedString(ResourceName paramResourceName)
    {
        CharSequence localCharSequence;
        synchronized (sSync)
        {
            WeakReference localWeakReference = (WeakReference)sStringCache.get(paramResourceName);
            if (localWeakReference != null)
            {
                localCharSequence = (CharSequence)localWeakReference.get();
                if (localCharSequence == null)
                    sStringCache.remove(paramResourceName);
            }
            else
            {
                localCharSequence = null;
            }
        }
        return localCharSequence;
    }

    static void handlePackageBroadcast(int paramInt, String[] paramArrayOfString, boolean paramBoolean)
    {
        int i = 0;
        if (paramInt == 1)
            i = 1;
        if ((paramArrayOfString != null) && (paramArrayOfString.length > 0))
        {
            int j = 0;
            int k = paramArrayOfString.length;
            int m = 0;
            while (true)
            {
                String str;
                if (m < k)
                    str = paramArrayOfString[m];
                synchronized (sSync)
                {
                    if (sIconCache.size() > 0)
                    {
                        Iterator localIterator2 = sIconCache.keySet().iterator();
                        while (localIterator2.hasNext())
                            if (((ResourceName)localIterator2.next()).packageName.equals(str))
                            {
                                localIterator2.remove();
                                j = 1;
                            }
                    }
                    if (sStringCache.size() > 0)
                    {
                        Iterator localIterator1 = sStringCache.keySet().iterator();
                        while (localIterator1.hasNext())
                            if (((ResourceName)localIterator1.next()).packageName.equals(str))
                            {
                                localIterator1.remove();
                                j = 1;
                            }
                    }
                    m++;
                }
            }
            if (paramBoolean)
            {
                if (i == 0)
                    break label218;
                Runtime.getRuntime().gc();
            }
        }
        while (true)
        {
            return;
            label218: ActivityThread.currentActivityThread().scheduleGcIdler();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    static void putCachedIcon(ResourceName paramResourceName, Drawable paramDrawable)
    {
        synchronized (sSync)
        {
            sIconCache.put(paramResourceName, new WeakReference(paramDrawable.getConstantState()));
            return;
        }
    }

    private void putCachedString(ResourceName paramResourceName, CharSequence paramCharSequence)
    {
        synchronized (sSync)
        {
            sStringCache.put(paramResourceName, new WeakReference(paramCharSequence));
            return;
        }
    }

    public void addPackageToPreferred(String paramString)
    {
        try
        {
            this.mPM.addPackageToPreferred(paramString);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public boolean addPermission(PermissionInfo paramPermissionInfo)
    {
        try
        {
            boolean bool = this.mPM.addPermission(paramPermissionInfo);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public boolean addPermissionAsync(PermissionInfo paramPermissionInfo)
    {
        try
        {
            boolean bool = this.mPM.addPermissionAsync(paramPermissionInfo);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
    {
        try
        {
            this.mPM.addPreferredActivity(paramIntentFilter, paramInt, paramArrayOfComponentName, paramComponentName);
            label14: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public String[] canonicalToCurrentPackageNames(String[] paramArrayOfString)
    {
        try
        {
            String[] arrayOfString = this.mPM.canonicalToCurrentPackageNames(paramArrayOfString);
            return arrayOfString;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public int checkPermission(String paramString1, String paramString2)
    {
        try
        {
            int i = this.mPM.checkPermission(paramString1, paramString2);
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public int checkSignatures(int paramInt1, int paramInt2)
    {
        try
        {
            int i = this.mPM.checkUidSignatures(paramInt1, paramInt2);
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public int checkSignatures(String paramString1, String paramString2)
    {
        try
        {
            int i = this.mPM.checkSignatures(paramString1, paramString2);
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver)
    {
        try
        {
            this.mPM.clearApplicationUserData(paramString, paramIPackageDataObserver, UserId.myUserId());
            label14: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public void clearPackagePreferredActivities(String paramString)
    {
        try
        {
            this.mPM.clearPackagePreferredActivities(paramString);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public UserInfo createUser(String paramString, int paramInt)
    {
        try
        {
            UserInfo localUserInfo2 = this.mPM.createUser(paramString, paramInt);
            localUserInfo1 = localUserInfo2;
            return localUserInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                UserInfo localUserInfo1 = null;
        }
    }

    public String[] currentToCanonicalPackageNames(String[] paramArrayOfString)
    {
        try
        {
            String[] arrayOfString = this.mPM.currentToCanonicalPackageNames(paramArrayOfString);
            return arrayOfString;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void deleteApplicationCacheFiles(String paramString, IPackageDataObserver paramIPackageDataObserver)
    {
        try
        {
            this.mPM.deleteApplicationCacheFiles(paramString, paramIPackageDataObserver);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void deletePackage(String paramString, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt)
    {
        try
        {
            this.mPM.deletePackage(paramString, paramIPackageDeleteObserver, paramInt);
            label12: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label12;
        }
    }

    public void freeStorage(long paramLong, IntentSender paramIntentSender)
    {
        try
        {
            this.mPM.freeStorage(paramLong, paramIntentSender);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void freeStorageAndNotify(long paramLong, IPackageDataObserver paramIPackageDataObserver)
    {
        try
        {
            this.mPM.freeStorageAndNotify(paramLong, paramIPackageDataObserver);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public Drawable getActivityIcon(ComponentName paramComponentName)
        throws PackageManager.NameNotFoundException
    {
        return getActivityInfo(paramComponentName, 0).loadIcon(this);
    }

    public Drawable getActivityIcon(Intent paramIntent)
        throws PackageManager.NameNotFoundException
    {
        if (paramIntent.getComponent() != null);
        ResolveInfo localResolveInfo;
        for (Drawable localDrawable = getActivityIcon(paramIntent.getComponent()); ; localDrawable = localResolveInfo.activityInfo.loadIcon(this))
        {
            return localDrawable;
            localResolveInfo = resolveActivity(paramIntent, 65536);
            if (localResolveInfo == null)
                break;
        }
        throw new PackageManager.NameNotFoundException(paramIntent.toUri(0));
    }

    public ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            ActivityInfo localActivityInfo = this.mPM.getActivityInfo(paramComponentName, paramInt, UserId.myUserId());
            if (localActivityInfo != null)
                return localActivityInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    }

    public Drawable getActivityLogo(ComponentName paramComponentName)
        throws PackageManager.NameNotFoundException
    {
        return getActivityInfo(paramComponentName, 0).loadLogo(this);
    }

    public Drawable getActivityLogo(Intent paramIntent)
        throws PackageManager.NameNotFoundException
    {
        if (paramIntent.getComponent() != null);
        ResolveInfo localResolveInfo;
        for (Drawable localDrawable = getActivityLogo(paramIntent.getComponent()); ; localDrawable = localResolveInfo.activityInfo.loadLogo(this))
        {
            return localDrawable;
            localResolveInfo = resolveActivity(paramIntent, 65536);
            if (localResolveInfo == null)
                break;
        }
        throw new PackageManager.NameNotFoundException(paramIntent.toUri(0));
    }

    public List<PermissionGroupInfo> getAllPermissionGroups(int paramInt)
    {
        try
        {
            List localList = this.mPM.getAllPermissionGroups(paramInt);
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public int getApplicationEnabledSetting(String paramString)
    {
        try
        {
            int j = this.mPM.getApplicationEnabledSetting(paramString, UserId.myUserId());
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public Drawable getApplicationIcon(ApplicationInfo paramApplicationInfo)
    {
        return paramApplicationInfo.loadIcon(this);
    }

    public Drawable getApplicationIcon(String paramString)
        throws PackageManager.NameNotFoundException
    {
        return getApplicationIcon(getApplicationInfo(paramString, 0));
    }

    public ApplicationInfo getApplicationInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            ApplicationInfo localApplicationInfo = this.mPM.getApplicationInfo(paramString, paramInt, UserId.myUserId());
            if (localApplicationInfo != null)
                return localApplicationInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramString);
    }

    public CharSequence getApplicationLabel(ApplicationInfo paramApplicationInfo)
    {
        return paramApplicationInfo.loadLabel(this);
    }

    public Drawable getApplicationLogo(ApplicationInfo paramApplicationInfo)
    {
        return paramApplicationInfo.loadLogo(this);
    }

    public Drawable getApplicationLogo(String paramString)
        throws PackageManager.NameNotFoundException
    {
        return getApplicationLogo(getApplicationInfo(paramString, 0));
    }

    public int getComponentEnabledSetting(ComponentName paramComponentName)
    {
        try
        {
            int j = this.mPM.getComponentEnabledSetting(paramComponentName, UserId.myUserId());
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public Drawable getDefaultActivityIcon()
    {
        return Resources.getSystem().getDrawable(17301651);
    }

    // ERROR //
    public Drawable getDrawable(String paramString, int paramInt, ApplicationInfo paramApplicationInfo)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: new 6	android/app/ApplicationPackageManager$ResourceName
        //     6: dup
        //     7: aload_1
        //     8: iload_2
        //     9: invokespecial 343	android/app/ApplicationPackageManager$ResourceName:<init>	(Ljava/lang/String;I)V
        //     12: astore 5
        //     14: aload 5
        //     16: invokestatic 345	android/app/ApplicationPackageManager:getCachedIcon	(Landroid/app/ApplicationPackageManager$ResourceName;)Landroid/graphics/drawable/Drawable;
        //     19: astore 6
        //     21: aload 6
        //     23: ifnull +10 -> 33
        //     26: aload 6
        //     28: astore 4
        //     30: aload 4
        //     32: areturn
        //     33: aload_3
        //     34: ifnonnull +14 -> 48
        //     37: aload_0
        //     38: aload_1
        //     39: iconst_0
        //     40: invokevirtual 305	android/app/ApplicationPackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //     43: astore 15
        //     45: aload 15
        //     47: astore_3
        //     48: aload_0
        //     49: aload_3
        //     50: invokevirtual 349	android/app/ApplicationPackageManager:getResourcesForApplication	(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
        //     53: iload_2
        //     54: invokevirtual 337	android/content/res/Resources:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     57: astore 13
        //     59: aload 5
        //     61: aload 13
        //     63: invokestatic 351	android/app/ApplicationPackageManager:putCachedIcon	(Landroid/app/ApplicationPackageManager$ResourceName;Landroid/graphics/drawable/Drawable;)V
        //     66: aload 13
        //     68: astore 4
        //     70: goto -40 -> 30
        //     73: astore 14
        //     75: goto -45 -> 30
        //     78: astore 11
        //     80: ldc_w 353
        //     83: new 355	java/lang/StringBuilder
        //     86: dup
        //     87: invokespecial 356	java/lang/StringBuilder:<init>	()V
        //     90: ldc_w 358
        //     93: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     96: aload_3
        //     97: getfield 365	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     100: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     103: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     106: invokestatic 371	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     109: pop
        //     110: goto -80 -> 30
        //     113: astore 9
        //     115: ldc_w 353
        //     118: new 355	java/lang/StringBuilder
        //     121: dup
        //     122: invokespecial 356	java/lang/StringBuilder:<init>	()V
        //     125: ldc_w 358
        //     128: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: aload_3
        //     132: getfield 365	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     135: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: ldc_w 373
        //     141: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     144: aload 9
        //     146: invokevirtual 376	android/content/res/Resources$NotFoundException:getMessage	()Ljava/lang/String;
        //     149: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     152: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     155: invokestatic 371	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     158: pop
        //     159: goto -129 -> 30
        //     162: astore 7
        //     164: ldc_w 353
        //     167: new 355	java/lang/StringBuilder
        //     170: dup
        //     171: invokespecial 356	java/lang/StringBuilder:<init>	()V
        //     174: ldc_w 378
        //     177: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     180: iload_2
        //     181: invokestatic 383	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     184: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     187: ldc_w 385
        //     190: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     193: aload_1
        //     194: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     197: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     200: aload 7
        //     202: invokestatic 388	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     205: pop
        //     206: goto -176 -> 30
        //
        // Exception table:
        //     from	to	target	type
        //     37	45	73	android/content/pm/PackageManager$NameNotFoundException
        //     48	66	78	android/content/pm/PackageManager$NameNotFoundException
        //     48	66	113	android/content/res/Resources$NotFoundException
        //     48	66	162	java/lang/RuntimeException
    }

    public List<ApplicationInfo> getInstalledApplications(int paramInt)
    {
        int i = UserId.getUserId(Process.myUid());
        try
        {
            ArrayList localArrayList = new ArrayList();
            ApplicationInfo localApplicationInfo = null;
            if (localApplicationInfo != null);
            for (String str = localApplicationInfo.packageName; ; str = null)
            {
                ParceledListSlice localParceledListSlice = this.mPM.getInstalledApplications(paramInt, str, i);
                localApplicationInfo = (ApplicationInfo)localParceledListSlice.populateList(localArrayList, ApplicationInfo.CREATOR);
                boolean bool = localParceledListSlice.isLastSlice();
                if (!bool)
                    break;
                return localArrayList;
            }
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<PackageInfo> getInstalledPackages(int paramInt)
    {
        try
        {
            ArrayList localArrayList = new ArrayList();
            PackageInfo localPackageInfo = null;
            if (localPackageInfo != null);
            for (String str = localPackageInfo.packageName; ; str = null)
            {
                ParceledListSlice localParceledListSlice = this.mPM.getInstalledPackages(paramInt, str);
                localPackageInfo = (PackageInfo)localParceledListSlice.populateList(localArrayList, PackageInfo.CREATOR);
                boolean bool = localParceledListSlice.isLastSlice();
                if (!bool)
                    break;
                return localArrayList;
            }
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public String getInstallerPackageName(String paramString)
    {
        try
        {
            String str2 = this.mPM.getInstallerPackageName(paramString);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String str1 = null;
        }
    }

    public InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            InstrumentationInfo localInstrumentationInfo = this.mPM.getInstrumentationInfo(paramComponentName, paramInt);
            if (localInstrumentationInfo != null)
                return localInstrumentationInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    }

    public Intent getLaunchIntentForPackage(String paramString)
    {
        Intent localIntent1 = new Intent("android.intent.action.MAIN");
        localIntent1.addCategory("android.intent.category.INFO");
        localIntent1.setPackage(paramString);
        List localList = queryIntentActivities(localIntent1, 0);
        if ((localList == null) || (localList.size() <= 0))
        {
            localIntent1.removeCategory("android.intent.category.INFO");
            localIntent1.addCategory("android.intent.category.LAUNCHER");
            localIntent1.setPackage(paramString);
            localList = queryIntentActivities(localIntent1, 0);
        }
        Intent localIntent2;
        if ((localList == null) || (localList.size() <= 0))
            localIntent2 = null;
        while (true)
        {
            return localIntent2;
            localIntent2 = new Intent(localIntent1);
            localIntent2.setFlags(268435456);
            localIntent2.setClassName(((ResolveInfo)localList.get(0)).activityInfo.packageName, ((ResolveInfo)localList.get(0)).activityInfo.name);
        }
    }

    public String getNameForUid(int paramInt)
    {
        try
        {
            String str = this.mPM.getNameForUid(paramInt);
            return str;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public int[] getPackageGids(String paramString)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            int[] arrayOfInt = this.mPM.getPackageGids(paramString);
            if (arrayOfInt != null)
            {
                int i = arrayOfInt.length;
                if (i <= 0);
            }
            else
            {
                return arrayOfInt;
            }
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramString);
    }

    public PackageInfo getPackageInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            PackageInfo localPackageInfo = this.mPM.getPackageInfo(paramString, paramInt, UserId.myUserId());
            if (localPackageInfo != null)
                return localPackageInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramString);
    }

    public void getPackageSizeInfo(String paramString, IPackageStatsObserver paramIPackageStatsObserver)
    {
        try
        {
            this.mPM.getPackageSizeInfo(paramString, paramIPackageStatsObserver);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public String[] getPackagesForUid(int paramInt)
    {
        try
        {
            String[] arrayOfString = this.mPM.getPackagesForUid(paramInt);
            return arrayOfString;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            PermissionGroupInfo localPermissionGroupInfo = this.mPM.getPermissionGroupInfo(paramString, paramInt);
            if (localPermissionGroupInfo != null)
                return localPermissionGroupInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramString);
    }

    public PermissionInfo getPermissionInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            PermissionInfo localPermissionInfo = this.mPM.getPermissionInfo(paramString, paramInt);
            if (localPermissionInfo != null)
                return localPermissionInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramString);
    }

    public int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString)
    {
        try
        {
            int j = this.mPM.getPreferredActivities(paramList, paramList1, paramString);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public List<PackageInfo> getPreferredPackages(int paramInt)
    {
        try
        {
            List localList = this.mPM.getPreferredPackages(paramInt);
            localObject = localList;
            return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Object localObject = new ArrayList();
        }
    }

    public ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            ProviderInfo localProviderInfo = this.mPM.getProviderInfo(paramComponentName, paramInt, UserId.myUserId());
            if (localProviderInfo != null)
                return localProviderInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    }

    public ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            ActivityInfo localActivityInfo = this.mPM.getReceiverInfo(paramComponentName, paramInt, UserId.myUserId());
            if (localActivityInfo != null)
                return localActivityInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    }

    public Resources getResourcesForActivity(ComponentName paramComponentName)
        throws PackageManager.NameNotFoundException
    {
        return getResourcesForApplication(getActivityInfo(paramComponentName, 0).applicationInfo);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Resources getResourcesForApplication(ApplicationInfo paramApplicationInfo)
        throws PackageManager.NameNotFoundException
    {
        Resources localResources;
        if (paramApplicationInfo.packageName.equals("system"))
        {
            localResources = this.mContext.mMainThread.getSystemContext().getResources();
            return localResources;
        }
        ActivityThread localActivityThread = this.mContext.mMainThread;
        String str1 = paramApplicationInfo.packageName;
        if (paramApplicationInfo.uid == Process.myUid());
        for (String str2 = paramApplicationInfo.sourceDir; ; str2 = paramApplicationInfo.publicSourceDir)
        {
            localResources = localActivityThread.getTopLevelResources(str1, str2, this.mContext.mPackageInfo);
            if (localResources != null)
                break;
            throw new PackageManager.NameNotFoundException("Unable to open " + paramApplicationInfo.publicSourceDir);
        }
    }

    public Resources getResourcesForApplication(String paramString)
        throws PackageManager.NameNotFoundException
    {
        return getResourcesForApplication(getApplicationInfo(paramString, 0));
    }

    public ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            ServiceInfo localServiceInfo = this.mPM.getServiceInfo(paramComponentName, paramInt, UserId.myUserId());
            if (localServiceInfo != null)
                return localServiceInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    }

    public FeatureInfo[] getSystemAvailableFeatures()
    {
        try
        {
            FeatureInfo[] arrayOfFeatureInfo = this.mPM.getSystemAvailableFeatures();
            return arrayOfFeatureInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public String[] getSystemSharedLibraryNames()
    {
        try
        {
            String[] arrayOfString = this.mPM.getSystemSharedLibraryNames();
            return arrayOfString;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    // ERROR //
    public CharSequence getText(String paramString, int paramInt, ApplicationInfo paramApplicationInfo)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: new 6	android/app/ApplicationPackageManager$ResourceName
        //     6: dup
        //     7: aload_1
        //     8: iload_2
        //     9: invokespecial 343	android/app/ApplicationPackageManager$ResourceName:<init>	(Ljava/lang/String;I)V
        //     12: astore 5
        //     14: aload_0
        //     15: aload 5
        //     17: invokespecial 580	android/app/ApplicationPackageManager:getCachedString	(Landroid/app/ApplicationPackageManager$ResourceName;)Ljava/lang/CharSequence;
        //     20: astore 6
        //     22: aload 6
        //     24: ifnull +10 -> 34
        //     27: aload 6
        //     29: astore 4
        //     31: aload 4
        //     33: areturn
        //     34: aload_3
        //     35: ifnonnull +14 -> 49
        //     38: aload_0
        //     39: aload_1
        //     40: iconst_0
        //     41: invokevirtual 305	android/app/ApplicationPackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //     44: astore 13
        //     46: aload 13
        //     48: astore_3
        //     49: aload_0
        //     50: aload_3
        //     51: invokevirtual 349	android/app/ApplicationPackageManager:getResourcesForApplication	(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
        //     54: iload_2
        //     55: invokevirtual 583	android/content/res/Resources:getText	(I)Ljava/lang/CharSequence;
        //     58: astore 11
        //     60: aload_0
        //     61: aload 5
        //     63: aload 11
        //     65: invokespecial 585	android/app/ApplicationPackageManager:putCachedString	(Landroid/app/ApplicationPackageManager$ResourceName;Ljava/lang/CharSequence;)V
        //     68: aload 11
        //     70: astore 4
        //     72: goto -41 -> 31
        //     75: astore 12
        //     77: goto -46 -> 31
        //     80: astore 9
        //     82: ldc_w 353
        //     85: new 355	java/lang/StringBuilder
        //     88: dup
        //     89: invokespecial 356	java/lang/StringBuilder:<init>	()V
        //     92: ldc_w 358
        //     95: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     98: aload_3
        //     99: getfield 365	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     102: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     108: invokestatic 371	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     111: pop
        //     112: goto -81 -> 31
        //     115: astore 7
        //     117: ldc_w 353
        //     120: new 355	java/lang/StringBuilder
        //     123: dup
        //     124: invokespecial 356	java/lang/StringBuilder:<init>	()V
        //     127: ldc_w 587
        //     130: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: iload_2
        //     134: invokestatic 383	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     137: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     140: ldc_w 385
        //     143: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     146: aload_1
        //     147: invokevirtual 362	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     150: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     153: aload 7
        //     155: invokestatic 388	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     158: pop
        //     159: goto -128 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     38	46	75	android/content/pm/PackageManager$NameNotFoundException
        //     49	68	80	android/content/pm/PackageManager$NameNotFoundException
        //     49	68	115	java/lang/RuntimeException
    }

    public int getUidForSharedUser(String paramString)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            int i = this.mPM.getUidForSharedUser(paramString);
            if (i != -1)
                return i;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException("No shared userid for user:" + paramString);
    }

    public UserInfo getUser(int paramInt)
    {
        try
        {
            UserInfo localUserInfo2 = this.mPM.getUser(paramInt);
            localUserInfo1 = localUserInfo2;
            return localUserInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                UserInfo localUserInfo1 = null;
        }
    }

    public List<UserInfo> getUsers()
    {
        try
        {
            List localList = this.mPM.getUsers();
            localObject = localList;
            return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Object localObject = new ArrayList();
                ((ArrayList)localObject).add(new UserInfo(0, "Root!", 3));
            }
        }
    }

    public VerifierDeviceIdentity getVerifierDeviceIdentity()
    {
        try
        {
            VerifierDeviceIdentity localVerifierDeviceIdentity2 = this.mPM.getVerifierDeviceIdentity();
            localVerifierDeviceIdentity1 = localVerifierDeviceIdentity2;
            return localVerifierDeviceIdentity1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                VerifierDeviceIdentity localVerifierDeviceIdentity1 = null;
        }
    }

    public XmlResourceParser getXml(String paramString, int paramInt, ApplicationInfo paramApplicationInfo)
    {
        Object localObject = null;
        if (paramApplicationInfo == null);
        try
        {
            ApplicationInfo localApplicationInfo = getApplicationInfo(paramString, 0);
            paramApplicationInfo = localApplicationInfo;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException2)
        {
            try
            {
                XmlResourceParser localXmlResourceParser = getResourcesForApplication(paramApplicationInfo).getXml(paramInt);
                localObject = localXmlResourceParser;
                while (true)
                {
                    return localObject;
                    localNameNotFoundException2 = localNameNotFoundException2;
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                    Log.w("PackageManager", "Failure retrieving xml 0x" + Integer.toHexString(paramInt) + " in package " + paramString, localRuntimeException);
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException1)
            {
                while (true)
                    Log.w("PackageManager", "Failure retrieving resources for " + paramApplicationInfo.packageName);
            }
        }
    }

    public void grantPermission(String paramString1, String paramString2)
    {
        try
        {
            this.mPM.grantPermission(paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public boolean hasSystemFeature(String paramString)
    {
        try
        {
            boolean bool = this.mPM.hasSystemFeature(paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void installPackage(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString)
    {
        try
        {
            this.mPM.installPackage(paramUri, paramIPackageInstallObserver, paramInt, paramString);
            label14: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public void installPackageWithVerification(Uri paramUri1, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString, Uri paramUri2, ManifestDigest paramManifestDigest, ContainerEncryptionParams paramContainerEncryptionParams)
    {
        try
        {
            this.mPM.installPackageWithVerification(paramUri1, paramIPackageInstallObserver, paramInt, paramString, paramUri2, paramManifestDigest, paramContainerEncryptionParams);
            label20: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    public boolean isSafeMode()
    {
        int i = 1;
        try
        {
            int k;
            if (this.mCachedSafeMode < 0)
            {
                if (this.mPM.isSafeMode())
                {
                    k = i;
                    this.mCachedSafeMode = k;
                }
            }
            else
            {
                int j = this.mCachedSafeMode;
                if (j == 0)
                    break label47;
            }
            while (true)
            {
                return i;
                k = 0;
                break;
                label47: i = 0;
            }
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void movePackage(String paramString, IPackageMoveObserver paramIPackageMoveObserver, int paramInt)
    {
        try
        {
            this.mPM.movePackage(paramString, paramIPackageMoveObserver, paramInt);
            label12: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label12;
        }
    }

    public List<ResolveInfo> queryBroadcastReceivers(Intent paramIntent, int paramInt)
    {
        try
        {
            List localList = this.mPM.queryIntentReceivers(paramIntent, paramIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), paramInt, UserId.myUserId());
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<ProviderInfo> queryContentProviders(String paramString, int paramInt1, int paramInt2)
    {
        try
        {
            List localList = this.mPM.queryContentProviders(paramString, paramInt1, paramInt2);
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<InstrumentationInfo> queryInstrumentation(String paramString, int paramInt)
    {
        try
        {
            List localList = this.mPM.queryInstrumentation(paramString, paramInt);
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<ResolveInfo> queryIntentActivities(Intent paramIntent, int paramInt)
    {
        try
        {
            List localList = this.mPM.queryIntentActivities(paramIntent, paramIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), paramInt, UserId.myUserId());
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<ResolveInfo> queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, Intent paramIntent, int paramInt)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String[] arrayOfString = null;
        if (paramArrayOfIntent != null)
        {
            int i = paramArrayOfIntent.length;
            for (int j = 0; j < i; j++)
            {
                Intent localIntent = paramArrayOfIntent[j];
                if (localIntent != null)
                {
                    String str = localIntent.resolveTypeIfNeeded(localContentResolver);
                    if (str != null)
                    {
                        if (arrayOfString == null)
                            arrayOfString = new String[i];
                        arrayOfString[j] = str;
                    }
                }
            }
        }
        try
        {
            List localList = this.mPM.queryIntentActivityOptions(paramComponentName, paramArrayOfIntent, arrayOfString, paramIntent, paramIntent.resolveTypeIfNeeded(localContentResolver), paramInt, UserId.myUserId());
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<ResolveInfo> queryIntentServices(Intent paramIntent, int paramInt)
    {
        try
        {
            List localList = this.mPM.queryIntentServices(paramIntent, paramIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), paramInt, UserId.myUserId());
            return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public List<PermissionInfo> queryPermissionsByGroup(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException
    {
        try
        {
            List localList = this.mPM.queryPermissionsByGroup(paramString, paramInt);
            if (localList != null)
                return localList;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
        throw new PackageManager.NameNotFoundException(paramString);
    }

    public void removePackageFromPreferred(String paramString)
    {
        try
        {
            this.mPM.removePackageFromPreferred(paramString);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void removePermission(String paramString)
    {
        try
        {
            this.mPM.removePermission(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public boolean removeUser(int paramInt)
    {
        try
        {
            boolean bool2 = this.mPM.removeUser(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
    {
        try
        {
            this.mPM.replacePreferredActivity(paramIntentFilter, paramInt, paramArrayOfComponentName, paramComponentName);
            label14: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public ResolveInfo resolveActivity(Intent paramIntent, int paramInt)
    {
        try
        {
            ResolveInfo localResolveInfo = this.mPM.resolveIntent(paramIntent, paramIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), paramInt, UserId.myUserId());
            return localResolveInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public ProviderInfo resolveContentProvider(String paramString, int paramInt)
    {
        try
        {
            ProviderInfo localProviderInfo = this.mPM.resolveContentProvider(paramString, paramInt, UserId.myUserId());
            return localProviderInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public ResolveInfo resolveService(Intent paramIntent, int paramInt)
    {
        try
        {
            ResolveInfo localResolveInfo = this.mPM.resolveService(paramIntent, paramIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), paramInt, UserId.myUserId());
            return localResolveInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void revokePermission(String paramString1, String paramString2)
    {
        try
        {
            this.mPM.revokePermission(paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Package manager has died", localRemoteException);
        }
    }

    public void setApplicationEnabledSetting(String paramString, int paramInt1, int paramInt2)
    {
        try
        {
            this.mPM.setApplicationEnabledSetting(paramString, paramInt1, paramInt2, UserId.myUserId());
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2)
    {
        try
        {
            this.mPM.setComponentEnabledSetting(paramComponentName, paramInt1, paramInt2, UserId.myUserId());
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void setInstallerPackageName(String paramString1, String paramString2)
    {
        try
        {
            this.mPM.setInstallerPackageName(paramString1, paramString2);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void updateUserFlags(int paramInt1, int paramInt2)
    {
    }

    public void updateUserName(int paramInt, String paramString)
    {
        try
        {
            this.mPM.updateUserName(paramInt, paramString);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void verifyPendingInstall(int paramInt1, int paramInt2)
    {
        try
        {
            this.mPM.verifyPendingInstall(paramInt1, paramInt2);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    static final class ResourceName
    {
        final int iconId;
        final String packageName;

        ResourceName(ApplicationInfo paramApplicationInfo, int paramInt)
        {
            this(paramApplicationInfo.packageName, paramInt);
        }

        ResourceName(ComponentInfo paramComponentInfo, int paramInt)
        {
            this(paramComponentInfo.applicationInfo.packageName, paramInt);
        }

        ResourceName(ResolveInfo paramResolveInfo, int paramInt)
        {
            this(paramResolveInfo.activityInfo.applicationInfo.packageName, paramInt);
        }

        ResourceName(String paramString, int paramInt)
        {
            this.packageName = paramString;
            this.iconId = paramInt;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = true;
            if (this == paramObject);
            while (true)
            {
                return bool;
                if ((paramObject == null) || (getClass() != paramObject.getClass()))
                {
                    bool = false;
                }
                else
                {
                    ResourceName localResourceName = (ResourceName)paramObject;
                    if (this.iconId != localResourceName.iconId)
                        bool = false;
                    else if (this.packageName != null)
                    {
                        if (this.packageName.equals(localResourceName.packageName));
                    }
                    else
                        while (localResourceName.packageName != null)
                        {
                            bool = false;
                            break;
                        }
                }
            }
        }

        public int hashCode()
        {
            return 31 * this.packageName.hashCode() + this.iconId;
        }

        public String toString()
        {
            return "{ResourceName " + this.packageName + " / " + this.iconId + "}";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.ApplicationPackageManager
 * JD-Core Version:        0.6.2
 */