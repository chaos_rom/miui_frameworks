package android.app;

import android.content.pm.IPackageManager;

public class AppGlobals
{
    public static Application getInitialApplication()
    {
        return ActivityThread.currentApplication();
    }

    public static String getInitialPackage()
    {
        return ActivityThread.currentPackageName();
    }

    public static int getIntCoreSetting(String paramString, int paramInt)
    {
        ActivityThread localActivityThread = ActivityThread.currentActivityThread();
        if (localActivityThread != null)
            paramInt = localActivityThread.getIntCoreSetting(paramString, paramInt);
        return paramInt;
    }

    public static IPackageManager getPackageManager()
    {
        return ActivityThread.getPackageManager();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.AppGlobals
 * JD-Core Version:        0.6.2
 */