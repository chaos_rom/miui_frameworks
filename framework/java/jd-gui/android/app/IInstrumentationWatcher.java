package android.app;

import android.content.ComponentName;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IInstrumentationWatcher extends IInterface
{
    public abstract void instrumentationFinished(ComponentName paramComponentName, int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract void instrumentationStatus(ComponentName paramComponentName, int paramInt, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInstrumentationWatcher
    {
        private static final String DESCRIPTOR = "android.app.IInstrumentationWatcher";
        static final int TRANSACTION_instrumentationFinished = 2;
        static final int TRANSACTION_instrumentationStatus = 1;

        public Stub()
        {
            attachInterface(this, "android.app.IInstrumentationWatcher");
        }

        public static IInstrumentationWatcher asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.app.IInstrumentationWatcher");
                if ((localIInterface != null) && ((localIInterface instanceof IInstrumentationWatcher)))
                    localObject = (IInstrumentationWatcher)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.app.IInstrumentationWatcher");
                }
            case 1:
                paramParcel1.enforceInterface("android.app.IInstrumentationWatcher");
                ComponentName localComponentName2;
                label90: int j;
                if (paramParcel1.readInt() != 0)
                {
                    localComponentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                    j = paramParcel1.readInt();
                    if (paramParcel1.readInt() == 0)
                        break label136;
                }
                label136: for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                {
                    instrumentationStatus(localComponentName2, j, localBundle2);
                    break;
                    localComponentName2 = null;
                    break label90;
                }
            case 2:
            }
            paramParcel1.enforceInterface("android.app.IInstrumentationWatcher");
            ComponentName localComponentName1;
            label169: int i;
            if (paramParcel1.readInt() != 0)
            {
                localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                i = paramParcel1.readInt();
                if (paramParcel1.readInt() == 0)
                    break label215;
            }
            label215: for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
            {
                instrumentationFinished(localComponentName1, i, localBundle1);
                break;
                localComponentName1 = null;
                break label169;
            }
        }

        private static class Proxy
            implements IInstrumentationWatcher
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.app.IInstrumentationWatcher";
            }

            public void instrumentationFinished(ComponentName paramComponentName, int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.app.IInstrumentationWatcher");
                        if (paramComponentName != null)
                        {
                            localParcel.writeInt(1);
                            paramComponentName.writeToParcel(localParcel, 0);
                            localParcel.writeInt(paramInt);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                this.mRemote.transact(2, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public void instrumentationStatus(ComponentName paramComponentName, int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.app.IInstrumentationWatcher");
                        if (paramComponentName != null)
                        {
                            localParcel.writeInt(1);
                            paramComponentName.writeToParcel(localParcel, 0);
                            localParcel.writeInt(paramInt);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                this.mRemote.transact(1, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IInstrumentationWatcher
 * JD-Core Version:        0.6.2
 */