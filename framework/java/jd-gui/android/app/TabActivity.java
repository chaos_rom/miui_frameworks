package android.app;

import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

@Deprecated
public class TabActivity extends ActivityGroup
{
    private String mDefaultTab = null;
    private int mDefaultTabIndex = -1;
    private TabHost mTabHost;

    private void ensureTabHost()
    {
        if (this.mTabHost == null)
            setContentView(17367216);
    }

    public TabHost getTabHost()
    {
        ensureTabHost();
        return this.mTabHost;
    }

    public TabWidget getTabWidget()
    {
        return this.mTabHost.getTabWidget();
    }

    protected void onChildTitleChanged(Activity paramActivity, CharSequence paramCharSequence)
    {
        if (getLocalActivityManager().getCurrentActivity() == paramActivity)
        {
            View localView = this.mTabHost.getCurrentTabView();
            if ((localView != null) && ((localView instanceof TextView)))
                ((TextView)localView).setText(paramCharSequence);
        }
    }

    public void onContentChanged()
    {
        super.onContentChanged();
        this.mTabHost = ((TabHost)findViewById(16908306));
        if (this.mTabHost == null)
            throw new RuntimeException("Your content must have a TabHost whose id attribute is 'android.R.id.tabhost'");
        this.mTabHost.setup(getLocalActivityManager());
    }

    protected void onPostCreate(Bundle paramBundle)
    {
        super.onPostCreate(paramBundle);
        ensureTabHost();
        if (this.mTabHost.getCurrentTab() == -1)
            this.mTabHost.setCurrentTab(0);
    }

    protected void onRestoreInstanceState(Bundle paramBundle)
    {
        super.onRestoreInstanceState(paramBundle);
        ensureTabHost();
        String str = paramBundle.getString("currentTab");
        if (str != null)
            this.mTabHost.setCurrentTabByTag(str);
        if (this.mTabHost.getCurrentTab() < 0)
        {
            if (this.mDefaultTab == null)
                break label57;
            this.mTabHost.setCurrentTabByTag(this.mDefaultTab);
        }
        while (true)
        {
            return;
            label57: if (this.mDefaultTabIndex >= 0)
                this.mTabHost.setCurrentTab(this.mDefaultTabIndex);
        }
    }

    protected void onSaveInstanceState(Bundle paramBundle)
    {
        super.onSaveInstanceState(paramBundle);
        String str = this.mTabHost.getCurrentTabTag();
        if (str != null)
            paramBundle.putString("currentTab", str);
    }

    public void setDefaultTab(int paramInt)
    {
        this.mDefaultTab = null;
        this.mDefaultTabIndex = paramInt;
    }

    public void setDefaultTab(String paramString)
    {
        this.mDefaultTab = paramString;
        this.mDefaultTabIndex = -1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.TabActivity
 * JD-Core Version:        0.6.2
 */