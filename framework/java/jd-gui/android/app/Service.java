package android.app;

import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class Service extends ContextWrapper
    implements ComponentCallbacks2
{
    public static final int START_CONTINUATION_MASK = 15;
    public static final int START_FLAG_REDELIVERY = 1;
    public static final int START_FLAG_RETRY = 2;
    public static final int START_NOT_STICKY = 2;
    public static final int START_REDELIVER_INTENT = 3;
    public static final int START_STICKY = 1;
    public static final int START_STICKY_COMPATIBILITY = 0;
    public static final int START_TASK_REMOVED_COMPLETE = 1000;
    private static final String TAG = "Service";
    private IActivityManager mActivityManager = null;
    private Application mApplication = null;
    private String mClassName = null;
    private boolean mStartCompatibility = false;
    private ActivityThread mThread = null;
    private IBinder mToken = null;

    public Service()
    {
        super(null);
    }

    public final void attach(Context paramContext, ActivityThread paramActivityThread, String paramString, IBinder paramIBinder, Application paramApplication, Object paramObject)
    {
        attachBaseContext(paramContext);
        this.mThread = paramActivityThread;
        this.mClassName = paramString;
        this.mToken = paramIBinder;
        this.mApplication = paramApplication;
        this.mActivityManager = ((IActivityManager)paramObject);
        if (getApplicationInfo().targetSdkVersion < 5);
        for (boolean bool = true; ; bool = false)
        {
            this.mStartCompatibility = bool;
            return;
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("nothing to dump");
    }

    public final Application getApplication()
    {
        return this.mApplication;
    }

    final String getClassName()
    {
        return this.mClassName;
    }

    public abstract IBinder onBind(Intent paramIntent);

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
    }

    public void onCreate()
    {
    }

    public void onDestroy()
    {
    }

    public void onLowMemory()
    {
    }

    public void onRebind(Intent paramIntent)
    {
    }

    @Deprecated
    public void onStart(Intent paramIntent, int paramInt)
    {
    }

    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        onStart(paramIntent, paramInt2);
        if (this.mStartCompatibility);
        for (int i = 0; ; i = 1)
            return i;
    }

    public void onTaskRemoved(Intent paramIntent)
    {
    }

    public void onTrimMemory(int paramInt)
    {
    }

    public boolean onUnbind(Intent paramIntent)
    {
        return false;
    }

    @Deprecated
    public final void setForeground(boolean paramBoolean)
    {
        Log.w("Service", "setForeground: ignoring old API call on " + getClass().getName());
    }

    public final void startForeground(int paramInt, Notification paramNotification)
    {
        try
        {
            this.mActivityManager.setServiceForeground(new ComponentName(this, this.mClassName), this.mToken, paramInt, paramNotification, true);
            label28: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label28;
        }
    }

    public final void stopForeground(boolean paramBoolean)
    {
        try
        {
            this.mActivityManager.setServiceForeground(new ComponentName(this, this.mClassName), this.mToken, 0, null, paramBoolean);
            label28: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label28;
        }
    }

    public final void stopSelf()
    {
        stopSelf(-1);
    }

    public final void stopSelf(int paramInt)
    {
        if (this.mActivityManager == null);
        while (true)
        {
            return;
            try
            {
                this.mActivityManager.stopServiceToken(new ComponentName(this, this.mClassName), this.mToken, paramInt);
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public final boolean stopSelfResult(int paramInt)
    {
        boolean bool1 = false;
        if (this.mActivityManager == null);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mActivityManager.stopServiceToken(new ComponentName(this, this.mClassName), this.mToken, paramInt);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Service
 * JD-Core Version:        0.6.2
 */