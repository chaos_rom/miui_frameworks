package android.app;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class QueuedWork
{
    private static final ConcurrentLinkedQueue<Runnable> sPendingWorkFinishers = new ConcurrentLinkedQueue();
    private static ExecutorService sSingleThreadExecutor = null;

    public static void add(Runnable paramRunnable)
    {
        sPendingWorkFinishers.add(paramRunnable);
    }

    public static boolean hasPendingWork()
    {
        if (!sPendingWorkFinishers.isEmpty());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static void remove(Runnable paramRunnable)
    {
        sPendingWorkFinishers.remove(paramRunnable);
    }

    public static ExecutorService singleThreadExecutor()
    {
        try
        {
            if (sSingleThreadExecutor == null)
                sSingleThreadExecutor = Executors.newSingleThreadExecutor();
            ExecutorService localExecutorService = sSingleThreadExecutor;
            return localExecutorService;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static void waitToFinish()
    {
        while (true)
        {
            Runnable localRunnable = (Runnable)sPendingWorkFinishers.poll();
            if (localRunnable == null)
                break;
            localRunnable.run();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.QueuedWork
 * JD-Core Version:        0.6.2
 */