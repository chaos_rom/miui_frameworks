package android.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnShowListener;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.KeyEvent.Callback;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.app.ActionBarImpl;
import com.android.internal.policy.PolicyManager;
import java.lang.ref.WeakReference;

public class Dialog
    implements DialogInterface, Window.Callback, KeyEvent.Callback, View.OnCreateContextMenuListener
{
    private static final int CANCEL = 68;
    private static final String DIALOG_HIERARCHY_TAG = "android:dialogHierarchy";
    private static final String DIALOG_SHOWING_TAG = "android:dialogShowing";
    private static final int DISMISS = 67;
    private static final int SHOW = 69;
    private static final String TAG = "Dialog";
    private ActionBarImpl mActionBar;
    private ActionMode mActionMode;
    private String mCancelAndDismissTaken;
    private Message mCancelMessage;
    protected boolean mCancelable = true;
    private boolean mCanceled = false;
    final Context mContext;
    private boolean mCreated = false;
    View mDecor;
    private final Runnable mDismissAction = new Runnable()
    {
        public void run()
        {
            Dialog.this.dismissDialog();
        }
    };
    private Message mDismissMessage;
    private final Handler mHandler = new Handler();
    private Handler mListenersHandler;
    private DialogInterface.OnKeyListener mOnKeyListener;
    private Activity mOwnerActivity;
    private Message mShowMessage;
    private boolean mShowing = false;
    Window mWindow;
    final WindowManager mWindowManager;

    public Dialog(Context paramContext)
    {
        this(paramContext, 0, true);
    }

    public Dialog(Context paramContext, int paramInt)
    {
        this(paramContext, paramInt, true);
    }

    Dialog(Context paramContext, int paramInt, boolean paramBoolean)
    {
        if (paramInt == 0)
        {
            TypedValue localTypedValue = new TypedValue();
            paramContext.getTheme().resolveAttribute(16843528, localTypedValue, true);
            paramInt = localTypedValue.resourceId;
        }
        if (paramBoolean);
        for (Object localObject = new ContextThemeWrapper(paramContext, paramInt); ; localObject = paramContext)
        {
            this.mContext = ((Context)localObject);
            this.mWindowManager = ((WindowManager)paramContext.getSystemService("window"));
            Window localWindow = PolicyManager.makeNewWindow(this.mContext);
            this.mWindow = localWindow;
            localWindow.setCallback(this);
            localWindow.setWindowManager(this.mWindowManager, null, null);
            localWindow.setGravity(17);
            this.mListenersHandler = new ListenersHandler(this);
            return;
        }
    }

    protected Dialog(Context paramContext, boolean paramBoolean, DialogInterface.OnCancelListener paramOnCancelListener)
    {
        this(paramContext);
        this.mCancelable = paramBoolean;
        setOnCancelListener(paramOnCancelListener);
    }

    @Deprecated
    protected Dialog(Context paramContext, boolean paramBoolean, Message paramMessage)
    {
        this(paramContext);
        this.mCancelable = paramBoolean;
        this.mCancelMessage = paramMessage;
    }

    private ComponentName getAssociatedActivity()
    {
        ComponentName localComponentName = null;
        Activity localActivity = this.mOwnerActivity;
        Context localContext = getContext();
        while ((localActivity == null) && (localContext != null))
            if ((localContext instanceof Activity))
            {
                localActivity = (Activity)localContext;
            }
            else
            {
                if ((localContext instanceof ContextWrapper));
                for (localContext = ((ContextWrapper)localContext).getBaseContext(); ; localContext = null)
                    break;
            }
        if (localActivity == null);
        while (true)
        {
            return localComponentName;
            localComponentName = localActivity.getComponentName();
        }
    }

    private void sendDismissMessage()
    {
        if (this.mDismissMessage != null)
            Message.obtain(this.mDismissMessage).sendToTarget();
    }

    private void sendShowMessage()
    {
        if (this.mShowMessage != null)
            Message.obtain(this.mShowMessage).sendToTarget();
    }

    public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        this.mWindow.addContentView(paramView, paramLayoutParams);
    }

    public void cancel()
    {
        if ((!this.mCanceled) && (this.mCancelMessage != null))
        {
            this.mCanceled = true;
            Message.obtain(this.mCancelMessage).sendToTarget();
        }
        dismiss();
    }

    public void closeOptionsMenu()
    {
        this.mWindow.closePanel(0);
    }

    public void dismiss()
    {
        if (Looper.myLooper() == this.mHandler.getLooper())
            dismissDialog();
        while (true)
        {
            return;
            this.mHandler.post(this.mDismissAction);
        }
    }

    void dismissDialog()
    {
        if ((this.mDecor == null) || (!this.mShowing));
        while (true)
        {
            return;
            if (this.mWindow.isDestroyed())
            {
                Log.e("Dialog", "Tried to dismissDialog() but the Dialog's window was already destroyed!");
                continue;
            }
            try
            {
                this.mWindowManager.removeView(this.mDecor);
                if (this.mActionMode != null)
                    this.mActionMode.finish();
                this.mDecor = null;
                this.mWindow.closeAllPanels();
                onStop();
                this.mShowing = false;
                sendDismissMessage();
            }
            finally
            {
                if (this.mActionMode != null)
                    this.mActionMode.finish();
                this.mDecor = null;
                this.mWindow.closeAllPanels();
                onStop();
                this.mShowing = false;
                sendDismissMessage();
            }
        }
    }

    public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if (this.mWindow.superDispatchGenericMotionEvent(paramMotionEvent));
        for (boolean bool = true; ; bool = onGenericMotionEvent(paramMotionEvent))
            return bool;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        if ((this.mOnKeyListener != null) && (this.mOnKeyListener.onKey(this, paramKeyEvent.getKeyCode(), paramKeyEvent)));
        while (this.mWindow.superDispatchKeyEvent(paramKeyEvent))
            return bool;
        if (this.mDecor != null);
        for (KeyEvent.DispatcherState localDispatcherState = this.mDecor.getKeyDispatcherState(); ; localDispatcherState = null)
        {
            bool = paramKeyEvent.dispatch(this, localDispatcherState, this);
            break;
        }
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
    {
        if (this.mWindow.superDispatchKeyShortcutEvent(paramKeyEvent));
        for (boolean bool = true; ; bool = onKeyShortcut(paramKeyEvent.getKeyCode(), paramKeyEvent))
            return bool;
    }

    void dispatchOnCreate(Bundle paramBundle)
    {
        if (!this.mCreated)
        {
            onCreate(paramBundle);
            this.mCreated = true;
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        paramAccessibilityEvent.setClassName(getClass().getName());
        paramAccessibilityEvent.setPackageName(this.mContext.getPackageName());
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        if ((localLayoutParams.width == -1) && (localLayoutParams.height == -1));
        for (boolean bool = true; ; bool = false)
        {
            paramAccessibilityEvent.setFullScreen(bool);
            return false;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mWindow.superDispatchTouchEvent(paramMotionEvent));
        for (boolean bool = true; ; bool = onTouchEvent(paramMotionEvent))
            return bool;
    }

    public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
    {
        if (this.mWindow.superDispatchTrackballEvent(paramMotionEvent));
        for (boolean bool = true; ; bool = onTrackballEvent(paramMotionEvent))
            return bool;
    }

    public View findViewById(int paramInt)
    {
        return this.mWindow.findViewById(paramInt);
    }

    public ActionBar getActionBar()
    {
        return this.mActionBar;
    }

    public final Context getContext()
    {
        return this.mContext;
    }

    public View getCurrentFocus()
    {
        if (this.mWindow != null);
        for (View localView = this.mWindow.getCurrentFocus(); ; localView = null)
            return localView;
    }

    public LayoutInflater getLayoutInflater()
    {
        return getWindow().getLayoutInflater();
    }

    public final Activity getOwnerActivity()
    {
        return this.mOwnerActivity;
    }

    public final int getVolumeControlStream()
    {
        return getWindow().getVolumeControlStream();
    }

    public Window getWindow()
    {
        return this.mWindow;
    }

    public void hide()
    {
        if (this.mDecor != null)
            this.mDecor.setVisibility(8);
    }

    public void invalidateOptionsMenu()
    {
        this.mWindow.invalidatePanelMenu(0);
    }

    public boolean isShowing()
    {
        return this.mShowing;
    }

    public void onActionModeFinished(ActionMode paramActionMode)
    {
        if (paramActionMode == this.mActionMode)
            this.mActionMode = null;
    }

    public void onActionModeStarted(ActionMode paramActionMode)
    {
        this.mActionMode = paramActionMode;
    }

    public void onAttachedToWindow()
    {
    }

    public void onBackPressed()
    {
        if (this.mCancelable)
            cancel();
    }

    public void onContentChanged()
    {
    }

    public boolean onContextItemSelected(MenuItem paramMenuItem)
    {
        return false;
    }

    public void onContextMenuClosed(Menu paramMenu)
    {
    }

    protected void onCreate(Bundle paramBundle)
    {
    }

    public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
    {
    }

    public boolean onCreateOptionsMenu(Menu paramMenu)
    {
        return true;
    }

    public boolean onCreatePanelMenu(int paramInt, Menu paramMenu)
    {
        if (paramInt == 0);
        for (boolean bool = onCreateOptionsMenu(paramMenu); ; bool = false)
            return bool;
    }

    public View onCreatePanelView(int paramInt)
    {
        return null;
    }

    public void onDetachedFromWindow()
    {
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if (paramInt == 4)
            paramKeyEvent.startTracking();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((paramInt == 4) && (paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
            onBackPressed();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem)
    {
        return false;
    }

    public boolean onMenuOpened(int paramInt, Menu paramMenu)
    {
        if (paramInt == 8)
            this.mActionBar.dispatchMenuVisibilityChanged(true);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem)
    {
        return false;
    }

    public void onOptionsMenuClosed(Menu paramMenu)
    {
    }

    public void onPanelClosed(int paramInt, Menu paramMenu)
    {
        if (paramInt == 8)
            this.mActionBar.dispatchMenuVisibilityChanged(false);
    }

    public boolean onPrepareOptionsMenu(Menu paramMenu)
    {
        return true;
    }

    public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu)
    {
        boolean bool = true;
        if ((paramInt != 0) || (paramMenu == null) || ((onPrepareOptionsMenu(paramMenu)) && (paramMenu.hasVisibleItems())));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public void onRestoreInstanceState(Bundle paramBundle)
    {
        Bundle localBundle = paramBundle.getBundle("android:dialogHierarchy");
        if (localBundle == null);
        while (true)
        {
            return;
            dispatchOnCreate(paramBundle);
            this.mWindow.restoreHierarchyState(localBundle);
            if (paramBundle.getBoolean("android:dialogShowing"))
                show();
        }
    }

    public Bundle onSaveInstanceState()
    {
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("android:dialogShowing", this.mShowing);
        if (this.mCreated)
            localBundle.putBundle("android:dialogHierarchy", this.mWindow.saveHierarchyState());
        return localBundle;
    }

    public boolean onSearchRequested()
    {
        boolean bool = false;
        SearchManager localSearchManager = (SearchManager)this.mContext.getSystemService("search");
        ComponentName localComponentName = getAssociatedActivity();
        if ((localComponentName != null) && (localSearchManager.getSearchableInfo(localComponentName) != null))
        {
            localSearchManager.startSearch(null, false, localComponentName, null, false);
            dismiss();
            bool = true;
        }
        return bool;
    }

    protected void onStart()
    {
        if (this.mActionBar != null)
            this.mActionBar.setShowHideAnimationEnabled(true);
    }

    protected void onStop()
    {
        if (this.mActionBar != null)
            this.mActionBar.setShowHideAnimationEnabled(false);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if ((this.mCancelable) && (this.mShowing) && (this.mWindow.shouldCloseOnTouch(this.mContext, paramMotionEvent)))
            cancel();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams)
    {
        if (this.mDecor != null)
            this.mWindowManager.updateViewLayout(this.mDecor, paramLayoutParams);
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
    }

    public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback)
    {
        if (this.mActionBar != null);
        for (ActionMode localActionMode = this.mActionBar.startActionMode(paramCallback); ; localActionMode = null)
            return localActionMode;
    }

    public void openContextMenu(View paramView)
    {
        paramView.showContextMenu();
    }

    public void openOptionsMenu()
    {
        this.mWindow.openPanel(0, null);
    }

    public void registerForContextMenu(View paramView)
    {
        paramView.setOnCreateContextMenuListener(this);
    }

    public final boolean requestWindowFeature(int paramInt)
    {
        return getWindow().requestFeature(paramInt);
    }

    public void setCancelMessage(Message paramMessage)
    {
        this.mCancelMessage = paramMessage;
    }

    public void setCancelable(boolean paramBoolean)
    {
        this.mCancelable = paramBoolean;
    }

    public void setCanceledOnTouchOutside(boolean paramBoolean)
    {
        if ((paramBoolean) && (!this.mCancelable))
            this.mCancelable = true;
        this.mWindow.setCloseOnTouchOutside(paramBoolean);
    }

    public void setContentView(int paramInt)
    {
        this.mWindow.setContentView(paramInt);
    }

    public void setContentView(View paramView)
    {
        this.mWindow.setContentView(paramView);
    }

    public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        this.mWindow.setContentView(paramView, paramLayoutParams);
    }

    public void setDismissMessage(Message paramMessage)
    {
        this.mDismissMessage = paramMessage;
    }

    public final void setFeatureDrawable(int paramInt, Drawable paramDrawable)
    {
        getWindow().setFeatureDrawable(paramInt, paramDrawable);
    }

    public final void setFeatureDrawableAlpha(int paramInt1, int paramInt2)
    {
        getWindow().setFeatureDrawableAlpha(paramInt1, paramInt2);
    }

    public final void setFeatureDrawableResource(int paramInt1, int paramInt2)
    {
        getWindow().setFeatureDrawableResource(paramInt1, paramInt2);
    }

    public final void setFeatureDrawableUri(int paramInt, Uri paramUri)
    {
        getWindow().setFeatureDrawableUri(paramInt, paramUri);
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener paramOnCancelListener)
    {
        if (this.mCancelAndDismissTaken != null)
            throw new IllegalStateException("OnCancelListener is already taken by " + this.mCancelAndDismissTaken + " and can not be replaced.");
        if (paramOnCancelListener != null);
        for (this.mCancelMessage = this.mListenersHandler.obtainMessage(68, paramOnCancelListener); ; this.mCancelMessage = null)
            return;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener paramOnDismissListener)
    {
        if (this.mCancelAndDismissTaken != null)
            throw new IllegalStateException("OnDismissListener is already taken by " + this.mCancelAndDismissTaken + " and can not be replaced.");
        if (paramOnDismissListener != null);
        for (this.mDismissMessage = this.mListenersHandler.obtainMessage(67, paramOnDismissListener); ; this.mDismissMessage = null)
            return;
    }

    public void setOnKeyListener(DialogInterface.OnKeyListener paramOnKeyListener)
    {
        this.mOnKeyListener = paramOnKeyListener;
    }

    public void setOnShowListener(DialogInterface.OnShowListener paramOnShowListener)
    {
        if (paramOnShowListener != null);
        for (this.mShowMessage = this.mListenersHandler.obtainMessage(69, paramOnShowListener); ; this.mShowMessage = null)
            return;
    }

    public final void setOwnerActivity(Activity paramActivity)
    {
        this.mOwnerActivity = paramActivity;
        getWindow().setVolumeControlStream(this.mOwnerActivity.getVolumeControlStream());
    }

    public void setTitle(int paramInt)
    {
        setTitle(this.mContext.getText(paramInt));
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        this.mWindow.setTitle(paramCharSequence);
        this.mWindow.getAttributes().setTitle(paramCharSequence);
    }

    public final void setVolumeControlStream(int paramInt)
    {
        getWindow().setVolumeControlStream(paramInt);
    }

    public void show()
    {
        if (this.mShowing)
            if (this.mDecor != null)
            {
                if (this.mWindow.hasFeature(8))
                    this.mWindow.invalidatePanelMenu(8);
                this.mDecor.setVisibility(0);
            }
        while (true)
        {
            return;
            this.mCanceled = false;
            if (!this.mCreated)
                dispatchOnCreate(null);
            onStart();
            this.mDecor = this.mWindow.getDecorView();
            if ((this.mActionBar == null) && (this.mWindow.hasFeature(8)))
                this.mActionBar = new ActionBarImpl(this);
            Object localObject1 = this.mWindow.getAttributes();
            if ((0x100 & ((WindowManager.LayoutParams)localObject1).softInputMode) == 0)
            {
                WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
                localLayoutParams.copyFrom((WindowManager.LayoutParams)localObject1);
                localLayoutParams.softInputMode = (0x100 | localLayoutParams.softInputMode);
                localObject1 = localLayoutParams;
            }
            try
            {
                this.mWindowManager.addView(this.mDecor, (ViewGroup.LayoutParams)localObject1);
                this.mShowing = true;
                sendShowMessage();
            }
            finally
            {
            }
        }
    }

    public boolean takeCancelAndDismissListeners(String paramString, DialogInterface.OnCancelListener paramOnCancelListener, DialogInterface.OnDismissListener paramOnDismissListener)
    {
        if (this.mCancelAndDismissTaken != null)
        {
            this.mCancelAndDismissTaken = null;
            setOnCancelListener(paramOnCancelListener);
            setOnDismissListener(paramOnDismissListener);
            this.mCancelAndDismissTaken = paramString;
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            if ((this.mCancelMessage == null) && (this.mDismissMessage == null))
                break;
        }
    }

    public void takeKeyEvents(boolean paramBoolean)
    {
        this.mWindow.takeKeyEvents(paramBoolean);
    }

    public void unregisterForContextMenu(View paramView)
    {
        paramView.setOnCreateContextMenuListener(null);
    }

    private static final class ListenersHandler extends Handler
    {
        private WeakReference<DialogInterface> mDialog;

        public ListenersHandler(Dialog paramDialog)
        {
            this.mDialog = new WeakReference(paramDialog);
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 67:
            case 68:
            case 69:
            }
            while (true)
            {
                return;
                ((DialogInterface.OnDismissListener)paramMessage.obj).onDismiss((DialogInterface)this.mDialog.get());
                continue;
                ((DialogInterface.OnCancelListener)paramMessage.obj).onCancel((DialogInterface)this.mDialog.get());
                continue;
                ((DialogInterface.OnShowListener)paramMessage.obj).onShow((DialogInterface)this.mDialog.get());
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.Dialog
 * JD-Core Version:        0.6.2
 */