package android.app;

import android.util.AndroidRuntimeException;

final class IntentReceiverLeaked extends AndroidRuntimeException
{
    public IntentReceiverLeaked(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.app.IntentReceiverLeaked
 * JD-Core Version:        0.6.2
 */