package android.opengl;

public class Matrix
{
    private static final float[] sTemp = new float[32];

    public static void frustumM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        if (paramFloat1 == paramFloat2)
            throw new IllegalArgumentException("left == right");
        if (paramFloat4 == paramFloat3)
            throw new IllegalArgumentException("top == bottom");
        if (paramFloat5 == paramFloat6)
            throw new IllegalArgumentException("near == far");
        if (paramFloat5 <= 0.0F)
            throw new IllegalArgumentException("near <= 0.0f");
        if (paramFloat6 <= 0.0F)
            throw new IllegalArgumentException("far <= 0.0f");
        float f1 = 1.0F / (paramFloat2 - paramFloat1);
        float f2 = 1.0F / (paramFloat4 - paramFloat3);
        float f3 = 1.0F / (paramFloat5 - paramFloat6);
        float f4 = 2.0F * (paramFloat5 * f1);
        float f5 = 2.0F * (paramFloat5 * f2);
        float f6 = 2.0F * (f1 * (paramFloat2 + paramFloat1));
        float f7 = f2 * (paramFloat4 + paramFloat3);
        float f8 = f3 * (paramFloat6 + paramFloat5);
        float f9 = 2.0F * (f3 * (paramFloat6 * paramFloat5));
        paramArrayOfFloat[(paramInt + 0)] = f4;
        paramArrayOfFloat[(paramInt + 5)] = f5;
        paramArrayOfFloat[(paramInt + 8)] = f6;
        paramArrayOfFloat[(paramInt + 9)] = f7;
        paramArrayOfFloat[(paramInt + 10)] = f8;
        paramArrayOfFloat[(paramInt + 14)] = f9;
        paramArrayOfFloat[(paramInt + 11)] = -1.0F;
        paramArrayOfFloat[(paramInt + 1)] = 0.0F;
        paramArrayOfFloat[(paramInt + 2)] = 0.0F;
        paramArrayOfFloat[(paramInt + 3)] = 0.0F;
        paramArrayOfFloat[(paramInt + 4)] = 0.0F;
        paramArrayOfFloat[(paramInt + 6)] = 0.0F;
        paramArrayOfFloat[(paramInt + 7)] = 0.0F;
        paramArrayOfFloat[(paramInt + 12)] = 0.0F;
        paramArrayOfFloat[(paramInt + 13)] = 0.0F;
        paramArrayOfFloat[(paramInt + 15)] = 0.0F;
    }

    public static boolean invertM(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2)
    {
        float f1 = paramArrayOfFloat2[(paramInt2 + 0)];
        float f2 = paramArrayOfFloat2[(paramInt2 + 1)];
        float f3 = paramArrayOfFloat2[(paramInt2 + 2)];
        float f4 = paramArrayOfFloat2[(paramInt2 + 3)];
        float f5 = paramArrayOfFloat2[(paramInt2 + 4)];
        float f6 = paramArrayOfFloat2[(paramInt2 + 5)];
        float f7 = paramArrayOfFloat2[(paramInt2 + 6)];
        float f8 = paramArrayOfFloat2[(paramInt2 + 7)];
        float f9 = paramArrayOfFloat2[(paramInt2 + 8)];
        float f10 = paramArrayOfFloat2[(paramInt2 + 9)];
        float f11 = paramArrayOfFloat2[(paramInt2 + 10)];
        float f12 = paramArrayOfFloat2[(paramInt2 + 11)];
        float f13 = paramArrayOfFloat2[(paramInt2 + 12)];
        float f14 = paramArrayOfFloat2[(paramInt2 + 13)];
        float f15 = paramArrayOfFloat2[(paramInt2 + 14)];
        float f16 = paramArrayOfFloat2[(paramInt2 + 15)];
        float f17 = f11 * f16;
        float f18 = f15 * f12;
        float f19 = f7 * f16;
        float f20 = f15 * f8;
        float f21 = f7 * f12;
        float f22 = f11 * f8;
        float f23 = f3 * f16;
        float f24 = f15 * f4;
        float f25 = f3 * f12;
        float f26 = f11 * f4;
        float f27 = f3 * f8;
        float f28 = f7 * f4;
        float f29 = f17 * f6 + f20 * f10 + f21 * f14 - (f18 * f6 + f19 * f10 + f22 * f14);
        float f30 = f18 * f2 + f23 * f10 + f26 * f14 - (f17 * f2 + f24 * f10 + f25 * f14);
        float f31 = f19 * f2 + f24 * f6 + f27 * f14 - (f20 * f2 + f23 * f6 + f28 * f14);
        float f32 = f22 * f2 + f25 * f6 + f28 * f10 - (f21 * f2 + f26 * f6 + f27 * f10);
        float f33 = f18 * f5 + f19 * f9 + f22 * f13 - (f17 * f5 + f20 * f9 + f21 * f13);
        float f34 = f17 * f1 + f24 * f9 + f25 * f13 - (f18 * f1 + f23 * f9 + f26 * f13);
        float f35 = f20 * f1 + f23 * f5 + f28 * f13 - (f19 * f1 + f24 * f5 + f27 * f13);
        float f36 = f21 * f1 + f26 * f5 + f27 * f9 - (f22 * f1 + f25 * f5 + f28 * f9);
        float f37 = f9 * f14;
        float f38 = f13 * f10;
        float f39 = f5 * f14;
        float f40 = f13 * f6;
        float f41 = f5 * f10;
        float f42 = f9 * f6;
        float f43 = f1 * f14;
        float f44 = f13 * f2;
        float f45 = f1 * f10;
        float f46 = f9 * f2;
        float f47 = f1 * f6;
        float f48 = f5 * f2;
        float f49 = f37 * f8 + f40 * f12 + f41 * f16 - (f38 * f8 + f39 * f12 + f42 * f16);
        float f50 = f38 * f4 + f43 * f12 + f46 * f16 - (f37 * f4 + f44 * f12 + f45 * f16);
        float f51 = f39 * f4 + f44 * f8 + f47 * f16 - (f40 * f4 + f43 * f8 + f48 * f16);
        float f52 = f42 * f4 + f45 * f8 + f48 * f12 - (f41 * f4 + f46 * f8 + f47 * f12);
        float f53 = f39 * f11 + f42 * f15 + f38 * f7 - (f41 * f15 + f37 * f7 + f40 * f11);
        float f54 = f45 * f15 + f37 * f3 + f44 * f11 - (f43 * f11 + f46 * f15 + f38 * f3);
        float f55 = f43 * f7 + f48 * f15 + f40 * f3 - (f47 * f15 + f39 * f3 + f44 * f7);
        float f56 = f47 * f11 + f41 * f3 + f46 * f7 - (f45 * f7 + f48 * f11 + f42 * f3);
        float f57 = f1 * f29 + f5 * f30 + f9 * f31 + f13 * f32;
        if (f57 == 0.0F);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            float f58 = 1.0F / f57;
            paramArrayOfFloat1[paramInt1] = (f29 * f58);
            paramArrayOfFloat1[(paramInt1 + 1)] = (f30 * f58);
            paramArrayOfFloat1[(paramInt1 + 2)] = (f31 * f58);
            paramArrayOfFloat1[(paramInt1 + 3)] = (f32 * f58);
            paramArrayOfFloat1[(paramInt1 + 4)] = (f33 * f58);
            paramArrayOfFloat1[(paramInt1 + 5)] = (f34 * f58);
            paramArrayOfFloat1[(paramInt1 + 6)] = (f35 * f58);
            paramArrayOfFloat1[(paramInt1 + 7)] = (f36 * f58);
            paramArrayOfFloat1[(paramInt1 + 8)] = (f49 * f58);
            paramArrayOfFloat1[(paramInt1 + 9)] = (f50 * f58);
            paramArrayOfFloat1[(paramInt1 + 10)] = (f51 * f58);
            paramArrayOfFloat1[(paramInt1 + 11)] = (f52 * f58);
            paramArrayOfFloat1[(paramInt1 + 12)] = (f53 * f58);
            paramArrayOfFloat1[(paramInt1 + 13)] = (f54 * f58);
            paramArrayOfFloat1[(paramInt1 + 14)] = (f55 * f58);
            paramArrayOfFloat1[(paramInt1 + 15)] = (f56 * f58);
        }
    }

    public static float length(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return (float)Math.sqrt(paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2 + paramFloat3 * paramFloat3);
    }

    public static native void multiplyMM(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, float[] paramArrayOfFloat3, int paramInt3);

    public static native void multiplyMV(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, float[] paramArrayOfFloat3, int paramInt3);

    public static void orthoM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        if (paramFloat1 == paramFloat2)
            throw new IllegalArgumentException("left == right");
        if (paramFloat3 == paramFloat4)
            throw new IllegalArgumentException("bottom == top");
        if (paramFloat5 == paramFloat6)
            throw new IllegalArgumentException("near == far");
        float f1 = 1.0F / (paramFloat2 - paramFloat1);
        float f2 = 1.0F / (paramFloat4 - paramFloat3);
        float f3 = 1.0F / (paramFloat6 - paramFloat5);
        float f4 = 2.0F * f1;
        float f5 = 2.0F * f2;
        float f6 = -2.0F * f3;
        float f7 = f1 * -(paramFloat2 + paramFloat1);
        float f8 = f2 * -(paramFloat4 + paramFloat3);
        float f9 = f3 * -(paramFloat6 + paramFloat5);
        paramArrayOfFloat[(paramInt + 0)] = f4;
        paramArrayOfFloat[(paramInt + 5)] = f5;
        paramArrayOfFloat[(paramInt + 10)] = f6;
        paramArrayOfFloat[(paramInt + 12)] = f7;
        paramArrayOfFloat[(paramInt + 13)] = f8;
        paramArrayOfFloat[(paramInt + 14)] = f9;
        paramArrayOfFloat[(paramInt + 15)] = 1.0F;
        paramArrayOfFloat[(paramInt + 1)] = 0.0F;
        paramArrayOfFloat[(paramInt + 2)] = 0.0F;
        paramArrayOfFloat[(paramInt + 3)] = 0.0F;
        paramArrayOfFloat[(paramInt + 4)] = 0.0F;
        paramArrayOfFloat[(paramInt + 6)] = 0.0F;
        paramArrayOfFloat[(paramInt + 7)] = 0.0F;
        paramArrayOfFloat[(paramInt + 8)] = 0.0F;
        paramArrayOfFloat[(paramInt + 9)] = 0.0F;
        paramArrayOfFloat[(paramInt + 11)] = 0.0F;
    }

    public static void perspectiveM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        float f1 = 1.0F / (float)Math.tan(0.008726646259971648D * paramFloat1);
        float f2 = 1.0F / (paramFloat3 - paramFloat4);
        paramArrayOfFloat[(paramInt + 0)] = (f1 / paramFloat2);
        paramArrayOfFloat[(paramInt + 1)] = 0.0F;
        paramArrayOfFloat[(paramInt + 2)] = 0.0F;
        paramArrayOfFloat[(paramInt + 3)] = 0.0F;
        paramArrayOfFloat[(paramInt + 4)] = 0.0F;
        paramArrayOfFloat[(paramInt + 5)] = f1;
        paramArrayOfFloat[(paramInt + 6)] = 0.0F;
        paramArrayOfFloat[(paramInt + 7)] = 0.0F;
        paramArrayOfFloat[(paramInt + 8)] = 0.0F;
        paramArrayOfFloat[(paramInt + 9)] = 0.0F;
        paramArrayOfFloat[(paramInt + 10)] = (f2 * (paramFloat4 + paramFloat3));
        paramArrayOfFloat[(paramInt + 11)] = -1.0F;
        paramArrayOfFloat[(paramInt + 12)] = 0.0F;
        paramArrayOfFloat[(paramInt + 13)] = 0.0F;
        paramArrayOfFloat[(paramInt + 14)] = (f2 * (paramFloat3 * (2.0F * paramFloat4)));
        paramArrayOfFloat[(paramInt + 15)] = 0.0F;
    }

    public static void rotateM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        synchronized (sTemp)
        {
            setRotateM(sTemp, 0, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
            multiplyMM(sTemp, 16, paramArrayOfFloat, paramInt, sTemp, 0);
            System.arraycopy(sTemp, 16, paramArrayOfFloat, paramInt, 16);
            return;
        }
    }

    public static void rotateM(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        synchronized (sTemp)
        {
            setRotateM(sTemp, 0, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
            multiplyMM(paramArrayOfFloat1, paramInt1, paramArrayOfFloat2, paramInt2, sTemp, 0);
            return;
        }
    }

    public static void scaleM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
    {
        for (int i = 0; i < 4; i++)
        {
            int j = paramInt + i;
            paramArrayOfFloat[j] = (paramFloat1 * paramArrayOfFloat[j]);
            int k = j + 4;
            paramArrayOfFloat[k] = (paramFloat2 * paramArrayOfFloat[k]);
            int m = j + 8;
            paramArrayOfFloat[m] = (paramFloat3 * paramArrayOfFloat[m]);
        }
    }

    public static void scaleM(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3)
    {
        for (int i = 0; i < 4; i++)
        {
            int j = paramInt1 + i;
            int k = paramInt2 + i;
            paramArrayOfFloat1[j] = (paramFloat1 * paramArrayOfFloat2[k]);
            paramArrayOfFloat1[(j + 4)] = (paramFloat2 * paramArrayOfFloat2[(k + 4)]);
            paramArrayOfFloat1[(j + 8)] = (paramFloat3 * paramArrayOfFloat2[(k + 8)]);
            paramArrayOfFloat1[(j + 12)] = paramArrayOfFloat2[(k + 12)];
        }
    }

    public static void setIdentityM(float[] paramArrayOfFloat, int paramInt)
    {
        for (int i = 0; i < 16; i++)
            paramArrayOfFloat[(paramInt + i)] = 0.0F;
        for (int j = 0; j < 16; j += 5)
            paramArrayOfFloat[(paramInt + j)] = 1.0F;
    }

    public static void setLookAtM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9)
    {
        float f1 = paramFloat4 - paramFloat1;
        float f2 = paramFloat5 - paramFloat2;
        float f3 = paramFloat6 - paramFloat3;
        float f4 = 1.0F / length(f1, f2, f3);
        float f5 = f1 * f4;
        float f6 = f2 * f4;
        float f7 = f3 * f4;
        float f8 = f6 * paramFloat9 - f7 * paramFloat8;
        float f9 = f7 * paramFloat7 - f5 * paramFloat9;
        float f10 = f5 * paramFloat8 - f6 * paramFloat7;
        float f11 = 1.0F / length(f8, f9, f10);
        float f12 = f8 * f11;
        float f13 = f9 * f11;
        float f14 = f10 * f11;
        float f15 = f13 * f7 - f14 * f6;
        float f16 = f14 * f5 - f12 * f7;
        float f17 = f12 * f6 - f13 * f5;
        paramArrayOfFloat[(paramInt + 0)] = f12;
        paramArrayOfFloat[(paramInt + 1)] = f15;
        paramArrayOfFloat[(paramInt + 2)] = (-f5);
        paramArrayOfFloat[(paramInt + 3)] = 0.0F;
        paramArrayOfFloat[(paramInt + 4)] = f13;
        paramArrayOfFloat[(paramInt + 5)] = f16;
        paramArrayOfFloat[(paramInt + 6)] = (-f6);
        paramArrayOfFloat[(paramInt + 7)] = 0.0F;
        paramArrayOfFloat[(paramInt + 8)] = f14;
        paramArrayOfFloat[(paramInt + 9)] = f17;
        paramArrayOfFloat[(paramInt + 10)] = (-f7);
        paramArrayOfFloat[(paramInt + 11)] = 0.0F;
        paramArrayOfFloat[(paramInt + 12)] = 0.0F;
        paramArrayOfFloat[(paramInt + 13)] = 0.0F;
        paramArrayOfFloat[(paramInt + 14)] = 0.0F;
        paramArrayOfFloat[(paramInt + 15)] = 1.0F;
        translateM(paramArrayOfFloat, paramInt, -paramFloat1, -paramFloat2, -paramFloat3);
    }

    public static void setRotateEulerM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
    {
        float f1 = paramFloat1 * 0.01745329F;
        float f2 = paramFloat2 * 0.01745329F;
        float f3 = paramFloat3 * 0.01745329F;
        float f4 = (float)Math.cos(f1);
        float f5 = (float)Math.sin(f1);
        float f6 = (float)Math.cos(f2);
        float f7 = (float)Math.sin(f2);
        float f8 = (float)Math.cos(f3);
        float f9 = (float)Math.sin(f3);
        float f10 = f4 * f7;
        float f11 = f5 * f7;
        paramArrayOfFloat[(paramInt + 0)] = (f6 * f8);
        paramArrayOfFloat[(paramInt + 1)] = (f9 * -f6);
        paramArrayOfFloat[(paramInt + 2)] = f7;
        paramArrayOfFloat[(paramInt + 3)] = 0.0F;
        paramArrayOfFloat[(paramInt + 4)] = (f10 * f8 + f4 * f9);
        paramArrayOfFloat[(paramInt + 5)] = (f9 * -f10 + f4 * f8);
        paramArrayOfFloat[(paramInt + 6)] = (f6 * -f5);
        paramArrayOfFloat[(paramInt + 7)] = 0.0F;
        paramArrayOfFloat[(paramInt + 8)] = (f8 * -f11 + f5 * f9);
        paramArrayOfFloat[(paramInt + 9)] = (f11 * f9 + f5 * f8);
        paramArrayOfFloat[(paramInt + 10)] = (f4 * f6);
        paramArrayOfFloat[(paramInt + 11)] = 0.0F;
        paramArrayOfFloat[(paramInt + 12)] = 0.0F;
        paramArrayOfFloat[(paramInt + 13)] = 0.0F;
        paramArrayOfFloat[(paramInt + 14)] = 0.0F;
        paramArrayOfFloat[(paramInt + 15)] = 1.0F;
    }

    public static void setRotateM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        paramArrayOfFloat[(paramInt + 3)] = 0.0F;
        paramArrayOfFloat[(paramInt + 7)] = 0.0F;
        paramArrayOfFloat[(paramInt + 11)] = 0.0F;
        paramArrayOfFloat[(paramInt + 12)] = 0.0F;
        paramArrayOfFloat[(paramInt + 13)] = 0.0F;
        paramArrayOfFloat[(paramInt + 14)] = 0.0F;
        paramArrayOfFloat[(paramInt + 15)] = 1.0F;
        float f1 = paramFloat1 * 0.01745329F;
        float f2 = (float)Math.sin(f1);
        float f3 = (float)Math.cos(f1);
        if ((1.0F == paramFloat2) && (0.0F == paramFloat3) && (0.0F == paramFloat4))
        {
            paramArrayOfFloat[(paramInt + 5)] = f3;
            paramArrayOfFloat[(paramInt + 10)] = f3;
            paramArrayOfFloat[(paramInt + 6)] = f2;
            paramArrayOfFloat[(paramInt + 9)] = (-f2);
            paramArrayOfFloat[(paramInt + 1)] = 0.0F;
            paramArrayOfFloat[(paramInt + 2)] = 0.0F;
            paramArrayOfFloat[(paramInt + 4)] = 0.0F;
            paramArrayOfFloat[(paramInt + 8)] = 0.0F;
            paramArrayOfFloat[(paramInt + 0)] = 1.0F;
        }
        while (true)
        {
            return;
            if ((0.0F == paramFloat2) && (1.0F == paramFloat3) && (0.0F == paramFloat4))
            {
                paramArrayOfFloat[(paramInt + 0)] = f3;
                paramArrayOfFloat[(paramInt + 10)] = f3;
                paramArrayOfFloat[(paramInt + 8)] = f2;
                paramArrayOfFloat[(paramInt + 2)] = (-f2);
                paramArrayOfFloat[(paramInt + 1)] = 0.0F;
                paramArrayOfFloat[(paramInt + 4)] = 0.0F;
                paramArrayOfFloat[(paramInt + 6)] = 0.0F;
                paramArrayOfFloat[(paramInt + 9)] = 0.0F;
                paramArrayOfFloat[(paramInt + 5)] = 1.0F;
            }
            else if ((0.0F == paramFloat2) && (0.0F == paramFloat3) && (1.0F == paramFloat4))
            {
                paramArrayOfFloat[(paramInt + 0)] = f3;
                paramArrayOfFloat[(paramInt + 5)] = f3;
                paramArrayOfFloat[(paramInt + 1)] = f2;
                paramArrayOfFloat[(paramInt + 4)] = (-f2);
                paramArrayOfFloat[(paramInt + 2)] = 0.0F;
                paramArrayOfFloat[(paramInt + 6)] = 0.0F;
                paramArrayOfFloat[(paramInt + 8)] = 0.0F;
                paramArrayOfFloat[(paramInt + 9)] = 0.0F;
                paramArrayOfFloat[(paramInt + 10)] = 1.0F;
            }
            else
            {
                float f4 = length(paramFloat2, paramFloat3, paramFloat4);
                if (1.0F != f4)
                {
                    float f12 = 1.0F / f4;
                    paramFloat2 *= f12;
                    paramFloat3 *= f12;
                    paramFloat4 *= f12;
                }
                float f5 = 1.0F - f3;
                float f6 = paramFloat2 * paramFloat3;
                float f7 = paramFloat3 * paramFloat4;
                float f8 = paramFloat4 * paramFloat2;
                float f9 = paramFloat2 * f2;
                float f10 = paramFloat3 * f2;
                float f11 = paramFloat4 * f2;
                paramArrayOfFloat[(paramInt + 0)] = (f3 + f5 * (paramFloat2 * paramFloat2));
                paramArrayOfFloat[(paramInt + 4)] = (f6 * f5 - f11);
                paramArrayOfFloat[(paramInt + 8)] = (f10 + f8 * f5);
                paramArrayOfFloat[(paramInt + 1)] = (f11 + f6 * f5);
                paramArrayOfFloat[(paramInt + 5)] = (f3 + f5 * (paramFloat3 * paramFloat3));
                paramArrayOfFloat[(paramInt + 9)] = (f7 * f5 - f9);
                paramArrayOfFloat[(paramInt + 2)] = (f8 * f5 - f10);
                paramArrayOfFloat[(paramInt + 6)] = (f9 + f7 * f5);
                paramArrayOfFloat[(paramInt + 10)] = (f3 + f5 * (paramFloat4 * paramFloat4));
            }
        }
    }

    public static void translateM(float[] paramArrayOfFloat, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
    {
        for (int i = 0; i < 4; i++)
        {
            int j = paramInt + i;
            int k = j + 12;
            paramArrayOfFloat[k] += paramFloat1 * paramArrayOfFloat[j] + paramFloat2 * paramArrayOfFloat[(j + 4)] + paramFloat3 * paramArrayOfFloat[(j + 8)];
        }
    }

    public static void translateM(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3)
    {
        for (int i = 0; i < 12; i++)
            paramArrayOfFloat1[(paramInt1 + i)] = paramArrayOfFloat2[(paramInt2 + i)];
        for (int j = 0; j < 4; j++)
        {
            int k = paramInt1 + j;
            int m = paramInt2 + j;
            paramArrayOfFloat1[(k + 12)] = (paramFloat1 * paramArrayOfFloat2[m] + paramFloat2 * paramArrayOfFloat2[(m + 4)] + paramFloat3 * paramArrayOfFloat2[(m + 8)] + paramArrayOfFloat2[(m + 12)]);
        }
    }

    public static void transposeM(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2)
    {
        for (int i = 0; i < 4; i++)
        {
            int j = paramInt2 + i * 4;
            paramArrayOfFloat1[(i + paramInt1)] = paramArrayOfFloat2[j];
            paramArrayOfFloat1[(paramInt1 + (i + 4))] = paramArrayOfFloat2[(j + 1)];
            paramArrayOfFloat1[(paramInt1 + (i + 8))] = paramArrayOfFloat2[(j + 2)];
            paramArrayOfFloat1[(paramInt1 + (i + 12))] = paramArrayOfFloat2[(j + 3)];
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.Matrix
 * JD-Core Version:        0.6.2
 */