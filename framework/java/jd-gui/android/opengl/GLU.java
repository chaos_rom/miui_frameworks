package android.opengl;

import javax.microedition.khronos.opengles.GL10;

public class GLU
{
    private static final float[] sScratch = new float[32];

    public static String gluErrorString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = null;
        case 0:
        case 1280:
        case 1281:
        case 1282:
        case 1283:
        case 1284:
        case 1285:
        }
        while (true)
        {
            return str;
            str = "no error";
            continue;
            str = "invalid enum";
            continue;
            str = "invalid value";
            continue;
            str = "invalid operation";
            continue;
            str = "stack overflow";
            continue;
            str = "stack underflow";
            continue;
            str = "out of memory";
        }
    }

    public static void gluLookAt(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9)
    {
        synchronized (sScratch)
        {
            Matrix.setLookAtM(???, 0, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8, paramFloat9);
            paramGL10.glMultMatrixf(???, 0);
            return;
        }
    }

    public static void gluOrtho2D(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        paramGL10.glOrthof(paramFloat1, paramFloat2, paramFloat3, paramFloat4, -1.0F, 1.0F);
    }

    public static void gluPerspective(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        float f1 = paramFloat3 * (float)Math.tan(0.008726646259971648D * paramFloat1);
        float f2 = -f1;
        paramGL10.glFrustumf(f2 * paramFloat2, f1 * paramFloat2, f2, f1, paramFloat3, paramFloat4);
    }

    public static int gluProject(float paramFloat1, float paramFloat2, float paramFloat3, float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, int[] paramArrayOfInt, int paramInt3, float[] paramArrayOfFloat3, int paramInt4)
    {
        int i;
        synchronized (sScratch)
        {
            Matrix.multiplyMM(???, 0, paramArrayOfFloat2, paramInt2, paramArrayOfFloat1, paramInt1);
            ???[16] = paramFloat1;
            ???[17] = paramFloat2;
            ???[18] = paramFloat3;
            ???[19] = 1.0F;
            Matrix.multiplyMV(???, 20, ???, 0, ???, 16);
            float f1 = ???[23];
            if (f1 == 0.0F)
            {
                i = 0;
            }
            else
            {
                float f2 = 1.0F / f1;
                paramArrayOfFloat3[paramInt4] = (paramArrayOfInt[paramInt3] + 0.5F * (paramArrayOfInt[(paramInt3 + 2)] * (1.0F + f2 * ???[20])));
                paramArrayOfFloat3[(paramInt4 + 1)] = (paramArrayOfInt[(paramInt3 + 1)] + 0.5F * (paramArrayOfInt[(paramInt3 + 3)] * (1.0F + f2 * ???[21])));
                paramArrayOfFloat3[(paramInt4 + 2)] = (0.5F * (1.0F + f2 * ???[22]));
                i = 1;
            }
        }
        return i;
    }

    public static int gluUnProject(float paramFloat1, float paramFloat2, float paramFloat3, float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, int[] paramArrayOfInt, int paramInt3, float[] paramArrayOfFloat3, int paramInt4)
    {
        int i;
        synchronized (sScratch)
        {
            Matrix.multiplyMM(???, 0, paramArrayOfFloat2, paramInt2, paramArrayOfFloat1, paramInt1);
            if (!Matrix.invertM(???, 16, ???, 0))
            {
                i = 0;
            }
            else
            {
                ???[0] = (2.0F * (paramFloat1 - paramArrayOfInt[(paramInt3 + 0)]) / paramArrayOfInt[(paramInt3 + 2)] - 1.0F);
                ???[1] = (2.0F * (paramFloat2 - paramArrayOfInt[(paramInt3 + 1)]) / paramArrayOfInt[(paramInt3 + 3)] - 1.0F);
                ???[2] = (2.0F * paramFloat3 - 1.0F);
                ???[3] = 1.0F;
                Matrix.multiplyMV(paramArrayOfFloat3, paramInt4, ???, 16, ???, 0);
                i = 1;
            }
        }
        return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.GLU
 * JD-Core Version:        0.6.2
 */