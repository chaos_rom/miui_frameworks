package android.opengl;

import java.util.ArrayList;
import javax.microedition.khronos.egl.EGLContext;

public abstract class ManagedEGLContext
{
    static final String TAG = "ManagedEGLContext";
    static final ArrayList<ManagedEGLContext> sActive = new ArrayList();
    final EGLContext mContext;

    public ManagedEGLContext(EGLContext paramEGLContext)
    {
        this.mContext = paramEGLContext;
        synchronized (sActive)
        {
            sActive.add(this);
            return;
        }
    }

    // ERROR //
    public static boolean doTerminate()
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_0
        //     2: invokestatic 38	android/os/Looper:getMainLooper	()Landroid/os/Looper;
        //     5: invokestatic 41	android/os/Looper:myLooper	()Landroid/os/Looper;
        //     8: if_acmpeq +13 -> 21
        //     11: new 43	java/lang/IllegalStateException
        //     14: dup
        //     15: ldc 45
        //     17: invokespecial 48	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     20: athrow
        //     21: getstatic 22	android/opengl/ManagedEGLContext:sActive	Ljava/util/ArrayList;
        //     24: astore_1
        //     25: aload_1
        //     26: monitorenter
        //     27: getstatic 22	android/opengl/ManagedEGLContext:sActive	Ljava/util/ArrayList;
        //     30: invokevirtual 52	java/util/ArrayList:size	()I
        //     33: ifgt +8 -> 41
        //     36: aload_1
        //     37: monitorexit
        //     38: goto +157 -> 195
        //     41: invokestatic 58	javax/microedition/khronos/egl/EGLContext:getEGL	()Ljavax/microedition/khronos/egl/EGL;
        //     44: checkcast 60	javax/microedition/khronos/egl/EGL10
        //     47: getstatic 64	javax/microedition/khronos/egl/EGL10:EGL_DEFAULT_DISPLAY	Ljava/lang/Object;
        //     50: invokeinterface 68 2 0
        //     55: astore_3
        //     56: aload_3
        //     57: getstatic 72	javax/microedition/khronos/egl/EGL10:EGL_NO_DISPLAY	Ljavax/microedition/khronos/egl/EGLDisplay;
        //     60: if_acmpne +21 -> 81
        //     63: ldc 8
        //     65: ldc 74
        //     67: invokestatic 80	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     70: pop
        //     71: aload_1
        //     72: monitorexit
        //     73: goto +122 -> 195
        //     76: astore_2
        //     77: aload_1
        //     78: monitorexit
        //     79: aload_2
        //     80: athrow
        //     81: aload_3
        //     82: invokestatic 86	com/google/android/gles_jni/EGLImpl:getInitCount	(Ljavax/microedition/khronos/egl/EGLDisplay;)I
        //     85: getstatic 22	android/opengl/ManagedEGLContext:sActive	Ljava/util/ArrayList;
        //     88: invokevirtual 52	java/util/ArrayList:size	()I
        //     91: if_icmpeq +50 -> 141
        //     94: ldc 8
        //     96: new 88	java/lang/StringBuilder
        //     99: dup
        //     100: invokespecial 89	java/lang/StringBuilder:<init>	()V
        //     103: ldc 91
        //     105: invokevirtual 95	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     108: aload_3
        //     109: invokestatic 86	com/google/android/gles_jni/EGLImpl:getInitCount	(Ljavax/microedition/khronos/egl/EGLDisplay;)I
        //     112: invokevirtual 98	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     115: ldc 100
        //     117: invokevirtual 95	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     120: getstatic 22	android/opengl/ManagedEGLContext:sActive	Ljava/util/ArrayList;
        //     123: invokevirtual 52	java/util/ArrayList:size	()I
        //     126: invokevirtual 98	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     129: invokevirtual 104	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     132: invokestatic 80	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     135: pop
        //     136: aload_1
        //     137: monitorexit
        //     138: goto +57 -> 195
        //     141: new 17	java/util/ArrayList
        //     144: dup
        //     145: getstatic 22	android/opengl/ManagedEGLContext:sActive	Ljava/util/ArrayList;
        //     148: invokespecial 107	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     151: astore 4
        //     153: getstatic 22	android/opengl/ManagedEGLContext:sActive	Ljava/util/ArrayList;
        //     156: invokevirtual 110	java/util/ArrayList:clear	()V
        //     159: aload_1
        //     160: monitorexit
        //     161: iconst_0
        //     162: istore 5
        //     164: iload 5
        //     166: aload 4
        //     168: invokevirtual 52	java/util/ArrayList:size	()I
        //     171: if_icmpge +22 -> 193
        //     174: aload 4
        //     176: iload 5
        //     178: invokevirtual 114	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     181: checkcast 2	android/opengl/ManagedEGLContext
        //     184: invokevirtual 117	android/opengl/ManagedEGLContext:execTerminate	()V
        //     187: iinc 5 1
        //     190: goto -26 -> 164
        //     193: iconst_1
        //     194: istore_0
        //     195: iload_0
        //     196: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     27	79	76	finally
        //     81	161	76	finally
    }

    void execTerminate()
    {
        onTerminate(this.mContext);
    }

    public EGLContext getContext()
    {
        return this.mContext;
    }

    public abstract void onTerminate(EGLContext paramEGLContext);

    public void terminate()
    {
        execTerminate();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.ManagedEGLContext
 * JD-Core Version:        0.6.2
 */