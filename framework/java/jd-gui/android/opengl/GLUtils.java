package android.opengl;

import android.graphics.Bitmap;

public final class GLUtils
{
    static
    {
        nativeClassInit();
    }

    public static void enableTracing()
    {
        native_enableTracing();
    }

    public static String getEGLErrorString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "0x" + Integer.toHexString(paramInt);
        case 12288:
        case 12289:
        case 12290:
        case 12291:
        case 12292:
        case 12293:
        case 12294:
        case 12295:
        case 12296:
        case 12297:
        case 12298:
        case 12299:
        case 12300:
        case 12301:
        case 12302:
        }
        while (true)
        {
            return str;
            str = "EGL_SUCCESS";
            continue;
            str = "EGL_NOT_INITIALIZED";
            continue;
            str = "EGL_BAD_ACCESS";
            continue;
            str = "EGL_BAD_ALLOC";
            continue;
            str = "EGL_BAD_ATTRIBUTE";
            continue;
            str = "EGL_BAD_CONFIG";
            continue;
            str = "EGL_BAD_CONTEXT";
            continue;
            str = "EGL_BAD_CURRENT_SURFACE";
            continue;
            str = "EGL_BAD_DISPLAY";
            continue;
            str = "EGL_BAD_MATCH";
            continue;
            str = "EGL_BAD_NATIVE_PIXMAP";
            continue;
            str = "EGL_BAD_NATIVE_WINDOW";
            continue;
            str = "EGL_BAD_PARAMETER";
            continue;
            str = "EGL_BAD_SURFACE";
            continue;
            str = "EGL_CONTEXT_LOST";
        }
    }

    public static int getInternalFormat(Bitmap paramBitmap)
    {
        if (paramBitmap == null)
            throw new NullPointerException("getInternalFormat can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        int i = native_getInternalFormat(paramBitmap);
        if (i < 0)
            throw new IllegalArgumentException("Unknown internalformat");
        return i;
    }

    public static int getType(Bitmap paramBitmap)
    {
        if (paramBitmap == null)
            throw new NullPointerException("getType can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        int i = native_getType(paramBitmap);
        if (i < 0)
            throw new IllegalArgumentException("Unknown type");
        return i;
    }

    private static native void nativeClassInit();

    private static native void native_enableTracing();

    private static native int native_getInternalFormat(Bitmap paramBitmap);

    private static native int native_getType(Bitmap paramBitmap);

    private static native int native_texImage2D(int paramInt1, int paramInt2, int paramInt3, Bitmap paramBitmap, int paramInt4, int paramInt5);

    private static native int native_texSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Bitmap paramBitmap, int paramInt5, int paramInt6);

    public static void texImage2D(int paramInt1, int paramInt2, int paramInt3, Bitmap paramBitmap, int paramInt4)
    {
        if (paramBitmap == null)
            throw new NullPointerException("texImage2D can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        if (native_texImage2D(paramInt1, paramInt2, paramInt3, paramBitmap, -1, paramInt4) != 0)
            throw new IllegalArgumentException("invalid Bitmap format");
    }

    public static void texImage2D(int paramInt1, int paramInt2, int paramInt3, Bitmap paramBitmap, int paramInt4, int paramInt5)
    {
        if (paramBitmap == null)
            throw new NullPointerException("texImage2D can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        if (native_texImage2D(paramInt1, paramInt2, paramInt3, paramBitmap, paramInt4, paramInt5) != 0)
            throw new IllegalArgumentException("invalid Bitmap format");
    }

    public static void texImage2D(int paramInt1, int paramInt2, Bitmap paramBitmap, int paramInt3)
    {
        if (paramBitmap == null)
            throw new NullPointerException("texImage2D can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        if (native_texImage2D(paramInt1, paramInt2, -1, paramBitmap, -1, paramInt3) != 0)
            throw new IllegalArgumentException("invalid Bitmap format");
    }

    public static void texSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Bitmap paramBitmap)
    {
        if (paramBitmap == null)
            throw new NullPointerException("texSubImage2D can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        if (native_texSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramBitmap, -1, getType(paramBitmap)) != 0)
            throw new IllegalArgumentException("invalid Bitmap format");
    }

    public static void texSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Bitmap paramBitmap, int paramInt5, int paramInt6)
    {
        if (paramBitmap == null)
            throw new NullPointerException("texSubImage2D can't be used with a null Bitmap");
        if (paramBitmap.isRecycled())
            throw new IllegalArgumentException("bitmap is recycled");
        if (native_texSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramBitmap, paramInt5, paramInt6) != 0)
            throw new IllegalArgumentException("invalid Bitmap format");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.GLUtils
 * JD-Core Version:        0.6.2
 */