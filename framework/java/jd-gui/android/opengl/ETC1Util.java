package android.opengl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ETC1Util
{
    public static ETC1Texture compressTexture(Buffer paramBuffer, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocateDirect(ETC1.getEncodedDataSize(paramInt1, paramInt2)).order(ByteOrder.nativeOrder());
        ETC1.encodeImage(paramBuffer, paramInt1, paramInt2, paramInt3, paramInt4, localByteBuffer);
        return new ETC1Texture(paramInt1, paramInt2, localByteBuffer);
    }

    public static ETC1Texture createTexture(InputStream paramInputStream)
        throws IOException
    {
        byte[] arrayOfByte = new byte[4096];
        if (paramInputStream.read(arrayOfByte, 0, 16) != 16)
            throw new IOException("Unable to read PKM file header.");
        ByteBuffer localByteBuffer1 = ByteBuffer.allocateDirect(16).order(ByteOrder.nativeOrder());
        localByteBuffer1.put(arrayOfByte, 0, 16).position(0);
        if (!ETC1.isValid(localByteBuffer1))
            throw new IOException("Not a PKM file.");
        int i = ETC1.getWidth(localByteBuffer1);
        int j = ETC1.getHeight(localByteBuffer1);
        int k = ETC1.getEncodedDataSize(i, j);
        ByteBuffer localByteBuffer2 = ByteBuffer.allocateDirect(k).order(ByteOrder.nativeOrder());
        int m = 0;
        while (m < k)
        {
            int n = Math.min(arrayOfByte.length, k - m);
            if (paramInputStream.read(arrayOfByte, 0, n) != n)
                throw new IOException("Unable to read PKM file data.");
            localByteBuffer2.put(arrayOfByte, 0, n);
            m += n;
        }
        localByteBuffer2.position(0);
        return new ETC1Texture(i, j, localByteBuffer2);
    }

    public static boolean isETC1Supported()
    {
        boolean bool = false;
        int[] arrayOfInt = new int[20];
        GLES10.glGetIntegerv(34466, arrayOfInt, 0);
        int i = arrayOfInt[0];
        if (i > arrayOfInt.length)
            arrayOfInt = new int[i];
        GLES10.glGetIntegerv(34467, arrayOfInt, 0);
        for (int j = 0; ; j++)
            if (j < i)
            {
                if (arrayOfInt[j] == 36196)
                    bool = true;
            }
            else
                return bool;
    }

    public static void loadTexture(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, ETC1Texture paramETC1Texture)
    {
        if (paramInt4 != 6407)
            throw new IllegalArgumentException("fallbackFormat must be GL_RGB");
        if ((paramInt5 != 33635) && (paramInt5 != 5121))
            throw new IllegalArgumentException("Unsupported fallbackType");
        int i = paramETC1Texture.getWidth();
        int j = paramETC1Texture.getHeight();
        ByteBuffer localByteBuffer1 = paramETC1Texture.getData();
        if (isETC1Supported())
        {
            GLES10.glCompressedTexImage2D(paramInt1, paramInt2, 36196, i, j, paramInt3, localByteBuffer1.remaining(), localByteBuffer1);
            return;
        }
        int k;
        if (paramInt5 != 5121)
        {
            k = 1;
            label100: if (k == 0)
                break label171;
        }
        label171: for (int m = 2; ; m = 3)
        {
            int n = m * i;
            ByteBuffer localByteBuffer2 = ByteBuffer.allocateDirect(n * j).order(ByteOrder.nativeOrder());
            ETC1.decodeImage(localByteBuffer1, localByteBuffer2, i, j, m, n);
            GLES10.glTexImage2D(paramInt1, paramInt2, paramInt4, i, j, paramInt3, paramInt4, paramInt5, localByteBuffer2);
            break;
            k = 0;
            break label100;
        }
    }

    public static void loadTexture(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, InputStream paramInputStream)
        throws IOException
    {
        loadTexture(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, createTexture(paramInputStream));
    }

    public static void writeTexture(ETC1Texture paramETC1Texture, OutputStream paramOutputStream)
        throws IOException
    {
        ByteBuffer localByteBuffer1 = paramETC1Texture.getData();
        int i = localByteBuffer1.position();
        try
        {
            int j = paramETC1Texture.getWidth();
            int k = paramETC1Texture.getHeight();
            ByteBuffer localByteBuffer2 = ByteBuffer.allocateDirect(16).order(ByteOrder.nativeOrder());
            ETC1.formatHeader(localByteBuffer2, j, k);
            byte[] arrayOfByte = new byte[4096];
            localByteBuffer2.get(arrayOfByte, 0, 16);
            paramOutputStream.write(arrayOfByte, 0, 16);
            int m = ETC1.getEncodedDataSize(j, k);
            int n = 0;
            if (n < m)
            {
                int i1 = Math.min(arrayOfByte.length, m - n);
                localByteBuffer1.get(arrayOfByte, 0, i1);
                paramOutputStream.write(arrayOfByte, 0, i1);
                n += i1;
            }
        }
        finally
        {
            localByteBuffer1.position(i);
        }
    }

    public static class ETC1Texture
    {
        private ByteBuffer mData;
        private int mHeight;
        private int mWidth;

        public ETC1Texture(int paramInt1, int paramInt2, ByteBuffer paramByteBuffer)
        {
            this.mWidth = paramInt1;
            this.mHeight = paramInt2;
            this.mData = paramByteBuffer;
        }

        public ByteBuffer getData()
        {
            return this.mData;
        }

        public int getHeight()
        {
            return this.mHeight;
        }

        public int getWidth()
        {
            return this.mWidth;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.ETC1Util
 * JD-Core Version:        0.6.2
 */