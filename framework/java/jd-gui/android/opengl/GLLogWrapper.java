package android.opengl;

import java.io.IOException;
import java.io.Writer;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL10Ext;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;
import javax.microedition.khronos.opengles.GL11ExtensionPack;

class GLLogWrapper extends GLWrapperBase
{
    private static final int FORMAT_FIXED = 2;
    private static final int FORMAT_FLOAT = 1;
    private static final int FORMAT_INT;
    private int mArgCount;
    boolean mColorArrayEnabled;
    private PointerInfo mColorPointer = new PointerInfo();
    private Writer mLog;
    private boolean mLogArgumentNames;
    boolean mNormalArrayEnabled;
    private PointerInfo mNormalPointer = new PointerInfo();
    StringBuilder mStringBuilder;
    private PointerInfo mTexCoordPointer = new PointerInfo();
    boolean mTextureCoordArrayEnabled;
    boolean mVertexArrayEnabled;
    private PointerInfo mVertexPointer = new PointerInfo();

    public GLLogWrapper(GL paramGL, Writer paramWriter, boolean paramBoolean)
    {
        super(paramGL);
        this.mLog = paramWriter;
        this.mLogArgumentNames = paramBoolean;
    }

    private void arg(String paramString, float paramFloat)
    {
        arg(paramString, Float.toString(paramFloat));
    }

    private void arg(String paramString, int paramInt)
    {
        arg(paramString, Integer.toString(paramInt));
    }

    private void arg(String paramString, int paramInt, FloatBuffer paramFloatBuffer)
    {
        arg(paramString, toString(paramInt, paramFloatBuffer));
    }

    private void arg(String paramString, int paramInt, IntBuffer paramIntBuffer)
    {
        arg(paramString, toString(paramInt, 0, paramIntBuffer));
    }

    private void arg(String paramString, int paramInt, ShortBuffer paramShortBuffer)
    {
        arg(paramString, toString(paramInt, paramShortBuffer));
    }

    private void arg(String paramString, int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        arg(paramString, toString(paramInt1, paramArrayOfFloat, paramInt2));
    }

    private void arg(String paramString, int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        arg(paramString, toString(paramInt1, 0, paramArrayOfInt, paramInt2));
    }

    private void arg(String paramString, int paramInt1, short[] paramArrayOfShort, int paramInt2)
    {
        arg(paramString, toString(paramInt1, paramArrayOfShort, paramInt2));
    }

    private void arg(String paramString1, String paramString2)
    {
        int i = this.mArgCount;
        this.mArgCount = (i + 1);
        if (i > 0)
            log(", ");
        if (this.mLogArgumentNames)
            log(paramString1 + "=");
        log(paramString2);
    }

    private void arg(String paramString, boolean paramBoolean)
    {
        arg(paramString, Boolean.toString(paramBoolean));
    }

    private void argPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        arg("size", paramInt1);
        arg("type", getPointerTypeName(paramInt2));
        arg("stride", paramInt3);
        arg("pointer", paramBuffer.toString());
    }

    private void begin(String paramString)
    {
        log(paramString + '(');
        this.mArgCount = 0;
    }

    private void bindArrays()
    {
        if (this.mColorArrayEnabled)
            this.mColorPointer.bindByteBuffer();
        if (this.mNormalArrayEnabled)
            this.mNormalPointer.bindByteBuffer();
        if (this.mTextureCoordArrayEnabled)
            this.mTexCoordPointer.bindByteBuffer();
        if (this.mVertexArrayEnabled)
            this.mVertexPointer.bindByteBuffer();
    }

    private void checkError()
    {
        int i = this.mgl.glGetError();
        if (i != 0)
            logLine("glError: " + Integer.toString(i));
    }

    private void doArrayElement(StringBuilder paramStringBuilder, boolean paramBoolean, String paramString, PointerInfo paramPointerInfo, int paramInt)
    {
        if (!paramBoolean);
        while (true)
        {
            return;
            paramStringBuilder.append(" ");
            paramStringBuilder.append(paramString + ":{");
            if ((paramPointerInfo == null) || (paramPointerInfo.mTempByteBuffer == null))
            {
                paramStringBuilder.append("undefined }");
            }
            else if (paramPointerInfo.mStride < 0)
            {
                paramStringBuilder.append("invalid stride");
            }
            else
            {
                int i = paramPointerInfo.getStride();
                ByteBuffer localByteBuffer = paramPointerInfo.mTempByteBuffer;
                int j = paramPointerInfo.mSize;
                int k = paramPointerInfo.mType;
                int m = paramPointerInfo.sizeof(k);
                int n = i * paramInt;
                int i1 = 0;
                if (i1 < j)
                {
                    if (i1 > 0)
                        paramStringBuilder.append(", ");
                    switch (k)
                    {
                    default:
                        paramStringBuilder.append("?");
                    case 5120:
                    case 5121:
                    case 5122:
                    case 5132:
                    case 5126:
                    }
                    while (true)
                    {
                        n += m;
                        i1++;
                        break;
                        paramStringBuilder.append(Integer.toString(localByteBuffer.get(n)));
                        continue;
                        paramStringBuilder.append(Integer.toString(0xFF & localByteBuffer.get(n)));
                        continue;
                        paramStringBuilder.append(Integer.toString(localByteBuffer.asShortBuffer().get(n / 2)));
                        continue;
                        paramStringBuilder.append(Integer.toString(localByteBuffer.asIntBuffer().get(n / 4)));
                        continue;
                        paramStringBuilder.append(Float.toString(localByteBuffer.asFloatBuffer().get(n / 4)));
                    }
                }
                paramStringBuilder.append("}");
            }
        }
    }

    private void doElement(StringBuilder paramStringBuilder, int paramInt1, int paramInt2)
    {
        paramStringBuilder.append(" [" + paramInt1 + " : " + paramInt2 + "] =");
        doArrayElement(paramStringBuilder, this.mVertexArrayEnabled, "v", this.mVertexPointer, paramInt2);
        doArrayElement(paramStringBuilder, this.mNormalArrayEnabled, "n", this.mNormalPointer, paramInt2);
        doArrayElement(paramStringBuilder, this.mColorArrayEnabled, "c", this.mColorPointer, paramInt2);
        doArrayElement(paramStringBuilder, this.mTextureCoordArrayEnabled, "t", this.mTexCoordPointer, paramInt2);
        paramStringBuilder.append("\n");
    }

    private void end()
    {
        log(");\n");
        flush();
    }

    private void endLogIndices()
    {
        log(this.mStringBuilder.toString());
        unbindArrays();
    }

    private void flush()
    {
        try
        {
            this.mLog.flush();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                this.mLog = null;
        }
    }

    private void formattedAppend(StringBuilder paramStringBuilder, int paramInt1, int paramInt2)
    {
        switch (paramInt2)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            paramStringBuilder.append(paramInt1);
            continue;
            paramStringBuilder.append(Float.intBitsToFloat(paramInt1));
            continue;
            paramStringBuilder.append(paramInt1 / 65536.0F);
        }
    }

    private String getBeginMode(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return str;
            str = "GL_POINTS";
            continue;
            str = "GL_LINES";
            continue;
            str = "GL_LINE_LOOP";
            continue;
            str = "GL_LINE_STRIP";
            continue;
            str = "GL_TRIANGLES";
            continue;
            str = "GL_TRIANGLE_STRIP";
            continue;
            str = "GL_TRIANGLE_FAN";
        }
    }

    private String getCap(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 2912:
        case 2896:
        case 3553:
        case 2884:
        case 3008:
        case 3042:
        case 3058:
        case 3024:
        case 2960:
        case 2929:
        case 16384:
        case 16385:
        case 16386:
        case 16387:
        case 16388:
        case 16389:
        case 16390:
        case 16391:
        case 2832:
        case 2848:
        case 2903:
        case 2977:
        case 32826:
        case 32884:
        case 32885:
        case 32886:
        case 32888:
        case 32925:
        case 32926:
        case 32927:
        case 32928:
        case 3089:
        }
        while (true)
        {
            return str;
            str = "GL_FOG";
            continue;
            str = "GL_LIGHTING";
            continue;
            str = "GL_TEXTURE_2D";
            continue;
            str = "GL_CULL_FACE";
            continue;
            str = "GL_ALPHA_TEST";
            continue;
            str = "GL_BLEND";
            continue;
            str = "GL_COLOR_LOGIC_OP";
            continue;
            str = "GL_DITHER";
            continue;
            str = "GL_STENCIL_TEST";
            continue;
            str = "GL_DEPTH_TEST";
            continue;
            str = "GL_LIGHT0";
            continue;
            str = "GL_LIGHT1";
            continue;
            str = "GL_LIGHT2";
            continue;
            str = "GL_LIGHT3";
            continue;
            str = "GL_LIGHT4";
            continue;
            str = "GL_LIGHT5";
            continue;
            str = "GL_LIGHT6";
            continue;
            str = "GL_LIGHT7";
            continue;
            str = "GL_POINT_SMOOTH";
            continue;
            str = "GL_LINE_SMOOTH";
            continue;
            str = "GL_COLOR_MATERIAL";
            continue;
            str = "GL_NORMALIZE";
            continue;
            str = "GL_RESCALE_NORMAL";
            continue;
            str = "GL_VERTEX_ARRAY";
            continue;
            str = "GL_NORMAL_ARRAY";
            continue;
            str = "GL_COLOR_ARRAY";
            continue;
            str = "GL_TEXTURE_COORD_ARRAY";
            continue;
            str = "GL_MULTISAMPLE";
            continue;
            str = "GL_SAMPLE_ALPHA_TO_COVERAGE";
            continue;
            str = "GL_SAMPLE_ALPHA_TO_ONE";
            continue;
            str = "GL_SAMPLE_COVERAGE";
            continue;
            str = "GL_SCISSOR_TEST";
        }
    }

    private String getClearBufferMask(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if ((paramInt & 0x100) != 0)
        {
            localStringBuilder.append("GL_DEPTH_BUFFER_BIT");
            paramInt &= -257;
        }
        if ((paramInt & 0x400) != 0)
        {
            if (localStringBuilder.length() > 0)
                localStringBuilder.append(" | ");
            localStringBuilder.append("GL_STENCIL_BUFFER_BIT");
            paramInt &= -1025;
        }
        if ((paramInt & 0x4000) != 0)
        {
            if (localStringBuilder.length() > 0)
                localStringBuilder.append(" | ");
            localStringBuilder.append("GL_COLOR_BUFFER_BIT");
            paramInt &= -16385;
        }
        if (paramInt != 0)
        {
            if (localStringBuilder.length() > 0)
                localStringBuilder.append(" | ");
            localStringBuilder.append(getHex(paramInt));
        }
        return localStringBuilder.toString();
    }

    private String getClientState(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 32887:
        default:
            str = getHex(paramInt);
        case 32886:
        case 32884:
        case 32885:
        case 32888:
        }
        while (true)
        {
            return str;
            str = "GL_COLOR_ARRAY";
            continue;
            str = "GL_VERTEX_ARRAY";
            continue;
            str = "GL_NORMAL_ARRAY";
            continue;
            str = "GL_TEXTURE_COORD_ARRAY";
        }
    }

    public static String getErrorString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 0:
        case 1280:
        case 1281:
        case 1282:
        case 1283:
        case 1284:
        case 1285:
        }
        while (true)
        {
            return str;
            str = "GL_NO_ERROR";
            continue;
            str = "GL_INVALID_ENUM";
            continue;
            str = "GL_INVALID_VALUE";
            continue;
            str = "GL_INVALID_OPERATION";
            continue;
            str = "GL_STACK_OVERFLOW";
            continue;
            str = "GL_STACK_UNDERFLOW";
            continue;
            str = "GL_OUT_OF_MEMORY";
        }
    }

    private String getFaceName(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 1032:
        }
        for (String str = getHex(paramInt); ; str = "GL_FRONT_AND_BACK")
            return str;
    }

    private String getFactor(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 0:
        case 1:
        case 768:
        case 769:
        case 774:
        case 775:
        case 770:
        case 771:
        case 772:
        case 773:
        case 776:
        }
        while (true)
        {
            return str;
            str = "GL_ZERO";
            continue;
            str = "GL_ONE";
            continue;
            str = "GL_SRC_COLOR";
            continue;
            str = "GL_ONE_MINUS_SRC_COLOR";
            continue;
            str = "GL_DST_COLOR";
            continue;
            str = "GL_ONE_MINUS_DST_COLOR";
            continue;
            str = "GL_SRC_ALPHA";
            continue;
            str = "GL_ONE_MINUS_SRC_ALPHA";
            continue;
            str = "GL_DST_ALPHA";
            continue;
            str = "GL_ONE_MINUS_DST_ALPHA";
            continue;
            str = "GL_SRC_ALPHA_SATURATE";
        }
    }

    private String getFogPName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 2914:
        case 2915:
        case 2916:
        case 2917:
        case 2918:
        }
        while (true)
        {
            return str;
            str = "GL_FOG_DENSITY";
            continue;
            str = "GL_FOG_START";
            continue;
            str = "GL_FOG_END";
            continue;
            str = "GL_FOG_MODE";
            continue;
            str = "GL_FOG_COLOR";
        }
    }

    private int getFogParamCount(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
        case 2914:
        case 2915:
        case 2916:
        case 2917:
        case 2918:
        }
        for (i = 0; ; i = 4)
            return i;
    }

    private static String getHex(int paramInt)
    {
        return "0x" + Integer.toHexString(paramInt);
    }

    private String getHintMode(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 4353:
        case 4354:
        case 4352:
        }
        while (true)
        {
            return str;
            str = "GL_FASTEST";
            continue;
            str = "GL_NICEST";
            continue;
            str = "GL_DONT_CARE";
        }
    }

    private String getHintTarget(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 3156:
        case 3154:
        case 3152:
        case 3153:
        case 3155:
        case 33170:
        }
        while (true)
        {
            return str;
            str = "GL_FOG_HINT";
            continue;
            str = "GL_LINE_SMOOTH_HINT";
            continue;
            str = "GL_PERSPECTIVE_CORRECTION_HINT";
            continue;
            str = "GL_POINT_SMOOTH_HINT";
            continue;
            str = "GL_POLYGON_SMOOTH_HINT";
            continue;
            str = "GL_GENERATE_MIPMAP_HINT";
        }
    }

    private String getIndexType(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 5122:
        default:
            str = getHex(paramInt);
        case 5123:
        case 5121:
        }
        while (true)
        {
            return str;
            str = "GL_UNSIGNED_SHORT";
            continue;
            str = "GL_UNSIGNED_BYTE";
        }
    }

    private int getIntegerStateFormat(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 35213:
        case 35214:
        case 35215:
        }
        for (int i = 0; ; i = 1)
            return i;
    }

    private String getIntegerStateName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 3413:
        case 33902:
        case 33901:
        case 3412:
        case 34467:
        case 3414:
        case 3411:
        case 33001:
        case 33000:
        case 3377:
        case 3379:
        case 3386:
        case 3382:
        case 3384:
        case 3385:
        case 34018:
        case 34466:
        case 3410:
        case 2850:
        case 2834:
        case 3415:
        case 3408:
        case 35213:
        case 35214:
        case 35215:
        }
        while (true)
        {
            return str;
            str = "GL_ALPHA_BITS";
            continue;
            str = "GL_ALIASED_LINE_WIDTH_RANGE";
            continue;
            str = "GL_ALIASED_POINT_SIZE_RANGE";
            continue;
            str = "GL_BLUE_BITS";
            continue;
            str = "GL_COMPRESSED_TEXTURE_FORMATS";
            continue;
            str = "GL_DEPTH_BITS";
            continue;
            str = "GL_GREEN_BITS";
            continue;
            str = "GL_MAX_ELEMENTS_INDICES";
            continue;
            str = "GL_MAX_ELEMENTS_VERTICES";
            continue;
            str = "GL_MAX_LIGHTS";
            continue;
            str = "GL_MAX_TEXTURE_SIZE";
            continue;
            str = "GL_MAX_VIEWPORT_DIMS";
            continue;
            str = "GL_MAX_MODELVIEW_STACK_DEPTH";
            continue;
            str = "GL_MAX_PROJECTION_STACK_DEPTH";
            continue;
            str = "GL_MAX_TEXTURE_STACK_DEPTH";
            continue;
            str = "GL_MAX_TEXTURE_UNITS";
            continue;
            str = "GL_NUM_COMPRESSED_TEXTURE_FORMATS";
            continue;
            str = "GL_RED_BITS";
            continue;
            str = "GL_SMOOTH_LINE_WIDTH_RANGE";
            continue;
            str = "GL_SMOOTH_POINT_SIZE_RANGE";
            continue;
            str = "GL_STENCIL_BITS";
            continue;
            str = "GL_SUBPIXEL_BITS";
            continue;
            str = "GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES";
            continue;
            str = "GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES";
            continue;
            str = "GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES";
        }
    }

    private int getIntegerStateSize(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
            i = 0;
        case 3377:
        case 3379:
        case 3382:
        case 3384:
        case 3385:
        case 3408:
        case 3410:
        case 3411:
        case 3412:
        case 3413:
        case 3414:
        case 3415:
        case 33000:
        case 33001:
        case 34018:
        case 34466:
        case 33902:
        case 33901:
        case 34467:
        case 3386:
        case 2850:
        case 2834:
        case 35213:
        case 35214:
        case 35215:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 2;
            continue;
            int[] arrayOfInt = new int[i];
            this.mgl.glGetIntegerv(34466, arrayOfInt, 0);
            i = arrayOfInt[0];
            continue;
            i = 2;
            continue;
            i = 2;
            continue;
            i = 2;
            continue;
            i = 16;
        }
    }

    private String getLightModelPName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 2899:
        case 2898:
        }
        while (true)
        {
            return str;
            str = "GL_LIGHT_MODEL_AMBIENT";
            continue;
            str = "GL_LIGHT_MODEL_TWO_SIDE";
        }
    }

    private int getLightModelParamCount(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 0;
        case 2899:
        case 2898:
        }
        while (true)
        {
            return i;
            i = 4;
            continue;
            i = 1;
        }
    }

    private String getLightName(int paramInt)
    {
        if ((paramInt >= 16384) && (paramInt <= 16391));
        for (String str = "GL_LIGHT" + Integer.toString(paramInt); ; str = getHex(paramInt))
            return str;
    }

    private String getLightPName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 4608:
        case 4609:
        case 4610:
        case 4611:
        case 4612:
        case 4613:
        case 4614:
        case 4615:
        case 4616:
        case 4617:
        }
        while (true)
        {
            return str;
            str = "GL_AMBIENT";
            continue;
            str = "GL_DIFFUSE";
            continue;
            str = "GL_SPECULAR";
            continue;
            str = "GL_POSITION";
            continue;
            str = "GL_SPOT_DIRECTION";
            continue;
            str = "GL_SPOT_EXPONENT";
            continue;
            str = "GL_SPOT_CUTOFF";
            continue;
            str = "GL_CONSTANT_ATTENUATION";
            continue;
            str = "GL_LINEAR_ATTENUATION";
            continue;
            str = "GL_QUADRATIC_ATTENUATION";
        }
    }

    private int getLightParamCount(int paramInt)
    {
        int i = 4;
        switch (paramInt)
        {
        default:
            i = 0;
        case 4608:
        case 4609:
        case 4610:
        case 4611:
        case 4612:
        case 4613:
        case 4614:
        case 4615:
        case 4616:
        case 4617:
        }
        while (true)
        {
            return i;
            i = 3;
            continue;
            i = 1;
            continue;
            i = 1;
            continue;
            i = 1;
            continue;
            i = 1;
            continue;
            i = 1;
        }
    }

    private String getMaterialPName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 4608:
        case 4609:
        case 4610:
        case 5632:
        case 5633:
        case 5634:
        }
        while (true)
        {
            return str;
            str = "GL_AMBIENT";
            continue;
            str = "GL_DIFFUSE";
            continue;
            str = "GL_SPECULAR";
            continue;
            str = "GL_EMISSION";
            continue;
            str = "GL_SHININESS";
            continue;
            str = "GL_AMBIENT_AND_DIFFUSE";
        }
    }

    private int getMaterialParamCount(int paramInt)
    {
        int i = 4;
        switch (paramInt)
        {
        default:
        case 4608:
        case 4609:
        case 4610:
        case 5632:
        case 5634:
        case 5633:
        }
        for (i = 0; ; i = 1)
            return i;
    }

    private String getMatrixMode(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 5888:
        case 5889:
        case 5890:
        }
        while (true)
        {
            return str;
            str = "GL_MODELVIEW";
            continue;
            str = "GL_PROJECTION";
            continue;
            str = "GL_TEXTURE";
        }
    }

    private String getPointerTypeName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 5120:
        case 5121:
        case 5122:
        case 5132:
        case 5126:
        }
        while (true)
        {
            return str;
            str = "GL_BYTE";
            continue;
            str = "GL_UNSIGNED_BYTE";
            continue;
            str = "GL_SHORT";
            continue;
            str = "GL_FIXED";
            continue;
            str = "GL_FLOAT";
        }
    }

    private String getShadeModel(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 7424:
        case 7425:
        }
        while (true)
        {
            return str;
            str = "GL_FLAT";
            continue;
            str = "GL_SMOOTH";
        }
    }

    private String getTextureEnvPName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 8704:
        case 8705:
        }
        while (true)
        {
            return str;
            str = "GL_TEXTURE_ENV_MODE";
            continue;
            str = "GL_TEXTURE_ENV_COLOR";
        }
    }

    private int getTextureEnvParamCount(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 0;
        case 8704:
        case 8705:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 4;
        }
    }

    private String getTextureEnvParamName(float paramFloat)
    {
        int i = (int)paramFloat;
        String str;
        if (paramFloat == i)
            switch (i)
            {
            default:
                str = getHex(i);
            case 7681:
            case 8448:
            case 8449:
            case 3042:
            case 260:
            case 34160:
            }
        while (true)
        {
            return str;
            str = "GL_REPLACE";
            continue;
            str = "GL_MODULATE";
            continue;
            str = "GL_DECAL";
            continue;
            str = "GL_BLEND";
            continue;
            str = "GL_ADD";
            continue;
            str = "GL_COMBINE";
            continue;
            str = Float.toString(paramFloat);
        }
    }

    private String getTextureEnvTarget(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 8960:
        }
        for (String str = getHex(paramInt); ; str = "GL_TEXTURE_ENV")
            return str;
    }

    private String getTexturePName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getHex(paramInt);
        case 10240:
        case 10241:
        case 10242:
        case 10243:
        case 33169:
        case 35741:
        }
        while (true)
        {
            return str;
            str = "GL_TEXTURE_MAG_FILTER";
            continue;
            str = "GL_TEXTURE_MIN_FILTER";
            continue;
            str = "GL_TEXTURE_WRAP_S";
            continue;
            str = "GL_TEXTURE_WRAP_T";
            continue;
            str = "GL_GENERATE_MIPMAP";
            continue;
            str = "GL_TEXTURE_CROP_RECT_OES";
        }
    }

    private String getTextureParamName(float paramFloat)
    {
        int i = (int)paramFloat;
        String str;
        if (paramFloat == i)
            switch (i)
            {
            default:
                str = getHex(i);
            case 33071:
            case 10497:
            case 9728:
            case 9729:
            case 9984:
            case 9985:
            case 9986:
            case 9987:
            }
        while (true)
        {
            return str;
            str = "GL_CLAMP_TO_EDGE";
            continue;
            str = "GL_REPEAT";
            continue;
            str = "GL_NEAREST";
            continue;
            str = "GL_LINEAR";
            continue;
            str = "GL_NEAREST_MIPMAP_NEAREST";
            continue;
            str = "GL_LINEAR_MIPMAP_NEAREST";
            continue;
            str = "GL_NEAREST_MIPMAP_LINEAR";
            continue;
            str = "GL_LINEAR_MIPMAP_LINEAR";
            continue;
            str = Float.toString(paramFloat);
        }
    }

    private String getTextureTarget(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 3553:
        }
        for (String str = getHex(paramInt); ; str = "GL_TEXTURE_2D")
            return str;
    }

    private void log(String paramString)
    {
        try
        {
            this.mLog.write(paramString);
            label8: return;
        }
        catch (IOException localIOException)
        {
            break label8;
        }
    }

    private void logLine(String paramString)
    {
        log(paramString + '\n');
    }

    private void returns(int paramInt)
    {
        returns(Integer.toString(paramInt));
    }

    private void returns(String paramString)
    {
        log(") returns " + paramString + ";\n");
        flush();
    }

    private void startLogIndices()
    {
        this.mStringBuilder = new StringBuilder();
        this.mStringBuilder.append("\n");
        bindArrays();
    }

    private ByteBuffer toByteBuffer(int paramInt, Buffer paramBuffer)
    {
        int i;
        if (paramInt < 0)
            i = 1;
        ByteBuffer localByteBuffer1;
        while ((paramBuffer instanceof ByteBuffer))
        {
            ByteBuffer localByteBuffer2 = (ByteBuffer)paramBuffer;
            int i9 = localByteBuffer2.position();
            if (i != 0)
                paramInt = localByteBuffer2.limit() - i9;
            localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localByteBuffer2.order());
            int i10 = 0;
            while (true)
                if (i10 < paramInt)
                {
                    localByteBuffer1.put(localByteBuffer2.get());
                    i10++;
                    continue;
                    i = 0;
                    break;
                }
            localByteBuffer2.position(i9);
        }
        while (true)
        {
            localByteBuffer1.rewind();
            localByteBuffer1.order(ByteOrder.nativeOrder());
            return localByteBuffer1;
            if ((paramBuffer instanceof CharBuffer))
            {
                CharBuffer localCharBuffer1 = (CharBuffer)paramBuffer;
                int i7 = localCharBuffer1.position();
                if (i != 0)
                    paramInt = 2 * (localCharBuffer1.limit() - i7);
                localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localCharBuffer1.order());
                CharBuffer localCharBuffer2 = localByteBuffer1.asCharBuffer();
                for (int i8 = 0; i8 < paramInt / 2; i8++)
                    localCharBuffer2.put(localCharBuffer1.get());
                localCharBuffer1.position(i7);
            }
            else if ((paramBuffer instanceof ShortBuffer))
            {
                ShortBuffer localShortBuffer1 = (ShortBuffer)paramBuffer;
                int i5 = localShortBuffer1.position();
                if (i != 0)
                    paramInt = 2 * (localShortBuffer1.limit() - i5);
                localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localShortBuffer1.order());
                ShortBuffer localShortBuffer2 = localByteBuffer1.asShortBuffer();
                for (int i6 = 0; i6 < paramInt / 2; i6++)
                    localShortBuffer2.put(localShortBuffer1.get());
                localShortBuffer1.position(i5);
            }
            else if ((paramBuffer instanceof IntBuffer))
            {
                IntBuffer localIntBuffer1 = (IntBuffer)paramBuffer;
                int i3 = localIntBuffer1.position();
                if (i != 0)
                    paramInt = 4 * (localIntBuffer1.limit() - i3);
                localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localIntBuffer1.order());
                IntBuffer localIntBuffer2 = localByteBuffer1.asIntBuffer();
                for (int i4 = 0; i4 < paramInt / 4; i4++)
                    localIntBuffer2.put(localIntBuffer1.get());
                localIntBuffer1.position(i3);
            }
            else if ((paramBuffer instanceof FloatBuffer))
            {
                FloatBuffer localFloatBuffer1 = (FloatBuffer)paramBuffer;
                int i1 = localFloatBuffer1.position();
                if (i != 0)
                    paramInt = 4 * (localFloatBuffer1.limit() - i1);
                localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localFloatBuffer1.order());
                FloatBuffer localFloatBuffer2 = localByteBuffer1.asFloatBuffer();
                for (int i2 = 0; i2 < paramInt / 4; i2++)
                    localFloatBuffer2.put(localFloatBuffer1.get());
                localFloatBuffer1.position(i1);
            }
            else if ((paramBuffer instanceof DoubleBuffer))
            {
                DoubleBuffer localDoubleBuffer1 = (DoubleBuffer)paramBuffer;
                int m = localDoubleBuffer1.position();
                if (i != 0)
                    paramInt = 8 * (localDoubleBuffer1.limit() - m);
                localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localDoubleBuffer1.order());
                DoubleBuffer localDoubleBuffer2 = localByteBuffer1.asDoubleBuffer();
                for (int n = 0; n < paramInt / 8; n++)
                    localDoubleBuffer2.put(localDoubleBuffer1.get());
                localDoubleBuffer1.position(m);
            }
            else
            {
                if (!(paramBuffer instanceof LongBuffer))
                    break;
                LongBuffer localLongBuffer1 = (LongBuffer)paramBuffer;
                int j = localLongBuffer1.position();
                if (i != 0)
                    paramInt = 8 * (localLongBuffer1.limit() - j);
                localByteBuffer1 = ByteBuffer.allocate(paramInt).order(localLongBuffer1.order());
                LongBuffer localLongBuffer2 = localByteBuffer1.asLongBuffer();
                for (int k = 0; k < paramInt / 8; k++)
                    localLongBuffer2.put(localLongBuffer1.get());
                localLongBuffer1.position(j);
            }
        }
        throw new RuntimeException("Unimplemented Buffer subclass.");
    }

    private char[] toCharIndices(int paramInt1, int paramInt2, Buffer paramBuffer)
    {
        char[] arrayOfChar = new char[paramInt1];
        switch (paramInt2)
        {
        case 5122:
        default:
            return arrayOfChar;
        case 5121:
            ByteBuffer localByteBuffer = toByteBuffer(paramInt1, paramBuffer);
            byte[] arrayOfByte = localByteBuffer.array();
            int j = localByteBuffer.arrayOffset();
            for (int k = 0; k < paramInt1; k++)
                arrayOfChar[k] = ((char)(0xFF & arrayOfByte[(j + k)]));
        case 5123:
        }
        if ((paramBuffer instanceof CharBuffer));
        for (CharBuffer localCharBuffer = (CharBuffer)paramBuffer; ; localCharBuffer = toByteBuffer(paramInt1 * 2, paramBuffer).asCharBuffer())
        {
            int i = localCharBuffer.position();
            localCharBuffer.position(0);
            localCharBuffer.get(arrayOfChar);
            localCharBuffer.position(i);
            break;
        }
    }

    private String toString(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{\n");
        for (int i = 0; i < paramInt1; i++)
        {
            localStringBuilder.append(" [" + i + "] = ");
            formattedAppend(localStringBuilder, paramIntBuffer.get(i), paramInt2);
            localStringBuilder.append('\n');
        }
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    private String toString(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{\n");
        int i = paramArrayOfInt.length;
        int j = 0;
        if (j < paramInt1)
        {
            int k = paramInt3 + j;
            localStringBuilder.append(" [" + k + "] = ");
            if ((k < 0) || (k >= i))
                localStringBuilder.append("out of bounds");
            while (true)
            {
                localStringBuilder.append('\n');
                j++;
                break;
                formattedAppend(localStringBuilder, paramArrayOfInt[k], paramInt2);
            }
        }
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    private String toString(int paramInt, FloatBuffer paramFloatBuffer)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{\n");
        for (int i = 0; i < paramInt; i++)
            localStringBuilder.append(" [" + i + "] = " + paramFloatBuffer.get(i) + '\n');
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    private String toString(int paramInt, ShortBuffer paramShortBuffer)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{\n");
        for (int i = 0; i < paramInt; i++)
            localStringBuilder.append(" [" + i + "] = " + paramShortBuffer.get(i) + '\n');
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    private String toString(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{\n");
        int i = paramArrayOfFloat.length;
        int j = 0;
        if (j < paramInt1)
        {
            int k = paramInt2 + j;
            localStringBuilder.append("[" + k + "] = ");
            if ((k < 0) || (k >= i))
                localStringBuilder.append("out of bounds");
            while (true)
            {
                localStringBuilder.append('\n');
                j++;
                break;
                localStringBuilder.append(paramArrayOfFloat[k]);
            }
        }
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    private String toString(int paramInt1, short[] paramArrayOfShort, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{\n");
        int i = paramArrayOfShort.length;
        int j = 0;
        if (j < paramInt1)
        {
            int k = paramInt2 + j;
            localStringBuilder.append(" [" + k + "] = ");
            if ((k < 0) || (k >= i))
                localStringBuilder.append("out of bounds");
            while (true)
            {
                localStringBuilder.append('\n');
                j++;
                break;
                localStringBuilder.append(paramArrayOfShort[k]);
            }
        }
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    private void unbindArrays()
    {
        if (this.mColorArrayEnabled)
            this.mColorPointer.unbindByteBuffer();
        if (this.mNormalArrayEnabled)
            this.mNormalPointer.unbindByteBuffer();
        if (this.mTextureCoordArrayEnabled)
            this.mTexCoordPointer.unbindByteBuffer();
        if (this.mVertexArrayEnabled)
            this.mVertexPointer.unbindByteBuffer();
    }

    public void glActiveTexture(int paramInt)
    {
        begin("glActiveTexture");
        arg("texture", paramInt);
        end();
        this.mgl.glActiveTexture(paramInt);
        checkError();
    }

    public void glAlphaFunc(int paramInt, float paramFloat)
    {
        begin("glAlphaFunc");
        arg("func", paramInt);
        arg("ref", paramFloat);
        end();
        this.mgl.glAlphaFunc(paramInt, paramFloat);
        checkError();
    }

    public void glAlphaFuncx(int paramInt1, int paramInt2)
    {
        begin("glAlphaFuncx");
        arg("func", paramInt1);
        arg("ref", paramInt2);
        end();
        this.mgl.glAlphaFuncx(paramInt1, paramInt2);
        checkError();
    }

    public void glBindBuffer(int paramInt1, int paramInt2)
    {
        begin("glBindBuffer");
        arg("target", paramInt1);
        arg("buffer", paramInt2);
        end();
        this.mgl11.glBindBuffer(paramInt1, paramInt2);
        checkError();
    }

    public void glBindFramebufferOES(int paramInt1, int paramInt2)
    {
        begin("glBindFramebufferOES");
        arg("target", paramInt1);
        arg("framebuffer", paramInt2);
        end();
        this.mgl11ExtensionPack.glBindFramebufferOES(paramInt1, paramInt2);
        checkError();
    }

    public void glBindRenderbufferOES(int paramInt1, int paramInt2)
    {
        begin("glBindRenderbufferOES");
        arg("target", paramInt1);
        arg("renderbuffer", paramInt2);
        end();
        this.mgl11ExtensionPack.glBindRenderbufferOES(paramInt1, paramInt2);
        checkError();
    }

    public void glBindTexture(int paramInt1, int paramInt2)
    {
        begin("glBindTexture");
        arg("target", getTextureTarget(paramInt1));
        arg("texture", paramInt2);
        end();
        this.mgl.glBindTexture(paramInt1, paramInt2);
        checkError();
    }

    public void glBlendEquation(int paramInt)
    {
        begin("glBlendEquation");
        arg("mode", paramInt);
        end();
        this.mgl11ExtensionPack.glBlendEquation(paramInt);
        checkError();
    }

    public void glBlendEquationSeparate(int paramInt1, int paramInt2)
    {
        begin("glBlendEquationSeparate");
        arg("modeRGB", paramInt1);
        arg("modeAlpha", paramInt2);
        end();
        this.mgl11ExtensionPack.glBlendEquationSeparate(paramInt1, paramInt2);
        checkError();
    }

    public void glBlendFunc(int paramInt1, int paramInt2)
    {
        begin("glBlendFunc");
        arg("sfactor", getFactor(paramInt1));
        arg("dfactor", getFactor(paramInt2));
        end();
        this.mgl.glBlendFunc(paramInt1, paramInt2);
        checkError();
    }

    public void glBlendFuncSeparate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glBlendFuncSeparate");
        arg("srcRGB", paramInt1);
        arg("dstRGB", paramInt2);
        arg("srcAlpha", paramInt3);
        arg("dstAlpha", paramInt4);
        end();
        this.mgl11ExtensionPack.glBlendFuncSeparate(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glBufferData(int paramInt1, int paramInt2, Buffer paramBuffer, int paramInt3)
    {
        begin("glBufferData");
        arg("target", paramInt1);
        arg("size", paramInt2);
        arg("data", paramBuffer.toString());
        arg("usage", paramInt3);
        end();
        this.mgl11.glBufferData(paramInt1, paramInt2, paramBuffer, paramInt3);
        checkError();
    }

    public void glBufferSubData(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glBufferSubData");
        arg("target", paramInt1);
        arg("offset", paramInt2);
        arg("size", paramInt3);
        arg("data", paramBuffer.toString());
        end();
        this.mgl11.glBufferSubData(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    public int glCheckFramebufferStatusOES(int paramInt)
    {
        begin("glCheckFramebufferStatusOES");
        arg("target", paramInt);
        end();
        int i = this.mgl11ExtensionPack.glCheckFramebufferStatusOES(paramInt);
        checkError();
        return i;
    }

    public void glClear(int paramInt)
    {
        begin("glClear");
        arg("mask", getClearBufferMask(paramInt));
        end();
        this.mgl.glClear(paramInt);
        checkError();
    }

    public void glClearColor(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        begin("glClearColor");
        arg("red", paramFloat1);
        arg("green", paramFloat2);
        arg("blue", paramFloat3);
        arg("alpha", paramFloat4);
        end();
        this.mgl.glClearColor(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
        checkError();
    }

    public void glClearColorx(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glClearColor");
        arg("red", paramInt1);
        arg("green", paramInt2);
        arg("blue", paramInt3);
        arg("alpha", paramInt4);
        end();
        this.mgl.glClearColorx(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glClearDepthf(float paramFloat)
    {
        begin("glClearDepthf");
        arg("depth", paramFloat);
        end();
        this.mgl.glClearDepthf(paramFloat);
        checkError();
    }

    public void glClearDepthx(int paramInt)
    {
        begin("glClearDepthx");
        arg("depth", paramInt);
        end();
        this.mgl.glClearDepthx(paramInt);
        checkError();
    }

    public void glClearStencil(int paramInt)
    {
        begin("glClearStencil");
        arg("s", paramInt);
        end();
        this.mgl.glClearStencil(paramInt);
        checkError();
    }

    public void glClientActiveTexture(int paramInt)
    {
        begin("glClientActiveTexture");
        arg("texture", paramInt);
        end();
        this.mgl.glClientActiveTexture(paramInt);
        checkError();
    }

    public void glClipPlanef(int paramInt, FloatBuffer paramFloatBuffer)
    {
        begin("glClipPlanef");
        arg("plane", paramInt);
        arg("equation", 4, paramFloatBuffer);
        end();
        this.mgl11.glClipPlanef(paramInt, paramFloatBuffer);
        checkError();
    }

    public void glClipPlanef(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        begin("glClipPlanef");
        arg("plane", paramInt1);
        arg("equation", 4, paramArrayOfFloat, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl11.glClipPlanef(paramInt1, paramArrayOfFloat, paramInt2);
        checkError();
    }

    public void glClipPlanex(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glClipPlanef");
        arg("plane", paramInt);
        arg("equation", 4, paramIntBuffer);
        end();
        this.mgl11.glClipPlanex(paramInt, paramIntBuffer);
        checkError();
    }

    public void glClipPlanex(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glClipPlanex");
        arg("plane", paramInt1);
        arg("equation", 4, paramArrayOfInt, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl11.glClipPlanex(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glColor4f(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        begin("glColor4f");
        arg("red", paramFloat1);
        arg("green", paramFloat2);
        arg("blue", paramFloat3);
        arg("alpha", paramFloat4);
        end();
        this.mgl.glColor4f(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
        checkError();
    }

    public void glColor4ub(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4)
    {
        begin("glColor4ub");
        arg("red", paramByte1);
        arg("green", paramByte2);
        arg("blue", paramByte3);
        arg("alpha", paramByte4);
        end();
        this.mgl11.glColor4ub(paramByte1, paramByte2, paramByte3, paramByte4);
        checkError();
    }

    public void glColor4x(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glColor4x");
        arg("red", paramInt1);
        arg("green", paramInt2);
        arg("blue", paramInt3);
        arg("alpha", paramInt4);
        end();
        this.mgl.glColor4x(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glColorMask(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
    {
        begin("glColorMask");
        arg("red", paramBoolean1);
        arg("green", paramBoolean2);
        arg("blue", paramBoolean3);
        arg("alpha", paramBoolean4);
        end();
        this.mgl.glColorMask(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4);
        checkError();
    }

    public void glColorPointer(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glColorPointer");
        arg("size", paramInt1);
        arg("type", paramInt2);
        arg("stride", paramInt3);
        arg("offset", paramInt4);
        end();
        this.mgl11.glColorPointer(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glColorPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glColorPointer");
        argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        end();
        this.mColorPointer = new PointerInfo(paramInt1, paramInt2, paramInt3, paramBuffer);
        this.mgl.glColorPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    public void glCompressedTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Buffer paramBuffer)
    {
        begin("glCompressedTexImage2D");
        arg("target", getTextureTarget(paramInt1));
        arg("level", paramInt2);
        arg("internalformat", paramInt3);
        arg("width", paramInt4);
        arg("height", paramInt5);
        arg("border", paramInt6);
        arg("imageSize", paramInt7);
        arg("data", paramBuffer.toString());
        end();
        this.mgl.glCompressedTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramBuffer);
        checkError();
    }

    public void glCompressedTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer)
    {
        begin("glCompressedTexSubImage2D");
        arg("target", getTextureTarget(paramInt1));
        arg("level", paramInt2);
        arg("xoffset", paramInt3);
        arg("yoffset", paramInt4);
        arg("width", paramInt5);
        arg("height", paramInt6);
        arg("format", paramInt7);
        arg("imageSize", paramInt8);
        arg("data", paramBuffer.toString());
        end();
        this.mgl.glCompressedTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
        checkError();
    }

    public void glCopyTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        begin("glCopyTexImage2D");
        arg("target", getTextureTarget(paramInt1));
        arg("level", paramInt2);
        arg("internalformat", paramInt3);
        arg("x", paramInt4);
        arg("y", paramInt5);
        arg("width", paramInt6);
        arg("height", paramInt7);
        arg("border", paramInt8);
        end();
        this.mgl.glCopyTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
        checkError();
    }

    public void glCopyTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        begin("glCopyTexSubImage2D");
        arg("target", getTextureTarget(paramInt1));
        arg("level", paramInt2);
        arg("xoffset", paramInt3);
        arg("yoffset", paramInt4);
        arg("x", paramInt5);
        arg("y", paramInt6);
        arg("width", paramInt7);
        arg("height", paramInt8);
        end();
        this.mgl.glCopyTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
        checkError();
    }

    public void glCullFace(int paramInt)
    {
        begin("glCullFace");
        arg("mode", paramInt);
        end();
        this.mgl.glCullFace(paramInt);
        checkError();
    }

    public void glCurrentPaletteMatrixOES(int paramInt)
    {
        begin("glCurrentPaletteMatrixOES");
        arg("matrixpaletteindex", paramInt);
        end();
        this.mgl11Ext.glCurrentPaletteMatrixOES(paramInt);
        checkError();
    }

    public void glDeleteBuffers(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glDeleteBuffers");
        arg("n", paramInt);
        arg("buffers", paramIntBuffer.toString());
        end();
        this.mgl11.glDeleteBuffers(paramInt, paramIntBuffer);
        checkError();
    }

    public void glDeleteBuffers(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glDeleteBuffers");
        arg("n", paramInt1);
        arg("buffers", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glDeleteBuffers(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glDeleteFramebuffersOES(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glDeleteFramebuffersOES");
        arg("n", paramInt);
        arg("framebuffers", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glDeleteFramebuffersOES(paramInt, paramIntBuffer);
        checkError();
    }

    public void glDeleteFramebuffersOES(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glDeleteFramebuffersOES");
        arg("n", paramInt1);
        arg("framebuffers", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11ExtensionPack.glDeleteFramebuffersOES(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glDeleteRenderbuffersOES(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glDeleteRenderbuffersOES");
        arg("n", paramInt);
        arg("renderbuffers", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glDeleteRenderbuffersOES(paramInt, paramIntBuffer);
        checkError();
    }

    public void glDeleteRenderbuffersOES(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glDeleteRenderbuffersOES");
        arg("n", paramInt1);
        arg("renderbuffers", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11ExtensionPack.glDeleteRenderbuffersOES(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glDeleteTextures(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glDeleteTextures");
        arg("n", paramInt);
        arg("textures", paramInt, paramIntBuffer);
        end();
        this.mgl.glDeleteTextures(paramInt, paramIntBuffer);
        checkError();
    }

    public void glDeleteTextures(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glDeleteTextures");
        arg("n", paramInt1);
        arg("textures", paramInt1, paramArrayOfInt, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl.glDeleteTextures(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glDepthFunc(int paramInt)
    {
        begin("glDepthFunc");
        arg("func", paramInt);
        end();
        this.mgl.glDepthFunc(paramInt);
        checkError();
    }

    public void glDepthMask(boolean paramBoolean)
    {
        begin("glDepthMask");
        arg("flag", paramBoolean);
        end();
        this.mgl.glDepthMask(paramBoolean);
        checkError();
    }

    public void glDepthRangef(float paramFloat1, float paramFloat2)
    {
        begin("glDepthRangef");
        arg("near", paramFloat1);
        arg("far", paramFloat2);
        end();
        this.mgl.glDepthRangef(paramFloat1, paramFloat2);
        checkError();
    }

    public void glDepthRangex(int paramInt1, int paramInt2)
    {
        begin("glDepthRangex");
        arg("near", paramInt1);
        arg("far", paramInt2);
        end();
        this.mgl.glDepthRangex(paramInt1, paramInt2);
        checkError();
    }

    public void glDisable(int paramInt)
    {
        begin("glDisable");
        arg("cap", getCap(paramInt));
        end();
        this.mgl.glDisable(paramInt);
        checkError();
    }

    public void glDisableClientState(int paramInt)
    {
        begin("glDisableClientState");
        arg("array", getClientState(paramInt));
        end();
        switch (paramInt)
        {
        case 32887:
        default:
        case 32886:
        case 32885:
        case 32888:
        case 32884:
        }
        while (true)
        {
            this.mgl.glDisableClientState(paramInt);
            checkError();
            return;
            this.mColorArrayEnabled = false;
            continue;
            this.mNormalArrayEnabled = false;
            continue;
            this.mTextureCoordArrayEnabled = false;
            continue;
            this.mVertexArrayEnabled = false;
        }
    }

    public void glDrawArrays(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glDrawArrays");
        arg("mode", paramInt1);
        arg("first", paramInt2);
        arg("count", paramInt3);
        startLogIndices();
        for (int i = 0; i < paramInt3; i++)
            doElement(this.mStringBuilder, i, paramInt2 + i);
        endLogIndices();
        end();
        this.mgl.glDrawArrays(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glDrawElements(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glDrawElements");
        arg("mode", paramInt1);
        arg("count", paramInt2);
        arg("type", paramInt3);
        arg("offset", paramInt4);
        end();
        this.mgl11.glDrawElements(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glDrawElements(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glDrawElements");
        arg("mode", getBeginMode(paramInt1));
        arg("count", paramInt2);
        arg("type", getIndexType(paramInt3));
        char[] arrayOfChar = toCharIndices(paramInt2, paramInt3, paramBuffer);
        int i = arrayOfChar.length;
        startLogIndices();
        for (int j = 0; j < i; j++)
            doElement(this.mStringBuilder, j, arrayOfChar[j]);
        endLogIndices();
        end();
        this.mgl.glDrawElements(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    public void glDrawTexfOES(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
    {
        begin("glDrawTexfOES");
        arg("x", paramFloat1);
        arg("y", paramFloat2);
        arg("z", paramFloat3);
        arg("width", paramFloat4);
        arg("height", paramFloat5);
        end();
        this.mgl11Ext.glDrawTexfOES(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5);
        checkError();
    }

    public void glDrawTexfvOES(FloatBuffer paramFloatBuffer)
    {
        begin("glDrawTexfvOES");
        arg("coords", 5, paramFloatBuffer);
        end();
        this.mgl11Ext.glDrawTexfvOES(paramFloatBuffer);
        checkError();
    }

    public void glDrawTexfvOES(float[] paramArrayOfFloat, int paramInt)
    {
        begin("glDrawTexfvOES");
        arg("coords", 5, paramArrayOfFloat, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl11Ext.glDrawTexfvOES(paramArrayOfFloat, paramInt);
        checkError();
    }

    public void glDrawTexiOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        begin("glDrawTexiOES");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("z", paramInt3);
        arg("width", paramInt4);
        arg("height", paramInt5);
        end();
        this.mgl11Ext.glDrawTexiOES(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        checkError();
    }

    public void glDrawTexivOES(IntBuffer paramIntBuffer)
    {
        begin("glDrawTexivOES");
        arg("coords", 5, paramIntBuffer);
        end();
        this.mgl11Ext.glDrawTexivOES(paramIntBuffer);
        checkError();
    }

    public void glDrawTexivOES(int[] paramArrayOfInt, int paramInt)
    {
        begin("glDrawTexivOES");
        arg("coords", 5, paramArrayOfInt, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl11Ext.glDrawTexivOES(paramArrayOfInt, paramInt);
        checkError();
    }

    public void glDrawTexsOES(short paramShort1, short paramShort2, short paramShort3, short paramShort4, short paramShort5)
    {
        begin("glDrawTexsOES");
        arg("x", paramShort1);
        arg("y", paramShort2);
        arg("z", paramShort3);
        arg("width", paramShort4);
        arg("height", paramShort5);
        end();
        this.mgl11Ext.glDrawTexsOES(paramShort1, paramShort2, paramShort3, paramShort4, paramShort5);
        checkError();
    }

    public void glDrawTexsvOES(ShortBuffer paramShortBuffer)
    {
        begin("glDrawTexsvOES");
        arg("coords", 5, paramShortBuffer);
        end();
        this.mgl11Ext.glDrawTexsvOES(paramShortBuffer);
        checkError();
    }

    public void glDrawTexsvOES(short[] paramArrayOfShort, int paramInt)
    {
        begin("glDrawTexsvOES");
        arg("coords", 5, paramArrayOfShort, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl11Ext.glDrawTexsvOES(paramArrayOfShort, paramInt);
        checkError();
    }

    public void glDrawTexxOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        begin("glDrawTexxOES");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("z", paramInt3);
        arg("width", paramInt4);
        arg("height", paramInt5);
        end();
        this.mgl11Ext.glDrawTexxOES(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        checkError();
    }

    public void glDrawTexxvOES(IntBuffer paramIntBuffer)
    {
        begin("glDrawTexxvOES");
        arg("coords", 5, paramIntBuffer);
        end();
        this.mgl11Ext.glDrawTexxvOES(paramIntBuffer);
        checkError();
    }

    public void glDrawTexxvOES(int[] paramArrayOfInt, int paramInt)
    {
        begin("glDrawTexxvOES");
        arg("coords", 5, paramArrayOfInt, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl11Ext.glDrawTexxvOES(paramArrayOfInt, paramInt);
        checkError();
    }

    public void glEnable(int paramInt)
    {
        begin("glEnable");
        arg("cap", getCap(paramInt));
        end();
        this.mgl.glEnable(paramInt);
        checkError();
    }

    public void glEnableClientState(int paramInt)
    {
        begin("glEnableClientState");
        arg("array", getClientState(paramInt));
        end();
        switch (paramInt)
        {
        case 32887:
        default:
        case 32886:
        case 32885:
        case 32888:
        case 32884:
        }
        while (true)
        {
            this.mgl.glEnableClientState(paramInt);
            checkError();
            return;
            this.mColorArrayEnabled = true;
            continue;
            this.mNormalArrayEnabled = true;
            continue;
            this.mTextureCoordArrayEnabled = true;
            continue;
            this.mVertexArrayEnabled = true;
        }
    }

    public void glFinish()
    {
        begin("glFinish");
        end();
        this.mgl.glFinish();
        checkError();
    }

    public void glFlush()
    {
        begin("glFlush");
        end();
        this.mgl.glFlush();
        checkError();
    }

    public void glFogf(int paramInt, float paramFloat)
    {
        begin("glFogf");
        arg("pname", paramInt);
        arg("param", paramFloat);
        end();
        this.mgl.glFogf(paramInt, paramFloat);
        checkError();
    }

    public void glFogfv(int paramInt, FloatBuffer paramFloatBuffer)
    {
        begin("glFogfv");
        arg("pname", getFogPName(paramInt));
        arg("params", getFogParamCount(paramInt), paramFloatBuffer);
        end();
        this.mgl.glFogfv(paramInt, paramFloatBuffer);
        checkError();
    }

    public void glFogfv(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        begin("glFogfv");
        arg("pname", getFogPName(paramInt1));
        arg("params", getFogParamCount(paramInt1), paramArrayOfFloat, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl.glFogfv(paramInt1, paramArrayOfFloat, paramInt2);
        checkError();
    }

    public void glFogx(int paramInt1, int paramInt2)
    {
        begin("glFogx");
        arg("pname", getFogPName(paramInt1));
        arg("param", paramInt2);
        end();
        this.mgl.glFogx(paramInt1, paramInt2);
        checkError();
    }

    public void glFogxv(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glFogxv");
        arg("pname", getFogPName(paramInt));
        arg("params", getFogParamCount(paramInt), paramIntBuffer);
        end();
        this.mgl.glFogxv(paramInt, paramIntBuffer);
        checkError();
    }

    public void glFogxv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glFogxv");
        arg("pname", getFogPName(paramInt1));
        arg("params", getFogParamCount(paramInt1), paramArrayOfInt, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl.glFogxv(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glFramebufferRenderbufferOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glFramebufferRenderbufferOES");
        arg("target", paramInt1);
        arg("attachment", paramInt2);
        arg("renderbuffertarget", paramInt3);
        arg("renderbuffer", paramInt4);
        end();
        this.mgl11ExtensionPack.glFramebufferRenderbufferOES(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glFramebufferTexture2DOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        begin("glFramebufferTexture2DOES");
        arg("target", paramInt1);
        arg("attachment", paramInt2);
        arg("textarget", paramInt3);
        arg("texture", paramInt4);
        arg("level", paramInt5);
        end();
        this.mgl11ExtensionPack.glFramebufferTexture2DOES(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        checkError();
    }

    public void glFrontFace(int paramInt)
    {
        begin("glFrontFace");
        arg("mode", paramInt);
        end();
        this.mgl.glFrontFace(paramInt);
        checkError();
    }

    public void glFrustumf(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        begin("glFrustumf");
        arg("left", paramFloat1);
        arg("right", paramFloat2);
        arg("bottom", paramFloat3);
        arg("top", paramFloat4);
        arg("near", paramFloat5);
        arg("far", paramFloat6);
        end();
        this.mgl.glFrustumf(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
        checkError();
    }

    public void glFrustumx(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        begin("glFrustumx");
        arg("left", paramInt1);
        arg("right", paramInt2);
        arg("bottom", paramInt3);
        arg("top", paramInt4);
        arg("near", paramInt5);
        arg("far", paramInt6);
        end();
        this.mgl.glFrustumx(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
        checkError();
    }

    public void glGenBuffers(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGenBuffers");
        arg("n", paramInt);
        arg("buffers", paramIntBuffer.toString());
        end();
        this.mgl11.glGenBuffers(paramInt, paramIntBuffer);
        checkError();
    }

    public void glGenBuffers(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGenBuffers");
        arg("n", paramInt1);
        arg("buffers", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glGenBuffers(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glGenFramebuffersOES(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGenFramebuffersOES");
        arg("n", paramInt);
        arg("framebuffers", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGenFramebuffersOES(paramInt, paramIntBuffer);
        checkError();
    }

    public void glGenFramebuffersOES(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGenFramebuffersOES");
        arg("n", paramInt1);
        arg("framebuffers", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11ExtensionPack.glGenFramebuffersOES(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glGenRenderbuffersOES(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGenRenderbuffersOES");
        arg("n", paramInt);
        arg("renderbuffers", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGenRenderbuffersOES(paramInt, paramIntBuffer);
        checkError();
    }

    public void glGenRenderbuffersOES(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGenRenderbuffersOES");
        arg("n", paramInt1);
        arg("renderbuffers", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11ExtensionPack.glGenRenderbuffersOES(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glGenTextures(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGenTextures");
        arg("n", paramInt);
        arg("textures", paramIntBuffer.toString());
        this.mgl.glGenTextures(paramInt, paramIntBuffer);
        returns(toString(paramInt, 0, paramIntBuffer));
        checkError();
    }

    public void glGenTextures(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGenTextures");
        arg("n", paramInt1);
        arg("textures", Arrays.toString(paramArrayOfInt));
        arg("offset", paramInt2);
        this.mgl.glGenTextures(paramInt1, paramArrayOfInt, paramInt2);
        returns(toString(paramInt1, 0, paramArrayOfInt, paramInt2));
        checkError();
    }

    public void glGenerateMipmapOES(int paramInt)
    {
        begin("glGenerateMipmapOES");
        arg("target", paramInt);
        end();
        this.mgl11ExtensionPack.glGenerateMipmapOES(paramInt);
        checkError();
    }

    public void glGetBooleanv(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGetBooleanv");
        arg("pname", paramInt);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetBooleanv(paramInt, paramIntBuffer);
        checkError();
    }

    public void glGetBooleanv(int paramInt1, boolean[] paramArrayOfBoolean, int paramInt2)
    {
        begin("glGetBooleanv");
        arg("pname", paramInt1);
        arg("params", paramArrayOfBoolean.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glGetBooleanv(paramInt1, paramArrayOfBoolean, paramInt2);
        checkError();
    }

    public void glGetBufferParameteriv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetBufferParameteriv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetBufferParameteriv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetBufferParameteriv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetBufferParameteriv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetBufferParameteriv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetClipPlanef(int paramInt, FloatBuffer paramFloatBuffer)
    {
        begin("glGetClipPlanef");
        arg("pname", paramInt);
        arg("eqn", paramFloatBuffer.toString());
        end();
        this.mgl11.glGetClipPlanef(paramInt, paramFloatBuffer);
        checkError();
    }

    public void glGetClipPlanef(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        begin("glGetClipPlanef");
        arg("pname", paramInt1);
        arg("eqn", paramArrayOfFloat.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glGetClipPlanef(paramInt1, paramArrayOfFloat, paramInt2);
        checkError();
    }

    public void glGetClipPlanex(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGetClipPlanex");
        arg("pname", paramInt);
        arg("eqn", paramIntBuffer.toString());
        end();
        this.mgl11.glGetClipPlanex(paramInt, paramIntBuffer);
        checkError();
    }

    public void glGetClipPlanex(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGetClipPlanex");
        arg("pname", paramInt1);
        arg("eqn", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glGetClipPlanex(paramInt1, paramArrayOfInt, paramInt2);
    }

    public int glGetError()
    {
        begin("glGetError");
        int i = this.mgl.glGetError();
        returns(i);
        return i;
    }

    public void glGetFixedv(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGetFixedv");
        arg("pname", paramInt);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetFixedv(paramInt, paramIntBuffer);
        checkError();
    }

    public void glGetFixedv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGetFixedv");
        arg("pname", paramInt1);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glGetFixedv(paramInt1, paramArrayOfInt, paramInt2);
    }

    public void glGetFloatv(int paramInt, FloatBuffer paramFloatBuffer)
    {
        begin("glGetFloatv");
        arg("pname", paramInt);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11.glGetFloatv(paramInt, paramFloatBuffer);
        checkError();
    }

    public void glGetFloatv(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        begin("glGetFloatv");
        arg("pname", paramInt1);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glGetFloatv(paramInt1, paramArrayOfFloat, paramInt2);
    }

    public void glGetFramebufferAttachmentParameterivOES(int paramInt1, int paramInt2, int paramInt3, IntBuffer paramIntBuffer)
    {
        begin("glGetFramebufferAttachmentParameterivOES");
        arg("target", paramInt1);
        arg("attachment", paramInt2);
        arg("pname", paramInt3);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGetFramebufferAttachmentParameterivOES(paramInt1, paramInt2, paramInt3, paramIntBuffer);
        checkError();
    }

    public void glGetFramebufferAttachmentParameterivOES(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt, int paramInt4)
    {
        begin("glGetFramebufferAttachmentParameterivOES");
        arg("target", paramInt1);
        arg("attachment", paramInt2);
        arg("pname", paramInt3);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt4);
        end();
        this.mgl11ExtensionPack.glGetFramebufferAttachmentParameterivOES(paramInt1, paramInt2, paramInt3, paramArrayOfInt, paramInt4);
        checkError();
    }

    public void glGetIntegerv(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glGetIntegerv");
        arg("pname", getIntegerStateName(paramInt));
        arg("params", paramIntBuffer.toString());
        this.mgl.glGetIntegerv(paramInt, paramIntBuffer);
        returns(toString(getIntegerStateSize(paramInt), getIntegerStateFormat(paramInt), paramIntBuffer));
        checkError();
    }

    public void glGetIntegerv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glGetIntegerv");
        arg("pname", getIntegerStateName(paramInt1));
        arg("params", Arrays.toString(paramArrayOfInt));
        arg("offset", paramInt2);
        this.mgl.glGetIntegerv(paramInt1, paramArrayOfInt, paramInt2);
        returns(toString(getIntegerStateSize(paramInt1), getIntegerStateFormat(paramInt1), paramArrayOfInt, paramInt2));
        checkError();
    }

    public void glGetLightfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glGetLightfv");
        arg("light", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11.glGetLightfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glGetLightfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glGetLightfv");
        arg("light", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetLightfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glGetLightxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetLightxv");
        arg("light", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetLightxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetLightxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetLightxv");
        arg("light", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetLightxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetMaterialfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glGetMaterialfv");
        arg("face", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11.glGetMaterialfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glGetMaterialfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glGetMaterialfv");
        arg("face", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetMaterialfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glGetMaterialxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetMaterialxv");
        arg("face", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetMaterialxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetMaterialxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetMaterialxv");
        arg("face", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetMaterialxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetPointerv(int paramInt, Buffer[] paramArrayOfBuffer)
    {
        begin("glGetPointerv");
        arg("pname", paramInt);
        arg("params", paramArrayOfBuffer.toString());
        end();
        this.mgl11.glGetPointerv(paramInt, paramArrayOfBuffer);
        checkError();
    }

    public void glGetRenderbufferParameterivOES(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetRenderbufferParameterivOES");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGetRenderbufferParameterivOES(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetRenderbufferParameterivOES(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetRenderbufferParameterivOES");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glGetRenderbufferParameterivOES(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public String glGetString(int paramInt)
    {
        begin("glGetString");
        arg("name", paramInt);
        String str = this.mgl.glGetString(paramInt);
        returns(str);
        checkError();
        return str;
    }

    public void glGetTexEnviv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetTexEnviv");
        arg("env", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetTexEnviv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetTexEnviv");
        arg("env", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetTexEnvxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetTexEnviv");
        arg("env", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetTexEnvxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetTexEnvxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetTexEnviv");
        arg("env", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetTexGenfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glGetTexGenfv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGetTexGenfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glGetTexGenfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glGetTexGenfv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glGetTexGenfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glGetTexGeniv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetTexGeniv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGetTexGeniv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetTexGeniv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetTexGeniv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glGetTexGeniv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetTexGenxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetTexGenxv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glGetTexGenxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetTexGenxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetTexGenxv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glGetTexGenxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetTexParameterfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glGetTexParameterfv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11.glGetTexParameterfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glGetTexParameterfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glGetTexParameterfv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetTexParameterfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glGetTexParameteriv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetTexParameteriv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetTexParameteriv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetTexParameteriv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetTexParameteriv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glGetTexParameterxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glGetTexParameterxv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glGetTexParameterxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glGetTexParameterxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glGetTexParameterxv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glGetTexParameterxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glHint(int paramInt1, int paramInt2)
    {
        begin("glHint");
        arg("target", getHintTarget(paramInt1));
        arg("mode", getHintMode(paramInt2));
        end();
        this.mgl.glHint(paramInt1, paramInt2);
        checkError();
    }

    public boolean glIsBuffer(int paramInt)
    {
        begin("glIsBuffer");
        arg("buffer", paramInt);
        end();
        boolean bool = this.mgl11.glIsBuffer(paramInt);
        checkError();
        return bool;
    }

    public boolean glIsEnabled(int paramInt)
    {
        begin("glIsEnabled");
        arg("cap", paramInt);
        end();
        boolean bool = this.mgl11.glIsEnabled(paramInt);
        checkError();
        return bool;
    }

    public boolean glIsFramebufferOES(int paramInt)
    {
        begin("glIsFramebufferOES");
        arg("framebuffer", paramInt);
        end();
        boolean bool = this.mgl11ExtensionPack.glIsFramebufferOES(paramInt);
        checkError();
        return bool;
    }

    public boolean glIsRenderbufferOES(int paramInt)
    {
        begin("glIsRenderbufferOES");
        arg("renderbuffer", paramInt);
        end();
        this.mgl11ExtensionPack.glIsRenderbufferOES(paramInt);
        checkError();
        return false;
    }

    public boolean glIsTexture(int paramInt)
    {
        begin("glIsTexture");
        arg("texture", paramInt);
        end();
        boolean bool = this.mgl11.glIsTexture(paramInt);
        checkError();
        return bool;
    }

    public void glLightModelf(int paramInt, float paramFloat)
    {
        begin("glLightModelf");
        arg("pname", getLightModelPName(paramInt));
        arg("param", paramFloat);
        end();
        this.mgl.glLightModelf(paramInt, paramFloat);
        checkError();
    }

    public void glLightModelfv(int paramInt, FloatBuffer paramFloatBuffer)
    {
        begin("glLightModelfv");
        arg("pname", getLightModelPName(paramInt));
        arg("params", getLightModelParamCount(paramInt), paramFloatBuffer);
        end();
        this.mgl.glLightModelfv(paramInt, paramFloatBuffer);
        checkError();
    }

    public void glLightModelfv(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        begin("glLightModelfv");
        arg("pname", getLightModelPName(paramInt1));
        arg("params", getLightModelParamCount(paramInt1), paramArrayOfFloat, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl.glLightModelfv(paramInt1, paramArrayOfFloat, paramInt2);
        checkError();
    }

    public void glLightModelx(int paramInt1, int paramInt2)
    {
        begin("glLightModelx");
        arg("pname", getLightModelPName(paramInt1));
        arg("param", paramInt2);
        end();
        this.mgl.glLightModelx(paramInt1, paramInt2);
        checkError();
    }

    public void glLightModelxv(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glLightModelfv");
        arg("pname", getLightModelPName(paramInt));
        arg("params", getLightModelParamCount(paramInt), paramIntBuffer);
        end();
        this.mgl.glLightModelxv(paramInt, paramIntBuffer);
        checkError();
    }

    public void glLightModelxv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glLightModelxv");
        arg("pname", getLightModelPName(paramInt1));
        arg("params", getLightModelParamCount(paramInt1), paramArrayOfInt, paramInt2);
        arg("offset", paramInt2);
        end();
        this.mgl.glLightModelxv(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glLightf(int paramInt1, int paramInt2, float paramFloat)
    {
        begin("glLightf");
        arg("light", getLightName(paramInt1));
        arg("pname", getLightPName(paramInt2));
        arg("param", paramFloat);
        end();
        this.mgl.glLightf(paramInt1, paramInt2, paramFloat);
        checkError();
    }

    public void glLightfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glLightfv");
        arg("light", getLightName(paramInt1));
        arg("pname", getLightPName(paramInt2));
        arg("params", getLightParamCount(paramInt2), paramFloatBuffer);
        end();
        this.mgl.glLightfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glLightfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glLightfv");
        arg("light", getLightName(paramInt1));
        arg("pname", getLightPName(paramInt2));
        arg("params", getLightParamCount(paramInt2), paramArrayOfFloat, paramInt3);
        arg("offset", paramInt3);
        end();
        this.mgl.glLightfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glLightx(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glLightx");
        arg("light", getLightName(paramInt1));
        arg("pname", getLightPName(paramInt2));
        arg("param", paramInt3);
        end();
        this.mgl.glLightx(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glLightxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glLightxv");
        arg("light", getLightName(paramInt1));
        arg("pname", getLightPName(paramInt2));
        arg("params", getLightParamCount(paramInt2), paramIntBuffer);
        end();
        this.mgl.glLightxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glLightxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glLightxv");
        arg("light", getLightName(paramInt1));
        arg("pname", getLightPName(paramInt2));
        arg("params", getLightParamCount(paramInt2), paramArrayOfInt, paramInt3);
        arg("offset", paramInt3);
        end();
        this.mgl.glLightxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glLineWidth(float paramFloat)
    {
        begin("glLineWidth");
        arg("width", paramFloat);
        end();
        this.mgl.glLineWidth(paramFloat);
        checkError();
    }

    public void glLineWidthx(int paramInt)
    {
        begin("glLineWidthx");
        arg("width", paramInt);
        end();
        this.mgl.glLineWidthx(paramInt);
        checkError();
    }

    public void glLoadIdentity()
    {
        begin("glLoadIdentity");
        end();
        this.mgl.glLoadIdentity();
        checkError();
    }

    public void glLoadMatrixf(FloatBuffer paramFloatBuffer)
    {
        begin("glLoadMatrixf");
        arg("m", 16, paramFloatBuffer);
        end();
        this.mgl.glLoadMatrixf(paramFloatBuffer);
        checkError();
    }

    public void glLoadMatrixf(float[] paramArrayOfFloat, int paramInt)
    {
        begin("glLoadMatrixf");
        arg("m", 16, paramArrayOfFloat, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl.glLoadMatrixf(paramArrayOfFloat, paramInt);
        checkError();
    }

    public void glLoadMatrixx(IntBuffer paramIntBuffer)
    {
        begin("glLoadMatrixx");
        arg("m", 16, paramIntBuffer);
        end();
        this.mgl.glLoadMatrixx(paramIntBuffer);
        checkError();
    }

    public void glLoadMatrixx(int[] paramArrayOfInt, int paramInt)
    {
        begin("glLoadMatrixx");
        arg("m", 16, paramArrayOfInt, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl.glLoadMatrixx(paramArrayOfInt, paramInt);
        checkError();
    }

    public void glLoadPaletteFromModelViewMatrixOES()
    {
        begin("glLoadPaletteFromModelViewMatrixOES");
        end();
        this.mgl11Ext.glLoadPaletteFromModelViewMatrixOES();
        checkError();
    }

    public void glLogicOp(int paramInt)
    {
        begin("glLogicOp");
        arg("opcode", paramInt);
        end();
        this.mgl.glLogicOp(paramInt);
        checkError();
    }

    public void glMaterialf(int paramInt1, int paramInt2, float paramFloat)
    {
        begin("glMaterialf");
        arg("face", getFaceName(paramInt1));
        arg("pname", getMaterialPName(paramInt2));
        arg("param", paramFloat);
        end();
        this.mgl.glMaterialf(paramInt1, paramInt2, paramFloat);
        checkError();
    }

    public void glMaterialfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glMaterialfv");
        arg("face", getFaceName(paramInt1));
        arg("pname", getMaterialPName(paramInt2));
        arg("params", getMaterialParamCount(paramInt2), paramFloatBuffer);
        end();
        this.mgl.glMaterialfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glMaterialfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glMaterialfv");
        arg("face", getFaceName(paramInt1));
        arg("pname", getMaterialPName(paramInt2));
        arg("params", getMaterialParamCount(paramInt2), paramArrayOfFloat, paramInt3);
        arg("offset", paramInt3);
        end();
        this.mgl.glMaterialfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glMaterialx(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glMaterialx");
        arg("face", getFaceName(paramInt1));
        arg("pname", getMaterialPName(paramInt2));
        arg("param", paramInt3);
        end();
        this.mgl.glMaterialx(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glMaterialxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glMaterialxv");
        arg("face", getFaceName(paramInt1));
        arg("pname", getMaterialPName(paramInt2));
        arg("params", getMaterialParamCount(paramInt2), paramIntBuffer);
        end();
        this.mgl.glMaterialxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glMaterialxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glMaterialxv");
        arg("face", getFaceName(paramInt1));
        arg("pname", getMaterialPName(paramInt2));
        arg("params", getMaterialParamCount(paramInt2), paramArrayOfInt, paramInt3);
        arg("offset", paramInt3);
        end();
        this.mgl.glMaterialxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glMatrixIndexPointerOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glMatrixIndexPointerOES");
        arg("size", paramInt1);
        arg("type", paramInt2);
        arg("stride", paramInt3);
        arg("offset", paramInt4);
        end();
        this.mgl11Ext.glMatrixIndexPointerOES(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glMatrixIndexPointerOES(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glMatrixIndexPointerOES");
        argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        end();
        this.mgl11Ext.glMatrixIndexPointerOES(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    public void glMatrixMode(int paramInt)
    {
        begin("glMatrixMode");
        arg("mode", getMatrixMode(paramInt));
        end();
        this.mgl.glMatrixMode(paramInt);
        checkError();
    }

    public void glMultMatrixf(FloatBuffer paramFloatBuffer)
    {
        begin("glMultMatrixf");
        arg("m", 16, paramFloatBuffer);
        end();
        this.mgl.glMultMatrixf(paramFloatBuffer);
        checkError();
    }

    public void glMultMatrixf(float[] paramArrayOfFloat, int paramInt)
    {
        begin("glMultMatrixf");
        arg("m", 16, paramArrayOfFloat, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl.glMultMatrixf(paramArrayOfFloat, paramInt);
        checkError();
    }

    public void glMultMatrixx(IntBuffer paramIntBuffer)
    {
        begin("glMultMatrixx");
        arg("m", 16, paramIntBuffer);
        end();
        this.mgl.glMultMatrixx(paramIntBuffer);
        checkError();
    }

    public void glMultMatrixx(int[] paramArrayOfInt, int paramInt)
    {
        begin("glMultMatrixx");
        arg("m", 16, paramArrayOfInt, paramInt);
        arg("offset", paramInt);
        end();
        this.mgl.glMultMatrixx(paramArrayOfInt, paramInt);
        checkError();
    }

    public void glMultiTexCoord4f(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        begin("glMultiTexCoord4f");
        arg("target", paramInt);
        arg("s", paramFloat1);
        arg("t", paramFloat2);
        arg("r", paramFloat3);
        arg("q", paramFloat4);
        end();
        this.mgl.glMultiTexCoord4f(paramInt, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
        checkError();
    }

    public void glMultiTexCoord4x(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        begin("glMultiTexCoord4x");
        arg("target", paramInt1);
        arg("s", paramInt2);
        arg("t", paramInt3);
        arg("r", paramInt4);
        arg("q", paramInt5);
        end();
        this.mgl.glMultiTexCoord4x(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        checkError();
    }

    public void glNormal3f(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        begin("glNormal3f");
        arg("nx", paramFloat1);
        arg("ny", paramFloat2);
        arg("nz", paramFloat3);
        end();
        this.mgl.glNormal3f(paramFloat1, paramFloat2, paramFloat3);
        checkError();
    }

    public void glNormal3x(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glNormal3x");
        arg("nx", paramInt1);
        arg("ny", paramInt2);
        arg("nz", paramInt3);
        end();
        this.mgl.glNormal3x(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glNormalPointer(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glNormalPointer");
        arg("type", paramInt1);
        arg("stride", paramInt2);
        arg("offset", paramInt3);
        end();
        this.mgl11.glNormalPointer(paramInt1, paramInt2, paramInt3);
    }

    public void glNormalPointer(int paramInt1, int paramInt2, Buffer paramBuffer)
    {
        begin("glNormalPointer");
        arg("type", paramInt1);
        arg("stride", paramInt2);
        arg("pointer", paramBuffer.toString());
        end();
        this.mNormalPointer = new PointerInfo(3, paramInt1, paramInt2, paramBuffer);
        this.mgl.glNormalPointer(paramInt1, paramInt2, paramBuffer);
        checkError();
    }

    public void glOrthof(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        begin("glOrthof");
        arg("left", paramFloat1);
        arg("right", paramFloat2);
        arg("bottom", paramFloat3);
        arg("top", paramFloat4);
        arg("near", paramFloat5);
        arg("far", paramFloat6);
        end();
        this.mgl.glOrthof(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
        checkError();
    }

    public void glOrthox(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        begin("glOrthox");
        arg("left", paramInt1);
        arg("right", paramInt2);
        arg("bottom", paramInt3);
        arg("top", paramInt4);
        arg("near", paramInt5);
        arg("far", paramInt6);
        end();
        this.mgl.glOrthox(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
        checkError();
    }

    public void glPixelStorei(int paramInt1, int paramInt2)
    {
        begin("glPixelStorei");
        arg("pname", paramInt1);
        arg("param", paramInt2);
        end();
        this.mgl.glPixelStorei(paramInt1, paramInt2);
        checkError();
    }

    public void glPointParameterf(int paramInt, float paramFloat)
    {
        begin("glPointParameterf");
        arg("pname", paramInt);
        arg("param", paramFloat);
        end();
        this.mgl11.glPointParameterf(paramInt, paramFloat);
        checkError();
    }

    public void glPointParameterfv(int paramInt, FloatBuffer paramFloatBuffer)
    {
        begin("glPointParameterfv");
        arg("pname", paramInt);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11.glPointParameterfv(paramInt, paramFloatBuffer);
        checkError();
    }

    public void glPointParameterfv(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
    {
        begin("glPointParameterfv");
        arg("pname", paramInt1);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glPointParameterfv(paramInt1, paramArrayOfFloat, paramInt2);
        checkError();
    }

    public void glPointParameterx(int paramInt1, int paramInt2)
    {
        begin("glPointParameterfv");
        arg("pname", paramInt1);
        arg("param", paramInt2);
        end();
        this.mgl11.glPointParameterx(paramInt1, paramInt2);
        checkError();
    }

    public void glPointParameterxv(int paramInt, IntBuffer paramIntBuffer)
    {
        begin("glPointParameterxv");
        arg("pname", paramInt);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glPointParameterxv(paramInt, paramIntBuffer);
        checkError();
    }

    public void glPointParameterxv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        begin("glPointParameterxv");
        arg("pname", paramInt1);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt2);
        end();
        this.mgl11.glPointParameterxv(paramInt1, paramArrayOfInt, paramInt2);
        checkError();
    }

    public void glPointSize(float paramFloat)
    {
        begin("glPointSize");
        arg("size", paramFloat);
        end();
        this.mgl.glPointSize(paramFloat);
        checkError();
    }

    public void glPointSizePointerOES(int paramInt1, int paramInt2, Buffer paramBuffer)
    {
        begin("glPointSizePointerOES");
        arg("type", paramInt1);
        arg("stride", paramInt2);
        arg("params", paramBuffer.toString());
        end();
        this.mgl11.glPointSizePointerOES(paramInt1, paramInt2, paramBuffer);
        checkError();
    }

    public void glPointSizex(int paramInt)
    {
        begin("glPointSizex");
        arg("size", paramInt);
        end();
        this.mgl.glPointSizex(paramInt);
        checkError();
    }

    public void glPolygonOffset(float paramFloat1, float paramFloat2)
    {
        begin("glPolygonOffset");
        arg("factor", paramFloat1);
        arg("units", paramFloat2);
        end();
        this.mgl.glPolygonOffset(paramFloat1, paramFloat2);
        checkError();
    }

    public void glPolygonOffsetx(int paramInt1, int paramInt2)
    {
        begin("glPolygonOffsetx");
        arg("factor", paramInt1);
        arg("units", paramInt2);
        end();
        this.mgl.glPolygonOffsetx(paramInt1, paramInt2);
        checkError();
    }

    public void glPopMatrix()
    {
        begin("glPopMatrix");
        end();
        this.mgl.glPopMatrix();
        checkError();
    }

    public void glPushMatrix()
    {
        begin("glPushMatrix");
        end();
        this.mgl.glPushMatrix();
        checkError();
    }

    public int glQueryMatrixxOES(IntBuffer paramIntBuffer1, IntBuffer paramIntBuffer2)
    {
        begin("glQueryMatrixxOES");
        arg("mantissa", paramIntBuffer1.toString());
        arg("exponent", paramIntBuffer2.toString());
        end();
        int i = this.mgl10Ext.glQueryMatrixxOES(paramIntBuffer1, paramIntBuffer2);
        returns(toString(16, 2, paramIntBuffer1));
        returns(toString(16, 0, paramIntBuffer2));
        checkError();
        return i;
    }

    public int glQueryMatrixxOES(int[] paramArrayOfInt1, int paramInt1, int[] paramArrayOfInt2, int paramInt2)
    {
        begin("glQueryMatrixxOES");
        arg("mantissa", Arrays.toString(paramArrayOfInt1));
        arg("exponent", Arrays.toString(paramArrayOfInt2));
        end();
        int i = this.mgl10Ext.glQueryMatrixxOES(paramArrayOfInt1, paramInt1, paramArrayOfInt2, paramInt2);
        returns(toString(16, 2, paramArrayOfInt1, paramInt1));
        returns(toString(16, 0, paramArrayOfInt2, paramInt2));
        checkError();
        return i;
    }

    public void glReadPixels(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Buffer paramBuffer)
    {
        begin("glReadPixels");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("width", paramInt3);
        arg("height", paramInt4);
        arg("format", paramInt5);
        arg("type", paramInt6);
        arg("pixels", paramBuffer.toString());
        end();
        this.mgl.glReadPixels(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBuffer);
        checkError();
    }

    public void glRenderbufferStorageOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glRenderbufferStorageOES");
        arg("target", paramInt1);
        arg("internalformat", paramInt2);
        arg("width", paramInt3);
        arg("height", paramInt4);
        end();
        this.mgl11ExtensionPack.glRenderbufferStorageOES(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glRotatef(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        begin("glRotatef");
        arg("angle", paramFloat1);
        arg("x", paramFloat2);
        arg("y", paramFloat3);
        arg("z", paramFloat4);
        end();
        this.mgl.glRotatef(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
        checkError();
    }

    public void glRotatex(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glRotatex");
        arg("angle", paramInt1);
        arg("x", paramInt2);
        arg("y", paramInt3);
        arg("z", paramInt4);
        end();
        this.mgl.glRotatex(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glSampleCoverage(float paramFloat, boolean paramBoolean)
    {
        begin("glSampleCoveragex");
        arg("value", paramFloat);
        arg("invert", paramBoolean);
        end();
        this.mgl.glSampleCoverage(paramFloat, paramBoolean);
        checkError();
    }

    public void glSampleCoveragex(int paramInt, boolean paramBoolean)
    {
        begin("glSampleCoveragex");
        arg("value", paramInt);
        arg("invert", paramBoolean);
        end();
        this.mgl.glSampleCoveragex(paramInt, paramBoolean);
        checkError();
    }

    public void glScalef(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        begin("glScalef");
        arg("x", paramFloat1);
        arg("y", paramFloat2);
        arg("z", paramFloat3);
        end();
        this.mgl.glScalef(paramFloat1, paramFloat2, paramFloat3);
        checkError();
    }

    public void glScalex(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glScalex");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("z", paramInt3);
        end();
        this.mgl.glScalex(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glScissor(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glScissor");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("width", paramInt3);
        arg("height", paramInt4);
        end();
        this.mgl.glScissor(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glShadeModel(int paramInt)
    {
        begin("glShadeModel");
        arg("mode", getShadeModel(paramInt));
        end();
        this.mgl.glShadeModel(paramInt);
        checkError();
    }

    public void glStencilFunc(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glStencilFunc");
        arg("func", paramInt1);
        arg("ref", paramInt2);
        arg("mask", paramInt3);
        end();
        this.mgl.glStencilFunc(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glStencilMask(int paramInt)
    {
        begin("glStencilMask");
        arg("mask", paramInt);
        end();
        this.mgl.glStencilMask(paramInt);
        checkError();
    }

    public void glStencilOp(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glStencilOp");
        arg("fail", paramInt1);
        arg("zfail", paramInt2);
        arg("zpass", paramInt3);
        end();
        this.mgl.glStencilOp(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexCoordPointer(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glTexCoordPointer");
        arg("size", paramInt1);
        arg("type", paramInt2);
        arg("stride", paramInt3);
        arg("offset", paramInt4);
        end();
        this.mgl11.glTexCoordPointer(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void glTexCoordPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glTexCoordPointer");
        argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        end();
        this.mTexCoordPointer = new PointerInfo(paramInt1, paramInt2, paramInt3, paramBuffer);
        this.mgl.glTexCoordPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    public void glTexEnvf(int paramInt1, int paramInt2, float paramFloat)
    {
        begin("glTexEnvf");
        arg("target", getTextureEnvTarget(paramInt1));
        arg("pname", getTextureEnvPName(paramInt2));
        arg("param", getTextureEnvParamName(paramFloat));
        end();
        this.mgl.glTexEnvf(paramInt1, paramInt2, paramFloat);
        checkError();
    }

    public void glTexEnvfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glTexEnvfv");
        arg("target", getTextureEnvTarget(paramInt1));
        arg("pname", getTextureEnvPName(paramInt2));
        arg("params", getTextureEnvParamCount(paramInt2), paramFloatBuffer);
        end();
        this.mgl.glTexEnvfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glTexEnvfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glTexEnvfv");
        arg("target", getTextureEnvTarget(paramInt1));
        arg("pname", getTextureEnvPName(paramInt2));
        arg("params", getTextureEnvParamCount(paramInt2), paramArrayOfFloat, paramInt3);
        arg("offset", paramInt3);
        end();
        this.mgl.glTexEnvfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glTexEnvi(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTexEnvi");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("param", paramInt3);
        end();
        this.mgl11.glTexEnvi(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexEnviv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glTexEnviv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glTexEnviv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glTexEnviv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glTexEnviv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glTexEnviv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glTexEnvx(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTexEnvx");
        arg("target", getTextureEnvTarget(paramInt1));
        arg("pname", getTextureEnvPName(paramInt2));
        arg("param", paramInt3);
        end();
        this.mgl.glTexEnvx(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexEnvxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glTexEnvxv");
        arg("target", getTextureEnvTarget(paramInt1));
        arg("pname", getTextureEnvPName(paramInt2));
        arg("params", getTextureEnvParamCount(paramInt2), paramIntBuffer);
        end();
        this.mgl.glTexEnvxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glTexEnvxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glTexEnvxv");
        arg("target", getTextureEnvTarget(paramInt1));
        arg("pname", getTextureEnvPName(paramInt2));
        arg("params", getTextureEnvParamCount(paramInt2), paramArrayOfInt, paramInt3);
        arg("offset", paramInt3);
        end();
        this.mgl.glTexEnvxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glTexGenf(int paramInt1, int paramInt2, float paramFloat)
    {
        begin("glTexGenf");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("param", paramFloat);
        end();
        this.mgl11ExtensionPack.glTexGenf(paramInt1, paramInt2, paramFloat);
        checkError();
    }

    public void glTexGenfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glTexGenfv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11ExtensionPack.glTexGenfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glTexGenfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glTexGenfv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glTexGenfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glTexGeni(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTexGeni");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("param", paramInt3);
        end();
        this.mgl11ExtensionPack.glTexGeni(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexGeniv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glTexGeniv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glTexGeniv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glTexGeniv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glTexGeniv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glTexGeniv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glTexGenx(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTexGenx");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("param", paramInt3);
        end();
        this.mgl11ExtensionPack.glTexGenx(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexGenxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glTexGenxv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11ExtensionPack.glTexGenxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glTexGenxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glTexGenxv");
        arg("coord", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11ExtensionPack.glTexGenxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer)
    {
        begin("glTexImage2D");
        arg("target", paramInt1);
        arg("level", paramInt2);
        arg("internalformat", paramInt3);
        arg("width", paramInt4);
        arg("height", paramInt5);
        arg("border", paramInt6);
        arg("format", paramInt7);
        arg("type", paramInt8);
        arg("pixels", paramBuffer.toString());
        end();
        this.mgl.glTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
        checkError();
    }

    public void glTexParameterf(int paramInt1, int paramInt2, float paramFloat)
    {
        begin("glTexParameterf");
        arg("target", getTextureTarget(paramInt1));
        arg("pname", getTexturePName(paramInt2));
        arg("param", getTextureParamName(paramFloat));
        end();
        this.mgl.glTexParameterf(paramInt1, paramInt2, paramFloat);
        checkError();
    }

    public void glTexParameterfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
    {
        begin("glTexParameterfv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramFloatBuffer.toString());
        end();
        this.mgl11.glTexParameterfv(paramInt1, paramInt2, paramFloatBuffer);
        checkError();
    }

    public void glTexParameterfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
    {
        begin("glTexParameterfv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfFloat.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glTexParameterfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
        checkError();
    }

    public void glTexParameteri(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTexParameterxv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("param", paramInt3);
        end();
        this.mgl11.glTexParameteri(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexParameteriv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glTexParameteriv");
        arg("target", getTextureTarget(paramInt1));
        arg("pname", getTexturePName(paramInt2));
        arg("params", 4, paramIntBuffer);
        end();
        this.mgl11.glTexParameteriv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glTexParameteriv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glTexParameteriv");
        arg("target", getTextureTarget(paramInt1));
        arg("pname", getTexturePName(paramInt2));
        arg("params", 4, paramArrayOfInt, paramInt3);
        end();
        this.mgl11.glTexParameteriv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glTexParameterx(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTexParameterx");
        arg("target", getTextureTarget(paramInt1));
        arg("pname", getTexturePName(paramInt2));
        arg("param", paramInt3);
        end();
        this.mgl.glTexParameterx(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glTexParameterxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
    {
        begin("glTexParameterxv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramIntBuffer.toString());
        end();
        this.mgl11.glTexParameterxv(paramInt1, paramInt2, paramIntBuffer);
        checkError();
    }

    public void glTexParameterxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        begin("glTexParameterxv");
        arg("target", paramInt1);
        arg("pname", paramInt2);
        arg("params", paramArrayOfInt.toString());
        arg("offset", paramInt3);
        end();
        this.mgl11.glTexParameterxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        checkError();
    }

    public void glTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer)
    {
        begin("glTexSubImage2D");
        arg("target", getTextureTarget(paramInt1));
        arg("level", paramInt2);
        arg("xoffset", paramInt3);
        arg("yoffset", paramInt4);
        arg("width", paramInt5);
        arg("height", paramInt6);
        arg("format", paramInt7);
        arg("type", paramInt8);
        arg("pixels", paramBuffer.toString());
        end();
        this.mgl.glTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
        checkError();
    }

    public void glTranslatef(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        begin("glTranslatef");
        arg("x", paramFloat1);
        arg("y", paramFloat2);
        arg("z", paramFloat3);
        end();
        this.mgl.glTranslatef(paramFloat1, paramFloat2, paramFloat3);
        checkError();
    }

    public void glTranslatex(int paramInt1, int paramInt2, int paramInt3)
    {
        begin("glTranslatex");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("z", paramInt3);
        end();
        this.mgl.glTranslatex(paramInt1, paramInt2, paramInt3);
        checkError();
    }

    public void glVertexPointer(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glVertexPointer");
        arg("size", paramInt1);
        arg("type", paramInt2);
        arg("stride", paramInt3);
        arg("offset", paramInt4);
        end();
        this.mgl11.glVertexPointer(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void glVertexPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glVertexPointer");
        argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        end();
        this.mVertexPointer = new PointerInfo(paramInt1, paramInt2, paramInt3, paramBuffer);
        this.mgl.glVertexPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    public void glViewport(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glViewport");
        arg("x", paramInt1);
        arg("y", paramInt2);
        arg("width", paramInt3);
        arg("height", paramInt4);
        end();
        this.mgl.glViewport(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glWeightPointerOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        begin("glWeightPointerOES");
        arg("size", paramInt1);
        arg("type", paramInt2);
        arg("stride", paramInt3);
        arg("offset", paramInt4);
        end();
        this.mgl11Ext.glWeightPointerOES(paramInt1, paramInt2, paramInt3, paramInt4);
        checkError();
    }

    public void glWeightPointerOES(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
    {
        begin("glWeightPointerOES");
        argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
        end();
        this.mgl11Ext.glWeightPointerOES(paramInt1, paramInt2, paramInt3, paramBuffer);
        checkError();
    }

    private class PointerInfo
    {
        public Buffer mPointer;
        public int mSize;
        public int mStride;
        public ByteBuffer mTempByteBuffer;
        public int mType;

        public PointerInfo()
        {
        }

        public PointerInfo(int paramInt1, int paramInt2, int paramBuffer, Buffer arg5)
        {
            this.mSize = paramInt1;
            this.mType = paramInt2;
            this.mStride = paramBuffer;
            Object localObject;
            this.mPointer = localObject;
        }

        public void bindByteBuffer()
        {
            if (this.mPointer == null);
            for (ByteBuffer localByteBuffer = null; ; localByteBuffer = GLLogWrapper.this.toByteBuffer(-1, this.mPointer))
            {
                this.mTempByteBuffer = localByteBuffer;
                return;
            }
        }

        public int getStride()
        {
            if (this.mStride > 0);
            for (int i = this.mStride; ; i = sizeof(this.mType) * this.mSize)
                return i;
        }

        public int sizeof(int paramInt)
        {
            int i = 1;
            switch (paramInt)
            {
            default:
                i = 0;
            case 5120:
            case 5121:
            case 5122:
            case 5132:
            case 5126:
            }
            while (true)
            {
                return i;
                i = 2;
                continue;
                i = 4;
                continue;
                i = 4;
            }
        }

        public void unbindByteBuffer()
        {
            this.mTempByteBuffer = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.GLLogWrapper
 * JD-Core Version:        0.6.2
 */