package android.opengl;

import java.io.Writer;
import javax.microedition.khronos.egl.EGL;
import javax.microedition.khronos.opengles.GL;

public class GLDebugHelper
{
    public static final int CONFIG_CHECK_GL_ERROR = 1;
    public static final int CONFIG_CHECK_THREAD = 2;
    public static final int CONFIG_LOG_ARGUMENT_NAMES = 4;
    public static final int ERROR_WRONG_THREAD = 28672;

    public static EGL wrap(EGL paramEGL, int paramInt, Writer paramWriter)
    {
        if (paramWriter != null)
            paramEGL = new EGLLogWrapper(paramEGL, paramInt, paramWriter);
        return paramEGL;
    }

    public static GL wrap(GL paramGL, int paramInt, Writer paramWriter)
    {
        if (paramInt != 0);
        for (Object localObject1 = new GLErrorWrapper(paramGL, paramInt); ; localObject1 = paramGL)
        {
            boolean bool;
            if (paramWriter != null)
                if ((paramInt & 0x4) != 0)
                    bool = true;
            for (Object localObject2 = new GLLogWrapper((GL)localObject1, paramWriter, bool); ; localObject2 = localObject1)
            {
                return localObject2;
                bool = false;
                break;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.GLDebugHelper
 * JD-Core Version:        0.6.2
 */