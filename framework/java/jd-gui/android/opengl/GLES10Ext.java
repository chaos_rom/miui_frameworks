package android.opengl;

import java.nio.IntBuffer;

public class GLES10Ext
{
    static
    {
        _nativeClassInit();
    }

    private static native void _nativeClassInit();

    public static native int glQueryMatrixxOES(IntBuffer paramIntBuffer1, IntBuffer paramIntBuffer2);

    public static native int glQueryMatrixxOES(int[] paramArrayOfInt1, int paramInt1, int[] paramArrayOfInt2, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.GLES10Ext
 * JD-Core Version:        0.6.2
 */