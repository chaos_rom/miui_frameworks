package android.opengl;

import android.content.Context;
import android.os.SystemProperties;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLSurfaceView extends SurfaceView
    implements SurfaceHolder.Callback
{
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    private static final boolean LOG_ATTACH_DETACH = false;
    private static final boolean LOG_EGL = false;
    private static final boolean LOG_PAUSE_RESUME = false;
    private static final boolean LOG_RENDERER = false;
    private static final boolean LOG_RENDERER_DRAW_FRAME = false;
    private static final boolean LOG_SURFACE = false;
    private static final boolean LOG_THREADS = false;
    public static final int RENDERMODE_CONTINUOUSLY = 1;
    public static final int RENDERMODE_WHEN_DIRTY = 0;
    private static final String TAG = "GLSurfaceView";
    private static final GLThreadManager sGLThreadManager = new GLThreadManager(null);
    private int mDebugFlags;
    private boolean mDetached;
    private EGLConfigChooser mEGLConfigChooser;
    private int mEGLContextClientVersion;
    private EGLContextFactory mEGLContextFactory;
    private EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private GLThread mGLThread;
    private GLWrapper mGLWrapper;
    private boolean mPreserveEGLContextOnPause;
    private Renderer mRenderer;
    private final WeakReference<GLSurfaceView> mThisWeakRef = new WeakReference(this);

    public GLSurfaceView(Context paramContext)
    {
        super(paramContext);
        init();
    }

    public GLSurfaceView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        init();
    }

    private void checkRenderThreadState()
    {
        if (this.mGLThread != null)
            throw new IllegalStateException("setRenderer has already been called for this instance.");
    }

    private void init()
    {
        getHolder().addCallback(this);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (this.mGLThread != null)
                this.mGLThread.requestExitAndWait();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getDebugFlags()
    {
        return this.mDebugFlags;
    }

    public boolean getPreserveEGLContextOnPause()
    {
        return this.mPreserveEGLContextOnPause;
    }

    public int getRenderMode()
    {
        return this.mGLThread.getRenderMode();
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if ((this.mDetached) && (this.mRenderer != null))
        {
            int i = 1;
            if (this.mGLThread != null)
                i = this.mGLThread.getRenderMode();
            this.mGLThread = new GLThread(this.mThisWeakRef);
            if (i != 1)
                this.mGLThread.setRenderMode(i);
            this.mGLThread.start();
        }
        this.mDetached = false;
    }

    protected void onDetachedFromWindow()
    {
        if (this.mGLThread != null)
            this.mGLThread.requestExitAndWait();
        this.mDetached = true;
        super.onDetachedFromWindow();
    }

    public void onPause()
    {
        this.mGLThread.onPause();
    }

    public void onResume()
    {
        this.mGLThread.onResume();
    }

    public void queueEvent(Runnable paramRunnable)
    {
        this.mGLThread.queueEvent(paramRunnable);
    }

    public void requestRender()
    {
        this.mGLThread.requestRender();
    }

    public void setDebugFlags(int paramInt)
    {
        this.mDebugFlags = paramInt;
    }

    public void setEGLConfigChooser(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        setEGLConfigChooser(new ComponentSizeChooser(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6));
    }

    public void setEGLConfigChooser(EGLConfigChooser paramEGLConfigChooser)
    {
        checkRenderThreadState();
        this.mEGLConfigChooser = paramEGLConfigChooser;
    }

    public void setEGLConfigChooser(boolean paramBoolean)
    {
        setEGLConfigChooser(new SimpleEGLConfigChooser(paramBoolean));
    }

    public void setEGLContextClientVersion(int paramInt)
    {
        checkRenderThreadState();
        this.mEGLContextClientVersion = paramInt;
    }

    public void setEGLContextFactory(EGLContextFactory paramEGLContextFactory)
    {
        checkRenderThreadState();
        this.mEGLContextFactory = paramEGLContextFactory;
    }

    public void setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory paramEGLWindowSurfaceFactory)
    {
        checkRenderThreadState();
        this.mEGLWindowSurfaceFactory = paramEGLWindowSurfaceFactory;
    }

    public void setGLWrapper(GLWrapper paramGLWrapper)
    {
        this.mGLWrapper = paramGLWrapper;
    }

    public void setPreserveEGLContextOnPause(boolean paramBoolean)
    {
        this.mPreserveEGLContextOnPause = paramBoolean;
    }

    public void setRenderMode(int paramInt)
    {
        this.mGLThread.setRenderMode(paramInt);
    }

    public void setRenderer(Renderer paramRenderer)
    {
        checkRenderThreadState();
        if (this.mEGLConfigChooser == null)
            this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
        if (this.mEGLContextFactory == null)
            this.mEGLContextFactory = new DefaultContextFactory(null);
        if (this.mEGLWindowSurfaceFactory == null)
            this.mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory(null);
        this.mRenderer = paramRenderer;
        this.mGLThread = new GLThread(this.mThisWeakRef);
        this.mGLThread.start();
    }

    public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mGLThread.onWindowResize(paramInt2, paramInt3);
    }

    public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
    {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
    {
        this.mGLThread.surfaceDestroyed();
    }

    private static class GLThreadManager
    {
        private static String TAG = "GLThreadManager";
        private static final int kGLES_20 = 131072;
        private static final String kMSM7K_RENDERER_PREFIX = "Q3Dimension MSM7500 ";
        private GLSurfaceView.GLThread mEglOwner;
        private boolean mGLESDriverCheckComplete;
        private int mGLESVersion;
        private boolean mGLESVersionCheckComplete;
        private boolean mLimitedGLESContexts;
        private boolean mMultipleGLESContextsAllowed;

        private void checkGLESVersion()
        {
            if (!this.mGLESVersionCheckComplete)
            {
                this.mGLESVersion = SystemProperties.getInt("ro.opengles.version", 0);
                if (this.mGLESVersion >= 131072)
                    this.mMultipleGLESContextsAllowed = true;
                this.mGLESVersionCheckComplete = true;
            }
        }

        /** @deprecated */
        public void checkGLDriver(GL10 paramGL10)
        {
            boolean bool1 = true;
            try
            {
                if (!this.mGLESDriverCheckComplete)
                {
                    checkGLESVersion();
                    String str = paramGL10.glGetString(7937);
                    if (this.mGLESVersion < 131072)
                        if (str.startsWith("Q3Dimension MSM7500 "))
                            break label78;
                }
                label78: for (boolean bool2 = bool1; ; bool2 = false)
                {
                    this.mMultipleGLESContextsAllowed = bool2;
                    notifyAll();
                    if (this.mMultipleGLESContextsAllowed)
                        break;
                    this.mLimitedGLESContexts = bool1;
                    this.mGLESDriverCheckComplete = true;
                    return;
                }
                bool1 = false;
            }
            finally
            {
            }
        }

        public void releaseEglContextLocked(GLSurfaceView.GLThread paramGLThread)
        {
            if (this.mEglOwner == paramGLThread)
                this.mEglOwner = null;
            notifyAll();
        }

        /** @deprecated */
        public boolean shouldReleaseEGLContextWhenPausing()
        {
            try
            {
                boolean bool = this.mLimitedGLESContexts;
                return bool;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public boolean shouldTerminateEGLWhenPausing()
        {
            try
            {
                checkGLESVersion();
                boolean bool1 = this.mMultipleGLESContextsAllowed;
                if (!bool1)
                {
                    bool2 = true;
                    return bool2;
                }
                boolean bool2 = false;
            }
            finally
            {
            }
        }

        /** @deprecated */
        public void threadExiting(GLSurfaceView.GLThread paramGLThread)
        {
            try
            {
                GLSurfaceView.GLThread.access$1102(paramGLThread, true);
                if (this.mEglOwner == paramGLThread)
                    this.mEglOwner = null;
                notifyAll();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public boolean tryAcquireEglContextLocked(GLSurfaceView.GLThread paramGLThread)
        {
            boolean bool = true;
            if ((this.mEglOwner == paramGLThread) || (this.mEglOwner == null))
            {
                this.mEglOwner = paramGLThread;
                notifyAll();
            }
            while (true)
            {
                return bool;
                checkGLESVersion();
                if (!this.mMultipleGLESContextsAllowed)
                {
                    if (this.mEglOwner != null)
                        this.mEglOwner.requestReleaseEglContextLocked();
                    bool = false;
                }
            }
        }
    }

    static class LogWriter extends Writer
    {
        private StringBuilder mBuilder = new StringBuilder();

        private void flushBuilder()
        {
            if (this.mBuilder.length() > 0)
            {
                Log.v("GLSurfaceView", this.mBuilder.toString());
                this.mBuilder.delete(0, this.mBuilder.length());
            }
        }

        public void close()
        {
            flushBuilder();
        }

        public void flush()
        {
            flushBuilder();
        }

        public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        {
            int i = 0;
            if (i < paramInt2)
            {
                char c = paramArrayOfChar[(paramInt1 + i)];
                if (c == '\n')
                    flushBuilder();
                while (true)
                {
                    i++;
                    break;
                    this.mBuilder.append(c);
                }
            }
        }
    }

    static class GLThread extends Thread
    {
        private GLSurfaceView.EglHelper mEglHelper;
        private ArrayList<Runnable> mEventQueue = new ArrayList();
        private boolean mExited;
        private WeakReference<GLSurfaceView> mGLSurfaceViewWeakRef;
        private boolean mHasSurface;
        private boolean mHaveEglContext;
        private boolean mHaveEglSurface;
        private int mHeight = 0;
        private boolean mPaused;
        private boolean mRenderComplete;
        private int mRenderMode = 1;
        private boolean mRequestPaused;
        private boolean mRequestRender = true;
        private boolean mShouldExit;
        private boolean mShouldReleaseEglContext;
        private boolean mSizeChanged = true;
        private boolean mSurfaceIsBad;
        private boolean mWaitingForSurface;
        private int mWidth = 0;

        GLThread(WeakReference<GLSurfaceView> paramWeakReference)
        {
            this.mGLSurfaceViewWeakRef = paramWeakReference;
        }

        // ERROR //
        private void guardedRun()
            throws InterruptedException
        {
            // Byte code:
            //     0: aload_0
            //     1: new 66	android/opengl/GLSurfaceView$EglHelper
            //     4: dup
            //     5: aload_0
            //     6: getfield 55	android/opengl/GLSurfaceView$GLThread:mGLSurfaceViewWeakRef	Ljava/lang/ref/WeakReference;
            //     9: invokespecial 68	android/opengl/GLSurfaceView$EglHelper:<init>	(Ljava/lang/ref/WeakReference;)V
            //     12: putfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     15: aload_0
            //     16: iconst_0
            //     17: putfield 72	android/opengl/GLSurfaceView$GLThread:mHaveEglContext	Z
            //     20: aload_0
            //     21: iconst_0
            //     22: putfield 74	android/opengl/GLSurfaceView$GLThread:mHaveEglSurface	Z
            //     25: aconst_null
            //     26: astore_1
            //     27: iconst_0
            //     28: istore_2
            //     29: iconst_0
            //     30: istore_3
            //     31: iconst_0
            //     32: istore 4
            //     34: iconst_0
            //     35: istore 5
            //     37: iconst_0
            //     38: istore 6
            //     40: iconst_0
            //     41: istore 7
            //     43: iconst_0
            //     44: istore 8
            //     46: iconst_0
            //     47: istore 9
            //     49: iconst_0
            //     50: istore 10
            //     52: iconst_0
            //     53: istore 11
            //     55: aconst_null
            //     56: astore 12
            //     58: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     61: astore 16
            //     63: aload 16
            //     65: monitorenter
            //     66: aload_0
            //     67: getfield 80	android/opengl/GLSurfaceView$GLThread:mShouldExit	Z
            //     70: ifeq +26 -> 96
            //     73: aload 16
            //     75: monitorexit
            //     76: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     79: astore 31
            //     81: aload 31
            //     83: monitorenter
            //     84: aload_0
            //     85: invokespecial 83	android/opengl/GLSurfaceView$GLThread:stopEglSurfaceLocked	()V
            //     88: aload_0
            //     89: invokespecial 86	android/opengl/GLSurfaceView$GLThread:stopEglContextLocked	()V
            //     92: aload 31
            //     94: monitorexit
            //     95: return
            //     96: aload_0
            //     97: getfield 43	android/opengl/GLSurfaceView$GLThread:mEventQueue	Ljava/util/ArrayList;
            //     100: invokevirtual 90	java/util/ArrayList:isEmpty	()Z
            //     103: ifne +37 -> 140
            //     106: aload_0
            //     107: getfield 43	android/opengl/GLSurfaceView$GLThread:mEventQueue	Ljava/util/ArrayList;
            //     110: iconst_0
            //     111: invokevirtual 94	java/util/ArrayList:remove	(I)Ljava/lang/Object;
            //     114: checkcast 96	java/lang/Runnable
            //     117: astore 12
            //     119: aload 16
            //     121: monitorexit
            //     122: aload 12
            //     124: ifnull +466 -> 590
            //     127: aload 12
            //     129: invokeinterface 99 1 0
            //     134: aconst_null
            //     135: astore 12
            //     137: goto -79 -> 58
            //     140: iconst_0
            //     141: istore 18
            //     143: aload_0
            //     144: getfield 101	android/opengl/GLSurfaceView$GLThread:mPaused	Z
            //     147: aload_0
            //     148: getfield 103	android/opengl/GLSurfaceView$GLThread:mRequestPaused	Z
            //     151: if_icmpeq +23 -> 174
            //     154: aload_0
            //     155: getfield 103	android/opengl/GLSurfaceView$GLThread:mRequestPaused	Z
            //     158: istore 18
            //     160: aload_0
            //     161: aload_0
            //     162: getfield 103	android/opengl/GLSurfaceView$GLThread:mRequestPaused	Z
            //     165: putfield 101	android/opengl/GLSurfaceView$GLThread:mPaused	Z
            //     168: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     171: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     174: aload_0
            //     175: getfield 110	android/opengl/GLSurfaceView$GLThread:mShouldReleaseEglContext	Z
            //     178: ifeq +19 -> 197
            //     181: aload_0
            //     182: invokespecial 83	android/opengl/GLSurfaceView$GLThread:stopEglSurfaceLocked	()V
            //     185: aload_0
            //     186: invokespecial 86	android/opengl/GLSurfaceView$GLThread:stopEglContextLocked	()V
            //     189: aload_0
            //     190: iconst_0
            //     191: putfield 110	android/opengl/GLSurfaceView$GLThread:mShouldReleaseEglContext	Z
            //     194: iconst_1
            //     195: istore 9
            //     197: iload 5
            //     199: ifeq +14 -> 213
            //     202: aload_0
            //     203: invokespecial 83	android/opengl/GLSurfaceView$GLThread:stopEglSurfaceLocked	()V
            //     206: aload_0
            //     207: invokespecial 86	android/opengl/GLSurfaceView$GLThread:stopEglContextLocked	()V
            //     210: iconst_0
            //     211: istore 5
            //     213: iload 18
            //     215: ifeq +14 -> 229
            //     218: aload_0
            //     219: getfield 74	android/opengl/GLSurfaceView$GLThread:mHaveEglSurface	Z
            //     222: ifeq +7 -> 229
            //     225: aload_0
            //     226: invokespecial 83	android/opengl/GLSurfaceView$GLThread:stopEglSurfaceLocked	()V
            //     229: iload 18
            //     231: ifeq +48 -> 279
            //     234: aload_0
            //     235: getfield 72	android/opengl/GLSurfaceView$GLThread:mHaveEglContext	Z
            //     238: ifeq +41 -> 279
            //     241: aload_0
            //     242: getfield 55	android/opengl/GLSurfaceView$GLThread:mGLSurfaceViewWeakRef	Ljava/lang/ref/WeakReference;
            //     245: invokevirtual 116	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     248: checkcast 6	android/opengl/GLSurfaceView
            //     251: astore 29
            //     253: aload 29
            //     255: ifnonnull +267 -> 522
            //     258: iconst_0
            //     259: istore 30
            //     261: iload 30
            //     263: ifeq +12 -> 275
            //     266: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     269: invokevirtual 121	android/opengl/GLSurfaceView$GLThreadManager:shouldReleaseEGLContextWhenPausing	()Z
            //     272: ifeq +7 -> 279
            //     275: aload_0
            //     276: invokespecial 86	android/opengl/GLSurfaceView$GLThread:stopEglContextLocked	()V
            //     279: iload 18
            //     281: ifeq +19 -> 300
            //     284: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     287: invokevirtual 124	android/opengl/GLSurfaceView$GLThreadManager:shouldTerminateEGLWhenPausing	()Z
            //     290: ifeq +10 -> 300
            //     293: aload_0
            //     294: getfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     297: invokevirtual 127	android/opengl/GLSurfaceView$EglHelper:finish	()V
            //     300: aload_0
            //     301: getfield 129	android/opengl/GLSurfaceView$GLThread:mHasSurface	Z
            //     304: ifne +37 -> 341
            //     307: aload_0
            //     308: getfield 131	android/opengl/GLSurfaceView$GLThread:mWaitingForSurface	Z
            //     311: ifne +30 -> 341
            //     314: aload_0
            //     315: getfield 74	android/opengl/GLSurfaceView$GLThread:mHaveEglSurface	Z
            //     318: ifeq +7 -> 325
            //     321: aload_0
            //     322: invokespecial 83	android/opengl/GLSurfaceView$GLThread:stopEglSurfaceLocked	()V
            //     325: aload_0
            //     326: iconst_1
            //     327: putfield 131	android/opengl/GLSurfaceView$GLThread:mWaitingForSurface	Z
            //     330: aload_0
            //     331: iconst_0
            //     332: putfield 133	android/opengl/GLSurfaceView$GLThread:mSurfaceIsBad	Z
            //     335: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     338: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     341: aload_0
            //     342: getfield 129	android/opengl/GLSurfaceView$GLThread:mHasSurface	Z
            //     345: ifeq +21 -> 366
            //     348: aload_0
            //     349: getfield 131	android/opengl/GLSurfaceView$GLThread:mWaitingForSurface	Z
            //     352: ifeq +14 -> 366
            //     355: aload_0
            //     356: iconst_0
            //     357: putfield 131	android/opengl/GLSurfaceView$GLThread:mWaitingForSurface	Z
            //     360: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     363: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     366: iload 8
            //     368: ifeq +20 -> 388
            //     371: iconst_0
            //     372: istore 7
            //     374: iconst_0
            //     375: istore 8
            //     377: aload_0
            //     378: iconst_1
            //     379: putfield 135	android/opengl/GLSurfaceView$GLThread:mRenderComplete	Z
            //     382: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     385: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     388: aload_0
            //     389: invokespecial 138	android/opengl/GLSurfaceView$GLThread:readyToDraw	()Z
            //     392: ifeq +189 -> 581
            //     395: aload_0
            //     396: getfield 72	android/opengl/GLSurfaceView$GLThread:mHaveEglContext	Z
            //     399: ifne +11 -> 410
            //     402: iload 9
            //     404: ifeq +128 -> 532
            //     407: iconst_0
            //     408: istore 9
            //     410: aload_0
            //     411: getfield 72	android/opengl/GLSurfaceView$GLThread:mHaveEglContext	Z
            //     414: ifeq +23 -> 437
            //     417: aload_0
            //     418: getfield 74	android/opengl/GLSurfaceView$GLThread:mHaveEglSurface	Z
            //     421: ifne +16 -> 437
            //     424: aload_0
            //     425: iconst_1
            //     426: putfield 74	android/opengl/GLSurfaceView$GLThread:mHaveEglSurface	Z
            //     429: iconst_1
            //     430: istore_3
            //     431: iconst_1
            //     432: istore 4
            //     434: iconst_1
            //     435: istore 6
            //     437: aload_0
            //     438: getfield 74	android/opengl/GLSurfaceView$GLThread:mHaveEglSurface	Z
            //     441: ifeq +140 -> 581
            //     444: aload_0
            //     445: getfield 45	android/opengl/GLSurfaceView$GLThread:mSizeChanged	Z
            //     448: ifeq +28 -> 476
            //     451: iconst_1
            //     452: istore 6
            //     454: aload_0
            //     455: getfield 47	android/opengl/GLSurfaceView$GLThread:mWidth	I
            //     458: istore 10
            //     460: aload_0
            //     461: getfield 49	android/opengl/GLSurfaceView$GLThread:mHeight	I
            //     464: istore 11
            //     466: iconst_1
            //     467: istore 7
            //     469: iconst_1
            //     470: istore_3
            //     471: aload_0
            //     472: iconst_0
            //     473: putfield 45	android/opengl/GLSurfaceView$GLThread:mSizeChanged	Z
            //     476: aload_0
            //     477: iconst_0
            //     478: putfield 51	android/opengl/GLSurfaceView$GLThread:mRequestRender	Z
            //     481: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     484: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     487: goto -368 -> 119
            //     490: astore 17
            //     492: aload 16
            //     494: monitorexit
            //     495: aload 17
            //     497: athrow
            //     498: astore 13
            //     500: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     503: astore 14
            //     505: aload 14
            //     507: monitorenter
            //     508: aload_0
            //     509: invokespecial 83	android/opengl/GLSurfaceView$GLThread:stopEglSurfaceLocked	()V
            //     512: aload_0
            //     513: invokespecial 86	android/opengl/GLSurfaceView$GLThread:stopEglContextLocked	()V
            //     516: aload 14
            //     518: monitorexit
            //     519: aload 13
            //     521: athrow
            //     522: aload 29
            //     524: invokestatic 142	android/opengl/GLSurfaceView:access$900	(Landroid/opengl/GLSurfaceView;)Z
            //     527: istore 30
            //     529: goto -268 -> 261
            //     532: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     535: aload_0
            //     536: invokevirtual 146	android/opengl/GLSurfaceView$GLThreadManager:tryAcquireEglContextLocked	(Landroid/opengl/GLSurfaceView$GLThread;)Z
            //     539: istore 27
            //     541: iload 27
            //     543: ifeq -133 -> 410
            //     546: aload_0
            //     547: getfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     550: invokevirtual 149	android/opengl/GLSurfaceView$EglHelper:start	()V
            //     553: aload_0
            //     554: iconst_1
            //     555: putfield 72	android/opengl/GLSurfaceView$GLThread:mHaveEglContext	Z
            //     558: iconst_1
            //     559: istore_2
            //     560: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     563: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     566: goto -156 -> 410
            //     569: astore 28
            //     571: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     574: aload_0
            //     575: invokevirtual 153	android/opengl/GLSurfaceView$GLThreadManager:releaseEglContextLocked	(Landroid/opengl/GLSurfaceView$GLThread;)V
            //     578: aload 28
            //     580: athrow
            //     581: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     584: invokevirtual 156	java/lang/Object:wait	()V
            //     587: goto -521 -> 66
            //     590: iload_3
            //     591: ifeq +46 -> 637
            //     594: aload_0
            //     595: getfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     598: invokevirtual 159	android/opengl/GLSurfaceView$EglHelper:createSurface	()Z
            //     601: ifne +269 -> 870
            //     604: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     607: astore 25
            //     609: aload 25
            //     611: monitorenter
            //     612: aload_0
            //     613: iconst_1
            //     614: putfield 133	android/opengl/GLSurfaceView$GLThread:mSurfaceIsBad	Z
            //     617: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     620: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     623: aload 25
            //     625: monitorexit
            //     626: goto -568 -> 58
            //     629: astore 26
            //     631: aload 25
            //     633: monitorexit
            //     634: aload 26
            //     636: athrow
            //     637: iload 4
            //     639: ifeq +24 -> 663
            //     642: aload_0
            //     643: getfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     646: invokevirtual 163	android/opengl/GLSurfaceView$EglHelper:createGL	()Ljavax/microedition/khronos/opengles/GL;
            //     649: checkcast 165	javax/microedition/khronos/opengles/GL10
            //     652: astore_1
            //     653: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     656: aload_1
            //     657: invokevirtual 169	android/opengl/GLSurfaceView$GLThreadManager:checkGLDriver	(Ljavax/microedition/khronos/opengles/GL10;)V
            //     660: iconst_0
            //     661: istore 4
            //     663: iload_2
            //     664: ifeq +41 -> 705
            //     667: aload_0
            //     668: getfield 55	android/opengl/GLSurfaceView$GLThread:mGLSurfaceViewWeakRef	Ljava/lang/ref/WeakReference;
            //     671: invokevirtual 116	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     674: checkcast 6	android/opengl/GLSurfaceView
            //     677: astore 24
            //     679: aload 24
            //     681: ifnull +194 -> 875
            //     684: aload 24
            //     686: invokestatic 173	android/opengl/GLSurfaceView:access$1000	(Landroid/opengl/GLSurfaceView;)Landroid/opengl/GLSurfaceView$Renderer;
            //     689: aload_1
            //     690: aload_0
            //     691: getfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     694: getfield 177	android/opengl/GLSurfaceView$EglHelper:mEglConfig	Ljavax/microedition/khronos/egl/EGLConfig;
            //     697: invokeinterface 183 3 0
            //     702: goto +173 -> 875
            //     705: iload 6
            //     707: ifeq +38 -> 745
            //     710: aload_0
            //     711: getfield 55	android/opengl/GLSurfaceView$GLThread:mGLSurfaceViewWeakRef	Ljava/lang/ref/WeakReference;
            //     714: invokevirtual 116	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     717: checkcast 6	android/opengl/GLSurfaceView
            //     720: astore 23
            //     722: aload 23
            //     724: ifnull +156 -> 880
            //     727: aload 23
            //     729: invokestatic 173	android/opengl/GLSurfaceView:access$1000	(Landroid/opengl/GLSurfaceView;)Landroid/opengl/GLSurfaceView$Renderer;
            //     732: aload_1
            //     733: iload 10
            //     735: iload 11
            //     737: invokeinterface 187 4 0
            //     742: goto +138 -> 880
            //     745: aload_0
            //     746: getfield 55	android/opengl/GLSurfaceView$GLThread:mGLSurfaceViewWeakRef	Ljava/lang/ref/WeakReference;
            //     749: invokevirtual 116	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     752: checkcast 6	android/opengl/GLSurfaceView
            //     755: astore 19
            //     757: aload 19
            //     759: ifnull +14 -> 773
            //     762: aload 19
            //     764: invokestatic 173	android/opengl/GLSurfaceView:access$1000	(Landroid/opengl/GLSurfaceView;)Landroid/opengl/GLSurfaceView$Renderer;
            //     767: aload_1
            //     768: invokeinterface 190 2 0
            //     773: aload_0
            //     774: getfield 70	android/opengl/GLSurfaceView$GLThread:mEglHelper	Landroid/opengl/GLSurfaceView$EglHelper;
            //     777: invokevirtual 194	android/opengl/GLSurfaceView$EglHelper:swap	()I
            //     780: istore 20
            //     782: iload 20
            //     784: lookupswitch	default:+28->812, 12288:+102->886, 12302:+113->897
            //     813: monitorexit
            //     814: ldc 197
            //     816: iload 20
            //     818: invokestatic 201	android/opengl/GLSurfaceView$EglHelper:logEglErrorAsWarning	(Ljava/lang/String;Ljava/lang/String;I)V
            //     821: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     824: astore 21
            //     826: aload 21
            //     828: monitorenter
            //     829: aload_0
            //     830: iconst_1
            //     831: putfield 133	android/opengl/GLSurfaceView$GLThread:mSurfaceIsBad	Z
            //     834: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     837: invokevirtual 108	java/lang/Object:notifyAll	()V
            //     840: aload 21
            //     842: monitorexit
            //     843: goto +43 -> 886
            //     846: astore 22
            //     848: aload 21
            //     850: monitorexit
            //     851: aload 22
            //     853: athrow
            //     854: astore 15
            //     856: aload 14
            //     858: monitorexit
            //     859: aload 15
            //     861: athrow
            //     862: astore 32
            //     864: aload 31
            //     866: monitorexit
            //     867: aload 32
            //     869: athrow
            //     870: iconst_0
            //     871: istore_3
            //     872: goto -235 -> 637
            //     875: iconst_0
            //     876: istore_2
            //     877: goto -172 -> 705
            //     880: iconst_0
            //     881: istore 6
            //     883: goto -138 -> 745
            //     886: iload 7
            //     888: ifeq -830 -> 58
            //     891: iconst_1
            //     892: istore 8
            //     894: goto -836 -> 58
            //     897: iconst_1
            //     898: istore 5
            //     900: goto -14 -> 886
            //
            // Exception table:
            //     from	to	target	type
            //     66	76	490	finally
            //     96	122	490	finally
            //     143	495	490	finally
            //     522	541	490	finally
            //     546	553	490	finally
            //     553	587	490	finally
            //     58	66	498	finally
            //     127	134	498	finally
            //     495	498	498	finally
            //     594	612	498	finally
            //     634	829	498	finally
            //     851	854	498	finally
            //     546	553	569	java/lang/RuntimeException
            //     612	634	629	finally
            //     829	851	846	finally
            //     508	519	854	finally
            //     856	859	854	finally
            //     84	95	862	finally
            //     864	867	862	finally
        }

        private boolean readyToDraw()
        {
            int i = 1;
            if ((!this.mPaused) && (this.mHasSurface) && (!this.mSurfaceIsBad) && (this.mWidth > 0) && (this.mHeight > 0) && ((this.mRequestRender) || (this.mRenderMode == i)));
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        private void stopEglContextLocked()
        {
            if (this.mHaveEglContext)
            {
                this.mEglHelper.finish();
                this.mHaveEglContext = false;
                GLSurfaceView.sGLThreadManager.releaseEglContextLocked(this);
            }
        }

        private void stopEglSurfaceLocked()
        {
            if (this.mHaveEglSurface)
            {
                this.mHaveEglSurface = false;
                this.mEglHelper.destroySurface();
            }
        }

        public boolean ableToDraw()
        {
            if ((this.mHaveEglContext) && (this.mHaveEglSurface) && (readyToDraw()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public int getRenderMode()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                int i = this.mRenderMode;
                return i;
            }
        }

        public void onPause()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mRequestPaused = true;
                GLSurfaceView.sGLThreadManager.notifyAll();
                while (true)
                    if (!this.mExited)
                    {
                        boolean bool = this.mPaused;
                        if (!bool)
                            try
                            {
                                GLSurfaceView.sGLThreadManager.wait();
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                Thread.currentThread().interrupt();
                            }
                    }
            }
        }

        public void onResume()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mRequestPaused = false;
                this.mRequestRender = true;
                this.mRenderComplete = false;
                GLSurfaceView.sGLThreadManager.notifyAll();
                while (true)
                    if ((!this.mExited) && (this.mPaused))
                    {
                        boolean bool = this.mRenderComplete;
                        if (!bool)
                            try
                            {
                                GLSurfaceView.sGLThreadManager.wait();
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                Thread.currentThread().interrupt();
                            }
                    }
            }
        }

        public void onWindowResize(int paramInt1, int paramInt2)
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mWidth = paramInt1;
                this.mHeight = paramInt2;
                this.mSizeChanged = true;
                this.mRequestRender = true;
                this.mRenderComplete = false;
                GLSurfaceView.sGLThreadManager.notifyAll();
                while (true)
                    if ((!this.mExited) && (!this.mPaused) && (!this.mRenderComplete))
                    {
                        boolean bool = ableToDraw();
                        if (bool)
                            try
                            {
                                GLSurfaceView.sGLThreadManager.wait();
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                Thread.currentThread().interrupt();
                            }
                    }
            }
        }

        public void queueEvent(Runnable paramRunnable)
        {
            if (paramRunnable == null)
                throw new IllegalArgumentException("r must not be null");
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mEventQueue.add(paramRunnable);
                GLSurfaceView.sGLThreadManager.notifyAll();
                return;
            }
        }

        public void requestExitAndWait()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mShouldExit = true;
                GLSurfaceView.sGLThreadManager.notifyAll();
                while (true)
                {
                    boolean bool = this.mExited;
                    if (!bool)
                        try
                        {
                            GLSurfaceView.sGLThreadManager.wait();
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                            Thread.currentThread().interrupt();
                        }
                }
            }
        }

        public void requestReleaseEglContextLocked()
        {
            this.mShouldReleaseEglContext = true;
            GLSurfaceView.sGLThreadManager.notifyAll();
        }

        public void requestRender()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mRequestRender = true;
                GLSurfaceView.sGLThreadManager.notifyAll();
                return;
            }
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: aload_0
            //     1: new 237	java/lang/StringBuilder
            //     4: dup
            //     5: invokespecial 238	java/lang/StringBuilder:<init>	()V
            //     8: ldc 240
            //     10: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     13: aload_0
            //     14: invokevirtual 248	android/opengl/GLSurfaceView$GLThread:getId	()J
            //     17: invokevirtual 251	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
            //     20: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     23: invokevirtual 258	android/opengl/GLSurfaceView$GLThread:setName	(Ljava/lang/String;)V
            //     26: aload_0
            //     27: invokespecial 260	android/opengl/GLSurfaceView$GLThread:guardedRun	()V
            //     30: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     33: astore_2
            //     34: aload_2
            //     35: aload_0
            //     36: invokevirtual 263	android/opengl/GLSurfaceView$GLThreadManager:threadExiting	(Landroid/opengl/GLSurfaceView$GLThread;)V
            //     39: return
            //     40: astore_3
            //     41: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     44: aload_0
            //     45: invokevirtual 263	android/opengl/GLSurfaceView$GLThreadManager:threadExiting	(Landroid/opengl/GLSurfaceView$GLThread;)V
            //     48: aload_3
            //     49: athrow
            //     50: astore_1
            //     51: invokestatic 78	android/opengl/GLSurfaceView:access$800	()Landroid/opengl/GLSurfaceView$GLThreadManager;
            //     54: astore_2
            //     55: goto -21 -> 34
            //
            // Exception table:
            //     from	to	target	type
            //     26	30	40	finally
            //     26	30	50	java/lang/InterruptedException
        }

        public void setRenderMode(int paramInt)
        {
            if ((paramInt < 0) || (paramInt > 1))
                throw new IllegalArgumentException("renderMode");
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mRenderMode = paramInt;
                GLSurfaceView.sGLThreadManager.notifyAll();
                return;
            }
        }

        public void surfaceCreated()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mHasSurface = true;
                GLSurfaceView.sGLThreadManager.notifyAll();
                while (true)
                    if (this.mWaitingForSurface)
                    {
                        boolean bool = this.mExited;
                        if (!bool)
                            try
                            {
                                GLSurfaceView.sGLThreadManager.wait();
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                Thread.currentThread().interrupt();
                            }
                    }
            }
        }

        public void surfaceDestroyed()
        {
            synchronized (GLSurfaceView.sGLThreadManager)
            {
                this.mHasSurface = false;
                GLSurfaceView.sGLThreadManager.notifyAll();
                while (true)
                    if (!this.mWaitingForSurface)
                    {
                        boolean bool = this.mExited;
                        if (!bool)
                            try
                            {
                                GLSurfaceView.sGLThreadManager.wait();
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                Thread.currentThread().interrupt();
                            }
                    }
            }
        }
    }

    private static class EglHelper
    {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;
        private WeakReference<GLSurfaceView> mGLSurfaceViewWeakRef;

        public EglHelper(WeakReference<GLSurfaceView> paramWeakReference)
        {
            this.mGLSurfaceViewWeakRef = paramWeakReference;
        }

        private void destroySurfaceImp()
        {
            if ((this.mEglSurface != null) && (this.mEglSurface != EGL10.EGL_NO_SURFACE))
            {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                GLSurfaceView localGLSurfaceView = (GLSurfaceView)this.mGLSurfaceViewWeakRef.get();
                if (localGLSurfaceView != null)
                    localGLSurfaceView.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
        }

        public static String formatEglError(String paramString, int paramInt)
        {
            return paramString + " failed: " + EGLLogWrapper.getErrorString(paramInt);
        }

        public static void logEglErrorAsWarning(String paramString1, String paramString2, int paramInt)
        {
            Log.w(paramString1, formatEglError(paramString2, paramInt));
        }

        private void throwEglException(String paramString)
        {
            throwEglException(paramString, this.mEgl.eglGetError());
        }

        public static void throwEglException(String paramString, int paramInt)
        {
            throw new RuntimeException(formatEglError(paramString, paramInt));
        }

        GL createGL()
        {
            GL localGL = this.mEglContext.getGL();
            GLSurfaceView localGLSurfaceView = (GLSurfaceView)this.mGLSurfaceViewWeakRef.get();
            if (localGLSurfaceView != null)
            {
                if (localGLSurfaceView.mGLWrapper != null)
                    localGL = localGLSurfaceView.mGLWrapper.wrap(localGL);
                if ((0x3 & localGLSurfaceView.mDebugFlags) != 0)
                {
                    int i = 0;
                    GLSurfaceView.LogWriter localLogWriter = null;
                    if ((0x1 & localGLSurfaceView.mDebugFlags) != 0)
                        i = 0x0 | 0x1;
                    if ((0x2 & localGLSurfaceView.mDebugFlags) != 0)
                        localLogWriter = new GLSurfaceView.LogWriter();
                    localGL = GLDebugHelper.wrap(localGL, i, localLogWriter);
                }
            }
            return localGL;
        }

        public boolean createSurface()
        {
            boolean bool = false;
            if (this.mEgl == null)
                throw new RuntimeException("egl not initialized");
            if (this.mEglDisplay == null)
                throw new RuntimeException("eglDisplay not initialized");
            if (this.mEglConfig == null)
                throw new RuntimeException("mEglConfig not initialized");
            destroySurfaceImp();
            GLSurfaceView localGLSurfaceView = (GLSurfaceView)this.mGLSurfaceViewWeakRef.get();
            if (localGLSurfaceView != null)
            {
                this.mEglSurface = localGLSurfaceView.mEGLWindowSurfaceFactory.createWindowSurface(this.mEgl, this.mEglDisplay, this.mEglConfig, localGLSurfaceView.getHolder());
                if ((this.mEglSurface != null) && (this.mEglSurface != EGL10.EGL_NO_SURFACE))
                    break label151;
                if (this.mEgl.eglGetError() == 12299)
                    Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
            }
            while (true)
            {
                return bool;
                this.mEglSurface = null;
                break;
                label151: if (!this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext))
                    logEglErrorAsWarning("EGLHelper", "eglMakeCurrent", this.mEgl.eglGetError());
                else
                    bool = true;
            }
        }

        public void destroySurface()
        {
            destroySurfaceImp();
        }

        public void finish()
        {
            if (this.mEglContext != null)
            {
                GLSurfaceView localGLSurfaceView = (GLSurfaceView)this.mGLSurfaceViewWeakRef.get();
                if (localGLSurfaceView != null)
                    localGLSurfaceView.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null)
            {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }

        public void start()
        {
            this.mEgl = ((EGL10)EGLContext.getEGL());
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            if (this.mEglDisplay == EGL10.EGL_NO_DISPLAY)
                throw new RuntimeException("eglGetDisplay failed");
            int[] arrayOfInt = new int[2];
            if (!this.mEgl.eglInitialize(this.mEglDisplay, arrayOfInt))
                throw new RuntimeException("eglInitialize failed");
            GLSurfaceView localGLSurfaceView = (GLSurfaceView)this.mGLSurfaceViewWeakRef.get();
            if (localGLSurfaceView == null)
                this.mEglConfig = null;
            for (this.mEglContext = null; ; this.mEglContext = localGLSurfaceView.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig))
            {
                if ((this.mEglContext == null) || (this.mEglContext == EGL10.EGL_NO_CONTEXT))
                {
                    this.mEglContext = null;
                    throwEglException("createContext");
                }
                this.mEglSurface = null;
                return;
                this.mEglConfig = localGLSurfaceView.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
            }
        }

        public int swap()
        {
            if (!this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface));
            for (int i = this.mEgl.eglGetError(); ; i = 12288)
                return i;
        }
    }

    private class SimpleEGLConfigChooser extends GLSurfaceView.ComponentSizeChooser
    {
        public SimpleEGLConfigChooser(boolean arg2)
        {
        }
    }

    private class ComponentSizeChooser extends GLSurfaceView.BaseConfigChooser
    {
        protected int mAlphaSize;
        protected int mBlueSize;
        protected int mDepthSize;
        protected int mGreenSize;
        protected int mRedSize;
        protected int mStencilSize;
        private int[] mValue = new int[1];

        public ComponentSizeChooser(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int arg7)
        {
            super(arrayOfInt);
            this.mRedSize = paramInt1;
            this.mGreenSize = paramInt2;
            this.mBlueSize = paramInt3;
            this.mAlphaSize = paramInt4;
            this.mDepthSize = paramInt5;
            this.mStencilSize = i;
        }

        private int findConfigAttrib(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int paramInt1, int paramInt2)
        {
            if (paramEGL10.eglGetConfigAttrib(paramEGLDisplay, paramEGLConfig, paramInt1, this.mValue))
                paramInt2 = this.mValue[0];
            return paramInt2;
        }

        public EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig)
        {
            int i = paramArrayOfEGLConfig.length;
            int j = 0;
            EGLConfig localEGLConfig;
            if (j < i)
            {
                localEGLConfig = paramArrayOfEGLConfig[j];
                int k = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12325, 0);
                int m = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12326, 0);
                if ((k >= this.mDepthSize) && (m >= this.mStencilSize))
                {
                    int n = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12324, 0);
                    int i1 = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12323, 0);
                    int i2 = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12322, 0);
                    int i3 = findConfigAttrib(paramEGL10, paramEGLDisplay, localEGLConfig, 12321, 0);
                    if ((n != this.mRedSize) || (i1 != this.mGreenSize) || (i2 != this.mBlueSize) || (i3 != this.mAlphaSize));
                }
            }
            while (true)
            {
                return localEGLConfig;
                j++;
                break;
                localEGLConfig = null;
            }
        }
    }

    private abstract class BaseConfigChooser
        implements GLSurfaceView.EGLConfigChooser
    {
        protected int[] mConfigSpec;

        public BaseConfigChooser(int[] arg2)
        {
            int[] arrayOfInt;
            this.mConfigSpec = filterConfigSpec(arrayOfInt);
        }

        private int[] filterConfigSpec(int[] paramArrayOfInt)
        {
            if (GLSurfaceView.this.mEGLContextClientVersion != 2);
            while (true)
            {
                return paramArrayOfInt;
                int i = paramArrayOfInt.length;
                int[] arrayOfInt = new int[i + 2];
                System.arraycopy(paramArrayOfInt, 0, arrayOfInt, 0, i - 1);
                arrayOfInt[(i - 1)] = 12352;
                arrayOfInt[i] = 4;
                arrayOfInt[(i + 1)] = 12344;
                paramArrayOfInt = arrayOfInt;
            }
        }

        public EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay)
        {
            int[] arrayOfInt = new int[1];
            if (!paramEGL10.eglChooseConfig(paramEGLDisplay, this.mConfigSpec, null, 0, arrayOfInt))
                throw new IllegalArgumentException("eglChooseConfig failed");
            int i = arrayOfInt[0];
            if (i <= 0)
                throw new IllegalArgumentException("No configs match configSpec");
            EGLConfig[] arrayOfEGLConfig = new EGLConfig[i];
            if (!paramEGL10.eglChooseConfig(paramEGLDisplay, this.mConfigSpec, arrayOfEGLConfig, i, arrayOfInt))
                throw new IllegalArgumentException("eglChooseConfig#2 failed");
            EGLConfig localEGLConfig = chooseConfig(paramEGL10, paramEGLDisplay, arrayOfEGLConfig);
            if (localEGLConfig == null)
                throw new IllegalArgumentException("No config chosen");
            return localEGLConfig;
        }

        abstract EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig);
    }

    public static abstract interface EGLConfigChooser
    {
        public abstract EGLConfig chooseConfig(EGL10 paramEGL10, EGLDisplay paramEGLDisplay);
    }

    private static class DefaultWindowSurfaceFactory
        implements GLSurfaceView.EGLWindowSurfaceFactory
    {
        public EGLSurface createWindowSurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject)
        {
            Object localObject = null;
            try
            {
                EGLSurface localEGLSurface = paramEGL10.eglCreateWindowSurface(paramEGLDisplay, paramEGLConfig, paramObject, null);
                localObject = localEGLSurface;
                return localObject;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                while (true)
                    Log.e("GLSurfaceView", "eglCreateWindowSurface", localIllegalArgumentException);
            }
        }

        public void destroySurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface)
        {
            paramEGL10.eglDestroySurface(paramEGLDisplay, paramEGLSurface);
        }
    }

    public static abstract interface EGLWindowSurfaceFactory
    {
        public abstract EGLSurface createWindowSurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject);

        public abstract void destroySurface(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface);
    }

    private class DefaultContextFactory
        implements GLSurfaceView.EGLContextFactory
    {
        private int EGL_CONTEXT_CLIENT_VERSION = 12440;

        private DefaultContextFactory()
        {
        }

        public EGLContext createContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig)
        {
            int[] arrayOfInt = new int[3];
            arrayOfInt[0] = this.EGL_CONTEXT_CLIENT_VERSION;
            arrayOfInt[1] = GLSurfaceView.this.mEGLContextClientVersion;
            arrayOfInt[2] = 12344;
            EGLContext localEGLContext = EGL10.EGL_NO_CONTEXT;
            if (GLSurfaceView.this.mEGLContextClientVersion != 0);
            while (true)
            {
                return paramEGL10.eglCreateContext(paramEGLDisplay, paramEGLConfig, localEGLContext, arrayOfInt);
                arrayOfInt = null;
            }
        }

        public void destroyContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLContext paramEGLContext)
        {
            if (!paramEGL10.eglDestroyContext(paramEGLDisplay, paramEGLContext))
            {
                Log.e("DefaultContextFactory", "display:" + paramEGLDisplay + " context: " + paramEGLContext);
                GLSurfaceView.EglHelper.throwEglException("eglDestroyContex", paramEGL10.eglGetError());
            }
        }
    }

    public static abstract interface EGLContextFactory
    {
        public abstract EGLContext createContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig);

        public abstract void destroyContext(EGL10 paramEGL10, EGLDisplay paramEGLDisplay, EGLContext paramEGLContext);
    }

    public static abstract interface Renderer
    {
        public abstract void onDrawFrame(GL10 paramGL10);

        public abstract void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2);

        public abstract void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig);
    }

    public static abstract interface GLWrapper
    {
        public abstract GL wrap(GL paramGL);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.opengl.GLSurfaceView
 * JD-Core Version:        0.6.2
 */