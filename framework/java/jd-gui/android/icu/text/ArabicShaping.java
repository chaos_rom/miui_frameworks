package android.icu.text;

public class ArabicShaping
{
    private static final int ALEFTYPE = 32;
    private static final int DESHAPE_MODE = 1;
    public static final int DIGITS_AN2EN = 64;
    public static final int DIGITS_EN2AN = 32;
    public static final int DIGITS_EN2AN_INIT_AL = 128;
    public static final int DIGITS_EN2AN_INIT_LR = 96;
    public static final int DIGITS_MASK = 224;
    public static final int DIGITS_NOOP = 0;
    public static final int DIGIT_TYPE_AN = 0;
    public static final int DIGIT_TYPE_AN_EXTENDED = 256;
    public static final int DIGIT_TYPE_MASK = 256;
    private static final char HAMZA06_CHAR = 'ء';
    private static final char HAMZAFE_CHAR = 'ﺀ';
    private static final int IRRELEVANT = 4;
    public static final int LAMALEF_AUTO = 65536;
    public static final int LAMALEF_BEGIN = 3;
    public static final int LAMALEF_END = 2;
    public static final int LAMALEF_MASK = 65539;
    public static final int LAMALEF_NEAR = 1;
    public static final int LAMALEF_RESIZE = 0;
    private static final char LAMALEF_SPACE_SUB = '￿';
    private static final int LAMTYPE = 16;
    private static final char LAM_CHAR = 'ل';
    public static final int LENGTH_FIXED_SPACES_AT_BEGINNING = 3;
    public static final int LENGTH_FIXED_SPACES_AT_END = 2;
    public static final int LENGTH_FIXED_SPACES_NEAR = 1;
    public static final int LENGTH_GROW_SHRINK = 0;
    public static final int LENGTH_MASK = 65539;
    public static final int LETTERS_MASK = 24;
    public static final int LETTERS_NOOP = 0;
    public static final int LETTERS_SHAPE = 8;
    public static final int LETTERS_SHAPE_TASHKEEL_ISOLATED = 24;
    public static final int LETTERS_UNSHAPE = 16;
    private static final int LINKL = 2;
    private static final int LINKR = 1;
    private static final int LINK_MASK = 3;
    private static final char NEW_TAIL_CHAR = 'ﹳ';
    private static final char OLD_TAIL_CHAR = '​';
    public static final int SEEN_MASK = 7340032;
    public static final int SEEN_TWOCELL_NEAR = 2097152;
    private static final char SHADDA_CHAR = 'ﹼ';
    private static final char SHADDA_TATWEEL_CHAR = 'ﹽ';
    public static final ArabicShaping SHAPER = new ArabicShaping(9);
    private static final int SHAPE_MODE = 0;
    public static final int SHAPE_TAIL_NEW_UNICODE = 134217728;
    public static final int SHAPE_TAIL_TYPE_MASK = 134217728;
    public static final int SPACES_RELATIVE_TO_TEXT_BEGIN_END = 67108864;
    public static final int SPACES_RELATIVE_TO_TEXT_MASK = 67108864;
    private static final char SPACE_CHAR = ' ';
    private static final char SPACE_CHAR_FOR_LAMALEF = '﻿';
    public static final int TASHKEEL_BEGIN = 262144;
    public static final int TASHKEEL_END = 393216;
    public static final int TASHKEEL_MASK = 917504;
    public static final int TASHKEEL_REPLACE_BY_TATWEEL = 786432;
    public static final int TASHKEEL_RESIZE = 524288;
    private static final char TASHKEEL_SPACE_SUB = '￾';
    private static final char TATWEEL_CHAR = 'ـ';
    public static final int TEXT_DIRECTION_LOGICAL = 0;
    public static final int TEXT_DIRECTION_MASK = 4;
    public static final int TEXT_DIRECTION_VISUAL_LTR = 4;
    public static final int TEXT_DIRECTION_VISUAL_RTL = 0;
    public static final int YEHHAMZA_MASK = 58720256;
    public static final int YEHHAMZA_TWOCELL_NEAR = 16777216;
    private static final char YEH_HAMZAFE_CHAR = 'ﺉ';
    private static final char YEH_HAMZA_CHAR = 'ئ';
    private static final int[] araLink;
    private static int[] convertFEto06;
    private static final char[] convertNormalizedLamAlef;
    private static final int[] irrelevantPos;
    private static final int[] presLink;
    private static final int[][][] shapeTable = arrayOfInt;
    private static final int[] tailFamilyIsolatedFinal;
    private static final int[] tashkeelMedial;
    private static final char[] yehHamzaToYeh;
    private boolean isLogical;
    private final int options;
    private boolean spacesRelativeToTextBeginEnd;
    private char tailChar;

    static
    {
        int[] arrayOfInt1 = new int[8];
        arrayOfInt1[0] = 0;
        arrayOfInt1[1] = 2;
        arrayOfInt1[2] = 4;
        arrayOfInt1[3] = 6;
        arrayOfInt1[4] = 8;
        arrayOfInt1[5] = 10;
        arrayOfInt1[6] = 12;
        arrayOfInt1[7] = 14;
        irrelevantPos = arrayOfInt1;
        int[] arrayOfInt2 = new int[14];
        arrayOfInt2[0] = 1;
        arrayOfInt2[1] = 1;
        arrayOfInt2[2] = 0;
        arrayOfInt2[3] = 0;
        arrayOfInt2[4] = 1;
        arrayOfInt2[5] = 1;
        arrayOfInt2[6] = 0;
        arrayOfInt2[7] = 0;
        arrayOfInt2[8] = 1;
        arrayOfInt2[9] = 1;
        arrayOfInt2[10] = 0;
        arrayOfInt2[11] = 0;
        arrayOfInt2[12] = 1;
        arrayOfInt2[13] = 1;
        tailFamilyIsolatedFinal = arrayOfInt2;
        int[] arrayOfInt3 = new int[16];
        arrayOfInt3[0] = 0;
        arrayOfInt3[1] = 1;
        arrayOfInt3[2] = 0;
        arrayOfInt3[3] = 0;
        arrayOfInt3[4] = 0;
        arrayOfInt3[5] = 0;
        arrayOfInt3[6] = 0;
        arrayOfInt3[7] = 1;
        arrayOfInt3[8] = 0;
        arrayOfInt3[9] = 1;
        arrayOfInt3[10] = 0;
        arrayOfInt3[11] = 1;
        arrayOfInt3[12] = 0;
        arrayOfInt3[13] = 1;
        arrayOfInt3[14] = 0;
        arrayOfInt3[15] = 1;
        tashkeelMedial = arrayOfInt3;
        char[] arrayOfChar1 = new char[2];
        arrayOfChar1[0] = -273;
        arrayOfChar1[1] = -272;
        yehHamzaToYeh = arrayOfChar1;
        char[] arrayOfChar2 = new char[4];
        arrayOfChar2[0] = 1570;
        arrayOfChar2[1] = 1571;
        arrayOfChar2[2] = 1573;
        arrayOfChar2[3] = 1575;
        convertNormalizedLamAlef = arrayOfChar2;
        int[] arrayOfInt4 = new int['²'];
        arrayOfInt4[0] = 4385;
        arrayOfInt4[1] = 4897;
        arrayOfInt4[2] = 5377;
        arrayOfInt4[3] = 5921;
        arrayOfInt4[4] = 6403;
        arrayOfInt4[5] = 7457;
        arrayOfInt4[6] = 7939;
        arrayOfInt4[7] = 8961;
        arrayOfInt4[8] = 9475;
        arrayOfInt4[9] = 10499;
        arrayOfInt4[10] = 11523;
        arrayOfInt4[11] = 12547;
        arrayOfInt4[12] = 13571;
        arrayOfInt4[13] = 14593;
        arrayOfInt4[14] = 15105;
        arrayOfInt4[15] = 15617;
        arrayOfInt4[16] = 16129;
        arrayOfInt4[17] = 16643;
        arrayOfInt4[18] = 17667;
        arrayOfInt4[19] = 18691;
        arrayOfInt4[20] = 19715;
        arrayOfInt4[21] = 20739;
        arrayOfInt4[22] = 21763;
        arrayOfInt4[23] = 22787;
        arrayOfInt4[24] = 23811;
        arrayOfInt4[25] = 0;
        arrayOfInt4[26] = 0;
        arrayOfInt4[27] = 0;
        arrayOfInt4[28] = 0;
        arrayOfInt4[29] = 0;
        arrayOfInt4[30] = 3;
        arrayOfInt4[31] = 24835;
        arrayOfInt4[32] = 25859;
        arrayOfInt4[33] = 26883;
        arrayOfInt4[34] = 27923;
        arrayOfInt4[35] = 28931;
        arrayOfInt4[36] = 29955;
        arrayOfInt4[37] = 30979;
        arrayOfInt4[38] = 32001;
        arrayOfInt4[39] = 32513;
        arrayOfInt4[40] = 33027;
        arrayOfInt4[41] = 4;
        arrayOfInt4[42] = 4;
        arrayOfInt4[43] = 4;
        arrayOfInt4[44] = 4;
        arrayOfInt4[45] = 4;
        arrayOfInt4[46] = 4;
        arrayOfInt4[47] = 4;
        arrayOfInt4[48] = 4;
        arrayOfInt4[49] = 4;
        arrayOfInt4[50] = 4;
        arrayOfInt4[51] = 4;
        arrayOfInt4[52] = 0;
        arrayOfInt4[53] = 0;
        arrayOfInt4[54] = 0;
        arrayOfInt4[55] = 0;
        arrayOfInt4[56] = 0;
        arrayOfInt4[57] = 0;
        arrayOfInt4[58] = 34049;
        arrayOfInt4[59] = 34561;
        arrayOfInt4[60] = 35073;
        arrayOfInt4[61] = 35585;
        arrayOfInt4[62] = 0;
        arrayOfInt4[63] = 0;
        arrayOfInt4[64] = 0;
        arrayOfInt4[65] = 0;
        arrayOfInt4[66] = 0;
        arrayOfInt4[67] = 0;
        arrayOfInt4[68] = 0;
        arrayOfInt4[69] = 0;
        arrayOfInt4[70] = 0;
        arrayOfInt4[71] = 0;
        arrayOfInt4[72] = 0;
        arrayOfInt4[73] = 0;
        arrayOfInt4[74] = 0;
        arrayOfInt4[75] = 0;
        arrayOfInt4[76] = 0;
        arrayOfInt4[77] = 0;
        arrayOfInt4[78] = 4;
        arrayOfInt4[79] = 0;
        arrayOfInt4[80] = 33;
        arrayOfInt4[81] = 33;
        arrayOfInt4[82] = 0;
        arrayOfInt4[83] = 33;
        arrayOfInt4[84] = 1;
        arrayOfInt4[85] = 1;
        arrayOfInt4[86] = 3;
        arrayOfInt4[87] = 3;
        arrayOfInt4[88] = 3;
        arrayOfInt4[89] = 3;
        arrayOfInt4[90] = 3;
        arrayOfInt4[91] = 3;
        arrayOfInt4[92] = 3;
        arrayOfInt4[93] = 3;
        arrayOfInt4[94] = 3;
        arrayOfInt4[95] = 3;
        arrayOfInt4[96] = 3;
        arrayOfInt4[97] = 3;
        arrayOfInt4[98] = 3;
        arrayOfInt4[99] = 3;
        arrayOfInt4[100] = 3;
        arrayOfInt4[101] = 3;
        arrayOfInt4[102] = 1;
        arrayOfInt4[103] = 1;
        arrayOfInt4[104] = 1;
        arrayOfInt4[105] = 1;
        arrayOfInt4[106] = 1;
        arrayOfInt4[107] = 1;
        arrayOfInt4[108] = 1;
        arrayOfInt4[109] = 1;
        arrayOfInt4[110] = 1;
        arrayOfInt4[111] = 1;
        arrayOfInt4[112] = 1;
        arrayOfInt4[113] = 1;
        arrayOfInt4[114] = 1;
        arrayOfInt4[115] = 1;
        arrayOfInt4[116] = 1;
        arrayOfInt4[117] = 1;
        arrayOfInt4[118] = 1;
        arrayOfInt4[119] = 1;
        arrayOfInt4[120] = 3;
        arrayOfInt4[121] = 3;
        arrayOfInt4[122] = 3;
        arrayOfInt4[123] = 3;
        arrayOfInt4[124] = 3;
        arrayOfInt4[125] = 3;
        arrayOfInt4[126] = 3;
        arrayOfInt4[127] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 3;
        arrayOfInt4[''] = 1;
        arrayOfInt4[''] = 3;
        arrayOfInt4[' '] = 1;
        arrayOfInt4['¡'] = 1;
        arrayOfInt4['¢'] = 1;
        arrayOfInt4['£'] = 1;
        arrayOfInt4['¤'] = 1;
        arrayOfInt4['¥'] = 1;
        arrayOfInt4['¦'] = 1;
        arrayOfInt4['§'] = 1;
        arrayOfInt4['¨'] = 1;
        arrayOfInt4['©'] = 1;
        arrayOfInt4['ª'] = 3;
        arrayOfInt4['«'] = 1;
        arrayOfInt4['¬'] = 3;
        arrayOfInt4['­'] = 3;
        arrayOfInt4['®'] = 3;
        arrayOfInt4['¯'] = 3;
        arrayOfInt4['°'] = 1;
        arrayOfInt4['±'] = 1;
        araLink = arrayOfInt4;
        int[] arrayOfInt5 = new int[''];
        arrayOfInt5[0] = 3;
        arrayOfInt5[1] = 3;
        arrayOfInt5[2] = 3;
        arrayOfInt5[3] = 0;
        arrayOfInt5[4] = 3;
        arrayOfInt5[5] = 0;
        arrayOfInt5[6] = 3;
        arrayOfInt5[7] = 3;
        arrayOfInt5[8] = 3;
        arrayOfInt5[9] = 3;
        arrayOfInt5[10] = 3;
        arrayOfInt5[11] = 3;
        arrayOfInt5[12] = 3;
        arrayOfInt5[13] = 3;
        arrayOfInt5[14] = 3;
        arrayOfInt5[15] = 3;
        arrayOfInt5[16] = 0;
        arrayOfInt5[17] = 32;
        arrayOfInt5[18] = 33;
        arrayOfInt5[19] = 32;
        arrayOfInt5[20] = 33;
        arrayOfInt5[21] = 0;
        arrayOfInt5[22] = 1;
        arrayOfInt5[23] = 32;
        arrayOfInt5[24] = 33;
        arrayOfInt5[25] = 0;
        arrayOfInt5[26] = 2;
        arrayOfInt5[27] = 3;
        arrayOfInt5[28] = 1;
        arrayOfInt5[29] = 32;
        arrayOfInt5[30] = 33;
        arrayOfInt5[31] = 0;
        arrayOfInt5[32] = 2;
        arrayOfInt5[33] = 3;
        arrayOfInt5[34] = 1;
        arrayOfInt5[35] = 0;
        arrayOfInt5[36] = 1;
        arrayOfInt5[37] = 0;
        arrayOfInt5[38] = 2;
        arrayOfInt5[39] = 3;
        arrayOfInt5[40] = 1;
        arrayOfInt5[41] = 0;
        arrayOfInt5[42] = 2;
        arrayOfInt5[43] = 3;
        arrayOfInt5[44] = 1;
        arrayOfInt5[45] = 0;
        arrayOfInt5[46] = 2;
        arrayOfInt5[47] = 3;
        arrayOfInt5[48] = 1;
        arrayOfInt5[49] = 0;
        arrayOfInt5[50] = 2;
        arrayOfInt5[51] = 3;
        arrayOfInt5[52] = 1;
        arrayOfInt5[53] = 0;
        arrayOfInt5[54] = 2;
        arrayOfInt5[55] = 3;
        arrayOfInt5[56] = 1;
        arrayOfInt5[57] = 0;
        arrayOfInt5[58] = 1;
        arrayOfInt5[59] = 0;
        arrayOfInt5[60] = 1;
        arrayOfInt5[61] = 0;
        arrayOfInt5[62] = 1;
        arrayOfInt5[63] = 0;
        arrayOfInt5[64] = 1;
        arrayOfInt5[65] = 0;
        arrayOfInt5[66] = 2;
        arrayOfInt5[67] = 3;
        arrayOfInt5[68] = 1;
        arrayOfInt5[69] = 0;
        arrayOfInt5[70] = 2;
        arrayOfInt5[71] = 3;
        arrayOfInt5[72] = 1;
        arrayOfInt5[73] = 0;
        arrayOfInt5[74] = 2;
        arrayOfInt5[75] = 3;
        arrayOfInt5[76] = 1;
        arrayOfInt5[77] = 0;
        arrayOfInt5[78] = 2;
        arrayOfInt5[79] = 3;
        arrayOfInt5[80] = 1;
        arrayOfInt5[81] = 0;
        arrayOfInt5[82] = 2;
        arrayOfInt5[83] = 3;
        arrayOfInt5[84] = 1;
        arrayOfInt5[85] = 0;
        arrayOfInt5[86] = 2;
        arrayOfInt5[87] = 3;
        arrayOfInt5[88] = 1;
        arrayOfInt5[89] = 0;
        arrayOfInt5[90] = 2;
        arrayOfInt5[91] = 3;
        arrayOfInt5[92] = 1;
        arrayOfInt5[93] = 0;
        arrayOfInt5[94] = 2;
        arrayOfInt5[95] = 3;
        arrayOfInt5[96] = 1;
        arrayOfInt5[97] = 0;
        arrayOfInt5[98] = 2;
        arrayOfInt5[99] = 3;
        arrayOfInt5[100] = 1;
        arrayOfInt5[101] = 0;
        arrayOfInt5[102] = 2;
        arrayOfInt5[103] = 3;
        arrayOfInt5[104] = 1;
        arrayOfInt5[105] = 0;
        arrayOfInt5[106] = 2;
        arrayOfInt5[107] = 3;
        arrayOfInt5[108] = 1;
        arrayOfInt5[109] = 16;
        arrayOfInt5[110] = 18;
        arrayOfInt5[111] = 19;
        arrayOfInt5[112] = 17;
        arrayOfInt5[113] = 0;
        arrayOfInt5[114] = 2;
        arrayOfInt5[115] = 3;
        arrayOfInt5[116] = 1;
        arrayOfInt5[117] = 0;
        arrayOfInt5[118] = 2;
        arrayOfInt5[119] = 3;
        arrayOfInt5[120] = 1;
        arrayOfInt5[121] = 0;
        arrayOfInt5[122] = 2;
        arrayOfInt5[123] = 3;
        arrayOfInt5[124] = 1;
        arrayOfInt5[125] = 0;
        arrayOfInt5[126] = 1;
        arrayOfInt5[127] = 0;
        arrayOfInt5[''] = 1;
        arrayOfInt5[''] = 0;
        arrayOfInt5[''] = 2;
        arrayOfInt5[''] = 3;
        arrayOfInt5[''] = 1;
        arrayOfInt5[''] = 0;
        arrayOfInt5[''] = 1;
        arrayOfInt5[''] = 0;
        arrayOfInt5[''] = 1;
        arrayOfInt5[''] = 0;
        arrayOfInt5[''] = 1;
        arrayOfInt5[''] = 0;
        arrayOfInt5[''] = 1;
        presLink = arrayOfInt5;
        int[] arrayOfInt6 = new int[''];
        arrayOfInt6[0] = 1611;
        arrayOfInt6[1] = 1611;
        arrayOfInt6[2] = 1612;
        arrayOfInt6[3] = 1612;
        arrayOfInt6[4] = 1613;
        arrayOfInt6[5] = 1613;
        arrayOfInt6[6] = 1614;
        arrayOfInt6[7] = 1614;
        arrayOfInt6[8] = 1615;
        arrayOfInt6[9] = 1615;
        arrayOfInt6[10] = 1616;
        arrayOfInt6[11] = 1616;
        arrayOfInt6[12] = 1617;
        arrayOfInt6[13] = 1617;
        arrayOfInt6[14] = 1618;
        arrayOfInt6[15] = 1618;
        arrayOfInt6[16] = 1569;
        arrayOfInt6[17] = 1570;
        arrayOfInt6[18] = 1570;
        arrayOfInt6[19] = 1571;
        arrayOfInt6[20] = 1571;
        arrayOfInt6[21] = 1572;
        arrayOfInt6[22] = 1572;
        arrayOfInt6[23] = 1573;
        arrayOfInt6[24] = 1573;
        arrayOfInt6[25] = 1574;
        arrayOfInt6[26] = 1574;
        arrayOfInt6[27] = 1574;
        arrayOfInt6[28] = 1574;
        arrayOfInt6[29] = 1575;
        arrayOfInt6[30] = 1575;
        arrayOfInt6[31] = 1576;
        arrayOfInt6[32] = 1576;
        arrayOfInt6[33] = 1576;
        arrayOfInt6[34] = 1576;
        arrayOfInt6[35] = 1577;
        arrayOfInt6[36] = 1577;
        arrayOfInt6[37] = 1578;
        arrayOfInt6[38] = 1578;
        arrayOfInt6[39] = 1578;
        arrayOfInt6[40] = 1578;
        arrayOfInt6[41] = 1579;
        arrayOfInt6[42] = 1579;
        arrayOfInt6[43] = 1579;
        arrayOfInt6[44] = 1579;
        arrayOfInt6[45] = 1580;
        arrayOfInt6[46] = 1580;
        arrayOfInt6[47] = 1580;
        arrayOfInt6[48] = 1580;
        arrayOfInt6[49] = 1581;
        arrayOfInt6[50] = 1581;
        arrayOfInt6[51] = 1581;
        arrayOfInt6[52] = 1581;
        arrayOfInt6[53] = 1582;
        arrayOfInt6[54] = 1582;
        arrayOfInt6[55] = 1582;
        arrayOfInt6[56] = 1582;
        arrayOfInt6[57] = 1583;
        arrayOfInt6[58] = 1583;
        arrayOfInt6[59] = 1584;
        arrayOfInt6[60] = 1584;
        arrayOfInt6[61] = 1585;
        arrayOfInt6[62] = 1585;
        arrayOfInt6[63] = 1586;
        arrayOfInt6[64] = 1586;
        arrayOfInt6[65] = 1587;
        arrayOfInt6[66] = 1587;
        arrayOfInt6[67] = 1587;
        arrayOfInt6[68] = 1587;
        arrayOfInt6[69] = 1588;
        arrayOfInt6[70] = 1588;
        arrayOfInt6[71] = 1588;
        arrayOfInt6[72] = 1588;
        arrayOfInt6[73] = 1589;
        arrayOfInt6[74] = 1589;
        arrayOfInt6[75] = 1589;
        arrayOfInt6[76] = 1589;
        arrayOfInt6[77] = 1590;
        arrayOfInt6[78] = 1590;
        arrayOfInt6[79] = 1590;
        arrayOfInt6[80] = 1590;
        arrayOfInt6[81] = 1591;
        arrayOfInt6[82] = 1591;
        arrayOfInt6[83] = 1591;
        arrayOfInt6[84] = 1591;
        arrayOfInt6[85] = 1592;
        arrayOfInt6[86] = 1592;
        arrayOfInt6[87] = 1592;
        arrayOfInt6[88] = 1592;
        arrayOfInt6[89] = 1593;
        arrayOfInt6[90] = 1593;
        arrayOfInt6[91] = 1593;
        arrayOfInt6[92] = 1593;
        arrayOfInt6[93] = 1594;
        arrayOfInt6[94] = 1594;
        arrayOfInt6[95] = 1594;
        arrayOfInt6[96] = 1594;
        arrayOfInt6[97] = 1601;
        arrayOfInt6[98] = 1601;
        arrayOfInt6[99] = 1601;
        arrayOfInt6[100] = 1601;
        arrayOfInt6[101] = 1602;
        arrayOfInt6[102] = 1602;
        arrayOfInt6[103] = 1602;
        arrayOfInt6[104] = 1602;
        arrayOfInt6[105] = 1603;
        arrayOfInt6[106] = 1603;
        arrayOfInt6[107] = 1603;
        arrayOfInt6[108] = 1603;
        arrayOfInt6[109] = 1604;
        arrayOfInt6[110] = 1604;
        arrayOfInt6[111] = 1604;
        arrayOfInt6[112] = 1604;
        arrayOfInt6[113] = 1605;
        arrayOfInt6[114] = 1605;
        arrayOfInt6[115] = 1605;
        arrayOfInt6[116] = 1605;
        arrayOfInt6[117] = 1606;
        arrayOfInt6[118] = 1606;
        arrayOfInt6[119] = 1606;
        arrayOfInt6[120] = 1606;
        arrayOfInt6[121] = 1607;
        arrayOfInt6[122] = 1607;
        arrayOfInt6[123] = 1607;
        arrayOfInt6[124] = 1607;
        arrayOfInt6[125] = 1608;
        arrayOfInt6[126] = 1608;
        arrayOfInt6[127] = 1609;
        arrayOfInt6[''] = 1609;
        arrayOfInt6[''] = 1610;
        arrayOfInt6[''] = 1610;
        arrayOfInt6[''] = 1610;
        arrayOfInt6[''] = 1610;
        arrayOfInt6[''] = 1628;
        arrayOfInt6[''] = 1628;
        arrayOfInt6[''] = 1629;
        arrayOfInt6[''] = 1629;
        arrayOfInt6[''] = 1630;
        arrayOfInt6[''] = 1630;
        arrayOfInt6[''] = 1631;
        arrayOfInt6[''] = 1631;
        convertFEto06 = arrayOfInt6;
        int[][][] arrayOfInt = new int[4][][];
        int[][] arrayOfInt7 = new int[4][];
        int[] arrayOfInt8 = new int[4];
        arrayOfInt8[0] = 0;
        arrayOfInt8[1] = 0;
        arrayOfInt8[2] = 0;
        arrayOfInt8[3] = 0;
        arrayOfInt7[0] = arrayOfInt8;
        int[] arrayOfInt9 = new int[4];
        arrayOfInt9[0] = 0;
        arrayOfInt9[1] = 0;
        arrayOfInt9[2] = 0;
        arrayOfInt9[3] = 0;
        arrayOfInt7[1] = arrayOfInt9;
        int[] arrayOfInt10 = new int[4];
        arrayOfInt10[0] = 0;
        arrayOfInt10[1] = 1;
        arrayOfInt10[2] = 0;
        arrayOfInt10[3] = 3;
        arrayOfInt7[2] = arrayOfInt10;
        int[] arrayOfInt11 = new int[4];
        arrayOfInt11[0] = 0;
        arrayOfInt11[1] = 1;
        arrayOfInt11[2] = 0;
        arrayOfInt11[3] = 1;
        arrayOfInt7[3] = arrayOfInt11;
        arrayOfInt[0] = arrayOfInt7;
        int[][] arrayOfInt12 = new int[4][];
        int[] arrayOfInt13 = new int[4];
        arrayOfInt13[0] = 0;
        arrayOfInt13[1] = 0;
        arrayOfInt13[2] = 2;
        arrayOfInt13[3] = 2;
        arrayOfInt12[0] = arrayOfInt13;
        int[] arrayOfInt14 = new int[4];
        arrayOfInt14[0] = 0;
        arrayOfInt14[1] = 0;
        arrayOfInt14[2] = 1;
        arrayOfInt14[3] = 2;
        arrayOfInt12[1] = arrayOfInt14;
        int[] arrayOfInt15 = new int[4];
        arrayOfInt15[0] = 0;
        arrayOfInt15[1] = 1;
        arrayOfInt15[2] = 1;
        arrayOfInt15[3] = 2;
        arrayOfInt12[2] = arrayOfInt15;
        int[] arrayOfInt16 = new int[4];
        arrayOfInt16[0] = 0;
        arrayOfInt16[1] = 1;
        arrayOfInt16[2] = 1;
        arrayOfInt16[3] = 3;
        arrayOfInt12[3] = arrayOfInt16;
        arrayOfInt[1] = arrayOfInt12;
        int[][] arrayOfInt17 = new int[4][];
        int[] arrayOfInt18 = new int[4];
        arrayOfInt18[0] = 0;
        arrayOfInt18[1] = 0;
        arrayOfInt18[2] = 0;
        arrayOfInt18[3] = 0;
        arrayOfInt17[0] = arrayOfInt18;
        int[] arrayOfInt19 = new int[4];
        arrayOfInt19[0] = 0;
        arrayOfInt19[1] = 0;
        arrayOfInt19[2] = 0;
        arrayOfInt19[3] = 0;
        arrayOfInt17[1] = arrayOfInt19;
        int[] arrayOfInt20 = new int[4];
        arrayOfInt20[0] = 0;
        arrayOfInt20[1] = 1;
        arrayOfInt20[2] = 0;
        arrayOfInt20[3] = 3;
        arrayOfInt17[2] = arrayOfInt20;
        int[] arrayOfInt21 = new int[4];
        arrayOfInt21[0] = 0;
        arrayOfInt21[1] = 1;
        arrayOfInt21[2] = 0;
        arrayOfInt21[3] = 3;
        arrayOfInt17[3] = arrayOfInt21;
        arrayOfInt[2] = arrayOfInt17;
        int[][] arrayOfInt22 = new int[4][];
        int[] arrayOfInt23 = new int[4];
        arrayOfInt23[0] = 0;
        arrayOfInt23[1] = 0;
        arrayOfInt23[2] = 1;
        arrayOfInt23[3] = 2;
        arrayOfInt22[0] = arrayOfInt23;
        int[] arrayOfInt24 = new int[4];
        arrayOfInt24[0] = 0;
        arrayOfInt24[1] = 0;
        arrayOfInt24[2] = 1;
        arrayOfInt24[3] = 2;
        arrayOfInt22[1] = arrayOfInt24;
        int[] arrayOfInt25 = new int[4];
        arrayOfInt25[0] = 0;
        arrayOfInt25[1] = 1;
        arrayOfInt25[2] = 1;
        arrayOfInt25[3] = 2;
        arrayOfInt22[2] = arrayOfInt25;
        int[] arrayOfInt26 = new int[4];
        arrayOfInt26[0] = 0;
        arrayOfInt26[1] = 1;
        arrayOfInt26[2] = 1;
        arrayOfInt26[3] = 3;
        arrayOfInt22[3] = arrayOfInt26;
        arrayOfInt[3] = arrayOfInt22;
    }

    public ArabicShaping(int paramInt)
    {
        this.options = paramInt;
        if ((paramInt & 0xE0) > 128)
            throw new IllegalArgumentException("bad DIGITS options");
        boolean bool2;
        if ((paramInt & 0x4) == 0)
        {
            bool2 = bool1;
            this.isLogical = bool2;
            if ((paramInt & 0x4000000) != 67108864)
                break label80;
            label54: this.spacesRelativeToTextBeginEnd = bool1;
            if ((paramInt & 0x8000000) != 134217728)
                break label85;
        }
        label80: label85: for (this.tailChar = 65139; ; this.tailChar = '​')
        {
            return;
            bool2 = false;
            break;
            bool1 = false;
            break label54;
        }
    }

    private int calculateSize(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = paramInt2;
        switch (0x18 & this.options)
        {
        default:
        case 8:
        case 24:
        case 16:
        }
        while (true)
        {
            return i;
            if (this.isLogical)
            {
                int i1 = paramInt1;
                int i2 = -1 + (paramInt1 + paramInt2);
                while (i1 < i2)
                {
                    if (((paramArrayOfChar[i1] == 'ل') && (isAlefChar(paramArrayOfChar[(i1 + 1)]))) || (isTashkeelCharFE(paramArrayOfChar[i1])))
                        i--;
                    i1++;
                }
            }
            int m = paramInt1 + 1;
            int n = paramInt1 + paramInt2;
            while (m < n)
            {
                if (((paramArrayOfChar[m] == 'ل') && (isAlefChar(paramArrayOfChar[(m - 1)]))) || (isTashkeelCharFE(paramArrayOfChar[m])))
                    i--;
                m++;
            }
            int j = paramInt1;
            int k = paramInt1 + paramInt2;
            while (j < k)
            {
                if (isLamAlefChar(paramArrayOfChar[j]))
                    i++;
                j++;
            }
        }
    }

    private static char changeLamAlef(char paramChar)
    {
        char c;
        switch (paramChar)
        {
        case 'ؤ':
        case 'ئ':
        default:
            c = '\000';
        case 'آ':
        case 'أ':
        case 'إ':
        case 'ا':
        }
        while (true)
        {
            return c;
            int i = 1628;
            continue;
            i = 1629;
            continue;
            i = 1630;
            continue;
            i = 1631;
        }
    }

    public static int countSpaceSub(char[] paramArrayOfChar, int paramInt, char paramChar)
    {
        int i = 0;
        int j = 0;
        while (i < paramInt)
        {
            if (paramArrayOfChar[i] == paramChar)
                j++;
            i++;
        }
        return j;
    }

    private static int countSpacesLeft(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        int j = paramInt1 + paramInt2;
        while (true)
        {
            if (i < j)
            {
                if (paramArrayOfChar[i] != ' ')
                    paramInt2 = i - paramInt1;
            }
            else
                return paramInt2;
            i++;
        }
    }

    private static int countSpacesRight(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = paramInt1 + paramInt2;
        do
        {
            i--;
            if (i < paramInt1)
                break;
        }
        while (paramArrayOfChar[i] == ' ');
        paramInt2 = -1 + (paramInt1 + paramInt2) - i;
        return paramInt2;
    }

    private int deShapeUnicode(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3)
        throws ArabicShaping.ArabicShapingException
    {
        int i = deshapeNormalize(paramArrayOfChar, paramInt1, paramInt2);
        if (i != 0);
        for (int j = expandCompositChar(paramArrayOfChar, paramInt1, paramInt2, i, 1); ; j = paramInt2)
            return j;
    }

    private int deshapeNormalize(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = 0;
        int j;
        int k;
        label33: int m;
        label42: int i1;
        if ((0x3800000 & this.options) == 16777216)
        {
            j = 1;
            if ((0x700000 & this.options) != 2097152)
                break label123;
            k = 1;
            m = paramInt1;
            int n = m + paramInt2;
            if (m >= n)
                break label215;
            i1 = paramArrayOfChar[m];
            if ((j != 1) || ((i1 != 1569) && (i1 != 65152)) || (m >= paramInt2 - 1) || (!isAlefMaksouraChar(paramArrayOfChar[(m + 1)])))
                break label129;
            paramArrayOfChar[m] = ' ';
            paramArrayOfChar[(m + 1)] = 'ئ';
        }
        while (true)
        {
            m++;
            break label42;
            j = 0;
            break;
            label123: k = 0;
            break label33;
            label129: if ((k == 1) && (isTailChar(i1)) && (m < paramInt2 - 1) && (isSeenTailFamilyChar(paramArrayOfChar[(m + 1)]) == 1))
            {
                paramArrayOfChar[m] = ' ';
            }
            else if ((i1 >= 65136) && (i1 <= 65276))
            {
                if (isLamAlefChar(i1))
                    i++;
                paramArrayOfChar[m] = ((char)convertFEto06[(i1 - 65136)]);
            }
        }
        label215: return i;
    }

    private int expandCompositChar(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        throws ArabicShaping.ArabicShapingException
    {
        int i = 0x10003 & this.options;
        int j = 0x700000 & this.options;
        int k = 0x3800000 & this.options;
        if ((!this.isLogical) && (!this.spacesRelativeToTextBeginEnd))
            switch (i)
            {
            default:
            case 3:
            case 2:
            }
        while (true)
            if (paramInt4 == 1)
                if (i == 65536)
                {
                    if (this.isLogical)
                    {
                        boolean bool2 = expandCompositCharAtEnd(paramArrayOfChar, paramInt1, paramInt2, paramInt3);
                        if (bool2)
                            bool2 = expandCompositCharAtBegin(paramArrayOfChar, paramInt1, paramInt2, paramInt3);
                        if (bool2)
                            bool2 = expandCompositCharAtNear(paramArrayOfChar, paramInt1, paramInt2, 0, 0, 1);
                        if (bool2)
                        {
                            throw new ArabicShapingException("No spacefor lamalef");
                            i = 2;
                            continue;
                            i = 3;
                        }
                    }
                    else
                    {
                        boolean bool1 = expandCompositCharAtBegin(paramArrayOfChar, paramInt1, paramInt2, paramInt3);
                        if (bool1)
                            bool1 = expandCompositCharAtEnd(paramArrayOfChar, paramInt1, paramInt2, paramInt3);
                        if (bool1)
                            bool1 = expandCompositCharAtNear(paramArrayOfChar, paramInt1, paramInt2, 0, 0, 1);
                        if (bool1)
                            throw new ArabicShapingException("No spacefor lamalef");
                    }
                }
                else if (i == 2)
                {
                    if (expandCompositCharAtEnd(paramArrayOfChar, paramInt1, paramInt2, paramInt3))
                        throw new ArabicShapingException("No spacefor lamalef");
                }
                else if (i == 3)
                {
                    if (expandCompositCharAtBegin(paramArrayOfChar, paramInt1, paramInt2, paramInt3))
                        throw new ArabicShapingException("No spacefor lamalef");
                }
                else if (i == 1)
                {
                    if (expandCompositCharAtNear(paramArrayOfChar, paramInt1, paramInt2, 0, 0, 1))
                        throw new ArabicShapingException("No spacefor lamalef");
                }
                else if (i == 0)
                {
                    int m = paramInt1 + paramInt2;
                    int n = m + paramInt3;
                    while (true)
                    {
                        m--;
                        if (m < paramInt1)
                            break;
                        int i1 = paramArrayOfChar[m];
                        if (isNormalizedLamAlefChar(i1))
                        {
                            int i2 = n - 1;
                            paramArrayOfChar[i2] = 'ل';
                            n = i2 - 1;
                            paramArrayOfChar[n] = convertNormalizedLamAlef[(i1 + -1628)];
                        }
                        else
                        {
                            n--;
                            paramArrayOfChar[n] = i1;
                        }
                    }
                    paramInt2 += paramInt3;
                }
        do
        {
            return paramInt2;
            if ((j == 2097152) && (expandCompositCharAtNear(paramArrayOfChar, paramInt1, paramInt2, 0, 1, 0)))
                throw new ArabicShapingException("No space for Seen tail expansion");
        }
        while ((k != 16777216) || (!expandCompositCharAtNear(paramArrayOfChar, paramInt1, paramInt2, 1, 0, 0)));
        throw new ArabicShapingException("No space for YehHamza expansion");
    }

    private boolean expandCompositCharAtBegin(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt3 > countSpacesRight(paramArrayOfChar, paramInt1, paramInt2));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            int i = paramInt1 + paramInt2 - paramInt3;
            int j = paramInt1 + paramInt2;
            while (true)
            {
                i--;
                if (i < paramInt1)
                    break;
                int k = paramArrayOfChar[i];
                if (isNormalizedLamAlefChar(k))
                {
                    int m = j - 1;
                    paramArrayOfChar[m] = 'ل';
                    j = m - 1;
                    paramArrayOfChar[j] = convertNormalizedLamAlef[(k + -1628)];
                }
                else
                {
                    j--;
                    paramArrayOfChar[j] = k;
                }
            }
        }
    }

    private boolean expandCompositCharAtEnd(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt3 > countSpacesLeft(paramArrayOfChar, paramInt1, paramInt2));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            int i = paramInt1 + paramInt3;
            int j = paramInt1 + paramInt2;
            int k = paramInt1;
            if (i < j)
            {
                int m = paramArrayOfChar[i];
                int n;
                if (isNormalizedLamAlefChar(m))
                {
                    int i1 = k + 1;
                    paramArrayOfChar[k] = convertNormalizedLamAlef[(m + -1628)];
                    int i2 = i1 + 1;
                    paramArrayOfChar[i1] = 'ل';
                    n = i2;
                }
                while (true)
                {
                    i++;
                    k = n;
                    break;
                    n = k + 1;
                    paramArrayOfChar[k] = m;
                }
            }
        }
    }

    private boolean expandCompositCharAtNear(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        boolean bool;
        if (isNormalizedLamAlefChar(paramArrayOfChar[paramInt1]))
            bool = true;
        while (true)
        {
            return bool;
            int i = paramInt1 + paramInt2;
            while (true)
            {
                i--;
                if (i < paramInt1)
                    break label211;
                int j = paramArrayOfChar[i];
                if ((paramInt5 == 1) && (isNormalizedLamAlefChar(j)))
                {
                    if ((i > paramInt1) && (paramArrayOfChar[(i - 1)] == ' '))
                    {
                        paramArrayOfChar[i] = 'ل';
                        i--;
                        paramArrayOfChar[i] = convertNormalizedLamAlef[(j + -1628)];
                        continue;
                    }
                    bool = true;
                    break;
                }
                if ((paramInt4 == 1) && (isSeenTailFamilyChar(j) == 1))
                {
                    if ((i > paramInt1) && (paramArrayOfChar[(i - 1)] == ' '))
                    {
                        paramArrayOfChar[(i - 1)] = this.tailChar;
                        continue;
                    }
                    bool = true;
                    break;
                }
                if ((paramInt3 == 1) && (isYehHamzaChar(j)))
                {
                    if ((i <= paramInt1) || (paramArrayOfChar[(i - 1)] != ' '))
                        break label205;
                    paramArrayOfChar[i] = yehHamzaToYeh[(j - 65161)];
                    paramArrayOfChar[(i - 1)] = 65152;
                }
            }
            label205: bool = true;
            continue;
            label211: bool = false;
        }
    }

    public static int flipArray(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3)
    {
        int k;
        if (paramInt3 > paramInt1)
        {
            int j = paramInt3;
            int m;
            for (k = paramInt1; j < paramInt2; k = m)
            {
                m = k + 1;
                int n = j + 1;
                paramArrayOfChar[k] = paramArrayOfChar[j];
                j = n;
            }
        }
        for (int i = paramInt2; ; i = k)
            return i;
    }

    private static int getLink(char paramChar)
    {
        int i;
        if ((paramChar >= 'آ') && (paramChar <= 'ۓ'))
            i = araLink[(paramChar + '\0������6')];
        while (true)
        {
            return i;
            if (paramChar == '‍')
                i = 3;
            else if ((paramChar >= '⁭') && (paramChar <= '⁯'))
                i = 4;
            else if ((paramChar >= 65136) && (paramChar <= 65276))
                i = presLink[(paramChar - 65136)];
            else
                i = 0;
        }
    }

    private int handleGeneratedSpaces(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = 0x10003 & this.options;
        int j = 0xE0000 & this.options;
        int k = 0;
        int m = 0;
        int n;
        int i1;
        if (!this.isLogical)
        {
            n = 1;
            if (this.spacesRelativeToTextBeginEnd)
                break label153;
            i1 = 1;
            label44: if ((n & i1) != 0)
                switch (i)
                {
                default:
                    label76: switch (j)
                    {
                    default:
                    case 262144:
                    case 393216:
                    }
                    break;
                case 3:
                case 2:
                }
        }
        while (true)
        {
            if (i != 1)
                break label185;
            int i13 = paramInt1;
            int i14 = i13 + paramInt2;
            while (i13 < i14)
            {
                if (paramArrayOfChar[i13] == 65535)
                    paramArrayOfChar[i13] = 65279;
                i13++;
            }
            n = 0;
            break;
            label153: i1 = 0;
            break label44;
            i = 2;
            break label76;
            i = 3;
            break label76;
            j = 393216;
            continue;
            j = 262144;
        }
        label185: int i2 = paramInt1 + paramInt2;
        int i3 = countSpaceSub(paramArrayOfChar, paramInt2, 65535);
        int i4 = countSpaceSub(paramArrayOfChar, paramInt2, 65534);
        if (i == 2)
            k = 1;
        if (j == 393216)
            m = 1;
        if ((k != 0) && (i == 2))
        {
            shiftArray(paramArrayOfChar, paramInt1, i2, 65535);
            while (i3 > paramInt1)
            {
                i3--;
                paramArrayOfChar[i3] = ' ';
            }
        }
        if ((m != 0) && (j == 393216))
        {
            shiftArray(paramArrayOfChar, paramInt1, i2, 65534);
            while (i4 > paramInt1)
            {
                i4--;
                paramArrayOfChar[i4] = ' ';
            }
        }
        int i5 = 0;
        int i6 = 0;
        if (i == 0)
            i5 = 1;
        if (j == 524288)
            i6 = 1;
        if ((i5 != 0) && (i == 0))
        {
            shiftArray(paramArrayOfChar, paramInt1, i2, 65535);
            i3 = flipArray(paramArrayOfChar, paramInt1, i2, i3);
            paramInt2 = i3 - paramInt1;
        }
        if ((i6 != 0) && (j == 524288))
        {
            shiftArray(paramArrayOfChar, paramInt1, i2, 65534);
            i4 = flipArray(paramArrayOfChar, paramInt1, i2, i4);
            paramInt2 = i4 - paramInt1;
        }
        int i7 = 0;
        int i8 = 0;
        if ((i == 3) || (i == 65536))
            i7 = 1;
        if (j == 262144)
            i8 = 1;
        int i11;
        if ((i7 != 0) && ((i == 3) || (i == 65536)))
        {
            shiftArray(paramArrayOfChar, paramInt1, i2, 65535);
            int i12;
            for (i11 = flipArray(paramArrayOfChar, paramInt1, i2, i3); i11 < i2; i11 = i12)
            {
                i12 = i11 + 1;
                paramArrayOfChar[i11] = ' ';
            }
        }
        if ((i8 != 0) && (j == 262144))
        {
            shiftArray(paramArrayOfChar, paramInt1, i2, 65534);
            int i10;
            for (int i9 = flipArray(paramArrayOfChar, paramInt1, i2, i4); i9 < i2; i9 = i10)
            {
                i10 = i9 + 1;
                paramArrayOfChar[i9] = ' ';
            }
        }
        return paramInt2;
    }

    private static int handleTashkeelWithTatweel(char[] paramArrayOfChar, int paramInt)
    {
        int i = 0;
        if (i < paramInt)
        {
            if (isTashkeelOnTatweelChar(paramArrayOfChar[i]) == 1)
                paramArrayOfChar[i] = 'ـ';
            while (true)
            {
                i++;
                break;
                if (isTashkeelOnTatweelChar(paramArrayOfChar[i]) == 2)
                    paramArrayOfChar[i] = 65149;
                else if ((isIsolatedTashkeelChar(paramArrayOfChar[i]) == 1) && (paramArrayOfChar[i] != 65148))
                    paramArrayOfChar[i] = ' ';
            }
        }
        return paramInt;
    }

    private int internalShape(char[] paramArrayOfChar1, int paramInt1, int paramInt2, char[] paramArrayOfChar2, int paramInt3, int paramInt4)
        throws ArabicShaping.ArabicShapingException
    {
        if (paramInt2 == 0);
        for (paramInt2 = 0; ; paramInt2 = calculateSize(paramArrayOfChar1, paramInt1, paramInt2))
            do
            {
                return paramInt2;
                if (paramInt4 != 0)
                    break;
            }
            while (((0x18 & this.options) == 0) || ((0x10003 & this.options) != 0));
        char[] arrayOfChar = new char[paramInt2 * 2];
        System.arraycopy(paramArrayOfChar1, paramInt1, arrayOfChar, 0, paramInt2);
        if (this.isLogical)
            invertBuffer(arrayOfChar, 0, paramInt2);
        int i = paramInt2;
        switch (0x18 & this.options)
        {
        default:
        case 24:
        case 8:
        case 16:
        }
        while (i > paramInt4)
        {
            throw new ArabicShapingException("not enough room for result data");
            i = shapeUnicode(arrayOfChar, 0, paramInt2, paramInt4, 1);
            continue;
            if (((0xE0000 & this.options) > 0) && ((0xE0000 & this.options) != 786432))
            {
                i = shapeUnicode(arrayOfChar, 0, paramInt2, paramInt4, 2);
            }
            else
            {
                i = shapeUnicode(arrayOfChar, 0, paramInt2, paramInt4, 0);
                if ((0xE0000 & this.options) == 786432)
                {
                    i = handleTashkeelWithTatweel(arrayOfChar, paramInt2);
                    continue;
                    i = deShapeUnicode(arrayOfChar, 0, paramInt2, paramInt4);
                }
            }
        }
        int j;
        if ((0xE0 & this.options) != 0)
        {
            j = 48;
            switch (0x100 & this.options)
            {
            default:
                label292: switch (0xE0 & this.options)
                {
                default:
                case 32:
                case 64:
                case 96:
                case 128:
                }
                break;
            case 0:
            case 256:
            }
        }
        while (true)
        {
            if (this.isLogical)
                invertBuffer(arrayOfChar, 0, i);
            System.arraycopy(arrayOfChar, 0, paramArrayOfChar2, paramInt3, i);
            paramInt2 = i;
            break;
            j = 1632;
            break label292;
            j = 1776;
            break label292;
            int i2 = j + -48;
            for (int i3 = 0; i3 < i; i3++)
            {
                int i4 = arrayOfChar[i3];
                if ((i4 <= 57) && (i4 >= 48))
                    arrayOfChar[i3] = (i2 + arrayOfChar[i3]);
            }
            int k = j + 9;
            int m = 48 - j;
            for (int n = 0; n < i; n++)
            {
                int i1 = arrayOfChar[n];
                if ((i1 <= k) && (i1 >= j))
                    arrayOfChar[n] = (m + arrayOfChar[n]);
            }
            shapeToArabicDigitsWithContext(arrayOfChar, 0, i, j, false);
            continue;
            shapeToArabicDigitsWithContext(arrayOfChar, 0, i, j, true);
        }
    }

    private static void invertBuffer(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        for (int j = -1 + (paramInt1 + paramInt2); i < j; j--)
        {
            int k = paramArrayOfChar[i];
            paramArrayOfChar[i] = paramArrayOfChar[j];
            paramArrayOfChar[j] = k;
            i++;
        }
    }

    private static boolean isAlefChar(char paramChar)
    {
        if ((paramChar == 'آ') || (paramChar == 'أ') || (paramChar == 'إ') || (paramChar == 'ا'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isAlefMaksouraChar(char paramChar)
    {
        if ((paramChar == 65263) || (paramChar == 65264) || (paramChar == 'ى'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static int isIsolatedTashkeelChar(char paramChar)
    {
        int i;
        if ((paramChar >= 65136) && (paramChar <= 65151) && (paramChar != 65139) && (paramChar != 65141))
            i = 1 - tashkeelMedial[(paramChar - 65136)];
        while (true)
        {
            return i;
            if ((paramChar >= 64606) && (paramChar <= 64611))
                i = 1;
            else
                i = 0;
        }
    }

    private static boolean isLamAlefChar(char paramChar)
    {
        if ((paramChar >= 65269) && (paramChar <= 65276));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isNormalizedLamAlefChar(char paramChar)
    {
        if ((paramChar >= 'ٜ') && (paramChar <= 'ٟ'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static int isSeenFamilyChar(char paramChar)
    {
        if ((paramChar >= 'س') && (paramChar <= 'ض'));
        for (int i = 1; ; i = 0)
            return i;
    }

    private static int isSeenTailFamilyChar(char paramChar)
    {
        if ((paramChar >= 65201) && (paramChar < 65215));
        for (int i = tailFamilyIsolatedFinal[(paramChar - 65201)]; ; i = 0)
            return i;
    }

    private static boolean isTailChar(char paramChar)
    {
        if ((paramChar == '​') || (paramChar == 65139));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isTashkeelChar(char paramChar)
    {
        if ((paramChar >= 'ً') && (paramChar <= 'ْ'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isTashkeelCharFE(char paramChar)
    {
        if ((paramChar != 65141) && (paramChar >= 65136) && (paramChar <= 65151));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static int isTashkeelOnTatweelChar(char paramChar)
    {
        int i;
        if ((paramChar >= 65136) && (paramChar <= 65151) && (paramChar != 65139) && (paramChar != 65141) && (paramChar != 65149))
            i = tashkeelMedial[(paramChar - 65136)];
        while (true)
        {
            return i;
            if (((paramChar >= 64754) && (paramChar <= 64756)) || (paramChar == 65149))
                i = 2;
            else
                i = 0;
        }
    }

    private static boolean isYehHamzaChar(char paramChar)
    {
        if ((paramChar == 65161) || (paramChar == 65162));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private int normalize(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = 0;
        int j = paramInt1;
        int k = j + paramInt2;
        while (j < k)
        {
            int m = paramArrayOfChar[j];
            if ((m >= 65136) && (m <= 65276))
            {
                if (isLamAlefChar(m))
                    i++;
                paramArrayOfChar[j] = ((char)convertFEto06[(m - 65136)]);
            }
            j++;
        }
        return i;
    }

    private void shapeToArabicDigitsWithContext(char[] paramArrayOfChar, int paramInt1, int paramInt2, char paramChar, boolean paramBoolean)
    {
        int i = paramChar + '\0*0';
        int j = paramInt1 + paramInt2;
        while (true)
        {
            j--;
            if (j < paramInt1)
                break;
            int k = paramArrayOfChar[j];
            switch (Character.getDirectionality(k))
            {
            default:
                break;
            case 0:
            case 1:
                paramBoolean = false;
                break;
            case 2:
                paramBoolean = true;
                break;
            case 3:
                if ((paramBoolean) && (k <= 57))
                    paramArrayOfChar[j] = (k + i);
                break;
            }
        }
    }

    private int shapeUnicode(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        throws ArabicShaping.ArabicShapingException
    {
        int i = normalize(paramArrayOfChar, paramInt1, paramInt2);
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 0;
        int i1 = -1 + (paramInt1 + paramInt2);
        int i2 = getLink(paramArrayOfChar[i1]);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = i1;
        int i7 = -2;
        while (i1 >= 0)
        {
            label231: int i10;
            int i11;
            if (((0xFF00 & i2) > 0) || (isTashkeelChar(paramArrayOfChar[i1])))
            {
                int i9 = i1 - 1;
                i7 = -2;
                while (i7 < 0)
                    if (i9 == -1)
                    {
                        i3 = 0;
                        i7 = 2147483647;
                    }
                    else
                    {
                        i3 = getLink(paramArrayOfChar[i9]);
                        if ((i3 & 0x4) == 0)
                            i7 = i9;
                        else
                            i9--;
                    }
                if (((i2 & 0x20) > 0) && ((i5 & 0x10) > 0))
                {
                    j = 1;
                    char c = changeLamAlef(paramArrayOfChar[i1]);
                    if (c != 0)
                    {
                        paramArrayOfChar[i1] = 65535;
                        paramArrayOfChar[i6] = c;
                        i1 = i6;
                    }
                    i5 = i4;
                    i2 = getLink(c);
                }
                if ((i1 <= 0) || (paramArrayOfChar[(i1 - 1)] != ' '))
                    break label349;
                if (isSeenFamilyChar(paramArrayOfChar[i1]) != 1)
                    break label333;
                k = 1;
                i10 = specialChar(paramArrayOfChar[i1]);
                i11 = shapeTable[(i3 & 0x3)][(i5 & 0x3)][(i2 & 0x3)];
                if (i10 != 1)
                    break label387;
                i11 &= 1;
                label272: if (i10 != 2)
                    break label490;
                if (paramInt4 != 2)
                    break label464;
                paramArrayOfChar[i1] = 65534;
                n = 1;
            }
            while (true)
            {
                if ((i2 & 0x4) == 0)
                {
                    i4 = i5;
                    i5 = i2;
                    i6 = i1;
                }
                i1--;
                if (i1 != i7)
                    break label509;
                i2 = i3;
                i7 = -2;
                break;
                label333: if (paramArrayOfChar[i1] != 'ئ')
                    break label231;
                m = 1;
                break label231;
                label349: if (i1 != 0)
                    break label231;
                if (isSeenFamilyChar(paramArrayOfChar[i1]) == 1)
                {
                    k = 1;
                    break label231;
                }
                if (paramArrayOfChar[i1] != 'ئ')
                    break label231;
                m = 1;
                break label231;
                label387: if (i10 != 2)
                    break label272;
                if ((paramInt4 == 0) && ((i5 & 0x2) != 0) && ((i3 & 0x1) != 0) && (paramArrayOfChar[i1] != 'ٌ') && (paramArrayOfChar[i1] != 'ٍ') && (((i3 & 0x20) != 32) || ((i5 & 0x10) != 16)))
                {
                    i11 = 1;
                    break label272;
                }
                i11 = 0;
                break label272;
                label464: paramArrayOfChar[i1] = ((char)(i11 + (65136 + irrelevantPos[('\0������5' + paramArrayOfChar[i1])])));
                continue;
                label490: paramArrayOfChar[i1] = ((char)(i11 + (65136 + (i2 >> 8))));
            }
            label509: if (i1 != -1)
                i2 = getLink(paramArrayOfChar[i1]);
        }
        int i8 = paramInt2;
        if ((j != 0) || (n != 0))
            i8 = handleGeneratedSpaces(paramArrayOfChar, paramInt1, paramInt2);
        if ((k != 0) || (m != 0))
            i8 = expandCompositChar(paramArrayOfChar, paramInt1, i8, i, 0);
        return i8;
    }

    public static void shiftArray(char[] paramArrayOfChar, int paramInt1, int paramInt2, char paramChar)
    {
        int i = paramInt2;
        int j = paramInt2;
        while (true)
        {
            j--;
            if (j < paramInt1)
                break;
            char c = paramArrayOfChar[j];
            if (c != paramChar)
            {
                i--;
                if (i != j)
                    paramArrayOfChar[i] = c;
            }
        }
    }

    private static int specialChar(char paramChar)
    {
        int i;
        if (((paramChar > 'ء') && (paramChar < 'ئ')) || (paramChar == 'ا') || ((paramChar > 'خ') && (paramChar < 'س')) || ((paramChar > 'ه') && (paramChar < 'ي')) || (paramChar == 'ة'))
            i = 1;
        while (true)
        {
            return i;
            if ((paramChar >= 'ً') && (paramChar <= 'ْ'))
                i = 2;
            else if (((paramChar >= 'ٓ') && (paramChar <= 'ٕ')) || (paramChar == 'ٰ') || ((paramChar >= 65136) && (paramChar <= 65151)))
                i = 3;
            else
                i = 0;
        }
    }

    public boolean equals(Object paramObject)
    {
        if ((paramObject != null) && (paramObject.getClass() == ArabicShaping.class) && (this.options == ((ArabicShaping)paramObject).options));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int hashCode()
    {
        return this.options;
    }

    public int shape(char[] paramArrayOfChar1, int paramInt1, int paramInt2, char[] paramArrayOfChar2, int paramInt3, int paramInt4)
        throws ArabicShaping.ArabicShapingException
    {
        if (paramArrayOfChar1 == null)
            throw new IllegalArgumentException("source can not be null");
        if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfChar1.length))
            throw new IllegalArgumentException("bad source start (" + paramInt1 + ") or length (" + paramInt2 + ") for buffer of length " + paramArrayOfChar1.length);
        if ((paramArrayOfChar2 == null) && (paramInt4 != 0))
            throw new IllegalArgumentException("null dest requires destSize == 0");
        if ((paramInt4 != 0) && ((paramInt3 < 0) || (paramInt4 < 0) || (paramInt3 + paramInt4 > paramArrayOfChar2.length)))
            throw new IllegalArgumentException("bad dest start (" + paramInt3 + ") or size (" + paramInt4 + ") for buffer of length " + paramArrayOfChar2.length);
        if (((0xE0000 & this.options) > 0) && ((0xE0000 & this.options) != 262144) && ((0xE0000 & this.options) != 393216) && ((0xE0000 & this.options) != 524288) && ((0xE0000 & this.options) != 786432))
            throw new IllegalArgumentException("Wrong Tashkeel argument");
        if (((0x10003 & this.options) > 0) && ((0x10003 & this.options) != 3) && ((0x10003 & this.options) != 2) && ((0x10003 & this.options) != 0) && ((0x10003 & this.options) != 65536) && ((0x10003 & this.options) != 1))
            throw new IllegalArgumentException("Wrong Lam Alef argument");
        if (((0xE0000 & this.options) > 0) && ((0x18 & this.options) == 16))
            throw new IllegalArgumentException("Tashkeel replacement should not be enabled in deshaping mode ");
        return internalShape(paramArrayOfChar1, paramInt1, paramInt2, paramArrayOfChar2, paramInt3, paramInt4);
    }

    public String shape(String paramString)
        throws ArabicShaping.ArabicShapingException
    {
        char[] arrayOfChar1 = paramString.toCharArray();
        char[] arrayOfChar2 = arrayOfChar1;
        if (((0x10003 & this.options) == 0) && ((0x18 & this.options) == 16))
            arrayOfChar2 = new char[2 * arrayOfChar1.length];
        return new String(arrayOfChar2, 0, shape(arrayOfChar1, 0, arrayOfChar1.length, arrayOfChar2, 0, arrayOfChar2.length));
    }

    public void shape(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws ArabicShaping.ArabicShapingException
    {
        if ((0x10003 & this.options) == 0)
            throw new ArabicShapingException("Cannot shape in place with length option resize.");
        shape(paramArrayOfChar, paramInt1, paramInt2, paramArrayOfChar, paramInt1, paramInt2);
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer(super.toString());
        localStringBuffer.append('[');
        switch (0x10003 & this.options)
        {
        default:
            label156: label180: label312: switch (0x4 & this.options)
            {
            default:
                label108: switch (0x18 & this.options)
                {
                default:
                    switch (0x700000 & this.options)
                    {
                    default:
                        switch (0x3800000 & this.options)
                        {
                        default:
                            switch (0xE0000 & this.options)
                            {
                            default:
                                label252: switch (0xE0 & this.options)
                                {
                                default:
                                    switch (0x100 & this.options)
                                    {
                                    default:
                                    case 0:
                                    case 256:
                                    }
                                    break;
                                case 0:
                                case 32:
                                case 64:
                                case 96:
                                case 128:
                                }
                                break;
                            case 262144:
                            case 393216:
                            case 786432:
                            case 524288:
                            }
                            break;
                        case 16777216:
                        }
                        break;
                    case 2097152:
                    }
                    break;
                case 0:
                case 8:
                case 24:
                case 16:
                }
                label204: break;
            case 0:
            case 4:
            }
            break;
        case 0:
        case 1:
        case 3:
        case 2:
        case 65536:
        }
        while (true)
        {
            localStringBuffer.append("]");
            return localStringBuffer.toString();
            localStringBuffer.append("LamAlef resize");
            break;
            localStringBuffer.append("LamAlef spaces at near");
            break;
            localStringBuffer.append("LamAlef spaces at begin");
            break;
            localStringBuffer.append("LamAlef spaces at end");
            break;
            localStringBuffer.append("lamAlef auto");
            break;
            localStringBuffer.append(", logical");
            break label108;
            localStringBuffer.append(", visual");
            break label108;
            localStringBuffer.append(", no letter shaping");
            break label156;
            localStringBuffer.append(", shape letters");
            break label156;
            localStringBuffer.append(", shape letters tashkeel isolated");
            break label156;
            localStringBuffer.append(", unshape letters");
            break label156;
            localStringBuffer.append(", Seen at near");
            break label180;
            localStringBuffer.append(", Yeh Hamza at near");
            break label204;
            localStringBuffer.append(", Tashkeel at begin");
            break label252;
            localStringBuffer.append(", Tashkeel at end");
            break label252;
            localStringBuffer.append(", Tashkeel replace with tatweel");
            break label252;
            localStringBuffer.append(", Tashkeel resize");
            break label252;
            localStringBuffer.append(", no digit shaping");
            break label312;
            localStringBuffer.append(", shape digits to AN");
            break label312;
            localStringBuffer.append(", shape digits to EN");
            break label312;
            localStringBuffer.append(", shape digits to AN contextually: default EN");
            break label312;
            localStringBuffer.append(", shape digits to AN contextually: default AL");
            break label312;
            localStringBuffer.append(", standard Arabic-Indic digits");
            continue;
            localStringBuffer.append(", extended Arabic-Indic digits");
        }
    }

    private static class ArabicShapingException extends RuntimeException
    {
        ArabicShapingException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.icu.text.ArabicShaping
 * JD-Core Version:        0.6.2
 */