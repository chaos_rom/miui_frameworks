package android.security;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public final class KeyChain
{
    public static final String ACCOUNT_TYPE = "com.android.keychain";
    private static final String ACTION_CHOOSER = "com.android.keychain.CHOOSER";
    private static final String ACTION_INSTALL = "android.credentials.INSTALL";
    public static final String ACTION_STORAGE_CHANGED = "android.security.STORAGE_CHANGED";
    public static final String EXTRA_ALIAS = "alias";
    public static final String EXTRA_CERTIFICATE = "CERT";
    public static final String EXTRA_HOST = "host";
    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_PKCS12 = "PKCS12";
    public static final String EXTRA_PORT = "port";
    public static final String EXTRA_RESPONSE = "response";
    public static final String EXTRA_SENDER = "sender";
    private static final String TAG = "KeyChain";

    public static KeyChainConnection bind(Context paramContext)
        throws InterruptedException
    {
        if (paramContext == null)
            throw new NullPointerException("context == null");
        ensureNotOnMainThread(paramContext);
        LinkedBlockingQueue localLinkedBlockingQueue = new LinkedBlockingQueue(1);
        ServiceConnection local1 = new ServiceConnection()
        {
            volatile boolean mConnectedAtLeastOnce = false;

            public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
            {
                if (!this.mConnectedAtLeastOnce)
                    this.mConnectedAtLeastOnce = true;
                try
                {
                    KeyChain.this.put(IKeyChainService.Stub.asInterface(paramAnonymousIBinder));
                    label25: return;
                }
                catch (InterruptedException localInterruptedException)
                {
                    break label25;
                }
            }

            public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
            {
            }
        };
        if (!paramContext.bindService(new Intent(IKeyChainService.class.getName()), local1, 1))
            throw new AssertionError("could not bind to KeyChainService");
        return new KeyChainConnection(paramContext, local1, (IKeyChainService)localLinkedBlockingQueue.take(), null);
    }

    public static void choosePrivateKeyAlias(Activity paramActivity, KeyChainAliasCallback paramKeyChainAliasCallback, String[] paramArrayOfString, Principal[] paramArrayOfPrincipal, String paramString1, int paramInt, String paramString2)
    {
        if (paramActivity == null)
            throw new NullPointerException("activity == null");
        if (paramKeyChainAliasCallback == null)
            throw new NullPointerException("response == null");
        Intent localIntent = new Intent("com.android.keychain.CHOOSER");
        localIntent.putExtra("response", new AliasResponse(paramKeyChainAliasCallback, null));
        localIntent.putExtra("host", paramString1);
        localIntent.putExtra("port", paramInt);
        localIntent.putExtra("alias", paramString2);
        localIntent.putExtra("sender", PendingIntent.getActivity(paramActivity, 0, new Intent(), 0));
        paramActivity.startActivity(localIntent);
    }

    public static Intent createInstallIntent()
    {
        Intent localIntent = new Intent("android.credentials.INSTALL");
        localIntent.setClassName("com.android.certinstaller", "com.android.certinstaller.CertInstallerMain");
        return localIntent;
    }

    private static void ensureNotOnMainThread(Context paramContext)
    {
        Looper localLooper = Looper.myLooper();
        if ((localLooper != null) && (localLooper == paramContext.getMainLooper()))
            throw new IllegalStateException("calling this from your main thread can lead to deadlock");
    }

    // ERROR //
    public static X509Certificate[] getCertificateChain(Context paramContext, String paramString)
        throws KeyChainException, InterruptedException
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +13 -> 14
        //     4: new 62	java/lang/NullPointerException
        //     7: dup
        //     8: ldc 180
        //     10: invokespecial 67	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     13: athrow
        //     14: aload_0
        //     15: invokestatic 182	android/security/KeyChain:bind	(Landroid/content/Context;)Landroid/security/KeyChain$KeyChainConnection;
        //     18: astore_2
        //     19: aload_2
        //     20: invokevirtual 186	android/security/KeyChain$KeyChainConnection:getService	()Landroid/security/IKeyChainService;
        //     23: aload_1
        //     24: invokeinterface 190 2 0
        //     29: astore 6
        //     31: new 192	java/util/ArrayList
        //     34: dup
        //     35: invokespecial 193	java/util/ArrayList:<init>	()V
        //     38: astore 7
        //     40: aload 7
        //     42: aload 6
        //     44: invokestatic 197	android/security/KeyChain:toCertificate	([B)Ljava/security/cert/X509Certificate;
        //     47: invokeinterface 203 2 0
        //     52: pop
        //     53: new 205	org/apache/harmony/xnet/provider/jsse/TrustedCertificateStore
        //     56: dup
        //     57: invokespecial 206	org/apache/harmony/xnet/provider/jsse/TrustedCertificateStore:<init>	()V
        //     60: astore 9
        //     62: iconst_0
        //     63: istore 10
        //     65: aload 7
        //     67: iload 10
        //     69: invokeinterface 210 2 0
        //     74: checkcast 212	java/security/cert/X509Certificate
        //     77: astore 11
        //     79: aload 11
        //     81: invokevirtual 216	java/security/cert/X509Certificate:getSubjectX500Principal	()Ljavax/security/auth/x500/X500Principal;
        //     84: aload 11
        //     86: invokevirtual 219	java/security/cert/X509Certificate:getIssuerX500Principal	()Ljavax/security/auth/x500/X500Principal;
        //     89: invokestatic 225	libcore/util/Objects:equal	(Ljava/lang/Object;Ljava/lang/Object;)Z
        //     92: ifeq +32 -> 124
        //     95: aload 7
        //     97: aload 7
        //     99: invokeinterface 229 1 0
        //     104: anewarray 212	java/security/cert/X509Certificate
        //     107: invokeinterface 233 2 0
        //     112: checkcast 235	[Ljava/security/cert/X509Certificate;
        //     115: astore 14
        //     117: aload_2
        //     118: invokevirtual 238	android/security/KeyChain$KeyChainConnection:close	()V
        //     121: aload 14
        //     123: areturn
        //     124: aload 9
        //     126: aload 11
        //     128: invokevirtual 242	org/apache/harmony/xnet/provider/jsse/TrustedCertificateStore:findIssuer	(Ljava/security/cert/X509Certificate;)Ljava/security/cert/X509Certificate;
        //     131: astore 12
        //     133: aload 12
        //     135: ifnull -40 -> 95
        //     138: aload 7
        //     140: aload 12
        //     142: invokeinterface 203 2 0
        //     147: pop
        //     148: iinc 10 1
        //     151: goto -86 -> 65
        //     154: astore 5
        //     156: new 174	android/security/KeyChainException
        //     159: dup
        //     160: aload 5
        //     162: invokespecial 245	android/security/KeyChainException:<init>	(Ljava/lang/Throwable;)V
        //     165: athrow
        //     166: astore 4
        //     168: aload_2
        //     169: invokevirtual 238	android/security/KeyChain$KeyChainConnection:close	()V
        //     172: aload 4
        //     174: athrow
        //     175: astore_3
        //     176: new 174	android/security/KeyChainException
        //     179: dup
        //     180: aload_3
        //     181: invokespecial 245	android/security/KeyChainException:<init>	(Ljava/lang/Throwable;)V
        //     184: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     19	117	154	android/os/RemoteException
        //     124	148	154	android/os/RemoteException
        //     19	117	166	finally
        //     124	148	166	finally
        //     156	166	166	finally
        //     176	185	166	finally
        //     19	117	175	java/lang/RuntimeException
        //     124	148	175	java/lang/RuntimeException
    }

    // ERROR //
    public static java.security.PrivateKey getPrivateKey(Context paramContext, String paramString)
        throws KeyChainException, InterruptedException
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +13 -> 14
        //     4: new 62	java/lang/NullPointerException
        //     7: dup
        //     8: ldc 180
        //     10: invokespecial 67	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     13: athrow
        //     14: aload_0
        //     15: invokestatic 182	android/security/KeyChain:bind	(Landroid/content/Context;)Landroid/security/KeyChain$KeyChainConnection;
        //     18: astore_2
        //     19: aload_2
        //     20: invokevirtual 186	android/security/KeyChain$KeyChainConnection:getService	()Landroid/security/IKeyChainService;
        //     23: aload_1
        //     24: invokeinterface 253 2 0
        //     29: astore 7
        //     31: aload 7
        //     33: ifnonnull +34 -> 67
        //     36: new 174	android/security/KeyChainException
        //     39: dup
        //     40: ldc 255
        //     42: invokespecial 256	android/security/KeyChainException:<init>	(Ljava/lang/String;)V
        //     45: athrow
        //     46: astore 6
        //     48: new 174	android/security/KeyChainException
        //     51: dup
        //     52: aload 6
        //     54: invokespecial 245	android/security/KeyChainException:<init>	(Ljava/lang/Throwable;)V
        //     57: athrow
        //     58: astore 4
        //     60: aload_2
        //     61: invokevirtual 238	android/security/KeyChain$KeyChainConnection:close	()V
        //     64: aload 4
        //     66: athrow
        //     67: ldc_w 258
        //     70: invokestatic 264	org/apache/harmony/xnet/provider/jsse/OpenSSLEngine:getInstance	(Ljava/lang/String;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;
        //     73: aload 7
        //     75: invokevirtual 268	org/apache/harmony/xnet/provider/jsse/OpenSSLEngine:getPrivateKeyById	(Ljava/lang/String;)Ljava/security/PrivateKey;
        //     78: astore 8
        //     80: aload_2
        //     81: invokevirtual 238	android/security/KeyChain$KeyChainConnection:close	()V
        //     84: aload 8
        //     86: areturn
        //     87: astore 5
        //     89: new 174	android/security/KeyChainException
        //     92: dup
        //     93: aload 5
        //     95: invokespecial 245	android/security/KeyChainException:<init>	(Ljava/lang/Throwable;)V
        //     98: athrow
        //     99: astore_3
        //     100: new 174	android/security/KeyChainException
        //     103: dup
        //     104: aload_3
        //     105: invokespecial 245	android/security/KeyChainException:<init>	(Ljava/lang/Throwable;)V
        //     108: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     19	46	46	android/os/RemoteException
        //     67	80	46	android/os/RemoteException
        //     19	46	58	finally
        //     48	58	58	finally
        //     67	80	58	finally
        //     89	109	58	finally
        //     19	46	87	java/lang/RuntimeException
        //     67	80	87	java/lang/RuntimeException
        //     19	46	99	java/security/InvalidKeyException
        //     67	80	99	java/security/InvalidKeyException
    }

    private static X509Certificate toCertificate(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null)
            throw new IllegalArgumentException("bytes == null");
        try
        {
            X509Certificate localX509Certificate = (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(paramArrayOfByte));
            return localX509Certificate;
        }
        catch (CertificateException localCertificateException)
        {
            throw new AssertionError(localCertificateException);
        }
    }

    public static final class KeyChainConnection
        implements Closeable
    {
        private final Context context;
        private final IKeyChainService service;
        private final ServiceConnection serviceConnection;

        private KeyChainConnection(Context paramContext, ServiceConnection paramServiceConnection, IKeyChainService paramIKeyChainService)
        {
            this.context = paramContext;
            this.serviceConnection = paramServiceConnection;
            this.service = paramIKeyChainService;
        }

        public void close()
        {
            this.context.unbindService(this.serviceConnection);
        }

        public IKeyChainService getService()
        {
            return this.service;
        }
    }

    private static class AliasResponse extends IKeyChainAliasCallback.Stub
    {
        private final KeyChainAliasCallback keyChainAliasResponse;

        private AliasResponse(KeyChainAliasCallback paramKeyChainAliasCallback)
        {
            this.keyChainAliasResponse = paramKeyChainAliasCallback;
        }

        public void alias(String paramString)
        {
            this.keyChainAliasResponse.alias(paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.security.KeyChain
 * JD-Core Version:        0.6.2
 */