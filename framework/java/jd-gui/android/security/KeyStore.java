package android.security;

import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import java.io.UTFDataFormatException;
import java.nio.charset.Charsets;
import java.nio.charset.ModifiedUtf8;
import java.util.ArrayList;

public class KeyStore
{
    public static final int KEY_NOT_FOUND = 7;
    public static final int LOCKED = 2;
    public static final int NO_ERROR = 1;
    public static final int PERMISSION_DENIED = 6;
    public static final int PROTOCOL_ERROR = 5;
    public static final int SYSTEM_ERROR = 4;
    public static final int UNDEFINED_ACTION = 9;
    public static final int UNINITIALIZED = 3;
    public static final int VALUE_CORRUPTED = 8;
    public static final int WRONG_PASSWORD = 10;
    private static final LocalSocketAddress sAddress = new LocalSocketAddress("keystore", LocalSocketAddress.Namespace.RESERVED);
    private int mError = 1;

    private boolean contains(byte[] paramArrayOfByte)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[i][];
        arrayOfByte[0] = paramArrayOfByte;
        execute(101, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean delKey(byte[] paramArrayOfByte)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[i][];
        arrayOfByte[0] = paramArrayOfByte;
        execute(107, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean delete(byte[] paramArrayOfByte)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[i][];
        arrayOfByte[0] = paramArrayOfByte;
        execute(100, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    // ERROR //
    private ArrayList<byte[]> execute(int paramInt, byte[][] paramArrayOfByte)
    {
        // Byte code:
        //     0: aload_0
        //     1: iconst_5
        //     2: putfield 53	android/security/KeyStore:mError	I
        //     5: aload_2
        //     6: arraylength
        //     7: istore_3
        //     8: iconst_0
        //     9: istore 4
        //     11: iload 4
        //     13: iload_3
        //     14: if_icmpge +34 -> 48
        //     17: aload_2
        //     18: iload 4
        //     20: aaload
        //     21: astore 25
        //     23: aload 25
        //     25: ifnull +11 -> 36
        //     28: aload 25
        //     30: arraylength
        //     31: ldc 66
        //     33: if_icmple +9 -> 42
        //     36: aconst_null
        //     37: astore 10
        //     39: aload 10
        //     41: areturn
        //     42: iinc 4 1
        //     45: goto -34 -> 11
        //     48: new 68	android/net/LocalSocket
        //     51: dup
        //     52: invokespecial 69	android/net/LocalSocket:<init>	()V
        //     55: astore 5
        //     57: aload 5
        //     59: getstatic 49	android/security/KeyStore:sAddress	Landroid/net/LocalSocketAddress;
        //     62: invokevirtual 73	android/net/LocalSocket:connect	(Landroid/net/LocalSocketAddress;)V
        //     65: aload 5
        //     67: invokevirtual 77	android/net/LocalSocket:getOutputStream	()Ljava/io/OutputStream;
        //     70: astore 11
        //     72: aload 11
        //     74: iload_1
        //     75: invokevirtual 83	java/io/OutputStream:write	(I)V
        //     78: aload_2
        //     79: arraylength
        //     80: istore 12
        //     82: iconst_0
        //     83: istore 13
        //     85: iload 13
        //     87: iload 12
        //     89: if_icmpge +41 -> 130
        //     92: aload_2
        //     93: iload 13
        //     95: aaload
        //     96: astore 24
        //     98: aload 11
        //     100: aload 24
        //     102: arraylength
        //     103: bipush 8
        //     105: ishr
        //     106: invokevirtual 83	java/io/OutputStream:write	(I)V
        //     109: aload 11
        //     111: aload 24
        //     113: arraylength
        //     114: invokevirtual 83	java/io/OutputStream:write	(I)V
        //     117: aload 11
        //     119: aload 24
        //     121: invokevirtual 86	java/io/OutputStream:write	([B)V
        //     124: iinc 13 1
        //     127: goto -42 -> 85
        //     130: aload 11
        //     132: invokevirtual 89	java/io/OutputStream:flush	()V
        //     135: aload 5
        //     137: invokevirtual 92	android/net/LocalSocket:shutdownOutput	()V
        //     140: aload 5
        //     142: invokevirtual 96	android/net/LocalSocket:getInputStream	()Ljava/io/InputStream;
        //     145: astore 14
        //     147: aload 14
        //     149: invokevirtual 102	java/io/InputStream:read	()I
        //     152: istore 15
        //     154: iload 15
        //     156: iconst_1
        //     157: if_icmpeq +32 -> 189
        //     160: iload 15
        //     162: bipush 255
        //     164: if_icmpeq +9 -> 173
        //     167: aload_0
        //     168: iload 15
        //     170: putfield 53	android/security/KeyStore:mError	I
        //     173: aconst_null
        //     174: astore 10
        //     176: aload 5
        //     178: invokevirtual 105	android/net/LocalSocket:close	()V
        //     181: goto -142 -> 39
        //     184: astore 23
        //     186: goto -147 -> 39
        //     189: new 107	java/util/ArrayList
        //     192: dup
        //     193: invokespecial 108	java/util/ArrayList:<init>	()V
        //     196: astore 10
        //     198: aload 14
        //     200: invokevirtual 102	java/io/InputStream:read	()I
        //     203: istore 16
        //     205: iload 16
        //     207: bipush 255
        //     209: if_icmpne +16 -> 225
        //     212: aload_0
        //     213: iconst_1
        //     214: putfield 53	android/security/KeyStore:mError	I
        //     217: aload 5
        //     219: invokevirtual 105	android/net/LocalSocket:close	()V
        //     222: goto -183 -> 39
        //     225: aload 14
        //     227: invokevirtual 102	java/io/InputStream:read	()I
        //     230: istore 17
        //     232: iload 17
        //     234: bipush 255
        //     236: if_icmpne +14 -> 250
        //     239: aconst_null
        //     240: astore 10
        //     242: aload 5
        //     244: invokevirtual 105	android/net/LocalSocket:close	()V
        //     247: goto -208 -> 39
        //     250: iload 17
        //     252: iload 16
        //     254: bipush 8
        //     256: ishl
        //     257: ior
        //     258: istore 18
        //     260: iload 18
        //     262: newarray byte
        //     264: astore 19
        //     266: iconst_0
        //     267: istore 20
        //     269: iload 20
        //     271: aload 19
        //     273: arraylength
        //     274: if_icmpge +48 -> 322
        //     277: aload 14
        //     279: aload 19
        //     281: iload 20
        //     283: aload 19
        //     285: arraylength
        //     286: iload 20
        //     288: isub
        //     289: invokevirtual 111	java/io/InputStream:read	([BII)I
        //     292: istore 22
        //     294: iload 22
        //     296: bipush 255
        //     298: if_icmpne +14 -> 312
        //     301: aconst_null
        //     302: astore 10
        //     304: aload 5
        //     306: invokevirtual 105	android/net/LocalSocket:close	()V
        //     309: goto -270 -> 39
        //     312: iload 20
        //     314: iload 22
        //     316: iadd
        //     317: istore 20
        //     319: goto -50 -> 269
        //     322: aload 10
        //     324: aload 19
        //     326: invokevirtual 115	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     329: pop
        //     330: goto -132 -> 198
        //     333: astore 8
        //     335: aload 5
        //     337: invokevirtual 105	android/net/LocalSocket:close	()V
        //     340: aconst_null
        //     341: astore 10
        //     343: goto -304 -> 39
        //     346: astore 6
        //     348: aload 5
        //     350: invokevirtual 105	android/net/LocalSocket:close	()V
        //     353: aload 6
        //     355: athrow
        //     356: astore 9
        //     358: goto -18 -> 340
        //     361: astore 7
        //     363: goto -10 -> 353
        //
        // Exception table:
        //     from	to	target	type
        //     176	181	184	java/io/IOException
        //     217	222	184	java/io/IOException
        //     242	247	184	java/io/IOException
        //     304	309	184	java/io/IOException
        //     57	173	333	java/io/IOException
        //     189	217	333	java/io/IOException
        //     225	232	333	java/io/IOException
        //     260	294	333	java/io/IOException
        //     322	330	333	java/io/IOException
        //     57	173	346	finally
        //     189	217	346	finally
        //     225	232	346	finally
        //     260	294	346	finally
        //     322	330	346	finally
        //     335	340	356	java/io/IOException
        //     348	353	361	java/io/IOException
    }

    private boolean generate(byte[] paramArrayOfByte)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[i][];
        arrayOfByte[0] = paramArrayOfByte;
        execute(97, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private byte[] get(byte[] paramArrayOfByte)
    {
        byte[][] arrayOfByte = new byte[1][];
        arrayOfByte[0] = paramArrayOfByte;
        ArrayList localArrayList = execute(103, arrayOfByte);
        if ((localArrayList == null) || (localArrayList.isEmpty()));
        for (byte[] arrayOfByte1 = null; ; arrayOfByte1 = (byte[])localArrayList.get(0))
            return arrayOfByte1;
    }

    public static KeyStore getInstance()
    {
        return new KeyStore();
    }

    private static byte[] getKeyBytes(String paramString)
    {
        try
        {
            byte[] arrayOfByte = new byte[(int)ModifiedUtf8.countBytes(paramString, false)];
            ModifiedUtf8.encode(arrayOfByte, 0, paramString);
            return arrayOfByte;
        }
        catch (UTFDataFormatException localUTFDataFormatException)
        {
            throw new RuntimeException(localUTFDataFormatException);
        }
    }

    private static byte[] getPasswordBytes(String paramString)
    {
        return paramString.getBytes(Charsets.UTF_8);
    }

    private byte[] getPubkey(byte[] paramArrayOfByte)
    {
        byte[][] arrayOfByte = new byte[1][];
        arrayOfByte[0] = paramArrayOfByte;
        ArrayList localArrayList = execute(98, arrayOfByte);
        if ((localArrayList == null) || (localArrayList.isEmpty()));
        for (byte[] arrayOfByte1 = null; ; arrayOfByte1 = (byte[])localArrayList.get(0))
            return arrayOfByte1;
    }

    private static byte[] getUidBytes(int paramInt)
    {
        return Integer.toString(paramInt).getBytes(Charsets.UTF_8);
    }

    private boolean grant(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[2][];
        arrayOfByte[0] = paramArrayOfByte1;
        arrayOfByte[i] = paramArrayOfByte2;
        execute(120, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean importKey(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[2][];
        arrayOfByte[0] = paramArrayOfByte1;
        arrayOfByte[i] = paramArrayOfByte2;
        execute(109, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean password(byte[] paramArrayOfByte)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[i][];
        arrayOfByte[0] = paramArrayOfByte;
        execute(112, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean put(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[2][];
        arrayOfByte[0] = paramArrayOfByte1;
        arrayOfByte[i] = paramArrayOfByte2;
        execute(105, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private byte[] sign(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        byte[][] arrayOfByte = new byte[2][];
        arrayOfByte[0] = paramArrayOfByte1;
        arrayOfByte[1] = paramArrayOfByte2;
        ArrayList localArrayList = execute(110, arrayOfByte);
        if ((localArrayList == null) || (localArrayList.isEmpty()));
        for (byte[] arrayOfByte1 = null; ; arrayOfByte1 = (byte[])localArrayList.get(0))
            return arrayOfByte1;
    }

    private static String toKeyString(byte[] paramArrayOfByte)
    {
        try
        {
            String str = ModifiedUtf8.decode(paramArrayOfByte, new char[paramArrayOfByte.length], 0, paramArrayOfByte.length);
            return str;
        }
        catch (UTFDataFormatException localUTFDataFormatException)
        {
            throw new RuntimeException(localUTFDataFormatException);
        }
    }

    private boolean ungrant(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[2][];
        arrayOfByte[0] = paramArrayOfByte1;
        arrayOfByte[i] = paramArrayOfByte2;
        execute(121, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean unlock(byte[] paramArrayOfByte)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[i][];
        arrayOfByte[0] = paramArrayOfByte;
        execute(117, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean verify(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3)
    {
        int i = 1;
        byte[][] arrayOfByte = new byte[3][];
        arrayOfByte[0] = paramArrayOfByte1;
        arrayOfByte[i] = paramArrayOfByte2;
        arrayOfByte[2] = paramArrayOfByte3;
        execute(118, arrayOfByte);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean contains(String paramString)
    {
        return contains(getKeyBytes(paramString));
    }

    public boolean delKey(String paramString)
    {
        return delKey(getKeyBytes(paramString));
    }

    public boolean delete(String paramString)
    {
        return delete(getKeyBytes(paramString));
    }

    public boolean generate(String paramString)
    {
        return generate(getKeyBytes(paramString));
    }

    public byte[] get(String paramString)
    {
        return get(getKeyBytes(paramString));
    }

    public int getLastError()
    {
        return this.mError;
    }

    public byte[] getPubkey(String paramString)
    {
        return getPubkey(getKeyBytes(paramString));
    }

    public boolean grant(String paramString, int paramInt)
    {
        return grant(getKeyBytes(paramString), getUidBytes(paramInt));
    }

    public boolean importKey(String paramString, byte[] paramArrayOfByte)
    {
        return importKey(getKeyBytes(paramString), paramArrayOfByte);
    }

    public boolean isEmpty()
    {
        boolean bool = false;
        execute(122, new byte[0][]);
        if (this.mError == 7)
            bool = true;
        return bool;
    }

    public boolean lock()
    {
        int i = 1;
        execute(108, new byte[0][]);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean password(String paramString)
    {
        return password(getPasswordBytes(paramString));
    }

    public boolean put(String paramString, byte[] paramArrayOfByte)
    {
        return put(getKeyBytes(paramString), paramArrayOfByte);
    }

    public boolean reset()
    {
        int i = 1;
        execute(114, new byte[0][]);
        if (this.mError == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public String[] saw(String paramString)
    {
        byte[][] arrayOfByte = saw(getKeyBytes(paramString));
        String[] arrayOfString;
        if (arrayOfByte == null)
            arrayOfString = null;
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[arrayOfByte.length];
            for (int i = 0; i < arrayOfByte.length; i++)
                arrayOfString[i] = toKeyString(arrayOfByte[i]);
        }
    }

    public byte[][] saw(byte[] paramArrayOfByte)
    {
        byte[][] arrayOfByte = new byte[1][];
        arrayOfByte[0] = paramArrayOfByte;
        ArrayList localArrayList = execute(115, arrayOfByte);
        if (localArrayList == null);
        for (byte[][] arrayOfByte1 = (byte[][])null; ; arrayOfByte1 = (byte[][])localArrayList.toArray(new byte[localArrayList.size()][]))
            return arrayOfByte1;
    }

    public byte[] sign(String paramString, byte[] paramArrayOfByte)
    {
        return sign(getKeyBytes(paramString), paramArrayOfByte);
    }

    public State state()
    {
        execute(116, new byte[0][]);
        State localState;
        switch (this.mError)
        {
        default:
            throw new AssertionError(this.mError);
        case 1:
            localState = State.UNLOCKED;
        case 2:
        case 3:
        }
        while (true)
        {
            return localState;
            localState = State.LOCKED;
            continue;
            localState = State.UNINITIALIZED;
        }
    }

    public boolean ungrant(String paramString, int paramInt)
    {
        return ungrant(getKeyBytes(paramString), getUidBytes(paramInt));
    }

    public boolean unlock(String paramString)
    {
        return unlock(getPasswordBytes(paramString));
    }

    public boolean verify(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        return verify(getKeyBytes(paramString), paramArrayOfByte1, paramArrayOfByte2);
    }

    public static enum State
    {
        static
        {
            LOCKED = new State("LOCKED", 1);
            UNINITIALIZED = new State("UNINITIALIZED", 2);
            State[] arrayOfState = new State[3];
            arrayOfState[0] = UNLOCKED;
            arrayOfState[1] = LOCKED;
            arrayOfState[2] = UNINITIALIZED;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.security.KeyStore
 * JD-Core Version:        0.6.2
 */