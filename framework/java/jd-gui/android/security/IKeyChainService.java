package android.security;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IKeyChainService extends IInterface
{
    public abstract boolean deleteCaCertificate(String paramString)
        throws RemoteException;

    public abstract byte[] getCertificate(String paramString)
        throws RemoteException;

    public abstract boolean hasGrant(int paramInt, String paramString)
        throws RemoteException;

    public abstract void installCaCertificate(byte[] paramArrayOfByte)
        throws RemoteException;

    public abstract String requestPrivateKey(String paramString)
        throws RemoteException;

    public abstract boolean reset()
        throws RemoteException;

    public abstract void setGrant(int paramInt, String paramString, boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IKeyChainService
    {
        private static final String DESCRIPTOR = "android.security.IKeyChainService";
        static final int TRANSACTION_deleteCaCertificate = 4;
        static final int TRANSACTION_getCertificate = 2;
        static final int TRANSACTION_hasGrant = 7;
        static final int TRANSACTION_installCaCertificate = 3;
        static final int TRANSACTION_requestPrivateKey = 1;
        static final int TRANSACTION_reset = 5;
        static final int TRANSACTION_setGrant = 6;

        public Stub()
        {
            attachInterface(this, "android.security.IKeyChainService");
        }

        public static IKeyChainService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.security.IKeyChainService");
                if ((localIInterface != null) && ((localIInterface instanceof IKeyChainService)))
                    localObject = (IKeyChainService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.security.IKeyChainService");
                continue;
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                String str2 = requestPrivateKey(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str2);
                continue;
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                byte[] arrayOfByte = getCertificate(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeByteArray(arrayOfByte);
                continue;
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                installCaCertificate(paramParcel1.createByteArray());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                boolean bool4 = deleteCaCertificate(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool4)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                boolean bool3 = reset();
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                int k = paramParcel1.readInt();
                String str1 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = j; ; bool2 = false)
                {
                    setGrant(k, str1, bool2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.security.IKeyChainService");
                boolean bool1 = hasGrant(paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
            }
        }

        private static class Proxy
            implements IKeyChainService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean deleteCaCertificate(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public byte[] getCertificate(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    byte[] arrayOfByte = localParcel2.createByteArray();
                    return arrayOfByte;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.security.IKeyChainService";
            }

            public boolean hasGrant(int paramInt, String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void installCaCertificate(byte[] paramArrayOfByte)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    localParcel1.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String requestPrivateKey(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean reset()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setGrant(int paramInt, String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.security.IKeyChainService");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.security.IKeyChainService
 * JD-Core Version:        0.6.2
 */