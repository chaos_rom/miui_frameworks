package android.security;

import android.os.Environment;
import android.os.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import libcore.io.IoUtils;

public class SystemKeyStore
{
    private static final String KEY_FILE_EXTENSION = ".sks";
    private static final String SYSTEM_KEYSTORE_DIRECTORY = "misc/systemkeys";
    private static SystemKeyStore mInstance = new SystemKeyStore();

    public static SystemKeyStore getInstance()
    {
        return mInstance;
    }

    private File getKeyFile(String paramString)
    {
        return new File(new File(Environment.getDataDirectory(), "misc/systemkeys"), paramString + ".sks");
    }

    public static String toHexString(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null);
        StringBuilder localStringBuilder;
        for (String str1 = null; ; str1 = localStringBuilder.toString())
        {
            return str1;
            paramArrayOfByte.length;
            localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
            for (int i = 0; i < paramArrayOfByte.length; i++)
            {
                String str2 = Integer.toString(0xFF & paramArrayOfByte[i], 16);
                if (str2.length() == 1)
                    str2 = "0" + str2;
                localStringBuilder.append(str2);
            }
        }
    }

    public void deleteKey(String paramString)
    {
        File localFile = getKeyFile(paramString);
        if (!localFile.exists())
            throw new IllegalArgumentException();
        localFile.delete();
    }

    public byte[] generateNewKey(int paramInt, String paramString1, String paramString2)
        throws NoSuchAlgorithmException
    {
        File localFile = getKeyFile(paramString2);
        if (localFile.exists())
            throw new IllegalArgumentException();
        KeyGenerator localKeyGenerator = KeyGenerator.getInstance(paramString1);
        localKeyGenerator.init(paramInt, SecureRandom.getInstance("SHA1PRNG"));
        byte[] arrayOfByte = localKeyGenerator.generateKey().getEncoded();
        try
        {
            if (!localFile.createNewFile())
                throw new IllegalArgumentException();
            FileOutputStream localFileOutputStream = new FileOutputStream(localFile);
            localFileOutputStream.write(arrayOfByte);
            localFileOutputStream.flush();
            FileUtils.sync(localFileOutputStream);
            localFileOutputStream.close();
            FileUtils.setPermissions(localFile.getName(), 384, -1, -1);
        }
        catch (IOException localIOException)
        {
            arrayOfByte = null;
        }
        return arrayOfByte;
    }

    public String generateNewKeyHexString(int paramInt, String paramString1, String paramString2)
        throws NoSuchAlgorithmException
    {
        return toHexString(generateNewKey(paramInt, paramString1, paramString2));
    }

    public byte[] retrieveKey(String paramString)
        throws IOException
    {
        File localFile = getKeyFile(paramString);
        if (!localFile.exists());
        for (byte[] arrayOfByte = null; ; arrayOfByte = IoUtils.readFileAsByteArray(localFile.toString()))
            return arrayOfByte;
    }

    public String retrieveKeyHexString(String paramString)
        throws IOException
    {
        return toHexString(retrieveKey(paramString));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.security.SystemKeyStore
 * JD-Core Version:        0.6.2
 */