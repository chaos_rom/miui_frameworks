package android.security;

public class KeyChainException extends Exception
{
    public KeyChainException()
    {
    }

    public KeyChainException(String paramString)
    {
        super(paramString);
    }

    public KeyChainException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public KeyChainException(Throwable paramThrowable)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.security.KeyChainException
 * JD-Core Version:        0.6.2
 */