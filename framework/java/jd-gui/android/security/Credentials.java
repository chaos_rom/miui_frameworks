package android.security;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.android.org.bouncycastle.openssl.PEMReader;
import com.android.org.bouncycastle.openssl.PEMWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class Credentials
{
    public static final String CA_CERTIFICATE = "CACERT_";
    public static final String EXTENSION_CER = ".cer";
    public static final String EXTENSION_CRT = ".crt";
    public static final String EXTENSION_P12 = ".p12";
    public static final String EXTENSION_PFX = ".pfx";
    public static final String EXTRA_CA_CERTIFICATES_DATA = "ca_certificates_data";
    public static final String EXTRA_CA_CERTIFICATES_NAME = "ca_certificates_name";
    public static final String EXTRA_PRIVATE_KEY = "PKEY";
    public static final String EXTRA_PUBLIC_KEY = "KEY";
    public static final String EXTRA_USER_CERTIFICATE_DATA = "user_certificate_data";
    public static final String EXTRA_USER_CERTIFICATE_NAME = "user_certificate_name";
    public static final String EXTRA_USER_PRIVATE_KEY_DATA = "user_private_key_data";
    public static final String EXTRA_USER_PRIVATE_KEY_NAME = "user_private_key_name";
    public static final String INSTALL_ACTION = "android.credentials.INSTALL";
    private static final String LOGTAG = "Credentials";
    public static final String UNLOCK_ACTION = "com.android.credentials.UNLOCK";
    public static final String USER_CERTIFICATE = "USRCERT_";
    public static final String USER_PRIVATE_KEY = "USRPKEY_";
    public static final String VPN = "VPN_";
    public static final String WIFI = "WIFI_";
    private static Credentials singleton;

    public static List<Object> convertFromPem(byte[] paramArrayOfByte)
        throws IOException
    {
        PEMReader localPEMReader = new PEMReader(new InputStreamReader(new ByteArrayInputStream(paramArrayOfByte), Charsets.US_ASCII));
        ArrayList localArrayList = new ArrayList();
        while (true)
        {
            Object localObject = localPEMReader.readObject();
            if (localObject == null)
                break;
            localArrayList.add(localObject);
        }
        localPEMReader.close();
        return localArrayList;
    }

    public static byte[] convertToPem(Object[] paramArrayOfObject)
        throws IOException
    {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        PEMWriter localPEMWriter = new PEMWriter(new OutputStreamWriter(localByteArrayOutputStream, Charsets.US_ASCII));
        int i = paramArrayOfObject.length;
        for (int j = 0; j < i; j++)
            localPEMWriter.writeObject(paramArrayOfObject[j]);
        localPEMWriter.close();
        return localByteArrayOutputStream.toByteArray();
    }

    public static Credentials getInstance()
    {
        if (singleton == null)
            singleton = new Credentials();
        return singleton;
    }

    public void install(Context paramContext)
    {
        try
        {
            paramContext.startActivity(KeyChain.createInstallIntent());
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Log.w("Credentials", localActivityNotFoundException.toString());
        }
    }

    public void install(Context paramContext, String paramString, byte[] paramArrayOfByte)
    {
        try
        {
            Intent localIntent = KeyChain.createInstallIntent();
            localIntent.putExtra(paramString, paramArrayOfByte);
            paramContext.startActivity(localIntent);
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Log.w("Credentials", localActivityNotFoundException.toString());
        }
    }

    public void install(Context paramContext, KeyPair paramKeyPair)
    {
        try
        {
            Intent localIntent = KeyChain.createInstallIntent();
            localIntent.putExtra("PKEY", paramKeyPair.getPrivate().getEncoded());
            localIntent.putExtra("KEY", paramKeyPair.getPublic().getEncoded());
            paramContext.startActivity(localIntent);
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Log.w("Credentials", localActivityNotFoundException.toString());
        }
    }

    public void unlock(Context paramContext)
    {
        try
        {
            paramContext.startActivity(new Intent("com.android.credentials.UNLOCK"));
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Log.w("Credentials", localActivityNotFoundException.toString());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.security.Credentials
 * JD-Core Version:        0.6.2
 */