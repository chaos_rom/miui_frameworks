package android.drm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class DrmUtils
{
    public static ExtendedMetadataParser getExtendedMetadataParser(byte[] paramArrayOfByte)
    {
        return new ExtendedMetadataParser(paramArrayOfByte, null);
    }

    private static void quietlyDispose(InputStream paramInputStream)
    {
        if (paramInputStream != null);
        try
        {
            paramInputStream.close();
            label8: return;
        }
        catch (IOException localIOException)
        {
            break label8;
        }
    }

    private static void quietlyDispose(OutputStream paramOutputStream)
    {
        if (paramOutputStream != null);
        try
        {
            paramOutputStream.close();
            label8: return;
        }
        catch (IOException localIOException)
        {
            break label8;
        }
    }

    static byte[] readBytes(File paramFile)
        throws IOException
    {
        FileInputStream localFileInputStream = new FileInputStream(paramFile);
        BufferedInputStream localBufferedInputStream = new BufferedInputStream(localFileInputStream);
        byte[] arrayOfByte = null;
        try
        {
            int i = localBufferedInputStream.available();
            if (i > 0)
            {
                arrayOfByte = new byte[i];
                localBufferedInputStream.read(arrayOfByte);
            }
            return arrayOfByte;
        }
        finally
        {
            quietlyDispose(localBufferedInputStream);
            quietlyDispose(localFileInputStream);
        }
    }

    static byte[] readBytes(String paramString)
        throws IOException
    {
        return readBytes(new File(paramString));
    }

    static void removeFile(String paramString)
        throws IOException
    {
        new File(paramString).delete();
    }

    // ERROR //
    static void writeToFile(String paramString, byte[] paramArrayOfByte)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: ifnull +25 -> 28
        //     6: aload_1
        //     7: ifnull +21 -> 28
        //     10: new 69	java/io/FileOutputStream
        //     13: dup
        //     14: aload_0
        //     15: invokespecial 70	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     18: astore_3
        //     19: aload_3
        //     20: aload_1
        //     21: invokevirtual 74	java/io/FileOutputStream:write	([B)V
        //     24: aload_3
        //     25: invokestatic 76	android/drm/DrmUtils:quietlyDispose	(Ljava/io/OutputStream;)V
        //     28: return
        //     29: astore 4
        //     31: aload_2
        //     32: invokestatic 76	android/drm/DrmUtils:quietlyDispose	(Ljava/io/OutputStream;)V
        //     35: aload 4
        //     37: athrow
        //     38: astore 4
        //     40: aload_3
        //     41: astore_2
        //     42: goto -11 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     10	19	29	finally
        //     19	24	38	finally
    }

    public static class ExtendedMetadataParser
    {
        HashMap<String, String> mMap = new HashMap();

        private ExtendedMetadataParser(byte[] paramArrayOfByte)
        {
            int i = 0;
            while (i < paramArrayOfByte.length)
            {
                int j = readByte(paramArrayOfByte, i);
                int k = i + 1;
                int m = readByte(paramArrayOfByte, k);
                int n = k + 1;
                String str1 = readMultipleBytes(paramArrayOfByte, j, n);
                int i1 = n + j;
                String str2 = readMultipleBytes(paramArrayOfByte, m, i1);
                if (str2.equals(" "))
                    str2 = "";
                i = i1 + m;
                this.mMap.put(str1, str2);
            }
        }

        private int readByte(byte[] paramArrayOfByte, int paramInt)
        {
            return paramArrayOfByte[paramInt];
        }

        private String readMultipleBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        {
            byte[] arrayOfByte = new byte[paramInt1];
            int i = paramInt2;
            for (int j = 0; i < paramInt2 + paramInt1; j++)
            {
                arrayOfByte[j] = paramArrayOfByte[i];
                i++;
            }
            return new String(arrayOfByte);
        }

        public String get(String paramString)
        {
            return (String)this.mMap.get(paramString);
        }

        public Iterator<String> iterator()
        {
            return this.mMap.values().iterator();
        }

        public Iterator<String> keyIterator()
        {
            return this.mMap.keySet().iterator();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.DrmUtils
 * JD-Core Version:        0.6.2
 */