package android.drm;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class DrmManagerClient
{
    private static final int ACTION_PROCESS_DRM_INFO = 1002;
    private static final int ACTION_REMOVE_ALL_RIGHTS = 1001;
    public static final int ERROR_NONE = 0;
    public static final int ERROR_UNKNOWN = -2000;
    private static final String TAG = "DrmManagerClient";
    private Context mContext;
    private EventHandler mEventHandler;
    HandlerThread mEventThread;
    private InfoHandler mInfoHandler;
    HandlerThread mInfoThread;
    private int mNativeContext;
    private OnErrorListener mOnErrorListener;
    private OnEventListener mOnEventListener;
    private OnInfoListener mOnInfoListener;
    private boolean mReleased;
    private int mUniqueId;

    static
    {
        System.loadLibrary("drmframework_jni");
    }

    public DrmManagerClient(Context paramContext)
    {
        this.mContext = paramContext;
        this.mReleased = false;
        createEventThreads();
        this.mUniqueId = _initialize();
    }

    private native DrmInfo _acquireDrmInfo(int paramInt, DrmInfoRequest paramDrmInfoRequest);

    private native boolean _canHandle(int paramInt, String paramString1, String paramString2);

    private native int _checkRightsStatus(int paramInt1, String paramString, int paramInt2);

    private native DrmConvertedStatus _closeConvertSession(int paramInt1, int paramInt2);

    private native DrmConvertedStatus _convertData(int paramInt1, int paramInt2, byte[] paramArrayOfByte);

    private native DrmSupportInfo[] _getAllSupportInfo(int paramInt);

    private native ContentValues _getConstraints(int paramInt1, String paramString, int paramInt2);

    private native int _getDrmObjectType(int paramInt, String paramString1, String paramString2);

    private native ContentValues _getMetadata(int paramInt, String paramString);

    private native String _getOriginalMimeType(int paramInt, String paramString);

    private native int _initialize();

    private native void _installDrmEngine(int paramInt, String paramString);

    private native int _openConvertSession(int paramInt, String paramString);

    private native DrmInfoStatus _processDrmInfo(int paramInt, DrmInfo paramDrmInfo);

    private native void _release(int paramInt);

    private native int _removeAllRights(int paramInt);

    private native int _removeRights(int paramInt, String paramString);

    private native int _saveRights(int paramInt, DrmRights paramDrmRights, String paramString1, String paramString2);

    private native void _setListeners(int paramInt, Object paramObject);

    private String convertUriToPath(Uri paramUri)
    {
        Object localObject1 = null;
        String str1;
        if (paramUri != null)
        {
            str1 = paramUri.getScheme();
            if ((str1 != null) && (!str1.equals("")) && (!str1.equals("file")))
                break label40;
            localObject1 = paramUri.getPath();
        }
        while (true)
        {
            return localObject1;
            label40: if (str1.equals("http"))
            {
                localObject1 = paramUri.toString();
            }
            else
            {
                if (!str1.equals("content"))
                    break;
                String[] arrayOfString = new String[1];
                arrayOfString[0] = "_data";
                Cursor localCursor = null;
                try
                {
                    localCursor = this.mContext.getContentResolver().query(paramUri, arrayOfString, null, null, null);
                    if ((localCursor == null) || (localCursor.getCount() == 0) || (!localCursor.moveToFirst()))
                        throw new IllegalArgumentException("Given Uri could not be found in media store");
                }
                catch (SQLiteException localSQLiteException)
                {
                    throw new IllegalArgumentException("Given Uri is not formatted in a way so that it can be found in media store.");
                }
                finally
                {
                    if (localCursor != null)
                        localCursor.close();
                }
                String str2 = localCursor.getString(localCursor.getColumnIndexOrThrow("_data"));
                localObject1 = str2;
                if (localCursor != null)
                    localCursor.close();
            }
        }
        throw new IllegalArgumentException("Given Uri scheme is not supported");
    }

    private void createEventThreads()
    {
        if ((this.mEventHandler == null) && (this.mInfoHandler == null))
        {
            this.mInfoThread = new HandlerThread("DrmManagerClient.InfoHandler");
            this.mInfoThread.start();
            this.mInfoHandler = new InfoHandler(this.mInfoThread.getLooper());
            this.mEventThread = new HandlerThread("DrmManagerClient.EventHandler");
            this.mEventThread.start();
            this.mEventHandler = new EventHandler(this.mEventThread.getLooper());
        }
    }

    private void createListeners()
    {
        _setListeners(this.mUniqueId, new WeakReference(this));
    }

    private int getErrorType(int paramInt)
    {
        int i = -1;
        switch (paramInt)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            i = 2006;
        }
    }

    private int getEventType(int paramInt)
    {
        int i = -1;
        switch (paramInt)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            i = 1002;
        }
    }

    public static void notify(Object paramObject, int paramInt1, int paramInt2, String paramString)
    {
        DrmManagerClient localDrmManagerClient = (DrmManagerClient)((WeakReference)paramObject).get();
        if ((localDrmManagerClient != null) && (localDrmManagerClient.mInfoHandler != null))
        {
            Message localMessage = localDrmManagerClient.mInfoHandler.obtainMessage(1, paramInt1, paramInt2, paramString);
            localDrmManagerClient.mInfoHandler.sendMessage(localMessage);
        }
    }

    public DrmInfo acquireDrmInfo(DrmInfoRequest paramDrmInfoRequest)
    {
        if ((paramDrmInfoRequest == null) || (!paramDrmInfoRequest.isValid()))
            throw new IllegalArgumentException("Given drmInfoRequest is invalid/null");
        return _acquireDrmInfo(this.mUniqueId, paramDrmInfoRequest);
    }

    public int acquireRights(DrmInfoRequest paramDrmInfoRequest)
    {
        DrmInfo localDrmInfo = acquireDrmInfo(paramDrmInfoRequest);
        if (localDrmInfo == null);
        for (int i = -2000; ; i = processDrmInfo(localDrmInfo))
            return i;
    }

    public boolean canHandle(Uri paramUri, String paramString)
    {
        if (((paramUri == null) || (Uri.EMPTY == paramUri)) && ((paramString == null) || (paramString.equals(""))))
            throw new IllegalArgumentException("Uri or the mimetype should be non null");
        return canHandle(convertUriToPath(paramUri), paramString);
    }

    public boolean canHandle(String paramString1, String paramString2)
    {
        if (((paramString1 == null) || (paramString1.equals(""))) && ((paramString2 == null) || (paramString2.equals(""))))
            throw new IllegalArgumentException("Path or the mimetype should be non null");
        return _canHandle(this.mUniqueId, paramString1, paramString2);
    }

    public int checkRightsStatus(Uri paramUri)
    {
        if ((paramUri == null) || (Uri.EMPTY == paramUri))
            throw new IllegalArgumentException("Given uri is not valid");
        return checkRightsStatus(convertUriToPath(paramUri));
    }

    public int checkRightsStatus(Uri paramUri, int paramInt)
    {
        if ((paramUri == null) || (Uri.EMPTY == paramUri))
            throw new IllegalArgumentException("Given uri is not valid");
        return checkRightsStatus(convertUriToPath(paramUri), paramInt);
    }

    public int checkRightsStatus(String paramString)
    {
        return checkRightsStatus(paramString, 0);
    }

    public int checkRightsStatus(String paramString, int paramInt)
    {
        if ((paramString == null) || (paramString.equals("")) || (!DrmStore.Action.isValid(paramInt)))
            throw new IllegalArgumentException("Given path or action is not valid");
        return _checkRightsStatus(this.mUniqueId, paramString, paramInt);
    }

    public DrmConvertedStatus closeConvertSession(int paramInt)
    {
        return _closeConvertSession(this.mUniqueId, paramInt);
    }

    public DrmConvertedStatus convertData(int paramInt, byte[] paramArrayOfByte)
    {
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length <= 0))
            throw new IllegalArgumentException("Given inputData should be non null");
        return _convertData(this.mUniqueId, paramInt, paramArrayOfByte);
    }

    protected void finalize()
    {
        if (!this.mReleased)
        {
            Log.w("DrmManagerClient", "You should have called release()");
            release();
        }
    }

    public String[] getAvailableDrmEngines()
    {
        DrmSupportInfo[] arrayOfDrmSupportInfo = _getAllSupportInfo(this.mUniqueId);
        ArrayList localArrayList = new ArrayList();
        for (int i = 0; i < arrayOfDrmSupportInfo.length; i++)
            localArrayList.add(arrayOfDrmSupportInfo[i].getDescriprition());
        return (String[])localArrayList.toArray(new String[localArrayList.size()]);
    }

    public ContentValues getConstraints(Uri paramUri, int paramInt)
    {
        if ((paramUri == null) || (Uri.EMPTY == paramUri))
            throw new IllegalArgumentException("Uri should be non null");
        return getConstraints(convertUriToPath(paramUri), paramInt);
    }

    public ContentValues getConstraints(String paramString, int paramInt)
    {
        if ((paramString == null) || (paramString.equals("")) || (!DrmStore.Action.isValid(paramInt)))
            throw new IllegalArgumentException("Given usage or path is invalid/null");
        return _getConstraints(this.mUniqueId, paramString, paramInt);
    }

    public int getDrmObjectType(Uri paramUri, String paramString)
    {
        if (((paramUri == null) || (Uri.EMPTY == paramUri)) && ((paramString == null) || (paramString.equals(""))))
            throw new IllegalArgumentException("Uri or the mimetype should be non null");
        Object localObject = "";
        try
        {
            String str = convertUriToPath(paramUri);
            localObject = str;
            return getDrmObjectType((String)localObject, paramString);
        }
        catch (Exception localException)
        {
            while (true)
                Log.w("DrmManagerClient", "Given Uri could not be found in media store");
        }
    }

    public int getDrmObjectType(String paramString1, String paramString2)
    {
        if (((paramString1 == null) || (paramString1.equals(""))) && ((paramString2 == null) || (paramString2.equals(""))))
            throw new IllegalArgumentException("Path or the mimetype should be non null");
        return _getDrmObjectType(this.mUniqueId, paramString1, paramString2);
    }

    public ContentValues getMetadata(Uri paramUri)
    {
        if ((paramUri == null) || (Uri.EMPTY == paramUri))
            throw new IllegalArgumentException("Uri should be non null");
        return getMetadata(convertUriToPath(paramUri));
    }

    public ContentValues getMetadata(String paramString)
    {
        if ((paramString == null) || (paramString.equals("")))
            throw new IllegalArgumentException("Given path is invalid/null");
        return _getMetadata(this.mUniqueId, paramString);
    }

    public String getOriginalMimeType(Uri paramUri)
    {
        if ((paramUri == null) || (Uri.EMPTY == paramUri))
            throw new IllegalArgumentException("Given uri is not valid");
        return getOriginalMimeType(convertUriToPath(paramUri));
    }

    public String getOriginalMimeType(String paramString)
    {
        if ((paramString == null) || (paramString.equals("")))
            throw new IllegalArgumentException("Given path should be non null");
        return _getOriginalMimeType(this.mUniqueId, paramString);
    }

    public void installDrmEngine(String paramString)
    {
        if ((paramString == null) || (paramString.equals("")))
            throw new IllegalArgumentException("Given engineFilePath: " + paramString + "is not valid");
        _installDrmEngine(this.mUniqueId, paramString);
    }

    public int openConvertSession(String paramString)
    {
        if ((paramString == null) || (paramString.equals("")))
            throw new IllegalArgumentException("Path or the mimeType should be non null");
        return _openConvertSession(this.mUniqueId, paramString);
    }

    public int processDrmInfo(DrmInfo paramDrmInfo)
    {
        if ((paramDrmInfo == null) || (!paramDrmInfo.isValid()))
            throw new IllegalArgumentException("Given drmInfo is invalid/null");
        int i = -2000;
        if (this.mEventHandler != null)
        {
            Message localMessage = this.mEventHandler.obtainMessage(1002, paramDrmInfo);
            if (this.mEventHandler.sendMessage(localMessage))
                i = 0;
        }
        return i;
    }

    public void release()
    {
        if (this.mReleased)
            Log.w("DrmManagerClient", "You have already called release()");
        while (true)
        {
            return;
            this.mReleased = true;
            if (this.mEventHandler != null)
            {
                this.mEventThread.quit();
                this.mEventThread = null;
            }
            if (this.mInfoHandler != null)
            {
                this.mInfoThread.quit();
                this.mInfoThread = null;
            }
            this.mEventHandler = null;
            this.mInfoHandler = null;
            this.mOnEventListener = null;
            this.mOnInfoListener = null;
            this.mOnErrorListener = null;
            _release(this.mUniqueId);
        }
    }

    public int removeAllRights()
    {
        int i = -2000;
        if (this.mEventHandler != null)
        {
            Message localMessage = this.mEventHandler.obtainMessage(1001);
            if (this.mEventHandler.sendMessage(localMessage))
                i = 0;
        }
        return i;
    }

    public int removeRights(Uri paramUri)
    {
        if ((paramUri == null) || (Uri.EMPTY == paramUri))
            throw new IllegalArgumentException("Given uri is not valid");
        return removeRights(convertUriToPath(paramUri));
    }

    public int removeRights(String paramString)
    {
        if ((paramString == null) || (paramString.equals("")))
            throw new IllegalArgumentException("Given path should be non null");
        return _removeRights(this.mUniqueId, paramString);
    }

    public int saveRights(DrmRights paramDrmRights, String paramString1, String paramString2)
        throws IOException
    {
        if ((paramDrmRights == null) || (!paramDrmRights.isValid()))
            throw new IllegalArgumentException("Given drmRights or contentPath is not valid");
        if ((paramString1 != null) && (!paramString1.equals("")))
            DrmUtils.writeToFile(paramString1, paramDrmRights.getData());
        return _saveRights(this.mUniqueId, paramDrmRights, paramString1, paramString2);
    }

    /** @deprecated */
    public void setOnErrorListener(OnErrorListener paramOnErrorListener)
    {
        try
        {
            this.mOnErrorListener = paramOnErrorListener;
            if (paramOnErrorListener != null)
                createListeners();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setOnEventListener(OnEventListener paramOnEventListener)
    {
        try
        {
            this.mOnEventListener = paramOnEventListener;
            if (paramOnEventListener != null)
                createListeners();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setOnInfoListener(OnInfoListener paramOnInfoListener)
    {
        try
        {
            this.mOnInfoListener = paramOnInfoListener;
            if (paramOnInfoListener != null)
                createListeners();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private class InfoHandler extends Handler
    {
        public static final int INFO_EVENT_TYPE = 1;

        public InfoHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            DrmInfoEvent localDrmInfoEvent = null;
            DrmErrorEvent localDrmErrorEvent = null;
            switch (paramMessage.what)
            {
            default:
                Log.e("DrmManagerClient", "Unknown message type " + paramMessage.what);
                return;
            case 1:
            }
            int i = paramMessage.arg1;
            int j = paramMessage.arg2;
            String str = paramMessage.obj.toString();
            switch (j)
            {
            default:
                localDrmErrorEvent = new DrmErrorEvent(i, j, str);
            case 2:
            case 1:
            case 3:
            case 4:
            case 5:
            case 6:
            }
            while (true)
            {
                if ((DrmManagerClient.this.mOnInfoListener != null) && (localDrmInfoEvent != null))
                    DrmManagerClient.this.mOnInfoListener.onInfo(DrmManagerClient.this, localDrmInfoEvent);
                if ((DrmManagerClient.this.mOnErrorListener == null) || (localDrmErrorEvent == null))
                    break;
                DrmManagerClient.this.mOnErrorListener.onError(DrmManagerClient.this, localDrmErrorEvent);
                break;
                try
                {
                    DrmUtils.removeFile(str);
                    localDrmInfoEvent = new DrmInfoEvent(i, j, str);
                }
                catch (IOException localIOException)
                {
                    while (true)
                        localIOException.printStackTrace();
                }
                localDrmInfoEvent = new DrmInfoEvent(i, j, str);
            }
        }
    }

    private class EventHandler extends Handler
    {
        public EventHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            DrmEvent localDrmEvent = null;
            DrmErrorEvent localDrmErrorEvent = null;
            HashMap localHashMap = new HashMap();
            DrmInfo localDrmInfo;
            DrmInfoStatus localDrmInfoStatus;
            switch (paramMessage.what)
            {
            default:
                Log.e("DrmManagerClient", "Unknown message type " + paramMessage.what);
                return;
            case 1002:
                localDrmInfo = (DrmInfo)paramMessage.obj;
                localDrmInfoStatus = DrmManagerClient.this._processDrmInfo(DrmManagerClient.this.mUniqueId, localDrmInfo);
                localHashMap.put("drm_info_status_object", localDrmInfoStatus);
                localHashMap.put("drm_info_object", localDrmInfo);
                if ((localDrmInfoStatus != null) && (1 == localDrmInfoStatus.statusCode))
                    localDrmEvent = new DrmEvent(DrmManagerClient.this.mUniqueId, DrmManagerClient.this.getEventType(localDrmInfoStatus.infoType), null, localHashMap);
                break;
            case 1001:
            }
            while (true)
            {
                if ((DrmManagerClient.this.mOnEventListener != null) && (localDrmEvent != null))
                    DrmManagerClient.this.mOnEventListener.onEvent(DrmManagerClient.this, localDrmEvent);
                if ((DrmManagerClient.this.mOnErrorListener == null) || (localDrmErrorEvent == null))
                    break;
                DrmManagerClient.this.mOnErrorListener.onError(DrmManagerClient.this, localDrmErrorEvent);
                break;
                if (localDrmInfoStatus != null);
                for (int i = localDrmInfoStatus.infoType; ; i = localDrmInfo.getInfoType())
                {
                    localDrmErrorEvent = new DrmErrorEvent(DrmManagerClient.this.mUniqueId, DrmManagerClient.this.getErrorType(i), null, localHashMap);
                    break;
                }
                if (DrmManagerClient.this._removeAllRights(DrmManagerClient.this.mUniqueId) == 0)
                    localDrmEvent = new DrmEvent(DrmManagerClient.this.mUniqueId, 1001, null);
                else
                    localDrmErrorEvent = new DrmErrorEvent(DrmManagerClient.this.mUniqueId, 2007, null);
            }
        }
    }

    public static abstract interface OnErrorListener
    {
        public abstract void onError(DrmManagerClient paramDrmManagerClient, DrmErrorEvent paramDrmErrorEvent);
    }

    public static abstract interface OnEventListener
    {
        public abstract void onEvent(DrmManagerClient paramDrmManagerClient, DrmEvent paramDrmEvent);
    }

    public static abstract interface OnInfoListener
    {
        public abstract void onInfo(DrmManagerClient paramDrmManagerClient, DrmInfoEvent paramDrmInfoEvent);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.DrmManagerClient
 * JD-Core Version:        0.6.2
 */