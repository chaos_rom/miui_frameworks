package android.drm.mobile1;

public class DrmException extends Exception
{
    private DrmException()
    {
    }

    public DrmException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.mobile1.DrmException
 * JD-Core Version:        0.6.2
 */