package android.drm.mobile1;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public class DrmRawContent
{
    public static final int DRM_COMBINED_DELIVERY = 2;
    public static final int DRM_FORWARD_LOCK = 1;
    private static final int DRM_MIMETYPE_CONTENT = 2;
    public static final String DRM_MIMETYPE_CONTENT_STRING = "application/vnd.oma.drm.content";
    private static final int DRM_MIMETYPE_MESSAGE = 1;
    public static final String DRM_MIMETYPE_MESSAGE_STRING = "application/vnd.oma.drm.message";
    public static final int DRM_SEPARATE_DELIVERY = 3;
    public static final int DRM_SEPARATE_DELIVERY_DM = 4;
    public static final int DRM_UNKNOWN_DATA_LEN = -1;
    private static final int JNI_DRM_EOF = -2;
    private static final int JNI_DRM_FAILURE = -1;
    private static final int JNI_DRM_SUCCESS = 0;
    private static final int JNI_DRM_UNKNOWN_DATA_LEN = -3;
    private int id = -1;
    private BufferedInputStream inData;
    private int inDataLen;
    private String mediaType;
    private int rawType;
    private String rightsIssuer;

    static
    {
        try
        {
            System.loadLibrary("drm1_jni");
            return;
        }
        catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
        {
            while (true)
                System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    public DrmRawContent(InputStream paramInputStream, int paramInt, String paramString)
        throws DrmException, IOException
    {
        this.inData = new BufferedInputStream(paramInputStream, 1024);
        this.inDataLen = paramInt;
        int i;
        if ("application/vnd.oma.drm.message".equals(paramString))
            i = 1;
        while (paramInt <= 0)
        {
            throw new IllegalArgumentException("len must be > 0");
            if ("application/vnd.oma.drm.content".equals(paramString))
                i = 2;
            else
                throw new IllegalArgumentException("mimeType must be DRM_MIMETYPE_MESSAGE or DRM_MIMETYPE_CONTENT");
        }
        this.id = nativeConstructDrmContent(this.inData, this.inDataLen, i);
        if (-1 == this.id)
            throw new DrmException("nativeConstructDrmContent() returned JNI_DRM_FAILURE");
        this.rightsIssuer = nativeGetRightsAddress();
        this.rawType = nativeGetDeliveryMethod();
        if (-1 == this.rawType)
            throw new DrmException("nativeGetDeliveryMethod() returned JNI_DRM_FAILURE");
        this.mediaType = nativeGetContentType();
        if (this.mediaType == null)
            throw new DrmException("nativeGetContentType() returned null");
    }

    private native int nativeConstructDrmContent(InputStream paramInputStream, int paramInt1, int paramInt2);

    private native int nativeGetContentLength();

    private native String nativeGetContentType();

    private native int nativeGetDeliveryMethod();

    private native String nativeGetRightsAddress();

    private native int nativeReadContent(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3);

    protected native void finalize();

    public InputStream getContentInputStream(DrmRights paramDrmRights)
    {
        if (paramDrmRights == null)
            throw new NullPointerException();
        return new DrmInputStream(paramDrmRights);
    }

    public int getContentLength(DrmRights paramDrmRights)
        throws DrmException
    {
        if (paramDrmRights == null)
            throw new NullPointerException();
        int i = nativeGetContentLength();
        if (-1 == i)
            throw new DrmException("nativeGetContentLength() returned JNI_DRM_FAILURE");
        if (-3 == i)
            i = -1;
        return i;
    }

    public String getContentType()
    {
        return this.mediaType;
    }

    public int getRawType()
    {
        return this.rawType;
    }

    public String getRightsAddress()
    {
        return this.rightsIssuer;
    }

    class DrmInputStream extends InputStream
    {
        private byte[] b = new byte[1];
        private boolean isClosed = false;
        private int offset = 0;

        public DrmInputStream(DrmRights arg2)
        {
        }

        public int available()
            throws IOException
        {
            int i = DrmRawContent.this.nativeGetContentLength();
            if (-1 == i)
                throw new IOException();
            int j;
            if (-3 == i)
                j = 0;
            do
            {
                return j;
                j = i - this.offset;
            }
            while (j >= 0);
            throw new IOException();
        }

        public void close()
        {
            this.isClosed = true;
        }

        public void mark(int paramInt)
        {
        }

        public boolean markSupported()
        {
            return false;
        }

        public int read()
            throws IOException
        {
            int i = -1;
            if (i == read(this.b, 0, 1));
            while (true)
            {
                return i;
                i = 0xFF & this.b[0];
            }
        }

        public int read(byte[] paramArrayOfByte)
            throws IOException
        {
            return read(paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            int i = -1;
            if (paramArrayOfByte == null)
                throw new NullPointerException();
            if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length))
                throw new IndexOutOfBoundsException();
            if (true == this.isClosed)
                throw new IOException();
            if (paramInt2 == 0)
                i = 0;
            while (true)
            {
                return i;
                int j = DrmRawContent.this.nativeReadContent(paramArrayOfByte, paramInt1, paramInt2, this.offset);
                if (i == j)
                    throw new IOException();
                if (-2 != j)
                {
                    this.offset = (j + this.offset);
                    i = j;
                }
            }
        }

        public void reset()
            throws IOException
        {
            throw new IOException();
        }

        public long skip(long paramLong)
            throws IOException
        {
            return 0L;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.mobile1.DrmRawContent
 * JD-Core Version:        0.6.2
 */