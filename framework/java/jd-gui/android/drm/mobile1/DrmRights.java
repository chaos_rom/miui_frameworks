package android.drm.mobile1;

import java.io.PrintStream;

public class DrmRights
{
    public static final int DRM_PERMISSION_DISPLAY = 2;
    public static final int DRM_PERMISSION_EXECUTE = 3;
    public static final int DRM_PERMISSION_PLAY = 1;
    public static final int DRM_PERMISSION_PRINT = 4;
    private static final int JNI_DRM_FAILURE = -1;
    private static final int JNI_DRM_SUCCESS;
    private String roId = "";

    static
    {
        try
        {
            System.loadLibrary("drm1_jni");
            return;
        }
        catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
        {
            while (true)
                System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    private native int nativeConsumeRights(int paramInt);

    private native int nativeGetConstraintInfo(int paramInt, DrmConstraintInfo paramDrmConstraintInfo);

    public boolean consumeRights(int paramInt)
    {
        if (-1 == nativeConsumeRights(paramInt));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public DrmConstraintInfo getConstraint(int paramInt)
    {
        DrmConstraintInfo localDrmConstraintInfo = new DrmConstraintInfo();
        if (-1 == nativeGetConstraintInfo(paramInt, localDrmConstraintInfo))
            localDrmConstraintInfo = null;
        return localDrmConstraintInfo;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.mobile1.DrmRights
 * JD-Core Version:        0.6.2
 */