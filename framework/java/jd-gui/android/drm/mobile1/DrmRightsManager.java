package android.drm.mobile1;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class DrmRightsManager
{
    private static final int DRM_MIMETYPE_MESSAGE = 1;
    private static final int DRM_MIMETYPE_RIGHTS_WBXML = 4;
    public static final String DRM_MIMETYPE_RIGHTS_WBXML_STRING = "application/vnd.oma.drm.rights+wbxml";
    private static final int DRM_MIMETYPE_RIGHTS_XML = 3;
    public static final String DRM_MIMETYPE_RIGHTS_XML_STRING = "application/vnd.oma.drm.rights+xml";
    private static final int JNI_DRM_FAILURE = -1;
    private static final int JNI_DRM_SUCCESS;
    private static DrmRightsManager singleton = null;

    static
    {
        try
        {
            System.loadLibrary("drm1_jni");
            return;
        }
        catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
        {
            while (true)
                System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    /** @deprecated */
    public static DrmRightsManager getInstance()
    {
        try
        {
            if (singleton == null)
                singleton = new DrmRightsManager();
            DrmRightsManager localDrmRightsManager = singleton;
            return localDrmRightsManager;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private native int nativeDeleteRights(DrmRights paramDrmRights);

    private native int nativeGetNumOfRights();

    private native int nativeGetRightsList(DrmRights[] paramArrayOfDrmRights, int paramInt);

    private native int nativeInstallDrmRights(InputStream paramInputStream, int paramInt1, int paramInt2, DrmRights paramDrmRights);

    private native int nativeQueryRights(DrmRawContent paramDrmRawContent, DrmRights paramDrmRights);

    /** @deprecated */
    public void deleteRights(DrmRights paramDrmRights)
    {
        try
        {
            int i = nativeDeleteRights(paramDrmRights);
            if (-1 == i);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public List getRightsList()
    {
        while (true)
        {
            try
            {
                ArrayList localArrayList = new ArrayList();
                int i = nativeGetNumOfRights();
                if (-1 == i)
                {
                    localArrayList = null;
                    return localArrayList;
                }
                if (i <= 0)
                    continue;
                DrmRights[] arrayOfDrmRights = new DrmRights[i];
                int j = 0;
                if (j < i)
                {
                    arrayOfDrmRights[j] = new DrmRights();
                    j++;
                    continue;
                }
                int k = nativeGetRightsList(arrayOfDrmRights, i);
                if (-1 == k)
                {
                    localArrayList = null;
                    continue;
                    if (m < k)
                    {
                        localArrayList.add(arrayOfDrmRights[m]);
                        m++;
                        continue;
                    }
                    continue;
                }
            }
            finally
            {
            }
            int m = 0;
        }
    }

    /** @deprecated */
    public DrmRights installRights(InputStream paramInputStream, int paramInt, String paramString)
        throws DrmException, IOException
    {
        int i;
        DrmRights localDrmRights;
        do
        {
            try
            {
                boolean bool = "application/vnd.oma.drm.rights+xml".equals(paramString);
                if (bool)
                    i = 3;
                while (paramInt <= 0)
                {
                    localDrmRights = null;
                    return localDrmRights;
                    if ("application/vnd.oma.drm.rights+wbxml".equals(paramString))
                        i = 4;
                    else if ("application/vnd.oma.drm.message".equals(paramString))
                        i = 1;
                    else
                        throw new IllegalArgumentException("mimeType must be DRM_MIMETYPE_RIGHTS_XML or DRM_MIMETYPE_RIGHTS_WBXML or DRM_MIMETYPE_MESSAGE");
                }
            }
            finally
            {
            }
            localDrmRights = new DrmRights();
        }
        while (-1 != nativeInstallDrmRights(paramInputStream, paramInt, i, localDrmRights));
        throw new DrmException("nativeInstallDrmRights() returned JNI_DRM_FAILURE");
    }

    /** @deprecated */
    public DrmRights queryRights(DrmRawContent paramDrmRawContent)
    {
        try
        {
            DrmRights localDrmRights = new DrmRights();
            int i = nativeQueryRights(paramDrmRawContent, localDrmRights);
            if (-1 == i)
                localDrmRights = null;
            return localDrmRights;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.mobile1.DrmRightsManager
 * JD-Core Version:        0.6.2
 */