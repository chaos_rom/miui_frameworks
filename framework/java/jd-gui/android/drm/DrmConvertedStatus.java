package android.drm;

public class DrmConvertedStatus
{
    public static final int STATUS_ERROR = 3;
    public static final int STATUS_INPUTDATA_ERROR = 2;
    public static final int STATUS_OK = 1;
    public final byte[] convertedData;
    public final int offset;
    public final int statusCode;

    public DrmConvertedStatus(int paramInt1, byte[] paramArrayOfByte, int paramInt2)
    {
        if (!isValidStatusCode(paramInt1))
            throw new IllegalArgumentException("Unsupported status code: " + paramInt1);
        this.statusCode = paramInt1;
        this.convertedData = paramArrayOfByte;
        this.offset = paramInt2;
    }

    private boolean isValidStatusCode(int paramInt)
    {
        int i = 1;
        if ((paramInt == i) || (paramInt == 2) || (paramInt == 3));
        while (true)
        {
            return i;
            i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.DrmConvertedStatus
 * JD-Core Version:        0.6.2
 */