package android.drm;

import java.util.ArrayList;
import java.util.Iterator;

public class DrmSupportInfo
{
    private String mDescription = "";
    private final ArrayList<String> mFileSuffixList = new ArrayList();
    private final ArrayList<String> mMimeTypeList = new ArrayList();

    public void addFileSuffix(String paramString)
    {
        if (paramString == "")
            throw new IllegalArgumentException("fileSuffix is an empty string");
        this.mFileSuffixList.add(paramString);
    }

    public void addMimeType(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("mimeType is null");
        if (paramString == "")
            throw new IllegalArgumentException("mimeType is an empty string");
        this.mMimeTypeList.add(paramString);
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof DrmSupportInfo))
        {
            DrmSupportInfo localDrmSupportInfo = (DrmSupportInfo)paramObject;
            if ((this.mFileSuffixList.equals(localDrmSupportInfo.mFileSuffixList)) && (this.mMimeTypeList.equals(localDrmSupportInfo.mMimeTypeList)) && (this.mDescription.equals(localDrmSupportInfo.mDescription)))
                bool = true;
        }
        return bool;
    }

    public String getDescriprition()
    {
        return this.mDescription;
    }

    public String getDescription()
    {
        return this.mDescription;
    }

    public Iterator<String> getFileSuffixIterator()
    {
        return this.mFileSuffixList.iterator();
    }

    public Iterator<String> getMimeTypeIterator()
    {
        return this.mMimeTypeList.iterator();
    }

    public int hashCode()
    {
        return this.mFileSuffixList.hashCode() + this.mMimeTypeList.hashCode() + this.mDescription.hashCode();
    }

    boolean isSupportedFileSuffix(String paramString)
    {
        return this.mFileSuffixList.contains(paramString);
    }

    boolean isSupportedMimeType(String paramString)
    {
        int i;
        if ((paramString != null) && (!paramString.equals("")))
        {
            i = 0;
            if (i < this.mMimeTypeList.size())
                if (!((String)this.mMimeTypeList.get(i)).startsWith(paramString));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    public void setDescription(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("description is null");
        if (paramString == "")
            throw new IllegalArgumentException("description is an empty string");
        this.mDescription = paramString;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.DrmSupportInfo
 * JD-Core Version:        0.6.2
 */