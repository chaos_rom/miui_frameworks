package android.drm;

public class DrmInfoStatus
{
    public static final int STATUS_ERROR = 2;
    public static final int STATUS_OK = 1;
    public final ProcessedData data;
    public final int infoType;
    public final String mimeType;
    public final int statusCode;

    public DrmInfoStatus(int paramInt1, int paramInt2, ProcessedData paramProcessedData, String paramString)
    {
        if (!DrmInfoRequest.isValidType(paramInt2))
            throw new IllegalArgumentException("infoType: " + paramInt2);
        if (!isValidStatusCode(paramInt1))
            throw new IllegalArgumentException("Unsupported status code: " + paramInt1);
        if ((paramString == null) || (paramString == ""))
            throw new IllegalArgumentException("mimeType is null or an empty string");
        this.statusCode = paramInt1;
        this.infoType = paramInt2;
        this.data = paramProcessedData;
        this.mimeType = paramString;
    }

    private boolean isValidStatusCode(int paramInt)
    {
        int i = 1;
        if ((paramInt == i) || (paramInt == 2));
        while (true)
        {
            return i;
            i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.DrmInfoStatus
 * JD-Core Version:        0.6.2
 */