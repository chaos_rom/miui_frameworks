package android.drm;

public class ProcessedData
{
    private String mAccountId = "_NO_USER";
    private final byte[] mData;
    private String mSubscriptionId = "";

    ProcessedData(byte[] paramArrayOfByte, String paramString)
    {
        this.mData = paramArrayOfByte;
        this.mAccountId = paramString;
    }

    ProcessedData(byte[] paramArrayOfByte, String paramString1, String paramString2)
    {
        this.mData = paramArrayOfByte;
        this.mAccountId = paramString1;
        this.mSubscriptionId = paramString2;
    }

    public String getAccountId()
    {
        return this.mAccountId;
    }

    public byte[] getData()
    {
        return this.mData;
    }

    public String getSubscriptionId()
    {
        return this.mSubscriptionId;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.drm.ProcessedData
 * JD-Core Version:        0.6.2
 */