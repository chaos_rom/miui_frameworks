package android.webkit;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

abstract class WebSyncManager
    implements Runnable
{
    protected static final String LOGTAG = "websync";
    private static int SYNC_LATER_INTERVAL = 0;
    private static final int SYNC_MESSAGE = 101;
    private static int SYNC_NOW_INTERVAL = 100;
    protected WebViewDatabase mDataBase;
    protected Handler mHandler;
    private int mStartSyncRefCount;
    private Thread mSyncThread;
    private String mThreadName;

    protected WebSyncManager(Context paramContext, String paramString)
    {
        this.mThreadName = paramString;
        if (paramContext != null)
        {
            this.mSyncThread = new Thread(this);
            this.mSyncThread.setName(this.mThreadName);
            this.mSyncThread.start();
            return;
        }
        throw new IllegalStateException("WebSyncManager can't be created without context");
    }

    protected Object clone()
        throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException("doesn't implement Cloneable");
    }

    protected void onSyncInit()
    {
    }

    public void resetSync()
    {
        if (this.mHandler == null);
        while (true)
        {
            return;
            this.mHandler.removeMessages(101);
            Message localMessage = this.mHandler.obtainMessage(101);
            this.mHandler.sendMessageDelayed(localMessage, SYNC_LATER_INTERVAL);
        }
    }

    public void run()
    {
        Looper.prepare();
        this.mHandler = new SyncHandler(null);
        onSyncInit();
        Process.setThreadPriority(10);
        Message localMessage = this.mHandler.obtainMessage(101);
        this.mHandler.sendMessageDelayed(localMessage, SYNC_LATER_INTERVAL);
        Looper.loop();
    }

    public void startSync()
    {
        if (this.mHandler == null);
        while (true)
        {
            return;
            int i = 1 + this.mStartSyncRefCount;
            this.mStartSyncRefCount = i;
            if (i == 1)
            {
                Message localMessage = this.mHandler.obtainMessage(101);
                this.mHandler.sendMessageDelayed(localMessage, SYNC_LATER_INTERVAL);
            }
        }
    }

    public void stopSync()
    {
        if (this.mHandler == null);
        while (true)
        {
            return;
            int i = -1 + this.mStartSyncRefCount;
            this.mStartSyncRefCount = i;
            if (i == 0)
                this.mHandler.removeMessages(101);
        }
    }

    public void sync()
    {
        if (this.mHandler == null);
        while (true)
        {
            return;
            this.mHandler.removeMessages(101);
            Message localMessage = this.mHandler.obtainMessage(101);
            this.mHandler.sendMessageDelayed(localMessage, SYNC_NOW_INTERVAL);
        }
    }

    abstract void syncFromRamToFlash();

    private class SyncHandler extends Handler
    {
        private SyncHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            if (paramMessage.what == 101)
            {
                WebSyncManager.this.syncFromRamToFlash();
                sendMessageDelayed(obtainMessage(101), WebSyncManager.SYNC_LATER_INTERVAL);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebSyncManager
 * JD-Core Version:        0.6.2
 */