package android.webkit;

public abstract class WebSettings
{
    public static final int LOAD_CACHE_ELSE_NETWORK = 1;
    public static final int LOAD_CACHE_ONLY = 3;
    public static final int LOAD_DEFAULT = -1;
    public static final int LOAD_NORMAL = 0;
    public static final int LOAD_NO_CACHE = 2;

    public boolean enableSmoothTransition()
    {
        throw new MustOverrideException();
    }

    public boolean getAllowContentAccess()
    {
        throw new MustOverrideException();
    }

    public boolean getAllowFileAccess()
    {
        throw new MustOverrideException();
    }

    public abstract boolean getAllowFileAccessFromFileURLs();

    public abstract boolean getAllowUniversalAccessFromFileURLs();

    /** @deprecated */
    public boolean getBlockNetworkImage()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean getBlockNetworkLoads()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public boolean getBuiltInZoomControls()
    {
        throw new MustOverrideException();
    }

    public int getCacheMode()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public String getCursiveFontFamily()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean getDatabaseEnabled()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getDatabasePath()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getDefaultFixedFontSize()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getDefaultFontSize()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getDefaultTextEncodingName()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public ZoomDensity getDefaultZoom()
    {
        throw new MustOverrideException();
    }

    public boolean getDisplayZoomControls()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public boolean getDomStorageEnabled()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getFantasyFontFamily()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getFixedFontFamily()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean getJavaScriptCanOpenWindowsAutomatically()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean getJavaScriptEnabled()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public LayoutAlgorithm getLayoutAlgorithm()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public boolean getLightTouchEnabled()
    {
        throw new MustOverrideException();
    }

    public boolean getLoadWithOverviewMode()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public boolean getLoadsImagesAutomatically()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getMinimumFontSize()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getMinimumLogicalFontSize()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public boolean getNavDump()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public PluginState getPluginState()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public boolean getPluginsEnabled()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    // ERROR //
    @Deprecated
    public String getPluginsPath()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: monitorexit
        //     4: ldc 78
        //     6: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	2	7	finally
    }

    /** @deprecated */
    public String getSansSerifFontFamily()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public boolean getSaveFormData()
    {
        throw new MustOverrideException();
    }

    public boolean getSavePassword()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public String getSerifFontFamily()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getStandardFontFamily()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public TextSize getTextSize()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getTextZoom()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public boolean getUseDoubleTree()
    {
        return false;
    }

    @Deprecated
    public boolean getUseWebViewBackgroundForOverscrollBackground()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public boolean getUseWideViewPort()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public int getUserAgent()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getUserAgentString()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setAllowContentAccess(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public void setAllowFileAccess(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public abstract void setAllowFileAccessFromFileURLs(boolean paramBoolean);

    public abstract void setAllowUniversalAccessFromFileURLs(boolean paramBoolean);

    /** @deprecated */
    public void setAppCacheEnabled(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setAppCacheMaxSize(long paramLong)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setAppCachePath(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setBlockNetworkImage(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setBlockNetworkLoads(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setBuiltInZoomControls(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public void setCacheMode(int paramInt)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setCursiveFontFamily(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setDatabaseEnabled(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setDatabasePath(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setDefaultFixedFontSize(int paramInt)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setDefaultFontSize(int paramInt)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setDefaultTextEncodingName(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setDefaultZoom(ZoomDensity paramZoomDensity)
    {
        throw new MustOverrideException();
    }

    public void setDisplayZoomControls(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setDomStorageEnabled(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setEnableSmoothTransition(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setFantasyFontFamily(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setFixedFontFamily(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setGeolocationDatabasePath(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setGeolocationEnabled(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setJavaScriptCanOpenWindowsAutomatically(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setJavaScriptEnabled(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setLayoutAlgorithm(LayoutAlgorithm paramLayoutAlgorithm)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setLightTouchEnabled(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public void setLoadWithOverviewMode(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setLoadsImagesAutomatically(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setMinimumFontSize(int paramInt)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setMinimumLogicalFontSize(int paramInt)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public void setNavDump(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public void setNeedInitialFocus(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setPluginState(PluginState paramPluginState)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public void setPluginsEnabled(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public void setPluginsPath(String paramString)
    {
    }

    /** @deprecated */
    public void setRenderPriority(RenderPriority paramRenderPriority)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setSansSerifFontFamily(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setSaveFormData(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public void setSavePassword(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setSerifFontFamily(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setStandardFontFamily(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setSupportMultipleWindows(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void setSupportZoom(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setTextSize(TextSize paramTextSize)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setTextZoom(int paramInt)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public void setUseDoubleTree(boolean paramBoolean)
    {
    }

    @Deprecated
    public void setUseWebViewBackgroundForOverscrollBackground(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setUseWideViewPort(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    @Deprecated
    public void setUserAgent(int paramInt)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setUserAgentString(String paramString)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean supportMultipleWindows()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public boolean supportZoom()
    {
        throw new MustOverrideException();
    }

    public static enum PluginState
    {
        static
        {
            OFF = new PluginState("OFF", 2);
            PluginState[] arrayOfPluginState = new PluginState[3];
            arrayOfPluginState[0] = ON;
            arrayOfPluginState[1] = ON_DEMAND;
            arrayOfPluginState[2] = OFF;
        }
    }

    public static enum RenderPriority
    {
        static
        {
            HIGH = new RenderPriority("HIGH", 1);
            LOW = new RenderPriority("LOW", 2);
            RenderPriority[] arrayOfRenderPriority = new RenderPriority[3];
            arrayOfRenderPriority[0] = NORMAL;
            arrayOfRenderPriority[1] = HIGH;
            arrayOfRenderPriority[2] = LOW;
        }
    }

    public static enum ZoomDensity
    {
        int value;

        static
        {
            CLOSE = new ZoomDensity("CLOSE", 2, 75);
            ZoomDensity[] arrayOfZoomDensity = new ZoomDensity[3];
            arrayOfZoomDensity[0] = FAR;
            arrayOfZoomDensity[1] = MEDIUM;
            arrayOfZoomDensity[2] = CLOSE;
        }

        private ZoomDensity(int paramInt)
        {
            this.value = paramInt;
        }
    }

    public static enum TextSize
    {
        int value;

        static
        {
            SMALLER = new TextSize("SMALLER", 1, 75);
            NORMAL = new TextSize("NORMAL", 2, 100);
            LARGER = new TextSize("LARGER", 3, 150);
            LARGEST = new TextSize("LARGEST", 4, 200);
            TextSize[] arrayOfTextSize = new TextSize[5];
            arrayOfTextSize[0] = SMALLEST;
            arrayOfTextSize[1] = SMALLER;
            arrayOfTextSize[2] = NORMAL;
            arrayOfTextSize[3] = LARGER;
            arrayOfTextSize[4] = LARGEST;
        }

        private TextSize(int paramInt)
        {
            this.value = paramInt;
        }
    }

    public static enum LayoutAlgorithm
    {
        static
        {
            NARROW_COLUMNS = new LayoutAlgorithm("NARROW_COLUMNS", 2);
            LayoutAlgorithm[] arrayOfLayoutAlgorithm = new LayoutAlgorithm[3];
            arrayOfLayoutAlgorithm[0] = NORMAL;
            arrayOfLayoutAlgorithm[1] = SINGLE_COLUMN;
            arrayOfLayoutAlgorithm[2] = NARROW_COLUMNS;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebSettings
 * JD-Core Version:        0.6.2
 */