package android.webkit;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class SelectionFloatPanel extends FrameLayout
{
    private static int sHeight = 0;
    private static int sWidth = 0;

    public SelectionFloatPanel(Context paramContext)
    {
        super(paramContext);
    }

    public SelectionFloatPanel(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public static SelectionFloatPanel getInstance(Context paramContext, WebViewClassic paramWebViewClassic)
    {
        final SelectionFloatPanel localSelectionFloatPanel = (SelectionFloatPanel)LayoutInflater.from(paramContext).inflate(100859959, null);
        View.OnClickListener local1 = new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                SelectionFloatPanel.this.copySelection();
                SelectionFloatPanel.this.selectionDone();
                localSelectionFloatPanel.setVisibility(4);
            }
        };
        View localView1 = localSelectionFloatPanel.findViewById(101384213);
        if (localView1 != null)
        {
            localView1.setOnClickListener(local1);
            View.OnClickListener local2 = new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    SelectionFloatPanel.this.selectAll();
                    localSelectionFloatPanel.setVisibility(0);
                }
            };
            View localView2 = localSelectionFloatPanel.findViewById(101384218);
            if (localView2 == null)
                break label137;
            localView2.setOnClickListener(local2);
            View.OnClickListener local3 = new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    SelectionFloatPanel.this.webSearchText();
                    localSelectionFloatPanel.setVisibility(4);
                }
            };
            View localView3 = localSelectionFloatPanel.findViewById(101384330);
            if (localView3 == null)
                break label150;
            localView3.setOnClickListener(local3);
            ((ViewGroup)paramWebViewClassic.getWebView().getRootView()).addView(localSelectionFloatPanel);
        }
        while (true)
        {
            return localSelectionFloatPanel;
            Log.e("showFloat", "no copy view.");
            localSelectionFloatPanel = null;
            continue;
            label137: Log.e("showFloat", "no selectAll view.");
            localSelectionFloatPanel = null;
            continue;
            label150: Log.e("showFloat", "no webSearch view.");
            localSelectionFloatPanel = null;
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        sHeight = getMeasuredHeight();
        sWidth = getMeasuredWidth();
    }

    public void showAt(int paramInt1, int paramInt2)
    {
        setVisibility(0);
        setX(paramInt1);
        setY(paramInt2);
        requestLayout();
    }

    public int viewHeight()
    {
        return sHeight;
    }

    public int viewWidth()
    {
        return sWidth;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.SelectionFloatPanel
 * JD-Core Version:        0.6.2
 */