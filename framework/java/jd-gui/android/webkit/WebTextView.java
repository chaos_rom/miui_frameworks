package android.webkit;

import android.util.Log;
import java.net.MalformedURLException;
import java.net.URL;

abstract class WebTextView
{
    static final int EMAIL = 4;
    static final int FORM_NOT_AUTOFILLABLE = -1;
    private static final String LOGTAG = "WebTextView";
    static final int NORMAL_TEXT_FIELD = 0;
    static final int NUMBER = 5;
    static final int PASSWORD = 2;
    static final int SEARCH = 3;
    static final int TELEPHONE = 6;
    static final int TEXT_AREA = 1;
    static final int URL = 7;

    static String urlForAutoCompleteData(String paramString)
    {
        Object localObject = null;
        try
        {
            URL localURL = new URL(paramString);
            localObject = localURL;
            if (localObject != null)
            {
                str = localObject.getProtocol() + "://" + localObject.getHost() + localObject.getPath();
                return str;
            }
        }
        catch (MalformedURLException localMalformedURLException)
        {
            while (true)
            {
                Log.e("WebTextView", "Unable to parse URL " + null);
                continue;
                String str = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebTextView
 * JD-Core Version:        0.6.2
 */