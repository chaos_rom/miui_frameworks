package android.webkit;

import android.text.TextUtils;
import java.util.regex.Pattern;
import libcore.net.MimeUtils;

public class MimeTypeMap
{
    private static final MimeTypeMap sMimeTypeMap = new MimeTypeMap();

    public static String getFileExtensionFromUrl(String paramString)
    {
        String str2;
        int m;
        if (!TextUtils.isEmpty(paramString))
        {
            int i = paramString.lastIndexOf('#');
            if (i > 0)
                paramString = paramString.substring(0, i);
            int j = paramString.lastIndexOf('?');
            if (j > 0)
                paramString = paramString.substring(0, j);
            int k = paramString.lastIndexOf('/');
            if (k >= 0)
            {
                str2 = paramString.substring(k + 1);
                if ((str2.isEmpty()) || (!Pattern.matches("[a-zA-Z_0-9\\.\\-\\(\\)\\%]+", str2)))
                    break label116;
                m = str2.lastIndexOf('.');
                if (m < 0)
                    break label116;
            }
        }
        label116: for (String str1 = str2.substring(m + 1); ; str1 = "")
        {
            return str1;
            str2 = paramString;
            break;
        }
    }

    public static MimeTypeMap getSingleton()
    {
        return sMimeTypeMap;
    }

    private static String mimeTypeFromExtension(String paramString)
    {
        return MimeUtils.guessMimeTypeFromExtension(paramString);
    }

    public String getExtensionFromMimeType(String paramString)
    {
        return MimeUtils.guessExtensionFromMimeType(paramString);
    }

    public String getMimeTypeFromExtension(String paramString)
    {
        return MimeUtils.guessMimeTypeFromExtension(paramString);
    }

    public boolean hasExtension(String paramString)
    {
        return MimeUtils.hasExtension(paramString);
    }

    public boolean hasMimeType(String paramString)
    {
        return MimeUtils.hasMimeType(paramString);
    }

    String remapGenericMimeType(String paramString1, String paramString2, String paramString3)
    {
        if (("text/plain".equals(paramString1)) || ("application/octet-stream".equals(paramString1)))
        {
            String str1 = null;
            if (paramString3 != null)
                str1 = URLUtil.parseContentDisposition(paramString3);
            if (str1 != null)
                paramString2 = str1;
            String str2 = getMimeTypeFromExtension(getFileExtensionFromUrl(paramString2));
            if (str2 != null)
                paramString1 = str2;
        }
        while (true)
        {
            return paramString1;
            if ("text/vnd.wap.wml".equals(paramString1))
                paramString1 = "text/plain";
            else if ("application/vnd.wap.xhtml+xml".equals(paramString1))
                paramString1 = "application/xhtml+xml";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.MimeTypeMap
 * JD-Core Version:        0.6.2
 */