package android.webkit;

import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

final class SearchBoxImpl
    implements SearchBox
{
    private static final String DISPATCH_EVENT_SCRIPT = "if (window.chrome && window.chrome.searchBox && window.chrome.searchBox.on%1$s) {    window.chrome.searchBox.on%1$s();    window.searchBoxJavaBridge_.dispatchCompleteCallback('%1$s', %2$d, true);} else {    window.searchBoxJavaBridge_.dispatchCompleteCallback('%1$s', %2$d, false);}";
    private static final String EVENT_CANCEL = "cancel";
    private static final String EVENT_CHANGE = "change";
    private static final String EVENT_RESIZE = "resize";
    private static final String EVENT_SUBMIT = "submit";
    private static final String IS_SUPPORTED_SCRIPT = "if (window.searchBoxJavaBridge_) {    if (window.chrome && window.chrome.sv) {        window.searchBoxJavaBridge_.isSupportedCallback(true);    } else {        window.searchBoxJavaBridge_.isSupportedCallback(false);    }}";
    static final String JS_BRIDGE = "(function(){if (!window.chrome) {    window.chrome = {};}if (!window.chrome.searchBox) {    var sb = window.chrome.searchBox = {};    sb.setSuggestions = function(suggestions) {        if (window.searchBoxJavaBridge_) {            window.searchBoxJavaBridge_.setSuggestions(JSON.stringify(suggestions));        }    };    sb.setValue = function(valueArray) { sb.value = valueArray[0]; };    sb.value = '';    sb.x = 0;    sb.y = 0;    sb.width = 0;    sb.height = 0;    sb.selectionStart = 0;    sb.selectionEnd = 0;    sb.verbatim = false;}})();";
    static final String JS_INTERFACE_NAME = "searchBoxJavaBridge_";
    private static final String SET_DIMENSIONS_SCRIPT = "if (window.chrome && window.chrome.searchBox) {     var f = window.chrome.searchBox;    f.x = %d;    f.y = %d;    f.width = %d;    f.height = %d;}";
    private static final String SET_QUERY_SCRIPT = "if (window.chrome && window.chrome.searchBox) {    window.chrome.searchBox.setValue(%s);}";
    private static final String SET_SELECTION_SCRIPT = "if (window.chrome && window.chrome.searchBox) {    var f = window.chrome.searchBox;    f.selectionStart = %d    f.selectionEnd = %d}";
    private static final String SET_VERBATIM_SCRIPT = "if (window.chrome && window.chrome.searchBox) {    window.chrome.searchBox.verbatim = %1$s;}";
    private static final String TAG = "WebKit.SearchBoxImpl";
    private final CallbackProxy mCallbackProxy;
    private final HashMap<Integer, SearchBox.SearchBoxListener> mEventCallbacks;
    private final List<SearchBox.SearchBoxListener> mListeners = new ArrayList();
    private int mNextEventId = 1;
    private SearchBox.IsSupportedCallback mSupportedCallback;
    private final WebViewCore mWebViewCore;

    SearchBoxImpl(WebViewCore paramWebViewCore, CallbackProxy paramCallbackProxy)
    {
        this.mWebViewCore = paramWebViewCore;
        this.mCallbackProxy = paramCallbackProxy;
        this.mEventCallbacks = new HashMap();
    }

    private void dispatchEvent(String paramString, SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        if (paramSearchBoxListener != null);
        while (true)
        {
            try
            {
                i = this.mNextEventId;
                this.mNextEventId = (i + 1);
                this.mEventCallbacks.put(Integer.valueOf(i), paramSearchBoxListener);
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = paramString;
                arrayOfObject[1] = Integer.valueOf(i);
                dispatchJs(String.format("if (window.chrome && window.chrome.searchBox && window.chrome.searchBox.on%1$s) {    window.chrome.searchBox.on%1$s();    window.searchBoxJavaBridge_.dispatchCompleteCallback('%1$s', %2$d, true);} else {    window.searchBoxJavaBridge_.dispatchCompleteCallback('%1$s', %2$d, false);}", arrayOfObject));
                return;
            }
            finally
            {
            }
            int i = 0;
        }
    }

    private void dispatchJs(String paramString)
    {
        this.mWebViewCore.sendMessage(194, paramString);
    }

    private static String jsonSerialize(String paramString)
    {
        JSONStringer localJSONStringer = new JSONStringer();
        try
        {
            localJSONStringer.array().value(paramString).endArray();
            str = localJSONStringer.toString();
            return str;
        }
        catch (JSONException localJSONException)
        {
            while (true)
            {
                Log.w("WebKit.SearchBoxImpl", "Error serializing query : " + paramString);
                String str = null;
            }
        }
    }

    public void addSearchBoxListener(SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        synchronized (this.mListeners)
        {
            this.mListeners.add(paramSearchBoxListener);
            return;
        }
    }

    public void dispatchCompleteCallback(String paramString, int paramInt, boolean paramBoolean)
    {
        this.mCallbackProxy.onSearchboxDispatchCompleteCallback(paramString, paramInt, paramBoolean);
    }

    public void handleDispatchCompleteCallback(String paramString, int paramInt, boolean paramBoolean)
    {
        if (paramInt != 0);
        while (true)
        {
            SearchBox.SearchBoxListener localSearchBoxListener;
            try
            {
                localSearchBoxListener = (SearchBox.SearchBoxListener)this.mEventCallbacks.get(Integer.valueOf(paramInt));
                this.mEventCallbacks.remove(Integer.valueOf(paramInt));
                if (localSearchBoxListener != null)
                {
                    if (TextUtils.equals("change", paramString))
                        localSearchBoxListener.onChangeComplete(paramBoolean);
                }
                else
                    return;
            }
            finally
            {
            }
            if (TextUtils.equals("submit", paramString))
                localSearchBoxListener.onSubmitComplete(paramBoolean);
            else if (TextUtils.equals("resize", paramString))
                localSearchBoxListener.onResizeComplete(paramBoolean);
            else if (TextUtils.equals("cancel", paramString))
                localSearchBoxListener.onCancelComplete(paramBoolean);
        }
    }

    public void handleIsSupportedCallback(boolean paramBoolean)
    {
        SearchBox.IsSupportedCallback localIsSupportedCallback = this.mSupportedCallback;
        this.mSupportedCallback = null;
        if (localIsSupportedCallback != null)
            localIsSupportedCallback.searchBoxIsSupported(paramBoolean);
    }

    void handleSuggestions(String paramString, List<String> paramList)
    {
        synchronized (this.mListeners)
        {
            for (int i = -1 + this.mListeners.size(); i >= 0; i--)
                ((SearchBox.SearchBoxListener)this.mListeners.get(i)).onSuggestionsReceived(paramString, paramList);
            return;
        }
    }

    public void isSupported(SearchBox.IsSupportedCallback paramIsSupportedCallback)
    {
        this.mSupportedCallback = paramIsSupportedCallback;
        dispatchJs("if (window.searchBoxJavaBridge_) {    if (window.chrome && window.chrome.sv) {        window.searchBoxJavaBridge_.isSupportedCallback(true);    } else {        window.searchBoxJavaBridge_.isSupportedCallback(false);    }}");
    }

    public void isSupportedCallback(boolean paramBoolean)
    {
        this.mCallbackProxy.onIsSupportedCallback(paramBoolean);
    }

    public void oncancel(SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        dispatchEvent("cancel", paramSearchBoxListener);
    }

    public void onchange(SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        dispatchEvent("change", paramSearchBoxListener);
    }

    public void onresize(SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        dispatchEvent("resize", paramSearchBoxListener);
    }

    public void onsubmit(SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        dispatchEvent("submit", paramSearchBoxListener);
    }

    public void removeSearchBoxListener(SearchBox.SearchBoxListener paramSearchBoxListener)
    {
        synchronized (this.mListeners)
        {
            this.mListeners.remove(paramSearchBoxListener);
            return;
        }
    }

    public void setDimensions(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        dispatchJs(String.format("if (window.chrome && window.chrome.searchBox) {     var f = window.chrome.searchBox;    f.x = %d;    f.y = %d;    f.width = %d;    f.height = %d;}", arrayOfObject));
    }

    public void setQuery(String paramString)
    {
        String str = jsonSerialize(paramString);
        if (str != null)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = str;
            dispatchJs(String.format("if (window.chrome && window.chrome.searchBox) {    window.chrome.searchBox.setValue(%s);}", arrayOfObject));
        }
    }

    public void setSelection(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        dispatchJs(String.format("if (window.chrome && window.chrome.searchBox) {    var f = window.chrome.searchBox;    f.selectionStart = %d    f.selectionEnd = %d}", arrayOfObject));
    }

    public void setSuggestions(String paramString)
    {
        if (paramString == null);
        while (true)
        {
            return;
            ArrayList localArrayList = new ArrayList();
            String str1;
            try
            {
                JSONObject localJSONObject = new JSONObject(paramString);
                str1 = localJSONObject.getString("query");
                JSONArray localJSONArray = localJSONObject.getJSONArray("suggestions");
                for (int i = 0; i < localJSONArray.length(); i++)
                {
                    String str2 = localJSONArray.getJSONObject(i).getString("value");
                    if (str2 != null)
                        localArrayList.add(str2);
                }
            }
            catch (JSONException localJSONException)
            {
                Log.w("WebKit.SearchBoxImpl", "Error parsing json [" + paramString + "], exception = " + localJSONException);
            }
            continue;
            this.mCallbackProxy.onSearchboxSuggestionsReceived(str1, localArrayList);
        }
    }

    public void setVerbatim(boolean paramBoolean)
    {
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = String.valueOf(paramBoolean);
        dispatchJs(String.format("if (window.chrome && window.chrome.searchBox) {    window.chrome.searchBox.verbatim = %1$s;}", arrayOfObject));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.SearchBoxImpl
 * JD-Core Version:        0.6.2
 */