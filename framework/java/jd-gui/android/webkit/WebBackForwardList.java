package android.webkit;

import java.io.Serializable;
import java.util.ArrayList;

public class WebBackForwardList
    implements Cloneable, Serializable
{
    private ArrayList<WebHistoryItem> mArray = new ArrayList();
    private final CallbackProxy mCallbackProxy;
    private boolean mClearPending;
    private int mCurrentIndex = -1;

    WebBackForwardList(CallbackProxy paramCallbackProxy)
    {
        this.mCallbackProxy = paramCallbackProxy;
    }

    private static native void nativeClose(int paramInt);

    /** @deprecated */
    private void removeHistoryItem(int paramInt)
    {
        try
        {
            ((WebHistoryItem)this.mArray.remove(paramInt));
            this.mCurrentIndex = (-1 + this.mCurrentIndex);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    static synchronized native void restoreIndex(int paramInt1, int paramInt2);

    /** @deprecated */
    void addHistoryItem(WebHistoryItem paramWebHistoryItem)
    {
        try
        {
            this.mCurrentIndex = (1 + this.mCurrentIndex);
            int i = this.mArray.size();
            int j = this.mCurrentIndex;
            if (j != i)
                for (int k = i - 1; k >= j; k--)
                    ((WebHistoryItem)this.mArray.remove(k));
            this.mArray.add(paramWebHistoryItem);
            if (this.mCallbackProxy != null)
                this.mCallbackProxy.onNewHistoryItem(paramWebHistoryItem);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    protected WebBackForwardList clone()
    {
        try
        {
            WebBackForwardList localWebBackForwardList = new WebBackForwardList(null);
            if (this.mClearPending)
                localWebBackForwardList.addHistoryItem(getCurrentItem());
            while (true)
            {
                return localWebBackForwardList;
                localWebBackForwardList.mCurrentIndex = this.mCurrentIndex;
                int i = getSize();
                localWebBackForwardList.mArray = new ArrayList(i);
                for (int j = 0; j < i; j++)
                    localWebBackForwardList.mArray.add(((WebHistoryItem)this.mArray.get(j)).clone());
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    void close(int paramInt)
    {
        try
        {
            this.mArray.clear();
            this.mCurrentIndex = -1;
            nativeClose(paramInt);
            this.mClearPending = false;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    boolean getClearPending()
    {
        try
        {
            boolean bool = this.mClearPending;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getCurrentIndex()
    {
        try
        {
            int i = this.mCurrentIndex;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public WebHistoryItem getCurrentItem()
    {
        try
        {
            WebHistoryItem localWebHistoryItem = getItemAtIndex(this.mCurrentIndex);
            return localWebHistoryItem;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public WebHistoryItem getItemAtIndex(int paramInt)
    {
        if (paramInt >= 0);
        try
        {
            int i = getSize();
            if (paramInt >= i);
            for (WebHistoryItem localWebHistoryItem = null; ; localWebHistoryItem = (WebHistoryItem)this.mArray.get(paramInt))
                return localWebHistoryItem;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getSize()
    {
        try
        {
            int i = this.mArray.size();
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void setClearPending()
    {
        try
        {
            this.mClearPending = true;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void setCurrentIndex(int paramInt)
    {
        try
        {
            this.mCurrentIndex = paramInt;
            if (this.mCallbackProxy != null)
                this.mCallbackProxy.onIndexChanged(getItemAtIndex(paramInt), paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebBackForwardList
 * JD-Core Version:        0.6.2
 */