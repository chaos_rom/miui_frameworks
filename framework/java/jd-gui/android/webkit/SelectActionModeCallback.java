package android.webkit;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.provider.Browser;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

class SelectActionModeCallback
    implements ActionMode.Callback
{
    private ActionMode mActionMode;
    private boolean mIsTextSelected = true;
    private WebViewClassic mWebView;

    private void setMenuVisibility(Menu paramMenu, boolean paramBoolean, int paramInt)
    {
        MenuItem localMenuItem = paramMenu.findItem(paramInt);
        if (localMenuItem != null)
            localMenuItem.setVisible(paramBoolean);
    }

    void finish()
    {
        if (this.mActionMode != null)
            this.mActionMode.finish();
    }

    public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
        boolean bool = false;
        switch (paramMenuItem.getItemId())
        {
        default:
            return bool;
        case 16908320:
            this.mWebView.cutSelection();
            paramActionMode.finish();
        case 16908321:
        case 16908322:
        case 16909171:
        case 16909170:
        case 16909172:
        case 16909173:
        }
        while (true)
        {
            bool = true;
            break;
            this.mWebView.copySelection();
            paramActionMode.finish();
            continue;
            this.mWebView.pasteFromClipboard();
            paramActionMode.finish();
            continue;
            String str2 = this.mWebView.getSelection();
            Browser.sendString(this.mWebView.getContext(), str2);
            paramActionMode.finish();
            continue;
            this.mWebView.selectAll();
            continue;
            String str1 = this.mWebView.getSelection();
            paramActionMode.finish();
            this.mWebView.showFindDialog(str1, false);
            continue;
            paramActionMode.finish();
            Intent localIntent = new Intent("android.intent.action.WEB_SEARCH");
            localIntent.putExtra("new_search", true);
            localIntent.putExtra("query", this.mWebView.getSelection());
            if (!(this.mWebView.getContext() instanceof Activity))
                localIntent.addFlags(268435456);
            this.mWebView.getContext().startActivity(localIntent);
        }
    }

    public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
        boolean bool1 = false;
        paramActionMode.getMenuInflater().inflate(18087936, paramMenu);
        Context localContext = this.mWebView.getContext();
        paramActionMode.setTitle(localContext.getString(17040319));
        paramActionMode.setTitleOptionalHint(true);
        ClipboardManager localClipboardManager = (ClipboardManager)localContext.getSystemService("clipboard");
        boolean bool2 = paramActionMode.isUiFocusable();
        boolean bool3 = this.mWebView.focusCandidateIsEditableText();
        boolean bool4;
        if ((bool3) && (localClipboardManager.hasPrimaryClip()) && (bool2))
        {
            bool4 = true;
            if ((bool3) || (!bool2))
                break label189;
        }
        label189: for (boolean bool5 = true; ; bool5 = false)
        {
            if ((bool3) && (this.mIsTextSelected) && (bool2))
                bool1 = true;
            boolean bool6 = this.mIsTextSelected;
            boolean bool7 = this.mIsTextSelected;
            setMenuVisibility(paramMenu, bool5, 16909172);
            setMenuVisibility(paramMenu, bool4, 16908322);
            setMenuVisibility(paramMenu, bool1, 16908320);
            setMenuVisibility(paramMenu, bool6, 16908321);
            setMenuVisibility(paramMenu, bool7, 16909173);
            this.mActionMode = paramActionMode;
            return true;
            bool4 = false;
            break;
        }
    }

    public void onDestroyActionMode(ActionMode paramActionMode)
    {
        this.mWebView.selectionDone();
    }

    public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
        return true;
    }

    void setTextSelected(boolean paramBoolean)
    {
        this.mIsTextSelected = paramBoolean;
    }

    void setWebView(WebViewClassic paramWebViewClassic)
    {
        this.mWebView = paramWebViewClassic;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.SelectActionModeCallback
 * JD-Core Version:        0.6.2
 */