package android.webkit;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

final class WebViewInputDispatcher
{
    private static final boolean DEBUG = false;
    private static final int DOUBLE_TAP_TIMEOUT = 0;
    private static final boolean ENABLE_EVENT_BATCHING = true;
    public static final int EVENT_TYPE_CLICK = 4;
    public static final int EVENT_TYPE_DOUBLE_TAP = 5;
    public static final int EVENT_TYPE_HIT_TEST = 6;
    public static final int EVENT_TYPE_HOVER = 1;
    public static final int EVENT_TYPE_LONG_PRESS = 3;
    public static final int EVENT_TYPE_SCROLL = 2;
    public static final int EVENT_TYPE_TOUCH = 0;
    public static final int FLAG_PRIVATE = 1;
    public static final int FLAG_WEBKIT_IN_PROGRESS = 2;
    public static final int FLAG_WEBKIT_TIMEOUT = 4;
    public static final int FLAG_WEBKIT_TRANSFORMED_EVENT = 8;
    private static final int LONG_PRESS_TIMEOUT = 0;
    private static final int MAX_DISPATCH_EVENT_POOL_SIZE = 10;
    private static final int PRESSED_STATE_DURATION = 0;
    private static final String TAG = "WebViewInputDispatcher";
    private static final int TAP_TIMEOUT = 0;
    private static final long WEBKIT_TIMEOUT_MILLIS = 200L;
    private DispatchEvent mDispatchEventPool;
    private int mDispatchEventPoolSize;
    private float mDoubleTapSlopSquared;
    private float mInitialDownX;
    private float mInitialDownY;
    private boolean mIsDoubleTapCandidate;
    private boolean mIsTapCandidate;
    private final Object mLock = new Object();
    private boolean mPostClickScheduled;
    private boolean mPostDoNotSendTouchEventsToWebKitUntilNextGesture;
    private boolean mPostHideTapHighlightScheduled;
    private float mPostLastWebKitScale;
    private int mPostLastWebKitXOffset;
    private int mPostLastWebKitYOffset;
    private boolean mPostLongPressScheduled;
    private boolean mPostSendTouchEventsToWebKit;
    private boolean mPostShowTapHighlightScheduled;
    private final TouchStream mPostTouchStream = new TouchStream(null);
    private float mTouchSlopSquared;
    private final UiCallbacks mUiCallbacks;
    private final DispatchEventQueue mUiDispatchEventQueue = new DispatchEventQueue(null);
    private boolean mUiDispatchScheduled;
    private final UiHandler mUiHandler;
    private final TouchStream mUiTouchStream = new TouchStream(null);
    private final WebKitCallbacks mWebKitCallbacks;
    private final DispatchEventQueue mWebKitDispatchEventQueue = new DispatchEventQueue(null);
    private boolean mWebKitDispatchScheduled;
    private final WebKitHandler mWebKitHandler;
    private boolean mWebKitTimeoutScheduled;
    private long mWebKitTimeoutTime;
    private final TouchStream mWebKitTouchStream = new TouchStream(null);

    static
    {
        if (!WebViewInputDispatcher.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
            LONG_PRESS_TIMEOUT = ViewConfiguration.getLongPressTimeout() + TAP_TIMEOUT;
            DOUBLE_TAP_TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
            PRESSED_STATE_DURATION = ViewConfiguration.getPressedStateDuration();
            return;
        }
    }

    public WebViewInputDispatcher(UiCallbacks paramUiCallbacks, WebKitCallbacks paramWebKitCallbacks)
    {
        this.mUiCallbacks = paramUiCallbacks;
        this.mUiHandler = new UiHandler(paramUiCallbacks.getUiLooper());
        this.mWebKitCallbacks = paramWebKitCallbacks;
        this.mWebKitHandler = new WebKitHandler(paramWebKitCallbacks.getWebKitLooper());
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(this.mUiCallbacks.getContext());
        this.mDoubleTapSlopSquared = localViewConfiguration.getScaledDoubleTapSlop();
        this.mDoubleTapSlopSquared *= this.mDoubleTapSlopSquared;
        this.mTouchSlopSquared = localViewConfiguration.getScaledTouchSlop();
        this.mTouchSlopSquared *= this.mTouchSlopSquared;
    }

    private boolean batchEventLocked(DispatchEvent paramDispatchEvent1, DispatchEvent paramDispatchEvent2)
    {
        if ((paramDispatchEvent2 != null) && (paramDispatchEvent2.mEvent != null) && (paramDispatchEvent1.mEvent != null) && (paramDispatchEvent1.mEventType == paramDispatchEvent2.mEventType) && (paramDispatchEvent1.mFlags == paramDispatchEvent2.mFlags) && (paramDispatchEvent1.mWebKitXOffset == paramDispatchEvent2.mWebKitXOffset) && (paramDispatchEvent1.mWebKitYOffset == paramDispatchEvent2.mWebKitYOffset) && (paramDispatchEvent1.mWebKitScale == paramDispatchEvent2.mWebKitScale));
        for (boolean bool = paramDispatchEvent2.mEvent.addBatch(paramDispatchEvent1.mEvent); ; bool = false)
            return bool;
    }

    private void checkForDoubleTapOnDownLocked(MotionEvent paramMotionEvent)
    {
        this.mIsDoubleTapCandidate = false;
        if (!this.mPostClickScheduled);
        while (true)
        {
            return;
            int i = (int)this.mInitialDownX - (int)paramMotionEvent.getX();
            int j = (int)this.mInitialDownY - (int)paramMotionEvent.getY();
            if (i * i + j * j < this.mDoubleTapSlopSquared)
            {
                unscheduleClickLocked();
                this.mIsDoubleTapCandidate = true;
            }
        }
    }

    private void checkForSlopLocked(MotionEvent paramMotionEvent)
    {
        if (!this.mIsTapCandidate);
        while (true)
        {
            return;
            int i = (int)this.mInitialDownX - (int)paramMotionEvent.getX();
            int j = (int)this.mInitialDownY - (int)paramMotionEvent.getY();
            if (i * i + j * j > this.mTouchSlopSquared)
            {
                unscheduleLongPressLocked();
                this.mIsTapCandidate = false;
                hideTapCandidateLocked();
            }
        }
    }

    private DispatchEvent copyDispatchEventLocked(DispatchEvent paramDispatchEvent)
    {
        DispatchEvent localDispatchEvent = obtainUninitializedDispatchEventLocked();
        if (paramDispatchEvent.mEvent != null)
            localDispatchEvent.mEvent = paramDispatchEvent.mEvent.copy();
        localDispatchEvent.mEventType = paramDispatchEvent.mEventType;
        localDispatchEvent.mFlags = paramDispatchEvent.mFlags;
        localDispatchEvent.mTimeoutTime = paramDispatchEvent.mTimeoutTime;
        localDispatchEvent.mWebKitXOffset = paramDispatchEvent.mWebKitXOffset;
        localDispatchEvent.mWebKitYOffset = paramDispatchEvent.mWebKitYOffset;
        localDispatchEvent.mWebKitScale = paramDispatchEvent.mWebKitScale;
        localDispatchEvent.mNext = paramDispatchEvent.mNext;
        return localDispatchEvent;
    }

    private void dispatchUiEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
    {
        this.mUiCallbacks.dispatchUiEvent(paramMotionEvent, paramInt1, paramInt2);
    }

    private void dispatchUiEvents(boolean paramBoolean)
    {
        synchronized (this.mLock)
        {
            MotionEvent localMotionEvent;
            int i;
            int j;
            do
            {
                DispatchEvent localDispatchEvent = this.mUiDispatchEventQueue.dequeue();
                if (localDispatchEvent == null)
                {
                    if (this.mUiDispatchScheduled)
                    {
                        this.mUiDispatchScheduled = false;
                        if (!paramBoolean)
                            this.mUiHandler.removeMessages(1);
                    }
                    return;
                }
                localMotionEvent = localDispatchEvent.mEvent;
                if ((localMotionEvent != null) && ((0x8 & localDispatchEvent.mFlags) != 0))
                {
                    localMotionEvent.scale(1.0F / localDispatchEvent.mWebKitScale);
                    localMotionEvent.offsetLocation(-localDispatchEvent.mWebKitXOffset, -localDispatchEvent.mWebKitYOffset);
                    localDispatchEvent.mFlags = (0xFFFFFFF7 & localDispatchEvent.mFlags);
                }
                i = localDispatchEvent.mEventType;
                if (i == 0)
                    localMotionEvent = this.mUiTouchStream.update(localMotionEvent);
                j = localDispatchEvent.mFlags;
                if (localMotionEvent == localDispatchEvent.mEvent)
                    localDispatchEvent.mEvent = null;
                recycleDispatchEventLocked(localDispatchEvent);
                if (i == 4)
                    scheduleHideTapHighlightLocked();
            }
            while (localMotionEvent == null);
            dispatchUiEvent(localMotionEvent, i, j);
            localMotionEvent.recycle();
        }
    }

    private boolean dispatchWebKitEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
    {
        return this.mWebKitCallbacks.dispatchWebKitEvent(this, paramMotionEvent, paramInt1, paramInt2);
    }

    // ERROR //
    private void dispatchWebKitEvents(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 152	android/webkit/WebViewInputDispatcher:mWebKitDispatchEventQueue	Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;
        //     11: getfield 338	android/webkit/WebViewInputDispatcher$DispatchEventQueue:mHead	Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     14: astore 4
        //     16: aload 4
        //     18: ifnonnull +30 -> 48
        //     21: aload_0
        //     22: getfield 340	android/webkit/WebViewInputDispatcher:mWebKitDispatchScheduled	Z
        //     25: ifeq +20 -> 45
        //     28: aload_0
        //     29: iconst_0
        //     30: putfield 340	android/webkit/WebViewInputDispatcher:mWebKitDispatchScheduled	Z
        //     33: iload_1
        //     34: ifne +11 -> 45
        //     37: aload_0
        //     38: getfield 177	android/webkit/WebViewInputDispatcher:mWebKitHandler	Landroid/webkit/WebViewInputDispatcher$WebKitHandler;
        //     41: iconst_1
        //     42: invokevirtual 341	android/webkit/WebViewInputDispatcher$WebKitHandler:removeMessages	(I)V
        //     45: aload_2
        //     46: monitorexit
        //     47: return
        //     48: aload 4
        //     50: getfield 228	android/webkit/WebViewInputDispatcher$DispatchEvent:mEvent	Landroid/view/MotionEvent;
        //     53: astore 5
        //     55: aload 5
        //     57: ifnull +43 -> 100
        //     60: aload 5
        //     62: aload 4
        //     64: getfield 237	android/webkit/WebViewInputDispatcher$DispatchEvent:mWebKitXOffset	I
        //     67: i2f
        //     68: aload 4
        //     70: getfield 240	android/webkit/WebViewInputDispatcher$DispatchEvent:mWebKitYOffset	I
        //     73: i2f
        //     74: invokevirtual 315	android/view/MotionEvent:offsetLocation	(FF)V
        //     77: aload 5
        //     79: aload 4
        //     81: getfield 243	android/webkit/WebViewInputDispatcher$DispatchEvent:mWebKitScale	F
        //     84: invokevirtual 311	android/view/MotionEvent:scale	(F)V
        //     87: aload 4
        //     89: bipush 8
        //     91: aload 4
        //     93: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     96: ior
        //     97: putfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     100: aload 4
        //     102: getfield 231	android/webkit/WebViewInputDispatcher$DispatchEvent:mEventType	I
        //     105: istore 6
        //     107: iload 6
        //     109: ifne +14 -> 123
        //     112: aload_0
        //     113: getfield 154	android/webkit/WebViewInputDispatcher:mWebKitTouchStream	Landroid/webkit/WebViewInputDispatcher$TouchStream;
        //     116: aload 5
        //     118: invokevirtual 319	android/webkit/WebViewInputDispatcher$TouchStream:update	(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
        //     121: astore 5
        //     123: aload 4
        //     125: iconst_2
        //     126: aload 4
        //     128: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     131: ior
        //     132: putfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     135: aload 4
        //     137: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     140: istore 7
        //     142: aload_2
        //     143: monitorexit
        //     144: aload 5
        //     146: ifnonnull +102 -> 248
        //     149: iconst_0
        //     150: istore 8
        //     152: aload_0
        //     153: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     156: astore 9
        //     158: aload 9
        //     160: monitorenter
        //     161: aload 4
        //     163: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     166: istore 11
        //     168: aload 4
        //     170: iload 11
        //     172: bipush 253
        //     174: iand
        //     175: putfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     178: aload 5
        //     180: aload 4
        //     182: getfield 228	android/webkit/WebViewInputDispatcher$DispatchEvent:mEvent	Landroid/view/MotionEvent;
        //     185: if_acmpeq +78 -> 263
        //     188: iconst_1
        //     189: istore 12
        //     191: iload 11
        //     193: iconst_4
        //     194: iand
        //     195: ifeq +74 -> 269
        //     198: aload_0
        //     199: aload 4
        //     201: invokespecial 323	android/webkit/WebViewInputDispatcher:recycleDispatchEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     204: aload 5
        //     206: ifnull +13 -> 219
        //     209: iload 12
        //     211: ifeq +8 -> 219
        //     214: aload 5
        //     216: invokevirtual 330	android/view/MotionEvent:recycle	()V
        //     219: iload 6
        //     221: iconst_4
        //     222: if_icmpne +7 -> 229
        //     225: aload_0
        //     226: invokespecial 326	android/webkit/WebViewInputDispatcher:scheduleHideTapHighlightLocked	()V
        //     229: aload 9
        //     231: monitorexit
        //     232: goto -232 -> 0
        //     235: astore 10
        //     237: aload 9
        //     239: monitorexit
        //     240: aload 10
        //     242: athrow
        //     243: astore_3
        //     244: aload_2
        //     245: monitorexit
        //     246: aload_3
        //     247: athrow
        //     248: aload_0
        //     249: aload 5
        //     251: iload 6
        //     253: iload 7
        //     255: invokespecial 343	android/webkit/WebViewInputDispatcher:dispatchWebKitEvent	(Landroid/view/MotionEvent;II)Z
        //     258: istore 8
        //     260: goto -108 -> 152
        //     263: iconst_0
        //     264: istore 12
        //     266: goto -75 -> 191
        //     269: getstatic 115	android/webkit/WebViewInputDispatcher:$assertionsDisabled	Z
        //     272: ifne +23 -> 295
        //     275: aload_0
        //     276: getfield 152	android/webkit/WebViewInputDispatcher:mWebKitDispatchEventQueue	Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;
        //     279: getfield 338	android/webkit/WebViewInputDispatcher$DispatchEventQueue:mHead	Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     282: aload 4
        //     284: if_acmpeq +11 -> 295
        //     287: new 345	java/lang/AssertionError
        //     290: dup
        //     291: invokespecial 346	java/lang/AssertionError:<init>	()V
        //     294: athrow
        //     295: aload_0
        //     296: getfield 152	android/webkit/WebViewInputDispatcher:mWebKitDispatchEventQueue	Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;
        //     299: invokevirtual 301	android/webkit/WebViewInputDispatcher$DispatchEventQueue:dequeue	()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     302: pop
        //     303: aload_0
        //     304: invokespecial 349	android/webkit/WebViewInputDispatcher:updateWebKitTimeoutLocked	()V
        //     307: iload 11
        //     309: iconst_1
        //     310: iand
        //     311: ifeq +12 -> 323
        //     314: aload_0
        //     315: aload 4
        //     317: invokespecial 323	android/webkit/WebViewInputDispatcher:recycleDispatchEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     320: goto -116 -> 204
        //     323: iload 8
        //     325: ifeq +22 -> 347
        //     328: aload 4
        //     330: getfield 231	android/webkit/WebViewInputDispatcher$DispatchEvent:mEventType	I
        //     333: ifne -129 -> 204
        //     336: aload_0
        //     337: invokespecial 352	android/webkit/WebViewInputDispatcher:enqueueUiCancelTouchEventIfNeededLocked	()V
        //     340: aload_0
        //     341: invokespecial 275	android/webkit/WebViewInputDispatcher:unscheduleLongPressLocked	()V
        //     344: goto -140 -> 204
        //     347: aload_0
        //     348: aload 4
        //     350: invokespecial 355	android/webkit/WebViewInputDispatcher:enqueueUiEventUnbatchedLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     353: goto -149 -> 204
        //
        // Exception table:
        //     from	to	target	type
        //     161	240	235	finally
        //     269	353	235	finally
        //     7	144	243	finally
        //     244	246	243	finally
    }

    private void drainStaleWebKitEventsLocked()
    {
        DispatchEvent localDispatchEvent;
        for (Object localObject = this.mWebKitDispatchEventQueue.mHead; (localObject != null) && (((DispatchEvent)localObject).mNext != null) && (isMoveEventLocked((DispatchEvent)localObject)) && (isMoveEventLocked(((DispatchEvent)localObject).mNext)); localObject = localDispatchEvent)
        {
            localDispatchEvent = ((DispatchEvent)localObject).mNext;
            skipWebKitEventLocked((DispatchEvent)localObject);
        }
        this.mWebKitDispatchEventQueue.mHead = ((DispatchEvent)localObject);
    }

    private void enqueueDoubleTapLocked(MotionEvent paramMotionEvent)
    {
        enqueueEventLocked(obtainDispatchEventLocked(MotionEvent.obtainNoHistory(paramMotionEvent), 5, 0, this.mPostLastWebKitXOffset, this.mPostLastWebKitYOffset, this.mPostLastWebKitScale));
    }

    private void enqueueEventLocked(DispatchEvent paramDispatchEvent)
    {
        if (!shouldSkipWebKit(paramDispatchEvent))
            enqueueWebKitEventLocked(paramDispatchEvent);
        while (true)
        {
            return;
            enqueueUiEventLocked(paramDispatchEvent);
        }
    }

    private void enqueueHitTestLocked(MotionEvent paramMotionEvent)
    {
        this.mUiCallbacks.clearPreviousHitTest();
        enqueueEventLocked(obtainDispatchEventLocked(MotionEvent.obtainNoHistory(paramMotionEvent), 6, 0, this.mPostLastWebKitXOffset, this.mPostLastWebKitYOffset, this.mPostLastWebKitScale));
    }

    private void enqueueUiCancelTouchEventIfNeededLocked()
    {
        if ((this.mUiTouchStream.isCancelNeeded()) || (!this.mUiDispatchEventQueue.isEmpty()))
            enqueueUiEventUnbatchedLocked(obtainDispatchEventLocked(null, 0, 1, 0, 0, 1.0F));
    }

    private void enqueueUiEventLocked(DispatchEvent paramDispatchEvent)
    {
        if (batchEventLocked(paramDispatchEvent, this.mUiDispatchEventQueue.mTail))
            recycleDispatchEventLocked(paramDispatchEvent);
        while (true)
        {
            return;
            enqueueUiEventUnbatchedLocked(paramDispatchEvent);
        }
    }

    private void enqueueUiEventUnbatchedLocked(DispatchEvent paramDispatchEvent)
    {
        this.mUiDispatchEventQueue.enqueue(paramDispatchEvent);
        scheduleUiDispatchLocked();
    }

    private void enqueueWebKitCancelTouchEventIfNeededLocked()
    {
        if ((this.mWebKitTouchStream.isCancelNeeded()) || (!this.mWebKitDispatchEventQueue.isEmpty()))
        {
            enqueueWebKitEventUnbatchedLocked(obtainDispatchEventLocked(null, 0, 1, 0, 0, 1.0F));
            this.mPostDoNotSendTouchEventsToWebKitUntilNextGesture = true;
        }
    }

    private void enqueueWebKitEventLocked(DispatchEvent paramDispatchEvent)
    {
        if (batchEventLocked(paramDispatchEvent, this.mWebKitDispatchEventQueue.mTail))
            recycleDispatchEventLocked(paramDispatchEvent);
        while (true)
        {
            return;
            enqueueWebKitEventUnbatchedLocked(paramDispatchEvent);
        }
    }

    private void enqueueWebKitEventUnbatchedLocked(DispatchEvent paramDispatchEvent)
    {
        this.mWebKitDispatchEventQueue.enqueue(paramDispatchEvent);
        scheduleWebKitDispatchLocked();
        updateWebKitTimeoutLocked();
    }

    // ERROR //
    private void handleWebKitTimeout()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 421	android/webkit/WebViewInputDispatcher:mWebKitTimeoutScheduled	Z
        //     11: ifne +8 -> 19
        //     14: aload_1
        //     15: monitorexit
        //     16: goto +101 -> 117
        //     19: aload_0
        //     20: iconst_0
        //     21: putfield 421	android/webkit/WebViewInputDispatcher:mWebKitTimeoutScheduled	Z
        //     24: aload_0
        //     25: getfield 152	android/webkit/WebViewInputDispatcher:mWebKitDispatchEventQueue	Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;
        //     28: invokevirtual 424	android/webkit/WebViewInputDispatcher$DispatchEventQueue:dequeueList	()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     31: astore_3
        //     32: iconst_2
        //     33: aload_3
        //     34: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     37: iand
        //     38: ifeq +27 -> 65
        //     41: aload_3
        //     42: iconst_4
        //     43: aload_3
        //     44: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     47: ior
        //     48: putfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     51: iconst_1
        //     52: aload_3
        //     53: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     56: iand
        //     57: ifeq +29 -> 86
        //     60: aload_3
        //     61: getfield 294	android/webkit/WebViewInputDispatcher$DispatchEvent:mNext	Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     64: astore_3
        //     65: aload_3
        //     66: ifnull +45 -> 111
        //     69: aload_3
        //     70: getfield 294	android/webkit/WebViewInputDispatcher$DispatchEvent:mNext	Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     73: astore 4
        //     75: aload_0
        //     76: aload_3
        //     77: invokespecial 363	android/webkit/WebViewInputDispatcher:skipWebKitEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     80: aload 4
        //     82: astore_3
        //     83: goto -18 -> 65
        //     86: aload_0
        //     87: aload_3
        //     88: invokespecial 426	android/webkit/WebViewInputDispatcher:copyDispatchEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     91: astore_3
        //     92: aload_3
        //     93: bipush 253
        //     95: aload_3
        //     96: getfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     99: iand
        //     100: putfield 234	android/webkit/WebViewInputDispatcher$DispatchEvent:mFlags	I
        //     103: goto -38 -> 65
        //     106: astore_2
        //     107: aload_1
        //     108: monitorexit
        //     109: aload_2
        //     110: athrow
        //     111: aload_0
        //     112: invokespecial 428	android/webkit/WebViewInputDispatcher:enqueueWebKitCancelTouchEventIfNeededLocked	()V
        //     115: aload_1
        //     116: monitorexit
        //     117: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	109	106	finally
        //     111	117	106	finally
    }

    private void hideTapCandidateLocked()
    {
        unscheduleHideTapHighlightLocked();
        unscheduleShowTapHighlightLocked();
        this.mUiCallbacks.showTapHighlight(false);
    }

    private boolean isClickCandidateLocked(MotionEvent paramMotionEvent)
    {
        int i = 1;
        if ((paramMotionEvent == null) || (paramMotionEvent.getActionMasked() != i) || (!this.mIsTapCandidate))
            i = 0;
        while (true)
        {
            return i;
            if (paramMotionEvent.getEventTime() - paramMotionEvent.getDownTime() >= LONG_PRESS_TIMEOUT)
                int j = 0;
        }
    }

    private boolean isMoveEventLocked(DispatchEvent paramDispatchEvent)
    {
        if ((paramDispatchEvent.mEvent != null) && (paramDispatchEvent.mEvent.getActionMasked() == 2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private DispatchEvent obtainDispatchEventLocked(MotionEvent paramMotionEvent, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat)
    {
        DispatchEvent localDispatchEvent = obtainUninitializedDispatchEventLocked();
        localDispatchEvent.mEvent = paramMotionEvent;
        localDispatchEvent.mEventType = paramInt1;
        localDispatchEvent.mFlags = paramInt2;
        localDispatchEvent.mTimeoutTime = (200L + SystemClock.uptimeMillis());
        localDispatchEvent.mWebKitXOffset = paramInt3;
        localDispatchEvent.mWebKitYOffset = paramInt4;
        localDispatchEvent.mWebKitScale = paramFloat;
        return localDispatchEvent;
    }

    private DispatchEvent obtainUninitializedDispatchEventLocked()
    {
        DispatchEvent localDispatchEvent = this.mDispatchEventPool;
        if (localDispatchEvent != null)
        {
            this.mDispatchEventPoolSize = (-1 + this.mDispatchEventPoolSize);
            this.mDispatchEventPool = localDispatchEvent.mNext;
            localDispatchEvent.mNext = null;
        }
        while (true)
        {
            return localDispatchEvent;
            localDispatchEvent = new DispatchEvent(null);
        }
    }

    // ERROR //
    private void postClick()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 255	android/webkit/WebViewInputDispatcher:mPostClickScheduled	Z
        //     11: ifne +8 -> 19
        //     14: aload_1
        //     15: monitorexit
        //     16: goto +70 -> 86
        //     19: aload_0
        //     20: iconst_0
        //     21: putfield 255	android/webkit/WebViewInputDispatcher:mPostClickScheduled	Z
        //     24: aload_0
        //     25: getfield 149	android/webkit/WebViewInputDispatcher:mPostTouchStream	Landroid/webkit/WebViewInputDispatcher$TouchStream;
        //     28: invokevirtual 461	android/webkit/WebViewInputDispatcher$TouchStream:getLastEvent	()Landroid/view/MotionEvent;
        //     31: astore_3
        //     32: aload_3
        //     33: ifnull +11 -> 44
        //     36: aload_3
        //     37: invokevirtual 464	android/view/MotionEvent:getAction	()I
        //     40: iconst_1
        //     41: if_icmpeq +13 -> 54
        //     44: aload_1
        //     45: monitorexit
        //     46: goto +40 -> 86
        //     49: astore_2
        //     50: aload_1
        //     51: monitorexit
        //     52: aload_2
        //     53: athrow
        //     54: aload_0
        //     55: invokespecial 467	android/webkit/WebViewInputDispatcher:showTapCandidateLocked	()V
        //     58: aload_0
        //     59: aload_0
        //     60: aload_3
        //     61: invokestatic 367	android/view/MotionEvent:obtainNoHistory	(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
        //     64: iconst_4
        //     65: iconst_0
        //     66: aload_0
        //     67: getfield 369	android/webkit/WebViewInputDispatcher:mPostLastWebKitXOffset	I
        //     70: aload_0
        //     71: getfield 371	android/webkit/WebViewInputDispatcher:mPostLastWebKitYOffset	I
        //     74: aload_0
        //     75: getfield 373	android/webkit/WebViewInputDispatcher:mPostLastWebKitScale	F
        //     78: invokespecial 377	android/webkit/WebViewInputDispatcher:obtainDispatchEventLocked	(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     81: invokespecial 380	android/webkit/WebViewInputDispatcher:enqueueEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     84: aload_1
        //     85: monitorexit
        //     86: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	52	49	finally
        //     54	86	49	finally
    }

    // ERROR //
    private void postLongPress()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 469	android/webkit/WebViewInputDispatcher:mPostLongPressScheduled	Z
        //     11: ifne +8 -> 19
        //     14: aload_1
        //     15: monitorexit
        //     16: goto +119 -> 135
        //     19: aload_0
        //     20: iconst_0
        //     21: putfield 469	android/webkit/WebViewInputDispatcher:mPostLongPressScheduled	Z
        //     24: aload_0
        //     25: getfield 149	android/webkit/WebViewInputDispatcher:mPostTouchStream	Landroid/webkit/WebViewInputDispatcher$TouchStream;
        //     28: invokevirtual 461	android/webkit/WebViewInputDispatcher$TouchStream:getLastEvent	()Landroid/view/MotionEvent;
        //     31: astore_3
        //     32: aload_3
        //     33: ifnonnull +13 -> 46
        //     36: aload_1
        //     37: monitorexit
        //     38: goto +97 -> 135
        //     41: astore_2
        //     42: aload_1
        //     43: monitorexit
        //     44: aload_2
        //     45: athrow
        //     46: aload_3
        //     47: invokevirtual 441	android/view/MotionEvent:getActionMasked	()I
        //     50: tableswitch	default:+42 -> 92, 0:+47->97, 1:+42->92, 2:+47->97, 3:+42->92, 4:+42->92, 5:+47->97, 6:+47->97
        //     93: monitorexit
        //     94: goto +41 -> 135
        //     97: aload_3
        //     98: invokestatic 367	android/view/MotionEvent:obtainNoHistory	(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
        //     101: astore 4
        //     103: aload 4
        //     105: iconst_2
        //     106: invokevirtual 472	android/view/MotionEvent:setAction	(I)V
        //     109: aload_0
        //     110: aload_0
        //     111: aload 4
        //     113: iconst_3
        //     114: iconst_0
        //     115: aload_0
        //     116: getfield 369	android/webkit/WebViewInputDispatcher:mPostLastWebKitXOffset	I
        //     119: aload_0
        //     120: getfield 371	android/webkit/WebViewInputDispatcher:mPostLastWebKitYOffset	I
        //     123: aload_0
        //     124: getfield 373	android/webkit/WebViewInputDispatcher:mPostLastWebKitScale	F
        //     127: invokespecial 377	android/webkit/WebViewInputDispatcher:obtainDispatchEventLocked	(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     130: invokespecial 380	android/webkit/WebViewInputDispatcher:enqueueEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     133: aload_1
        //     134: monitorexit
        //     135: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	44	41	finally
        //     46	135	41	finally
    }

    // ERROR //
    private void postShowTapHighlight(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: iload_1
        //     8: ifeq +40 -> 48
        //     11: aload_0
        //     12: getfield 474	android/webkit/WebViewInputDispatcher:mPostShowTapHighlightScheduled	Z
        //     15: ifne +8 -> 23
        //     18: aload_2
        //     19: monitorexit
        //     20: goto +48 -> 68
        //     23: aload_0
        //     24: iconst_0
        //     25: putfield 474	android/webkit/WebViewInputDispatcher:mPostShowTapHighlightScheduled	Z
        //     28: aload_0
        //     29: getfield 160	android/webkit/WebViewInputDispatcher:mUiCallbacks	Landroid/webkit/WebViewInputDispatcher$UiCallbacks;
        //     32: iload_1
        //     33: invokeinterface 437 2 0
        //     38: aload_2
        //     39: monitorexit
        //     40: goto +28 -> 68
        //     43: astore_3
        //     44: aload_2
        //     45: monitorexit
        //     46: aload_3
        //     47: athrow
        //     48: aload_0
        //     49: getfield 476	android/webkit/WebViewInputDispatcher:mPostHideTapHighlightScheduled	Z
        //     52: ifne +8 -> 60
        //     55: aload_2
        //     56: monitorexit
        //     57: goto +11 -> 68
        //     60: aload_0
        //     61: iconst_0
        //     62: putfield 476	android/webkit/WebViewInputDispatcher:mPostHideTapHighlightScheduled	Z
        //     65: goto -37 -> 28
        //     68: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	46	43	finally
        //     48	65	43	finally
    }

    private void recycleDispatchEventLocked(DispatchEvent paramDispatchEvent)
    {
        if (paramDispatchEvent.mEvent != null)
        {
            paramDispatchEvent.mEvent.recycle();
            paramDispatchEvent.mEvent = null;
        }
        if (this.mDispatchEventPoolSize < 10)
        {
            this.mDispatchEventPoolSize = (1 + this.mDispatchEventPoolSize);
            paramDispatchEvent.mNext = this.mDispatchEventPool;
            this.mDispatchEventPool = paramDispatchEvent;
        }
    }

    private void scheduleClickLocked()
    {
        unscheduleClickLocked();
        this.mPostClickScheduled = true;
        this.mUiHandler.sendEmptyMessageDelayed(4, DOUBLE_TAP_TIMEOUT);
    }

    private void scheduleHideTapHighlightLocked()
    {
        unscheduleHideTapHighlightLocked();
        this.mPostHideTapHighlightScheduled = true;
        this.mUiHandler.sendEmptyMessageDelayed(6, PRESSED_STATE_DURATION);
    }

    private void scheduleLongPressLocked()
    {
        unscheduleLongPressLocked();
        this.mPostLongPressScheduled = true;
        this.mUiHandler.sendEmptyMessageDelayed(3, LONG_PRESS_TIMEOUT);
    }

    private void scheduleShowTapHighlightLocked()
    {
        unscheduleShowTapHighlightLocked();
        this.mPostShowTapHighlightScheduled = true;
        this.mUiHandler.sendEmptyMessageDelayed(5, TAP_TIMEOUT);
    }

    private void scheduleUiDispatchLocked()
    {
        if (!this.mUiDispatchScheduled)
        {
            this.mUiHandler.sendEmptyMessage(1);
            this.mUiDispatchScheduled = true;
        }
    }

    private void scheduleWebKitDispatchLocked()
    {
        if (!this.mWebKitDispatchScheduled)
        {
            this.mWebKitHandler.sendEmptyMessage(1);
            this.mWebKitDispatchScheduled = true;
        }
    }

    private boolean shouldSkipWebKit(DispatchEvent paramDispatchEvent)
    {
        boolean bool = false;
        switch (paramDispatchEvent.mEventType)
        {
        case 3:
        case 5:
        default:
            bool = true;
        case 1:
        case 2:
        case 4:
        case 6:
        case 0:
        }
        while (true)
        {
            return bool;
            if ((this.mIsTapCandidate) && (paramDispatchEvent.mEvent != null) && (paramDispatchEvent.mEvent.getActionMasked() == 2))
                bool = true;
            else if ((!this.mPostSendTouchEventsToWebKit) || (this.mPostDoNotSendTouchEventsToWebKitUntilNextGesture))
                bool = true;
        }
    }

    private void showTapCandidateLocked()
    {
        unscheduleHideTapHighlightLocked();
        unscheduleShowTapHighlightLocked();
        this.mUiCallbacks.showTapHighlight(true);
    }

    private void skipWebKitEventLocked(DispatchEvent paramDispatchEvent)
    {
        paramDispatchEvent.mNext = null;
        if ((0x1 & paramDispatchEvent.mFlags) != 0)
            recycleDispatchEventLocked(paramDispatchEvent);
        while (true)
        {
            return;
            paramDispatchEvent.mFlags = (0x4 | paramDispatchEvent.mFlags);
            enqueueUiEventUnbatchedLocked(paramDispatchEvent);
        }
    }

    private void unscheduleClickLocked()
    {
        if (this.mPostClickScheduled)
        {
            this.mPostClickScheduled = false;
            this.mUiHandler.removeMessages(4);
        }
    }

    private void unscheduleHideTapHighlightLocked()
    {
        if (this.mPostHideTapHighlightScheduled)
        {
            this.mPostHideTapHighlightScheduled = false;
            this.mUiHandler.removeMessages(6);
        }
    }

    private void unscheduleLongPressLocked()
    {
        if (this.mPostLongPressScheduled)
        {
            this.mPostLongPressScheduled = false;
            this.mUiHandler.removeMessages(3);
        }
    }

    private void unscheduleShowTapHighlightLocked()
    {
        if (this.mPostShowTapHighlightScheduled)
        {
            this.mPostShowTapHighlightScheduled = false;
            this.mUiHandler.removeMessages(5);
        }
    }

    private void updateStateTrackersLocked(DispatchEvent paramDispatchEvent, MotionEvent paramMotionEvent)
    {
        this.mPostLastWebKitXOffset = paramDispatchEvent.mWebKitXOffset;
        this.mPostLastWebKitYOffset = paramDispatchEvent.mWebKitYOffset;
        this.mPostLastWebKitScale = paramDispatchEvent.mWebKitScale;
        int i;
        if (paramMotionEvent != null)
        {
            i = paramMotionEvent.getAction();
            if (paramDispatchEvent.mEventType == 0)
                break label46;
        }
        while (true)
        {
            return;
            i = 3;
            break;
            label46: if ((i == 3) || (paramMotionEvent.getPointerCount() > 1))
            {
                unscheduleLongPressLocked();
                unscheduleClickLocked();
                hideTapCandidateLocked();
                this.mIsDoubleTapCandidate = false;
                this.mIsTapCandidate = false;
                hideTapCandidateLocked();
            }
            else if (i == 0)
            {
                checkForDoubleTapOnDownLocked(paramMotionEvent);
                scheduleLongPressLocked();
                this.mIsTapCandidate = true;
                this.mInitialDownX = paramMotionEvent.getX();
                this.mInitialDownY = paramMotionEvent.getY();
                enqueueHitTestLocked(paramMotionEvent);
                if (this.mIsDoubleTapCandidate)
                    hideTapCandidateLocked();
                else
                    scheduleShowTapHighlightLocked();
            }
            else if (i == 1)
            {
                unscheduleLongPressLocked();
                if (isClickCandidateLocked(paramMotionEvent))
                {
                    if (this.mIsDoubleTapCandidate)
                    {
                        hideTapCandidateLocked();
                        enqueueDoubleTapLocked(paramMotionEvent);
                    }
                    else
                    {
                        scheduleClickLocked();
                    }
                }
                else
                    hideTapCandidateLocked();
            }
            else if (i == 2)
            {
                checkForSlopLocked(paramMotionEvent);
            }
        }
    }

    private void updateWebKitTimeoutLocked()
    {
        DispatchEvent localDispatchEvent = this.mWebKitDispatchEventQueue.mHead;
        if ((localDispatchEvent != null) && (this.mWebKitTimeoutScheduled) && (this.mWebKitTimeoutTime == localDispatchEvent.mTimeoutTime));
        while (true)
        {
            return;
            if (this.mWebKitTimeoutScheduled)
            {
                this.mUiHandler.removeMessages(2);
                this.mWebKitTimeoutScheduled = false;
            }
            if (localDispatchEvent != null)
            {
                this.mUiHandler.sendEmptyMessageAtTime(2, localDispatchEvent.mTimeoutTime);
                this.mWebKitTimeoutScheduled = true;
                this.mWebKitTimeoutTime = localDispatchEvent.mTimeoutTime;
            }
        }
    }

    public void dispatchUiEvents()
    {
        dispatchUiEvents(false);
    }

    public void dispatchWebKitEvents()
    {
        dispatchWebKitEvents(false);
    }

    // ERROR //
    public boolean postPointerEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2, float paramFloat)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 5
        //     3: aload_1
        //     4: ifnonnull +14 -> 18
        //     7: new 520	java/lang/IllegalArgumentException
        //     10: dup
        //     11: ldc_w 522
        //     14: invokespecial 525	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     17: athrow
        //     18: aload_1
        //     19: invokevirtual 441	android/view/MotionEvent:getActionMasked	()I
        //     22: istore 6
        //     24: iload 6
        //     26: tableswitch	default:+58 -> 84, 0:+61->87, 1:+61->87, 2:+61->87, 3:+61->87, 4:+58->84, 5:+61->87, 6:+61->87, 7:+128->154, 8:+122->148, 9:+128->154, 10:+128->154
        //     85: iconst_2
        //     86: ireturn
        //     87: iconst_0
        //     88: istore 7
        //     90: aload_0
        //     91: getfield 144	android/webkit/WebViewInputDispatcher:mLock	Ljava/lang/Object;
        //     94: astore 8
        //     96: aload 8
        //     98: monitorenter
        //     99: aload_1
        //     100: astore 9
        //     102: iload 7
        //     104: ifne +87 -> 191
        //     107: aload_0
        //     108: getfield 149	android/webkit/WebViewInputDispatcher:mPostTouchStream	Landroid/webkit/WebViewInputDispatcher$TouchStream;
        //     111: aload_1
        //     112: invokevirtual 319	android/webkit/WebViewInputDispatcher$TouchStream:update	(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
        //     115: astore 9
        //     117: aload 9
        //     119: ifnonnull +41 -> 160
        //     122: aload_0
        //     123: invokespecial 275	android/webkit/WebViewInputDispatcher:unscheduleLongPressLocked	()V
        //     126: aload_0
        //     127: invokespecial 269	android/webkit/WebViewInputDispatcher:unscheduleClickLocked	()V
        //     130: aload_0
        //     131: invokespecial 278	android/webkit/WebViewInputDispatcher:hideTapCandidateLocked	()V
        //     134: aload 8
        //     136: monitorexit
        //     137: goto -53 -> 84
        //     140: astore 11
        //     142: aload 8
        //     144: monitorexit
        //     145: aload 11
        //     147: athrow
        //     148: iconst_2
        //     149: istore 7
        //     151: goto -61 -> 90
        //     154: iconst_1
        //     155: istore 7
        //     157: goto -67 -> 90
        //     160: iload 6
        //     162: ifne +29 -> 191
        //     165: aload_0
        //     166: getfield 490	android/webkit/WebViewInputDispatcher:mPostSendTouchEventsToWebKit	Z
        //     169: ifeq +22 -> 191
        //     172: aload_0
        //     173: getfield 160	android/webkit/WebViewInputDispatcher:mUiCallbacks	Landroid/webkit/WebViewInputDispatcher$UiCallbacks;
        //     176: aload 9
        //     178: invokeinterface 528 2 0
        //     183: ifeq +57 -> 240
        //     186: aload_0
        //     187: iconst_1
        //     188: putfield 416	android/webkit/WebViewInputDispatcher:mPostDoNotSendTouchEventsToWebKitUntilNextGesture	Z
        //     191: aload 9
        //     193: aload_1
        //     194: if_acmpne +9 -> 203
        //     197: aload_1
        //     198: invokevirtual 288	android/view/MotionEvent:copy	()Landroid/view/MotionEvent;
        //     201: astore 9
        //     203: aload_0
        //     204: aload 9
        //     206: iload 7
        //     208: iconst_0
        //     209: iload_2
        //     210: iload_3
        //     211: fload 4
        //     213: invokespecial 377	android/webkit/WebViewInputDispatcher:obtainDispatchEventLocked	(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
        //     216: astore 10
        //     218: aload_0
        //     219: aload 10
        //     221: aload_1
        //     222: invokespecial 530	android/webkit/WebViewInputDispatcher:updateStateTrackersLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;Landroid/view/MotionEvent;)V
        //     225: aload_0
        //     226: aload 10
        //     228: invokespecial 380	android/webkit/WebViewInputDispatcher:enqueueEventLocked	(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
        //     231: aload 8
        //     233: monitorexit
        //     234: iconst_1
        //     235: istore 5
        //     237: goto -153 -> 84
        //     240: aload_0
        //     241: getfield 416	android/webkit/WebViewInputDispatcher:mPostDoNotSendTouchEventsToWebKitUntilNextGesture	Z
        //     244: ifeq -53 -> 191
        //     247: aload_0
        //     248: iconst_0
        //     249: putfield 416	android/webkit/WebViewInputDispatcher:mPostDoNotSendTouchEventsToWebKitUntilNextGesture	Z
        //     252: goto -61 -> 191
        //
        // Exception table:
        //     from	to	target	type
        //     107	145	140	finally
        //     165	252	140	finally
    }

    public void setWebKitWantsTouchEvents(boolean paramBoolean)
    {
        synchronized (this.mLock)
        {
            if (this.mPostSendTouchEventsToWebKit != paramBoolean)
            {
                if (!paramBoolean)
                    enqueueWebKitCancelTouchEventIfNeededLocked();
                this.mPostSendTouchEventsToWebKit = paramBoolean;
            }
            return;
        }
    }

    public void skipWebkitForRemainingTouchStream()
    {
        handleWebKitTimeout();
    }

    private static final class TouchStream
    {
        private MotionEvent mLastEvent;

        private void updateLastEvent(MotionEvent paramMotionEvent)
        {
            if (this.mLastEvent != null)
                this.mLastEvent.recycle();
            if (paramMotionEvent != null);
            for (MotionEvent localMotionEvent = MotionEvent.obtainNoHistory(paramMotionEvent); ; localMotionEvent = null)
            {
                this.mLastEvent = localMotionEvent;
                return;
            }
        }

        public MotionEvent getLastEvent()
        {
            return this.mLastEvent;
        }

        public boolean isCancelNeeded()
        {
            int i = 1;
            if ((this.mLastEvent != null) && (this.mLastEvent.getAction() != i));
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        public MotionEvent update(MotionEvent paramMotionEvent)
        {
            MotionEvent localMotionEvent = null;
            if (paramMotionEvent == null)
            {
                if (isCancelNeeded())
                {
                    paramMotionEvent = this.mLastEvent;
                    if (paramMotionEvent != null)
                    {
                        paramMotionEvent.setAction(3);
                        this.mLastEvent = null;
                    }
                }
                localMotionEvent = paramMotionEvent;
            }
            while (true)
            {
                return localMotionEvent;
                switch (paramMotionEvent.getActionMasked())
                {
                case 4:
                default:
                    break;
                case 0:
                    updateLastEvent(paramMotionEvent);
                    localMotionEvent = paramMotionEvent;
                    break;
                case 1:
                case 2:
                case 5:
                case 6:
                    if ((this.mLastEvent != null) && (this.mLastEvent.getAction() != 1))
                    {
                        updateLastEvent(paramMotionEvent);
                        localMotionEvent = paramMotionEvent;
                    }
                    break;
                case 3:
                    if (this.mLastEvent != null)
                    {
                        updateLastEvent(null);
                        localMotionEvent = paramMotionEvent;
                    }
                    break;
                }
            }
        }
    }

    private static final class DispatchEventQueue
    {
        public WebViewInputDispatcher.DispatchEvent mHead;
        public WebViewInputDispatcher.DispatchEvent mTail;

        public WebViewInputDispatcher.DispatchEvent dequeue()
        {
            WebViewInputDispatcher.DispatchEvent localDispatchEvent1 = this.mHead;
            WebViewInputDispatcher.DispatchEvent localDispatchEvent2;
            if (localDispatchEvent1 != null)
            {
                localDispatchEvent2 = localDispatchEvent1.mNext;
                if (localDispatchEvent2 != null)
                    break label30;
                this.mHead = null;
                this.mTail = null;
            }
            while (true)
            {
                return localDispatchEvent1;
                label30: this.mHead = localDispatchEvent2;
                localDispatchEvent1.mNext = null;
            }
        }

        public WebViewInputDispatcher.DispatchEvent dequeueList()
        {
            WebViewInputDispatcher.DispatchEvent localDispatchEvent = this.mHead;
            if (localDispatchEvent != null)
            {
                this.mHead = null;
                this.mTail = null;
            }
            return localDispatchEvent;
        }

        public void enqueue(WebViewInputDispatcher.DispatchEvent paramDispatchEvent)
        {
            if (this.mHead == null)
                this.mHead = paramDispatchEvent;
            for (this.mTail = paramDispatchEvent; ; this.mTail = paramDispatchEvent)
            {
                return;
                this.mTail.mNext = paramDispatchEvent;
            }
        }

        public boolean isEmpty()
        {
            if (this.mHead != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private static final class DispatchEvent
    {
        public MotionEvent mEvent;
        public int mEventType;
        public int mFlags;
        public DispatchEvent mNext;
        public long mTimeoutTime;
        public float mWebKitScale;
        public int mWebKitXOffset;
        public int mWebKitYOffset;
    }

    private final class WebKitHandler extends Handler
    {
        public static final int MSG_DISPATCH_WEBKIT_EVENTS = 1;

        public WebKitHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                throw new IllegalStateException("Unknown message type: " + paramMessage.what);
            case 1:
            }
            WebViewInputDispatcher.this.dispatchWebKitEvents(true);
        }
    }

    private final class UiHandler extends Handler
    {
        public static final int MSG_CLICK = 4;
        public static final int MSG_DISPATCH_UI_EVENTS = 1;
        public static final int MSG_HIDE_TAP_HIGHLIGHT = 6;
        public static final int MSG_LONG_PRESS = 3;
        public static final int MSG_SHOW_TAP_HIGHLIGHT = 5;
        public static final int MSG_WEBKIT_TIMEOUT = 2;

        public UiHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                throw new IllegalStateException("Unknown message type: " + paramMessage.what);
            case 1:
                WebViewInputDispatcher.this.dispatchUiEvents(true);
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            }
            while (true)
            {
                return;
                WebViewInputDispatcher.this.handleWebKitTimeout();
                continue;
                WebViewInputDispatcher.this.postLongPress();
                continue;
                WebViewInputDispatcher.this.postClick();
                continue;
                WebViewInputDispatcher.this.postShowTapHighlight(true);
                continue;
                WebViewInputDispatcher.this.postShowTapHighlight(false);
            }
        }
    }

    public static abstract interface WebKitCallbacks
    {
        public abstract boolean dispatchWebKitEvent(WebViewInputDispatcher paramWebViewInputDispatcher, MotionEvent paramMotionEvent, int paramInt1, int paramInt2);

        public abstract Looper getWebKitLooper();
    }

    public static abstract interface UiCallbacks
    {
        public abstract void clearPreviousHitTest();

        public abstract void dispatchUiEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2);

        public abstract Context getContext();

        public abstract Looper getUiLooper();

        public abstract boolean shouldInterceptTouchEvent(MotionEvent paramMotionEvent);

        public abstract void showTapHighlight(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewInputDispatcher
 * JD-Core Version:        0.6.2
 */