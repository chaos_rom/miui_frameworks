package android.webkit;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.EdgeEffect;
import android.widget.OverScroller;

public class OverScrollGlow
{
    private EdgeEffect mEdgeGlowBottom;
    private EdgeEffect mEdgeGlowLeft;
    private EdgeEffect mEdgeGlowRight;
    private EdgeEffect mEdgeGlowTop;
    private WebViewClassic mHostView;
    private int mOverScrollDeltaX;
    private int mOverScrollDeltaY;

    public OverScrollGlow(WebViewClassic paramWebViewClassic)
    {
        this.mHostView = paramWebViewClassic;
        Context localContext = paramWebViewClassic.getContext();
        this.mEdgeGlowTop = new EdgeEffect(localContext);
        this.mEdgeGlowBottom = new EdgeEffect(localContext);
        this.mEdgeGlowLeft = new EdgeEffect(localContext);
        this.mEdgeGlowRight = new EdgeEffect(localContext);
    }

    public void absorbGlow(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        if ((paramInt6 > 0) || (this.mHostView.getWebView().getOverScrollMode() == 0))
        {
            if ((paramInt2 >= 0) || (paramInt4 < 0))
                break label111;
            this.mEdgeGlowTop.onAbsorb((int)this.mHostView.mScroller.getCurrVelocity());
            if (!this.mEdgeGlowBottom.isFinished())
                this.mEdgeGlowBottom.onRelease();
        }
        label162: label210: 
        while (true)
        {
            if (paramInt5 > 0)
            {
                if ((paramInt1 >= 0) || (paramInt3 < 0))
                    break label162;
                this.mEdgeGlowLeft.onAbsorb((int)this.mHostView.mScroller.getCurrVelocity());
                if (!this.mEdgeGlowRight.isFinished())
                    this.mEdgeGlowRight.onRelease();
            }
            while (true)
            {
                return;
                label111: if ((paramInt2 <= paramInt6) || (paramInt4 > paramInt6))
                    break label210;
                this.mEdgeGlowBottom.onAbsorb((int)this.mHostView.mScroller.getCurrVelocity());
                if (this.mEdgeGlowTop.isFinished())
                    break;
                this.mEdgeGlowTop.onRelease();
                break;
                if ((paramInt1 > paramInt5) && (paramInt3 <= paramInt5))
                {
                    this.mEdgeGlowRight.onAbsorb((int)this.mHostView.mScroller.getCurrVelocity());
                    if (!this.mEdgeGlowLeft.isFinished())
                        this.mEdgeGlowLeft.onRelease();
                }
            }
        }
    }

    public boolean drawEdgeGlows(Canvas paramCanvas)
    {
        int i = this.mHostView.getScrollX();
        int j = this.mHostView.getScrollY();
        int k = this.mHostView.getWidth();
        int m = this.mHostView.getHeight();
        boolean bool = false;
        if (!this.mEdgeGlowTop.isFinished())
        {
            int i3 = paramCanvas.save();
            paramCanvas.translate(i, this.mHostView.getVisibleTitleHeight() + Math.min(0, j));
            this.mEdgeGlowTop.setSize(k, m);
            bool = false | this.mEdgeGlowTop.draw(paramCanvas);
            paramCanvas.restoreToCount(i3);
        }
        if (!this.mEdgeGlowBottom.isFinished())
        {
            int i2 = paramCanvas.save();
            paramCanvas.translate(i + -k, m + Math.max(this.mHostView.computeMaxScrollY(), j));
            paramCanvas.rotate(180.0F, k, 0.0F);
            this.mEdgeGlowBottom.setSize(k, m);
            bool |= this.mEdgeGlowBottom.draw(paramCanvas);
            paramCanvas.restoreToCount(i2);
        }
        if (!this.mEdgeGlowLeft.isFinished())
        {
            int i1 = paramCanvas.save();
            paramCanvas.rotate(270.0F);
            paramCanvas.translate(-m - j, Math.min(0, i));
            this.mEdgeGlowLeft.setSize(m, k);
            bool |= this.mEdgeGlowLeft.draw(paramCanvas);
            paramCanvas.restoreToCount(i1);
        }
        if (!this.mEdgeGlowRight.isFinished())
        {
            int n = paramCanvas.save();
            paramCanvas.rotate(90.0F);
            paramCanvas.translate(j, -(k + Math.max(this.mHostView.computeMaxScrollX(), i)));
            this.mEdgeGlowRight.setSize(m, k);
            bool |= this.mEdgeGlowRight.draw(paramCanvas);
            paramCanvas.restoreToCount(n);
        }
        return bool;
    }

    public boolean isAnimating()
    {
        if ((!this.mEdgeGlowTop.isFinished()) || (!this.mEdgeGlowBottom.isFinished()) || (!this.mEdgeGlowLeft.isFinished()) || (!this.mEdgeGlowRight.isFinished()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void pullGlow(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        int j;
        int i;
        if ((paramInt3 == this.mHostView.getScrollX()) && (paramInt4 == this.mHostView.getScrollY()))
        {
            if (paramInt5 > 0)
            {
                j = paramInt3 + this.mOverScrollDeltaX;
                if (j >= 0)
                    break label160;
                this.mEdgeGlowLeft.onPull(this.mOverScrollDeltaX / this.mHostView.getWidth());
                if (!this.mEdgeGlowRight.isFinished())
                    this.mEdgeGlowRight.onRelease();
                this.mOverScrollDeltaX = 0;
            }
            if ((paramInt6 > 0) || (this.mHostView.getWebView().getOverScrollMode() == 0))
            {
                i = paramInt4 + this.mOverScrollDeltaY;
                if (i >= 0)
                    break label208;
                this.mEdgeGlowTop.onPull(this.mOverScrollDeltaY / this.mHostView.getHeight());
                if (!this.mEdgeGlowBottom.isFinished())
                    this.mEdgeGlowBottom.onRelease();
            }
        }
        while (true)
        {
            this.mOverScrollDeltaY = 0;
            return;
            label160: if (j <= paramInt5)
                break;
            this.mEdgeGlowRight.onPull(this.mOverScrollDeltaX / this.mHostView.getWidth());
            if (this.mEdgeGlowLeft.isFinished())
                break;
            this.mEdgeGlowLeft.onRelease();
            break;
            label208: if (i > paramInt6)
            {
                this.mEdgeGlowBottom.onPull(this.mOverScrollDeltaY / this.mHostView.getHeight());
                if (!this.mEdgeGlowTop.isFinished())
                    this.mEdgeGlowTop.onRelease();
            }
        }
    }

    public void releaseAll()
    {
        this.mEdgeGlowTop.onRelease();
        this.mEdgeGlowBottom.onRelease();
        this.mEdgeGlowLeft.onRelease();
        this.mEdgeGlowRight.onRelease();
    }

    public void setOverScrollDeltas(int paramInt1, int paramInt2)
    {
        this.mOverScrollDeltaX = paramInt1;
        this.mOverScrollDeltaY = paramInt2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.OverScrollGlow
 * JD-Core Version:        0.6.2
 */