package android.webkit;

public class JsResult
{
    private final ResultReceiver mReceiver;
    private boolean mResult;

    public JsResult(ResultReceiver paramResultReceiver)
    {
        this.mReceiver = paramResultReceiver;
    }

    private final void wakeUp()
    {
        this.mReceiver.onJsResultComplete(this);
    }

    public final void cancel()
    {
        this.mResult = false;
        wakeUp();
    }

    public final void confirm()
    {
        this.mResult = true;
        wakeUp();
    }

    public final boolean getResult()
    {
        return this.mResult;
    }

    public static abstract interface ResultReceiver
    {
        public abstract void onJsResultComplete(JsResult paramJsResult);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.JsResult
 * JD-Core Version:        0.6.2
 */