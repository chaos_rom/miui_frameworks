package android.webkit;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

final class GeolocationService
    implements LocationListener
{
    private static final String TAG = "geolocationService";
    private boolean mIsGpsEnabled;
    private boolean mIsGpsProviderAvailable;
    private boolean mIsNetworkProviderAvailable;
    private boolean mIsRunning;
    private LocationManager mLocationManager;
    private long mNativeObject;

    public GeolocationService(Context paramContext, long paramLong)
    {
        this.mNativeObject = paramLong;
        this.mLocationManager = ((LocationManager)paramContext.getSystemService("location"));
        if (this.mLocationManager == null)
            Log.e("geolocationService", "Could not get location manager.");
    }

    private void maybeReportError(String paramString)
    {
        if ((this.mIsRunning) && (!this.mIsNetworkProviderAvailable) && (!this.mIsGpsProviderAvailable))
            nativeNewErrorAvailable(this.mNativeObject, paramString);
    }

    private static native void nativeNewErrorAvailable(long paramLong, String paramString);

    private static native void nativeNewLocationAvailable(long paramLong, Location paramLocation);

    // ERROR //
    private void registerForLocationUpdates()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 38	android/webkit/GeolocationService:mLocationManager	Landroid/location/LocationManager;
        //     4: ldc 67
        //     6: lconst_0
        //     7: fconst_0
        //     8: aload_0
        //     9: invokevirtual 71	android/location/LocationManager:requestLocationUpdates	(Ljava/lang/String;JFLandroid/location/LocationListener;)V
        //     12: aload_0
        //     13: iconst_1
        //     14: putfield 52	android/webkit/GeolocationService:mIsNetworkProviderAvailable	Z
        //     17: aload_0
        //     18: getfield 73	android/webkit/GeolocationService:mIsGpsEnabled	Z
        //     21: istore 4
        //     23: iload 4
        //     25: ifeq +20 -> 45
        //     28: aload_0
        //     29: getfield 38	android/webkit/GeolocationService:mLocationManager	Landroid/location/LocationManager;
        //     32: ldc 75
        //     34: lconst_0
        //     35: fconst_0
        //     36: aload_0
        //     37: invokevirtual 71	android/location/LocationManager:requestLocationUpdates	(Ljava/lang/String;JFLandroid/location/LocationListener;)V
        //     40: aload_0
        //     41: iconst_1
        //     42: putfield 54	android/webkit/GeolocationService:mIsGpsProviderAvailable	Z
        //     45: return
        //     46: astore_2
        //     47: ldc 10
        //     49: ldc 77
        //     51: invokestatic 46	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     54: pop
        //     55: goto -10 -> 45
        //     58: astore 5
        //     60: goto -15 -> 45
        //     63: astore_1
        //     64: goto -47 -> 17
        //
        // Exception table:
        //     from	to	target	type
        //     0	17	46	java/lang/SecurityException
        //     17	23	46	java/lang/SecurityException
        //     28	45	46	java/lang/SecurityException
        //     28	45	58	java/lang/IllegalArgumentException
        //     0	17	63	java/lang/IllegalArgumentException
    }

    private void unregisterFromLocationUpdates()
    {
        this.mLocationManager.removeUpdates(this);
        this.mIsNetworkProviderAvailable = false;
        this.mIsGpsProviderAvailable = false;
    }

    public void onLocationChanged(Location paramLocation)
    {
        if (this.mIsRunning)
            nativeNewLocationAvailable(this.mNativeObject, paramLocation);
    }

    public void onProviderDisabled(String paramString)
    {
        if ("network".equals(paramString))
            this.mIsNetworkProviderAvailable = false;
        while (true)
        {
            maybeReportError("The last location provider was disabled");
            return;
            if ("gps".equals(paramString))
                this.mIsGpsProviderAvailable = false;
        }
    }

    public void onProviderEnabled(String paramString)
    {
        if ("network".equals(paramString))
            this.mIsNetworkProviderAvailable = true;
        while (true)
        {
            return;
            if ("gps".equals(paramString))
                this.mIsGpsProviderAvailable = true;
        }
    }

    public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
    {
        boolean bool;
        if (paramInt == 2)
        {
            bool = true;
            if (!"network".equals(paramString))
                break label36;
            this.mIsNetworkProviderAvailable = bool;
        }
        while (true)
        {
            maybeReportError("The last location provider is no longer available");
            return;
            bool = false;
            break;
            label36: if ("gps".equals(paramString))
                this.mIsGpsProviderAvailable = bool;
        }
    }

    public void setEnableGps(boolean paramBoolean)
    {
        if (this.mIsGpsEnabled != paramBoolean)
        {
            this.mIsGpsEnabled = paramBoolean;
            if (this.mIsRunning)
            {
                unregisterFromLocationUpdates();
                registerForLocationUpdates();
                maybeReportError("The last location provider is no longer available");
            }
        }
    }

    public boolean start()
    {
        boolean bool = true;
        registerForLocationUpdates();
        this.mIsRunning = bool;
        if ((this.mIsNetworkProviderAvailable) || (this.mIsGpsProviderAvailable));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public void stop()
    {
        unregisterFromLocationUpdates();
        this.mIsRunning = false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.GeolocationService
 * JD-Core Version:        0.6.2
 */