package android.webkit;

import android.content.res.Resources;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsoluteLayout.LayoutParams;
import java.util.ArrayList;
import java.util.Iterator;

class ViewManager
{
    private static final int MAX_SURFACE_DIMENSION = 2048;
    private final int MAX_SURFACE_AREA;
    private final ArrayList<ChildView> mChildren = new ArrayList();
    private boolean mHidden;
    private boolean mReadyToDraw;
    private final WebViewClassic mWebView;
    private boolean mZoomInProgress = false;

    ViewManager(WebViewClassic paramWebViewClassic)
    {
        this.mWebView = paramWebViewClassic;
        DisplayMetrics localDisplayMetrics = paramWebViewClassic.getWebView().getResources().getDisplayMetrics();
        this.MAX_SURFACE_AREA = ((int)(2.75D * (localDisplayMetrics.widthPixels * localDisplayMetrics.heightPixels)));
    }

    private void requestLayout(ChildView paramChildView)
    {
        int i = this.mWebView.contentToViewDimension(paramChildView.width);
        int j = this.mWebView.contentToViewDimension(paramChildView.height);
        int k = this.mWebView.contentToViewX(paramChildView.x);
        int m = this.mWebView.contentToViewY(paramChildView.y);
        ViewGroup.LayoutParams localLayoutParams = paramChildView.mView.getLayoutParams();
        AbsoluteLayout.LayoutParams localLayoutParams1;
        final SurfaceView localSurfaceView;
        if ((localLayoutParams instanceof AbsoluteLayout.LayoutParams))
        {
            localLayoutParams1 = (AbsoluteLayout.LayoutParams)localLayoutParams;
            localLayoutParams1.width = i;
            localLayoutParams1.height = j;
            localLayoutParams1.x = k;
            localLayoutParams1.y = m;
            paramChildView.mView.setLayoutParams(localLayoutParams1);
            if ((paramChildView.mView instanceof SurfaceView))
            {
                localSurfaceView = (SurfaceView)paramChildView.mView;
                if ((!localSurfaceView.isFixedSize()) || (!this.mZoomInProgress))
                    break label162;
            }
        }
        while (true)
        {
            return;
            localLayoutParams1 = new AbsoluteLayout.LayoutParams(i, j, k, m);
            break;
            label162: int n = i;
            int i1 = j;
            label215: float f;
            if ((n > 2048) || (i1 > 2048))
            {
                if (paramChildView.width > paramChildView.height)
                {
                    n = 2048;
                    i1 = 2048 * paramChildView.height / paramChildView.width;
                }
            }
            else if (n * i1 > this.MAX_SURFACE_AREA)
            {
                f = this.MAX_SURFACE_AREA;
                if (paramChildView.width <= paramChildView.height)
                    break label332;
                n = (int)Math.sqrt(f * paramChildView.width / paramChildView.height);
                i1 = n * paramChildView.height / paramChildView.width;
            }
            while (true)
            {
                if ((n == i) && (i1 == j))
                    break label370;
                localSurfaceView.getHolder().setFixedSize(n, i1);
                break;
                i1 = 2048;
                n = 2048 * paramChildView.width / paramChildView.height;
                break label215;
                label332: i1 = (int)Math.sqrt(f * paramChildView.height / paramChildView.width);
                n = i1 * paramChildView.width / paramChildView.height;
            }
            label370: if ((!localSurfaceView.isFixedSize()) && (this.mZoomInProgress))
                localSurfaceView.getHolder().setFixedSize(localSurfaceView.getWidth(), localSurfaceView.getHeight());
            else if ((localSurfaceView.isFixedSize()) && (!this.mZoomInProgress))
                if (localSurfaceView.getVisibility() == 0)
                {
                    localSurfaceView.setVisibility(4);
                    localSurfaceView.getHolder().setSizeFromLayout();
                    this.mWebView.mPrivateHandler.post(new Runnable()
                    {
                        public void run()
                        {
                            localSurfaceView.setVisibility(0);
                        }
                    });
                }
                else
                {
                    localSurfaceView.getHolder().setSizeFromLayout();
                }
        }
    }

    ChildView createView()
    {
        return new ChildView();
    }

    void endZoom()
    {
        this.mZoomInProgress = false;
        Iterator localIterator = this.mChildren.iterator();
        while (localIterator.hasNext())
            requestLayout((ChildView)localIterator.next());
    }

    void hideAll()
    {
        if (this.mHidden);
        while (true)
        {
            return;
            Iterator localIterator = this.mChildren.iterator();
            while (localIterator.hasNext())
                ((ChildView)localIterator.next()).mView.setVisibility(8);
            this.mHidden = true;
        }
    }

    ChildView hitTest(int paramInt1, int paramInt2)
    {
        ChildView localChildView;
        if (this.mHidden)
            localChildView = null;
        while (true)
        {
            return localChildView;
            Iterator localIterator = this.mChildren.iterator();
            while (true)
                if (localIterator.hasNext())
                {
                    localChildView = (ChildView)localIterator.next();
                    if ((localChildView.mView.getVisibility() == 0) && (paramInt1 >= localChildView.x) && (paramInt1 < localChildView.x + localChildView.width) && (paramInt2 >= localChildView.y) && (paramInt2 < localChildView.y + localChildView.height))
                        break;
                }
            localChildView = null;
        }
    }

    void postReadyToDrawAll()
    {
        this.mWebView.mPrivateHandler.post(new Runnable()
        {
            public void run()
            {
                ViewManager.access$402(ViewManager.this, true);
                Iterator localIterator = ViewManager.this.mChildren.iterator();
                while (localIterator.hasNext())
                    ((ViewManager.ChildView)localIterator.next()).mView.setVisibility(0);
            }
        });
    }

    void postResetStateAll()
    {
        this.mWebView.mPrivateHandler.post(new Runnable()
        {
            public void run()
            {
                ViewManager.access$402(ViewManager.this, false);
            }
        });
    }

    void scaleAll()
    {
        Iterator localIterator = this.mChildren.iterator();
        while (localIterator.hasNext())
            requestLayout((ChildView)localIterator.next());
    }

    void showAll()
    {
        if (!this.mHidden);
        while (true)
        {
            return;
            Iterator localIterator = this.mChildren.iterator();
            while (localIterator.hasNext())
                ((ChildView)localIterator.next()).mView.setVisibility(0);
            this.mHidden = false;
        }
    }

    void startZoom()
    {
        this.mZoomInProgress = true;
        Iterator localIterator = this.mChildren.iterator();
        while (localIterator.hasNext())
            requestLayout((ChildView)localIterator.next());
    }

    class ChildView
    {
        int height;
        View mView;
        int width;
        int x;
        int y;

        ChildView()
        {
        }

        private void attachViewOnUIThread()
        {
            ViewManager.this.mWebView.getWebView().addView(this.mView);
            ViewManager.this.mChildren.add(this);
            if (!ViewManager.this.mReadyToDraw)
                this.mView.setVisibility(8);
        }

        private void removeViewOnUIThread()
        {
            ViewManager.this.mWebView.getWebView().removeView(this.mView);
            ViewManager.this.mChildren.remove(this);
        }

        void attachView(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            if (this.mView == null);
            while (true)
            {
                return;
                setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
                ViewManager.this.mWebView.mPrivateHandler.post(new Runnable()
                {
                    public void run()
                    {
                        ViewManager.this.requestLayout(ViewManager.ChildView.this);
                        if (ViewManager.ChildView.this.mView.getParent() == null)
                            ViewManager.ChildView.this.attachViewOnUIThread();
                    }
                });
            }
        }

        void removeView()
        {
            if (this.mView == null);
            while (true)
            {
                return;
                ViewManager.this.mWebView.mPrivateHandler.post(new Runnable()
                {
                    public void run()
                    {
                        ViewManager.ChildView.this.removeViewOnUIThread();
                    }
                });
            }
        }

        void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.x = paramInt1;
            this.y = paramInt2;
            this.width = paramInt3;
            this.height = paramInt4;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ViewManager
 * JD-Core Version:        0.6.2
 */