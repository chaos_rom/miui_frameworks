package android.webkit;

import android.content.Context;
import android.security.Credentials;
import android.util.Log;
import com.android.org.bouncycastle.asn1.DERObject;
import com.android.org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import com.android.org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import com.android.org.bouncycastle.jce.netscape.NetscapeCertRequest;
import com.android.org.bouncycastle.util.encoders.Base64;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.HashMap;

final class CertTool
{
    private static final String LOGTAG = "CertTool";
    private static final AlgorithmIdentifier MD5_WITH_RSA = new AlgorithmIdentifier(PKCSObjectIdentifiers.md5WithRSAEncryption);
    private static HashMap<String, String> sCertificateTypeMap = new HashMap();

    static
    {
        sCertificateTypeMap.put("application/x-x509-ca-cert", "CERT");
        sCertificateTypeMap.put("application/x-x509-user-cert", "CERT");
        sCertificateTypeMap.put("application/x-pkcs12", "PKCS12");
    }

    static void addCertificate(Context paramContext, String paramString, byte[] paramArrayOfByte)
    {
        Credentials.getInstance().install(paramContext, paramString, paramArrayOfByte);
    }

    static String getCertType(String paramString)
    {
        return (String)sCertificateTypeMap.get(paramString);
    }

    static String[] getKeyStrengthList()
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "High Grade";
        arrayOfString[1] = "Medium Grade";
        return arrayOfString;
    }

    static String getSignedPublicKey(Context paramContext, int paramInt, String paramString)
    {
        try
        {
            KeyPairGenerator localKeyPairGenerator = KeyPairGenerator.getInstance("RSA");
            if (paramInt == 0);
            for (int i = 2048; ; i = 1024)
            {
                localKeyPairGenerator.initialize(i);
                KeyPair localKeyPair = localKeyPairGenerator.genKeyPair();
                NetscapeCertRequest localNetscapeCertRequest = new NetscapeCertRequest(paramString, MD5_WITH_RSA, localKeyPair.getPublic());
                localNetscapeCertRequest.sign(localKeyPair.getPrivate());
                byte[] arrayOfByte = localNetscapeCertRequest.toASN1Object().getDEREncoded();
                Credentials.getInstance().install(paramContext, localKeyPair);
                str = new String(Base64.encode(arrayOfByte));
                return str;
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                Log.w("CertTool", localException);
                String str = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.CertTool
 * JD-Core Version:        0.6.2
 */