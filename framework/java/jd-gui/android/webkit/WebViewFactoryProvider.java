package android.webkit;

import android.content.Context;

public abstract interface WebViewFactoryProvider
{
    public abstract WebViewProvider createWebView(WebView paramWebView, WebView.PrivateAccess paramPrivateAccess);

    public abstract CookieManager getCookieManager();

    public abstract GeolocationPermissions getGeolocationPermissions();

    public abstract Statics getStatics();

    public abstract WebIconDatabase getWebIconDatabase();

    public abstract WebStorage getWebStorage();

    public abstract WebViewDatabase getWebViewDatabase(Context paramContext);

    public static abstract interface Statics
    {
        public abstract String findAddress(String paramString);

        public abstract void setPlatformNotificationsEnabled(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewFactoryProvider
 * JD-Core Version:        0.6.2
 */