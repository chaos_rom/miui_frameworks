package android.webkit;

final class DeviceMotionAndOrientationManager
{
    private WebViewCore mWebViewCore;

    static
    {
        if (!DeviceMotionAndOrientationManager.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    public DeviceMotionAndOrientationManager(WebViewCore paramWebViewCore)
    {
        this.mWebViewCore = paramWebViewCore;
    }

    private static native void nativeOnMotionChange(WebViewCore paramWebViewCore, boolean paramBoolean1, double paramDouble1, boolean paramBoolean2, double paramDouble2, boolean paramBoolean3, double paramDouble3, double paramDouble4);

    private static native void nativeOnOrientationChange(WebViewCore paramWebViewCore, boolean paramBoolean1, double paramDouble1, boolean paramBoolean2, double paramDouble2, boolean paramBoolean3, double paramDouble3);

    private static native void nativeSetMockOrientation(WebViewCore paramWebViewCore, boolean paramBoolean1, double paramDouble1, boolean paramBoolean2, double paramDouble2, boolean paramBoolean3, double paramDouble3);

    private static native void nativeSetUseMock(WebViewCore paramWebViewCore);

    public void onMotionChange(Double paramDouble1, Double paramDouble2, Double paramDouble3, double paramDouble)
    {
        WebViewCore localWebViewCore = this.mWebViewCore;
        boolean bool1;
        double d1;
        label23: boolean bool2;
        label30: double d2;
        label40: boolean bool3;
        if (paramDouble1 != null)
        {
            bool1 = true;
            if (paramDouble1 == null)
                break label83;
            d1 = paramDouble1.doubleValue();
            if (paramDouble2 == null)
                break label89;
            bool2 = true;
            if (paramDouble2 == null)
                break label95;
            d2 = paramDouble2.doubleValue();
            if (paramDouble3 == null)
                break label101;
            bool3 = true;
            label47: if (paramDouble3 == null)
                break label107;
        }
        label83: label89: label95: label101: label107: for (double d3 = paramDouble3.doubleValue(); ; d3 = 0.0D)
        {
            nativeOnMotionChange(localWebViewCore, bool1, d1, bool2, d2, bool3, d3, paramDouble);
            return;
            bool1 = false;
            break;
            d1 = 0.0D;
            break label23;
            bool2 = false;
            break label30;
            d2 = 0.0D;
            break label40;
            bool3 = false;
            break label47;
        }
    }

    public void onOrientationChange(Double paramDouble1, Double paramDouble2, Double paramDouble3)
    {
        boolean bool1 = true;
        double d1 = 0.0D;
        WebViewCore localWebViewCore = this.mWebViewCore;
        boolean bool2;
        double d2;
        label30: boolean bool3;
        label38: double d3;
        if (paramDouble1 != null)
        {
            bool2 = bool1;
            if (paramDouble1 == null)
                break label86;
            d2 = paramDouble1.doubleValue();
            if (paramDouble2 == null)
                break label93;
            bool3 = bool1;
            if (paramDouble2 == null)
                break label99;
            d3 = paramDouble2.doubleValue();
            label48: if (paramDouble3 == null)
                break label106;
        }
        while (true)
        {
            if (paramDouble3 != null)
                d1 = paramDouble3.doubleValue();
            nativeOnOrientationChange(localWebViewCore, bool2, d2, bool3, d3, bool1, d1);
            return;
            bool2 = false;
            break;
            label86: d2 = d1;
            break label30;
            label93: bool3 = false;
            break label38;
            label99: d3 = d1;
            break label48;
            label106: bool1 = false;
        }
    }

    public void setMockOrientation(boolean paramBoolean1, double paramDouble1, boolean paramBoolean2, double paramDouble2, boolean paramBoolean3, double paramDouble3)
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        nativeSetMockOrientation(this.mWebViewCore, paramBoolean1, paramDouble1, paramBoolean2, paramDouble2, paramBoolean3, paramDouble3);
    }

    public void setUseMock()
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        nativeSetUseMock(this.mWebViewCore);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.DeviceMotionAndOrientationManager
 * JD-Core Version:        0.6.2
 */