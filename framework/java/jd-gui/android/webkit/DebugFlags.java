package android.webkit;

class DebugFlags
{
    public static final boolean BROWSER_FRAME;
    public static final boolean CACHE_MANAGER;
    public static final boolean CALLBACK_PROXY;
    public static final boolean COOKIE_MANAGER;
    public static final boolean COOKIE_SYNC_MANAGER;
    public static final boolean FRAME_LOADER;
    public static final boolean J_WEB_CORE_JAVA_BRIDGE;
    public static final boolean LOAD_LISTENER;
    public static final boolean MEASURE_PAGE_SWAP_FPS;
    public static final boolean NETWORK;
    public static final boolean SSL_ERROR_HANDLER;
    public static final boolean STREAM_LOADER;
    public static final boolean URL_UTIL;
    public static final boolean WEB_BACK_FORWARD_LIST;
    public static final boolean WEB_SETTINGS;
    public static final boolean WEB_SYNC_MANAGER;
    public static final boolean WEB_VIEW;
    public static final boolean WEB_VIEW_CORE;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.DebugFlags
 * JD-Core Version:        0.6.2
 */