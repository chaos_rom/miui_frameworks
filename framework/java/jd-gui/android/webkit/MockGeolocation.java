package android.webkit;

public final class MockGeolocation
{
    private static MockGeolocation sMockGeolocation;

    public static MockGeolocation getInstance()
    {
        if (sMockGeolocation == null)
            sMockGeolocation = new MockGeolocation();
        return sMockGeolocation;
    }

    private static native void nativeSetError(int paramInt, String paramString);

    private static native void nativeSetPosition(double paramDouble1, double paramDouble2, double paramDouble3);

    public void setError(int paramInt, String paramString)
    {
        nativeSetError(paramInt, paramString);
    }

    public void setPosition(double paramDouble1, double paramDouble2, double paramDouble3)
    {
        nativeSetPosition(paramDouble1, paramDouble2, paramDouble3);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.MockGeolocation
 * JD-Core Version:        0.6.2
 */