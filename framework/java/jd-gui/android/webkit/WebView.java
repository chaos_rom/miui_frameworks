package android.webkit;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.http.SslCertificate;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.view.ViewTreeObserver.OnGlobalFocusChangeListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.AbsoluteLayout;
import java.io.File;
import java.util.Map;

public class WebView extends AbsoluteLayout
    implements ViewTreeObserver.OnGlobalFocusChangeListener, ViewGroup.OnHierarchyChangeListener
{
    private static final String LOGTAG = "webview_proxy";
    public static final String SCHEME_GEO = "geo:0,0?q=";
    public static final String SCHEME_MAILTO = "mailto:";
    public static final String SCHEME_TEL = "tel:";
    private WebViewProvider mProvider;

    public WebView(Context paramContext)
    {
        this(paramContext, null);
    }

    public WebView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842885);
    }

    public WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        this(paramContext, paramAttributeSet, paramInt, false);
    }

    protected WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt, Map<String, Object> paramMap, boolean paramBoolean)
    {
        super(paramContext, paramAttributeSet, paramInt);
        if (paramContext == null)
            throw new IllegalArgumentException("Invalid context argument");
        checkThread();
        ensureProviderCreated();
        this.mProvider.init(paramMap, paramBoolean);
    }

    public WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt, boolean paramBoolean)
    {
        this(paramContext, paramAttributeSet, paramInt, null, paramBoolean);
    }

    private static void checkThread()
    {
        if (Looper.myLooper() != Looper.getMainLooper())
        {
            Throwable localThrowable = new Throwable("Warning: A WebView method was called on thread '" + Thread.currentThread().getName() + "'. " + "All WebView methods must be called on the UI thread. " + "Future versions of WebView may not support use on other threads.");
            Log.w("webview_proxy", Log.getStackTraceString(localThrowable));
            StrictMode.onWebViewMethodCalledOnWrongThread(localThrowable);
        }
    }

    @Deprecated
    public static void disablePlatformNotifications()
    {
        checkThread();
        getFactory().getStatics().setPlatformNotificationsEnabled(false);
    }

    @Deprecated
    public static void enablePlatformNotifications()
    {
        checkThread();
        getFactory().getStatics().setPlatformNotificationsEnabled(true);
    }

    private void ensureProviderCreated()
    {
        checkThread();
        if (this.mProvider == null)
            this.mProvider = getFactory().createWebView(this, new PrivateAccess());
    }

    public static String findAddress(String paramString)
    {
        checkThread();
        return getFactory().getStatics().findAddress(paramString);
    }

    /** @deprecated */
    private static WebViewFactoryProvider getFactory()
    {
        try
        {
            checkThread();
            WebViewFactoryProvider localWebViewFactoryProvider = WebViewFactory.getProvider();
            return localWebViewFactoryProvider;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public static PluginList getPluginList()
    {
        try
        {
            checkThread();
            PluginList localPluginList = new PluginList();
            return localPluginList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void addJavascriptInterface(Object paramObject, String paramString)
    {
        checkThread();
        this.mProvider.addJavascriptInterface(paramObject, paramString);
    }

    public boolean canGoBack()
    {
        checkThread();
        return this.mProvider.canGoBack();
    }

    public boolean canGoBackOrForward(int paramInt)
    {
        checkThread();
        return this.mProvider.canGoBackOrForward(paramInt);
    }

    public boolean canGoForward()
    {
        checkThread();
        return this.mProvider.canGoForward();
    }

    public boolean canZoomIn()
    {
        checkThread();
        return this.mProvider.canZoomIn();
    }

    public boolean canZoomOut()
    {
        checkThread();
        return this.mProvider.canZoomOut();
    }

    public Picture capturePicture()
    {
        checkThread();
        return this.mProvider.capturePicture();
    }

    public void clearCache(boolean paramBoolean)
    {
        checkThread();
        this.mProvider.clearCache(paramBoolean);
    }

    public void clearFormData()
    {
        checkThread();
        this.mProvider.clearFormData();
    }

    public void clearHistory()
    {
        checkThread();
        this.mProvider.clearHistory();
    }

    public void clearMatches()
    {
        checkThread();
        this.mProvider.clearMatches();
    }

    public void clearSslPreferences()
    {
        checkThread();
        this.mProvider.clearSslPreferences();
    }

    public void clearView()
    {
        checkThread();
        this.mProvider.clearView();
    }

    protected int computeHorizontalScrollOffset()
    {
        return this.mProvider.getScrollDelegate().computeHorizontalScrollOffset();
    }

    protected int computeHorizontalScrollRange()
    {
        return this.mProvider.getScrollDelegate().computeHorizontalScrollRange();
    }

    public void computeScroll()
    {
        this.mProvider.getScrollDelegate().computeScroll();
    }

    protected int computeVerticalScrollExtent()
    {
        return this.mProvider.getScrollDelegate().computeVerticalScrollExtent();
    }

    protected int computeVerticalScrollOffset()
    {
        return this.mProvider.getScrollDelegate().computeVerticalScrollOffset();
    }

    protected int computeVerticalScrollRange()
    {
        return this.mProvider.getScrollDelegate().computeVerticalScrollRange();
    }

    public WebBackForwardList copyBackForwardList()
    {
        checkThread();
        return this.mProvider.copyBackForwardList();
    }

    @Deprecated
    public void debugDump()
    {
        checkThread();
        this.mProvider.debugDump();
    }

    public void destroy()
    {
        checkThread();
        this.mProvider.destroy();
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        return this.mProvider.getViewDelegate().dispatchKeyEvent(paramKeyEvent);
    }

    public void documentHasImages(Message paramMessage)
    {
        checkThread();
        this.mProvider.documentHasImages(paramMessage);
    }

    @Deprecated
    public void emulateShiftHeld()
    {
        checkThread();
        this.mProvider.emulateShiftHeld();
    }

    @Deprecated
    public int findAll(String paramString)
    {
        checkThread();
        StrictMode.noteSlowCall("findAll blocks UI: prefer findAllAsync");
        return this.mProvider.findAll(paramString);
    }

    public void findAllAsync(String paramString)
    {
        checkThread();
        this.mProvider.findAllAsync(paramString);
    }

    public void findNext(boolean paramBoolean)
    {
        checkThread();
        this.mProvider.findNext(paramBoolean);
    }

    public void flingScroll(int paramInt1, int paramInt2)
    {
        checkThread();
        this.mProvider.flingScroll(paramInt1, paramInt2);
    }

    public void freeMemory()
    {
        checkThread();
        this.mProvider.freeMemory();
    }

    public SslCertificate getCertificate()
    {
        checkThread();
        return this.mProvider.getCertificate();
    }

    public int getContentHeight()
    {
        checkThread();
        return this.mProvider.getContentHeight();
    }

    public int getContentWidth()
    {
        return this.mProvider.getContentWidth();
    }

    public Bitmap getFavicon()
    {
        checkThread();
        return this.mProvider.getFavicon();
    }

    public HitTestResult getHitTestResult()
    {
        checkThread();
        return this.mProvider.getHitTestResult();
    }

    public String[] getHttpAuthUsernamePassword(String paramString1, String paramString2)
    {
        checkThread();
        return this.mProvider.getHttpAuthUsernamePassword(paramString1, paramString2);
    }

    public String getOriginalUrl()
    {
        checkThread();
        return this.mProvider.getOriginalUrl();
    }

    public int getProgress()
    {
        checkThread();
        return this.mProvider.getProgress();
    }

    public float getScale()
    {
        checkThread();
        return this.mProvider.getScale();
    }

    public WebSettings getSettings()
    {
        checkThread();
        return this.mProvider.getSettings();
    }

    public String getTitle()
    {
        checkThread();
        return this.mProvider.getTitle();
    }

    public String getTouchIconUrl()
    {
        return this.mProvider.getTouchIconUrl();
    }

    public String getUrl()
    {
        checkThread();
        return this.mProvider.getUrl();
    }

    public int getVisibleTitleHeight()
    {
        checkThread();
        return this.mProvider.getVisibleTitleHeight();
    }

    public WebViewProvider getWebViewProvider()
    {
        return this.mProvider;
    }

    @Deprecated
    public View getZoomControls()
    {
        checkThread();
        return this.mProvider.getZoomControls();
    }

    public void goBack()
    {
        checkThread();
        this.mProvider.goBack();
    }

    public void goBackOrForward(int paramInt)
    {
        checkThread();
        this.mProvider.goBackOrForward(paramInt);
    }

    public void goForward()
    {
        checkThread();
        this.mProvider.goForward();
    }

    public void invokeZoomPicker()
    {
        checkThread();
        this.mProvider.invokeZoomPicker();
    }

    public boolean isPaused()
    {
        return this.mProvider.isPaused();
    }

    public boolean isPrivateBrowsingEnabled()
    {
        checkThread();
        return this.mProvider.isPrivateBrowsingEnabled();
    }

    public void loadData(String paramString1, String paramString2, String paramString3)
    {
        checkThread();
        this.mProvider.loadData(paramString1, paramString2, paramString3);
    }

    public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        checkThread();
        this.mProvider.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void loadUrl(String paramString)
    {
        checkThread();
        this.mProvider.loadUrl(WebViewUtils.processUrl(paramString));
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void loadUrl(String paramString, Map<String, String> paramMap)
    {
        checkThread();
        this.mProvider.loadUrl(WebViewUtils.processUrl(paramString), paramMap);
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.mProvider.getViewDelegate().onAttachedToWindow();
    }

    @Deprecated
    public void onChildViewAdded(View paramView1, View paramView2)
    {
    }

    @Deprecated
    public void onChildViewRemoved(View paramView1, View paramView2)
    {
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        this.mProvider.getViewDelegate().onConfigurationChanged(paramConfiguration);
    }

    public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo)
    {
        return this.mProvider.getViewDelegate().onCreateInputConnection(paramEditorInfo);
    }

    protected void onDetachedFromWindow()
    {
        this.mProvider.getViewDelegate().onDetachedFromWindow();
        super.onDetachedFromWindow();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        this.mProvider.getViewDelegate().onDraw(paramCanvas);
    }

    protected void onDrawVerticalScrollBar(Canvas paramCanvas, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mProvider.getViewDelegate().onDrawVerticalScrollBar(paramCanvas, paramDrawable, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        this.mProvider.getViewDelegate().onFocusChanged(paramBoolean, paramInt, paramRect);
        super.onFocusChanged(paramBoolean, paramInt, paramRect);
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        return this.mProvider.getViewDelegate().onGenericMotionEvent(paramMotionEvent);
    }

    @Deprecated
    public void onGlobalFocusChanged(View paramView1, View paramView2)
    {
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        return this.mProvider.getViewDelegate().onHoverEvent(paramMotionEvent);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(WebView.class.getName());
        this.mProvider.getViewDelegate().onInitializeAccessibilityEvent(paramAccessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(WebView.class.getName());
        this.mProvider.getViewDelegate().onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        return this.mProvider.getViewDelegate().onKeyDown(paramInt, paramKeyEvent);
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        return this.mProvider.getViewDelegate().onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        return this.mProvider.getViewDelegate().onKeyUp(paramInt, paramKeyEvent);
    }

    @Deprecated
    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        this.mProvider.getViewDelegate().onMeasure(paramInt1, paramInt2);
    }

    protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mProvider.getViewDelegate().onOverScrolled(paramInt1, paramInt2, paramBoolean1, paramBoolean2);
    }

    public void onPause()
    {
        checkThread();
        this.mProvider.onPause();
    }

    public void onResume()
    {
        checkThread();
        this.mProvider.onResume();
    }

    protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mProvider.getViewDelegate().onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mProvider.getViewDelegate().onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        return this.mProvider.getViewDelegate().onTouchEvent(paramMotionEvent);
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        return this.mProvider.getViewDelegate().onTrackballEvent(paramMotionEvent);
    }

    protected void onVisibilityChanged(View paramView, int paramInt)
    {
        super.onVisibilityChanged(paramView, paramInt);
        this.mProvider.getViewDelegate().onVisibilityChanged(paramView, paramInt);
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        this.mProvider.getViewDelegate().onWindowFocusChanged(paramBoolean);
        super.onWindowFocusChanged(paramBoolean);
    }

    protected void onWindowVisibilityChanged(int paramInt)
    {
        super.onWindowVisibilityChanged(paramInt);
        this.mProvider.getViewDelegate().onWindowVisibilityChanged(paramInt);
    }

    public boolean overlayHorizontalScrollbar()
    {
        checkThread();
        return this.mProvider.overlayHorizontalScrollbar();
    }

    public boolean overlayVerticalScrollbar()
    {
        checkThread();
        return this.mProvider.overlayVerticalScrollbar();
    }

    public boolean pageDown(boolean paramBoolean)
    {
        checkThread();
        return this.mProvider.pageDown(paramBoolean);
    }

    public boolean pageUp(boolean paramBoolean)
    {
        checkThread();
        return this.mProvider.pageUp(paramBoolean);
    }

    public void pauseTimers()
    {
        checkThread();
        this.mProvider.pauseTimers();
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        return this.mProvider.getViewDelegate().performAccessibilityAction(paramInt, paramBundle);
    }

    public boolean performLongClick()
    {
        return this.mProvider.getViewDelegate().performLongClick();
    }

    public void postUrl(String paramString, byte[] paramArrayOfByte)
    {
        checkThread();
        this.mProvider.postUrl(paramString, paramArrayOfByte);
    }

    @Deprecated
    public void refreshPlugins(boolean paramBoolean)
    {
        checkThread();
    }

    public void reload()
    {
        checkThread();
        this.mProvider.reload();
    }

    public void removeJavascriptInterface(String paramString)
    {
        checkThread();
        this.mProvider.removeJavascriptInterface(paramString);
    }

    public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
    {
        return this.mProvider.getViewDelegate().requestChildRectangleOnScreen(paramView, paramRect, paramBoolean);
    }

    public boolean requestFocus(int paramInt, Rect paramRect)
    {
        return this.mProvider.getViewDelegate().requestFocus(paramInt, paramRect);
    }

    public void requestFocusNodeHref(Message paramMessage)
    {
        checkThread();
        this.mProvider.requestFocusNodeHref(paramMessage);
    }

    public void requestImageRef(Message paramMessage)
    {
        checkThread();
        this.mProvider.requestImageRef(paramMessage);
    }

    @Deprecated
    public boolean restorePicture(Bundle paramBundle, File paramFile)
    {
        checkThread();
        return this.mProvider.restorePicture(paramBundle, paramFile);
    }

    public WebBackForwardList restoreState(Bundle paramBundle)
    {
        checkThread();
        return this.mProvider.restoreState(paramBundle);
    }

    public void resumeTimers()
    {
        checkThread();
        this.mProvider.resumeTimers();
    }

    public void savePassword(String paramString1, String paramString2, String paramString3)
    {
        checkThread();
        this.mProvider.savePassword(paramString1, paramString2, paramString3);
    }

    @Deprecated
    public boolean savePicture(Bundle paramBundle, File paramFile)
    {
        checkThread();
        return this.mProvider.savePicture(paramBundle, paramFile);
    }

    public WebBackForwardList saveState(Bundle paramBundle)
    {
        checkThread();
        return this.mProvider.saveState(paramBundle);
    }

    public void saveWebArchive(String paramString)
    {
        checkThread();
        this.mProvider.saveWebArchive(paramString);
    }

    public void saveWebArchive(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback)
    {
        checkThread();
        this.mProvider.saveWebArchive(paramString, paramBoolean, paramValueCallback);
    }

    public void setBackgroundColor(int paramInt)
    {
        this.mProvider.getViewDelegate().setBackgroundColor(paramInt);
    }

    public void setCertificate(SslCertificate paramSslCertificate)
    {
        checkThread();
        this.mProvider.setCertificate(paramSslCertificate);
    }

    public void setDownloadListener(DownloadListener paramDownloadListener)
    {
        checkThread();
        this.mProvider.setDownloadListener(paramDownloadListener);
    }

    public void setFindListener(FindListener paramFindListener)
    {
        checkThread();
        this.mProvider.setFindListener(paramFindListener);
    }

    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return this.mProvider.getViewDelegate().setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void setHorizontalScrollbarOverlay(boolean paramBoolean)
    {
        checkThread();
        this.mProvider.setHorizontalScrollbarOverlay(paramBoolean);
    }

    public void setHttpAuthUsernamePassword(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        checkThread();
        this.mProvider.setHttpAuthUsernamePassword(paramString1, paramString2, paramString3, paramString4);
    }

    public void setInitialScale(int paramInt)
    {
        checkThread();
        this.mProvider.setInitialScale(paramInt);
    }

    public void setLayerType(int paramInt, Paint paramPaint)
    {
        super.setLayerType(paramInt, paramPaint);
        this.mProvider.getViewDelegate().setLayerType(paramInt, paramPaint);
    }

    public void setLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        this.mProvider.getViewDelegate().setLayoutParams(paramLayoutParams);
    }

    public void setMapTrackballToArrowKeys(boolean paramBoolean)
    {
        checkThread();
        this.mProvider.setMapTrackballToArrowKeys(paramBoolean);
    }

    public void setNetworkAvailable(boolean paramBoolean)
    {
        checkThread();
        this.mProvider.setNetworkAvailable(paramBoolean);
    }

    public void setOverScrollMode(int paramInt)
    {
        super.setOverScrollMode(paramInt);
        ensureProviderCreated();
        this.mProvider.getViewDelegate().setOverScrollMode(paramInt);
    }

    @Deprecated
    public void setPictureListener(PictureListener paramPictureListener)
    {
        checkThread();
        this.mProvider.setPictureListener(paramPictureListener);
    }

    public void setScrollBarStyle(int paramInt)
    {
        this.mProvider.getViewDelegate().setScrollBarStyle(paramInt);
        super.setScrollBarStyle(paramInt);
    }

    public void setVerticalScrollbarOverlay(boolean paramBoolean)
    {
        checkThread();
        this.mProvider.setVerticalScrollbarOverlay(paramBoolean);
    }

    public void setWebChromeClient(WebChromeClient paramWebChromeClient)
    {
        checkThread();
        this.mProvider.setWebChromeClient(paramWebChromeClient);
    }

    public void setWebViewClient(WebViewClient paramWebViewClient)
    {
        checkThread();
        this.mProvider.setWebViewClient(paramWebViewClient);
    }

    @Deprecated
    public boolean shouldDelayChildPressedState()
    {
        return this.mProvider.getViewDelegate().shouldDelayChildPressedState();
    }

    public boolean showFindDialog(String paramString, boolean paramBoolean)
    {
        checkThread();
        return this.mProvider.showFindDialog(paramString, paramBoolean);
    }

    public void stopLoading()
    {
        checkThread();
        this.mProvider.stopLoading();
    }

    public boolean zoomIn()
    {
        checkThread();
        return this.mProvider.zoomIn();
    }

    public boolean zoomOut()
    {
        checkThread();
        return this.mProvider.zoomOut();
    }

    public class PrivateAccess
    {
        public PrivateAccess()
        {
        }

        public void awakenScrollBars(int paramInt)
        {
            WebView.this.awakenScrollBars(paramInt);
        }

        public void awakenScrollBars(int paramInt, boolean paramBoolean)
        {
            WebView.this.awakenScrollBars(paramInt, paramBoolean);
        }

        public float getHorizontalScrollFactor()
        {
            return WebView.this.getHorizontalScrollFactor();
        }

        public int getHorizontalScrollbarHeight()
        {
            return WebView.this.getHorizontalScrollbarHeight();
        }

        public float getVerticalScrollFactor()
        {
            return WebView.this.getVerticalScrollFactor();
        }

        public void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            WebView.this.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        }

        public void overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean)
        {
            WebView.this.overScrollBy(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBoolean);
        }

        public void setMeasuredDimension(int paramInt1, int paramInt2)
        {
            WebView.this.setMeasuredDimension(paramInt1, paramInt2);
        }

        public void setScrollXRaw(int paramInt)
        {
            WebView.access$1802(WebView.this, paramInt);
        }

        public void setScrollYRaw(int paramInt)
        {
            WebView.access$1902(WebView.this, paramInt);
        }

        public void super_computeScroll()
        {
            WebView.this.computeScroll();
        }

        public boolean super_dispatchKeyEvent(KeyEvent paramKeyEvent)
        {
            return WebView.this.dispatchKeyEvent(paramKeyEvent);
        }

        public int super_getScrollBarStyle()
        {
            return WebView.this.getScrollBarStyle();
        }

        public boolean super_onGenericMotionEvent(MotionEvent paramMotionEvent)
        {
            return WebView.this.onGenericMotionEvent(paramMotionEvent);
        }

        public boolean super_onHoverEvent(MotionEvent paramMotionEvent)
        {
            return WebView.this.onHoverEvent(paramMotionEvent);
        }

        public boolean super_performAccessibilityAction(int paramInt, Bundle paramBundle)
        {
            return WebView.this.performAccessibilityAction(paramInt, paramBundle);
        }

        public boolean super_performLongClick()
        {
            return WebView.this.performLongClick();
        }

        public boolean super_requestFocus(int paramInt, Rect paramRect)
        {
            return WebView.this.requestFocus(paramInt, paramRect);
        }

        public void super_scrollTo(int paramInt1, int paramInt2)
        {
            WebView.this.scrollTo(paramInt1, paramInt2);
        }

        public boolean super_setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            return WebView.this.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        }

        public void super_setLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            WebView.this.setLayoutParams(paramLayoutParams);
        }
    }

    public static class HitTestResult
    {

        @Deprecated
        public static final int ANCHOR_TYPE = 1;
        public static final int EDIT_TEXT_TYPE = 9;
        public static final int EMAIL_TYPE = 4;
        public static final int GEO_TYPE = 3;

        @Deprecated
        public static final int IMAGE_ANCHOR_TYPE = 6;
        public static final int IMAGE_TYPE = 5;
        public static final int PHONE_TYPE = 2;
        public static final int SRC_ANCHOR_TYPE = 7;
        public static final int SRC_IMAGE_ANCHOR_TYPE = 8;
        public static final int UNKNOWN_TYPE;
        private String mExtra;
        private int mType = 0;

        public String getExtra()
        {
            return this.mExtra;
        }

        public int getType()
        {
            return this.mType;
        }

        public void setExtra(String paramString)
        {
            this.mExtra = paramString;
        }

        public void setType(int paramInt)
        {
            this.mType = paramInt;
        }
    }

    @Deprecated
    public static abstract interface PictureListener
    {
        @Deprecated
        public abstract void onNewPicture(WebView paramWebView, Picture paramPicture);
    }

    public static abstract interface FindListener
    {
        public abstract void onFindResultReceived(int paramInt1, int paramInt2, boolean paramBoolean);
    }

    public class WebViewTransport
    {
        private WebView mWebview;

        public WebViewTransport()
        {
        }

        /** @deprecated */
        public WebView getWebView()
        {
            try
            {
                WebView localWebView = this.mWebview;
                return localWebView;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public void setWebView(WebView paramWebView)
        {
            try
            {
                this.mWebview = paramWebView;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebView
 * JD-Core Version:        0.6.2
 */