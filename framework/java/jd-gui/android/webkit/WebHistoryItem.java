package android.webkit;

import android.graphics.Bitmap;
import java.net.MalformedURLException;
import java.net.URL;

public class WebHistoryItem
    implements Cloneable
{
    private static int sNextId = 0;
    private Object mCustomData;
    private Bitmap mFavicon;
    private byte[] mFlattenedData;
    private final int mId;
    private int mNativeBridge;
    private String mTouchIconUrlFromLink;
    private String mTouchIconUrlServerDefault;

    private WebHistoryItem(int paramInt)
    {
        try
        {
            int i = sNextId;
            sNextId = i + 1;
            this.mId = i;
            this.mNativeBridge = paramInt;
            nativeRef(this.mNativeBridge);
            return;
        }
        finally
        {
        }
    }

    private WebHistoryItem(WebHistoryItem paramWebHistoryItem)
    {
        this.mFlattenedData = paramWebHistoryItem.mFlattenedData;
        this.mId = paramWebHistoryItem.mId;
        this.mFavicon = paramWebHistoryItem.mFavicon;
        this.mNativeBridge = paramWebHistoryItem.mNativeBridge;
        if (this.mNativeBridge != 0)
            nativeRef(this.mNativeBridge);
    }

    WebHistoryItem(byte[] paramArrayOfByte)
    {
        this.mFlattenedData = paramArrayOfByte;
        try
        {
            int i = sNextId;
            sNextId = i + 1;
            this.mId = i;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private native int inflate(int paramInt, byte[] paramArrayOfByte);

    private native Bitmap nativeGetFavicon(int paramInt);

    private native byte[] nativeGetFlattenedData(int paramInt);

    private native String nativeGetOriginalUrl(int paramInt);

    private native String nativeGetTitle(int paramInt);

    private native String nativeGetUrl(int paramInt);

    private native void nativeRef(int paramInt);

    private native void nativeUnref(int paramInt);

    /** @deprecated */
    protected WebHistoryItem clone()
    {
        try
        {
            WebHistoryItem localWebHistoryItem = new WebHistoryItem(this);
            return localWebHistoryItem;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mNativeBridge != 0)
        {
            nativeUnref(this.mNativeBridge);
            this.mNativeBridge = 0;
        }
    }

    public Object getCustomData()
    {
        return this.mCustomData;
    }

    public Bitmap getFavicon()
    {
        if ((this.mFavicon == null) && (this.mNativeBridge != 0))
            this.mFavicon = nativeGetFavicon(this.mNativeBridge);
        return this.mFavicon;
    }

    byte[] getFlattenedData()
    {
        if (this.mNativeBridge != 0);
        for (byte[] arrayOfByte = nativeGetFlattenedData(this.mNativeBridge); ; arrayOfByte = this.mFlattenedData)
            return arrayOfByte;
    }

    @Deprecated
    public int getId()
    {
        return this.mId;
    }

    public String getOriginalUrl()
    {
        if (this.mNativeBridge == 0);
        for (String str = null; ; str = nativeGetOriginalUrl(this.mNativeBridge))
            return str;
    }

    public String getTitle()
    {
        if (this.mNativeBridge == 0);
        for (String str = null; ; str = nativeGetTitle(this.mNativeBridge))
            return str;
    }

    public String getTouchIconUrl()
    {
        String str;
        if (this.mTouchIconUrlFromLink != null)
            str = this.mTouchIconUrlFromLink;
        while (true)
        {
            return str;
            if (this.mTouchIconUrlServerDefault != null)
                str = this.mTouchIconUrlServerDefault;
            else
                try
                {
                    URL localURL = new URL(getOriginalUrl());
                    this.mTouchIconUrlServerDefault = new URL(localURL.getProtocol(), localURL.getHost(), localURL.getPort(), "/apple-touch-icon.png").toString();
                    str = this.mTouchIconUrlServerDefault;
                }
                catch (MalformedURLException localMalformedURLException)
                {
                    str = null;
                }
        }
    }

    public String getUrl()
    {
        if (this.mNativeBridge == 0);
        for (String str = null; ; str = nativeGetUrl(this.mNativeBridge))
            return str;
    }

    void inflate(int paramInt)
    {
        this.mNativeBridge = inflate(paramInt, this.mFlattenedData);
        this.mFlattenedData = null;
    }

    public void setCustomData(Object paramObject)
    {
        this.mCustomData = paramObject;
    }

    void setFavicon(Bitmap paramBitmap)
    {
        this.mFavicon = paramBitmap;
    }

    void setTouchIconUrl(String paramString, boolean paramBoolean)
    {
        if ((paramBoolean) || (this.mTouchIconUrlFromLink == null))
            this.mTouchIconUrlFromLink = paramString;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebHistoryItem
 * JD-Core Version:        0.6.2
 */