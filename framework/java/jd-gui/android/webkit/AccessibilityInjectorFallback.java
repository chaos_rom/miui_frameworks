package android.webkit;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.text.TextUtils.SimpleStringSplitter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

class AccessibilityInjectorFallback
{
    private static final int ACTION_PERFORM_AXIS_TRANSITION = 3;
    private static final int ACTION_SET_CURRENT_AXIS = 0;
    private static final int ACTION_TRAVERSE_CURRENT_AXIS = 1;
    private static final int ACTION_TRAVERSE_DEFAULT_WEB_VIEW_BEHAVIOR_AXIS = 4;
    private static final int ACTION_TRAVERSE_GIVEN_AXIS = 2;
    private static final boolean DEBUG = true;
    private static final String LOG_TAG = "AccessibilityInjector";
    private static final int NAVIGATION_AXIS_CHARACTER = 0;
    private static final int NAVIGATION_AXIS_DEFAULT_WEB_VIEW_BEHAVIOR = 7;
    private static final int NAVIGATION_AXIS_DOCUMENT = 6;
    private static final int NAVIGATION_AXIS_HEADING = 3;
    private static final int NAVIGATION_AXIS_PARENT_FIRST_CHILD = 5;
    private static final int NAVIGATION_AXIS_SENTENCE = 2;
    private static final int NAVIGATION_AXIS_SIBLING = 5;
    private static final int NAVIGATION_AXIS_WORD = 1;
    private static final int NAVIGATION_DIRECTION_BACKWARD = 0;
    private static final int NAVIGATION_DIRECTION_FORWARD = 1;
    private static ArrayList<AccessibilityWebContentKeyBinding> sBindings = new ArrayList();
    private int mCurrentAxis = 2;
    private boolean mIsLastSelectionStringNull;
    private int mLastDirection;
    private boolean mLastDownEventHandled;
    private final Stack<AccessibilityEvent> mScheduledEventStack = new Stack();
    private final WebViewClassic mWebView;
    private final WebView mWebViewInternal;

    public AccessibilityInjectorFallback(WebViewClassic paramWebViewClassic)
    {
        this.mWebView = paramWebViewClassic;
        this.mWebViewInternal = this.mWebView.getWebView();
        ensureWebContentKeyBindings();
    }

    private void ensureWebContentKeyBindings()
    {
        if (sBindings.size() > 0);
        while (true)
        {
            return;
            String str1 = Settings.Secure.getString(this.mWebView.getContext().getContentResolver(), "accessibility_web_content_key_bindings");
            TextUtils.SimpleStringSplitter localSimpleStringSplitter = new TextUtils.SimpleStringSplitter(';');
            localSimpleStringSplitter.setString(str1);
            while (localSimpleStringSplitter.hasNext())
            {
                String str2 = localSimpleStringSplitter.next();
                if (TextUtils.isEmpty(str2))
                {
                    Log.e("AccessibilityInjector", "Disregarding malformed Web content key binding: " + str1);
                }
                else
                {
                    String[] arrayOfString1 = str2.split("=");
                    if (arrayOfString1.length != 2)
                        Log.e("AccessibilityInjector", "Disregarding malformed Web content key binding: " + str2);
                    else
                        try
                        {
                            long l = Long.decode(arrayOfString1[0].trim()).longValue();
                            String[] arrayOfString2 = arrayOfString1[1].split(":");
                            int[] arrayOfInt = new int[arrayOfString2.length];
                            int i = 0;
                            int j = arrayOfInt.length;
                            while (i < j)
                            {
                                arrayOfInt[i] = Integer.decode(arrayOfString2[i].trim()).intValue();
                                i++;
                            }
                            sBindings.add(new AccessibilityWebContentKeyBinding(l, arrayOfInt));
                        }
                        catch (NumberFormatException localNumberFormatException)
                        {
                            Log.e("AccessibilityInjector", "Disregarding malformed key binding: " + str2);
                        }
                }
            }
        }
    }

    private static int getAxisForGranularity(int paramInt)
    {
        int i = 2;
        switch (paramInt)
        {
        default:
            i = -1;
        case 4:
        case 8:
        case 1:
        case 2:
        case 16:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
            continue;
            i = 6;
        }
    }

    private static int getDirectionForAction(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 256:
        case 1024:
        case 512:
        case 2048:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 0;
        }
    }

    private AccessibilityEvent getPartialyPopulatedAccessibilityEvent(int paramInt)
    {
        AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt);
        this.mWebViewInternal.onInitializeAccessibilityEvent(localAccessibilityEvent);
        return localAccessibilityEvent;
    }

    private boolean isEnterActionKey(int paramInt)
    {
        if ((paramInt == 23) || (paramInt == 66) || (paramInt == 160));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void performAxisTransition(int paramInt1, int paramInt2, boolean paramBoolean, String paramString)
    {
        if (this.mCurrentAxis == paramInt1)
            setCurrentAxis(paramInt2, paramBoolean, paramString);
    }

    private void sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        Log.d("AccessibilityInjector", "Dispatching: " + paramAccessibilityEvent);
        AccessibilityManager localAccessibilityManager = AccessibilityManager.getInstance(this.mWebView.getContext());
        if (localAccessibilityManager.isEnabled())
            localAccessibilityManager.sendAccessibilityEvent(paramAccessibilityEvent);
    }

    private void setCurrentAxis(int paramInt, boolean paramBoolean, String paramString)
    {
        this.mCurrentAxis = paramInt;
        if (paramBoolean)
        {
            AccessibilityEvent localAccessibilityEvent = getPartialyPopulatedAccessibilityEvent(16384);
            localAccessibilityEvent.getText().add(String.valueOf(paramInt));
            localAccessibilityEvent.setContentDescription(paramString);
            sendAccessibilityEvent(localAccessibilityEvent);
        }
    }

    private boolean traverseCurrentAxis(int paramInt, boolean paramBoolean, String paramString)
    {
        return traverseGivenAxis(paramInt, this.mCurrentAxis, paramBoolean, paramString);
    }

    private boolean traverseGivenAxis(int paramInt1, int paramInt2, boolean paramBoolean, String paramString)
    {
        boolean bool = false;
        WebViewCore localWebViewCore = this.mWebView.getWebViewCore();
        if (localWebViewCore == null);
        while (true)
        {
            return bool;
            AccessibilityEvent localAccessibilityEvent = null;
            if (paramBoolean)
            {
                localAccessibilityEvent = getPartialyPopulatedAccessibilityEvent(131072);
                localAccessibilityEvent.setContentDescription(paramString);
            }
            this.mScheduledEventStack.push(localAccessibilityEvent);
            if (paramInt2 != 7)
            {
                localWebViewCore.sendMessage(190, paramInt1, paramInt2);
                bool = true;
            }
        }
    }

    public boolean onKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool1;
        if (isEnterActionKey(paramKeyEvent.getKeyCode()))
            bool1 = false;
        while (true)
        {
            return bool1;
            if (paramKeyEvent.getAction() == 1)
            {
                bool1 = this.mLastDownEventHandled;
            }
            else
            {
                this.mLastDownEventHandled = false;
                Object localObject = null;
                Iterator localIterator = sBindings.iterator();
                while (localIterator.hasNext())
                {
                    AccessibilityWebContentKeyBinding localAccessibilityWebContentKeyBinding = (AccessibilityWebContentKeyBinding)localIterator.next();
                    if ((paramKeyEvent.getKeyCode() == localAccessibilityWebContentKeyBinding.getKeyCode()) && (paramKeyEvent.hasModifiers(localAccessibilityWebContentKeyBinding.getModifiers())))
                        localObject = localAccessibilityWebContentKeyBinding;
                }
                if (localObject == null)
                {
                    bool1 = false;
                }
                else
                {
                    int i = 0;
                    int j = localObject.getActionCount();
                    label114: if (i < j)
                    {
                        int k = localObject.getActionCode(i);
                        String str = Integer.toHexString(localObject.getAction(i));
                        switch (k)
                        {
                        default:
                            Log.w("AccessibilityInjector", "Unknown action code: " + k);
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        }
                        while (true)
                        {
                            i++;
                            break label114;
                            int i4 = localObject.getFirstArgument(i);
                            if (localObject.getSecondArgument(i) == 1);
                            for (boolean bool6 = true; ; bool6 = false)
                            {
                                setCurrentAxis(i4, bool6, str);
                                this.mLastDownEventHandled = true;
                                break;
                            }
                            int i3 = localObject.getFirstArgument(i);
                            if ((i3 == this.mLastDirection) && (this.mIsLastSelectionStringNull))
                            {
                                this.mIsLastSelectionStringNull = false;
                                bool1 = false;
                                break;
                            }
                            this.mLastDirection = i3;
                            if (localObject.getSecondArgument(i) == 1);
                            for (boolean bool5 = true; ; bool5 = false)
                            {
                                this.mLastDownEventHandled = traverseCurrentAxis(i3, bool5, str);
                                break;
                            }
                            int i1 = localObject.getFirstArgument(i);
                            if ((i1 == this.mLastDirection) && (this.mIsLastSelectionStringNull))
                            {
                                this.mIsLastSelectionStringNull = false;
                                bool1 = false;
                                break;
                            }
                            this.mLastDirection = i1;
                            int i2 = localObject.getSecondArgument(i);
                            if (localObject.getThirdArgument(i) == 1);
                            for (boolean bool4 = true; ; bool4 = false)
                            {
                                traverseGivenAxis(i1, i2, bool4, str);
                                this.mLastDownEventHandled = true;
                                break;
                            }
                            int m = localObject.getFirstArgument(i);
                            int n = localObject.getSecondArgument(i);
                            if (localObject.getThirdArgument(i) == 1);
                            for (boolean bool3 = true; ; bool3 = false)
                            {
                                performAxisTransition(m, n, bool3, str);
                                this.mLastDownEventHandled = true;
                                break;
                            }
                            if (this.mCurrentAxis == 7)
                            {
                                this.mLastDirection = localObject.getFirstArgument(i);
                                if (localObject.getSecondArgument(i) == 1);
                                for (boolean bool2 = true; ; bool2 = false)
                                {
                                    traverseGivenAxis(this.mLastDirection, 7, bool2, str);
                                    this.mLastDownEventHandled = false;
                                    break;
                                }
                            }
                            this.mLastDownEventHandled = true;
                        }
                    }
                    bool1 = this.mLastDownEventHandled;
                }
            }
        }
    }

    public void onSelectionStringChange(String paramString)
    {
        Log.d("AccessibilityInjector", "Selection string: " + paramString);
        boolean bool;
        if (paramString == null)
        {
            bool = true;
            this.mIsLastSelectionStringNull = bool;
            if (!this.mScheduledEventStack.isEmpty())
                break label53;
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label53: AccessibilityEvent localAccessibilityEvent = (AccessibilityEvent)this.mScheduledEventStack.pop();
            if ((localAccessibilityEvent != null) && (paramString != null))
            {
                localAccessibilityEvent.getText().add(paramString);
                localAccessibilityEvent.setFromIndex(0);
                localAccessibilityEvent.setToIndex(paramString.length());
                sendAccessibilityEvent(localAccessibilityEvent);
            }
        }
    }

    boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool;
        switch (paramInt)
        {
        default:
            bool = false;
        case 256:
        case 512:
        case 1024:
        case 2048:
        }
        while (true)
        {
            return bool;
            bool = traverseGivenAxis(getDirectionForAction(paramInt), getAxisForGranularity(paramBundle.getInt("ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT")), true, null);
            continue;
            bool = traverseGivenAxis(getDirectionForAction(paramInt), 2, true, null);
        }
    }

    private static final class AccessibilityWebContentKeyBinding
    {
        private static final int ACTION_MASK = -16777216;
        private static final int ACTION_OFFSET = 24;
        private static final int FIRST_ARGUMENT_MASK = 16711680;
        private static final int FIRST_ARGUMENT_OFFSET = 16;
        private static final long KEY_CODE_MASK = 4294967295L;
        private static final int KEY_CODE_OFFSET = 0;
        private static final long MODIFIERS_MASK = 1152921500311879680L;
        private static final int MODIFIERS_OFFSET = 32;
        private static final int SECOND_ARGUMENT_MASK = 65280;
        private static final int SECOND_ARGUMENT_OFFSET = 8;
        private static final int THIRD_ARGUMENT_MASK = 255;
        private static final int THIRD_ARGUMENT_OFFSET;
        private final int[] mActionSequence;
        private final long mKeyCodeAndModifiers;

        public AccessibilityWebContentKeyBinding(long paramLong, int[] paramArrayOfInt)
        {
            this.mKeyCodeAndModifiers = paramLong;
            this.mActionSequence = paramArrayOfInt;
        }

        public int getAction(int paramInt)
        {
            return this.mActionSequence[paramInt];
        }

        public int getActionCode(int paramInt)
        {
            return (0xFF000000 & this.mActionSequence[paramInt]) >> 24;
        }

        public int getActionCount()
        {
            return this.mActionSequence.length;
        }

        public int getFirstArgument(int paramInt)
        {
            return (0xFF0000 & this.mActionSequence[paramInt]) >> 16;
        }

        public int getKeyCode()
        {
            return (int)((0xFFFFFFFF & this.mKeyCodeAndModifiers) >> 0);
        }

        public int getModifiers()
        {
            return (int)((0x0 & this.mKeyCodeAndModifiers) >> 32);
        }

        public int getSecondArgument(int paramInt)
        {
            return (0xFF00 & this.mActionSequence[paramInt]) >> 8;
        }

        public int getThirdArgument(int paramInt)
        {
            return (0xFF & this.mActionSequence[paramInt]) >> 0;
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("modifiers: ");
            localStringBuilder.append(getModifiers());
            localStringBuilder.append(", keyCode: ");
            localStringBuilder.append(getKeyCode());
            localStringBuilder.append(", actions[");
            int i = 0;
            int j = getActionCount();
            while (i < j)
            {
                localStringBuilder.append("{actionCode");
                localStringBuilder.append(i);
                localStringBuilder.append(": ");
                localStringBuilder.append(getActionCode(i));
                localStringBuilder.append(", firstArgument: ");
                localStringBuilder.append(getFirstArgument(i));
                localStringBuilder.append(", secondArgument: ");
                localStringBuilder.append(getSecondArgument(i));
                localStringBuilder.append(", thirdArgument: ");
                localStringBuilder.append(getThirdArgument(i));
                localStringBuilder.append("}");
                i++;
            }
            localStringBuilder.append("]");
            return localStringBuilder.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.AccessibilityInjectorFallback
 * JD-Core Version:        0.6.2
 */