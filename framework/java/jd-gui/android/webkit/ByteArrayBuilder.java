package android.webkit;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.LinkedList;
import java.util.ListIterator;

class ByteArrayBuilder
{
    private static final int DEFAULT_CAPACITY = 8192;
    private static final LinkedList<SoftReference<Chunk>> sPool = new LinkedList();
    private static final ReferenceQueue<Chunk> sQueue = new ReferenceQueue();
    private LinkedList<Chunk> mChunks = new LinkedList();

    private Chunk obtainChunk(int paramInt)
    {
        if (paramInt < 8192)
            paramInt = 8192;
        Chunk localChunk;
        synchronized (sPool)
        {
            processPoolLocked();
            if (!sPool.isEmpty())
            {
                localChunk = (Chunk)((SoftReference)sPool.removeFirst()).get();
                if (localChunk != null);
            }
            else
            {
                localChunk = new Chunk(paramInt);
            }
        }
        return localChunk;
    }

    private void processPoolLocked()
    {
        while (true)
        {
            SoftReference localSoftReference = (SoftReference)sQueue.poll();
            if (localSoftReference == null)
                return;
            sPool.remove(localSoftReference);
        }
    }

    /** @deprecated */
    public void append(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        while (true)
        {
            if (paramInt2 > 0);
            try
            {
                if (this.mChunks.isEmpty())
                {
                    localChunk = obtainChunk(paramInt2);
                    this.mChunks.addLast(localChunk);
                }
                do
                {
                    int i = Math.min(paramInt2, localChunk.mArray.length - localChunk.mLength);
                    System.arraycopy(paramArrayOfByte, paramInt1, localChunk.mArray, localChunk.mLength, i);
                    localChunk.mLength = (i + localChunk.mLength);
                    paramInt2 -= i;
                    paramInt1 += i;
                    break;
                    localChunk = (Chunk)this.mChunks.getLast();
                }
                while (localChunk.mLength != localChunk.mArray.length);
                Chunk localChunk = obtainChunk(paramInt2);
                this.mChunks.addLast(localChunk);
            }
            finally
            {
            }
        }
    }

    /** @deprecated */
    public void clear()
    {
        try
        {
            Chunk localChunk;
            for (Object localObject2 = getFirstChunk(); localObject2 != null; localObject2 = localChunk)
            {
                ((Chunk)localObject2).release();
                localChunk = getFirstChunk();
            }
            return;
        }
        finally
        {
            localObject1 = finally;
            throw localObject1;
        }
    }

    /** @deprecated */
    public int getByteSize()
    {
        int i = 0;
        try
        {
            ListIterator localListIterator = this.mChunks.listIterator(0);
            while (localListIterator.hasNext())
            {
                int j = ((Chunk)localListIterator.next()).mLength;
                i += j;
            }
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public Chunk getFirstChunk()
    {
        try
        {
            boolean bool = this.mChunks.isEmpty();
            if (bool);
            for (Chunk localChunk = null; ; localChunk = (Chunk)this.mChunks.removeFirst())
                return localChunk;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean isEmpty()
    {
        try
        {
            boolean bool = this.mChunks.isEmpty();
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static class Chunk
    {
        public byte[] mArray;
        public int mLength;

        public Chunk(int paramInt)
        {
            this.mArray = new byte[paramInt];
            this.mLength = 0;
        }

        public void release()
        {
            this.mLength = 0;
            synchronized (ByteArrayBuilder.sPool)
            {
                ByteArrayBuilder.sPool.offer(new SoftReference(this, ByteArrayBuilder.sQueue));
                ByteArrayBuilder.sPool.notifyAll();
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ByteArrayBuilder
 * JD-Core Version:        0.6.2
 */