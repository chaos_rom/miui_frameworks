package android.webkit;

import android.graphics.Point;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class ViewStateSerializer
{
    static final int VERSION = 1;
    private static final int WORKING_STREAM_STORAGE = 16384;

    static WebViewCore.DrawData deserializeViewState(InputStream paramInputStream)
        throws IOException
    {
        DataInputStream localDataInputStream = new DataInputStream(paramInputStream);
        int i = localDataInputStream.readInt();
        if (i > 1)
            throw new IOException("Unexpected version: " + i);
        int j = localDataInputStream.readInt();
        int k = localDataInputStream.readInt();
        int m = nativeDeserializeViewState(i, localDataInputStream, new byte[16384]);
        WebViewCore.DrawData localDrawData = new WebViewCore.DrawData();
        localDrawData.mViewState = new WebViewCore.ViewState();
        localDrawData.mContentSize = new Point(j, k);
        localDrawData.mBaseLayer = m;
        paramInputStream.close();
        return localDrawData;
    }

    private static native int nativeDeserializeViewState(int paramInt, InputStream paramInputStream, byte[] paramArrayOfByte);

    private static native boolean nativeSerializeViewState(int paramInt, OutputStream paramOutputStream, byte[] paramArrayOfByte);

    static boolean serializeViewState(OutputStream paramOutputStream, WebViewCore.DrawData paramDrawData)
        throws IOException
    {
        int i = paramDrawData.mBaseLayer;
        if (i == 0);
        DataOutputStream localDataOutputStream;
        for (boolean bool = false; ; bool = nativeSerializeViewState(i, localDataOutputStream, new byte[16384]))
        {
            return bool;
            localDataOutputStream = new DataOutputStream(paramOutputStream);
            localDataOutputStream.writeInt(1);
            localDataOutputStream.writeInt(paramDrawData.mContentSize.x);
            localDataOutputStream.writeInt(paramDrawData.mContentSize.y);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ViewStateSerializer
 * JD-Core Version:        0.6.2
 */