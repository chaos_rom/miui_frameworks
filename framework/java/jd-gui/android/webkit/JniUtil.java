package android.webkit;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.util.Log;
import java.io.File;
import java.io.InputStream;

class JniUtil
{
    private static final String ANDROID_CONTENT = "content:";
    private static final String LOGTAG = "webkit";
    private static String sCacheDirectory;
    private static Context sContext;
    private static String sDatabaseDirectory;

    static
    {
        System.loadLibrary("webcore");
        System.loadLibrary("chromium_net");
    }

    private static boolean canSatisfyMemoryAllocation(long paramLong)
    {
        checkInitialized();
        ActivityManager localActivityManager = (ActivityManager)sContext.getSystemService("activity");
        ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
        localActivityManager.getMemoryInfo(localMemoryInfo);
        long l = localMemoryInfo.availMem - localMemoryInfo.threshold;
        if ((!localMemoryInfo.lowMemory) && (paramLong < l));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static void checkInitialized()
    {
        if (sContext == null)
            throw new IllegalStateException("Call CookieSyncManager::createInstance() or create a webview before using this class");
    }

    /** @deprecated */
    private static long contentUrlSize(String paramString)
    {
        try
        {
            boolean bool = paramString.startsWith("content:");
            if (bool);
            while (true)
            {
                try
                {
                    int i = paramString.lastIndexOf('?');
                    if (i != -1)
                        paramString = paramString.substring(0, i);
                    Uri localUri = Uri.parse(paramString);
                    InputStream localInputStream = sContext.getContentResolver().openInputStream(localUri);
                    byte[] arrayOfByte = new byte[1024];
                    l = 0L;
                    try
                    {
                        int j = localInputStream.read(arrayOfByte);
                        if (j != -1)
                        {
                            l += j;
                            continue;
                        }
                        localInputStream.close();
                        return l;
                    }
                    finally
                    {
                        localObject2 = finally;
                        localInputStream.close();
                        throw localObject2;
                    }
                }
                catch (Exception localException)
                {
                    Log.e("webkit", "Exception: " + paramString);
                    l = 0L;
                    continue;
                }
                long l = 0L;
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private static InputStream contentUrlStream(String paramString)
    {
        Object localObject1 = null;
        try
        {
            boolean bool = paramString.startsWith("content:");
            if (bool);
            try
            {
                int i = paramString.lastIndexOf('?');
                if (i != -1)
                    paramString = paramString.substring(0, i);
                Uri localUri = Uri.parse(paramString);
                InputStream localInputStream = sContext.getContentResolver().openInputStream(localUri);
                localObject1 = localInputStream;
                return localObject1;
            }
            catch (Exception localException)
            {
                while (true)
                    Log.e("webkit", "Exception: " + paramString);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private static String getAutofillQueryUrl()
    {
        try
        {
            checkInitialized();
            String str = Settings.Secure.getString(sContext.getContentResolver(), "web_autofill_query_url");
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private static String getCacheDirectory()
    {
        try
        {
            checkInitialized();
            File localFile;
            if (sCacheDirectory == null)
            {
                localFile = sContext.getCacheDir();
                if (localFile != null)
                    break label37;
            }
            label37: for (sCacheDirectory = ""; ; sCacheDirectory = localFile.getAbsolutePath())
            {
                String str = sCacheDirectory;
                return str;
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    protected static Context getContext()
    {
        try
        {
            Context localContext = sContext;
            return localContext;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private static String getDatabaseDirectory()
    {
        try
        {
            checkInitialized();
            if (sDatabaseDirectory == null)
                sDatabaseDirectory = sContext.getDatabasePath("dummy").getParent();
            String str = sDatabaseDirectory;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private static String getPackageName()
    {
        try
        {
            checkInitialized();
            String str = sContext.getPackageName();
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    protected static void setContext(Context paramContext)
    {
        try
        {
            Context localContext = sContext;
            if (localContext != null);
            while (true)
            {
                return;
                sContext = paramContext.getApplicationContext();
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.JniUtil
 * JD-Core Version:        0.6.2
 */