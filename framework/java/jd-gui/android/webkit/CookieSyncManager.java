package android.webkit;

import android.content.Context;

public final class CookieSyncManager extends WebSyncManager
{
    private static CookieSyncManager sRef;

    private CookieSyncManager(Context paramContext)
    {
        super(paramContext, "CookieSyncManager");
    }

    private static void checkInstanceIsCreated()
    {
        if (sRef == null)
            throw new IllegalStateException("CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()");
    }

    /** @deprecated */
    public static CookieSyncManager createInstance(Context paramContext)
    {
        if (paramContext == null)
            try
            {
                throw new IllegalArgumentException("Invalid context argument");
            }
            finally
            {
            }
        JniUtil.setContext(paramContext);
        Context localContext = paramContext.getApplicationContext();
        if (sRef == null)
            sRef = new CookieSyncManager(localContext);
        CookieSyncManager localCookieSyncManager = sRef;
        return localCookieSyncManager;
    }

    /** @deprecated */
    public static CookieSyncManager getInstance()
    {
        try
        {
            checkInstanceIsCreated();
            CookieSyncManager localCookieSyncManager = sRef;
            return localCookieSyncManager;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void syncFromRamToFlash()
    {
        CookieManager localCookieManager = CookieManager.getInstance();
        if (!localCookieManager.acceptCookie());
        while (true)
        {
            return;
            localCookieManager.flushCookieStore();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.CookieSyncManager
 * JD-Core Version:        0.6.2
 */