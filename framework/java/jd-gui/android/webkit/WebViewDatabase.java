package android.webkit;

import android.content.Context;

public class WebViewDatabase
{
    protected static final String LOGTAG = "webviewdatabase";

    /** @deprecated */
    public static WebViewDatabase getInstance(Context paramContext)
    {
        try
        {
            WebViewDatabase localWebViewDatabase = WebViewFactory.getProvider().getWebViewDatabase(paramContext);
            return localWebViewDatabase;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void clearFormData()
    {
        throw new MustOverrideException();
    }

    public void clearHttpAuthUsernamePassword()
    {
        throw new MustOverrideException();
    }

    public void clearUsernamePassword()
    {
        throw new MustOverrideException();
    }

    public boolean hasFormData()
    {
        throw new MustOverrideException();
    }

    public boolean hasHttpAuthUsernamePassword()
    {
        throw new MustOverrideException();
    }

    public boolean hasUsernamePassword()
    {
        throw new MustOverrideException();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewDatabase
 * JD-Core Version:        0.6.2
 */