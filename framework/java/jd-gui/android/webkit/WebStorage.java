package android.webkit;

import java.util.Map;

public class WebStorage
{
    public static WebStorage getInstance()
    {
        return WebViewFactory.getProvider().getWebStorage();
    }

    public void deleteAllData()
    {
    }

    public void deleteOrigin(String paramString)
    {
    }

    public void getOrigins(ValueCallback<Map> paramValueCallback)
    {
    }

    public void getQuotaForOrigin(String paramString, ValueCallback<Long> paramValueCallback)
    {
    }

    public void getUsageForOrigin(String paramString, ValueCallback<Long> paramValueCallback)
    {
    }

    public void setQuotaForOrigin(String paramString, long paramLong)
    {
    }

    public static class Origin
    {
        private String mOrigin = null;
        private long mQuota = 0L;
        private long mUsage = 0L;

        protected Origin(String paramString)
        {
            this.mOrigin = paramString;
        }

        protected Origin(String paramString, long paramLong)
        {
            this.mOrigin = paramString;
            this.mQuota = paramLong;
        }

        protected Origin(String paramString, long paramLong1, long paramLong2)
        {
            this.mOrigin = paramString;
            this.mQuota = paramLong1;
            this.mUsage = paramLong2;
        }

        public String getOrigin()
        {
            return this.mOrigin;
        }

        public long getQuota()
        {
            return this.mQuota;
        }

        public long getUsage()
        {
            return this.mUsage;
        }
    }

    public static abstract interface QuotaUpdater
    {
        public abstract void updateQuota(long paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebStorage
 * JD-Core Version:        0.6.2
 */