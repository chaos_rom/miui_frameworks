package android.webkit;

import android.os.Handler;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import org.apache.harmony.xnet.provider.jsse.NativeCrypto;
import org.apache.harmony.xnet.provider.jsse.OpenSSLDSAPrivateKey;
import org.apache.harmony.xnet.provider.jsse.OpenSSLRSAPrivateKey;

public final class ClientCertRequestHandler extends Handler
{
    private final BrowserFrame mBrowserFrame;
    private final int mHandle;
    private final String mHostAndPort;
    private final SslClientCertLookupTable mTable;

    ClientCertRequestHandler(BrowserFrame paramBrowserFrame, int paramInt, String paramString, SslClientCertLookupTable paramSslClientCertLookupTable)
    {
        this.mBrowserFrame = paramBrowserFrame;
        this.mHandle = paramInt;
        this.mHostAndPort = paramString;
        this.mTable = paramSslClientCertLookupTable;
    }

    private void setSslClientCertFromCtx(final int paramInt, final byte[][] paramArrayOfByte)
    {
        post(new Runnable()
        {
            public void run()
            {
                ClientCertRequestHandler.this.mBrowserFrame.nativeSslClientCert(ClientCertRequestHandler.this.mHandle, paramInt, paramArrayOfByte);
            }
        });
    }

    private void setSslClientCertFromPKCS8(final byte[] paramArrayOfByte, final byte[][] paramArrayOfByte1)
    {
        post(new Runnable()
        {
            public void run()
            {
                ClientCertRequestHandler.this.mBrowserFrame.nativeSslClientCert(ClientCertRequestHandler.this.mHandle, paramArrayOfByte, paramArrayOfByte1);
            }
        });
    }

    public void cancel()
    {
        this.mTable.Deny(this.mHostAndPort);
        post(new Runnable()
        {
            public void run()
            {
                ClientCertRequestHandler.this.mBrowserFrame.nativeSslClientCert(ClientCertRequestHandler.this.mHandle, 0, (byte[][])null);
            }
        });
    }

    public void ignore()
    {
        post(new Runnable()
        {
            public void run()
            {
                ClientCertRequestHandler.this.mBrowserFrame.nativeSslClientCert(ClientCertRequestHandler.this.mHandle, 0, (byte[][])null);
            }
        });
    }

    public void proceed(PrivateKey paramPrivateKey, X509Certificate[] paramArrayOfX509Certificate)
    {
        byte[][] arrayOfByte;
        try
        {
            arrayOfByte = NativeCrypto.encodeCertificates(paramArrayOfX509Certificate);
            this.mTable.Allow(this.mHostAndPort, paramPrivateKey, arrayOfByte);
            if ((paramPrivateKey instanceof OpenSSLRSAPrivateKey))
                setSslClientCertFromCtx(((OpenSSLRSAPrivateKey)paramPrivateKey).getPkeyContext(), arrayOfByte);
            else if ((paramPrivateKey instanceof OpenSSLDSAPrivateKey))
                setSslClientCertFromCtx(((OpenSSLDSAPrivateKey)paramPrivateKey).getPkeyContext(), arrayOfByte);
        }
        catch (CertificateEncodingException localCertificateEncodingException)
        {
            post(new Runnable()
            {
                public void run()
                {
                    ClientCertRequestHandler.this.mBrowserFrame.nativeSslClientCert(ClientCertRequestHandler.this.mHandle, 0, (byte[][])null);
                }
            });
        }
        setSslClientCertFromPKCS8(paramPrivateKey.getEncoded(), arrayOfByte);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ClientCertRequestHandler
 * JD-Core Version:        0.6.2
 */