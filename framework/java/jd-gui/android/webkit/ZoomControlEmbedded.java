package android.webkit;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;
import android.widget.ZoomButtonsController;
import android.widget.ZoomButtonsController.OnZoomListener;

class ZoomControlEmbedded
    implements ZoomControlBase
{
    private final WebViewClassic mWebView;
    private ZoomButtonsController mZoomButtonsController;
    private final ZoomManager mZoomManager;

    public ZoomControlEmbedded(ZoomManager paramZoomManager, WebViewClassic paramWebViewClassic)
    {
        this.mZoomManager = paramZoomManager;
        this.mWebView = paramWebViewClassic;
    }

    private ZoomButtonsController getControls()
    {
        if (this.mZoomButtonsController == null)
        {
            this.mZoomButtonsController = new ZoomButtonsController(this.mWebView.getWebView());
            this.mZoomButtonsController.setOnZoomListener(new ZoomListener(null));
            ViewGroup.LayoutParams localLayoutParams = this.mZoomButtonsController.getZoomControls().getLayoutParams();
            if ((localLayoutParams instanceof FrameLayout.LayoutParams))
                ((FrameLayout.LayoutParams)localLayoutParams).gravity = 5;
        }
        return this.mZoomButtonsController;
    }

    public void hide()
    {
        if (this.mZoomButtonsController != null)
            this.mZoomButtonsController.setVisible(false);
    }

    public boolean isVisible()
    {
        if ((this.mZoomButtonsController != null) && (this.mZoomButtonsController.isVisible()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void show()
    {
        if ((!getControls().isVisible()) && (!this.mZoomManager.isZoomScaleFixed()))
        {
            this.mZoomButtonsController.setVisible(true);
            if (this.mZoomManager.isDoubleTapEnabled())
            {
                WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
                int i = localWebSettingsClassic.getDoubleTapToastCount();
                if ((this.mZoomManager.isInZoomOverview()) && (i > 0))
                {
                    localWebSettingsClassic.setDoubleTapToastCount(i - 1);
                    Toast.makeText(this.mWebView.getContext(), 17040193, 1).show();
                }
            }
        }
    }

    public void update()
    {
        if (this.mZoomButtonsController == null);
        while (true)
        {
            return;
            boolean bool1 = this.mZoomManager.canZoomIn();
            if ((this.mZoomManager.canZoomOut()) && (!this.mZoomManager.isInZoomOverview()));
            for (boolean bool2 = true; ; bool2 = false)
            {
                if ((bool1) || (bool2))
                    break label66;
                this.mZoomButtonsController.getZoomControls().setVisibility(8);
                break;
            }
            label66: this.mZoomButtonsController.setZoomInEnabled(bool1);
            this.mZoomButtonsController.setZoomOutEnabled(bool2);
        }
    }

    private class ZoomListener
        implements ZoomButtonsController.OnZoomListener
    {
        private ZoomListener()
        {
        }

        public void onVisibilityChanged(boolean paramBoolean)
        {
            if (paramBoolean)
            {
                ZoomControlEmbedded.this.mWebView.switchOutDrawHistory();
                ZoomControlEmbedded.this.mZoomButtonsController.getZoomControls().setVisibility(0);
                ZoomControlEmbedded.this.update();
            }
        }

        public void onZoom(boolean paramBoolean)
        {
            if (paramBoolean)
                ZoomControlEmbedded.this.mWebView.zoomIn();
            while (true)
            {
                ZoomControlEmbedded.this.update();
                return;
                ZoomControlEmbedded.this.mWebView.zoomOut();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ZoomControlEmbedded
 * JD-Core Version:        0.6.2
 */