package android.webkit;

import android.content.Context;
import android.content.res.Resources;
import java.util.Calendar;

public class DateSorter
{
    public static final int DAY_COUNT = 5;
    private static final String LOGTAG = "webkit";
    private static final int NUM_DAYS_AGO = 7;
    private long[] mBins = new long[4];
    private String[] mLabels = new String[5];

    public DateSorter(Context paramContext)
    {
        Resources localResources = paramContext.getResources();
        Calendar localCalendar = Calendar.getInstance();
        beginningOfDay(localCalendar);
        this.mBins[0] = localCalendar.getTimeInMillis();
        localCalendar.add(6, -1);
        this.mBins[1] = localCalendar.getTimeInMillis();
        localCalendar.add(6, -6);
        this.mBins[2] = localCalendar.getTimeInMillis();
        localCalendar.add(6, 7);
        localCalendar.add(2, -1);
        this.mBins[3] = localCalendar.getTimeInMillis();
        this.mLabels[0] = paramContext.getText(17039484).toString();
        this.mLabels[1] = paramContext.getText(17039483).toString();
        String str = localResources.getQuantityString(18022403, 7);
        String[] arrayOfString = this.mLabels;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(7);
        arrayOfString[2] = String.format(str, arrayOfObject);
        this.mLabels[3] = paramContext.getString(17040293);
        this.mLabels[4] = paramContext.getString(17040294);
    }

    private void beginningOfDay(Calendar paramCalendar)
    {
        paramCalendar.set(11, 0);
        paramCalendar.set(12, 0);
        paramCalendar.set(13, 0);
        paramCalendar.set(14, 0);
    }

    public long getBoundary(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 4))
            paramInt = 0;
        if (paramInt == 4);
        for (long l = -9223372036854775808L; ; l = this.mBins[paramInt])
            return l;
    }

    public int getIndex(long paramLong)
    {
        int i = 0;
        if (i < 4)
            if (paramLong <= this.mBins[i]);
        while (true)
        {
            return i;
            i++;
            break;
            i = 4;
        }
    }

    public String getLabel(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= 5));
        for (String str = ""; ; str = this.mLabels[paramInt])
            return str;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.DateSorter
 * JD-Core Version:        0.6.2
 */