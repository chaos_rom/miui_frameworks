package android.webkit;

import android.content.Context;
import android.net.http.Headers;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

@Deprecated
public final class CacheManager
{
    static final String HEADER_KEY_IFMODIFIEDSINCE = "if-modified-since";
    static final String HEADER_KEY_IFNONEMATCH = "if-none-match";
    private static final String LOGTAG = "cache";
    private static File mBaseDir;

    static
    {
        if (!CacheManager.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    @Deprecated
    public static boolean cacheDisabled()
    {
        return false;
    }

    static CacheResult createCacheFile(String paramString1, int paramInt, Headers paramHeaders, String paramString2, boolean paramBoolean)
    {
        return null;
    }

    @Deprecated
    public static boolean endCacheTransaction()
    {
        return false;
    }

    static CacheResult getCacheFile(String paramString, long paramLong, Map<String, String> paramMap)
    {
        CacheResult localCacheResult = nativeGetCacheResult(paramString);
        if (localCacheResult == null)
            localCacheResult = null;
        while (true)
        {
            return localCacheResult;
            File localFile = new File(mBaseDir, localCacheResult.localPath);
            try
            {
                localCacheResult.inStream = new FileInputStream(localFile);
                if ((paramMap == null) || (localCacheResult.expires < 0L) || (localCacheResult.expires > System.currentTimeMillis()))
                    continue;
                if ((localCacheResult.lastModified != null) || (localCacheResult.etag != null))
                    break label130;
                localCacheResult = null;
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                Log.v("cache", "getCacheFile(): Failed to open file: " + localFileNotFoundException);
                localCacheResult = null;
            }
            continue;
            label130: if (localCacheResult.etag != null)
                paramMap.put("if-none-match", localCacheResult.etag);
            if (localCacheResult.lastModified != null)
                paramMap.put("if-modified-since", localCacheResult.lastModified);
        }
    }

    @Deprecated
    public static CacheResult getCacheFile(String paramString, Map<String, String> paramMap)
    {
        return getCacheFile(paramString, 0L, paramMap);
    }

    @Deprecated
    public static File getCacheFileBaseDir()
    {
        return mBaseDir;
    }

    static void init(Context paramContext)
    {
        mBaseDir = new File(paramContext.getCacheDir(), "webviewCacheChromiumStaging");
        if (!mBaseDir.exists())
            mBaseDir.mkdirs();
    }

    private static native CacheResult nativeGetCacheResult(String paramString);

    static boolean removeAllCacheFiles()
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    String[] arrayOfString = CacheManager.mBaseDir.list();
                    if (arrayOfString != null)
                        for (int i = 0; i < arrayOfString.length; i++)
                        {
                            File localFile = new File(CacheManager.mBaseDir, arrayOfString[i]);
                            if (!localFile.delete())
                                Log.e("cache", localFile.getPath() + " delete failed.");
                        }
                }
                catch (SecurityException localSecurityException)
                {
                }
            }
        }).start();
        return true;
    }

    static void saveCacheFile(String paramString, long paramLong, CacheResult paramCacheResult)
    {
        try
        {
            paramCacheResult.outStream.close();
            if (!$assertionsDisabled)
                throw new AssertionError();
        }
        catch (IOException localIOException)
        {
        }
    }

    @Deprecated
    public static void saveCacheFile(String paramString, CacheResult paramCacheResult)
    {
        saveCacheFile(paramString, 0L, paramCacheResult);
    }

    @Deprecated
    public static boolean startCacheTransaction()
    {
        return false;
    }

    @Deprecated
    public static class CacheResult
    {
        long contentLength;
        String contentdisposition;
        String crossDomain;
        String encoding;
        String etag;
        long expires;
        String expiresString;
        int httpStatusCode;
        InputStream inStream;
        String lastModified;
        String localPath;
        String location;
        String mimeType;
        File outFile;
        OutputStream outStream;

        public String getContentDisposition()
        {
            return this.contentdisposition;
        }

        public long getContentLength()
        {
            return this.contentLength;
        }

        public String getETag()
        {
            return this.etag;
        }

        public String getEncoding()
        {
            return this.encoding;
        }

        public long getExpires()
        {
            return this.expires;
        }

        public String getExpiresString()
        {
            return this.expiresString;
        }

        public int getHttpStatusCode()
        {
            return this.httpStatusCode;
        }

        public InputStream getInputStream()
        {
            return this.inStream;
        }

        public String getLastModified()
        {
            return this.lastModified;
        }

        public String getLocalPath()
        {
            return this.localPath;
        }

        public String getLocation()
        {
            return this.location;
        }

        public String getMimeType()
        {
            return this.mimeType;
        }

        public OutputStream getOutputStream()
        {
            return this.outStream;
        }

        public void setContentLength(long paramLong)
        {
            this.contentLength = paramLong;
        }

        public void setEncoding(String paramString)
        {
            this.encoding = paramString;
        }

        public void setInputStream(InputStream paramInputStream)
        {
            this.inStream = paramInputStream;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.CacheManager
 * JD-Core Version:        0.6.2
 */