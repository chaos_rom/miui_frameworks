package android.webkit;

import java.util.Set;

public class GeolocationPermissions
{
    public static GeolocationPermissions getInstance()
    {
        return WebViewFactory.getProvider().getGeolocationPermissions();
    }

    public void allow(String paramString)
    {
    }

    public void clear(String paramString)
    {
    }

    public void clearAll()
    {
    }

    public void getAllowed(String paramString, ValueCallback<Boolean> paramValueCallback)
    {
    }

    public void getOrigins(ValueCallback<Set<String>> paramValueCallback)
    {
    }

    public static abstract interface Callback
    {
        public abstract void invoke(String paramString, boolean paramBoolean1, boolean paramBoolean2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.GeolocationPermissions
 * JD-Core Version:        0.6.2
 */