package android.webkit;

import java.util.Map;

@Deprecated
public abstract interface UrlInterceptHandler
{
    @Deprecated
    public abstract PluginData getPluginData(String paramString, Map<String, String> paramMap);

    @Deprecated
    public abstract CacheManager.CacheResult service(String paramString, Map<String, String> paramMap);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.UrlInterceptHandler
 * JD-Core Version:        0.6.2
 */