package android.webkit;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsoluteLayout.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Filter;
import android.widget.Filter.FilterListener;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;

class AutoCompletePopup
    implements AdapterView.OnItemClickListener, Filter.FilterListener, PopupWindow.OnDismissListener
{
    private static final int AUTOFILL_FORM = 100;
    private ListAdapter mAdapter;
    private View mAnchor;
    private Filter mFilter;
    private Handler mHandler;
    private WebViewClassic.WebViewInputConnection mInputConnection;
    private boolean mIsAutoFillProfileSet;
    private ListPopupWindow mPopup;
    private int mQueryId;
    private CharSequence mText;
    private WebViewClassic mWebView;

    public AutoCompletePopup(WebViewClassic paramWebViewClassic, WebViewClassic.WebViewInputConnection paramWebViewInputConnection)
    {
        this.mInputConnection = paramWebViewInputConnection;
        this.mWebView = paramWebViewClassic;
        this.mHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                switch (paramAnonymousMessage.what)
                {
                default:
                case 100:
                }
                while (true)
                {
                    return;
                    AutoCompletePopup.this.mWebView.autoFillForm(AutoCompletePopup.this.mQueryId);
                }
            }
        };
    }

    private void ensurePopup()
    {
        if (this.mPopup == null)
        {
            this.mPopup = new ListPopupWindow(this.mWebView.getContext());
            this.mAnchor = new AnchorView(this.mWebView.getContext());
            this.mWebView.getWebView().addView(this.mAnchor);
            this.mPopup.setOnItemClickListener(this);
            this.mPopup.setAnchorView(this.mAnchor);
            this.mPopup.setPromptPosition(1);
        }
        while (true)
        {
            return;
            if (this.mWebView.getWebView().indexOfChild(this.mAnchor) < 0)
                this.mWebView.getWebView().addView(this.mAnchor);
        }
    }

    private void pushTextToInputConnection()
    {
        Editable localEditable = this.mInputConnection.getEditable();
        this.mInputConnection.setSelection(0, localEditable.length());
        this.mInputConnection.replaceSelection(this.mText);
        this.mInputConnection.setSelection(this.mText.length(), this.mText.length());
    }

    public void clearAdapter()
    {
        this.mAdapter = null;
        this.mFilter = null;
        if (this.mPopup != null)
        {
            this.mPopup.dismiss();
            this.mPopup.setAdapter(null);
        }
    }

    public void onDismiss()
    {
        this.mWebView.getWebView().removeView(this.mAnchor);
    }

    public void onFilterComplete(int paramInt)
    {
        ensurePopup();
        int i;
        if ((paramInt > 0) && ((this.mInputConnection.getIsAutoFillable()) || (this.mText.length() > 0)))
        {
            i = 1;
            if (i == 0)
                break label78;
            if (!this.mPopup.isShowing())
                this.mPopup.setInputMethodMode(1);
            this.mPopup.show();
            this.mPopup.getListView().setOverScrollMode(0);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label78: this.mPopup.dismiss();
        }
    }

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        if (this.mPopup == null)
            return;
        if ((paramLong == 0L) && (paramInt == 0) && (this.mInputConnection.getIsAutoFillable()))
        {
            this.mText = "";
            pushTextToInputConnection();
            if (this.mIsAutoFillProfileSet)
                this.mWebView.autoFillForm(this.mQueryId);
        }
        label149: 
        while (true)
        {
            this.mPopup.dismiss();
            break;
            WebChromeClient localWebChromeClient = this.mWebView.getWebChromeClient();
            if (localWebChromeClient != null)
            {
                localWebChromeClient.setupAutoFill(this.mHandler.obtainMessage(100));
                continue;
                if (paramInt < 0);
                for (Object localObject = this.mPopup.getSelectedItem(); ; localObject = this.mAdapter.getItem(paramInt))
                {
                    if (localObject == null)
                        break label149;
                    setText(this.mFilter.convertResultToString(localObject));
                    pushTextToInputConnection();
                    break;
                }
            }
        }
    }

    public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (this.mPopup == null);
        while (true)
        {
            return bool;
            if ((paramInt == 4) && (this.mPopup.isShowing()))
            {
                if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getRepeatCount() == 0))
                {
                    KeyEvent.DispatcherState localDispatcherState2 = this.mAnchor.getKeyDispatcherState();
                    if (localDispatcherState2 != null)
                        localDispatcherState2.startTracking(paramKeyEvent, this);
                    bool = true;
                }
                else if (paramKeyEvent.getAction() == 1)
                {
                    KeyEvent.DispatcherState localDispatcherState1 = this.mAnchor.getKeyDispatcherState();
                    if (localDispatcherState1 != null)
                        localDispatcherState1.handleUpEvent(paramKeyEvent);
                    if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                    {
                        this.mPopup.dismiss();
                        bool = true;
                    }
                }
            }
            else if (this.mPopup.isShowing())
                bool = this.mPopup.onKeyPreIme(paramInt, paramKeyEvent);
        }
    }

    public void resetRect()
    {
        ensurePopup();
        int i = this.mWebView.contentToViewX(this.mWebView.mEditTextContentBounds.left);
        int j = this.mWebView.contentToViewX(this.mWebView.mEditTextContentBounds.right) - i;
        this.mPopup.setWidth(j);
        int k = this.mWebView.contentToViewY(this.mWebView.mEditTextContentBounds.bottom);
        int m = this.mWebView.contentToViewY(this.mWebView.mEditTextContentBounds.top);
        int n = k - m;
        AbsoluteLayout.LayoutParams localLayoutParams = (AbsoluteLayout.LayoutParams)this.mAnchor.getLayoutParams();
        int i1 = 0;
        if (localLayoutParams == null)
            localLayoutParams = new AbsoluteLayout.LayoutParams(j, n, i, m);
        while (true)
        {
            if (i1 != 0)
                this.mAnchor.setLayoutParams(localLayoutParams);
            if (this.mPopup.isShowing())
                this.mPopup.show();
            return;
            if ((localLayoutParams.x != i) || (localLayoutParams.y != m) || (localLayoutParams.width != j) || (localLayoutParams.height != n))
            {
                i1 = 1;
                localLayoutParams.x = i;
                localLayoutParams.y = m;
                localLayoutParams.width = j;
                localLayoutParams.height = n;
            }
        }
    }

    public <T extends ListAdapter,    extends Filterable> void setAdapter(T paramT)
    {
        ensurePopup();
        this.mPopup.setAdapter(paramT);
        this.mAdapter = paramT;
        if (paramT != null)
        {
            this.mFilter = ((Filterable)paramT).getFilter();
            this.mFilter.filter(this.mText, this);
        }
        while (true)
        {
            resetRect();
            return;
            this.mFilter = null;
        }
    }

    public void setAutoFillQueryId(int paramInt)
    {
        this.mQueryId = paramInt;
    }

    public void setIsAutoFillProfileSet(boolean paramBoolean)
    {
        this.mIsAutoFillProfileSet = paramBoolean;
    }

    public void setText(CharSequence paramCharSequence)
    {
        this.mText = paramCharSequence;
        if (this.mFilter != null)
            this.mFilter.filter(paramCharSequence, this);
    }

    private static class AnchorView extends View
    {
        AnchorView(Context paramContext)
        {
            super();
            setFocusable(false);
            setVisibility(4);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.AutoCompletePopup
 * JD-Core Version:        0.6.2
 */