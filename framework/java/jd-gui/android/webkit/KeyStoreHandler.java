package android.webkit;

import android.os.Handler;

class KeyStoreHandler extends Handler
{
    private static final String LOGTAG = "KeyStoreHandler";
    private final ByteArrayBuilder mDataBuilder = new ByteArrayBuilder();
    private String mMimeType;

    public KeyStoreHandler(String paramString)
    {
        this.mMimeType = paramString;
    }

    public void didReceiveData(byte[] paramArrayOfByte, int paramInt)
    {
        synchronized (this.mDataBuilder)
        {
            this.mDataBuilder.append(paramArrayOfByte, 0, paramInt);
            return;
        }
    }

    // ERROR //
    public void installCert(android.content.Context paramContext)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 23	android/webkit/KeyStoreHandler:mMimeType	Ljava/lang/String;
        //     4: invokestatic 37	android/webkit/CertTool:getCertType	(Ljava/lang/String;)Ljava/lang/String;
        //     7: astore_2
        //     8: aload_2
        //     9: ifnonnull +4 -> 13
        //     12: return
        //     13: aload_0
        //     14: getfield 21	android/webkit/KeyStoreHandler:mDataBuilder	Landroid/webkit/ByteArrayBuilder;
        //     17: astore_3
        //     18: aload_3
        //     19: monitorenter
        //     20: aload_0
        //     21: getfield 21	android/webkit/KeyStoreHandler:mDataBuilder	Landroid/webkit/ByteArrayBuilder;
        //     24: invokevirtual 41	android/webkit/ByteArrayBuilder:getByteSize	()I
        //     27: newarray byte
        //     29: astore 5
        //     31: iconst_0
        //     32: istore 6
        //     34: aload_0
        //     35: getfield 21	android/webkit/KeyStoreHandler:mDataBuilder	Landroid/webkit/ByteArrayBuilder;
        //     38: invokevirtual 45	android/webkit/ByteArrayBuilder:getFirstChunk	()Landroid/webkit/ByteArrayBuilder$Chunk;
        //     41: astore 7
        //     43: aload 7
        //     45: ifnonnull +22 -> 67
        //     48: aload_1
        //     49: aload_2
        //     50: aload 5
        //     52: invokestatic 49	android/webkit/CertTool:addCertificate	(Landroid/content/Context;Ljava/lang/String;[B)V
        //     55: aload_3
        //     56: monitorexit
        //     57: goto -45 -> 12
        //     60: astore 4
        //     62: aload_3
        //     63: monitorexit
        //     64: aload 4
        //     66: athrow
        //     67: aload 7
        //     69: getfield 55	android/webkit/ByteArrayBuilder$Chunk:mLength	I
        //     72: ifeq +31 -> 103
        //     75: aload 7
        //     77: getfield 59	android/webkit/ByteArrayBuilder$Chunk:mArray	[B
        //     80: iconst_0
        //     81: aload 5
        //     83: iload 6
        //     85: aload 7
        //     87: getfield 55	android/webkit/ByteArrayBuilder$Chunk:mLength	I
        //     90: invokestatic 65	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     93: iload 6
        //     95: aload 7
        //     97: getfield 55	android/webkit/ByteArrayBuilder$Chunk:mLength	I
        //     100: iadd
        //     101: istore 6
        //     103: aload 7
        //     105: invokevirtual 68	android/webkit/ByteArrayBuilder$Chunk:release	()V
        //     108: goto -74 -> 34
        //
        // Exception table:
        //     from	to	target	type
        //     20	64	60	finally
        //     67	108	60	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.KeyStoreHandler
 * JD-Core Version:        0.6.2
 */