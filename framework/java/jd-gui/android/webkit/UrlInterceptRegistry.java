package android.webkit;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;

@Deprecated
public final class UrlInterceptRegistry
{
    private static final String LOGTAG = "intercept";
    private static boolean mDisabled = false;
    private static LinkedList mHandlerList;

    /** @deprecated */
    private static LinkedList getHandlers()
    {
        try
        {
            if (mHandlerList == null)
                mHandlerList = new LinkedList();
            LinkedList localLinkedList = mHandlerList;
            return localLinkedList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public static PluginData getPluginData(String paramString, Map<String, String> paramMap)
    {
        try
        {
            boolean bool = urlInterceptDisabled();
            Object localObject2;
            if (bool)
                localObject2 = null;
            while (true)
            {
                return localObject2;
                ListIterator localListIterator = getHandlers().listIterator();
                while (true)
                    if (localListIterator.hasNext())
                    {
                        PluginData localPluginData = ((UrlInterceptHandler)localListIterator.next()).getPluginData(paramString, paramMap);
                        localObject2 = localPluginData;
                        if (localObject2 != null)
                            break;
                    }
                localObject2 = null;
            }
        }
        finally
        {
        }
    }

    @Deprecated
    public static CacheManager.CacheResult getSurrogate(String paramString, Map<String, String> paramMap)
    {
        try
        {
            boolean bool = urlInterceptDisabled();
            Object localObject2;
            if (bool)
                localObject2 = null;
            while (true)
            {
                return localObject2;
                ListIterator localListIterator = getHandlers().listIterator();
                while (true)
                    if (localListIterator.hasNext())
                    {
                        CacheManager.CacheResult localCacheResult = ((UrlInterceptHandler)localListIterator.next()).service(paramString, paramMap);
                        localObject2 = localCacheResult;
                        if (localObject2 != null)
                            break;
                    }
                localObject2 = null;
            }
        }
        finally
        {
        }
    }

    @Deprecated
    public static boolean registerHandler(UrlInterceptHandler paramUrlInterceptHandler)
    {
        try
        {
            if (!getHandlers().contains(paramUrlInterceptHandler))
            {
                getHandlers().addFirst(paramUrlInterceptHandler);
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    @Deprecated
    public static void setUrlInterceptDisabled(boolean paramBoolean)
    {
        try
        {
            mDisabled = paramBoolean;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public static boolean unregisterHandler(UrlInterceptHandler paramUrlInterceptHandler)
    {
        try
        {
            boolean bool = getHandlers().remove(paramUrlInterceptHandler);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public static boolean urlInterceptDisabled()
    {
        try
        {
            boolean bool = mDisabled;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.UrlInterceptRegistry
 * JD-Core Version:        0.6.2
 */