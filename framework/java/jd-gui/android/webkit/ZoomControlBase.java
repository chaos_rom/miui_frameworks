package android.webkit;

abstract interface ZoomControlBase
{
    public abstract void hide();

    public abstract boolean isVisible();

    public abstract void show();

    public abstract void update();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ZoomControlBase
 * JD-Core Version:        0.6.2
 */