package android.webkit;

import android.util.Log;

class WebViewFactory
{
    private static final boolean DEBUG = false;
    private static final String DEFAULT_WEB_VIEW_FACTORY = "android.webkit.WebViewClassic$Factory";
    private static final String LOGTAG = "WebViewFactory";
    private static WebViewFactoryProvider sProviderInstance;

    private static WebViewFactoryProvider getFactoryByName(String paramString)
    {
        try
        {
            localWebViewFactoryProvider = (WebViewFactoryProvider)Class.forName(paramString).newInstance();
            return localWebViewFactoryProvider;
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            while (true)
            {
                Log.e("WebViewFactory", "error loading " + paramString, localClassNotFoundException);
                WebViewFactoryProvider localWebViewFactoryProvider = null;
            }
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
            while (true)
                Log.e("WebViewFactory", "error loading " + paramString, localIllegalAccessException);
        }
        catch (InstantiationException localInstantiationException)
        {
            while (true)
                Log.e("WebViewFactory", "error loading " + paramString, localInstantiationException);
        }
    }

    /** @deprecated */
    static WebViewFactoryProvider getProvider()
    {
        try
        {
            if (sProviderInstance != null);
            for (WebViewFactoryProvider localWebViewFactoryProvider = sProviderInstance; ; localWebViewFactoryProvider = sProviderInstance)
            {
                return localWebViewFactoryProvider;
                sProviderInstance = getFactoryByName("android.webkit.WebViewClassic$Factory");
                if (sProviderInstance == null)
                    sProviderInstance = new WebViewClassic.Factory();
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewFactory
 * JD-Core Version:        0.6.2
 */