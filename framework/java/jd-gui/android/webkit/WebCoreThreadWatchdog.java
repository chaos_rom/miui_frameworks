package android.webkit;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import java.util.HashSet;
import java.util.Set;

class WebCoreThreadWatchdog
    implements Runnable
{
    private static final int HEARTBEAT_PERIOD = 10000;
    private static final int IS_ALIVE = 100;
    private static final int SUBSEQUENT_TIMEOUT_PERIOD = 15000;
    private static final int TIMED_OUT = 101;
    private static final int TIMEOUT_PERIOD = 30000;
    private static WebCoreThreadWatchdog sInstance;
    private Handler mHandler;
    private boolean mPaused;
    private Handler mWebCoreThreadHandler;
    private Set<WebViewClassic> mWebViews;

    private WebCoreThreadWatchdog(Handler paramHandler)
    {
        this.mWebCoreThreadHandler = paramHandler;
    }

    private void addWebView(WebViewClassic paramWebViewClassic)
    {
        if (this.mWebViews == null)
            this.mWebViews = new HashSet();
        this.mWebViews.add(paramWebViewClassic);
    }

    private void createHandler()
    {
        try
        {
            this.mHandler = new Handler()
            {
                // ERROR //
                public void handleMessage(Message paramAnonymousMessage)
                {
                    // Byte code:
                    //     0: aload_1
                    //     1: getfield 25	android/os/Message:what	I
                    //     4: tableswitch	default:+24 -> 28, 100:+25->29, 101:+117->121
                    //     29: ldc 6
                    //     31: monitorenter
                    //     32: aload_0
                    //     33: getfield 15	android/webkit/WebCoreThreadWatchdog$1:this$0	Landroid/webkit/WebCoreThreadWatchdog;
                    //     36: invokestatic 29	android/webkit/WebCoreThreadWatchdog:access$000	(Landroid/webkit/WebCoreThreadWatchdog;)Z
                    //     39: ifeq +17 -> 56
                    //     42: ldc 6
                    //     44: monitorexit
                    //     45: goto -17 -> 28
                    //     48: astore 7
                    //     50: ldc 6
                    //     52: monitorexit
                    //     53: aload 7
                    //     55: athrow
                    //     56: aload_0
                    //     57: bipush 101
                    //     59: invokevirtual 33	android/webkit/WebCoreThreadWatchdog$1:removeMessages	(I)V
                    //     62: aload_0
                    //     63: aload_0
                    //     64: bipush 101
                    //     66: invokevirtual 37	android/webkit/WebCoreThreadWatchdog$1:obtainMessage	(I)Landroid/os/Message;
                    //     69: ldc2_w 38
                    //     72: invokevirtual 43	android/webkit/WebCoreThreadWatchdog$1:sendMessageDelayed	(Landroid/os/Message;J)Z
                    //     75: pop
                    //     76: aload_0
                    //     77: getfield 15	android/webkit/WebCoreThreadWatchdog$1:this$0	Landroid/webkit/WebCoreThreadWatchdog;
                    //     80: invokestatic 47	android/webkit/WebCoreThreadWatchdog:access$200	(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;
                    //     83: aload_0
                    //     84: getfield 15	android/webkit/WebCoreThreadWatchdog$1:this$0	Landroid/webkit/WebCoreThreadWatchdog;
                    //     87: invokestatic 47	android/webkit/WebCoreThreadWatchdog:access$200	(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;
                    //     90: sipush 197
                    //     93: aload_0
                    //     94: getfield 15	android/webkit/WebCoreThreadWatchdog$1:this$0	Landroid/webkit/WebCoreThreadWatchdog;
                    //     97: invokestatic 50	android/webkit/WebCoreThreadWatchdog:access$100	(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;
                    //     100: bipush 100
                    //     102: invokevirtual 51	android/os/Handler:obtainMessage	(I)Landroid/os/Message;
                    //     105: invokevirtual 54	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
                    //     108: ldc2_w 55
                    //     111: invokevirtual 57	android/os/Handler:sendMessageDelayed	(Landroid/os/Message;J)Z
                    //     114: pop
                    //     115: ldc 6
                    //     117: monitorexit
                    //     118: goto -90 -> 28
                    //     121: iconst_0
                    //     122: istore_2
                    //     123: ldc 6
                    //     125: monitorenter
                    //     126: aload_0
                    //     127: getfield 15	android/webkit/WebCoreThreadWatchdog$1:this$0	Landroid/webkit/WebCoreThreadWatchdog;
                    //     130: invokestatic 61	android/webkit/WebCoreThreadWatchdog:access$300	(Landroid/webkit/WebCoreThreadWatchdog;)Ljava/util/Set;
                    //     133: invokeinterface 67 1 0
                    //     138: astore 4
                    //     140: aload 4
                    //     142: invokeinterface 73 1 0
                    //     147: ifeq +61 -> 208
                    //     150: aload 4
                    //     152: invokeinterface 77 1 0
                    //     157: checkcast 79	android/webkit/WebViewClassic
                    //     160: invokevirtual 83	android/webkit/WebViewClassic:getWebView	()Landroid/webkit/WebView;
                    //     163: astore 6
                    //     165: aload 6
                    //     167: invokevirtual 89	android/webkit/WebView:getWindowToken	()Landroid/os/IBinder;
                    //     170: ifnull -30 -> 140
                    //     173: aload 6
                    //     175: invokevirtual 93	android/webkit/WebView:getViewRootImpl	()Landroid/view/ViewRootImpl;
                    //     178: ifnull -38 -> 140
                    //     181: aload 6
                    //     183: new 95	android/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable
                    //     186: dup
                    //     187: aload_0
                    //     188: getfield 15	android/webkit/WebCoreThreadWatchdog$1:this$0	Landroid/webkit/WebCoreThreadWatchdog;
                    //     191: aload 6
                    //     193: invokevirtual 99	android/webkit/WebView:getContext	()Landroid/content/Context;
                    //     196: aload_0
                    //     197: invokespecial 102	android/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable:<init>	(Landroid/webkit/WebCoreThreadWatchdog;Landroid/content/Context;Landroid/os/Handler;)V
                    //     200: invokevirtual 106	android/webkit/WebView:post	(Ljava/lang/Runnable;)Z
                    //     203: istore_2
                    //     204: iload_2
                    //     205: ifeq -65 -> 140
                    //     208: iload_2
                    //     209: ifne +17 -> 226
                    //     212: aload_0
                    //     213: aload_0
                    //     214: bipush 101
                    //     216: invokevirtual 37	android/webkit/WebCoreThreadWatchdog$1:obtainMessage	(I)Landroid/os/Message;
                    //     219: ldc2_w 107
                    //     222: invokevirtual 43	android/webkit/WebCoreThreadWatchdog$1:sendMessageDelayed	(Landroid/os/Message;J)Z
                    //     225: pop
                    //     226: ldc 6
                    //     228: monitorexit
                    //     229: goto -201 -> 28
                    //     232: astore_3
                    //     233: ldc 6
                    //     235: monitorexit
                    //     236: aload_3
                    //     237: athrow
                    //
                    // Exception table:
                    //     from	to	target	type
                    //     32	53	48	finally
                    //     56	118	48	finally
                    //     126	236	232	finally
                }
            };
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public static void pause()
    {
        try
        {
            if (sInstance != null)
                sInstance.pauseWatchdog();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void pauseWatchdog()
    {
        this.mPaused = true;
        if (this.mHandler == null);
        while (true)
        {
            return;
            this.mHandler.removeMessages(101);
            this.mHandler.removeMessages(100);
            this.mWebCoreThreadHandler.removeMessages(197);
        }
    }

    /** @deprecated */
    public static void registerWebView(WebViewClassic paramWebViewClassic)
    {
        try
        {
            if (sInstance != null)
                sInstance.addWebView(paramWebViewClassic);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void removeWebView(WebViewClassic paramWebViewClassic)
    {
        this.mWebViews.remove(paramWebViewClassic);
    }

    /** @deprecated */
    public static void resume()
    {
        try
        {
            if (sInstance != null)
                sInstance.resumeWatchdog();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void resumeWatchdog()
    {
        if (!this.mPaused);
        while (true)
        {
            return;
            this.mPaused = false;
            if (this.mHandler != null)
            {
                this.mWebCoreThreadHandler.obtainMessage(197, this.mHandler.obtainMessage(100)).sendToTarget();
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(101), 30000L);
            }
        }
    }

    /** @deprecated */
    public static WebCoreThreadWatchdog start(Handler paramHandler)
    {
        try
        {
            if (sInstance == null)
            {
                sInstance = new WebCoreThreadWatchdog(paramHandler);
                new Thread(sInstance, "WebCoreThreadWatchdog").start();
            }
            WebCoreThreadWatchdog localWebCoreThreadWatchdog = sInstance;
            return localWebCoreThreadWatchdog;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public static void unregisterWebView(WebViewClassic paramWebViewClassic)
    {
        try
        {
            if (sInstance != null)
                sInstance.removeWebView(paramWebViewClassic);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void run()
    {
        Looper.prepare();
        createHandler();
        try
        {
            if (!this.mPaused)
            {
                this.mWebCoreThreadHandler.obtainMessage(197, this.mHandler.obtainMessage(100)).sendToTarget();
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(101), 30000L);
            }
            Looper.loop();
            return;
        }
        finally
        {
        }
    }

    private class PageNotRespondingRunnable
        implements Runnable
    {
        Context mContext;
        private Handler mWatchdogHandler;

        static
        {
            if (!WebCoreThreadWatchdog.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        public PageNotRespondingRunnable(Context paramHandler, Handler arg3)
        {
            this.mContext = paramHandler;
            Object localObject;
            this.mWatchdogHandler = localObject;
        }

        public void run()
        {
            assert (Looper.getMainLooper().getThread() == Thread.currentThread());
            new AlertDialog.Builder(this.mContext).setMessage(17040346).setPositiveButton(17040343, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    Process.killProcess(Process.myPid());
                }
            }).setNegativeButton(17040345, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    WebCoreThreadWatchdog.PageNotRespondingRunnable.this.mWatchdogHandler.sendMessageDelayed(WebCoreThreadWatchdog.PageNotRespondingRunnable.this.mWatchdogHandler.obtainMessage(101), 15000L);
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                public void onCancel(DialogInterface paramAnonymousDialogInterface)
                {
                    WebCoreThreadWatchdog.PageNotRespondingRunnable.this.mWatchdogHandler.sendMessageDelayed(WebCoreThreadWatchdog.PageNotRespondingRunnable.this.mWatchdogHandler.obtainMessage(101), 15000L);
                }
            }).setIcon(17301543).show();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebCoreThreadWatchdog
 * JD-Core Version:        0.6.2
 */