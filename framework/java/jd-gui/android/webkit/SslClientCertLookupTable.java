package android.webkit;

import java.security.PrivateKey;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class SslClientCertLookupTable
{
    private static SslClientCertLookupTable sTable;
    private final Map<String, byte[][]> certificateChains = new HashMap();
    private final Set<String> denied = new HashSet();
    private final Map<String, PrivateKey> privateKeys = new HashMap();

    /** @deprecated */
    public static SslClientCertLookupTable getInstance()
    {
        try
        {
            if (sTable == null)
                sTable = new SslClientCertLookupTable();
            SslClientCertLookupTable localSslClientCertLookupTable = sTable;
            return localSslClientCertLookupTable;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void Allow(String paramString, PrivateKey paramPrivateKey, byte[][] paramArrayOfByte)
    {
        this.privateKeys.put(paramString, paramPrivateKey);
        this.certificateChains.put(paramString, paramArrayOfByte);
        this.denied.remove(paramString);
    }

    public byte[][] CertificateChain(String paramString)
    {
        return (byte[][])this.certificateChains.get(paramString);
    }

    public void Deny(String paramString)
    {
        this.privateKeys.remove(paramString);
        this.certificateChains.remove(paramString);
        this.denied.add(paramString);
    }

    public boolean IsAllowed(String paramString)
    {
        return this.privateKeys.containsKey(paramString);
    }

    public boolean IsDenied(String paramString)
    {
        return this.denied.contains(paramString);
    }

    public PrivateKey PrivateKey(String paramString)
    {
        return (PrivateKey)this.privateKeys.get(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.SslClientCertLookupTable
 * JD-Core Version:        0.6.2
 */