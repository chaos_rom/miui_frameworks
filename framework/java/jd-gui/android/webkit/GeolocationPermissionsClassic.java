package android.webkit;

import android.os.Handler;
import android.os.Message;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

final class GeolocationPermissionsClassic extends GeolocationPermissions
{
    static final int ALLOW = 3;
    private static final String ALLOWED = "allowed";
    private static final String CALLBACK = "callback";
    static final int CLEAR = 2;
    static final int CLEAR_ALL = 4;
    static final int GET_ALLOWED = 1;
    static final int GET_ORIGINS = 0;
    private static final String ORIGIN = "origin";
    private static final String ORIGINS = "origins";
    static final int RETURN_ALLOWED = 1;
    static final int RETURN_ORIGINS;
    private static GeolocationPermissionsClassic sInstance;
    private Handler mHandler;
    private Vector<Message> mQueuedMessages;
    private Handler mUIHandler;

    public static GeolocationPermissionsClassic getInstance()
    {
        if (sInstance == null)
            sInstance = new GeolocationPermissionsClassic();
        return sInstance;
    }

    private static native void nativeAllow(String paramString);

    private static native void nativeClear(String paramString);

    private static native void nativeClearAll();

    private static native boolean nativeGetAllowed(String paramString);

    private static native Set nativeGetOrigins();

    /** @deprecated */
    private void postMessage(Message paramMessage)
    {
        try
        {
            if (this.mHandler == null)
            {
                if (this.mQueuedMessages == null)
                    this.mQueuedMessages = new Vector();
                this.mQueuedMessages.add(paramMessage);
            }
            while (true)
            {
                return;
                this.mHandler.sendMessage(paramMessage);
            }
        }
        finally
        {
        }
    }

    private void postUIMessage(Message paramMessage)
    {
        if (this.mUIHandler != null)
            this.mUIHandler.sendMessage(paramMessage);
    }

    public void allow(String paramString)
    {
        postMessage(Message.obtain(null, 3, paramString));
    }

    public void clear(String paramString)
    {
        postMessage(Message.obtain(null, 2, paramString));
    }

    public void clearAll()
    {
        postMessage(Message.obtain(null, 4));
    }

    /** @deprecated */
    public void createHandler()
    {
        try
        {
            if (this.mHandler != null)
                break label70;
            this.mHandler = new Handler()
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    switch (paramAnonymousMessage.what)
                    {
                    default:
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    }
                    while (true)
                    {
                        return;
                        Set localSet = GeolocationPermissionsClassic.access$000();
                        ValueCallback localValueCallback2 = (ValueCallback)paramAnonymousMessage.obj;
                        HashMap localHashMap2 = new HashMap();
                        localHashMap2.put("callback", localValueCallback2);
                        localHashMap2.put("origins", localSet);
                        GeolocationPermissionsClassic.this.postUIMessage(Message.obtain(null, 0, localHashMap2));
                        continue;
                        Map localMap = (Map)paramAnonymousMessage.obj;
                        String str = (String)localMap.get("origin");
                        ValueCallback localValueCallback1 = (ValueCallback)localMap.get("callback");
                        boolean bool = GeolocationPermissionsClassic.nativeGetAllowed(str);
                        HashMap localHashMap1 = new HashMap();
                        localHashMap1.put("callback", localValueCallback1);
                        localHashMap1.put("allowed", Boolean.valueOf(bool));
                        GeolocationPermissionsClassic.this.postUIMessage(Message.obtain(null, 1, localHashMap1));
                        continue;
                        GeolocationPermissionsClassic.nativeClear((String)paramAnonymousMessage.obj);
                        continue;
                        GeolocationPermissionsClassic.nativeAllow((String)paramAnonymousMessage.obj);
                        continue;
                        GeolocationPermissionsClassic.access$500();
                    }
                }
            };
            if (this.mQueuedMessages == null)
                break label70;
            while (!this.mQueuedMessages.isEmpty())
                this.mHandler.sendMessage((Message)this.mQueuedMessages.remove(0));
        }
        finally
        {
        }
        this.mQueuedMessages = null;
        label70:
    }

    public void createUIHandler()
    {
        if (this.mUIHandler == null)
            this.mUIHandler = new Handler()
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    switch (paramAnonymousMessage.what)
                    {
                    default:
                    case 0:
                    case 1:
                    }
                    while (true)
                    {
                        return;
                        Map localMap2 = (Map)paramAnonymousMessage.obj;
                        Set localSet = (Set)localMap2.get("origins");
                        ((ValueCallback)localMap2.get("callback")).onReceiveValue(localSet);
                        continue;
                        Map localMap1 = (Map)paramAnonymousMessage.obj;
                        Boolean localBoolean = (Boolean)localMap1.get("allowed");
                        ((ValueCallback)localMap1.get("callback")).onReceiveValue(localBoolean);
                    }
                }
            };
    }

    public void getAllowed(String paramString, ValueCallback<Boolean> paramValueCallback)
    {
        if (paramValueCallback == null);
        while (true)
        {
            return;
            if (paramString == null)
            {
                paramValueCallback.onReceiveValue(null);
            }
            else if ("WebViewCoreThread".equals(Thread.currentThread().getName()))
            {
                paramValueCallback.onReceiveValue(Boolean.valueOf(nativeGetAllowed(paramString)));
            }
            else
            {
                HashMap localHashMap = new HashMap();
                localHashMap.put("origin", paramString);
                localHashMap.put("callback", paramValueCallback);
                postMessage(Message.obtain(null, 1, localHashMap));
            }
        }
    }

    public void getOrigins(ValueCallback<Set<String>> paramValueCallback)
    {
        if (paramValueCallback != null)
        {
            if (!"WebViewCoreThread".equals(Thread.currentThread().getName()))
                break label28;
            paramValueCallback.onReceiveValue(nativeGetOrigins());
        }
        while (true)
        {
            return;
            label28: postMessage(Message.obtain(null, 0, paramValueCallback));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.GeolocationPermissionsClassic
 * JD-Core Version:        0.6.2
 */