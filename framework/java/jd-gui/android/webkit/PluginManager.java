package android.webkit;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Message;
import android.os.SystemProperties;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;

public class PluginManager
{
    private static final String LOGTAG = "PluginManager";
    public static final String PLUGIN_ACTION = "android.webkit.PLUGIN";
    public static final String PLUGIN_PERMISSION = "android.webkit.permission.PLUGIN";
    private static final String PLUGIN_SYSTEM_LIB = "/system/lib/plugins/";
    private static final String PLUGIN_TYPE = "type";
    private static final Signature[] SIGNATURES = arrayOfSignature;
    private static final String SIGNATURE_1 = "308204c5308203ada003020102020900d7cb412f75f4887e300d06092a864886f70d010105050030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564301e170d3039313030313030323331345a170d3337303231363030323331345a30819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f726174656430820120300d06092a864886f70d01010105000382010d0030820108028201010099724f3e05bbd78843794f357776e04b340e13cb1c9ccb3044865180d7d8fec8166c5bbd876da8b80aa71eb6ba3d4d3455c9a8de162d24a25c4c1cd04c9523affd06a279fc8f0d018f242486bdbb2dbfbf6fcb21ed567879091928b876f7ccebc7bccef157366ebe74e33ae1d7e9373091adab8327482154afc0693a549522f8c796dd84d16e24bb221f5dbb809ca56dd2b6e799c5fa06b6d9c5c09ada54ea4c5db1523a9794ed22a3889e5e05b29f8ee0a8d61efe07ae28f65dece2ff7edc5b1416d7c7aad7f0d35e8f4a4b964dbf50ae9aa6d620157770d974131b3e7e3abd6d163d65758e2f0822db9c88598b9db6263d963d13942c91fc5efe34fc1e06e3020103a382010630820102301d0603551d0e041604145af418e419a639e1657db960996364a37ef20d403081d20603551d230481ca3081c780145af418e419a639e1657db960996364a37ef20d40a181a3a481a030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564820900d7cb412f75f4887e300c0603551d13040530030101ff300d06092a864886f70d0101050500038201010076c2a11fe303359689c2ebc7b2c398eff8c3f9ad545cdbac75df63bf7b5395b6988d1842d6aa1556d595b5692e08224d667a4c9c438f05e74906c53dd8016dde7004068866f01846365efd146e9bfaa48c9ecf657f87b97c757da11f225c4a24177bf2d7188e6cce2a70a1e8a841a14471eb51457398b8a0addd8b6c8c1538ca8f1e40b4d8b960009ea22c188d28924813d2c0b4a4d334b7cf05507e1fcf0a06fe946c7ffc435e173af6fc3e3400643710acc806f830a14788291d46f2feed9fb5c70423ca747ed1572d752894ac1f19f93989766308579393fabb43649aa8806a313b1ab9a50922a44c2467b9062037f2da0d484d9ffd8fe628eeea629ba637";
    private static final String TYPE_NATIVE = "native";
    private static PluginManager mInstance = null;
    private final Context mContext;
    private ArrayList<PackageInfo> mPackageInfoCache;

    static
    {
        Signature[] arrayOfSignature = new Signature[1];
        arrayOfSignature[0] = new Signature("308204c5308203ada003020102020900d7cb412f75f4887e300d06092a864886f70d010105050030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564301e170d3039313030313030323331345a170d3337303231363030323331345a30819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f726174656430820120300d06092a864886f70d01010105000382010d0030820108028201010099724f3e05bbd78843794f357776e04b340e13cb1c9ccb3044865180d7d8fec8166c5bbd876da8b80aa71eb6ba3d4d3455c9a8de162d24a25c4c1cd04c9523affd06a279fc8f0d018f242486bdbb2dbfbf6fcb21ed567879091928b876f7ccebc7bccef157366ebe74e33ae1d7e9373091adab8327482154afc0693a549522f8c796dd84d16e24bb221f5dbb809ca56dd2b6e799c5fa06b6d9c5c09ada54ea4c5db1523a9794ed22a3889e5e05b29f8ee0a8d61efe07ae28f65dece2ff7edc5b1416d7c7aad7f0d35e8f4a4b964dbf50ae9aa6d620157770d974131b3e7e3abd6d163d65758e2f0822db9c88598b9db6263d963d13942c91fc5efe34fc1e06e3020103a382010630820102301d0603551d0e041604145af418e419a639e1657db960996364a37ef20d403081d20603551d230481ca3081c780145af418e419a639e1657db960996364a37ef20d40a181a3a481a030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564820900d7cb412f75f4887e300c0603551d13040530030101ff300d06092a864886f70d0101050500038201010076c2a11fe303359689c2ebc7b2c398eff8c3f9ad545cdbac75df63bf7b5395b6988d1842d6aa1556d595b5692e08224d667a4c9c438f05e74906c53dd8016dde7004068866f01846365efd146e9bfaa48c9ecf657f87b97c757da11f225c4a24177bf2d7188e6cce2a70a1e8a841a14471eb51457398b8a0addd8b6c8c1538ca8f1e40b4d8b960009ea22c188d28924813d2c0b4a4d334b7cf05507e1fcf0a06fe946c7ffc435e173af6fc3e3400643710acc806f830a14788291d46f2feed9fb5c70423ca747ed1572d752894ac1f19f93989766308579393fabb43649aa8806a313b1ab9a50922a44c2467b9062037f2da0d484d9ffd8fe628eeea629ba637");
    }

    private PluginManager(Context paramContext)
    {
        this.mContext = paramContext;
        this.mPackageInfoCache = new ArrayList();
    }

    private static boolean containsPluginPermissionAndSignatures(PackageInfo paramPackageInfo)
    {
        boolean bool = false;
        String[] arrayOfString = paramPackageInfo.requestedPermissions;
        if (arrayOfString == null);
        while (true)
        {
            return bool;
            int i = 0;
            int j = arrayOfString.length;
            int k = 0;
            int m;
            int i1;
            label78: Signature localSignature;
            if (k < j)
            {
                if ("android.webkit.permission.PLUGIN".equals(arrayOfString[k]))
                    i = 1;
            }
            else
            {
                if (i == 0)
                    continue;
                Signature[] arrayOfSignature = paramPackageInfo.signatures;
                if (arrayOfSignature == null)
                    continue;
                if (!SystemProperties.getBoolean("ro.secure", false))
                    break label144;
                m = 0;
                int n = arrayOfSignature.length;
                i1 = 0;
                if (i1 >= n)
                    break label139;
                localSignature = arrayOfSignature[i1];
            }
            for (int i2 = 0; ; i2++)
                if (i2 < SIGNATURES.length)
                {
                    if (SIGNATURES[i2].equals(localSignature))
                        m = 1;
                }
                else
                {
                    i1++;
                    break label78;
                    k++;
                    break;
                }
            label139: if (m != 0)
                label144: bool = true;
        }
    }

    /** @deprecated */
    public static PluginManager getInstance(Context paramContext)
    {
        try
        {
            if (mInstance != null)
                break label43;
            if (paramContext == null)
                throw new IllegalStateException("First call to PluginManager need a valid context.");
        }
        finally
        {
        }
        mInstance = new PluginManager(paramContext.getApplicationContext());
        label43: PluginManager localPluginManager = mInstance;
        return localPluginManager;
    }

    boolean containsPluginPermissionAndSignatures(String paramString)
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        try
        {
            PackageInfo localPackageInfo = localPackageManager.getPackageInfo(paramString, 4160);
            if (localPackageInfo != null)
            {
                boolean bool2 = containsPluginPermissionAndSignatures(localPackageInfo);
                bool1 = bool2;
                return bool1;
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
            {
                Log.w("PluginManager", "Can't find plugin: " + paramString);
                boolean bool1 = false;
            }
        }
    }

    Class<?> getPluginClass(String paramString1, String paramString2)
        throws PackageManager.NameNotFoundException, ClassNotFoundException
    {
        return this.mContext.createPackageContext(paramString1, 3).getClassLoader().loadClass(paramString2);
    }

    // ERROR //
    String[] getPluginDirectories()
    {
        // Byte code:
        //     0: new 54	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 55	java/util/ArrayList:<init>	()V
        //     7: astore_1
        //     8: aload_0
        //     9: getfield 52	android/webkit/PluginManager:mContext	Landroid/content/Context;
        //     12: invokevirtual 105	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     15: astore_2
        //     16: aload_2
        //     17: new 154	android/content/Intent
        //     20: dup
        //     21: ldc 11
        //     23: invokespecial 155	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     26: sipush 132
        //     29: invokevirtual 159	android/content/pm/PackageManager:queryIntentServices	(Landroid/content/Intent;I)Ljava/util/List;
        //     32: astore_3
        //     33: aload_0
        //     34: getfield 57	android/webkit/PluginManager:mPackageInfoCache	Ljava/util/ArrayList;
        //     37: astore 4
        //     39: aload 4
        //     41: monitorenter
        //     42: aload_0
        //     43: getfield 57	android/webkit/PluginManager:mPackageInfoCache	Ljava/util/ArrayList;
        //     46: invokevirtual 162	java/util/ArrayList:clear	()V
        //     49: aload_3
        //     50: invokeinterface 168 1 0
        //     55: astore 6
        //     57: aload 6
        //     59: invokeinterface 174 1 0
        //     64: ifeq +412 -> 476
        //     67: aload 6
        //     69: invokeinterface 178 1 0
        //     74: checkcast 180	android/content/pm/ResolveInfo
        //     77: getfield 184	android/content/pm/ResolveInfo:serviceInfo	Landroid/content/pm/ServiceInfo;
        //     80: astore 7
        //     82: aload 7
        //     84: ifnonnull +22 -> 106
        //     87: ldc 8
        //     89: ldc 186
        //     91: invokestatic 132	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     94: pop
        //     95: goto -38 -> 57
        //     98: astore 5
        //     100: aload 4
        //     102: monitorexit
        //     103: aload 5
        //     105: athrow
        //     106: aload_2
        //     107: aload 7
        //     109: getfield 191	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     112: sipush 4160
        //     115: invokevirtual 111	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
        //     118: astore 10
        //     120: aload 10
        //     122: ifnull -65 -> 57
        //     125: new 115	java/lang/StringBuilder
        //     128: dup
        //     129: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     132: aload 10
        //     134: getfield 195	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     137: getfield 200	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     140: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     143: ldc 202
        //     145: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     148: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     151: astore 11
        //     153: sipush 129
        //     156: aload 10
        //     158: getfield 195	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     161: getfield 206	android/content/pm/ApplicationInfo:flags	I
        //     164: iand
        //     165: iconst_1
        //     166: if_icmpne +28 -> 194
        //     169: new 115	java/lang/StringBuilder
        //     172: dup
        //     173: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     176: ldc 17
        //     178: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     181: aload 10
        //     183: getfield 207	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     186: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     189: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     192: astore 11
        //     194: aload 10
        //     196: invokestatic 113	android/webkit/PluginManager:containsPluginPermissionAndSignatures	(Landroid/content/pm/PackageInfo;)Z
        //     199: ifeq -142 -> 57
        //     202: aload 7
        //     204: getfield 211	android/content/pm/PackageItemInfo:metaData	Landroid/os/Bundle;
        //     207: ifnonnull +74 -> 281
        //     210: ldc 8
        //     212: new 115	java/lang/StringBuilder
        //     215: dup
        //     216: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     219: ldc 213
        //     221: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     224: aload 7
        //     226: getfield 216	android/content/pm/PackageItemInfo:name	Ljava/lang/String;
        //     229: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     232: ldc 218
        //     234: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     237: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     240: invokestatic 221	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     243: pop
        //     244: goto -187 -> 57
        //     247: astore 8
        //     249: ldc 8
        //     251: new 115	java/lang/StringBuilder
        //     254: dup
        //     255: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     258: ldc 118
        //     260: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     263: aload 7
        //     265: getfield 191	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     268: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     271: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     274: invokestatic 132	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     277: pop
        //     278: goto -221 -> 57
        //     281: aload 7
        //     283: getfield 211	android/content/pm/PackageItemInfo:metaData	Landroid/os/Bundle;
        //     286: ldc 20
        //     288: invokevirtual 227	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
        //     291: astore 12
        //     293: ldc 28
        //     295: aload 12
        //     297: invokevirtual 71	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     300: ifne +32 -> 332
        //     303: ldc 8
        //     305: new 115	java/lang/StringBuilder
        //     308: dup
        //     309: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     312: ldc 229
        //     314: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     317: aload 12
        //     319: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     322: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     325: invokestatic 221	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     328: pop
        //     329: goto -272 -> 57
        //     332: aload_0
        //     333: aload 7
        //     335: getfield 191	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     338: aload 7
        //     340: getfield 216	android/content/pm/PackageItemInfo:name	Ljava/lang/String;
        //     343: invokevirtual 231	android/webkit/PluginManager:getPluginClass	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;
        //     346: pop
        //     347: iconst_1
        //     348: ifne +108 -> 456
        //     351: ldc 8
        //     353: new 115	java/lang/StringBuilder
        //     356: dup
        //     357: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     360: ldc 233
        //     362: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     365: aload 7
        //     367: getfield 216	android/content/pm/PackageItemInfo:name	Ljava/lang/String;
        //     370: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     373: ldc 235
        //     375: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     378: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     381: invokestatic 221	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     384: pop
        //     385: goto -328 -> 57
        //     388: astore 15
        //     390: ldc 8
        //     392: new 115	java/lang/StringBuilder
        //     395: dup
        //     396: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     399: ldc 118
        //     401: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     404: aload 7
        //     406: getfield 191	android/content/pm/PackageItemInfo:packageName	Ljava/lang/String;
        //     409: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     412: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     415: invokestatic 221	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     418: pop
        //     419: goto -362 -> 57
        //     422: astore 13
        //     424: ldc 8
        //     426: new 115	java/lang/StringBuilder
        //     429: dup
        //     430: invokespecial 116	java/lang/StringBuilder:<init>	()V
        //     433: ldc 237
        //     435: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     438: aload 7
        //     440: getfield 216	android/content/pm/PackageItemInfo:name	Ljava/lang/String;
        //     443: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     446: invokevirtual 126	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     449: invokestatic 221	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     452: pop
        //     453: goto -396 -> 57
        //     456: aload_0
        //     457: getfield 57	android/webkit/PluginManager:mPackageInfoCache	Ljava/util/ArrayList;
        //     460: aload 10
        //     462: invokevirtual 240	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     465: pop
        //     466: aload_1
        //     467: aload 11
        //     469: invokevirtual 240	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     472: pop
        //     473: goto -416 -> 57
        //     476: aload 4
        //     478: monitorexit
        //     479: aload_1
        //     480: aload_1
        //     481: invokevirtual 244	java/util/ArrayList:size	()I
        //     484: anewarray 67	java/lang/String
        //     487: invokevirtual 248	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     490: checkcast 249	[Ljava/lang/String;
        //     493: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     42	103	98	finally
        //     106	120	98	finally
        //     125	329	98	finally
        //     332	385	98	finally
        //     390	479	98	finally
        //     106	120	247	android/content/pm/PackageManager$NameNotFoundException
        //     332	385	388	android/content/pm/PackageManager$NameNotFoundException
        //     332	385	422	java/lang/ClassNotFoundException
    }

    String getPluginSharedDataDirectory()
    {
        return this.mContext.getDir("plugins", 0).getPath();
    }

    // ERROR //
    String getPluginsAPKName(String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_1
        //     3: ifnull +10 -> 13
        //     6: aload_1
        //     7: invokevirtual 265	java/lang/String:length	()I
        //     10: ifne +5 -> 15
        //     13: aload_2
        //     14: areturn
        //     15: aload_0
        //     16: getfield 57	android/webkit/PluginManager:mPackageInfoCache	Ljava/util/ArrayList;
        //     19: astore_3
        //     20: aload_3
        //     21: monitorenter
        //     22: aload_0
        //     23: getfield 57	android/webkit/PluginManager:mPackageInfoCache	Ljava/util/ArrayList;
        //     26: invokevirtual 266	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     29: astore 5
        //     31: aload 5
        //     33: invokeinterface 174 1 0
        //     38: ifeq +45 -> 83
        //     41: aload 5
        //     43: invokeinterface 178 1 0
        //     48: checkcast 61	android/content/pm/PackageInfo
        //     51: astore 6
        //     53: aload_1
        //     54: aload 6
        //     56: getfield 207	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     59: invokevirtual 270	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
        //     62: ifeq -31 -> 31
        //     65: aload 6
        //     67: getfield 207	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     70: astore_2
        //     71: aload_3
        //     72: monitorexit
        //     73: goto -60 -> 13
        //     76: astore 4
        //     78: aload_3
        //     79: monitorexit
        //     80: aload 4
        //     82: athrow
        //     83: aload_3
        //     84: monitorexit
        //     85: goto -72 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     22	80	76	finally
        //     83	85	76	finally
    }

    public void refreshPlugins(boolean paramBoolean)
    {
        BrowserFrame.sJavaBridge.obtainMessage(100, Boolean.valueOf(paramBoolean)).sendToTarget();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.PluginManager
 * JD-Core Version:        0.6.2
 */