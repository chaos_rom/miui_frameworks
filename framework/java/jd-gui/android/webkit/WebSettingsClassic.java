package android.webkit;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.util.EventLog;
import java.util.Locale;

public class WebSettingsClassic extends WebSettings
{
    private static final String ACCEPT_LANG_FOR_US_LOCALE = "en-US";
    private static final String DESKTOP_USERAGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24";
    private static final String DOUBLE_TAP_TOAST_COUNT = "double_tap_toast_count";
    private static final String IPHONE_USERAGENT = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16";
    private static final String PREF_FILE = "WebViewSettings";
    private static final String PREVIOUS_VERSION = "4.0.4";
    private static int mDoubleTapToastCount = 3;
    private static Locale sLocale;
    private static Object sLockForLocaleSettings;
    private String mAcceptLanguage;
    private boolean mAllowContentAccess = true;
    private boolean mAllowFileAccess = true;
    private boolean mAllowFileAccessFromFileURLs = false;
    private boolean mAllowUniversalAccessFromFileURLs = false;
    private boolean mAppCacheEnabled = false;
    private long mAppCacheMaxSize = 9223372036854775807L;
    private String mAppCachePath = null;
    private boolean mAutoFillEnabled = false;
    private AutoFillProfile mAutoFillProfile;
    private boolean mBlockNetworkImage = false;
    private boolean mBlockNetworkLoads;
    private BrowserFrame mBrowserFrame;
    private boolean mBuiltInZoomControls = false;
    private Context mContext;
    private String mCursiveFontFamily = "cursive";
    private boolean mDatabaseEnabled = false;
    private String mDatabasePath = "";
    private boolean mDatabasePathHasBeenSet = false;
    private int mDefaultFixedFontSize = 13;
    private int mDefaultFontSize = 16;
    private String mDefaultTextEncoding;
    private WebSettings.ZoomDensity mDefaultZoom = WebSettings.ZoomDensity.MEDIUM;
    private boolean mDisplayZoomControls = true;
    private boolean mDomStorageEnabled = false;
    private int mDoubleTapZoom = 100;
    private boolean mEnableSmoothTransition = false;
    private final EventHandler mEventHandler = new EventHandler(null);
    private String mFantasyFontFamily = "fantasy";
    private String mFixedFontFamily = "monospace";
    private boolean mForceUserScalable = false;
    private String mGeolocationDatabasePath = "";
    private boolean mGeolocationEnabled = true;
    private boolean mHardwareAccelSkia = false;
    private boolean mJavaScriptCanOpenWindowsAutomatically = false;
    private boolean mJavaScriptEnabled = false;
    private WebSettings.LayoutAlgorithm mLayoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS;
    private boolean mLightTouchEnabled = false;
    private boolean mLinkPrefetchEnabled = false;
    private boolean mLoadWithOverviewMode = false;
    private boolean mLoadsImagesAutomatically = true;
    private long mMaximumDecodedImageSize = 0L;
    private int mMinimumFontSize = 8;
    private int mMinimumLogicalFontSize = 8;
    private boolean mNavDump = false;
    private boolean mNeedInitialFocus = true;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private boolean mNightReadMode = false;
    private int mOverrideCacheMode = -1;
    private int mPageCacheCapacity = 0;
    private boolean mPasswordEchoEnabled = true;
    private WebSettings.PluginState mPluginState = WebSettings.PluginState.OFF;
    private boolean mPrivateBrowsingEnabled = false;
    private WebSettings.RenderPriority mRenderPriority = WebSettings.RenderPriority.NORMAL;
    private String mSansSerifFontFamily = "sans-serif";
    private boolean mSaveFormData = true;
    private boolean mSavePassword = true;
    private String mSerifFontFamily = "serif";
    private boolean mShowVisualIndicator = false;
    private boolean mShrinksStandaloneImagesToFit = false;
    private String mStandardFontFamily = "sans-serif";
    private boolean mSupportMultipleWindows = false;
    private boolean mSupportZoom = true;
    private boolean mSyncPending = false;
    private boolean mSyntheticLinksEnabled = true;
    private int mTextSize = 100;
    private boolean mUseDefaultUserAgent;
    private boolean mUseDoubleTree = false;
    private boolean mUseWebViewBackgroundForOverscroll = true;
    private boolean mUseWideViewport = false;
    private String mUserAgent;
    private WebViewClassic mWebView;
    private boolean mWorkersEnabled = false;
    private boolean mXSSAuditorEnabled = false;

    WebSettingsClassic(Context paramContext, WebViewClassic paramWebViewClassic)
    {
        this.mContext = paramContext;
        this.mWebView = paramWebViewClassic;
        this.mDefaultTextEncoding = paramContext.getString(17039546);
        if (sLockForLocaleSettings == null)
        {
            sLockForLocaleSettings = new Object();
            sLocale = Locale.getDefault();
        }
        this.mAcceptLanguage = getCurrentAcceptLanguage();
        this.mUserAgent = getCurrentUserAgent();
        this.mUseDefaultUserAgent = true;
        boolean bool2;
        if (this.mContext.checkPermission("android.permission.INTERNET", Process.myPid(), Process.myUid()) != 0)
            bool2 = true;
        while (true)
        {
            this.mBlockNetworkLoads = bool2;
            if (this.mContext.getApplicationInfo().targetSdkVersion < 16)
            {
                this.mAllowUniversalAccessFromFileURLs = true;
                this.mAllowFileAccessFromFileURLs = true;
            }
            try
            {
                if (Settings.System.getInt(paramContext.getContentResolver(), "show_password") != 0)
                    bool1 = true;
                this.mPasswordEchoEnabled = bool1;
                return;
                bool2 = false;
            }
            catch (Settings.SettingNotFoundException localSettingNotFoundException)
            {
                while (true)
                    this.mPasswordEchoEnabled = true;
            }
        }
    }

    private static void addLocaleToHttpAcceptLanguage(StringBuilder paramStringBuilder, Locale paramLocale)
    {
        String str1 = convertObsoleteLanguageCodeToNew(paramLocale.getLanguage());
        if (str1 != null)
        {
            paramStringBuilder.append(str1);
            String str2 = paramLocale.getCountry();
            if (str2 != null)
            {
                paramStringBuilder.append("-");
                paramStringBuilder.append(str2);
            }
        }
    }

    private static String convertObsoleteLanguageCodeToNew(String paramString)
    {
        if (paramString == null)
            paramString = null;
        while (true)
        {
            return paramString;
            if ("iw".equals(paramString))
                paramString = "he";
            else if ("in".equals(paramString))
                paramString = "id";
            else if ("ji".equals(paramString))
                paramString = "yi";
        }
    }

    private String getCurrentAcceptLanguage()
    {
        synchronized (sLockForLocaleSettings)
        {
            Locale localLocale = sLocale;
            StringBuilder localStringBuilder = new StringBuilder();
            addLocaleToHttpAcceptLanguage(localStringBuilder, localLocale);
            if (!Locale.US.equals(localLocale))
            {
                if (localStringBuilder.length() > 0)
                    localStringBuilder.append(", ");
                localStringBuilder.append("en-US");
            }
            return localStringBuilder.toString();
        }
    }

    /** @deprecated */
    private String getCurrentUserAgent()
    {
        while (true)
        {
            StringBuffer localStringBuffer;
            try
            {
                synchronized (sLockForLocaleSettings)
                {
                    Locale localLocale = sLocale;
                    localStringBuffer = new StringBuffer();
                    String str1 = Build.VERSION.RELEASE;
                    if (str1.length() <= 0)
                        break label291;
                    if (Character.isDigit(str1.charAt(0)))
                    {
                        localStringBuffer.append(str1);
                        localStringBuffer.append("; ");
                        String str2 = localLocale.getLanguage();
                        if (str2 == null)
                            break label303;
                        localStringBuffer.append(convertObsoleteLanguageCodeToNew(str2));
                        String str8 = localLocale.getCountry();
                        if (str8 != null)
                        {
                            localStringBuffer.append("-");
                            localStringBuffer.append(str8.toLowerCase());
                        }
                        localStringBuffer.append(";");
                        if ("REL".equals(Build.VERSION.CODENAME))
                        {
                            String str7 = Build.MODEL;
                            if (str7.length() > 0)
                            {
                                localStringBuffer.append(" ");
                                localStringBuffer.append(str7);
                            }
                        }
                        String str3 = Build.ID;
                        if (str3.length() > 0)
                        {
                            localStringBuffer.append(" Build/");
                            localStringBuffer.append(str3);
                        }
                        String str4 = this.mContext.getResources().getText(17040188).toString();
                        String str5 = this.mContext.getResources().getText(17040187).toString();
                        Object[] arrayOfObject = new Object[2];
                        arrayOfObject[0] = localStringBuffer;
                        arrayOfObject[1] = str4;
                        String str6 = String.format(str5, arrayOfObject);
                        return str6;
                    }
                }
            }
            finally
            {
            }
            localStringBuffer.append("4.0.4");
            continue;
            label291: localStringBuffer.append("1.0");
            continue;
            label303: localStringBuffer.append("en");
        }
    }

    private native void nativeSync(int paramInt);

    private int pin(int paramInt)
    {
        if (paramInt < 1)
            paramInt = 1;
        while (true)
        {
            return paramInt;
            if (paramInt > 72)
                paramInt = 72;
        }
    }

    /** @deprecated */
    private void postSync()
    {
        try
        {
            if (!this.mSyncPending)
                this.mSyncPending = this.mEventHandler.sendMessage(Message.obtain(null, 0));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void verifyNetworkAccess()
    {
        if ((!this.mBlockNetworkLoads) && (this.mContext.checkPermission("android.permission.INTERNET", Process.myPid(), Process.myUid()) != 0))
            throw new SecurityException("Permission denied - application missing INTERNET permission");
    }

    public boolean enableSmoothTransition()
    {
        return this.mEnableSmoothTransition;
    }

    public boolean forceUserScalable()
    {
        return this.mForceUserScalable;
    }

    /** @deprecated */
    String getAcceptLanguage()
    {
        try
        {
            synchronized (sLockForLocaleSettings)
            {
                Locale localLocale = Locale.getDefault();
                if (!sLocale.equals(localLocale))
                {
                    sLocale = localLocale;
                    this.mAcceptLanguage = getCurrentAcceptLanguage();
                }
                String str = this.mAcceptLanguage;
                return str;
            }
        }
        finally
        {
        }
    }

    public boolean getAllowContentAccess()
    {
        return this.mAllowContentAccess;
    }

    public boolean getAllowFileAccess()
    {
        return this.mAllowFileAccess;
    }

    /** @deprecated */
    public boolean getAllowFileAccessFromFileURLs()
    {
        try
        {
            boolean bool = this.mAllowFileAccessFromFileURLs;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getAllowUniversalAccessFromFileURLs()
    {
        try
        {
            boolean bool = this.mAllowUniversalAccessFromFileURLs;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getAutoFillEnabled()
    {
        try
        {
            boolean bool = this.mAutoFillEnabled;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public AutoFillProfile getAutoFillProfile()
    {
        try
        {
            AutoFillProfile localAutoFillProfile = this.mAutoFillProfile;
            return localAutoFillProfile;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getBlockNetworkImage()
    {
        try
        {
            boolean bool = this.mBlockNetworkImage;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getBlockNetworkLoads()
    {
        try
        {
            boolean bool = this.mBlockNetworkLoads;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean getBuiltInZoomControls()
    {
        return this.mBuiltInZoomControls;
    }

    public int getCacheMode()
    {
        return this.mOverrideCacheMode;
    }

    /** @deprecated */
    public String getCursiveFontFamily()
    {
        try
        {
            String str = this.mCursiveFontFamily;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getDatabaseEnabled()
    {
        try
        {
            boolean bool = this.mDatabaseEnabled;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public String getDatabasePath()
    {
        try
        {
            String str = this.mDatabasePath;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getDefaultFixedFontSize()
    {
        try
        {
            int i = this.mDefaultFixedFontSize;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getDefaultFontSize()
    {
        try
        {
            int i = this.mDefaultFontSize;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public String getDefaultTextEncodingName()
    {
        try
        {
            String str = this.mDefaultTextEncoding;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public WebSettings.ZoomDensity getDefaultZoom()
    {
        return this.mDefaultZoom;
    }

    public boolean getDisplayZoomControls()
    {
        return this.mDisplayZoomControls;
    }

    /** @deprecated */
    public boolean getDomStorageEnabled()
    {
        try
        {
            boolean bool = this.mDomStorageEnabled;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    int getDoubleTapToastCount()
    {
        return mDoubleTapToastCount;
    }

    public int getDoubleTapZoom()
    {
        return this.mDoubleTapZoom;
    }

    /** @deprecated */
    public String getFantasyFontFamily()
    {
        try
        {
            String str = this.mFantasyFontFamily;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public String getFixedFontFamily()
    {
        try
        {
            String str = this.mFixedFontFamily;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getHardwareAccelSkiaEnabled()
    {
        try
        {
            boolean bool = this.mHardwareAccelSkia;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getJavaScriptCanOpenWindowsAutomatically()
    {
        try
        {
            boolean bool = this.mJavaScriptCanOpenWindowsAutomatically;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getJavaScriptEnabled()
    {
        try
        {
            boolean bool = this.mJavaScriptEnabled;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public WebSettings.LayoutAlgorithm getLayoutAlgorithm()
    {
        try
        {
            WebSettings.LayoutAlgorithm localLayoutAlgorithm = this.mLayoutAlgorithm;
            return localLayoutAlgorithm;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean getLightTouchEnabled()
    {
        return this.mLightTouchEnabled;
    }

    public boolean getLoadWithOverviewMode()
    {
        return this.mLoadWithOverviewMode;
    }

    /** @deprecated */
    public boolean getLoadsImagesAutomatically()
    {
        try
        {
            boolean bool = this.mLoadsImagesAutomatically;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getMinimumFontSize()
    {
        try
        {
            int i = this.mMinimumFontSize;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getMinimumLogicalFontSize()
    {
        try
        {
            int i = this.mMinimumLogicalFontSize;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public boolean getNavDump()
    {
        return this.mNavDump;
    }

    boolean getNeedInitialFocus()
    {
        return this.mNeedInitialFocus;
    }

    /** @deprecated */
    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public boolean getNightReadModeEnabled()
    {
        try
        {
            boolean bool = this.mNightReadMode;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public WebSettings.PluginState getPluginState()
    {
        try
        {
            WebSettings.PluginState localPluginState = this.mPluginState;
            return localPluginState;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public boolean getPluginsEnabled()
    {
        try
        {
            WebSettings.PluginState localPluginState1 = this.mPluginState;
            WebSettings.PluginState localPluginState2 = WebSettings.PluginState.ON;
            if (localPluginState1 == localPluginState2)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    // ERROR //
    @Deprecated
    public String getPluginsPath()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: monitorexit
        //     4: ldc 239
        //     6: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	2	7	finally
    }

    public String getProperty(String paramString)
    {
        return this.mWebView.nativeGetProperty(paramString);
    }

    /** @deprecated */
    public String getSansSerifFontFamily()
    {
        try
        {
            String str = this.mSansSerifFontFamily;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean getSaveFormData()
    {
        if ((this.mSaveFormData) && (!this.mPrivateBrowsingEnabled));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean getSavePassword()
    {
        return this.mSavePassword;
    }

    /** @deprecated */
    public String getSerifFontFamily()
    {
        try
        {
            String str = this.mSerifFontFamily;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean getShowVisualIndicator()
    {
        try
        {
            boolean bool = this.mShowVisualIndicator;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public String getStandardFontFamily()
    {
        try
        {
            String str = this.mStandardFontFamily;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public WebSettings.TextSize getTextSize()
    {
        Object localObject1 = null;
        int i = 2147483647;
        try
        {
            Object localObject3;
            for (localObject3 : WebSettings.TextSize.values())
            {
                int m = Math.abs(this.mTextSize - ((WebSettings.TextSize)localObject3).value);
                if (m == 0)
                    return localObject3;
                if (m < i)
                {
                    i = m;
                    localObject1 = localObject3;
                }
            }
            if (localObject1 != null);
            while (true)
            {
                localObject3 = localObject1;
                break;
                localObject1 = WebSettings.TextSize.NORMAL;
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getTextZoom()
    {
        try
        {
            int i = this.mTextSize;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public boolean getUseDoubleTree()
    {
        return false;
    }

    boolean getUseFixedViewport()
    {
        return getUseWideViewPort();
    }

    @Deprecated
    public boolean getUseWebViewBackgroundForOverscrollBackground()
    {
        return this.mUseWebViewBackgroundForOverscroll;
    }

    /** @deprecated */
    public boolean getUseWideViewPort()
    {
        try
        {
            boolean bool = this.mUseWideViewport;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public int getUserAgent()
    {
        try
        {
            boolean bool1 = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24".equals(this.mUserAgent);
            int i;
            if (bool1)
                i = 1;
            while (true)
            {
                return i;
                if ("Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16".equals(this.mUserAgent))
                {
                    i = 2;
                }
                else
                {
                    boolean bool2 = this.mUseDefaultUserAgent;
                    if (bool2)
                        i = 0;
                    else
                        i = -1;
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getUserAgentString()
    {
        try
        {
            String str;
            if (("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24".equals(this.mUserAgent)) || ("Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16".equals(this.mUserAgent)) || (!this.mUseDefaultUserAgent))
                str = this.mUserAgent;
            while (true)
            {
                return str;
                int i = 0;
                synchronized (sLockForLocaleSettings)
                {
                    Locale localLocale = Locale.getDefault();
                    if (!sLocale.equals(localLocale))
                    {
                        sLocale = localLocale;
                        this.mUserAgent = getCurrentUserAgent();
                        this.mAcceptLanguage = getCurrentAcceptLanguage();
                        i = 1;
                    }
                    if (i != 0)
                        postSync();
                    str = this.mUserAgent;
                }
            }
        }
        finally
        {
        }
    }

    boolean isNarrowColumnLayout()
    {
        if (getLayoutAlgorithm() == WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isPrivateBrowsingEnabled()
    {
        return this.mPrivateBrowsingEnabled;
    }

    /** @deprecated */
    void onDestroyed()
    {
    }

    public void setAllowContentAccess(boolean paramBoolean)
    {
        this.mAllowContentAccess = paramBoolean;
    }

    public void setAllowFileAccess(boolean paramBoolean)
    {
        this.mAllowFileAccess = paramBoolean;
    }

    /** @deprecated */
    public void setAllowFileAccessFromFileURLs(boolean paramBoolean)
    {
        try
        {
            if (this.mAllowFileAccessFromFileURLs != paramBoolean)
            {
                this.mAllowFileAccessFromFileURLs = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setAllowUniversalAccessFromFileURLs(boolean paramBoolean)
    {
        try
        {
            if (this.mAllowUniversalAccessFromFileURLs != paramBoolean)
            {
                this.mAllowUniversalAccessFromFileURLs = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setAppCacheEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mAppCacheEnabled != paramBoolean)
            {
                this.mAppCacheEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setAppCacheMaxSize(long paramLong)
    {
        try
        {
            if (paramLong != this.mAppCacheMaxSize)
            {
                this.mAppCacheMaxSize = paramLong;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setAppCachePath(String paramString)
    {
        try
        {
            if ((this.mAppCachePath == null) && (paramString != null) && (!paramString.isEmpty()))
            {
                this.mAppCachePath = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setAutoFillEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        try
        {
            if (!this.mPrivateBrowsingEnabled)
            {
                bool = true;
                if (this.mAutoFillEnabled != bool)
                {
                    this.mAutoFillEnabled = bool;
                    postSync();
                }
                return;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setAutoFillProfile(AutoFillProfile paramAutoFillProfile)
    {
        try
        {
            if (this.mAutoFillProfile != paramAutoFillProfile)
            {
                this.mAutoFillProfile = paramAutoFillProfile;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setBlockNetworkImage(boolean paramBoolean)
    {
        try
        {
            if (this.mBlockNetworkImage != paramBoolean)
            {
                this.mBlockNetworkImage = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setBlockNetworkLoads(boolean paramBoolean)
    {
        try
        {
            if (this.mBlockNetworkLoads != paramBoolean)
            {
                this.mBlockNetworkLoads = paramBoolean;
                verifyNetworkAccess();
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setBuiltInZoomControls(boolean paramBoolean)
    {
        this.mBuiltInZoomControls = paramBoolean;
        this.mWebView.updateMultiTouchSupport(this.mContext);
    }

    public void setCacheMode(int paramInt)
    {
        if (paramInt != this.mOverrideCacheMode)
        {
            this.mOverrideCacheMode = paramInt;
            postSync();
        }
    }

    /** @deprecated */
    public void setCursiveFontFamily(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mCursiveFontFamily))
            {
                this.mCursiveFontFamily = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDatabaseEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mDatabaseEnabled != paramBoolean)
            {
                this.mDatabaseEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDatabasePath(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!this.mDatabasePathHasBeenSet)
            {
                this.mDatabasePath = paramString;
                this.mDatabasePathHasBeenSet = true;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDefaultFixedFontSize(int paramInt)
    {
        try
        {
            int i = pin(paramInt);
            if (this.mDefaultFixedFontSize != i)
            {
                this.mDefaultFixedFontSize = i;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDefaultFontSize(int paramInt)
    {
        try
        {
            int i = pin(paramInt);
            if (this.mDefaultFontSize != i)
            {
                this.mDefaultFontSize = i;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDefaultTextEncodingName(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mDefaultTextEncoding))
            {
                this.mDefaultTextEncoding = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setDefaultZoom(WebSettings.ZoomDensity paramZoomDensity)
    {
        if (this.mDefaultZoom != paramZoomDensity)
        {
            this.mDefaultZoom = paramZoomDensity;
            this.mWebView.adjustDefaultZoomDensity(paramZoomDensity.value);
        }
    }

    public void setDisplayZoomControls(boolean paramBoolean)
    {
        this.mDisplayZoomControls = paramBoolean;
        this.mWebView.updateMultiTouchSupport(this.mContext);
    }

    /** @deprecated */
    public void setDomStorageEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mDomStorageEnabled != paramBoolean)
            {
                this.mDomStorageEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setDoubleTapToastCount(int paramInt)
    {
        if (mDoubleTapToastCount != paramInt)
        {
            mDoubleTapToastCount = paramInt;
            this.mEventHandler.sendMessage(Message.obtain(null, 2));
        }
    }

    public void setDoubleTapZoom(int paramInt)
    {
        if (this.mDoubleTapZoom != paramInt)
        {
            this.mDoubleTapZoom = paramInt;
            this.mWebView.updateDoubleTapZoom(paramInt);
        }
    }

    public void setEnableSmoothTransition(boolean paramBoolean)
    {
        this.mEnableSmoothTransition = paramBoolean;
    }

    /** @deprecated */
    public void setFantasyFontFamily(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mFantasyFontFamily))
            {
                this.mFantasyFontFamily = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setFixedFontFamily(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mFixedFontFamily))
            {
                this.mFixedFontFamily = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setForceUserScalable(boolean paramBoolean)
    {
        try
        {
            this.mForceUserScalable = paramBoolean;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setGeolocationDatabasePath(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mGeolocationDatabasePath))
            {
                this.mGeolocationDatabasePath = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setGeolocationEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mGeolocationEnabled != paramBoolean)
            {
                this.mGeolocationEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setHardwareAccelSkiaEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mHardwareAccelSkia != paramBoolean)
            {
                this.mHardwareAccelSkia = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setJavaScriptCanOpenWindowsAutomatically(boolean paramBoolean)
    {
        try
        {
            if (this.mJavaScriptCanOpenWindowsAutomatically != paramBoolean)
            {
                this.mJavaScriptCanOpenWindowsAutomatically = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setJavaScriptEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mJavaScriptEnabled != paramBoolean)
            {
                this.mJavaScriptEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setLayoutAlgorithm(WebSettings.LayoutAlgorithm paramLayoutAlgorithm)
    {
        try
        {
            if (this.mLayoutAlgorithm != paramLayoutAlgorithm)
            {
                this.mLayoutAlgorithm = paramLayoutAlgorithm;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setLightTouchEnabled(boolean paramBoolean)
    {
        this.mLightTouchEnabled = paramBoolean;
    }

    /** @deprecated */
    public void setLinkPrefetchEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mLinkPrefetchEnabled != paramBoolean)
            {
                this.mLinkPrefetchEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setLoadWithOverviewMode(boolean paramBoolean)
    {
        this.mLoadWithOverviewMode = paramBoolean;
    }

    /** @deprecated */
    public void setLoadsImagesAutomatically(boolean paramBoolean)
    {
        try
        {
            if (this.mLoadsImagesAutomatically != paramBoolean)
            {
                this.mLoadsImagesAutomatically = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setMaximumDecodedImageSize(long paramLong)
    {
        if (this.mMaximumDecodedImageSize != paramLong)
        {
            this.mMaximumDecodedImageSize = paramLong;
            postSync();
        }
    }

    /** @deprecated */
    public void setMinimumFontSize(int paramInt)
    {
        try
        {
            int i = pin(paramInt);
            if (this.mMinimumFontSize != i)
            {
                this.mMinimumFontSize = i;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setMinimumLogicalFontSize(int paramInt)
    {
        try
        {
            int i = pin(paramInt);
            if (this.mMinimumLogicalFontSize != i)
            {
                this.mMinimumLogicalFontSize = i;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public void setNavDump(boolean paramBoolean)
    {
        this.mNavDump = paramBoolean;
    }

    public void setNeedInitialFocus(boolean paramBoolean)
    {
        if (this.mNeedInitialFocus != paramBoolean)
            this.mNeedInitialFocus = paramBoolean;
    }

    /** @deprecated */
    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setNightReadModeEnabled(boolean paramBoolean)
    {
        try
        {
            this.mNightReadMode = paramBoolean;
            postSync();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setPageCacheCapacity(int paramInt)
    {
        if (paramInt < 0)
            paramInt = 0;
        if (paramInt > 20)
            paramInt = 20;
        try
        {
            if (this.mPageCacheCapacity != paramInt)
            {
                this.mPageCacheCapacity = paramInt;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setPluginState(WebSettings.PluginState paramPluginState)
    {
        try
        {
            if (this.mPluginState != paramPluginState)
            {
                this.mPluginState = paramPluginState;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public void setPluginsEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        try
        {
            for (WebSettings.PluginState localPluginState = WebSettings.PluginState.ON; ; localPluginState = WebSettings.PluginState.OFF)
            {
                setPluginState(localPluginState);
                return;
            }
        }
        finally
        {
        }
    }

    @Deprecated
    public void setPluginsPath(String paramString)
    {
    }

    /** @deprecated */
    void setPrivateBrowsingEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mPrivateBrowsingEnabled != paramBoolean)
            {
                this.mPrivateBrowsingEnabled = paramBoolean;
                setAutoFillEnabled(this.mAutoFillEnabled);
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void setProperty(String paramString1, String paramString2)
    {
        if (this.mWebView.nativeSetProperty(paramString1, paramString2))
            this.mWebView.contentInvalidateAll();
    }

    /** @deprecated */
    public void setRenderPriority(WebSettings.RenderPriority paramRenderPriority)
    {
        try
        {
            if (this.mRenderPriority != paramRenderPriority)
            {
                this.mRenderPriority = paramRenderPriority;
                this.mEventHandler.sendMessage(Message.obtain(null, 1));
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setSansSerifFontFamily(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mSansSerifFontFamily))
            {
                this.mSansSerifFontFamily = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setSaveFormData(boolean paramBoolean)
    {
        this.mSaveFormData = paramBoolean;
    }

    public void setSavePassword(boolean paramBoolean)
    {
        this.mSavePassword = paramBoolean;
    }

    /** @deprecated */
    public void setSerifFontFamily(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mSerifFontFamily))
            {
                this.mSerifFontFamily = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setShowVisualIndicator(boolean paramBoolean)
    {
        try
        {
            if (this.mShowVisualIndicator != paramBoolean)
            {
                this.mShowVisualIndicator = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setShrinksStandaloneImagesToFit(boolean paramBoolean)
    {
        if (this.mShrinksStandaloneImagesToFit != paramBoolean)
        {
            this.mShrinksStandaloneImagesToFit = paramBoolean;
            postSync();
        }
    }

    /** @deprecated */
    public void setStandardFontFamily(String paramString)
    {
        if (paramString != null);
        try
        {
            if (!paramString.equals(this.mStandardFontFamily))
            {
                this.mStandardFontFamily = paramString;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setSupportMultipleWindows(boolean paramBoolean)
    {
        try
        {
            if (this.mSupportMultipleWindows != paramBoolean)
            {
                this.mSupportMultipleWindows = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setSupportZoom(boolean paramBoolean)
    {
        this.mSupportZoom = paramBoolean;
        this.mWebView.updateMultiTouchSupport(this.mContext);
    }

    /** @deprecated */
    void setSyntheticLinksEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mSyntheticLinksEnabled != paramBoolean)
            {
                this.mSyntheticLinksEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setTextSize(WebSettings.TextSize paramTextSize)
    {
        try
        {
            setTextZoom(paramTextSize.value);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setTextZoom(int paramInt)
    {
        try
        {
            if (this.mTextSize != paramInt)
            {
                if (WebViewClassic.mLogEvent)
                {
                    Object[] arrayOfObject = new Object[2];
                    arrayOfObject[0] = Integer.valueOf(this.mTextSize);
                    arrayOfObject[1] = Integer.valueOf(paramInt);
                    EventLog.writeEvent(70151, arrayOfObject);
                }
                this.mTextSize = paramInt;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public void setUseDoubleTree(boolean paramBoolean)
    {
    }

    @Deprecated
    public void setUseWebViewBackgroundForOverscrollBackground(boolean paramBoolean)
    {
        this.mUseWebViewBackgroundForOverscroll = paramBoolean;
    }

    /** @deprecated */
    public void setUseWideViewPort(boolean paramBoolean)
    {
        try
        {
            if (this.mUseWideViewport != paramBoolean)
            {
                this.mUseWideViewport = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public void setUserAgent(int paramInt)
    {
        String str = null;
        if (paramInt == 1);
        while (true)
        {
            try
            {
                boolean bool = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24".equals(this.mUserAgent);
                if (bool)
                    return;
                str = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24";
                setUserAgentString(str);
                continue;
            }
            finally
            {
            }
            if (paramInt == 2)
            {
                if (!"Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16".equals(this.mUserAgent))
                    str = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16";
            }
            else if (paramInt == 0);
        }
    }

    /** @deprecated */
    public void setUserAgentString(String paramString)
    {
        if (paramString != null);
        while (true)
        {
            try
            {
                if (paramString.length() == 0)
                    synchronized (sLockForLocaleSettings)
                    {
                        Locale localLocale = Locale.getDefault();
                        if (!sLocale.equals(localLocale))
                        {
                            sLocale = localLocale;
                            this.mAcceptLanguage = getCurrentAcceptLanguage();
                        }
                        paramString = getCurrentUserAgent();
                        this.mUseDefaultUserAgent = true;
                        if (!paramString.equals(this.mUserAgent))
                        {
                            this.mUserAgent = paramString;
                            postSync();
                        }
                        return;
                    }
            }
            finally
            {
            }
            this.mUseDefaultUserAgent = false;
        }
    }

    /** @deprecated */
    public void setWorkersEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mWorkersEnabled != paramBoolean)
            {
                this.mWorkersEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setXSSAuditorEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mXSSAuditorEnabled != paramBoolean)
            {
                this.mXSSAuditorEnabled = paramBoolean;
                postSync();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean supportMultipleWindows()
    {
        try
        {
            boolean bool = this.mSupportMultipleWindows;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean supportZoom()
    {
        return this.mSupportZoom;
    }

    /** @deprecated */
    void syncSettingsAndCreateHandler(BrowserFrame paramBrowserFrame)
    {
        try
        {
            this.mBrowserFrame = paramBrowserFrame;
            SharedPreferences localSharedPreferences = this.mContext.getSharedPreferences("WebViewSettings", 0);
            if (mDoubleTapToastCount > 0)
                mDoubleTapToastCount = localSharedPreferences.getInt("double_tap_toast_count", mDoubleTapToastCount);
            nativeSync(paramBrowserFrame.mNativeFrame);
            this.mSyncPending = false;
            this.mEventHandler.createHandler();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private class EventHandler
    {
        static final int PRIORITY = 1;
        static final int SET_DOUBLE_TAP_TOAST_COUNT = 2;
        static final int SYNC;
        private Handler mHandler;

        private EventHandler()
        {
        }

        /** @deprecated */
        private void createHandler()
        {
            try
            {
                setRenderPriority();
                this.mHandler = new Handler()
                {
                    public void handleMessage(Message paramAnonymousMessage)
                    {
                        switch (paramAnonymousMessage.what)
                        {
                        default:
                        case 0:
                        case 1:
                        case 2:
                        }
                        while (true)
                        {
                            return;
                            synchronized (WebSettingsClassic.this)
                            {
                                if (WebSettingsClassic.this.mBrowserFrame.mNativeFrame != 0)
                                    WebSettingsClassic.this.nativeSync(WebSettingsClassic.this.mBrowserFrame.mNativeFrame);
                                WebSettingsClassic.access$202(WebSettingsClassic.this, false);
                            }
                            WebSettingsClassic.EventHandler.this.setRenderPriority();
                            continue;
                            SharedPreferences.Editor localEditor = WebSettingsClassic.this.mContext.getSharedPreferences("WebViewSettings", 0).edit();
                            localEditor.putInt("double_tap_toast_count", WebSettingsClassic.mDoubleTapToastCount);
                            localEditor.commit();
                        }
                    }
                };
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        private boolean sendMessage(Message paramMessage)
        {
            try
            {
                if (this.mHandler != null)
                {
                    this.mHandler.sendMessage(paramMessage);
                    bool = true;
                    return bool;
                }
                boolean bool = false;
            }
            finally
            {
            }
        }

        // ERROR //
        private void setRenderPriority()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 23	android/webkit/WebSettingsClassic$EventHandler:this$0	Landroid/webkit/WebSettingsClassic;
            //     4: astore_1
            //     5: aload_1
            //     6: monitorenter
            //     7: aload_0
            //     8: getfield 23	android/webkit/WebSettingsClassic$EventHandler:this$0	Landroid/webkit/WebSettingsClassic;
            //     11: invokestatic 55	android/webkit/WebSettingsClassic:access$600	(Landroid/webkit/WebSettingsClassic;)Landroid/webkit/WebSettings$RenderPriority;
            //     14: getstatic 61	android/webkit/WebSettings$RenderPriority:NORMAL	Landroid/webkit/WebSettings$RenderPriority;
            //     17: if_acmpne +10 -> 27
            //     20: iconst_0
            //     21: invokestatic 67	android/os/Process:setThreadPriority	(I)V
            //     24: aload_1
            //     25: monitorexit
            //     26: return
            //     27: aload_0
            //     28: getfield 23	android/webkit/WebSettingsClassic$EventHandler:this$0	Landroid/webkit/WebSettingsClassic;
            //     31: invokestatic 55	android/webkit/WebSettingsClassic:access$600	(Landroid/webkit/WebSettingsClassic;)Landroid/webkit/WebSettings$RenderPriority;
            //     34: getstatic 70	android/webkit/WebSettings$RenderPriority:HIGH	Landroid/webkit/WebSettings$RenderPriority;
            //     37: if_acmpne +16 -> 53
            //     40: bipush 255
            //     42: invokestatic 67	android/os/Process:setThreadPriority	(I)V
            //     45: goto -21 -> 24
            //     48: astore_2
            //     49: aload_1
            //     50: monitorexit
            //     51: aload_2
            //     52: athrow
            //     53: aload_0
            //     54: getfield 23	android/webkit/WebSettingsClassic$EventHandler:this$0	Landroid/webkit/WebSettingsClassic;
            //     57: invokestatic 55	android/webkit/WebSettingsClassic:access$600	(Landroid/webkit/WebSettingsClassic;)Landroid/webkit/WebSettings$RenderPriority;
            //     60: getstatic 73	android/webkit/WebSettings$RenderPriority:LOW	Landroid/webkit/WebSettings$RenderPriority;
            //     63: if_acmpne -39 -> 24
            //     66: bipush 10
            //     68: invokestatic 67	android/os/Process:setThreadPriority	(I)V
            //     71: goto -47 -> 24
            //
            // Exception table:
            //     from	to	target	type
            //     7	51	48	finally
            //     53	71	48	finally
        }
    }

    public static class AutoFillProfile
    {
        private String mAddressLine1;
        private String mAddressLine2;
        private String mCity;
        private String mCompanyName;
        private String mCountry;
        private String mEmailAddress;
        private String mFullName;
        private String mPhoneNumber;
        private String mState;
        private int mUniqueId;
        private String mZipCode;

        public AutoFillProfile(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10)
        {
            this.mUniqueId = paramInt;
            this.mFullName = paramString1;
            this.mEmailAddress = paramString2;
            this.mCompanyName = paramString3;
            this.mAddressLine1 = paramString4;
            this.mAddressLine2 = paramString5;
            this.mCity = paramString6;
            this.mState = paramString7;
            this.mZipCode = paramString8;
            this.mCountry = paramString9;
            this.mPhoneNumber = paramString10;
        }

        public String getAddressLine1()
        {
            return this.mAddressLine1;
        }

        public String getAddressLine2()
        {
            return this.mAddressLine2;
        }

        public String getCity()
        {
            return this.mCity;
        }

        public String getCompanyName()
        {
            return this.mCompanyName;
        }

        public String getCountry()
        {
            return this.mCountry;
        }

        public String getEmailAddress()
        {
            return this.mEmailAddress;
        }

        public String getFullName()
        {
            return this.mFullName;
        }

        public String getPhoneNumber()
        {
            return this.mPhoneNumber;
        }

        public String getState()
        {
            return this.mState;
        }

        public int getUniqueId()
        {
            return this.mUniqueId;
        }

        public String getZipCode()
        {
            return this.mZipCode;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebSettingsClassic
 * JD-Core Version:        0.6.2
 */