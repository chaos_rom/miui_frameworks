package android.webkit;

import android.graphics.PointF;

class QuadF
{
    public PointF p1 = new PointF();
    public PointF p2 = new PointF();
    public PointF p3 = new PointF();
    public PointF p4 = new PointF();

    private static boolean isPointInTriangle(float paramFloat1, float paramFloat2, PointF paramPointF1, PointF paramPointF2, PointF paramPointF3)
    {
        float f1 = paramPointF1.x - paramPointF3.x;
        float f2 = paramPointF1.y - paramPointF3.y;
        float f3 = paramPointF2.x - paramPointF3.x;
        float f4 = paramPointF2.y - paramPointF3.y;
        float f5 = paramFloat1 - paramPointF3.x;
        float f6 = paramFloat2 - paramPointF3.y;
        float f7 = f4 * f1 - f3 * f2;
        float f8 = (f4 * f5 - f3 * f6) / f7;
        float f9 = (f1 * f6 - f2 * f5) / f7;
        float f10 = 1.0F - f8 - f9;
        if ((f8 >= 0.0F) && (f9 >= 0.0F) && (f10 >= 0.0F));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean containsPoint(float paramFloat1, float paramFloat2)
    {
        if ((isPointInTriangle(paramFloat1, paramFloat2, this.p1, this.p2, this.p3)) || (isPointInTriangle(paramFloat1, paramFloat2, this.p1, this.p3, this.p4)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void offset(float paramFloat1, float paramFloat2)
    {
        this.p1.offset(paramFloat1, paramFloat2);
        this.p2.offset(paramFloat1, paramFloat2);
        this.p3.offset(paramFloat1, paramFloat2);
        this.p4.offset(paramFloat1, paramFloat2);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("QuadF(");
        localStringBuilder.append(this.p1.x).append(",").append(this.p1.y);
        localStringBuilder.append(" - ");
        localStringBuilder.append(this.p2.x).append(",").append(this.p2.y);
        localStringBuilder.append(" - ");
        localStringBuilder.append(this.p3.x).append(",").append(this.p3.y);
        localStringBuilder.append(" - ");
        localStringBuilder.append(this.p4.x).append(",").append(this.p4.y);
        localStringBuilder.append(")");
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.QuadF
 * JD-Core Version:        0.6.2
 */