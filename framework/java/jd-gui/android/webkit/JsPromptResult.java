package android.webkit;

public class JsPromptResult extends JsResult
{
    private String mStringResult;

    public JsPromptResult(JsResult.ResultReceiver paramResultReceiver)
    {
        super(paramResultReceiver);
    }

    public void confirm(String paramString)
    {
        this.mStringResult = paramString;
        confirm();
    }

    public String getStringResult()
    {
        return this.mStringResult;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.JsPromptResult
 * JD-Core Version:        0.6.2
 */