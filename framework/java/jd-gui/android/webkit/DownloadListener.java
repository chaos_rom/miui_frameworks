package android.webkit;

public abstract interface DownloadListener
{
    public abstract void onDownloadStart(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.DownloadListener
 * JD-Core Version:        0.6.2
 */