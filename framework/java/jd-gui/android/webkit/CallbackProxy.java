package android.webkit;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

class CallbackProxy extends Handler
{
    private static final int ADD_HISTORY_ITEM = 135;
    private static final int ADD_MESSAGE_TO_CONSOLE = 129;
    private static final int ASYNC_KEYEVENTS = 116;
    private static final int AUTH_CREDENTIALS = 137;
    private static final int AUTH_REQUEST = 104;
    private static final int AUTO_LOGIN = 140;
    private static final int CLIENT_CERT_REQUEST = 141;
    private static final int CLOSE_WINDOW = 110;
    private static final int CREATE_WINDOW = 109;
    private static final int DOWNLOAD_FILE = 118;
    private static final int EXCEEDED_DATABASE_QUOTA = 126;
    private static final int GEOLOCATION_PERMISSIONS_HIDE_PROMPT = 131;
    private static final int GEOLOCATION_PERMISSIONS_SHOW_PROMPT = 130;
    private static final int GET_VISITED_HISTORY = 133;
    private static final int HISTORY_INDEX_CHANGED = 136;
    private static final int JS_ALERT = 112;
    private static final int JS_CONFIRM = 113;
    private static final int JS_PROMPT = 114;
    private static final int JS_TIMEOUT = 128;
    private static final int JS_UNLOAD = 115;
    private static final int LOAD_RESOURCE = 108;
    private static final String LOGTAG = "CallbackProxy";

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private static final int MAIN_FRAME_FINISH_PARSING = 210;
    private static final int NOTIFY = 200;
    private static final int NOTIFY_SEARCHBOX_LISTENERS = 139;
    private static final int OPEN_FILE_CHOOSER = 134;
    private static final int OVERRIDE_URL = 103;
    private static final int PAGE_FINISHED = 121;
    private static final int PAGE_STARTED = 100;
    private static final boolean PERF_PROBE = false;
    private static final int PROCEEDED_AFTER_SSL_ERROR = 144;
    private static final int PROGRESS = 106;
    private static final int REACHED_APPCACHE_MAXSIZE = 127;
    private static final int RECEIVED_CERTIFICATE = 124;
    private static final int RECEIVED_ICON = 101;
    private static final int RECEIVED_TITLE = 102;
    private static final int RECEIVED_TOUCH_ICON_URL = 132;
    private static final int REPORT_ERROR = 119;
    private static final int REQUEST_FOCUS = 122;
    private static final int RESEND_POST_DATA = 120;
    private static final int SAVE_PASSWORD = 111;
    private static final int SCALE_CHANGED = 123;
    private static final int SEARCHBOX_DISPATCH_COMPLETE_CALLBACK = 143;
    private static final int SEARCHBOX_IS_SUPPORTED_CALLBACK = 142;
    private static final int SET_INSTALLABLE_WEBAPP = 138;
    private static final int SSL_ERROR = 105;
    private static final int SWITCH_OUT_HISTORY = 125;
    private static final int UPDATE_VISITED = 107;
    private final WebBackForwardList mBackForwardList;
    private boolean mBlockMessages;
    private final Context mContext;
    private volatile DownloadListener mDownloadListener;
    private volatile int mLatestProgress = 100;
    private boolean mProgressUpdatePending;
    private volatile WebBackForwardListClient mWebBackForwardListClient;
    private volatile WebChromeClient mWebChromeClient;
    private long mWebCoreIdleTime;
    private long mWebCoreThreadTime;
    private final WebViewClassic mWebView;
    private volatile WebViewClient mWebViewClient;

    public CallbackProxy(Context paramContext, WebViewClassic paramWebViewClassic)
    {
        this.mContext = paramContext;
        this.mWebView = paramWebViewClassic;
        this.mBackForwardList = new WebBackForwardList(this);
    }

    private String getJsDialogTitle(String paramString)
    {
        Object localObject = paramString;
        if (URLUtil.isDataUrl(paramString))
            localObject = this.mContext.getString(17040190);
        while (true)
        {
            return localObject;
            try
            {
                URL localURL = new URL(paramString);
                Context localContext = this.mContext;
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = (localURL.getProtocol() + "://" + localURL.getHost());
                String str = localContext.getString(17040189, arrayOfObject);
                localObject = str;
            }
            catch (MalformedURLException localMalformedURLException)
            {
            }
        }
    }

    /** @deprecated */
    private void sendMessageToUiThreadSync(Message paramMessage)
    {
        try
        {
            sendMessage(paramMessage);
            WebCoreThreadWatchdog.pause();
            try
            {
                wait();
                WebCoreThreadWatchdog.resume();
                return;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                {
                    Log.e("CallbackProxy", "Caught exception waiting for synchronous UI message to be processed");
                    Log.e("CallbackProxy", Log.getStackTraceString(localInterruptedException));
                }
            }
        }
        finally
        {
        }
    }

    public void addMessageToConsole(String paramString1, int paramInt1, String paramString2, int paramInt2)
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(129);
            localMessage.getData().putString("message", paramString1);
            localMessage.getData().putString("sourceID", paramString2);
            localMessage.getData().putInt("lineNumber", paramInt1);
            localMessage.getData().putInt("msgLevel", paramInt2);
            sendMessage(localMessage);
        }
    }

    /** @deprecated */
    protected void blockMessages()
    {
        try
        {
            this.mBlockMessages = true;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    boolean canShowAlertDialog()
    {
        return this.mContext instanceof Activity;
    }

    public BrowserFrame createWindow(boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 1;
        BrowserFrame localBrowserFrame;
        if (this.mWebChromeClient == null)
            localBrowserFrame = null;
        while (true)
        {
            return localBrowserFrame;
            WebView localWebView = this.mWebView.getWebView();
            localWebView.getClass();
            WebView.WebViewTransport localWebViewTransport = new WebView.WebViewTransport(localWebView);
            Message localMessage = obtainMessage(200);
            localMessage.obj = localWebViewTransport;
            int j;
            if (paramBoolean1)
            {
                j = i;
                label64: if (!paramBoolean2)
                    break label131;
            }
            while (true)
            {
                sendMessageToUiThreadSync(obtainMessage(109, j, i, localMessage));
                WebViewClassic localWebViewClassic = WebViewClassic.fromWebView(localWebViewTransport.getWebView());
                if (localWebViewClassic == null)
                    break label136;
                WebViewCore localWebViewCore = localWebViewClassic.getWebViewCore();
                if (localWebViewCore == null)
                    break label136;
                localWebViewCore.initializeSubwindow();
                localBrowserFrame = localWebViewCore.getBrowserFrame();
                break;
                j = 0;
                break label64;
                label131: i = 0;
            }
            label136: localBrowserFrame = null;
        }
    }

    public void doUpdateVisitedHistory(String paramString, boolean paramBoolean)
    {
        if (this.mWebViewClient == null)
            return;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            sendMessage(obtainMessage(107, i, 0, paramString));
            break;
        }
    }

    public WebBackForwardList getBackForwardList()
    {
        return this.mBackForwardList;
    }

    public int getProgress()
    {
        return this.mLatestProgress;
    }

    public void getVisitedHistory(ValueCallback<String[]> paramValueCallback)
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(133);
            localMessage.obj = paramValueCallback;
            sendMessage(localMessage);
        }
    }

    WebBackForwardListClient getWebBackForwardListClient()
    {
        return this.mWebBackForwardListClient;
    }

    public WebChromeClient getWebChromeClient()
    {
        return this.mWebChromeClient;
    }

    public WebViewClient getWebViewClient()
    {
        return this.mWebViewClient;
    }

    public void handleMessage(Message paramMessage)
    {
        if (messagesBlocked());
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            case 117:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 159:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 167:
            case 168:
            case 169:
            case 170:
            case 171:
            case 172:
            case 173:
            case 174:
            case 175:
            case 176:
            case 177:
            case 178:
            case 179:
            case 180:
            case 181:
            case 182:
            case 183:
            case 184:
            case 185:
            case 186:
            case 187:
            case 188:
            case 189:
            case 190:
            case 191:
            case 192:
            case 193:
            case 194:
            case 195:
            case 196:
            case 197:
            case 198:
            case 199:
            case 201:
            case 202:
            case 203:
            case 204:
            case 205:
            case 206:
            case 207:
            case 208:
            case 209:
            default:
                break;
            case 100:
                String str38 = paramMessage.getData().getString("url");
                this.mWebView.onPageStarted(str38);
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onPageStarted(this.mWebView.getWebView(), str38, (Bitmap)paramMessage.obj);
                break;
            case 121:
                String str37 = (String)paramMessage.obj;
                this.mWebView.onPageFinished(str37);
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onPageFinished(this.mWebView.getWebView(), str37);
                break;
            case 210:
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onMainFrameFinishParsing(this.mWebView.getWebView());
                break;
            case 101:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.onReceivedIcon(this.mWebView.getWebView(), (Bitmap)paramMessage.obj);
                break;
            case 132:
                if (this.mWebChromeClient != null)
                {
                    WebChromeClient localWebChromeClient2 = this.mWebChromeClient;
                    WebView localWebView3 = this.mWebView.getWebView();
                    String str36 = (String)paramMessage.obj;
                    if (paramMessage.arg1 == 1);
                    for (boolean bool5 = true; ; bool5 = false)
                    {
                        localWebChromeClient2.onReceivedTouchIconUrl(localWebView3, str36, bool5);
                        break;
                    }
                }
                break;
            case 102:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.onReceivedTitle(this.mWebView.getWebView(), (String)paramMessage.obj);
                break;
            case 119:
                if (this.mWebViewClient != null)
                {
                    int m = paramMessage.arg1;
                    String str34 = paramMessage.getData().getString("description");
                    String str35 = paramMessage.getData().getString("failingUrl");
                    this.mWebViewClient.onReceivedError(this.mWebView.getWebView(), m, str34, str35);
                }
                break;
            case 120:
                Message localMessage1 = (Message)paramMessage.getData().getParcelable("resend");
                Message localMessage2 = (Message)paramMessage.getData().getParcelable("dontResend");
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onFormResubmission(this.mWebView.getWebView(), localMessage2, localMessage1);
                else
                    localMessage2.sendToTarget();
                break;
            case 103:
                boolean bool4 = uiOverrideUrlLoading(paramMessage.getData().getString("url"));
                ResultTransport localResultTransport = (ResultTransport)paramMessage.obj;
                try
                {
                    localResultTransport.setResult(Boolean.valueOf(bool4));
                    notify();
                }
                finally
                {
                    localObject5 = finally;
                    throw localObject5;
                }
            case 104:
                if (this.mWebViewClient != null)
                {
                    HttpAuthHandler localHttpAuthHandler = (HttpAuthHandler)paramMessage.obj;
                    String str32 = paramMessage.getData().getString("host");
                    String str33 = paramMessage.getData().getString("realm");
                    this.mWebViewClient.onReceivedHttpAuthRequest(this.mWebView.getWebView(), localHttpAuthHandler, str32, str33);
                }
                break;
            case 105:
                if (this.mWebViewClient != null)
                {
                    HashMap localHashMap5 = (HashMap)paramMessage.obj;
                    this.mWebViewClient.onReceivedSslError(this.mWebView.getWebView(), (SslErrorHandler)localHashMap5.get("handler"), (SslError)localHashMap5.get("error"));
                }
                break;
            case 144:
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onProceededAfterSslError(this.mWebView.getWebView(), (SslError)paramMessage.obj);
                break;
            case 141:
                if (this.mWebViewClient != null)
                {
                    HashMap localHashMap4 = (HashMap)paramMessage.obj;
                    this.mWebViewClient.onReceivedClientCertRequest(this.mWebView.getWebView(), (ClientCertRequestHandler)localHashMap4.get("handler"), (String)localHashMap4.get("host_and_port"));
                }
                break;
            case 106:
                try
                {
                    if (this.mWebChromeClient != null)
                        this.mWebChromeClient.onProgressChanged(this.mWebView.getWebView(), this.mLatestProgress);
                    this.mProgressUpdatePending = false;
                }
                finally
                {
                    localObject4 = finally;
                    throw localObject4;
                }
            case 107:
                if (this.mWebViewClient != null)
                {
                    WebViewClient localWebViewClient = this.mWebViewClient;
                    WebView localWebView2 = this.mWebView.getWebView();
                    String str31 = (String)paramMessage.obj;
                    if (paramMessage.arg1 != 0);
                    for (boolean bool3 = true; ; bool3 = false)
                    {
                        localWebViewClient.doUpdateVisitedHistory(localWebView2, str31, bool3);
                        break;
                    }
                }
                break;
            case 108:
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onLoadResource(this.mWebView.getWebView(), (String)paramMessage.obj);
                break;
            case 118:
                if (this.mDownloadListener != null)
                {
                    String str27 = paramMessage.getData().getString("url");
                    String str28 = paramMessage.getData().getString("userAgent");
                    String str29 = paramMessage.getData().getString("contentDisposition");
                    String str30 = paramMessage.getData().getString("mimetype");
                    Long localLong = Long.valueOf(paramMessage.getData().getLong("contentLength"));
                    this.mDownloadListener.onDownloadStart(str27, str28, str29, str30, localLong.longValue());
                }
                break;
            case 109:
                if (this.mWebChromeClient != null)
                {
                    WebChromeClient localWebChromeClient1 = this.mWebChromeClient;
                    WebView localWebView1 = this.mWebView.getWebView();
                    boolean bool1;
                    boolean bool2;
                    if (paramMessage.arg1 == 1)
                    {
                        bool1 = true;
                        if (paramMessage.arg2 != 1)
                            break label1424;
                        bool2 = true;
                        if (localWebChromeClient1.onCreateWindow(localWebView1, bool1, bool2, (Message)paramMessage.obj));
                    }
                    try
                    {
                        notify();
                        this.mWebView.dismissZoomControl();
                        continue;
                        bool1 = false;
                        break label1368;
                        bool2 = false;
                        break label1379;
                    }
                    finally
                    {
                    }
                }
                break;
            case 122:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.onRequestFocus(this.mWebView.getWebView());
                break;
            case 110:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.onCloseWindow(((WebViewClassic)paramMessage.obj).getWebView());
                break;
            case 111:
                Bundle localBundle = paramMessage.getData();
                String str24 = localBundle.getString("host");
                String str25 = localBundle.getString("username");
                String str26 = localBundle.getString("password");
                if (!this.mWebView.onSavePassword(str24, str25, str26, (Message)paramMessage.obj))
                    try
                    {
                        notify();
                    }
                    finally
                    {
                        localObject2 = finally;
                        throw localObject2;
                    }
                break;
            case 116:
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onUnhandledKeyEvent(this.mWebView.getWebView(), (KeyEvent)paramMessage.obj);
                break;
            case 126:
                if (this.mWebChromeClient != null)
                {
                    HashMap localHashMap3 = (HashMap)paramMessage.obj;
                    String str22 = (String)localHashMap3.get("databaseIdentifier");
                    String str23 = (String)localHashMap3.get("url");
                    long l3 = ((Long)localHashMap3.get("quota")).longValue();
                    long l4 = ((Long)localHashMap3.get("totalQuota")).longValue();
                    long l5 = ((Long)localHashMap3.get("estimatedDatabaseSize")).longValue();
                    WebStorage.QuotaUpdater localQuotaUpdater2 = (WebStorage.QuotaUpdater)localHashMap3.get("quotaUpdater");
                    this.mWebChromeClient.onExceededDatabaseQuota(str23, str22, l3, l5, l4, localQuotaUpdater2);
                }
                break;
            case 127:
                if (this.mWebChromeClient != null)
                {
                    HashMap localHashMap2 = (HashMap)paramMessage.obj;
                    long l1 = ((Long)localHashMap2.get("requiredStorage")).longValue();
                    long l2 = ((Long)localHashMap2.get("quota")).longValue();
                    WebStorage.QuotaUpdater localQuotaUpdater1 = (WebStorage.QuotaUpdater)localHashMap2.get("quotaUpdater");
                    this.mWebChromeClient.onReachedMaxAppCacheSize(l1, l2, localQuotaUpdater1);
                }
                break;
            case 130:
                if (this.mWebChromeClient != null)
                {
                    HashMap localHashMap1 = (HashMap)paramMessage.obj;
                    String str21 = (String)localHashMap1.get("origin");
                    GeolocationPermissions.Callback localCallback = (GeolocationPermissions.Callback)localHashMap1.get("callback");
                    this.mWebChromeClient.onGeolocationPermissionsShowPrompt(str21, localCallback);
                }
                break;
            case 131:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.onGeolocationPermissionsHidePrompt();
                break;
            case 112:
                if (this.mWebChromeClient != null)
                {
                    JsResultReceiver localJsResultReceiver5 = (JsResultReceiver)paramMessage.obj;
                    final JsPromptResult localJsPromptResult5 = localJsResultReceiver5.mJsResult;
                    String str19 = paramMessage.getData().getString("message");
                    String str20 = paramMessage.getData().getString("url");
                    if (!this.mWebChromeClient.onJsAlert(this.mWebView.getWebView(), str20, str19, localJsPromptResult5))
                    {
                        if (!canShowAlertDialog())
                        {
                            localJsPromptResult5.cancel();
                            localJsResultReceiver5.setReady();
                        }
                        else
                        {
                            new AlertDialog.Builder(this.mContext).setTitle(getJsDialogTitle(str20)).setMessage(str19).setPositiveButton(17039370, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult5.confirm();
                                }
                            }).setOnCancelListener(new DialogInterface.OnCancelListener()
                            {
                                public void onCancel(DialogInterface paramAnonymousDialogInterface)
                                {
                                    localJsPromptResult5.cancel();
                                }
                            }).show();
                        }
                    }
                    else
                        localJsResultReceiver5.setReady();
                }
                break;
            case 113:
                if (this.mWebChromeClient != null)
                {
                    JsResultReceiver localJsResultReceiver4 = (JsResultReceiver)paramMessage.obj;
                    final JsPromptResult localJsPromptResult4 = localJsResultReceiver4.mJsResult;
                    String str17 = paramMessage.getData().getString("message");
                    String str18 = paramMessage.getData().getString("url");
                    if (!this.mWebChromeClient.onJsConfirm(this.mWebView.getWebView(), str18, str17, localJsPromptResult4))
                    {
                        if (!canShowAlertDialog())
                        {
                            localJsPromptResult4.cancel();
                            localJsResultReceiver4.setReady();
                        }
                        else
                        {
                            new AlertDialog.Builder(this.mContext).setTitle(getJsDialogTitle(str18)).setMessage(str17).setPositiveButton(17039370, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult4.confirm();
                                }
                            }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult4.cancel();
                                }
                            }).setOnCancelListener(new DialogInterface.OnCancelListener()
                            {
                                public void onCancel(DialogInterface paramAnonymousDialogInterface)
                                {
                                    localJsPromptResult4.cancel();
                                }
                            }).show();
                        }
                    }
                    else
                        localJsResultReceiver4.setReady();
                }
                break;
            case 114:
                if (this.mWebChromeClient != null)
                {
                    JsResultReceiver localJsResultReceiver3 = (JsResultReceiver)paramMessage.obj;
                    final JsPromptResult localJsPromptResult3 = localJsResultReceiver3.mJsResult;
                    String str14 = paramMessage.getData().getString("message");
                    String str15 = paramMessage.getData().getString("default");
                    String str16 = paramMessage.getData().getString("url");
                    if (!this.mWebChromeClient.onJsPrompt(this.mWebView.getWebView(), str16, str14, str15, localJsPromptResult3))
                    {
                        if (!canShowAlertDialog())
                        {
                            localJsPromptResult3.cancel();
                            localJsResultReceiver3.setReady();
                        }
                        else
                        {
                            View localView = LayoutInflater.from(this.mContext).inflate(17367114, null);
                            final EditText localEditText = (EditText)localView.findViewById(16908946);
                            localEditText.setText(str15);
                            ((TextView)localView.findViewById(16908299)).setText(str14);
                            new AlertDialog.Builder(this.mContext).setTitle(getJsDialogTitle(str16)).setView(localView).setPositiveButton(17039370, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult3.confirm(localEditText.getText().toString());
                                }
                            }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult3.cancel();
                                }
                            }).setOnCancelListener(new DialogInterface.OnCancelListener()
                            {
                                public void onCancel(DialogInterface paramAnonymousDialogInterface)
                                {
                                    localJsPromptResult3.cancel();
                                }
                            }).show();
                        }
                    }
                    else
                        localJsResultReceiver3.setReady();
                }
                break;
            case 115:
                if (this.mWebChromeClient != null)
                {
                    JsResultReceiver localJsResultReceiver2 = (JsResultReceiver)paramMessage.obj;
                    final JsPromptResult localJsPromptResult2 = localJsResultReceiver2.mJsResult;
                    String str11 = paramMessage.getData().getString("message");
                    String str12 = paramMessage.getData().getString("url");
                    if (!this.mWebChromeClient.onJsBeforeUnload(this.mWebView.getWebView(), str12, str11, localJsPromptResult2))
                    {
                        if (!canShowAlertDialog())
                        {
                            localJsPromptResult2.cancel();
                            localJsResultReceiver2.setReady();
                        }
                        else
                        {
                            Context localContext = this.mContext;
                            Object[] arrayOfObject = new Object[1];
                            arrayOfObject[0] = str11;
                            String str13 = localContext.getString(17040191, arrayOfObject);
                            new AlertDialog.Builder(this.mContext).setMessage(str13).setPositiveButton(17039370, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult2.confirm();
                                }
                            }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                                {
                                    localJsPromptResult2.cancel();
                                }
                            }).show();
                        }
                    }
                    else
                        localJsResultReceiver2.setReady();
                }
                break;
            case 128:
                if (this.mWebChromeClient != null)
                {
                    JsResultReceiver localJsResultReceiver1 = (JsResultReceiver)paramMessage.obj;
                    JsPromptResult localJsPromptResult1 = localJsResultReceiver1.mJsResult;
                    if (this.mWebChromeClient.onJsTimeout())
                        localJsPromptResult1.confirm();
                    while (true)
                    {
                        localJsResultReceiver1.setReady();
                        break;
                        localJsPromptResult1.cancel();
                    }
                }
                break;
            case 124:
                this.mWebView.setCertificate((SslCertificate)paramMessage.obj);
                break;
            case 200:
                try
                {
                    notify();
                }
                finally
                {
                    localObject1 = finally;
                    throw localObject1;
                }
            case 123:
                if (this.mWebViewClient != null)
                    this.mWebViewClient.onScaleChanged(this.mWebView.getWebView(), paramMessage.getData().getFloat("old"), paramMessage.getData().getFloat("new"));
                break;
            case 125:
                this.mWebView.switchOutDrawHistory();
                break;
            case 129:
                if (this.mWebChromeClient != null)
                {
                    String str8 = paramMessage.getData().getString("message");
                    String str9 = paramMessage.getData().getString("sourceID");
                    int i = paramMessage.getData().getInt("lineNumber");
                    int j = paramMessage.getData().getInt("msgLevel");
                    int k = ConsoleMessage.MessageLevel.values().length;
                    if ((j < 0) || (j >= k))
                        j = 0;
                    ConsoleMessage.MessageLevel localMessageLevel = ConsoleMessage.MessageLevel.values()[j];
                    if (!this.mWebChromeClient.onConsoleMessage(new ConsoleMessage(str8, str9, i, localMessageLevel)))
                    {
                        String str10 = str8 + " at " + str9 + ":" + i;
                        switch (11.$SwitchMap$android$webkit$ConsoleMessage$MessageLevel[localMessageLevel.ordinal()])
                        {
                        default:
                            break;
                        case 1:
                            Log.v("Web Console", str10);
                            break;
                        case 2:
                            Log.i("Web Console", str10);
                            break;
                        case 3:
                            Log.w("Web Console", str10);
                            break;
                        case 4:
                            Log.e("Web Console", str10);
                            break;
                        case 5:
                            Log.d("Web Console", str10);
                        }
                    }
                }
                break;
            case 133:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.getVisitedHistory((ValueCallback)paramMessage.obj);
                break;
            case 134:
                if (this.mWebChromeClient != null)
                {
                    UploadFileMessageData localUploadFileMessageData = (UploadFileMessageData)paramMessage.obj;
                    this.mWebChromeClient.openFileChooser(localUploadFileMessageData.getUploadFile(), localUploadFileMessageData.getAcceptType(), localUploadFileMessageData.getCapture());
                }
                break;
            case 135:
                if (this.mWebBackForwardListClient != null)
                    this.mWebBackForwardListClient.onNewHistoryItem((WebHistoryItem)paramMessage.obj);
                break;
            case 136:
                if (this.mWebBackForwardListClient != null)
                    this.mWebBackForwardListClient.onIndexChanged((WebHistoryItem)paramMessage.obj, paramMessage.arg1);
                break;
            case 137:
                String str4 = paramMessage.getData().getString("host");
                String str5 = paramMessage.getData().getString("realm");
                String str6 = paramMessage.getData().getString("username");
                String str7 = paramMessage.getData().getString("password");
                this.mWebView.setHttpAuthUsernamePassword(str4, str5, str6, str7);
                break;
            case 138:
                if (this.mWebChromeClient != null)
                    this.mWebChromeClient.setInstallableWebApp();
                break;
            case 139:
                SearchBoxImpl localSearchBoxImpl2 = (SearchBoxImpl)this.mWebView.getSearchBox();
                List localList = (List)paramMessage.obj;
                localSearchBoxImpl2.handleSuggestions(paramMessage.getData().getString("query"), localList);
                break;
            case 140:
                if (this.mWebViewClient != null)
                {
                    String str1 = paramMessage.getData().getString("realm");
                    String str2 = paramMessage.getData().getString("account");
                    String str3 = paramMessage.getData().getString("args");
                    this.mWebViewClient.onReceivedLoginRequest(this.mWebView.getWebView(), str1, str2, str3);
                }
                break;
            case 142:
                ((SearchBoxImpl)this.mWebView.getSearchBox()).handleIsSupportedCallback(((Boolean)paramMessage.obj).booleanValue());
                break;
            case 143:
                label1368: label1379: SearchBoxImpl localSearchBoxImpl1 = (SearchBoxImpl)this.mWebView.getSearchBox();
                label1424: Boolean localBoolean = (Boolean)paramMessage.obj;
                localSearchBoxImpl1.handleDispatchCompleteCallback(paramMessage.getData().getString("function"), paramMessage.getData().getInt("id"), localBoolean.booleanValue());
            }
        }
    }

    /** @deprecated */
    protected boolean messagesBlocked()
    {
        try
        {
            boolean bool = this.mBlockMessages;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void onCloseWindow(WebViewClassic paramWebViewClassic)
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(110, paramWebViewClassic));
        }
    }

    public boolean onDownloadStart(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong)
    {
        if (this.mDownloadListener == null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            Message localMessage = obtainMessage(118);
            Bundle localBundle = localMessage.getData();
            localBundle.putString("url", paramString1);
            localBundle.putString("userAgent", paramString2);
            localBundle.putString("mimetype", paramString4);
            localBundle.putLong("contentLength", paramLong);
            localBundle.putString("contentDisposition", paramString3);
            sendMessage(localMessage);
        }
    }

    public void onExceededDatabaseQuota(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, WebStorage.QuotaUpdater paramQuotaUpdater)
    {
        if (this.mWebChromeClient == null)
            paramQuotaUpdater.updateQuota(paramLong1);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(126);
            HashMap localHashMap = new HashMap();
            localHashMap.put("databaseIdentifier", paramString2);
            localHashMap.put("url", paramString1);
            localHashMap.put("quota", Long.valueOf(paramLong1));
            localHashMap.put("estimatedDatabaseSize", Long.valueOf(paramLong2));
            localHashMap.put("totalQuota", Long.valueOf(paramLong3));
            localHashMap.put("quotaUpdater", paramQuotaUpdater);
            localMessage.obj = localHashMap;
            sendMessage(localMessage);
        }
    }

    public void onFormResubmission(Message paramMessage1, Message paramMessage2)
    {
        if (this.mWebViewClient == null)
            paramMessage1.sendToTarget();
        while (true)
        {
            return;
            Message localMessage = obtainMessage(120);
            Bundle localBundle = localMessage.getData();
            localBundle.putParcelable("resend", paramMessage2);
            localBundle.putParcelable("dontResend", paramMessage1);
            sendMessage(localMessage);
        }
    }

    public void onGeolocationPermissionsHidePrompt()
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(131));
        }
    }

    public void onGeolocationPermissionsShowPrompt(String paramString, GeolocationPermissions.Callback paramCallback)
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(130);
            HashMap localHashMap = new HashMap();
            localHashMap.put("origin", paramString);
            localHashMap.put("callback", paramCallback);
            localMessage.obj = localHashMap;
            sendMessage(localMessage);
        }
    }

    void onIndexChanged(WebHistoryItem paramWebHistoryItem, int paramInt)
    {
        if (this.mWebBackForwardListClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(136, paramInt, 0, paramWebHistoryItem));
        }
    }

    void onIsSupportedCallback(boolean paramBoolean)
    {
        Message localMessage = obtainMessage(142);
        localMessage.obj = Boolean.valueOf(paramBoolean);
        sendMessage(localMessage);
    }

    public void onJsAlert(String paramString1, String paramString2)
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(112, new JsResultReceiver(null));
            localMessage.getData().putString("message", paramString2);
            localMessage.getData().putString("url", paramString1);
            sendMessageToUiThreadSync(localMessage);
        }
    }

    public boolean onJsBeforeUnload(String paramString1, String paramString2)
    {
        if (this.mWebChromeClient == null);
        JsResultReceiver localJsResultReceiver;
        for (boolean bool = true; ; bool = localJsResultReceiver.mJsResult.getResult())
        {
            return bool;
            localJsResultReceiver = new JsResultReceiver(null);
            Message localMessage = obtainMessage(115, localJsResultReceiver);
            localMessage.getData().putString("message", paramString2);
            localMessage.getData().putString("url", paramString1);
            sendMessageToUiThreadSync(localMessage);
        }
    }

    public boolean onJsConfirm(String paramString1, String paramString2)
    {
        if (this.mWebChromeClient == null);
        JsResultReceiver localJsResultReceiver;
        for (boolean bool = false; ; bool = localJsResultReceiver.mJsResult.getResult())
        {
            return bool;
            localJsResultReceiver = new JsResultReceiver(null);
            Message localMessage = obtainMessage(113, localJsResultReceiver);
            localMessage.getData().putString("message", paramString2);
            localMessage.getData().putString("url", paramString1);
            sendMessageToUiThreadSync(localMessage);
        }
    }

    public String onJsPrompt(String paramString1, String paramString2, String paramString3)
    {
        String str = null;
        if (this.mWebChromeClient == null);
        while (true)
        {
            return str;
            JsResultReceiver localJsResultReceiver = new JsResultReceiver(null);
            Message localMessage = obtainMessage(114, localJsResultReceiver);
            localMessage.getData().putString("message", paramString2);
            localMessage.getData().putString("default", paramString3);
            localMessage.getData().putString("url", paramString1);
            sendMessageToUiThreadSync(localMessage);
            str = localJsResultReceiver.mJsResult.getStringResult();
        }
    }

    public boolean onJsTimeout()
    {
        if (this.mWebChromeClient == null);
        JsResultReceiver localJsResultReceiver;
        for (boolean bool = true; ; bool = localJsResultReceiver.mJsResult.getResult())
        {
            return bool;
            localJsResultReceiver = new JsResultReceiver(null);
            sendMessageToUiThreadSync(obtainMessage(128, localJsResultReceiver));
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void onMainFrameFinishParsing()
    {
        sendMessage(obtainMessage(210));
    }

    void onNewHistoryItem(WebHistoryItem paramWebHistoryItem)
    {
        if (this.mWebBackForwardListClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(135, paramWebHistoryItem));
        }
    }

    public void onPageFinished(String paramString)
    {
        sendMessage(obtainMessage(121, paramString));
    }

    public void onPageStarted(String paramString, Bitmap paramBitmap)
    {
        Message localMessage = obtainMessage(100);
        localMessage.obj = paramBitmap;
        localMessage.getData().putString("url", paramString);
        sendMessage(localMessage);
    }

    public void onProceededAfterSslError(SslError paramSslError)
    {
        if (this.mWebViewClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(144);
            localMessage.obj = paramSslError;
            sendMessage(localMessage);
        }
    }

    // ERROR //
    public void onProgressChanged(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 169	android/webkit/CallbackProxy:mLatestProgress	I
        //     6: iload_1
        //     7: if_icmpne +8 -> 15
        //     10: aload_0
        //     11: monitorexit
        //     12: goto +46 -> 58
        //     15: aload_0
        //     16: iload_1
        //     17: putfield 169	android/webkit/CallbackProxy:mLatestProgress	I
        //     20: aload_0
        //     21: getfield 262	android/webkit/CallbackProxy:mWebChromeClient	Landroid/webkit/WebChromeClient;
        //     24: ifnonnull +13 -> 37
        //     27: aload_0
        //     28: monitorexit
        //     29: goto +29 -> 58
        //     32: astore_2
        //     33: aload_0
        //     34: monitorexit
        //     35: aload_2
        //     36: athrow
        //     37: aload_0
        //     38: getfield 492	android/webkit/CallbackProxy:mProgressUpdatePending	Z
        //     41: ifne +15 -> 56
        //     44: aload_0
        //     45: bipush 106
        //     47: invokevirtual 892	android/webkit/CallbackProxy:sendEmptyMessage	(I)Z
        //     50: pop
        //     51: aload_0
        //     52: iconst_1
        //     53: putfield 492	android/webkit/CallbackProxy:mProgressUpdatePending	Z
        //     56: aload_0
        //     57: monitorexit
        //     58: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	35	32	finally
        //     37	58	32	finally
    }

    public void onReachedMaxAppCacheSize(long paramLong1, long paramLong2, WebStorage.QuotaUpdater paramQuotaUpdater)
    {
        if (this.mWebChromeClient == null)
            paramQuotaUpdater.updateQuota(paramLong2);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(127);
            HashMap localHashMap = new HashMap();
            localHashMap.put("requiredStorage", Long.valueOf(paramLong1));
            localHashMap.put("quota", Long.valueOf(paramLong2));
            localHashMap.put("quotaUpdater", paramQuotaUpdater);
            localMessage.obj = localHashMap;
            sendMessage(localMessage);
        }
    }

    public void onReceivedCertificate(SslCertificate paramSslCertificate)
    {
        sendMessage(obtainMessage(124, paramSslCertificate));
    }

    public void onReceivedClientCertRequest(ClientCertRequestHandler paramClientCertRequestHandler, String paramString)
    {
        if (this.mWebViewClient == null)
            paramClientCertRequestHandler.cancel();
        while (true)
        {
            return;
            Message localMessage = obtainMessage(141);
            HashMap localHashMap = new HashMap();
            localHashMap.put("handler", paramClientCertRequestHandler);
            localHashMap.put("host_and_port", paramString);
            localMessage.obj = localHashMap;
            sendMessage(localMessage);
        }
    }

    public void onReceivedError(int paramInt, String paramString1, String paramString2)
    {
        if (this.mWebViewClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(119);
            localMessage.arg1 = paramInt;
            localMessage.getData().putString("description", paramString1);
            localMessage.getData().putString("failingUrl", paramString2);
            sendMessage(localMessage);
        }
    }

    public void onReceivedHttpAuthCredentials(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        Message localMessage = obtainMessage(137);
        localMessage.getData().putString("host", paramString1);
        localMessage.getData().putString("realm", paramString2);
        localMessage.getData().putString("username", paramString3);
        localMessage.getData().putString("password", paramString4);
        sendMessage(localMessage);
    }

    public void onReceivedHttpAuthRequest(HttpAuthHandler paramHttpAuthHandler, String paramString1, String paramString2)
    {
        if (this.mWebViewClient == null)
            paramHttpAuthHandler.cancel();
        while (true)
        {
            return;
            Message localMessage = obtainMessage(104, paramHttpAuthHandler);
            localMessage.getData().putString("host", paramString1);
            localMessage.getData().putString("realm", paramString2);
            sendMessage(localMessage);
        }
    }

    public void onReceivedIcon(Bitmap paramBitmap)
    {
        WebHistoryItem localWebHistoryItem = this.mBackForwardList.getCurrentItem();
        if (localWebHistoryItem != null)
            localWebHistoryItem.setFavicon(paramBitmap);
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(101, paramBitmap));
        }
    }

    void onReceivedLoginRequest(String paramString1, String paramString2, String paramString3)
    {
        if (this.mWebViewClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(140);
            Bundle localBundle = localMessage.getData();
            localBundle.putString("realm", paramString1);
            localBundle.putString("account", paramString2);
            localBundle.putString("args", paramString3);
            sendMessage(localMessage);
        }
    }

    public void onReceivedSslError(SslErrorHandler paramSslErrorHandler, SslError paramSslError)
    {
        if (this.mWebViewClient == null)
            paramSslErrorHandler.cancel();
        while (true)
        {
            return;
            Message localMessage = obtainMessage(105);
            HashMap localHashMap = new HashMap();
            localHashMap.put("handler", paramSslErrorHandler);
            localHashMap.put("error", paramSslError);
            localMessage.obj = localHashMap;
            sendMessage(localMessage);
        }
    }

    public void onReceivedTitle(String paramString)
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(102, paramString));
        }
    }

    void onReceivedTouchIconUrl(String paramString, boolean paramBoolean)
    {
        WebHistoryItem localWebHistoryItem = this.mBackForwardList.getCurrentItem();
        if (localWebHistoryItem != null)
            localWebHistoryItem.setTouchIconUrl(paramString, paramBoolean);
        if (this.mWebChromeClient == null)
            return;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            sendMessage(obtainMessage(132, i, 0, paramString));
            break;
        }
    }

    public void onRequestFocus()
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            sendEmptyMessage(122);
        }
    }

    public boolean onSavePassword(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        Message localMessage = obtainMessage(111, obtainMessage(200));
        Bundle localBundle = localMessage.getData();
        localBundle.putString("host", paramString1);
        localBundle.putString("username", paramString2);
        localBundle.putString("password", paramString3);
        sendMessageToUiThreadSync(localMessage);
        return false;
    }

    public void onScaleChanged(float paramFloat1, float paramFloat2)
    {
        if (this.mWebViewClient == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(123);
            Bundle localBundle = localMessage.getData();
            localBundle.putFloat("old", paramFloat1);
            localBundle.putFloat("new", paramFloat2);
            sendMessage(localMessage);
        }
    }

    void onSearchboxDispatchCompleteCallback(String paramString, int paramInt, boolean paramBoolean)
    {
        Message localMessage = obtainMessage(143);
        localMessage.obj = Boolean.valueOf(paramBoolean);
        localMessage.getData().putString("function", paramString);
        localMessage.getData().putInt("id", paramInt);
        sendMessage(localMessage);
    }

    void onSearchboxSuggestionsReceived(String paramString, List<String> paramList)
    {
        Message localMessage = obtainMessage(139);
        localMessage.obj = paramList;
        localMessage.getData().putString("query", paramString);
        sendMessage(localMessage);
    }

    public void onTooManyRedirects(Message paramMessage1, Message paramMessage2)
    {
    }

    public void onUnhandledKeyEvent(KeyEvent paramKeyEvent)
    {
        if (this.mWebViewClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(116, paramKeyEvent));
        }
    }

    Uri openFileChooser(String paramString1, String paramString2)
    {
        Uri localUri = null;
        if (this.mWebChromeClient == null);
        while (true)
        {
            return localUri;
            Message localMessage = obtainMessage(134);
            UploadFile localUploadFile = new UploadFile(null);
            localMessage.obj = new UploadFileMessageData(localUploadFile, paramString1, paramString2);
            sendMessageToUiThreadSync(localMessage);
            localUri = localUploadFile.getResult();
        }
    }

    public void setDownloadListener(DownloadListener paramDownloadListener)
    {
        this.mDownloadListener = paramDownloadListener;
    }

    void setInstallableWebApp()
    {
        if (this.mWebChromeClient == null);
        while (true)
        {
            return;
            sendMessage(obtainMessage(138));
        }
    }

    void setWebBackForwardListClient(WebBackForwardListClient paramWebBackForwardListClient)
    {
        this.mWebBackForwardListClient = paramWebBackForwardListClient;
    }

    public void setWebChromeClient(WebChromeClient paramWebChromeClient)
    {
        this.mWebChromeClient = paramWebChromeClient;
    }

    public void setWebViewClient(WebViewClient paramWebViewClient)
    {
        this.mWebViewClient = paramWebViewClient;
    }

    WebResourceResponse shouldInterceptRequest(String paramString)
    {
        WebResourceResponse localWebResourceResponse;
        if (this.mWebViewClient == null)
            localWebResourceResponse = null;
        while (true)
        {
            return localWebResourceResponse;
            localWebResourceResponse = this.mWebViewClient.shouldInterceptRequest(this.mWebView.getWebView(), paramString);
            if (localWebResourceResponse == null)
                sendMessage(obtainMessage(108, paramString));
        }
    }

    public boolean shouldOverrideUrlLoading(String paramString)
    {
        ResultTransport localResultTransport = new ResultTransport(Boolean.valueOf(false));
        Message localMessage = obtainMessage(103);
        localMessage.getData().putString("url", paramString);
        localMessage.obj = localResultTransport;
        sendMessageToUiThreadSync(localMessage);
        return ((Boolean)localResultTransport.getResult()).booleanValue();
    }

    protected void shutdown()
    {
        removeCallbacksAndMessages(null);
        setWebViewClient(null);
        setWebChromeClient(null);
    }

    void switchOutDrawHistory()
    {
        sendMessage(obtainMessage(125));
    }

    public boolean uiOverrideKeyEvent(KeyEvent paramKeyEvent)
    {
        if (this.mWebViewClient != null);
        for (boolean bool = this.mWebViewClient.shouldOverrideKeyEvent(this.mWebView.getWebView(), paramKeyEvent); ; bool = false)
            return bool;
    }

    public boolean uiOverrideUrlLoading(String paramString)
    {
        boolean bool;
        if ((paramString == null) || (paramString.length() == 0))
            bool = false;
        while (true)
        {
            return bool;
            bool = false;
            if (this.mWebViewClient != null)
            {
                bool = this.mWebViewClient.shouldOverrideUrlLoading(this.mWebView.getWebView(), paramString);
            }
            else
            {
                Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
                localIntent.addCategory("android.intent.category.BROWSABLE");
                localIntent.putExtra("com.android.browser.application_id", this.mContext.getPackageName());
                try
                {
                    this.mContext.startActivity(localIntent);
                    bool = true;
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                }
            }
        }
    }

    private class UploadFile
        implements ValueCallback<Uri>
    {
        private Uri mValue;

        private UploadFile()
        {
        }

        public Uri getResult()
        {
            return this.mValue;
        }

        public void onReceiveValue(Uri paramUri)
        {
            this.mValue = paramUri;
            synchronized (CallbackProxy.this)
            {
                CallbackProxy.this.notify();
                return;
            }
        }
    }

    private static class UploadFileMessageData
    {
        private String mAcceptType;
        private CallbackProxy.UploadFile mCallback;
        private String mCapture;

        public UploadFileMessageData(CallbackProxy.UploadFile paramUploadFile, String paramString1, String paramString2)
        {
            this.mCallback = paramUploadFile;
            this.mAcceptType = paramString1;
            this.mCapture = paramString2;
        }

        public String getAcceptType()
        {
            return this.mAcceptType;
        }

        public String getCapture()
        {
            return this.mCapture;
        }

        public CallbackProxy.UploadFile getUploadFile()
        {
            return this.mCallback;
        }
    }

    private class JsResultReceiver
        implements JsResult.ResultReceiver
    {
        public JsPromptResult mJsResult = new JsPromptResult(this);
        private boolean mReady;
        private boolean mTriedToNotifyBeforeReady;

        private JsResultReceiver()
        {
        }

        private void notifyCallbackProxy()
        {
            synchronized (CallbackProxy.this)
            {
                CallbackProxy.this.notify();
                return;
            }
        }

        public void onJsResultComplete(JsResult paramJsResult)
        {
            if (this.mReady)
                notifyCallbackProxy();
            while (true)
            {
                return;
                this.mTriedToNotifyBeforeReady = true;
            }
        }

        final void setReady()
        {
            this.mReady = true;
            if (this.mTriedToNotifyBeforeReady)
                notifyCallbackProxy();
        }
    }

    private static class ResultTransport<E>
    {
        private E mResult;

        public ResultTransport(E paramE)
        {
            this.mResult = paramE;
        }

        /** @deprecated */
        public E getResult()
        {
            try
            {
                Object localObject2 = this.mResult;
                return localObject2;
            }
            finally
            {
                localObject1 = finally;
                throw localObject1;
            }
        }

        /** @deprecated */
        public void setResult(E paramE)
        {
            try
            {
                this.mResult = paramE;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.CallbackProxy
 * JD-Core Version:        0.6.2
 */