package android.webkit;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager;
import android.content.ComponentCallbacks;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.net.ParseException;
import android.net.Uri;
import android.net.WebAddress;
import android.net.http.ErrorStrings;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewRootImpl;
import android.view.WindowManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import libcore.net.MimeUtils;
import org.apache.harmony.security.provider.cert.X509CertImpl;
import org.apache.harmony.xnet.provider.jsse.OpenSSLDSAPrivateKey;
import org.apache.harmony.xnet.provider.jsse.OpenSSLRSAPrivateKey;

class BrowserFrame extends Handler
{
    static final int DRAWABLEDIR = 3;
    private static final int FILE_UPLOAD_LABEL = 4;
    private static final int FILE_UPLOAD_NO_FILE_CHOSEN = 7;
    static final int FRAME_COMPLETED = 1001;
    static final int FRAME_LOADTYPE_BACK = 1;
    static final int FRAME_LOADTYPE_FORWARD = 2;
    static final int FRAME_LOADTYPE_INDEXEDBACKFORWARD = 3;
    static final int FRAME_LOADTYPE_REDIRECT = 7;
    static final int FRAME_LOADTYPE_RELOAD = 4;
    static final int FRAME_LOADTYPE_RELOADALLOWINGSTALEDATA = 5;
    static final int FRAME_LOADTYPE_REPLACE = 8;
    static final int FRAME_LOADTYPE_SAME = 6;
    static final int FRAME_LOADTYPE_STANDARD = 0;
    private static final int LOADERROR = 2;
    private static final String LOGTAG = "webkit";
    private static final int MAX_OUTSTANDING_REQUESTS = 300;
    private static final int NODOMAIN = 1;
    static final int ORIENTATION_CHANGED = 1002;
    static final int POLICY_FUNCTION = 1003;
    static final int POLICY_IGNORE = 2;
    static final int POLICY_USE = 0;
    private static final int RESET_LABEL = 5;
    private static final int SUBMIT_LABEL = 6;
    private static final int TRANSITION_SWITCH_THRESHOLD = 75;
    static ConfigCallback sConfigCallback;
    static JWebCoreJavaBridge sJavaBridge;
    private boolean mBlockMessages = false;
    private final CallbackProxy mCallbackProxy;
    private boolean mCommitted = true;
    private final Context mContext;
    private boolean mFirstLayoutDone = true;
    private boolean mIsMainFrame;
    private Map<String, Object> mJavaScriptObjects;
    private KeyStoreHandler mKeyStoreHandler = null;
    boolean mLoadInitFromJava;
    private int mLoadType;
    int mNativeFrame;
    private int mOrientation = -1;
    private Set<Object> mRemovedJavaScriptObjects;
    private final SearchBoxImpl mSearchBox;
    private final WebSettingsClassic mSettings;
    private final WebViewCore mWebViewCore;

    static
    {
        if (!BrowserFrame.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    public BrowserFrame(Context paramContext, WebViewCore paramWebViewCore, CallbackProxy paramCallbackProxy, WebSettingsClassic paramWebSettingsClassic, Map<String, Object> paramMap)
    {
        Context localContext = paramContext.getApplicationContext();
        if (sJavaBridge == null)
        {
            sJavaBridge = new JWebCoreJavaBridge();
            if (((ActivityManager)paramContext.getSystemService("activity")).getMemoryClass() <= 16)
                break label238;
            sJavaBridge.setCacheSize(8388608);
        }
        while (true)
        {
            CacheManager.init(localContext);
            CookieSyncManager.createInstance(localContext);
            PluginManager.getInstance(localContext);
            if (sConfigCallback == null)
            {
                sConfigCallback = new ConfigCallback((WindowManager)localContext.getSystemService("window"));
                ViewRootImpl.addConfigCallback(sConfigCallback);
            }
            sConfigCallback.addHandler(this);
            this.mJavaScriptObjects = paramMap;
            if (this.mJavaScriptObjects == null)
                this.mJavaScriptObjects = new HashMap();
            this.mRemovedJavaScriptObjects = new HashSet();
            this.mSettings = paramWebSettingsClassic;
            this.mContext = paramContext;
            this.mCallbackProxy = paramCallbackProxy;
            this.mWebViewCore = paramWebViewCore;
            this.mSearchBox = new SearchBoxImpl(this.mWebViewCore, this.mCallbackProxy);
            this.mJavaScriptObjects.put("searchBoxJavaBridge_", this.mSearchBox);
            nativeCreateFrame(paramWebViewCore, paramContext.getAssets(), paramCallbackProxy.getBackForwardList());
            return;
            label238: sJavaBridge.setCacheSize(4194304);
        }
    }

    private void autoLogin(String paramString1, String paramString2, String paramString3)
    {
        this.mCallbackProxy.onReceivedLoginRequest(paramString1, paramString2, paramString3);
    }

    private native String childFramesAsText();

    private void closeWindow(WebViewCore paramWebViewCore)
    {
        this.mCallbackProxy.onCloseWindow(paramWebViewCore.getWebViewClassic());
    }

    private BrowserFrame createWindow(boolean paramBoolean1, boolean paramBoolean2)
    {
        return this.mCallbackProxy.createWindow(paramBoolean1, paramBoolean2);
    }

    private void decidePolicyForFormResubmission(int paramInt)
    {
        Message localMessage1 = obtainMessage(1003, paramInt, 2);
        Message localMessage2 = obtainMessage(1003, paramInt, 0);
        this.mCallbackProxy.onFormResubmission(localMessage1, localMessage2);
    }

    private float density()
    {
        return this.mContext.getResources().getDisplayMetrics().density;
    }

    private void didFinishLoading()
    {
        if (this.mKeyStoreHandler != null)
        {
            this.mKeyStoreHandler.installCert(this.mContext);
            this.mKeyStoreHandler = null;
        }
    }

    private void didReceiveAuthenticationChallenge(final int paramInt, String paramString1, String paramString2, final boolean paramBoolean1, final boolean paramBoolean2)
    {
        HttpAuthHandler local1 = new HttpAuthHandler()
        {
            public void cancel()
            {
                BrowserFrame.this.nativeAuthenticationCancel(paramInt);
            }

            public void proceed(String paramAnonymousString1, String paramAnonymousString2)
            {
                BrowserFrame.this.nativeAuthenticationProceed(paramInt, paramAnonymousString1, paramAnonymousString2);
            }

            public boolean suppressDialog()
            {
                return paramBoolean2;
            }

            public boolean useHttpAuthUsernamePassword()
            {
                return paramBoolean1;
            }
        };
        this.mCallbackProxy.onReceivedHttpAuthRequest(local1, paramString1, paramString2);
    }

    private void didReceiveData(byte[] paramArrayOfByte, int paramInt)
    {
        if (this.mKeyStoreHandler != null)
            this.mKeyStoreHandler.didReceiveData(paramArrayOfByte, paramInt);
    }

    private void didReceiveIcon(Bitmap paramBitmap)
    {
        this.mCallbackProxy.onReceivedIcon(paramBitmap);
    }

    private void didReceiveTouchIconUrl(String paramString, boolean paramBoolean)
    {
        this.mCallbackProxy.onReceivedTouchIconUrl(paramString, paramBoolean);
    }

    private native String documentAsText();

    private void downloadStart(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong)
    {
        if (paramString4.isEmpty());
        try
        {
            paramString4 = MimeUtils.guessMimeTypeFromExtension(paramString1.substring(1 + paramString1.lastIndexOf('.')));
            if (paramString4 == null)
                paramString4 = "";
            label35: String str = MimeTypeMap.getSingleton().remapGenericMimeType(paramString4, paramString1, paramString3);
            if (CertTool.getCertType(str) != null)
                this.mKeyStoreHandler = new KeyStoreHandler(str);
            while (true)
            {
                return;
                this.mCallbackProxy.onDownloadStart(paramString1, paramString2, paramString3, str, paramLong);
            }
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
            break label35;
        }
    }

    private native String externalRepresentation();

    private int getFile(String paramString, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        try
        {
            InputStream localInputStream = this.mContext.getContentResolver().openInputStream(Uri.parse(paramString));
            i = localInputStream.available();
            if ((i <= paramInt2) && (paramArrayOfByte != null) && (paramArrayOfByte.length - paramInt1 >= i))
                localInputStream.read(paramArrayOfByte, paramInt1, i);
            while (true)
            {
                localInputStream.close();
                return i;
                i = 0;
            }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            while (true)
            {
                Log.e("webkit", "FileNotFoundException:" + localFileNotFoundException);
                i = 0;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.e("webkit", "IOException: " + localIOException);
                int i = 0;
            }
        }
    }

    private int getFileSize(String paramString)
    {
        int i = 0;
        try
        {
            InputStream localInputStream = this.mContext.getContentResolver().openInputStream(Uri.parse(paramString));
            i = localInputStream.available();
            localInputStream.close();
            label29: return i;
        }
        catch (Exception localException)
        {
            break label29;
        }
    }

    private String getRawResFilename(int paramInt)
    {
        return getRawResFilename(paramInt, this.mContext);
    }

    static String getRawResFilename(int paramInt, Context paramContext)
    {
        String str1;
        switch (paramInt)
        {
        default:
            Log.e("webkit", "getRawResFilename got incompatible resource ID");
            str1 = "";
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return str1;
            int i = 17825796;
            TypedValue localTypedValue;
            String str2;
            int j;
            while (true)
            {
                localTypedValue = new TypedValue();
                paramContext.getResources().getValue(i, localTypedValue, true);
                if (paramInt != 3)
                    break label212;
                str2 = localTypedValue.string.toString();
                j = str2.lastIndexOf('/');
                if (j >= 0)
                    break label198;
                Log.e("webkit", "Can't find drawable directory.");
                str1 = "";
                break;
                i = 17825795;
                continue;
                i = 17301731;
            }
            str1 = paramContext.getResources().getString(17040511);
            continue;
            str1 = paramContext.getResources().getString(17040513);
            continue;
            str1 = paramContext.getResources().getString(17040514);
            continue;
            str1 = paramContext.getResources().getString(17040512);
            continue;
            label198: str1 = str2.substring(0, j + 1);
            continue;
            label212: str1 = localTypedValue.string.toString();
        }
    }

    private native String[] getUsernamePassword();

    private native boolean hasPasswordField();

    private InputStream inputStreamForAndroidResource(String paramString)
    {
        String str2;
        Object localObject;
        if (paramString.startsWith("file:///android_res/"))
        {
            str2 = paramString.replaceFirst("file:///android_res/", "");
            if ((str2 == null) || (str2.length() == 0))
            {
                Log.e("webkit", "url has length 0 " + str2);
                localObject = null;
            }
        }
        while (true)
        {
            return localObject;
            int j = str2.indexOf('/');
            int k = str2.indexOf('.', j);
            if ((j == -1) || (k == -1))
            {
                Log.e("webkit", "Incorrect res path: " + str2);
                localObject = null;
            }
            else
            {
                String str3 = str2.substring(0, j);
                String str4 = str2.substring(j + 1, k);
                try
                {
                    int m = this.mContext.getApplicationContext().getClassLoader().loadClass(this.mContext.getPackageName() + ".R$" + str3).getField(str4).getInt(null);
                    TypedValue localTypedValue = new TypedValue();
                    this.mContext.getResources().getValue(m, localTypedValue, true);
                    if (localTypedValue.type == 3)
                    {
                        localObject = this.mContext.getAssets().openNonAsset(localTypedValue.assetCookie, localTypedValue.string.toString(), 2);
                        continue;
                    }
                    Log.e("webkit", "not of type string: " + str2);
                    localObject = null;
                }
                catch (Exception localException2)
                {
                    Log.e("webkit", "Exception: " + str2);
                    localObject = null;
                }
                continue;
                if (paramString.startsWith("file:///android_asset/"))
                {
                    String str1 = paramString.replaceFirst("file:///android_asset/", "");
                    try
                    {
                        InputStream localInputStream2 = this.mContext.getAssets().open(Uri.parse(str1).getPath(), 2);
                        localObject = localInputStream2;
                    }
                    catch (IOException localIOException)
                    {
                        localObject = null;
                    }
                }
                else if ((this.mSettings.getAllowContentAccess()) && (paramString.startsWith("content:")))
                {
                    try
                    {
                        int i = paramString.lastIndexOf('?');
                        if (i != -1)
                            paramString = paramString.substring(0, i);
                        Uri localUri = Uri.parse(paramString);
                        InputStream localInputStream1 = this.mContext.getContentResolver().openInputStream(localUri);
                        localObject = localInputStream1;
                    }
                    catch (Exception localException1)
                    {
                        Log.e("webkit", "Exception: " + paramString);
                        localObject = null;
                    }
                }
                else
                {
                    localObject = null;
                }
            }
        }
    }

    private void loadFinished(String paramString, int paramInt, boolean paramBoolean)
    {
        if (((paramBoolean) || (paramInt == 0)) && (paramBoolean))
        {
            resetLoadingStates();
            this.mCallbackProxy.switchOutDrawHistory();
            this.mCallbackProxy.onPageFinished(paramString);
        }
    }

    private void loadStarted(String paramString, Bitmap paramBitmap, int paramInt, boolean paramBoolean)
    {
        this.mIsMainFrame = paramBoolean;
        if ((paramBoolean) || (paramInt == 0))
        {
            this.mLoadType = paramInt;
            if (paramBoolean)
            {
                this.mCallbackProxy.onPageStarted(paramString, paramBitmap);
                this.mFirstLayoutDone = false;
                this.mCommitted = false;
                this.mWebViewCore.clearContent();
                this.mWebViewCore.removeMessages(130);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private void mainFrameFinishParsing()
    {
        this.mCallbackProxy.onMainFrameFinishParsing();
    }

    private void maybeSavePassword(byte[] paramArrayOfByte, String paramString1, String paramString2)
    {
        if ((paramArrayOfByte == null) || (paramString1 == null) || (paramString1.isEmpty()) || (paramString2 == null) || (paramString2.isEmpty()));
        while (true)
        {
            return;
            if (this.mSettings.getSavePassword())
                try
                {
                    WebAddress localWebAddress = new WebAddress(this.mCallbackProxy.getBackForwardList().getCurrentItem().getUrl());
                    String str1 = localWebAddress.getScheme() + localWebAddress.getHost();
                    String str2 = new String(paramArrayOfByte);
                    WebViewDatabaseClassic localWebViewDatabaseClassic = WebViewDatabaseClassic.getInstance(this.mContext);
                    if ((!str2.contains(URLEncoder.encode(paramString1))) || (!str2.contains(URLEncoder.encode(paramString2))))
                        continue;
                    String[] arrayOfString = localWebViewDatabaseClassic.getUsernamePassword(str1);
                    if (arrayOfString != null)
                    {
                        if (arrayOfString[0] == null)
                            continue;
                        localWebViewDatabaseClassic.setUsernamePassword(str1, paramString1, paramString2);
                        continue;
                    }
                    this.mCallbackProxy.onSavePassword(str1, paramString1, paramString2, null);
                }
                catch (ParseException localParseException)
                {
                }
        }
    }

    private native void nativeAddJavascriptInterface(int paramInt, Object paramObject, String paramString);

    private native void nativeAuthenticationCancel(int paramInt);

    private native void nativeAuthenticationProceed(int paramInt, String paramString1, String paramString2);

    private native void nativeCallPolicyFunction(int paramInt1, int paramInt2);

    private native void nativeCreateFrame(WebViewCore paramWebViewCore, AssetManager paramAssetManager, WebBackForwardList paramWebBackForwardList);

    private native boolean nativeGetShouldStartScrolledRight(int paramInt);

    private native void nativeGoBackOrForward(int paramInt);

    private native void nativeLoadData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);

    private native void nativeLoadUrl(String paramString, Map<String, String> paramMap);

    private native void nativeOrientationChanged(int paramInt);

    private native void nativePostUrl(String paramString, byte[] paramArrayOfByte);

    private native String nativeSaveWebArchive(String paramString, boolean paramBoolean);

    private native void nativeSslCertErrorCancel(int paramInt1, int paramInt2);

    private native void nativeSslCertErrorProceed(int paramInt);

    private native void nativeStopLoading();

    private void reportError(int paramInt, String paramString1, String paramString2)
    {
        resetLoadingStates();
        if ((paramString1 == null) || (paramString1.isEmpty()))
            paramString1 = ErrorStrings.getString(paramInt, this.mContext);
        this.mCallbackProxy.onReceivedError(paramInt, paramString1, paramString2);
    }

    private void reportSslCertError(final int paramInt1, final int paramInt2, byte[] paramArrayOfByte, String paramString)
    {
        try
        {
            localSslError = SslError.SslErrorFromChromiumErrorCode(paramInt2, new SslCertificate(new X509CertImpl(paramArrayOfByte)), paramString);
            if (SslCertLookupTable.getInstance().isAllowed(localSslError))
            {
                nativeSslCertErrorProceed(paramInt1);
                this.mCallbackProxy.onProceededAfterSslError(localSslError);
                return;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                final SslError localSslError;
                Log.e("webkit", "Can't get the certificate from WebKit, canceling");
                nativeSslCertErrorCancel(paramInt1, paramInt2);
                continue;
                SslErrorHandler local2 = new SslErrorHandler()
                {
                    public void cancel()
                    {
                        post(new Runnable()
                        {
                            public void run()
                            {
                                BrowserFrame.this.nativeSslCertErrorCancel(BrowserFrame.2.this.val$handle, BrowserFrame.2.this.val$certError);
                            }
                        });
                    }

                    public void proceed()
                    {
                        SslCertLookupTable.getInstance().setIsAllowed(localSslError);
                        post(new Runnable()
                        {
                            public void run()
                            {
                                BrowserFrame.this.nativeSslCertErrorProceed(BrowserFrame.2.this.val$handle);
                            }
                        });
                    }
                };
                this.mCallbackProxy.onReceivedSslError(local2, localSslError);
            }
        }
    }

    private void requestClientCert(int paramInt, String paramString)
    {
        SslClientCertLookupTable localSslClientCertLookupTable = SslClientCertLookupTable.getInstance();
        PrivateKey localPrivateKey;
        if (localSslClientCertLookupTable.IsAllowed(paramString))
        {
            localPrivateKey = localSslClientCertLookupTable.PrivateKey(paramString);
            if ((localPrivateKey instanceof OpenSSLRSAPrivateKey))
                nativeSslClientCert(paramInt, ((OpenSSLRSAPrivateKey)localPrivateKey).getPkeyContext(), localSslClientCertLookupTable.CertificateChain(paramString));
        }
        while (true)
        {
            return;
            if ((localPrivateKey instanceof OpenSSLDSAPrivateKey))
            {
                nativeSslClientCert(paramInt, ((OpenSSLDSAPrivateKey)localPrivateKey).getPkeyContext(), localSslClientCertLookupTable.CertificateChain(paramString));
            }
            else
            {
                nativeSslClientCert(paramInt, localPrivateKey.getEncoded(), localSslClientCertLookupTable.CertificateChain(paramString));
                continue;
                if (localSslClientCertLookupTable.IsDenied(paramString))
                    nativeSslClientCert(paramInt, 0, (byte[][])null);
                else
                    this.mCallbackProxy.onReceivedClientCertRequest(new ClientCertRequestHandler(this, paramInt, paramString, localSslClientCertLookupTable), paramString);
            }
        }
    }

    private void requestFocus()
    {
        this.mCallbackProxy.onRequestFocus();
    }

    private void resetLoadingStates()
    {
        this.mCommitted = true;
        this.mFirstLayoutDone = true;
    }

    private void saveFormData(HashMap<String, String> paramHashMap)
    {
        if (this.mSettings.getSaveFormData())
        {
            WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
            if (localWebHistoryItem != null)
            {
                String str = WebTextView.urlForAutoCompleteData(localWebHistoryItem.getUrl());
                if (str != null)
                    WebViewDatabaseClassic.getInstance(this.mContext).setFormData(str, paramHashMap);
            }
        }
    }

    private void setCertificate(byte[] paramArrayOfByte)
    {
        try
        {
            X509CertImpl localX509CertImpl = new X509CertImpl(paramArrayOfByte);
            this.mCallbackProxy.onReceivedCertificate(new SslCertificate(localX509CertImpl));
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e("webkit", "Can't get the certificate from WebKit, canceling");
        }
    }

    private void setProgress(int paramInt)
    {
        this.mCallbackProxy.onProgressChanged(paramInt);
        if (paramInt == 100)
            sendMessageDelayed(obtainMessage(1001), 100L);
        if ((this.mFirstLayoutDone) && (paramInt > 75))
            this.mCallbackProxy.switchOutDrawHistory();
    }

    private void setTitle(String paramString)
    {
        this.mCallbackProxy.onReceivedTitle(paramString);
    }

    private native void setUsernamePassword(String paramString1, String paramString2);

    private WebResourceResponse shouldInterceptRequest(String paramString)
    {
        InputStream localInputStream = inputStreamForAndroidResource(paramString);
        Object localObject;
        if (localInputStream != null)
            localObject = new WebResourceResponse(null, null, localInputStream);
        while (true)
        {
            return localObject;
            if ((!this.mSettings.getAllowFileAccess()) && (paramString.startsWith("file://")))
            {
                localObject = new WebResourceResponse(null, null, null);
            }
            else
            {
                localObject = this.mCallbackProxy.shouldInterceptRequest(paramString);
                if ((localObject == null) && ("browser:incognito".equals(paramString)))
                    try
                    {
                        WebResourceResponse localWebResourceResponse = new WebResourceResponse("text/html", "utf8", this.mContext.getResources().openRawResource(17825794));
                        localObject = localWebResourceResponse;
                    }
                    catch (Resources.NotFoundException localNotFoundException)
                    {
                        Log.w("webkit", "Failed opening raw.incognito_mode_start_page", localNotFoundException);
                    }
            }
        }
    }

    private boolean shouldSaveFormData()
    {
        boolean bool = false;
        if (this.mSettings.getSaveFormData())
        {
            WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
            if ((localWebHistoryItem != null) && (localWebHistoryItem.getUrl() != null))
                bool = true;
        }
        return bool;
    }

    private void transitionToCommitted(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean)
        {
            this.mCommitted = true;
            this.mWebViewCore.getWebViewClassic().mViewManager.postResetStateAll();
        }
    }

    private void updateVisitedHistory(String paramString, boolean paramBoolean)
    {
        this.mCallbackProxy.doUpdateVisitedHistory(paramString, paramBoolean);
    }

    private void windowObjectCleared(int paramInt)
    {
        Iterator localIterator = this.mJavaScriptObjects.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            if (this.mJavaScriptObjects.get(str) != null)
                nativeAddJavascriptInterface(paramInt, this.mJavaScriptObjects.get(str), str);
        }
        this.mRemovedJavaScriptObjects.clear();
        stringByEvaluatingJavaScriptFromString("(function(){if (!window.chrome) {    window.chrome = {};}if (!window.chrome.searchBox) {    var sb = window.chrome.searchBox = {};    sb.setSuggestions = function(suggestions) {        if (window.searchBoxJavaBridge_) {            window.searchBoxJavaBridge_.setSuggestions(JSON.stringify(suggestions));        }    };    sb.setValue = function(valueArray) { sb.value = valueArray[0]; };    sb.value = '';    sb.x = 0;    sb.y = 0;    sb.width = 0;    sb.height = 0;    sb.selectionStart = 0;    sb.selectionEnd = 0;    sb.verbatim = false;}})();");
    }

    public void addJavascriptInterface(Object paramObject, String paramString)
    {
        assert (paramObject != null);
        removeJavascriptInterface(paramString);
        this.mJavaScriptObjects.put(paramString, paramObject);
    }

    public native void clearCache();

    boolean committed()
    {
        return this.mCommitted;
    }

    public void destroy()
    {
        nativeDestroyFrame();
        this.mBlockMessages = true;
        removeCallbacksAndMessages(null);
    }

    void didFirstLayout()
    {
        if (!this.mFirstLayoutDone)
        {
            this.mFirstLayoutDone = true;
            this.mWebViewCore.contentDraw();
        }
    }

    public void documentAsText(Message paramMessage)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if (paramMessage.arg1 != 0)
            localStringBuilder.append(documentAsText());
        if (paramMessage.arg2 != 0)
            localStringBuilder.append(childFramesAsText());
        paramMessage.obj = localStringBuilder.toString();
        paramMessage.sendToTarget();
    }

    public native boolean documentHasImages();

    public void externalRepresentation(Message paramMessage)
    {
        paramMessage.obj = externalRepresentation();
        paramMessage.sendToTarget();
    }

    boolean firstLayoutDone()
    {
        return this.mFirstLayoutDone;
    }

    CallbackProxy getCallbackProxy()
    {
        return this.mCallbackProxy;
    }

    SearchBox getSearchBox()
    {
        return this.mSearchBox;
    }

    boolean getShouldStartScrolledRight()
    {
        return nativeGetShouldStartScrolledRight(this.mNativeFrame);
    }

    String getUserAgentString()
    {
        return this.mSettings.getUserAgentString();
    }

    public void goBackOrForward(int paramInt)
    {
        this.mLoadInitFromJava = true;
        nativeGoBackOrForward(paramInt);
        this.mLoadInitFromJava = false;
    }

    public void handleMessage(Message paramMessage)
    {
        if (this.mBlockMessages);
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            default:
                break;
            case 1001:
                if ((this.mSettings.getSavePassword()) && (hasPasswordField()))
                {
                    WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
                    if (localWebHistoryItem != null)
                    {
                        WebAddress localWebAddress = new WebAddress(localWebHistoryItem.getUrl());
                        String str = localWebAddress.getScheme() + localWebAddress.getHost();
                        String[] arrayOfString = WebViewDatabaseClassic.getInstance(this.mContext).getUsernamePassword(str);
                        if ((arrayOfString != null) && (arrayOfString[0] != null))
                            setUsernamePassword(arrayOfString[0], arrayOfString[1]);
                    }
                }
                break;
            case 1003:
                nativeCallPolicyFunction(paramMessage.arg1, paramMessage.arg2);
                break;
            case 1002:
                if (this.mOrientation != paramMessage.arg1)
                {
                    this.mOrientation = paramMessage.arg1;
                    nativeOrientationChanged(paramMessage.arg1);
                }
                break;
            }
        }
    }

    public boolean handleUrl(String paramString)
    {
        boolean bool = false;
        if (this.mLoadInitFromJava == true);
        while (true)
        {
            return bool;
            if (this.mCallbackProxy.shouldOverrideUrlLoading(paramString))
            {
                didFirstLayout();
                bool = true;
            }
        }
    }

    public void loadData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        this.mLoadInitFromJava = true;
        if ((paramString5 == null) || (paramString5.length() == 0))
            paramString5 = "about:blank";
        if (paramString2 == null)
            paramString2 = "";
        if ((paramString1 == null) || (paramString1.length() == 0))
            paramString1 = "about:blank";
        if ((paramString3 == null) || (paramString3.length() == 0))
            paramString3 = "text/html";
        nativeLoadData(paramString1, paramString2, paramString3, paramString4, paramString5);
        this.mLoadInitFromJava = false;
    }

    int loadType()
    {
        return this.mLoadType;
    }

    public void loadUrl(String paramString, Map<String, String> paramMap)
    {
        this.mLoadInitFromJava = true;
        if (URLUtil.isJavaScriptUrl(paramString))
            stringByEvaluatingJavaScriptFromString(paramString.substring("javascript:".length()));
        while (true)
        {
            this.mLoadInitFromJava = false;
            return;
            nativeLoadUrl(paramString, paramMap);
        }
    }

    public native void nativeDestroyFrame();

    native void nativeSslClientCert(int paramInt1, int paramInt2, byte[][] paramArrayOfByte);

    native void nativeSslClientCert(int paramInt, byte[] paramArrayOfByte, byte[][] paramArrayOfByte1);

    public void postUrl(String paramString, byte[] paramArrayOfByte)
    {
        this.mLoadInitFromJava = true;
        nativePostUrl(paramString, paramArrayOfByte);
        this.mLoadInitFromJava = false;
    }

    public native void reload(boolean paramBoolean);

    public void removeJavascriptInterface(String paramString)
    {
        if (this.mJavaScriptObjects.containsKey(paramString))
            this.mRemovedJavaScriptObjects.add(this.mJavaScriptObjects.remove(paramString));
    }

    String saveWebArchive(String paramString, boolean paramBoolean)
    {
        return nativeSaveWebArchive(paramString, paramBoolean);
    }

    public void stopLoading()
    {
        if (this.mIsMainFrame)
            resetLoadingStates();
        nativeStopLoading();
    }

    public native String stringByEvaluatingJavaScriptFromString(String paramString);

    private static class ConfigCallback
        implements ComponentCallbacks
    {
        private final ArrayList<WeakReference<Handler>> mHandlers = new ArrayList();
        private final WindowManager mWindowManager;

        ConfigCallback(WindowManager paramWindowManager)
        {
            this.mWindowManager = paramWindowManager;
        }

        /** @deprecated */
        public void addHandler(Handler paramHandler)
        {
            try
            {
                this.mHandlers.add(new WeakReference(paramHandler));
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        // ERROR //
        public void onConfigurationChanged(android.content.res.Configuration paramConfiguration)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 24	android/webkit/BrowserFrame$ConfigCallback:mHandlers	Ljava/util/ArrayList;
            //     4: invokevirtual 43	java/util/ArrayList:size	()I
            //     7: ifne +4 -> 11
            //     10: return
            //     11: aload_0
            //     12: getfield 26	android/webkit/BrowserFrame$ConfigCallback:mWindowManager	Landroid/view/WindowManager;
            //     15: invokeinterface 49 1 0
            //     20: invokevirtual 54	android/view/Display:getOrientation	()I
            //     23: istore_2
            //     24: iload_2
            //     25: tableswitch	default:+31 -> 56, 0:+139->164, 1:+120->145, 2:+126->151, 3:+133->158
            //     57: monitorenter
            //     58: new 21	java/util/ArrayList
            //     61: dup
            //     62: aload_0
            //     63: getfield 24	android/webkit/BrowserFrame$ConfigCallback:mHandlers	Ljava/util/ArrayList;
            //     66: invokevirtual 43	java/util/ArrayList:size	()I
            //     69: invokespecial 57	java/util/ArrayList:<init>	(I)V
            //     72: astore_3
            //     73: aload_0
            //     74: getfield 24	android/webkit/BrowserFrame$ConfigCallback:mHandlers	Ljava/util/ArrayList;
            //     77: invokevirtual 61	java/util/ArrayList:iterator	()Ljava/util/Iterator;
            //     80: astore 5
            //     82: aload 5
            //     84: invokeinterface 67 1 0
            //     89: ifeq +90 -> 179
            //     92: aload 5
            //     94: invokeinterface 71 1 0
            //     99: checkcast 30	java/lang/ref/WeakReference
            //     102: astore 9
            //     104: aload 9
            //     106: invokevirtual 74	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     109: checkcast 76	android/os/Handler
            //     112: astore 10
            //     114: aload 10
            //     116: ifnull +53 -> 169
            //     119: aload 10
            //     121: aload 10
            //     123: sipush 1002
            //     126: iload_2
            //     127: iconst_0
            //     128: invokevirtual 80	android/os/Handler:obtainMessage	(III)Landroid/os/Message;
            //     131: invokevirtual 84	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
            //     134: pop
            //     135: goto -53 -> 82
            //     138: astore 4
            //     140: aload_0
            //     141: monitorexit
            //     142: aload 4
            //     144: athrow
            //     145: bipush 90
            //     147: istore_2
            //     148: goto -92 -> 56
            //     151: sipush 180
            //     154: istore_2
            //     155: goto -99 -> 56
            //     158: bipush 166
            //     160: istore_2
            //     161: goto -105 -> 56
            //     164: iconst_0
            //     165: istore_2
            //     166: goto -110 -> 56
            //     169: aload_3
            //     170: aload 9
            //     172: invokevirtual 37	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     175: pop
            //     176: goto -94 -> 82
            //     179: aload_3
            //     180: invokevirtual 61	java/util/ArrayList:iterator	()Ljava/util/Iterator;
            //     183: astore 6
            //     185: aload 6
            //     187: invokeinterface 67 1 0
            //     192: ifeq +28 -> 220
            //     195: aload 6
            //     197: invokeinterface 71 1 0
            //     202: checkcast 30	java/lang/ref/WeakReference
            //     205: astore 7
            //     207: aload_0
            //     208: getfield 24	android/webkit/BrowserFrame$ConfigCallback:mHandlers	Ljava/util/ArrayList;
            //     211: aload 7
            //     213: invokevirtual 87	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
            //     216: pop
            //     217: goto -32 -> 185
            //     220: aload_0
            //     221: monitorexit
            //     222: goto -212 -> 10
            //
            // Exception table:
            //     from	to	target	type
            //     58	142	138	finally
            //     169	222	138	finally
        }

        public void onLowMemory()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.BrowserFrame
 * JD-Core Version:        0.6.2
 */