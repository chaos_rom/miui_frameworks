package android.webkit;

import android.animation.ObjectAnimator;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Region.Op;
import android.graphics.RegionIterator;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.net.ProxyProperties;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.EventLog;
import android.util.Log;
import android.view.Display;
import android.view.HardwareCanvas;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.ViewRootImpl;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.OverScroller;
import android.widget.PopupWindow;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import junit.framework.Assert;

public final class WebViewClassic
    implements WebViewProvider, WebViewProvider.ScrollDelegate, WebViewProvider.ViewDelegate
{
    private static final float ANGLE_HORIZ = 0.0F;
    private static final float ANGLE_VERT = 2.0F;
    static final int AUTOFILL_COMPLETE = 134;
    static final int AUTOFILL_FORM = 148;
    private static final boolean AUTO_REDRAW_HACK = false;
    private static final long CARET_HANDLE_STAMINA_MS = 3000L;
    static final int CENTER_FIT_RECT = 127;
    static final int CLEAR_CARET_HANDLE = 144;
    static final int CLEAR_TEXT_ENTRY = 111;
    static final int COPY_TO_CLIPBOARD = 141;
    private static final boolean DEBUG_TOUCH_HIGHLIGHT = true;
    static final int DEFAULT_VIEWPORT_WIDTH = 980;
    private static final int DRAG_HELD_MOTIONLESS = 8;
    private static final int DRAG_LAYER_FINGER_DISTANCE = 20000;
    private static final int DRAW_EXTRAS_CURSOR_RING = 2;
    private static final int DRAW_EXTRAS_NONE = 0;
    private static final int DRAW_EXTRAS_SELECTION = 1;
    private static final int EDIT_RECT_BUFFER = 10;
    static final int EDIT_TEXT_SIZE_CHANGED = 150;
    static final int ENTER_FULLSCREEN_VIDEO = 137;
    static final int EXIT_FULLSCREEN_VIDEO = 140;
    private static final int FIRST_PACKAGE_MSG_ID = 101;
    private static final int FIRST_PRIVATE_MSG_ID = 1;
    static final int FOCUS_NODE_CHANGED = 147;
    static final int HANDLE_ID_LEFT = 0;
    static final int HANDLE_ID_RIGHT = 1;
    static final int HIDE_FULLSCREEN = 121;
    static final int HIGHLIGHT_COLOR = 1714664933;
    static final int HIT_TEST_RESULT = 131;
    private static final float HSLOPE_TO_BREAK_SNAP = 0.4F;
    private static final float HSLOPE_TO_START_SNAP = 0.25F;
    static final String[] HandlerPackageDebugString;
    static final String[] HandlerPrivateDebugString;
    static final int INIT_EDIT_FIELD = 142;
    static final int INVAL_RECT_MSG_ID = 117;
    static final int KEY_PRESS = 145;
    private static final int LAST_PACKAGE_MSG_ID = 131;
    private static final int LAST_PRIVATE_MSG_ID = 11;
    static final String LOGTAG = "webview";
    static final int LONG_PRESS_CENTER = 114;
    private static final int LONG_PRESS_TIMEOUT = 1000;
    private static final int MAX_DURATION = 750;
    private static final float MINIMUM_VELOCITY_RATIO_FOR_ACCELERATION = 0.2F;
    private static final int MIN_FLING_TIME = 250;
    private static final float MMA_WEIGHT_N = 5.0F;
    private static final int MOTIONLESS_FALSE = 0;
    private static final int MOTIONLESS_IGNORE = 3;
    private static final int MOTIONLESS_PENDING = 1;
    private static final int MOTIONLESS_TIME = 100;
    private static final int MOTIONLESS_TRUE = 2;
    private static final int NEVER_REMEMBER_PASSWORD = 2;
    static final int NEW_PICTURE_MSG_ID = 105;
    static final int NO_LEFTEDGE = -1;
    private static final int PAGE_SCROLL_OVERLAP = 24;
    private static final int PREVENT_DEFAULT_TIMEOUT = 10;
    static final int PREVENT_TOUCH_ID = 115;
    private static final int RELEASE_SINGLE_TAP = 5;
    static final int RELOCATE_AUTO_COMPLETE_POPUP = 146;
    private static final int REMEMBER_PASSWORD = 1;
    static final int REPLACE_TEXT = 143;
    private static final int REQUEST_FORM_DATA = 6;
    static final int REQUEST_KEYBOARD = 118;
    static final int SAVE_WEBARCHIVE_FINISHED = 132;
    public static final String SCHEME_GEO = "geo:0,0?q=";
    public static final String SCHEME_MAILTO = "mailto:";
    public static final String SCHEME_TEL = "tel:";
    static final int SCREEN_ON = 136;
    private static final int SCROLLBAR_ALWAYSOFF = 1;
    private static final int SCROLLBAR_ALWAYSON = 2;
    private static final int SCROLLBAR_AUTO = 0;
    private static final int SCROLL_BITS = 6;
    static final int SCROLL_EDIT_TEXT = 149;
    private static final int SCROLL_SELECT_TEXT = 11;
    static final int SCROLL_TO_MSG_ID = 101;
    static final int SELECTION_STRING_CHANGED = 130;
    private static final int SELECT_CURSOR_OFFSET = 16;
    private static final int SELECT_SCROLL = 5;
    private static final long SELECT_SCROLL_INTERVAL = 16L;
    static final int SET_AUTOFILLABLE = 133;
    static final int SET_SCROLLBAR_MODES = 129;
    static final int SHOW_CARET_HANDLE = 151;
    static final int SHOW_FULLSCREEN = 120;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    static final int SHOW_MAGNIFIER = 5000;
    static final int SHOW_RECT_MSG_ID = 113;
    private static final int SNAP_LOCK = 1;
    private static final int SNAP_NONE = 0;
    private static final int SNAP_X = 2;
    private static final int SNAP_Y = 4;
    private static final int STD_SPEED = 480;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    static final int SWITCH_READ_MODE = 5001;
    private static final int SWITCH_TO_LONGPRESS = 4;
    private static final int SWITCH_TO_SHORTPRESS = 3;
    static final int TAKE_FOCUS = 110;
    private static final int TAP_TIMEOUT = 300;
    private static final long TEXT_SCROLL_FIRST_SCROLL_MS = 16L;
    private static final float TEXT_SCROLL_RATE = 0.01F;
    private static final int TOUCH_DONE_MODE = 7;
    private static final int TOUCH_DOUBLE_TAP_MODE = 6;
    private static final int TOUCH_DRAG_LAYER_MODE = 9;
    private static final int TOUCH_DRAG_MODE = 3;
    private static final int TOUCH_DRAG_START_MODE = 2;
    private static final int TOUCH_DRAG_TEXT_MODE = 10;
    private static final int TOUCH_HIGHLIGHT_ELAPSE_TIME = 2000;
    private static final int TOUCH_INIT_MODE = 1;
    private static final int TOUCH_PINCH_DRAG = 8;
    private static final int TOUCH_SENT_INTERVAL = 0;
    private static final int TOUCH_SHORTPRESS_MODE = 5;
    private static final int TOUCH_SHORTPRESS_START_MODE = 4;
    private static final int TRACKBALL_KEY_TIMEOUT = 1000;
    private static final int TRACKBALL_MOVE_COUNT = 10;
    private static final int TRACKBALL_MULTIPLIER = 3;
    private static final int TRACKBALL_SCALE = 400;
    private static final int TRACKBALL_SCROLL_COUNT = 5;
    private static final int TRACKBALL_TIMEOUT = 200;
    private static final int TRACKBALL_WAIT = 100;
    static final int UPDATE_CONTENT_BOUNDS = 152;
    static final int UPDATE_MATCH_COUNT = 126;
    static final int UPDATE_TEXTFIELD_TEXT_MSG_ID = 108;
    static final int UPDATE_TEXT_SELECTION_MSG_ID = 112;
    static final int UPDATE_ZOOM_DENSITY = 139;
    static final int UPDATE_ZOOM_RANGE = 109;
    private static final float VSLOPE_TO_BREAK_SNAP = 0.95F;
    private static final float VSLOPE_TO_START_SNAP = 1.25F;
    static final int WEBCORE_INITIALIZED_MSG_ID = 107;
    static final int WEBCORE_NEED_TOUCH_EVENTS = 116;
    private static final int ZOOM_BITS = 134;
    static boolean mLogEvent;
    private static Paint mOverScrollBackground;
    private static Paint mOverScrollBorder;
    private static Set<String> sGoogleApps;
    static int sMaxViewportWidth;
    private static boolean sNotificationsEnabled;
    private static boolean sPackageInstallationReceiverAdded;
    private static ProxyReceiver sProxyReceiver;
    private static TrustStorageListener sTrustStorageListener;
    private float DRAG_LAYER_INVERSE_DENSITY_SQUARED;
    private AccessibilityInjector mAccessibilityInjector;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private String mAfterStart;
    private AutoCompletePopup mAutoCompletePopup;
    private WebViewCore.AutoFillData mAutoFillData;
    private boolean mAutoRedraw;
    private int mAutoScrollX = 0;
    private int mAutoScrollY = 0;
    private float mAverageAngle;
    double mAverageSwapFps;
    private int mBackgroundColor = -1;
    ArrayList<Message> mBatchedTextChanges = new ArrayList();

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private String mBeforeStart;
    private boolean mBlockWebkitViewMessages = false;
    private int mCachedOverlappingActionModeHeight = -1;
    private CallbackProxy mCallbackProxy;
    private SslCertificate mCertificate;
    private boolean mConfirmMove;
    private int mContentHeight;
    private int mContentWidth;
    private final Context mContext;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private SelectionFloatPanel mCopyFloatPanel;
    private int mCurrentScrollingLayerId;
    private int mCurrentTouchInterval = 0;
    private WebViewDatabaseClassic mDatabase;
    private WebViewCore.DrawData mDelaySetPicture;
    private int mDoubleTapSlopSquare;
    private boolean mDrawCursorRing = true;
    private boolean mDrawHistory = false;
    Rect mEditTextContent = new Rect();
    Rect mEditTextContentBounds = new Rect();
    int mEditTextLayerId;
    Scroller mEditTextScroller;
    private int mFieldPointer;
    private FindActionModeCallback mFindCallback;
    private boolean mFindIsUp;
    private WebView.FindListener mFindListener;
    private WebViewCore.FindAllRequest mFindRequest = null;
    private FocusTransitionDrawable mFocusTransition;
    private WebViewCore.WebKitHitTest mFocusedNode;
    PluginFullScreenHolder mFullScreenHolder;
    private Point mGlobalVisibleOffset = new Point();
    private Rect mGlobalVisibleRect = new Rect();
    private boolean mGotCenterDown = false;
    private HTML5VideoViewProxy mHTML5VideoViewProxy;
    private SelectionHandleAlpha mHandleAlpha = new SelectionHandleAlpha(null);
    private ObjectAnimator mHandleAlphaAnimator;
    private boolean mHardwareAccelSkia = false;
    boolean mHeightCanMeasure;
    private int mHeldMotionless;
    private int mHistoryHeight = 0;
    private Picture mHistoryPicture = null;
    private int mHistoryWidth = 0;
    private int mHorizontalScrollBarMode = 0;
    private boolean mInOverScrollMode = false;
    private WebView.HitTestResult mInitialHitTestResult;
    private int mInitialScaleInPercent = 0;
    WebViewInputConnection mInputConnection = null;
    private WebViewInputDispatcher mInputDispatcher;
    private final Rect mInvScreenRect = new Rect();

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private boolean mIsActionUp;
    boolean mIsBatchingTextChanges = false;
    private boolean mIsCaretSelection;
    boolean mIsEditingText = false;
    private boolean mIsPaused;
    private boolean mIsWebViewVisible = true;
    private Vector<Integer> mKeysPressed;
    int mLastActualHeightSent;
    private Rect mLastCursorBounds;
    private long mLastCursorTime = 0L;
    private long mLastEditScroll = 0L;
    private Rect mLastGlobalRect = new Rect();
    int mLastHeightSent;
    private long mLastSentTouchTime;
    long mLastSwapTime;
    private long mLastTouchTime;
    private long mLastTouchUpTime = 0L;
    private int mLastTouchX;
    private int mLastTouchY;
    private float mLastVelX;
    private float mLastVelY;
    private float mLastVelocity;
    private Rect mLastVisibleRectSent = new Rect();
    int mLastWidthSent;
    private AlertDialog mListBoxDialog = null;
    private Message mListBoxMessage;
    private WebViewCore.DrawData mLoadedPicture;
    private boolean mMapTrackballToArrowKeys;
    private int mMaxAutoScrollX = 0;
    private int mMaxAutoScrollY = 0;
    private int mMaximumFling;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private int mMenuLeft = -1;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private int mMenuTop = -1;
    private int mMinAutoScrollX = 0;
    private int mMinAutoScrollY = 0;
    private int mNativeClass;
    private int mNavSlop;
    private int mOrientation = 0;
    private OverScrollGlow mOverScrollGlow;
    private int mOverflingDistance;
    private boolean mOverlayHorizontalScrollbar = true;
    private boolean mOverlayVerticalScrollbar = false;
    private int mOverscrollDistance;
    private PastePopupWindow mPasteWindow;
    private WebView.PictureListener mPictureListener;
    private boolean mPictureUpdatePausedForFocusChange = false;
    final Handler mPrivateHandler = new PrivateHandler();
    private Message mResumeMsg;
    private final Rect mScreenRect = new Rect();
    private final DrawFilter mScrollFilter = new PaintFlagsDrawFilter(6, 0);
    private Point mScrollOffset = new Point();
    OverScroller mScroller;
    private Rect mScrollingLayerBounds = new Rect();
    private Rect mScrollingLayerRect = new Rect();
    private Point mSelectCursorLeft = new Point();
    private int mSelectCursorLeftLayerId;
    private QuadF mSelectCursorLeftTextQuad = new QuadF();
    private Point mSelectCursorRight = new Point();
    private int mSelectCursorRightLayerId;
    private QuadF mSelectCursorRightTextQuad = new QuadF();
    private Point mSelectDraggingCursor;
    private Point mSelectDraggingOffset;
    private QuadF mSelectDraggingTextQuad;
    private Drawable mSelectHandleCenter;
    private Point mSelectHandleCenterOffset;
    private Drawable mSelectHandleLeft;
    private Point mSelectHandleLeftOffset;
    private Drawable mSelectHandleRight;
    private Point mSelectHandleRightOffset;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private Drawable mSelectHighlight;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private Drawable mSelectMagnifier;
    private int mSelectX = 0;
    private int mSelectY = 0;
    private boolean mSelectingText = false;
    private boolean mSelectionStarted = false;
    private boolean mSendScrollEvent = true;
    private boolean mSentAutoScrollMessage = false;
    private boolean mShowTapHighlight;
    private boolean mShowTextSelectionExtra = false;
    private boolean mSnapPositive;
    private int mSnapScrollMode = 0;
    private int mStartTouchX;
    private int mStartTouchY;
    private final Rect mTempContentVisibleRect = new Rect();
    private final Rect mTempVisibleRect = new Rect();
    private final Point mTempVisibleRectOffset = new Point();
    private int mTextGeneration;
    private Paint mTouchCrossHairColor;
    private Region mTouchHighlightRegion = new Region();
    private int mTouchHighlightX;
    private int mTouchHighlightY;
    private Paint mTouchHightlightPaint = new Paint();
    private boolean mTouchInEditText;
    private int mTouchMode = 7;
    private int mTouchSlopSquare;
    private boolean mTrackballDown = false;
    private long mTrackballFirstTime = 0L;
    private long mTrackballLastTime = 0L;
    private float mTrackballRemainsX = 0.0F;
    private float mTrackballRemainsY = 0.0F;
    private long mTrackballUpTime = 0L;
    private int mTrackballXMove = 0;
    private int mTrackballYMove = 0;
    VelocityTracker mVelocityTracker;
    private int mVerticalScrollBarMode = 0;
    ViewManager mViewManager;
    private final RectF mVisibleContentRect = new RectF();
    private Rect mVisibleRect = new Rect();
    private final WebView mWebView;
    private WebViewCore mWebViewCore;
    private final WebView.PrivateAccess mWebViewPrivate;
    boolean mWidthCanMeasure;
    private boolean mWrapContent;
    private final DrawFilter mZoomFilter = new PaintFlagsDrawFilter(134, 64);
    private ZoomManager mZoomManager;

    static
    {
        String[] arrayOfString1 = new String[11];
        arrayOfString1[0] = "REMEMBER_PASSWORD";
        arrayOfString1[1] = "NEVER_REMEMBER_PASSWORD";
        arrayOfString1[2] = "SWITCH_TO_SHORTPRESS";
        arrayOfString1[3] = "SWITCH_TO_LONGPRESS";
        arrayOfString1[4] = "RELEASE_SINGLE_TAP";
        arrayOfString1[5] = "REQUEST_FORM_DATA";
        arrayOfString1[6] = "RESUME_WEBCORE_PRIORITY";
        arrayOfString1[7] = "DRAG_HELD_MOTIONLESS";
        arrayOfString1[8] = "";
        arrayOfString1[9] = "PREVENT_DEFAULT_TIMEOUT";
        arrayOfString1[10] = "SCROLL_SELECT_TEXT";
        HandlerPrivateDebugString = arrayOfString1;
        String[] arrayOfString2 = new String[38];
        arrayOfString2[0] = "SCROLL_TO_MSG_ID";
        arrayOfString2[1] = "102";
        arrayOfString2[2] = "103";
        arrayOfString2[3] = "104";
        arrayOfString2[4] = "NEW_PICTURE_MSG_ID";
        arrayOfString2[5] = "UPDATE_TEXT_ENTRY_MSG_ID";
        arrayOfString2[6] = "WEBCORE_INITIALIZED_MSG_ID";
        arrayOfString2[7] = "UPDATE_TEXTFIELD_TEXT_MSG_ID";
        arrayOfString2[8] = "UPDATE_ZOOM_RANGE";
        arrayOfString2[9] = "UNHANDLED_NAV_KEY";
        arrayOfString2[10] = "CLEAR_TEXT_ENTRY";
        arrayOfString2[11] = "UPDATE_TEXT_SELECTION_MSG_ID";
        arrayOfString2[12] = "SHOW_RECT_MSG_ID";
        arrayOfString2[13] = "LONG_PRESS_CENTER";
        arrayOfString2[14] = "PREVENT_TOUCH_ID";
        arrayOfString2[15] = "WEBCORE_NEED_TOUCH_EVENTS";
        arrayOfString2[16] = "INVAL_RECT_MSG_ID";
        arrayOfString2[17] = "REQUEST_KEYBOARD";
        arrayOfString2[18] = "DO_MOTION_UP";
        arrayOfString2[19] = "SHOW_FULLSCREEN";
        arrayOfString2[20] = "HIDE_FULLSCREEN";
        arrayOfString2[21] = "DOM_FOCUS_CHANGED";
        arrayOfString2[22] = "REPLACE_BASE_CONTENT";
        arrayOfString2[23] = "RETURN_LABEL";
        arrayOfString2[24] = "UPDATE_MATCH_COUNT";
        arrayOfString2[25] = "CENTER_FIT_RECT";
        arrayOfString2[26] = "REQUEST_KEYBOARD_WITH_SELECTION_MSG_ID";
        arrayOfString2[27] = "SET_SCROLLBAR_MODES";
        arrayOfString2[28] = "SELECTION_STRING_CHANGED";
        arrayOfString2[29] = "SET_TOUCH_HIGHLIGHT_RECTS";
        arrayOfString2[30] = "SAVE_WEBARCHIVE_FINISHED";
        arrayOfString2[31] = "SET_AUTOFILLABLE";
        arrayOfString2[32] = "AUTOFILL_COMPLETE";
        arrayOfString2[33] = "SELECT_AT";
        arrayOfString2[34] = "SCREEN_ON";
        arrayOfString2[35] = "ENTER_FULLSCREEN_VIDEO";
        arrayOfString2[36] = "UPDATE_SELECTION";
        arrayOfString2[37] = "UPDATE_ZOOM_DENSITY";
        HandlerPackageDebugString = arrayOfString2;
        sMaxViewportWidth = 980;
        mLogEvent = true;
        sNotificationsEnabled = true;
        sPackageInstallationReceiverAdded = false;
        sGoogleApps = new HashSet();
        sGoogleApps.add("com.google.android.youtube");
    }

    public WebViewClassic(WebView paramWebView, WebView.PrivateAccess paramPrivateAccess)
    {
        SelectionHandleAlpha localSelectionHandleAlpha = this.mHandleAlpha;
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 0;
        this.mHandleAlphaAnimator = ObjectAnimator.ofInt(localSelectionHandleAlpha, "alpha", arrayOfInt);
        this.mMapTrackballToArrowKeys = true;
        this.mFocusTransition = null;
        this.mWebView = paramWebView;
        this.mWebViewPrivate = paramPrivateAccess;
        this.mContext = paramWebView.getContext();
    }

    private void abortAnimation()
    {
        this.mScroller.abortAnimation();
        this.mLastVelocity = 0.0F;
    }

    private void adjustSelectionCursors()
    {
        if (this.mIsCaretSelection)
        {
            syncSelectionCursors();
            return;
        }
        int i;
        label25: int j;
        int k;
        int i3;
        label108: int i4;
        label135: int i5;
        label152: Point localPoint1;
        label163: QuadF localQuadF;
        if (this.mSelectDraggingCursor == this.mSelectCursorLeft)
        {
            i = 1;
            j = this.mSelectDraggingCursor.x;
            k = this.mSelectDraggingCursor.y;
            int m = this.mSelectCursorLeft.x;
            int n = this.mSelectCursorLeft.y;
            int i1 = this.mSelectCursorRight.x;
            int i2 = this.mSelectCursorRight.y;
            syncSelectionCursors();
            if ((i1 == this.mSelectCursorRight.x) && (i2 == this.mSelectCursorRight.y))
                break label220;
            i3 = 1;
            if ((m == this.mSelectCursorLeft.x) && (n == this.mSelectCursorLeft.y))
                break label226;
            i4 = 1;
            if ((i4 != 0) && (i3 != 0))
            {
                if (i != 0)
                    break label232;
                i5 = 1;
                if (i5 == 0)
                    break label238;
                localPoint1 = this.mSelectCursorLeft;
                this.mSelectDraggingCursor = localPoint1;
                if (i5 == 0)
                    break label247;
                localQuadF = this.mSelectCursorLeftTextQuad;
                label180: this.mSelectDraggingTextQuad = localQuadF;
                if (i5 == 0)
                    break label256;
            }
        }
        label256: for (Point localPoint2 = this.mSelectHandleLeftOffset; ; localPoint2 = this.mSelectHandleRightOffset)
        {
            this.mSelectDraggingOffset = localPoint2;
            this.mSelectDraggingCursor.set(j, k);
            break;
            i = 0;
            break label25;
            label220: i3 = 0;
            break label108;
            label226: i4 = 0;
            break label135;
            label232: i5 = 0;
            break label152;
            label238: localPoint1 = this.mSelectCursorRight;
            break label163;
            label247: localQuadF = this.mSelectCursorRightTextQuad;
            break label180;
        }
    }

    private void beginScrollEdit()
    {
        if (this.mLastEditScroll == 0L)
        {
            this.mLastEditScroll = (SystemClock.uptimeMillis() - 16L);
            scrollEditWithCursor();
        }
    }

    private void beginTextBatch()
    {
        this.mIsBatchingTextChanges = true;
    }

    private void calcOurContentVisibleRect(Rect paramRect)
    {
        calcOurVisibleRect(paramRect);
        paramRect.left = viewToContentX(paramRect.left);
        paramRect.top = viewToContentY(paramRect.top + getVisibleTitleHeightImpl());
        paramRect.right = viewToContentX(paramRect.right);
        paramRect.bottom = viewToContentY(paramRect.bottom);
    }

    private void calcOurContentVisibleRectF(RectF paramRectF)
    {
        calcOurVisibleRect(this.mTempContentVisibleRect);
        viewToContentVisibleRect(paramRectF, this.mTempContentVisibleRect);
    }

    private void calcOurVisibleRect(Rect paramRect)
    {
        this.mWebView.getGlobalVisibleRect(paramRect, this.mGlobalVisibleOffset);
        paramRect.offset(-this.mGlobalVisibleOffset.x, -this.mGlobalVisibleOffset.y);
    }

    private Point calculateCaretTop()
    {
        float f = scaleAlongSegment(this.mSelectCursorLeft.x, this.mSelectCursorLeft.y, this.mSelectCursorLeftTextQuad.p4, this.mSelectCursorLeftTextQuad.p3);
        return new Point(Math.round(scaleCoordinate(f, this.mSelectCursorLeftTextQuad.p1.x, this.mSelectCursorLeftTextQuad.p2.x)), Math.round(scaleCoordinate(f, this.mSelectCursorLeftTextQuad.p1.y, this.mSelectCursorLeftTextQuad.p2.y)));
    }

    private float calculateDragAngle(int paramInt1, int paramInt2)
    {
        int i = Math.abs(paramInt1);
        return (float)Math.atan2(Math.abs(paramInt2), i);
    }

    private boolean canTextScroll(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        int i = getTextScrollX();
        int j = getTextScrollY();
        int k = getMaxTextScrollX();
        int m = getMaxTextScrollY();
        int n;
        int i1;
        if (paramInt1 > 0)
            if (i < k)
            {
                n = 1;
                if (paramInt2 <= 0)
                    break label97;
                if (j >= m)
                    break label91;
                i1 = 1;
            }
        while (true)
        {
            if ((n != 0) || (i1 != 0))
                bool = true;
            return bool;
            n = 0;
            break;
            if (i > 0)
            {
                n = 1;
                break;
            }
            n = 0;
            break;
            label91: i1 = 0;
            continue;
            label97: if (j > 0)
                i1 = 1;
            else
                i1 = 0;
        }
    }

    private void cancelSelectDialog()
    {
        if (this.mListBoxDialog != null)
        {
            this.mListBoxDialog.cancel();
            this.mListBoxDialog = null;
        }
    }

    private void cancelTouch()
    {
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
        if (((this.mTouchMode == 3) || (this.mTouchMode == 9)) && (!this.mSelectingText))
        {
            WebViewCore.resumePriority();
            WebViewCore.resumeUpdatePicture(this.mWebViewCore);
            nativeSetIsScrolling(false);
        }
        this.mPrivateHandler.removeMessages(3);
        this.mPrivateHandler.removeMessages(4);
        this.mPrivateHandler.removeMessages(8);
        removeTouchHighlight();
        this.mHeldMotionless = 2;
        this.mTouchMode = 7;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void clearActionModes()
    {
        if (this.mFindCallback != null)
            this.mFindCallback.finish();
    }

    private void clearHelpers()
    {
        hideSoftKeyboard();
        clearActionModes();
        dismissFullScreenMode();
        cancelSelectDialog();
    }

    private void commitTextBatch()
    {
        if (this.mWebViewCore != null)
            this.mWebViewCore.sendMessages(this.mBatchedTextChanges);
        this.mBatchedTextChanges.clear();
        this.mIsBatchingTextChanges = false;
    }

    private static int computeDuration(int paramInt1, int paramInt2)
    {
        return Math.min(1000 * Math.max(Math.abs(paramInt1), Math.abs(paramInt2)) / 480, 750);
    }

    private int computeRealHorizontalScrollRange()
    {
        if (this.mDrawHistory);
        for (int i = this.mHistoryWidth; ; i = (int)Math.floor(this.mContentWidth * this.mZoomManager.getScale()))
            return i;
    }

    private int computeRealVerticalScrollRange()
    {
        if (this.mDrawHistory);
        for (int i = this.mHistoryHeight; ; i = (int)Math.floor(this.mContentHeight * this.mZoomManager.getScale()))
            return i;
    }

    private void contentScrollTo(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (this.mDrawHistory);
        while (true)
        {
            return;
            pinScrollTo(contentToViewX(paramInt1), contentToViewY(paramInt2), paramBoolean, 0);
        }
    }

    private void contentSizeChanged(boolean paramBoolean)
    {
        if ((this.mContentWidth | this.mContentHeight) == 0);
        while (true)
        {
            return;
            if (this.mHeightCanMeasure)
            {
                if ((this.mWebView.getMeasuredHeight() != contentToViewDimension(this.mContentHeight)) || (paramBoolean))
                    this.mWebView.requestLayout();
            }
            else if (this.mWidthCanMeasure)
            {
                if ((this.mWebView.getMeasuredWidth() != contentToViewDimension(this.mContentWidth)) || (paramBoolean))
                    this.mWebView.requestLayout();
            }
            else
                sendViewSizeZoom(false);
        }
    }

    private Rect contentToViewRect(Rect paramRect)
    {
        return new Rect(contentToViewX(paramRect.left), contentToViewY(paramRect.top), contentToViewX(paramRect.right), contentToViewY(paramRect.bottom));
    }

    private void copyToClipboard(String paramString)
    {
        ((ClipboardManager)this.mContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(getTitle(), paramString));
    }

    private void destroyJava()
    {
        this.mCallbackProxy.blockMessages();
        clearHelpers();
        if (this.mListBoxDialog != null)
        {
            this.mListBoxDialog.dismiss();
            this.mListBoxDialog = null;
        }
        if (this.mWebViewCore != null);
        try
        {
            WebViewCore localWebViewCore = this.mWebViewCore;
            this.mWebViewCore = null;
            localWebViewCore.destroy();
            this.mPrivateHandler.removeCallbacksAndMessages(null);
            return;
        }
        finally
        {
        }
    }

    private void destroyNative()
    {
        if (this.mNativeClass == 0);
        while (true)
        {
            return;
            int i = this.mNativeClass;
            this.mNativeClass = 0;
            if (Thread.currentThread() == this.mPrivateHandler.getLooper().getThread())
                nativeDestroy(i);
            else
                this.mPrivateHandler.post(new DestroyNativeRunnable(i));
        }
    }

    @Deprecated
    public static void disablePlatformNotifications()
    {
        try
        {
            sNotificationsEnabled = false;
            Context localContext = JniUtil.getContext();
            if (localContext != null)
                disableProxyListener(localContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private static void disableProxyListener(Context paramContext)
    {
        try
        {
            ProxyReceiver localProxyReceiver = sProxyReceiver;
            if (localProxyReceiver == null);
            while (true)
            {
                return;
                paramContext.getApplicationContext().unregisterReceiver(sProxyReceiver);
                sProxyReceiver = null;
            }
        }
        finally
        {
        }
    }

    private void dismissFullScreenMode()
    {
        if (inFullScreenMode())
        {
            this.mFullScreenHolder.hide();
            this.mFullScreenHolder = null;
            invalidate();
        }
    }

    private void displaySoftKeyboard(boolean paramBoolean)
    {
        InputMethodManager localInputMethodManager = (InputMethodManager)this.mContext.getSystemService("input_method");
        if (this.mZoomManager.getScale() < this.mZoomManager.getDefaultScale());
        for (int i = 1; ; i = 0)
        {
            if (i != 0)
            {
                this.mZoomManager.setZoomCenter(this.mLastTouchX, this.mLastTouchY);
                this.mZoomManager.setZoomScale(this.mZoomManager.getDefaultScale(), false);
            }
            localInputMethodManager.showSoftInput(this.mWebView, 0);
            return;
        }
    }

    private float distanceSquared(int paramInt1, int paramInt2, Point paramPoint)
    {
        float f1 = paramPoint.x - paramInt1;
        float f2 = paramPoint.y - paramInt2;
        return f1 * f1 + f2 * f2;
    }

    private boolean doDrag(int paramInt1, int paramInt2)
    {
        boolean bool = true;
        int i;
        int j;
        int k;
        int m;
        int n;
        int i1;
        if ((paramInt1 | paramInt2) != 0)
        {
            i = getScrollX();
            j = getScrollY();
            k = computeMaxScrollX();
            m = computeMaxScrollY();
            n = (int)Math.floor(paramInt1 * this.mZoomManager.getInvScale());
            i1 = (int)Math.floor(paramInt2 * this.mZoomManager.getInvScale());
            this.mTouchMode = 3;
            if ((!this.mIsEditingText) || (!this.mTouchInEditText) || (!canTextScroll(paramInt1, paramInt2)))
                break label216;
            i = getTextScrollX();
            k = getMaxTextScrollX();
            paramInt1 = n;
            j = getTextScrollY();
            m = getMaxTextScrollY();
            paramInt2 = i1;
            this.mTouchMode = 10;
            bool = false;
        }
        while (true)
        {
            if (this.mOverScrollGlow != null)
                this.mOverScrollGlow.setOverScrollDeltas(paramInt1, paramInt2);
            WebView.PrivateAccess localPrivateAccess = this.mWebViewPrivate;
            int i6 = this.mOverscrollDistance;
            int i7 = this.mOverscrollDistance;
            localPrivateAccess.overScrollBy(paramInt1, paramInt2, i, j, k, m, i6, i7, true);
            if ((this.mOverScrollGlow != null) && (this.mOverScrollGlow.isAnimating()))
                invalidate();
            this.mZoomManager.keepZoomPickerVisible();
            return bool;
            label216: if (this.mCurrentScrollingLayerId != 0)
            {
                int i2 = this.mScrollingLayerRect.right;
                int i3 = this.mScrollingLayerRect.bottom;
                int i4 = Math.max(0, Math.min(n + this.mScrollingLayerRect.left, i2));
                int i5 = Math.max(0, Math.min(i1 + this.mScrollingLayerRect.top, i3));
                if ((i4 != this.mScrollingLayerRect.left) || (i5 != this.mScrollingLayerRect.top) || ((n | i1) == 0))
                {
                    this.mTouchMode = 9;
                    paramInt1 = n;
                    paramInt2 = i1;
                    i = this.mScrollingLayerRect.left;
                    j = this.mScrollingLayerRect.top;
                    k = i2;
                    m = i3;
                    bool = false;
                }
            }
        }
    }

    private void doFling()
    {
        if (this.mVelocityTracker == null)
            return;
        int i = computeMaxScrollX();
        int j = computeMaxScrollY();
        this.mVelocityTracker.computeCurrentVelocity(1000, this.mMaximumFling);
        int k = (int)this.mVelocityTracker.getXVelocity();
        int m = (int)this.mVelocityTracker.getYVelocity();
        int n = getScrollX();
        int i1 = getScrollY();
        int i2 = this.mOverscrollDistance;
        int i3 = this.mOverflingDistance;
        if (this.mTouchMode == 9)
        {
            n = this.mScrollingLayerRect.left;
            i1 = this.mScrollingLayerRect.top;
            i = this.mScrollingLayerRect.right;
            j = this.mScrollingLayerRect.bottom;
            i3 = 0;
            i2 = 0;
            label125: if (this.mSnapScrollMode != 0)
            {
                if ((0x2 & this.mSnapScrollMode) != 2)
                    break label244;
                m = 0;
            }
        }
        while (true)
        {
            if (((i != 0) || (m != 0)) && ((j != 0) || (k != 0)))
                break label249;
            WebViewCore.resumePriority();
            if (!this.mSelectingText)
                WebViewCore.resumeUpdatePicture(this.mWebViewCore);
            if (!this.mScroller.springBack(n, i1, 0, i, 0, j))
                break;
            invalidate();
            break;
            if (this.mTouchMode != 10)
                break label125;
            n = getTextScrollX();
            i1 = getTextScrollY();
            i = getMaxTextScrollX();
            j = getMaxTextScrollY();
            i3 = 0;
            i2 = 0;
            break label125;
            label244: k = 0;
        }
        label249: float f1 = this.mScroller.getCurrVelocity();
        float f2 = (float)Math.hypot(k, m);
        if ((this.mLastVelocity > 0.0F) && (f1 > 0.0F) && (f2 > 0.2F * this.mLastVelocity))
        {
            float f3 = (float)Math.abs(Math.atan2(this.mLastVelY, this.mLastVelX) - Math.atan2(m, k));
            if ((f3 > 5.654867F) || (f3 < 0.6283186F))
            {
                k = (int)(k + f1 * this.mLastVelX / this.mLastVelocity);
                m = (int)(m + f1 * this.mLastVelY / this.mLastVelocity);
                f2 = (float)Math.hypot(k, m);
            }
        }
        if (((n == 0) || (n == i)) && (Math.abs(k) < Math.abs(m)))
            k = 0;
        if (((i1 == 0) || (i1 == j)) && (Math.abs(m) < Math.abs(k)))
            m = 0;
        if (i2 < i3)
        {
            if (((k > 0) && (n == -i2)) || ((k < 0) && (n == i + i2)))
                k = 0;
            if (((m > 0) && (i1 == -i2)) || ((m < 0) && (i1 == j + i2)))
                m = 0;
        }
        this.mLastVelX = k;
        this.mLastVelY = m;
        this.mLastVelocity = f2;
        OverScroller localOverScroller = this.mScroller;
        int i4 = -k;
        int i5 = -m;
        if (i == 0);
        for (int i6 = 0; ; i6 = i3)
        {
            localOverScroller.fling(n, i1, i4, i5, 0, i, 0, j, i6, i3);
            invalidate();
            break;
        }
    }

    private void doTrackball(long paramLong, int paramInt)
    {
        int i = (int)(this.mTrackballLastTime - this.mTrackballFirstTime);
        if (i == 0)
            i = 200;
        float f1 = 1000.0F * this.mTrackballRemainsX / i;
        float f2 = 1000.0F * this.mTrackballRemainsY / i;
        int j = getViewWidth();
        int k = getViewHeight();
        Math.max(Math.abs(f1), Math.abs(f2));
        int m = this.mContentWidth - j;
        int n = this.mContentHeight - k;
        if (m < 0)
            m = 0;
        if (n < 0)
            n = 0;
        float f3 = Math.abs(3.0F * this.mTrackballRemainsX);
        float f4 = Math.abs(3.0F * this.mTrackballRemainsY);
        int i1 = Math.max(0, (int)Math.max(f3, f4));
        int i2 = getScrollX();
        int i3 = getScrollY();
        if (i1 > 0)
        {
            int i6;
            if (f3 < f4)
                if (this.mTrackballRemainsY < 0.0F)
                    i6 = 19;
            while (true)
            {
                i1 = Math.min(i1, 10);
                if (this.mNativeClass == 0)
                    break label268;
                for (int i7 = 0; i7 < i1; i7++)
                    letPageHandleNavKey(i6, paramLong, true, paramInt);
                i6 = 20;
                continue;
                if (this.mTrackballRemainsX < 0.0F)
                    i6 = 21;
                else
                    i6 = 22;
            }
            letPageHandleNavKey(i6, paramLong, false, paramInt);
            label268: this.mTrackballRemainsY = 0.0F;
            this.mTrackballRemainsX = 0.0F;
        }
        if (i1 >= 5)
        {
            int i4 = scaleTrackballX(f1, m);
            int i5 = scaleTrackballY(f2, n);
            if (Math.abs(getScrollX() - i2) > Math.abs(i4))
                i4 = 0;
            if (Math.abs(getScrollY() - i3) > Math.abs(i5))
                i5 = 0;
            if ((i4 != 0) || (i5 != 0))
                pinScrollBy(i4, i5, true, 0);
        }
    }

    private void drawContent(Canvas paramCanvas)
    {
        if (this.mDrawHistory)
        {
            paramCanvas.scale(this.mZoomManager.getScale(), this.mZoomManager.getScale());
            paramCanvas.drawPicture(this.mHistoryPicture);
            return;
        }
        int i;
        label84: int j;
        label164: int k;
        int m;
        Rect localRect1;
        if (this.mNativeClass != 0)
        {
            boolean bool = this.mZoomManager.isFixedLengthAnimationInProgress();
            if (((this.mScroller.isFinished()) && (this.mVelocityTracker == null)) || ((this.mTouchMode == 3) && (this.mHeldMotionless == 2)))
                break label370;
            i = 1;
            if (this.mTouchMode == 3)
            {
                if (this.mHeldMotionless == 1)
                {
                    this.mPrivateHandler.removeMessages(8);
                    this.mHeldMotionless = 0;
                }
                if (this.mHeldMotionless == 0)
                {
                    this.mPrivateHandler.sendMessageDelayed(this.mPrivateHandler.obtainMessage(8), 100L);
                    this.mHeldMotionless = 1;
                }
            }
            j = paramCanvas.save();
            if (!bool)
                break label375;
            this.mZoomManager.animateZoom(paramCanvas);
            k = 0;
            if ((this.mNativeClass != 0) && (!paramCanvas.isHardwareAccelerated()) && (nativeEvaluateLayersAnimations(this.mNativeClass)))
            {
                k = 1;
                this.mWebViewCore.sendMessage(196);
                invalidate();
            }
            m = 0;
            if ((!this.mFindIsUp) && (this.mShowTextSelectionExtra))
                m = 1;
            calcOurContentVisibleRectF(this.mVisibleContentRect);
            if (!paramCanvas.isHardwareAccelerated())
                break label415;
            if (!this.mIsWebViewVisible)
                break label403;
            localRect1 = this.mInvScreenRect;
            label257: if (!this.mIsWebViewVisible)
                break label409;
        }
        label403: label409: for (Rect localRect2 = this.mScreenRect; ; localRect2 = null)
        {
            int n = nativeCreateDrawGLFunction(this.mNativeClass, localRect1, localRect2, this.mVisibleContentRect, getScale(), m);
            ((HardwareCanvas)paramCanvas).callDrawGLFunction(n);
            if (this.mHardwareAccelSkia != getSettings().getHardwareAccelSkiaEnabled())
            {
                this.mHardwareAccelSkia = getSettings().getHardwareAccelSkiaEnabled();
                nativeUseHardwareAccelSkia(this.mHardwareAccelSkia);
            }
            paramCanvas.restoreToCount(j);
            drawTextSelectionHandles(paramCanvas);
            if ((m != 2) || (this.mTouchMode != 4))
                break;
            this.mTouchMode = 5;
            break;
            break;
            label370: i = 0;
            break label84;
            label375: if (paramCanvas.isHardwareAccelerated())
                break label164;
            paramCanvas.scale(this.mZoomManager.getScale(), this.mZoomManager.getScale());
            break label164;
            localRect1 = null;
            break label257;
        }
        label415: DrawFilter localDrawFilter = null;
        if ((this.mZoomManager.isZoomAnimating()) || (k != 0))
            localDrawFilter = this.mZoomFilter;
        while (true)
        {
            paramCanvas.setDrawFilter(localDrawFilter);
            nativeDraw(paramCanvas, this.mVisibleContentRect, this.mBackgroundColor, m);
            paramCanvas.setDrawFilter(null);
            break;
            if (i != 0)
                localDrawFilter = this.mScrollFilter;
        }
    }

    private void drawOverScrollBackground(Canvas paramCanvas)
    {
        if (mOverScrollBackground == null)
        {
            mOverScrollBackground = new Paint();
            Bitmap localBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), 17302881);
            mOverScrollBackground.setShader(new BitmapShader(localBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
            mOverScrollBorder = new Paint();
            mOverScrollBorder.setStyle(Paint.Style.STROKE);
            mOverScrollBorder.setStrokeWidth(0.0F);
            mOverScrollBorder.setColor(-4473925);
        }
        int i = computeRealHorizontalScrollRange();
        int j = 0 + computeRealVerticalScrollRange();
        paramCanvas.save();
        paramCanvas.translate(getScrollX(), getScrollY());
        paramCanvas.clipRect(-getScrollX(), 0 - getScrollY(), i - getScrollX(), j - getScrollY(), Region.Op.DIFFERENCE);
        paramCanvas.drawPaint(mOverScrollBackground);
        paramCanvas.restore();
        paramCanvas.drawRect(-1.0F, -1, i, j, mOverScrollBorder);
        paramCanvas.clipRect(0, 0, i, j);
    }

    private void drawTextSelectionHandles(Canvas paramCanvas)
    {
        if (this.mHandleAlpha.getAlpha() == 0);
        while (true)
        {
            return;
            ensureSelectionHandles();
            Rect localRect1 = new Rect(-1, -1, -1, -1);
            Rect localRect2 = new Rect(-1, -1, -1, -1);
            int i;
            int j;
            int k;
            int m;
            int n;
            if (this.mSelectingText)
            {
                int[] arrayOfInt = new int[4];
                getSelectionHandles(arrayOfInt);
                i = contentToViewDimension(arrayOfInt[0]);
                j = contentToViewDimension(arrayOfInt[1]);
                k = contentToViewDimension(arrayOfInt[2]);
                m = contentToViewDimension(arrayOfInt[3]);
                if (!this.mIsCaretSelection)
                    break label219;
                n = i - this.mSelectHandleCenter.getIntrinsicWidth() / 2;
                this.mSelectHandleCenter.setBounds(n, j, n + this.mSelectHandleCenter.getIntrinsicWidth(), j + this.mSelectHandleCenter.getIntrinsicHeight());
            }
            while (true)
            {
                localRect1 = new Rect(n, j, n + 60, j + 60);
                localRect2 = new Rect(k, m, k + 60, m + 60);
                if (!this.mIsCaretSelection)
                    break label314;
                this.mSelectHandleCenter.draw(paramCanvas);
                break;
                label219: n = i - 3 * this.mSelectHandleLeft.getIntrinsicWidth() / 4;
                this.mSelectHandleLeft.setBounds(n, j, n + this.mSelectHandleLeft.getIntrinsicWidth(), j + this.mSelectHandleLeft.getIntrinsicHeight());
                k -= this.mSelectHandleRight.getIntrinsicWidth() / 4;
                this.mSelectHandleRight.setBounds(k, m, k + this.mSelectHandleRight.getIntrinsicWidth(), m + this.mSelectHandleRight.getIntrinsicHeight());
            }
            label314: this.mSelectHandleLeft.draw(paramCanvas);
            this.mSelectHandleRight.draw(paramCanvas);
            if (this.mSelectingText)
                showMagnifier(paramCanvas, localRect1, localRect2);
        }
    }

    @Deprecated
    public static void enablePlatformNotifications()
    {
        try
        {
            sNotificationsEnabled = true;
            Context localContext = JniUtil.getContext();
            if (localContext != null)
                setupProxyListener(localContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void endScrollEdit()
    {
        this.mLastEditScroll = 0L;
    }

    private void endSelectingText()
    {
        this.mSelectingText = false;
        this.mShowTextSelectionExtra = false;
        ObjectAnimator localObjectAnimator = this.mHandleAlphaAnimator;
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 0;
        localObjectAnimator.setIntValues(arrayOfInt);
        this.mHandleAlphaAnimator.start();
    }

    private void ensureFunctorDetached()
    {
        if (this.mWebView.isHardwareAccelerated())
        {
            int i = nativeGetDrawGLFunction(this.mNativeClass);
            ViewRootImpl localViewRootImpl = this.mWebView.getViewRootImpl();
            if ((i != 0) && (localViewRootImpl != null))
                localViewRootImpl.detachFunctor(i);
        }
    }

    private void ensureSelectionHandles()
    {
        if (this.mSelectHandleCenter == null)
        {
            this.mSelectHandleCenter = this.mContext.getResources().getDrawable(17302971).mutate();
            this.mSelectHandleLeft = this.mContext.getResources().getDrawable(17302970).mutate();
            this.mSelectHandleRight = this.mContext.getResources().getDrawable(17302972).mutate();
            this.mHandleAlpha.setAlpha(this.mHandleAlpha.getAlpha());
            this.mSelectHandleCenterOffset = new Point(0, -this.mSelectHandleCenter.getIntrinsicHeight());
            this.mSelectHandleLeftOffset = new Point(0, -this.mSelectHandleLeft.getIntrinsicHeight());
            this.mSelectHandleRightOffset = new Point(-this.mSelectHandleLeft.getIntrinsicWidth() / 2, -this.mSelectHandleRight.getIntrinsicHeight());
        }
    }

    private boolean extendScroll(int paramInt)
    {
        boolean bool = false;
        int i = this.mScroller.getFinalY();
        int j = pinLocY(i + paramInt);
        if (j == i);
        while (true)
        {
            return bool;
            this.mScroller.setFinalY(j);
            this.mScroller.extendDuration(computeDuration(0, paramInt));
            bool = true;
        }
    }

    public static String findAddress(String paramString)
    {
        return findAddress(paramString, false);
    }

    public static String findAddress(String paramString, boolean paramBoolean)
    {
        return WebViewCore.nativeFindAddress(paramString, paramBoolean);
    }

    private int findAllBody(String paramString, boolean paramBoolean)
    {
        int i = 0;
        if (this.mNativeClass == 0);
        while (true)
        {
            return i;
            this.mFindRequest = null;
            if (paramString != null)
            {
                this.mWebViewCore.removeMessages(221);
                this.mFindRequest = new WebViewCore.FindAllRequest(paramString);
                if (paramBoolean)
                {
                    this.mWebViewCore.sendMessage(221, this.mFindRequest);
                }
                else
                {
                    synchronized (this.mFindRequest)
                    {
                        try
                        {
                            this.mWebViewCore.sendMessageAtFrontOfQueue(221, this.mFindRequest);
                            while (this.mFindRequest.mMatchCount == -1)
                                this.mFindRequest.wait();
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                        }
                    }
                    i = this.mFindRequest.mMatchCount;
                }
            }
        }
    }

    public static WebViewClassic fromWebView(WebView paramWebView)
    {
        if (paramWebView == null);
        for (WebViewClassic localWebViewClassic = null; ; localWebViewClassic = (WebViewClassic)paramWebView.getWebViewProvider())
            return localWebViewClassic;
    }

    private AccessibilityInjector getAccessibilityInjector()
    {
        if (this.mAccessibilityInjector == null)
            this.mAccessibilityInjector = new AccessibilityInjector(this);
        return this.mAccessibilityInjector;
    }

    private int getMaxTextScrollX()
    {
        return Math.max(0, this.mEditTextContent.width() - this.mEditTextContentBounds.width());
    }

    private int getMaxTextScrollY()
    {
        return Math.max(0, this.mEditTextContent.height() - this.mEditTextContentBounds.height());
    }

    private int getOverlappingActionModeHeight()
    {
        int i = 0;
        if (this.mFindCallback == null);
        while (true)
        {
            return i;
            if (this.mCachedOverlappingActionModeHeight < 0)
            {
                this.mWebView.getGlobalVisibleRect(this.mGlobalVisibleRect, this.mGlobalVisibleOffset);
                this.mCachedOverlappingActionModeHeight = Math.max(0, this.mFindCallback.getActionModeGlobalBottom() - this.mGlobalVisibleRect.top);
            }
            i = this.mCachedOverlappingActionModeHeight;
        }
    }

    @Deprecated
    public static PluginList getPluginList()
    {
        try
        {
            PluginList localPluginList = new PluginList();
            return localPluginList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private int getScaledMaxXScroll()
    {
        if (!this.mHeightCanMeasure);
        Rect localRect;
        for (int i = getViewWidth() / 4; ; i = localRect.width() / 2)
        {
            return viewToContentX(i);
            localRect = new Rect();
            calcOurVisibleRect(localRect);
        }
    }

    private int getScaledMaxYScroll()
    {
        if (!this.mHeightCanMeasure);
        Rect localRect;
        for (int i = getViewHeight() / 4; ; i = localRect.height() / 2)
        {
            return Math.round(i * this.mZoomManager.getInvScale());
            localRect = new Rect();
            calcOurVisibleRect(localRect);
        }
    }

    private void getSelectionHandles(int[] paramArrayOfInt)
    {
        paramArrayOfInt[0] = this.mSelectCursorLeft.x;
        paramArrayOfInt[1] = this.mSelectCursorLeft.y;
        paramArrayOfInt[2] = this.mSelectCursorRight.x;
        paramArrayOfInt[3] = this.mSelectCursorRight.y;
    }

    private static int getTextScrollDelta(float paramFloat, long paramLong)
    {
        float f1 = paramFloat * (float)paramLong;
        int i = (int)Math.floor(f1);
        float f2 = f1 - i;
        if (Math.random() < f2)
            i++;
        return i;
    }

    private static float getTextScrollSpeed(int paramInt1, int paramInt2, int paramInt3)
    {
        float f;
        if (paramInt1 < paramInt2)
            f = 0.01F * (paramInt1 - paramInt2);
        while (true)
        {
            return f;
            if (paramInt1 >= paramInt3)
                f = 0.01F * (1 + (paramInt1 - paramInt3));
            else
                f = 0.0F;
        }
    }

    private int getTextScrollX()
    {
        return -this.mEditTextContent.left;
    }

    private int getTextScrollY()
    {
        return -this.mEditTextContent.top;
    }

    private int getVisibleTitleHeightImpl()
    {
        return Math.max(getTitleHeight() - Math.max(0, getScrollY()), getOverlappingActionModeHeight());
    }

    private void goBackOrForward(int paramInt, boolean paramBoolean)
    {
        WebViewCore localWebViewCore;
        if (paramInt != 0)
        {
            clearHelpers();
            localWebViewCore = this.mWebViewCore;
            if (!paramBoolean)
                break label30;
        }
        label30: for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(106, paramInt, i);
            return;
        }
    }

    private void goBackOrForwardImpl(int paramInt)
    {
        goBackOrForward(paramInt, false);
    }

    private static void handleCertTrustChanged()
    {
        WebViewCore.sendStaticMessage(220, null);
    }

    private static void handleProxyBroadcast(Intent paramIntent)
    {
        ProxyProperties localProxyProperties = (ProxyProperties)paramIntent.getExtra("proxy");
        if ((localProxyProperties == null) || (localProxyProperties.getHost() == null))
            WebViewCore.sendStaticMessage(193, null);
        while (true)
        {
            return;
            WebViewCore.sendStaticMessage(193, localProxyProperties);
        }
    }

    private void handleTouchEventCommon(MotionEvent paramMotionEvent, int paramInt1, int paramInt2, int paramInt3)
    {
        ScaleGestureDetector localScaleGestureDetector = this.mZoomManager.getScaleGestureDetector();
        long l = paramMotionEvent.getEventTime();
        int i = Math.min(paramInt2, -1 + getViewWidth());
        int j = Math.min(paramInt3, -1 + getViewHeightWithTitle());
        int k = this.mLastTouchX - i;
        int m = this.mLastTouchY - j;
        int n = viewToContentX(i + getScrollX());
        int i1 = viewToContentY(j + getScrollY());
        switch (paramInt1)
        {
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            return;
            this.mConfirmMove = false;
            if (!this.mEditTextScroller.isFinished())
                this.mEditTextScroller.abortAnimation();
            if (!this.mScroller.isFinished())
            {
                this.mScroller.abortAnimation();
                this.mTouchMode = 2;
                this.mConfirmMove = true;
                nativeSetIsScrolling(false);
            }
            while (true)
            {
                if ((!this.mSelectingText) && ((this.mTouchMode == 1) || (this.mTouchMode == 6)))
                {
                    this.mPrivateHandler.sendEmptyMessageDelayed(3, 300L);
                    this.mPrivateHandler.sendEmptyMessageDelayed(4, 1000L);
                }
                startTouch(i, j, l);
                if (!this.mIsEditingText)
                    break;
                this.mTouchInEditText = this.mEditTextContentBounds.contains(n, i1);
                break;
                if (this.mPrivateHandler.hasMessages(5))
                {
                    this.mPrivateHandler.removeMessages(5);
                    removeTouchHighlight();
                    if (k * k + m * m < this.mDoubleTapSlopSquare)
                        this.mTouchMode = 6;
                    else
                        this.mTouchMode = 1;
                }
                else
                {
                    this.mTouchMode = 1;
                    if ((mLogEvent) && (l - this.mLastTouchUpTime < 1000L))
                    {
                        Object[] arrayOfObject = new Object[2];
                        arrayOfObject[0] = Long.valueOf(l - this.mLastTouchUpTime);
                        arrayOfObject[1] = Long.valueOf(l);
                        EventLog.writeEvent(70102, arrayOfObject);
                    }
                    this.mSelectionStarted = false;
                    if (this.mSelectingText)
                    {
                        ensureSelectionHandles();
                        int i6 = j - getTitleHeight() + getScrollY();
                        int i7 = i + getScrollX();
                        if ((this.mSelectHandleCenter != null) && (this.mSelectHandleCenter.getBounds().contains(i7, i6)))
                        {
                            this.mSelectionStarted = true;
                            this.mSelectDraggingCursor = this.mSelectCursorLeft;
                            this.mSelectDraggingOffset = this.mSelectHandleCenterOffset;
                            this.mSelectDraggingTextQuad = this.mSelectCursorLeftTextQuad;
                            this.mPrivateHandler.removeMessages(144);
                            hidePasteButton();
                        }
                        else if ((this.mSelectHandleLeft != null) && (this.mSelectHandleLeft.getBounds().contains(i7, i6)))
                        {
                            this.mSelectionStarted = true;
                            this.mSelectDraggingOffset = this.mSelectHandleLeftOffset;
                            this.mSelectDraggingCursor = this.mSelectCursorLeft;
                            this.mSelectDraggingTextQuad = this.mSelectCursorLeftTextQuad;
                        }
                        else if ((this.mSelectHandleRight != null) && (this.mSelectHandleRight.getBounds().contains(i7, i6)))
                        {
                            this.mSelectionStarted = true;
                            this.mSelectDraggingOffset = this.mSelectHandleRightOffset;
                            this.mSelectDraggingCursor = this.mSelectCursorRight;
                            this.mSelectDraggingTextQuad = this.mSelectCursorRightTextQuad;
                        }
                        else if (this.mIsCaretSelection)
                        {
                            selectionDone();
                        }
                    }
                }
            }
            if ((!this.mConfirmMove) && (k * k + m * m >= this.mTouchSlopSquare))
            {
                this.mPrivateHandler.removeMessages(3);
                this.mPrivateHandler.removeMessages(4);
                this.mConfirmMove = true;
                if (this.mTouchMode == 6)
                    this.mTouchMode = 1;
                removeTouchHighlight();
            }
            if ((this.mSelectingText) && (this.mSelectionStarted))
            {
                ViewParent localViewParent = this.mWebView.getParent();
                if (localViewParent != null)
                    localViewParent.requestDisallowInterceptTouchEvent(true);
                if ((k != 0) || (m != 0))
                {
                    int i4 = n + viewToContentDimension(this.mSelectDraggingOffset.x);
                    int i5 = i1 + viewToContentDimension(this.mSelectDraggingOffset.y);
                    this.mSelectDraggingCursor.set(i4, i5);
                    boolean bool5 = this.mSelectDraggingTextQuad.containsPoint(i4, i5);
                    boolean bool6 = this.mEditTextContentBounds.contains(i4, i5);
                    if ((this.mIsEditingText) && (!bool6))
                        beginScrollEdit();
                    while (true)
                    {
                        if ((bool5) || ((this.mIsEditingText) && (!bool6)))
                            snapDraggingCursor();
                        updateWebkitSelection();
                        if ((!bool5) && (this.mIsEditingText) && (bool6))
                            snapDraggingCursor();
                        this.mLastTouchX = i;
                        this.mLastTouchY = j;
                        invalidate();
                        break;
                        endScrollEdit();
                    }
                }
            }
            else if (this.mTouchMode != 7)
            {
                if (this.mVelocityTracker == null)
                {
                    Log.e("webview", "Got null mVelocityTracker when    mTouchMode = " + this.mTouchMode);
                    label926: if ((this.mTouchMode != 3) && (this.mTouchMode != 9) && (this.mTouchMode != 10))
                    {
                        if (!this.mConfirmMove)
                            continue;
                        this.mAverageAngle = calculateDragAngle(k, m);
                        if ((localScaleGestureDetector == null) || (!localScaleGestureDetector.isInProgress()))
                        {
                            if (this.mAverageAngle >= 0.25F)
                                break label1085;
                            this.mSnapScrollMode = 2;
                            if (k <= 0)
                                break label1079;
                            bool4 = true;
                            this.mSnapPositive = bool4;
                            this.mAverageAngle = 0.0F;
                        }
                    }
                }
                else
                {
                    label1079: label1085: 
                    while (this.mAverageAngle <= 1.25F)
                        while (true)
                        {
                            this.mTouchMode = 3;
                            this.mLastTouchX = i;
                            this.mLastTouchY = j;
                            k = 0;
                            m = 0;
                            startScrollingLayer(i, j);
                            startDrag();
                            if ((k != 0) || (m != 0))
                                break label1129;
                            break;
                            this.mVelocityTracker.addMovement(paramMotionEvent);
                            break label926;
                            boolean bool4 = false;
                        }
                    this.mSnapScrollMode = 4;
                    if (m > 0);
                    for (boolean bool3 = true; ; bool3 = false)
                    {
                        this.mSnapPositive = bool3;
                        this.mAverageAngle = 2.0F;
                        break;
                    }
                    label1129: this.mAverageAngle += (calculateDragAngle(k, m) - this.mAverageAngle) / 5.0F;
                    if (this.mSnapScrollMode != 0)
                    {
                        if ((this.mSnapScrollMode == 4) && (this.mAverageAngle < 0.95F))
                            this.mSnapScrollMode = 0;
                        if ((this.mSnapScrollMode == 2) && (this.mAverageAngle > 0.4F))
                            this.mSnapScrollMode = 0;
                        label1208: if (this.mSnapScrollMode != 0)
                        {
                            if ((0x2 & this.mSnapScrollMode) != 2)
                                break label1384;
                            m = 0;
                        }
                        label1228: if (k * k + m * m <= this.mTouchSlopSquare)
                            break label1390;
                    }
                    label1384: label1390: for (this.mHeldMotionless = 0; ; this.mHeldMotionless = 2)
                    {
                        this.mLastTouchTime = l;
                        if (!doDrag(k, m))
                            break label1398;
                        this.mLastTouchX = i;
                        this.mLastTouchY = j;
                        break;
                        if (this.mAverageAngle < 0.25F)
                        {
                            this.mSnapScrollMode = 2;
                            if (k > 0);
                            for (boolean bool2 = true; ; bool2 = false)
                            {
                                this.mSnapPositive = bool2;
                                this.mAverageAngle = ((0.0F + this.mAverageAngle) / 2.0F);
                                break;
                            }
                        }
                        if (this.mAverageAngle <= 1.25F)
                            break label1208;
                        this.mSnapScrollMode = 4;
                        if (m > 0);
                        for (boolean bool1 = true; ; bool1 = false)
                        {
                            this.mSnapPositive = bool1;
                            this.mAverageAngle = ((2.0F + this.mAverageAngle) / 2.0F);
                            break;
                        }
                        k = 0;
                        break label1228;
                    }
                    label1398: int i2 = contentToViewDimension((int)Math.floor(k * this.mZoomManager.getInvScale()));
                    int i3 = contentToViewDimension((int)Math.floor(m * this.mZoomManager.getInvScale()));
                    this.mLastTouchX -= i2;
                    this.mLastTouchY -= i3;
                    continue;
                    endScrollEdit();
                    if ((!this.mConfirmMove) && (this.mIsEditingText) && (this.mSelectionStarted) && (this.mIsCaretSelection))
                    {
                        showPasteWindow();
                        stopTouch();
                    }
                    else
                    {
                        this.mLastTouchUpTime = l;
                        if (this.mSentAutoScrollMessage)
                        {
                            this.mAutoScrollY = 0;
                            this.mAutoScrollX = 0;
                        }
                        switch (this.mTouchMode)
                        {
                        case 7:
                        case 8:
                        default:
                        case 6:
                        case 1:
                        case 4:
                        case 5:
                        case 3:
                        case 9:
                        case 10:
                        case 2:
                        }
                        while (true)
                        {
                            stopTouch();
                            break;
                            this.mPrivateHandler.removeMessages(3);
                            this.mPrivateHandler.removeMessages(4);
                            this.mTouchMode = 7;
                            continue;
                            this.mPrivateHandler.removeMessages(3);
                            this.mPrivateHandler.removeMessages(4);
                            if (!this.mConfirmMove)
                            {
                                if (this.mSelectingText)
                                {
                                    if (!this.mSelectionStarted)
                                        selectionDone();
                                }
                                else if ((this.mTouchMode == 1) && ((canZoomIn()) || (canZoomOut())))
                                    this.mPrivateHandler.sendEmptyMessageDelayed(5, ViewConfiguration.getDoubleTapTimeout());
                            }
                            else
                            {
                                this.mPrivateHandler.removeMessages(8);
                                if (l - this.mLastTouchTime <= 250L)
                                {
                                    if (this.mVelocityTracker == null)
                                        Log.e("webview", "Got null mVelocityTracker");
                                    while (true)
                                    {
                                        this.mHeldMotionless = 3;
                                        doFling();
                                        break;
                                        this.mVelocityTracker.addMovement(paramMotionEvent);
                                    }
                                }
                                if (this.mScroller.springBack(getScrollX(), getScrollY(), 0, computeMaxScrollX(), 0, computeMaxScrollY()))
                                    invalidate();
                                this.mHeldMotionless = 2;
                                invalidate();
                                this.mLastVelocity = 0.0F;
                                WebViewCore.resumePriority();
                                if (!this.mSelectingText)
                                    WebViewCore.resumeUpdatePicture(this.mWebViewCore);
                            }
                        }
                        if (this.mTouchMode == 3)
                        {
                            this.mScroller.springBack(getScrollX(), getScrollY(), 0, computeMaxScrollX(), 0, computeMaxScrollY());
                            invalidate();
                        }
                        cancelTouch();
                    }
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private void hideFloatView()
    {
        if (this.mCopyFloatPanel != null)
            this.mCopyFloatPanel.setVisibility(8);
    }

    private void hidePasteButton()
    {
        if (this.mPasteWindow != null)
            this.mPasteWindow.hide();
    }

    private void hideSoftKeyboard()
    {
        InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
        if ((localInputMethodManager != null) && (localInputMethodManager.isActive(this.mWebView)))
            localInputMethodManager.hideSoftInputFromWindow(this.mWebView.getWindowToken(), 0);
    }

    private boolean inFullScreenMode()
    {
        if (this.mFullScreenHolder != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void init()
    {
        OnTrimMemoryListener.init(this.mContext);
        this.mWebView.setWillNotDraw(false);
        this.mWebView.setClickable(true);
        this.mWebView.setLongClickable(true);
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(this.mContext);
        int i = localViewConfiguration.getScaledTouchSlop();
        this.mTouchSlopSquare = (i * i);
        int j = localViewConfiguration.getScaledDoubleTapSlop();
        this.mDoubleTapSlopSquare = (j * j);
        float f = this.mContext.getResources().getDisplayMetrics().density;
        this.mNavSlop = ((int)(16.0F * f));
        this.mZoomManager.init(f);
        this.mMaximumFling = localViewConfiguration.getScaledMaximumFlingVelocity();
        this.DRAG_LAYER_INVERSE_DENSITY_SQUARED = (1.0F / (f * f));
        this.mOverscrollDistance = localViewConfiguration.getScaledOverscrollDistance();
        this.mOverflingDistance = localViewConfiguration.getScaledOverflingDistance();
        setScrollBarStyle(this.mWebViewPrivate.super_getScrollBarStyle());
        this.mKeysPressed = new Vector(2);
        this.mHTML5VideoViewProxy = null;
    }

    private void invalidateContentRect(Rect paramRect)
    {
        viewInvalidate(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    private boolean isAccessibilityEnabled()
    {
        return AccessibilityManager.getInstance(this.mContext).isEnabled();
    }

    private boolean isEnterActionKey(int paramInt)
    {
        if ((paramInt == 23) || (paramInt == 66) || (paramInt == 160));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isScrollableForAccessibility()
    {
        if ((contentToViewX(getContentWidth()) > getWidth() - this.mWebView.getPaddingLeft() - this.mWebView.getPaddingRight()) || (contentToViewY(getContentHeight()) > getHeight() - this.mWebView.getPaddingTop() - this.mWebView.getPaddingBottom()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private int keyCodeToSoundsEffect(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 0;
        case 19:
        case 22:
        case 20:
        case 21:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 3;
            continue;
            i = 4;
            continue;
            i = 1;
        }
    }

    private void letPageHandleNavKey(int paramInt1, long paramLong, boolean paramBoolean, int paramInt2)
    {
        if (paramBoolean);
        for (int i = 0; ; i = 1)
        {
            sendKeyEvent(new KeyEvent(paramLong, paramLong, i, paramInt1, 1, paramInt2 & 0x1 | paramInt2 & 0x2 | paramInt2 & 0x4, -1, 0, 0));
            return;
        }
    }

    private void loadDataImpl(String paramString1, String paramString2, String paramString3)
    {
        StringBuilder localStringBuilder = new StringBuilder("data:");
        localStringBuilder.append(paramString2);
        if ("base64".equals(paramString3))
            localStringBuilder.append(";base64");
        localStringBuilder.append(",");
        localStringBuilder.append(paramString1);
        loadUrlImpl(localStringBuilder.toString());
    }

    private void loadUrlImpl(String paramString)
    {
        if (paramString == null);
        while (true)
        {
            return;
            loadUrlImpl(paramString, null);
        }
    }

    private void loadUrlImpl(String paramString, Map<String, String> paramMap)
    {
        switchOutDrawHistory();
        WebViewCore.GetUrlData localGetUrlData = new WebViewCore.GetUrlData();
        localGetUrlData.mUrl = paramString;
        localGetUrlData.mExtraHeaders = paramMap;
        this.mWebViewCore.sendMessage(100, localGetUrlData);
        clearHelpers();
    }

    private native void nativeCopyBaseContentToPicture(Picture paramPicture);

    private native void nativeCreate(int paramInt, String paramString, boolean paramBoolean);

    private native int nativeCreateDrawGLFunction(int paramInt1, Rect paramRect1, Rect paramRect2, RectF paramRectF, float paramFloat, int paramInt2);

    private native void nativeDebugDump();

    private static native void nativeDestroy(int paramInt);

    private native void nativeDiscardAllTextures();

    private native void nativeDraw(Canvas paramCanvas, RectF paramRectF, int paramInt1, int paramInt2);

    private native void nativeDumpDisplayTree(String paramString);

    private native boolean nativeEvaluateLayersAnimations(int paramInt);

    private static native void nativeFindMaxVisibleRect(int paramInt1, int paramInt2, Rect paramRect);

    private native int nativeGetBackgroundColor(int paramInt);

    private native int nativeGetBaseLayer(int paramInt);

    private native int nativeGetDrawGLFunction(int paramInt);

    private static native int nativeGetHandleLayerId(int paramInt1, int paramInt2, Point paramPoint, QuadF paramQuadF);

    private native String nativeGetSelection();

    private native boolean nativeHasContent();

    private static native void nativeMapLayerRect(int paramInt1, int paramInt2, Rect paramRect);

    private static native void nativeOnTrimMemory(int paramInt);

    private native boolean nativeScrollLayer(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private native int nativeScrollableLayer(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2);

    private native boolean nativeSetBaseLayer(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, int paramInt3);

    private native void nativeSetHeightCanMeasure(boolean paramBoolean);

    private static native int nativeSetHwAccelerated(int paramInt, boolean paramBoolean);

    private native void nativeSetIsScrolling(boolean paramBoolean);

    private static native void nativeSetPauseDrawing(int paramInt, boolean paramBoolean);

    private static native void nativeSetTextSelection(int paramInt1, int paramInt2);

    private native void nativeStopGL(int paramInt);

    private native void nativeTileProfilingClear();

    private native float nativeTileProfilingGetFloat(int paramInt1, int paramInt2, String paramString);

    private native int nativeTileProfilingGetInt(int paramInt1, int paramInt2, String paramString);

    private native int nativeTileProfilingNumFrames();

    private native int nativeTileProfilingNumTilesInFrame(int paramInt);

    private native void nativeTileProfilingStart();

    private native float nativeTileProfilingStop();

    private native void nativeUpdateDrawGLFunction(int paramInt, Rect paramRect1, Rect paramRect2, RectF paramRectF, float paramFloat);

    private native void nativeUseHardwareAccelSkia(boolean paramBoolean);

    private void onHandleUiEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
    {
        switch (paramInt1)
        {
        case 1:
        case 2:
        default:
        case 3:
        case 5:
        case 0:
        case 4:
        }
        while (true)
        {
            return;
            if (getHitTestResult() != null)
            {
                this.mWebView.performLongClick();
                continue;
                this.mZoomManager.handleDoubleTap(paramMotionEvent.getX(), paramMotionEvent.getY());
                continue;
                onHandleUiTouchEvent(paramMotionEvent);
                continue;
                if ((this.mFocusedNode != null) && (this.mFocusedNode.mIntentUrl != null))
                {
                    this.mWebView.playSoundEffect(0);
                    overrideLoading(this.mFocusedNode.mIntentUrl);
                }
            }
        }
    }

    private void onHandleUiTouchEvent(MotionEvent paramMotionEvent)
    {
        ScaleGestureDetector localScaleGestureDetector = this.mZoomManager.getScaleGestureDetector();
        int i = paramMotionEvent.getActionMasked();
        int j;
        int k;
        label36: int m;
        label47: float f1;
        float f2;
        int n;
        int i1;
        if (i == 6)
        {
            j = 1;
            if ((i != 6) && (i != 5))
                break label88;
            k = 1;
            if (j == 0)
                break label94;
            m = paramMotionEvent.getActionIndex();
            f1 = 0.0F;
            f2 = 0.0F;
            n = paramMotionEvent.getPointerCount();
            i1 = 0;
            label62: if (i1 >= n)
                break label126;
            if (m != i1)
                break label101;
        }
        while (true)
        {
            i1++;
            break label62;
            j = 0;
            break;
            label88: k = 0;
            break label36;
            label94: m = -1;
            break label47;
            label101: f1 += paramMotionEvent.getX(i1);
            f2 += paramMotionEvent.getY(i1);
        }
        label126: int i2;
        float f3;
        float f4;
        if (j != 0)
        {
            i2 = n - 1;
            f3 = f1 / i2;
            f4 = f2 / i2;
            if (k != 0)
            {
                this.mLastTouchX = Math.round(f3);
                this.mLastTouchY = Math.round(f4);
                this.mLastTouchTime = paramMotionEvent.getEventTime();
                this.mWebView.cancelLongPress();
                this.mPrivateHandler.removeMessages(4);
            }
            if (localScaleGestureDetector == null)
                break label261;
            localScaleGestureDetector.onTouchEvent(paramMotionEvent);
            if (!localScaleGestureDetector.isInProgress())
                break label261;
            this.mLastTouchTime = paramMotionEvent.getEventTime();
            if (this.mZoomManager.supportsPanDuringZoom())
                break label242;
        }
        while (true)
        {
            return;
            i2 = n;
            break;
            label242: this.mTouchMode = 3;
            if (this.mVelocityTracker == null)
                this.mVelocityTracker = VelocityTracker.obtain();
            label261: if (i == 5)
            {
                cancelTouch();
                i = 0;
            }
            do
            {
                do
                {
                    handleTouchEventCommon(paramMotionEvent, i, Math.round(f3), Math.round(f4));
                    break;
                }
                while (i != 2);
                if (f3 < 0.0F)
                    break;
            }
            while (f4 >= 0.0F);
        }
    }

    private void onZoomAnimationEnd()
    {
        this.mPrivateHandler.sendEmptyMessage(146);
    }

    private void onZoomAnimationStart()
    {
        if ((!this.mSelectingText) && (this.mHandleAlpha.getAlpha() > 0))
            this.mHandleAlphaAnimator.end();
    }

    private void overrideLoading(String paramString)
    {
        this.mCallbackProxy.uiOverrideUrlLoading(paramString);
    }

    static int pinLoc(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt3 < paramInt2)
            paramInt1 = 0;
        while (true)
        {
            return paramInt1;
            if (paramInt1 < 0)
                paramInt1 = 0;
            else if (paramInt1 + paramInt2 > paramInt3)
                paramInt1 = paramInt3 - paramInt2;
        }
    }

    private boolean pinScrollBy(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
        return pinScrollTo(paramInt1 + getScrollX(), paramInt2 + getScrollY(), paramBoolean, paramInt3);
    }

    private boolean pinScrollTo(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
        abortAnimation();
        int i = pinLocX(paramInt1);
        int j = pinLocY(paramInt2);
        int k = i - getScrollX();
        int m = j - getScrollY();
        boolean bool;
        if ((k | m) == 0)
        {
            bool = false;
            return bool;
        }
        int i2;
        if (paramBoolean)
        {
            OverScroller localOverScroller = this.mScroller;
            int n = getScrollX();
            int i1 = getScrollY();
            if (paramInt3 > 0)
            {
                i2 = paramInt3;
                label81: localOverScroller.startScroll(n, i1, k, m, i2);
                invalidate();
            }
        }
        while (true)
        {
            bool = true;
            break;
            i2 = computeDuration(k, m);
            break label81;
            this.mWebView.scrollTo(i, j);
        }
    }

    private void postInvalidate()
    {
        this.mWebView.postInvalidate();
    }

    private void recordNewContentSize(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if ((paramInt1 | paramInt2) == 0)
            invalidate();
        while (true)
        {
            return;
            if ((this.mContentWidth != paramInt1) || (this.mContentHeight != paramInt2))
            {
                this.mContentWidth = paramInt1;
                this.mContentHeight = paramInt2;
                if (!this.mDrawHistory)
                {
                    updateScrollCoordinates(pinLocX(getScrollX()), pinLocY(getScrollY()));
                    if (!this.mScroller.isFinished())
                    {
                        this.mScroller.setFinalX(pinLocX(this.mScroller.getFinalX()));
                        this.mScroller.setFinalY(pinLocY(this.mScroller.getFinalY()));
                    }
                }
                invalidate();
            }
            contentSizeChanged(paramBoolean);
        }
    }

    private void relocateAutoCompletePopup()
    {
        if (this.mAutoCompletePopup != null)
        {
            this.mAutoCompletePopup.resetRect();
            this.mAutoCompletePopup.setText(this.mInputConnection.getEditable());
        }
    }

    private void removeTouchHighlight()
    {
        setTouchHighlightRects(null);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void resetCaretTimer()
    {
        this.mPrivateHandler.removeMessages(144);
        if (!this.mSelectionStarted)
            this.mPrivateHandler.sendEmptyMessageDelayed(144, 3000L);
    }

    private void restoreHistoryPictureFields(Picture paramPicture, Bundle paramBundle)
    {
        int i = paramBundle.getInt("scrollX", 0);
        int j = paramBundle.getInt("scrollY", 0);
        this.mDrawHistory = true;
        this.mHistoryPicture = paramPicture;
        setScrollXRaw(i);
        setScrollYRaw(j);
        this.mZoomManager.restoreZoomState(paramBundle);
        float f = this.mZoomManager.getScale();
        this.mHistoryWidth = Math.round(f * paramPicture.getWidth());
        this.mHistoryHeight = Math.round(f * paramPicture.getHeight());
        invalidate();
    }

    private void saveWebArchiveImpl(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback)
    {
        this.mWebViewCore.sendMessage(147, new SaveWebArchiveMessage(paramString, paramBoolean, paramValueCallback));
    }

    private static float scaleAlongSegment(int paramInt1, int paramInt2, PointF paramPointF1, PointF paramPointF2)
    {
        float f1 = paramPointF2.x - paramPointF1.x;
        float f2 = paramPointF2.y - paramPointF1.y;
        float f3 = f1 * f1 + f2 * f2;
        float f4 = paramInt1 - paramPointF1.x;
        float f5 = paramInt2 - paramPointF1.y;
        return (f4 * f1 + f5 * f2) / f3;
    }

    private static float scaleCoordinate(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return paramFloat2 + paramFloat1 * (paramFloat3 - paramFloat2);
    }

    private int scaleTrackballX(float paramFloat, int paramInt)
    {
        int i = (int)(paramFloat / 400.0F * paramInt);
        int j = i;
        if (i > 0)
            if (i > this.mTrackballXMove)
                i -= this.mTrackballXMove;
        while (true)
        {
            this.mTrackballXMove = j;
            return i;
            if (i < this.mTrackballXMove)
                i -= this.mTrackballXMove;
        }
    }

    private int scaleTrackballY(float paramFloat, int paramInt)
    {
        int i = (int)(paramFloat / 400.0F * paramInt);
        int j = i;
        if (i > 0)
            if (i > this.mTrackballYMove)
                i -= this.mTrackballYMove;
        while (true)
        {
            this.mTrackballYMove = j;
            return i;
            if (i < this.mTrackballYMove)
                i -= this.mTrackballYMove;
        }
    }

    private void scrollEditIntoView()
    {
        Rect localRect1 = new Rect(viewToContentX(getScrollX()), viewToContentY(getScrollY()), viewToContentX(getScrollX() + getWidth()), viewToContentY(getScrollY() + getViewHeightWithTitle()));
        if (localRect1.contains(this.mEditTextContentBounds))
            return;
        syncSelectionCursors();
        nativeFindMaxVisibleRect(this.mNativeClass, this.mEditTextLayerId, localRect1);
        int i = Math.max(1, viewToContentDimension(10));
        Rect localRect2 = new Rect(Math.max(0, this.mEditTextContentBounds.left - i), Math.max(0, this.mEditTextContentBounds.top - i), i + this.mEditTextContentBounds.right, i + this.mEditTextContentBounds.bottom);
        Point localPoint = calculateCaretTop();
        label204: int j;
        label261: label304: int k;
        if (localRect1.width() < this.mEditTextContentBounds.width())
        {
            if (this.mSelectCursorLeft.x < localPoint.x)
            {
                localRect2.left = Math.max(0, this.mSelectCursorLeft.x - i);
                localRect2.right = (i + localPoint.x);
            }
        }
        else
        {
            if (localRect1.height() < this.mEditTextContentBounds.height())
            {
                if (this.mSelectCursorLeft.y <= localPoint.y)
                    break label382;
                localRect2.top = Math.max(0, localPoint.y - i);
                localRect2.bottom = (i + this.mSelectCursorLeft.y);
            }
            if (localRect1.contains(localRect2))
                break label411;
            j = viewToContentX(getScrollX());
            if (localRect1.left <= localRect2.left)
                break label413;
            j += localRect2.left - localRect1.left;
            k = viewToContentY(getScrollY());
            if (localRect1.top <= localRect2.top)
                break label441;
            k += localRect2.top - localRect1.top;
        }
        while (true)
        {
            contentScrollTo(j, k, false);
            break;
            localRect2.left = Math.max(0, localPoint.x - i);
            localRect2.right = (i + this.mSelectCursorLeft.x);
            break label204;
            label382: localRect2.top = Math.max(0, this.mSelectCursorLeft.y - i);
            localRect2.bottom = (i + localPoint.y);
            break label261;
            label411: break;
            label413: if (localRect1.right >= localRect2.right)
                break label304;
            j += localRect2.right - localRect1.right;
            break label304;
            label441: if (localRect1.bottom < localRect2.bottom)
                k += localRect2.bottom - localRect1.bottom;
        }
    }

    private void scrollEditText(int paramInt1, int paramInt2)
    {
        float f1 = getMaxTextScrollX();
        float f2 = paramInt1 / f1;
        this.mEditTextContent.offsetTo(-paramInt1, -paramInt2);
        this.mWebViewCore.sendMessageAtFrontOfQueue(99, 0, paramInt2, Float.valueOf(f2));
    }

    private void scrollEditWithCursor()
    {
        int i;
        float f1;
        int j;
        float f2;
        if (this.mLastEditScroll != 0L)
        {
            i = viewToContentX(this.mLastTouchX + getScrollX() + this.mSelectDraggingOffset.x);
            f1 = getTextScrollSpeed(i, this.mEditTextContentBounds.left, this.mEditTextContentBounds.right);
            j = viewToContentY(this.mLastTouchY + getScrollY() + this.mSelectDraggingOffset.y);
            f2 = getTextScrollSpeed(j, this.mEditTextContentBounds.top, this.mEditTextContentBounds.bottom);
            if ((f1 != 0.0F) || (f2 != 0.0F))
                break label110;
            endScrollEdit();
        }
        while (true)
        {
            return;
            label110: long l1 = SystemClock.uptimeMillis();
            long l2 = l1 - this.mLastEditScroll;
            int k = getTextScrollDelta(f1, l2);
            int m = getTextScrollDelta(f2, l2);
            this.mLastEditScroll = l1;
            if ((k == 0) && (m == 0))
            {
                this.mPrivateHandler.sendEmptyMessageDelayed(149, 16L);
            }
            else
            {
                int n = k + getTextScrollX();
                int i1 = Math.max(0, Math.min(getMaxTextScrollX(), n));
                int i2 = m + getTextScrollY();
                scrollEditText(i1, Math.max(0, Math.min(getMaxTextScrollY(), i2)));
                int i3 = this.mSelectDraggingCursor.x;
                int i4 = this.mSelectDraggingCursor.y;
                this.mSelectDraggingCursor.set(i - k, j - m);
                updateWebkitSelection();
                this.mSelectDraggingCursor.set(i3, i4);
            }
        }
    }

    private void scrollLayerTo(int paramInt1, int paramInt2)
    {
        int i = this.mScrollingLayerRect.left - paramInt1;
        int j = this.mScrollingLayerRect.top - paramInt2;
        if (((i == 0) && (j == 0)) || (this.mNativeClass == 0))
            return;
        if (this.mSelectingText)
        {
            if (this.mSelectCursorLeftLayerId == this.mCurrentScrollingLayerId)
            {
                this.mSelectCursorLeft.offset(i, j);
                this.mSelectCursorLeftTextQuad.offset(i, j);
            }
            if (this.mSelectCursorRightLayerId == this.mCurrentScrollingLayerId)
            {
                this.mSelectCursorRight.offset(i, j);
                this.mSelectCursorRightTextQuad.offset(i, j);
            }
        }
        while (true)
        {
            if ((this.mAutoCompletePopup != null) && (this.mCurrentScrollingLayerId == this.mEditTextLayerId))
            {
                this.mEditTextContentBounds.offset(i, j);
                this.mAutoCompletePopup.resetRect();
            }
            nativeScrollLayer(this.mNativeClass, this.mCurrentScrollingLayerId, paramInt1, paramInt2);
            this.mScrollingLayerRect.left = paramInt1;
            this.mScrollingLayerRect.top = paramInt2;
            this.mWebViewCore.sendMessage(198, this.mCurrentScrollingLayerId, this.mScrollingLayerRect);
            this.mWebViewPrivate.onScrollChanged(getScrollX(), getScrollY(), getScrollX(), getScrollY());
            invalidate();
            break;
            if (this.mHandleAlpha.getAlpha() > 0)
                this.mHandleAlphaAnimator.end();
        }
    }

    private void sendKeyEvent(KeyEvent paramKeyEvent)
    {
        int i = 0;
        switch (paramKeyEvent.getKeyCode())
        {
        default:
        case 20:
        case 19:
        case 21:
        case 22:
            while (true)
            {
                if ((i != 0) && (this.mWebView.focusSearch(i) == null))
                    i = 0;
                int j = 104;
                if (paramKeyEvent.getAction() == 0)
                {
                    j = 103;
                    int k = keyCodeToSoundsEffect(paramKeyEvent.getKeyCode());
                    if (k != 0)
                        this.mWebView.playSoundEffect(k);
                }
                sendBatchableInputMessage(j, i, 0, paramKeyEvent);
                return;
                i = 130;
                continue;
                i = 33;
                continue;
                i = 17;
                continue;
                i = 66;
            }
        case 61:
        }
        if (paramKeyEvent.isShiftPressed());
        for (i = 1; ; i = 2)
            break;
    }

    private boolean setContentScrollBy(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        boolean bool = false;
        if (this.mDrawHistory);
        while (true)
        {
            return bool;
            int i = contentToViewDimension(paramInt1);
            int j = contentToViewDimension(paramInt2);
            if (this.mHeightCanMeasure)
            {
                if (j != 0)
                {
                    Rect localRect = new Rect();
                    calcOurVisibleRect(localRect);
                    localRect.offset(i, j);
                    this.mWebView.requestRectangleOnScreen(localRect);
                }
                if ((j == 0) && (i != 0) && (pinScrollBy(i, 0, paramBoolean, 0)))
                    bool = true;
            }
            else
            {
                bool = pinScrollBy(i, j, paramBoolean, 0);
            }
        }
    }

    private void setFindIsUp(boolean paramBoolean)
    {
        this.mFindIsUp = paramBoolean;
    }

    private void setHitTestResult(WebViewCore.WebKitHitTest paramWebKitHitTest)
    {
        if (paramWebKitHitTest == null)
            this.mInitialHitTestResult = null;
        while (true)
        {
            return;
            this.mInitialHitTestResult = new WebView.HitTestResult();
            if (paramWebKitHitTest.mLinkUrl != null)
            {
                setHitTestTypeFromUrl(paramWebKitHitTest.mLinkUrl);
                if ((paramWebKitHitTest.mImageUrl != null) && (this.mInitialHitTestResult.getType() == 7))
                {
                    this.mInitialHitTestResult.setType(8);
                    this.mInitialHitTestResult.setExtra(paramWebKitHitTest.mImageUrl);
                }
            }
            else if (paramWebKitHitTest.mImageUrl != null)
            {
                this.mInitialHitTestResult.setType(5);
                this.mInitialHitTestResult.setExtra(paramWebKitHitTest.mImageUrl);
            }
            else if (paramWebKitHitTest.mEditable)
            {
                this.mInitialHitTestResult.setType(9);
            }
            else if (paramWebKitHitTest.mIntentUrl != null)
            {
                setHitTestTypeFromUrl(paramWebKitHitTest.mIntentUrl);
            }
        }
    }

    private void setHitTestTypeFromUrl(String paramString)
    {
        String str;
        if (paramString.startsWith("geo:0,0?q="))
        {
            this.mInitialHitTestResult.setType(3);
            str = paramString.substring("geo:0,0?q=".length());
        }
        try
        {
            this.mInitialHitTestResult.setExtra(URLDecoder.decode(str, "UTF-8"));
            while (true)
            {
                return;
                if (paramString.startsWith("tel:"))
                {
                    this.mInitialHitTestResult.setType(2);
                    str = paramString.substring("tel:".length());
                    break;
                }
                if (paramString.startsWith("mailto:"))
                {
                    this.mInitialHitTestResult.setType(4);
                    str = paramString.substring("mailto:".length());
                    break;
                }
                this.mInitialHitTestResult.setType(7);
                this.mInitialHitTestResult.setExtra(paramString);
            }
        }
        catch (Throwable localThrowable)
        {
            while (true)
            {
                Log.w("webview", "Failed to decode URL! " + str, localThrowable);
                this.mInitialHitTestResult.setType(0);
            }
        }
    }

    public static void setShouldMonitorWebCoreThread()
    {
        WebViewCore.setShouldMonitorWebCoreThread();
    }

    private void setTouchHighlightRects(WebViewCore.WebKitHitTest paramWebKitHitTest)
    {
        FocusTransitionDrawable localFocusTransitionDrawable1 = null;
        if (shouldAnimateTo(paramWebKitHitTest))
            localFocusTransitionDrawable1 = new FocusTransitionDrawable(this);
        if (paramWebKitHitTest != null);
        for (Rect[] arrayOfRect1 = paramWebKitHitTest.mTouchRects; ; arrayOfRect1 = null)
        {
            if (!this.mTouchHighlightRegion.isEmpty())
            {
                this.mWebView.invalidate(this.mTouchHighlightRegion.getBounds());
                if (localFocusTransitionDrawable1 != null)
                    localFocusTransitionDrawable1.mPreviousRegion = new Region(this.mTouchHighlightRegion);
                this.mTouchHighlightRegion.setEmpty();
            }
            if (arrayOfRect1 == null)
                return;
            this.mTouchHightlightPaint.setColor(paramWebKitHitTest.mTapHighlightColor);
            Rect[] arrayOfRect2 = arrayOfRect1;
            int i = arrayOfRect2.length;
            for (int j = 0; j < i; j++)
            {
                Rect localRect = contentToViewRect(arrayOfRect2[j]);
                if ((localRect.width() < getWidth() >> 1) || (localRect.height() < getHeight() >> 1))
                    this.mTouchHighlightRegion.union(localRect);
            }
        }
        this.mWebView.invalidate(this.mTouchHighlightRegion.getBounds());
        if ((localFocusTransitionDrawable1 != null) && (localFocusTransitionDrawable1.mPreviousRegion != null))
        {
            localFocusTransitionDrawable1.mNewRegion = new Region(this.mTouchHighlightRegion);
            this.mFocusTransition = localFocusTransitionDrawable1;
            FocusTransitionDrawable localFocusTransitionDrawable2 = this.mFocusTransition;
            float[] arrayOfFloat = new float[1];
            arrayOfFloat[0] = 1.0F;
            ObjectAnimator.ofFloat(localFocusTransitionDrawable2, "progress", arrayOfFloat).start();
        }
    }

    private void setupPackageListener(Context paramContext)
    {
        try
        {
            if (sPackageInstallationReceiverAdded)
                return;
            IntentFilter localIntentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
            localIntentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            localIntentFilter.addDataScheme("package");
            PackageListener localPackageListener = new PackageListener(null);
            paramContext.getApplicationContext().registerReceiver(localPackageListener, localIntentFilter);
            sPackageInstallationReceiverAdded = true;
            new AsyncTask()
            {
                protected Set<String> doInBackground(Void[] paramAnonymousArrayOfVoid)
                {
                    HashSet localHashSet = new HashSet();
                    PackageManager localPackageManager = WebViewClassic.this.mContext.getPackageManager();
                    Iterator localIterator = WebViewClassic.sGoogleApps.iterator();
                    while (localIterator.hasNext())
                    {
                        String str = (String)localIterator.next();
                        try
                        {
                            localPackageManager.getPackageInfo(str, 5);
                            localHashSet.add(str);
                        }
                        catch (PackageManager.NameNotFoundException localNameNotFoundException)
                        {
                        }
                    }
                    return localHashSet;
                }

                protected void onPostExecute(Set<String> paramAnonymousSet)
                {
                    if (WebViewClassic.this.mWebViewCore != null)
                        WebViewClassic.this.mWebViewCore.sendMessage(184, paramAnonymousSet);
                }
            }
            .execute(new Void[0]);
        }
        finally
        {
        }
    }

    /** @deprecated */
    private static void setupProxyListener(Context paramContext)
    {
        try
        {
            if (sProxyReceiver == null)
            {
                boolean bool = sNotificationsEnabled;
                if (bool)
                    break label21;
            }
            while (true)
            {
                return;
                label21: IntentFilter localIntentFilter = new IntentFilter();
                localIntentFilter.addAction("android.intent.action.PROXY_CHANGE");
                sProxyReceiver = new ProxyReceiver(null);
                Intent localIntent = paramContext.getApplicationContext().registerReceiver(sProxyReceiver, localIntentFilter);
                if (localIntent != null)
                    handleProxyBroadcast(localIntent);
            }
        }
        finally
        {
        }
    }

    private static void setupTrustStorageListener(Context paramContext)
    {
        if (sTrustStorageListener != null);
        while (true)
        {
            return;
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.security.STORAGE_CHANGED");
            sTrustStorageListener = new TrustStorageListener(null);
            if (paramContext.getApplicationContext().registerReceiver(sTrustStorageListener, localIntentFilter) != null)
                handleCertTrustChanged();
        }
    }

    private boolean setupWebkitSelect()
    {
        syncSelectionCursors();
        if ((!this.mIsCaretSelection) && (!startSelectActionMode()))
            selectionDone();
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            startSelectingText();
            this.mTouchMode = 3;
        }
    }

    private boolean shouldAnimateTo(WebViewCore.WebKitHitTest paramWebKitHitTest)
    {
        return false;
    }

    private boolean shouldDrawHighlightRect()
    {
        boolean bool = false;
        if ((this.mFocusedNode == null) || (this.mInitialHitTestResult == null));
        while (true)
        {
            return bool;
            if (!this.mTouchHighlightRegion.isEmpty())
                if ((this.mFocusedNode.mHasFocus) && (!this.mWebView.isInTouchMode()))
                {
                    if ((this.mDrawCursorRing) && (!this.mFocusedNode.mEditable))
                        bool = true;
                }
                else if ((!this.mFocusedNode.mHasFocus) || (!this.mFocusedNode.mEditable))
                    bool = this.mShowTapHighlight;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private void showFloatView()
    {
        if (this.mCopyFloatPanel == null)
            this.mCopyFloatPanel = SelectionFloatPanel.getInstance(this.mContext, this);
        if (!this.mSelectingText)
            this.mCopyFloatPanel.setVisibility(4);
        int i;
        int j;
        int k;
        int m;
        int n;
        int i1;
        Rect localRect2;
        Point localPoint;
        Rect localRect3;
        do
        {
            return;
            View localView = this.mCopyFloatPanel.findViewById(101384219);
            localView.measure(0, 0);
            i = localView.getMeasuredWidth();
            j = localView.getMeasuredHeight();
            int[] arrayOfInt = new int[4];
            getSelectionHandles(arrayOfInt);
            k = contentToViewDimension(arrayOfInt[0]);
            m = contentToViewDimension(arrayOfInt[1]);
            n = contentToViewDimension(arrayOfInt[2]);
            i1 = contentToViewDimension(arrayOfInt[3]);
            if (m >= i1)
            {
                int i9 = i1;
                i1 = m;
                m = i9;
            }
            this.mMenuLeft = ((k + n) / 2 - i / 2);
            Rect localRect1 = new Rect();
            localRect2 = new Rect();
            localPoint = new Point();
            getWebView().getGlobalVisibleRect(localRect1, localPoint);
            getWebView().getLocalVisibleRect(localRect2);
            if (this.mMenuLeft < localRect2.left)
                this.mMenuLeft = localRect2.left;
            if (i + this.mMenuLeft > localRect2.left + localRect2.width())
                this.mMenuLeft = (localRect2.left + localRect2.width() - i);
            localRect3 = new Rect(this.mMenuLeft, this.mMenuTop, i + this.mMenuLeft, j + this.mMenuTop);
        }
        while ((this.mSelectHandleLeft == null) || (this.mSelectHandleRight == null));
        Rect localRect4 = this.mSelectHandleLeft.getBounds();
        Rect localRect5 = this.mSelectHandleRight.getBounds();
        int i2 = (int)(80.0F * this.mContext.getResources().getDisplayMetrics().density);
        this.mMenuTop = (i1 - i2);
        localRect3.set(this.mMenuLeft, this.mMenuTop, i + this.mMenuLeft, j + this.mMenuTop);
        Rect localRect6 = new Rect(localRect3);
        if (localRect6.intersect(localRect4))
            this.mMenuTop = (m - i2);
        int i3 = k;
        int i4 = m;
        if (i1 < n)
        {
            i3 = k;
            i4 = m;
        }
        Rect localRect7 = new Rect(i3, m, i4, i1);
        if ((this.mMenuTop < localRect2.top) || (this.mMenuTop > localRect2.bottom) || (this.mMenuLeft < localRect2.left) || (this.mMenuLeft > localRect2.right))
            if (localRect7.intersect(localRect2))
            {
                if ((k < localRect2.left) || (k >= localRect2.right))
                    break label724;
                i5 = localRect2.top;
                if (m < i5)
                    break label724;
                i6 = localRect2.bottom;
                if (m >= i6)
                    break label724;
                i7 = localRect2.top + localRect2.height() / 2;
                if (m >= i7)
                    break label691;
                this.mMenuTop = (localRect4.bottom + i2 / 4);
                localRect6.set(this.mMenuLeft, this.mMenuTop, i + this.mMenuLeft, j + this.mMenuTop);
                if (localRect6.intersect(localRect5))
                {
                    this.mMenuTop = (localRect5.bottom + i2 / 4);
                    if (this.mMenuTop > localRect2.bottom)
                        this.mMenuTop = (localRect5.top + i2 / 4);
                }
            }
        label691: label724: 
        while (this.mMenuTop >= localRect2.top)
            while (true)
            {
                int i5;
                int i6;
                int i7;
                this.mCopyFloatPanel.showAt(this.mMenuLeft + localPoint.x, this.mMenuTop + getTitleHeight() + localPoint.y);
                break;
                this.mMenuTop = (m - i2);
                if (this.mMenuTop < localRect2.top)
                {
                    this.mMenuTop = localRect4.top;
                    continue;
                    if ((n >= localRect2.left) && (n < localRect2.right) && (i1 >= localRect2.top) && (i1 < localRect2.bottom))
                    {
                        if (i1 < localRect2.top + localRect2.height() / 2)
                            this.mMenuTop = (localRect5.bottom + i2 / 4);
                        else
                            this.mMenuTop = (i1 - i2);
                    }
                    else
                    {
                        this.mMenuLeft = (localRect2.left + localRect2.width() / 2 - i / 2);
                        this.mMenuTop = (localRect2.top + localRect2.height() / 2 - i2);
                    }
                }
            }
        if (localRect4.top > localRect2.top);
        for (int i8 = localRect4.top; ; i8 = localRect2.top + localRect4.height() + i2 / 4)
        {
            this.mMenuTop = i8;
            localRect6.set(this.mMenuLeft, this.mMenuTop, i + this.mMenuLeft, j + this.mMenuTop);
            if (!localRect6.intersect(localRect5))
                break;
            this.mMenuTop = (localRect5.bottom + i2 / 4);
            break;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private void showMagnifier(Canvas paramCanvas, Rect paramRect1, Rect paramRect2)
    {
        getSelectionHandles(new int[5]);
        int i;
        Rect localRect1;
        int j;
        int k;
        int m;
        label215: int n;
        int i1;
        int i3;
        Rect localRect3;
        label484: Paint localPaint2;
        String str;
        int i13;
        label604: int i9;
        int i10;
        if (this.mSelectDraggingCursor == this.mSelectCursorLeft)
        {
            i = 1;
            if ((!this.mIsActionUp) && (this.mSelectingText) && ((this.mBeforeStart != null) || (this.mAfterStart != null)))
            {
                localRect1 = new Rect();
                if (this.mSelectMagnifier == null)
                    this.mSelectMagnifier = this.mContext.getResources().getDrawable(100794406);
                Rect localRect2 = new Rect();
                this.mSelectMagnifier.getPadding(localRect2);
                float f1 = this.mContext.getResources().getDisplayMetrics().density / 1.5F;
                j = (int)(f1 * this.mSelectMagnifier.getIntrinsicHeight());
                k = (int)(f1 * (this.mSelectMagnifier.getIntrinsicWidth() / 2));
                m = (int)(20.0F * f1);
                if (i == 0)
                    break label815;
                paramRect1.left -= k;
                localRect1.top = (paramRect1.top - j - m);
                localRect1.right = (k + paramRect1.left);
                localRect1.bottom = (paramRect1.top - m);
                this.mMenuLeft = ((paramRect1.left + paramRect2.left) / 2);
                this.mMenuTop = paramRect1.top;
                this.mSelectMagnifier.setBounds(localRect1.left, localRect1.top, localRect1.left + k * 2, j + localRect1.top);
                this.mSelectMagnifier.draw(paramCanvas);
                if (this.mSelectHighlight == null)
                    this.mSelectHighlight = this.mContext.getResources().getDrawable(100794405);
                n = (int)(f1 * localRect2.left);
                i1 = (int)(f1 * localRect2.top);
                int i2 = (int)(f1 * localRect2.right);
                i3 = (int)(f1 * localRect2.bottom);
                this.mSelectHighlight.setBounds(n + localRect1.left, i1 + localRect1.top, k + localRect1.left - i2, j + localRect1.top - i3);
                this.mSelectHighlight.draw(paramCanvas);
                Paint localPaint1 = new Paint();
                localPaint1.setColor(1714664933);
                if (i == 0)
                    break label869;
                int i14 = k + localRect1.left;
                int i15 = i1 + localRect1.top;
                int i16 = localRect1.right - i2;
                int i17 = localRect1.bottom - i3;
                localRect3 = new Rect(i14, i15, i16, i17);
                paramCanvas.drawRect(localRect3, localPaint1);
                paramCanvas.clipRect(n + localRect1.left, i1 + localRect1.top, localRect1.right - i2, localRect1.bottom - i3);
                localPaint2 = new Paint();
                localPaint2.setStyle(Paint.Style.STROKE);
                localPaint2.setStrokeWidth(0.0F);
                localPaint2.setColor(-16777216);
                localPaint2.setTextSize(f1 * (3.0F * localPaint1.getTextSize()));
                int i8 = 0;
                str = " ";
                if (this.mBeforeStart != null)
                {
                    int i12 = this.mBeforeStart.length();
                    i13 = 0;
                    if (i13 < i12)
                    {
                        str = this.mBeforeStart.substring(-1 + (i12 - i13));
                        i8 = (int)localPaint2.measureText(str);
                        if (i8 <= k * 2)
                            break label929;
                    }
                    float f4 = -2 + (k + localRect1.left - i8);
                    float f5 = localRect1.bottom - i3 - m / 2 + localPaint2.descent() / 2.0F;
                    paramCanvas.drawText(str, f4, f5, localPaint2);
                }
                if (this.mAfterStart != null)
                {
                    i9 = this.mAfterStart.length();
                    i10 = k * 2;
                }
            }
        }
        for (int i11 = 0; ; i11++)
            if (i11 < i9)
            {
                str = this.mAfterStart.substring(0, i11 + 1);
                if ((int)localPaint2.measureText(str) <= i10);
            }
            else
            {
                float f2 = 2 + (k + localRect1.left);
                float f3 = localRect1.bottom - i3 - m / 2 + localPaint2.descent() / 2.0F;
                paramCanvas.drawText(str, f2, f3, localPaint2);
                return;
                i = 0;
                break;
                label815: localRect1.left = (paramRect2.right - k);
                localRect1.top = (paramRect2.top - j - m);
                localRect1.right = (k + paramRect2.right);
                localRect1.bottom = (paramRect2.top - m);
                break label215;
                label869: int i4 = n + localRect1.left;
                int i5 = i1 + localRect1.top;
                int i6 = k + localRect1.left;
                int i7 = localRect1.bottom - i3;
                localRect3 = new Rect(i4, i5, i6, i7);
                break label484;
                label929: i13++;
                break label604;
            }
    }

    private void showPasteWindow()
    {
        if (((ClipboardManager)this.mContext.getSystemService("clipboard")).hasPrimaryClip())
        {
            Point localPoint1 = new Point(contentToViewX(this.mSelectCursorLeft.x), contentToViewY(this.mSelectCursorLeft.y));
            Point localPoint2 = calculateCaretTop();
            localPoint2.set(contentToViewX(localPoint2.x), contentToViewY(localPoint2.y));
            int[] arrayOfInt = new int[2];
            this.mWebView.getLocationInWindow(arrayOfInt);
            int i = arrayOfInt[0] - getScrollX();
            int j = arrayOfInt[1] - getScrollY();
            localPoint1.offset(i, j);
            localPoint2.offset(i, j);
            if (this.mPasteWindow == null)
                this.mPasteWindow = new PastePopupWindow();
            this.mPasteWindow.show(localPoint1, localPoint2, arrayOfInt[0], arrayOfInt[1]);
        }
    }

    private void snapDraggingCursor()
    {
        float f1 = Math.min(Math.max(0.0F, scaleAlongSegment(this.mSelectDraggingCursor.x, this.mSelectDraggingCursor.y, this.mSelectDraggingTextQuad.p4, this.mSelectDraggingTextQuad.p3)), 1.0F);
        float f2 = scaleCoordinate(f1, this.mSelectDraggingTextQuad.p4.x, this.mSelectDraggingTextQuad.p3.x);
        float f3 = scaleCoordinate(f1, this.mSelectDraggingTextQuad.p4.y, this.mSelectDraggingTextQuad.p3.y);
        int i = Math.round(f2);
        int j = Math.round(f3);
        if (this.mIsEditingText)
        {
            i = Math.max(this.mEditTextContentBounds.left, Math.min(this.mEditTextContentBounds.right, i));
            j = Math.max(this.mEditTextContentBounds.top, Math.min(this.mEditTextContentBounds.bottom, j));
        }
        this.mSelectDraggingCursor.set(i, j);
    }

    private void startDrag()
    {
        WebViewCore.reducePriority();
        WebViewCore.pauseUpdatePicture(this.mWebViewCore);
        nativeSetIsScrolling(true);
        if ((this.mHorizontalScrollBarMode != 1) || (this.mVerticalScrollBarMode != 1))
            this.mZoomManager.invokeZoomPicker();
    }

    private void startPrivateBrowsing()
    {
        getSettings().setPrivateBrowsingEnabled(true);
    }

    private void startScrollingLayer(float paramFloat1, float paramFloat2)
    {
        if (this.mNativeClass == 0);
        while (true)
        {
            return;
            int i = viewToContentX((int)paramFloat1 + getScrollX());
            int j = viewToContentY((int)paramFloat2 + getScrollY());
            this.mCurrentScrollingLayerId = nativeScrollableLayer(this.mNativeClass, i, j, this.mScrollingLayerRect, this.mScrollingLayerBounds);
            if (this.mCurrentScrollingLayerId != 0)
                this.mTouchMode = 9;
        }
    }

    private boolean startSelectActionMode()
    {
        this.mWebView.performHapticFeedback(0);
        return true;
    }

    private void startSelectingText()
    {
        this.mSelectingText = true;
        this.mShowTextSelectionExtra = true;
        ObjectAnimator localObjectAnimator = this.mHandleAlphaAnimator;
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 255;
        localObjectAnimator.setIntValues(arrayOfInt);
        this.mHandleAlphaAnimator.start();
    }

    private void startTouch(float paramFloat1, float paramFloat2, long paramLong)
    {
        int i = Math.round(paramFloat1);
        this.mLastTouchX = i;
        this.mStartTouchX = i;
        int j = Math.round(paramFloat2);
        this.mLastTouchY = j;
        this.mStartTouchY = j;
        this.mLastTouchTime = paramLong;
        this.mVelocityTracker = VelocityTracker.obtain();
        this.mSnapScrollMode = 0;
    }

    private void stopTouch()
    {
        if ((this.mScroller.isFinished()) && (!this.mSelectingText) && ((this.mTouchMode == 3) || (this.mTouchMode == 9)))
        {
            WebViewCore.resumePriority();
            WebViewCore.resumeUpdatePicture(this.mWebViewCore);
            nativeSetIsScrolling(false);
        }
        if (this.mVelocityTracker != null)
        {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
        if (this.mOverScrollGlow != null)
            this.mOverScrollGlow.releaseAll();
        if (this.mSelectingText)
        {
            this.mSelectionStarted = false;
            syncSelectionCursors();
            if (this.mIsCaretSelection)
                resetCaretTimer();
            invalidate();
        }
    }

    private void syncSelectionCursors()
    {
        this.mSelectCursorLeftLayerId = nativeGetHandleLayerId(this.mNativeClass, 0, this.mSelectCursorLeft, this.mSelectCursorLeftTextQuad);
        this.mSelectCursorRightLayerId = nativeGetHandleLayerId(this.mNativeClass, 1, this.mSelectCursorRight, this.mSelectCursorRightTextQuad);
    }

    private void updateHwAccelerated()
    {
        if (this.mNativeClass == 0);
        while (true)
        {
            return;
            boolean bool = false;
            if ((this.mWebView.isHardwareAccelerated()) && (this.mWebView.getLayerType() != 1))
                bool = true;
            int i = nativeSetHwAccelerated(this.mNativeClass, bool);
            if ((this.mWebViewCore != null) && (!this.mBlockWebkitViewMessages) && (i != 0))
                this.mWebViewCore.contentDraw();
        }
    }

    private void updateTextSelectionFromMessage(int paramInt1, int paramInt2, WebViewCore.TextSelectionData paramTextSelectionData)
    {
        int i = 1;
        if ((paramInt2 == this.mTextGeneration) && (this.mInputConnection != null) && (this.mFieldPointer == paramInt1))
            this.mInputConnection.setSelection(paramTextSelectionData.mStart, paramTextSelectionData.mEnd);
        nativeSetTextSelection(this.mNativeClass, paramTextSelectionData.mSelectTextPtr);
        if ((paramTextSelectionData.mSelectionReason == i) || ((!this.mSelectingText) && (paramTextSelectionData.mStart != paramTextSelectionData.mEnd) && (paramTextSelectionData.mSelectionReason != 2)))
        {
            selectionDone();
            this.mShowTextSelectionExtra = i;
            invalidate();
            return;
        }
        if ((paramTextSelectionData.mSelectTextPtr != 0) && ((paramTextSelectionData.mStart != paramTextSelectionData.mEnd) || ((this.mFieldPointer == paramInt1) && (this.mFieldPointer != 0))))
            if (paramTextSelectionData.mStart == paramTextSelectionData.mEnd)
            {
                label147: this.mIsCaretSelection = i;
                if ((!this.mIsCaretSelection) || ((this.mInputConnection != null) && (this.mInputConnection.getEditable().length() != 0)))
                    break label199;
                selectionDone();
            }
        while (true)
        {
            invalidate();
            break;
            i = 0;
            break label147;
            label199: if (!this.mSelectingText)
                setupWebkitSelect();
            while (true)
            {
                if (!this.mIsCaretSelection)
                    break label244;
                resetCaretTimer();
                break;
                if (!this.mSelectionStarted)
                    syncSelectionCursors();
                else
                    adjustSelectionCursors();
            }
            label244: continue;
            selectionDone();
        }
    }

    private void updateWebkitSelection()
    {
        int i = 0;
        int[] arrayOfInt = null;
        if (this.mIsCaretSelection)
            this.mSelectCursorRight.set(this.mSelectCursorLeft.x, this.mSelectCursorLeft.y);
        if (this.mSelectingText)
        {
            arrayOfInt = new int[5];
            getSelectionHandles(arrayOfInt);
            if (this.mSelectDraggingCursor == this.mSelectCursorLeft)
                i = 1;
            arrayOfInt[4] = i;
        }
        while (true)
        {
            this.mWebViewCore.removeMessages(213);
            this.mWebViewCore.sendMessageAtFrontOfQueue(213, arrayOfInt);
            return;
            nativeSetTextSelection(this.mNativeClass, 0);
        }
    }

    private void viewInvalidate()
    {
        invalidate();
    }

    private void viewInvalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        float f = this.mZoomManager.getScale();
        int i = getTitleHeight();
        this.mWebView.invalidate((int)Math.floor(f * paramInt1), i + (int)Math.floor(f * paramInt2), (int)Math.ceil(f * paramInt3), i + (int)Math.ceil(f * paramInt4));
    }

    private void viewInvalidateDelayed(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        float f = this.mZoomManager.getScale();
        int i = getTitleHeight();
        this.mWebView.postInvalidateDelayed(paramLong, (int)Math.floor(f * paramInt1), i + (int)Math.floor(f * paramInt2), (int)Math.ceil(f * paramInt3), i + (int)Math.ceil(f * paramInt4));
    }

    private int viewToContentDimension(int paramInt)
    {
        return Math.round(paramInt * this.mZoomManager.getInvScale());
    }

    private void viewToContentVisibleRect(RectF paramRectF, Rect paramRect)
    {
        paramRectF.left = (viewToContentXf(paramRect.left) / this.mWebView.getScaleX());
        paramRectF.top = (viewToContentYf(paramRect.top + getVisibleTitleHeightImpl()) / this.mWebView.getScaleY());
        paramRectF.right = (viewToContentXf(paramRect.right) / this.mWebView.getScaleX());
        paramRectF.bottom = (viewToContentYf(paramRect.bottom) / this.mWebView.getScaleY());
    }

    private float viewToContentXf(int paramInt)
    {
        return paramInt * this.mZoomManager.getInvScale();
    }

    private float viewToContentYf(int paramInt)
    {
        return (paramInt - getTitleHeight()) * this.mZoomManager.getInvScale();
    }

    public void addJavascriptInterface(Object paramObject, String paramString)
    {
        if (paramObject == null);
        while (true)
        {
            return;
            WebViewCore.JSInterfaceData localJSInterfaceData = new WebViewCore.JSInterfaceData();
            localJSInterfaceData.mObject = paramObject;
            localJSInterfaceData.mInterfaceName = paramString;
            this.mWebViewCore.sendMessage(138, localJSInterfaceData);
        }
    }

    void adjustDefaultZoomDensity(int paramInt)
    {
        updateDefaultZoomDensity(100.0F * this.mContext.getResources().getDisplayMetrics().density / paramInt);
    }

    void autoFillForm(int paramInt)
    {
        this.mPrivateHandler.obtainMessage(148, paramInt, 0).sendToTarget();
    }

    public boolean canGoBack()
    {
        boolean bool = false;
        synchronized (this.mCallbackProxy.getBackForwardList())
        {
            if (!???.getClearPending())
                if (???.getCurrentIndex() > 0)
                    bool = true;
        }
        return bool;
    }

    public boolean canGoBackOrForward(int paramInt)
    {
        boolean bool = false;
        synchronized (this.mCallbackProxy.getBackForwardList())
        {
            if (!???.getClearPending())
            {
                int i = paramInt + ???.getCurrentIndex();
                if ((i >= 0) && (i < ???.getSize()))
                    bool = true;
            }
        }
        return bool;
    }

    public boolean canGoForward()
    {
        boolean bool = false;
        synchronized (this.mCallbackProxy.getBackForwardList())
        {
            if (!???.getClearPending())
                if (???.getCurrentIndex() < -1 + ???.getSize())
                    bool = true;
        }
        return bool;
    }

    public boolean canZoomIn()
    {
        return this.mZoomManager.canZoomIn();
    }

    public boolean canZoomOut()
    {
        return this.mZoomManager.canZoomOut();
    }

    public Picture capturePicture()
    {
        Picture localPicture;
        if (this.mNativeClass == 0)
            localPicture = null;
        while (true)
        {
            return localPicture;
            localPicture = new Picture();
            nativeCopyBaseContentToPicture(localPicture);
        }
    }

    void centerFitRect(Rect paramRect)
    {
        int i = paramRect.width();
        int j = paramRect.height();
        int k = getViewWidth();
        int m = getViewHeightWithTitle();
        float f1 = Math.min(k / i, m / j);
        float f2 = this.mZoomManager.computeScaleWithLimits(f1);
        if (!this.mZoomManager.willScaleTriggerZoom(f2))
        {
            pinScrollTo(contentToViewX(paramRect.left + i / 2) - k / 2, contentToViewY(paramRect.top + j / 2) - m / 2, true, 0);
            return;
        }
        float f3 = this.mZoomManager.getScale();
        float f4 = f3 * paramRect.left - getScrollX();
        float f5 = f2 * paramRect.left;
        float f6 = f2 * i;
        float f7 = f2 * this.mContentWidth;
        float f8 = (k - f6) / 2.0F;
        label178: float f9;
        float f10;
        float f11;
        float f12;
        float f13;
        float f14;
        if (f8 > f5)
        {
            f8 = f5;
            f9 = (f4 * f2 - f8 * f3) / (f2 - f3);
            f10 = f3 * paramRect.top + getTitleHeight() - getScrollY();
            f11 = f2 * paramRect.top + getTitleHeight();
            f12 = f2 * j;
            f13 = f2 * this.mContentHeight + getTitleHeight();
            f14 = (m - f12) / 2.0F;
            if (f14 <= f11)
                break label352;
            f14 = f11;
        }
        while (true)
        {
            float f15 = (f10 * f2 - f14 * f3) / (f2 - f3);
            this.mZoomManager.setZoomCenter(f9, f15);
            this.mZoomManager.startZoomAnimation(f2, false);
            break;
            if (f8 <= f7 - f5 - f6)
                break label178;
            f8 = k - (f7 - f5);
            break label178;
            label352: if (f14 > f13 - f11 - f12)
                f14 = m - (f13 - f11);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void checkIfReadModeAvailable()
    {
        this.mWebViewCore.sendMessage(5010, 0);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void checkIfReadModeAvailable(boolean paramBoolean)
    {
        WebViewCore localWebViewCore = this.mWebViewCore;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(5010, i);
            return;
        }
    }

    public void clearCache(boolean paramBoolean)
    {
        WebViewCore localWebViewCore = this.mWebViewCore;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(111, i, 0);
            return;
        }
    }

    public void clearFormData()
    {
        if (this.mAutoCompletePopup != null)
            this.mAutoCompletePopup.clearAdapter();
    }

    public void clearHistory()
    {
        this.mCallbackProxy.getBackForwardList().setClearPending();
        this.mWebViewCore.sendMessage(112);
    }

    public void clearMatches()
    {
        if (this.mNativeClass == 0);
        while (true)
        {
            return;
            this.mWebViewCore.removeMessages(221);
            this.mWebViewCore.sendMessage(221, null);
        }
    }

    public void clearSslPreferences()
    {
        this.mWebViewCore.sendMessage(150);
    }

    public void clearView()
    {
        this.mContentWidth = 0;
        this.mContentHeight = 0;
        setBaseLayer(0, false, false);
        this.mWebViewCore.sendMessage(134);
    }

    public void clearViewState()
    {
        this.mBlockWebkitViewMessages = false;
        this.mLoadedPicture = null;
        invalidate();
    }

    public int computeHorizontalScrollOffset()
    {
        return Math.max(getScrollX(), 0);
    }

    public int computeHorizontalScrollRange()
    {
        int i = computeRealHorizontalScrollRange();
        int j = getScrollX();
        int k = computeMaxScrollX();
        if (j < 0)
            i -= j;
        while (true)
        {
            return i;
            if (j > k)
                i += j - k;
        }
    }

    int computeMaxScrollX()
    {
        return Math.max(computeRealHorizontalScrollRange() - getViewWidth(), 0);
    }

    int computeMaxScrollY()
    {
        return Math.max(computeRealVerticalScrollRange() + getTitleHeight() - getViewHeightWithTitle(), 0);
    }

    float computeReadingLevelScale(float paramFloat)
    {
        return this.mZoomManager.computeReadingLevelScale(paramFloat);
    }

    public void computeScroll()
    {
        int i;
        int j;
        int k;
        int m;
        int n;
        int i1;
        int i2;
        if (this.mScroller.computeScrollOffset())
        {
            i = getScrollX();
            j = getScrollY();
            k = this.mScroller.getCurrX();
            m = this.mScroller.getCurrY();
            invalidate();
            if (!this.mScroller.isFinished())
            {
                n = computeMaxScrollX();
                i1 = computeMaxScrollY();
                i2 = this.mOverflingDistance;
                if (this.mTouchMode == 9)
                {
                    i = this.mScrollingLayerRect.left;
                    j = this.mScrollingLayerRect.top;
                    n = this.mScrollingLayerRect.right;
                    i1 = this.mScrollingLayerRect.bottom;
                    i2 = 0;
                }
            }
        }
        label315: 
        while (true)
        {
            this.mWebViewPrivate.overScrollBy(k - i, m - j, i, j, n, i1, i2, i2, false);
            if (this.mOverScrollGlow != null)
                this.mOverScrollGlow.absorbGlow(k, m, i, j, n, i1);
            while (true)
            {
                return;
                if (this.mTouchMode != 10)
                    break label315;
                i = getTextScrollX();
                j = getTextScrollY();
                n = getMaxTextScrollX();
                i1 = getMaxTextScrollY();
                i2 = 0;
                break;
                if (this.mTouchMode == 9)
                    scrollLayerTo(k, m);
                while (true)
                {
                    abortAnimation();
                    nativeSetIsScrolling(false);
                    if (!this.mBlockWebkitViewMessages)
                    {
                        WebViewCore.resumePriority();
                        if (!this.mSelectingText)
                            WebViewCore.resumeUpdatePicture(this.mWebViewCore);
                    }
                    if ((i == getScrollX()) && (j == getScrollY()))
                        break;
                    sendOurVisibleRect();
                    break;
                    if (this.mTouchMode == 10)
                    {
                        scrollEditText(k, m);
                    }
                    else
                    {
                        setScrollXRaw(k);
                        setScrollYRaw(m);
                    }
                }
                this.mWebViewPrivate.super_computeScroll();
            }
        }
    }

    public int computeVerticalScrollExtent()
    {
        return getViewHeight();
    }

    public int computeVerticalScrollOffset()
    {
        return Math.max(getScrollY() - getTitleHeight(), 0);
    }

    public int computeVerticalScrollRange()
    {
        int i = computeRealVerticalScrollRange();
        int j = getScrollY();
        int k = computeMaxScrollY();
        if (j < 0)
            i -= j;
        while (true)
        {
            return i;
            if (j > k)
                i += j - k;
        }
    }

    protected void contentInvalidateAll()
    {
        if ((this.mWebViewCore != null) && (!this.mBlockWebkitViewMessages))
            this.mWebViewCore.sendMessage(175);
    }

    int contentToViewDimension(int paramInt)
    {
        return Math.round(paramInt * this.mZoomManager.getScale());
    }

    int contentToViewX(int paramInt)
    {
        return contentToViewDimension(paramInt);
    }

    int contentToViewY(int paramInt)
    {
        return contentToViewDimension(paramInt) + getTitleHeight();
    }

    public WebBackForwardList copyBackForwardList()
    {
        return this.mCallbackProxy.getBackForwardList().clone();
    }

    public boolean copySelection()
    {
        boolean bool = false;
        String str = getSelection();
        if ((str != null) && (str != ""))
        {
            Toast.makeText(this.mContext, 17040278, 0).show();
            bool = true;
            ((ClipboardManager)this.mContext.getSystemService("clipboard")).setText(str);
            int[] arrayOfInt = new int[4];
            getSelectionHandles(arrayOfInt);
            this.mWebViewCore.sendMessage(210, arrayOfInt);
        }
        invalidate();
        return bool;
    }

    public void cutSelection()
    {
        copySelection();
        int[] arrayOfInt = new int[4];
        getSelectionHandles(arrayOfInt);
        this.mWebViewCore.sendMessage(211, arrayOfInt);
    }

    @Deprecated
    public void debugDump()
    {
    }

    void deleteSelection(int paramInt1, int paramInt2)
    {
        this.mTextGeneration = (1 + this.mTextGeneration);
        WebViewCore.TextSelectionData localTextSelectionData = new WebViewCore.TextSelectionData(paramInt1, paramInt2, 0);
        this.mWebViewCore.sendMessage(122, this.mTextGeneration, 0, localTextSelectionData);
    }

    public void destroy()
    {
        if (this.mWebView.getViewRootImpl() != null)
            Log.e("webview", "Error: WebView.destroy() called while still attached!");
        ensureFunctorDetached();
        destroyJava();
        destroyNative();
    }

    public void discardAllTextures()
    {
        nativeDiscardAllTextures();
    }

    void dismissZoomControl()
    {
        this.mZoomManager.dismissZoomPicker();
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        switch (paramKeyEvent.getAction())
        {
        case 2:
        default:
        case 0:
        case 1:
        }
        while (true)
        {
            int i;
            for (boolean bool = this.mWebViewPrivate.super_dispatchKeyEvent(paramKeyEvent); ; bool = false)
            {
                return bool;
                this.mKeysPressed.add(Integer.valueOf(paramKeyEvent.getKeyCode()));
                break;
                i = this.mKeysPressed.indexOf(Integer.valueOf(paramKeyEvent.getKeyCode()));
                if (i != -1)
                    break label90;
            }
            label90: this.mKeysPressed.remove(i);
        }
    }

    public void documentAsText(Message paramMessage)
    {
        this.mWebViewCore.sendMessage(161, paramMessage);
    }

    public void documentHasImages(Message paramMessage)
    {
        if (paramMessage == null);
        while (true)
        {
            return;
            this.mWebViewCore.sendMessage(120, paramMessage);
        }
    }

    boolean drawHistory()
    {
        return this.mDrawHistory;
    }

    public void dumpDisplayTree()
    {
        nativeDumpDisplayTree(getUrl());
    }

    public void dumpDomTree(boolean paramBoolean)
    {
        WebViewCore localWebViewCore = this.mWebViewCore;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(170, i, 0);
            return;
        }
    }

    public void dumpRenderTree(boolean paramBoolean)
    {
        WebViewCore localWebViewCore = this.mWebViewCore;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(171, i, 0);
            return;
        }
    }

    @Deprecated
    public void emulateShiftHeld()
    {
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void enterReadMode()
    {
        if (this.mWebViewCore.getReadModeString() != null);
    }

    public void externalRepresentation(Message paramMessage)
    {
        this.mWebViewCore.sendMessage(160, paramMessage);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            destroy();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int findAll(String paramString)
    {
        return findAllBody(paramString, false);
    }

    public void findAllAsync(String paramString)
    {
        findAllBody(paramString, true);
    }

    public void findNext(boolean paramBoolean)
    {
        if (this.mNativeClass == 0)
            return;
        WebViewCore localWebViewCore;
        if (this.mFindRequest != null)
        {
            localWebViewCore = this.mWebViewCore;
            if (!paramBoolean)
                break label41;
        }
        label41: for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(222, i, this.mFindRequest);
            break;
            break;
        }
    }

    public void flingScroll(int paramInt1, int paramInt2)
    {
        this.mScroller.fling(getScrollX(), getScrollY(), paramInt1, paramInt2, 0, computeMaxScrollX(), 0, computeMaxScrollY(), this.mOverflingDistance, this.mOverflingDistance);
        invalidate();
    }

    boolean focusCandidateIsEditableText()
    {
        if (this.mFocusedNode != null);
        for (boolean bool = this.mFocusedNode.mEditable; ; bool = false)
            return bool;
    }

    public void freeMemory()
    {
        this.mWebViewCore.sendMessage(145);
    }

    int getBaseLayer()
    {
        if (this.mNativeClass == 0);
        for (int i = 0; ; i = nativeGetBaseLayer(this.mNativeClass))
            return i;
    }

    int getBlockLeftEdge(int paramInt1, int paramInt2, float paramFloat)
    {
        int i = (int)(1.0F / paramFloat * getViewWidth());
        int j = -1;
        int m;
        Rect localRect;
        if (this.mFocusedNode != null)
        {
            int k = this.mFocusedNode.mEnclosingParentRects.length;
            m = 0;
            if (m < k)
            {
                localRect = this.mFocusedNode.mEnclosingParentRects[m];
                if (localRect.width() >= this.mFocusedNode.mHitTestSlop);
            }
        }
        while (true)
        {
            m++;
            break;
            if (localRect.width() > i)
                return j;
            j = localRect.left;
        }
    }

    public SslCertificate getCertificate()
    {
        return this.mCertificate;
    }

    public int getContentHeight()
    {
        return this.mContentHeight;
    }

    public int getContentWidth()
    {
        return this.mContentWidth;
    }

    Context getContext()
    {
        return this.mContext;
    }

    float getDefaultZoomScale()
    {
        return this.mZoomManager.getDefaultScale();
    }

    public Bitmap getFavicon()
    {
        WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
        if (localWebHistoryItem != null);
        for (Bitmap localBitmap = localWebHistoryItem.getFavicon(); ; localBitmap = null)
            return localBitmap;
    }

    int getHeight()
    {
        return this.mWebView.getHeight();
    }

    int getHistoryPictureWidth()
    {
        if (this.mHistoryPicture != null);
        for (int i = this.mHistoryPicture.getWidth(); ; i = 0)
            return i;
    }

    public WebView.HitTestResult getHitTestResult()
    {
        return this.mInitialHitTestResult;
    }

    public String[] getHttpAuthUsernamePassword(String paramString1, String paramString2)
    {
        return this.mDatabase.getHttpAuthUsernamePassword(paramString1, paramString2);
    }

    public String getOriginalUrl()
    {
        WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
        if (localWebHistoryItem != null);
        for (String str = localWebHistoryItem.getOriginalUrl(); ; str = null)
            return str;
    }

    public int getPageBackgroundColor()
    {
        int i;
        if (this.mNativeClass == 0)
            i = -1;
        while (true)
        {
            return i;
            if (getSettings().getNightReadModeEnabled())
                i = -16777216;
            else
                i = nativeGetBackgroundColor(this.mNativeClass);
        }
    }

    public int getProgress()
    {
        return this.mCallbackProxy.getProgress();
    }

    public float getScale()
    {
        return this.mZoomManager.getScale();
    }

    int getScaledNavSlop()
    {
        return viewToContentDimension(this.mNavSlop);
    }

    public WebViewProvider.ScrollDelegate getScrollDelegate()
    {
        return this;
    }

    int getScrollX()
    {
        return this.mWebView.getScrollX();
    }

    int getScrollY()
    {
        return this.mWebView.getScrollY();
    }

    public SearchBox getSearchBox()
    {
        if ((this.mWebViewCore == null) || (this.mWebViewCore.getBrowserFrame() == null));
        for (SearchBox localSearchBox = null; ; localSearchBox = this.mWebViewCore.getBrowserFrame().getSearchBox())
            return localSearchBox;
    }

    String getSelection()
    {
        if (this.mNativeClass == 0);
        for (String str = ""; ; str = nativeGetSelection())
            return str;
    }

    public WebSettingsClassic getSettings()
    {
        if (this.mWebViewCore != null);
        for (WebSettingsClassic localWebSettingsClassic = this.mWebViewCore.getSettings(); ; localWebSettingsClassic = null)
            return localWebSettingsClassic;
    }

    public String getTitle()
    {
        WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
        if (localWebHistoryItem != null);
        for (String str = localWebHistoryItem.getTitle(); ; str = null)
            return str;
    }

    protected int getTitleHeight()
    {
        if ((this.mWebView instanceof TitleBarDelegate));
        for (int i = ((TitleBarDelegate)this.mWebView).getTitleHeight(); ; i = 0)
            return i;
    }

    public String getTouchIconUrl()
    {
        WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
        if (localWebHistoryItem != null);
        for (String str = localWebHistoryItem.getTouchIconUrl(); ; str = null)
            return str;
    }

    public String getUrl()
    {
        WebHistoryItem localWebHistoryItem = this.mCallbackProxy.getBackForwardList().getCurrentItem();
        if (localWebHistoryItem != null);
        for (String str = localWebHistoryItem.getUrl(); ; str = null)
            return str;
    }

    public WebViewProvider.ViewDelegate getViewDelegate()
    {
        return this;
    }

    int getViewHeight()
    {
        return getViewHeightWithTitle() - getVisibleTitleHeightImpl();
    }

    int getViewHeightWithTitle()
    {
        int i = getHeight();
        if ((this.mWebView.isHorizontalScrollBarEnabled()) && (!this.mOverlayHorizontalScrollbar))
            i -= this.mWebViewPrivate.getHorizontalScrollbarHeight();
        return i;
    }

    ViewManager getViewManager()
    {
        return this.mViewManager;
    }

    int getViewWidth()
    {
        if ((!this.mWebView.isVerticalScrollBarEnabled()) || (this.mOverlayVerticalScrollbar));
        for (int i = getWidth(); ; i = Math.max(0, getWidth() - this.mWebView.getVerticalScrollbarWidth()))
            return i;
    }

    @Deprecated
    public int getVisibleTitleHeight()
    {
        return getVisibleTitleHeightImpl();
    }

    public WebBackForwardListClient getWebBackForwardListClient()
    {
        return this.mCallbackProxy.getWebBackForwardListClient();
    }

    public WebChromeClient getWebChromeClient()
    {
        return this.mCallbackProxy.getWebChromeClient();
    }

    public WebView getWebView()
    {
        return this.mWebView;
    }

    public WebViewClient getWebViewClient()
    {
        return this.mCallbackProxy.getWebViewClient();
    }

    /** @deprecated */
    public WebViewCore getWebViewCore()
    {
        try
        {
            WebViewCore localWebViewCore = this.mWebViewCore;
            return localWebViewCore;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    int getWidth()
    {
        return this.mWebView.getWidth();
    }

    @Deprecated
    public View getZoomControls()
    {
        if (!getSettings().supportZoom())
            Log.w("webview", "This WebView doesn't support zoom.");
        for (View localView = null; ; localView = this.mZoomManager.getExternalZoomPicker())
            return localView;
    }

    float getZoomOverviewScale()
    {
        return this.mZoomManager.getZoomOverviewScale();
    }

    public void goBack()
    {
        goBackOrForwardImpl(-1);
    }

    public void goBackOrForward(int paramInt)
    {
        goBackOrForwardImpl(paramInt);
    }

    public void goForward()
    {
        goBackOrForwardImpl(1);
    }

    void incrementTextGeneration()
    {
        this.mTextGeneration = (1 + this.mTextGeneration);
    }

    public void init(Map<String, Object> paramMap, boolean paramBoolean)
    {
        Context localContext = this.mContext;
        JniUtil.setContext(localContext);
        this.mCallbackProxy = new CallbackProxy(localContext, this);
        this.mViewManager = new ViewManager(this);
        L10nUtils.setApplicationContext(localContext.getApplicationContext());
        this.mWebViewCore = new WebViewCore(localContext, this, this.mCallbackProxy, paramMap);
        this.mDatabase = WebViewDatabaseClassic.getInstance(localContext);
        this.mScroller = new OverScroller(localContext, null, 0.0F, 0.0F, false);
        this.mZoomManager = new ZoomManager(this, this.mCallbackProxy);
        init();
        setupPackageListener(localContext);
        setupProxyListener(localContext);
        setupTrustStorageListener(localContext);
        updateMultiTouchSupport(localContext);
        if (paramBoolean)
            startPrivateBrowsing();
        this.mAutoFillData = new WebViewCore.AutoFillData();
        this.mEditTextScroller = new Scroller(localContext);
    }

    void invalidate()
    {
        this.mWebView.invalidate();
    }

    public void invokeZoomPicker()
    {
        if (!getSettings().supportZoom())
            Log.w("webview", "This WebView doesn't support zoom.");
        while (true)
        {
            return;
            clearHelpers();
            this.mZoomManager.invokeZoomPicker();
        }
    }

    public boolean isPaused()
    {
        return this.mIsPaused;
    }

    public boolean isPrivateBrowsingEnabled()
    {
        WebSettingsClassic localWebSettingsClassic = getSettings();
        if (localWebSettingsClassic != null);
        for (boolean bool = localWebSettingsClassic.isPrivateBrowsingEnabled(); ; bool = false)
            return bool;
    }

    boolean isRectFitOnScreen(Rect paramRect)
    {
        int i = paramRect.width();
        int j = paramRect.height();
        int k = getViewWidth();
        int m = getViewHeightWithTitle();
        float f1 = Math.min(k / i, m / j);
        float f2 = this.mZoomManager.computeScaleWithLimits(f1);
        if ((!this.mZoomManager.willScaleTriggerZoom(f2)) && (contentToViewX(paramRect.left) >= getScrollX()) && (contentToViewX(paramRect.right) <= k + getScrollX()) && (contentToViewY(paramRect.top) >= getScrollY()) && (contentToViewY(paramRect.bottom) <= m + getScrollY()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void loadData(String paramString1, String paramString2, String paramString3)
    {
        loadDataImpl(paramString1, paramString2, paramString3);
    }

    public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        if ((paramString1 != null) && (paramString1.toLowerCase().startsWith("data:")))
            loadDataImpl(paramString2, paramString3, paramString4);
        while (true)
        {
            return;
            switchOutDrawHistory();
            WebViewCore.BaseUrlData localBaseUrlData = new WebViewCore.BaseUrlData();
            localBaseUrlData.mBaseUrl = paramString1;
            localBaseUrlData.mData = paramString2;
            localBaseUrlData.mMimeType = paramString3;
            localBaseUrlData.mEncoding = paramString4;
            localBaseUrlData.mHistoryUrl = paramString5;
            this.mWebViewCore.sendMessage(139, localBaseUrlData);
            clearHelpers();
        }
    }

    public void loadUrl(String paramString)
    {
        loadUrlImpl(paramString);
    }

    public void loadUrl(String paramString, Map<String, String> paramMap)
    {
        loadUrlImpl(paramString, paramMap);
    }

    public void loadViewState(InputStream paramInputStream)
    {
        this.mBlockWebkitViewMessages = true;
        AsyncTask local8 = new AsyncTask()
        {
            protected WebViewCore.DrawData doInBackground(InputStream[] paramAnonymousArrayOfInputStream)
            {
                try
                {
                    WebViewCore.DrawData localDrawData2 = ViewStateSerializer.deserializeViewState(paramAnonymousArrayOfInputStream[0]);
                    localDrawData1 = localDrawData2;
                    return localDrawData1;
                }
                catch (IOException localIOException)
                {
                    while (true)
                        WebViewCore.DrawData localDrawData1 = null;
                }
            }

            protected void onPostExecute(WebViewCore.DrawData paramAnonymousDrawData)
            {
                if (paramAnonymousDrawData == null)
                    Log.e("webview", "Failed to load view state!");
                while (true)
                {
                    return;
                    paramAnonymousDrawData.mViewSize = new Point(WebViewClassic.this.getViewWidth(), WebViewClassic.this.getViewHeightWithTitle() - WebViewClassic.this.getTitleHeight());
                    paramAnonymousDrawData.mViewState.mDefaultScale = WebViewClassic.this.getDefaultZoomScale();
                    WebViewClassic.access$2002(WebViewClassic.this, paramAnonymousDrawData);
                    WebViewClassic.this.setNewPicture(WebViewClassic.this.mLoadedPicture, true);
                    WebViewClassic.this.mLoadedPicture.mViewState = null;
                }
            }
        };
        InputStream[] arrayOfInputStream = new InputStream[1];
        arrayOfInputStream[0] = paramInputStream;
        local8.execute(arrayOfInputStream);
    }

    native String nativeGetProperty(String paramString);

    native boolean nativeSetProperty(String paramString1, String paramString2);

    void notifyFindDialogDismissed()
    {
        this.mFindCallback = null;
        this.mCachedOverlappingActionModeHeight = -1;
        if (this.mWebViewCore == null);
        while (true)
        {
            return;
            clearMatches();
            setFindIsUp(false);
            pinScrollTo(getScrollX(), getScrollY(), false, 0);
            invalidate();
        }
    }

    public void onAttachedToWindow()
    {
        if (this.mWebView.hasWindowFocus())
            setActive(true);
        if (isAccessibilityEnabled())
            getAccessibilityInjector().addAccessibilityApisIfNecessary();
        updateHwAccelerated();
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        this.mCachedOverlappingActionModeHeight = -1;
        if ((this.mSelectingText) && (this.mOrientation != paramConfiguration.orientation))
            selectionDone();
        this.mOrientation = paramConfiguration.orientation;
        if ((this.mWebViewCore != null) && (!this.mBlockWebkitViewMessages))
            this.mWebViewCore.sendMessage(134);
    }

    public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo)
    {
        if (this.mInputConnection == null)
        {
            this.mInputConnection = new WebViewInputConnection();
            this.mAutoCompletePopup = new AutoCompletePopup(this, this.mInputConnection);
        }
        this.mInputConnection.setupEditorInfo(paramEditorInfo);
        return this.mInputConnection;
    }

    public void onDetachedFromWindow()
    {
        clearHelpers();
        this.mZoomManager.dismissZoomPicker();
        if (this.mWebView.hasWindowFocus())
            setActive(false);
        if (isAccessibilityEnabled())
            getAccessibilityInjector().removeAccessibilityApisIfNecessary();
        while (true)
        {
            updateHwAccelerated();
            ensureFunctorDetached();
            return;
            this.mAccessibilityInjector = null;
        }
    }

    public void onDraw(Canvas paramCanvas)
    {
        if (inFullScreenMode())
        {
            label7: return;
        }
        else
        {
            while (true)
            {
                if ((getSettings() != null) && (getSettings().getNightReadModeEnabled()))
                {
                    this.mBackgroundColor = -16777216;
                    paramCanvas.drawColor(-16777216);
                }
                if (this.mNativeClass == 0)
                {
                    paramCanvas.drawColor(this.mBackgroundColor);
                }
                else
                {
                    if (((this.mContentWidth | this.mContentHeight) != 0) || (this.mHistoryPicture != null))
                        break;
                    paramCanvas.drawColor(this.mBackgroundColor);
                }
            }
            if (!paramCanvas.isHardwareAccelerated())
                break label349;
            this.mZoomManager.setHardwareAccelerated();
            label101: int i = paramCanvas.save();
            if ((this.mInOverScrollMode) && (!getSettings().getUseWebViewBackgroundForOverscrollBackground()))
                drawOverScrollBackground(paramCanvas);
            paramCanvas.translate(0.0F, getTitleHeight());
            drawContent(paramCanvas);
            paramCanvas.restoreToCount(i);
            this.mWebViewCore.signalRepaintDone();
            if ((this.mOverScrollGlow != null) && (this.mOverScrollGlow.drawEdgeGlows(paramCanvas)))
                invalidate();
            if (this.mFocusTransition == null)
                break label359;
            this.mFocusTransition.draw(paramCanvas);
        }
        while (true)
        {
            if ((!getSettings().getNavDump()) || ((this.mTouchHighlightX | this.mTouchHighlightY) == 0))
                break label7;
            if (this.mTouchCrossHairColor == null)
            {
                this.mTouchCrossHairColor = new Paint();
                this.mTouchCrossHairColor.setColor(-65536);
            }
            paramCanvas.drawLine(this.mTouchHighlightX - this.mNavSlop, this.mTouchHighlightY - this.mNavSlop, 1 + (this.mTouchHighlightX + this.mNavSlop), 1 + (this.mTouchHighlightY + this.mNavSlop), this.mTouchCrossHairColor);
            paramCanvas.drawLine(1 + (this.mTouchHighlightX + this.mNavSlop), this.mTouchHighlightY - this.mNavSlop, this.mTouchHighlightX - this.mNavSlop, 1 + (this.mTouchHighlightY + this.mNavSlop), this.mTouchCrossHairColor);
            break label7;
            label349: this.mWebViewCore.resumeWebKitDraw();
            break label101;
            label359: if (!shouldDrawHighlightRect())
                break;
            RegionIterator localRegionIterator = new RegionIterator(this.mTouchHighlightRegion);
            Rect localRect = new Rect();
            while (localRegionIterator.next(localRect))
                paramCanvas.drawRect(localRect, this.mTouchHightlightPaint);
        }
    }

    public void onDrawVerticalScrollBar(Canvas paramCanvas, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (getScrollY() < 0)
            paramInt2 -= getScrollY();
        paramDrawable.setBounds(paramInt1, paramInt2 + getVisibleTitleHeightImpl(), paramInt3, paramInt4);
        paramDrawable.draw(paramCanvas);
    }

    void onFixedLengthZoomAnimationEnd()
    {
        if ((!this.mBlockWebkitViewMessages) && (!this.mSelectingText))
            WebViewCore.resumeUpdatePicture(this.mWebViewCore);
        onZoomAnimationEnd();
    }

    void onFixedLengthZoomAnimationStart()
    {
        WebViewCore.pauseUpdatePicture(getWebViewCore());
        onZoomAnimationStart();
    }

    public void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
        if (paramBoolean)
        {
            this.mDrawCursorRing = true;
            setFocusControllerActive(true);
        }
        while (true)
        {
            if (!this.mTouchHighlightRegion.isEmpty())
                this.mWebView.invalidate(this.mTouchHighlightRegion.getBounds());
            return;
            this.mDrawCursorRing = false;
            setFocusControllerActive(false);
            this.mKeysPressed.clear();
        }
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if ((0x2 & paramMotionEvent.getSource()) != 0)
            switch (paramMotionEvent.getAction())
            {
            default:
            case 8:
            }
        label131: 
        while (true)
        {
            boolean bool = this.mWebViewPrivate.super_onGenericMotionEvent(paramMotionEvent);
            label41: return bool;
            float f1;
            if ((0x1 & paramMotionEvent.getMetaState()) != 0)
                f1 = 0.0F;
            for (float f2 = paramMotionEvent.getAxisValue(9); ; f2 = paramMotionEvent.getAxisValue(10))
            {
                if ((f2 == 0.0F) && (f1 == 0.0F))
                    break label131;
                int i = (int)(f1 * this.mWebViewPrivate.getVerticalScrollFactor());
                if (!pinScrollBy((int)(f2 * this.mWebViewPrivate.getHorizontalScrollFactor()), i, false, 0))
                    break;
                bool = true;
                break label41;
                f1 = -paramMotionEvent.getAxisValue(9);
            }
        }
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        if (this.mNativeClass == 0);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            int i = viewToContentX((int)paramMotionEvent.getX() + getScrollX());
            int j = viewToContentY((int)paramMotionEvent.getY() + getScrollY());
            this.mWebViewCore.sendMessage(135, i, j);
            this.mWebViewPrivate.super_onHoverEvent(paramMotionEvent);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        paramAccessibilityEvent.setScrollable(isScrollableForAccessibility());
        paramAccessibilityEvent.setScrollX(getScrollX());
        paramAccessibilityEvent.setScrollY(getScrollY());
        paramAccessibilityEvent.setMaxScrollX(Math.max(contentToViewX(getContentWidth()) - (getWidth() - this.mWebView.getPaddingLeft() - this.mWebView.getPaddingLeft()), 0));
        paramAccessibilityEvent.setMaxScrollY(Math.max(contentToViewY(getContentHeight()) - (getHeight() - this.mWebView.getPaddingTop() - this.mWebView.getPaddingBottom()), 0));
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        if (!this.mWebView.isEnabled())
            return;
        paramAccessibilityNodeInfo.setScrollable(isScrollableForAccessibility());
        int i = Math.max(contentToViewY(getContentHeight()) - (getHeight() - this.mWebView.getPaddingTop() - this.mWebView.getPaddingBottom()), 0);
        if (getScrollY() > 0)
            label60: if (getScrollY() - i <= 0)
                break label107;
        label107: for (int j = 1; ; j = 0)
        {
            if (j != 0)
                paramAccessibilityNodeInfo.addAction(4096);
            if (j != 0)
                paramAccessibilityNodeInfo.addAction(8192);
            getAccessibilityInjector().onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
            break;
            break label60;
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool1 = false;
        boolean bool2 = true;
        if (this.mIsCaretSelection)
            selectionDone();
        if (this.mBlockWebkitViewMessages)
            bool2 = false;
        do
        {
            while (true)
            {
                return bool2;
                if (paramKeyEvent.isCtrlPressed())
                    bool2 = false;
                else if (this.mNativeClass == 0)
                    bool2 = false;
                else if ((paramKeyEvent.isSystem()) || (this.mCallbackProxy.uiOverrideKeyEvent(paramKeyEvent)))
                    bool2 = false;
                else if ((!isAccessibilityEnabled()) || (!getAccessibilityInjector().handleKeyEventIfNecessary(paramKeyEvent)))
                    if (paramInt == 92)
                    {
                        if (paramKeyEvent.hasNoModifiers())
                            pageUp(false);
                        else if (paramKeyEvent.hasModifiers(2))
                            pageUp(bool2);
                    }
                    else if (paramInt == 93)
                    {
                        if (paramKeyEvent.hasNoModifiers())
                            pageDown(false);
                        else if (paramKeyEvent.hasModifiers(2))
                            pageDown(bool2);
                    }
                    else if ((paramInt == 122) && (paramKeyEvent.hasNoModifiers()))
                    {
                        pageUp(bool2);
                    }
                    else
                    {
                        if ((paramInt != 123) || (!paramKeyEvent.hasNoModifiers()))
                            break;
                        pageDown(bool2);
                    }
            }
            if ((paramInt >= 19) && (paramInt <= 22))
                switchOutDrawHistory();
            if (!isEnterActionKey(paramInt))
                break;
            switchOutDrawHistory();
            if (paramKeyEvent.getRepeatCount() != 0)
                break;
        }
        while (this.mSelectingText);
        this.mGotCenterDown = bool2;
        this.mPrivateHandler.sendMessageDelayed(this.mPrivateHandler.obtainMessage(114), 1000L);
        if (getSettings().getNavDump())
            switch (paramInt)
            {
            default:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            }
        while (true)
        {
            sendKeyEvent(paramKeyEvent);
            break;
            dumpDisplayTree();
            continue;
            if (paramInt == 12)
                bool1 = bool2;
            dumpDomTree(bool1);
            continue;
            if (paramInt == 14)
                bool1 = bool2;
            dumpRenderTree(bool1);
        }
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (this.mBlockWebkitViewMessages);
        while (true)
        {
            return bool;
            if ((paramInt1 == 0) && (paramKeyEvent.getCharacters() != null))
            {
                sendBatchableInputMessage(103, 0, 0, paramKeyEvent);
                sendBatchableInputMessage(104, 0, 0, paramKeyEvent);
                bool = true;
            }
        }
    }

    public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
    {
        if (this.mAutoCompletePopup != null);
        for (boolean bool = this.mAutoCompletePopup.onKeyPreIme(paramInt, paramKeyEvent); ; bool = false)
            return bool;
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (this.mBlockWebkitViewMessages);
        while (true)
        {
            return bool;
            if (this.mNativeClass != 0)
                if ((paramInt == 5) && (this.mInitialHitTestResult != null) && (this.mInitialHitTestResult.getType() == 2))
                {
                    Intent localIntent = new Intent("android.intent.action.DIAL", Uri.parse(this.mInitialHitTestResult.getExtra()));
                    this.mContext.startActivity(localIntent);
                    bool = true;
                }
                else if ((!paramKeyEvent.isSystem()) && (!this.mCallbackProxy.uiOverrideKeyEvent(paramKeyEvent)))
                {
                    if ((isAccessibilityEnabled()) && (getAccessibilityInjector().handleKeyEventIfNecessary(paramKeyEvent)))
                    {
                        bool = true;
                    }
                    else if (isEnterActionKey(paramInt))
                    {
                        this.mPrivateHandler.removeMessages(114);
                        this.mGotCenterDown = false;
                        if (this.mSelectingText)
                        {
                            copySelection();
                            selectionDone();
                            bool = true;
                        }
                    }
                    else
                    {
                        sendKeyEvent(paramKeyEvent);
                        bool = true;
                    }
                }
        }
    }

    public void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt2);
        int j = View.MeasureSpec.getSize(paramInt2);
        int k = View.MeasureSpec.getMode(paramInt1);
        int m = View.MeasureSpec.getSize(paramInt1);
        int n = j;
        int i1 = m;
        int i2 = contentToViewDimension(this.mContentHeight);
        int i3 = contentToViewDimension(this.mContentWidth);
        if (i != 1073741824)
        {
            this.mHeightCanMeasure = true;
            n = i2;
            if ((i == -2147483648) && (n > j))
            {
                this.mHeightCanMeasure = false;
                n = j | 0x1000000;
            }
        }
        while (true)
        {
            if (this.mNativeClass != 0)
                nativeSetHeightCanMeasure(this.mHeightCanMeasure);
            if (k == 0)
            {
                this.mWidthCanMeasure = true;
                i1 = i3;
            }
            try
            {
                label123: this.mWebViewPrivate.setMeasuredDimension(i1, n);
                return;
                this.mHeightCanMeasure = false;
                continue;
                if (i1 < i3)
                    i1 |= 16777216;
                this.mWidthCanMeasure = false;
                break label123;
            }
            finally
            {
            }
        }
    }

    public void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mTouchMode == 10)
            scrollEditText(paramInt1, paramInt2);
        while (true)
        {
            return;
            if (this.mTouchMode != 9)
                break;
            scrollLayerTo(paramInt1, paramInt2);
        }
        this.mInOverScrollMode = false;
        int i = computeMaxScrollX();
        int j = computeMaxScrollY();
        if (i == 0)
            paramInt1 = pinLocX(paramInt1);
        while (true)
        {
            if ((paramInt2 < 0) || (paramInt2 > j))
                this.mInOverScrollMode = true;
            int k = getScrollX();
            int m = getScrollY();
            this.mWebViewPrivate.super_scrollTo(paramInt1, paramInt2);
            if (this.mOverScrollGlow == null)
                break;
            this.mOverScrollGlow.pullGlow(getScrollX(), getScrollY(), k, m, i, j);
            break;
            if ((paramInt1 < 0) || (paramInt1 > i))
                this.mInOverScrollMode = true;
        }
    }

    void onPageFinished(String paramString)
    {
        this.mZoomManager.onPageFinished(paramString);
        if (isAccessibilityEnabled())
            getAccessibilityInjector().onPageFinished(paramString);
    }

    void onPageStarted(String paramString)
    {
        this.mWebView.setCertificate(null);
        if (isAccessibilityEnabled())
            getAccessibilityInjector().onPageStarted(paramString);
        this.mIsEditingText = false;
    }

    public void onPause()
    {
        if (!this.mIsPaused)
        {
            this.mIsPaused = true;
            this.mWebViewCore.sendMessage(143);
            if (this.mHTML5VideoViewProxy != null)
                this.mHTML5VideoViewProxy.pauseAndDispatch();
            if (this.mNativeClass != 0)
                nativeSetPauseDrawing(this.mNativeClass, true);
            cancelSelectDialog();
            WebCoreThreadWatchdog.pause();
        }
    }

    void onPinchToZoomAnimationEnd(ScaleGestureDetector paramScaleGestureDetector)
    {
        onZoomAnimationEnd();
        this.mTouchMode = 8;
        this.mConfirmMove = true;
        startTouch(paramScaleGestureDetector.getFocusX(), paramScaleGestureDetector.getFocusY(), this.mLastTouchTime);
    }

    void onPinchToZoomAnimationStart()
    {
        cancelTouch();
        onZoomAnimationStart();
    }

    public void onResume()
    {
        if (this.mIsPaused)
        {
            this.mIsPaused = false;
            this.mWebViewCore.sendMessage(144);
            if (this.mNativeClass != 0)
                nativeSetPauseDrawing(this.mNativeClass, false);
        }
        WebCoreThreadWatchdog.resume();
    }

    boolean onSavePassword(String paramString1, String paramString2, String paramString3, final Message paramMessage)
    {
        int i = 1;
        int j = 0;
        if (paramMessage == null)
            this.mDatabase.setUsernamePassword(paramString1, paramString2, paramString3);
        while (true)
        {
            i = j;
            while (true)
            {
                return i;
                if (this.mResumeMsg == null)
                    break;
                Log.w("webview", "onSavePassword should not be called while dialog is up");
                paramMessage.sendToTarget();
            }
            this.mResumeMsg = paramMessage;
            final Message localMessage1 = this.mPrivateHandler.obtainMessage(i);
            localMessage1.getData().putString("host", paramString1);
            localMessage1.getData().putString("username", paramString2);
            localMessage1.getData().putString("password", paramString3);
            localMessage1.obj = paramMessage;
            final Message localMessage2 = this.mPrivateHandler.obtainMessage(2);
            localMessage2.getData().putString("host", paramString1);
            localMessage2.getData().putString("username", paramString2);
            localMessage2.getData().putString("password", paramString3);
            localMessage2.obj = paramMessage;
            new AlertDialog.Builder(this.mContext).setTitle(17040192).setMessage(17040273).setPositiveButton(17040274, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    if (WebViewClassic.this.mResumeMsg != null)
                    {
                        paramMessage.sendToTarget();
                        WebViewClassic.access$1702(WebViewClassic.this, null);
                    }
                }
            }).setNeutralButton(17040275, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    if (WebViewClassic.this.mResumeMsg != null)
                    {
                        localMessage1.sendToTarget();
                        WebViewClassic.access$1702(WebViewClassic.this, null);
                    }
                }
            }).setNegativeButton(17040276, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    if (WebViewClassic.this.mResumeMsg != null)
                    {
                        localMessage2.sendToTarget();
                        WebViewClassic.access$1702(WebViewClassic.this, null);
                    }
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                public void onCancel(DialogInterface paramAnonymousDialogInterface)
                {
                    if (WebViewClassic.this.mResumeMsg != null)
                    {
                        paramMessage.sendToTarget();
                        WebViewClassic.access$1702(WebViewClassic.this, null);
                    }
                }
            }).show();
            j = 1;
        }
    }

    public void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (!this.mInOverScrollMode)
        {
            sendOurVisibleRect();
            int i = getTitleHeight();
            if (Math.max(i - paramInt2, 0) != Math.max(i - paramInt4, 0))
                sendViewSizeZoom(false);
        }
    }

    public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = (int)(Math.max(paramInt1, paramInt2) / this.mZoomManager.getDefaultMinZoomScale());
        if (i > sMaxViewportWidth)
            sMaxViewportWidth = i;
        this.mZoomManager.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        if ((this.mLoadedPicture != null) && (this.mDelaySetPicture == null))
            setNewPicture(this.mLoadedPicture, false);
        if (this.mIsEditingText)
            scrollEditIntoView();
        relocateAutoCompletePopup();
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if ((this.mNativeClass == 0) || ((!this.mWebView.isClickable()) && (!this.mWebView.isLongClickable())));
        while (true)
        {
            return bool;
            if (this.mInputDispatcher != null)
            {
                if ((this.mWebView.isFocusable()) && (this.mWebView.isFocusableInTouchMode()) && (!this.mWebView.isFocused()))
                    this.mWebView.requestFocus();
                if (paramMotionEvent.getAction() == 1)
                {
                    this.mIsActionUp = true;
                    this.mAfterStart = null;
                    this.mBeforeStart = null;
                    invalidate();
                    if (this.mSelectingText)
                        showFloatView();
                }
                while (true)
                {
                    if (!this.mInputDispatcher.postPointerEvent(paramMotionEvent, getScrollX(), getScrollY() - getTitleHeight(), this.mZoomManager.getInvScale()))
                        break label191;
                    this.mInputDispatcher.dispatchUiEvents();
                    bool = true;
                    break;
                    if (this.mSelectingText)
                    {
                        this.mIsActionUp = false;
                        if (paramMotionEvent.getAction() == 3)
                            showFloatView();
                        else
                            hideFloatView();
                    }
                }
                label191: Log.w("webview", "mInputDispatcher rejected the event!");
            }
        }
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        int i = 1;
        long l = paramMotionEvent.getEventTime();
        if ((0x2 & paramMotionEvent.getMetaState()) != 0)
        {
            if (paramMotionEvent.getY() > 0.0F)
                pageDown(i);
            if (paramMotionEvent.getY() < 0.0F)
                pageUp(i);
        }
        while (true)
        {
            return i;
            if (paramMotionEvent.getAction() == 0)
            {
                if (!this.mSelectingText)
                {
                    this.mTrackballDown = i;
                    if (this.mNativeClass == 0)
                    {
                        i = 0;
                    }
                    else
                    {
                        if (this.mWebView.isInTouchMode())
                            this.mWebView.requestFocusFromTouch();
                        i = 0;
                    }
                }
            }
            else
            {
                int j;
                if (paramMotionEvent.getAction() == i)
                {
                    this.mPrivateHandler.removeMessages(114);
                    this.mTrackballDown = false;
                    this.mTrackballUpTime = l;
                    if (this.mSelectingText)
                    {
                        copySelection();
                        selectionDone();
                    }
                    else
                    {
                        j = 0;
                    }
                }
                else if (((this.mMapTrackballToArrowKeys) && ((0x1 & paramMotionEvent.getMetaState()) == 0)) || (AccessibilityManager.getInstance(this.mContext).isEnabled()))
                {
                    j = 0;
                }
                else if ((!this.mTrackballDown) && (l - this.mTrackballUpTime >= 200L))
                {
                    switchOutDrawHistory();
                    if (l - this.mTrackballLastTime > 200L)
                    {
                        this.mTrackballFirstTime = l;
                        this.mTrackballYMove = 0;
                        this.mTrackballXMove = 0;
                    }
                    this.mTrackballLastTime = l;
                    this.mTrackballRemainsX += paramMotionEvent.getX();
                    this.mTrackballRemainsY += paramMotionEvent.getY();
                    doTrackball(l, paramMotionEvent.getMetaState());
                }
            }
        }
    }

    public void onVisibilityChanged(View paramView, int paramInt)
    {
        if ((paramInt != 0) && (this.mZoomManager != null))
            this.mZoomManager.dismissZoomPicker();
        updateDrawingState();
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        setActive(paramBoolean);
        if (paramBoolean)
        {
            JWebCoreJavaBridge.setActiveWebView(this);
            if (this.mPictureUpdatePausedForFocusChange)
                WebViewCore.resumeUpdatePicture(this.mWebViewCore);
        }
        for (this.mPictureUpdatePausedForFocusChange = false; ; this.mPictureUpdatePausedForFocusChange = true)
        {
            WebSettingsClassic localWebSettingsClassic;
            do
            {
                return;
                JWebCoreJavaBridge.removeActiveWebView(this);
                localWebSettingsClassic = getSettings();
            }
            while ((localWebSettingsClassic == null) || (!localWebSettingsClassic.enableSmoothTransition()) || (this.mWebViewCore == null) || (WebViewCore.isUpdatePicturePaused(this.mWebViewCore)));
            WebViewCore.pauseUpdatePicture(this.mWebViewCore);
        }
    }

    public void onWindowVisibilityChanged(int paramInt)
    {
        updateDrawingState();
    }

    public boolean overlayHorizontalScrollbar()
    {
        return this.mOverlayHorizontalScrollbar;
    }

    public boolean overlayVerticalScrollbar()
    {
        return this.mOverlayVerticalScrollbar;
    }

    public boolean pageDown(boolean paramBoolean)
    {
        boolean bool = false;
        if (this.mNativeClass == 0);
        while (true)
        {
            return bool;
            if (paramBoolean)
            {
                bool = pinScrollTo(getScrollX(), computeRealVerticalScrollRange(), true, 0);
            }
            else
            {
                int i = getHeight();
                if (i > 48);
                for (int j = i - 24; ; j = i / 2)
                {
                    if (!this.mScroller.isFinished())
                        break label81;
                    bool = pinScrollBy(0, j, true, 0);
                    break;
                }
                label81: bool = extendScroll(j);
            }
        }
    }

    protected void pageSwapCallback(boolean paramBoolean)
    {
        this.mWebViewCore.resumeWebKitDraw();
        if (paramBoolean)
            this.mWebViewCore.sendMessage(196);
        if ((this.mWebView instanceof PageSwapDelegate))
            ((PageSwapDelegate)this.mWebView).onPageSwapOccurred(paramBoolean);
        if (this.mPictureListener != null)
            this.mPictureListener.onNewPicture(getWebView(), capturePicture());
    }

    public boolean pageUp(boolean paramBoolean)
    {
        boolean bool = false;
        if (this.mNativeClass == 0);
        while (true)
        {
            return bool;
            if (paramBoolean)
            {
                bool = pinScrollTo(getScrollX(), 0, true, 0);
            }
            else
            {
                int i = getHeight();
                if (i > 48);
                for (int j = 24 + -i; ; j = -i / 2)
                {
                    if (!this.mScroller.isFinished())
                        break label80;
                    bool = pinScrollBy(0, j, true, 0);
                    break;
                }
                label80: bool = extendScroll(j);
            }
        }
    }

    void passToJavaScript(String paramString, KeyEvent paramKeyEvent)
    {
        if (this.mWebViewCore == null);
        while (true)
        {
            return;
            WebViewCore.JSKeyData localJSKeyData = new WebViewCore.JSKeyData();
            localJSKeyData.mEvent = paramKeyEvent;
            localJSKeyData.mCurrentText = paramString;
            this.mTextGeneration = (1 + this.mTextGeneration);
            this.mWebViewCore.sendMessage(115, this.mTextGeneration, 0, localJSKeyData);
            this.mWebViewCore.removeMessages(128);
            this.mWebViewCore.sendMessageDelayed(128, null, 1000L);
        }
    }

    public void pasteFromClipboard()
    {
        ClipData localClipData = ((ClipboardManager)this.mContext.getSystemService("clipboard")).getPrimaryClip();
        if (localClipData != null)
        {
            CharSequence localCharSequence = localClipData.getItemAt(0).getText();
            if (this.mInputConnection != null)
                this.mInputConnection.replaceSelection(localCharSequence);
        }
    }

    public void pauseTimers()
    {
        this.mWebViewCore.sendMessage(109);
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool1 = true;
        if (!this.mWebView.isEnabled())
            bool1 = this.mWebViewPrivate.super_performAccessibilityAction(paramInt, paramBundle);
        while (true)
        {
            return bool1;
            if (getAccessibilityInjector().supportsAccessibilityAction(paramInt))
                bool1 = getAccessibilityInjector().performAccessibilityAction(paramInt, paramBundle);
            else
                switch (paramInt)
                {
                default:
                    bool1 = this.mWebViewPrivate.super_performAccessibilityAction(paramInt, paramBundle);
                    break;
                case 4096:
                case 8192:
                    int i = contentToViewY(getContentHeight());
                    int j = getHeight() - this.mWebView.getPaddingTop() - this.mWebView.getPaddingBottom();
                    int k = Math.max(i - j, 0);
                    boolean bool2;
                    if (getScrollY() > 0)
                    {
                        bool2 = bool1;
                        label142: if (getScrollY() - k <= 0)
                            break label186;
                    }
                    label186: for (boolean bool3 = bool1; ; bool3 = false)
                    {
                        if ((paramInt != 8192) || (!bool2))
                            break label192;
                        this.mWebView.scrollBy(0, j);
                        break;
                        bool2 = false;
                        break label142;
                    }
                    label192: if ((paramInt == 4096) && (bool3))
                        this.mWebView.scrollBy(0, -j);
                    else
                        bool1 = false;
                    break;
                }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean performLongClick()
    {
        boolean bool;
        if (this.mWebView.getParent() == null)
            bool = false;
        while (true)
        {
            return bool;
            ScaleGestureDetector localScaleGestureDetector = this.mZoomManager.getScaleGestureDetector();
            if ((localScaleGestureDetector != null) && (localScaleGestureDetector.isInProgress()))
            {
                bool = false;
            }
            else if (this.mSelectingText)
            {
                bool = false;
            }
            else if (this.mWebViewPrivate.super_performLongClick())
            {
                bool = true;
            }
            else
            {
                bool = selectText();
                if (bool)
                    this.mWebView.performHapticFeedback(0);
            }
        }
    }

    int pinLocX(int paramInt)
    {
        if (this.mInOverScrollMode);
        while (true)
        {
            return paramInt;
            paramInt = pinLoc(paramInt, getViewWidth(), computeRealHorizontalScrollRange());
        }
    }

    int pinLocY(int paramInt)
    {
        if (this.mInOverScrollMode);
        while (true)
        {
            return paramInt;
            paramInt = pinLoc(paramInt, getViewHeightWithTitle(), computeRealVerticalScrollRange() + getTitleHeight());
        }
    }

    public void postUrl(String paramString, byte[] paramArrayOfByte)
    {
        if (URLUtil.isNetworkUrl(paramString))
        {
            switchOutDrawHistory();
            WebViewCore.PostUrlData localPostUrlData = new WebViewCore.PostUrlData();
            localPostUrlData.mUrl = paramString;
            localPostUrlData.mPostData = paramArrayOfByte;
            this.mWebViewCore.sendMessage(132, localPostUrlData);
            clearHelpers();
        }
        while (true)
        {
            return;
            loadUrlImpl(paramString);
        }
    }

    @Deprecated
    public void refreshPlugins(boolean paramBoolean)
    {
    }

    public void reload()
    {
        clearHelpers();
        switchOutDrawHistory();
        this.mWebViewCore.sendMessage(102);
    }

    public void removeJavascriptInterface(String paramString)
    {
        if (this.mWebViewCore != null)
        {
            WebViewCore.JSInterfaceData localJSInterfaceData = new WebViewCore.JSInterfaceData();
            localJSInterfaceData.mInterfaceName = paramString;
            this.mWebViewCore.sendMessage(149, localJSInterfaceData);
        }
    }

    void replaceTextfieldText(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4)
    {
        WebViewCore.ReplaceTextData localReplaceTextData = new WebViewCore.ReplaceTextData();
        localReplaceTextData.mReplace = paramString;
        localReplaceTextData.mNewStart = paramInt3;
        localReplaceTextData.mNewEnd = paramInt4;
        this.mTextGeneration = (1 + this.mTextGeneration);
        localReplaceTextData.mTextGeneration = this.mTextGeneration;
        sendBatchableInputMessage(114, paramInt1, paramInt2, localReplaceTextData);
    }

    public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
    {
        boolean bool1;
        if (this.mNativeClass == 0)
            bool1 = false;
        while (true)
        {
            return bool1;
            if (this.mZoomManager.isFixedLengthAnimationInProgress())
            {
                bool1 = false;
            }
            else
            {
                paramRect.offset(paramView.getLeft() - paramView.getScrollX(), paramView.getTop() - paramView.getScrollY());
                Rect localRect = new Rect(viewToContentX(getScrollX()), viewToContentY(getScrollY()), viewToContentX(getScrollX() + getWidth() - this.mWebView.getVerticalScrollbarWidth()), viewToContentY(getScrollY() + getViewHeightWithTitle()));
                int i = contentToViewY(localRect.top);
                int j = contentToViewY(localRect.bottom);
                int k = j - i;
                int m = 0;
                int i4;
                label177: int n;
                int i1;
                int i3;
                if (paramRect.bottom > j)
                {
                    i4 = k / 3;
                    if (paramRect.height() > i4 * 2)
                    {
                        m = paramRect.top - i;
                        n = contentToViewX(localRect.left);
                        i1 = contentToViewX(localRect.right);
                        int i2 = i1 - n;
                        i3 = 0;
                        if ((paramRect.right <= i1) || (paramRect.left <= n))
                            break label328;
                        if (paramRect.width() <= i2)
                            break label314;
                        i3 = 0 + (paramRect.left - n);
                        label247: if ((m | i3) == 0)
                            break label357;
                        if (paramBoolean)
                            break label351;
                    }
                }
                label314: label328: label351: for (boolean bool2 = true; ; bool2 = false)
                {
                    bool1 = pinScrollBy(i3, m, bool2, 0);
                    break;
                    m = paramRect.top - (i + i4);
                    break label177;
                    if (paramRect.top >= i)
                        break label177;
                    m = paramRect.top - i;
                    break label177;
                    i3 = 0 + (paramRect.right - i1);
                    break label247;
                    if (paramRect.left >= n)
                        break label247;
                    i3 = 0 - (n - paramRect.left);
                    break label247;
                }
                label357: bool1 = false;
            }
        }
    }

    public boolean requestFocus(int paramInt, Rect paramRect)
    {
        boolean bool;
        if (this.mFindIsUp)
            bool = false;
        while (true)
        {
            return bool;
            bool = this.mWebViewPrivate.super_requestFocus(paramInt, paramRect);
            if ((this.mWebViewCore.getSettings().getNeedInitialFocus()) && (!this.mWebView.isInTouchMode()))
                switch (paramInt)
                {
                default:
                case 17:
                case 33:
                case 130:
                case 66:
                }
        }
        int i = 21;
        while (true)
        {
            this.mWebViewCore.sendMessage(224, i);
            break;
            i = 19;
            continue;
            i = 20;
            continue;
            i = 22;
        }
    }

    public void requestFocusNodeHref(Message paramMessage)
    {
        if (paramMessage == null);
        while (true)
        {
            return;
            int i = viewToContentX(this.mLastTouchX + getScrollX());
            int j = viewToContentY(this.mLastTouchY + getScrollY());
            if ((this.mFocusedNode != null) && (this.mFocusedNode.mHitTestX == i) && (this.mFocusedNode.mHitTestY == j))
            {
                paramMessage.getData().putString("url", this.mFocusedNode.mLinkUrl);
                paramMessage.getData().putString("title", this.mFocusedNode.mAnchorText);
                paramMessage.getData().putString("src", this.mFocusedNode.mImageUrl);
                paramMessage.sendToTarget();
            }
            else
            {
                this.mWebViewCore.sendMessage(137, i, j, paramMessage);
            }
        }
    }

    void requestFormData(String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mWebViewCore.getSettings().getSaveFormData())
        {
            Message localMessage = this.mPrivateHandler.obtainMessage(6);
            localMessage.arg1 = paramInt;
            new Thread(new RequestFormData(paramString, getUrl(), localMessage, paramBoolean1, paramBoolean2)).start();
        }
    }

    public void requestImageRef(Message paramMessage)
    {
        if (this.mNativeClass == 0)
            return;
        if (this.mFocusedNode != null);
        for (String str = this.mFocusedNode.mImageUrl; ; str = null)
        {
            Bundle localBundle = paramMessage.getData();
            localBundle.putString("url", str);
            paramMessage.setData(localBundle);
            paramMessage.sendToTarget();
            break;
        }
    }

    void requestListBox(String[] paramArrayOfString, int[] paramArrayOfInt, int paramInt)
    {
        this.mPrivateHandler.post(new InvokeListBox(paramArrayOfString, paramArrayOfInt, paramInt, null));
    }

    void requestListBox(String[] paramArrayOfString, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        this.mPrivateHandler.post(new InvokeListBox(paramArrayOfString, paramArrayOfInt1, paramArrayOfInt2, null));
    }

    void resetTrackballTime()
    {
        this.mTrackballLastTime = 0L;
    }

    @Deprecated
    public boolean restorePicture(Bundle paramBundle, File paramFile)
    {
        boolean bool = false;
        if ((paramFile == null) || (paramBundle == null));
        while (true)
        {
            return bool;
            if (!paramFile.exists())
                continue;
            try
            {
                new Thread(new Runnable()
                {
                    // ERROR //
                    public void run()
                    {
                        // Byte code:
                        //     0: aload_0
                        //     1: getfield 25	android/webkit/WebViewClassic$7:val$in	Ljava/io/FileInputStream;
                        //     4: invokestatic 39	android/graphics/Picture:createFromStream	(Ljava/io/InputStream;)Landroid/graphics/Picture;
                        //     7: astore_3
                        //     8: aload_3
                        //     9: ifnull +23 -> 32
                        //     12: aload_0
                        //     13: getfield 23	android/webkit/WebViewClassic$7:this$0	Landroid/webkit/WebViewClassic;
                        //     16: getfield 43	android/webkit/WebViewClassic:mPrivateHandler	Landroid/os/Handler;
                        //     19: new 8	android/webkit/WebViewClassic$7$1
                        //     22: dup
                        //     23: aload_0
                        //     24: aload_3
                        //     25: invokespecial 46	android/webkit/WebViewClassic$7$1:<init>	(Landroid/webkit/WebViewClassic$7;Landroid/graphics/Picture;)V
                        //     28: invokevirtual 52	android/os/Handler:post	(Ljava/lang/Runnable;)Z
                        //     31: pop
                        //     32: aload_0
                        //     33: getfield 25	android/webkit/WebViewClassic$7:val$in	Ljava/io/FileInputStream;
                        //     36: invokevirtual 57	java/io/FileInputStream:close	()V
                        //     39: return
                        //     40: astore_1
                        //     41: aload_0
                        //     42: getfield 25	android/webkit/WebViewClassic$7:val$in	Ljava/io/FileInputStream;
                        //     45: invokevirtual 57	java/io/FileInputStream:close	()V
                        //     48: aload_1
                        //     49: athrow
                        //     50: astore 4
                        //     52: goto -13 -> 39
                        //     55: astore_2
                        //     56: goto -8 -> 48
                        //
                        // Exception table:
                        //     from	to	target	type
                        //     0	32	40	finally
                        //     32	39	50	java/lang/Exception
                        //     41	48	55	java/lang/Exception
                    }
                }).start();
                bool = true;
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                while (true)
                    localFileNotFoundException.printStackTrace();
            }
        }
    }

    // ERROR //
    public WebBackForwardList restoreState(Bundle paramBundle)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aconst_null
        //     3: astore_3
        //     4: aload_1
        //     5: ifnonnull +7 -> 12
        //     8: aconst_null
        //     9: astore_2
        //     10: aload_2
        //     11: areturn
        //     12: aload_1
        //     13: ldc_w 3883
        //     16: invokevirtual 3886	android/os/Bundle:containsKey	(Ljava/lang/String;)Z
        //     19: ifeq +207 -> 226
        //     22: aload_1
        //     23: ldc_w 3888
        //     26: invokevirtual 3886	android/os/Bundle:containsKey	(Ljava/lang/String;)Z
        //     29: ifeq +197 -> 226
        //     32: aload_0
        //     33: aload_1
        //     34: ldc_w 3890
        //     37: invokevirtual 3894	android/os/Bundle:getBundle	(Ljava/lang/String;)Landroid/os/Bundle;
        //     40: invokestatic 3899	android/net/http/SslCertificate:restoreState	(Landroid/os/Bundle;)Landroid/net/http/SslCertificate;
        //     43: putfield 3081	android/webkit/WebViewClassic:mCertificate	Landroid/net/http/SslCertificate;
        //     46: aload_0
        //     47: getfield 1428	android/webkit/WebViewClassic:mCallbackProxy	Landroid/webkit/CallbackProxy;
        //     50: invokevirtual 2892	android/webkit/CallbackProxy:getBackForwardList	()Landroid/webkit/WebBackForwardList;
        //     53: astore 4
        //     55: aload_1
        //     56: ldc_w 3883
        //     59: invokevirtual 3901	android/os/Bundle:getInt	(Ljava/lang/String;)I
        //     62: istore 5
        //     64: aload 4
        //     66: monitorenter
        //     67: aload_1
        //     68: ldc_w 3888
        //     71: invokevirtual 3905	android/os/Bundle:getSerializable	(Ljava/lang/String;)Ljava/io/Serializable;
        //     74: checkcast 3907	java/util/List
        //     77: astore 7
        //     79: aload 7
        //     81: invokeinterface 3910 1 0
        //     86: istore 8
        //     88: iload 5
        //     90: iflt +10 -> 100
        //     93: iload 5
        //     95: iload 8
        //     97: if_icmplt +17 -> 114
        //     100: aload 4
        //     102: monitorexit
        //     103: goto -93 -> 10
        //     106: astore 6
        //     108: aload 4
        //     110: monitorexit
        //     111: aload 6
        //     113: athrow
        //     114: iconst_0
        //     115: istore 9
        //     117: iload 9
        //     119: iload 8
        //     121: if_icmpge +47 -> 168
        //     124: aload 7
        //     126: iconst_0
        //     127: invokeinterface 3911 2 0
        //     132: checkcast 3912	[B
        //     135: astore 10
        //     137: aload 10
        //     139: ifnonnull +9 -> 148
        //     142: aload 4
        //     144: monitorexit
        //     145: goto -135 -> 10
        //     148: aload 4
        //     150: new 3090	android/webkit/WebHistoryItem
        //     153: dup
        //     154: aload 10
        //     156: invokespecial 3915	android/webkit/WebHistoryItem:<init>	([B)V
        //     159: invokevirtual 3919	android/webkit/WebBackForwardList:addHistoryItem	(Landroid/webkit/WebHistoryItem;)V
        //     162: iinc 9 1
        //     165: goto -48 -> 117
        //     168: aload_0
        //     169: invokevirtual 3921	android/webkit/WebViewClassic:copyBackForwardList	()Landroid/webkit/WebBackForwardList;
        //     172: astore_3
        //     173: aload_3
        //     174: iload 5
        //     176: invokevirtual 3924	android/webkit/WebBackForwardList:setCurrentIndex	(I)V
        //     179: aload 4
        //     181: monitorexit
        //     182: aload_1
        //     183: ldc_w 3926
        //     186: invokevirtual 3929	android/os/Bundle:getBoolean	(Ljava/lang/String;)Z
        //     189: ifeq +11 -> 200
        //     192: aload_0
        //     193: invokevirtual 1686	android/webkit/WebViewClassic:getSettings	()Landroid/webkit/WebSettingsClassic;
        //     196: iconst_1
        //     197: invokevirtual 2784	android/webkit/WebSettingsClassic:setPrivateBrowsingEnabled	(Z)V
        //     200: aload_0
        //     201: getfield 970	android/webkit/WebViewClassic:mZoomManager	Landroid/webkit/ZoomManager;
        //     204: aload_1
        //     205: invokevirtual 2459	android/webkit/ZoomManager:restoreZoomState	(Landroid/os/Bundle;)V
        //     208: aload_0
        //     209: getfield 890	android/webkit/WebViewClassic:mWebViewCore	Landroid/webkit/WebViewCore;
        //     212: invokevirtual 3931	android/webkit/WebViewCore:removeMessages	()V
        //     215: aload_0
        //     216: getfield 890	android/webkit/WebViewClassic:mWebViewCore	Landroid/webkit/WebViewCore;
        //     219: bipush 108
        //     221: iload 5
        //     223: invokevirtual 2927	android/webkit/WebViewCore:sendMessage	(II)V
        //     226: aload_3
        //     227: astore_2
        //     228: goto -218 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     67	111	106	finally
        //     124	182	106	finally
    }

    public void resumeTimers()
    {
        this.mWebViewCore.sendMessage(110);
    }

    public void savePassword(String paramString1, String paramString2, String paramString3)
    {
        this.mDatabase.setUsernamePassword(paramString1, paramString2, paramString3);
    }

    @Deprecated
    public boolean savePicture(Bundle paramBundle, final File paramFile)
    {
        if ((paramFile == null) || (paramBundle == null));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            final Picture localPicture = capturePicture();
            new Thread(new Runnable()
            {
                // ERROR //
                public void run()
                {
                    // Byte code:
                    //     0: aconst_null
                    //     1: astore_1
                    //     2: new 36	java/io/FileOutputStream
                    //     5: dup
                    //     6: aload_0
                    //     7: getfield 24	android/webkit/WebViewClassic$6:val$temp	Ljava/io/File;
                    //     10: invokespecial 39	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
                    //     13: astore_2
                    //     14: aload_0
                    //     15: getfield 26	android/webkit/WebViewClassic$6:val$p	Landroid/graphics/Picture;
                    //     18: aload_2
                    //     19: invokevirtual 45	android/graphics/Picture:writeToStream	(Ljava/io/OutputStream;)V
                    //     22: aload_0
                    //     23: getfield 24	android/webkit/WebViewClassic$6:val$temp	Ljava/io/File;
                    //     26: aload_0
                    //     27: getfield 28	android/webkit/WebViewClassic$6:val$dest	Ljava/io/File;
                    //     30: invokevirtual 51	java/io/File:renameTo	(Ljava/io/File;)Z
                    //     33: pop
                    //     34: aload_2
                    //     35: ifnull +7 -> 42
                    //     38: aload_2
                    //     39: invokevirtual 54	java/io/FileOutputStream:close	()V
                    //     42: aload_0
                    //     43: getfield 24	android/webkit/WebViewClassic$6:val$temp	Ljava/io/File;
                    //     46: invokevirtual 58	java/io/File:delete	()Z
                    //     49: pop
                    //     50: return
                    //     51: astore 6
                    //     53: aload_1
                    //     54: ifnull +7 -> 61
                    //     57: aload_1
                    //     58: invokevirtual 54	java/io/FileOutputStream:close	()V
                    //     61: aload_0
                    //     62: getfield 24	android/webkit/WebViewClassic$6:val$temp	Ljava/io/File;
                    //     65: invokevirtual 58	java/io/File:delete	()Z
                    //     68: pop
                    //     69: aload 6
                    //     71: athrow
                    //     72: astore 12
                    //     74: aload_1
                    //     75: ifnull +7 -> 82
                    //     78: aload_1
                    //     79: invokevirtual 54	java/io/FileOutputStream:close	()V
                    //     82: aload_0
                    //     83: getfield 24	android/webkit/WebViewClassic$6:val$temp	Ljava/io/File;
                    //     86: invokevirtual 58	java/io/File:delete	()Z
                    //     89: pop
                    //     90: goto -40 -> 50
                    //     93: astore 5
                    //     95: goto -13 -> 82
                    //     98: astore 8
                    //     100: goto -39 -> 61
                    //     103: astore 11
                    //     105: goto -63 -> 42
                    //     108: astore 6
                    //     110: aload_2
                    //     111: astore_1
                    //     112: goto -59 -> 53
                    //     115: astore_3
                    //     116: aload_2
                    //     117: astore_1
                    //     118: goto -44 -> 74
                    //
                    // Exception table:
                    //     from	to	target	type
                    //     2	14	51	finally
                    //     2	14	72	java/lang/Exception
                    //     78	82	93	java/lang/Exception
                    //     57	61	98	java/lang/Exception
                    //     38	42	103	java/lang/Exception
                    //     14	34	108	finally
                    //     14	34	115	java/lang/Exception
                }
            }).start();
            paramBundle.putInt("scrollX", getScrollX());
            paramBundle.putInt("scrollY", getScrollY());
            this.mZoomManager.saveZoomState(paramBundle);
        }
    }

    public WebBackForwardList saveState(Bundle paramBundle)
    {
        WebBackForwardList localWebBackForwardList;
        if (paramBundle == null)
            localWebBackForwardList = null;
        while (true)
        {
            return localWebBackForwardList;
            localWebBackForwardList = copyBackForwardList();
            int i = localWebBackForwardList.getCurrentIndex();
            int j = localWebBackForwardList.getSize();
            if ((i < 0) || (i >= j) || (j == 0))
            {
                localWebBackForwardList = null;
            }
            else
            {
                paramBundle.putInt("index", i);
                ArrayList localArrayList = new ArrayList(j);
                for (int k = 0; ; k++)
                {
                    if (k >= j)
                        break label131;
                    WebHistoryItem localWebHistoryItem = localWebBackForwardList.getItemAtIndex(k);
                    if (localWebHistoryItem == null)
                    {
                        Log.w("webview", "saveState: Unexpected null history item.");
                        localWebBackForwardList = null;
                        break;
                    }
                    byte[] arrayOfByte = localWebHistoryItem.getFlattenedData();
                    if (arrayOfByte == null)
                    {
                        localWebBackForwardList = null;
                        break;
                    }
                    localArrayList.add(arrayOfByte);
                }
                label131: paramBundle.putSerializable("history", localArrayList);
                if (this.mCertificate != null)
                    paramBundle.putBundle("certificate", SslCertificate.saveState(this.mCertificate));
                paramBundle.putBoolean("privateBrowsingEnabled", isPrivateBrowsingEnabled());
                this.mZoomManager.saveZoomState(paramBundle);
            }
        }
    }

    public void saveViewState(OutputStream paramOutputStream, ValueCallback<Boolean> paramValueCallback)
    {
        if (this.mWebViewCore == null)
            paramValueCallback.onReceiveValue(Boolean.valueOf(false));
        while (true)
        {
            return;
            this.mWebViewCore.sendMessageAtFrontOfQueue(225, new WebViewCore.SaveViewStateRequest(paramOutputStream, paramValueCallback));
        }
    }

    public void saveWebArchive(String paramString)
    {
        saveWebArchiveImpl(paramString, false, null);
    }

    public void saveWebArchive(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback)
    {
        saveWebArchiveImpl(paramString, paramBoolean, paramValueCallback);
    }

    public void selectAll()
    {
        this.mWebViewCore.sendMessage(215);
    }

    public boolean selectText()
    {
        return selectText(viewToContentX(this.mLastTouchX + getScrollX()), viewToContentY(this.mLastTouchY + getScrollY()));
    }

    boolean selectText(int paramInt1, int paramInt2)
    {
        if (this.mWebViewCore == null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mWebViewCore.sendMessage(214, paramInt1, paramInt2);
        }
    }

    void selectionDone()
    {
        if (this.mSelectingText)
        {
            hideFloatView();
            hidePasteButton();
            endSelectingText();
            invalidate();
            this.mAutoScrollX = 0;
            this.mAutoScrollY = 0;
            this.mSentAutoScrollMessage = false;
        }
    }

    void sendBatchableInputMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        if (this.mWebViewCore == null);
        while (true)
        {
            return;
            Message localMessage = Message.obtain(null, paramInt1, paramInt2, paramInt3, paramObject);
            if (this.mIsBatchingTextChanges)
                this.mBatchedTextChanges.add(localMessage);
            else
                this.mWebViewCore.sendMessage(localMessage);
        }
    }

    Rect sendOurVisibleRect()
    {
        Rect localRect;
        if (this.mZoomManager.isPreventingWebkitUpdates())
        {
            localRect = this.mLastVisibleRectSent;
            return localRect;
        }
        calcOurContentVisibleRect(this.mVisibleRect);
        WebViewCore localWebViewCore;
        if (!this.mVisibleRect.equals(this.mLastVisibleRectSent))
            if (!this.mBlockWebkitViewMessages)
            {
                this.mScrollOffset.set(this.mVisibleRect.left, this.mVisibleRect.top);
                this.mWebViewCore.removeMessages(107);
                localWebViewCore = this.mWebViewCore;
                if (!this.mSendScrollEvent)
                    break label187;
            }
        label187: for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(107, i, this.mScrollOffset);
            this.mLastVisibleRectSent.set(this.mVisibleRect);
            this.mPrivateHandler.removeMessages(4);
            if ((this.mWebView.getGlobalVisibleRect(this.mGlobalVisibleRect)) && (!this.mGlobalVisibleRect.equals(this.mLastGlobalRect)))
            {
                if (!this.mBlockWebkitViewMessages)
                    this.mWebViewCore.sendMessage(116, this.mGlobalVisibleRect);
                this.mLastGlobalRect.set(this.mGlobalVisibleRect);
            }
            localRect = this.mVisibleRect;
            break;
        }
    }

    boolean sendViewSizeZoom(boolean paramBoolean)
    {
        boolean bool = false;
        if (this.mBlockWebkitViewMessages);
        while (true)
        {
            return bool;
            if (!this.mZoomManager.isPreventingWebkitUpdates())
            {
                int i = getViewWidth();
                int j = Math.round(i * this.mZoomManager.getInvScale());
                int k = getViewHeightWithTitle() - getTitleHeight();
                int m = Math.round(k * this.mZoomManager.getInvScale());
                float f = k / i;
                if ((j > this.mLastWidthSent) && (this.mWrapContent))
                {
                    m = 0;
                    f = 0.0F;
                }
                int n = Math.round(getViewHeight() * this.mZoomManager.getInvScale());
                if ((j != this.mLastWidthSent) || (m != this.mLastHeightSent) || (paramBoolean) || (n != this.mLastActualHeightSent))
                {
                    ViewSizeData localViewSizeData = new ViewSizeData();
                    localViewSizeData.mWidth = j;
                    localViewSizeData.mHeight = m;
                    localViewSizeData.mHeightWidthRatio = f;
                    localViewSizeData.mActualViewHeight = n;
                    localViewSizeData.mTextWrapWidth = Math.round(i / this.mZoomManager.getTextWrapScale());
                    localViewSizeData.mScale = this.mZoomManager.getScale();
                    if ((this.mZoomManager.isFixedLengthAnimationInProgress()) && (!this.mHeightCanMeasure))
                        bool = true;
                    localViewSizeData.mIgnoreHeight = bool;
                    localViewSizeData.mAnchorX = this.mZoomManager.getDocumentAnchorX();
                    localViewSizeData.mAnchorY = this.mZoomManager.getDocumentAnchorY();
                    this.mWebViewCore.sendMessage(105, localViewSizeData);
                    this.mLastWidthSent = j;
                    this.mLastHeightSent = m;
                    this.mLastActualHeightSent = n;
                    this.mZoomManager.clearDocumentAnchor();
                    bool = true;
                }
            }
        }
    }

    void setActive(boolean paramBoolean)
    {
        if (paramBoolean)
            if (this.mWebView.hasFocus())
            {
                this.mDrawCursorRing = true;
                setFocusControllerActive(true);
            }
        while (true)
        {
            invalidate();
            return;
            this.mDrawCursorRing = false;
            setFocusControllerActive(false);
            continue;
            if (!this.mZoomManager.isZoomPickerVisible())
                this.mDrawCursorRing = false;
            this.mKeysPressed.clear();
            this.mPrivateHandler.removeMessages(4);
            this.mTouchMode = 7;
            setFocusControllerActive(false);
        }
    }

    public void setBackgroundColor(int paramInt)
    {
        this.mBackgroundColor = paramInt;
        this.mWebViewCore.sendMessage(126, paramInt);
    }

    void setBaseLayer(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mNativeClass == 0);
        label23: label70: label78: 
        while (true)
        {
            return;
            int i;
            if (this.mTouchMode == 9)
            {
                i = this.mCurrentScrollingLayerId;
                if (!nativeSetBaseLayer(this.mNativeClass, paramInt, paramBoolean1, paramBoolean2, i))
                    break label70;
                this.mWebViewCore.pauseWebKitDraw();
            }
            while (true)
            {
                if (this.mHTML5VideoViewProxy == null)
                    break label78;
                this.mHTML5VideoViewProxy.setBaseLayer(paramInt);
                break;
                i = 0;
                break label23;
                this.mWebViewCore.resumeWebKitDraw();
            }
        }
    }

    public void setCertificate(SslCertificate paramSslCertificate)
    {
        this.mCertificate = paramSslCertificate;
    }

    public void setDownloadListener(DownloadListener paramDownloadListener)
    {
        this.mCallbackProxy.setDownloadListener(paramDownloadListener);
    }

    public void setFindListener(WebView.FindListener paramFindListener)
    {
        this.mFindListener = paramFindListener;
    }

    void setFocusControllerActive(boolean paramBoolean)
    {
        if (this.mWebViewCore == null)
            return;
        WebViewCore localWebViewCore = this.mWebViewCore;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(142, i, 0);
            if ((!paramBoolean) || (this.mListBoxMessage == null))
                break;
            this.mWebViewCore.sendMessage(this.mListBoxMessage);
            this.mListBoxMessage = null;
            break;
        }
    }

    public boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool = this.mWebViewPrivate.super_setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
        if ((!bool) && (this.mHeightCanMeasure))
            sendViewSizeZoom(false);
        updateRectsForGL();
        return bool;
    }

    public void setHTML5VideoViewProxy(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        this.mHTML5VideoViewProxy = paramHTML5VideoViewProxy;
    }

    public void setHorizontalScrollbarOverlay(boolean paramBoolean)
    {
        this.mOverlayHorizontalScrollbar = paramBoolean;
    }

    public void setHttpAuthUsernamePassword(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        this.mDatabase.setHttpAuthUsernamePassword(paramString1, paramString2, paramString3, paramString4);
    }

    public void setInitialScale(int paramInt)
    {
        this.mZoomManager.setInitialScaleInPercent(paramInt);
    }

    public void setJsFlags(String paramString)
    {
        this.mWebViewCore.sendMessage(174, paramString);
    }

    public void setLayerType(int paramInt, Paint paramPaint)
    {
        updateHwAccelerated();
    }

    public void setLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        if (paramLayoutParams.height == -2)
            this.mWrapContent = true;
        this.mWebViewPrivate.super_setLayoutParams(paramLayoutParams);
    }

    public void setMapTrackballToArrowKeys(boolean paramBoolean)
    {
        this.mMapTrackballToArrowKeys = paramBoolean;
    }

    public void setMockDeviceOrientation(boolean paramBoolean1, double paramDouble1, boolean paramBoolean2, double paramDouble2, boolean paramBoolean3, double paramDouble3)
    {
        this.mWebViewCore.setMockDeviceOrientation(paramBoolean1, paramDouble1, paramBoolean2, paramDouble2, paramBoolean3, paramDouble3);
    }

    public void setNetworkAvailable(boolean paramBoolean)
    {
        WebViewCore localWebViewCore = this.mWebViewCore;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localWebViewCore.sendMessage(119, i, 0);
            return;
        }
    }

    public void setNetworkType(String paramString1, String paramString2)
    {
        HashMap localHashMap = new HashMap();
        localHashMap.put("type", paramString1);
        localHashMap.put("subtype", paramString2);
        this.mWebViewCore.sendMessage(183, localHashMap);
    }

    void setNewPicture(WebViewCore.DrawData paramDrawData, boolean paramBoolean)
    {
        boolean bool1 = false;
        if (this.mNativeClass == 0)
        {
            if (this.mDelaySetPicture != null)
                throw new IllegalStateException("Tried to setNewPicture with a delay picture already set! (memory leak)");
            this.mDelaySetPicture = paramDrawData;
            return;
        }
        WebViewCore.ViewState localViewState = paramDrawData.mViewState;
        boolean bool2;
        label47: boolean bool3;
        if (localViewState != null)
        {
            bool2 = true;
            if (paramBoolean)
                setBaseLayer(paramDrawData.mBaseLayer, getSettings().getShowVisualIndicator(), bool2);
            Point localPoint = paramDrawData.mViewSize;
            if ((localPoint.x != this.mLastWidthSent) || (localPoint.y != this.mLastHeightSent))
                break label362;
            bool3 = true;
            label101: this.mSendScrollEvent = false;
            recordNewContentSize(paramDrawData.mContentSize.x, paramDrawData.mContentSize.y, bool3);
            if (bool2)
            {
                this.mLastWidthSent = 0;
                this.mZoomManager.onFirstLayout(paramDrawData);
                if (!localViewState.mShouldStartScrolledRight)
                    break label368;
            }
        }
        label362: label368: for (int j = getContentWidth(); ; j = localViewState.mScrollX)
        {
            contentScrollTo(j, localViewState.mScrollY, false);
            if (!this.mDrawHistory)
                hideSoftKeyboard();
            this.mSendScrollEvent = true;
            int i = 0;
            boolean bool4 = bool2;
            ViewRootImpl localViewRootImpl = this.mWebView.getViewRootImpl();
            if ((this.mWebView.isHardwareAccelerated()) && (localViewRootImpl != null))
            {
                i = nativeGetDrawGLFunction(this.mNativeClass);
                if (i != 0)
                {
                    if (!localViewRootImpl.attachFunctor(i))
                        bool1 = true;
                    bool4 |= bool1;
                }
            }
            if ((i == 0) || (bool4) || (this.mWebView.getLayerType() != 0))
                this.mWebView.invalidate();
            if (this.mZoomManager.onNewPicture(paramDrawData))
                invalidate();
            if (bool2)
                this.mViewManager.postReadyToDrawAll();
            scrollEditWithCursor();
            if ((this.mPictureListener == null) || ((this.mWebView.isHardwareAccelerated()) && (this.mWebView.getLayerType() != 1)))
                break;
            this.mPictureListener.onNewPicture(getWebView(), capturePicture());
            break;
            bool2 = false;
            break label47;
            bool3 = false;
            break label101;
        }
    }

    public void setOverScrollMode(int paramInt)
    {
        if (paramInt != 2)
            if (this.mOverScrollGlow != null);
        for (this.mOverScrollGlow = new OverScrollGlow(this); ; this.mOverScrollGlow = null)
            return;
    }

    @Deprecated
    public void setPictureListener(WebView.PictureListener paramPictureListener)
    {
        this.mPictureListener = paramPictureListener;
    }

    public void setScrollBarStyle(int paramInt)
    {
        if ((paramInt == 16777216) || (paramInt == 50331648))
            this.mOverlayVerticalScrollbar = false;
        for (this.mOverlayHorizontalScrollbar = false; ; this.mOverlayHorizontalScrollbar = true)
        {
            return;
            this.mOverlayVerticalScrollbar = true;
        }
    }

    void setScrollXRaw(int paramInt)
    {
        this.mWebViewPrivate.setScrollXRaw(paramInt);
    }

    void setScrollYRaw(int paramInt)
    {
        this.mWebViewPrivate.setScrollYRaw(paramInt);
    }

    void setSelection(int paramInt1, int paramInt2)
    {
        if (this.mWebViewCore != null)
            this.mWebViewCore.sendMessage(113, paramInt1, paramInt2);
    }

    public void setTouchInterval(int paramInt)
    {
        this.mCurrentTouchInterval = paramInt;
    }

    public void setUseMockDeviceOrientation()
    {
        this.mWebViewCore.sendMessage(191);
    }

    public void setVerticalScrollbarOverlay(boolean paramBoolean)
    {
        this.mOverlayVerticalScrollbar = paramBoolean;
    }

    public void setWebBackForwardListClient(WebBackForwardListClient paramWebBackForwardListClient)
    {
        this.mCallbackProxy.setWebBackForwardListClient(paramWebBackForwardListClient);
    }

    public void setWebChromeClient(WebChromeClient paramWebChromeClient)
    {
        this.mCallbackProxy.setWebChromeClient(paramWebChromeClient);
    }

    public void setWebViewClient(WebViewClient paramWebViewClient)
    {
        this.mCallbackProxy.setWebViewClient(paramWebViewClient);
    }

    public boolean shouldDelayChildPressedState()
    {
        return true;
    }

    public boolean showFindDialog(String paramString, boolean paramBoolean)
    {
        boolean bool = true;
        FindActionModeCallback localFindActionModeCallback = new FindActionModeCallback(this.mContext);
        if ((this.mWebView.getParent() == null) || (this.mWebView.startActionMode(localFindActionModeCallback) == null))
            bool = false;
        label134: label143: 
        while (true)
        {
            return bool;
            this.mCachedOverlappingActionModeHeight = -1;
            this.mFindCallback = localFindActionModeCallback;
            setFindIsUp(bool);
            this.mFindCallback.setWebView(this);
            if (paramBoolean)
            {
                this.mFindCallback.showSoftInput();
                label77: if (paramString == null)
                    if (this.mFindRequest != null)
                        break label134;
            }
            for (paramString = null; ; paramString = this.mFindRequest.mSearchText)
            {
                if (paramString == null)
                    break label143;
                this.mFindCallback.setText(paramString);
                this.mFindCallback.findAll();
                break;
                if (paramString == null)
                    break label77;
                this.mFindCallback.setText(paramString);
                this.mFindCallback.findAll();
                break;
            }
        }
    }

    public void stopLoading()
    {
        switchOutDrawHistory();
        this.mWebViewCore.sendMessage(101);
    }

    public void stopScroll()
    {
        this.mScroller.forceFinished(true);
        this.mLastVelocity = 0.0F;
    }

    void switchOutDrawHistory()
    {
        if (this.mWebViewCore == null);
        while (true)
        {
            return;
            if ((this.mDrawHistory) && ((getProgress() == 100) || (nativeHasContent())))
            {
                this.mDrawHistory = false;
                this.mHistoryPicture = null;
                invalidate();
                int i = getScrollX();
                int j = getScrollY();
                setScrollXRaw(pinLocX(getScrollX()));
                setScrollYRaw(pinLocY(getScrollY()));
                if ((i != getScrollX()) || (j != getScrollY()))
                    this.mWebViewPrivate.onScrollChanged(getScrollX(), getScrollY(), i, j);
                else
                    sendOurVisibleRect();
            }
        }
    }

    public void tileProfilingClear()
    {
        nativeTileProfilingClear();
    }

    public float tileProfilingGetFloat(int paramInt1, int paramInt2, String paramString)
    {
        return nativeTileProfilingGetFloat(paramInt1, paramInt2, paramString);
    }

    public int tileProfilingGetInt(int paramInt1, int paramInt2, String paramString)
    {
        return nativeTileProfilingGetInt(paramInt1, paramInt2, paramString);
    }

    public int tileProfilingNumFrames()
    {
        return nativeTileProfilingNumFrames();
    }

    public int tileProfilingNumTilesInFrame(int paramInt)
    {
        return nativeTileProfilingNumTilesInFrame(paramInt);
    }

    public void tileProfilingStart()
    {
        nativeTileProfilingStart();
    }

    public float tileProfilingStop()
    {
        return nativeTileProfilingStop();
    }

    void updateDefaultZoomDensity(float paramFloat)
    {
        this.mNavSlop = ((int)(16.0F * paramFloat));
        this.mZoomManager.updateDefaultZoomDensity(paramFloat);
    }

    void updateDoubleTapZoom(int paramInt)
    {
        this.mZoomManager.updateDoubleTapZoom(paramInt);
    }

    void updateDrawingState()
    {
        if ((this.mNativeClass == 0) || (this.mIsPaused));
        while (true)
        {
            return;
            if (this.mWebView.getWindowVisibility() != 0)
                nativeSetPauseDrawing(this.mNativeClass, true);
            else if (this.mWebView.getVisibility() != 0)
                nativeSetPauseDrawing(this.mNativeClass, true);
            else
                nativeSetPauseDrawing(this.mNativeClass, false);
        }
    }

    void updateMultiTouchSupport(Context paramContext)
    {
        this.mZoomManager.updateMultiTouchSupport(paramContext);
    }

    void updateRectsForGL()
    {
        boolean bool = this.mWebView.getGlobalVisibleRect(this.mTempVisibleRect, this.mTempVisibleRectOffset);
        this.mInvScreenRect.set(this.mTempVisibleRect);
        int i;
        Rect localRect1;
        if (bool)
        {
            int j = this.mWebView.getRootView().getHeight();
            this.mScreenRect.set(this.mInvScreenRect);
            int k = this.mInvScreenRect.bottom;
            this.mInvScreenRect.bottom = (j - this.mInvScreenRect.top - getVisibleTitleHeightImpl());
            this.mInvScreenRect.top = (j - k);
            this.mIsWebViewVisible = true;
            this.mTempVisibleRect.offset(-this.mTempVisibleRectOffset.x, -this.mTempVisibleRectOffset.y);
            viewToContentVisibleRect(this.mVisibleContentRect, this.mTempVisibleRect);
            i = this.mNativeClass;
            if (!this.mIsWebViewVisible)
                break label192;
            localRect1 = this.mInvScreenRect;
            label154: if (!this.mIsWebViewVisible)
                break label197;
        }
        label192: label197: for (Rect localRect2 = this.mScreenRect; ; localRect2 = null)
        {
            nativeUpdateDrawGLFunction(i, localRect1, localRect2, this.mVisibleContentRect, getScale());
            return;
            this.mIsWebViewVisible = false;
            break;
            localRect1 = null;
            break label154;
        }
    }

    boolean updateScrollCoordinates(int paramInt1, int paramInt2)
    {
        int i = getScrollX();
        int j = getScrollY();
        setScrollXRaw(paramInt1);
        setScrollYRaw(paramInt2);
        if ((i != getScrollX()) || (j != getScrollY()))
            this.mWebViewPrivate.onScrollChanged(getScrollX(), getScrollY(), i, j);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    int viewToContentX(int paramInt)
    {
        return viewToContentDimension(paramInt);
    }

    int viewToContentY(int paramInt)
    {
        return viewToContentDimension(paramInt - getTitleHeight());
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void webSearchText()
    {
        String str = getSelection();
        endSelectingText();
        invalidate();
        Intent localIntent = new Intent("android.intent.action.WEB_SEARCH");
        localIntent.putExtra("new_search", true);
        localIntent.putExtra("query", str);
        getContext().startActivity(localIntent);
    }

    public boolean zoomIn()
    {
        return this.mZoomManager.zoomIn();
    }

    public boolean zoomOut()
    {
        return this.mZoomManager.zoomOut();
    }

    private class InvokeListBox
        implements Runnable
    {
        private Container[] mContainers;
        private boolean mMultiple;
        private int[] mSelectedArray;
        private int mSelection;

        private InvokeListBox(String[] paramArrayOfInt, int[] paramInt, int arg4)
        {
            int i;
            this.mSelection = i;
            this.mMultiple = false;
            int j = paramArrayOfInt.length;
            this.mContainers = new Container[j];
            for (int k = 0; k < j; k++)
            {
                this.mContainers[k] = new Container(null);
                this.mContainers[k].mString = paramArrayOfInt[k];
                this.mContainers[k].mEnabled = paramInt[k];
                this.mContainers[k].mId = k;
            }
        }

        private InvokeListBox(String[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] arg4)
        {
            this.mMultiple = true;
            Object localObject;
            this.mSelectedArray = localObject;
            int i = paramArrayOfInt1.length;
            this.mContainers = new Container[i];
            for (int j = 0; j < i; j++)
            {
                this.mContainers[j] = new Container(null);
                this.mContainers[j].mString = paramArrayOfInt1[j];
                this.mContainers[j].mEnabled = paramArrayOfInt2[j];
                this.mContainers[j].mId = j;
            }
        }

        public void run()
        {
            if ((WebViewClassic.this.mWebViewCore == null) || (WebViewClassic.this.getWebView().getWindowToken() == null) || (WebViewClassic.this.getWebView().getViewRootImpl() == null));
            while (true)
            {
                return;
                final ListView localListView = (ListView)LayoutInflater.from(WebViewClassic.this.mContext).inflate(17367204, null);
                final MyArrayListAdapter localMyArrayListAdapter = new MyArrayListAdapter();
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(WebViewClassic.this.mContext).setView(localListView).setCancelable(true).setInverseBackgroundForced(true);
                if (this.mMultiple)
                {
                    localBuilder.setPositiveButton(17039370, new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                        {
                            WebViewClassic.this.mWebViewCore.sendMessage(123, localMyArrayListAdapter.getCount(), 0, localListView.getCheckedItemPositions());
                        }
                    });
                    localBuilder.setNegativeButton(17039360, new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                        {
                            WebViewClassic.this.mWebViewCore.sendMessage(124, -2, 0);
                        }
                    });
                }
                WebViewClassic.access$8102(WebViewClassic.this, localBuilder.create());
                localListView.setAdapter(localMyArrayListAdapter);
                localListView.setFocusableInTouchMode(true);
                if (!this.mMultiple);
                for (boolean bool = true; ; bool = false)
                {
                    localListView.setTextFilterEnabled(bool);
                    if (!this.mMultiple)
                        break;
                    localListView.setChoiceMode(2);
                    int i = this.mSelectedArray.length;
                    for (int j = 0; j < i; j++)
                        localListView.setItemChecked(this.mSelectedArray[j], true);
                }
                localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                    {
                        WebViewClassic.access$8202(WebViewClassic.this, Message.obtain(null, 124, (int)paramAnonymousLong, 0));
                        if (WebViewClassic.this.mListBoxDialog != null)
                        {
                            WebViewClassic.this.mListBoxDialog.dismiss();
                            WebViewClassic.access$8102(WebViewClassic.this, null);
                        }
                    }
                });
                if (this.mSelection != -1)
                {
                    localListView.setSelection(this.mSelection);
                    localListView.setChoiceMode(1);
                    localListView.setItemChecked(this.mSelection, true);
                    localMyArrayListAdapter.registerDataSetObserver(new SingleDataSetObserver(localMyArrayListAdapter.getItemId(this.mSelection), localListView, localMyArrayListAdapter));
                }
                WebViewClassic.this.mListBoxDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
                {
                    public void onCancel(DialogInterface paramAnonymousDialogInterface)
                    {
                        WebViewClassic.this.mWebViewCore.sendMessage(124, -2, 0);
                        WebViewClassic.access$8102(WebViewClassic.this, null);
                    }
                });
                WebViewClassic.this.mListBoxDialog.show();
            }
        }

        private class SingleDataSetObserver extends DataSetObserver
        {
            private Adapter mAdapter;
            private long mCheckedId;
            private ListView mListView;

            public SingleDataSetObserver(long arg2, ListView paramAdapter, Adapter arg5)
            {
                this.mCheckedId = ???;
                this.mListView = paramAdapter;
                Object localObject;
                this.mAdapter = localObject;
            }

            public void onChanged()
            {
                int i = this.mListView.getCheckedItemPosition();
                long l = this.mAdapter.getItemId(i);
                int j;
                if (this.mCheckedId != l)
                {
                    this.mListView.clearChoices();
                    j = this.mAdapter.getCount();
                }
                for (int k = 0; ; k++)
                    if (k < j)
                    {
                        if (this.mAdapter.getItemId(k) == this.mCheckedId)
                            this.mListView.setItemChecked(k, true);
                    }
                    else
                        return;
            }
        }

        private class MyArrayListAdapter extends ArrayAdapter<WebViewClassic.InvokeListBox.Container>
        {
            public MyArrayListAdapter()
            {
            }

            private WebViewClassic.InvokeListBox.Container item(int paramInt)
            {
                if ((paramInt < 0) || (paramInt >= getCount()));
                for (WebViewClassic.InvokeListBox.Container localContainer = null; ; localContainer = (WebViewClassic.InvokeListBox.Container)getItem(paramInt))
                    return localContainer;
            }

            public boolean areAllItemsEnabled()
            {
                return false;
            }

            public long getItemId(int paramInt)
            {
                WebViewClassic.InvokeListBox.Container localContainer = item(paramInt);
                if (localContainer == null);
                for (long l = -1L; ; l = localContainer.mId)
                    return l;
            }

            public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
            {
                View localView1 = super.getView(paramInt, null, paramViewGroup);
                WebViewClassic.InvokeListBox.Container localContainer = item(paramInt);
                Object localObject;
                if ((localContainer != null) && (1 != localContainer.mEnabled))
                {
                    localObject = new LinearLayout(WebViewClassic.this.mContext);
                    ((LinearLayout)localObject).setOrientation(1);
                    if (paramInt > 0)
                    {
                        View localView2 = new View(WebViewClassic.this.mContext);
                        localView2.setBackgroundResource(17301522);
                        ((LinearLayout)localObject).addView(localView2);
                    }
                    if (-1 == localContainer.mEnabled)
                    {
                        if (WebViewClassic.InvokeListBox.this.mMultiple)
                        {
                            Assert.assertTrue(localView1 instanceof CheckedTextView);
                            ((CheckedTextView)localView1).setCheckMarkDrawable(null);
                        }
                        ((LinearLayout)localObject).addView(localView1);
                        if (paramInt < -1 + getCount())
                        {
                            View localView3 = new View(WebViewClassic.this.mContext);
                            localView3.setBackgroundResource(17301522);
                            ((LinearLayout)localObject).addView(localView3);
                        }
                    }
                }
                while (true)
                {
                    return localObject;
                    localView1.setEnabled(false);
                    break;
                    localObject = localView1;
                }
            }

            public boolean hasStableIds()
            {
                return false;
            }

            public boolean isEnabled(int paramInt)
            {
                int i = 1;
                boolean bool = false;
                WebViewClassic.InvokeListBox.Container localContainer = item(paramInt);
                if (localContainer == null)
                    return bool;
                if (i == localContainer.mEnabled);
                while (true)
                {
                    bool = i;
                    break;
                    i = 0;
                }
            }
        }

        private class Container
        {
            static final int OPTGROUP = -1;
            static final int OPTION_DISABLED = 0;
            static final int OPTION_ENABLED = 1;
            int mEnabled;
            int mId;
            String mString;

            private Container()
            {
            }

            public String toString()
            {
                return this.mString;
            }
        }
    }

    public static abstract interface PageSwapDelegate
    {
        public abstract void onPageSwapOccurred(boolean paramBoolean);
    }

    static class FocusTransitionDrawable extends Drawable
    {
        int mMaxAlpha;
        Region mNewRegion;
        Paint mPaint;
        Region mPreviousRegion;
        float mProgress = 0.0F;
        Point mTranslate;
        WebViewClassic mWebView;

        public FocusTransitionDrawable(WebViewClassic paramWebViewClassic)
        {
            this.mWebView = paramWebViewClassic;
            this.mPaint = new Paint(this.mWebView.mTouchHightlightPaint);
            this.mMaxAlpha = this.mPaint.getAlpha();
        }

        public void draw(Canvas paramCanvas)
        {
            if (this.mTranslate == null)
            {
                Rect localRect3 = this.mPreviousRegion.getBounds();
                Point localPoint1 = new Point(localRect3.centerX(), localRect3.centerY());
                this.mNewRegion.getBounds(localRect3);
                Point localPoint2 = new Point(localRect3.centerX(), localRect3.centerY());
                this.mTranslate = new Point(localPoint1.x - localPoint2.x, localPoint1.y - localPoint2.y);
            }
            int i = (int)(this.mProgress * this.mMaxAlpha);
            RegionIterator localRegionIterator1 = new RegionIterator(this.mPreviousRegion);
            Rect localRect1 = new Rect();
            this.mPaint.setAlpha(this.mMaxAlpha - i);
            float f1 = this.mTranslate.x * this.mProgress;
            float f2 = this.mTranslate.y * this.mProgress;
            int j = paramCanvas.save(1);
            paramCanvas.translate(-f1, -f2);
            while (localRegionIterator1.next(localRect1))
                paramCanvas.drawRect(localRect1, this.mPaint);
            paramCanvas.restoreToCount(j);
            RegionIterator localRegionIterator2 = new RegionIterator(this.mNewRegion);
            Rect localRect2 = new Rect();
            this.mPaint.setAlpha(i);
            int k = paramCanvas.save(1);
            paramCanvas.translate(this.mTranslate.x - f1, this.mTranslate.y - f2);
            while (localRegionIterator2.next(localRect2))
                paramCanvas.drawRect(localRect2, this.mPaint);
            paramCanvas.restoreToCount(k);
        }

        public int getOpacity()
        {
            return 0;
        }

        public float getProgress()
        {
            return this.mProgress;
        }

        public void setAlpha(int paramInt)
        {
        }

        public void setColorFilter(ColorFilter paramColorFilter)
        {
        }

        public void setProgress(float paramFloat)
        {
            this.mProgress = paramFloat;
            if (this.mWebView.mFocusTransition == this)
            {
                if (this.mProgress == 1.0F)
                    WebViewClassic.access$7702(this.mWebView, null);
                this.mWebView.invalidate();
            }
        }
    }

    class PrivateHandler extends Handler
        implements WebViewInputDispatcher.UiCallbacks
    {
        PrivateHandler()
        {
        }

        public void clearPreviousHitTest()
        {
            WebViewClassic.this.setHitTestResult(null);
        }

        public void dispatchUiEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
        {
            WebViewClassic.this.onHandleUiEvent(paramMotionEvent, paramInt1, paramInt2);
        }

        public Context getContext()
        {
            return WebViewClassic.this.getContext();
        }

        public Looper getUiLooper()
        {
            return getLooper();
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public void handleMessage(Message paramMessage)
        {
            if (WebViewClassic.this.mWebViewCore == null);
            while (true)
            {
                return;
                if ((!WebViewClassic.this.mBlockWebkitViewMessages) || (paramMessage.what == 107))
                {
                    WebViewClassic localWebViewClassic1;
                    switch (paramMessage.what)
                    {
                    default:
                        super.handleMessage(paramMessage);
                        break;
                    case 5001:
                        String[] arrayOfString2 = WebViewClassic.this.mWebViewCore.getReadModeString();
                        WebViewClassic.this.getWebViewClient().onReadModeDataReady(arrayOfString2[0], arrayOfString2[1], arrayOfString2[2], WebViewClassic.this.mWebView);
                        break;
                    case 1:
                        WebViewClassic.this.mDatabase.setUsernamePassword(paramMessage.getData().getString("host"), paramMessage.getData().getString("username"), paramMessage.getData().getString("password"));
                        ((Message)paramMessage.obj).sendToTarget();
                        break;
                    case 2:
                        WebViewClassic.this.mDatabase.setUsernamePassword(paramMessage.getData().getString("host"), null, null);
                        ((Message)paramMessage.obj).sendToTarget();
                        break;
                    case 11:
                        if ((WebViewClassic.this.mAutoScrollX == 0) && (WebViewClassic.this.mAutoScrollY == 0))
                        {
                            WebViewClassic.access$2902(WebViewClassic.this, false);
                        }
                        else
                        {
                            if (WebViewClassic.this.mCurrentScrollingLayerId == 0)
                                WebViewClassic.this.pinScrollBy(WebViewClassic.this.mAutoScrollX, WebViewClassic.this.mAutoScrollY, true, 0);
                            while (true)
                            {
                                sendEmptyMessageDelayed(11, 16L);
                                break;
                                WebViewClassic.this.scrollLayerTo(WebViewClassic.this.mScrollingLayerRect.left + WebViewClassic.this.mAutoScrollX, WebViewClassic.this.mScrollingLayerRect.top + WebViewClassic.this.mAutoScrollY);
                            }
                        }
                        break;
                    case 101:
                        if (paramMessage.arg2 == 1)
                        {
                            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
                            if ((localInputMethodManager == null) || (!localInputMethodManager.isAcceptingText()) || (!localInputMethodManager.isActive(WebViewClassic.this.mWebView)));
                        }
                        else
                        {
                            Point localPoint = (Point)paramMessage.obj;
                            WebViewClassic localWebViewClassic2 = WebViewClassic.this;
                            int i15 = localPoint.x;
                            int i16 = localPoint.y;
                            if (paramMessage.arg1 == 1);
                            for (boolean bool4 = true; ; bool4 = false)
                            {
                                localWebViewClassic2.contentScrollTo(i15, i16, bool4);
                                break;
                            }
                        }
                        break;
                    case 109:
                        WebViewCore.ViewState localViewState = (WebViewCore.ViewState)paramMessage.obj;
                        WebViewClassic.this.mZoomManager.updateZoomRange(localViewState, WebViewClassic.this.getViewWidth(), localViewState.mScrollX);
                        break;
                    case 139:
                        float f = ((Float)paramMessage.obj).floatValue();
                        WebViewClassic.this.mZoomManager.updateDefaultZoomDensity(f);
                        break;
                    case 105:
                        WebViewCore.DrawData localDrawData = (WebViewCore.DrawData)paramMessage.obj;
                        WebViewClassic.this.setNewPicture(localDrawData, true);
                        break;
                    case 107:
                        String str4 = BrowserFrame.getRawResFilename(3, WebViewClassic.this.mContext);
                        Display localDisplay = ((WindowManager)WebViewClassic.this.mContext.getSystemService("window")).getDefaultDisplay();
                        WebViewClassic.this.nativeCreate(paramMessage.arg1, str4, ActivityManager.isHighEndGfx(localDisplay));
                        if (WebViewClassic.this.mDelaySetPicture != null)
                        {
                            WebViewClassic.this.setNewPicture(WebViewClassic.this.mDelaySetPicture, true);
                            WebViewClassic.access$3702(WebViewClassic.this, null);
                        }
                        if (WebViewClassic.this.mIsPaused)
                            WebViewClassic.nativeSetPauseDrawing(WebViewClassic.this.mNativeClass, true);
                        WebViewClassic.access$4102(WebViewClassic.this, new WebViewInputDispatcher(this, WebViewClassic.this.mWebViewCore.getInputDispatcherCallbacks()));
                        break;
                    case 108:
                        if (paramMessage.arg2 == WebViewClassic.this.mTextGeneration)
                        {
                            String str3 = (String)paramMessage.obj;
                            if (str3 == null)
                                str3 = "";
                            if ((WebViewClassic.this.mInputConnection != null) && (WebViewClassic.this.mFieldPointer == paramMessage.arg1))
                                WebViewClassic.this.mInputConnection.setTextAndKeepSelection(str3);
                        }
                        break;
                    case 112:
                        WebViewClassic.this.updateTextSelectionFromMessage(paramMessage.arg1, paramMessage.arg2, (WebViewCore.TextSelectionData)paramMessage.obj);
                        break;
                    case 110:
                        int i14 = paramMessage.arg1;
                        View localView2 = WebViewClassic.this.mWebView.focusSearch(i14);
                        if ((localView2 != null) && (localView2 != WebViewClassic.this.mWebView))
                            localView2.requestFocus();
                        break;
                    case 111:
                        WebViewClassic.this.hideSoftKeyboard();
                        break;
                    case 117:
                        Rect localRect = (Rect)paramMessage.obj;
                        if (localRect == null)
                            WebViewClassic.this.invalidate();
                        else
                            WebViewClassic.this.viewInvalidate(localRect.left, localRect.top, localRect.right, localRect.bottom);
                        break;
                    case 6:
                        if (WebViewClassic.this.mFieldPointer == paramMessage.arg1)
                        {
                            ArrayAdapter localArrayAdapter = (ArrayAdapter)paramMessage.obj;
                            WebViewClassic.this.mAutoCompletePopup.setAdapter(localArrayAdapter);
                        }
                        break;
                    case 114:
                        WebViewClassic.access$4502(WebViewClassic.this, false);
                        WebViewClassic.access$4602(WebViewClassic.this, false);
                        WebViewClassic.this.mWebView.performLongClick();
                        break;
                    case 116:
                        WebViewInputDispatcher localWebViewInputDispatcher = WebViewClassic.this.mInputDispatcher;
                        if (paramMessage.arg1 != 0);
                        for (boolean bool3 = true; ; bool3 = false)
                        {
                            localWebViewInputDispatcher.setWebKitWantsTouchEvents(bool3);
                            break;
                        }
                    case 118:
                        if (paramMessage.arg1 == 0)
                            WebViewClassic.this.hideSoftKeyboard();
                        else
                            WebViewClassic.this.displaySoftKeyboard(false);
                        break;
                    case 8:
                        WebViewClassic.access$4802(WebViewClassic.this, 2);
                        WebViewClassic.this.invalidate();
                        break;
                    case 136:
                        WebView localWebView = WebViewClassic.this.mWebView;
                        if (paramMessage.arg1 == 1);
                        for (boolean bool2 = true; ; bool2 = false)
                        {
                            localWebView.setKeepScreenOn(bool2);
                            break;
                        }
                    case 137:
                        int i13 = paramMessage.arg1;
                        String str2 = (String)paramMessage.obj;
                        if (WebViewClassic.this.mHTML5VideoViewProxy != null)
                            WebViewClassic.this.mHTML5VideoViewProxy.enterFullScreenVideo(i13, str2);
                        break;
                    case 140:
                        if (WebViewClassic.this.mHTML5VideoViewProxy != null)
                            WebViewClassic.this.mHTML5VideoViewProxy.exitFullScreenVideo();
                        break;
                    case 120:
                        View localView1 = (View)paramMessage.obj;
                        int i11 = paramMessage.arg1;
                        int i12 = paramMessage.arg2;
                        if (WebViewClassic.this.inFullScreenMode())
                        {
                            Log.w("webview", "Should not have another full screen.");
                            WebViewClassic.this.dismissFullScreenMode();
                        }
                        WebViewClassic.this.mFullScreenHolder = new PluginFullScreenHolder(WebViewClassic.this, i11, i12);
                        WebViewClassic.this.mFullScreenHolder.setContentView(localView1);
                        WebViewClassic.this.mFullScreenHolder.show();
                        WebViewClassic.this.invalidate();
                        break;
                    case 121:
                        WebViewClassic.this.dismissFullScreenMode();
                        break;
                    case 113:
                        WebViewCore.ShowRectData localShowRectData = (WebViewCore.ShowRectData)paramMessage.obj;
                        int i1 = WebViewClassic.this.contentToViewX(localShowRectData.mLeft);
                        int i2 = WebViewClassic.this.contentToViewDimension(localShowRectData.mWidth);
                        int i3 = WebViewClassic.this.contentToViewDimension(localShowRectData.mContentWidth);
                        int i4 = WebViewClassic.this.getViewWidth();
                        int i5 = Math.max(0, Math.min(i3, i4 + (int)(i1 + localShowRectData.mXPercentInDoc * i2 - localShowRectData.mXPercentInView * i4)) - i4);
                        int i6 = WebViewClassic.this.contentToViewY(localShowRectData.mTop);
                        int i7 = WebViewClassic.this.contentToViewDimension(localShowRectData.mHeight);
                        int i8 = WebViewClassic.this.contentToViewDimension(localShowRectData.mContentHeight);
                        int i9 = WebViewClassic.this.getViewHeight();
                        int i10 = Math.max(0, Math.max(0, Math.min(i8, i9 + (int)(i6 + localShowRectData.mYPercentInDoc * i7 - localShowRectData.mYPercentInView * i9)) - i9) - WebViewClassic.this.getVisibleTitleHeightImpl());
                        WebViewClassic.this.mWebView.scrollTo(i5, i10);
                        break;
                    case 127:
                        WebViewClassic.this.centerFitRect((Rect)paramMessage.obj);
                        break;
                    case 129:
                        WebViewClassic.access$5302(WebViewClassic.this, paramMessage.arg1);
                        WebViewClassic.access$5402(WebViewClassic.this, paramMessage.arg2);
                        break;
                    case 130:
                        if (WebViewClassic.this.isAccessibilityEnabled())
                            WebViewClassic.this.getAccessibilityInjector().handleSelectionChangedIfNecessary((String)paramMessage.obj);
                        break;
                    case 147:
                        localWebViewClassic1 = WebViewClassic.this;
                        if (paramMessage.arg1 != WebViewClassic.this.mFieldPointer);
                    case 131:
                        for (boolean bool1 = true; ; bool1 = false)
                        {
                            localWebViewClassic1.mIsEditingText = bool1;
                            if ((WebViewClassic.this.mAutoCompletePopup != null) && (!WebViewClassic.this.mIsEditingText))
                                WebViewClassic.this.mAutoCompletePopup.clearAdapter();
                            WebViewCore.WebKitHitTest localWebKitHitTest = (WebViewCore.WebKitHitTest)paramMessage.obj;
                            WebViewClassic.access$5702(WebViewClassic.this, localWebKitHitTest);
                            WebViewClassic.this.setTouchHighlightRects(localWebKitHitTest);
                            WebViewClassic.this.setHitTestResult(localWebKitHitTest);
                            break;
                        }
                    case 132:
                        WebViewClassic.SaveWebArchiveMessage localSaveWebArchiveMessage = (WebViewClassic.SaveWebArchiveMessage)paramMessage.obj;
                        if (localSaveWebArchiveMessage.mCallback != null)
                            localSaveWebArchiveMessage.mCallback.onReceiveValue(localSaveWebArchiveMessage.mResultFile);
                        break;
                    case 133:
                        WebViewClassic.access$2302(WebViewClassic.this, (WebViewCore.AutoFillData)paramMessage.obj);
                        if (WebViewClassic.this.mInputConnection != null)
                        {
                            WebViewClassic.this.mInputConnection.setAutoFillable(WebViewClassic.this.mAutoFillData.getQueryId());
                            WebViewClassic.this.mAutoCompletePopup.setAutoFillQueryId(WebViewClassic.this.mAutoFillData.getQueryId());
                        }
                        break;
                    case 134:
                        if (WebViewClassic.this.mAutoCompletePopup != null)
                        {
                            ArrayList localArrayList = new ArrayList();
                            WebViewClassic.this.mAutoCompletePopup.setAdapter(new ArrayAdapter(WebViewClassic.this.mContext, 17367238, localArrayList));
                        }
                        break;
                    case 141:
                        WebViewClassic.this.copyToClipboard((String)paramMessage.obj);
                        break;
                    case 5000:
                        String[] arrayOfString1 = (String[])paramMessage.obj;
                        WebViewClassic.access$6102(WebViewClassic.this, arrayOfString1[0]);
                        WebViewClassic.access$6202(WebViewClassic.this, arrayOfString1[1]);
                        WebViewClassic.this.invalidate();
                        break;
                    case 142:
                        if (WebViewClassic.this.mInputConnection != null)
                        {
                            WebViewCore.TextFieldInitData localTextFieldInitData = (WebViewCore.TextFieldInitData)paramMessage.obj;
                            WebViewClassic.access$4202(WebViewClassic.this, 0);
                            WebViewClassic.access$102(WebViewClassic.this, localTextFieldInitData.mFieldPointer);
                            WebViewClassic.this.mInputConnection.initEditorInfo(localTextFieldInitData);
                            WebViewClassic.this.mInputConnection.setTextAndKeepSelection(localTextFieldInitData.mText);
                            WebViewClassic.this.mEditTextContentBounds.set(localTextFieldInitData.mContentBounds);
                            WebViewClassic.this.mEditTextLayerId = localTextFieldInitData.mNodeLayerId;
                            WebViewClassic.nativeMapLayerRect(WebViewClassic.this.mNativeClass, WebViewClassic.this.mEditTextLayerId, WebViewClassic.this.mEditTextContentBounds);
                            WebViewClassic.this.mEditTextContent.set(localTextFieldInitData.mContentRect);
                            WebViewClassic.this.relocateAutoCompletePopup();
                        }
                        break;
                    case 143:
                        String str1 = (String)paramMessage.obj;
                        int k = paramMessage.arg1;
                        int m = paramMessage.arg2;
                        int n = k + str1.length();
                        WebViewClassic.this.replaceTextfieldText(k, m, str1, n, n);
                        WebViewClassic.this.selectionDone();
                        break;
                    case 126:
                        WebViewCore.FindAllRequest localFindAllRequest1 = (WebViewCore.FindAllRequest)paramMessage.obj;
                        if (localFindAllRequest1 == null)
                        {
                            if (WebViewClassic.this.mFindCallback != null)
                                WebViewClassic.this.mFindCallback.updateMatchCount(0, 0, true);
                        }
                        else if (localFindAllRequest1 == WebViewClassic.this.mFindRequest)
                            synchronized (WebViewClassic.this.mFindRequest)
                            {
                                int i = localFindAllRequest1.mMatchCount;
                                int j = localFindAllRequest1.mMatchIndex;
                                if (WebViewClassic.this.mFindCallback != null)
                                    WebViewClassic.this.mFindCallback.updateMatchCount(j, i, false);
                                if (WebViewClassic.this.mFindListener != null)
                                    WebViewClassic.this.mFindListener.onFindResultReceived(j, i, true);
                            }
                        break;
                    case 144:
                        if (WebViewClassic.this.mIsCaretSelection)
                            WebViewClassic.this.selectionDone();
                        break;
                    case 145:
                        WebViewClassic.this.sendBatchableInputMessage(223, paramMessage.arg1, 0, null);
                        break;
                    case 146:
                        WebViewClassic.this.relocateAutoCompletePopup();
                        break;
                    case 148:
                        WebViewClassic.this.mWebViewCore.sendMessage(192, paramMessage.arg1, 0);
                        break;
                    case 150:
                        if (paramMessage.arg1 == WebViewClassic.this.mFieldPointer)
                            WebViewClassic.this.mEditTextContent.set((Rect)paramMessage.obj);
                        break;
                    case 151:
                        if ((!WebViewClassic.this.mSelectingText) && (WebViewClassic.this.mIsEditingText) && (WebViewClassic.this.mIsCaretSelection))
                        {
                            WebViewClassic.this.setupWebkitSelect();
                            WebViewClassic.this.resetCaretTimer();
                            WebViewClassic.this.showPasteWindow();
                        }
                        break;
                    case 152:
                        WebViewClassic.this.mEditTextContentBounds.set((Rect)paramMessage.obj);
                        WebViewClassic.nativeMapLayerRect(WebViewClassic.this.mNativeClass, WebViewClassic.this.mEditTextLayerId, WebViewClassic.this.mEditTextContentBounds);
                        break;
                    case 149:
                        WebViewClassic.this.scrollEditWithCursor();
                    }
                }
            }
        }

        public boolean shouldInterceptTouchEvent(MotionEvent paramMotionEvent)
        {
            boolean bool = false;
            if (!WebViewClassic.this.mSelectingText);
            while (true)
            {
                return bool;
                WebViewClassic.this.ensureSelectionHandles();
                int i = Math.round(paramMotionEvent.getY() - WebViewClassic.this.getTitleHeight() + WebViewClassic.this.getScrollY());
                int j = Math.round(paramMotionEvent.getX() + WebViewClassic.this.getScrollX());
                if (WebViewClassic.this.mIsCaretSelection)
                    bool = WebViewClassic.this.mSelectHandleCenter.getBounds().contains(j, i);
                else if ((WebViewClassic.this.mSelectHandleLeft.getBounds().contains(j, i)) || (WebViewClassic.this.mSelectHandleRight.getBounds().contains(j, i)))
                    bool = true;
            }
        }

        public void showTapHighlight(boolean paramBoolean)
        {
            if (WebViewClassic.this.mShowTapHighlight != paramBoolean)
            {
                WebViewClassic.access$7502(WebViewClassic.this, paramBoolean);
                WebViewClassic.this.invalidate();
            }
        }
    }

    private class RequestFormData
        implements Runnable
    {
        private boolean mAutoComplete;
        private boolean mAutoFillable;
        private String mName;
        private Message mUpdateMessage;
        private String mUrl;
        private WebSettingsClassic mWebSettings;

        public RequestFormData(String paramString1, String paramMessage, Message paramBoolean1, boolean paramBoolean2, boolean arg6)
        {
            this.mName = paramString1;
            this.mUrl = WebTextView.urlForAutoCompleteData(paramMessage);
            this.mUpdateMessage = paramBoolean1;
            this.mAutoFillable = paramBoolean2;
            boolean bool;
            this.mAutoComplete = bool;
            this.mWebSettings = WebViewClassic.this.getSettings();
        }

        public void run()
        {
            ArrayList localArrayList = new ArrayList();
            if (this.mAutoFillable)
            {
                if ((this.mWebSettings == null) || (this.mWebSettings.getAutoFillProfile() == null))
                    break label168;
                localArrayList.add(WebViewClassic.this.mWebView.getResources().getText(17040194).toString() + " " + WebViewClassic.this.mAutoFillData.getPreviewString());
                WebViewClassic.this.mAutoCompletePopup.setIsAutoFillProfileSet(true);
            }
            while (true)
            {
                if (this.mAutoComplete)
                    localArrayList.addAll(WebViewClassic.this.mDatabase.getFormData(this.mUrl, this.mName));
                if (localArrayList.size() > 0)
                {
                    ArrayAdapter localArrayAdapter = new ArrayAdapter(WebViewClassic.this.mContext, 17367238, localArrayList);
                    this.mUpdateMessage.obj = localArrayAdapter;
                    this.mUpdateMessage.sendToTarget();
                }
                return;
                label168: localArrayList.add(WebViewClassic.this.mWebView.getResources().getText(17040195).toString());
                WebViewClassic.this.mAutoCompletePopup.setIsAutoFillProfileSet(false);
            }
        }
    }

    private class SelectionHandleAlpha
    {
        private int mAlpha = 0;

        private SelectionHandleAlpha()
        {
        }

        public int getAlpha()
        {
            return this.mAlpha;
        }

        public void setAlpha(int paramInt)
        {
            this.mAlpha = paramInt;
            if (WebViewClassic.this.mSelectHandleCenter != null)
            {
                WebViewClassic.this.mSelectHandleCenter.setAlpha(paramInt);
                WebViewClassic.this.mSelectHandleLeft.setAlpha(paramInt);
                WebViewClassic.this.mSelectHandleRight.setAlpha(paramInt);
                WebViewClassic.this.invalidate();
            }
        }
    }

    static class ViewSizeData
    {
        int mActualViewHeight;
        int mAnchorX;
        int mAnchorY;
        int mHeight;
        float mHeightWidthRatio;
        boolean mIgnoreHeight;
        float mScale;
        int mTextWrapWidth;
        int mWidth;
    }

    static class SaveWebArchiveMessage
    {
        final boolean mAutoname;
        final String mBasename;
        final ValueCallback<String> mCallback;
        String mResultFile;

        SaveWebArchiveMessage(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback)
        {
            this.mBasename = paramString;
            this.mAutoname = paramBoolean;
            this.mCallback = paramValueCallback;
        }
    }

    private static class DestroyNativeRunnable
        implements Runnable
    {
        private int mNativePtr;

        public DestroyNativeRunnable(int paramInt)
        {
            this.mNativePtr = paramInt;
        }

        public void run()
        {
            WebViewClassic.nativeDestroy(this.mNativePtr);
        }
    }

    public static abstract interface TitleBarDelegate
    {
        public abstract int getTitleHeight();

        public abstract void onSetEmbeddedTitleBar(View paramView);
    }

    private static class PackageListener extends BroadcastReceiver
    {
        public void onReceive(Context paramContext, Intent paramIntent)
        {
            String str1 = paramIntent.getAction();
            String str2 = paramIntent.getData().getSchemeSpecificPart();
            boolean bool = paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false);
            if (("android.intent.action.PACKAGE_REMOVED".equals(str1)) && (bool))
                return;
            if (WebViewClassic.sGoogleApps.contains(str2))
            {
                if (!"android.intent.action.PACKAGE_ADDED".equals(str1))
                    break label98;
                WebViewCore.sendStaticMessage(185, str2);
            }
            while (true)
            {
                PluginManager localPluginManager = PluginManager.getInstance(paramContext);
                if (!localPluginManager.containsPluginPermissionAndSignatures(str2))
                    break;
                localPluginManager.refreshPlugins("android.intent.action.PACKAGE_ADDED".equals(str1));
                break;
                label98: WebViewCore.sendStaticMessage(186, str2);
            }
        }
    }

    private static class ProxyReceiver extends BroadcastReceiver
    {
        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if (paramIntent.getAction().equals("android.intent.action.PROXY_CHANGE"))
                WebViewClassic.handleProxyBroadcast(paramIntent);
        }
    }

    private static class TrustStorageListener extends BroadcastReceiver
    {
        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if (paramIntent.getAction().equals("android.security.STORAGE_CHANGED"))
                WebViewClassic.access$1000();
        }
    }

    static class Factory
        implements WebViewFactoryProvider, WebViewFactoryProvider.Statics
    {
        public WebViewProvider createWebView(WebView paramWebView, WebView.PrivateAccess paramPrivateAccess)
        {
            return new WebViewClassic(paramWebView, paramPrivateAccess);
        }

        public String findAddress(String paramString)
        {
            return WebViewClassic.findAddress(paramString);
        }

        public CookieManager getCookieManager()
        {
            return CookieManagerClassic.getInstance();
        }

        public GeolocationPermissions getGeolocationPermissions()
        {
            return GeolocationPermissionsClassic.getInstance();
        }

        public WebViewFactoryProvider.Statics getStatics()
        {
            return this;
        }

        public WebIconDatabase getWebIconDatabase()
        {
            return WebIconDatabaseClassic.getInstance();
        }

        public WebStorage getWebStorage()
        {
            return WebStorageClassic.getInstance();
        }

        public WebViewDatabase getWebViewDatabase(Context paramContext)
        {
            return WebViewDatabaseClassic.getInstance(paramContext);
        }

        public void setPlatformNotificationsEnabled(boolean paramBoolean)
        {
            if (paramBoolean)
                WebViewClassic.enablePlatformNotifications();
            while (true)
            {
                return;
                WebViewClassic.disablePlatformNotifications();
            }
        }
    }

    static class FocusNodeHref
    {
        static final String SRC = "src";
        static final String TITLE = "title";
        static final String URL = "url";
    }

    private static class OnTrimMemoryListener
        implements ComponentCallbacks2
    {
        private static OnTrimMemoryListener sInstance = null;

        private OnTrimMemoryListener(Context paramContext)
        {
            paramContext.registerComponentCallbacks(this);
        }

        static void init(Context paramContext)
        {
            if (sInstance == null)
                sInstance = new OnTrimMemoryListener(paramContext.getApplicationContext());
        }

        public void onConfigurationChanged(Configuration paramConfiguration)
        {
        }

        public void onLowMemory()
        {
        }

        public void onTrimMemory(int paramInt)
        {
            if (paramInt >= 20)
                HTML5VideoInline.cleanupSurfaceTexture();
            WebViewClassic.nativeOnTrimMemory(paramInt);
        }
    }

    private class PastePopupWindow extends PopupWindow
        implements View.OnClickListener
    {
        private ViewGroup mContentView;
        private TextView mPasteTextView;

        public PastePopupWindow()
        {
            super(null, 16843464);
            setClippingEnabled(true);
            LinearLayout localLinearLayout = new LinearLayout(WebViewClassic.this.mContext);
            localLinearLayout.setOrientation(0);
            this.mContentView = localLinearLayout;
            this.mContentView.setBackgroundResource(17302967);
            LayoutInflater localLayoutInflater = (LayoutInflater)WebViewClassic.this.mContext.getSystemService("layout_inflater");
            ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-2, -2);
            this.mPasteTextView = ((TextView)localLayoutInflater.inflate(17367220, null));
            this.mPasteTextView.setLayoutParams(localLayoutParams);
            this.mContentView.addView(this.mPasteTextView);
            this.mPasteTextView.setText(17039371);
            this.mPasteTextView.setOnClickListener(this);
            setContentView(this.mContentView);
        }

        public void hide()
        {
            dismiss();
        }

        protected void measureContent()
        {
            DisplayMetrics localDisplayMetrics = WebViewClassic.this.mContext.getResources().getDisplayMetrics();
            this.mContentView.measure(View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.widthPixels, -2147483648), View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.heightPixels, -2147483648));
        }

        public void onClick(View paramView)
        {
            WebViewClassic.this.pasteFromClipboard();
            WebViewClassic.this.selectionDone();
        }

        public void show(Point paramPoint1, Point paramPoint2, int paramInt1, int paramInt2)
        {
            measureContent();
            int i = this.mContentView.getMeasuredWidth();
            int j = this.mContentView.getMeasuredHeight();
            int k = paramPoint2.y - j;
            int m = paramPoint2.x - i / 2;
            if (k < paramInt2)
            {
                WebViewClassic.this.ensureSelectionHandles();
                k = paramPoint1.y + WebViewClassic.this.mSelectHandleCenter.getIntrinsicHeight();
                m = paramPoint1.x - i / 2;
            }
            if (m < paramInt1)
                m = paramInt1;
            if (!isShowing())
                showAtLocation(WebViewClassic.this.mWebView, 0, m, k);
            update(m, k, i, j);
        }
    }

    class WebViewInputConnection extends BaseInputConnection
    {
        private int mBatchLevel;
        private String mHint;
        private int mImeOptions;
        private int mInputType;
        private boolean mIsAutoCompleteEnabled;
        private boolean mIsAutoFillable;
        private boolean mIsKeySentByMe;
        private KeyCharacterMap mKeyCharacterMap;
        private int mMaxLength;
        private String mName;

        public WebViewInputConnection()
        {
            super(true);
        }

        private CharSequence limitReplaceTextByMaxLength(CharSequence paramCharSequence, int paramInt)
        {
            if (this.mMaxLength > 0)
            {
                Editable localEditable = getEditable();
                int i = paramInt + (this.mMaxLength - localEditable.length());
                if (i < paramCharSequence.length())
                    paramCharSequence = paramCharSequence.subSequence(0, Math.max(i, 0));
            }
            return paramCharSequence;
        }

        private void restartInput()
        {
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if (localInputMethodManager != null)
                localInputMethodManager.restartInput(WebViewClassic.this.mWebView);
        }

        private void sendCharacter(char paramChar)
        {
            if (this.mKeyCharacterMap == null)
                this.mKeyCharacterMap = KeyCharacterMap.load(-1);
            char[] arrayOfChar = new char[1];
            arrayOfChar[0] = paramChar;
            KeyEvent[] arrayOfKeyEvent = this.mKeyCharacterMap.getEvents(arrayOfChar);
            if (arrayOfKeyEvent != null)
            {
                int i = arrayOfKeyEvent.length;
                for (int j = 0; j < i; j++)
                    sendKeyEvent(arrayOfKeyEvent[j]);
            }
            Message localMessage = WebViewClassic.this.mPrivateHandler.obtainMessage(145, paramChar, 0);
            WebViewClassic.this.mPrivateHandler.sendMessage(localMessage);
        }

        private void sendKey(int paramInt)
        {
            long l = SystemClock.uptimeMillis();
            sendKeyEvent(new KeyEvent(l, l, 0, paramInt, 0, 0, -1, 0, 2));
            sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), l, 1, paramInt, 0, 0, -1, 0, 2));
        }

        private void setNewText(int paramInt1, int paramInt2, CharSequence paramCharSequence)
        {
            this.mIsKeySentByMe = true;
            Editable localEditable = getEditable();
            CharSequence localCharSequence = localEditable.subSequence(paramInt1, paramInt2);
            int i = 0;
            int j = 0;
            int k = paramCharSequence.length();
            int m = localCharSequence.length();
            if (Selection.getSelectionStart(localEditable) == Selection.getSelectionEnd(localEditable))
            {
                if (k <= m)
                    break label198;
                if ((k == m + 1) && (TextUtils.regionMatches(paramCharSequence, 0, localCharSequence, 0, m)))
                    i = 1;
            }
            else
            {
                if (i == 0)
                    break label239;
                sendCharacter(paramCharSequence.charAt(k - 1));
            }
            while (true)
            {
                if (WebViewClassic.this.mAutoCompletePopup != null)
                {
                    StringBuilder localStringBuilder = new StringBuilder();
                    localStringBuilder.append(localEditable.subSequence(0, paramInt1));
                    localStringBuilder.append(paramCharSequence);
                    localStringBuilder.append(localEditable.subSequence(paramInt2, localEditable.length()));
                    WebViewClassic.this.mAutoCompletePopup.setText(localStringBuilder.toString());
                }
                this.mIsKeySentByMe = false;
                return;
                i = 0;
                break;
                label198: if (m <= k)
                    break;
                if ((k == m - 1) && (TextUtils.regionMatches(paramCharSequence, 0, localCharSequence, 0, k)));
                for (j = 1; ; j = 0)
                    break;
                label239: if (j != 0)
                {
                    sendKey(67);
                }
                else if ((k != m) || (!TextUtils.regionMatches(paramCharSequence, 0, localCharSequence, 0, k)))
                {
                    Message localMessage = WebViewClassic.this.mPrivateHandler.obtainMessage(143, paramInt1, paramInt2, paramCharSequence.toString());
                    WebViewClassic.this.mPrivateHandler.sendMessage(localMessage);
                }
            }
        }

        private void updateSelection()
        {
            Editable localEditable = getEditable();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            int k = getComposingSpanStart(localEditable);
            int m = getComposingSpanEnd(localEditable);
            InputMethodManager localInputMethodManager = InputMethodManager.peekInstance();
            if (localInputMethodManager != null)
                localInputMethodManager.updateSelection(WebViewClassic.this.mWebView, i, j, k, m);
        }

        public boolean beginBatchEdit()
        {
            if (this.mBatchLevel == 0)
                WebViewClassic.this.beginTextBatch();
            this.mBatchLevel = (1 + this.mBatchLevel);
            return false;
        }

        public boolean commitText(CharSequence paramCharSequence, int paramInt)
        {
            setComposingText(paramCharSequence, paramInt);
            finishComposingText();
            return true;
        }

        public boolean deleteSurroundingText(int paramInt1, int paramInt2)
        {
            Editable localEditable = getEditable();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            if (i > j)
            {
                int i3 = i;
                i = j;
                j = i3;
            }
            int k = getComposingSpanStart(localEditable);
            int m = getComposingSpanEnd(localEditable);
            if (m < k)
            {
                int i2 = k;
                k = m;
                m = i2;
            }
            if ((k != -1) && (m != -1))
            {
                if (k < i)
                    i = k;
                if (m > j)
                    j = m;
            }
            int n = Math.min(localEditable.length(), j + paramInt2);
            if (n > j)
                setNewText(j, n, "");
            int i1 = Math.max(0, i - paramInt1);
            if (i1 < i)
                setNewText(i1, i, "");
            return super.deleteSurroundingText(paramInt1, paramInt2);
        }

        public boolean endBatchEdit()
        {
            this.mBatchLevel = (-1 + this.mBatchLevel);
            if (this.mBatchLevel == 0)
                WebViewClassic.this.commitTextBatch();
            return false;
        }

        public boolean getIsAutoFillable()
        {
            return this.mIsAutoFillable;
        }

        public void initEditorInfo(WebViewCore.TextFieldInitData paramTextFieldInitData)
        {
            int i = paramTextFieldInitData.mType;
            int j = 161;
            int k = 301989888;
            if (!paramTextFieldInitData.mIsSpellCheckEnabled)
                j |= 524288;
            if (1 != i)
            {
                if (paramTextFieldInitData.mIsTextFieldNext)
                    k |= 134217728;
                if (paramTextFieldInitData.mIsTextFieldPrev)
                    k |= 67108864;
            }
            int m;
            switch (i)
            {
            default:
                m = k | 0x2;
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            }
            while (true)
            {
                this.mHint = paramTextFieldInitData.mLabel;
                this.mInputType = j;
                this.mImeOptions = m;
                this.mMaxLength = paramTextFieldInitData.mMaxLength;
                this.mIsAutoCompleteEnabled = paramTextFieldInitData.mIsAutoCompleteEnabled;
                this.mName = paramTextFieldInitData.mName;
                WebViewClassic.this.mAutoCompletePopup.clearAdapter();
                return;
                m = k | 0x2;
                continue;
                j |= 180224;
                m = k | 0x1;
                continue;
                j |= 224;
                m = k | 0x2;
                continue;
                m = k | 0x3;
                continue;
                j = 209;
                m = k | 0x2;
                continue;
                j = 12290;
                m = k | 0x5;
                continue;
                j = 3;
                m = k | 0x5;
                continue;
                m = k | 0x2;
                j |= 16;
            }
        }

        public boolean performEditorAction(int paramInt)
        {
            boolean bool = true;
            switch (paramInt)
            {
            case 4:
            default:
                bool = super.performEditorAction(paramInt);
            case 5:
            case 7:
            case 6:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool;
                WebViewClassic.this.mWebView.requestFocus(2);
                continue;
                WebViewClassic.this.mWebView.requestFocus(1);
                continue;
                WebViewClassic.this.hideSoftKeyboard();
                continue;
                WebViewClassic.this.hideSoftKeyboard();
                String str = getEditable().toString();
                WebViewClassic.this.passToJavaScript(str, new KeyEvent(0, 66));
                WebViewClassic.this.passToJavaScript(str, new KeyEvent(1, 66));
            }
        }

        public void replaceSelection(CharSequence paramCharSequence)
        {
            Editable localEditable = getEditable();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            CharSequence localCharSequence = limitReplaceTextByMaxLength(paramCharSequence, j - i);
            setNewText(i, j, localCharSequence);
            localEditable.replace(i, j, localCharSequence);
            restartInput();
            int k = i + localCharSequence.length();
            setSelection(k, k);
        }

        public boolean sendKeyEvent(KeyEvent paramKeyEvent)
        {
            int i = 1;
            int j;
            if (!this.mIsKeySentByMe)
                if (paramKeyEvent.getAction() == i)
                    if (paramKeyEvent.getKeyCode() == 67)
                        j = deleteSurroundingText(i, 0);
            while (true)
            {
                return j;
                int k;
                if (paramKeyEvent.getKeyCode() == 112)
                {
                    k = deleteSurroundingText(0, j);
                }
                else
                {
                    boolean bool;
                    if (paramKeyEvent.getUnicodeChar() != 0)
                    {
                        bool = commitText(Character.toString((char)paramKeyEvent.getUnicodeChar()), k);
                        continue;
                        if ((paramKeyEvent.getAction() == 0) && ((paramKeyEvent.getKeyCode() == 67) || (paramKeyEvent.getKeyCode() == 112) || (paramKeyEvent.getUnicodeChar() != 0)));
                    }
                    else
                    {
                        bool = super.sendKeyEvent(paramKeyEvent);
                    }
                }
            }
        }

        public void setAutoFillable(int paramInt)
        {
            if ((WebViewClassic.this.getSettings().getAutoFillEnabled()) && (paramInt != -1));
            for (boolean bool = true; ; bool = false)
            {
                this.mIsAutoFillable = bool;
                if (((0xFF0 & this.mInputType) != 224) && ((this.mIsAutoFillable) || (this.mIsAutoCompleteEnabled)) && (this.mName != null) && (this.mName.length() > 0))
                    WebViewClassic.this.requestFormData(this.mName, WebViewClassic.this.mFieldPointer, this.mIsAutoFillable, this.mIsAutoCompleteEnabled);
                return;
            }
        }

        public boolean setComposingRegion(int paramInt1, int paramInt2)
        {
            boolean bool = super.setComposingRegion(paramInt1, paramInt2);
            updateSelection();
            return bool;
        }

        public boolean setComposingText(CharSequence paramCharSequence, int paramInt)
        {
            Editable localEditable = getEditable();
            int i = getComposingSpanStart(localEditable);
            int j = getComposingSpanEnd(localEditable);
            if ((i < 0) || (j < 0))
            {
                i = Selection.getSelectionStart(localEditable);
                j = Selection.getSelectionEnd(localEditable);
            }
            if (j < i)
            {
                int m = j;
                j = i;
                i = m;
            }
            CharSequence localCharSequence = limitReplaceTextByMaxLength(paramCharSequence, j - i);
            setNewText(i, j, localCharSequence);
            if (localCharSequence != paramCharSequence)
                paramInt -= paramCharSequence.length() - localCharSequence.length();
            super.setComposingText(localCharSequence, paramInt);
            updateSelection();
            if (localCharSequence != paramCharSequence)
            {
                restartInput();
                int k = i + localCharSequence.length();
                finishComposingText();
                setSelection(k, k);
            }
            return true;
        }

        public boolean setSelection(int paramInt1, int paramInt2)
        {
            boolean bool = super.setSelection(paramInt1, paramInt2);
            updateSelection();
            return bool;
        }

        public void setTextAndKeepSelection(CharSequence paramCharSequence)
        {
            Editable localEditable = getEditable();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            CharSequence localCharSequence = limitReplaceTextByMaxLength(paramCharSequence, localEditable.length());
            localEditable.replace(0, localEditable.length(), localCharSequence);
            restartInput();
            setSelection(Math.min(i, localEditable.length()), Math.min(j, localEditable.length()));
            finishComposingText();
        }

        public void setupEditorInfo(EditorInfo paramEditorInfo)
        {
            paramEditorInfo.inputType = this.mInputType;
            paramEditorInfo.imeOptions = this.mImeOptions;
            paramEditorInfo.hintText = this.mHint;
            paramEditorInfo.initialCapsMode = getCursorCapsMode(1);
            Editable localEditable = getEditable();
            int i = Selection.getSelectionStart(localEditable);
            int j = Selection.getSelectionEnd(localEditable);
            if ((i < 0) || (j < 0))
            {
                i = localEditable.length();
                j = i;
            }
            paramEditorInfo.initialSelStart = i;
            paramEditorInfo.initialSelEnd = j;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewClassic
 * JD-Core Version:        0.6.2
 */