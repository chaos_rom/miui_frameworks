package android.webkit;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.http.SslCertificate;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import java.io.File;
import java.util.Map;

public abstract interface WebViewProvider
{
    public abstract void addJavascriptInterface(Object paramObject, String paramString);

    public abstract boolean canGoBack();

    public abstract boolean canGoBackOrForward(int paramInt);

    public abstract boolean canGoForward();

    public abstract boolean canZoomIn();

    public abstract boolean canZoomOut();

    public abstract Picture capturePicture();

    public abstract void clearCache(boolean paramBoolean);

    public abstract void clearFormData();

    public abstract void clearHistory();

    public abstract void clearMatches();

    public abstract void clearSslPreferences();

    public abstract void clearView();

    public abstract WebBackForwardList copyBackForwardList();

    public abstract void debugDump();

    public abstract void destroy();

    public abstract void documentHasImages(Message paramMessage);

    public abstract void emulateShiftHeld();

    public abstract int findAll(String paramString);

    public abstract void findAllAsync(String paramString);

    public abstract void findNext(boolean paramBoolean);

    public abstract void flingScroll(int paramInt1, int paramInt2);

    public abstract void freeMemory();

    public abstract SslCertificate getCertificate();

    public abstract int getContentHeight();

    public abstract int getContentWidth();

    public abstract Bitmap getFavicon();

    public abstract WebView.HitTestResult getHitTestResult();

    public abstract String[] getHttpAuthUsernamePassword(String paramString1, String paramString2);

    public abstract String getOriginalUrl();

    public abstract int getProgress();

    public abstract float getScale();

    public abstract ScrollDelegate getScrollDelegate();

    public abstract WebSettings getSettings();

    public abstract String getTitle();

    public abstract String getTouchIconUrl();

    public abstract String getUrl();

    public abstract ViewDelegate getViewDelegate();

    public abstract int getVisibleTitleHeight();

    public abstract View getZoomControls();

    public abstract void goBack();

    public abstract void goBackOrForward(int paramInt);

    public abstract void goForward();

    public abstract void init(Map<String, Object> paramMap, boolean paramBoolean);

    public abstract void invokeZoomPicker();

    public abstract boolean isPaused();

    public abstract boolean isPrivateBrowsingEnabled();

    public abstract void loadData(String paramString1, String paramString2, String paramString3);

    public abstract void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);

    public abstract void loadUrl(String paramString);

    public abstract void loadUrl(String paramString, Map<String, String> paramMap);

    public abstract void onPause();

    public abstract void onResume();

    public abstract boolean overlayHorizontalScrollbar();

    public abstract boolean overlayVerticalScrollbar();

    public abstract boolean pageDown(boolean paramBoolean);

    public abstract boolean pageUp(boolean paramBoolean);

    public abstract void pauseTimers();

    public abstract void postUrl(String paramString, byte[] paramArrayOfByte);

    public abstract void reload();

    public abstract void removeJavascriptInterface(String paramString);

    public abstract void requestFocusNodeHref(Message paramMessage);

    public abstract void requestImageRef(Message paramMessage);

    public abstract boolean restorePicture(Bundle paramBundle, File paramFile);

    public abstract WebBackForwardList restoreState(Bundle paramBundle);

    public abstract void resumeTimers();

    public abstract void savePassword(String paramString1, String paramString2, String paramString3);

    public abstract boolean savePicture(Bundle paramBundle, File paramFile);

    public abstract WebBackForwardList saveState(Bundle paramBundle);

    public abstract void saveWebArchive(String paramString);

    public abstract void saveWebArchive(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback);

    public abstract void setCertificate(SslCertificate paramSslCertificate);

    public abstract void setDownloadListener(DownloadListener paramDownloadListener);

    public abstract void setFindListener(WebView.FindListener paramFindListener);

    public abstract void setHorizontalScrollbarOverlay(boolean paramBoolean);

    public abstract void setHttpAuthUsernamePassword(String paramString1, String paramString2, String paramString3, String paramString4);

    public abstract void setInitialScale(int paramInt);

    public abstract void setMapTrackballToArrowKeys(boolean paramBoolean);

    public abstract void setNetworkAvailable(boolean paramBoolean);

    public abstract void setPictureListener(WebView.PictureListener paramPictureListener);

    public abstract void setVerticalScrollbarOverlay(boolean paramBoolean);

    public abstract void setWebChromeClient(WebChromeClient paramWebChromeClient);

    public abstract void setWebViewClient(WebViewClient paramWebViewClient);

    public abstract boolean showFindDialog(String paramString, boolean paramBoolean);

    public abstract void stopLoading();

    public abstract boolean zoomIn();

    public abstract boolean zoomOut();

    public static abstract interface ScrollDelegate
    {
        public abstract int computeHorizontalScrollOffset();

        public abstract int computeHorizontalScrollRange();

        public abstract void computeScroll();

        public abstract int computeVerticalScrollExtent();

        public abstract int computeVerticalScrollOffset();

        public abstract int computeVerticalScrollRange();
    }

    public static abstract interface ViewDelegate
    {
        public abstract boolean dispatchKeyEvent(KeyEvent paramKeyEvent);

        public abstract void onAttachedToWindow();

        public abstract void onConfigurationChanged(Configuration paramConfiguration);

        public abstract InputConnection onCreateInputConnection(EditorInfo paramEditorInfo);

        public abstract void onDetachedFromWindow();

        public abstract void onDraw(Canvas paramCanvas);

        public abstract void onDrawVerticalScrollBar(Canvas paramCanvas, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4);

        public abstract void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect);

        public abstract boolean onGenericMotionEvent(MotionEvent paramMotionEvent);

        public abstract boolean onHoverEvent(MotionEvent paramMotionEvent);

        public abstract void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent);

        public abstract void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo);

        public abstract boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent);

        public abstract boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent);

        public abstract boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent);

        public abstract void onMeasure(int paramInt1, int paramInt2);

        public abstract void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2);

        public abstract void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

        public abstract void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

        public abstract boolean onTouchEvent(MotionEvent paramMotionEvent);

        public abstract boolean onTrackballEvent(MotionEvent paramMotionEvent);

        public abstract void onVisibilityChanged(View paramView, int paramInt);

        public abstract void onWindowFocusChanged(boolean paramBoolean);

        public abstract void onWindowVisibilityChanged(int paramInt);

        public abstract boolean performAccessibilityAction(int paramInt, Bundle paramBundle);

        public abstract boolean performLongClick();

        public abstract boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean);

        public abstract boolean requestFocus(int paramInt, Rect paramRect);

        public abstract void setBackgroundColor(int paramInt);

        public abstract boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

        public abstract void setLayerType(int paramInt, Paint paramPaint);

        public abstract void setLayoutParams(ViewGroup.LayoutParams paramLayoutParams);

        public abstract void setOverScrollMode(int paramInt);

        public abstract void setScrollBarStyle(int paramInt);

        public abstract boolean shouldDelayChildPressedState();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewProvider
 * JD-Core Version:        0.6.2
 */