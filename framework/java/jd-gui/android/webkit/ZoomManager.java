package android.webkit;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.FloatMath;
import android.util.Log;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;

class ZoomManager
{
    protected static final float DEFAULT_MAX_ZOOM_SCALE_FACTOR = 4.0F;
    protected static final float DEFAULT_MIN_ZOOM_SCALE_FACTOR = 0.25F;
    static final String LOGTAG = "webviewZoom";
    private static float MINIMUM_SCALE_INCREMENT = 0.0F;
    private static float MINIMUM_SCALE_WITHOUT_JITTER = 0.0F;
    private static float MIN_DOUBLE_TAP_SCALE_INCREMENT = 0.0F;
    private static final int ZOOM_ANIMATION_LENGTH = 175;
    private float mActualScale;
    private boolean mAllowPanAndScale;
    private int mAnchorX;
    private int mAnchorY;
    private final CallbackProxy mCallbackProxy;
    private float mDefaultMaxZoomScale;
    private float mDefaultMinZoomScale;
    private float mDefaultScale;
    private float mDisplayDensity;
    private float mDoubleTapZoomFactor = 1.0F;
    private ZoomControlEmbedded mEmbeddedZoomControl;
    private ZoomControlExternal mExternalZoomControl;
    private FocusMovementQueue mFocusMovementQueue;
    private float mFocusX;
    private float mFocusY;
    private boolean mHardwareAccelerated = false;
    private boolean mInHWAcceleratedZoom = false;
    private boolean mInZoomOverview = false;
    private float mInitialScale;
    private int mInitialScrollX;
    private int mInitialScrollY;
    private boolean mInitialZoomOverview = false;
    private float mInvActualScale;
    private float mInvDefaultScale;
    private float mInvFinalZoomScale;
    private float mInvInitialZoomScale;
    private float mInvZoomOverviewWidth;
    private float mMaxZoomScale;
    private float mMinZoomScale;
    private boolean mMinZoomScaleFixed = true;
    private boolean mPinchToZoomAnimating = false;
    private ScaleGestureDetector mScaleDetector;
    private boolean mSupportMultiTouch;
    private float mTextWrapScale;
    private final WebViewClassic mWebView;
    private float mZoomCenterX;
    private float mZoomCenterY;
    private int mZoomOverviewWidth;
    private float mZoomScale;
    private long mZoomStart;

    static
    {
        if (!ZoomManager.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            MIN_DOUBLE_TAP_SCALE_INCREMENT = 0.5F;
            MINIMUM_SCALE_INCREMENT = 0.007F;
            MINIMUM_SCALE_WITHOUT_JITTER = 0.007F;
            return;
        }
    }

    public ZoomManager(WebViewClassic paramWebViewClassic, CallbackProxy paramCallbackProxy)
    {
        this.mWebView = paramWebViewClassic;
        this.mCallbackProxy = paramCallbackProxy;
        setZoomOverviewWidth(980);
        this.mFocusMovementQueue = new FocusMovementQueue();
    }

    public static final boolean exceedsMinScaleIncrement(float paramFloat1, float paramFloat2)
    {
        if (Math.abs(paramFloat1 - paramFloat2) >= MINIMUM_SCALE_INCREMENT);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private ZoomControlBase getCurrentZoomControl()
    {
        Object localObject;
        if ((this.mWebView.getSettings() != null) && (this.mWebView.getSettings().supportZoom()))
            if (this.mWebView.getSettings().getBuiltInZoomControls())
            {
                if ((this.mEmbeddedZoomControl == null) && (this.mWebView.getSettings().getDisplayZoomControls()))
                    this.mEmbeddedZoomControl = new ZoomControlEmbedded(this, this.mWebView);
                localObject = this.mEmbeddedZoomControl;
            }
        while (true)
        {
            return localObject;
            if (this.mExternalZoomControl == null)
                this.mExternalZoomControl = new ZoomControlExternal(this.mWebView);
            localObject = this.mExternalZoomControl;
            continue;
            localObject = null;
        }
    }

    private void setDefaultZoomScale(float paramFloat)
    {
        float f = this.mDefaultScale;
        this.mDefaultScale = paramFloat;
        this.mInvDefaultScale = (1.0F / paramFloat);
        this.mDefaultMaxZoomScale = (4.0F * paramFloat);
        this.mDefaultMinZoomScale = (0.25F * paramFloat);
        if ((f > 0.0D) && (this.mMaxZoomScale > 0.0D))
        {
            this.mMaxZoomScale = (paramFloat / f * this.mMaxZoomScale);
            if ((f <= 0.0D) || (this.mMinZoomScale <= 0.0D))
                break label125;
        }
        label125: for (this.mMinZoomScale = (paramFloat / f * this.mMinZoomScale); ; this.mMinZoomScale = this.mDefaultMinZoomScale)
        {
            if (!exceedsMinScaleIncrement(this.mMinZoomScale, this.mMaxZoomScale))
                this.mMaxZoomScale = this.mMinZoomScale;
            return;
            this.mMaxZoomScale = this.mDefaultMaxZoomScale;
            break;
        }
    }

    private void setZoomOverviewWidth(int paramInt)
    {
        if (paramInt == 0);
        for (this.mZoomOverviewWidth = 980; ; this.mZoomOverviewWidth = paramInt)
        {
            this.mInvZoomOverviewWidth = (1.0F / paramInt);
            return;
        }
    }

    private void setZoomScale(float paramFloat, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramFloat < this.mMinZoomScale);
        float f1;
        for (int i = 1; ; i = 0)
        {
            f1 = computeScaleWithLimits(paramFloat);
            if ((i == 0) || (this.mMinZoomScale >= this.mDefaultScale))
                break;
            this.mInZoomOverview = true;
            if ((paramBoolean1) && (!this.mWebView.getSettings().getUseFixedViewport()))
                this.mTextWrapScale = f1;
            if ((f1 != this.mActualScale) || (paramBoolean2))
            {
                float f2 = this.mInvActualScale;
                if ((f1 != this.mActualScale) && (!this.mPinchToZoomAnimating))
                    this.mCallbackProxy.onScaleChanged(this.mActualScale, f1);
                this.mActualScale = f1;
                this.mInvActualScale = (1.0F / f1);
                if ((!this.mWebView.drawHistory()) && (!this.mInHWAcceleratedZoom))
                {
                    int j = this.mWebView.getScrollX();
                    int k = this.mWebView.getScrollY();
                    float f3 = f1 * f2;
                    float f4 = f3 * j + (f3 - 1.0F) * this.mZoomCenterX;
                    float f5 = f3 * k + (f3 - 1.0F) * (this.mZoomCenterY - this.mWebView.getTitleHeight());
                    this.mWebView.mViewManager.scaleAll();
                    int m = this.mWebView.pinLocX(Math.round(f4));
                    int n = this.mWebView.pinLocY(Math.round(f5));
                    if (!this.mWebView.updateScrollCoordinates(m, n))
                        this.mWebView.sendOurVisibleRect();
                }
                this.mWebView.sendViewSizeZoom(paramBoolean1);
            }
            return;
        }
        if (!exceedsMinScaleIncrement(f1, getZoomOverviewScale()));
        for (boolean bool = true; ; bool = false)
        {
            this.mInZoomOverview = bool;
            break;
        }
    }

    private boolean setupZoomOverviewWidth(WebViewCore.DrawData paramDrawData, int paramInt)
    {
        WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
        int i = this.mZoomOverviewWidth;
        if (localWebSettingsClassic.getUseWideViewPort())
        {
            if (paramDrawData.mContentSize.x > 0)
                i = Math.min(WebViewClassic.sMaxViewportWidth, paramDrawData.mContentSize.x);
            if (i == this.mZoomOverviewWidth)
                break label82;
            setZoomOverviewWidth(i);
        }
        label82: for (boolean bool = true; ; bool = false)
        {
            return bool;
            i = Math.round(paramInt / this.mDefaultScale);
            break;
        }
    }

    private boolean zoom(float paramFloat)
    {
        boolean bool = false;
        this.mInitialZoomOverview = false;
        this.mWebView.switchOutDrawHistory();
        this.mZoomCenterX = (0.5F * this.mWebView.getViewWidth());
        this.mZoomCenterY = (0.5F * this.mWebView.getViewHeight());
        this.mAnchorX = this.mWebView.viewToContentX((int)this.mZoomCenterX + this.mWebView.getScrollX());
        this.mAnchorY = this.mWebView.viewToContentY((int)this.mZoomCenterY + this.mWebView.getScrollY());
        float f = paramFloat * this.mActualScale;
        if (!this.mWebView.getSettings().getUseFixedViewport())
            bool = true;
        return startZoomAnimation(f, bool);
    }

    private void zoomToOverview()
    {
        boolean bool = false;
        if (this.mWebView.getScrollY() < this.mWebView.getTitleHeight())
            this.mWebView.updateScrollCoordinates(this.mWebView.getScrollX(), 0);
        float f = getZoomOverviewScale();
        if (!this.mWebView.getSettings().getUseFixedViewport())
            bool = true;
        startZoomAnimation(f, bool);
    }

    private void zoomToReadingLevel()
    {
        boolean bool = false;
        float f = getReadingLevelScale();
        int i = this.mWebView.getBlockLeftEdge(this.mAnchorX, this.mAnchorY, f);
        int j;
        int k;
        if (i != -1)
        {
            WebViewClassic localWebViewClassic = this.mWebView;
            if (i >= 5)
                break label105;
            j = 0;
            k = localWebViewClassic.contentToViewX(j) - this.mWebView.getScrollX();
            if (k <= 0)
                break label113;
        }
        for (this.mZoomCenterX = (f * k / (f - this.mActualScale)); ; this.mZoomCenterX = 0.0F)
        {
            if (!this.mWebView.getSettings().getUseFixedViewport())
                bool = true;
            startZoomAnimation(f, bool);
            return;
            label105: j = i - 5;
            break;
            label113: this.mWebView.getWebView().scrollBy(k, 0);
        }
    }

    public void animateZoom(Canvas paramCanvas)
    {
        this.mInitialZoomOverview = false;
        if (this.mZoomScale == 0.0F)
        {
            Log.w("webviewZoom", "A WebView is attempting to perform a fixed length zoom animation when no zoom is in progress");
            this.mInHWAcceleratedZoom = false;
        }
        while (true)
        {
            return;
            int i = (int)(SystemClock.uptimeMillis() - this.mZoomStart);
            float f1;
            label81: int j;
            int k;
            int m;
            if (i < 175)
            {
                float f3 = i / 175.0F;
                f1 = 1.0F / (this.mInvInitialZoomScale + f3 * (this.mInvFinalZoomScale - this.mInvInitialZoomScale));
                this.mWebView.invalidate();
                float f2 = f1 * this.mInvInitialZoomScale;
                j = -WebViewClassic.pinLoc(Math.round(f2 * (this.mInitialScrollX + this.mZoomCenterX) - this.mZoomCenterX), this.mWebView.getViewWidth(), Math.round(f1 * this.mWebView.getContentWidth())) + this.mWebView.getScrollX();
                k = this.mWebView.getTitleHeight();
                m = Math.round(f2 * (this.mInitialScrollY + this.mZoomCenterY - k) - (this.mZoomCenterY - k));
                if (m > k)
                    break label309;
            }
            int i1;
            label309: for (int n = Math.max(m, 0); ; n = k + WebViewClassic.pinLoc(m - k, this.mWebView.getViewHeight(), Math.round(f1 * this.mWebView.getContentHeight())))
            {
                i1 = -n + this.mWebView.getScrollY();
                if (!this.mHardwareAccelerated)
                    break label345;
                this.mWebView.updateScrollCoordinates(this.mWebView.getScrollX() - j, this.mWebView.getScrollY() - i1);
                paramCanvas.translate(j, i1);
                setZoomScale(f1, false);
                if (this.mZoomScale != 0.0F)
                    break;
                this.mInHWAcceleratedZoom = false;
                this.mWebView.sendViewSizeZoom(false);
                break;
                f1 = this.mZoomScale;
                this.mZoomScale = 0.0F;
                this.mWebView.onFixedLengthZoomAnimationEnd();
                break label81;
            }
            label345: paramCanvas.translate(j, i1);
            paramCanvas.scale(f1, f1);
        }
    }

    public final boolean canZoomIn()
    {
        if (this.mMaxZoomScale - this.mActualScale > MINIMUM_SCALE_INCREMENT);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean canZoomOut()
    {
        if (this.mActualScale - this.mMinZoomScale > MINIMUM_SCALE_INCREMENT);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final void clearDocumentAnchor()
    {
        this.mAnchorY = 0;
        this.mAnchorX = 0;
    }

    final float computeReadingLevelScale(float paramFloat)
    {
        return Math.max(this.mDisplayDensity * this.mDoubleTapZoomFactor, paramFloat + MIN_DOUBLE_TAP_SCALE_INCREMENT);
    }

    public final float computeScaleWithLimits(float paramFloat)
    {
        if (paramFloat < this.mMinZoomScale)
            paramFloat = this.mMinZoomScale;
        while (true)
        {
            return paramFloat;
            if (paramFloat > this.mMaxZoomScale)
                paramFloat = this.mMaxZoomScale;
        }
    }

    public void dismissZoomPicker()
    {
        ZoomControlBase localZoomControlBase = getCurrentZoomControl();
        if (localZoomControlBase != null)
            localZoomControlBase.hide();
    }

    public final float getDefaultMaxZoomScale()
    {
        return this.mDefaultMaxZoomScale;
    }

    public final float getDefaultMinZoomScale()
    {
        return this.mDefaultMinZoomScale;
    }

    public final float getDefaultScale()
    {
        return this.mDefaultScale;
    }

    public final int getDocumentAnchorX()
    {
        return this.mAnchorX;
    }

    public final int getDocumentAnchorY()
    {
        return this.mAnchorY;
    }

    public View getExternalZoomPicker()
    {
        ZoomControlBase localZoomControlBase = getCurrentZoomControl();
        if ((localZoomControlBase != null) && (localZoomControlBase == this.mExternalZoomControl));
        for (ZoomControlExternal.ExtendedZoomControls localExtendedZoomControls = this.mExternalZoomControl.getControls(); ; localExtendedZoomControls = null)
            return localExtendedZoomControls;
    }

    public final float getInvDefaultScale()
    {
        return this.mInvDefaultScale;
    }

    public final float getInvScale()
    {
        return this.mInvActualScale;
    }

    public final float getMaxZoomScale()
    {
        return this.mMaxZoomScale;
    }

    public final float getMinZoomScale()
    {
        return this.mMinZoomScale;
    }

    public final float getReadingLevelScale()
    {
        return computeScaleWithLimits(computeReadingLevelScale(getZoomOverviewScale()));
    }

    public final float getScale()
    {
        return this.mActualScale;
    }

    public ScaleGestureDetector getScaleGestureDetector()
    {
        return this.mScaleDetector;
    }

    public final float getTextWrapScale()
    {
        return this.mTextWrapScale;
    }

    float getZoomOverviewScale()
    {
        return this.mWebView.getViewWidth() * this.mInvZoomOverviewWidth;
    }

    public void handleDoubleTap(float paramFloat1, float paramFloat2)
    {
        int i = 0;
        this.mInitialZoomOverview = false;
        WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
        if (!isDoubleTapEnabled());
        while (true)
        {
            return;
            setZoomCenter(paramFloat1, paramFloat2);
            this.mAnchorX = this.mWebView.viewToContentX((int)paramFloat1 + this.mWebView.getScrollX());
            this.mAnchorY = this.mWebView.viewToContentY((int)paramFloat2 + this.mWebView.getScrollY());
            localWebSettingsClassic.setDoubleTapToastCount(0);
            dismissZoomPicker();
            if (localWebSettingsClassic.getUseFixedViewport());
            for (float f = Math.max(this.mActualScale, getReadingLevelScale()); ; f = this.mActualScale)
            {
                if (!exceedsMinScaleIncrement(this.mActualScale, this.mTextWrapScale))
                    i = 1;
                if ((i != 0) || (this.mInZoomOverview))
                    this.mTextWrapScale = f;
                if ((!localWebSettingsClassic.isNarrowColumnLayout()) || (!exceedsMinScaleIncrement(this.mTextWrapScale, f)) || (i != 0) || (this.mInZoomOverview))
                    break label190;
                this.mTextWrapScale = f;
                refreshZoomScale(true);
                break;
            }
            label190: if ((!this.mInZoomOverview) && (willScaleTriggerZoom(getZoomOverviewScale())))
            {
                if (this.mTextWrapScale > getReadingLevelScale())
                {
                    this.mTextWrapScale = getReadingLevelScale();
                    refreshZoomScale(true);
                }
                zoomToOverview();
            }
            else
            {
                zoomToReadingLevel();
            }
        }
    }

    public void init(float paramFloat)
    {
        assert (paramFloat > 0.0F);
        this.mDisplayDensity = paramFloat;
        setDefaultZoomScale(paramFloat);
        this.mActualScale = paramFloat;
        this.mInvActualScale = (1.0F / paramFloat);
        this.mTextWrapScale = getReadingLevelScale();
    }

    public void invokeZoomPicker()
    {
        ZoomControlBase localZoomControlBase = getCurrentZoomControl();
        if (localZoomControlBase != null)
            localZoomControlBase.show();
    }

    public boolean isDoubleTapEnabled()
    {
        WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
        if ((localWebSettingsClassic != null) && (localWebSettingsClassic.getUseWideViewPort()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isFixedLengthAnimationInProgress()
    {
        if ((this.mZoomScale != 0.0F) || (this.mInHWAcceleratedZoom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isInZoomOverview()
    {
        return this.mInZoomOverview;
    }

    public boolean isPreventingWebkitUpdates()
    {
        return isZoomAnimating();
    }

    public final boolean isScaleOverLimits(float paramFloat)
    {
        if ((paramFloat <= this.mMinZoomScale) || (paramFloat >= this.mMaxZoomScale));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isZoomAnimating()
    {
        if ((isFixedLengthAnimationInProgress()) || (this.mPinchToZoomAnimating));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isZoomPickerVisible()
    {
        ZoomControlBase localZoomControlBase = getCurrentZoomControl();
        if (localZoomControlBase != null);
        for (boolean bool = localZoomControlBase.isVisible(); ; bool = false)
            return bool;
    }

    public final boolean isZoomScaleFixed()
    {
        if (this.mMinZoomScale >= this.mMaxZoomScale);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void keepZoomPickerVisible()
    {
        ZoomControlBase localZoomControlBase = getCurrentZoomControl();
        if ((localZoomControlBase != null) && (localZoomControlBase == this.mExternalZoomControl))
            localZoomControlBase.show();
    }

    public void onFirstLayout(WebViewCore.DrawData paramDrawData)
    {
        assert (paramDrawData != null);
        assert (paramDrawData.mViewState != null);
        assert (this.mWebView.getSettings() != null);
        WebViewCore.ViewState localViewState = paramDrawData.mViewState;
        updateZoomRange(localViewState, paramDrawData.mViewSize.x, paramDrawData.mMinPrefWidth);
        setupZoomOverviewWidth(paramDrawData, this.mWebView.getViewWidth());
        float f1 = getZoomOverviewScale();
        WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
        float f2;
        float f3;
        label193: boolean bool1;
        if ((!this.mMinZoomScaleFixed) || (localWebSettingsClassic.getUseWideViewPort()))
        {
            if (this.mInitialScale > 0.0F)
            {
                f2 = Math.min(this.mInitialScale, f1);
                this.mMinZoomScale = f2;
                this.mMaxZoomScale = Math.max(this.mMaxZoomScale, this.mMinZoomScale);
            }
        }
        else if (!this.mWebView.drawHistory())
        {
            if (this.mInitialScale <= 0.0F)
                break label291;
            f3 = this.mInitialScale;
            bool1 = false;
            if (!localViewState.mIsRestored)
            {
                if (localWebSettingsClassic.getUseFixedViewport())
                {
                    f3 = Math.max(f3, f1);
                    this.mTextWrapScale = Math.max(this.mTextWrapScale, f1);
                }
                bool1 = exceedsMinScaleIncrement(this.mTextWrapScale, f3);
            }
            if ((!localWebSettingsClassic.getLoadWithOverviewMode()) || (exceedsMinScaleIncrement(f3, f1)))
                break label420;
        }
        label291: label420: for (boolean bool2 = true; ; bool2 = false)
        {
            this.mInitialZoomOverview = bool2;
            setZoomScale(f3, bool1);
            updateZoomPicker();
            return;
            f2 = f1;
            break;
            if ((localViewState.mIsRestored) || (localViewState.mViewScale > 0.0F))
            {
                if (localViewState.mViewScale > 0.0F)
                {
                    f3 = localViewState.mViewScale;
                    label322: if (localViewState.mTextWrapScale <= 0.0F)
                        break label353;
                }
                label353: for (float f4 = localViewState.mTextWrapScale; ; f4 = getReadingLevelScale())
                {
                    this.mTextWrapScale = f4;
                    break;
                    f3 = f1;
                    break label322;
                }
            }
            f3 = f1;
            if ((!localWebSettingsClassic.getUseWideViewPort()) || (!localWebSettingsClassic.getLoadWithOverviewMode()))
                f3 = Math.max(this.mDefaultScale, f3);
            if ((!localWebSettingsClassic.isNarrowColumnLayout()) || (!localWebSettingsClassic.getUseFixedViewport()))
                break label193;
            this.mTextWrapScale = getReadingLevelScale();
            break label193;
        }
    }

    public boolean onNewPicture(WebViewCore.DrawData paramDrawData)
    {
        boolean bool1 = true;
        boolean bool2 = setupZoomOverviewWidth(paramDrawData, this.mWebView.getViewWidth());
        float f = getZoomOverviewScale();
        WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
        boolean bool3;
        boolean bool4;
        label162: boolean bool5;
        if ((bool2) && (localWebSettingsClassic.isNarrowColumnLayout()) && (localWebSettingsClassic.getUseFixedViewport()) && ((this.mInitialZoomOverview) || (this.mInZoomOverview)))
        {
            if ((exceedsMinScaleIncrement(this.mTextWrapScale, this.mDefaultScale)) || (exceedsMinScaleIncrement(f, this.mDefaultScale)))
                this.mTextWrapScale = getReadingLevelScale();
        }
        else
        {
            if ((!this.mMinZoomScaleFixed) || (localWebSettingsClassic.getUseWideViewPort()))
            {
                this.mMinZoomScale = f;
                this.mMaxZoomScale = Math.max(this.mMaxZoomScale, this.mMinZoomScale);
            }
            bool3 = exceedsMinScaleIncrement(f, this.mActualScale);
            if (f - this.mActualScale < MINIMUM_SCALE_INCREMENT)
                break label299;
            bool4 = bool1;
            if ((!this.mInZoomOverview) || (exceedsMinScaleIncrement(f, this.mDefaultScale)))
                break label305;
            bool5 = bool1;
            label184: if ((this.mWebView.drawHistory()) || (((!bool4) || (!localWebSettingsClassic.getUseWideViewPort())) && (((!this.mInitialZoomOverview) && (!bool5)) || (!bool3) || (!bool2))))
                break label316;
            this.mInitialZoomOverview = false;
            if ((willScaleTriggerZoom(this.mTextWrapScale)) || (this.mWebView.getSettings().getUseFixedViewport()))
                break label311;
        }
        while (true)
        {
            setZoomScale(f, bool1);
            if ((paramDrawData.mFirstLayoutForNonStandardLoad) && (localWebSettingsClassic.getLoadWithOverviewMode()))
                this.mInitialZoomOverview = this.mInZoomOverview;
            return bool3;
            this.mTextWrapScale = f;
            break;
            label299: bool4 = false;
            break label162;
            label305: bool5 = false;
            break label184;
            label311: bool1 = false;
        }
        label316: if (!bool3);
        while (true)
        {
            this.mInZoomOverview = bool1;
            break;
            bool1 = false;
        }
    }

    void onPageFinished(String paramString)
    {
        this.mInitialZoomOverview = false;
    }

    public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        boolean bool1 = true;
        if (!isFixedLengthAnimationInProgress())
        {
            int j = this.mWebView.getVisibleTitleHeight();
            this.mZoomCenterX = 0.0F;
            this.mZoomCenterY = j;
            this.mAnchorX = this.mWebView.viewToContentX(this.mWebView.getScrollX());
            this.mAnchorY = this.mWebView.viewToContentY(j + this.mWebView.getScrollY());
        }
        int i;
        if (!this.mMinZoomScaleFixed)
        {
            float f = this.mWebView.getViewWidth();
            if (!this.mWebView.drawHistory())
                break label216;
            i = this.mWebView.getHistoryPictureWidth();
            this.mMinZoomScale = Math.min(1.0F, f / i);
            if ((this.mInitialScale > 0.0F) && (this.mInitialScale < this.mMinZoomScale))
                this.mMinZoomScale = this.mInitialScale;
        }
        dismissZoomPicker();
        WebView localWebView = this.mWebView.getWebView();
        boolean bool2;
        label184: boolean bool3;
        if ((paramInt1 != paramInt3) && (!this.mWebView.getSettings().getUseFixedViewport()))
        {
            bool2 = bool1;
            bool3 = this.mInZoomOverview;
            if (paramInt1 >= paramInt3)
                break label231;
        }
        while (true)
        {
            localWebView.post(new PostScale(bool2, bool3, bool1));
            return;
            label216: i = this.mZoomOverviewWidth;
            break;
            bool2 = false;
            break label184;
            label231: bool1 = false;
        }
    }

    public void refreshZoomScale(boolean paramBoolean)
    {
        setZoomScale(this.mActualScale, paramBoolean, true);
    }

    public void restoreZoomState(Bundle paramBundle)
    {
        this.mActualScale = paramBundle.getFloat("scale", 1.0F);
        this.mInvActualScale = (1.0F / this.mActualScale);
        this.mTextWrapScale = paramBundle.getFloat("textwrapScale", this.mActualScale);
        this.mInZoomOverview = paramBundle.getBoolean("overview");
    }

    public void saveZoomState(Bundle paramBundle)
    {
        paramBundle.putFloat("scale", this.mActualScale);
        paramBundle.putFloat("textwrapScale", this.mTextWrapScale);
        paramBundle.putBoolean("overview", this.mInZoomOverview);
    }

    public void setHardwareAccelerated()
    {
        this.mHardwareAccelerated = true;
    }

    public final void setInitialScaleInPercent(int paramInt)
    {
        this.mInitialScale = (0.01F * paramInt);
    }

    public final void setZoomCenter(float paramFloat1, float paramFloat2)
    {
        this.mZoomCenterX = paramFloat1;
        this.mZoomCenterY = paramFloat2;
    }

    public void setZoomScale(float paramFloat, boolean paramBoolean)
    {
        setZoomScale(paramFloat, paramBoolean, false);
    }

    public boolean startZoomAnimation(float paramFloat, boolean paramBoolean)
    {
        boolean bool = true;
        this.mInitialZoomOverview = false;
        float f = this.mActualScale;
        this.mInitialScrollX = this.mWebView.getScrollX();
        this.mInitialScrollY = this.mWebView.getScrollY();
        if (!exceedsMinScaleIncrement(paramFloat, getReadingLevelScale()))
            paramFloat = getReadingLevelScale();
        setZoomScale(paramFloat, paramBoolean);
        if (f != this.mActualScale)
        {
            if (this.mHardwareAccelerated)
                this.mInHWAcceleratedZoom = bool;
            this.mZoomStart = SystemClock.uptimeMillis();
            this.mInvInitialZoomScale = (1.0F / f);
            this.mInvFinalZoomScale = (1.0F / this.mActualScale);
            this.mZoomScale = this.mActualScale;
            this.mWebView.onFixedLengthZoomAnimationStart();
            this.mWebView.invalidate();
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean supportsMultiTouchZoom()
    {
        return this.mSupportMultiTouch;
    }

    public boolean supportsPanDuringZoom()
    {
        return this.mAllowPanAndScale;
    }

    public void updateDefaultZoomDensity(float paramFloat)
    {
        assert (paramFloat > 0.0F);
        float f1;
        if (Math.abs(paramFloat - this.mDefaultScale) > MINIMUM_SCALE_INCREMENT)
        {
            f1 = this.mDefaultScale;
            this.mDisplayDensity = paramFloat;
            setDefaultZoomScale(paramFloat);
            if (f1 <= 0.0D)
                break label74;
        }
        label74: for (float f2 = paramFloat / f1; ; f2 = 1.0F)
        {
            setZoomScale(f2 * this.mActualScale, true);
            return;
        }
    }

    public void updateDoubleTapZoom(int paramInt)
    {
        int i;
        if (this.mTextWrapScale - this.mActualScale < 0.1F)
        {
            i = 1;
            this.mDoubleTapZoomFactor = (paramInt / 100.0F);
            this.mTextWrapScale = getReadingLevelScale();
            if (i == 0)
                break label58;
        }
        label58: for (float f = this.mTextWrapScale; ; f = Math.min(this.mTextWrapScale, this.mActualScale))
        {
            setZoomScale(f, true, true);
            return;
            i = 0;
            break;
        }
    }

    public void updateMultiTouchSupport(Context paramContext)
    {
        boolean bool1 = false;
        assert (this.mWebView.getSettings() != null);
        WebSettingsClassic localWebSettingsClassic = this.mWebView.getSettings();
        PackageManager localPackageManager = paramContext.getPackageManager();
        boolean bool2;
        if (((localPackageManager.hasSystemFeature("android.hardware.touchscreen.multitouch")) || (localPackageManager.hasSystemFeature("android.hardware.faketouch.multitouch.distinct"))) && (localWebSettingsClassic.supportZoom()) && (localWebSettingsClassic.getBuiltInZoomControls()))
        {
            bool2 = true;
            this.mSupportMultiTouch = bool2;
            if ((localPackageManager.hasSystemFeature("android.hardware.touchscreen.multitouch.distinct")) || (localPackageManager.hasSystemFeature("android.hardware.faketouch.multitouch.distinct")))
                bool1 = true;
            this.mAllowPanAndScale = bool1;
            if ((!this.mSupportMultiTouch) || (this.mScaleDetector != null))
                break label156;
        }
        for (this.mScaleDetector = new ScaleGestureDetector(paramContext, new ScaleDetectorListener(null)); ; this.mScaleDetector = null)
            label156: 
            do
            {
                return;
                bool2 = false;
                break;
            }
            while ((this.mSupportMultiTouch) || (this.mScaleDetector == null));
    }

    public void updateZoomPicker()
    {
        ZoomControlBase localZoomControlBase = getCurrentZoomControl();
        if (localZoomControlBase != null)
            localZoomControlBase.update();
    }

    public void updateZoomRange(WebViewCore.ViewState paramViewState, int paramInt1, int paramInt2)
    {
        if (paramViewState.mMinScale == 0.0F)
            if (paramViewState.mMobileSite)
                if (paramInt2 > Math.max(0, paramInt1))
                {
                    this.mMinZoomScale = (paramInt1 / paramInt2);
                    this.mMinZoomScaleFixed = false;
                    if (paramViewState.mMaxScale != 0.0F)
                        break label105;
                }
        label105: for (this.mMaxZoomScale = this.mDefaultMaxZoomScale; ; this.mMaxZoomScale = paramViewState.mMaxScale)
        {
            return;
            this.mMinZoomScale = paramViewState.mDefaultScale;
            this.mMinZoomScaleFixed = true;
            break;
            this.mMinZoomScale = this.mDefaultMinZoomScale;
            this.mMinZoomScaleFixed = false;
            break;
            this.mMinZoomScale = paramViewState.mMinScale;
            this.mMinZoomScaleFixed = true;
            break;
        }
    }

    public boolean willScaleTriggerZoom(float paramFloat)
    {
        return exceedsMinScaleIncrement(paramFloat, this.mActualScale);
    }

    public boolean zoomIn()
    {
        return zoom(1.25F);
    }

    public boolean zoomOut()
    {
        return zoom(0.8F);
    }

    private class PostScale
        implements Runnable
    {
        final boolean mInPortraitMode;
        final boolean mInZoomOverviewBeforeSizeChange;
        final boolean mUpdateTextWrap;

        public PostScale(boolean paramBoolean1, boolean paramBoolean2, boolean arg4)
        {
            this.mUpdateTextWrap = paramBoolean1;
            this.mInZoomOverviewBeforeSizeChange = paramBoolean2;
            boolean bool;
            this.mInPortraitMode = bool;
        }

        public void run()
        {
            if (ZoomManager.this.mWebView.getWebViewCore() != null)
            {
                float f = ZoomManager.this.mActualScale;
                if ((ZoomManager.this.mWebView.getSettings().getUseWideViewPort()) && (this.mInPortraitMode) && (this.mInZoomOverviewBeforeSizeChange))
                    f = ZoomManager.this.getZoomOverviewScale();
                ZoomManager.this.setZoomScale(f, this.mUpdateTextWrap, true);
                ZoomManager.this.updateZoomPicker();
            }
        }
    }

    private class ScaleDetectorListener
        implements ScaleGestureDetector.OnScaleGestureListener
    {
        private float mAccumulatedSpan;

        private ScaleDetectorListener()
        {
        }

        public boolean handleScale(ScaleGestureDetector paramScaleGestureDetector)
        {
            float f1 = paramScaleGestureDetector.getScaleFactor() * ZoomManager.this.mActualScale;
            boolean bool;
            float f2;
            float f3;
            label119: float f4;
            if ((ZoomManager.this.isScaleOverLimits(f1)) || (f1 < ZoomManager.this.getZoomOverviewScale()))
            {
                bool = true;
                f2 = Math.max(ZoomManager.this.computeScaleWithLimits(f1), ZoomManager.this.getZoomOverviewScale());
                if ((ZoomManager.this.mPinchToZoomAnimating) || (ZoomManager.this.willScaleTriggerZoom(f2)))
                {
                    ZoomManager.access$1002(ZoomManager.this, true);
                    if (f2 <= ZoomManager.this.mActualScale)
                        break label157;
                    f3 = Math.min(f2, 1.25F * ZoomManager.this.mActualScale);
                    f4 = ZoomManager.this.computeScaleWithLimits(f3);
                    if (Math.abs(f4 - ZoomManager.this.mActualScale) >= ZoomManager.MINIMUM_SCALE_WITHOUT_JITTER)
                        break label177;
                }
            }
            while (true)
            {
                return bool;
                bool = false;
                break;
                label157: f3 = Math.max(f2, 0.8F * ZoomManager.this.mActualScale);
                break label119;
                label177: ZoomManager.this.setZoomCenter(paramScaleGestureDetector.getFocusX(), paramScaleGestureDetector.getFocusY());
                ZoomManager.this.setZoomScale(f4, false);
                ZoomManager.this.mWebView.invalidate();
                bool = true;
            }
        }

        public boolean isPanningOnly(ScaleGestureDetector paramScaleGestureDetector)
        {
            float f1 = ZoomManager.this.mFocusX;
            float f2 = ZoomManager.this.mFocusY;
            ZoomManager.access$402(ZoomManager.this, paramScaleGestureDetector.getFocusX());
            ZoomManager.access$502(ZoomManager.this, paramScaleGestureDetector.getFocusY());
            float f3;
            float f4;
            boolean bool;
            if ((f1 == 0.0F) && (f2 == 0.0F))
            {
                f3 = 0.0F;
                ZoomManager.FocusMovementQueue.access$700(ZoomManager.this.mFocusMovementQueue, f3);
                f4 = paramScaleGestureDetector.getCurrentSpan() - paramScaleGestureDetector.getPreviousSpan() + this.mAccumulatedSpan;
                if (ZoomManager.FocusMovementQueue.access$800(ZoomManager.this.mFocusMovementQueue) <= Math.abs(f4))
                    break label171;
                bool = true;
                label105: if (!bool)
                    break label177;
            }
            label171: label177: for (this.mAccumulatedSpan = (f4 + this.mAccumulatedSpan); ; this.mAccumulatedSpan = 0.0F)
            {
                return bool;
                f3 = FloatMath.sqrt((ZoomManager.this.mFocusX - f1) * (ZoomManager.this.mFocusX - f1) + (ZoomManager.this.mFocusY - f2) * (ZoomManager.this.mFocusY - f2));
                break;
                bool = false;
                break label105;
            }
        }

        public boolean onScale(ScaleGestureDetector paramScaleGestureDetector)
        {
            if ((isPanningOnly(paramScaleGestureDetector)) || (handleScale(paramScaleGestureDetector)))
                ZoomManager.FocusMovementQueue.access$300(ZoomManager.this.mFocusMovementQueue);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean onScaleBegin(ScaleGestureDetector paramScaleGestureDetector)
        {
            ZoomManager.access$102(ZoomManager.this, false);
            ZoomManager.this.dismissZoomPicker();
            ZoomManager.FocusMovementQueue.access$300(ZoomManager.this.mFocusMovementQueue);
            ZoomManager.access$402(ZoomManager.this, paramScaleGestureDetector.getFocusX());
            ZoomManager.access$502(ZoomManager.this, paramScaleGestureDetector.getFocusY());
            ZoomManager.this.mWebView.mViewManager.startZoom();
            ZoomManager.this.mWebView.onPinchToZoomAnimationStart();
            this.mAccumulatedSpan = 0.0F;
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector paramScaleGestureDetector)
        {
            boolean bool1 = true;
            boolean bool2;
            ZoomManager localZoomManager;
            if (ZoomManager.this.mPinchToZoomAnimating)
            {
                ZoomManager.access$1002(ZoomManager.this, false);
                ZoomManager.access$1202(ZoomManager.this, ZoomManager.this.mWebView.viewToContentX((int)ZoomManager.this.mZoomCenterX + ZoomManager.this.mWebView.getScrollX()));
                ZoomManager.access$1402(ZoomManager.this, ZoomManager.this.mWebView.viewToContentY((int)ZoomManager.this.mZoomCenterY + ZoomManager.this.mWebView.getScrollY()));
                if ((ZoomManager.this.canZoomOut()) && (ZoomManager.this.mActualScale > 0.8D * ZoomManager.this.mTextWrapScale))
                    break label200;
                bool2 = bool1;
                localZoomManager = ZoomManager.this;
                if ((!bool2) || (ZoomManager.this.mWebView.getSettings().getUseFixedViewport()))
                    break label206;
            }
            while (true)
            {
                localZoomManager.refreshZoomScale(bool1);
                ZoomManager.this.mWebView.invalidate();
                ZoomManager.this.mWebView.mViewManager.endZoom();
                ZoomManager.this.mWebView.onPinchToZoomAnimationEnd(paramScaleGestureDetector);
                return;
                label200: bool2 = false;
                break;
                label206: bool1 = false;
            }
        }
    }

    private class FocusMovementQueue
    {
        private static final int QUEUE_CAPACITY = 5;
        private int mIndex = 0;
        private float[] mQueue = new float[5];
        private int mSize = 0;
        private float mSum = 0.0F;

        FocusMovementQueue()
        {
        }

        private void add(float paramFloat)
        {
            this.mSum = (paramFloat + this.mSum);
            if (this.mSize < 5)
                this.mSize = (1 + this.mSize);
            while (true)
            {
                this.mQueue[this.mIndex] = paramFloat;
                this.mIndex = ((1 + this.mIndex) % 5);
                return;
                this.mSum -= this.mQueue[this.mIndex];
            }
        }

        private void clear()
        {
            this.mSize = 0;
            this.mSum = 0.0F;
            this.mIndex = 0;
            for (int i = 0; i < 5; i++)
                this.mQueue[i] = 0.0F;
        }

        private float getSum()
        {
            return this.mSum;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ZoomManager
 * JD-Core Version:        0.6.2
 */