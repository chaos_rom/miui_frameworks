package android.webkit;

import android.net.ParseException;
import android.net.WebAddress;
import android.os.AsyncTask;
import android.util.Log;

class CookieManagerClassic extends CookieManager
{
    private static final String LOGTAG = "webkit";
    private static CookieManagerClassic sRef;
    private int mPendingCookieOperations = 0;

    static
    {
        if (!CookieManagerClassic.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    /** @deprecated */
    public static CookieManagerClassic getInstance()
    {
        try
        {
            if (sRef == null)
                sRef = new CookieManagerClassic();
            CookieManagerClassic localCookieManagerClassic = sRef;
            return localCookieManagerClassic;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static native boolean nativeAcceptCookie();

    private static native boolean nativeAcceptFileSchemeCookies();

    private static native void nativeFlushCookieStore();

    private static native String nativeGetCookie(String paramString, boolean paramBoolean);

    private static native boolean nativeHasCookies(boolean paramBoolean);

    private static native void nativeRemoveAllCookie();

    private static native void nativeRemoveExpiredCookie();

    private static native void nativeRemoveSessionCookie();

    private static native void nativeSetAcceptCookie(boolean paramBoolean);

    private static native void nativeSetAcceptFileSchemeCookies(boolean paramBoolean);

    private static native void nativeSetCookie(String paramString1, String paramString2, boolean paramBoolean);

    /** @deprecated */
    private void signalCookieOperationsComplete()
    {
        try
        {
            this.mPendingCookieOperations = (-1 + this.mPendingCookieOperations);
            if ((!$assertionsDisabled) && (this.mPendingCookieOperations <= -1))
                throw new AssertionError();
        }
        finally
        {
        }
        notify();
    }

    /** @deprecated */
    private void signalCookieOperationsStart()
    {
        try
        {
            this.mPendingCookieOperations = (1 + this.mPendingCookieOperations);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean acceptCookie()
    {
        try
        {
            boolean bool = nativeAcceptCookie();
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected boolean allowFileSchemeCookiesImpl()
    {
        return nativeAcceptFileSchemeCookies();
    }

    protected void flushCookieStore()
    {
        nativeFlushCookieStore();
    }

    /** @deprecated */
    public String getCookie(WebAddress paramWebAddress)
    {
        try
        {
            String str = nativeGetCookie(paramWebAddress.toString(), false);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getCookie(String paramString)
    {
        return getCookie(paramString, false);
    }

    public String getCookie(String paramString, boolean paramBoolean)
    {
        try
        {
            WebAddress localWebAddress = new WebAddress(paramString);
            str = nativeGetCookie(localWebAddress.toString(), paramBoolean);
            return str;
        }
        catch (ParseException localParseException)
        {
            while (true)
            {
                Log.e("webkit", "Bad address: " + paramString);
                String str = null;
            }
        }
    }

    /** @deprecated */
    public boolean hasCookies()
    {
        try
        {
            boolean bool = hasCookies(false);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean hasCookies(boolean paramBoolean)
    {
        try
        {
            boolean bool = nativeHasCookies(paramBoolean);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void removeAllCookie()
    {
        nativeRemoveAllCookie();
    }

    public void removeExpiredCookie()
    {
        nativeRemoveExpiredCookie();
    }

    public void removeSessionCookie()
    {
        signalCookieOperationsStart();
        new AsyncTask()
        {
            protected Void doInBackground(Void[] paramAnonymousArrayOfVoid)
            {
                CookieManagerClassic.access$000();
                CookieManagerClassic.this.signalCookieOperationsComplete();
                return null;
            }
        }
        .execute(new Void[0]);
    }

    /** @deprecated */
    public void setAcceptCookie(boolean paramBoolean)
    {
        try
        {
            nativeSetAcceptCookie(paramBoolean);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void setAcceptFileSchemeCookiesImpl(boolean paramBoolean)
    {
        nativeSetAcceptFileSchemeCookies(paramBoolean);
    }

    public void setCookie(String paramString1, String paramString2)
    {
        setCookie(paramString1, paramString2, false);
    }

    void setCookie(String paramString1, String paramString2, boolean paramBoolean)
    {
        try
        {
            WebAddress localWebAddress = new WebAddress(paramString1);
            nativeSetCookie(localWebAddress.toString(), paramString2, paramBoolean);
            return;
        }
        catch (ParseException localParseException)
        {
            while (true)
                Log.e("webkit", "Bad address: " + paramString1);
        }
    }

    void waitForCookieOperationsToComplete()
    {
        try
        {
            while (true)
            {
                int i = this.mPendingCookieOperations;
                if (i <= 0)
                    break;
                try
                {
                    wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            return;
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.CookieManagerClassic
 * JD-Core Version:        0.6.2
 */