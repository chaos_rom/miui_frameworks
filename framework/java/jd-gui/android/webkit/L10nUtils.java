package android.webkit;

import android.content.Context;
import android.content.res.Resources;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class L10nUtils
{
    private static Context mApplicationContext;
    private static int[] mIdsArray = arrayOfInt;
    private static Map<Integer, SoftReference<String>> mStrings;

    static
    {
        int[] arrayOfInt = new int[57];
        arrayOfInt[0] = 17040196;
        arrayOfInt[1] = 17040197;
        arrayOfInt[2] = 17040198;
        arrayOfInt[3] = 17040199;
        arrayOfInt[4] = 17040200;
        arrayOfInt[5] = 17040201;
        arrayOfInt[6] = 17040202;
        arrayOfInt[7] = 17040203;
        arrayOfInt[8] = 17040204;
        arrayOfInt[9] = 17040205;
        arrayOfInt[10] = 17040206;
        arrayOfInt[11] = 17040207;
        arrayOfInt[12] = 17040208;
        arrayOfInt[13] = 17040209;
        arrayOfInt[14] = 17040210;
        arrayOfInt[15] = 17040211;
        arrayOfInt[16] = 17040212;
        arrayOfInt[17] = 17040213;
        arrayOfInt[18] = 17040214;
        arrayOfInt[19] = 17040215;
        arrayOfInt[20] = 17040216;
        arrayOfInt[21] = 17040217;
        arrayOfInt[22] = 17040218;
        arrayOfInt[23] = 17040219;
        arrayOfInt[24] = 17040220;
        arrayOfInt[25] = 17040221;
        arrayOfInt[26] = 17040222;
        arrayOfInt[27] = 17040223;
        arrayOfInt[28] = 17040224;
        arrayOfInt[29] = 17040225;
        arrayOfInt[30] = 17040226;
        arrayOfInt[31] = 17040227;
        arrayOfInt[32] = 17040228;
        arrayOfInt[33] = 17040229;
        arrayOfInt[34] = 17040230;
        arrayOfInt[35] = 17040231;
        arrayOfInt[36] = 17040232;
        arrayOfInt[37] = 17040233;
        arrayOfInt[38] = 17040234;
        arrayOfInt[39] = 17040235;
        arrayOfInt[40] = 17040236;
        arrayOfInt[41] = 17040237;
        arrayOfInt[42] = 17040238;
        arrayOfInt[43] = 17040239;
        arrayOfInt[44] = 17040240;
        arrayOfInt[45] = 17040241;
        arrayOfInt[46] = 17040242;
        arrayOfInt[47] = 17040243;
        arrayOfInt[48] = 17040244;
        arrayOfInt[49] = 17040245;
        arrayOfInt[50] = 17040246;
        arrayOfInt[51] = 17040247;
        arrayOfInt[52] = 17040248;
        arrayOfInt[53] = 17040249;
        arrayOfInt[54] = 17040250;
        arrayOfInt[55] = 17040251;
        arrayOfInt[56] = 17040252;
    }

    public static String getLocalisedString(int paramInt)
    {
        String str;
        if (mStrings == null)
            str = loadString(paramInt);
        while (true)
        {
            return str;
            SoftReference localSoftReference = (SoftReference)mStrings.get(Integer.valueOf(paramInt));
            if ((localSoftReference == null) || (localSoftReference.get() == null));
            for (int i = 1; ; i = 0)
            {
                if (i == 0)
                    break label59;
                str = loadString(paramInt);
                break;
            }
            label59: str = (String)localSoftReference.get();
        }
    }

    private static String loadString(int paramInt)
    {
        if (mStrings == null)
            mStrings = new HashMap(mIdsArray.length);
        String str = mApplicationContext.getResources().getString(mIdsArray[paramInt]);
        mStrings.put(Integer.valueOf(paramInt), new SoftReference(str));
        return str;
    }

    public static void setApplicationContext(Context paramContext)
    {
        mApplicationContext = paramContext.getApplicationContext();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.L10nUtils
 * JD-Core Version:        0.6.2
 */