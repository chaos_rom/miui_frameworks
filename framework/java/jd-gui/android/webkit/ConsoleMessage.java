package android.webkit;

public class ConsoleMessage
{
    private MessageLevel mLevel;
    private int mLineNumber;
    private String mMessage;
    private String mSourceId;

    public ConsoleMessage(String paramString1, String paramString2, int paramInt, MessageLevel paramMessageLevel)
    {
        this.mMessage = paramString1;
        this.mSourceId = paramString2;
        this.mLineNumber = paramInt;
        this.mLevel = paramMessageLevel;
    }

    public int lineNumber()
    {
        return this.mLineNumber;
    }

    public String message()
    {
        return this.mMessage;
    }

    public MessageLevel messageLevel()
    {
        return this.mLevel;
    }

    public String sourceId()
    {
        return this.mSourceId;
    }

    public static enum MessageLevel
    {
        static
        {
            LOG = new MessageLevel("LOG", 1);
            WARNING = new MessageLevel("WARNING", 2);
            ERROR = new MessageLevel("ERROR", 3);
            DEBUG = new MessageLevel("DEBUG", 4);
            MessageLevel[] arrayOfMessageLevel = new MessageLevel[5];
            arrayOfMessageLevel[0] = TIP;
            arrayOfMessageLevel[1] = LOG;
            arrayOfMessageLevel[2] = WARNING;
            arrayOfMessageLevel[3] = ERROR;
            arrayOfMessageLevel[4] = DEBUG;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ConsoleMessage
 * JD-Core Version:        0.6.2
 */