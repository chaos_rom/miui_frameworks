package android.webkit;

import java.util.List;

public abstract interface SearchBox
{
    public abstract void addSearchBoxListener(SearchBoxListener paramSearchBoxListener);

    public abstract void isSupported(IsSupportedCallback paramIsSupportedCallback);

    public abstract void oncancel(SearchBoxListener paramSearchBoxListener);

    public abstract void onchange(SearchBoxListener paramSearchBoxListener);

    public abstract void onresize(SearchBoxListener paramSearchBoxListener);

    public abstract void onsubmit(SearchBoxListener paramSearchBoxListener);

    public abstract void removeSearchBoxListener(SearchBoxListener paramSearchBoxListener);

    public abstract void setDimensions(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    public abstract void setQuery(String paramString);

    public abstract void setSelection(int paramInt1, int paramInt2);

    public abstract void setVerbatim(boolean paramBoolean);

    public static abstract interface IsSupportedCallback
    {
        public abstract void searchBoxIsSupported(boolean paramBoolean);
    }

    public static abstract class SearchBoxListener
    {
        public void onCancelComplete(boolean paramBoolean)
        {
        }

        public void onChangeComplete(boolean paramBoolean)
        {
        }

        public void onResizeComplete(boolean paramBoolean)
        {
        }

        public void onSubmitComplete(boolean paramBoolean)
        {
        }

        public void onSuggestionsReceived(String paramString, List<String> paramList)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.SearchBox
 * JD-Core Version:        0.6.2
 */