package android.webkit;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.media.Metadata;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;

public class HTML5VideoFullScreen extends HTML5VideoView
    implements MediaController.MediaPlayerControl, MediaPlayer.OnPreparedListener, View.OnTouchListener
{
    static final int FULLSCREEN_OFF = 0;
    static final int FULLSCREEN_SURFACECREATED = 2;
    static final int FULLSCREEN_SURFACECREATING = 1;
    private static FrameLayout mLayout;
    private static View mProgressView;
    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener()
    {
        public void onBufferingUpdate(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt)
        {
            HTML5VideoFullScreen.access$1002(HTML5VideoFullScreen.this, paramAnonymousInt);
        }
    };
    private final WebChromeClient.CustomViewCallback mCallback = new WebChromeClient.CustomViewCallback()
    {
        public void onCustomViewHidden()
        {
            HTML5VideoFullScreen.this.mProxy.dispatchOnStopFullScreen();
            HTML5VideoFullScreen.mLayout.removeView(HTML5VideoFullScreen.this.getSurfaceView());
            if (HTML5VideoFullScreen.mProgressView != null)
            {
                HTML5VideoFullScreen.mLayout.removeView(HTML5VideoFullScreen.mProgressView);
                HTML5VideoFullScreen.access$902(null);
            }
            HTML5VideoFullScreen.access$802(null);
            HTML5VideoFullScreen.this.mProxy.getWebView().getViewManager().showAll();
            HTML5VideoFullScreen.this.mProxy = null;
            HTML5VideoFullScreen.access$202(HTML5VideoFullScreen.this, null);
            HTML5VideoView.mCurrentState = 4;
        }
    };
    private boolean mCanPause;
    private boolean mCanSeekBack;
    private boolean mCanSeekForward;
    private int mCurrentBufferPercentage;
    private int mFullScreenMode;
    private MediaController mMediaController;
    SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback()
    {
        public void surfaceChanged(SurfaceHolder paramAnonymousSurfaceHolder, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
        {
            if ((HTML5VideoView.mPlayer != null) && (HTML5VideoFullScreen.this.mMediaController != null) && (HTML5VideoView.mCurrentState == 2))
            {
                if (HTML5VideoFullScreen.this.mMediaController.isShowing())
                    HTML5VideoFullScreen.this.mMediaController.hide();
                HTML5VideoFullScreen.this.mMediaController.show();
            }
        }

        public void surfaceCreated(SurfaceHolder paramAnonymousSurfaceHolder)
        {
            HTML5VideoFullScreen.access$302(HTML5VideoFullScreen.this, paramAnonymousSurfaceHolder);
            HTML5VideoFullScreen.access$402(HTML5VideoFullScreen.this, 2);
            HTML5VideoFullScreen.this.prepareForFullScreen();
        }

        public void surfaceDestroyed(SurfaceHolder paramAnonymousSurfaceHolder)
        {
            HTML5VideoFullScreen.this.pauseAndDispatch(HTML5VideoFullScreen.this.mProxy);
            HTML5VideoView.mPlayer.release();
            HTML5VideoView.mPlayer = null;
            HTML5VideoFullScreen.access$302(HTML5VideoFullScreen.this, null);
            if (HTML5VideoFullScreen.this.mMediaController != null)
                HTML5VideoFullScreen.this.mMediaController.hide();
        }
    };
    MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener()
    {
        public void onVideoSizeChanged(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            HTML5VideoFullScreen.access$002(HTML5VideoFullScreen.this, paramAnonymousMediaPlayer.getVideoWidth());
            HTML5VideoFullScreen.access$102(HTML5VideoFullScreen.this, paramAnonymousMediaPlayer.getVideoHeight());
            if ((HTML5VideoFullScreen.this.mVideoWidth != 0) && (HTML5VideoFullScreen.this.mVideoHeight != 0))
                HTML5VideoFullScreen.this.mVideoSurfaceView.getHolder().setFixedSize(HTML5VideoFullScreen.this.mVideoWidth, HTML5VideoFullScreen.this.mVideoHeight);
        }
    };
    private SurfaceHolder mSurfaceHolder = null;
    private int mVideoHeight;
    private VideoSurfaceView mVideoSurfaceView;
    private int mVideoWidth;

    HTML5VideoFullScreen(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        this.mVideoSurfaceView = new VideoSurfaceView(paramContext);
        this.mFullScreenMode = 0;
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        init(paramInt1, paramInt2, paramBoolean);
    }

    private void attachMediaController()
    {
        if ((mPlayer != null) && (this.mMediaController != null))
        {
            this.mMediaController.setMediaPlayer(this);
            this.mMediaController.setAnchorView(this.mVideoSurfaceView);
            this.mMediaController.setEnabled(false);
        }
    }

    private SurfaceView getSurfaceView()
    {
        return this.mVideoSurfaceView;
    }

    private void prepareForFullScreen()
    {
        FullScreenMediaController localFullScreenMediaController = new FullScreenMediaController(this.mProxy.getContext(), mLayout);
        localFullScreenMediaController.setSystemUiVisibility(mLayout.getSystemUiVisibility());
        setMediaController(localFullScreenMediaController);
        mPlayer.setScreenOnWhilePlaying(true);
        mPlayer.setOnVideoSizeChangedListener(this.mSizeChangedListener);
        prepareDataAndDisplayMode(this.mProxy);
    }

    private void setMediaController(MediaController paramMediaController)
    {
        this.mMediaController = paramMediaController;
        attachMediaController();
    }

    private void toggleMediaControlsVisiblity()
    {
        if (this.mMediaController.isShowing())
            this.mMediaController.hide();
        while (true)
        {
            return;
            this.mMediaController.show();
        }
    }

    public boolean canPause()
    {
        return this.mCanPause;
    }

    public boolean canSeekBackward()
    {
        return this.mCanSeekBack;
    }

    public boolean canSeekForward()
    {
        return this.mCanSeekForward;
    }

    public void decideDisplayMode()
    {
        mPlayer.setDisplay(this.mSurfaceHolder);
    }

    public void enterFullScreenVideoState(int paramInt, HTML5VideoViewProxy paramHTML5VideoViewProxy, WebViewClassic paramWebViewClassic)
    {
        this.mFullScreenMode = 1;
        this.mCurrentBufferPercentage = 0;
        mPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
        this.mProxy = paramHTML5VideoViewProxy;
        this.mVideoSurfaceView.getHolder().addCallback(this.mSHCallback);
        this.mVideoSurfaceView.getHolder().setType(3);
        this.mVideoSurfaceView.setFocusable(true);
        this.mVideoSurfaceView.setFocusableInTouchMode(true);
        this.mVideoSurfaceView.requestFocus();
        mLayout = new FrameLayout(this.mProxy.getContext());
        FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
        mLayout.addView(getSurfaceView(), localLayoutParams);
        mLayout.setVisibility(0);
        WebChromeClient localWebChromeClient = paramWebViewClassic.getWebChromeClient();
        if (localWebChromeClient != null)
        {
            localWebChromeClient.onShowCustomView(mLayout, this.mCallback);
            if (paramWebViewClassic.getViewManager() != null)
                paramWebViewClassic.getViewManager().hideAll();
            mProgressView = localWebChromeClient.getVideoLoadingProgressView();
            if (mProgressView != null)
            {
                mLayout.addView(mProgressView, localLayoutParams);
                mProgressView.setVisibility(0);
            }
        }
    }

    public boolean fullScreenExited()
    {
        if (mLayout == null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int getBufferPercentage()
    {
        if (mPlayer != null);
        for (int i = this.mCurrentBufferPercentage; ; i = 0)
            return i;
    }

    public boolean isFullScreenMode()
    {
        return true;
    }

    public void onPrepared(MediaPlayer paramMediaPlayer)
    {
        super.onPrepared(paramMediaPlayer);
        this.mVideoSurfaceView.setOnTouchListener(this);
        Metadata localMetadata = paramMediaPlayer.getMetadata(false, false);
        boolean bool1;
        boolean bool2;
        label66: boolean bool3;
        if (localMetadata != null)
            if ((!localMetadata.has(1)) || (localMetadata.getBoolean(1)))
            {
                bool1 = true;
                this.mCanPause = bool1;
                if ((localMetadata.has(2)) && (!localMetadata.getBoolean(2)))
                    break label200;
                bool2 = true;
                this.mCanSeekBack = bool2;
                if ((localMetadata.has(3)) && (!localMetadata.getBoolean(3)))
                    break label206;
                bool3 = true;
                label91: this.mCanSeekForward = bool3;
            }
        while (true)
        {
            if (mProgressView != null)
                mProgressView.setVisibility(8);
            this.mVideoWidth = paramMediaPlayer.getVideoWidth();
            this.mVideoHeight = paramMediaPlayer.getVideoHeight();
            this.mVideoSurfaceView.getHolder().setFixedSize(this.mVideoWidth, this.mVideoHeight);
            this.mProxy.dispatchOnRestoreState();
            if (getStartWhenPrepared())
            {
                mPlayer.start();
                setStartWhenPrepared(false);
            }
            if (this.mMediaController != null)
            {
                this.mMediaController.setEnabled(true);
                this.mMediaController.show();
            }
            return;
            bool1 = false;
            break;
            label200: bool2 = false;
            break label66;
            label206: bool3 = false;
            break label91;
            this.mCanSeekForward = true;
            this.mCanSeekBack = true;
            this.mCanPause = true;
        }
    }

    public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
    {
        if ((this.mFullScreenMode >= 2) && (this.mMediaController != null))
            toggleMediaControlsVisiblity();
        return false;
    }

    public void showControllerInFullScreen()
    {
        if (this.mMediaController != null)
            this.mMediaController.show(0);
    }

    protected void switchProgressView(boolean paramBoolean)
    {
        if (mProgressView != null)
        {
            if (!paramBoolean)
                break label18;
            mProgressView.setVisibility(0);
        }
        while (true)
        {
            return;
            label18: mProgressView.setVisibility(8);
        }
    }

    static class FullScreenMediaController extends MediaController
    {
        View mVideoView;

        public FullScreenMediaController(Context paramContext, View paramView)
        {
            super();
            this.mVideoView = paramView;
        }

        public void hide()
        {
            if (this.mVideoView != null)
                this.mVideoView.setSystemUiVisibility(3);
            super.hide();
        }

        public void show()
        {
            super.show();
            if (this.mVideoView != null)
                this.mVideoView.setSystemUiVisibility(0);
        }
    }

    private class VideoSurfaceView extends SurfaceView
    {
        public VideoSurfaceView(Context arg2)
        {
            super();
        }

        protected void onMeasure(int paramInt1, int paramInt2)
        {
            int i = getDefaultSize(HTML5VideoFullScreen.this.mVideoWidth, paramInt1);
            int j = getDefaultSize(HTML5VideoFullScreen.this.mVideoHeight, paramInt2);
            if ((HTML5VideoFullScreen.this.mVideoWidth > 0) && (HTML5VideoFullScreen.this.mVideoHeight > 0))
            {
                if (j * HTML5VideoFullScreen.this.mVideoWidth <= i * HTML5VideoFullScreen.this.mVideoHeight)
                    break label94;
                j = i * HTML5VideoFullScreen.this.mVideoHeight / HTML5VideoFullScreen.this.mVideoWidth;
            }
            while (true)
            {
                setMeasuredDimension(i, j);
                return;
                label94: if (j * HTML5VideoFullScreen.this.mVideoWidth < i * HTML5VideoFullScreen.this.mVideoHeight)
                    i = j * HTML5VideoFullScreen.this.mVideoWidth / HTML5VideoFullScreen.this.mVideoHeight;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.HTML5VideoFullScreen
 * JD-Core Version:        0.6.2
 */