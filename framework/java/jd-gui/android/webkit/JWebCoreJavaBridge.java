package android.webkit;

import android.net.ProxyProperties;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Set;

final class JWebCoreJavaBridge extends Handler
{
    private static final int FUNCPTR_MESSAGE = 2;
    private static final String LOGTAG = "webkit-timers";
    static final int REFRESH_PLUGINS = 100;
    private static final int TIMER_MESSAGE = 1;
    private static WeakReference<WebViewClassic> sCurrentMainWebView = new WeakReference(null);
    private HashMap<String, String> mContentUriToFilePathMap;
    private boolean mHasDeferredTimers;
    private boolean mHasInstantTimer;
    private int mNativeBridge;
    private boolean mTimerPaused;

    public JWebCoreJavaBridge()
    {
        nativeConstructor();
    }

    private String cookies(String paramString)
    {
        return CookieManager.getInstance().getCookie(paramString);
    }

    private boolean cookiesEnabled()
    {
        return CookieManager.getInstance().acceptCookie();
    }

    private void fireSharedTimer()
    {
        this.mHasInstantTimer = false;
        sharedTimerFired();
    }

    private String[] getKeyStrengthList()
    {
        return CertTool.getKeyStrengthList();
    }

    private String[] getPluginDirectories()
    {
        return PluginManager.getInstance(null).getPluginDirectories();
    }

    private String getPluginSharedDataDirectory()
    {
        return PluginManager.getInstance(null).getPluginSharedDataDirectory();
    }

    /** @deprecated */
    private String getSignedPublicKey(int paramInt, String paramString1, String paramString2)
    {
        try
        {
            WebViewClassic localWebViewClassic = (WebViewClassic)sCurrentMainWebView.get();
            String str2;
            if (localWebViewClassic != null)
                str2 = CertTool.getSignedPublicKey(localWebViewClassic.getContext(), paramInt, paramString1);
            for (String str1 = str2; ; str1 = "")
            {
                return str1;
                Log.e("webkit-timers", "There is no active WebView for getSignedPublicKey");
            }
        }
        finally
        {
        }
    }

    private native void nativeConstructor();

    private native void nativeFinalize();

    private native void nativeServiceFuncPtrQueue();

    private native void nativeUpdatePluginDirectories(String[] paramArrayOfString, boolean paramBoolean);

    /** @deprecated */
    static void removeActiveWebView(WebViewClassic paramWebViewClassic)
    {
        try
        {
            Object localObject2 = sCurrentMainWebView.get();
            if (localObject2 != paramWebViewClassic);
            while (true)
            {
                return;
                sCurrentMainWebView.clear();
            }
        }
        finally
        {
        }
    }

    private String resolveFilePathForContentUri(String paramString)
    {
        String str;
        if (this.mContentUriToFilePathMap != null)
        {
            str = (String)this.mContentUriToFilePathMap.get(paramString);
            if (str == null);
        }
        while (true)
        {
            return str;
            str = Uri.parse(paramString).getLastPathSegment();
        }
    }

    /** @deprecated */
    static void setActiveWebView(WebViewClassic paramWebViewClassic)
    {
        try
        {
            Object localObject2 = sCurrentMainWebView.get();
            if (localObject2 != null);
            while (true)
            {
                return;
                sCurrentMainWebView = new WeakReference(paramWebViewClassic);
            }
        }
        finally
        {
        }
    }

    private void setCookies(String paramString1, String paramString2)
    {
        if ((paramString2.contains("\r")) || (paramString2.contains("\n")))
        {
            int i = paramString2.length();
            StringBuilder localStringBuilder = new StringBuilder(i);
            int j = 0;
            if ((j != -1) && (j < i))
            {
                int k = paramString2.indexOf('\r', j);
                int m = paramString2.indexOf('\n', j);
                int n;
                if (k == -1)
                {
                    n = m;
                    label80: if (n <= j)
                        break label145;
                    localStringBuilder.append(paramString2.subSequence(j, n));
                }
                label145: 
                while (n != -1)
                {
                    j = n + 1;
                    break;
                    if (m == -1)
                    {
                        n = k;
                        break label80;
                    }
                    if (k < m)
                    {
                        n = k;
                        break label80;
                    }
                    n = m;
                    break label80;
                }
                localStringBuilder.append(paramString2.subSequence(j, i));
            }
            paramString2 = localStringBuilder.toString();
        }
        CookieManager.getInstance().setCookie(paramString1, paramString2);
    }

    private void setSharedTimer(long paramLong)
    {
        if (paramLong <= 0L)
            if (!this.mHasInstantTimer);
        while (true)
        {
            return;
            this.mHasInstantTimer = true;
            sendMessageDelayed(obtainMessage(1), paramLong);
            continue;
            sendMessageDelayed(obtainMessage(1), paramLong);
        }
    }

    private native void sharedTimerFired();

    private void signalServiceFuncPtrQueue()
    {
        sendMessage(obtainMessage(2));
    }

    private void stopSharedTimer()
    {
        removeMessages(1);
        this.mHasInstantTimer = false;
        this.mHasDeferredTimers = false;
    }

    public native void addPackageName(String paramString);

    public native void addPackageNames(Set<String> paramSet);

    protected void finalize()
    {
        nativeFinalize();
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 1:
        case 2:
        case 100:
        }
        while (true)
        {
            return;
            if (this.mTimerPaused)
            {
                this.mHasDeferredTimers = true;
            }
            else
            {
                fireSharedTimer();
                continue;
                nativeServiceFuncPtrQueue();
                continue;
                nativeUpdatePluginDirectories(PluginManager.getInstance(null).getPluginDirectories(), ((Boolean)paramMessage.obj).booleanValue());
            }
        }
    }

    public native void nativeUpdateProxy(String paramString1, String paramString2);

    public void pause()
    {
        if (!this.mTimerPaused)
        {
            this.mTimerPaused = true;
            this.mHasDeferredTimers = false;
        }
    }

    public native void removePackageName(String paramString);

    public void resume()
    {
        if (this.mTimerPaused)
        {
            this.mTimerPaused = false;
            if (this.mHasDeferredTimers)
            {
                this.mHasDeferredTimers = false;
                fireSharedTimer();
            }
        }
    }

    public native void setCacheSize(int paramInt);

    public native void setNetworkOnLine(boolean paramBoolean);

    public native void setNetworkType(String paramString1, String paramString2);

    public void storeFilePathForContentUri(String paramString1, String paramString2)
    {
        if (this.mContentUriToFilePathMap == null)
            this.mContentUriToFilePathMap = new HashMap();
        this.mContentUriToFilePathMap.put(paramString2, paramString1);
    }

    public void updateProxy(ProxyProperties paramProxyProperties)
    {
        if (paramProxyProperties == null)
            nativeUpdateProxy("", "");
        while (true)
        {
            return;
            String str = paramProxyProperties.getHost();
            int i = paramProxyProperties.getPort();
            if (i != 0)
                str = str + ":" + i;
            nativeUpdateProxy(str, paramProxyProperties.getExclusionList());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.JWebCoreJavaBridge
 * JD-Core Version:        0.6.2
 */