package android.webkit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

final class WebViewDatabaseClassic extends WebViewDatabase
{
    private static final String CACHE_DATABASE_FILE = "webviewCache.db";
    private static final String DATABASE_FILE = "webview.db";
    private static final int DATABASE_VERSION = 11;
    private static final String FORMDATA_NAME_COL = "name";
    private static final String FORMDATA_URLID_COL = "urlid";
    private static final String FORMDATA_VALUE_COL = "value";
    private static final String FORMURL_URL_COL = "url";
    private static final String HTTPAUTH_HOST_COL = "host";
    private static final String HTTPAUTH_PASSWORD_COL = "password";
    private static final String HTTPAUTH_REALM_COL = "realm";
    private static final String HTTPAUTH_USERNAME_COL = "username";
    private static final String ID_COL = "_id";
    private static final String[] ID_PROJECTION = arrayOfString2;
    private static final String LOGTAG = "WebViewDatabaseClassic";
    private static final String PASSWORD_HOST_COL = "host";
    private static final String PASSWORD_PASSWORD_COL = "password";
    private static final String PASSWORD_USERNAME_COL = "username";
    private static final int TABLE_FORMDATA_ID = 2;
    private static final int TABLE_FORMURL_ID = 1;
    private static final int TABLE_HTTPAUTH_ID = 3;
    private static final int TABLE_PASSWORD_ID;
    private static final String[] mTableNames;
    private static SQLiteDatabase sDatabase;
    private static WebViewDatabaseClassic sInstance = null;
    private final Object mFormLock = new Object();
    private final Object mHttpAuthLock = new Object();
    private boolean mInitialized = false;
    private final Object mPasswordLock = new Object();

    static
    {
        sDatabase = null;
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "password";
        arrayOfString1[1] = "formurl";
        arrayOfString1[2] = "formdata";
        arrayOfString1[3] = "httpauth";
        mTableNames = arrayOfString1;
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = "_id";
    }

    WebViewDatabaseClassic(final Context paramContext)
    {
        new Thread()
        {
            public void run()
            {
                WebViewDatabaseClassic.this.init(paramContext);
            }
        }
        .start();
    }

    private boolean checkInitialized()
    {
        try
        {
            while (true)
            {
                boolean bool1 = this.mInitialized;
                if (bool1)
                    break;
                try
                {
                    wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                    Log.e("WebViewDatabaseClassic", "Caught exception while checking initialization");
                    Log.e("WebViewDatabaseClassic", Log.getStackTraceString(localInterruptedException));
                }
            }
        }
        finally
        {
        }
        if (sDatabase != null);
        for (boolean bool2 = true; ; bool2 = false)
            return bool2;
    }

    /** @deprecated */
    public static WebViewDatabaseClassic getInstance(Context paramContext)
    {
        try
        {
            if (sInstance == null)
                sInstance = new WebViewDatabaseClassic(paramContext);
            WebViewDatabaseClassic localWebViewDatabaseClassic = sInstance;
            return localWebViewDatabaseClassic;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private boolean hasEntries(int paramInt)
    {
        boolean bool1 = false;
        if (!checkInitialized());
        while (true)
        {
            return bool1;
            Cursor localCursor = null;
            boolean bool2 = false;
            try
            {
                localCursor = sDatabase.query(mTableNames[paramInt], ID_PROJECTION, null, null, null, null, null);
                boolean bool3 = localCursor.moveToFirst();
                if (bool3 == true);
                for (bool2 = true; ; bool2 = false)
                {
                    if (localCursor != null)
                        localCursor.close();
                    bool1 = bool2;
                    break;
                }
            }
            catch (IllegalStateException localIllegalStateException)
            {
                while (true)
                {
                    Log.e("WebViewDatabaseClassic", "hasEntries", localIllegalStateException);
                    if (localCursor == null);
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
        }
    }

    /** @deprecated */
    private void init(Context paramContext)
    {
        try
        {
            boolean bool = this.mInitialized;
            if (bool);
            while (true)
            {
                return;
                initDatabase(paramContext);
                paramContext.deleteDatabase("webviewCache.db");
                this.mInitialized = true;
                notify();
            }
        }
        finally
        {
        }
    }

    private void initDatabase(Context paramContext)
    {
        try
        {
            sDatabase = paramContext.openOrCreateDatabase("webview.db", 0, null);
            if (sDatabase == null)
            {
                this.mInitialized = true;
                notify();
                return;
            }
        }
        catch (SQLiteException localSQLiteException)
        {
            while (true)
            {
                if (!paramContext.deleteDatabase("webview.db"))
                    continue;
                sDatabase = paramContext.openOrCreateDatabase("webview.db", 0, null);
                continue;
                if (sDatabase.getVersion() == 11)
                    continue;
                sDatabase.beginTransactionNonExclusive();
                try
                {
                    upgradeDatabase();
                    sDatabase.setTransactionSuccessful();
                    sDatabase.endTransaction();
                }
                finally
                {
                    sDatabase.endTransaction();
                }
            }
        }
    }

    private static void upgradeDatabase()
    {
        upgradeDatabaseToV10();
        upgradeDatabaseFromV10ToV11();
        sDatabase.setVersion(11);
    }

    private static void upgradeDatabaseFromV10ToV11()
    {
        if (sDatabase.getVersion() >= 11);
        while (true)
        {
            return;
            sDatabase.execSQL("DROP TABLE IF EXISTS cookies");
            sDatabase.execSQL("DROP TABLE IF EXISTS cache");
            Cursor localCursor = sDatabase.query(mTableNames[1], null, null, null, null, null, null);
            while (localCursor.moveToNext())
            {
                String str1 = Long.toString(localCursor.getLong(localCursor.getColumnIndex("_id")));
                String str2 = localCursor.getString(localCursor.getColumnIndex("url"));
                ContentValues localContentValues = new ContentValues(1);
                localContentValues.put("url", WebTextView.urlForAutoCompleteData(str2));
                SQLiteDatabase localSQLiteDatabase = sDatabase;
                String str3 = mTableNames[1];
                String[] arrayOfString = new String[1];
                arrayOfString[0] = str1;
                localSQLiteDatabase.update(str3, localContentValues, "_id=?", arrayOfString);
            }
            localCursor.close();
        }
    }

    private static void upgradeDatabaseToV10()
    {
        int i = sDatabase.getVersion();
        if (i >= 10);
        while (true)
        {
            return;
            if (i != 0)
                Log.i("WebViewDatabaseClassic", "Upgrading database from version " + i + " to " + 11 + ", which will destroy old data");
            if (9 == i)
            {
                sDatabase.execSQL("DROP TABLE IF EXISTS " + mTableNames[3]);
                sDatabase.execSQL("CREATE TABLE " + mTableNames[3] + " (" + "_id" + " INTEGER PRIMARY KEY, " + "host" + " TEXT, " + "realm" + " TEXT, " + "username" + " TEXT, " + "password" + " TEXT," + " UNIQUE (" + "host" + ", " + "realm" + ") ON CONFLICT REPLACE);");
            }
            else
            {
                sDatabase.execSQL("DROP TABLE IF EXISTS cookies");
                sDatabase.execSQL("DROP TABLE IF EXISTS cache");
                sDatabase.execSQL("DROP TABLE IF EXISTS " + mTableNames[1]);
                sDatabase.execSQL("DROP TABLE IF EXISTS " + mTableNames[2]);
                sDatabase.execSQL("DROP TABLE IF EXISTS " + mTableNames[3]);
                sDatabase.execSQL("DROP TABLE IF EXISTS " + mTableNames[0]);
                sDatabase.execSQL("CREATE TABLE " + mTableNames[1] + " (" + "_id" + " INTEGER PRIMARY KEY, " + "url" + " TEXT" + ");");
                sDatabase.execSQL("CREATE TABLE " + mTableNames[2] + " (" + "_id" + " INTEGER PRIMARY KEY, " + "urlid" + " INTEGER, " + "name" + " TEXT, " + "value" + " TEXT," + " UNIQUE (" + "urlid" + ", " + "name" + ", " + "value" + ") ON CONFLICT IGNORE);");
                sDatabase.execSQL("CREATE TABLE " + mTableNames[3] + " (" + "_id" + " INTEGER PRIMARY KEY, " + "host" + " TEXT, " + "realm" + " TEXT, " + "username" + " TEXT, " + "password" + " TEXT," + " UNIQUE (" + "host" + ", " + "realm" + ") ON CONFLICT REPLACE);");
                sDatabase.execSQL("CREATE TABLE " + mTableNames[0] + " (" + "_id" + " INTEGER PRIMARY KEY, " + "host" + " TEXT, " + "username" + " TEXT, " + "password" + " TEXT," + " UNIQUE (" + "host" + ", " + "username" + ") ON CONFLICT REPLACE);");
            }
        }
    }

    public void clearFormData()
    {
        if (!checkInitialized());
        while (true)
        {
            return;
            synchronized (this.mFormLock)
            {
                sDatabase.delete(mTableNames[1], null, null);
                sDatabase.delete(mTableNames[2], null, null);
            }
        }
    }

    public void clearHttpAuthUsernamePassword()
    {
        if (!checkInitialized());
        while (true)
        {
            return;
            synchronized (this.mHttpAuthLock)
            {
                sDatabase.delete(mTableNames[3], null, null);
            }
        }
    }

    public void clearUsernamePassword()
    {
        if (!checkInitialized());
        while (true)
        {
            return;
            synchronized (this.mPasswordLock)
            {
                sDatabase.delete(mTableNames[0], null, null);
            }
        }
    }

    // ERROR //
    java.util.ArrayList<String> getFormData(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: new 311	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 312	java/util/ArrayList:<init>	()V
        //     7: astore_3
        //     8: aload_1
        //     9: ifnull +14 -> 23
        //     12: aload_2
        //     13: ifnull +10 -> 23
        //     16: aload_0
        //     17: invokespecial 141	android/webkit/WebViewDatabaseClassic:checkInitialized	()Z
        //     20: ifne +5 -> 25
        //     23: aload_3
        //     24: areturn
        //     25: aload_0
        //     26: getfield 98	android/webkit/WebViewDatabaseClassic:mFormLock	Ljava/lang/Object;
        //     29: astore 4
        //     31: aload 4
        //     33: monitorenter
        //     34: aconst_null
        //     35: astore 5
        //     37: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     40: astore 10
        //     42: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     45: iconst_1
        //     46: aaload
        //     47: astore 11
        //     49: getstatic 87	android/webkit/WebViewDatabaseClassic:ID_PROJECTION	[Ljava/lang/String;
        //     52: astore 12
        //     54: iconst_1
        //     55: anewarray 77	java/lang/String
        //     58: astore 13
        //     60: aload 13
        //     62: iconst_0
        //     63: aload_1
        //     64: aastore
        //     65: aload 10
        //     67: aload 11
        //     69: aload 12
        //     71: ldc_w 314
        //     74: aload 13
        //     76: aconst_null
        //     77: aconst_null
        //     78: aconst_null
        //     79: invokevirtual 147	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     82: astore 5
        //     84: aload 5
        //     86: invokeinterface 214 1 0
        //     91: ifeq +258 -> 349
        //     94: aload 5
        //     96: aload 5
        //     98: ldc 43
        //     100: invokeinterface 218 2 0
        //     105: invokeinterface 222 2 0
        //     110: lstore 14
        //     112: aconst_null
        //     113: astore 16
        //     115: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     118: astore 20
        //     120: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     123: iconst_2
        //     124: aaload
        //     125: astore 21
        //     127: iconst_2
        //     128: anewarray 77	java/lang/String
        //     131: astore 22
        //     133: aload 22
        //     135: iconst_0
        //     136: ldc 43
        //     138: aastore
        //     139: aload 22
        //     141: iconst_1
        //     142: ldc 25
        //     144: aastore
        //     145: iconst_2
        //     146: anewarray 77	java/lang/String
        //     149: astore 23
        //     151: aload 23
        //     153: iconst_0
        //     154: lload 14
        //     156: invokestatic 228	java/lang/Long:toString	(J)Ljava/lang/String;
        //     159: aastore
        //     160: aload 23
        //     162: iconst_1
        //     163: aload_2
        //     164: aastore
        //     165: aload 20
        //     167: aload 21
        //     169: aload 22
        //     171: ldc_w 316
        //     174: aload 23
        //     176: aconst_null
        //     177: aconst_null
        //     178: aconst_null
        //     179: invokevirtual 147	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     182: astore 16
        //     184: aload 16
        //     186: invokeinterface 152 1 0
        //     191: ifeq +42 -> 233
        //     194: aload 16
        //     196: ldc 25
        //     198: invokeinterface 218 2 0
        //     203: istore 24
        //     205: aload_3
        //     206: aload 16
        //     208: iload 24
        //     210: invokeinterface 232 2 0
        //     215: invokevirtual 320	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     218: pop
        //     219: aload 16
        //     221: invokeinterface 214 1 0
        //     226: istore 26
        //     228: iload 26
        //     230: ifne -25 -> 205
        //     233: aload 16
        //     235: ifnull -151 -> 84
        //     238: aload 16
        //     240: invokeinterface 155 1 0
        //     245: goto -161 -> 84
        //     248: astore 8
        //     250: ldc 48
        //     252: ldc_w 322
        //     255: aload 8
        //     257: invokestatic 159	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     260: pop
        //     261: aload 5
        //     263: ifnull +10 -> 273
        //     266: aload 5
        //     268: invokeinterface 155 1 0
        //     273: aload 4
        //     275: monitorexit
        //     276: goto -253 -> 23
        //     279: astore 7
        //     281: aload 4
        //     283: monitorexit
        //     284: aload 7
        //     286: athrow
        //     287: astore 18
        //     289: ldc 48
        //     291: ldc_w 324
        //     294: aload 18
        //     296: invokestatic 159	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     299: pop
        //     300: aload 16
        //     302: ifnull -218 -> 84
        //     305: aload 16
        //     307: invokeinterface 155 1 0
        //     312: goto -228 -> 84
        //     315: astore 6
        //     317: aload 5
        //     319: ifnull +10 -> 329
        //     322: aload 5
        //     324: invokeinterface 155 1 0
        //     329: aload 6
        //     331: athrow
        //     332: astore 17
        //     334: aload 16
        //     336: ifnull +10 -> 346
        //     339: aload 16
        //     341: invokeinterface 155 1 0
        //     346: aload 17
        //     348: athrow
        //     349: aload 5
        //     351: ifnull -78 -> 273
        //     354: aload 5
        //     356: invokeinterface 155 1 0
        //     361: goto -88 -> 273
        //
        // Exception table:
        //     from	to	target	type
        //     37	112	248	java/lang/IllegalStateException
        //     238	245	248	java/lang/IllegalStateException
        //     305	312	248	java/lang/IllegalStateException
        //     339	349	248	java/lang/IllegalStateException
        //     266	284	279	finally
        //     322	332	279	finally
        //     354	361	279	finally
        //     115	228	287	java/lang/IllegalStateException
        //     37	112	315	finally
        //     238	245	315	finally
        //     250	261	315	finally
        //     305	312	315	finally
        //     339	349	315	finally
        //     115	228	332	finally
        //     289	300	332	finally
    }

    // ERROR //
    String[] getHttpAuthUsernamePassword(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_1
        //     3: ifnull +14 -> 17
        //     6: aload_2
        //     7: ifnull +10 -> 17
        //     10: aload_0
        //     11: invokespecial 141	android/webkit/WebViewDatabaseClassic:checkInitialized	()Z
        //     14: ifne +5 -> 19
        //     17: aload_3
        //     18: areturn
        //     19: iconst_2
        //     20: anewarray 77	java/lang/String
        //     23: astore 4
        //     25: aload 4
        //     27: iconst_0
        //     28: ldc 40
        //     30: aastore
        //     31: aload 4
        //     33: iconst_1
        //     34: ldc 34
        //     36: aastore
        //     37: aload_0
        //     38: getfield 100	android/webkit/WebViewDatabaseClassic:mHttpAuthLock	Ljava/lang/Object;
        //     41: astore 5
        //     43: aload 5
        //     45: monitorenter
        //     46: aconst_null
        //     47: astore_3
        //     48: aconst_null
        //     49: astore 6
        //     51: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     54: astore 11
        //     56: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     59: iconst_3
        //     60: aaload
        //     61: astore 12
        //     63: iconst_2
        //     64: anewarray 77	java/lang/String
        //     67: astore 13
        //     69: aload 13
        //     71: iconst_0
        //     72: aload_1
        //     73: aastore
        //     74: aload 13
        //     76: iconst_1
        //     77: aload_2
        //     78: aastore
        //     79: aload 11
        //     81: aload 12
        //     83: aload 4
        //     85: ldc_w 328
        //     88: aload 13
        //     90: aconst_null
        //     91: aconst_null
        //     92: aconst_null
        //     93: invokevirtual 147	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     96: astore 6
        //     98: aload 6
        //     100: invokeinterface 152 1 0
        //     105: ifeq +46 -> 151
        //     108: iconst_2
        //     109: anewarray 77	java/lang/String
        //     112: astore_3
        //     113: aload_3
        //     114: iconst_0
        //     115: aload 6
        //     117: aload 6
        //     119: ldc 40
        //     121: invokeinterface 218 2 0
        //     126: invokeinterface 232 2 0
        //     131: aastore
        //     132: aload_3
        //     133: iconst_1
        //     134: aload 6
        //     136: aload 6
        //     138: ldc 34
        //     140: invokeinterface 218 2 0
        //     145: invokeinterface 232 2 0
        //     150: aastore
        //     151: aload 6
        //     153: ifnull +10 -> 163
        //     156: aload 6
        //     158: invokeinterface 155 1 0
        //     163: aload 5
        //     165: monitorexit
        //     166: goto -149 -> 17
        //     169: astore 8
        //     171: aload 5
        //     173: monitorexit
        //     174: aload 8
        //     176: athrow
        //     177: astore 9
        //     179: ldc 48
        //     181: ldc_w 329
        //     184: aload 9
        //     186: invokestatic 159	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     189: pop
        //     190: aload 6
        //     192: ifnull -29 -> 163
        //     195: aload 6
        //     197: invokeinterface 155 1 0
        //     202: goto -39 -> 163
        //     205: astore 7
        //     207: aload 6
        //     209: ifnull +10 -> 219
        //     212: aload 6
        //     214: invokeinterface 155 1 0
        //     219: aload 7
        //     221: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     156	174	169	finally
        //     195	222	169	finally
        //     51	151	177	java/lang/IllegalStateException
        //     51	151	205	finally
        //     179	190	205	finally
    }

    // ERROR //
    String[] getUsernamePassword(String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_1
        //     3: ifnull +10 -> 13
        //     6: aload_0
        //     7: invokespecial 141	android/webkit/WebViewDatabaseClassic:checkInitialized	()Z
        //     10: ifne +5 -> 15
        //     13: aload_2
        //     14: areturn
        //     15: iconst_2
        //     16: anewarray 77	java/lang/String
        //     19: astore_3
        //     20: aload_3
        //     21: iconst_0
        //     22: ldc 40
        //     24: aastore
        //     25: aload_3
        //     26: iconst_1
        //     27: ldc 34
        //     29: aastore
        //     30: aload_0
        //     31: getfield 96	android/webkit/WebViewDatabaseClassic:mPasswordLock	Ljava/lang/Object;
        //     34: astore 4
        //     36: aload 4
        //     38: monitorenter
        //     39: aconst_null
        //     40: astore_2
        //     41: aconst_null
        //     42: astore 5
        //     44: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     47: astore 10
        //     49: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     52: iconst_0
        //     53: aaload
        //     54: astore 11
        //     56: iconst_1
        //     57: anewarray 77	java/lang/String
        //     60: astore 12
        //     62: aload 12
        //     64: iconst_0
        //     65: aload_1
        //     66: aastore
        //     67: aload 10
        //     69: aload 11
        //     71: aload_3
        //     72: ldc_w 333
        //     75: aload 12
        //     77: aconst_null
        //     78: aconst_null
        //     79: aconst_null
        //     80: invokevirtual 147	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     83: astore 5
        //     85: aload 5
        //     87: invokeinterface 152 1 0
        //     92: ifeq +46 -> 138
        //     95: iconst_2
        //     96: anewarray 77	java/lang/String
        //     99: astore_2
        //     100: aload_2
        //     101: iconst_0
        //     102: aload 5
        //     104: aload 5
        //     106: ldc 40
        //     108: invokeinterface 218 2 0
        //     113: invokeinterface 232 2 0
        //     118: aastore
        //     119: aload_2
        //     120: iconst_1
        //     121: aload 5
        //     123: aload 5
        //     125: ldc 34
        //     127: invokeinterface 218 2 0
        //     132: invokeinterface 232 2 0
        //     137: aastore
        //     138: aload 5
        //     140: ifnull +10 -> 150
        //     143: aload 5
        //     145: invokeinterface 155 1 0
        //     150: aload 4
        //     152: monitorexit
        //     153: goto -140 -> 13
        //     156: astore 7
        //     158: aload 4
        //     160: monitorexit
        //     161: aload 7
        //     163: athrow
        //     164: astore 8
        //     166: ldc 48
        //     168: ldc_w 334
        //     171: aload 8
        //     173: invokestatic 159	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     176: pop
        //     177: aload 5
        //     179: ifnull -29 -> 150
        //     182: aload 5
        //     184: invokeinterface 155 1 0
        //     189: goto -39 -> 150
        //     192: astore 6
        //     194: aload 5
        //     196: ifnull +10 -> 206
        //     199: aload 5
        //     201: invokeinterface 155 1 0
        //     206: aload 6
        //     208: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     143	161	156	finally
        //     182	209	156	finally
        //     44	138	164	java/lang/IllegalStateException
        //     44	138	192	finally
        //     166	177	192	finally
    }

    public boolean hasFormData()
    {
        synchronized (this.mFormLock)
        {
            boolean bool = hasEntries(1);
            return bool;
        }
    }

    public boolean hasHttpAuthUsernamePassword()
    {
        synchronized (this.mHttpAuthLock)
        {
            boolean bool = hasEntries(3);
            return bool;
        }
    }

    public boolean hasUsernamePassword()
    {
        synchronized (this.mPasswordLock)
        {
            boolean bool = hasEntries(0);
            return bool;
        }
    }

    // ERROR //
    void setFormData(String paramString, java.util.HashMap<String, String> paramHashMap)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnull +14 -> 15
        //     4: aload_2
        //     5: ifnull +10 -> 15
        //     8: aload_0
        //     9: invokespecial 141	android/webkit/WebViewDatabaseClassic:checkInitialized	()Z
        //     12: ifne +4 -> 16
        //     15: return
        //     16: aload_0
        //     17: getfield 98	android/webkit/WebViewDatabaseClassic:mFormLock	Ljava/lang/Object;
        //     20: astore_3
        //     21: aload_3
        //     22: monitorenter
        //     23: ldc2_w 342
        //     26: lstore 4
        //     28: aconst_null
        //     29: astore 6
        //     31: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     34: astore 16
        //     36: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     39: iconst_1
        //     40: aaload
        //     41: astore 17
        //     43: getstatic 87	android/webkit/WebViewDatabaseClassic:ID_PROJECTION	[Ljava/lang/String;
        //     46: astore 18
        //     48: iconst_1
        //     49: anewarray 77	java/lang/String
        //     52: astore 19
        //     54: aload 19
        //     56: iconst_0
        //     57: aload_1
        //     58: aastore
        //     59: aload 16
        //     61: aload 17
        //     63: aload 18
        //     65: ldc_w 314
        //     68: aload 19
        //     70: aconst_null
        //     71: aconst_null
        //     72: aconst_null
        //     73: invokevirtual 147	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     76: astore 6
        //     78: aload 6
        //     80: invokeinterface 152 1 0
        //     85: ifeq +157 -> 242
        //     88: aload 6
        //     90: aload 6
        //     92: ldc 43
        //     94: invokeinterface 218 2 0
        //     99: invokeinterface 222 2 0
        //     104: lstore 23
        //     106: lload 23
        //     108: lstore 4
        //     110: aload 6
        //     112: ifnull +10 -> 122
        //     115: aload 6
        //     117: invokeinterface 155 1 0
        //     122: lload 4
        //     124: lconst_0
        //     125: lcmp
        //     126: iflt +201 -> 327
        //     129: aload_2
        //     130: invokevirtual 349	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     133: invokeinterface 355 1 0
        //     138: astore 11
        //     140: new 234	android/content/ContentValues
        //     143: dup
        //     144: invokespecial 356	android/content/ContentValues:<init>	()V
        //     147: astore 12
        //     149: aload 12
        //     151: ldc 22
        //     153: lload 4
        //     155: invokestatic 360	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     158: invokevirtual 363	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
        //     161: aload 11
        //     163: invokeinterface 368 1 0
        //     168: ifeq +159 -> 327
        //     171: aload 11
        //     173: invokeinterface 372 1 0
        //     178: checkcast 374	java/util/Map$Entry
        //     181: astore 13
        //     183: aload 12
        //     185: ldc 19
        //     187: aload 13
        //     189: invokeinterface 377 1 0
        //     194: checkcast 77	java/lang/String
        //     197: invokevirtual 246	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     200: aload 12
        //     202: ldc 25
        //     204: aload 13
        //     206: invokeinterface 380 1 0
        //     211: checkcast 77	java/lang/String
        //     214: invokevirtual 246	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     217: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     220: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     223: iconst_2
        //     224: aaload
        //     225: aconst_null
        //     226: aload 12
        //     228: invokevirtual 384	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
        //     231: pop2
        //     232: goto -71 -> 161
        //     235: astore 8
        //     237: aload_3
        //     238: monitorexit
        //     239: aload 8
        //     241: athrow
        //     242: new 234	android/content/ContentValues
        //     245: dup
        //     246: invokespecial 356	android/content/ContentValues:<init>	()V
        //     249: astore 20
        //     251: aload 20
        //     253: ldc 28
        //     255: aload_1
        //     256: invokevirtual 246	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     259: getstatic 75	android/webkit/WebViewDatabaseClassic:sDatabase	Landroid/database/sqlite/SQLiteDatabase;
        //     262: getstatic 85	android/webkit/WebViewDatabaseClassic:mTableNames	[Ljava/lang/String;
        //     265: iconst_1
        //     266: aaload
        //     267: aconst_null
        //     268: aload 20
        //     270: invokevirtual 384	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
        //     273: lstore 21
        //     275: lload 21
        //     277: lstore 4
        //     279: goto -169 -> 110
        //     282: astore 9
        //     284: ldc 48
        //     286: ldc_w 385
        //     289: aload 9
        //     291: invokestatic 159	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     294: pop
        //     295: aload 6
        //     297: ifnull -175 -> 122
        //     300: aload 6
        //     302: invokeinterface 155 1 0
        //     307: goto -185 -> 122
        //     310: astore 7
        //     312: aload 6
        //     314: ifnull +10 -> 324
        //     317: aload 6
        //     319: invokeinterface 155 1 0
        //     324: aload 7
        //     326: athrow
        //     327: aload_3
        //     328: monitorexit
        //     329: goto -314 -> 15
        //
        // Exception table:
        //     from	to	target	type
        //     115	239	235	finally
        //     300	329	235	finally
        //     31	106	282	java/lang/IllegalStateException
        //     242	275	282	java/lang/IllegalStateException
        //     31	106	310	finally
        //     242	275	310	finally
        //     284	295	310	finally
    }

    void setHttpAuthUsernamePassword(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        if ((paramString1 == null) || (paramString2 == null) || (!checkInitialized()));
        while (true)
        {
            return;
            synchronized (this.mHttpAuthLock)
            {
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("host", paramString1);
                localContentValues.put("realm", paramString2);
                localContentValues.put("username", paramString3);
                localContentValues.put("password", paramString4);
                sDatabase.insert(mTableNames[3], "host", localContentValues);
            }
        }
    }

    void setUsernamePassword(String paramString1, String paramString2, String paramString3)
    {
        if ((paramString1 == null) || (!checkInitialized()));
        while (true)
        {
            return;
            synchronized (this.mPasswordLock)
            {
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("host", paramString1);
                localContentValues.put("username", paramString2);
                localContentValues.put("password", paramString3);
                sDatabase.insert(mTableNames[0], "host", localContentValues);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewDatabaseClassic
 * JD-Core Version:        0.6.2
 */