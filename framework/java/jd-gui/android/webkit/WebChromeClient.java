package android.webkit;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.view.View;

public class WebChromeClient
{
    public Bitmap getDefaultVideoPoster()
    {
        return null;
    }

    public View getVideoLoadingProgressView()
    {
        return null;
    }

    public void getVisitedHistory(ValueCallback<String[]> paramValueCallback)
    {
    }

    public void onCloseWindow(WebView paramWebView)
    {
    }

    @Deprecated
    public void onConsoleMessage(String paramString1, int paramInt, String paramString2)
    {
    }

    public boolean onConsoleMessage(ConsoleMessage paramConsoleMessage)
    {
        onConsoleMessage(paramConsoleMessage.message(), paramConsoleMessage.lineNumber(), paramConsoleMessage.sourceId());
        return false;
    }

    public boolean onCreateWindow(WebView paramWebView, boolean paramBoolean1, boolean paramBoolean2, Message paramMessage)
    {
        return false;
    }

    public void onExceededDatabaseQuota(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, WebStorage.QuotaUpdater paramQuotaUpdater)
    {
        paramQuotaUpdater.updateQuota(paramLong1);
    }

    public void onGeolocationPermissionsHidePrompt()
    {
    }

    public void onGeolocationPermissionsShowPrompt(String paramString, GeolocationPermissions.Callback paramCallback)
    {
    }

    public void onHideCustomView()
    {
    }

    public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
    {
        return false;
    }

    public boolean onJsBeforeUnload(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
    {
        return false;
    }

    public boolean onJsConfirm(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
    {
        return false;
    }

    public boolean onJsPrompt(WebView paramWebView, String paramString1, String paramString2, String paramString3, JsPromptResult paramJsPromptResult)
    {
        return false;
    }

    public boolean onJsTimeout()
    {
        return true;
    }

    public void onProgressChanged(WebView paramWebView, int paramInt)
    {
    }

    public void onReachedMaxAppCacheSize(long paramLong1, long paramLong2, WebStorage.QuotaUpdater paramQuotaUpdater)
    {
        paramQuotaUpdater.updateQuota(paramLong2);
    }

    public void onReceivedIcon(WebView paramWebView, Bitmap paramBitmap)
    {
    }

    public void onReceivedTitle(WebView paramWebView, String paramString)
    {
    }

    public void onReceivedTouchIconUrl(WebView paramWebView, String paramString, boolean paramBoolean)
    {
    }

    public void onRequestFocus(WebView paramWebView)
    {
    }

    public void onShowCustomView(View paramView, int paramInt, CustomViewCallback paramCustomViewCallback)
    {
    }

    public void onShowCustomView(View paramView, CustomViewCallback paramCustomViewCallback)
    {
    }

    public void openFileChooser(ValueCallback<Uri> paramValueCallback, String paramString1, String paramString2)
    {
        paramValueCallback.onReceiveValue(null);
    }

    public void setInstallableWebApp()
    {
    }

    public void setupAutoFill(Message paramMessage)
    {
    }

    public static abstract interface CustomViewCallback
    {
        public abstract void onCustomViewHidden();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebChromeClient
 * JD-Core Version:        0.6.2
 */