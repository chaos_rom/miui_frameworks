package android.webkit;

class MustOverrideException extends RuntimeException
{
    MustOverrideException()
    {
        super("abstract function called: must be overriden!");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.MustOverrideException
 * JD-Core Version:        0.6.2
 */