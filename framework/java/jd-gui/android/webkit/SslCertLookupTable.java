package android.webkit;

import android.net.http.SslError;
import android.os.Bundle;
import java.net.MalformedURLException;
import java.net.URL;

final class SslCertLookupTable
{
    private static SslCertLookupTable sTable;
    private final Bundle table = new Bundle();

    public static SslCertLookupTable getInstance()
    {
        if (sTable == null)
            sTable = new SslCertLookupTable();
        return sTable;
    }

    public void clear()
    {
        this.table.clear();
    }

    public boolean isAllowed(SslError paramSslError)
    {
        boolean bool = false;
        try
        {
            String str = new URL(paramSslError.getUrl()).getHost();
            if ((this.table.containsKey(str)) && (paramSslError.getPrimaryError() <= this.table.getInt(str)))
                bool = true;
            label48: return bool;
        }
        catch (MalformedURLException localMalformedURLException)
        {
            break label48;
        }
    }

    public void setIsAllowed(SslError paramSslError)
    {
        try
        {
            String str = new URL(paramSslError.getUrl()).getHost();
            this.table.putInt(str, paramSslError.getPrimaryError());
            label27: return;
        }
        catch (MalformedURLException localMalformedURLException)
        {
            break label27;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.SslCertLookupTable
 * JD-Core Version:        0.6.2
 */