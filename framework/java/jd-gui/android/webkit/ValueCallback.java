package android.webkit;

public abstract interface ValueCallback<T>
{
    public abstract void onReceiveValue(T paramT);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ValueCallback
 * JD-Core Version:        0.6.2
 */