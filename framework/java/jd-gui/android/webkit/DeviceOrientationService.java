package android.webkit;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import java.util.List;

final class DeviceOrientationService
    implements SensorEventListener
{
    private static final double DELTA_DEGRESS = 1.0D;
    private Double mAlpha;
    private Double mBeta;
    private Context mContext;
    private Double mGamma;
    private float[] mGravityVector;
    private Handler mHandler;
    private boolean mHaveSentErrorEvent;
    private boolean mIsRunning;
    private float[] mMagneticFieldVector;
    private DeviceMotionAndOrientationManager mManager;
    private SensorManager mSensorManager;

    static
    {
        if (!DeviceOrientationService.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    public DeviceOrientationService(DeviceMotionAndOrientationManager paramDeviceMotionAndOrientationManager, Context paramContext)
    {
        this.mManager = paramDeviceMotionAndOrientationManager;
        assert (this.mManager != null);
        this.mContext = paramContext;
        assert (this.mContext != null);
    }

    private void getOrientationUsingGetRotationMatrix()
    {
        if ((this.mGravityVector == null) || (this.mMagneticFieldVector == null));
        while (true)
        {
            return;
            float[] arrayOfFloat1 = new float[9];
            if (SensorManager.getRotationMatrix(arrayOfFloat1, null, this.mGravityVector, this.mMagneticFieldVector))
            {
                float[] arrayOfFloat2 = new float[3];
                SensorManager.getOrientation(arrayOfFloat1, arrayOfFloat2);
                for (double d1 = Math.toDegrees(-arrayOfFloat2[0]) - 90.0D; d1 < 0.0D; d1 += 360.0D);
                for (double d2 = Math.toDegrees(-arrayOfFloat2[1]); d2 < -180.0D; d2 += 360.0D);
                for (double d3 = Math.toDegrees(arrayOfFloat2[2]); d3 < -90.0D; d3 += 360.0D);
                maybeSendChange(d1, d2, d3);
            }
        }
    }

    private SensorManager getSensorManager()
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        if (this.mSensorManager == null)
            this.mSensorManager = ((SensorManager)this.mContext.getSystemService("sensor"));
        return this.mSensorManager;
    }

    private void maybeSendChange(double paramDouble1, double paramDouble2, double paramDouble3)
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        if ((this.mAlpha == null) || (this.mBeta == null) || (this.mGamma == null) || (Math.abs(paramDouble1 - this.mAlpha.doubleValue()) > 1.0D) || (Math.abs(paramDouble2 - this.mBeta.doubleValue()) > 1.0D) || (Math.abs(paramDouble3 - this.mGamma.doubleValue()) > 1.0D))
        {
            this.mAlpha = Double.valueOf(paramDouble1);
            this.mBeta = Double.valueOf(paramDouble2);
            this.mGamma = Double.valueOf(paramDouble3);
            this.mManager.onOrientationChange(this.mAlpha, this.mBeta, this.mGamma);
            this.mHaveSentErrorEvent = false;
        }
    }

    private boolean registerForAccelerometerSensor()
    {
        List localList = getSensorManager().getSensorList(1);
        if (localList.isEmpty());
        for (boolean bool = false; ; bool = getSensorManager().registerListener(this, (Sensor)localList.get(0), 0, this.mHandler))
            return bool;
    }

    private boolean registerForMagneticFieldSensor()
    {
        List localList = getSensorManager().getSensorList(2);
        if (localList.isEmpty());
        for (boolean bool = false; ; bool = getSensorManager().registerListener(this, (Sensor)localList.get(0), 0, this.mHandler))
            return bool;
    }

    private void registerForSensors()
    {
        if (this.mHandler == null)
            this.mHandler = new Handler();
        if ((!registerForAccelerometerSensor()) || (!registerForMagneticFieldSensor()))
        {
            unregisterFromSensors();
            sendErrorEvent();
        }
    }

    private void sendErrorEvent()
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        if (this.mHaveSentErrorEvent);
        while (true)
        {
            return;
            this.mHaveSentErrorEvent = true;
            this.mHandler.post(new Runnable()
            {
                static
                {
                    if (!DeviceOrientationService.class.desiredAssertionStatus());
                    for (boolean bool = true; ; bool = false)
                    {
                        $assertionsDisabled = bool;
                        return;
                    }
                }

                public void run()
                {
                    assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
                    if (DeviceOrientationService.this.mIsRunning)
                        DeviceOrientationService.this.mManager.onOrientationChange(null, null, null);
                }
            });
        }
    }

    private void unregisterFromSensors()
    {
        getSensorManager().unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor paramSensor, int paramInt)
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
    }

    public void onSensorChanged(SensorEvent paramSensorEvent)
    {
        assert (paramSensorEvent.values.length == 3);
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        if (!this.mIsRunning);
        while (true)
        {
            return;
            switch (paramSensorEvent.sensor.getType())
            {
            default:
                if (!$assertionsDisabled)
                    throw new AssertionError();
                break;
            case 1:
                if (this.mGravityVector == null)
                    this.mGravityVector = new float[3];
                this.mGravityVector[0] = paramSensorEvent.values[0];
                this.mGravityVector[1] = paramSensorEvent.values[1];
                this.mGravityVector[2] = paramSensorEvent.values[2];
                getOrientationUsingGetRotationMatrix();
                break;
            case 2:
                if (this.mMagneticFieldVector == null)
                    this.mMagneticFieldVector = new float[3];
                this.mMagneticFieldVector[0] = paramSensorEvent.values[0];
                this.mMagneticFieldVector[1] = paramSensorEvent.values[1];
                this.mMagneticFieldVector[2] = paramSensorEvent.values[2];
                getOrientationUsingGetRotationMatrix();
            }
        }
    }

    public void resume()
    {
        if (this.mIsRunning)
            registerForSensors();
    }

    public void start()
    {
        this.mIsRunning = true;
        registerForSensors();
    }

    public void stop()
    {
        this.mIsRunning = false;
        unregisterFromSensors();
    }

    public void suspend()
    {
        if (this.mIsRunning)
            unregisterFromSensors();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.DeviceOrientationService
 * JD-Core Version:        0.6.2
 */