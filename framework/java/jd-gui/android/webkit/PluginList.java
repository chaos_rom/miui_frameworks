package android.webkit;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class PluginList
{
    private ArrayList<Plugin> mPlugins = new ArrayList();

    @Deprecated
    public void addPlugin(Plugin paramPlugin)
    {
        try
        {
            if (!this.mPlugins.contains(paramPlugin))
                this.mPlugins.add(paramPlugin);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public void clear()
    {
        try
        {
            this.mPlugins.clear();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public List getList()
    {
        try
        {
            ArrayList localArrayList = this.mPlugins;
            return localArrayList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    @Deprecated
    public void pluginClicked(android.content.Context paramContext, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 17	android/webkit/PluginList:mPlugins	Ljava/util/ArrayList;
        //     6: iload_2
        //     7: invokevirtual 39	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     10: checkcast 41	android/webkit/Plugin
        //     13: aload_1
        //     14: invokevirtual 45	android/webkit/Plugin:dispatchClickEvent	(Landroid/content/Context;)V
        //     17: aload_0
        //     18: monitorexit
        //     19: return
        //     20: astore 4
        //     22: aload_0
        //     23: monitorexit
        //     24: aload 4
        //     26: athrow
        //     27: astore_3
        //     28: goto -11 -> 17
        //
        // Exception table:
        //     from	to	target	type
        //     2	17	20	finally
        //     2	17	27	java/lang/IndexOutOfBoundsException
    }

    @Deprecated
    public void removePlugin(Plugin paramPlugin)
    {
        try
        {
            int i = this.mPlugins.indexOf(paramPlugin);
            if (i != -1)
                this.mPlugins.remove(i);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.PluginList
 * JD-Core Version:        0.6.2
 */