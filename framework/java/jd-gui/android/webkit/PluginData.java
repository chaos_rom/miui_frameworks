package android.webkit;

import java.io.InputStream;
import java.util.Map;

@Deprecated
public final class PluginData
{
    private long mContentLength;
    private Map<String, String[]> mHeaders;
    private int mStatusCode;
    private InputStream mStream;

    @Deprecated
    public PluginData(InputStream paramInputStream, long paramLong, Map<String, String[]> paramMap, int paramInt)
    {
        this.mStream = paramInputStream;
        this.mContentLength = paramLong;
        this.mHeaders = paramMap;
        this.mStatusCode = paramInt;
    }

    @Deprecated
    public long getContentLength()
    {
        return this.mContentLength;
    }

    @Deprecated
    public Map<String, String[]> getHeaders()
    {
        return this.mHeaders;
    }

    @Deprecated
    public InputStream getInputStream()
    {
        return this.mStream;
    }

    @Deprecated
    public int getStatusCode()
    {
        return this.mStatusCode;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.PluginData
 * JD-Core Version:        0.6.2
 */