package android.webkit;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaFile;
import android.net.ProxyProperties;
import android.net.Uri;
import android.net.http.CertificateChainValidator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import junit.framework.Assert;

public final class WebViewCore
{
    static final int ACTION_DOUBLETAP = 512;
    static final int ACTION_LONGPRESS = 256;
    static final String[] HandlerDebugString;
    private static final String LOGTAG = "webcore";
    static final String THREAD_NAME = "WebViewCoreThread";
    private static final int TOUCH_FLAG_HIT_HANDLER = 1;
    private static final int TOUCH_FLAG_PREVENT_DEFAULT = 2;
    private static boolean mRepaintScheduled;
    private static boolean sShouldMonitorWebCoreThread;
    private static Handler sWebCoreHandler;
    private BrowserFrame mBrowserFrame;
    private final CallbackProxy mCallbackProxy;
    private int mChromeCanFocusDirection;
    private final Context mContext;
    private int mCurrentViewHeight = 0;
    private float mCurrentViewScale = 1.0F;
    private int mCurrentViewWidth = 0;
    private DeviceMotionAndOrientationManager mDeviceMotionAndOrientationManager = new DeviceMotionAndOrientationManager(this);
    private DeviceMotionService mDeviceMotionService;
    private DeviceOrientationService mDeviceOrientationService;
    private boolean mDrawIsPaused;
    private boolean mDrawIsScheduled;
    private final EventHub mEventHub;
    private boolean mFirstLayoutForNonStandardLoad;
    private int mHighMemoryUsageThresholdMb;
    private int mHighUsageDeltaMb;
    private ViewState mInitialViewState = null;
    private boolean mIsRestored = false;
    private Map<String, Object> mJavascriptInterfaces;
    DrawData mLastDrawData = null;
    private int mLowMemoryUsageThresholdMb;
    private int mNativeClass;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private String[] mReadModeString = new String[3];

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private Vector<String> mReadModeTemplateVector = new Vector();
    private float mRestoredScale = 0.0F;
    private float mRestoredTextWrapScale = 0.0F;
    private int mRestoredX = 0;
    private int mRestoredY = 0;
    private final WebSettingsClassic mSettings;
    private int mTextSelectionChangeReason = 0;
    private int mViewportDensityDpi = -1;
    private int mViewportHeight = -1;
    private int mViewportInitialScale = 0;
    private int mViewportMaximumScale = 0;
    private int mViewportMinimumScale = 0;
    private boolean mViewportUserScalable = true;
    private int mViewportWidth = -1;
    private WebViewClassic mWebViewClassic;
    private boolean m_drawWasSkipped = false;
    private boolean m_skipDrawFlag = false;
    private Object m_skipDrawFlagLock = new Object();

    static
    {
        try
        {
            System.loadLibrary("webcore");
            System.loadLibrary("chromium_net");
            String[] arrayOfString = new String[54];
            arrayOfString[0] = "REVEAL_SELECTION";
            arrayOfString[1] = "";
            arrayOfString[2] = "";
            arrayOfString[3] = "SCROLL_TEXT_INPUT";
            arrayOfString[4] = "LOAD_URL";
            arrayOfString[5] = "STOP_LOADING";
            arrayOfString[6] = "RELOAD";
            arrayOfString[7] = "KEY_DOWN";
            arrayOfString[8] = "KEY_UP";
            arrayOfString[9] = "VIEW_SIZE_CHANGED";
            arrayOfString[10] = "GO_BACK_FORWARD";
            arrayOfString[11] = "SET_SCROLL_OFFSET";
            arrayOfString[12] = "RESTORE_STATE";
            arrayOfString[13] = "PAUSE_TIMERS";
            arrayOfString[14] = "RESUME_TIMERS";
            arrayOfString[15] = "CLEAR_CACHE";
            arrayOfString[16] = "CLEAR_HISTORY";
            arrayOfString[17] = "SET_SELECTION";
            arrayOfString[18] = "REPLACE_TEXT";
            arrayOfString[19] = "PASS_TO_JS";
            arrayOfString[20] = "SET_GLOBAL_BOUNDS";
            arrayOfString[21] = "";
            arrayOfString[22] = "CLICK";
            arrayOfString[23] = "SET_NETWORK_STATE";
            arrayOfString[24] = "DOC_HAS_IMAGES";
            arrayOfString[25] = "FAKE_CLICK";
            arrayOfString[26] = "DELETE_SELECTION";
            arrayOfString[27] = "LISTBOX_CHOICES";
            arrayOfString[28] = "SINGLE_LISTBOX_CHOICE";
            arrayOfString[29] = "MESSAGE_RELAY";
            arrayOfString[30] = "SET_BACKGROUND_COLOR";
            arrayOfString[31] = "SET_MOVE_FOCUS";
            arrayOfString[32] = "SAVE_DOCUMENT_STATE";
            arrayOfString[33] = "129";
            arrayOfString[34] = "WEBKIT_DRAW";
            arrayOfString[35] = "131";
            arrayOfString[36] = "POST_URL";
            arrayOfString[37] = "";
            arrayOfString[38] = "CLEAR_CONTENT";
            arrayOfString[39] = "";
            arrayOfString[40] = "";
            arrayOfString[41] = "REQUEST_CURSOR_HREF";
            arrayOfString[42] = "ADD_JS_INTERFACE";
            arrayOfString[43] = "LOAD_DATA";
            arrayOfString[44] = "";
            arrayOfString[45] = "";
            arrayOfString[46] = "SET_ACTIVE";
            arrayOfString[47] = "ON_PAUSE";
            arrayOfString[48] = "ON_RESUME";
            arrayOfString[49] = "FREE_MEMORY";
            arrayOfString[50] = "VALID_NODE_BOUNDS";
            arrayOfString[51] = "SAVE_WEBARCHIVE";
            arrayOfString[52] = "WEBKIT_DRAW_LAYERS";
            arrayOfString[53] = "REMOVE_JS_INTERFACE";
            HandlerDebugString = arrayOfString;
            mRepaintScheduled = false;
            return;
        }
        catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
        {
            while (true)
                Log.e("webcore", "Unable to load native support libraries.");
        }
    }

    public WebViewCore(Context paramContext, WebViewClassic paramWebViewClassic, CallbackProxy paramCallbackProxy, Map<String, Object> paramMap)
    {
        this.mCallbackProxy = paramCallbackProxy;
        this.mWebViewClassic = paramWebViewClassic;
        this.mJavascriptInterfaces = paramMap;
        this.mContext = paramContext;
        try
        {
            if (sWebCoreHandler == null)
            {
                Thread localThread = new Thread(new WebCoreThread(null));
                localThread.setName("WebViewCoreThread");
                localThread.start();
            }
            try
            {
                WebViewCore.class.wait();
                if (sShouldMonitorWebCoreThread)
                    WebCoreThreadWatchdog.start(sWebCoreHandler);
                WebCoreThreadWatchdog.registerWebView(paramWebViewClassic);
                this.mEventHub = new EventHub(null);
                this.mSettings = new WebSettingsClassic(this.mContext, this.mWebViewClassic);
                WebIconDatabase.getInstance();
                WebStorageClassic.getInstance().createUIHandler();
                GeolocationPermissionsClassic.getInstance().createUIHandler();
                ActivityManager localActivityManager = (ActivityManager)this.mContext.getSystemService("activity");
                localActivityManager.getMemoryInfo(new ActivityManager.MemoryInfo());
                this.mLowMemoryUsageThresholdMb = localActivityManager.getLargeMemoryClass();
                this.mHighMemoryUsageThresholdMb = ((int)(1.5D * this.mLowMemoryUsageThresholdMb));
                this.mHighUsageDeltaMb = (this.mLowMemoryUsageThresholdMb / 32);
                Message localMessage = sWebCoreHandler.obtainMessage(0, this);
                sWebCoreHandler.sendMessage(localMessage);
                return;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                {
                    Log.e("webcore", "Caught exception while waiting for thread creation.");
                    Log.e("webcore", Log.getStackTraceString(localInterruptedException));
                }
            }
        }
        finally
        {
        }
    }

    private ViewManager.ChildView addSurface(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        ViewManager.ChildView localChildView = createSurface(paramView);
        localChildView.attachView(paramInt1, paramInt2, paramInt3, paramInt4);
        return localChildView;
    }

    private int calculateWindowWidth(int paramInt)
    {
        int i = paramInt;
        if (this.mSettings.getUseWideViewPort())
        {
            if (this.mViewportWidth != -1)
                break label27;
            i = 980;
        }
        while (true)
        {
            return i;
            label27: if (this.mViewportWidth > 0)
                i = this.mViewportWidth;
            else
                i = Math.round(this.mWebViewClassic.getViewWidth() / this.mWebViewClassic.getDefaultZoomScale());
        }
    }

    private void centerFitRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            this.mWebViewClassic.mPrivateHandler.obtainMessage(127, new Rect(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4)).sendToTarget();
        }
    }

    private boolean chromeCanTakeFocus(int paramInt)
    {
        int i = mapDirection(paramInt);
        if ((i == this.mChromeCanFocusDirection) && (i != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void chromeTakeFocus(int paramInt)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            Message localMessage = this.mWebViewClassic.mPrivateHandler.obtainMessage(110);
            localMessage.arg1 = mapDirection(paramInt);
            localMessage.sendToTarget();
        }
    }

    private void clearCache(boolean paramBoolean)
    {
        this.mBrowserFrame.clearCache();
        if (paramBoolean)
            CacheManager.removeAllCacheFiles();
    }

    private void clearTextEntry()
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 111).sendToTarget();
        }
    }

    private void contentScrollTo(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 1;
        if (!this.mBrowserFrame.firstLayoutDone())
        {
            this.mRestoredX = paramInt1;
            this.mRestoredY = paramInt2;
        }
        while (true)
        {
            return;
            if (this.mWebViewClassic != null)
            {
                Handler localHandler = this.mWebViewClassic.mPrivateHandler;
                int j;
                if (paramBoolean1)
                {
                    j = i;
                    label48: if (!paramBoolean2)
                        break label106;
                }
                Message localMessage;
                while (true)
                {
                    localMessage = Message.obtain(localHandler, 101, j, i, new Point(paramInt1, paramInt2));
                    if (!this.mDrawIsScheduled)
                        break label112;
                    this.mEventHub.sendMessage(Message.obtain(null, 125, localMessage));
                    break;
                    j = 0;
                    break label48;
                    label106: i = 0;
                }
                label112: localMessage.sendToTarget();
            }
        }
    }

    private ViewManager.ChildView createSurface(View paramView)
    {
        ViewManager.ChildView localChildView = null;
        if (this.mWebViewClassic == null);
        while (true)
        {
            return localChildView;
            if (paramView == null)
            {
                Log.e("webcore", "Attempted to add an empty plugin view to the view hierarchy");
            }
            else
            {
                paramView.setWillNotDraw(false);
                if ((paramView instanceof SurfaceView))
                    ((SurfaceView)paramView).setZOrderOnTop(true);
                localChildView = this.mWebViewClassic.mViewManager.createView();
                localChildView.mView = paramView;
            }
        }
    }

    private TextSelectionData createTextSelection(int paramInt1, int paramInt2, int paramInt3)
    {
        TextSelectionData localTextSelectionData = new TextSelectionData(paramInt1, paramInt2, paramInt3);
        localTextSelectionData.mSelectionReason = this.mTextSelectionChangeReason;
        return localTextSelectionData;
    }

    private void destroySurface(ViewManager.ChildView paramChildView)
    {
        paramChildView.removeView();
    }

    private void didFirstLayout(boolean paramBoolean)
    {
        this.mBrowserFrame.didFirstLayout();
        if (this.mWebViewClassic == null)
            return;
        if ((paramBoolean) || (this.mIsRestored));
        for (boolean bool = true; ; bool = false)
        {
            setupViewport(bool);
            if (!bool)
                this.mWebViewClassic.mViewManager.postReadyToDrawAll();
            this.mWebViewClassic.mPrivateHandler.sendEmptyMessage(131);
            this.mRestoredY = 0;
            this.mRestoredX = 0;
            this.mIsRestored = false;
            this.mRestoredTextWrapScale = 0.0F;
            this.mRestoredScale = 0.0F;
            break;
        }
    }

    private void focusNodeChanged(int paramInt, WebKitHitTest paramWebKitHitTest)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            this.mWebViewClassic.mPrivateHandler.obtainMessage(147, paramInt, 0, paramWebKitHitTest).sendToTarget();
        }
    }

    private Class<?> getPluginClass(String paramString1, String paramString2)
    {
        Object localObject = null;
        if (this.mWebViewClassic == null);
        while (true)
        {
            return localObject;
            PluginManager localPluginManager = PluginManager.getInstance(null);
            String str = localPluginManager.getPluginsAPKName(paramString1);
            if (str == null)
                Log.w("webcore", "Unable to resolve " + paramString1 + " to a plugin APK");
            else
                try
                {
                    Class localClass = localPluginManager.getPluginClass(str, paramString2);
                    localObject = localClass;
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    Log.e("webcore", "Unable to find plugin classloader for the apk (" + str + ")");
                }
                catch (ClassNotFoundException localClassNotFoundException)
                {
                    Log.e("webcore", "Unable to find plugin class (" + paramString2 + ") in the apk (" + str + ")");
                }
        }
    }

    private long getUsedQuota()
    {
        Collection localCollection = WebStorageClassic.getInstance().getOriginsSync();
        long l;
        if (localCollection == null)
            l = 0L;
        while (true)
        {
            return l;
            l = 0L;
            Iterator localIterator = localCollection.iterator();
            while (localIterator.hasNext())
                l += ((WebStorage.Origin)localIterator.next()).getQuota();
        }
    }

    private WebView getWebView()
    {
        return this.mWebViewClassic.getWebView();
    }

    private void hideFullScreenPlugin()
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            this.mWebViewClassic.mPrivateHandler.obtainMessage(121).sendToTarget();
        }
    }

    private void initEditField(int paramInt1, int paramInt2, int paramInt3, TextFieldInitData paramTextFieldInitData)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 142, paramTextFieldInitData).sendToTarget();
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 112, paramTextFieldInitData.mFieldPointer, 0, createTextSelection(paramInt1, paramInt2, paramInt3)).sendToTarget();
        }
    }

    private void initialize()
    {
        this.mBrowserFrame = new BrowserFrame(this.mContext, this, this.mCallbackProxy, this.mSettings, this.mJavascriptInterfaces);
        this.mJavascriptInterfaces = null;
        this.mSettings.syncSettingsAndCreateHandler(this.mBrowserFrame);
        WebIconDatabaseClassic.getInstance().createHandler();
        WebStorageClassic.getInstance().createHandler();
        GeolocationPermissionsClassic.getInstance().createHandler();
        this.mEventHub.transferMessages();
        if (this.mWebViewClassic != null)
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 107, this.mNativeClass, 0).sendToTarget();
    }

    static boolean isSupportedMediaMimeType(String paramString)
    {
        int i = MediaFile.getFileTypeForMimeType(paramString);
        if ((MediaFile.isAudioFileType(i)) || (MediaFile.isVideoFileType(i)) || (MediaFile.isPlayListFileType(i)) || ((paramString != null) && (paramString.startsWith("video/m4v"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static boolean isUpdatePicturePaused(WebViewCore paramWebViewCore)
    {
        if (paramWebViewCore != null);
        for (boolean bool = paramWebViewCore.mDrawIsPaused; ; bool = false)
            return bool;
    }

    private void keepScreenOn(boolean paramBoolean)
    {
        Message localMessage;
        if (this.mWebViewClassic != null)
        {
            localMessage = this.mWebViewClassic.mPrivateHandler.obtainMessage(136);
            if (!paramBoolean)
                break label37;
        }
        label37: for (int i = 1; ; i = 0)
        {
            localMessage.arg1 = i;
            localMessage.sendToTarget();
            return;
        }
    }

    private void key(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean)
    {
        this.mChromeCanFocusDirection = paramInt;
        int i = paramKeyEvent.getKeyCode();
        int j = paramKeyEvent.getUnicodeChar();
        if ((i == 0) && (paramKeyEvent.getCharacters() != null) && (paramKeyEvent.getCharacters().length() > 0))
            j = paramKeyEvent.getCharacters().codePointAt(0);
        boolean bool = nativeKey(this.mNativeClass, i, j, paramKeyEvent.getRepeatCount(), paramKeyEvent.isShiftPressed(), paramKeyEvent.isAltPressed(), paramKeyEvent.isSymPressed(), paramBoolean);
        this.mChromeCanFocusDirection = 0;
        if ((!bool) && (i != 66))
        {
            if ((i < 19) || (i > 22))
                break label145;
            if ((paramInt != 0) && (paramBoolean))
            {
                Message localMessage = this.mWebViewClassic.mPrivateHandler.obtainMessage(110);
                localMessage.arg1 = paramInt;
                localMessage.sendToTarget();
            }
        }
        while (true)
        {
            return;
            label145: this.mCallbackProxy.onUnhandledKeyEvent(paramKeyEvent);
        }
    }

    private void keyPress(int paramInt)
    {
        nativeKey(this.mNativeClass, 0, paramInt, 0, false, false, false, true);
        nativeKey(this.mNativeClass, 0, paramInt, 0, false, false, false, false);
    }

    private void loadUrl(String paramString, Map<String, String> paramMap)
    {
        this.mBrowserFrame.loadUrl(paramString, paramMap);
    }

    private int mapDirection(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 0;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 1;
            continue;
            i = 33;
            continue;
            i = 130;
            continue;
            i = 17;
            continue;
            i = 66;
        }
    }

    private native void nativeAutoFillForm(int paramInt1, int paramInt2);

    private static native void nativeCertTrustChanged();

    private native void nativeClearContent(int paramInt);

    private native void nativeClearTextSelection(int paramInt);

    private native void nativeCloseIdleConnections(int paramInt);

    private native void nativeContentInvalidateAll(int paramInt);

    private native void nativeDeleteSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private native void nativeDeleteText(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private native void nativeDumpDomTree(int paramInt, boolean paramBoolean);

    private native void nativeDumpRenderTree(int paramInt, boolean paramBoolean);

    static native String nativeFindAddress(String paramString, boolean paramBoolean);

    private native int nativeFindAll(int paramInt, String paramString);

    private native int nativeFindNext(int paramInt, boolean paramBoolean);

    private native boolean nativeFocusBoundsChanged(int paramInt);

    private native void nativeFreeMemory(int paramInt);

    private native void nativeFullScreenPluginHidden(int paramInt1, int paramInt2);

    private native void nativeGeolocationPermissionsProvide(int paramInt, String paramString, boolean paramBoolean1, boolean paramBoolean2);

    private native int nativeGetContentMinPrefWidth(int paramInt);

    private native String nativeGetText(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private native int nativeHandleTouchEvent(int paramInt1, int paramInt2, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3, int paramInt3, int paramInt4, int paramInt5);

    private native WebKitHitTest nativeHitTest(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean);

    private native void nativeInsertText(int paramInt, String paramString);

    private native boolean nativeKey(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4);

    private native String nativeModifySelection(int paramInt1, int paramInt2, int paramInt3);

    private native boolean nativeMouseClick(int paramInt);

    private native void nativeMoveMouse(int paramInt1, int paramInt2, int paramInt3);

    private native void nativeNotifyAnimationStarted(int paramInt);

    private native void nativePause(int paramInt);

    private native void nativePluginSurfaceReady(int paramInt);

    private native void nativeProvideVisitedHistory(int paramInt, String[] paramArrayOfString);

    private native int nativeRecordContent(int paramInt, Point paramPoint);

    private native void nativeRegisterURLSchemeAsLocal(int paramInt, String paramString);

    private native void nativeReplaceTextfieldText(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5, int paramInt6);

    private native String nativeRequestLabel(int paramInt1, int paramInt2, int paramInt3);

    private native void nativeResume(int paramInt);

    private native String nativeRetrieveAnchorText(int paramInt1, int paramInt2, int paramInt3);

    private native String nativeRetrieveHref(int paramInt1, int paramInt2, int paramInt3);

    private native String nativeRetrieveImageSource(int paramInt1, int paramInt2, int paramInt3);

    private native void nativeRevealSelection(int paramInt);

    private native void nativeSaveDocumentState(int paramInt);

    private native void nativeScrollFocusedTextInput(int paramInt1, float paramFloat, int paramInt2, Rect paramRect);

    private native void nativeScrollLayer(int paramInt1, int paramInt2, Rect paramRect);

    private native void nativeSelectAll(int paramInt);

    private native String[] nativeSelectText(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private native boolean nativeSelectWordAt(int paramInt1, int paramInt2, int paramInt3);

    private native void nativeSendListBoxChoice(int paramInt1, int paramInt2);

    private native void nativeSendListBoxChoices(int paramInt1, boolean[] paramArrayOfBoolean, int paramInt2);

    private native void nativeSetBackgroundColor(int paramInt1, int paramInt2);

    private native void nativeSetFocusControllerActive(int paramInt, boolean paramBoolean);

    private native void nativeSetGlobalBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private native void nativeSetInitialFocus(int paramInt1, int paramInt2);

    private native void nativeSetIsPaused(int paramInt, boolean paramBoolean);

    private native void nativeSetJsFlags(int paramInt, String paramString);

    private native void nativeSetNewStorageLimit(int paramInt, long paramLong);

    private native void nativeSetScrollOffset(int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3);

    private native void nativeSetSelection(int paramInt1, int paramInt2, int paramInt3);

    private native void nativeSetSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean);

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private native String[] nativeSwitchReadMode(int paramInt, boolean paramBoolean, Vector<String> paramVector);

    private void needTouchEvents(boolean paramBoolean)
    {
        Handler localHandler;
        if (this.mWebViewClassic != null)
        {
            localHandler = this.mWebViewClassic.mPrivateHandler;
            if (!paramBoolean)
                break label33;
        }
        label33: for (int i = 1; ; i = 0)
        {
            Message.obtain(localHandler, 116, i, 0).sendToTarget();
            return;
        }
    }

    private String openFileChooser(String paramString1, String paramString2)
    {
        Uri localUri = this.mCallbackProxy.openFileChooser(paramString1, paramString2);
        Object localObject1;
        Cursor localCursor;
        if (localUri != null)
        {
            localObject1 = "";
            ContentResolver localContentResolver = this.mContext.getContentResolver();
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "_data";
            localCursor = localContentResolver.query(localUri, arrayOfString, null, null, null);
            if (localCursor == null);
        }
        while (true)
        {
            try
            {
                if (localCursor.moveToNext())
                {
                    String str2 = localCursor.getString(0);
                    localObject1 = str2;
                }
                localCursor.close();
                str1 = localUri.toString();
                BrowserFrame.sJavaBridge.storeFilePathForContentUri((String)localObject1, str1);
                return str1;
            }
            finally
            {
                localCursor.close();
            }
            localObject1 = localUri.getLastPathSegment();
            continue;
            String str1 = "";
        }
    }

    private native void passToJs(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4);

    public static void pauseTimers()
    {
        if (BrowserFrame.sJavaBridge == null)
            throw new IllegalStateException("No WebView has been created in this process!");
        BrowserFrame.sJavaBridge.pause();
    }

    // ERROR //
    static void pauseUpdatePicture(WebViewCore paramWebViewCore)
    {
        // Byte code:
        //     0: aload_0
        //     1: ifnull +13 -> 14
        //     4: aload_0
        //     5: invokevirtual 1184	android/webkit/WebViewCore:getSettings	()Landroid/webkit/WebSettingsClassic;
        //     8: invokevirtual 1187	android/webkit/WebSettingsClassic:enableSmoothTransition	()Z
        //     11: ifne +4 -> 15
        //     14: return
        //     15: aload_0
        //     16: monitorenter
        //     17: aload_0
        //     18: getfield 554	android/webkit/WebViewCore:mNativeClass	I
        //     21: ifne +22 -> 43
        //     24: ldc 89
        //     26: ldc_w 1189
        //     29: invokestatic 968	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     32: pop
        //     33: aload_0
        //     34: monitorexit
        //     35: goto -21 -> 14
        //     38: astore_1
        //     39: aload_0
        //     40: monitorexit
        //     41: aload_1
        //     42: athrow
        //     43: aload_0
        //     44: aload_0
        //     45: getfield 554	android/webkit/WebViewCore:mNativeClass	I
        //     48: iconst_1
        //     49: invokespecial 1191	android/webkit/WebViewCore:nativeSetIsPaused	(IZ)V
        //     52: aload_0
        //     53: iconst_1
        //     54: putfield 1066	android/webkit/WebViewCore:mDrawIsPaused	Z
        //     57: aload_0
        //     58: monitorexit
        //     59: goto -45 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     17	41	38	finally
        //     43	59	38	finally
    }

    private WebKitHitTest performHitTest(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        WebKitHitTest localWebKitHitTest = nativeHitTest(this.mNativeClass, paramInt1, paramInt2, paramInt3, paramBoolean);
        localWebKitHitTest.mHitTestX = paramInt1;
        localWebKitHitTest.mHitTestY = paramInt2;
        localWebKitHitTest.mHitTestSlop = paramInt3;
        localWebKitHitTest.mHitTestMovedMouse = paramBoolean;
        return localWebKitHitTest;
    }

    static void reducePriority()
    {
        sWebCoreHandler.removeMessages(1);
        sWebCoreHandler.removeMessages(2);
        sWebCoreHandler.sendMessageAtFrontOfQueue(sWebCoreHandler.obtainMessage(1));
    }

    private void requestKeyboard(boolean paramBoolean)
    {
        Handler localHandler;
        if (this.mWebViewClassic != null)
        {
            localHandler = this.mWebViewClassic.mPrivateHandler;
            if (!paramBoolean)
                break label33;
        }
        label33: for (int i = 1; ; i = 0)
        {
            Message.obtain(localHandler, 118, i, 0).sendToTarget();
            return;
        }
    }

    private void requestListBox(String[] paramArrayOfString, int[] paramArrayOfInt, int paramInt)
    {
        if (this.mWebViewClassic != null)
            this.mWebViewClassic.requestListBox(paramArrayOfString, paramArrayOfInt, paramInt);
    }

    private void requestListBox(String[] paramArrayOfString, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        if (this.mWebViewClassic != null)
            this.mWebViewClassic.requestListBox(paramArrayOfString, paramArrayOfInt1, paramArrayOfInt2);
    }

    private void restoreScale(float paramFloat1, float paramFloat2)
    {
        if (!this.mBrowserFrame.firstLayoutDone())
        {
            this.mIsRestored = true;
            this.mRestoredScale = paramFloat1;
            if (this.mSettings.getUseWideViewPort())
                this.mRestoredTextWrapScale = paramFloat2;
        }
    }

    private void restoreState(int paramInt)
    {
        WebBackForwardList localWebBackForwardList = this.mCallbackProxy.getBackForwardList();
        int i = localWebBackForwardList.getSize();
        for (int j = 0; j < i; j++)
            localWebBackForwardList.getItemAtIndex(j).inflate(this.mBrowserFrame.mNativeFrame);
        this.mBrowserFrame.mLoadInitFromJava = true;
        WebBackForwardList.restoreIndex(this.mBrowserFrame.mNativeFrame, paramInt);
        this.mBrowserFrame.mLoadInitFromJava = false;
    }

    static void resumePriority()
    {
        sWebCoreHandler.removeMessages(1);
        sWebCoreHandler.removeMessages(2);
        sWebCoreHandler.sendMessageAtFrontOfQueue(sWebCoreHandler.obtainMessage(2));
    }

    public static void resumeTimers()
    {
        if (BrowserFrame.sJavaBridge == null)
            throw new IllegalStateException("No WebView has been created in this process!");
        BrowserFrame.sJavaBridge.resume();
    }

    // ERROR //
    static void resumeUpdatePicture(WebViewCore paramWebViewCore)
    {
        // Byte code:
        //     0: aload_0
        //     1: ifnull +10 -> 11
        //     4: aload_0
        //     5: getfield 1066	android/webkit/WebViewCore:mDrawIsPaused	Z
        //     8: ifne +4 -> 12
        //     11: return
        //     12: aload_0
        //     13: monitorenter
        //     14: aload_0
        //     15: getfield 554	android/webkit/WebViewCore:mNativeClass	I
        //     18: ifne +22 -> 40
        //     21: ldc 89
        //     23: ldc_w 1257
        //     26: invokestatic 968	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     29: pop
        //     30: aload_0
        //     31: monitorexit
        //     32: goto -21 -> 11
        //     35: astore_1
        //     36: aload_0
        //     37: monitorexit
        //     38: aload_1
        //     39: athrow
        //     40: aload_0
        //     41: aload_0
        //     42: getfield 554	android/webkit/WebViewCore:mNativeClass	I
        //     45: iconst_0
        //     46: invokespecial 1191	android/webkit/WebViewCore:nativeSetIsPaused	(IZ)V
        //     49: aload_0
        //     50: iconst_0
        //     51: putfield 1066	android/webkit/WebViewCore:mDrawIsPaused	Z
        //     54: aload_0
        //     55: iconst_0
        //     56: putfield 789	android/webkit/WebViewCore:mDrawIsScheduled	Z
        //     59: aload_0
        //     60: monitorexit
        //     61: goto -50 -> 11
        //
        // Exception table:
        //     from	to	target	type
        //     14	38	35	finally
        //     40	61	35	finally
    }

    private void saveViewState(OutputStream paramOutputStream, ValueCallback<Boolean> paramValueCallback)
    {
        DrawData localDrawData = new DrawData();
        localDrawData.mBaseLayer = nativeRecordContent(this.mNativeClass, localDrawData.mContentSize);
        boolean bool1 = false;
        try
        {
            boolean bool2 = ViewStateSerializer.serializeViewState(paramOutputStream, localDrawData);
            bool1 = bool2;
            paramValueCallback.onReceiveValue(Boolean.valueOf(bool1));
            if (localDrawData.mBaseLayer != 0)
            {
                if (this.mDrawIsScheduled)
                {
                    this.mDrawIsScheduled = false;
                    this.mEventHub.removeMessages(130);
                }
                this.mLastDrawData = localDrawData;
                webkitDraw(localDrawData);
            }
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Log.w("webcore", "Failed to save view state", localThrowable);
        }
    }

    private String saveWebArchive(String paramString, boolean paramBoolean)
    {
        return this.mBrowserFrame.saveWebArchive(paramString, paramBoolean);
    }

    private void selectAt(int paramInt1, int paramInt2)
    {
    }

    private void sendNotifyProgressFinished()
    {
        contentDraw();
    }

    private void sendPluginDrawMsg()
    {
        sendMessage(195);
    }

    static void sendStaticMessage(int paramInt, Object paramObject)
    {
        if (sWebCoreHandler == null);
        while (true)
        {
            return;
            sWebCoreHandler.sendMessage(sWebCoreHandler.obtainMessage(paramInt, paramObject));
        }
    }

    private void sendViewInvalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (this.mWebViewClassic != null)
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 117, new Rect(paramInt1, paramInt2, paramInt3, paramInt4)).sendToTarget();
    }

    private void setScrollbarModes(int paramInt1, int paramInt2)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            this.mWebViewClassic.mPrivateHandler.obtainMessage(129, paramInt1, paramInt2).sendToTarget();
        }
    }

    static void setShouldMonitorWebCoreThread()
    {
        sShouldMonitorWebCoreThread = true;
    }

    private void setUseMockDeviceOrientation()
    {
        this.mDeviceMotionAndOrientationManager.setUseMock();
    }

    private native void setViewportSettingsFromNative(int paramInt);

    private void setWebTextViewAutoFillable(int paramInt, String paramString)
    {
        if (this.mWebViewClassic != null)
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 133, new AutoFillData(paramInt, paramString)).sendToTarget();
    }

    private void setupViewport(boolean paramBoolean)
    {
        if ((this.mWebViewClassic == null) || (this.mSettings == null));
        label144: label488: int j;
        label563: int k;
        while (true)
        {
            return;
            setViewportSettingsFromNative(this.mNativeClass);
            if (this.mViewportInitialScale > 0)
            {
                if (this.mViewportMinimumScale > 0)
                    this.mViewportInitialScale = Math.max(this.mViewportInitialScale, this.mViewportMinimumScale);
                if (this.mViewportMaximumScale > 0)
                    this.mViewportInitialScale = Math.min(this.mViewportInitialScale, this.mViewportMaximumScale);
            }
            if (this.mSettings.forceUserScalable())
            {
                this.mViewportUserScalable = true;
                if (this.mViewportInitialScale <= 0)
                    break label488;
                if (this.mViewportMinimumScale > 0)
                    this.mViewportMinimumScale = Math.min(this.mViewportMinimumScale, this.mViewportInitialScale / 2);
                if (this.mViewportMaximumScale > 0)
                    this.mViewportMaximumScale = Math.max(this.mViewportMaximumScale, 2 * this.mViewportInitialScale);
            }
            float f1 = 1.0F;
            if (this.mViewportDensityDpi == -1)
                f1 = this.mContext.getResources().getDisplayMetrics().density;
            while (true)
            {
                this.mWebViewClassic.mPrivateHandler.removeMessages(139);
                if (f1 != this.mWebViewClassic.getDefaultZoomScale())
                    Message.obtain(this.mWebViewClassic.mPrivateHandler, 139, Float.valueOf(f1)).sendToTarget();
                int i = (int)(100.0F * f1);
                if (this.mViewportInitialScale > 0)
                    this.mViewportInitialScale = ((int)(f1 * this.mViewportInitialScale));
                if (this.mViewportMinimumScale > 0)
                    this.mViewportMinimumScale = ((int)(f1 * this.mViewportMinimumScale));
                if (this.mViewportMaximumScale > 0)
                    this.mViewportMaximumScale = ((int)(f1 * this.mViewportMaximumScale));
                if ((this.mViewportWidth == 0) && (this.mViewportInitialScale == 0))
                    this.mViewportInitialScale = i;
                if (!this.mViewportUserScalable)
                {
                    this.mViewportInitialScale = i;
                    this.mViewportMinimumScale = i;
                    this.mViewportMaximumScale = i;
                }
                if ((this.mViewportMinimumScale > this.mViewportInitialScale) && (this.mViewportInitialScale != 0))
                    this.mViewportMinimumScale = this.mViewportInitialScale;
                if ((this.mViewportMaximumScale > 0) && (this.mViewportMaximumScale < this.mViewportInitialScale))
                    this.mViewportMaximumScale = this.mViewportInitialScale;
                if ((this.mViewportWidth < 0) && (this.mViewportInitialScale == i))
                    this.mViewportWidth = 0;
                if ((this.mViewportWidth == 0) || (paramBoolean))
                    break label563;
                this.mFirstLayoutForNonStandardLoad = true;
                ViewState localViewState7 = new ViewState();
                localViewState7.mMinScale = (this.mViewportMinimumScale / 100.0F);
                localViewState7.mMaxScale = (this.mViewportMaximumScale / 100.0F);
                localViewState7.mDefaultScale = f1;
                localViewState7.mMobileSite = false;
                localViewState7.mScrollX = 0;
                localViewState7.mShouldStartScrolledRight = false;
                Message.obtain(this.mWebViewClassic.mPrivateHandler, 109, localViewState7).sendToTarget();
                break;
                if (this.mViewportMinimumScale > 0)
                    this.mViewportMinimumScale = Math.min(this.mViewportMinimumScale, 50);
                if (this.mViewportMaximumScale <= 0)
                    break label144;
                this.mViewportMaximumScale = Math.max(this.mViewportMaximumScale, 200);
                break label144;
                if (this.mViewportDensityDpi > 0)
                    f1 = this.mContext.getResources().getDisplayMetrics().densityDpi / this.mViewportDensityDpi;
            }
            j = this.mCurrentViewWidth;
            label596: boolean bool1;
            label709: boolean bool2;
            if (j == 0)
            {
                k = this.mWebViewClassic.getViewWidth();
                j = (int)(k / f1);
                if (j == 0);
                this.mInitialViewState = new ViewState();
                this.mInitialViewState.mMinScale = (this.mViewportMinimumScale / 100.0F);
                this.mInitialViewState.mMaxScale = (this.mViewportMaximumScale / 100.0F);
                this.mInitialViewState.mDefaultScale = f1;
                this.mInitialViewState.mScrollX = this.mRestoredX;
                this.mInitialViewState.mScrollY = this.mRestoredY;
                ViewState localViewState1 = this.mInitialViewState;
                if ((this.mRestoredX != 0) || (this.mRestoredY != 0) || (this.mBrowserFrame == null) || (!this.mBrowserFrame.getShouldStartScrolledRight()))
                    break label909;
                bool1 = true;
                localViewState1.mShouldStartScrolledRight = bool1;
                ViewState localViewState2 = this.mInitialViewState;
                if (this.mViewportWidth != 0)
                    break label915;
                bool2 = true;
                label732: localViewState2.mMobileSite = bool2;
                if (!this.mIsRestored)
                    break label938;
                this.mInitialViewState.mIsRestored = true;
                this.mInitialViewState.mViewScale = this.mRestoredScale;
                if (this.mRestoredTextWrapScale <= 0.0F)
                    break label921;
                this.mInitialViewState.mTextWrapScale = this.mRestoredTextWrapScale;
            }
            while (true)
            {
                if (!this.mWebViewClassic.mHeightCanMeasure)
                    break label1091;
                this.mWebViewClassic.mLastHeightSent = 0;
                WebViewClassic.ViewSizeData localViewSizeData2 = new WebViewClassic.ViewSizeData();
                localViewSizeData2.mWidth = this.mWebViewClassic.mLastWidthSent;
                localViewSizeData2.mHeight = 0;
                localViewSizeData2.mTextWrapWidth = localViewSizeData2.mWidth;
                localViewSizeData2.mScale = -1.0F;
                localViewSizeData2.mIgnoreHeight = false;
                localViewSizeData2.mAnchorY = 0;
                localViewSizeData2.mAnchorX = 0;
                this.mEventHub.removeMessages(105);
                this.mEventHub.sendMessageAtFrontOfQueue(Message.obtain(null, 105, localViewSizeData2));
                break;
                k = Math.round(j * this.mCurrentViewScale);
                break label596;
                label909: bool1 = false;
                break label709;
                label915: bool2 = false;
                break label732;
                label921: this.mInitialViewState.mTextWrapScale = this.mInitialViewState.mViewScale;
                continue;
                label938: if (this.mViewportInitialScale > 0)
                {
                    ViewState localViewState5 = this.mInitialViewState;
                    ViewState localViewState6 = this.mInitialViewState;
                    float f5 = this.mViewportInitialScale / 100.0F;
                    localViewState6.mTextWrapScale = f5;
                    localViewState5.mViewScale = f5;
                }
                else if ((this.mViewportWidth > 0) && (this.mViewportWidth < k) && (!getSettings().getUseFixedViewport()))
                {
                    ViewState localViewState3 = this.mInitialViewState;
                    ViewState localViewState4 = this.mInitialViewState;
                    float f4 = k / this.mViewportWidth;
                    localViewState4.mTextWrapScale = f4;
                    localViewState3.mViewScale = f4;
                }
                else
                {
                    this.mInitialViewState.mTextWrapScale = f1;
                    if (this.mSettings.getUseWideViewPort())
                        this.mInitialViewState.mViewScale = 0.0F;
                    else
                        this.mInitialViewState.mViewScale = f1;
                }
            }
            label1091: if (j != 0)
                break;
            this.mWebViewClassic.mLastWidthSent = 0;
        }
        WebViewClassic.ViewSizeData localViewSizeData1 = new WebViewClassic.ViewSizeData();
        float f2 = this.mInitialViewState.mViewScale;
        if (f2 == 0.0F)
        {
            float f3 = this.mInitialViewState.mTextWrapScale;
            int n = calculateWindowWidth(Math.round(k / f3));
            localViewSizeData1.mScale = (k / n);
            if (!this.mSettings.getLoadWithOverviewMode())
                localViewSizeData1.mScale = Math.max(localViewSizeData1.mScale, f3);
            if (this.mSettings.isNarrowColumnLayout())
                this.mInitialViewState.mTextWrapScale = this.mWebViewClassic.computeReadingLevelScale(localViewSizeData1.mScale);
            label1222: localViewSizeData1.mWidth = Math.round(k / localViewSizeData1.mScale);
            if (this.mCurrentViewHeight != 0)
                break label1337;
        }
        label1337: for (int m = Math.round(this.mWebViewClassic.getViewHeight() / localViewSizeData1.mScale); ; m = Math.round(this.mCurrentViewHeight * localViewSizeData1.mWidth / j))
        {
            localViewSizeData1.mHeight = m;
            localViewSizeData1.mTextWrapWidth = Math.round(k / this.mInitialViewState.mTextWrapScale);
            localViewSizeData1.mIgnoreHeight = false;
            localViewSizeData1.mAnchorY = 0;
            localViewSizeData1.mAnchorX = 0;
            this.mEventHub.removeMessages(105);
            viewSizeChanged(localViewSizeData1);
            break;
            localViewSizeData1.mScale = f2;
            break label1222;
        }
    }

    private void showFullScreenPlugin(ViewManager.ChildView paramChildView, int paramInt1, int paramInt2)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            Message localMessage = this.mWebViewClassic.mPrivateHandler.obtainMessage(120);
            localMessage.obj = paramChildView.mView;
            localMessage.arg1 = paramInt1;
            localMessage.arg2 = paramInt2;
            localMessage.sendToTarget();
        }
    }

    private void showRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if (this.mWebViewClassic != null)
        {
            ShowRectData localShowRectData = new ShowRectData();
            localShowRectData.mLeft = paramInt1;
            localShowRectData.mTop = paramInt2;
            localShowRectData.mWidth = paramInt3;
            localShowRectData.mHeight = paramInt4;
            localShowRectData.mContentWidth = paramInt5;
            localShowRectData.mContentHeight = paramInt6;
            localShowRectData.mXPercentInDoc = paramFloat1;
            localShowRectData.mXPercentInView = paramFloat2;
            localShowRectData.mYPercentInDoc = paramFloat3;
            localShowRectData.mYPercentInView = paramFloat4;
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 113, localShowRectData).sendToTarget();
        }
    }

    private void updateSurface(ViewManager.ChildView paramChildView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        paramChildView.attachView(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    private void updateTextSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if (this.mWebViewClassic != null)
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 112, paramInt1, paramInt4, createTextSelection(paramInt2, paramInt3, paramInt5)).sendToTarget();
    }

    private void updateTextSizeAndScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if (this.mWebViewClassic != null)
        {
            Rect localRect = new Rect(-paramInt4, -paramInt5, paramInt2 - paramInt4, paramInt3 - paramInt5);
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 150, paramInt1, 0, localRect).sendToTarget();
        }
    }

    private void updateTextfield(int paramInt1, boolean paramBoolean, String paramString, int paramInt2)
    {
        if (this.mWebViewClassic != null)
        {
            Message localMessage = Message.obtain(this.mWebViewClassic.mPrivateHandler, 108, paramInt1, paramInt2, paramString);
            localMessage.getData().putBoolean("password", paramBoolean);
            localMessage.sendToTarget();
        }
    }

    private void updateViewport()
    {
        setupViewport(true);
    }

    private void viewSizeChanged(WebViewClassic.ViewSizeData paramViewSizeData)
    {
        int i = paramViewSizeData.mWidth;
        int j = paramViewSizeData.mHeight;
        int k = paramViewSizeData.mTextWrapWidth;
        float f1 = paramViewSizeData.mScale;
        if (i == 0)
        {
            Log.w("webcore", "skip viewSizeChanged as w is 0");
            return;
        }
        int m = calculateWindowWidth(i);
        int n = j;
        float f3;
        label69: int i1;
        if (m != i)
        {
            float f2 = paramViewSizeData.mHeightWidthRatio;
            if (f2 > 0.0F)
            {
                f3 = f2;
                n = Math.round(f3 * m);
            }
        }
        else
        {
            if (paramViewSizeData.mActualViewHeight <= 0)
                break label172;
            i1 = paramViewSizeData.mActualViewHeight;
            label93: nativeSetSize(this.mNativeClass, m, n, k, f1, i, i1, paramViewSizeData.mAnchorX, paramViewSizeData.mAnchorY, paramViewSizeData.mIgnoreHeight);
            if (this.mCurrentViewWidth != 0)
                break label178;
        }
        label172: label178: for (int i2 = 1; ; i2 = 0)
        {
            this.mCurrentViewWidth = i;
            this.mCurrentViewHeight = j;
            this.mCurrentViewScale = f1;
            if (i2 == 0)
                break;
            contentDraw();
            break;
            f3 = j / i;
            break label69;
            i1 = j;
            break label93;
        }
    }

    private void webkitDraw()
    {
        DrawData localDrawData;
        synchronized (this.m_skipDrawFlagLock)
        {
            if (this.m_skipDrawFlag)
            {
                this.m_drawWasSkipped = true;
            }
            else
            {
                this.mDrawIsScheduled = false;
                localDrawData = new DrawData();
                localDrawData.mBaseLayer = nativeRecordContent(this.mNativeClass, localDrawData.mContentSize);
                if (localDrawData.mBaseLayer == 0)
                {
                    if ((this.mWebViewClassic == null) || (this.mWebViewClassic.isPaused()))
                        return;
                    this.mEventHub.sendMessageDelayed(Message.obtain(null, 130), 10L);
                }
            }
        }
        this.mLastDrawData = localDrawData;
        webkitDraw(localDrawData);
    }

    private void webkitDraw(DrawData paramDrawData)
    {
        int i;
        if (this.mWebViewClassic != null)
        {
            paramDrawData.mFocusSizeChanged = nativeFocusBoundsChanged(this.mNativeClass);
            paramDrawData.mViewSize = new Point(this.mCurrentViewWidth, this.mCurrentViewHeight);
            if (this.mSettings.getUseWideViewPort())
            {
                if (this.mViewportWidth != -1)
                    break label135;
                i = 980;
            }
        }
        while (true)
        {
            paramDrawData.mMinPrefWidth = Math.max(i, nativeGetContentMinPrefWidth(this.mNativeClass));
            if (this.mInitialViewState != null)
            {
                paramDrawData.mViewState = this.mInitialViewState;
                this.mInitialViewState = null;
            }
            if (this.mFirstLayoutForNonStandardLoad)
            {
                paramDrawData.mFirstLayoutForNonStandardLoad = true;
                this.mFirstLayoutForNonStandardLoad = false;
            }
            pauseWebKitDraw();
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 105, paramDrawData).sendToTarget();
            return;
            label135: if (this.mViewportWidth == 0)
                i = this.mCurrentViewWidth;
            else
                i = this.mViewportWidth;
        }
    }

    protected void addMessageToConsole(String paramString1, int paramInt1, String paramString2, int paramInt2)
    {
        this.mCallbackProxy.addMessageToConsole(paramString1, paramInt1, paramString2, paramInt2);
    }

    void clearContent()
    {
        nativeClearContent(this.mNativeClass);
    }

    // ERROR //
    void contentDraw()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 349	android/webkit/WebViewCore:mWebViewClassic	Landroid/webkit/WebViewClassic;
        //     6: ifnull +10 -> 16
        //     9: aload_0
        //     10: getfield 480	android/webkit/WebViewCore:mBrowserFrame	Landroid/webkit/BrowserFrame;
        //     13: ifnonnull +8 -> 21
        //     16: aload_0
        //     17: monitorexit
        //     18: goto +63 -> 81
        //     21: aload_0
        //     22: getfield 331	android/webkit/WebViewCore:mCurrentViewWidth	I
        //     25: ifeq +13 -> 38
        //     28: aload_0
        //     29: getfield 480	android/webkit/WebViewCore:mBrowserFrame	Landroid/webkit/BrowserFrame;
        //     32: invokevirtual 866	android/webkit/BrowserFrame:firstLayoutDone	()Z
        //     35: ifne +13 -> 48
        //     38: aload_0
        //     39: monitorexit
        //     40: goto +41 -> 81
        //     43: astore_1
        //     44: aload_0
        //     45: monitorexit
        //     46: aload_1
        //     47: athrow
        //     48: aload_0
        //     49: getfield 789	android/webkit/WebViewCore:mDrawIsScheduled	Z
        //     52: ifeq +8 -> 60
        //     55: aload_0
        //     56: monitorexit
        //     57: goto +24 -> 81
        //     60: aload_0
        //     61: iconst_1
        //     62: putfield 789	android/webkit/WebViewCore:mDrawIsScheduled	Z
        //     65: aload_0
        //     66: getfield 388	android/webkit/WebViewCore:mEventHub	Landroid/webkit/WebViewCore$EventHub;
        //     69: aconst_null
        //     70: sipush 130
        //     73: invokestatic 861	android/os/Message:obtain	(Landroid/os/Handler;I)Landroid/os/Message;
        //     76: invokestatic 880	android/webkit/WebViewCore$EventHub:access$7400	(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V
        //     79: aload_0
        //     80: monitorexit
        //     81: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	46	43	finally
        //     48	81	43	finally
    }

    void destroy()
    {
        synchronized (this.mEventHub)
        {
            EventHub.access$1202(this.mEventHub, true);
            this.mEventHub.sendMessageAtFrontOfQueue(Message.obtain(null, 200));
            this.mEventHub.blockMessages();
            WebCoreThreadWatchdog.unregisterWebView(this.mWebViewClassic);
            return;
        }
    }

    protected void enterFullscreenForVideoLayer(int paramInt, String paramString)
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            Message localMessage = Message.obtain(this.mWebViewClassic.mPrivateHandler, 137, paramInt, 0);
            localMessage.obj = paramString;
            localMessage.sendToTarget();
        }
    }

    protected void exceededDatabaseQuota(String paramString1, String paramString2, long paramLong1, long paramLong2)
    {
        this.mCallbackProxy.onExceededDatabaseQuota(paramString1, paramString2, paramLong1, paramLong2, getUsedQuota(), new WebStorage.QuotaUpdater()
        {
            public void updateQuota(long paramAnonymousLong)
            {
                WebViewCore.this.nativeSetNewStorageLimit(WebViewCore.this.mNativeClass, paramAnonymousLong);
            }
        });
    }

    protected void exitFullscreenVideo()
    {
        if (this.mWebViewClassic == null);
        while (true)
        {
            return;
            Message.obtain(this.mWebViewClassic.mPrivateHandler, 140).sendToTarget();
        }
    }

    protected void geolocationPermissionsHidePrompt()
    {
        this.mCallbackProxy.onGeolocationPermissionsHidePrompt();
    }

    protected void geolocationPermissionsShowPrompt(String paramString)
    {
        this.mCallbackProxy.onGeolocationPermissionsShowPrompt(paramString, new GeolocationPermissions.Callback()
        {
            public void invoke(String paramAnonymousString, boolean paramAnonymousBoolean1, boolean paramAnonymousBoolean2)
            {
                WebViewCore.GeolocationPermissionsData localGeolocationPermissionsData = new WebViewCore.GeolocationPermissionsData();
                localGeolocationPermissionsData.mOrigin = paramAnonymousString;
                localGeolocationPermissionsData.mAllow = paramAnonymousBoolean1;
                localGeolocationPermissionsData.mRemember = paramAnonymousBoolean2;
                WebViewCore.this.sendMessage(180, localGeolocationPermissionsData);
            }
        });
    }

    /** @deprecated */
    BrowserFrame getBrowserFrame()
    {
        try
        {
            BrowserFrame localBrowserFrame = this.mBrowserFrame;
            return localBrowserFrame;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    Context getContext()
    {
        return this.mContext;
    }

    protected DeviceMotionService getDeviceMotionService()
    {
        if (this.mDeviceMotionService == null)
            this.mDeviceMotionService = new DeviceMotionService(this.mDeviceMotionAndOrientationManager, this.mContext);
        return this.mDeviceMotionService;
    }

    protected DeviceOrientationService getDeviceOrientationService()
    {
        if (this.mDeviceOrientationService == null)
            this.mDeviceOrientationService = new DeviceOrientationService(this.mDeviceMotionAndOrientationManager, this.mContext);
        return this.mDeviceOrientationService;
    }

    public WebViewInputDispatcher.WebKitCallbacks getInputDispatcherCallbacks()
    {
        return this.mEventHub;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    String[] getReadModeString()
    {
        return this.mReadModeString;
    }

    public WebSettingsClassic getSettings()
    {
        return this.mSettings;
    }

    WebViewClassic getWebViewClassic()
    {
        return this.mWebViewClassic;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public Vector<String> getmReadModeTemplate()
    {
        return this.mReadModeTemplateVector;
    }

    void initializeSubwindow()
    {
        initialize();
        sWebCoreHandler.removeMessages(0, this);
    }

    protected void jsAlert(String paramString1, String paramString2)
    {
        this.mCallbackProxy.onJsAlert(paramString1, paramString2);
    }

    protected boolean jsConfirm(String paramString1, String paramString2)
    {
        return this.mCallbackProxy.onJsConfirm(paramString1, paramString2);
    }

    protected boolean jsInterrupt()
    {
        return this.mCallbackProxy.onJsTimeout();
    }

    protected String jsPrompt(String paramString1, String paramString2, String paramString3)
    {
        return this.mCallbackProxy.onJsPrompt(paramString1, paramString2, paramString3);
    }

    protected boolean jsUnload(String paramString1, String paramString2)
    {
        return this.mCallbackProxy.onJsBeforeUnload(paramString1, paramString2);
    }

    void pauseWebKitDraw()
    {
        synchronized (this.m_skipDrawFlagLock)
        {
            if (!this.m_skipDrawFlag)
                this.m_skipDrawFlag = true;
            return;
        }
    }

    protected void populateVisitedLinks()
    {
        ValueCallback local3 = new ValueCallback()
        {
            public void onReceiveValue(String[] paramAnonymousArrayOfString)
            {
                WebViewCore.this.sendMessage(181, paramAnonymousArrayOfString);
            }
        };
        this.mCallbackProxy.getVisitedHistory(local3);
    }

    protected void reachedMaxAppCacheSize(long paramLong)
    {
        this.mCallbackProxy.onReachedMaxAppCacheSize(paramLong, getUsedQuota(), new WebStorage.QuotaUpdater()
        {
            public void updateQuota(long paramAnonymousLong)
            {
                WebViewCore.this.nativeSetNewStorageLimit(WebViewCore.this.mNativeClass, paramAnonymousLong);
            }
        });
    }

    void removeMessages()
    {
        this.mEventHub.removeMessages();
    }

    void removeMessages(int paramInt)
    {
        this.mEventHub.removeMessages(paramInt);
    }

    void resumeWebKitDraw()
    {
        synchronized (this.m_skipDrawFlagLock)
        {
            if ((this.m_skipDrawFlag) && (this.m_drawWasSkipped))
            {
                this.m_drawWasSkipped = false;
                this.mEventHub.sendMessage(Message.obtain(null, 130));
            }
            this.m_skipDrawFlag = false;
            return;
        }
    }

    void sendMessage(int paramInt)
    {
        this.mEventHub.sendMessage(Message.obtain(null, paramInt));
    }

    void sendMessage(int paramInt1, int paramInt2)
    {
        this.mEventHub.sendMessage(Message.obtain(null, paramInt1, paramInt2, 0));
    }

    void sendMessage(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mEventHub.sendMessage(Message.obtain(null, paramInt1, paramInt2, paramInt3));
    }

    void sendMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        this.mEventHub.sendMessage(Message.obtain(null, paramInt1, paramInt2, paramInt3, paramObject));
    }

    void sendMessage(int paramInt1, int paramInt2, Object paramObject)
    {
        this.mEventHub.sendMessage(Message.obtain(null, paramInt1, paramInt2, 0, paramObject));
    }

    void sendMessage(int paramInt, Object paramObject)
    {
        this.mEventHub.sendMessage(Message.obtain(null, paramInt, paramObject));
    }

    public void sendMessage(Message paramMessage)
    {
        this.mEventHub.sendMessage(paramMessage);
    }

    void sendMessageAtFrontOfQueue(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        this.mEventHub.sendMessageAtFrontOfQueue(Message.obtain(null, paramInt1, paramInt2, paramInt3, paramObject));
    }

    void sendMessageAtFrontOfQueue(int paramInt, Object paramObject)
    {
        this.mEventHub.sendMessageAtFrontOfQueue(Message.obtain(null, paramInt, paramObject));
    }

    void sendMessageDelayed(int paramInt, Object paramObject, long paramLong)
    {
        this.mEventHub.sendMessageDelayed(Message.obtain(null, paramInt, paramObject), paramLong);
    }

    void sendMessages(ArrayList<Message> paramArrayList)
    {
        EventHub localEventHub = this.mEventHub;
        int i = 0;
        try
        {
            while (i < paramArrayList.size())
            {
                this.mEventHub.sendMessage((Message)paramArrayList.get(i));
                i++;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void setInstallableWebApp()
    {
        this.mCallbackProxy.setInstallableWebApp();
    }

    public void setMockDeviceOrientation(boolean paramBoolean1, double paramDouble1, boolean paramBoolean2, double paramDouble2, boolean paramBoolean3, double paramDouble3)
    {
        this.mDeviceMotionAndOrientationManager.setMockOrientation(paramBoolean1, paramDouble1, paramBoolean2, paramDouble2, paramBoolean3, paramDouble3);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setmReadModeTemplate(Vector<String> paramVector)
    {
        this.mReadModeTemplateVector = paramVector;
    }

    void signalRepaintDone()
    {
        mRepaintScheduled = false;
    }

    void stopLoading()
    {
        if (this.mBrowserFrame != null)
            this.mBrowserFrame.stopLoading();
    }

    static class ShowRectData
    {
        int mContentHeight;
        int mContentWidth;
        int mHeight;
        int mLeft;
        int mTop;
        int mWidth;
        float mXPercentInDoc;
        float mXPercentInView;
        float mYPercentInDoc;
        float mYPercentInView;
    }

    static class DrawData
    {
        int mBaseLayer = 0;
        Point mContentSize = new Point();
        boolean mFirstLayoutForNonStandardLoad;
        boolean mFocusSizeChanged;
        int mMinPrefWidth;
        Point mViewSize;
        WebViewCore.ViewState mViewState;
    }

    static class ViewState
    {
        float mDefaultScale;
        boolean mIsRestored;
        float mMaxScale;
        float mMinScale;
        boolean mMobileSite;
        int mScrollX;
        int mScrollY;
        boolean mShouldStartScrolledRight;
        float mTextWrapScale;
        float mViewScale;
    }

    public class EventHub
        implements WebViewInputDispatcher.WebKitCallbacks
    {
        static final int ADD_JS_INTERFACE = 138;
        static final int ADD_PACKAGE_NAME = 185;
        static final int ADD_PACKAGE_NAMES = 184;
        static final int AUTOFILL_FORM = 192;

        @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
        static final int CHECK_READ_MODE = 5010;
        static final int CLEAR_CACHE = 111;
        static final int CLEAR_CONTENT = 134;
        static final int CLEAR_HISTORY = 112;
        static final int CLEAR_SSL_PREF_TABLE = 150;
        static final int CONTENT_INVALIDATE_ALL = 175;
        static final int COPY_TEXT = 210;
        static final int DELETE_SELECTION = 122;
        static final int DELETE_SURROUNDING_TEXT = 129;
        static final int DELETE_TEXT = 211;
        private static final int DESTROY = 200;
        static final int DOC_HAS_IMAGES = 120;
        static final int DUMP_DOMTREE = 170;
        static final int DUMP_RENDERTREE = 171;
        static final int EXECUTE_JS = 194;
        static final int FIND_ALL = 221;
        static final int FIND_NEXT = 222;
        private static final int FIRST_PACKAGE_MSG_ID = 96;
        static final int FREE_MEMORY = 145;
        static final int GEOLOCATION_PERMISSIONS_PROVIDE = 180;
        static final int GO_BACK_FORWARD = 106;
        static final int HEARTBEAT = 197;
        static final int HIDE_FULLSCREEN = 182;
        static final int INSERT_TEXT = 212;
        static final int KEY_DOWN = 103;
        static final int KEY_PRESS = 223;
        static final int KEY_UP = 104;
        private static final int LAST_PACKAGE_MSG_ID = 149;
        static final int LISTBOX_CHOICES = 123;
        static final int LOAD_DATA = 139;
        static final int LOAD_URL = 100;
        public static final int MESSAGE_RELAY = 125;
        static final int MODIFY_SELECTION = 190;
        static final int NOTIFY_ANIMATION_STARTED = 196;
        static final int ON_PAUSE = 143;
        static final int ON_RESUME = 144;
        static final int PASS_TO_JS = 115;
        static final int PAUSE_TIMERS = 109;
        static final int PLUGIN_SURFACE_READY = 195;
        static final int POPULATE_VISITED_LINKS = 181;
        static final int POST_URL = 132;
        static final int PROXY_CHANGED = 193;
        static final int RELOAD = 102;
        static final int REMOVE_JS_INTERFACE = 149;
        static final int REMOVE_PACKAGE_NAME = 186;
        static final int REPLACE_TEXT = 114;
        static final int REQUEST_CURSOR_HREF = 137;
        static final int REQUEST_DOC_AS_TEXT = 161;
        static final int REQUEST_EXT_REPRESENTATION = 160;
        static final int RESTORE_STATE = 108;
        static final int RESUME_TIMERS = 110;
        static final int REVEAL_SELECTION = 96;
        static final int SAVE_DOCUMENT_STATE = 128;
        static final int SAVE_VIEW_STATE = 225;
        static final int SAVE_WEBARCHIVE = 147;
        static final int SCROLL_LAYER = 198;
        static final int SCROLL_TEXT_INPUT = 99;
        static final int SELECT_ALL = 215;
        static final int SELECT_TEXT = 213;
        static final int SELECT_WORD_AT = 214;
        static final int SET_ACTIVE = 142;
        static final int SET_BACKGROUND_COLOR = 126;
        static final int SET_GLOBAL_BOUNDS = 116;
        static final int SET_INITIAL_FOCUS = 224;
        static final int SET_JS_FLAGS = 174;
        static final int SET_MOVE_MOUSE = 135;
        static final int SET_NETWORK_STATE = 119;
        static final int SET_NETWORK_TYPE = 183;
        static final int SET_SCROLL_OFFSET = 107;
        static final int SET_SELECTION = 113;
        static final int SET_USE_MOCK_DEVICE_ORIENTATION = 191;
        static final int SINGLE_LISTBOX_CHOICE = 124;
        static final int STOP_LOADING = 101;
        static final int TRUST_STORAGE_UPDATED = 220;
        static final int VIEW_SIZE_CHANGED = 105;
        static final int WEBKIT_DRAW = 130;
        private boolean mBlockMessages;
        private boolean mDestroying;
        private Handler mHandler;
        private LinkedList<Message> mMessages = new LinkedList();
        private int mSavedPriority;
        private int mTid;

        private EventHub()
        {
        }

        /** @deprecated */
        private void blockMessages()
        {
            try
            {
                this.mBlockMessages = true;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        private void removeMessages()
        {
            try
            {
                WebViewCore.access$7302(WebViewCore.this, false);
                if (this.mMessages != null)
                    this.mMessages.clear();
                while (true)
                {
                    return;
                    this.mHandler.removeCallbacksAndMessages(null);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        private void removeMessages(int paramInt)
        {
            while (true)
            {
                try
                {
                    boolean bool = this.mBlockMessages;
                    if (bool)
                        return;
                    if (paramInt == 130)
                        WebViewCore.access$7302(WebViewCore.this, false);
                    if (this.mMessages != null)
                    {
                        Iterator localIterator = this.mMessages.iterator();
                        if (localIterator.hasNext())
                        {
                            if (((Message)localIterator.next()).what != paramInt)
                                continue;
                            localIterator.remove();
                            continue;
                        }
                        continue;
                    }
                }
                finally
                {
                }
                this.mHandler.removeMessages(paramInt);
            }
        }

        /** @deprecated */
        private void sendMessage(Message paramMessage)
        {
            while (true)
            {
                try
                {
                    boolean bool = this.mBlockMessages;
                    if (bool)
                        return;
                    if (this.mMessages != null)
                    {
                        this.mMessages.add(paramMessage);
                        continue;
                    }
                }
                finally
                {
                }
                this.mHandler.sendMessage(paramMessage);
            }
        }

        /** @deprecated */
        private void sendMessageAtFrontOfQueue(Message paramMessage)
        {
            while (true)
            {
                try
                {
                    boolean bool = this.mBlockMessages;
                    if (bool)
                        return;
                    if (this.mMessages != null)
                    {
                        this.mMessages.add(0, paramMessage);
                        continue;
                    }
                }
                finally
                {
                }
                this.mHandler.sendMessageAtFrontOfQueue(paramMessage);
            }
        }

        /** @deprecated */
        private void sendMessageDelayed(Message paramMessage, long paramLong)
        {
            try
            {
                boolean bool = this.mBlockMessages;
                if (bool);
                while (true)
                {
                    return;
                    this.mHandler.sendMessageDelayed(paramMessage, paramLong);
                }
            }
            finally
            {
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        private void transferMessages()
        {
            this.mTid = Process.myTid();
            this.mSavedPriority = Process.getThreadPriority(this.mTid);
            this.mHandler = new Handler()
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    switch (paramAnonymousMessage.what)
                    {
                    default:
                        if ((WebViewCore.this.mWebViewClassic != null) && (WebViewCore.this.mNativeClass != 0))
                            break;
                    case 109:
                    case 110:
                    }
                    while (true)
                    {
                        return;
                        WebViewCore.EventHub.access$802(WebViewCore.EventHub.this, Process.getThreadPriority(WebViewCore.EventHub.this.mTid));
                        Process.setThreadPriority(WebViewCore.EventHub.this.mTid, 10);
                        WebViewCore.pauseTimers();
                        if (WebViewCore.this.mNativeClass != 0)
                        {
                            WebViewCore.this.nativeCloseIdleConnections(WebViewCore.this.mNativeClass);
                            continue;
                            Process.setThreadPriority(WebViewCore.EventHub.this.mTid, WebViewCore.EventHub.this.mSavedPriority);
                            WebViewCore.resumeTimers();
                            continue;
                            if ((WebViewCore.EventHub.this.mDestroying != true) || (paramAnonymousMessage.what == 200))
                                switch (paramAnonymousMessage.what)
                                {
                                default:
                                    break;
                                case 96:
                                    WebViewCore.this.nativeRevealSelection(WebViewCore.this.mNativeClass);
                                    break;
                                case 130:
                                    WebViewCore.this.webkitDraw();
                                    break;
                                case 200:
                                    synchronized (WebViewCore.this)
                                    {
                                        WebViewCore.this.mCallbackProxy.shutdown();
                                    }
                                    synchronized (WebViewCore.this.mCallbackProxy)
                                    {
                                        WebViewCore.this.mCallbackProxy.notify();
                                        WebViewCore.this.mBrowserFrame.destroy();
                                        WebViewCore.access$1502(WebViewCore.this, null);
                                        WebViewCore.this.mSettings.onDestroyed();
                                        WebViewCore.access$302(WebViewCore.this, 0);
                                        WebViewCore.access$1102(WebViewCore.this, null);
                                        continue;
                                        localObject3 = finally;
                                        throw localObject3;
                                    }
                                case 99:
                                    if (paramAnonymousMessage.obj == null);
                                    for (float f = 0.0F; ; f = ((Float)paramAnonymousMessage.obj).floatValue())
                                    {
                                        Rect localRect3 = new Rect();
                                        WebViewCore localWebViewCore8 = WebViewCore.this;
                                        int i15 = WebViewCore.this.mNativeClass;
                                        int i16 = paramAnonymousMessage.arg2;
                                        localWebViewCore8.nativeScrollFocusedTextInput(i15, f, i16, localRect3);
                                        Message.obtain(WebViewCore.this.mWebViewClassic.mPrivateHandler, 152, localRect3).sendToTarget();
                                        break;
                                    }
                                case 100:
                                    CookieManagerClassic.getInstance().waitForCookieOperationsToComplete();
                                    WebViewCore.GetUrlData localGetUrlData = (WebViewCore.GetUrlData)paramAnonymousMessage.obj;
                                    WebViewCore.this.loadUrl(localGetUrlData.mUrl, localGetUrlData.mExtraHeaders);
                                    break;
                                case 132:
                                    CookieManagerClassic.getInstance().waitForCookieOperationsToComplete();
                                    WebViewCore.PostUrlData localPostUrlData = (WebViewCore.PostUrlData)paramAnonymousMessage.obj;
                                    WebViewCore.this.mBrowserFrame.postUrl(localPostUrlData.mUrl, localPostUrlData.mPostData);
                                    break;
                                case 139:
                                    CookieManagerClassic.getInstance().waitForCookieOperationsToComplete();
                                    WebViewCore.BaseUrlData localBaseUrlData = (WebViewCore.BaseUrlData)paramAnonymousMessage.obj;
                                    String str3 = localBaseUrlData.mBaseUrl;
                                    if (str3 != null)
                                    {
                                        int i14 = str3.indexOf(':');
                                        if (i14 > 0)
                                        {
                                            String str4 = str3.substring(0, i14);
                                            if ((!str4.startsWith("http")) && (!str4.startsWith("ftp")) && (!str4.startsWith("about")) && (!str4.startsWith("javascript")))
                                                WebViewCore.this.nativeRegisterURLSchemeAsLocal(WebViewCore.this.mNativeClass, str4);
                                        }
                                    }
                                    WebViewCore.this.mBrowserFrame.loadData(str3, localBaseUrlData.mData, localBaseUrlData.mMimeType, localBaseUrlData.mEncoding, localBaseUrlData.mHistoryUrl);
                                    WebViewCore.this.nativeContentInvalidateAll(WebViewCore.this.mNativeClass);
                                    break;
                                case 101:
                                    if ((WebViewCore.this.mBrowserFrame.committed()) && (!WebViewCore.this.mBrowserFrame.firstLayoutDone()))
                                        WebViewCore.this.mBrowserFrame.didFirstLayout();
                                    WebViewCore.this.stopLoading();
                                    break;
                                case 102:
                                    WebViewCore.this.mBrowserFrame.reload(false);
                                    break;
                                case 103:
                                    WebViewCore.this.key((KeyEvent)paramAnonymousMessage.obj, paramAnonymousMessage.arg1, true);
                                    break;
                                case 104:
                                    WebViewCore.this.key((KeyEvent)paramAnonymousMessage.obj, paramAnonymousMessage.arg1, false);
                                    break;
                                case 223:
                                    WebViewCore.this.keyPress(paramAnonymousMessage.arg1);
                                    break;
                                case 105:
                                    WebViewCore.this.viewSizeChanged((WebViewClassic.ViewSizeData)paramAnonymousMessage.obj);
                                    break;
                                case 107:
                                    Point localPoint = (Point)paramAnonymousMessage.obj;
                                    WebViewCore localWebViewCore7 = WebViewCore.this;
                                    int i13 = WebViewCore.this.mNativeClass;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool8 = true; ; bool8 = false)
                                    {
                                        localWebViewCore7.nativeSetScrollOffset(i13, bool8, localPoint.x, localPoint.y);
                                        break;
                                    }
                                case 116:
                                    Rect localRect2 = (Rect)paramAnonymousMessage.obj;
                                    WebViewCore.this.nativeSetGlobalBounds(WebViewCore.this.mNativeClass, localRect2.left, localRect2.top, localRect2.width(), localRect2.height());
                                    break;
                                case 106:
                                    if ((!WebViewCore.this.mBrowserFrame.committed()) && (paramAnonymousMessage.arg1 == -1) && (WebViewCore.this.mBrowserFrame.loadType() == 0))
                                        WebViewCore.this.mBrowserFrame.reload(true);
                                    else
                                        WebViewCore.this.mBrowserFrame.goBackOrForward(paramAnonymousMessage.arg1);
                                    break;
                                case 108:
                                    WebViewCore.this.stopLoading();
                                    WebViewCore.this.restoreState(paramAnonymousMessage.arg1);
                                    break;
                                case 143:
                                    WebViewCore.this.nativePause(WebViewCore.this.mNativeClass);
                                    break;
                                case 144:
                                    WebViewCore.this.nativeResume(WebViewCore.this.mNativeClass);
                                    break;
                                case 145:
                                    WebViewCore.this.clearCache(false);
                                    WebViewCore.this.nativeFreeMemory(WebViewCore.this.mNativeClass);
                                    break;
                                case 119:
                                    if (BrowserFrame.sJavaBridge == null)
                                        throw new IllegalStateException("No WebView has been created in this process!");
                                    JWebCoreJavaBridge localJWebCoreJavaBridge = BrowserFrame.sJavaBridge;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool7 = true; ; bool7 = false)
                                    {
                                        localJWebCoreJavaBridge.setNetworkOnLine(bool7);
                                        break;
                                    }
                                case 183:
                                    if (BrowserFrame.sJavaBridge == null)
                                        throw new IllegalStateException("No WebView has been created in this process!");
                                    Map localMap = (Map)paramAnonymousMessage.obj;
                                    BrowserFrame.sJavaBridge.setNetworkType((String)localMap.get("type"), (String)localMap.get("subtype"));
                                    break;
                                case 111:
                                    WebViewCore localWebViewCore6 = WebViewCore.this;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool6 = true; ; bool6 = false)
                                    {
                                        localWebViewCore6.clearCache(bool6);
                                        break;
                                    }
                                case 112:
                                    WebViewCore.this.mCallbackProxy.getBackForwardList().close(WebViewCore.this.mBrowserFrame.mNativeFrame);
                                    break;
                                case 114:
                                    WebViewCore.ReplaceTextData localReplaceTextData = (WebViewCore.ReplaceTextData)paramAnonymousMessage.obj;
                                    WebViewCore.this.nativeReplaceTextfieldText(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1, paramAnonymousMessage.arg2, localReplaceTextData.mReplace, localReplaceTextData.mNewStart, localReplaceTextData.mNewEnd, localReplaceTextData.mTextGeneration);
                                    break;
                                case 115:
                                    WebViewCore.JSKeyData localJSKeyData = (WebViewCore.JSKeyData)paramAnonymousMessage.obj;
                                    KeyEvent localKeyEvent = localJSKeyData.mEvent;
                                    int i10 = localKeyEvent.getKeyCode();
                                    int i11 = localKeyEvent.getUnicodeChar();
                                    int i12 = paramAnonymousMessage.arg1;
                                    WebViewCore.this.passToJs(WebViewCore.this.mNativeClass, i12, localJSKeyData.mCurrentText, i10, i11, localKeyEvent.isDown(), localKeyEvent.isShiftPressed(), localKeyEvent.isAltPressed(), localKeyEvent.isSymPressed());
                                    break;
                                case 128:
                                    WebViewCore.this.nativeSaveDocumentState(WebViewCore.this.mNativeClass);
                                    break;
                                case 150:
                                    SslCertLookupTable.getInstance().clear();
                                    WebViewCore.this.nativeCloseIdleConnections(WebViewCore.this.mNativeClass);
                                    break;
                                case 142:
                                    WebViewCore localWebViewCore5 = WebViewCore.this;
                                    int i9 = WebViewCore.this.mNativeClass;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool5 = true; ; bool5 = false)
                                    {
                                        localWebViewCore5.nativeSetFocusControllerActive(i9, bool5);
                                        break;
                                    }
                                case 138:
                                    WebViewCore.JSInterfaceData localJSInterfaceData2 = (WebViewCore.JSInterfaceData)paramAnonymousMessage.obj;
                                    WebViewCore.this.mBrowserFrame.addJavascriptInterface(localJSInterfaceData2.mObject, localJSInterfaceData2.mInterfaceName);
                                    break;
                                case 149:
                                    WebViewCore.JSInterfaceData localJSInterfaceData1 = (WebViewCore.JSInterfaceData)paramAnonymousMessage.obj;
                                    WebViewCore.this.mBrowserFrame.removeJavascriptInterface(localJSInterfaceData1.mInterfaceName);
                                    break;
                                case 160:
                                    WebViewCore.this.mBrowserFrame.externalRepresentation((Message)paramAnonymousMessage.obj);
                                    break;
                                case 161:
                                    WebViewCore.this.mBrowserFrame.documentAsText((Message)paramAnonymousMessage.obj);
                                    break;
                                case 135:
                                    WebViewCore.this.nativeMoveMouse(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1, paramAnonymousMessage.arg2);
                                    break;
                                case 137:
                                    WebViewCore.WebKitHitTest localWebKitHitTest = WebViewCore.this.performHitTest(paramAnonymousMessage.arg1, paramAnonymousMessage.arg2, 1, false);
                                    Message localMessage2 = (Message)paramAnonymousMessage.obj;
                                    Bundle localBundle = localMessage2.getData();
                                    localBundle.putString("url", localWebKitHitTest.mLinkUrl);
                                    localBundle.putString("title", localWebKitHitTest.mAnchorText);
                                    localBundle.putString("src", localWebKitHitTest.mImageUrl);
                                    localMessage2.sendToTarget();
                                    break;
                                case 120:
                                    Message localMessage1 = (Message)paramAnonymousMessage.obj;
                                    if (WebViewCore.this.mBrowserFrame.documentHasImages());
                                    for (int i8 = 1; ; i8 = 0)
                                    {
                                        localMessage1.arg1 = i8;
                                        localMessage1.sendToTarget();
                                        break;
                                    }
                                case 122:
                                    WebViewCore.TextSelectionData localTextSelectionData = (WebViewCore.TextSelectionData)paramAnonymousMessage.obj;
                                    WebViewCore.this.nativeDeleteSelection(WebViewCore.this.mNativeClass, localTextSelectionData.mStart, localTextSelectionData.mEnd, paramAnonymousMessage.arg1);
                                    break;
                                case 113:
                                    WebViewCore.this.nativeSetSelection(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1, paramAnonymousMessage.arg2);
                                    break;
                                case 190:
                                    WebViewCore.access$4002(WebViewCore.this, 1);
                                    String str2 = WebViewCore.this.nativeModifySelection(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1, paramAnonymousMessage.arg2);
                                    WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(130, str2).sendToTarget();
                                    WebViewCore.access$4002(WebViewCore.this, 0);
                                    break;
                                case 123:
                                    SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)paramAnonymousMessage.obj;
                                    int i6 = paramAnonymousMessage.arg1;
                                    boolean[] arrayOfBoolean = new boolean[i6];
                                    for (int i7 = 0; i7 < i6; i7++)
                                        arrayOfBoolean[i7] = localSparseBooleanArray.get(i7);
                                    WebViewCore.this.nativeSendListBoxChoices(WebViewCore.this.mNativeClass, arrayOfBoolean, i6);
                                    break;
                                case 124:
                                    WebViewCore.this.nativeSendListBoxChoice(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1);
                                    break;
                                case 126:
                                    WebViewCore.this.nativeSetBackgroundColor(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1);
                                    break;
                                case 5010:
                                    long l1 = System.currentTimeMillis();
                                    WebViewCore localWebViewCore4 = WebViewCore.this;
                                    int i5 = WebViewCore.this.mNativeClass;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool4 = true; ; bool4 = false)
                                    {
                                        String[] arrayOfString2 = localWebViewCore4.nativeSwitchReadMode(i5, bool4, WebViewCore.this.mReadModeTemplateVector);
                                        if (arrayOfString2 == null)
                                            break;
                                        WebViewCore.this.mReadModeString[0] = arrayOfString2[0];
                                        WebViewCore.this.mReadModeString[1] = arrayOfString2[1];
                                        WebViewCore.this.mReadModeString[2] = arrayOfString2[2];
                                        long l2 = System.currentTimeMillis() - l1;
                                        Log.d("time-cost", "get read mode string cost time(ms): " + l2);
                                        WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(5001, null).sendToTarget();
                                        break;
                                    }
                                case 170:
                                    WebViewCore localWebViewCore3 = WebViewCore.this;
                                    int i4 = WebViewCore.this.mNativeClass;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool3 = true; ; bool3 = false)
                                    {
                                        localWebViewCore3.nativeDumpDomTree(i4, bool3);
                                        break;
                                    }
                                case 171:
                                    WebViewCore localWebViewCore2 = WebViewCore.this;
                                    int i3 = WebViewCore.this.mNativeClass;
                                    if (paramAnonymousMessage.arg1 == 1);
                                    for (boolean bool2 = true; ; bool2 = false)
                                    {
                                        localWebViewCore2.nativeDumpRenderTree(i3, bool2);
                                        break;
                                    }
                                case 174:
                                    WebViewCore.this.nativeSetJsFlags(WebViewCore.this.mNativeClass, (String)paramAnonymousMessage.obj);
                                    break;
                                case 175:
                                    WebViewCore.this.nativeContentInvalidateAll(WebViewCore.this.mNativeClass);
                                    break;
                                case 147:
                                    WebViewClassic.SaveWebArchiveMessage localSaveWebArchiveMessage = (WebViewClassic.SaveWebArchiveMessage)paramAnonymousMessage.obj;
                                    localSaveWebArchiveMessage.mResultFile = WebViewCore.this.saveWebArchive(localSaveWebArchiveMessage.mBasename, localSaveWebArchiveMessage.mAutoname);
                                    WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(132, localSaveWebArchiveMessage).sendToTarget();
                                    break;
                                case 180:
                                    WebViewCore.GeolocationPermissionsData localGeolocationPermissionsData = (WebViewCore.GeolocationPermissionsData)paramAnonymousMessage.obj;
                                    WebViewCore.this.nativeGeolocationPermissionsProvide(WebViewCore.this.mNativeClass, localGeolocationPermissionsData.mOrigin, localGeolocationPermissionsData.mAllow, localGeolocationPermissionsData.mRemember);
                                    break;
                                case 134:
                                    WebViewCore.this.clearContent();
                                    break;
                                case 125:
                                    ((Message)paramAnonymousMessage.obj).sendToTarget();
                                    break;
                                case 181:
                                    WebViewCore.this.nativeProvideVisitedHistory(WebViewCore.this.mNativeClass, (String[])paramAnonymousMessage.obj);
                                    break;
                                case 182:
                                    WebViewCore.this.nativeFullScreenPluginHidden(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1);
                                    break;
                                case 195:
                                    WebViewCore.this.nativePluginSurfaceReady(WebViewCore.this.mNativeClass);
                                    break;
                                case 196:
                                    WebViewCore.this.nativeNotifyAnimationStarted(WebViewCore.this.mNativeClass);
                                    break;
                                case 184:
                                    if (BrowserFrame.sJavaBridge == null)
                                        throw new IllegalStateException("No WebView has been created in this process!");
                                    BrowserFrame.sJavaBridge.addPackageNames((Set)paramAnonymousMessage.obj);
                                    break;
                                case 191:
                                    WebViewCore.this.setUseMockDeviceOrientation();
                                    break;
                                case 192:
                                    WebViewCore.this.nativeAutoFillForm(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1);
                                    WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(134, null).sendToTarget();
                                    break;
                                case 194:
                                    if ((paramAnonymousMessage.obj instanceof String))
                                        WebViewCore.this.mBrowserFrame.stringByEvaluatingJavaScriptFromString((String)paramAnonymousMessage.obj);
                                    break;
                                case 198:
                                    int i2 = paramAnonymousMessage.arg1;
                                    Rect localRect1 = (Rect)paramAnonymousMessage.obj;
                                    WebViewCore.this.nativeScrollLayer(WebViewCore.this.mNativeClass, i2, localRect1);
                                    break;
                                case 211:
                                    int[] arrayOfInt3 = (int[])paramAnonymousMessage.obj;
                                    WebViewCore.this.nativeDeleteText(WebViewCore.this.mNativeClass, arrayOfInt3[0], arrayOfInt3[1], arrayOfInt3[2], arrayOfInt3[3]);
                                    break;
                                case 210:
                                    int[] arrayOfInt2 = (int[])paramAnonymousMessage.obj;
                                    String str1 = WebViewCore.this.nativeGetText(WebViewCore.this.mNativeClass, arrayOfInt2[0], arrayOfInt2[1], arrayOfInt2[2], arrayOfInt2[3]);
                                    if (str1 != null)
                                        WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(141, str1).sendToTarget();
                                    break;
                                case 212:
                                    WebViewCore.this.nativeInsertText(WebViewCore.this.mNativeClass, (String)paramAnonymousMessage.obj);
                                    break;
                                case 213:
                                    int[] arrayOfInt1 = (int[])paramAnonymousMessage.obj;
                                    if (arrayOfInt1 == null)
                                    {
                                        WebViewCore.this.nativeClearTextSelection(WebViewCore.this.mNativeClass);
                                    }
                                    else
                                    {
                                        String[] arrayOfString1 = WebViewCore.this.nativeSelectText(WebViewCore.this.mNativeClass, arrayOfInt1[0], arrayOfInt1[1], arrayOfInt1[2], arrayOfInt1[3], arrayOfInt1[4]);
                                        WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(5000, arrayOfString1).sendToTarget();
                                    }
                                    break;
                                case 214:
                                    WebViewCore.access$4002(WebViewCore.this, 2);
                                    int n = paramAnonymousMessage.arg1;
                                    int i1 = paramAnonymousMessage.arg2;
                                    if (!WebViewCore.this.nativeSelectWordAt(WebViewCore.this.mNativeClass, n, i1))
                                        WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(151).sendToTarget();
                                    WebViewCore.access$4002(WebViewCore.this, 0);
                                    break;
                                case 215:
                                    WebViewCore.this.nativeSelectAll(WebViewCore.this.mNativeClass);
                                    break;
                                case 221:
                                    WebViewCore.FindAllRequest localFindAllRequest2 = (WebViewCore.FindAllRequest)paramAnonymousMessage.obj;
                                    int k;
                                    int m;
                                    if (localFindAllRequest2 != null)
                                    {
                                        k = WebViewCore.this.nativeFindAll(WebViewCore.this.mNativeClass, localFindAllRequest2.mSearchText);
                                        m = WebViewCore.this.nativeFindNext(WebViewCore.this.mNativeClass, true);
                                    }
                                    while (true)
                                    {
                                        try
                                        {
                                            localFindAllRequest2.mMatchCount = k;
                                            localFindAllRequest2.mMatchIndex = m;
                                            localFindAllRequest2.notify();
                                            Message.obtain(WebViewCore.this.mWebViewClassic.mPrivateHandler, 126, localFindAllRequest2).sendToTarget();
                                            break;
                                        }
                                        finally
                                        {
                                        }
                                        WebViewCore.this.nativeFindAll(WebViewCore.this.mNativeClass, null);
                                    }
                                case 222:
                                    WebViewCore.FindAllRequest localFindAllRequest1 = (WebViewCore.FindAllRequest)paramAnonymousMessage.obj;
                                    WebViewCore localWebViewCore1 = WebViewCore.this;
                                    int i = WebViewCore.this.mNativeClass;
                                    boolean bool1;
                                    int j;
                                    if (paramAnonymousMessage.arg1 != 0)
                                    {
                                        bool1 = true;
                                        j = localWebViewCore1.nativeFindNext(i, bool1);
                                    }
                                    try
                                    {
                                        localFindAllRequest1.mMatchIndex = j;
                                        Message.obtain(WebViewCore.this.mWebViewClassic.mPrivateHandler, 126, localFindAllRequest1).sendToTarget();
                                        continue;
                                        bool1 = false;
                                        break label4051;
                                    }
                                    finally
                                    {
                                    }
                                case 224:
                                    WebViewCore.this.nativeSetInitialFocus(WebViewCore.this.mNativeClass, paramAnonymousMessage.arg1);
                                    break;
                                case 225:
                                    label4051: WebViewCore.SaveViewStateRequest localSaveViewStateRequest = (WebViewCore.SaveViewStateRequest)paramAnonymousMessage.obj;
                                    WebViewCore.this.saveViewState(localSaveViewStateRequest.mStream, localSaveViewStateRequest.mCallback);
                                }
                        }
                    }
                }
            };
            try
            {
                int i = this.mMessages.size();
                for (int j = 0; j < i; j++)
                    this.mHandler.sendMessage((Message)this.mMessages.get(j));
                this.mMessages = null;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public boolean dispatchWebKitEvent(WebViewInputDispatcher paramWebViewInputDispatcher, MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
        {
            boolean bool;
            if (WebViewCore.this.mNativeClass == 0)
                bool = false;
            while (true)
            {
                return bool;
                switch (paramInt1)
                {
                default:
                    bool = false;
                    break;
                case 6:
                    int m = Math.round(paramMotionEvent.getX());
                    int n = Math.round(paramMotionEvent.getY());
                    WebViewCore.WebKitHitTest localWebKitHitTest = WebViewCore.this.performHitTest(m, n, WebViewCore.this.mWebViewClassic.getScaledNavSlop(), true);
                    WebViewCore.this.mWebViewClassic.mPrivateHandler.obtainMessage(131, localWebKitHitTest).sendToTarget();
                    bool = false;
                    break;
                case 4:
                    bool = WebViewCore.this.nativeMouseClick(WebViewCore.this.mNativeClass);
                    break;
                case 0:
                    int i = paramMotionEvent.getPointerCount();
                    int[] arrayOfInt1 = new int[i];
                    int[] arrayOfInt2 = new int[i];
                    int[] arrayOfInt3 = new int[i];
                    for (int j = 0; j < i; j++)
                    {
                        arrayOfInt1[j] = paramMotionEvent.getPointerId(j);
                        arrayOfInt2[j] = ((int)paramMotionEvent.getX(j));
                        arrayOfInt3[j] = ((int)paramMotionEvent.getY(j));
                    }
                    int k = WebViewCore.this.nativeHandleTouchEvent(WebViewCore.this.mNativeClass, paramMotionEvent.getActionMasked(), arrayOfInt1, arrayOfInt2, arrayOfInt3, i, paramMotionEvent.getActionIndex(), paramMotionEvent.getMetaState());
                    if ((k == 0) && (paramMotionEvent.getActionMasked() != 3) && ((paramInt2 & 0x1) == 0))
                        paramWebViewInputDispatcher.skipWebkitForRemainingTouchStream();
                    if ((k & 0x2) > 0)
                        bool = true;
                    else
                        bool = false;
                    break;
                }
            }
        }

        public Looper getWebKitLooper()
        {
            return this.mHandler.getLooper();
        }
    }

    static class SaveViewStateRequest
    {
        public ValueCallback<Boolean> mCallback;
        public OutputStream mStream;

        SaveViewStateRequest(OutputStream paramOutputStream, ValueCallback<Boolean> paramValueCallback)
        {
            this.mStream = paramOutputStream;
            this.mCallback = paramValueCallback;
        }
    }

    static class FindAllRequest
    {
        public int mMatchCount;
        public int mMatchIndex;
        public final String mSearchText;

        public FindAllRequest(String paramString)
        {
            this.mSearchText = paramString;
            this.mMatchCount = -1;
            this.mMatchIndex = -1;
        }
    }

    static class GeolocationPermissionsData
    {
        boolean mAllow;
        String mOrigin;
        boolean mRemember;
    }

    static class TouchEventData
    {
        int mAction;
        int mActionIndex;
        int[] mIds;
        int mMetaState;
        MotionEvent mMotionEvent;
        int mNativeLayer;
        Rect mNativeLayerRect = new Rect();
        boolean mNativeResult;
        Point[] mPoints;
        Point[] mPointsInView;
        boolean mReprocess;
        long mSequence;
    }

    static class TextFieldInitData
    {
        public Rect mContentBounds;
        public Rect mContentRect;
        public int mFieldPointer;
        public boolean mIsAutoCompleteEnabled;
        public boolean mIsSpellCheckEnabled;
        public boolean mIsTextFieldNext;
        public boolean mIsTextFieldPrev;
        public String mLabel;
        public int mMaxLength;
        public String mName;
        public int mNodeLayerId;
        public String mText;
        public int mType;
    }

    static class AutoFillData
    {
        private String mPreview;
        private int mQueryId;

        public AutoFillData()
        {
            this.mQueryId = -1;
            this.mPreview = "";
        }

        public AutoFillData(int paramInt, String paramString)
        {
            this.mQueryId = paramInt;
            this.mPreview = paramString;
        }

        public String getPreviewString()
        {
            return this.mPreview;
        }

        public int getQueryId()
        {
            return this.mQueryId;
        }
    }

    static class WebKitHitTest
    {
        String mAltDisplayString;
        String mAnchorText;
        boolean mEditable;
        Rect[] mEnclosingParentRects;
        boolean mHasFocus;
        boolean mHitTestMovedMouse;
        int mHitTestSlop;
        int mHitTestX;
        int mHitTestY;
        String mImageUrl;
        String mIntentUrl;
        String mLinkUrl;
        int mTapHighlightColor = 1714664933;
        String mTitle;
        Rect[] mTouchRects;
    }

    static class TouchHighlightData
    {
        int mNativeLayer;
        Rect mNativeLayerRect;
        int mSlop;
        int mX;
        int mY;
    }

    static class TouchUpData
    {
        int mFrame;
        int mMoveGeneration;
        int mNativeLayer;
        Rect mNativeLayerRect = new Rect();
        int mNode;
        int mX;
        int mY;
    }

    static class TextSelectionData
    {
        static final int REASON_ACCESSIBILITY_INJECTOR = 1;
        static final int REASON_SELECT_WORD = 2;
        static final int REASON_UNKNOWN;
        int mEnd;
        int mSelectTextPtr;
        int mSelectionReason = 0;
        int mStart;

        public TextSelectionData(int paramInt1, int paramInt2, int paramInt3)
        {
            this.mStart = paramInt1;
            this.mEnd = paramInt2;
            this.mSelectTextPtr = paramInt3;
        }
    }

    static class ReplaceTextData
    {
        int mNewEnd;
        int mNewStart;
        String mReplace;
        int mTextGeneration;
    }

    static class PostUrlData
    {
        byte[] mPostData;
        String mUrl;
    }

    static class GetUrlData
    {
        Map<String, String> mExtraHeaders;
        String mUrl;
    }

    static class MotionUpData
    {
        Rect mBounds;
        int mFrame;
        int mNode;
        int mX;
        int mY;
    }

    static class JSKeyData
    {
        String mCurrentText;
        KeyEvent mEvent;
    }

    static class JSInterfaceData
    {
        String mInterfaceName;
        Object mObject;
    }

    static class BaseUrlData
    {
        String mBaseUrl;
        String mData;
        String mEncoding;
        String mHistoryUrl;
        String mMimeType;
    }

    private static class WebCoreThread
        implements Runnable
    {
        private static final int INITIALIZE = 0;
        private static final int REDUCE_PRIORITY = 1;
        private static final int RESUME_PRIORITY = 2;

        public void run()
        {
            Looper.prepare();
            Assert.assertNull(WebViewCore.sWebCoreHandler);
            try
            {
                WebViewCore.access$502(new Handler()
                {
                    public void handleMessage(Message paramAnonymousMessage)
                    {
                        switch (paramAnonymousMessage.what)
                        {
                        default:
                        case 0:
                        case 1:
                        case 2:
                        case 185:
                        case 186:
                        case 193:
                        case 197:
                        case 220:
                        }
                        while (true)
                        {
                            return;
                            ((WebViewCore)paramAnonymousMessage.obj).initialize();
                            continue;
                            Process.setThreadPriority(3);
                            continue;
                            Process.setThreadPriority(0);
                            continue;
                            if (BrowserFrame.sJavaBridge == null)
                                throw new IllegalStateException("No WebView has been created in this process!");
                            BrowserFrame.sJavaBridge.addPackageName((String)paramAnonymousMessage.obj);
                            continue;
                            if (BrowserFrame.sJavaBridge == null)
                                throw new IllegalStateException("No WebView has been created in this process!");
                            BrowserFrame.sJavaBridge.removePackageName((String)paramAnonymousMessage.obj);
                            continue;
                            if (BrowserFrame.sJavaBridge == null)
                                throw new IllegalStateException("No WebView has been created in this process!");
                            BrowserFrame.sJavaBridge.updateProxy((ProxyProperties)paramAnonymousMessage.obj);
                            continue;
                            ((Message)paramAnonymousMessage.obj).sendToTarget();
                            continue;
                            WebViewCore.access$700();
                            CertificateChainValidator.handleTrustStorageUpdate();
                        }
                    }
                });
                WebViewCore.class.notify();
                Looper.loop();
                return;
            }
            finally
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebViewCore
 * JD-Core Version:        0.6.2
 */