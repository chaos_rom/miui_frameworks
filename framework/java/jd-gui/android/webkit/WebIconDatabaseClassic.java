package android.webkit;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.Vector;

class WebIconDatabaseClassic extends WebIconDatabase
{
    private static final String LOGTAG = "WebIconDatabase";
    private static WebIconDatabaseClassic sIconDatabase;
    private final EventHandler mEventHandler = new EventHandler(null);

    public static WebIconDatabaseClassic getInstance()
    {
        if (sIconDatabase == null)
            sIconDatabase = new WebIconDatabaseClassic();
        return sIconDatabase;
    }

    private static native void nativeClose();

    private static native Bitmap nativeIconForPageUrl(String paramString);

    private static native void nativeOpen(String paramString);

    private static native void nativeReleaseIconForPageUrl(String paramString);

    private static native void nativeRemoveAllIcons();

    private static native void nativeRetainIconForPageUrl(String paramString);

    public void bulkRequestIconForPageUrl(ContentResolver paramContentResolver, String paramString, WebIconDatabase.IconListener paramIconListener)
    {
        if (paramIconListener == null);
        while (true)
        {
            return;
            if (this.mEventHandler.hasHandler())
            {
                HashMap localHashMap = new HashMap();
                localHashMap.put("contentResolver", paramContentResolver);
                localHashMap.put("where", paramString);
                localHashMap.put("listener", paramIconListener);
                Message localMessage = Message.obtain(null, 6, localHashMap);
                this.mEventHandler.postMessage(localMessage);
            }
        }
    }

    public void close()
    {
        this.mEventHandler.postMessage(Message.obtain(null, 1));
    }

    void createHandler()
    {
        this.mEventHandler.createHandler();
    }

    public void open(String paramString)
    {
        if (paramString != null)
        {
            File localFile = new File(paramString);
            if (!localFile.exists())
                localFile.mkdirs();
            this.mEventHandler.postMessage(Message.obtain(null, 0, localFile.getAbsolutePath()));
        }
    }

    public void releaseIconForPageUrl(String paramString)
    {
        if (paramString != null)
            this.mEventHandler.postMessage(Message.obtain(null, 5, paramString));
    }

    public void removeAllIcons()
    {
        this.mEventHandler.postMessage(Message.obtain(null, 2));
    }

    public void requestIconForPageUrl(String paramString, WebIconDatabase.IconListener paramIconListener)
    {
        if ((paramIconListener == null) || (paramString == null));
        while (true)
        {
            return;
            Message localMessage = Message.obtain(null, 3, paramIconListener);
            localMessage.getData().putString("url", paramString);
            this.mEventHandler.postMessage(localMessage);
        }
    }

    public void retainIconForPageUrl(String paramString)
    {
        if (paramString != null)
            this.mEventHandler.postMessage(Message.obtain(null, 4, paramString));
    }

    private static class EventHandler extends Handler
    {
        static final int BULK_REQUEST_ICON = 6;
        static final int CLOSE = 1;
        private static final int ICON_RESULT = 10;
        static final int OPEN = 0;
        static final int RELEASE_ICON = 5;
        static final int REMOVE_ALL = 2;
        static final int REQUEST_ICON = 3;
        static final int RETAIN_ICON = 4;
        private Handler mHandler;
        private Vector<Message> mMessages = new Vector();

        private void bulkRequestIcons(Message paramMessage)
        {
            HashMap localHashMap = (HashMap)paramMessage.obj;
            WebIconDatabase.IconListener localIconListener = (WebIconDatabase.IconListener)localHashMap.get("listener");
            ContentResolver localContentResolver = (ContentResolver)localHashMap.get("contentResolver");
            String str = (String)localHashMap.get("where");
            Cursor localCursor = null;
            try
            {
                Uri localUri = Browser.BOOKMARKS_URI;
                String[] arrayOfString = new String[1];
                arrayOfString[0] = "url";
                localCursor = localContentResolver.query(localUri, arrayOfString, str, null, null);
                if (localCursor.moveToFirst())
                {
                    boolean bool;
                    do
                    {
                        requestIconAndSendResult(localCursor.getString(0), localIconListener);
                        bool = localCursor.moveToNext();
                    }
                    while (bool);
                }
                return;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                while (true)
                {
                    Log.e("WebIconDatabase", "BulkRequestIcons", localIllegalStateException);
                    if (localCursor == null);
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
        }

        /** @deprecated */
        private void createHandler()
        {
            try
            {
                if (this.mHandler == null)
                {
                    this.mHandler = new Handler()
                    {
                        public void handleMessage(Message paramAnonymousMessage)
                        {
                            switch (paramAnonymousMessage.what)
                            {
                            default:
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 6:
                            case 4:
                            case 5:
                            }
                            while (true)
                            {
                                return;
                                WebIconDatabaseClassic.nativeOpen((String)paramAnonymousMessage.obj);
                                continue;
                                WebIconDatabaseClassic.access$200();
                                continue;
                                WebIconDatabaseClassic.access$300();
                                continue;
                                WebIconDatabase.IconListener localIconListener = (WebIconDatabase.IconListener)paramAnonymousMessage.obj;
                                String str = paramAnonymousMessage.getData().getString("url");
                                WebIconDatabaseClassic.EventHandler.this.requestIconAndSendResult(str, localIconListener);
                                continue;
                                WebIconDatabaseClassic.EventHandler.this.bulkRequestIcons(paramAnonymousMessage);
                                continue;
                                WebIconDatabaseClassic.nativeRetainIconForPageUrl((String)paramAnonymousMessage.obj);
                                continue;
                                WebIconDatabaseClassic.nativeReleaseIconForPageUrl((String)paramAnonymousMessage.obj);
                            }
                        }
                    };
                    for (int i = this.mMessages.size(); i > 0; i--)
                        this.mHandler.sendMessage((Message)this.mMessages.remove(0));
                    this.mMessages = null;
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        private boolean hasHandler()
        {
            try
            {
                Handler localHandler = this.mHandler;
                if (localHandler != null)
                {
                    bool = true;
                    return bool;
                }
                boolean bool = false;
            }
            finally
            {
            }
        }

        /** @deprecated */
        private void postMessage(Message paramMessage)
        {
            try
            {
                if (this.mMessages != null)
                    this.mMessages.add(paramMessage);
                while (true)
                {
                    return;
                    this.mHandler.sendMessage(paramMessage);
                }
            }
            finally
            {
            }
        }

        private void requestIconAndSendResult(String paramString, WebIconDatabase.IconListener paramIconListener)
        {
            Bitmap localBitmap = WebIconDatabaseClassic.nativeIconForPageUrl(paramString);
            if (localBitmap != null)
                sendMessage(obtainMessage(10, new IconResult(paramString, localBitmap, paramIconListener)));
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 10:
            }
            while (true)
            {
                return;
                ((IconResult)paramMessage.obj).dispatch();
            }
        }

        private class IconResult
        {
            private final Bitmap mIcon;
            private final WebIconDatabase.IconListener mListener;
            private final String mUrl;

            IconResult(String paramBitmap, Bitmap paramIconListener, WebIconDatabase.IconListener arg4)
            {
                this.mUrl = paramBitmap;
                this.mIcon = paramIconListener;
                Object localObject;
                this.mListener = localObject;
            }

            void dispatch()
            {
                this.mListener.onReceivedIcon(this.mUrl, this.mIcon);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebIconDatabaseClassic
 * JD-Core Version:        0.6.2
 */