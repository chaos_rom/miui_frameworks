package android.webkit;

import android.net.ParseException;
import android.net.Uri;
import android.net.WebAddress;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URLUtil
{
    static final String ASSET_BASE = "file:///android_asset/";
    static final String CONTENT_BASE = "content:";
    private static final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$", 2);
    static final String FILE_BASE = "file://";
    private static final String LOGTAG = "webkit";
    static final String PROXY_BASE = "file:///cookieless_proxy/";
    static final String RESOURCE_BASE = "file:///android_res/";

    public static String composeSearchUrl(String paramString1, String paramString2, String paramString3)
    {
        String str = null;
        int i = paramString2.indexOf(paramString3);
        if (i < 0);
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append(paramString2.substring(0, i));
            try
            {
                localStringBuilder.append(URLEncoder.encode(paramString1, "utf-8"));
                localStringBuilder.append(paramString2.substring(i + paramString3.length()));
                str = localStringBuilder.toString();
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
            }
        }
    }

    public static byte[] decode(byte[] paramArrayOfByte)
        throws IllegalArgumentException
    {
        byte[] arrayOfByte2;
        if (paramArrayOfByte.length == 0)
            arrayOfByte2 = new byte[0];
        while (true)
        {
            return arrayOfByte2;
            byte[] arrayOfByte1 = new byte[paramArrayOfByte.length];
            int i = 0;
            int j = 0;
            while (j < paramArrayOfByte.length)
            {
                int k = paramArrayOfByte[j];
                if (k == 37)
                {
                    if (paramArrayOfByte.length - j > 2)
                    {
                        k = (byte)(16 * parseHex(paramArrayOfByte[(j + 1)]) + parseHex(paramArrayOfByte[(j + 2)]));
                        j += 2;
                    }
                }
                else
                {
                    int m = i + 1;
                    arrayOfByte1[i] = k;
                    j++;
                    i = m;
                    continue;
                }
                throw new IllegalArgumentException("Invalid format");
            }
            arrayOfByte2 = new byte[i];
            System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, i);
        }
    }

    public static final String guessFileName(String paramString1, String paramString2, String paramString3)
    {
        String str1 = null;
        String str2 = null;
        if ((0 == 0) && (paramString2 != null))
        {
            str1 = parseContentDisposition(paramString2);
            if (str1 != null)
            {
                int n = 1 + str1.lastIndexOf('/');
                if (n > 0)
                    str1 = str1.substring(n);
            }
        }
        if (str1 == null)
        {
            String str4 = Uri.decode(paramString1);
            if (str4 != null)
            {
                int k = str4.indexOf('?');
                if (k > 0)
                    str4 = str4.substring(0, k);
                if (!str4.endsWith("/"))
                {
                    int m = 1 + str4.lastIndexOf('/');
                    if (m > 0)
                        str1 = str4.substring(m);
                }
            }
        }
        if (str1 == null)
            str1 = "downloadfile";
        int i = str1.indexOf('.');
        if (i < 0)
        {
            if (paramString3 != null)
            {
                str2 = MimeTypeMap.getSingleton().getExtensionFromMimeType(paramString3);
                if (str2 != null)
                    str2 = "." + str2;
            }
            if (str2 == null)
            {
                if ((paramString3 == null) || (!paramString3.toLowerCase().startsWith("text/")))
                    break label238;
                if (!paramString3.equalsIgnoreCase("text/html"))
                    break label231;
                str2 = ".html";
            }
        }
        while (true)
        {
            return str1 + str2;
            label231: str2 = ".txt";
            continue;
            label238: str2 = ".bin";
            continue;
            if (paramString3 != null)
            {
                int j = str1.lastIndexOf('.');
                String str3 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str1.substring(j + 1));
                if ((str3 != null) && (!str3.equalsIgnoreCase(paramString3)))
                {
                    str2 = MimeTypeMap.getSingleton().getExtensionFromMimeType(paramString3);
                    if (str2 != null)
                        str2 = "." + str2;
                }
            }
            if (str2 == null)
                str2 = str1.substring(i);
            str1 = str1.substring(0, i);
        }
    }

    public static String guessUrl(String paramString)
    {
        String str = paramString;
        if (paramString.length() == 0);
        while (true)
        {
            return paramString;
            if ((!paramString.startsWith("about:")) && (!paramString.startsWith("data:")) && (!paramString.startsWith("file:")) && (!paramString.startsWith("javascript:")))
            {
                if (paramString.endsWith(".") == true)
                    paramString = paramString.substring(0, -1 + paramString.length());
                try
                {
                    WebAddress localWebAddress = new WebAddress(paramString);
                    if (localWebAddress.getHost().indexOf('.') == -1)
                        localWebAddress.setHost("www." + localWebAddress.getHost() + ".com");
                    paramString = localWebAddress.toString();
                }
                catch (ParseException localParseException)
                {
                    paramString = str;
                }
            }
        }
    }

    public static boolean isAboutUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("about:")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isAssetUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("file:///android_asset/")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isContentUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("content:")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @Deprecated
    public static boolean isCookielessProxyUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("file:///cookieless_proxy/")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isDataUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("data:")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isFileUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("file://")) && (!paramString.startsWith("file:///android_asset/")) && (!paramString.startsWith("file:///cookieless_proxy/")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isHttpUrl(String paramString)
    {
        boolean bool = false;
        if ((paramString != null) && (paramString.length() > 6) && (paramString.substring(0, 7).equalsIgnoreCase("http://")))
            bool = true;
        return bool;
    }

    public static boolean isHttpsUrl(String paramString)
    {
        boolean bool = false;
        if ((paramString != null) && (paramString.length() > 7) && (paramString.substring(0, 8).equalsIgnoreCase("https://")))
            bool = true;
        return bool;
    }

    public static boolean isJavaScriptUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("javascript:")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isNetworkUrl(String paramString)
    {
        boolean bool = false;
        if ((paramString == null) || (paramString.length() == 0));
        while (true)
        {
            return bool;
            if ((isHttpUrl(paramString)) || (isHttpsUrl(paramString)))
                bool = true;
        }
    }

    public static boolean isResourceUrl(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("file:///android_res/")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isValidUrl(String paramString)
    {
        boolean bool = false;
        if ((paramString == null) || (paramString.length() == 0));
        while (true)
        {
            return bool;
            if ((isAssetUrl(paramString)) || (isResourceUrl(paramString)) || (isFileUrl(paramString)) || (isAboutUrl(paramString)) || (isHttpUrl(paramString)) || (isHttpsUrl(paramString)) || (isJavaScriptUrl(paramString)) || (isContentUrl(paramString)))
                bool = true;
        }
    }

    static String parseContentDisposition(String paramString)
    {
        try
        {
            Matcher localMatcher = CONTENT_DISPOSITION_PATTERN.matcher(paramString);
            if (localMatcher.find())
            {
                String str2 = localMatcher.group(2);
                str1 = str2;
                return str1;
            }
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                String str1 = null;
        }
    }

    private static int parseHex(byte paramByte)
    {
        int i;
        if ((paramByte >= 48) && (paramByte <= 57))
            i = paramByte - 48;
        while (true)
        {
            return i;
            if ((paramByte >= 65) && (paramByte <= 70))
            {
                i = 10 + (paramByte - 65);
            }
            else
            {
                if ((paramByte < 97) || (paramByte > 102))
                    break;
                i = 10 + (paramByte - 97);
            }
        }
        throw new IllegalArgumentException("Invalid hex char '" + paramByte + "'");
    }

    public static String stripAnchor(String paramString)
    {
        int i = paramString.indexOf('#');
        if (i != -1)
            paramString = paramString.substring(0, i);
        return paramString;
    }

    static boolean verifyURLEncoding(String paramString)
    {
        boolean bool = false;
        int i = paramString.length();
        if (i == 0);
        while (true)
        {
            return bool;
            int j = paramString.indexOf('%');
            while (true)
            {
                if ((j < 0) || (j >= i))
                    break label88;
                if (j >= i - 2)
                    break;
                int k = j + 1;
                try
                {
                    parseHex((byte)paramString.charAt(k));
                    int m = k + 1;
                    parseHex((byte)paramString.charAt(m));
                    j = paramString.indexOf('%', m + 1);
                }
                catch (IllegalArgumentException localIllegalArgumentException)
                {
                }
            }
            continue;
            label88: bool = true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.URLUtil
 * JD-Core Version:        0.6.2
 */