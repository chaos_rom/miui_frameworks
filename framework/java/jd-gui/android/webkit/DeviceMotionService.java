package android.webkit;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import java.util.List;

final class DeviceMotionService
    implements SensorEventListener
{
    private static final int INTERVAL_MILLIS = 100;
    private Context mContext;
    private Handler mHandler;
    private boolean mHaveSentErrorEvent;
    private boolean mIsRunning;
    private float[] mLastAcceleration;
    private DeviceMotionAndOrientationManager mManager;
    private SensorManager mSensorManager;
    private Runnable mUpdateRunnable;

    static
    {
        if (!DeviceMotionService.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    public DeviceMotionService(DeviceMotionAndOrientationManager paramDeviceMotionAndOrientationManager, Context paramContext)
    {
        this.mManager = paramDeviceMotionAndOrientationManager;
        assert (this.mManager != null);
        this.mContext = paramContext;
        assert (this.mContext != null);
    }

    private void createHandler()
    {
        if (this.mHandler != null);
        while (true)
        {
            return;
            this.mHandler = new Handler();
            this.mUpdateRunnable = new Runnable()
            {
                static
                {
                    if (!DeviceMotionService.class.desiredAssertionStatus());
                    for (boolean bool = true; ; bool = false)
                    {
                        $assertionsDisabled = bool;
                        return;
                    }
                }

                public void run()
                {
                    assert (DeviceMotionService.this.mIsRunning);
                    DeviceMotionService.this.mManager.onMotionChange(new Double(DeviceMotionService.this.mLastAcceleration[0]), new Double(DeviceMotionService.this.mLastAcceleration[1]), new Double(DeviceMotionService.this.mLastAcceleration[2]), 100.0D);
                    DeviceMotionService.this.mHandler.postDelayed(DeviceMotionService.this.mUpdateRunnable, 100L);
                    DeviceMotionService.access$502(DeviceMotionService.this, false);
                }
            };
        }
    }

    private SensorManager getSensorManager()
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        if (this.mSensorManager == null)
            this.mSensorManager = ((SensorManager)this.mContext.getSystemService("sensor"));
        return this.mSensorManager;
    }

    private boolean registerForAccelerometerSensor()
    {
        boolean bool = false;
        List localList = getSensorManager().getSensorList(1);
        if (localList.isEmpty());
        while (true)
        {
            return bool;
            createHandler();
            bool = getSensorManager().registerListener(this, (Sensor)localList.get(0), 2, this.mHandler);
        }
    }

    private void registerForSensor()
    {
        if (!registerForAccelerometerSensor())
            sendErrorEvent();
    }

    private void sendErrorEvent()
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        if (this.mHaveSentErrorEvent);
        while (true)
        {
            return;
            this.mHaveSentErrorEvent = true;
            createHandler();
            this.mHandler.post(new Runnable()
            {
                static
                {
                    if (!DeviceMotionService.class.desiredAssertionStatus());
                    for (boolean bool = true; ; bool = false)
                    {
                        $assertionsDisabled = bool;
                        return;
                    }
                }

                public void run()
                {
                    assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
                    if (DeviceMotionService.this.mIsRunning)
                        DeviceMotionService.this.mManager.onMotionChange(null, null, null, 0.0D);
                }
            });
        }
    }

    private void startSendingUpdates()
    {
        createHandler();
        this.mUpdateRunnable.run();
    }

    private void stopSendingUpdates()
    {
        this.mHandler.removeCallbacks(this.mUpdateRunnable);
        this.mLastAcceleration = null;
    }

    private void unregisterFromSensor()
    {
        getSensorManager().unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor paramSensor, int paramInt)
    {
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
    }

    public void onSensorChanged(SensorEvent paramSensorEvent)
    {
        int i = 1;
        assert (paramSensorEvent.values.length == 3);
        assert ("WebViewCoreThread".equals(Thread.currentThread().getName()));
        assert (paramSensorEvent.sensor.getType() == i);
        if (!this.mIsRunning)
            return;
        if (this.mLastAcceleration == null);
        while (true)
        {
            this.mLastAcceleration = paramSensorEvent.values;
            if (i == 0)
                break;
            startSendingUpdates();
            break;
            i = 0;
        }
    }

    public void resume()
    {
        if (this.mIsRunning)
            registerForSensor();
    }

    public void start()
    {
        this.mIsRunning = true;
        registerForSensor();
    }

    public void stop()
    {
        this.mIsRunning = false;
        stopSendingUpdates();
        unregisterFromSensor();
    }

    public void suspend()
    {
        if (this.mIsRunning)
        {
            stopSendingUpdates();
            unregisterFromSensor();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.DeviceMotionService
 * JD-Core Version:        0.6.2
 */