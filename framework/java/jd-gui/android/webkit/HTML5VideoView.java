package android.webkit;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class HTML5VideoView
    implements MediaPlayer.OnPreparedListener
{
    protected static final String COOKIE = "Cookie";
    protected static final String HIDE_URL_LOGS = "x-hide-urls-from-log";
    protected static final String LOGTAG = "HTML5VideoView";
    static final int STATE_INITIALIZED = 0;
    static final int STATE_PLAYING = 3;
    static final int STATE_PREPARED = 2;
    static final int STATE_PREPARING = 1;
    static final int STATE_RESETTED = 4;
    private static final int TIMEUPDATE_PERIOD = 250;
    protected static int mCurrentState = -1;
    protected static MediaPlayer mPlayer = null;
    protected static Timer mTimer;
    protected Map<String, String> mHeaders;
    protected boolean mPauseDuringPreparing;
    public boolean mPlayerBuffering = false;
    protected HTML5VideoViewProxy mProxy;
    protected int mSaveSeekTime;
    private boolean mSkipPrepare = false;
    private boolean mStartWhenPrepared = false;
    protected Uri mUri;
    protected int mVideoLayerId;

    protected static Map<String, String> generateHeaders(String paramString, HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        boolean bool = paramHTML5VideoViewProxy.getWebView().isPrivateBrowsingEnabled();
        String str = CookieManager.getInstance().getCookie(paramString, bool);
        HashMap localHashMap = new HashMap();
        if (str != null)
            localHashMap.put("Cookie", str);
        if (bool)
            localHashMap.put("x-hide-urls-from-log", "true");
        return localHashMap;
    }

    public void decideDisplayMode()
    {
    }

    public void deleteSurfaceTexture()
    {
    }

    public void enterFullScreenVideoState(int paramInt, HTML5VideoViewProxy paramHTML5VideoViewProxy, WebViewClassic paramWebViewClassic)
    {
    }

    public boolean fullScreenExited()
    {
        return false;
    }

    public int getCurrentPosition()
    {
        if (mCurrentState == 2);
        for (int i = mPlayer.getCurrentPosition(); ; i = 0)
            return i;
    }

    public int getCurrentState()
    {
        if (isPlaying());
        for (int i = 3; ; i = mCurrentState)
            return i;
    }

    public int getDuration()
    {
        if (mCurrentState == 2);
        for (int i = mPlayer.getDuration(); ; i = -1)
            return i;
    }

    public boolean getPauseDuringPreparing()
    {
        return this.mPauseDuringPreparing;
    }

    public boolean getPlayerBuffering()
    {
        return this.mPlayerBuffering;
    }

    public boolean getReadyToUseSurfTex()
    {
        return false;
    }

    public boolean getStartWhenPrepared()
    {
        return this.mStartWhenPrepared;
    }

    public int getTextureName()
    {
        return 0;
    }

    public int getVideoLayerId()
    {
        return this.mVideoLayerId;
    }

    public void init(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (mPlayer == null)
        {
            mPlayer = new MediaPlayer();
            mCurrentState = 0;
        }
        this.mSkipPrepare = paramBoolean;
        if (!this.mSkipPrepare)
            mCurrentState = 0;
        this.mProxy = null;
        this.mVideoLayerId = paramInt1;
        this.mSaveSeekTime = paramInt2;
        mTimer = null;
        this.mPauseDuringPreparing = false;
    }

    public boolean isFullScreenMode()
    {
        return false;
    }

    public boolean isPlaying()
    {
        if (mCurrentState == 2);
        for (boolean bool = mPlayer.isPlaying(); ; bool = false)
            return bool;
    }

    public void onPrepared(MediaPlayer paramMediaPlayer)
    {
        mCurrentState = 2;
        seekTo(this.mSaveSeekTime);
        if (this.mProxy != null)
            this.mProxy.onPrepared(paramMediaPlayer);
        if (this.mPauseDuringPreparing)
        {
            pauseAndDispatch(this.mProxy);
            this.mPauseDuringPreparing = false;
        }
    }

    public void pause()
    {
        if (isPlaying())
            mPlayer.pause();
        while (true)
        {
            if (mTimer != null)
            {
                mTimer.purge();
                mTimer.cancel();
                mTimer = null;
            }
            return;
            if (mCurrentState == 1)
                this.mPauseDuringPreparing = true;
        }
    }

    public void pauseAndDispatch(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        pause();
        if (paramHTML5VideoViewProxy != null)
            paramHTML5VideoViewProxy.dispatchOnPaused();
    }

    public void prepareDataAndDisplayMode(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        decideDisplayMode();
        setOnCompletionListener(paramHTML5VideoViewProxy);
        setOnPreparedListener(paramHTML5VideoViewProxy);
        setOnErrorListener(paramHTML5VideoViewProxy);
        setOnInfoListener(paramHTML5VideoViewProxy);
        prepareDataCommon(paramHTML5VideoViewProxy);
    }

    public void prepareDataCommon(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        if (!this.mSkipPrepare);
        while (true)
        {
            try
            {
                mPlayer.reset();
                mPlayer.setDataSource(paramHTML5VideoViewProxy.getContext(), this.mUri, this.mHeaders);
                mPlayer.prepareAsync();
                mCurrentState = 1;
                return;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                localIllegalArgumentException.printStackTrace();
                continue;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                localIllegalStateException.printStackTrace();
                continue;
            }
            catch (IOException localIOException)
            {
                localIOException.printStackTrace();
                continue;
            }
            if (mCurrentState >= 2)
                onPrepared(mPlayer);
            this.mSkipPrepare = false;
        }
    }

    public void reprepareData(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        mPlayer.reset();
        prepareDataCommon(paramHTML5VideoViewProxy);
    }

    public void reset()
    {
        if (mCurrentState != 4)
            mPlayer.reset();
        mCurrentState = 4;
    }

    public void seekTo(int paramInt)
    {
        if (mCurrentState == 2)
            mPlayer.seekTo(paramInt);
        while (true)
        {
            return;
            this.mSaveSeekTime = paramInt;
        }
    }

    public void setOnCompletionListener(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        mPlayer.setOnCompletionListener(paramHTML5VideoViewProxy);
    }

    public void setOnErrorListener(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        mPlayer.setOnErrorListener(paramHTML5VideoViewProxy);
    }

    public void setOnInfoListener(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        mPlayer.setOnInfoListener(paramHTML5VideoViewProxy);
    }

    public void setOnPreparedListener(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        this.mProxy = paramHTML5VideoViewProxy;
        mPlayer.setOnPreparedListener(this);
    }

    public void setPlayerBuffering(boolean paramBoolean)
    {
        this.mPlayerBuffering = paramBoolean;
        switchProgressView(paramBoolean);
    }

    public void setStartWhenPrepared(boolean paramBoolean)
    {
        this.mStartWhenPrepared = paramBoolean;
    }

    public void setVideoURI(String paramString, HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        this.mUri = Uri.parse(paramString);
        this.mHeaders = generateHeaders(paramString, paramHTML5VideoViewProxy);
    }

    public void showControllerInFullScreen()
    {
    }

    public void start()
    {
        if (mCurrentState == 2)
        {
            if (mTimer == null)
            {
                mTimer = new Timer();
                mTimer.schedule(new TimeupdateTask(this.mProxy), 250L, 250L);
            }
            mPlayer.start();
            setPlayerBuffering(false);
        }
    }

    public void stopPlayback()
    {
        if (mCurrentState == 2)
            mPlayer.stop();
    }

    public boolean surfaceTextureDeleted()
    {
        return false;
    }

    protected void switchProgressView(boolean paramBoolean)
    {
    }

    private static final class TimeupdateTask extends TimerTask
    {
        private HTML5VideoViewProxy mProxy;

        public TimeupdateTask(HTML5VideoViewProxy paramHTML5VideoViewProxy)
        {
            this.mProxy = paramHTML5VideoViewProxy;
        }

        public void run()
        {
            this.mProxy.onTimeupdate();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.HTML5VideoView
 * JD-Core Version:        0.6.2
 */