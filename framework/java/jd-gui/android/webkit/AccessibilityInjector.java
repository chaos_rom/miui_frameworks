package android.webkit;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.speech.tts.TextToSpeech;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;

class AccessibilityInjector
{
    private static final String ACCESSIBILITY_ANDROIDVOX_TEMPLATE = "cvox.AndroidVox.performAction('%1s')";
    private static final String ACCESSIBILITY_SCREEN_READER_JAVASCRIPT_TEMPLATE = "javascript:(function() {        var chooser = document.createElement('script');        chooser.type = 'text/javascript';        chooser.src = '%1s';        document.getElementsByTagName('head')[0].appendChild(chooser);    })();";
    private static final int ACCESSIBILITY_SCRIPT_INJECTION_OPTED_OUT = 0;
    private static final int ACCESSIBILITY_SCRIPT_INJECTION_PROVIDED = 1;
    private static final int ACCESSIBILITY_SCRIPT_INJECTION_UNDEFINED = -1;
    private static final String ALIAS_TRAVERSAL_JS_INTERFACE = "accessibilityTraversal";
    private static final String ALIAS_TTS_JS_INTERFACE = "accessibility";
    private AccessibilityInjectorFallback mAccessibilityInjectorFallback;
    private JSONObject mAccessibilityJSONObject;
    private AccessibilityManager mAccessibilityManager;
    private boolean mAccessibilityScriptInjected;
    private CallbackHandler mCallback;
    private final Context mContext;
    private TextToSpeech mTextToSpeech;
    private final WebView mWebView;
    private final WebViewClassic mWebViewClassic;

    public AccessibilityInjector(WebViewClassic paramWebViewClassic)
    {
        this.mWebViewClassic = paramWebViewClassic;
        this.mWebView = paramWebViewClassic.getWebView();
        this.mContext = paramWebViewClassic.getContext();
        this.mAccessibilityManager = AccessibilityManager.getInstance(this.mContext);
    }

    private void addCallbackApis()
    {
        if (this.mCallback != null);
        while (true)
        {
            return;
            this.mCallback = new CallbackHandler("accessibilityTraversal", null);
            this.mWebView.addJavascriptInterface(this.mCallback, "accessibilityTraversal");
        }
    }

    private void addTtsApis()
    {
        if (this.mTextToSpeech != null);
        while (true)
        {
            return;
            String str = this.mContext.getPackageName();
            this.mTextToSpeech = new TextToSpeech(this.mContext, null, null, str + ".**webview**", true);
            this.mWebView.addJavascriptInterface(this.mTextToSpeech, "accessibility");
        }
    }

    private int getAxsUrlParameterValue(String paramString)
    {
        int i = -1;
        if (paramString == null);
        while (true)
        {
            return i;
            try
            {
                Iterator localIterator = URLEncodedUtils.parse(new URI(paramString), null).iterator();
                NameValuePair localNameValuePair;
                do
                {
                    if (!localIterator.hasNext())
                        break;
                    localNameValuePair = (NameValuePair)localIterator.next();
                }
                while (!"axs".equals(localNameValuePair.getName()));
                int j = verifyInjectionValue(localNameValuePair.getValue());
                i = j;
            }
            catch (URISyntaxException localURISyntaxException)
            {
            }
        }
    }

    private String getScreenReaderInjectionUrl()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "accessibility_script_injection_url");
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = str;
        return String.format("javascript:(function() {        var chooser = document.createElement('script');        chooser.type = 'text/javascript';        chooser.src = '%1s';        document.getElementsByTagName('head')[0].appendChild(chooser);    })();", arrayOfObject);
    }

    private boolean isAccessibilityEnabled()
    {
        return this.mAccessibilityManager.isEnabled();
    }

    private boolean isJavaScriptEnabled()
    {
        return this.mWebView.getSettings().getJavaScriptEnabled();
    }

    private boolean isScriptInjectionEnabled()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "accessibility_script_injection", 0) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void removeCallbackApis()
    {
        if (this.mCallback == null);
        while (true)
        {
            return;
            this.mWebView.removeJavascriptInterface("accessibilityTraversal");
            this.mCallback = null;
        }
    }

    private void removeTtsApis()
    {
        if (this.mTextToSpeech == null);
        while (true)
        {
            return;
            this.mWebView.removeJavascriptInterface("accessibility");
            this.mTextToSpeech.stop();
            this.mTextToSpeech.shutdown();
            this.mTextToSpeech = null;
        }
    }

    private boolean sendActionToAndroidVox(int paramInt, Bundle paramBundle)
    {
        boolean bool = false;
        if (this.mAccessibilityJSONObject == null)
            this.mAccessibilityJSONObject = new JSONObject();
        try
        {
            this.mAccessibilityJSONObject.accumulate("action", Integer.valueOf(paramInt));
            switch (paramInt)
            {
            default:
            case 256:
            case 512:
            case 1024:
            case 2048:
            }
            while (true)
            {
                String str2 = this.mAccessibilityJSONObject.toString();
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[bool] = str2;
                String str3 = String.format("cvox.AndroidVox.performAction('%1s')", arrayOfObject);
                bool = this.mCallback.performAction(this.mWebView, str3);
                label120: return bool;
                Iterator localIterator = this.mAccessibilityJSONObject.keys();
                while (localIterator.hasNext())
                {
                    localIterator.next();
                    localIterator.remove();
                }
                if (paramBundle != null)
                {
                    int i = paramBundle.getInt("ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT");
                    this.mAccessibilityJSONObject.accumulate("granularity", Integer.valueOf(i));
                    continue;
                    if (paramBundle != null)
                    {
                        String str1 = paramBundle.getString("ACTION_ARGUMENT_HTML_ELEMENT_STRING");
                        this.mAccessibilityJSONObject.accumulate("element", str1);
                    }
                }
            }
        }
        catch (JSONException localJSONException)
        {
            break label120;
        }
    }

    private boolean shouldInjectJavaScript(String paramString)
    {
        boolean bool = false;
        if (!isJavaScriptEnabled());
        while (true)
        {
            return bool;
            if ((getAxsUrlParameterValue(paramString) != 0) && (isScriptInjectionEnabled()))
                bool = true;
        }
    }

    private void toggleFallbackAccessibilityInjector(boolean paramBoolean)
    {
        if ((paramBoolean) && (this.mAccessibilityInjectorFallback == null));
        for (this.mAccessibilityInjectorFallback = new AccessibilityInjectorFallback(this.mWebViewClassic); ; this.mAccessibilityInjectorFallback = null)
            return;
    }

    private int verifyInjectionValue(String paramString)
    {
        try
        {
            int j = Integer.parseInt(paramString);
            label32: int i;
            switch (j)
            {
            default:
                i = -1;
            case 0:
            case 1:
            }
            while (true)
            {
                return i;
                i = 0;
                continue;
                i = 1;
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label32;
        }
    }

    public void addAccessibilityApisIfNecessary()
    {
        if ((!isAccessibilityEnabled()) || (!isJavaScriptEnabled()));
        while (true)
        {
            return;
            addTtsApis();
            addCallbackApis();
        }
    }

    public boolean handleKeyEventIfNecessary(KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (!isAccessibilityEnabled())
        {
            this.mAccessibilityScriptInjected = false;
            toggleFallbackAccessibilityInjector(false);
        }
        while (true)
        {
            return bool;
            if (this.mAccessibilityScriptInjected)
            {
                if (paramKeyEvent.getAction() == 1)
                    this.mWebViewClassic.sendBatchableInputMessage(104, 0, 0, paramKeyEvent);
                while (true)
                {
                    bool = true;
                    break;
                    if (paramKeyEvent.getAction() != 0)
                        break;
                    this.mWebViewClassic.sendBatchableInputMessage(103, 0, 0, paramKeyEvent);
                }
            }
            if (this.mAccessibilityInjectorFallback != null)
                bool = this.mAccessibilityInjectorFallback.onKeyEvent(paramKeyEvent);
        }
    }

    public void handleSelectionChangedIfNecessary(String paramString)
    {
        if (this.mAccessibilityInjectorFallback != null)
            this.mAccessibilityInjectorFallback.onSelectionStringChange(paramString);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        paramAccessibilityNodeInfo.setMovementGranularities(31);
        paramAccessibilityNodeInfo.addAction(256);
        paramAccessibilityNodeInfo.addAction(512);
        paramAccessibilityNodeInfo.addAction(1024);
        paramAccessibilityNodeInfo.addAction(2048);
        paramAccessibilityNodeInfo.addAction(16);
        paramAccessibilityNodeInfo.setClickable(true);
    }

    public void onPageFinished(String paramString)
    {
        if (!isAccessibilityEnabled())
        {
            this.mAccessibilityScriptInjected = false;
            toggleFallbackAccessibilityInjector(false);
        }
        while (true)
        {
            return;
            if (!shouldInjectJavaScript(paramString))
            {
                toggleFallbackAccessibilityInjector(true);
            }
            else
            {
                toggleFallbackAccessibilityInjector(false);
                String str = getScreenReaderInjectionUrl();
                this.mWebView.loadUrl(str);
                this.mAccessibilityScriptInjected = true;
            }
        }
    }

    public void onPageStarted(String paramString)
    {
        this.mAccessibilityScriptInjected = false;
    }

    public boolean performAccessibilityAction(int paramInt, Bundle paramBundle)
    {
        boolean bool = false;
        if (!isAccessibilityEnabled())
        {
            this.mAccessibilityScriptInjected = false;
            toggleFallbackAccessibilityInjector(false);
        }
        while (true)
        {
            return bool;
            if (this.mAccessibilityScriptInjected)
                bool = sendActionToAndroidVox(paramInt, paramBundle);
            else if (this.mAccessibilityInjectorFallback != null)
                bool = this.mAccessibilityInjectorFallback.performAccessibilityAction(paramInt, paramBundle);
        }
    }

    public void removeAccessibilityApisIfNecessary()
    {
        removeTtsApis();
        removeCallbackApis();
    }

    public boolean supportsAccessibilityAction(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 16:
        case 256:
        case 512:
        case 1024:
        case 2048:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private static class CallbackHandler
    {
        private static final String JAVASCRIPT_ACTION_TEMPLATE = "javascript:(function() { %s.onResult(%d, %s); })();";
        private static final long RESULT_TIMEOUT = 5000L;
        private final String mInterfaceName;
        private boolean mResult = false;
        private long mResultId = -1L;
        private final AtomicInteger mResultIdCounter = new AtomicInteger();
        private final Object mResultLock = new Object();

        private CallbackHandler(String paramString)
        {
            this.mInterfaceName = paramString;
        }

        private void clearResultLocked()
        {
            this.mResultId = -1L;
            this.mResult = false;
        }

        private boolean getResultAndClear(int paramInt)
        {
            while (true)
            {
                synchronized (this.mResultLock)
                {
                    if (waitForResultTimedLocked(paramInt))
                    {
                        bool = this.mResult;
                        clearResultLocked();
                        return bool;
                    }
                }
                boolean bool = false;
            }
        }

        private boolean performAction(WebView paramWebView, String paramString)
        {
            int i = this.mResultIdCounter.getAndIncrement();
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = this.mInterfaceName;
            arrayOfObject[1] = Integer.valueOf(i);
            arrayOfObject[2] = paramString;
            paramWebView.loadUrl(String.format("javascript:(function() { %s.onResult(%d, %s); })();", arrayOfObject));
            return getResultAndClear(i);
        }

        private boolean waitForResultTimedLocked(int paramInt)
        {
            boolean bool = false;
            long l1 = SystemClock.uptimeMillis();
            while (true)
                try
                {
                    if (this.mResultId == paramInt)
                    {
                        bool = true;
                        break;
                    }
                    if (this.mResultId > paramInt)
                        break;
                    long l2 = 5000L - (SystemClock.uptimeMillis() - l1);
                    if (l2 <= 0L)
                        break;
                    this.mResultLock.wait(l2);
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            return bool;
        }

        public void onResult(String paramString1, String paramString2)
        {
            try
            {
                long l = Long.parseLong(paramString1);
                synchronized (this.mResultLock)
                {
                    if (l > this.mResultId)
                    {
                        this.mResult = Boolean.parseBoolean(paramString2);
                        this.mResultId = l;
                    }
                    this.mResultLock.notifyAll();
                }
                label60: return;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label60;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.AccessibilityInjector
 * JD-Core Version:        0.6.2
 */