package android.webkit;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ZoomControls;

@Deprecated
class ZoomControlExternal
    implements ZoomControlBase
{
    private static final long ZOOM_CONTROLS_TIMEOUT = ViewConfiguration.getZoomControlsTimeout();
    private final Handler mPrivateHandler = new Handler();
    private final WebViewClassic mWebView;
    private Runnable mZoomControlRunnable;
    private ExtendedZoomControls mZoomControls;

    public ZoomControlExternal(WebViewClassic paramWebViewClassic)
    {
        this.mWebView = paramWebViewClassic;
    }

    private ExtendedZoomControls createZoomControls()
    {
        ExtendedZoomControls localExtendedZoomControls = new ExtendedZoomControls(this.mWebView.getContext());
        localExtendedZoomControls.setOnZoomInClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                ZoomControlExternal.this.mPrivateHandler.removeCallbacks(ZoomControlExternal.this.mZoomControlRunnable);
                ZoomControlExternal.this.mPrivateHandler.postDelayed(ZoomControlExternal.this.mZoomControlRunnable, ZoomControlExternal.ZOOM_CONTROLS_TIMEOUT);
                ZoomControlExternal.this.mWebView.zoomIn();
            }
        });
        localExtendedZoomControls.setOnZoomOutClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                ZoomControlExternal.this.mPrivateHandler.removeCallbacks(ZoomControlExternal.this.mZoomControlRunnable);
                ZoomControlExternal.this.mPrivateHandler.postDelayed(ZoomControlExternal.this.mZoomControlRunnable, ZoomControlExternal.ZOOM_CONTROLS_TIMEOUT);
                ZoomControlExternal.this.mWebView.zoomOut();
            }
        });
        return localExtendedZoomControls;
    }

    public ExtendedZoomControls getControls()
    {
        if (this.mZoomControls == null)
        {
            this.mZoomControls = createZoomControls();
            this.mZoomControls.setVisibility(0);
            this.mZoomControlRunnable = new Runnable()
            {
                public void run()
                {
                    if (!ZoomControlExternal.this.mZoomControls.hasFocus())
                        ZoomControlExternal.this.mZoomControls.hide();
                    while (true)
                    {
                        return;
                        ZoomControlExternal.this.mPrivateHandler.removeCallbacks(ZoomControlExternal.this.mZoomControlRunnable);
                        ZoomControlExternal.this.mPrivateHandler.postDelayed(ZoomControlExternal.this.mZoomControlRunnable, ZoomControlExternal.ZOOM_CONTROLS_TIMEOUT);
                    }
                }
            };
        }
        return this.mZoomControls;
    }

    public void hide()
    {
        if (this.mZoomControlRunnable != null)
            this.mPrivateHandler.removeCallbacks(this.mZoomControlRunnable);
        if (this.mZoomControls != null)
            this.mZoomControls.hide();
    }

    public boolean isVisible()
    {
        if ((this.mZoomControls != null) && (this.mZoomControls.isShown()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void show()
    {
        if (this.mZoomControlRunnable != null)
            this.mPrivateHandler.removeCallbacks(this.mZoomControlRunnable);
        getControls().show(true);
        this.mPrivateHandler.postDelayed(this.mZoomControlRunnable, ZOOM_CONTROLS_TIMEOUT);
    }

    public void update()
    {
    }

    private static class ExtendedZoomControls extends FrameLayout
    {
        private ZoomControls mPlusMinusZoomControls;

        public ExtendedZoomControls(Context paramContext)
        {
            super(null);
            ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(17367246, this, true);
            this.mPlusMinusZoomControls = ((ZoomControls)findViewById(16909165));
            findViewById(16909168).setVisibility(8);
        }

        private void fade(int paramInt, float paramFloat1, float paramFloat2)
        {
            AlphaAnimation localAlphaAnimation = new AlphaAnimation(paramFloat1, paramFloat2);
            localAlphaAnimation.setDuration(500L);
            startAnimation(localAlphaAnimation);
            setVisibility(paramInt);
        }

        public boolean hasFocus()
        {
            return this.mPlusMinusZoomControls.hasFocus();
        }

        public void hide()
        {
            fade(8, 1.0F, 0.0F);
        }

        public void setOnZoomInClickListener(View.OnClickListener paramOnClickListener)
        {
            this.mPlusMinusZoomControls.setOnZoomInClickListener(paramOnClickListener);
        }

        public void setOnZoomOutClickListener(View.OnClickListener paramOnClickListener)
        {
            this.mPlusMinusZoomControls.setOnZoomOutClickListener(paramOnClickListener);
        }

        public void show(boolean paramBoolean)
        {
            ZoomControls localZoomControls = this.mPlusMinusZoomControls;
            if (paramBoolean);
            for (int i = 0; ; i = 8)
            {
                localZoomControls.setVisibility(i);
                fade(0, 0.0F, 1.0F);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.ZoomControlExternal
 * JD-Core Version:        0.6.2
 */