package android.webkit;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

class PluginFullScreenHolder
{
    private static CustomFrameLayout mLayout;
    private final WebChromeClient.CustomViewCallback mCallback = new WebChromeClient.CustomViewCallback()
    {
        public void onCustomViewHidden()
        {
            PluginFullScreenHolder.this.mWebView.mPrivateHandler.obtainMessage(121).sendToTarget();
            PluginFullScreenHolder.this.mWebView.getWebViewCore().sendMessage(182, PluginFullScreenHolder.this.mNpp, 0);
            PluginFullScreenHolder.mLayout.removeView(PluginFullScreenHolder.this.mContentView);
            PluginFullScreenHolder.access$302(null);
            PluginFullScreenHolder.this.mWebView.getViewManager().showAll();
        }
    };
    private View mContentView;
    private final int mNpp;
    private final int mOrientation;
    private final WebViewClassic mWebView;

    PluginFullScreenHolder(WebViewClassic paramWebViewClassic, int paramInt1, int paramInt2)
    {
        this.mWebView = paramWebViewClassic;
        this.mNpp = paramInt2;
        this.mOrientation = paramInt1;
    }

    public void hide()
    {
        this.mWebView.getWebChromeClient().onHideCustomView();
    }

    public void setContentView(View paramView)
    {
        mLayout = new CustomFrameLayout(this.mWebView.getContext());
        FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1, 17);
        mLayout.addView(paramView, localLayoutParams);
        mLayout.setVisibility(0);
        if ((paramView instanceof SurfaceView))
        {
            SurfaceView localSurfaceView = (SurfaceView)paramView;
            if (localSurfaceView.isFixedSize())
                localSurfaceView.getHolder().setSizeFromLayout();
        }
        this.mContentView = paramView;
    }

    public void show()
    {
        if (this.mWebView.getViewManager() != null)
            this.mWebView.getViewManager().hideAll();
        this.mWebView.getWebChromeClient().onShowCustomView(mLayout, this.mOrientation, this.mCallback);
    }

    private class CustomFrameLayout extends FrameLayout
    {
        CustomFrameLayout(Context arg2)
        {
            super();
        }

        public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
        {
            if (paramKeyEvent.isSystem());
            for (boolean bool = super.onKeyDown(paramInt, paramKeyEvent); ; bool = true)
            {
                return bool;
                PluginFullScreenHolder.this.mWebView.onKeyDown(paramInt, paramKeyEvent);
            }
        }

        public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
        {
            if (paramKeyEvent.isSystem());
            for (boolean bool = super.onKeyUp(paramInt, paramKeyEvent); ; bool = true)
            {
                return bool;
                PluginFullScreenHolder.this.mWebView.onKeyUp(paramInt, paramKeyEvent);
            }
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            return true;
        }

        public boolean onTrackballEvent(MotionEvent paramMotionEvent)
        {
            PluginFullScreenHolder.this.mWebView.onTrackballEvent(paramMotionEvent);
            return true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.PluginFullScreenHolder
 * JD-Core Version:        0.6.2
 */