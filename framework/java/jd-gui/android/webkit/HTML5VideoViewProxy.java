package android.webkit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.http.EventHandler;
import android.net.http.Headers;
import android.net.http.RequestHandle;
import android.net.http.RequestQueue;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

class HTML5VideoViewProxy extends Handler
    implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, SurfaceTexture.OnFrameAvailableListener
{
    private static final int BUFFERING_END = 106;
    private static final int BUFFERING_START = 105;
    private static final int ENDED = 201;
    private static final int ERROR = 103;
    private static final int LOAD_DEFAULT_POSTER = 104;
    private static final String LOGTAG = "HTML5VideoViewProxy";
    private static final int PAUSE = 102;
    private static final int PAUSED = 203;
    private static final int PLAY = 100;
    private static final int POSTER_FETCHED = 202;
    private static final int PREPARED = 200;
    private static final int RESTORESTATE = 205;
    private static final int SEEK = 101;
    private static final int STOPFULLSCREEN = 204;
    private static final int TIMEUPDATE = 300;
    int mNativePointer;
    private Bitmap mPoster;
    private PosterDownloader mPosterDownloader;
    private int mSeekPosition;
    private Handler mWebCoreHandler;
    private WebViewClassic mWebView;

    private HTML5VideoViewProxy(WebViewClassic paramWebViewClassic, int paramInt)
    {
        super(Looper.getMainLooper());
        this.mWebView = paramWebViewClassic;
        this.mWebView.setHTML5VideoViewProxy(this);
        this.mNativePointer = paramInt;
        createWebCoreHandler();
    }

    private void createWebCoreHandler()
    {
        this.mWebCoreHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                switch (paramAnonymousMessage.what)
                {
                default:
                case 200:
                case 201:
                case 203:
                case 202:
                case 300:
                case 204:
                case 205:
                }
                while (true)
                {
                    return;
                    Map localMap = (Map)paramAnonymousMessage.obj;
                    Integer localInteger1 = (Integer)localMap.get("dur");
                    Integer localInteger2 = (Integer)localMap.get("width");
                    Integer localInteger3 = (Integer)localMap.get("height");
                    HTML5VideoViewProxy.this.nativeOnPrepared(localInteger1.intValue(), localInteger2.intValue(), localInteger3.intValue(), HTML5VideoViewProxy.this.mNativePointer);
                    continue;
                    HTML5VideoViewProxy.access$802(HTML5VideoViewProxy.this, 0);
                    HTML5VideoViewProxy.this.nativeOnEnded(HTML5VideoViewProxy.this.mNativePointer);
                    continue;
                    HTML5VideoViewProxy.this.nativeOnPaused(HTML5VideoViewProxy.this.mNativePointer);
                    continue;
                    Bitmap localBitmap = (Bitmap)paramAnonymousMessage.obj;
                    HTML5VideoViewProxy.this.nativeOnPosterFetched(localBitmap, HTML5VideoViewProxy.this.mNativePointer);
                    continue;
                    HTML5VideoViewProxy.this.nativeOnTimeupdate(paramAnonymousMessage.arg1, HTML5VideoViewProxy.this.mNativePointer);
                    continue;
                    HTML5VideoViewProxy.this.nativeOnStopFullscreen(HTML5VideoViewProxy.this.mNativePointer);
                    continue;
                    HTML5VideoViewProxy.this.nativeOnRestoreState(HTML5VideoViewProxy.this.mNativePointer);
                }
            }
        };
    }

    private void doSetPoster(Bitmap paramBitmap)
    {
        if (paramBitmap == null);
        while (true)
        {
            return;
            this.mPoster = paramBitmap;
            Message localMessage = Message.obtain(this.mWebCoreHandler, 202);
            localMessage.obj = paramBitmap;
            this.mWebCoreHandler.sendMessage(localMessage);
        }
    }

    public static HTML5VideoViewProxy getInstance(WebViewCore paramWebViewCore, int paramInt)
    {
        return new HTML5VideoViewProxy(paramWebViewCore.getWebViewClassic(), paramInt);
    }

    private native void nativeOnEnded(int paramInt);

    private native void nativeOnPaused(int paramInt);

    private native void nativeOnPosterFetched(Bitmap paramBitmap, int paramInt);

    private native void nativeOnPrepared(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private native void nativeOnRestoreState(int paramInt);

    private native void nativeOnStopFullscreen(int paramInt);

    private native void nativeOnTimeupdate(int paramInt1, int paramInt2);

    private static native boolean nativeSendSurfaceTexture(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private void sendTimeupdate()
    {
        Message localMessage = Message.obtain(this.mWebCoreHandler, 300);
        localMessage.arg1 = VideoPlayer.getCurrentPosition();
        this.mWebCoreHandler.sendMessage(localMessage);
    }

    public void dispatchOnEnded()
    {
        Message localMessage = Message.obtain(this.mWebCoreHandler, 201);
        this.mWebCoreHandler.sendMessage(localMessage);
    }

    public void dispatchOnPaused()
    {
        Message localMessage = Message.obtain(this.mWebCoreHandler, 203);
        this.mWebCoreHandler.sendMessage(localMessage);
    }

    public void dispatchOnRestoreState()
    {
        Message localMessage = Message.obtain(this.mWebCoreHandler, 205);
        this.mWebCoreHandler.sendMessage(localMessage);
    }

    public void dispatchOnStopFullScreen()
    {
        Message localMessage = Message.obtain(this.mWebCoreHandler, 204);
        this.mWebCoreHandler.sendMessage(localMessage);
    }

    public void enterFullScreenVideo(int paramInt, String paramString)
    {
        VideoPlayer.enterFullScreenVideo(paramInt, paramString, this, this.mWebView);
    }

    public void exitFullScreenVideo()
    {
        VideoPlayer.exitFullScreenVideo(this, this.mWebView);
    }

    public Context getContext()
    {
        return this.mWebView.getContext();
    }

    WebViewClassic getWebView()
    {
        return this.mWebView;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 100:
        case 101:
        case 102:
        case 201:
        case 103:
        case 104:
        case 300:
        case 105:
        case 106:
        }
        while (true)
        {
            return;
            String str = (String)paramMessage.obj;
            WebChromeClient localWebChromeClient3 = this.mWebView.getWebChromeClient();
            int i = paramMessage.arg1;
            if (localWebChromeClient3 != null)
            {
                VideoPlayer.play(str, this.mSeekPosition, this, localWebChromeClient3, i);
                continue;
                this.mSeekPosition = ((Integer)paramMessage.obj).intValue();
                VideoPlayer.seek(this.mSeekPosition, this);
                continue;
                VideoPlayer.pause(this);
                continue;
                if (paramMessage.arg1 == 1)
                    VideoPlayer.access$102(true);
                VideoPlayer.end();
                continue;
                WebChromeClient localWebChromeClient2 = this.mWebView.getWebChromeClient();
                if (localWebChromeClient2 != null)
                {
                    localWebChromeClient2.onHideCustomView();
                    continue;
                    WebChromeClient localWebChromeClient1 = this.mWebView.getWebChromeClient();
                    if (localWebChromeClient1 != null)
                    {
                        doSetPoster(localWebChromeClient1.getDefaultVideoPoster());
                        continue;
                        if (VideoPlayer.isPlaying(this))
                        {
                            sendTimeupdate();
                            continue;
                            VideoPlayer.setPlayerBuffering(true);
                            continue;
                            VideoPlayer.setPlayerBuffering(false);
                        }
                    }
                }
            }
        }
    }

    public void loadPoster(String paramString)
    {
        if (paramString == null)
            sendMessage(obtainMessage(104));
        while (true)
        {
            return;
            if (this.mPosterDownloader != null)
                this.mPosterDownloader.cancelAndReleaseQueue();
            this.mPosterDownloader = new PosterDownloader(paramString, this);
            this.mPosterDownloader.start();
        }
    }

    public void onCompletion(MediaPlayer paramMediaPlayer)
    {
        sendMessage(obtainMessage(201, 1, 0));
    }

    public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
    {
        sendMessage(obtainMessage(103));
        return false;
    }

    public void onFrameAvailable(SurfaceTexture paramSurfaceTexture)
    {
        this.mWebView.invalidate();
    }

    public boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
    {
        if (paramInt1 == 701)
            sendMessage(obtainMessage(105, paramInt1, paramInt2));
        while (true)
        {
            return false;
            if (paramInt1 == 702)
                sendMessage(obtainMessage(106, paramInt1, paramInt2));
        }
    }

    public void onPrepared(MediaPlayer paramMediaPlayer)
    {
        VideoPlayer.onPrepared();
        Message localMessage = Message.obtain(this.mWebCoreHandler, 200);
        HashMap localHashMap = new HashMap();
        localHashMap.put("dur", new Integer(paramMediaPlayer.getDuration()));
        localHashMap.put("width", new Integer(paramMediaPlayer.getVideoWidth()));
        localHashMap.put("height", new Integer(paramMediaPlayer.getVideoHeight()));
        localMessage.obj = localHashMap;
        this.mWebCoreHandler.sendMessage(localMessage);
    }

    public void onTimeupdate()
    {
        sendMessage(obtainMessage(300));
    }

    public void pause()
    {
        sendMessage(obtainMessage(102));
    }

    public void pauseAndDispatch()
    {
        VideoPlayer.pauseAndDispatch();
    }

    public void play(String paramString, int paramInt1, int paramInt2)
    {
        if (paramString == null);
        while (true)
        {
            return;
            if (paramInt1 > 0)
                seek(paramInt1);
            Message localMessage = obtainMessage(100);
            localMessage.arg1 = paramInt2;
            localMessage.obj = paramString;
            sendMessage(localMessage);
        }
    }

    public void seek(int paramInt)
    {
        Message localMessage = obtainMessage(101);
        localMessage.obj = new Integer(paramInt);
        sendMessage(localMessage);
    }

    public void setBaseLayer(int paramInt)
    {
        VideoPlayer.setBaseLayer(paramInt);
    }

    public void teardown()
    {
        if (this.mPosterDownloader != null)
            this.mPosterDownloader.cancelAndReleaseQueue();
        this.mNativePointer = 0;
    }

    private static final class PosterDownloader
        implements EventHandler
    {
        private static int mQueueRefCount = 0;
        private static RequestQueue mRequestQueue;
        private Handler mHandler;
        private Headers mHeaders;
        private ByteArrayOutputStream mPosterBytes;
        private final HTML5VideoViewProxy mProxy;
        private RequestHandle mRequestHandle;
        private int mStatusCode;
        private URL mUrl;

        public PosterDownloader(String paramString, HTML5VideoViewProxy paramHTML5VideoViewProxy)
        {
            try
            {
                this.mUrl = new URL(paramString);
                this.mProxy = paramHTML5VideoViewProxy;
                this.mHandler = new Handler();
                return;
            }
            catch (MalformedURLException localMalformedURLException)
            {
                while (true)
                    this.mUrl = null;
            }
        }

        // ERROR //
        private void cleanup()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 67	android/webkit/HTML5VideoViewProxy$PosterDownloader:mPosterBytes	Ljava/io/ByteArrayOutputStream;
            //     4: ifnull +15 -> 19
            //     7: aload_0
            //     8: getfield 67	android/webkit/HTML5VideoViewProxy$PosterDownloader:mPosterBytes	Ljava/io/ByteArrayOutputStream;
            //     11: invokevirtual 72	java/io/ByteArrayOutputStream:close	()V
            //     14: aload_0
            //     15: aconst_null
            //     16: putfield 67	android/webkit/HTML5VideoViewProxy$PosterDownloader:mPosterBytes	Ljava/io/ByteArrayOutputStream;
            //     19: return
            //     20: astore_2
            //     21: aload_0
            //     22: aconst_null
            //     23: putfield 67	android/webkit/HTML5VideoViewProxy$PosterDownloader:mPosterBytes	Ljava/io/ByteArrayOutputStream;
            //     26: aload_2
            //     27: athrow
            //     28: astore_1
            //     29: goto -15 -> 14
            //
            // Exception table:
            //     from	to	target	type
            //     7	14	20	finally
            //     7	14	28	java/io/IOException
        }

        private void releaseQueue()
        {
            if (mQueueRefCount == 0);
            while (true)
            {
                return;
                int i = -1 + mQueueRefCount;
                mQueueRefCount = i;
                if (i == 0)
                {
                    mRequestQueue.shutdown();
                    mRequestQueue = null;
                }
            }
        }

        private void retainQueue()
        {
            if (mRequestQueue == null)
                mRequestQueue = new RequestQueue(this.mProxy.getContext());
            mQueueRefCount = 1 + mQueueRefCount;
        }

        public void cancelAndReleaseQueue()
        {
            if (this.mRequestHandle != null)
            {
                this.mRequestHandle.cancel();
                this.mRequestHandle = null;
            }
            releaseQueue();
        }

        public void certificate(SslCertificate paramSslCertificate)
        {
        }

        public void data(byte[] paramArrayOfByte, int paramInt)
        {
            if (this.mPosterBytes == null)
                this.mPosterBytes = new ByteArrayOutputStream();
            this.mPosterBytes.write(paramArrayOfByte, 0, paramInt);
        }

        public void endData()
        {
            if (this.mStatusCode == 200)
            {
                if (this.mPosterBytes.size() > 0)
                {
                    Bitmap localBitmap = BitmapFactory.decodeByteArray(this.mPosterBytes.toByteArray(), 0, this.mPosterBytes.size());
                    this.mProxy.doSetPoster(localBitmap);
                }
                cleanup();
            }
            while (true)
            {
                return;
                if ((this.mStatusCode < 300) || (this.mStatusCode >= 400))
                    continue;
                try
                {
                    this.mUrl = new URL(this.mHeaders.getLocation());
                    if (this.mUrl == null)
                        continue;
                    this.mHandler.post(new Runnable()
                    {
                        public void run()
                        {
                            if (HTML5VideoViewProxy.PosterDownloader.this.mRequestHandle != null)
                                HTML5VideoViewProxy.PosterDownloader.this.mRequestHandle.setupRedirect(HTML5VideoViewProxy.PosterDownloader.this.mUrl.toString(), HTML5VideoViewProxy.PosterDownloader.this.mStatusCode, new HashMap());
                        }
                    });
                }
                catch (MalformedURLException localMalformedURLException)
                {
                    while (true)
                        this.mUrl = null;
                }
            }
        }

        public void error(int paramInt, String paramString)
        {
            cleanup();
        }

        public boolean handleSslErrorRequest(SslError paramSslError)
        {
            return false;
        }

        public void headers(Headers paramHeaders)
        {
            this.mHeaders = paramHeaders;
        }

        public void start()
        {
            retainQueue();
            if (this.mUrl == null);
            while (true)
            {
                return;
                String str = this.mUrl.getProtocol();
                if (("http".equals(str)) || ("https".equals(str)))
                    this.mRequestHandle = mRequestQueue.queueRequest(this.mUrl.toString(), "GET", null, this, null, 0);
            }
        }

        public void status(int paramInt1, int paramInt2, int paramInt3, String paramString)
        {
            this.mStatusCode = paramInt3;
        }
    }

    private static final class VideoPlayer
    {
        private static boolean isVideoSelfEnded = false;
        private static int mBaseLayer = 0;
        private static HTML5VideoViewProxy mCurrentProxy;
        private static HTML5VideoView mHTML5VideoView;

        public static void end()
        {
            mHTML5VideoView.showControllerInFullScreen();
            if (mCurrentProxy != null)
            {
                if (!isVideoSelfEnded)
                    break label29;
                mCurrentProxy.dispatchOnEnded();
            }
            while (true)
            {
                isVideoSelfEnded = false;
                return;
                label29: mCurrentProxy.dispatchOnPaused();
            }
        }

        public static void enterFullScreenVideo(int paramInt, String paramString, HTML5VideoViewProxy paramHTML5VideoViewProxy, WebViewClassic paramWebViewClassic)
        {
            int i = 0;
            boolean bool1 = false;
            boolean bool2 = false;
            int j;
            if (mHTML5VideoView != null)
            {
                if ((!mHTML5VideoView.fullScreenExited()) && (mHTML5VideoView.isFullScreenMode()))
                {
                    Log.w("HTML5VideoViewProxy", "Try to reenter the full screen mode");
                    return;
                }
                j = mHTML5VideoView.getCurrentState();
                if (paramInt == mHTML5VideoView.getVideoLayerId())
                {
                    i = mHTML5VideoView.getCurrentPosition();
                    if (((j != 1) && (j != 2) && (j != 3)) || (mHTML5VideoView.isFullScreenMode()))
                        break label162;
                }
            }
            label162: for (bool1 = true; ; bool1 = false)
            {
                if (bool1)
                    break label168;
                mHTML5VideoView.reset();
                mHTML5VideoView = new HTML5VideoFullScreen(paramHTML5VideoViewProxy.getContext(), paramInt, i, bool1);
                mHTML5VideoView.setStartWhenPrepared(bool2);
                mCurrentProxy = paramHTML5VideoViewProxy;
                mHTML5VideoView.setVideoURI(paramString, mCurrentProxy);
                mHTML5VideoView.enterFullScreenVideoState(paramInt, paramHTML5VideoViewProxy, paramWebViewClassic);
                break;
            }
            label168: if ((j == 1) || (j == 3));
            for (bool2 = true; ; bool2 = false)
                break;
        }

        public static void exitFullScreenVideo(HTML5VideoViewProxy paramHTML5VideoViewProxy, WebViewClassic paramWebViewClassic)
        {
            if ((!mHTML5VideoView.fullScreenExited()) && (mHTML5VideoView.isFullScreenMode()))
            {
                WebChromeClient localWebChromeClient = paramWebViewClassic.getWebChromeClient();
                if (localWebChromeClient != null)
                    localWebChromeClient.onHideCustomView();
            }
        }

        public static int getCurrentPosition()
        {
            int i = 0;
            if (mHTML5VideoView != null)
                i = mHTML5VideoView.getCurrentPosition();
            return i;
        }

        public static boolean isPlaying(HTML5VideoViewProxy paramHTML5VideoViewProxy)
        {
            if ((mCurrentProxy == paramHTML5VideoViewProxy) && (mHTML5VideoView != null) && (mHTML5VideoView.isPlaying()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public static void onPrepared()
        {
            if (!mHTML5VideoView.isFullScreenMode())
                mHTML5VideoView.start();
            if (mBaseLayer != 0)
                setBaseLayer(mBaseLayer);
        }

        public static void pause(HTML5VideoViewProxy paramHTML5VideoViewProxy)
        {
            if ((mCurrentProxy == paramHTML5VideoViewProxy) && (mHTML5VideoView != null))
                mHTML5VideoView.pause();
        }

        public static void pauseAndDispatch()
        {
            if (mHTML5VideoView != null)
            {
                mHTML5VideoView.pauseAndDispatch(mCurrentProxy);
                setBaseLayer(mBaseLayer);
            }
        }

        public static void play(String paramString, int paramInt1, HTML5VideoViewProxy paramHTML5VideoViewProxy, WebChromeClient paramWebChromeClient, int paramInt2)
        {
            int i = -1;
            boolean bool = false;
            if (mHTML5VideoView != null)
            {
                i = mHTML5VideoView.getVideoLayerId();
                bool = mHTML5VideoView.fullScreenExited();
                if ((mHTML5VideoView.isFullScreenMode()) && (!bool) && (i != paramInt2) && (mCurrentProxy != paramHTML5VideoViewProxy))
                {
                    mCurrentProxy = paramHTML5VideoViewProxy;
                    mHTML5VideoView.setStartWhenPrepared(true);
                    mHTML5VideoView.setVideoURI(paramString, paramHTML5VideoViewProxy);
                    mHTML5VideoView.reprepareData(paramHTML5VideoViewProxy);
                }
            }
            while (true)
            {
                return;
                if ((bool) || (i != paramInt2) || (mHTML5VideoView.surfaceTextureDeleted()))
                {
                    if (mHTML5VideoView != null)
                    {
                        if (!bool)
                            mHTML5VideoView.pauseAndDispatch(mCurrentProxy);
                        mHTML5VideoView.reset();
                    }
                    mCurrentProxy = paramHTML5VideoViewProxy;
                    mHTML5VideoView = new HTML5VideoInline(paramInt2, paramInt1);
                    mHTML5VideoView.setVideoURI(paramString, mCurrentProxy);
                    mHTML5VideoView.prepareDataAndDisplayMode(paramHTML5VideoViewProxy);
                }
                else if (mCurrentProxy == paramHTML5VideoViewProxy)
                {
                    if (!mHTML5VideoView.isPlaying())
                    {
                        mHTML5VideoView.seekTo(paramInt1);
                        mHTML5VideoView.start();
                    }
                }
                else if (mCurrentProxy != null)
                {
                    paramHTML5VideoViewProxy.dispatchOnEnded();
                }
            }
        }

        public static void seek(int paramInt, HTML5VideoViewProxy paramHTML5VideoViewProxy)
        {
            if ((mCurrentProxy == paramHTML5VideoViewProxy) && (paramInt >= 0) && (mHTML5VideoView != null))
                mHTML5VideoView.seekTo(paramInt);
        }

        public static void setBaseLayer(int paramInt)
        {
            if ((mHTML5VideoView != null) && (!mHTML5VideoView.isFullScreenMode()) && (!mHTML5VideoView.surfaceTextureDeleted()))
            {
                mBaseLayer = paramInt;
                int i = mHTML5VideoView.getVideoLayerId();
                SurfaceTexture localSurfaceTexture = HTML5VideoInline.getSurfaceTexture(i);
                int j = mHTML5VideoView.getTextureName();
                if ((paramInt != 0) && (localSurfaceTexture != null) && (i != -1))
                {
                    int k = mHTML5VideoView.getCurrentState();
                    if (mHTML5VideoView.getPlayerBuffering())
                        k = 1;
                    boolean bool = HTML5VideoViewProxy.nativeSendSurfaceTexture(localSurfaceTexture, paramInt, i, j, k);
                    if ((k >= 2) && (!bool))
                    {
                        mHTML5VideoView.pauseAndDispatch(mCurrentProxy);
                        mHTML5VideoView.deleteSurfaceTexture();
                    }
                }
            }
        }

        private static void setPlayerBuffering(boolean paramBoolean)
        {
            mHTML5VideoView.setPlayerBuffering(paramBoolean);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.HTML5VideoViewProxy
 * JD-Core Version:        0.6.2
 */