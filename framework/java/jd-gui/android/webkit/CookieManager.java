package android.webkit;

import android.net.WebAddress;

public class CookieManager
{
    public static boolean allowFileSchemeCookies()
    {
        return getInstance().allowFileSchemeCookiesImpl();
    }

    /** @deprecated */
    public static CookieManager getInstance()
    {
        try
        {
            CookieManager localCookieManager = WebViewFactory.getProvider().getCookieManager();
            return localCookieManager;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static void setAcceptFileSchemeCookies(boolean paramBoolean)
    {
        getInstance().setAcceptFileSchemeCookiesImpl(paramBoolean);
    }

    /** @deprecated */
    public boolean acceptCookie()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    protected boolean allowFileSchemeCookiesImpl()
    {
        throw new MustOverrideException();
    }

    protected Object clone()
        throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException("doesn't implement Cloneable");
    }

    protected void flushCookieStore()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public String getCookie(WebAddress paramWebAddress)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public String getCookie(String paramString)
    {
        throw new MustOverrideException();
    }

    public String getCookie(String paramString, boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public boolean hasCookies()
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean hasCookies(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    public void removeAllCookie()
    {
        throw new MustOverrideException();
    }

    public void removeExpiredCookie()
    {
        throw new MustOverrideException();
    }

    public void removeSessionCookie()
    {
        throw new MustOverrideException();
    }

    /** @deprecated */
    public void setAcceptCookie(boolean paramBoolean)
    {
        try
        {
            throw new MustOverrideException();
        }
        finally
        {
        }
    }

    protected void setAcceptFileSchemeCookiesImpl(boolean paramBoolean)
    {
        throw new MustOverrideException();
    }

    public void setCookie(String paramString1, String paramString2)
    {
        throw new MustOverrideException();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.CookieManager
 * JD-Core Version:        0.6.2
 */