package android.webkit;

import android.content.ContentResolver;
import android.graphics.Bitmap;

public class WebIconDatabase
{
    public static WebIconDatabase getInstance()
    {
        return WebViewFactory.getProvider().getWebIconDatabase();
    }

    public void bulkRequestIconForPageUrl(ContentResolver paramContentResolver, String paramString, IconListener paramIconListener)
    {
        throw new MustOverrideException();
    }

    public void close()
    {
        throw new MustOverrideException();
    }

    public void open(String paramString)
    {
        throw new MustOverrideException();
    }

    public void releaseIconForPageUrl(String paramString)
    {
        throw new MustOverrideException();
    }

    public void removeAllIcons()
    {
        throw new MustOverrideException();
    }

    public void requestIconForPageUrl(String paramString, IconListener paramIconListener)
    {
        throw new MustOverrideException();
    }

    public void retainIconForPageUrl(String paramString)
    {
        throw new MustOverrideException();
    }

    public static abstract interface IconListener
    {
        public abstract void onReceivedIcon(String paramString, Bitmap paramBitmap);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebIconDatabase
 * JD-Core Version:        0.6.2
 */