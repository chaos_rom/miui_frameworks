package android.webkit;

import android.os.Handler;
import android.os.Message;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class WebStorageClassic extends WebStorage
{
    private static final String CALLBACK = "callback";
    static final int DELETE_ALL = 3;
    static final int DELETE_ORIGIN = 2;
    static final int GET_ORIGINS = 4;
    static final int GET_QUOTA_ORIGIN = 6;
    static final int GET_USAGE_ORIGIN = 5;
    private static final String ORIGIN = "origin";
    private static final String ORIGINS = "origins";
    private static final String QUOTA = "quota";
    static final int RETURN_ORIGINS = 0;
    static final int RETURN_QUOTA_ORIGIN = 2;
    static final int RETURN_USAGE_ORIGIN = 1;
    static final int SET_QUOTA_ORIGIN = 1;
    static final int UPDATE = 0;
    private static final String USAGE = "usage";
    private static WebStorageClassic sWebStorage;
    private Handler mHandler = null;
    private Map<String, WebStorage.Origin> mOrigins;
    private Handler mUIHandler = null;

    public static WebStorageClassic getInstance()
    {
        if (sWebStorage == null)
            sWebStorage = new WebStorageClassic();
        return sWebStorage;
    }

    private static native void nativeDeleteAllData();

    private static native void nativeDeleteOrigin(String paramString);

    private static native Set nativeGetOrigins();

    private static native long nativeGetQuotaForOrigin(String paramString);

    private static native long nativeGetUsageForOrigin(String paramString);

    private static native void nativeSetAppCacheMaximumSize(long paramLong);

    private static native void nativeSetQuotaForOrigin(String paramString, long paramLong);

    /** @deprecated */
    private void postMessage(Message paramMessage)
    {
        try
        {
            if (this.mHandler != null)
                this.mHandler.sendMessage(paramMessage);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void postUIMessage(Message paramMessage)
    {
        if (this.mUIHandler != null)
            this.mUIHandler.sendMessage(paramMessage);
    }

    private void syncValues()
    {
        Set localSet = nativeGetOrigins();
        this.mOrigins = new HashMap();
        Iterator localIterator = localSet.iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            WebStorage.Origin localOrigin = new WebStorage.Origin(str, nativeGetQuotaForOrigin(str), nativeGetUsageForOrigin(str));
            this.mOrigins.put(str, localOrigin);
        }
    }

    /** @deprecated */
    public void createHandler()
    {
        try
        {
            if (this.mHandler == null)
                this.mHandler = new Handler()
                {
                    public void handleMessage(Message paramAnonymousMessage)
                    {
                        switch (paramAnonymousMessage.what)
                        {
                        default:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 0:
                        }
                        while (true)
                        {
                            return;
                            WebStorage.Origin localOrigin3 = (WebStorage.Origin)paramAnonymousMessage.obj;
                            WebStorageClassic.nativeSetQuotaForOrigin(localOrigin3.getOrigin(), localOrigin3.getQuota());
                            continue;
                            WebStorageClassic.nativeDeleteOrigin(((WebStorage.Origin)paramAnonymousMessage.obj).getOrigin());
                            continue;
                            WebStorageClassic.access$200();
                            continue;
                            WebStorageClassic.this.syncValues();
                            ValueCallback localValueCallback3 = (ValueCallback)paramAnonymousMessage.obj;
                            HashMap localHashMap3 = new HashMap(WebStorageClassic.this.mOrigins);
                            HashMap localHashMap4 = new HashMap();
                            localHashMap4.put("callback", localValueCallback3);
                            localHashMap4.put("origins", localHashMap3);
                            WebStorageClassic.this.postUIMessage(Message.obtain(null, 0, localHashMap4));
                            continue;
                            WebStorageClassic.this.syncValues();
                            Map localMap2 = (Map)paramAnonymousMessage.obj;
                            String str2 = (String)localMap2.get("origin");
                            ValueCallback localValueCallback2 = (ValueCallback)localMap2.get("callback");
                            WebStorage.Origin localOrigin2 = (WebStorage.Origin)WebStorageClassic.this.mOrigins.get(str2);
                            HashMap localHashMap2 = new HashMap();
                            localHashMap2.put("callback", localValueCallback2);
                            if (localOrigin2 != null)
                                localHashMap2.put("usage", new Long(localOrigin2.getUsage()));
                            WebStorageClassic.this.postUIMessage(Message.obtain(null, 1, localHashMap2));
                            continue;
                            WebStorageClassic.this.syncValues();
                            Map localMap1 = (Map)paramAnonymousMessage.obj;
                            String str1 = (String)localMap1.get("origin");
                            ValueCallback localValueCallback1 = (ValueCallback)localMap1.get("callback");
                            WebStorage.Origin localOrigin1 = (WebStorage.Origin)WebStorageClassic.this.mOrigins.get(str1);
                            HashMap localHashMap1 = new HashMap();
                            localHashMap1.put("callback", localValueCallback1);
                            if (localOrigin1 != null)
                                localHashMap1.put("quota", new Long(localOrigin1.getQuota()));
                            WebStorageClassic.this.postUIMessage(Message.obtain(null, 2, localHashMap1));
                            continue;
                            WebStorageClassic.this.syncValues();
                        }
                    }
                };
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void createUIHandler()
    {
        if (this.mUIHandler == null)
            this.mUIHandler = new Handler()
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    switch (paramAnonymousMessage.what)
                    {
                    default:
                    case 0:
                    case 1:
                    case 2:
                    }
                    while (true)
                    {
                        return;
                        Map localMap3 = (Map)paramAnonymousMessage.obj;
                        Map localMap4 = (Map)localMap3.get("origins");
                        ((ValueCallback)localMap3.get("callback")).onReceiveValue(localMap4);
                        continue;
                        Map localMap2 = (Map)paramAnonymousMessage.obj;
                        ((ValueCallback)localMap2.get("callback")).onReceiveValue((Long)localMap2.get("usage"));
                        continue;
                        Map localMap1 = (Map)paramAnonymousMessage.obj;
                        ((ValueCallback)localMap1.get("callback")).onReceiveValue((Long)localMap1.get("quota"));
                    }
                }
            };
    }

    public void deleteAllData()
    {
        if ("WebViewCoreThread".equals(Thread.currentThread().getName()))
            nativeDeleteAllData();
        while (true)
        {
            return;
            postMessage(Message.obtain(null, 3));
        }
    }

    public void deleteOrigin(String paramString)
    {
        if (paramString != null)
        {
            if (!"WebViewCoreThread".equals(Thread.currentThread().getName()))
                break label23;
            nativeDeleteOrigin(paramString);
        }
        while (true)
        {
            return;
            label23: postMessage(Message.obtain(null, 2, new WebStorage.Origin(paramString)));
        }
    }

    public void getOrigins(ValueCallback<Map> paramValueCallback)
    {
        if (paramValueCallback != null)
        {
            if (!"WebViewCoreThread".equals(Thread.currentThread().getName()))
                break label33;
            syncValues();
            paramValueCallback.onReceiveValue(this.mOrigins);
        }
        while (true)
        {
            return;
            label33: postMessage(Message.obtain(null, 4, paramValueCallback));
        }
    }

    Collection<WebStorage.Origin> getOriginsSync()
    {
        if ("WebViewCoreThread".equals(Thread.currentThread().getName()))
            update();
        for (Collection localCollection = this.mOrigins.values(); ; localCollection = null)
            return localCollection;
    }

    public void getQuotaForOrigin(String paramString, ValueCallback<Long> paramValueCallback)
    {
        if (paramValueCallback == null);
        while (true)
        {
            return;
            if (paramString == null)
            {
                paramValueCallback.onReceiveValue(null);
            }
            else if ("WebViewCoreThread".equals(Thread.currentThread().getName()))
            {
                syncValues();
                paramValueCallback.onReceiveValue(new Long(((WebStorage.Origin)this.mOrigins.get(paramString)).getUsage()));
            }
            else
            {
                HashMap localHashMap = new HashMap();
                localHashMap.put("origin", paramString);
                localHashMap.put("callback", paramValueCallback);
                postMessage(Message.obtain(null, 6, localHashMap));
            }
        }
    }

    public void getUsageForOrigin(String paramString, ValueCallback<Long> paramValueCallback)
    {
        if (paramValueCallback == null);
        while (true)
        {
            return;
            if (paramString == null)
            {
                paramValueCallback.onReceiveValue(null);
            }
            else if ("WebViewCoreThread".equals(Thread.currentThread().getName()))
            {
                syncValues();
                paramValueCallback.onReceiveValue(new Long(((WebStorage.Origin)this.mOrigins.get(paramString)).getUsage()));
            }
            else
            {
                HashMap localHashMap = new HashMap();
                localHashMap.put("origin", paramString);
                localHashMap.put("callback", paramValueCallback);
                postMessage(Message.obtain(null, 5, localHashMap));
            }
        }
    }

    public void setAppCacheMaximumSize(long paramLong)
    {
        nativeSetAppCacheMaximumSize(paramLong);
    }

    public void setQuotaForOrigin(String paramString, long paramLong)
    {
        if (paramString != null)
        {
            if (!"WebViewCoreThread".equals(Thread.currentThread().getName()))
                break label24;
            nativeSetQuotaForOrigin(paramString, paramLong);
        }
        while (true)
        {
            return;
            label24: postMessage(Message.obtain(null, 1, new WebStorage.Origin(paramString, paramLong)));
        }
    }

    public void update()
    {
        if ("WebViewCoreThread".equals(Thread.currentThread().getName()))
            syncValues();
        while (true)
        {
            return;
            postMessage(Message.obtain(null, 0));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.WebStorageClassic
 * JD-Core Version:        0.6.2
 */