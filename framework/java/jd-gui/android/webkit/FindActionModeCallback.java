package android.webkit;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

class FindActionModeCallback
    implements ActionMode.Callback, TextWatcher, View.OnClickListener
{
    private ActionMode mActionMode;
    private int mActiveMatchIndex;
    private View mCustomView;
    private EditText mEditText;
    private Point mGlobalVisibleOffset = new Point();
    private Rect mGlobalVisibleRect = new Rect();
    private InputMethodManager mInput;
    private TextView mMatches;
    private boolean mMatchesFound;
    private int mNumberOfMatches;
    private Resources mResources;
    private WebViewClassic mWebView;

    FindActionModeCallback(Context paramContext)
    {
        this.mCustomView = LayoutInflater.from(paramContext).inflate(17367239, null);
        this.mEditText = ((EditText)this.mCustomView.findViewById(16908291));
        this.mEditText.setCustomSelectionActionModeCallback(new NoAction());
        this.mEditText.setOnClickListener(this);
        setText("");
        this.mMatches = ((TextView)this.mCustomView.findViewById(16909159));
        this.mInput = ((InputMethodManager)paramContext.getSystemService("input_method"));
        this.mResources = paramContext.getResources();
    }

    private void findNext(boolean paramBoolean)
    {
        if (this.mWebView == null)
            throw new AssertionError("No WebView for FindActionModeCallback::findNext");
        if (!this.mMatchesFound)
            findAll();
        while (true)
        {
            return;
            if (this.mNumberOfMatches != 0)
            {
                this.mWebView.findNext(paramBoolean);
                updateMatchesString();
            }
        }
    }

    private void updateMatchesString()
    {
        if (this.mNumberOfMatches == 0)
            this.mMatches.setText(17040526);
        while (true)
        {
            this.mMatches.setVisibility(0);
            return;
            TextView localTextView = this.mMatches;
            Resources localResources = this.mResources;
            int i = this.mNumberOfMatches;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = Integer.valueOf(1 + this.mActiveMatchIndex);
            arrayOfObject[1] = Integer.valueOf(this.mNumberOfMatches);
            localTextView.setText(localResources.getQuantityString(18022419, i, arrayOfObject));
        }
    }

    public void afterTextChanged(Editable paramEditable)
    {
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    void findAll()
    {
        if (this.mWebView == null)
            throw new AssertionError("No WebView for FindActionModeCallback::findAll");
        Editable localEditable = this.mEditText.getText();
        if (localEditable.length() == 0)
        {
            this.mWebView.clearMatches();
            this.mMatches.setVisibility(8);
            this.mMatchesFound = false;
            this.mWebView.findAll(null);
        }
        while (true)
        {
            return;
            this.mMatchesFound = true;
            this.mMatches.setVisibility(4);
            this.mNumberOfMatches = 0;
            this.mWebView.findAllAsync(localEditable.toString());
        }
    }

    void finish()
    {
        this.mActionMode.finish();
    }

    public int getActionModeGlobalBottom()
    {
        if (this.mActionMode == null);
        for (int i = 0; ; i = this.mGlobalVisibleRect.bottom)
        {
            return i;
            View localView = (View)this.mCustomView.getParent();
            if (localView == null)
                localView = this.mCustomView;
            localView.getGlobalVisibleRect(this.mGlobalVisibleRect, this.mGlobalVisibleOffset);
        }
    }

    public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
        boolean bool = false;
        if (this.mWebView == null)
            throw new AssertionError("No WebView for FindActionModeCallback::onActionItemClicked");
        this.mInput.hideSoftInputFromWindow(this.mWebView.getWebView().getWindowToken(), 0);
        switch (paramMenuItem.getItemId())
        {
        default:
            return bool;
        case 16909174:
            findNext(false);
        case 16909175:
        }
        while (true)
        {
            bool = true;
            break;
            findNext(true);
        }
    }

    public void onClick(View paramView)
    {
        findNext(true);
    }

    public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
        boolean bool = false;
        if (!paramActionMode.isUiFocusable());
        while (true)
        {
            return bool;
            paramActionMode.setCustomView(this.mCustomView);
            paramActionMode.getMenuInflater().inflate(18087937, paramMenu);
            this.mActionMode = paramActionMode;
            Editable localEditable = this.mEditText.getText();
            Selection.setSelection(localEditable, localEditable.length());
            this.mMatches.setVisibility(8);
            this.mMatchesFound = false;
            this.mMatches.setText("0");
            this.mEditText.requestFocus();
            bool = true;
        }
    }

    public void onDestroyActionMode(ActionMode paramActionMode)
    {
        this.mActionMode = null;
        this.mWebView.notifyFindDialogDismissed();
        this.mInput.hideSoftInputFromWindow(this.mWebView.getWebView().getWindowToken(), 0);
    }

    public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
        return false;
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        findAll();
    }

    void setText(String paramString)
    {
        this.mEditText.setText(paramString);
        Editable localEditable = this.mEditText.getText();
        int i = localEditable.length();
        Selection.setSelection(localEditable, i, i);
        localEditable.setSpan(this, 0, i, 18);
        this.mMatchesFound = false;
    }

    void setWebView(WebViewClassic paramWebViewClassic)
    {
        if (paramWebViewClassic == null)
            throw new AssertionError("WebView supplied to FindActionModeCallback cannot be null");
        this.mWebView = paramWebViewClassic;
    }

    public void showSoftInput()
    {
        this.mInput.startGettingWindowFocus(this.mEditText.getRootView());
        this.mInput.focusIn(this.mEditText);
        this.mInput.showSoftInput(this.mEditText, 0);
    }

    public void updateMatchCount(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (!paramBoolean)
        {
            this.mNumberOfMatches = paramInt2;
            this.mActiveMatchIndex = paramInt1;
            updateMatchesString();
        }
        while (true)
        {
            return;
            this.mMatches.setVisibility(4);
            this.mNumberOfMatches = 0;
        }
    }

    public static class NoAction
        implements ActionMode.Callback
    {
        public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
        {
            return false;
        }

        public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            return false;
        }

        public void onDestroyActionMode(ActionMode paramActionMode)
        {
        }

        public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
        {
            return false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.FindActionModeCallback
 * JD-Core Version:        0.6.2
 */