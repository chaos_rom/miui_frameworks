package android.webkit;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.MediaPlayer;
import android.opengl.GLES20;
import android.view.Surface;

public class HTML5VideoInline extends HTML5VideoView
{
    private static SurfaceTexture mSurfaceTexture = null;
    private static int[] mTextureNames = null;
    private static int mVideoLayerUsingSurfaceTexture = -1;

    HTML5VideoInline(int paramInt1, int paramInt2)
    {
        init(paramInt1, paramInt2, false);
    }

    public static void cleanupSurfaceTexture()
    {
        mSurfaceTexture = null;
        mVideoLayerUsingSurfaceTexture = -1;
    }

    public static SurfaceTexture getSurfaceTexture(int paramInt)
    {
        if ((paramInt != mVideoLayerUsingSurfaceTexture) || (mSurfaceTexture == null) || (mTextureNames == null))
        {
            mTextureNames = new int[1];
            GLES20.glGenTextures(1, mTextureNames, 0);
            mSurfaceTexture = new SurfaceTexture(mTextureNames[0]);
        }
        mVideoLayerUsingSurfaceTexture = paramInt;
        return mSurfaceTexture;
    }

    private void setFrameAvailableListener(SurfaceTexture.OnFrameAvailableListener paramOnFrameAvailableListener)
    {
        mSurfaceTexture.setOnFrameAvailableListener(paramOnFrameAvailableListener);
    }

    public void decideDisplayMode()
    {
        Surface localSurface = new Surface(getSurfaceTexture(getVideoLayerId()));
        mPlayer.setSurface(localSurface);
        localSurface.release();
    }

    public void deleteSurfaceTexture()
    {
        cleanupSurfaceTexture();
    }

    public int getTextureName()
    {
        int i = 0;
        if (mTextureNames != null)
            i = mTextureNames[0];
        return i;
    }

    public void pauseAndDispatch(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        super.pauseAndDispatch(paramHTML5VideoViewProxy);
    }

    public void prepareDataAndDisplayMode(HTML5VideoViewProxy paramHTML5VideoViewProxy)
    {
        super.prepareDataAndDisplayMode(paramHTML5VideoViewProxy);
        setFrameAvailableListener(paramHTML5VideoViewProxy);
        if (this.mProxy.getContext().checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0)
            mPlayer.setWakeMode(paramHTML5VideoViewProxy.getContext(), 26);
    }

    public void start()
    {
        if (!getPauseDuringPreparing())
            super.start();
    }

    public boolean surfaceTextureDeleted()
    {
        if (mSurfaceTexture == null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.HTML5VideoInline
 * JD-Core Version:        0.6.2
 */