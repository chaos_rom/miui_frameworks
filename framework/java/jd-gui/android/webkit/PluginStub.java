package android.webkit;

import android.content.Context;
import android.view.View;

public abstract interface PluginStub
{
    public abstract View getEmbeddedView(int paramInt, Context paramContext);

    public abstract View getFullScreenView(int paramInt, Context paramContext);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.PluginStub
 * JD-Core Version:        0.6.2
 */