package android.webkit;

import android.util.EventLog;

public class EventLogTags
{
    public static final int BROWSER_DOUBLE_TAP_DURATION = 70102;
    public static final int BROWSER_SNAP_CENTER = 70150;
    public static final int BROWSER_TEXT_SIZE_CHANGE = 70151;
    public static final int BROWSER_ZOOM_LEVEL_CHANGE = 70101;

    public static void writeBrowserDoubleTapDuration(int paramInt, long paramLong)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = Long.valueOf(paramLong);
        EventLog.writeEvent(70102, arrayOfObject);
    }

    public static void writeBrowserSnapCenter()
    {
        EventLog.writeEvent(70150, new Object[0]);
    }

    public static void writeBrowserTextSizeChange(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(70151, arrayOfObject);
    }

    public static void writeBrowserZoomLevelChange(int paramInt1, int paramInt2, long paramLong)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Long.valueOf(paramLong);
        EventLog.writeEvent(70101, arrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.webkit.EventLogTags
 * JD-Core Version:        0.6.2
 */