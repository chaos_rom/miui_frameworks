package android.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target({java.lang.annotation.ElementType.FIELD})
public @interface SdkConstant
{
    public abstract SdkConstantType value();

    public static enum SdkConstantType
    {
        static
        {
            INTENT_CATEGORY = new SdkConstantType("INTENT_CATEGORY", 3);
            FEATURE = new SdkConstantType("FEATURE", 4);
            SdkConstantType[] arrayOfSdkConstantType = new SdkConstantType[5];
            arrayOfSdkConstantType[0] = ACTIVITY_INTENT_ACTION;
            arrayOfSdkConstantType[1] = BROADCAST_INTENT_ACTION;
            arrayOfSdkConstantType[2] = SERVICE_ACTION;
            arrayOfSdkConstantType[3] = INTENT_CATEGORY;
            arrayOfSdkConstantType[4] = FEATURE;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.annotation.SdkConstant
 * JD-Core Version:        0.6.2
 */