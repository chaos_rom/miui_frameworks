package android.emoji;

import android.graphics.Bitmap;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class EmojiFactory
{
    private Map<Integer, WeakReference<Bitmap>> mCache;
    private String mName;
    private int mNativeEmojiFactory;
    private int sCacheSize = 100;

    private EmojiFactory(int paramInt, String paramString)
    {
        this.mNativeEmojiFactory = paramInt;
        this.mName = paramString;
        this.mCache = new CustomLinkedHashMap();
    }

    private native void nativeDestructor(int paramInt);

    private native int nativeGetAndroidPuaFromVendorSpecificPua(int paramInt1, int paramInt2);

    private native int nativeGetAndroidPuaFromVendorSpecificSjis(int paramInt, char paramChar);

    private native Bitmap nativeGetBitmapFromAndroidPua(int paramInt1, int paramInt2);

    private native int nativeGetMaximumAndroidPua(int paramInt);

    private native int nativeGetMaximumVendorSpecificPua(int paramInt);

    private native int nativeGetMinimumAndroidPua(int paramInt);

    private native int nativeGetMinimumVendorSpecificPua(int paramInt);

    private native int nativeGetVendorSpecificPuaFromAndroidPua(int paramInt1, int paramInt2);

    private native int nativeGetVendorSpecificSjisFromAndroidPua(int paramInt1, int paramInt2);

    public static native EmojiFactory newAvailableInstance();

    public static native EmojiFactory newInstance(String paramString);

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestructor(this.mNativeEmojiFactory);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getAndroidPuaFromVendorSpecificPua(int paramInt)
    {
        return nativeGetAndroidPuaFromVendorSpecificPua(this.mNativeEmojiFactory, paramInt);
    }

    public String getAndroidPuaFromVendorSpecificPua(String paramString)
    {
        if (paramString == null);
        int[] arrayOfInt;
        int m;
        for (String str = null; ; str = new String(arrayOfInt, 0, m))
        {
            return str;
            int i = nativeGetMinimumVendorSpecificPua(this.mNativeEmojiFactory);
            int j = nativeGetMaximumVendorSpecificPua(this.mNativeEmojiFactory);
            int k = paramString.length();
            arrayOfInt = new int[paramString.codePointCount(0, k)];
            m = 0;
            int n = 0;
            if (n < k)
            {
                int i1 = paramString.codePointAt(n);
                if ((i <= i1) && (i1 <= j))
                {
                    int i2 = getAndroidPuaFromVendorSpecificPua(i1);
                    if (i2 > 0)
                        arrayOfInt[m] = i2;
                }
                while (true)
                {
                    n = paramString.offsetByCodePoints(n, 1);
                    m++;
                    break;
                    arrayOfInt[m] = i1;
                }
            }
        }
    }

    public int getAndroidPuaFromVendorSpecificSjis(char paramChar)
    {
        return nativeGetAndroidPuaFromVendorSpecificSjis(this.mNativeEmojiFactory, paramChar);
    }

    /** @deprecated */
    public Bitmap getBitmapFromAndroidPua(int paramInt)
    {
        while (true)
        {
            Bitmap localBitmap;
            try
            {
                WeakReference localWeakReference = (WeakReference)this.mCache.get(Integer.valueOf(paramInt));
                if (localWeakReference == null)
                {
                    localObject2 = nativeGetBitmapFromAndroidPua(this.mNativeEmojiFactory, paramInt);
                    if (localObject2 != null)
                        this.mCache.put(Integer.valueOf(paramInt), new WeakReference(localObject2));
                    return localObject2;
                }
                localBitmap = (Bitmap)localWeakReference.get();
                if (localBitmap == null)
                {
                    localObject2 = nativeGetBitmapFromAndroidPua(this.mNativeEmojiFactory, paramInt);
                    this.mCache.put(Integer.valueOf(paramInt), new WeakReference(localObject2));
                    continue;
                }
            }
            finally
            {
            }
            Object localObject2 = localBitmap;
        }
    }

    /** @deprecated */
    public Bitmap getBitmapFromVendorSpecificPua(int paramInt)
    {
        try
        {
            Bitmap localBitmap = getBitmapFromAndroidPua(getAndroidPuaFromVendorSpecificPua(paramInt));
            return localBitmap;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public Bitmap getBitmapFromVendorSpecificSjis(char paramChar)
    {
        try
        {
            Bitmap localBitmap = getBitmapFromAndroidPua(getAndroidPuaFromVendorSpecificSjis(paramChar));
            return localBitmap;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getMaximumAndroidPua()
    {
        return nativeGetMaximumAndroidPua(this.mNativeEmojiFactory);
    }

    public int getMinimumAndroidPua()
    {
        return nativeGetMinimumAndroidPua(this.mNativeEmojiFactory);
    }

    public int getVendorSpecificPuaFromAndroidPua(int paramInt)
    {
        return nativeGetVendorSpecificPuaFromAndroidPua(this.mNativeEmojiFactory, paramInt);
    }

    public String getVendorSpecificPuaFromAndroidPua(String paramString)
    {
        if (paramString == null);
        int[] arrayOfInt;
        int m;
        for (String str = null; ; str = new String(arrayOfInt, 0, m))
        {
            return str;
            int i = nativeGetMinimumAndroidPua(this.mNativeEmojiFactory);
            int j = nativeGetMaximumAndroidPua(this.mNativeEmojiFactory);
            int k = paramString.length();
            arrayOfInt = new int[paramString.codePointCount(0, k)];
            m = 0;
            int n = 0;
            if (n < k)
            {
                int i1 = paramString.codePointAt(n);
                if ((i <= i1) && (i1 <= j))
                {
                    int i2 = getVendorSpecificPuaFromAndroidPua(i1);
                    if (i2 > 0)
                        arrayOfInt[m] = i2;
                }
                while (true)
                {
                    n = paramString.offsetByCodePoints(n, 1);
                    m++;
                    break;
                    arrayOfInt[m] = i1;
                }
            }
        }
    }

    public int getVendorSpecificSjisFromAndroidPua(int paramInt)
    {
        return nativeGetVendorSpecificSjisFromAndroidPua(this.mNativeEmojiFactory, paramInt);
    }

    public String name()
    {
        return this.mName;
    }

    private class CustomLinkedHashMap<K, V> extends LinkedHashMap<K, V>
    {
        public CustomLinkedHashMap()
        {
            super(0.75F, true);
        }

        protected boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
        {
            if (size() > EmojiFactory.this.sCacheSize);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.emoji.EmojiFactory
 * JD-Core Version:        0.6.2
 */