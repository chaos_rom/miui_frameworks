package android.media;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

public class AudioRecord
{
    private static final int AUDIORECORD_ERROR_SETUP_INVALIDCHANNELMASK = -17;
    private static final int AUDIORECORD_ERROR_SETUP_INVALIDFORMAT = -18;
    private static final int AUDIORECORD_ERROR_SETUP_INVALIDSOURCE = -19;
    private static final int AUDIORECORD_ERROR_SETUP_NATIVEINITFAILED = -20;
    private static final int AUDIORECORD_ERROR_SETUP_ZEROFRAMECOUNT = -16;
    public static final int ERROR = -1;
    public static final int ERROR_BAD_VALUE = -2;
    public static final int ERROR_INVALID_OPERATION = -3;
    private static final int NATIVE_EVENT_MARKER = 2;
    private static final int NATIVE_EVENT_NEW_POS = 3;
    public static final int RECORDSTATE_RECORDING = 3;
    public static final int RECORDSTATE_STOPPED = 1;
    public static final int STATE_INITIALIZED = 1;
    public static final int STATE_UNINITIALIZED = 0;
    public static final int SUCCESS = 0;
    private static final String TAG = "AudioRecord-Java";
    private int mAudioFormat = 2;
    private int mChannelConfiguration = 16;
    private int mChannelCount = 1;
    private int mChannels = 16;
    private NativeEventHandler mEventHandler = null;
    private Looper mInitializationLooper = null;
    private int mNativeBufferSizeInBytes = 0;
    private int mNativeCallbackCookie;
    private int mNativeRecorderInJavaObj;
    private OnRecordPositionUpdateListener mPositionListener = null;
    private final Object mPositionListenerLock = new Object();
    private int mRecordSource = 0;
    private int mRecordingState = 1;
    private final Object mRecordingStateLock = new Object();
    private int mSampleRate = 22050;
    private int mSessionId = 0;
    private int mState = 0;

    public AudioRecord(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        throws IllegalArgumentException
    {
        Looper localLooper = Looper.myLooper();
        this.mInitializationLooper = localLooper;
        if (localLooper == null)
            this.mInitializationLooper = Looper.getMainLooper();
        audioParamCheck(paramInt1, paramInt2, paramInt3, paramInt4);
        audioBuffSizeCheck(paramInt5);
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 0;
        int i = native_setup(new WeakReference(this), this.mRecordSource, this.mSampleRate, this.mChannels, this.mAudioFormat, this.mNativeBufferSizeInBytes, arrayOfInt);
        if (i != 0)
            loge("Error code " + i + " when initializing native AudioRecord object.");
        while (true)
        {
            return;
            this.mSessionId = arrayOfInt[0];
            this.mState = 1;
        }
    }

    private void audioBuffSizeCheck(int paramInt)
    {
        int i = this.mChannelCount;
        if (this.mAudioFormat == 3);
        for (int j = 1; (paramInt % (i * j) != 0) || (paramInt < 1); j = 2)
            throw new IllegalArgumentException("Invalid audio buffer size.");
        this.mNativeBufferSizeInBytes = paramInt;
    }

    private void audioParamCheck(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramInt1 < 0) || (paramInt1 > MediaRecorder.getAudioSourceMax()))
            throw new IllegalArgumentException("Invalid audio source.");
        this.mRecordSource = paramInt1;
        if ((paramInt2 < 4000) || (paramInt2 > 48000))
            throw new IllegalArgumentException(paramInt2 + "Hz is not a supported sample rate.");
        this.mSampleRate = paramInt2;
        this.mChannelConfiguration = paramInt3;
        switch (paramInt3)
        {
        default:
            this.mChannelCount = 0;
            this.mChannels = 0;
            this.mChannelConfiguration = 0;
            throw new IllegalArgumentException("Unsupported channel configuration.");
        case 1:
        case 2:
        case 16:
            this.mChannelCount = 1;
        case 3:
        case 12:
        }
        for (this.mChannels = 16; ; this.mChannels = 12)
            switch (paramInt4)
            {
            default:
                this.mAudioFormat = 0;
                throw new IllegalArgumentException("Unsupported sample encoding. Should be ENCODING_PCM_8BIT or ENCODING_PCM_16BIT.");
                this.mChannelCount = 2;
            case 1:
            case 2:
            case 3:
            }
        for (this.mAudioFormat = 2; ; this.mAudioFormat = paramInt4)
            return;
    }

    public static int getMinBufferSize(int paramInt1, int paramInt2, int paramInt3)
    {
        int j;
        switch (paramInt2)
        {
        default:
            loge("getMinBufferSize(): Invalid channel configuration.");
            j = -2;
        case 1:
        case 2:
        case 16:
        case 3:
        case 12:
        }
        while (true)
        {
            return j;
            for (int i = 1; ; i = 2)
            {
                if (paramInt3 == 2)
                    break label88;
                loge("getMinBufferSize(): Invalid audio format.");
                j = -2;
                break;
            }
            label88: j = native_get_min_buff_size(paramInt1, i, paramInt3);
            if (j == 0)
                j = -2;
            else if (j == -1)
                j = -1;
        }
    }

    private static void logd(String paramString)
    {
        Log.d("AudioRecord-Java", "[ android.media.AudioRecord ] " + paramString);
    }

    private static void loge(String paramString)
    {
        Log.e("AudioRecord-Java", "[ android.media.AudioRecord ] " + paramString);
    }

    private final native void native_finalize();

    private final native int native_get_marker_pos();

    private static final native int native_get_min_buff_size(int paramInt1, int paramInt2, int paramInt3);

    private final native int native_get_pos_update_period();

    private final native int native_read_in_byte_array(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    private final native int native_read_in_direct_buffer(Object paramObject, int paramInt);

    private final native int native_read_in_short_array(short[] paramArrayOfShort, int paramInt1, int paramInt2);

    private final native void native_release();

    private final native int native_set_marker_pos(int paramInt);

    private final native int native_set_pos_update_period(int paramInt);

    private final native int native_setup(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int[] paramArrayOfInt);

    private final native int native_start(int paramInt1, int paramInt2);

    private final native void native_stop();

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        AudioRecord localAudioRecord = (AudioRecord)((WeakReference)paramObject1).get();
        if (localAudioRecord == null);
        while (true)
        {
            return;
            if (localAudioRecord.mEventHandler != null)
            {
                Message localMessage = localAudioRecord.mEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localAudioRecord.mEventHandler.sendMessage(localMessage);
            }
        }
    }

    protected void finalize()
    {
        native_finalize();
    }

    public int getAudioFormat()
    {
        return this.mAudioFormat;
    }

    public int getAudioSessionId()
    {
        return this.mSessionId;
    }

    public int getAudioSource()
    {
        return this.mRecordSource;
    }

    public int getChannelConfiguration()
    {
        return this.mChannelConfiguration;
    }

    public int getChannelCount()
    {
        return this.mChannelCount;
    }

    public int getNotificationMarkerPosition()
    {
        return native_get_marker_pos();
    }

    public int getPositionNotificationPeriod()
    {
        return native_get_pos_update_period();
    }

    public int getRecordingState()
    {
        return this.mRecordingState;
    }

    public int getSampleRate()
    {
        return this.mSampleRate;
    }

    public int getState()
    {
        return this.mState;
    }

    public int read(ByteBuffer paramByteBuffer, int paramInt)
    {
        int i;
        if (this.mState != 1)
            i = -3;
        while (true)
        {
            return i;
            if ((paramByteBuffer == null) || (paramInt < 0))
                i = -2;
            else
                i = native_read_in_direct_buffer(paramByteBuffer, paramInt);
        }
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        int i;
        if (this.mState != 1)
            i = -3;
        while (true)
        {
            return i;
            if ((paramArrayOfByte == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length))
                i = -2;
            else
                i = native_read_in_byte_array(paramArrayOfByte, paramInt1, paramInt2);
        }
    }

    public int read(short[] paramArrayOfShort, int paramInt1, int paramInt2)
    {
        int i;
        if (this.mState != 1)
            i = -3;
        while (true)
        {
            return i;
            if ((paramArrayOfShort == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfShort.length))
                i = -2;
            else
                i = native_read_in_short_array(paramArrayOfShort, paramInt1, paramInt2);
        }
    }

    public void release()
    {
        try
        {
            stop();
            label4: native_release();
            this.mState = 0;
            return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            break label4;
        }
    }

    public int setNotificationMarkerPosition(int paramInt)
    {
        return native_set_marker_pos(paramInt);
    }

    public int setPositionNotificationPeriod(int paramInt)
    {
        return native_set_pos_update_period(paramInt);
    }

    public void setRecordPositionUpdateListener(OnRecordPositionUpdateListener paramOnRecordPositionUpdateListener)
    {
        setRecordPositionUpdateListener(paramOnRecordPositionUpdateListener, null);
    }

    // ERROR //
    public void setRecordPositionUpdateListener(OnRecordPositionUpdateListener paramOnRecordPositionUpdateListener, Handler paramHandler)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 92	android/media/AudioRecord:mPositionListenerLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: aload_1
        //     9: putfield 90	android/media/AudioRecord:mPositionListener	Landroid/media/AudioRecord$OnRecordPositionUpdateListener;
        //     12: aload_1
        //     13: ifnull +54 -> 67
        //     16: aload_2
        //     17: ifnull +23 -> 40
        //     20: aload_0
        //     21: new 6	android/media/AudioRecord$NativeEventHandler
        //     24: dup
        //     25: aload_0
        //     26: aload_0
        //     27: aload_2
        //     28: invokevirtual 271	android/os/Handler:getLooper	()Landroid/os/Looper;
        //     31: invokespecial 274	android/media/AudioRecord$NativeEventHandler:<init>	(Landroid/media/AudioRecord;Landroid/media/AudioRecord;Landroid/os/Looper;)V
        //     34: putfield 94	android/media/AudioRecord:mEventHandler	Landroid/media/AudioRecord$NativeEventHandler;
        //     37: aload_3
        //     38: monitorexit
        //     39: return
        //     40: aload_0
        //     41: new 6	android/media/AudioRecord$NativeEventHandler
        //     44: dup
        //     45: aload_0
        //     46: aload_0
        //     47: aload_0
        //     48: getfield 96	android/media/AudioRecord:mInitializationLooper	Landroid/os/Looper;
        //     51: invokespecial 274	android/media/AudioRecord$NativeEventHandler:<init>	(Landroid/media/AudioRecord;Landroid/media/AudioRecord;Landroid/os/Looper;)V
        //     54: putfield 94	android/media/AudioRecord:mEventHandler	Landroid/media/AudioRecord$NativeEventHandler;
        //     57: goto -20 -> 37
        //     60: astore 4
        //     62: aload_3
        //     63: monitorexit
        //     64: aload 4
        //     66: athrow
        //     67: aload_0
        //     68: aconst_null
        //     69: putfield 94	android/media/AudioRecord:mEventHandler	Landroid/media/AudioRecord$NativeEventHandler;
        //     72: goto -35 -> 37
        //
        // Exception table:
        //     from	to	target	type
        //     7	64	60	finally
        //     67	72	60	finally
    }

    public void startRecording()
        throws IllegalStateException
    {
        if (this.mState != 1)
            throw new IllegalStateException("startRecording() called on an uninitialized AudioRecord.");
        synchronized (this.mRecordingStateLock)
        {
            if (native_start(0, 0) == 0)
                this.mRecordingState = 3;
            return;
        }
    }

    public void startRecording(MediaSyncEvent paramMediaSyncEvent)
        throws IllegalStateException
    {
        if (this.mState != 1)
            throw new IllegalStateException("startRecording() called on an uninitialized AudioRecord.");
        synchronized (this.mRecordingStateLock)
        {
            if (native_start(paramMediaSyncEvent.getType(), paramMediaSyncEvent.getAudioSessionId()) == 0)
                this.mRecordingState = 3;
            return;
        }
    }

    public void stop()
        throws IllegalStateException
    {
        if (this.mState != 1)
            throw new IllegalStateException("stop() called on an uninitialized AudioRecord.");
        synchronized (this.mRecordingStateLock)
        {
            native_stop();
            this.mRecordingState = 1;
            return;
        }
    }

    private class NativeEventHandler extends Handler
    {
        private final AudioRecord mAudioRecord;

        NativeEventHandler(AudioRecord paramLooper, Looper arg3)
        {
            super();
            this.mAudioRecord = paramLooper;
        }

        public void handleMessage(Message paramMessage)
        {
            while (true)
            {
                AudioRecord.OnRecordPositionUpdateListener localOnRecordPositionUpdateListener;
                synchronized (AudioRecord.this.mPositionListenerLock)
                {
                    localOnRecordPositionUpdateListener = this.mAudioRecord.mPositionListener;
                    switch (paramMessage.what)
                    {
                    default:
                        Log.e("AudioRecord-Java", "[ android.media.AudioRecord.NativeEventHandler ] Unknown event type: " + paramMessage.what);
                        return;
                    case 2:
                    case 3:
                    }
                }
                if (localOnRecordPositionUpdateListener != null)
                {
                    localOnRecordPositionUpdateListener.onMarkerReached(this.mAudioRecord);
                    continue;
                    if (localOnRecordPositionUpdateListener != null)
                        localOnRecordPositionUpdateListener.onPeriodicNotification(this.mAudioRecord);
                }
            }
        }
    }

    public static abstract interface OnRecordPositionUpdateListener
    {
        public abstract void onMarkerReached(AudioRecord paramAudioRecord);

        public abstract void onPeriodicNotification(AudioRecord paramAudioRecord);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AudioRecord
 * JD-Core Version:        0.6.2
 */