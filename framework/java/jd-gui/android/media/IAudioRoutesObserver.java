package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IAudioRoutesObserver extends IInterface
{
    public abstract void dispatchAudioRoutesChanged(AudioRoutesInfo paramAudioRoutesInfo)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAudioRoutesObserver
    {
        private static final String DESCRIPTOR = "android.media.IAudioRoutesObserver";
        static final int TRANSACTION_dispatchAudioRoutesChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.media.IAudioRoutesObserver");
        }

        public static IAudioRoutesObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IAudioRoutesObserver");
                if ((localIInterface != null) && ((localIInterface instanceof IAudioRoutesObserver)))
                    localObject = (IAudioRoutesObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.media.IAudioRoutesObserver");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.media.IAudioRoutesObserver");
            if (paramParcel1.readInt() != 0);
            for (AudioRoutesInfo localAudioRoutesInfo = (AudioRoutesInfo)AudioRoutesInfo.CREATOR.createFromParcel(paramParcel1); ; localAudioRoutesInfo = null)
            {
                dispatchAudioRoutesChanged(localAudioRoutesInfo);
                break;
            }
        }

        private static class Proxy
            implements IAudioRoutesObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dispatchAudioRoutesChanged(AudioRoutesInfo paramAudioRoutesInfo)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioRoutesObserver");
                    if (paramAudioRoutesInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramAudioRoutesInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IAudioRoutesObserver";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IAudioRoutesObserver
 * JD-Core Version:        0.6.2
 */