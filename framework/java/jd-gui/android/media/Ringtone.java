package android.media;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public class Ringtone
{
    private static final String[] DRM_COLUMNS = arrayOfString2;
    private static final boolean LOGD = true;
    private static final String[] MEDIA_COLUMNS;
    private static final String TAG = "Ringtone";
    private final boolean mAllowRemote;
    private final AudioManager mAudioManager;
    private final Context mContext;
    private MediaPlayer mLocalPlayer;
    private final IRingtonePlayer mRemotePlayer;
    private final Binder mRemoteToken;
    private int mStreamType = 2;
    private String mTitle;
    private Uri mUri;

    static
    {
        String[] arrayOfString1 = new String[3];
        arrayOfString1[0] = "_id";
        arrayOfString1[1] = "_data";
        arrayOfString1[2] = "title";
        MEDIA_COLUMNS = arrayOfString1;
        String[] arrayOfString2 = new String[3];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "_data";
        arrayOfString2[2] = "title";
    }

    public Ringtone(Context paramContext, boolean paramBoolean)
    {
        this.mContext = paramContext;
        this.mAudioManager = ((AudioManager)this.mContext.getSystemService("audio"));
        this.mAllowRemote = paramBoolean;
        if (paramBoolean);
        for (IRingtonePlayer localIRingtonePlayer = this.mAudioManager.getRingtonePlayer(); ; localIRingtonePlayer = null)
        {
            this.mRemotePlayer = localIRingtonePlayer;
            if (paramBoolean)
                localBinder = new Binder();
            this.mRemoteToken = localBinder;
            return;
        }
    }

    private void destroyLocalPlayer()
    {
        if (this.mLocalPlayer != null)
        {
            this.mLocalPlayer.reset();
            this.mLocalPlayer.release();
            this.mLocalPlayer = null;
        }
    }

    private static String getTitle(Context paramContext, Uri paramUri, boolean paramBoolean)
    {
        Object localObject1 = null;
        ContentResolver localContentResolver = paramContext.getContentResolver();
        Object localObject2 = null;
        String str1;
        if (paramUri != null)
        {
            str1 = paramUri.getAuthority();
            if (!"settings".equals(str1))
                break label101;
            if (paramBoolean)
            {
                String str4 = getTitle(paramContext, RingtoneManager.getActualDefaultRingtoneUri(paramContext, RingtoneManager.getDefaultType(paramUri)), false);
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = str4;
                localObject2 = paramContext.getString(17040383, arrayOfObject);
            }
        }
        while (true)
        {
            if (localObject2 == null)
            {
                localObject2 = paramContext.getString(17040386);
                if (localObject2 == null)
                    localObject2 = "";
            }
            Object localObject3 = localObject2;
            label98: return localObject3;
            try
            {
                label101: if ("drm".equals(str1))
                {
                    Cursor localCursor2 = localContentResolver.query(paramUri, DRM_COLUMNS, null, null, null);
                    localObject1 = localCursor2;
                    if (localObject1 == null)
                        break label205;
                }
                try
                {
                    label128: if (localObject1.getCount() == 1)
                    {
                        localObject1.moveToFirst();
                        String str3 = localObject1.getString(2);
                        localObject3 = str3;
                        if (localObject1 == null)
                            break label98;
                        localObject1.close();
                        break label98;
                        if (!"media".equals(str1))
                            break label128;
                        Cursor localCursor1 = localContentResolver.query(paramUri, MEDIA_COLUMNS, null, null, null);
                        localObject1 = localCursor1;
                        break label128;
                    }
                    label205: String str2 = paramUri.getLastPathSegment();
                    localObject2 = str2;
                    if (localObject1 == null)
                        continue;
                    localObject1.close();
                }
                finally
                {
                    if (localObject1 != null)
                        localObject1.close();
                }
            }
            catch (SecurityException localSecurityException)
            {
                break label128;
            }
        }
    }

    public int getStreamType()
    {
        return this.mStreamType;
    }

    public String getTitle(Context paramContext)
    {
        String str;
        if (this.mTitle != null)
            str = this.mTitle;
        while (true)
        {
            return str;
            str = getTitle(paramContext, this.mUri, true);
            this.mTitle = str;
        }
    }

    public Uri getUri()
    {
        return this.mUri;
    }

    public boolean isPlaying()
    {
        boolean bool1 = false;
        if (this.mLocalPlayer != null)
            bool1 = this.mLocalPlayer.isPlaying();
        while (true)
        {
            return bool1;
            if (this.mAllowRemote)
                try
                {
                    boolean bool2 = this.mRemotePlayer.isPlaying(this.mRemoteToken);
                    bool1 = bool2;
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("Ringtone", "Problem checking ringtone: " + localRemoteException);
                }
            else
                Log.w("Ringtone", "Neither local nor remote playback available");
        }
    }

    public void play()
    {
        if (this.mLocalPlayer != null)
            if (this.mAudioManager.getStreamVolume(this.mStreamType) != 0)
                this.mLocalPlayer.start();
        while (true)
        {
            return;
            if (this.mAllowRemote)
                try
                {
                    this.mRemotePlayer.play(this.mRemoteToken, this.mUri, this.mStreamType);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("Ringtone", "Problem playing ringtone: " + localRemoteException);
                }
            else
                Log.w("Ringtone", "Neither local nor remote playback available");
        }
    }

    public void setStreamType(int paramInt)
    {
        this.mStreamType = paramInt;
        setUri(this.mUri);
    }

    void setTitle(String paramString)
    {
        this.mTitle = paramString;
    }

    public void setUri(Uri paramUri)
    {
        destroyLocalPlayer();
        this.mUri = paramUri;
        if (this.mUri == null);
        while (true)
        {
            return;
            this.mLocalPlayer = new MediaPlayer();
            try
            {
                this.mLocalPlayer.setDataSource(this.mContext, this.mUri);
                this.mLocalPlayer.setAudioStreamType(this.mStreamType);
                this.mLocalPlayer.prepare();
                if (this.mLocalPlayer != null)
                    Log.d("Ringtone", "Successfully created local player");
            }
            catch (SecurityException localSecurityException)
            {
                while (true)
                {
                    destroyLocalPlayer();
                    if (!this.mAllowRemote)
                        Log.w("Ringtone", "Remote playback not allowed: " + localSecurityException);
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    destroyLocalPlayer();
                    if (!this.mAllowRemote)
                        Log.w("Ringtone", "Remote playback not allowed: " + localIOException);
                }
                Log.d("Ringtone", "Problem opening; delegating to remote player");
            }
        }
    }

    public void stop()
    {
        if (this.mLocalPlayer != null)
            destroyLocalPlayer();
        while (true)
        {
            return;
            if (this.mAllowRemote)
                try
                {
                    this.mRemotePlayer.stop(this.mRemoteToken);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("Ringtone", "Problem stopping ringtone: " + localRemoteException);
                }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.Ringtone
 * JD-Core Version:        0.6.2
 */