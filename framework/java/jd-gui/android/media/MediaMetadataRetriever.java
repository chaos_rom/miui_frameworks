package android.media;

import android.graphics.Bitmap;
import java.io.FileDescriptor;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MediaMetadataRetriever
{
    private static final int EMBEDDED_PICTURE_TYPE_ANY = 65535;
    public static final int METADATA_KEY_ALBUM = 1;
    public static final int METADATA_KEY_ALBUMARTIST = 13;
    public static final int METADATA_KEY_ARTIST = 2;
    public static final int METADATA_KEY_AUTHOR = 3;
    public static final int METADATA_KEY_BITRATE = 20;
    public static final int METADATA_KEY_CD_TRACK_NUMBER = 0;
    public static final int METADATA_KEY_COMPILATION = 15;
    public static final int METADATA_KEY_COMPOSER = 4;
    public static final int METADATA_KEY_DATE = 5;
    public static final int METADATA_KEY_DISC_NUMBER = 14;
    public static final int METADATA_KEY_DURATION = 9;
    public static final int METADATA_KEY_GENRE = 6;
    public static final int METADATA_KEY_HAS_AUDIO = 16;
    public static final int METADATA_KEY_HAS_VIDEO = 17;
    public static final int METADATA_KEY_IS_DRM = 22;
    public static final int METADATA_KEY_LOCATION = 23;
    public static final int METADATA_KEY_MIMETYPE = 12;
    public static final int METADATA_KEY_NUM_TRACKS = 10;
    public static final int METADATA_KEY_TIMED_TEXT_LANGUAGES = 21;
    public static final int METADATA_KEY_TITLE = 7;
    public static final int METADATA_KEY_VIDEO_HEIGHT = 19;
    public static final int METADATA_KEY_VIDEO_WIDTH = 18;
    public static final int METADATA_KEY_WRITER = 11;
    public static final int METADATA_KEY_YEAR = 8;
    public static final int OPTION_CLOSEST = 3;
    public static final int OPTION_CLOSEST_SYNC = 2;
    public static final int OPTION_NEXT_SYNC = 1;
    public static final int OPTION_PREVIOUS_SYNC;
    private int mNativeContext;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    public MediaMetadataRetriever()
    {
        native_setup();
    }

    private native Bitmap _getFrameAtTime(long paramLong, int paramInt);

    private native void _setDataSource(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
        throws IllegalArgumentException;

    private native byte[] getEmbeddedPicture(int paramInt);

    private final native void native_finalize();

    private static native void native_init();

    private native void native_setup();

    public native String extractMetadata(int paramInt);

    protected void finalize()
        throws Throwable
    {
        try
        {
            native_finalize();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public byte[] getEmbeddedPicture()
    {
        return getEmbeddedPicture(65535);
    }

    public Bitmap getFrameAtTime()
    {
        return getFrameAtTime(-1L, 2);
    }

    public Bitmap getFrameAtTime(long paramLong)
    {
        return getFrameAtTime(paramLong, 2);
    }

    public Bitmap getFrameAtTime(long paramLong, int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("Unsupported option: " + paramInt);
        return _getFrameAtTime(paramLong, paramInt);
    }

    public native void release();

    // ERROR //
    public void setDataSource(android.content.Context paramContext, android.net.Uri paramUri)
        throws IllegalArgumentException, java.lang.SecurityException
    {
        // Byte code:
        //     0: aload_2
        //     1: ifnonnull +11 -> 12
        //     4: new 85	java/lang/IllegalArgumentException
        //     7: dup
        //     8: invokespecial 137	java/lang/IllegalArgumentException:<init>	()V
        //     11: athrow
        //     12: aload_2
        //     13: invokevirtual 142	android/net/Uri:getScheme	()Ljava/lang/String;
        //     16: astore_3
        //     17: aload_3
        //     18: ifnull +12 -> 30
        //     21: aload_3
        //     22: ldc 144
        //     24: invokevirtual 150	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     27: ifeq +12 -> 39
        //     30: aload_0
        //     31: aload_2
        //     32: invokevirtual 153	android/net/Uri:getPath	()Ljava/lang/String;
        //     35: invokevirtual 155	android/media/MediaMetadataRetriever:setDataSource	(Ljava/lang/String;)V
        //     38: return
        //     39: aconst_null
        //     40: astore 4
        //     42: aload_1
        //     43: invokevirtual 161	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     46: astore 9
        //     48: aload 9
        //     50: aload_2
        //     51: ldc 163
        //     53: invokevirtual 169	android/content/ContentResolver:openAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
        //     56: astore 11
        //     58: aload 11
        //     60: astore 4
        //     62: aload 4
        //     64: ifnonnull +59 -> 123
        //     67: new 85	java/lang/IllegalArgumentException
        //     70: dup
        //     71: invokespecial 137	java/lang/IllegalArgumentException:<init>	()V
        //     74: athrow
        //     75: astore 7
        //     77: aload 4
        //     79: ifnull +8 -> 87
        //     82: aload 4
        //     84: invokevirtual 174	android/content/res/AssetFileDescriptor:close	()V
        //     87: aload_0
        //     88: aload_2
        //     89: invokevirtual 175	android/net/Uri:toString	()Ljava/lang/String;
        //     92: invokevirtual 155	android/media/MediaMetadataRetriever:setDataSource	(Ljava/lang/String;)V
        //     95: goto -57 -> 38
        //     98: astore 10
        //     100: new 85	java/lang/IllegalArgumentException
        //     103: dup
        //     104: invokespecial 137	java/lang/IllegalArgumentException:<init>	()V
        //     107: athrow
        //     108: astore 5
        //     110: aload 4
        //     112: ifnull +8 -> 120
        //     115: aload 4
        //     117: invokevirtual 174	android/content/res/AssetFileDescriptor:close	()V
        //     120: aload 5
        //     122: athrow
        //     123: aload 4
        //     125: invokevirtual 179	android/content/res/AssetFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     128: astore 12
        //     130: aload 12
        //     132: invokevirtual 185	java/io/FileDescriptor:valid	()Z
        //     135: ifne +11 -> 146
        //     138: new 85	java/lang/IllegalArgumentException
        //     141: dup
        //     142: invokespecial 137	java/lang/IllegalArgumentException:<init>	()V
        //     145: athrow
        //     146: aload 4
        //     148: invokevirtual 189	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     151: lconst_0
        //     152: lcmp
        //     153: ifge +27 -> 180
        //     156: aload_0
        //     157: aload 12
        //     159: invokevirtual 192	android/media/MediaMetadataRetriever:setDataSource	(Ljava/io/FileDescriptor;)V
        //     162: aload 4
        //     164: ifnull -126 -> 38
        //     167: aload 4
        //     169: invokevirtual 174	android/content/res/AssetFileDescriptor:close	()V
        //     172: goto -134 -> 38
        //     175: astore 13
        //     177: goto -139 -> 38
        //     180: aload_0
        //     181: aload 12
        //     183: aload 4
        //     185: invokevirtual 195	android/content/res/AssetFileDescriptor:getStartOffset	()J
        //     188: aload 4
        //     190: invokevirtual 189	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     193: invokevirtual 198	android/media/MediaMetadataRetriever:setDataSource	(Ljava/io/FileDescriptor;JJ)V
        //     196: goto -34 -> 162
        //     199: astore 8
        //     201: goto -114 -> 87
        //     204: astore 6
        //     206: goto -86 -> 120
        //
        // Exception table:
        //     from	to	target	type
        //     42	48	75	java/lang/SecurityException
        //     48	58	75	java/lang/SecurityException
        //     67	75	75	java/lang/SecurityException
        //     100	108	75	java/lang/SecurityException
        //     123	162	75	java/lang/SecurityException
        //     180	196	75	java/lang/SecurityException
        //     48	58	98	java/io/FileNotFoundException
        //     42	48	108	finally
        //     48	58	108	finally
        //     67	75	108	finally
        //     100	108	108	finally
        //     123	162	108	finally
        //     180	196	108	finally
        //     167	172	175	java/io/IOException
        //     82	87	199	java/io/IOException
        //     115	120	204	java/io/IOException
    }

    public void setDataSource(FileDescriptor paramFileDescriptor)
        throws IllegalArgumentException
    {
        setDataSource(paramFileDescriptor, 0L, 576460752303423487L);
    }

    public native void setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2)
        throws IllegalArgumentException;

    // ERROR //
    public void setDataSource(String paramString)
        throws IllegalArgumentException
    {
        // Byte code:
        //     0: new 204	java/io/FileInputStream
        //     3: dup
        //     4: aload_1
        //     5: invokespecial 205	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     8: astore_2
        //     9: aload_0
        //     10: aload_2
        //     11: invokevirtual 208	java/io/FileInputStream:getFD	()Ljava/io/FileDescriptor;
        //     14: lconst_0
        //     15: ldc2_w 199
        //     18: invokevirtual 198	android/media/MediaMetadataRetriever:setDataSource	(Ljava/io/FileDescriptor;JJ)V
        //     21: aload_2
        //     22: ifnull +7 -> 29
        //     25: aload_2
        //     26: invokevirtual 209	java/io/FileInputStream:close	()V
        //     29: return
        //     30: astore 7
        //     32: new 85	java/lang/IllegalArgumentException
        //     35: dup
        //     36: invokespecial 137	java/lang/IllegalArgumentException:<init>	()V
        //     39: athrow
        //     40: astore 6
        //     42: new 85	java/lang/IllegalArgumentException
        //     45: dup
        //     46: invokespecial 137	java/lang/IllegalArgumentException:<init>	()V
        //     49: athrow
        //     50: astore 5
        //     52: goto -23 -> 29
        //     55: astore 4
        //     57: goto -15 -> 42
        //     60: astore_3
        //     61: goto -29 -> 32
        //
        // Exception table:
        //     from	to	target	type
        //     0	9	30	java/io/FileNotFoundException
        //     0	9	40	java/io/IOException
        //     25	29	50	java/lang/Exception
        //     9	21	55	java/io/IOException
        //     9	21	60	java/io/FileNotFoundException
    }

    public void setDataSource(String paramString, Map<String, String> paramMap)
        throws IllegalArgumentException
    {
        int i = 0;
        String[] arrayOfString1 = new String[paramMap.size()];
        String[] arrayOfString2 = new String[paramMap.size()];
        Iterator localIterator = paramMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            arrayOfString1[i] = ((String)localEntry.getKey());
            arrayOfString2[i] = ((String)localEntry.getValue());
            i++;
        }
        _setDataSource(paramString, arrayOfString1, arrayOfString2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaMetadataRetriever
 * JD-Core Version:        0.6.2
 */