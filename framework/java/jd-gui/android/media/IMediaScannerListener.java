package android.media;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IMediaScannerListener extends IInterface
{
    public abstract void scanCompleted(String paramString, Uri paramUri)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMediaScannerListener
    {
        private static final String DESCRIPTOR = "android.media.IMediaScannerListener";
        static final int TRANSACTION_scanCompleted = 1;

        public Stub()
        {
            attachInterface(this, "android.media.IMediaScannerListener");
        }

        public static IMediaScannerListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IMediaScannerListener");
                if ((localIInterface != null) && ((localIInterface instanceof IMediaScannerListener)))
                    localObject = (IMediaScannerListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.media.IMediaScannerListener");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.media.IMediaScannerListener");
            String str = paramParcel1.readString();
            if (paramParcel1.readInt() != 0);
            for (Uri localUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel1); ; localUri = null)
            {
                scanCompleted(str, localUri);
                break;
            }
        }

        private static class Proxy
            implements IMediaScannerListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IMediaScannerListener";
            }

            public void scanCompleted(String paramString, Uri paramUri)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IMediaScannerListener");
                    localParcel.writeString(paramString);
                    if (paramUri != null)
                    {
                        localParcel.writeInt(1);
                        paramUri.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IMediaScannerListener
 * JD-Core Version:        0.6.2
 */