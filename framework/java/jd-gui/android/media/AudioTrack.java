package android.media;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.lang.ref.WeakReference;

public class AudioTrack
{
    public static final int ERROR = -1;
    public static final int ERROR_BAD_VALUE = -2;
    public static final int ERROR_INVALID_OPERATION = -3;
    private static final int ERROR_NATIVESETUP_AUDIOSYSTEM = -16;
    private static final int ERROR_NATIVESETUP_INVALIDCHANNELMASK = -17;
    private static final int ERROR_NATIVESETUP_INVALIDFORMAT = -18;
    private static final int ERROR_NATIVESETUP_INVALIDSTREAMTYPE = -19;
    private static final int ERROR_NATIVESETUP_NATIVEINITFAILED = -20;
    public static final int MODE_STATIC = 0;
    public static final int MODE_STREAM = 1;
    private static final int NATIVE_EVENT_MARKER = 3;
    private static final int NATIVE_EVENT_NEW_POS = 4;
    public static final int PLAYSTATE_PAUSED = 2;
    public static final int PLAYSTATE_PLAYING = 3;
    public static final int PLAYSTATE_STOPPED = 1;
    public static final int STATE_INITIALIZED = 1;
    public static final int STATE_NO_STATIC_DATA = 2;
    public static final int STATE_UNINITIALIZED = 0;
    public static final int SUCCESS = 0;
    private static final int SUPPORTED_OUT_CHANNELS = 1276;
    private static final String TAG = "AudioTrack-Java";
    private static final float VOLUME_MAX = 1.0F;
    private static final float VOLUME_MIN;
    private int mAudioFormat = 2;
    private int mChannelConfiguration = 4;
    private int mChannelCount = 1;
    private int mChannels = 4;
    private int mDataLoadMode = 1;
    private NativeEventHandlerDelegate mEventHandlerDelegate = null;
    private Looper mInitializationLooper = null;
    private int mJniData;
    private int mNativeBufferSizeInBytes = 0;
    private int mNativeTrackInJavaObj;
    private int mPlayState = 1;
    private final Object mPlayStateLock = new Object();
    private OnPlaybackPositionUpdateListener mPositionListener = null;
    private final Object mPositionListenerLock = new Object();
    private int mSampleRate;
    private int mSessionId = 0;
    private int mState = 0;
    private int mStreamType = 3;

    public AudioTrack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        throws IllegalArgumentException
    {
        this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, 0);
    }

    public AudioTrack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
        throws IllegalArgumentException
    {
        Looper localLooper = Looper.myLooper();
        this.mInitializationLooper = localLooper;
        if (localLooper == null)
            this.mInitializationLooper = Looper.getMainLooper();
        audioParamCheck(paramInt1, paramInt2, paramInt3, paramInt4, paramInt6);
        audioBuffSizeCheck(paramInt5);
        if (paramInt7 < 0)
            throw new IllegalArgumentException("Invalid audio session ID: " + paramInt7);
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = paramInt7;
        int i = native_setup(new WeakReference(this), this.mStreamType, this.mSampleRate, this.mChannels, this.mAudioFormat, this.mNativeBufferSizeInBytes, this.mDataLoadMode, arrayOfInt);
        if (i != 0)
            loge("Error code " + i + " when initializing AudioTrack.");
        while (true)
        {
            return;
            this.mSessionId = arrayOfInt[0];
            if (this.mDataLoadMode == 0)
                this.mState = 2;
            else
                this.mState = 1;
        }
    }

    private void audioBuffSizeCheck(int paramInt)
    {
        int i = this.mChannelCount;
        if (this.mAudioFormat == 3);
        for (int j = 1; (paramInt % (i * j) != 0) || (paramInt < 1); j = 2)
            throw new IllegalArgumentException("Invalid audio buffer size.");
        this.mNativeBufferSizeInBytes = paramInt;
    }

    private void audioParamCheck(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if ((paramInt1 != 4) && (paramInt1 != 3) && (paramInt1 != 2) && (paramInt1 != 1) && (paramInt1 != 0) && (paramInt1 != 5) && (paramInt1 != 6) && (paramInt1 != 8))
            throw new IllegalArgumentException("Invalid stream type.");
        this.mStreamType = paramInt1;
        if ((paramInt2 < 4000) || (paramInt2 > 48000))
            throw new IllegalArgumentException(paramInt2 + "Hz is not a supported sample rate.");
        this.mSampleRate = paramInt2;
        this.mChannelConfiguration = paramInt3;
        switch (paramInt3)
        {
        default:
            if (!isMultichannelConfigSupported(paramInt3))
            {
                this.mChannelCount = 0;
                this.mChannels = 0;
                this.mChannelConfiguration = 0;
                throw new IllegalArgumentException("Unsupported channel configuration.");
            }
            break;
        case 1:
        case 2:
        case 4:
            this.mChannelCount = 1;
            this.mChannels = 4;
        case 3:
        case 12:
        }
        while (true)
            switch (paramInt4)
            {
            default:
                this.mAudioFormat = 0;
                throw new IllegalArgumentException("Unsupported sample encoding. Should be ENCODING_PCM_8BIT or ENCODING_PCM_16BIT.");
                this.mChannelCount = 2;
                this.mChannels = 12;
                continue;
                this.mChannels = paramInt3;
                this.mChannelCount = Integer.bitCount(paramInt3);
            case 1:
            case 2:
            case 3:
            }
        for (this.mAudioFormat = 2; (paramInt5 != 1) && (paramInt5 != 0); this.mAudioFormat = paramInt4)
            throw new IllegalArgumentException("Invalid mode.");
        this.mDataLoadMode = paramInt5;
    }

    public static float getMaxVolume()
    {
        return 1.0F;
    }

    public static int getMinBufferSize(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = -2;
        switch (paramInt2)
        {
        default:
            if ((paramInt2 & 0x4FC) != paramInt2)
                loge("getMinBufferSize(): Invalid channel configuration.");
            break;
        case 2:
        case 4:
        case 3:
        case 12:
        }
        while (true)
        {
            return i;
            int j = 1;
            while (true)
            {
                if ((paramInt3 == 2) || (paramInt3 == 3))
                    break label100;
                loge("getMinBufferSize(): Invalid audio format.");
                break;
                j = 2;
                continue;
                j = Integer.bitCount(paramInt2);
            }
            label100: if ((paramInt1 < 4000) || (paramInt1 > 48000))
            {
                loge("getMinBufferSize(): " + paramInt1 + "Hz is not a supported sample rate.");
            }
            else
            {
                i = native_get_min_buff_size(paramInt1, j, paramInt3);
                if ((i == -1) || (i == 0))
                {
                    loge("getMinBufferSize(): error querying hardware");
                    i = -1;
                }
            }
        }
    }

    public static float getMinVolume()
    {
        return 0.0F;
    }

    public static int getNativeOutputSampleRate(int paramInt)
    {
        return native_get_output_sample_rate(paramInt);
    }

    private static boolean isMultichannelConfigSupported(int paramInt)
    {
        boolean bool = false;
        if ((paramInt & 0x4FC) != paramInt)
            Log.e("AudioTrack-Java", "Channel configuration features unsupported channels");
        while (true)
        {
            return bool;
            if ((paramInt & 0xC) != 12)
                Log.e("AudioTrack-Java", "Front channels must be present in multichannel configurations");
            else if (((paramInt & 0xC0) != 0) && ((paramInt & 0xC0) != 192))
                Log.e("AudioTrack-Java", "Rear channels can't be used independently");
            else
                bool = true;
        }
    }

    private static void logd(String paramString)
    {
        Log.d("AudioTrack-Java", "[ android.media.AudioTrack ] " + paramString);
    }

    private static void loge(String paramString)
    {
        Log.e("AudioTrack-Java", "[ android.media.AudioTrack ] " + paramString);
    }

    private final native int native_attachAuxEffect(int paramInt);

    private final native void native_finalize();

    private final native void native_flush();

    private final native int native_get_marker_pos();

    private static final native int native_get_min_buff_size(int paramInt1, int paramInt2, int paramInt3);

    private final native int native_get_native_frame_count();

    private static final native int native_get_output_sample_rate(int paramInt);

    private final native int native_get_playback_rate();

    private final native int native_get_pos_update_period();

    private final native int native_get_position();

    private final native int native_get_session_id();

    private final native void native_pause();

    private final native void native_release();

    private final native int native_reload_static();

    private final native void native_setAuxEffectSendLevel(float paramFloat);

    private final native void native_setVolume(float paramFloat1, float paramFloat2);

    private final native int native_set_loop(int paramInt1, int paramInt2, int paramInt3);

    private final native int native_set_marker_pos(int paramInt);

    private final native int native_set_playback_rate(int paramInt);

    private final native int native_set_pos_update_period(int paramInt);

    private final native int native_set_position(int paramInt);

    private final native int native_setup(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int[] paramArrayOfInt);

    private final native void native_start();

    private final native void native_stop();

    private final native int native_write_byte(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3);

    private final native int native_write_short(short[] paramArrayOfShort, int paramInt1, int paramInt2, int paramInt3);

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        AudioTrack localAudioTrack = (AudioTrack)((WeakReference)paramObject1).get();
        if (localAudioTrack == null);
        while (true)
        {
            return;
            if (localAudioTrack.mEventHandlerDelegate != null)
            {
                Message localMessage = localAudioTrack.mEventHandlerDelegate.getHandler().obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localAudioTrack.mEventHandlerDelegate.getHandler().sendMessage(localMessage);
            }
        }
    }

    public int attachAuxEffect(int paramInt)
    {
        if (this.mState != 1);
        for (int i = -3; ; i = native_attachAuxEffect(paramInt))
            return i;
    }

    protected void finalize()
    {
        native_finalize();
    }

    public void flush()
    {
        if (this.mState == 1)
            native_flush();
    }

    public int getAudioFormat()
    {
        return this.mAudioFormat;
    }

    public int getAudioSessionId()
    {
        return this.mSessionId;
    }

    public int getChannelConfiguration()
    {
        return this.mChannelConfiguration;
    }

    public int getChannelCount()
    {
        return this.mChannelCount;
    }

    protected int getNativeFrameCount()
    {
        return native_get_native_frame_count();
    }

    public int getNotificationMarkerPosition()
    {
        return native_get_marker_pos();
    }

    public int getPlayState()
    {
        synchronized (this.mPlayStateLock)
        {
            int i = this.mPlayState;
            return i;
        }
    }

    public int getPlaybackHeadPosition()
    {
        return native_get_position();
    }

    public int getPlaybackRate()
    {
        return native_get_playback_rate();
    }

    public int getPositionNotificationPeriod()
    {
        return native_get_pos_update_period();
    }

    public int getSampleRate()
    {
        return this.mSampleRate;
    }

    public int getState()
    {
        return this.mState;
    }

    public int getStreamType()
    {
        return this.mStreamType;
    }

    public void pause()
        throws IllegalStateException
    {
        if (this.mState != 1)
            throw new IllegalStateException("pause() called on uninitialized AudioTrack.");
        synchronized (this.mPlayStateLock)
        {
            native_pause();
            this.mPlayState = 2;
            return;
        }
    }

    public void play()
        throws IllegalStateException
    {
        if (this.mState != 1)
            throw new IllegalStateException("play() called on uninitialized AudioTrack.");
        synchronized (this.mPlayStateLock)
        {
            native_start();
            this.mPlayState = 3;
            return;
        }
    }

    public void release()
    {
        try
        {
            stop();
            label4: native_release();
            this.mState = 0;
            return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            break label4;
        }
    }

    public int reloadStaticData()
    {
        if (this.mDataLoadMode == 1);
        for (int i = -3; ; i = native_reload_static())
            return i;
    }

    public int setAuxEffectSendLevel(float paramFloat)
    {
        if (this.mState != 1);
        for (int i = -3; ; i = 0)
        {
            return i;
            if (paramFloat < getMinVolume())
                paramFloat = getMinVolume();
            if (paramFloat > getMaxVolume())
                paramFloat = getMaxVolume();
            native_setAuxEffectSendLevel(paramFloat);
        }
    }

    public int setLoopPoints(int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mDataLoadMode == 1);
        for (int i = -3; ; i = native_set_loop(paramInt1, paramInt2, paramInt3))
            return i;
    }

    public int setNotificationMarkerPosition(int paramInt)
    {
        if (this.mState != 1);
        for (int i = -3; ; i = native_set_marker_pos(paramInt))
            return i;
    }

    public int setPlaybackHeadPosition(int paramInt)
    {
        int i;
        synchronized (this.mPlayStateLock)
        {
            if ((this.mPlayState == 1) || (this.mPlayState == 2))
                i = native_set_position(paramInt);
            else
                i = -3;
        }
        return i;
    }

    public void setPlaybackPositionUpdateListener(OnPlaybackPositionUpdateListener paramOnPlaybackPositionUpdateListener)
    {
        setPlaybackPositionUpdateListener(paramOnPlaybackPositionUpdateListener, null);
    }

    public void setPlaybackPositionUpdateListener(OnPlaybackPositionUpdateListener paramOnPlaybackPositionUpdateListener, Handler paramHandler)
    {
        synchronized (this.mPositionListenerLock)
        {
            this.mPositionListener = paramOnPlaybackPositionUpdateListener;
            if (paramOnPlaybackPositionUpdateListener != null)
                this.mEventHandlerDelegate = new NativeEventHandlerDelegate(this, paramHandler);
            return;
        }
    }

    public int setPlaybackRate(int paramInt)
    {
        int i;
        if (this.mState != 1)
            i = -3;
        while (true)
        {
            return i;
            if (paramInt <= 0)
                i = -2;
            else
                i = native_set_playback_rate(paramInt);
        }
    }

    public int setPositionNotificationPeriod(int paramInt)
    {
        if (this.mState != 1);
        for (int i = -3; ; i = native_set_pos_update_period(paramInt))
            return i;
    }

    protected void setState(int paramInt)
    {
        this.mState = paramInt;
    }

    public int setStereoVolume(float paramFloat1, float paramFloat2)
    {
        if (this.mState != 1);
        for (int i = -3; ; i = 0)
        {
            return i;
            if (paramFloat1 < getMinVolume())
                paramFloat1 = getMinVolume();
            if (paramFloat1 > getMaxVolume())
                paramFloat1 = getMaxVolume();
            if (paramFloat2 < getMinVolume())
                paramFloat2 = getMinVolume();
            if (paramFloat2 > getMaxVolume())
                paramFloat2 = getMaxVolume();
            native_setVolume(paramFloat1, paramFloat2);
        }
    }

    public void stop()
        throws IllegalStateException
    {
        if (this.mState != 1)
            throw new IllegalStateException("stop() called on uninitialized AudioTrack.");
        synchronized (this.mPlayStateLock)
        {
            native_stop();
            this.mPlayState = 1;
            return;
        }
    }

    public int write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        if ((this.mDataLoadMode == 0) && (this.mState == 2) && (paramInt2 > 0))
            this.mState = 1;
        int i;
        if (this.mState != 1)
            i = -3;
        while (true)
        {
            return i;
            if ((paramArrayOfByte == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length))
                i = -2;
            else
                i = native_write_byte(paramArrayOfByte, paramInt1, paramInt2, this.mAudioFormat);
        }
    }

    public int write(short[] paramArrayOfShort, int paramInt1, int paramInt2)
    {
        if ((this.mDataLoadMode == 0) && (this.mState == 2) && (paramInt2 > 0))
            this.mState = 1;
        int i;
        if (this.mState != 1)
            i = -3;
        while (true)
        {
            return i;
            if ((paramArrayOfShort == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfShort.length))
                i = -2;
            else
                i = native_write_short(paramArrayOfShort, paramInt1, paramInt2, this.mAudioFormat);
        }
    }

    private class NativeEventHandlerDelegate
    {
        private final AudioTrack mAudioTrack;
        private final Handler mHandler;

        NativeEventHandlerDelegate(AudioTrack paramHandler, Handler arg3)
        {
            this.mAudioTrack = paramHandler;
            Object localObject;
            Looper localLooper;
            if (localObject != null)
            {
                localLooper = localObject.getLooper();
                if (localLooper == null)
                    break label54;
            }
            label54: for (this.mHandler = new Handler(localLooper)
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    if (AudioTrack.this == null);
                    while (true)
                    {
                        return;
                        AudioTrack.OnPlaybackPositionUpdateListener localOnPlaybackPositionUpdateListener;
                        synchronized (AudioTrack.this.mPositionListenerLock)
                        {
                            localOnPlaybackPositionUpdateListener = AudioTrack.this.mPositionListener;
                            switch (paramAnonymousMessage.what)
                            {
                            default:
                                Log.e("AudioTrack-Java", "[ android.media.AudioTrack.NativeEventHandler ] Unknown event type: " + paramAnonymousMessage.what);
                            case 3:
                            case 4:
                            }
                        }
                        if (localOnPlaybackPositionUpdateListener != null)
                        {
                            localOnPlaybackPositionUpdateListener.onMarkerReached(AudioTrack.this);
                            continue;
                            if (localOnPlaybackPositionUpdateListener != null)
                                localOnPlaybackPositionUpdateListener.onPeriodicNotification(AudioTrack.this);
                        }
                    }
                }
            }
            ; ; this.mHandler = null)
            {
                return;
                localLooper = AudioTrack.this.mInitializationLooper;
                break;
            }
        }

        Handler getHandler()
        {
            return this.mHandler;
        }
    }

    public static abstract interface OnPlaybackPositionUpdateListener
    {
        public abstract void onMarkerReached(AudioTrack paramAudioTrack);

        public abstract void onPeriodicNotification(AudioTrack paramAudioTrack);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AudioTrack
 * JD-Core Version:        0.6.2
 */