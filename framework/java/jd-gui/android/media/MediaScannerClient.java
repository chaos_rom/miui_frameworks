package android.media;

public abstract interface MediaScannerClient
{
    public abstract void handleStringTag(String paramString1, String paramString2);

    public abstract void scanFile(String paramString, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2);

    public abstract void setMimeType(String paramString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaScannerClient
 * JD-Core Version:        0.6.2
 */