package android.media;

public class MediaSyncEvent
{
    public static final int SYNC_EVENT_NONE = 0;
    public static final int SYNC_EVENT_PRESENTATION_COMPLETE = 1;
    private int mAudioSession = 0;
    private final int mType;

    private MediaSyncEvent(int paramInt)
    {
        this.mType = paramInt;
    }

    public static MediaSyncEvent createEvent(int paramInt)
        throws IllegalArgumentException
    {
        if (!isValidType(paramInt))
            throw new IllegalArgumentException(paramInt + "is not a valid MediaSyncEvent type.");
        return new MediaSyncEvent(paramInt);
    }

    private static boolean isValidType(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public int getAudioSessionId()
    {
        return this.mAudioSession;
    }

    public int getType()
    {
        return this.mType;
    }

    public MediaSyncEvent setAudioSessionId(int paramInt)
        throws IllegalArgumentException
    {
        if (paramInt > 0)
        {
            this.mAudioSession = paramInt;
            return this;
        }
        throw new IllegalArgumentException(paramInt + " is not a valid session ID.");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaSyncEvent
 * JD-Core Version:        0.6.2
 */