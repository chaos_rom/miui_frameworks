package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IAudioFocusDispatcher extends IInterface
{
    public abstract void dispatchAudioFocusChange(int paramInt, String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAudioFocusDispatcher
    {
        private static final String DESCRIPTOR = "android.media.IAudioFocusDispatcher";
        static final int TRANSACTION_dispatchAudioFocusChange = 1;

        public Stub()
        {
            attachInterface(this, "android.media.IAudioFocusDispatcher");
        }

        public static IAudioFocusDispatcher asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IAudioFocusDispatcher");
                if ((localIInterface != null) && ((localIInterface instanceof IAudioFocusDispatcher)))
                    localObject = (IAudioFocusDispatcher)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.media.IAudioFocusDispatcher");
                continue;
                paramParcel1.enforceInterface("android.media.IAudioFocusDispatcher");
                dispatchAudioFocusChange(paramParcel1.readInt(), paramParcel1.readString());
            }
        }

        private static class Proxy
            implements IAudioFocusDispatcher
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dispatchAudioFocusChange(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioFocusDispatcher");
                    localParcel.writeInt(paramInt);
                    localParcel.writeString(paramString);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IAudioFocusDispatcher";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IAudioFocusDispatcher
 * JD-Core Version:        0.6.2
 */