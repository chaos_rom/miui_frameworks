package android.media;

import android.view.Surface;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class MediaCodec
{
    public static final int BUFFER_FLAG_CODEC_CONFIG = 2;
    public static final int BUFFER_FLAG_END_OF_STREAM = 4;
    public static final int BUFFER_FLAG_SYNC_FRAME = 1;
    public static final int CONFIGURE_FLAG_ENCODE = 1;
    public static final int CRYPTO_MODE_AES_CTR = 1;
    public static final int CRYPTO_MODE_UNENCRYPTED = 0;
    public static final int INFO_OUTPUT_BUFFERS_CHANGED = -3;
    public static final int INFO_OUTPUT_FORMAT_CHANGED = -2;
    public static final int INFO_TRY_AGAIN_LATER = -1;
    public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT = 1;
    public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2;
    private int mNativeContext;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    private MediaCodec(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
        native_setup(paramString, paramBoolean1, paramBoolean2);
    }

    public static MediaCodec createByCodecName(String paramString)
    {
        return new MediaCodec(paramString, false, false);
    }

    public static MediaCodec createDecoderByType(String paramString)
    {
        return new MediaCodec(paramString, true, false);
    }

    public static MediaCodec createEncoderByType(String paramString)
    {
        return new MediaCodec(paramString, true, true);
    }

    private final native ByteBuffer[] getBuffers(boolean paramBoolean);

    private final native Map<String, Object> getOutputFormatNative();

    private final native void native_configure(String[] paramArrayOfString, Object[] paramArrayOfObject, Surface paramSurface, MediaCrypto paramMediaCrypto, int paramInt);

    private final native void native_finalize();

    private static final native void native_init();

    private final native void native_setup(String paramString, boolean paramBoolean1, boolean paramBoolean2);

    public void configure(MediaFormat paramMediaFormat, Surface paramSurface, MediaCrypto paramMediaCrypto, int paramInt)
    {
        Map localMap = paramMediaFormat.getMap();
        String[] arrayOfString = null;
        Object[] arrayOfObject = null;
        if (paramMediaFormat != null)
        {
            arrayOfString = new String[localMap.size()];
            arrayOfObject = new Object[localMap.size()];
            int i = 0;
            Iterator localIterator = localMap.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                arrayOfString[i] = ((String)localEntry.getKey());
                arrayOfObject[i] = localEntry.getValue();
                i++;
            }
        }
        native_configure(arrayOfString, arrayOfObject, paramSurface, paramMediaCrypto, paramInt);
    }

    public final native int dequeueInputBuffer(long paramLong);

    public final native int dequeueOutputBuffer(BufferInfo paramBufferInfo, long paramLong);

    protected void finalize()
    {
        native_finalize();
    }

    public final native void flush();

    public ByteBuffer[] getInputBuffers()
    {
        return getBuffers(true);
    }

    public ByteBuffer[] getOutputBuffers()
    {
        return getBuffers(false);
    }

    public final MediaFormat getOutputFormat()
    {
        return new MediaFormat(getOutputFormatNative());
    }

    public final native void queueInputBuffer(int paramInt1, int paramInt2, int paramInt3, long paramLong, int paramInt4)
        throws MediaCodec.CryptoException;

    public final native void queueSecureInputBuffer(int paramInt1, int paramInt2, CryptoInfo paramCryptoInfo, long paramLong, int paramInt3)
        throws MediaCodec.CryptoException;

    public final native void release();

    public final native void releaseOutputBuffer(int paramInt, boolean paramBoolean);

    public final native void setVideoScalingMode(int paramInt);

    public final native void start();

    public final native void stop();

    public static final class CryptoInfo
    {
        public byte[] iv;
        public byte[] key;
        public int mode;
        public int[] numBytesOfClearData;
        public int[] numBytesOfEncryptedData;
        public int numSubSamples;

        public void set(int paramInt1, int[] paramArrayOfInt1, int[] paramArrayOfInt2, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt2)
        {
            this.numSubSamples = paramInt1;
            this.numBytesOfClearData = paramArrayOfInt1;
            this.numBytesOfEncryptedData = paramArrayOfInt2;
            this.key = paramArrayOfByte1;
            this.iv = paramArrayOfByte2;
            this.mode = paramInt2;
        }
    }

    public static final class CryptoException extends RuntimeException
    {
        private int mErrorCode;

        public CryptoException(int paramInt, String paramString)
        {
            super();
            this.mErrorCode = paramInt;
        }

        public int getErrorCode()
        {
            return this.mErrorCode;
        }
    }

    public static final class BufferInfo
    {
        public int flags;
        public int offset;
        public long presentationTimeUs;
        public int size;

        public void set(int paramInt1, int paramInt2, long paramLong, int paramInt3)
        {
            this.offset = paramInt1;
            this.size = paramInt2;
            this.presentationTimeUs = paramLong;
            this.flags = paramInt3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaCodec
 * JD-Core Version:        0.6.2
 */