package android.media;

public final class MediaCodecList
{
    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    static final native MediaCodecInfo.CodecCapabilities getCodecCapabilities(int paramInt, String paramString);

    public static final native int getCodecCount();

    public static final MediaCodecInfo getCodecInfoAt(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > getCodecCount()))
            throw new IllegalArgumentException();
        return new MediaCodecInfo(paramInt);
    }

    static final native String getCodecName(int paramInt);

    static final native String[] getSupportedTypes(int paramInt);

    static final native boolean isEncoder(int paramInt);

    private static final native void native_init();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaCodecList
 * JD-Core Version:        0.6.2
 */