package android.media;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;

public class CamcorderProfile
{
    public static final int QUALITY_1080P = 6;
    public static final int QUALITY_480P = 4;
    public static final int QUALITY_720P = 5;
    public static final int QUALITY_CIF = 3;
    public static final int QUALITY_HIGH = 1;
    private static final int QUALITY_LIST_END = 7;
    private static final int QUALITY_LIST_START = 0;
    public static final int QUALITY_LOW = 0;
    public static final int QUALITY_QCIF = 2;
    public static final int QUALITY_QVGA = 7;
    public static final int QUALITY_TIME_LAPSE_1080P = 1006;
    public static final int QUALITY_TIME_LAPSE_480P = 1004;
    public static final int QUALITY_TIME_LAPSE_720P = 1005;
    public static final int QUALITY_TIME_LAPSE_CIF = 1003;
    public static final int QUALITY_TIME_LAPSE_HIGH = 1001;
    private static final int QUALITY_TIME_LAPSE_LIST_END = 1007;
    private static final int QUALITY_TIME_LAPSE_LIST_START = 1000;
    public static final int QUALITY_TIME_LAPSE_LOW = 1000;
    public static final int QUALITY_TIME_LAPSE_QCIF = 1002;
    public static final int QUALITY_TIME_LAPSE_QVGA = 1007;
    public int audioBitRate;
    public int audioChannels;
    public int audioCodec;
    public int audioSampleRate;
    public int duration;
    public int fileFormat;
    public int quality;
    public int videoBitRate;
    public int videoCodec;
    public int videoFrameHeight;
    public int videoFrameRate;
    public int videoFrameWidth;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    private CamcorderProfile(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12)
    {
        this.duration = paramInt1;
        this.quality = paramInt2;
        this.fileFormat = paramInt3;
        this.videoCodec = paramInt4;
        this.videoBitRate = paramInt5;
        this.videoFrameRate = paramInt6;
        this.videoFrameWidth = paramInt7;
        this.videoFrameHeight = paramInt8;
        this.audioCodec = paramInt9;
        this.audioBitRate = paramInt10;
        this.audioSampleRate = paramInt11;
        this.audioChannels = paramInt12;
    }

    public static CamcorderProfile get(int paramInt)
    {
        int i = Camera.getNumberOfCameras();
        Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
        int j = 0;
        if (j < i)
        {
            Camera.getCameraInfo(j, localCameraInfo);
            if (localCameraInfo.facing != 0);
        }
        for (CamcorderProfile localCamcorderProfile = get(j, paramInt); ; localCamcorderProfile = null)
        {
            return localCamcorderProfile;
            j++;
            break;
        }
    }

    public static CamcorderProfile get(int paramInt1, int paramInt2)
    {
        if (((paramInt2 < 0) || (paramInt2 > 7)) && ((paramInt2 < 1000) || (paramInt2 > 1007)))
            throw new IllegalArgumentException("Unsupported quality level: " + paramInt2);
        return native_get_camcorder_profile(paramInt1, paramInt2);
    }

    public static boolean hasProfile(int paramInt)
    {
        int i = Camera.getNumberOfCameras();
        Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
        int j = 0;
        if (j < i)
        {
            Camera.getCameraInfo(j, localCameraInfo);
            if (localCameraInfo.facing != 0);
        }
        for (boolean bool = hasProfile(j, paramInt); ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public static boolean hasProfile(int paramInt1, int paramInt2)
    {
        return native_has_camcorder_profile(paramInt1, paramInt2);
    }

    private static final native CamcorderProfile native_get_camcorder_profile(int paramInt1, int paramInt2);

    private static final native boolean native_has_camcorder_profile(int paramInt1, int paramInt2);

    private static final native void native_init();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.CamcorderProfile
 * JD-Core Version:        0.6.2
 */