package android.media;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DrmStore.Audio;
import android.provider.MediaStore.Audio.Media;
import android.provider.Settings.System;
import android.util.Log;
import com.android.internal.database.SortCursor;
import java.util.ArrayList;
import java.util.List;

public class RingtoneManager
{
    public static final String ACTION_RINGTONE_PICKER = "android.intent.action.RINGTONE_PICKER";
    private static final String[] DRM_COLUMNS;
    public static final String EXTRA_RINGTONE_DEFAULT_URI = "android.intent.extra.ringtone.DEFAULT_URI";
    public static final String EXTRA_RINGTONE_EXISTING_URI = "android.intent.extra.ringtone.EXISTING_URI";
    public static final String EXTRA_RINGTONE_INCLUDE_DRM = "android.intent.extra.ringtone.INCLUDE_DRM";
    public static final String EXTRA_RINGTONE_PICKED_URI = "android.intent.extra.ringtone.PICKED_URI";
    public static final String EXTRA_RINGTONE_SHOW_DEFAULT = "android.intent.extra.ringtone.SHOW_DEFAULT";
    public static final String EXTRA_RINGTONE_SHOW_SILENT = "android.intent.extra.ringtone.SHOW_SILENT";
    public static final String EXTRA_RINGTONE_TITLE = "android.intent.extra.ringtone.TITLE";
    public static final String EXTRA_RINGTONE_TYPE = "android.intent.extra.ringtone.TYPE";
    public static final int ID_COLUMN_INDEX = 0;
    private static final String[] INTERNAL_COLUMNS;
    private static final String[] MEDIA_COLUMNS = arrayOfString3;
    private static final String TAG = "RingtoneManager";
    public static final int TITLE_COLUMN_INDEX = 1;
    public static final int TYPE_ALARM = 4;
    public static final int TYPE_ALL = 7;
    public static final int TYPE_NOTIFICATION = 2;
    public static final int TYPE_RINGTONE = 1;
    public static final int URI_COLUMN_INDEX = 2;
    private Activity mActivity;
    private Context mContext;
    private Cursor mCursor;
    private final List<String> mFilterColumns = new ArrayList();
    private boolean mIncludeDrm;
    private Ringtone mPreviousRingtone;
    private boolean mStopPreviousRingtone = true;
    private int mType = 1;

    static
    {
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "_id";
        arrayOfString1[1] = "title";
        arrayOfString1[2] = ("\"" + MediaStore.Audio.Media.INTERNAL_CONTENT_URI + "\"");
        arrayOfString1[3] = "title_key";
        INTERNAL_COLUMNS = arrayOfString1;
        String[] arrayOfString2 = new String[4];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "title";
        arrayOfString2[2] = ("\"" + DrmStore.Audio.CONTENT_URI + "\"");
        arrayOfString2[3] = "title AS title_key";
        DRM_COLUMNS = arrayOfString2;
        String[] arrayOfString3 = new String[4];
        arrayOfString3[0] = "_id";
        arrayOfString3[1] = "title";
        arrayOfString3[2] = ("\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\"");
        arrayOfString3[3] = "title_key";
    }

    public RingtoneManager(Activity paramActivity)
    {
        this.mActivity = paramActivity;
        this.mContext = paramActivity;
        setType(this.mType);
    }

    public RingtoneManager(Context paramContext)
    {
        this.mContext = paramContext;
        setType(this.mType);
    }

    private static String constructBooleanTrueWhereClause(List<String> paramList, boolean paramBoolean)
    {
        if (paramList == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append("(");
            for (int i = -1 + paramList.size(); i >= 0; i--)
                localStringBuilder.append((String)paramList.get(i)).append("=1 or ");
            if (paramList.size() > 0)
                localStringBuilder.setLength(-4 + localStringBuilder.length());
            localStringBuilder.append(")");
            if (!paramBoolean)
            {
                localStringBuilder.append(" and ");
                localStringBuilder.append("is_drm");
                localStringBuilder.append("=0");
            }
        }
    }

    public static Uri getActualDefaultRingtoneUri(Context paramContext, int paramInt)
    {
        Uri localUri = null;
        String str1 = getSettingForType(paramInt);
        if (str1 == null);
        while (true)
        {
            return localUri;
            String str2 = Settings.System.getString(paramContext.getContentResolver(), str1);
            if (str2 != null)
                localUri = Uri.parse(str2);
        }
    }

    public static int getDefaultType(Uri paramUri)
    {
        int i = -1;
        if (paramUri == null);
        while (true)
        {
            return i;
            if (paramUri.equals(Settings.System.DEFAULT_RINGTONE_URI))
                i = 1;
            else if (paramUri.equals(Settings.System.DEFAULT_NOTIFICATION_URI))
                i = 2;
            else if (paramUri.equals(Settings.System.DEFAULT_ALARM_ALERT_URI))
                i = 4;
        }
    }

    public static Uri getDefaultUri(int paramInt)
    {
        Uri localUri;
        if ((paramInt & 0x1) != 0)
            localUri = Settings.System.DEFAULT_RINGTONE_URI;
        while (true)
        {
            return localUri;
            if ((paramInt & 0x2) != 0)
                localUri = Settings.System.DEFAULT_NOTIFICATION_URI;
            else if ((paramInt & 0x4) != 0)
                localUri = Settings.System.DEFAULT_ALARM_ALERT_URI;
            else
                localUri = null;
        }
    }

    private Cursor getDrmRingtones()
    {
        return query(DrmStore.Audio.CONTENT_URI, DRM_COLUMNS, null, null, "title");
    }

    private Cursor getInternalRingtones()
    {
        return query(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, INTERNAL_COLUMNS, constructBooleanTrueWhereClause(this.mFilterColumns, this.mIncludeDrm), null, "title_key");
    }

    private Cursor getMediaRingtones()
    {
        Cursor localCursor = null;
        String str = Environment.getExternalStorageState();
        if ((str.equals("mounted")) || (str.equals("mounted_ro")))
            localCursor = query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, MEDIA_COLUMNS, constructBooleanTrueWhereClause(this.mFilterColumns, this.mIncludeDrm), null, "title_key");
        return localCursor;
    }

    public static Ringtone getRingtone(Context paramContext, Uri paramUri)
    {
        return getRingtone(paramContext, paramUri, -1);
    }

    private static Ringtone getRingtone(Context paramContext, Uri paramUri, int paramInt)
    {
        try
        {
            localRingtone = new Ringtone(paramContext, true);
            if (paramInt >= 0)
                localRingtone.setStreamType(paramInt);
            localRingtone.setUri(paramUri);
            return localRingtone;
        }
        catch (Exception localException)
        {
            while (true)
            {
                Log.e("RingtoneManager", "Failed to open ringtone " + paramUri + ": " + localException);
                Ringtone localRingtone = null;
            }
        }
    }

    private static String getSettingForType(int paramInt)
    {
        String str;
        if ((paramInt & 0x1) != 0)
            str = "ringtone";
        while (true)
        {
            return str;
            if ((paramInt & 0x2) != 0)
                str = "notification_sound";
            else if ((paramInt & 0x4) != 0)
                str = "alarm_alert";
            else
                str = null;
        }
    }

    private static Uri getUriFromCursor(Cursor paramCursor)
    {
        return ContentUris.withAppendedId(Uri.parse(paramCursor.getString(2)), paramCursor.getLong(0));
    }

    public static Uri getValidRingtoneUri(Context paramContext)
    {
        RingtoneManager localRingtoneManager = new RingtoneManager(paramContext);
        Uri localUri = getValidRingtoneUriFromCursorAndClose(paramContext, localRingtoneManager.getInternalRingtones());
        if (localUri == null)
            localUri = getValidRingtoneUriFromCursorAndClose(paramContext, localRingtoneManager.getMediaRingtones());
        if (localUri == null)
            localUri = getValidRingtoneUriFromCursorAndClose(paramContext, localRingtoneManager.getDrmRingtones());
        return localUri;
    }

    private static Uri getValidRingtoneUriFromCursorAndClose(Context paramContext, Cursor paramCursor)
    {
        Uri localUri;
        if (paramCursor != null)
        {
            localUri = null;
            if (paramCursor.moveToFirst())
                localUri = getUriFromCursor(paramCursor);
            paramCursor.close();
        }
        while (true)
        {
            return localUri;
            localUri = null;
        }
    }

    public static boolean isDefault(Uri paramUri)
    {
        if (getDefaultType(paramUri) != -1);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        if (this.mActivity != null);
        for (Cursor localCursor = this.mActivity.managedQuery(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2); ; localCursor = this.mContext.getContentResolver().query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2))
            return localCursor;
    }

    public static void setActualDefaultRingtoneUri(Context paramContext, int paramInt, Uri paramUri)
    {
        String str1 = getSettingForType(paramInt);
        if (str1 == null)
            return;
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramUri != null);
        for (String str2 = paramUri.toString(); ; str2 = null)
        {
            Settings.System.putString(localContentResolver, str1, str2);
            break;
        }
    }

    private void setFilterColumnsList(int paramInt)
    {
        List localList = this.mFilterColumns;
        localList.clear();
        if ((paramInt & 0x1) != 0)
            localList.add("is_ringtone");
        if ((paramInt & 0x2) != 0)
            localList.add("is_notification");
        if ((paramInt & 0x4) != 0)
            localList.add("is_alarm");
    }

    public Cursor getCursor()
    {
        Object localObject;
        if ((this.mCursor != null) && (this.mCursor.requery()))
        {
            localObject = this.mCursor;
            return localObject;
        }
        Cursor localCursor1 = getInternalRingtones();
        if (this.mIncludeDrm);
        for (Cursor localCursor2 = getDrmRingtones(); ; localCursor2 = null)
        {
            Cursor localCursor3 = getMediaRingtones();
            Cursor[] arrayOfCursor = new Cursor[3];
            arrayOfCursor[0] = localCursor1;
            arrayOfCursor[1] = localCursor2;
            arrayOfCursor[2] = localCursor3;
            localObject = new SortCursor(arrayOfCursor, "title_key");
            this.mCursor = ((Cursor)localObject);
            break;
        }
    }

    public boolean getIncludeDrm()
    {
        return this.mIncludeDrm;
    }

    public Ringtone getRingtone(int paramInt)
    {
        if ((this.mStopPreviousRingtone) && (this.mPreviousRingtone != null))
            this.mPreviousRingtone.stop();
        this.mPreviousRingtone = getRingtone(this.mContext, getRingtoneUri(paramInt), inferStreamType());
        return this.mPreviousRingtone;
    }

    public int getRingtonePosition(Uri paramUri)
    {
        int j;
        if (paramUri == null)
            j = -1;
        while (true)
        {
            return j;
            Cursor localCursor = getCursor();
            int i = localCursor.getCount();
            if (!localCursor.moveToFirst())
            {
                j = -1;
            }
            else
            {
                Uri localUri = null;
                Object localObject = null;
                for (j = 0; ; j++)
                {
                    if (j >= i)
                        break label122;
                    String str = localCursor.getString(2);
                    if ((localUri == null) || (!str.equals(localObject)))
                        localUri = Uri.parse(str);
                    if (paramUri.equals(ContentUris.withAppendedId(localUri, localCursor.getLong(0))))
                        break;
                    localCursor.move(1);
                    localObject = str;
                }
                label122: j = -1;
            }
        }
    }

    public Uri getRingtoneUri(int paramInt)
    {
        if ((this.mCursor == null) || (!this.mCursor.moveToPosition(paramInt)));
        for (Uri localUri = null; ; localUri = getUriFromCursor(this.mCursor))
            return localUri;
    }

    public boolean getStopPreviousRingtone()
    {
        return this.mStopPreviousRingtone;
    }

    public int inferStreamType()
    {
        int i;
        switch (this.mType)
        {
        case 3:
        default:
            i = 2;
        case 4:
        case 2:
        }
        while (true)
        {
            return i;
            i = 4;
            continue;
            i = 5;
        }
    }

    public void setIncludeDrm(boolean paramBoolean)
    {
        this.mIncludeDrm = paramBoolean;
    }

    public void setStopPreviousRingtone(boolean paramBoolean)
    {
        this.mStopPreviousRingtone = paramBoolean;
    }

    public void setType(int paramInt)
    {
        if (this.mCursor != null)
            throw new IllegalStateException("Setting filter columns should be done before querying for ringtones.");
        this.mType = paramInt;
        setFilterColumnsList(paramInt);
    }

    public void stopPreviousRingtone()
    {
        if (this.mPreviousRingtone != null)
            this.mPreviousRingtone.stop();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.RingtoneManager
 * JD-Core Version:        0.6.2
 */