package android.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MediaPlayer
{
    public static final boolean APPLY_METADATA_FILTER = true;
    public static final boolean BYPASS_METADATA_FILTER = false;
    private static final String IMEDIA_PLAYER = "android.media.IMediaPlayer";
    private static final int INVOKE_ID_ADD_EXTERNAL_SOURCE = 2;
    private static final int INVOKE_ID_ADD_EXTERNAL_SOURCE_FD = 3;
    private static final int INVOKE_ID_DESELECT_TRACK = 5;
    private static final int INVOKE_ID_GET_TRACK_INFO = 1;
    private static final int INVOKE_ID_SELECT_TRACK = 4;
    private static final int INVOKE_ID_SET_VIDEO_SCALE_MODE = 6;
    private static final int MEDIA_BUFFERING_UPDATE = 3;
    private static final int MEDIA_ERROR = 100;
    public static final int MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200;
    public static final int MEDIA_ERROR_SERVER_DIED = 100;
    public static final int MEDIA_ERROR_UNKNOWN = 1;
    private static final int MEDIA_INFO = 200;
    public static final int MEDIA_INFO_BAD_INTERLEAVING = 800;
    public static final int MEDIA_INFO_BUFFERING_END = 702;
    public static final int MEDIA_INFO_BUFFERING_START = 701;
    public static final int MEDIA_INFO_METADATA_UPDATE = 802;
    public static final int MEDIA_INFO_NOT_SEEKABLE = 801;
    public static final int MEDIA_INFO_STARTED_AS_NEXT = 2;
    public static final int MEDIA_INFO_TIMED_TEXT_ERROR = 900;
    public static final int MEDIA_INFO_UNKNOWN = 1;
    public static final int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;
    public static final String MEDIA_MIMETYPE_TEXT_SUBRIP = "application/x-subrip";
    private static final int MEDIA_NOP = 0;
    private static final int MEDIA_PLAYBACK_COMPLETE = 2;
    private static final int MEDIA_PREPARED = 1;
    private static final int MEDIA_SEEK_COMPLETE = 4;
    private static final int MEDIA_SET_VIDEO_SIZE = 5;
    private static final int MEDIA_TIMED_TEXT = 99;
    public static final boolean METADATA_ALL = false;
    public static final boolean METADATA_UPDATE_ONLY = true;
    private static final String TAG = "MediaPlayer";
    public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT = 1;
    public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2;
    private EventHandler mEventHandler;
    private int mListenerContext;
    private int mNativeContext;
    private int mNativeSurfaceTexture;
    private OnBufferingUpdateListener mOnBufferingUpdateListener;
    private OnCompletionListener mOnCompletionListener;
    private OnErrorListener mOnErrorListener;
    private OnInfoListener mOnInfoListener;
    private OnPreparedListener mOnPreparedListener;
    private OnSeekCompleteListener mOnSeekCompleteListener;
    private OnTimedTextListener mOnTimedTextListener;
    private OnVideoSizeChangedListener mOnVideoSizeChangedListener;
    private boolean mScreenOnWhilePlaying;
    private boolean mStayAwake;
    private SurfaceHolder mSurfaceHolder;
    private PowerManager.WakeLock mWakeLock = null;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    public MediaPlayer()
    {
        Looper localLooper1 = Looper.myLooper();
        if (localLooper1 != null)
            this.mEventHandler = new EventHandler(this, localLooper1);
        while (true)
        {
            native_setup(new WeakReference(this));
            return;
            Looper localLooper2 = Looper.getMainLooper();
            if (localLooper2 != null)
                this.mEventHandler = new EventHandler(this, localLooper2);
            else
                this.mEventHandler = null;
        }
    }

    private native void _pause()
        throws IllegalStateException;

    private native void _release();

    private native void _reset();

    private native void _setDataSource(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    private native void _setVideoSurface(Surface paramSurface);

    private native void _start()
        throws IllegalStateException;

    private native void _stop()
        throws IllegalStateException;

    private static boolean availableMimeTypeForExternalSource(String paramString)
    {
        if (paramString == "application/x-subrip");
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static MediaPlayer create(Context paramContext, int paramInt)
    {
        MediaPlayer localMediaPlayer;
        try
        {
            AssetFileDescriptor localAssetFileDescriptor = paramContext.getResources().openRawResourceFd(paramInt);
            if (localAssetFileDescriptor == null)
            {
                localMediaPlayer = null;
            }
            else
            {
                localMediaPlayer = new MediaPlayer();
                localMediaPlayer.setDataSource(localAssetFileDescriptor.getFileDescriptor(), localAssetFileDescriptor.getStartOffset(), localAssetFileDescriptor.getLength());
                localAssetFileDescriptor.close();
                localMediaPlayer.prepare();
            }
        }
        catch (IOException localIOException)
        {
            Log.d("MediaPlayer", "create failed:", localIOException);
            localMediaPlayer = null;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                Log.d("MediaPlayer", "create failed:", localIllegalArgumentException);
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
                Log.d("MediaPlayer", "create failed:", localSecurityException);
        }
        return localMediaPlayer;
    }

    public static MediaPlayer create(Context paramContext, Uri paramUri)
    {
        return create(paramContext, paramUri, null);
    }

    public static MediaPlayer create(Context paramContext, Uri paramUri, SurfaceHolder paramSurfaceHolder)
    {
        try
        {
            localMediaPlayer = new MediaPlayer();
            localMediaPlayer.setDataSource(paramContext, paramUri);
            if (paramSurfaceHolder != null)
                localMediaPlayer.setDisplay(paramSurfaceHolder);
            localMediaPlayer.prepare();
            return localMediaPlayer;
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.d("MediaPlayer", "create failed:", localIOException);
                MediaPlayer localMediaPlayer = null;
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                Log.d("MediaPlayer", "create failed:", localIllegalArgumentException);
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
                Log.d("MediaPlayer", "create failed:", localSecurityException);
        }
    }

    private native void getParameter(int paramInt, Parcel paramParcel);

    private boolean isVideoScalingModeSupported(int paramInt)
    {
        int i = 1;
        if ((paramInt == i) || (paramInt == 2));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    private final native void native_finalize();

    private final native boolean native_getMetadata(boolean paramBoolean1, boolean paramBoolean2, Parcel paramParcel);

    private static final native void native_init();

    private final native int native_invoke(Parcel paramParcel1, Parcel paramParcel2);

    public static native int native_pullBatteryData(Parcel paramParcel);

    private final native int native_setMetadataFilter(Parcel paramParcel);

    private final native int native_setRetransmitEndpoint(String paramString, int paramInt);

    private final native void native_setup(Object paramObject);

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        MediaPlayer localMediaPlayer = (MediaPlayer)((WeakReference)paramObject1).get();
        if (localMediaPlayer == null);
        while (true)
        {
            return;
            if ((paramInt1 == 200) && (paramInt2 == 2))
                localMediaPlayer.start();
            if (localMediaPlayer.mEventHandler != null)
            {
                Message localMessage = localMediaPlayer.mEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localMediaPlayer.mEventHandler.sendMessage(localMessage);
            }
        }
    }

    private void selectOrDeselectTrack(int paramInt, boolean paramBoolean)
        throws IllegalStateException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.media.IMediaPlayer");
            if (paramBoolean)
            {
                i = 4;
                localParcel1.writeInt(i);
                localParcel1.writeInt(paramInt);
                invoke(localParcel1, localParcel2);
                return;
            }
            int i = 5;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    private void setDataSource(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        File localFile = new File(paramString);
        if (localFile.exists())
        {
            FileInputStream localFileInputStream = new FileInputStream(localFile);
            setDataSource(localFileInputStream.getFD());
            localFileInputStream.close();
        }
        while (true)
        {
            return;
            _setDataSource(paramString, paramArrayOfString1, paramArrayOfString2);
        }
    }

    private void stayAwake(boolean paramBoolean)
    {
        if (this.mWakeLock != null)
        {
            if ((!paramBoolean) || (this.mWakeLock.isHeld()))
                break label38;
            this.mWakeLock.acquire();
        }
        while (true)
        {
            this.mStayAwake = paramBoolean;
            updateSurfaceScreenOn();
            return;
            label38: if ((!paramBoolean) && (this.mWakeLock.isHeld()))
                this.mWakeLock.release();
        }
    }

    private void updateSurfaceScreenOn()
    {
        SurfaceHolder localSurfaceHolder;
        if (this.mSurfaceHolder != null)
        {
            localSurfaceHolder = this.mSurfaceHolder;
            if ((!this.mScreenOnWhilePlaying) || (!this.mStayAwake))
                break label36;
        }
        label36: for (boolean bool = true; ; bool = false)
        {
            localSurfaceHolder.setKeepScreenOn(bool);
            return;
        }
    }

    // ERROR //
    public void addTimedTextSource(Context paramContext, Uri paramUri, String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        // Byte code:
        //     0: aload_2
        //     1: invokevirtual 389	android/net/Uri:getScheme	()Ljava/lang/String;
        //     4: astore 4
        //     6: aload 4
        //     8: ifnull +14 -> 22
        //     11: aload 4
        //     13: ldc_w 391
        //     16: invokevirtual 397	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     19: ifeq +13 -> 32
        //     22: aload_0
        //     23: aload_2
        //     24: invokevirtual 400	android/net/Uri:getPath	()Ljava/lang/String;
        //     27: aload_3
        //     28: invokevirtual 403	android/media/MediaPlayer:addTimedTextSource	(Ljava/lang/String;Ljava/lang/String;)V
        //     31: return
        //     32: aconst_null
        //     33: astore 5
        //     35: aload_1
        //     36: invokevirtual 407	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     39: aload_2
        //     40: ldc_w 409
        //     43: invokevirtual 415	android/content/ContentResolver:openAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
        //     46: astore 9
        //     48: aload 9
        //     50: astore 5
        //     52: aload 5
        //     54: ifnonnull +16 -> 70
        //     57: aload 5
        //     59: ifnull -28 -> 31
        //     62: aload 5
        //     64: invokevirtual 259	android/content/res/AssetFileDescriptor:close	()V
        //     67: goto -36 -> 31
        //     70: aload_0
        //     71: aload 5
        //     73: invokevirtual 245	android/content/res/AssetFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     76: aload_3
        //     77: invokevirtual 418	android/media/MediaPlayer:addTimedTextSource	(Ljava/io/FileDescriptor;Ljava/lang/String;)V
        //     80: aload 5
        //     82: ifnull -51 -> 31
        //     85: goto -23 -> 62
        //     88: astore 8
        //     90: aload 5
        //     92: ifnull +8 -> 100
        //     95: aload 5
        //     97: invokevirtual 259	android/content/res/AssetFileDescriptor:close	()V
        //     100: aload 8
        //     102: athrow
        //     103: astore 7
        //     105: aload 5
        //     107: ifnull -76 -> 31
        //     110: goto -48 -> 62
        //     113: astore 6
        //     115: aload 5
        //     117: ifnull -86 -> 31
        //     120: goto -58 -> 62
        //
        // Exception table:
        //     from	to	target	type
        //     35	48	88	finally
        //     70	80	88	finally
        //     35	48	103	java/lang/SecurityException
        //     70	80	103	java/lang/SecurityException
        //     35	48	113	java/io/IOException
        //     70	80	113	java/io/IOException
    }

    public void addTimedTextSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, String paramString)
        throws IllegalArgumentException, IllegalStateException
    {
        if (!availableMimeTypeForExternalSource(paramString))
            throw new IllegalArgumentException("Illegal mimeType for timed text source: " + paramString);
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.media.IMediaPlayer");
            localParcel1.writeInt(3);
            localParcel1.writeFileDescriptor(paramFileDescriptor);
            localParcel1.writeLong(paramLong1);
            localParcel1.writeLong(paramLong2);
            localParcel1.writeString(paramString);
            invoke(localParcel1, localParcel2);
            return;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public void addTimedTextSource(FileDescriptor paramFileDescriptor, String paramString)
        throws IllegalArgumentException, IllegalStateException
    {
        addTimedTextSource(paramFileDescriptor, 0L, 576460752303423487L, paramString);
    }

    public void addTimedTextSource(String paramString1, String paramString2)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        if (!availableMimeTypeForExternalSource(paramString2))
            throw new IllegalArgumentException("Illegal mimeType for timed text source: " + paramString2);
        File localFile = new File(paramString1);
        if (localFile.exists())
        {
            FileInputStream localFileInputStream = new FileInputStream(localFile);
            addTimedTextSource(localFileInputStream.getFD(), paramString2);
            localFileInputStream.close();
            return;
        }
        throw new IOException(paramString1);
    }

    public native void attachAuxEffect(int paramInt);

    public void deselectTrack(int paramInt)
        throws IllegalStateException
    {
        selectOrDeselectTrack(paramInt, false);
    }

    protected void finalize()
    {
        native_finalize();
    }

    public native int getAudioSessionId();

    public native int getCurrentPosition();

    public native int getDuration();

    public native Bitmap getFrameAt(int paramInt)
        throws IllegalStateException;

    public int getIntParameter(int paramInt)
    {
        Parcel localParcel = Parcel.obtain();
        getParameter(paramInt, localParcel);
        int i = localParcel.readInt();
        localParcel.recycle();
        return i;
    }

    public Metadata getMetadata(boolean paramBoolean1, boolean paramBoolean2)
    {
        Parcel localParcel = Parcel.obtain();
        Metadata localMetadata = new Metadata();
        if (!native_getMetadata(paramBoolean1, paramBoolean2, localParcel))
        {
            localParcel.recycle();
            localMetadata = null;
        }
        while (true)
        {
            return localMetadata;
            if (!localMetadata.parse(localParcel))
            {
                localParcel.recycle();
                localMetadata = null;
            }
        }
    }

    public Parcel getParcelParameter(int paramInt)
    {
        Parcel localParcel = Parcel.obtain();
        getParameter(paramInt, localParcel);
        return localParcel;
    }

    public String getStringParameter(int paramInt)
    {
        Parcel localParcel = Parcel.obtain();
        getParameter(paramInt, localParcel);
        String str = localParcel.readString();
        localParcel.recycle();
        return str;
    }

    public TrackInfo[] getTrackInfo()
        throws IllegalStateException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.media.IMediaPlayer");
            localParcel1.writeInt(1);
            invoke(localParcel1, localParcel2);
            TrackInfo[] arrayOfTrackInfo = (TrackInfo[])localParcel2.createTypedArray(TrackInfo.CREATOR);
            return arrayOfTrackInfo;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public native int getVideoHeight();

    public native int getVideoWidth();

    public void invoke(Parcel paramParcel1, Parcel paramParcel2)
    {
        int i = native_invoke(paramParcel1, paramParcel2);
        paramParcel2.setDataPosition(0);
        if (i != 0)
            throw new RuntimeException("failure code: " + i);
    }

    public native boolean isLooping();

    public native boolean isPlaying();

    public Parcel newRequest()
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInterfaceToken("android.media.IMediaPlayer");
        return localParcel;
    }

    public void pause()
        throws IllegalStateException
    {
        stayAwake(false);
        _pause();
    }

    public native void prepare()
        throws IOException, IllegalStateException;

    public native void prepareAsync()
        throws IllegalStateException;

    public void release()
    {
        stayAwake(false);
        updateSurfaceScreenOn();
        this.mOnPreparedListener = null;
        this.mOnBufferingUpdateListener = null;
        this.mOnCompletionListener = null;
        this.mOnSeekCompleteListener = null;
        this.mOnErrorListener = null;
        this.mOnInfoListener = null;
        this.mOnVideoSizeChangedListener = null;
        this.mOnTimedTextListener = null;
        _release();
    }

    public void reset()
    {
        stayAwake(false);
        _reset();
        this.mEventHandler.removeCallbacksAndMessages(null);
    }

    public native void seekTo(int paramInt)
        throws IllegalStateException;

    public void selectTrack(int paramInt)
        throws IllegalStateException
    {
        selectOrDeselectTrack(paramInt, true);
    }

    public native void setAudioSessionId(int paramInt)
        throws IllegalArgumentException, IllegalStateException;

    public native void setAudioStreamType(int paramInt);

    public native void setAuxEffectSendLevel(float paramFloat);

    public void setDataSource(Context paramContext, Uri paramUri)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        setDataSource(paramContext, paramUri, null);
    }

    // ERROR //
    public void setDataSource(Context paramContext, Uri paramUri, Map<String, String> paramMap)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        // Byte code:
        //     0: aload_2
        //     1: invokevirtual 389	android/net/Uri:getScheme	()Ljava/lang/String;
        //     4: astore 4
        //     6: aload 4
        //     8: ifnull +14 -> 22
        //     11: aload 4
        //     13: ldc_w 391
        //     16: invokevirtual 397	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     19: ifeq +12 -> 31
        //     22: aload_0
        //     23: aload_2
        //     24: invokevirtual 400	android/net/Uri:getPath	()Ljava/lang/String;
        //     27: invokevirtual 540	android/media/MediaPlayer:setDataSource	(Ljava/lang/String;)V
        //     30: return
        //     31: aconst_null
        //     32: astore 5
        //     34: aload_1
        //     35: invokevirtual 407	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     38: aload_2
        //     39: ldc_w 409
        //     42: invokevirtual 415	android/content/ContentResolver:openAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
        //     45: astore 10
        //     47: aload 10
        //     49: astore 5
        //     51: aload 5
        //     53: ifnonnull +16 -> 69
        //     56: aload 5
        //     58: ifnull -28 -> 30
        //     61: aload 5
        //     63: invokevirtual 259	android/content/res/AssetFileDescriptor:close	()V
        //     66: goto -36 -> 30
        //     69: aload 5
        //     71: invokevirtual 543	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     74: lconst_0
        //     75: lcmp
        //     76: ifge +15 -> 91
        //     79: aload_0
        //     80: aload 5
        //     82: invokevirtual 245	android/content/res/AssetFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     85: invokevirtual 353	android/media/MediaPlayer:setDataSource	(Ljava/io/FileDescriptor;)V
        //     88: goto +83 -> 171
        //     91: aload_0
        //     92: aload 5
        //     94: invokevirtual 245	android/content/res/AssetFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     97: aload 5
        //     99: invokevirtual 249	android/content/res/AssetFileDescriptor:getStartOffset	()J
        //     102: aload 5
        //     104: invokevirtual 543	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     107: invokevirtual 256	android/media/MediaPlayer:setDataSource	(Ljava/io/FileDescriptor;JJ)V
        //     110: goto +61 -> 171
        //     113: astore 9
        //     115: aload 5
        //     117: ifnull +8 -> 125
        //     120: aload 5
        //     122: invokevirtual 259	android/content/res/AssetFileDescriptor:close	()V
        //     125: ldc 94
        //     127: ldc_w 545
        //     130: invokestatic 548	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     133: pop
        //     134: aload_0
        //     135: aload_2
        //     136: invokevirtual 549	android/net/Uri:toString	()Ljava/lang/String;
        //     139: aload_3
        //     140: invokevirtual 552	android/media/MediaPlayer:setDataSource	(Ljava/lang/String;Ljava/util/Map;)V
        //     143: goto -113 -> 30
        //     146: astore 8
        //     148: aload 5
        //     150: ifnull +8 -> 158
        //     153: aload 5
        //     155: invokevirtual 259	android/content/res/AssetFileDescriptor:close	()V
        //     158: aload 8
        //     160: athrow
        //     161: astore 6
        //     163: aload 5
        //     165: ifnull -40 -> 125
        //     168: goto -48 -> 120
        //     171: aload 5
        //     173: ifnull -143 -> 30
        //     176: goto -115 -> 61
        //
        // Exception table:
        //     from	to	target	type
        //     34	47	113	java/lang/SecurityException
        //     69	110	113	java/lang/SecurityException
        //     34	47	146	finally
        //     69	110	146	finally
        //     34	47	161	java/io/IOException
        //     69	110	161	java/io/IOException
    }

    public void setDataSource(FileDescriptor paramFileDescriptor)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        setDataSource(paramFileDescriptor, 0L, 576460752303423487L);
    }

    public native void setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2)
        throws IOException, IllegalArgumentException, IllegalStateException;

    public void setDataSource(String paramString)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        setDataSource(paramString, null, null);
    }

    public void setDataSource(String paramString, Map<String, String> paramMap)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        String[] arrayOfString1 = null;
        String[] arrayOfString2 = null;
        if (paramMap != null)
        {
            arrayOfString1 = new String[paramMap.size()];
            arrayOfString2 = new String[paramMap.size()];
            int i = 0;
            Iterator localIterator = paramMap.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                arrayOfString1[i] = ((String)localEntry.getKey());
                arrayOfString2[i] = ((String)localEntry.getValue());
                i++;
            }
        }
        setDataSource(paramString, arrayOfString1, arrayOfString2);
    }

    public void setDisplay(SurfaceHolder paramSurfaceHolder)
    {
        this.mSurfaceHolder = paramSurfaceHolder;
        if (paramSurfaceHolder != null);
        for (Surface localSurface = paramSurfaceHolder.getSurface(); ; localSurface = null)
        {
            _setVideoSurface(localSurface);
            updateSurfaceScreenOn();
            return;
        }
    }

    public native void setLooping(boolean paramBoolean);

    public int setMetadataFilter(Set<Integer> paramSet1, Set<Integer> paramSet2)
    {
        Parcel localParcel = newRequest();
        int i = localParcel.dataSize() + 4 * (1 + (1 + paramSet1.size()) + paramSet2.size());
        if (localParcel.dataCapacity() < i)
            localParcel.setDataCapacity(i);
        localParcel.writeInt(paramSet1.size());
        Iterator localIterator1 = paramSet1.iterator();
        while (localIterator1.hasNext())
            localParcel.writeInt(((Integer)localIterator1.next()).intValue());
        localParcel.writeInt(paramSet2.size());
        Iterator localIterator2 = paramSet2.iterator();
        while (localIterator2.hasNext())
            localParcel.writeInt(((Integer)localIterator2.next()).intValue());
        return native_setMetadataFilter(localParcel);
    }

    public native void setNextMediaPlayer(MediaPlayer paramMediaPlayer);

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener paramOnBufferingUpdateListener)
    {
        this.mOnBufferingUpdateListener = paramOnBufferingUpdateListener;
    }

    public void setOnCompletionListener(OnCompletionListener paramOnCompletionListener)
    {
        this.mOnCompletionListener = paramOnCompletionListener;
    }

    public void setOnErrorListener(OnErrorListener paramOnErrorListener)
    {
        this.mOnErrorListener = paramOnErrorListener;
    }

    public void setOnInfoListener(OnInfoListener paramOnInfoListener)
    {
        this.mOnInfoListener = paramOnInfoListener;
    }

    public void setOnPreparedListener(OnPreparedListener paramOnPreparedListener)
    {
        this.mOnPreparedListener = paramOnPreparedListener;
    }

    public void setOnSeekCompleteListener(OnSeekCompleteListener paramOnSeekCompleteListener)
    {
        this.mOnSeekCompleteListener = paramOnSeekCompleteListener;
    }

    public void setOnTimedTextListener(OnTimedTextListener paramOnTimedTextListener)
    {
        this.mOnTimedTextListener = paramOnTimedTextListener;
    }

    public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener paramOnVideoSizeChangedListener)
    {
        this.mOnVideoSizeChangedListener = paramOnVideoSizeChangedListener;
    }

    public boolean setParameter(int paramInt1, int paramInt2)
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeInt(paramInt2);
        boolean bool = setParameter(paramInt1, localParcel);
        localParcel.recycle();
        return bool;
    }

    public native boolean setParameter(int paramInt, Parcel paramParcel);

    public boolean setParameter(int paramInt, String paramString)
    {
        Parcel localParcel = Parcel.obtain();
        localParcel.writeString(paramString);
        boolean bool = setParameter(paramInt, localParcel);
        localParcel.recycle();
        return bool;
    }

    public void setRetransmitEndpoint(InetSocketAddress paramInetSocketAddress)
        throws IllegalStateException, IllegalArgumentException
    {
        String str = null;
        int i = 0;
        if (paramInetSocketAddress != null)
        {
            str = paramInetSocketAddress.getAddress().getHostAddress();
            i = paramInetSocketAddress.getPort();
        }
        int j = native_setRetransmitEndpoint(str, i);
        if (j != 0)
            throw new IllegalArgumentException("Illegal re-transmit endpoint; native ret " + j);
    }

    public void setScreenOnWhilePlaying(boolean paramBoolean)
    {
        if (this.mScreenOnWhilePlaying != paramBoolean)
        {
            if ((paramBoolean) && (this.mSurfaceHolder == null))
                Log.w("MediaPlayer", "setScreenOnWhilePlaying(true) is ineffective without a SurfaceHolder");
            this.mScreenOnWhilePlaying = paramBoolean;
            updateSurfaceScreenOn();
        }
    }

    public void setSurface(Surface paramSurface)
    {
        if ((this.mScreenOnWhilePlaying) && (paramSurface != null))
            Log.w("MediaPlayer", "setScreenOnWhilePlaying(true) is ineffective for Surface");
        this.mSurfaceHolder = null;
        _setVideoSurface(paramSurface);
        updateSurfaceScreenOn();
    }

    public void setVideoScalingMode(int paramInt)
    {
        if (!isVideoScalingModeSupported(paramInt))
            throw new IllegalArgumentException("Scaling mode " + paramInt + " is not supported");
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken("android.media.IMediaPlayer");
            localParcel1.writeInt(6);
            localParcel1.writeInt(paramInt);
            invoke(localParcel1, localParcel2);
            return;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public native void setVolume(float paramFloat1, float paramFloat2);

    public void setWakeMode(Context paramContext, int paramInt)
    {
        int i = 0;
        if (this.mWakeLock != null)
        {
            if (this.mWakeLock.isHeld())
            {
                i = 1;
                this.mWakeLock.release();
            }
            this.mWakeLock = null;
        }
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(0x20000000 | paramInt, MediaPlayer.class.getName());
        this.mWakeLock.setReferenceCounted(false);
        if (i != 0)
            this.mWakeLock.acquire();
    }

    public void start()
        throws IllegalStateException
    {
        stayAwake(true);
        _start();
    }

    public void stop()
        throws IllegalStateException
    {
        stayAwake(false);
        _stop();
    }

    public static abstract interface OnInfoListener
    {
        public abstract boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2);
    }

    public static abstract interface OnErrorListener
    {
        public abstract boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2);
    }

    public static abstract interface OnTimedTextListener
    {
        public abstract void onTimedText(MediaPlayer paramMediaPlayer, TimedText paramTimedText);
    }

    public static abstract interface OnVideoSizeChangedListener
    {
        public abstract void onVideoSizeChanged(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2);
    }

    public static abstract interface OnSeekCompleteListener
    {
        public abstract void onSeekComplete(MediaPlayer paramMediaPlayer);
    }

    public static abstract interface OnBufferingUpdateListener
    {
        public abstract void onBufferingUpdate(MediaPlayer paramMediaPlayer, int paramInt);
    }

    public static abstract interface OnCompletionListener
    {
        public abstract void onCompletion(MediaPlayer paramMediaPlayer);
    }

    public static abstract interface OnPreparedListener
    {
        public abstract void onPrepared(MediaPlayer paramMediaPlayer);
    }

    private class EventHandler extends Handler
    {
        private MediaPlayer mMediaPlayer;

        public EventHandler(MediaPlayer paramLooper, Looper arg3)
        {
            super();
            this.mMediaPlayer = paramLooper;
        }

        public void handleMessage(Message paramMessage)
        {
            if (this.mMediaPlayer.mNativeContext == 0)
                Log.w("MediaPlayer", "mediaplayer went away with unhandled events");
            while (true)
            {
                return;
                switch (paramMessage.what)
                {
                case 0:
                default:
                    Log.e("MediaPlayer", "Unknown message type " + paramMessage.what);
                    break;
                case 1:
                    if (MediaPlayer.this.mOnPreparedListener != null)
                        MediaPlayer.this.mOnPreparedListener.onPrepared(this.mMediaPlayer);
                    break;
                case 2:
                    if (MediaPlayer.this.mOnCompletionListener != null)
                        MediaPlayer.this.mOnCompletionListener.onCompletion(this.mMediaPlayer);
                    MediaPlayer.this.stayAwake(false);
                    break;
                case 3:
                    if (MediaPlayer.this.mOnBufferingUpdateListener != null)
                        MediaPlayer.this.mOnBufferingUpdateListener.onBufferingUpdate(this.mMediaPlayer, paramMessage.arg1);
                    break;
                case 4:
                    if (MediaPlayer.this.mOnSeekCompleteListener != null)
                        MediaPlayer.this.mOnSeekCompleteListener.onSeekComplete(this.mMediaPlayer);
                    break;
                case 5:
                    if (MediaPlayer.this.mOnVideoSizeChangedListener != null)
                        MediaPlayer.this.mOnVideoSizeChangedListener.onVideoSizeChanged(this.mMediaPlayer, paramMessage.arg1, paramMessage.arg2);
                    break;
                case 100:
                    Log.e("MediaPlayer", "Error (" + paramMessage.arg1 + "," + paramMessage.arg2 + ")");
                    boolean bool = false;
                    if (MediaPlayer.this.mOnErrorListener != null)
                        bool = MediaPlayer.this.mOnErrorListener.onError(this.mMediaPlayer, paramMessage.arg1, paramMessage.arg2);
                    if ((MediaPlayer.this.mOnCompletionListener != null) && (!bool))
                        MediaPlayer.this.mOnCompletionListener.onCompletion(this.mMediaPlayer);
                    MediaPlayer.this.stayAwake(false);
                    break;
                case 200:
                    if (paramMessage.arg1 != 700)
                        Log.i("MediaPlayer", "Info (" + paramMessage.arg1 + "," + paramMessage.arg2 + ")");
                    if (MediaPlayer.this.mOnInfoListener != null)
                        MediaPlayer.this.mOnInfoListener.onInfo(this.mMediaPlayer, paramMessage.arg1, paramMessage.arg2);
                    break;
                case 99:
                    if (MediaPlayer.this.mOnTimedTextListener != null)
                        if (paramMessage.obj == null)
                        {
                            MediaPlayer.this.mOnTimedTextListener.onTimedText(this.mMediaPlayer, null);
                        }
                        else if ((paramMessage.obj instanceof Parcel))
                        {
                            Parcel localParcel = (Parcel)paramMessage.obj;
                            TimedText localTimedText = new TimedText(localParcel);
                            localParcel.recycle();
                            MediaPlayer.this.mOnTimedTextListener.onTimedText(this.mMediaPlayer, localTimedText);
                        }
                    break;
                }
            }
        }
    }

    public static class TrackInfo
        implements Parcelable
    {
        static final Parcelable.Creator<TrackInfo> CREATOR = new Parcelable.Creator()
        {
            public MediaPlayer.TrackInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new MediaPlayer.TrackInfo(paramAnonymousParcel);
            }

            public MediaPlayer.TrackInfo[] newArray(int paramAnonymousInt)
            {
                return new MediaPlayer.TrackInfo[paramAnonymousInt];
            }
        };
        public static final int MEDIA_TRACK_TYPE_AUDIO = 2;
        public static final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;
        public static final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
        public static final int MEDIA_TRACK_TYPE_VIDEO = 1;
        final String mLanguage;
        final int mTrackType;

        TrackInfo(Parcel paramParcel)
        {
            this.mTrackType = paramParcel.readInt();
            this.mLanguage = paramParcel.readString();
        }

        public int describeContents()
        {
            return 0;
        }

        public String getLanguage()
        {
            return this.mLanguage;
        }

        public int getTrackType()
        {
            return this.mTrackType;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.mTrackType);
            paramParcel.writeString(this.mLanguage);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaPlayer
 * JD-Core Version:        0.6.2
 */