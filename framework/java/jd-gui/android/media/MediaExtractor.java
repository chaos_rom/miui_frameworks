package android.media;

import java.io.FileDescriptor;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class MediaExtractor
{
    public static final int SAMPLE_FLAG_ENCRYPTED = 2;
    public static final int SAMPLE_FLAG_SYNC = 1;
    public static final int SEEK_TO_CLOSEST_SYNC = 2;
    public static final int SEEK_TO_NEXT_SYNC = 1;
    public static final int SEEK_TO_PREVIOUS_SYNC;
    private int mNativeContext;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    public MediaExtractor()
    {
        native_setup();
    }

    private native Map<String, Object> getTrackFormatNative(int paramInt);

    private final native void native_finalize();

    private static final native void native_init();

    private final native void native_setup();

    private final native void setDataSource(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2);

    public native boolean advance();

    protected void finalize()
    {
        native_finalize();
    }

    public native long getCachedDuration();

    public native boolean getSampleCryptoInfo(MediaCodec.CryptoInfo paramCryptoInfo);

    public native int getSampleFlags();

    public native long getSampleTime();

    public native int getSampleTrackIndex();

    public final native int getTrackCount();

    public MediaFormat getTrackFormat(int paramInt)
    {
        return new MediaFormat(getTrackFormatNative(paramInt));
    }

    public native boolean hasCacheReachedEndOfStream();

    public native int readSampleData(ByteBuffer paramByteBuffer, int paramInt);

    public final native void release();

    public native void seekTo(long paramLong, int paramInt);

    public native void selectTrack(int paramInt);

    // ERROR //
    public final void setDataSource(android.content.Context paramContext, android.net.Uri paramUri, Map<String, String> paramMap)
        throws java.io.IOException
    {
        // Byte code:
        //     0: aload_2
        //     1: invokevirtual 79	android/net/Uri:getScheme	()Ljava/lang/String;
        //     4: astore 4
        //     6: aload 4
        //     8: ifnull +13 -> 21
        //     11: aload 4
        //     13: ldc 81
        //     15: invokevirtual 87	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     18: ifeq +12 -> 30
        //     21: aload_0
        //     22: aload_2
        //     23: invokevirtual 90	android/net/Uri:getPath	()Ljava/lang/String;
        //     26: invokevirtual 92	android/media/MediaExtractor:setDataSource	(Ljava/lang/String;)V
        //     29: return
        //     30: aconst_null
        //     31: astore 5
        //     33: aload_1
        //     34: invokevirtual 98	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     37: aload_2
        //     38: ldc 100
        //     40: invokevirtual 106	android/content/ContentResolver:openAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
        //     43: astore 9
        //     45: aload 9
        //     47: astore 5
        //     49: aload 5
        //     51: ifnonnull +16 -> 67
        //     54: aload 5
        //     56: ifnull -27 -> 29
        //     59: aload 5
        //     61: invokevirtual 111	android/content/res/AssetFileDescriptor:close	()V
        //     64: goto -35 -> 29
        //     67: aload 5
        //     69: invokevirtual 114	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     72: lconst_0
        //     73: lcmp
        //     74: ifge +15 -> 89
        //     77: aload_0
        //     78: aload 5
        //     80: invokevirtual 118	android/content/res/AssetFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     83: invokevirtual 121	android/media/MediaExtractor:setDataSource	(Ljava/io/FileDescriptor;)V
        //     86: goto +74 -> 160
        //     89: aload_0
        //     90: aload 5
        //     92: invokevirtual 118	android/content/res/AssetFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     95: aload 5
        //     97: invokevirtual 124	android/content/res/AssetFileDescriptor:getStartOffset	()J
        //     100: aload 5
        //     102: invokevirtual 114	android/content/res/AssetFileDescriptor:getDeclaredLength	()J
        //     105: invokevirtual 127	android/media/MediaExtractor:setDataSource	(Ljava/io/FileDescriptor;JJ)V
        //     108: goto +52 -> 160
        //     111: astore 8
        //     113: aload 5
        //     115: ifnull +8 -> 123
        //     118: aload 5
        //     120: invokevirtual 111	android/content/res/AssetFileDescriptor:close	()V
        //     123: aload_0
        //     124: aload_2
        //     125: invokevirtual 130	android/net/Uri:toString	()Ljava/lang/String;
        //     128: aload_3
        //     129: invokevirtual 133	android/media/MediaExtractor:setDataSource	(Ljava/lang/String;Ljava/util/Map;)V
        //     132: goto -103 -> 29
        //     135: astore 7
        //     137: aload 5
        //     139: ifnull +8 -> 147
        //     142: aload 5
        //     144: invokevirtual 111	android/content/res/AssetFileDescriptor:close	()V
        //     147: aload 7
        //     149: athrow
        //     150: astore 6
        //     152: aload 5
        //     154: ifnull -31 -> 123
        //     157: goto -39 -> 118
        //     160: aload 5
        //     162: ifnull -133 -> 29
        //     165: goto -106 -> 59
        //
        // Exception table:
        //     from	to	target	type
        //     33	45	111	java/lang/SecurityException
        //     67	108	111	java/lang/SecurityException
        //     33	45	135	finally
        //     67	108	135	finally
        //     33	45	150	java/io/IOException
        //     67	108	150	java/io/IOException
    }

    public final void setDataSource(FileDescriptor paramFileDescriptor)
    {
        setDataSource(paramFileDescriptor, 0L, 576460752303423487L);
    }

    public final native void setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2);

    public final void setDataSource(String paramString)
    {
        setDataSource(paramString, null, null);
    }

    public final void setDataSource(String paramString, Map<String, String> paramMap)
    {
        String[] arrayOfString1 = null;
        String[] arrayOfString2 = null;
        if (paramMap != null)
        {
            arrayOfString1 = new String[paramMap.size()];
            arrayOfString2 = new String[paramMap.size()];
            int i = 0;
            Iterator localIterator = paramMap.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                arrayOfString1[i] = ((String)localEntry.getKey());
                arrayOfString2[i] = ((String)localEntry.getValue());
                i++;
            }
        }
        setDataSource(paramString, arrayOfString1, arrayOfString2);
    }

    public native void unselectTrack(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaExtractor
 * JD-Core Version:        0.6.2
 */