package android.media;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Log;

public class RemoteControlClient
{
    public static final int DEFAULT_PLAYBACK_VOLUME = 15;
    public static final int DEFAULT_PLAYBACK_VOLUME_HANDLING = 1;
    public static final int FLAGS_KEY_MEDIA_NONE = 0;
    public static final int FLAG_INFORMATION_REQUEST_ALBUM_ART = 8;
    public static final int FLAG_INFORMATION_REQUEST_KEY_MEDIA = 2;
    public static final int FLAG_INFORMATION_REQUEST_METADATA = 1;
    public static final int FLAG_INFORMATION_REQUEST_PLAYSTATE = 4;
    public static final int FLAG_KEY_MEDIA_FAST_FORWARD = 64;
    public static final int FLAG_KEY_MEDIA_NEXT = 128;
    public static final int FLAG_KEY_MEDIA_PAUSE = 16;
    public static final int FLAG_KEY_MEDIA_PLAY = 4;
    public static final int FLAG_KEY_MEDIA_PLAY_PAUSE = 8;
    public static final int FLAG_KEY_MEDIA_PREVIOUS = 1;
    public static final int FLAG_KEY_MEDIA_REWIND = 2;
    public static final int FLAG_KEY_MEDIA_STOP = 32;
    private static final int[] METADATA_KEYS_TYPE_LONG = arrayOfInt2;
    private static final int[] METADATA_KEYS_TYPE_STRING;
    private static final int MSG_NEW_CURRENT_CLIENT_GEN = 6;
    private static final int MSG_NEW_INTERNAL_CLIENT_GEN = 5;
    private static final int MSG_PLUG_DISPLAY = 7;
    private static final int MSG_REQUEST_ARTWORK = 4;
    private static final int MSG_REQUEST_METADATA = 2;
    private static final int MSG_REQUEST_PLAYBACK_STATE = 1;
    private static final int MSG_REQUEST_TRANSPORTCONTROL = 3;
    private static final int MSG_UNPLUG_DISPLAY = 8;
    public static final int PLAYBACKINFO_INVALID_VALUE = -2147483648;
    public static final int PLAYBACKINFO_PLAYBACK_TYPE = 1;
    public static final int PLAYBACKINFO_PLAYSTATE = 255;
    public static final int PLAYBACKINFO_USES_STREAM = 5;
    public static final int PLAYBACKINFO_VOLUME = 2;
    public static final int PLAYBACKINFO_VOLUME_HANDLING = 4;
    public static final int PLAYBACKINFO_VOLUME_MAX = 3;
    public static final int PLAYBACK_TYPE_LOCAL = 0;
    private static final int PLAYBACK_TYPE_MAX = 1;
    private static final int PLAYBACK_TYPE_MIN = 0;
    public static final int PLAYBACK_TYPE_REMOTE = 1;
    public static final int PLAYBACK_VOLUME_FIXED = 0;
    public static final int PLAYBACK_VOLUME_VARIABLE = 1;
    public static final int PLAYSTATE_BUFFERING = 8;
    public static final int PLAYSTATE_ERROR = 9;
    public static final int PLAYSTATE_FAST_FORWARDING = 4;
    public static final int PLAYSTATE_NONE = 0;
    public static final int PLAYSTATE_PAUSED = 2;
    public static final int PLAYSTATE_PLAYING = 3;
    public static final int PLAYSTATE_REWINDING = 5;
    public static final int PLAYSTATE_SKIPPING_BACKWARDS = 7;
    public static final int PLAYSTATE_SKIPPING_FORWARDS = 6;
    public static final int PLAYSTATE_STOPPED = 1;
    public static final int RCSE_ID_UNREGISTERED = -1;
    private static final String TAG = "RemoteControlClient";
    private static IAudioService sService;
    private final int ARTWORK_DEFAULT_SIZE = 256;
    private final int ARTWORK_INVALID_SIZE = -1;
    private Bitmap mArtwork;
    private int mArtworkExpectedHeight = 256;
    private int mArtworkExpectedWidth = 256;
    private final Object mCacheLock = new Object();
    private int mCurrentClientGenId = -1;
    private EventHandler mEventHandler;
    private final IRemoteControlClient mIRCC = new IRemoteControlClient.Stub()
    {
        public void onInformationRequested(int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4)
        {
            if (RemoteControlClient.this.mEventHandler != null)
            {
                RemoteControlClient.this.mEventHandler.removeMessages(5);
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(5, paramAnonymousInt3, paramAnonymousInt4, new Integer(paramAnonymousInt1)));
                RemoteControlClient.this.mEventHandler.removeMessages(1);
                RemoteControlClient.this.mEventHandler.removeMessages(2);
                RemoteControlClient.this.mEventHandler.removeMessages(3);
                RemoteControlClient.this.mEventHandler.removeMessages(4);
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(1));
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(3));
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(2));
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(4));
            }
        }

        public void plugRemoteControlDisplay(IRemoteControlDisplay paramAnonymousIRemoteControlDisplay)
        {
            if (RemoteControlClient.this.mEventHandler != null)
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(7, paramAnonymousIRemoteControlDisplay));
        }

        public void setCurrentClientGenerationId(int paramAnonymousInt)
        {
            if (RemoteControlClient.this.mEventHandler != null)
            {
                RemoteControlClient.this.mEventHandler.removeMessages(6);
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(6, paramAnonymousInt, 0));
            }
        }

        public void unplugRemoteControlDisplay(IRemoteControlDisplay paramAnonymousIRemoteControlDisplay)
        {
            if (RemoteControlClient.this.mEventHandler != null)
                RemoteControlClient.this.mEventHandler.dispatchMessage(RemoteControlClient.this.mEventHandler.obtainMessage(8, paramAnonymousIRemoteControlDisplay));
        }
    };
    private int mInternalClientGenId = -2;
    private Bundle mMetadata = new Bundle();
    private int mPlaybackState = 0;
    private long mPlaybackStateChangeTimeMs = 0L;
    private int mPlaybackStream = 3;
    private int mPlaybackType = 0;
    private int mPlaybackVolume = 15;
    private int mPlaybackVolumeHandling = 1;
    private int mPlaybackVolumeMax = 15;
    private IRemoteControlDisplay mRcDisplay;
    private final PendingIntent mRcMediaIntent;
    private int mRcseId = -1;
    private int mTransportControlFlags = 0;

    static
    {
        int[] arrayOfInt1 = new int[11];
        arrayOfInt1[0] = 1;
        arrayOfInt1[1] = 13;
        arrayOfInt1[2] = 7;
        arrayOfInt1[3] = 2;
        arrayOfInt1[4] = 3;
        arrayOfInt1[5] = 15;
        arrayOfInt1[6] = 4;
        arrayOfInt1[7] = 5;
        arrayOfInt1[8] = 6;
        arrayOfInt1[9] = 7;
        arrayOfInt1[10] = 11;
        METADATA_KEYS_TYPE_STRING = arrayOfInt1;
        int[] arrayOfInt2 = new int[3];
        arrayOfInt2[0] = 0;
        arrayOfInt2[1] = 14;
        arrayOfInt2[2] = 9;
    }

    public RemoteControlClient(PendingIntent paramPendingIntent)
    {
        this.mRcMediaIntent = paramPendingIntent;
        Looper localLooper1 = Looper.myLooper();
        if (localLooper1 != null)
            this.mEventHandler = new EventHandler(this, localLooper1);
        while (true)
        {
            return;
            Looper localLooper2 = Looper.getMainLooper();
            if (localLooper2 != null)
            {
                this.mEventHandler = new EventHandler(this, localLooper2);
            }
            else
            {
                this.mEventHandler = null;
                Log.e("RemoteControlClient", "RemoteControlClient() couldn't find main application thread");
            }
        }
    }

    public RemoteControlClient(PendingIntent paramPendingIntent, Looper paramLooper)
    {
        this.mRcMediaIntent = paramPendingIntent;
        this.mEventHandler = new EventHandler(this, paramLooper);
    }

    private void detachFromDisplay_syncCacheLock()
    {
        this.mRcDisplay = null;
        this.mArtworkExpectedWidth = -1;
        this.mArtworkExpectedHeight = -1;
    }

    private static IAudioService getService()
    {
        if (sService != null);
        for (IAudioService localIAudioService = sService; ; localIAudioService = sService)
        {
            return localIAudioService;
            sService = IAudioService.Stub.asInterface(ServiceManager.getService("audio"));
        }
    }

    private void onNewCurrentClientGen(int paramInt)
    {
        synchronized (this.mCacheLock)
        {
            this.mCurrentClientGenId = paramInt;
            return;
        }
    }

    private void onNewInternalClientGen(Integer paramInteger, int paramInt1, int paramInt2)
    {
        synchronized (this.mCacheLock)
        {
            this.mInternalClientGenId = paramInteger.intValue();
            if (paramInt1 > 0)
            {
                this.mArtworkExpectedWidth = paramInt1;
                this.mArtworkExpectedHeight = paramInt2;
            }
            return;
        }
    }

    private void onPlugDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
    {
        synchronized (this.mCacheLock)
        {
            this.mRcDisplay = paramIRemoteControlDisplay;
            return;
        }
    }

    private void onUnplugDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
    {
        synchronized (this.mCacheLock)
        {
            if ((this.mRcDisplay != null) && (this.mRcDisplay.asBinder().equals(paramIRemoteControlDisplay.asBinder())))
            {
                this.mRcDisplay = null;
                this.mArtworkExpectedWidth = 256;
                this.mArtworkExpectedHeight = 256;
            }
            return;
        }
    }

    private Bitmap scaleBitmapIfTooBig(Bitmap paramBitmap, int paramInt1, int paramInt2)
    {
        if (paramBitmap != null)
        {
            int i = paramBitmap.getWidth();
            int j = paramBitmap.getHeight();
            if ((i > paramInt1) || (j > paramInt2))
            {
                float f = Math.min(paramInt1 / i, paramInt2 / j);
                int k = Math.round(f * i);
                int m = Math.round(f * j);
                Bitmap.Config localConfig = paramBitmap.getConfig();
                if (localConfig == null)
                    localConfig = Bitmap.Config.ARGB_8888;
                Bitmap localBitmap = Bitmap.createBitmap(k, m, localConfig);
                Canvas localCanvas = new Canvas(localBitmap);
                Paint localPaint = new Paint();
                localPaint.setAntiAlias(true);
                localPaint.setFilterBitmap(true);
                RectF localRectF = new RectF(0.0F, 0.0F, localBitmap.getWidth(), localBitmap.getHeight());
                localCanvas.drawBitmap(paramBitmap, null, localRectF, localPaint);
                paramBitmap = localBitmap;
            }
        }
        return paramBitmap;
    }

    private void sendArtwork_syncCacheLock()
    {
        if ((this.mCurrentClientGenId == this.mInternalClientGenId) && (this.mRcDisplay != null))
            this.mArtwork = scaleBitmapIfTooBig(this.mArtwork, this.mArtworkExpectedWidth, this.mArtworkExpectedHeight);
        try
        {
            this.mRcDisplay.setArtwork(this.mInternalClientGenId, this.mArtwork);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("RemoteControlClient", "Error in sendArtwork(), dead display " + localRemoteException);
                detachFromDisplay_syncCacheLock();
            }
        }
    }

    private void sendAudioServiceNewPlaybackInfo_syncCacheLock(int paramInt1, int paramInt2)
    {
        if (this.mRcseId == -1);
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.setPlaybackInfoForRcc(this.mRcseId, paramInt1, paramInt2);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("RemoteControlClient", "Dead object in sendAudioServiceNewPlaybackInfo_syncCacheLock", localRemoteException);
            }
        }
    }

    private void sendMetadataWithArtwork_syncCacheLock()
    {
        if ((this.mCurrentClientGenId == this.mInternalClientGenId) && (this.mRcDisplay != null))
            this.mArtwork = scaleBitmapIfTooBig(this.mArtwork, this.mArtworkExpectedWidth, this.mArtworkExpectedHeight);
        try
        {
            this.mRcDisplay.setAllMetadata(this.mInternalClientGenId, this.mMetadata, this.mArtwork);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("RemoteControlClient", "Error in setAllMetadata(), dead display " + localRemoteException);
                detachFromDisplay_syncCacheLock();
            }
        }
    }

    private void sendMetadata_syncCacheLock()
    {
        if ((this.mCurrentClientGenId == this.mInternalClientGenId) && (this.mRcDisplay != null));
        try
        {
            this.mRcDisplay.setMetadata(this.mInternalClientGenId, this.mMetadata);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("RemoteControlClient", "Error in sendPlaybackState(), dead display " + localRemoteException);
                detachFromDisplay_syncCacheLock();
            }
        }
    }

    private void sendPlaybackState_syncCacheLock()
    {
        if ((this.mCurrentClientGenId == this.mInternalClientGenId) && (this.mRcDisplay != null));
        try
        {
            this.mRcDisplay.setPlaybackState(this.mInternalClientGenId, this.mPlaybackState, this.mPlaybackStateChangeTimeMs);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("RemoteControlClient", "Error in setPlaybackState(), dead display " + localRemoteException);
                detachFromDisplay_syncCacheLock();
            }
        }
    }

    private void sendTransportControlFlags_syncCacheLock()
    {
        if ((this.mCurrentClientGenId == this.mInternalClientGenId) && (this.mRcDisplay != null));
        try
        {
            this.mRcDisplay.setTransportControlFlags(this.mInternalClientGenId, this.mTransportControlFlags);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("RemoteControlClient", "Error in sendTransportControlFlags(), dead display " + localRemoteException);
                detachFromDisplay_syncCacheLock();
            }
        }
    }

    private static boolean validTypeForKey(int paramInt, int[] paramArrayOfInt)
    {
        int i = 0;
        try
        {
            while (true)
            {
                int j = paramArrayOfInt[i];
                if (paramInt == j)
                {
                    bool = true;
                    return bool;
                }
                i++;
            }
        }
        catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public MetadataEditor editMetadata(boolean paramBoolean)
    {
        MetadataEditor localMetadataEditor = new MetadataEditor(null);
        if (paramBoolean)
        {
            localMetadataEditor.mEditorMetadata = new Bundle();
            localMetadataEditor.mEditorArtwork = null;
            localMetadataEditor.mMetadataChanged = true;
        }
        for (localMetadataEditor.mArtworkChanged = true; ; localMetadataEditor.mArtworkChanged = false)
        {
            return localMetadataEditor;
            localMetadataEditor.mEditorMetadata = new Bundle(this.mMetadata);
            localMetadataEditor.mEditorArtwork = this.mArtwork;
            localMetadataEditor.mMetadataChanged = false;
        }
    }

    public IRemoteControlClient getIRemoteControlClient()
    {
        return this.mIRCC;
    }

    // ERROR //
    public int getIntPlaybackInformation(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 139	android/media/RemoteControlClient:mCacheLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: iload_1
        //     8: tableswitch	default:+36 -> 44, 1:+70->78, 2:+87->95, 3:+97->105, 4:+117->125, 5:+107->115
        //     45: castore
        //     46: new 365	java/lang/StringBuilder
        //     49: dup
        //     50: invokespecial 366	java/lang/StringBuilder:<init>	()V
        //     53: ldc_w 448
        //     56: invokevirtual 372	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     59: iload_1
        //     60: invokevirtual 451	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     63: invokevirtual 379	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     66: invokestatic 193	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     69: pop
        //     70: ldc 55
        //     72: istore_3
        //     73: aload_2
        //     74: monitorexit
        //     75: goto +57 -> 132
        //     78: aload_0
        //     79: getfield 129	android/media/RemoteControlClient:mPlaybackType	I
        //     82: istore_3
        //     83: aload_2
        //     84: monitorexit
        //     85: goto +47 -> 132
        //     88: astore 4
        //     90: aload_2
        //     91: monitorexit
        //     92: aload 4
        //     94: athrow
        //     95: aload_0
        //     96: getfield 133	android/media/RemoteControlClient:mPlaybackVolume	I
        //     99: istore_3
        //     100: aload_2
        //     101: monitorexit
        //     102: goto +30 -> 132
        //     105: aload_0
        //     106: getfield 131	android/media/RemoteControlClient:mPlaybackVolumeMax	I
        //     109: istore_3
        //     110: aload_2
        //     111: monitorexit
        //     112: goto +20 -> 132
        //     115: aload_0
        //     116: getfield 137	android/media/RemoteControlClient:mPlaybackStream	I
        //     119: istore_3
        //     120: aload_2
        //     121: monitorexit
        //     122: goto +10 -> 132
        //     125: aload_0
        //     126: getfield 135	android/media/RemoteControlClient:mPlaybackVolumeHandling	I
        //     129: istore_3
        //     130: aload_2
        //     131: monitorexit
        //     132: iload_3
        //     133: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     44	92	88	finally
        //     95	132	88	finally
    }

    public PendingIntent getRcMediaIntent()
    {
        return this.mRcMediaIntent;
    }

    public int getRcseId()
    {
        return this.mRcseId;
    }

    // ERROR //
    public void setPlaybackInformation(int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 139	android/media/RemoteControlClient:mCacheLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: iload_1
        //     8: tableswitch	default:+36 -> 44, 1:+65->73, 2:+115->123, 3:+163->171, 4:+232->240, 5:+201->209
        //     45: castore
        //     46: new 365	java/lang/StringBuilder
        //     49: dup
        //     50: invokespecial 366	java/lang/StringBuilder:<init>	()V
        //     53: ldc_w 457
        //     56: invokevirtual 372	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     59: iload_1
        //     60: invokevirtual 451	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     63: invokevirtual 379	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     66: invokestatic 460	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     69: pop
        //     70: aload_3
        //     71: monitorexit
        //     72: return
        //     73: iload_2
        //     74: iflt +37 -> 111
        //     77: iload_2
        //     78: iconst_1
        //     79: if_icmpgt +32 -> 111
        //     82: aload_0
        //     83: getfield 129	android/media/RemoteControlClient:mPlaybackType	I
        //     86: iload_2
        //     87: if_icmpeq -17 -> 70
        //     90: aload_0
        //     91: iload_2
        //     92: putfield 129	android/media/RemoteControlClient:mPlaybackType	I
        //     95: aload_0
        //     96: iload_1
        //     97: iload_2
        //     98: invokespecial 462	android/media/RemoteControlClient:sendAudioServiceNewPlaybackInfo_syncCacheLock	(II)V
        //     101: goto -31 -> 70
        //     104: astore 5
        //     106: aload_3
        //     107: monitorexit
        //     108: aload 5
        //     110: athrow
        //     111: ldc 85
        //     113: ldc_w 464
        //     116: invokestatic 460	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     119: pop
        //     120: goto -50 -> 70
        //     123: iload_2
        //     124: bipush 255
        //     126: if_icmple +33 -> 159
        //     129: iload_2
        //     130: aload_0
        //     131: getfield 131	android/media/RemoteControlClient:mPlaybackVolumeMax	I
        //     134: if_icmpgt +25 -> 159
        //     137: aload_0
        //     138: getfield 133	android/media/RemoteControlClient:mPlaybackVolume	I
        //     141: iload_2
        //     142: if_icmpeq -72 -> 70
        //     145: aload_0
        //     146: iload_2
        //     147: putfield 133	android/media/RemoteControlClient:mPlaybackVolume	I
        //     150: aload_0
        //     151: iload_1
        //     152: iload_2
        //     153: invokespecial 462	android/media/RemoteControlClient:sendAudioServiceNewPlaybackInfo_syncCacheLock	(II)V
        //     156: goto -86 -> 70
        //     159: ldc 85
        //     161: ldc_w 466
        //     164: invokestatic 460	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     167: pop
        //     168: goto -98 -> 70
        //     171: iload_2
        //     172: ifle +25 -> 197
        //     175: aload_0
        //     176: getfield 131	android/media/RemoteControlClient:mPlaybackVolumeMax	I
        //     179: iload_2
        //     180: if_icmpeq -110 -> 70
        //     183: aload_0
        //     184: iload_2
        //     185: putfield 131	android/media/RemoteControlClient:mPlaybackVolumeMax	I
        //     188: aload_0
        //     189: iload_1
        //     190: iload_2
        //     191: invokespecial 462	android/media/RemoteControlClient:sendAudioServiceNewPlaybackInfo_syncCacheLock	(II)V
        //     194: goto -124 -> 70
        //     197: ldc 85
        //     199: ldc_w 468
        //     202: invokestatic 460	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     205: pop
        //     206: goto -136 -> 70
        //     209: iload_2
        //     210: iflt +18 -> 228
        //     213: iload_2
        //     214: invokestatic 473	android/media/AudioSystem:getNumStreamTypes	()I
        //     217: if_icmpge +11 -> 228
        //     220: aload_0
        //     221: iload_2
        //     222: putfield 137	android/media/RemoteControlClient:mPlaybackStream	I
        //     225: goto -155 -> 70
        //     228: ldc 85
        //     230: ldc_w 475
        //     233: invokestatic 460	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     236: pop
        //     237: goto -167 -> 70
        //     240: iload_2
        //     241: iflt +30 -> 271
        //     244: iload_2
        //     245: iconst_1
        //     246: if_icmpgt +25 -> 271
        //     249: aload_0
        //     250: getfield 135	android/media/RemoteControlClient:mPlaybackVolumeHandling	I
        //     253: iload_2
        //     254: if_icmpeq -184 -> 70
        //     257: aload_0
        //     258: iload_2
        //     259: putfield 135	android/media/RemoteControlClient:mPlaybackVolumeHandling	I
        //     262: aload_0
        //     263: iload_1
        //     264: iload_2
        //     265: invokespecial 462	android/media/RemoteControlClient:sendAudioServiceNewPlaybackInfo_syncCacheLock	(II)V
        //     268: goto -198 -> 70
        //     271: ldc 85
        //     273: ldc_w 477
        //     276: invokestatic 460	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     279: pop
        //     280: goto -210 -> 70
        //
        // Exception table:
        //     from	to	target	type
        //     44	108	104	finally
        //     111	280	104	finally
    }

    public void setPlaybackState(int paramInt)
    {
        synchronized (this.mCacheLock)
        {
            if (this.mPlaybackState != paramInt)
            {
                this.mPlaybackState = paramInt;
                this.mPlaybackStateChangeTimeMs = SystemClock.elapsedRealtime();
                sendPlaybackState_syncCacheLock();
                sendAudioServiceNewPlaybackInfo_syncCacheLock(255, paramInt);
            }
            return;
        }
    }

    public void setRcseId(int paramInt)
    {
        this.mRcseId = paramInt;
    }

    public void setTransportControlFlags(int paramInt)
    {
        synchronized (this.mCacheLock)
        {
            this.mTransportControlFlags = paramInt;
            sendTransportControlFlags_syncCacheLock();
            return;
        }
    }

    private class EventHandler extends Handler
    {
        public EventHandler(RemoteControlClient paramLooper, Looper arg3)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.e("RemoteControlClient", "Unknown event " + paramMessage.what + " in RemoteControlClient handler");
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return;
                synchronized (RemoteControlClient.this.mCacheLock)
                {
                    RemoteControlClient.this.sendPlaybackState_syncCacheLock();
                }
                synchronized (RemoteControlClient.this.mCacheLock)
                {
                    RemoteControlClient.this.sendMetadata_syncCacheLock();
                }
                synchronized (RemoteControlClient.this.mCacheLock)
                {
                    RemoteControlClient.this.sendTransportControlFlags_syncCacheLock();
                }
                synchronized (RemoteControlClient.this.mCacheLock)
                {
                    RemoteControlClient.this.sendArtwork_syncCacheLock();
                }
                RemoteControlClient.this.onNewInternalClientGen((Integer)paramMessage.obj, paramMessage.arg1, paramMessage.arg2);
                continue;
                RemoteControlClient.this.onNewCurrentClientGen(paramMessage.arg1);
                continue;
                RemoteControlClient.this.onPlugDisplay((IRemoteControlDisplay)paramMessage.obj);
                continue;
                RemoteControlClient.this.onUnplugDisplay((IRemoteControlDisplay)paramMessage.obj);
            }
        }
    }

    public class MetadataEditor
    {
        public static final int BITMAP_KEY_ARTWORK = 100;
        public static final int METADATA_KEY_ARTWORK = 100;
        private boolean mApplied = false;
        protected boolean mArtworkChanged;
        protected Bitmap mEditorArtwork;
        protected Bundle mEditorMetadata;
        protected boolean mMetadataChanged;

        private MetadataEditor()
        {
        }

        /** @deprecated */
        // ERROR //
        public void apply()
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 30	android/media/RemoteControlClient$MetadataEditor:mApplied	Z
            //     6: ifeq +14 -> 20
            //     9: ldc 36
            //     11: ldc 38
            //     13: invokestatic 44	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     16: pop
            //     17: aload_0
            //     18: monitorexit
            //     19: return
            //     20: aload_0
            //     21: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     24: invokestatic 48	android/media/RemoteControlClient:access$600	(Landroid/media/RemoteControlClient;)Ljava/lang/Object;
            //     27: astore_2
            //     28: aload_2
            //     29: monitorenter
            //     30: aload_0
            //     31: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     34: new 50	android/os/Bundle
            //     37: dup
            //     38: aload_0
            //     39: getfield 52	android/media/RemoteControlClient$MetadataEditor:mEditorMetadata	Landroid/os/Bundle;
            //     42: invokespecial 55	android/os/Bundle:<init>	(Landroid/os/Bundle;)V
            //     45: invokestatic 59	android/media/RemoteControlClient:access$702	(Landroid/media/RemoteControlClient;Landroid/os/Bundle;)Landroid/os/Bundle;
            //     48: pop
            //     49: aload_0
            //     50: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     53: invokestatic 63	android/media/RemoteControlClient:access$800	(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
            //     56: ifnull +30 -> 86
            //     59: aload_0
            //     60: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     63: invokestatic 63	android/media/RemoteControlClient:access$800	(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
            //     66: aload_0
            //     67: getfield 65	android/media/RemoteControlClient$MetadataEditor:mEditorArtwork	Landroid/graphics/Bitmap;
            //     70: invokevirtual 69	java/lang/Object:equals	(Ljava/lang/Object;)Z
            //     73: ifne +13 -> 86
            //     76: aload_0
            //     77: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     80: invokestatic 63	android/media/RemoteControlClient:access$800	(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
            //     83: invokevirtual 74	android/graphics/Bitmap:recycle	()V
            //     86: aload_0
            //     87: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     90: aload_0
            //     91: getfield 65	android/media/RemoteControlClient$MetadataEditor:mEditorArtwork	Landroid/graphics/Bitmap;
            //     94: invokestatic 78	android/media/RemoteControlClient:access$802	(Landroid/media/RemoteControlClient;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
            //     97: pop
            //     98: aload_0
            //     99: aconst_null
            //     100: putfield 65	android/media/RemoteControlClient$MetadataEditor:mEditorArtwork	Landroid/graphics/Bitmap;
            //     103: aload_0
            //     104: getfield 80	android/media/RemoteControlClient$MetadataEditor:mMetadataChanged	Z
            //     107: aload_0
            //     108: getfield 82	android/media/RemoteControlClient$MetadataEditor:mArtworkChanged	Z
            //     111: iand
            //     112: ifeq +30 -> 142
            //     115: aload_0
            //     116: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     119: invokestatic 85	android/media/RemoteControlClient:access$900	(Landroid/media/RemoteControlClient;)V
            //     122: aload_0
            //     123: iconst_1
            //     124: putfield 30	android/media/RemoteControlClient$MetadataEditor:mApplied	Z
            //     127: aload_2
            //     128: monitorexit
            //     129: goto -112 -> 17
            //     132: astore_3
            //     133: aload_2
            //     134: monitorexit
            //     135: aload_3
            //     136: athrow
            //     137: astore_1
            //     138: aload_0
            //     139: monitorexit
            //     140: aload_1
            //     141: athrow
            //     142: aload_0
            //     143: getfield 80	android/media/RemoteControlClient$MetadataEditor:mMetadataChanged	Z
            //     146: ifeq +13 -> 159
            //     149: aload_0
            //     150: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     153: invokestatic 88	android/media/RemoteControlClient:access$1000	(Landroid/media/RemoteControlClient;)V
            //     156: goto -34 -> 122
            //     159: aload_0
            //     160: getfield 82	android/media/RemoteControlClient$MetadataEditor:mArtworkChanged	Z
            //     163: ifeq -41 -> 122
            //     166: aload_0
            //     167: getfield 25	android/media/RemoteControlClient$MetadataEditor:this$0	Landroid/media/RemoteControlClient;
            //     170: invokestatic 91	android/media/RemoteControlClient:access$1100	(Landroid/media/RemoteControlClient;)V
            //     173: goto -51 -> 122
            //
            // Exception table:
            //     from	to	target	type
            //     30	135	132	finally
            //     142	173	132	finally
            //     2	17	137	finally
            //     20	30	137	finally
            //     135	137	137	finally
        }

        /** @deprecated */
        public void clear()
        {
            try
            {
                if (this.mApplied)
                    Log.e("RemoteControlClient", "Can't clear a previously applied MetadataEditor");
                while (true)
                {
                    return;
                    this.mEditorMetadata.clear();
                    this.mEditorArtwork = null;
                }
            }
            finally
            {
            }
        }

        public Object clone()
            throws CloneNotSupportedException
        {
            throw new CloneNotSupportedException();
        }

        /** @deprecated */
        public MetadataEditor putBitmap(int paramInt, Bitmap paramBitmap)
            throws IllegalArgumentException
        {
            MetadataEditor localMetadataEditor;
            try
            {
                if (this.mApplied)
                {
                    Log.e("RemoteControlClient", "Can't edit a previously applied MetadataEditor");
                    localMetadataEditor = this;
                    return localMetadataEditor;
                }
                if (paramInt != 100)
                    throw new IllegalArgumentException("Invalid type 'Bitmap' for key " + paramInt);
            }
            finally
            {
            }
            if ((RemoteControlClient.this.mArtworkExpectedWidth > 0) && (RemoteControlClient.this.mArtworkExpectedHeight > 0));
            for (this.mEditorArtwork = RemoteControlClient.this.scaleBitmapIfTooBig(paramBitmap, RemoteControlClient.this.mArtworkExpectedWidth, RemoteControlClient.this.mArtworkExpectedHeight); ; this.mEditorArtwork = paramBitmap)
            {
                this.mArtworkChanged = true;
                localMetadataEditor = this;
                break;
            }
        }

        /** @deprecated */
        public MetadataEditor putLong(int paramInt, long paramLong)
            throws IllegalArgumentException
        {
            while (true)
            {
                try
                {
                    if (this.mApplied)
                    {
                        Log.e("RemoteControlClient", "Can't edit a previously applied MetadataEditor");
                        localMetadataEditor = this;
                        return localMetadataEditor;
                    }
                    if (!RemoteControlClient.validTypeForKey(paramInt, RemoteControlClient.METADATA_KEYS_TYPE_LONG))
                        throw new IllegalArgumentException("Invalid type 'long' for key " + paramInt);
                }
                finally
                {
                }
                this.mEditorMetadata.putLong(String.valueOf(paramInt), paramLong);
                this.mMetadataChanged = true;
                MetadataEditor localMetadataEditor = this;
            }
        }

        /** @deprecated */
        public MetadataEditor putString(int paramInt, String paramString)
            throws IllegalArgumentException
        {
            while (true)
            {
                try
                {
                    if (this.mApplied)
                    {
                        Log.e("RemoteControlClient", "Can't edit a previously applied MetadataEditor");
                        localMetadataEditor = this;
                        return localMetadataEditor;
                    }
                    if (!RemoteControlClient.validTypeForKey(paramInt, RemoteControlClient.METADATA_KEYS_TYPE_STRING))
                        throw new IllegalArgumentException("Invalid type 'String' for key " + paramInt);
                }
                finally
                {
                }
                this.mEditorMetadata.putString(String.valueOf(paramInt), paramString);
                this.mMetadataChanged = true;
                MetadataEditor localMetadataEditor = this;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.RemoteControlClient
 * JD-Core Version:        0.6.2
 */