package android.media;

import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.KeyEvent;

public abstract interface IAudioService extends IInterface
{
    public abstract int abandonAudioFocus(IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString)
        throws RemoteException;

    public abstract void adjustLocalOrRemoteStreamVolume(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void adjustMasterVolume(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void adjustStreamVolume(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void adjustSuggestedStreamVolume(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void adjustVolume(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void dispatchMediaKeyEvent(KeyEvent paramKeyEvent)
        throws RemoteException;

    public abstract void dispatchMediaKeyEventUnderWakelock(KeyEvent paramKeyEvent)
        throws RemoteException;

    public abstract void forceVolumeControlStream(int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public abstract int getLastAudibleMasterVolume()
        throws RemoteException;

    public abstract int getLastAudibleStreamVolume(int paramInt)
        throws RemoteException;

    public abstract int getMasterMaxVolume()
        throws RemoteException;

    public abstract int getMasterStreamType()
        throws RemoteException;

    public abstract int getMasterVolume()
        throws RemoteException;

    public abstract int getMode()
        throws RemoteException;

    public abstract int getRemoteStreamMaxVolume()
        throws RemoteException;

    public abstract int getRemoteStreamVolume()
        throws RemoteException;

    public abstract int getRingerMode()
        throws RemoteException;

    public abstract IRingtonePlayer getRingtonePlayer()
        throws RemoteException;

    public abstract int getStreamMaxVolume(int paramInt)
        throws RemoteException;

    public abstract int getStreamVolume(int paramInt)
        throws RemoteException;

    public abstract int getVibrateSetting(int paramInt)
        throws RemoteException;

    public abstract boolean isBluetoothA2dpOn()
        throws RemoteException;

    public abstract boolean isBluetoothScoOn()
        throws RemoteException;

    public abstract boolean isMasterMute()
        throws RemoteException;

    public abstract boolean isSpeakerphoneOn()
        throws RemoteException;

    public abstract boolean isStreamMute(int paramInt)
        throws RemoteException;

    public abstract boolean loadSoundEffects()
        throws RemoteException;

    public abstract void playSoundEffect(int paramInt)
        throws RemoteException;

    public abstract void playSoundEffectVolume(int paramInt, float paramFloat)
        throws RemoteException;

    public abstract void registerMediaButtonEventReceiverForCalls(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void registerMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
        throws RemoteException;

    public abstract int registerRemoteControlClient(PendingIntent paramPendingIntent, IRemoteControlClient paramIRemoteControlClient, String paramString)
        throws RemoteException;

    public abstract void registerRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
        throws RemoteException;

    public abstract void registerRemoteVolumeObserverForRcc(int paramInt, IRemoteVolumeObserver paramIRemoteVolumeObserver)
        throws RemoteException;

    public abstract void reloadAudioSettings()
        throws RemoteException;

    public abstract void remoteControlDisplayUsesBitmapSize(IRemoteControlDisplay paramIRemoteControlDisplay, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract int requestAudioFocus(int paramInt1, int paramInt2, IBinder paramIBinder, IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString1, String paramString2)
        throws RemoteException;

    public abstract int setBluetoothA2dpDeviceConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt)
        throws RemoteException;

    public abstract void setBluetoothA2dpOn(boolean paramBoolean)
        throws RemoteException;

    public abstract void setBluetoothScoOn(boolean paramBoolean)
        throws RemoteException;

    public abstract void setMasterMute(boolean paramBoolean, int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public abstract void setMasterVolume(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setMode(int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public abstract void setPlaybackInfoForRcc(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void setRemoteStreamVolume(int paramInt)
        throws RemoteException;

    public abstract void setRingerMode(int paramInt)
        throws RemoteException;

    public abstract void setRingtonePlayer(IRingtonePlayer paramIRingtonePlayer)
        throws RemoteException;

    public abstract void setSpeakerphoneOn(boolean paramBoolean)
        throws RemoteException;

    public abstract void setStreamMute(int paramInt, boolean paramBoolean, IBinder paramIBinder)
        throws RemoteException;

    public abstract void setStreamSolo(int paramInt, boolean paramBoolean, IBinder paramIBinder)
        throws RemoteException;

    public abstract void setStreamVolume(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void setVibrateSetting(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString)
        throws RemoteException;

    public abstract boolean shouldVibrate(int paramInt)
        throws RemoteException;

    public abstract void startBluetoothSco(IBinder paramIBinder)
        throws RemoteException;

    public abstract AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver paramIAudioRoutesObserver)
        throws RemoteException;

    public abstract void stopBluetoothSco(IBinder paramIBinder)
        throws RemoteException;

    public abstract void unloadSoundEffects()
        throws RemoteException;

    public abstract void unregisterAudioFocusClient(String paramString)
        throws RemoteException;

    public abstract void unregisterMediaButtonEventReceiverForCalls()
        throws RemoteException;

    public abstract void unregisterMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
        throws RemoteException;

    public abstract void unregisterRemoteControlClient(PendingIntent paramPendingIntent, IRemoteControlClient paramIRemoteControlClient)
        throws RemoteException;

    public abstract void unregisterRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAudioService
    {
        private static final String DESCRIPTOR = "android.media.IAudioService";
        static final int TRANSACTION_abandonAudioFocus = 39;
        static final int TRANSACTION_adjustLocalOrRemoteStreamVolume = 2;
        static final int TRANSACTION_adjustMasterVolume = 5;
        static final int TRANSACTION_adjustStreamVolume = 4;
        static final int TRANSACTION_adjustSuggestedStreamVolume = 3;
        static final int TRANSACTION_adjustVolume = 1;
        static final int TRANSACTION_dispatchMediaKeyEvent = 41;
        static final int TRANSACTION_dispatchMediaKeyEventUnderWakelock = 42;
        static final int TRANSACTION_forceVolumeControlStream = 58;
        static final int TRANSACTION_getLastAudibleMasterVolume = 19;
        static final int TRANSACTION_getLastAudibleStreamVolume = 18;
        static final int TRANSACTION_getMasterMaxVolume = 17;
        static final int TRANSACTION_getMasterStreamType = 61;
        static final int TRANSACTION_getMasterVolume = 15;
        static final int TRANSACTION_getMode = 26;
        static final int TRANSACTION_getRemoteStreamMaxVolume = 53;
        static final int TRANSACTION_getRemoteStreamVolume = 54;
        static final int TRANSACTION_getRingerMode = 21;
        static final int TRANSACTION_getRingtonePlayer = 60;
        static final int TRANSACTION_getStreamMaxVolume = 16;
        static final int TRANSACTION_getStreamVolume = 14;
        static final int TRANSACTION_getVibrateSetting = 23;
        static final int TRANSACTION_isBluetoothA2dpOn = 37;
        static final int TRANSACTION_isBluetoothScoOn = 35;
        static final int TRANSACTION_isMasterMute = 13;
        static final int TRANSACTION_isSpeakerphoneOn = 33;
        static final int TRANSACTION_isStreamMute = 11;
        static final int TRANSACTION_loadSoundEffects = 29;
        static final int TRANSACTION_playSoundEffect = 27;
        static final int TRANSACTION_playSoundEffectVolume = 28;
        static final int TRANSACTION_registerMediaButtonEventReceiverForCalls = 45;
        static final int TRANSACTION_registerMediaButtonIntent = 43;
        static final int TRANSACTION_registerRemoteControlClient = 47;
        static final int TRANSACTION_registerRemoteControlDisplay = 49;
        static final int TRANSACTION_registerRemoteVolumeObserverForRcc = 55;
        static final int TRANSACTION_reloadAudioSettings = 31;
        static final int TRANSACTION_remoteControlDisplayUsesBitmapSize = 51;
        static final int TRANSACTION_requestAudioFocus = 38;
        static final int TRANSACTION_setBluetoothA2dpDeviceConnectionState = 63;
        static final int TRANSACTION_setBluetoothA2dpOn = 36;
        static final int TRANSACTION_setBluetoothScoOn = 34;
        static final int TRANSACTION_setMasterMute = 12;
        static final int TRANSACTION_setMasterVolume = 8;
        static final int TRANSACTION_setMode = 25;
        static final int TRANSACTION_setPlaybackInfoForRcc = 52;
        static final int TRANSACTION_setRemoteStreamVolume = 7;
        static final int TRANSACTION_setRingerMode = 20;
        static final int TRANSACTION_setRingtonePlayer = 59;
        static final int TRANSACTION_setSpeakerphoneOn = 32;
        static final int TRANSACTION_setStreamMute = 10;
        static final int TRANSACTION_setStreamSolo = 9;
        static final int TRANSACTION_setStreamVolume = 6;
        static final int TRANSACTION_setVibrateSetting = 22;
        static final int TRANSACTION_setWiredDeviceConnectionState = 62;
        static final int TRANSACTION_shouldVibrate = 24;
        static final int TRANSACTION_startBluetoothSco = 56;
        static final int TRANSACTION_startWatchingRoutes = 64;
        static final int TRANSACTION_stopBluetoothSco = 57;
        static final int TRANSACTION_unloadSoundEffects = 30;
        static final int TRANSACTION_unregisterAudioFocusClient = 40;
        static final int TRANSACTION_unregisterMediaButtonEventReceiverForCalls = 46;
        static final int TRANSACTION_unregisterMediaButtonIntent = 44;
        static final int TRANSACTION_unregisterRemoteControlClient = 48;
        static final int TRANSACTION_unregisterRemoteControlDisplay = 50;

        public Stub()
        {
            attachInterface(this, "android.media.IAudioService");
        }

        public static IAudioService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IAudioService");
                if ((localIInterface != null) && ((localIInterface instanceof IAudioService)))
                    localObject = (IAudioService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.media.IAudioService");
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                adjustVolume(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                adjustLocalOrRemoteStreamVolume(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                adjustSuggestedStreamVolume(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                adjustStreamVolume(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                adjustMasterVolume(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setStreamVolume(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setRemoteStreamVolume(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setMasterVolume(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i23 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0)
                    i = j;
                setStreamSolo(i23, i, paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i22 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0)
                    i = j;
                setStreamMute(i22, i, paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool7 = isStreamMute(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool7)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                int i21;
                for (int i20 = j; ; i21 = 0)
                {
                    setMasterMute(i20, paramParcel1.readInt(), paramParcel1.readStrongBinder());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool6 = isMasterMute();
                paramParcel2.writeNoException();
                if (bool6)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i19 = getStreamVolume(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i19);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i18 = getMasterVolume();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i18);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i17 = getStreamMaxVolume(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i17);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i16 = getMasterMaxVolume();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i16);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i15 = getLastAudibleStreamVolume(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i15);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i14 = getLastAudibleMasterVolume();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i14);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setRingerMode(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i13 = getRingerMode();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i13);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setVibrateSetting(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i12 = getVibrateSetting(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i12);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool5 = shouldVibrate(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool5)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setMode(paramParcel1.readInt(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i11 = getMode();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i11);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                playSoundEffect(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                playSoundEffectVolume(paramParcel1.readInt(), paramParcel1.readFloat());
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool4 = loadSoundEffects();
                paramParcel2.writeNoException();
                if (bool4)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                unloadSoundEffects();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                reloadAudioSettings();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                int i10;
                for (int i9 = j; ; i10 = 0)
                {
                    setSpeakerphoneOn(i9);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool3 = isSpeakerphoneOn();
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                int i8;
                for (int i7 = j; ; i8 = 0)
                {
                    setBluetoothScoOn(i7);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool2 = isBluetoothScoOn();
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                int i6;
                for (int i5 = j; ; i6 = 0)
                {
                    setBluetoothA2dpOn(i5);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                boolean bool1 = isBluetoothA2dpOn();
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i4 = requestAudioFocus(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readStrongBinder(), IAudioFocusDispatcher.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i4);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i3 = abandonAudioFocus(IAudioFocusDispatcher.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i3);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                unregisterAudioFocusClient(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                for (KeyEvent localKeyEvent2 = (KeyEvent)KeyEvent.CREATOR.createFromParcel(paramParcel1); ; localKeyEvent2 = null)
                {
                    dispatchMediaKeyEvent(localKeyEvent2);
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                for (KeyEvent localKeyEvent1 = (KeyEvent)KeyEvent.CREATOR.createFromParcel(paramParcel1); ; localKeyEvent1 = null)
                {
                    dispatchMediaKeyEventUnderWakelock(localKeyEvent1);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                PendingIntent localPendingIntent4;
                if (paramParcel1.readInt() != 0)
                {
                    localPendingIntent4 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1);
                    label1841: if (paramParcel1.readInt() == 0)
                        break label1879;
                }
                label1879: for (ComponentName localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName3 = null)
                {
                    registerMediaButtonIntent(localPendingIntent4, localComponentName3);
                    break;
                    localPendingIntent4 = null;
                    break label1841;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                PendingIntent localPendingIntent3;
                if (paramParcel1.readInt() != 0)
                {
                    localPendingIntent3 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1);
                    label1912: if (paramParcel1.readInt() == 0)
                        break label1950;
                }
                label1950: for (ComponentName localComponentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName2 = null)
                {
                    unregisterMediaButtonIntent(localPendingIntent3, localComponentName2);
                    break;
                    localPendingIntent3 = null;
                    break label1912;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName1 = null)
                {
                    registerMediaButtonEventReceiverForCalls(localComponentName1);
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                unregisterMediaButtonEventReceiverForCalls();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                for (PendingIntent localPendingIntent2 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent2 = null)
                {
                    int i2 = registerRemoteControlClient(localPendingIntent2, IRemoteControlClient.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i2);
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                for (PendingIntent localPendingIntent1 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent1 = null)
                {
                    unregisterRemoteControlClient(localPendingIntent1, IRemoteControlClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                registerRemoteControlDisplay(IRemoteControlDisplay.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                unregisterRemoteControlDisplay(IRemoteControlDisplay.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                remoteControlDisplayUsesBitmapSize(IRemoteControlDisplay.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setPlaybackInfoForRcc(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int i1 = getRemoteStreamMaxVolume();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i1);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                int n = getRemoteStreamVolume();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(n);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                registerRemoteVolumeObserverForRcc(paramParcel1.readInt(), IRemoteVolumeObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                startBluetoothSco(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                stopBluetoothSco(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                forceVolumeControlStream(paramParcel1.readInt(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setRingtonePlayer(IRingtonePlayer.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                IRingtonePlayer localIRingtonePlayer = getRingtonePlayer();
                paramParcel2.writeNoException();
                if (localIRingtonePlayer != null);
                for (IBinder localIBinder = localIRingtonePlayer.asBinder(); ; localIBinder = null)
                {
                    paramParcel2.writeStrongBinder(localIBinder);
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                int m = getMasterStreamType();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                setWiredDeviceConnectionState(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IAudioService");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice = null)
                {
                    int k = setBluetoothA2dpDeviceConnectionState(localBluetoothDevice, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    break;
                }
                paramParcel1.enforceInterface("android.media.IAudioService");
                AudioRoutesInfo localAudioRoutesInfo = startWatchingRoutes(IAudioRoutesObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                if (localAudioRoutesInfo != null)
                {
                    paramParcel2.writeInt(j);
                    localAudioRoutesInfo.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                }
            }
        }

        private static class Proxy
            implements IAudioService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public int abandonAudioFocus(IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramIAudioFocusDispatcher != null)
                    {
                        localIBinder = paramIAudioFocusDispatcher.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(39, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void adjustLocalOrRemoteStreamVolume(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void adjustMasterVolume(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void adjustStreamVolume(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void adjustSuggestedStreamVolume(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void adjustVolume(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dispatchMediaKeyEvent(KeyEvent paramKeyEvent)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    if (paramKeyEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramKeyEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(41, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchMediaKeyEventUnderWakelock(KeyEvent paramKeyEvent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramKeyEvent != null)
                    {
                        localParcel1.writeInt(1);
                        paramKeyEvent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(42, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void forceVolumeControlStream(int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(58, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IAudioService";
            }

            public int getLastAudibleMasterVolume()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getLastAudibleStreamVolume(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getMasterMaxVolume()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getMasterStreamType()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(61, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getMasterVolume()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getMode()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getRemoteStreamMaxVolume()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(53, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getRemoteStreamVolume()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(54, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getRingerMode()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IRingtonePlayer getRingtonePlayer()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(60, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    IRingtonePlayer localIRingtonePlayer = IRingtonePlayer.Stub.asInterface(localParcel2.readStrongBinder());
                    return localIRingtonePlayer;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getStreamMaxVolume(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getStreamVolume(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getVibrateSetting(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isBluetoothA2dpOn()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(37, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isBluetoothScoOn()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isMasterMute()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isSpeakerphoneOn()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isStreamMute(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean loadSoundEffects()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void playSoundEffect(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(27, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void playSoundEffectVolume(int paramInt, float paramFloat)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    localParcel.writeInt(paramInt);
                    localParcel.writeFloat(paramFloat);
                    this.mRemote.transact(28, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void registerMediaButtonEventReceiverForCalls(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    if (paramComponentName != null)
                    {
                        localParcel.writeInt(1);
                        paramComponentName.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(45, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void registerMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.media.IAudioService");
                        if (paramPendingIntent != null)
                        {
                            localParcel.writeInt(1);
                            paramPendingIntent.writeToParcel(localParcel, 0);
                            if (paramComponentName != null)
                            {
                                localParcel.writeInt(1);
                                paramComponentName.writeToParcel(localParcel, 0);
                                this.mRemote.transact(43, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public int registerRemoteControlClient(PendingIntent paramPendingIntent, IRemoteControlClient paramIRemoteControlClient, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.media.IAudioService");
                        if (paramPendingIntent != null)
                        {
                            localParcel1.writeInt(1);
                            paramPendingIntent.writeToParcel(localParcel1, 0);
                            if (paramIRemoteControlClient != null)
                            {
                                localIBinder = paramIRemoteControlClient.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                localParcel1.writeString(paramString);
                                this.mRemote.transact(47, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                return i;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                }
            }

            public void registerRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    if (paramIRemoteControlDisplay != null)
                        localIBinder = paramIRemoteControlDisplay.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(49, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void registerRemoteVolumeObserverForRcc(int paramInt, IRemoteVolumeObserver paramIRemoteVolumeObserver)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    localParcel.writeInt(paramInt);
                    if (paramIRemoteVolumeObserver != null)
                        localIBinder = paramIRemoteVolumeObserver.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(55, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void reloadAudioSettings()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(31, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void remoteControlDisplayUsesBitmapSize(IRemoteControlDisplay paramIRemoteControlDisplay, int paramInt1, int paramInt2)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    if (paramIRemoteControlDisplay != null)
                        localIBinder = paramIRemoteControlDisplay.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(51, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public int requestAudioFocus(int paramInt1, int paramInt2, IBinder paramIBinder, IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramIAudioFocusDispatcher != null)
                    {
                        localIBinder = paramIAudioFocusDispatcher.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        this.mRemote.transact(38, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int setBluetoothA2dpDeviceConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(63, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBluetoothA2dpOn(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(36, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBluetoothScoOn(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(34, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMasterMute(boolean paramBoolean, int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMasterVolume(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMode(int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPlaybackInfoForRcc(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    this.mRemote.transact(52, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setRemoteStreamVolume(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setRingerMode(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setRingtonePlayer(IRingtonePlayer paramIRingtonePlayer)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramIRingtonePlayer != null)
                    {
                        localIBinder = paramIRingtonePlayer.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(59, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setSpeakerphoneOn(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setStreamMute(int paramInt, boolean paramBoolean, IBinder paramIBinder)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setStreamSolo(int paramInt, boolean paramBoolean, IBinder paramIBinder)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setStreamVolume(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setVibrateSetting(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(62, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean shouldVibrate(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startBluetoothSco(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(56, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver paramIAudioRoutesObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    if (paramIAudioRoutesObserver != null);
                    for (IBinder localIBinder = paramIAudioRoutesObserver.asBinder(); ; localIBinder = null)
                    {
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(64, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        if (localParcel2.readInt() == 0)
                            break;
                        localAudioRoutesInfo = (AudioRoutesInfo)AudioRoutesInfo.CREATOR.createFromParcel(localParcel2);
                        return localAudioRoutesInfo;
                    }
                    AudioRoutesInfo localAudioRoutesInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopBluetoothSco(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(57, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unloadSoundEffects()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(30, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void unregisterAudioFocusClient(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IAudioService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(40, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unregisterMediaButtonEventReceiverForCalls()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    this.mRemote.transact(46, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void unregisterMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.media.IAudioService");
                        if (paramPendingIntent != null)
                        {
                            localParcel.writeInt(1);
                            paramPendingIntent.writeToParcel(localParcel, 0);
                            if (paramComponentName != null)
                            {
                                localParcel.writeInt(1);
                                paramComponentName.writeToParcel(localParcel, 0);
                                this.mRemote.transact(44, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public void unregisterRemoteControlClient(PendingIntent paramPendingIntent, IRemoteControlClient paramIRemoteControlClient)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    if (paramPendingIntent != null)
                    {
                        localParcel.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        if (paramIRemoteControlClient != null)
                            localIBinder = paramIRemoteControlClient.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        this.mRemote.transact(48, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void unregisterRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IAudioService");
                    if (paramIRemoteControlDisplay != null)
                        localIBinder = paramIRemoteControlDisplay.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(50, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IAudioService
 * JD-Core Version:        0.6.2
 */