package android.media;

import android.util.Log;

public class MediaActionSound
{
    public static final int FOCUS_COMPLETE = 1;
    private static final int NUM_MEDIA_SOUND_STREAMS = 1;
    public static final int SHUTTER_CLICK = 0;
    private static final String[] SOUND_FILES = arrayOfString;
    private static final int SOUND_NOT_LOADED = -1;
    public static final int START_VIDEO_RECORDING = 2;
    public static final int STOP_VIDEO_RECORDING = 3;
    private static final String TAG = "MediaActionSound";
    private SoundPool.OnLoadCompleteListener mLoadCompleteListener = new SoundPool.OnLoadCompleteListener()
    {
        public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            if (paramAnonymousInt2 == 0)
                if (MediaActionSound.this.mSoundIdToPlay == paramAnonymousInt1)
                {
                    paramAnonymousSoundPool.play(paramAnonymousInt1, 1.0F, 1.0F, 0, 0, 1.0F);
                    MediaActionSound.access$002(MediaActionSound.this, -1);
                }
            while (true)
            {
                return;
                Log.e("MediaActionSound", "Unable to load sound for playback (status: " + paramAnonymousInt2 + ")");
            }
        }
    };
    private int mSoundIdToPlay;
    private int[] mSoundIds;
    private SoundPool mSoundPool = new SoundPool(1, 7, 0);

    static
    {
        String[] arrayOfString = new String[4];
        arrayOfString[0] = "/system/media/audio/ui/camera_click.ogg";
        arrayOfString[1] = "/system/media/audio/ui/camera_focus.ogg";
        arrayOfString[2] = "/system/media/audio/ui/VideoRecord.ogg";
        arrayOfString[3] = "/system/media/audio/ui/VideoRecord.ogg";
    }

    public MediaActionSound()
    {
        this.mSoundPool.setOnLoadCompleteListener(this.mLoadCompleteListener);
        this.mSoundIds = new int[SOUND_FILES.length];
        for (int i = 0; i < this.mSoundIds.length; i++)
            this.mSoundIds[i] = -1;
        this.mSoundIdToPlay = -1;
    }

    /** @deprecated */
    public void load(int paramInt)
    {
        if (paramInt >= 0);
        try
        {
            if (paramInt >= SOUND_FILES.length)
                throw new RuntimeException("Unknown sound requested: " + paramInt);
        }
        finally
        {
        }
        if (this.mSoundIds[paramInt] == -1)
            this.mSoundIds[paramInt] = this.mSoundPool.load(SOUND_FILES[paramInt], 1);
    }

    /** @deprecated */
    public void play(int paramInt)
    {
        if (paramInt >= 0);
        try
        {
            if (paramInt >= SOUND_FILES.length)
                throw new RuntimeException("Unknown sound requested: " + paramInt);
        }
        finally
        {
        }
        if (this.mSoundIds[paramInt] == -1)
        {
            this.mSoundIdToPlay = this.mSoundPool.load(SOUND_FILES[paramInt], 1);
            this.mSoundIds[paramInt] = this.mSoundIdToPlay;
        }
        while (true)
        {
            return;
            this.mSoundPool.play(this.mSoundIds[paramInt], 1.0F, 1.0F, 0, 0, 1.0F);
        }
    }

    public void release()
    {
        if (this.mSoundPool != null)
        {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaActionSound
 * JD-Core Version:        0.6.2
 */