package android.media;

import java.util.UUID;

public final class MediaCrypto
{
    private int mNativeContext;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    public MediaCrypto(UUID paramUUID, byte[] paramArrayOfByte)
        throws MediaCryptoException
    {
        native_setup(getByteArrayFromUUID(paramUUID), paramArrayOfByte);
    }

    private static final byte[] getByteArrayFromUUID(UUID paramUUID)
    {
        long l1 = paramUUID.getMostSignificantBits();
        long l2 = paramUUID.getLeastSignificantBits();
        byte[] arrayOfByte = new byte[16];
        for (int i = 0; i < 8; i++)
        {
            arrayOfByte[i] = ((byte)(int)(l1 >>> 8 * (7 - i)));
            arrayOfByte[(i + 8)] = ((byte)(int)(l2 >>> 8 * (7 - i)));
        }
        return arrayOfByte;
    }

    public static final boolean isCryptoSchemeSupported(UUID paramUUID)
    {
        return isCryptoSchemeSupportedNative(getByteArrayFromUUID(paramUUID));
    }

    private static final native boolean isCryptoSchemeSupportedNative(byte[] paramArrayOfByte);

    private final native void native_finalize();

    private static final native void native_init();

    private final native void native_setup(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        throws MediaCryptoException;

    protected void finalize()
    {
        native_finalize();
    }

    public final native void release();

    public final native boolean requiresSecureDecoderComponent(String paramString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaCrypto
 * JD-Core Version:        0.6.2
 */