package android.media;

import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Surface;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class MediaRecorder
{
    public static final int MEDIA_RECORDER_ERROR_UNKNOWN = 1;
    public static final int MEDIA_RECORDER_INFO_MAX_DURATION_REACHED = 800;
    public static final int MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED = 801;
    public static final int MEDIA_RECORDER_INFO_UNKNOWN = 1;
    public static final int MEDIA_RECORDER_TRACK_INFO_COMPLETION_STATUS = 1000;
    public static final int MEDIA_RECORDER_TRACK_INFO_DATA_KBYTES = 1009;
    public static final int MEDIA_RECORDER_TRACK_INFO_DURATION_MS = 1003;
    public static final int MEDIA_RECORDER_TRACK_INFO_ENCODED_FRAMES = 1005;
    public static final int MEDIA_RECORDER_TRACK_INFO_INITIAL_DELAY_MS = 1007;
    public static final int MEDIA_RECORDER_TRACK_INFO_LIST_END = 2000;
    public static final int MEDIA_RECORDER_TRACK_INFO_LIST_START = 1000;
    public static final int MEDIA_RECORDER_TRACK_INFO_MAX_CHUNK_DUR_MS = 1004;
    public static final int MEDIA_RECORDER_TRACK_INFO_PROGRESS_IN_TIME = 1001;
    public static final int MEDIA_RECORDER_TRACK_INFO_START_OFFSET_MS = 1008;
    public static final int MEDIA_RECORDER_TRACK_INFO_TYPE = 1002;
    public static final int MEDIA_RECORDER_TRACK_INTER_CHUNK_TIME_MS = 1006;
    private static final String TAG = "MediaRecorder";
    private EventHandler mEventHandler;
    private FileDescriptor mFd;
    private int mNativeContext;
    private OnErrorListener mOnErrorListener;
    private OnInfoListener mOnInfoListener;
    private String mPath;
    private Surface mSurface;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    public MediaRecorder()
    {
        Looper localLooper1 = Looper.myLooper();
        if (localLooper1 != null)
            this.mEventHandler = new EventHandler(this, localLooper1);
        while (true)
        {
            native_setup(new WeakReference(this));
            return;
            Looper localLooper2 = Looper.getMainLooper();
            if (localLooper2 != null)
                this.mEventHandler = new EventHandler(this, localLooper2);
            else
                this.mEventHandler = null;
        }
    }

    private native void _prepare()
        throws IllegalStateException, IOException;

    private native void _setOutputFile(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2)
        throws IllegalStateException, IOException;

    public static final int getAudioSourceMax()
    {
        return 7;
    }

    private final native void native_finalize();

    private static final native void native_init();

    private native void native_reset();

    private final native void native_setup(Object paramObject)
        throws IllegalStateException;

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        MediaRecorder localMediaRecorder = (MediaRecorder)((WeakReference)paramObject1).get();
        if (localMediaRecorder == null);
        while (true)
        {
            return;
            if (localMediaRecorder.mEventHandler != null)
            {
                Message localMessage = localMediaRecorder.mEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localMediaRecorder.mEventHandler.sendMessage(localMessage);
            }
        }
    }

    private native void setParameter(String paramString);

    protected void finalize()
    {
        native_finalize();
    }

    public native int getMaxAmplitude()
        throws IllegalStateException;

    public void prepare()
        throws IllegalStateException, IOException
    {
        FileOutputStream localFileOutputStream;
        if (this.mPath != null)
            localFileOutputStream = new FileOutputStream(this.mPath);
        while (true)
        {
            try
            {
                _setOutputFile(localFileOutputStream.getFD(), 0L, 0L);
                localFileOutputStream.close();
                _prepare();
                return;
            }
            finally
            {
                localFileOutputStream.close();
            }
            if (this.mFd == null)
                break;
            _setOutputFile(this.mFd, 0L, 0L);
        }
        throw new IOException("No valid output file");
    }

    public native void release();

    public void reset()
    {
        native_reset();
        this.mEventHandler.removeCallbacksAndMessages(null);
    }

    public void setAudioChannels(int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("Number of channels is not positive");
        setParameter("audio-param-number-of-channels=" + paramInt);
    }

    public native void setAudioEncoder(int paramInt)
        throws IllegalStateException;

    public void setAudioEncodingBitRate(int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("Audio encoding bit rate is not positive");
        setParameter("audio-param-encoding-bitrate=" + paramInt);
    }

    public void setAudioSamplingRate(int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("Audio sampling rate is not positive");
        setParameter("audio-param-sampling-rate=" + paramInt);
    }

    public native void setAudioSource(int paramInt)
        throws IllegalStateException;

    public void setAuxiliaryOutputFile(FileDescriptor paramFileDescriptor)
    {
        Log.w("MediaRecorder", "setAuxiliaryOutputFile(FileDescriptor) is no longer supported.");
    }

    public void setAuxiliaryOutputFile(String paramString)
    {
        Log.w("MediaRecorder", "setAuxiliaryOutputFile(String) is no longer supported.");
    }

    public native void setCamera(Camera paramCamera);

    public void setCaptureRate(double paramDouble)
    {
        setParameter(String.format("time-lapse-enable=1", new Object[0]));
        int i = (int)(1000.0D * (1.0D / paramDouble));
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(i);
        setParameter(String.format("time-between-time-lapse-frame-capture=%d", arrayOfObject));
    }

    public void setLocation(float paramFloat1, float paramFloat2)
    {
        int i = (int)(0.5D + paramFloat1 * 10000.0F);
        int j = (int)(0.5D + paramFloat2 * 10000.0F);
        if ((i > 900000) || (i < -900000))
            throw new IllegalArgumentException("Latitude: " + paramFloat1 + " out of range.");
        if ((j > 1800000) || (j < -1800000))
            throw new IllegalArgumentException("Longitude: " + paramFloat2 + " out of range");
        setParameter("param-geotag-latitude=" + i);
        setParameter("param-geotag-longitude=" + j);
    }

    public native void setMaxDuration(int paramInt)
        throws IllegalArgumentException;

    public native void setMaxFileSize(long paramLong)
        throws IllegalArgumentException;

    public void setOnErrorListener(OnErrorListener paramOnErrorListener)
    {
        this.mOnErrorListener = paramOnErrorListener;
    }

    public void setOnInfoListener(OnInfoListener paramOnInfoListener)
    {
        this.mOnInfoListener = paramOnInfoListener;
    }

    public void setOrientationHint(int paramInt)
    {
        if ((paramInt != 0) && (paramInt != 90) && (paramInt != 180) && (paramInt != 270))
            throw new IllegalArgumentException("Unsupported angle: " + paramInt);
        setParameter("video-param-rotation-angle-degrees=" + paramInt);
    }

    public void setOutputFile(FileDescriptor paramFileDescriptor)
        throws IllegalStateException
    {
        this.mPath = null;
        this.mFd = paramFileDescriptor;
    }

    public void setOutputFile(String paramString)
        throws IllegalStateException
    {
        this.mFd = null;
        this.mPath = paramString;
    }

    public native void setOutputFormat(int paramInt)
        throws IllegalStateException;

    public void setPreviewDisplay(Surface paramSurface)
    {
        this.mSurface = paramSurface;
    }

    public void setProfile(CamcorderProfile paramCamcorderProfile)
    {
        setOutputFormat(paramCamcorderProfile.fileFormat);
        setVideoFrameRate(paramCamcorderProfile.videoFrameRate);
        setVideoSize(paramCamcorderProfile.videoFrameWidth, paramCamcorderProfile.videoFrameHeight);
        setVideoEncodingBitRate(paramCamcorderProfile.videoBitRate);
        setVideoEncoder(paramCamcorderProfile.videoCodec);
        if ((paramCamcorderProfile.quality >= 1000) && (paramCamcorderProfile.quality <= 1007));
        while (true)
        {
            return;
            setAudioEncodingBitRate(paramCamcorderProfile.audioBitRate);
            setAudioChannels(paramCamcorderProfile.audioChannels);
            setAudioSamplingRate(paramCamcorderProfile.audioSampleRate);
            setAudioEncoder(paramCamcorderProfile.audioCodec);
        }
    }

    public native void setVideoEncoder(int paramInt)
        throws IllegalStateException;

    public void setVideoEncodingBitRate(int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("Video encoding bit rate is not positive");
        setParameter("video-param-encoding-bitrate=" + paramInt);
    }

    public native void setVideoFrameRate(int paramInt)
        throws IllegalStateException;

    public native void setVideoSize(int paramInt1, int paramInt2)
        throws IllegalStateException;

    public native void setVideoSource(int paramInt)
        throws IllegalStateException;

    public native void start()
        throws IllegalStateException;

    public native void stop()
        throws IllegalStateException;

    private class EventHandler extends Handler
    {
        private static final int MEDIA_RECORDER_EVENT_ERROR = 1;
        private static final int MEDIA_RECORDER_EVENT_INFO = 2;
        private static final int MEDIA_RECORDER_EVENT_LIST_END = 99;
        private static final int MEDIA_RECORDER_EVENT_LIST_START = 1;
        private static final int MEDIA_RECORDER_TRACK_EVENT_ERROR = 100;
        private static final int MEDIA_RECORDER_TRACK_EVENT_INFO = 101;
        private static final int MEDIA_RECORDER_TRACK_EVENT_LIST_END = 1000;
        private static final int MEDIA_RECORDER_TRACK_EVENT_LIST_START = 100;
        private MediaRecorder mMediaRecorder;

        public EventHandler(MediaRecorder paramLooper, Looper arg3)
        {
            super();
            this.mMediaRecorder = paramLooper;
        }

        public void handleMessage(Message paramMessage)
        {
            if (this.mMediaRecorder.mNativeContext == 0)
                Log.w("MediaRecorder", "mediarecorder went away with unhandled events");
            while (true)
            {
                return;
                switch (paramMessage.what)
                {
                default:
                    Log.e("MediaRecorder", "Unknown message type " + paramMessage.what);
                    break;
                case 1:
                case 100:
                    if (MediaRecorder.this.mOnErrorListener != null)
                        MediaRecorder.this.mOnErrorListener.onError(this.mMediaRecorder, paramMessage.arg1, paramMessage.arg2);
                    break;
                case 2:
                case 101:
                    if (MediaRecorder.this.mOnInfoListener != null)
                        MediaRecorder.this.mOnInfoListener.onInfo(this.mMediaRecorder, paramMessage.arg1, paramMessage.arg2);
                    break;
                }
            }
        }
    }

    public static abstract interface OnInfoListener
    {
        public abstract void onInfo(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2);
    }

    public static abstract interface OnErrorListener
    {
        public abstract void onError(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2);
    }

    public final class VideoEncoder
    {
        public static final int DEFAULT = 0;
        public static final int H263 = 1;
        public static final int H264 = 2;
        public static final int MPEG_4_SP = 3;

        private VideoEncoder()
        {
        }
    }

    public final class AudioEncoder
    {
        public static final int AAC = 3;
        public static final int AAC_ELD = 5;
        public static final int AMR_NB = 1;
        public static final int AMR_WB = 2;
        public static final int DEFAULT = 0;
        public static final int HE_AAC = 4;

        private AudioEncoder()
        {
        }
    }

    public final class OutputFormat
    {
        public static final int AAC_ADIF = 5;
        public static final int AAC_ADTS = 6;
        public static final int AMR_NB = 3;
        public static final int AMR_WB = 4;
        public static final int DEFAULT = 0;
        public static final int MPEG_4 = 2;
        public static final int OUTPUT_FORMAT_MPEG2TS = 8;
        public static final int OUTPUT_FORMAT_RTP_AVP = 7;
        public static final int RAW_AMR = 3;
        public static final int THREE_GPP = 1;

        private OutputFormat()
        {
        }
    }

    public final class VideoSource
    {
        public static final int CAMERA = 1;
        public static final int DEFAULT = 0;
        public static final int GRALLOC_BUFFER = 2;

        private VideoSource()
        {
        }
    }

    public final class AudioSource
    {
        public static final int CAMCORDER = 5;
        public static final int DEFAULT = 0;
        public static final int MIC = 1;
        public static final int VOICE_CALL = 4;
        public static final int VOICE_COMMUNICATION = 7;
        public static final int VOICE_DOWNLINK = 3;
        public static final int VOICE_RECOGNITION = 6;
        public static final int VOICE_UPLINK = 2;

        private AudioSource()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaRecorder
 * JD-Core Version:        0.6.2
 */