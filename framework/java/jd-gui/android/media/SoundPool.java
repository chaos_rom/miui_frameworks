package android.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.util.AndroidRuntimeException;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class SoundPool
{
    private static final boolean DEBUG = false;
    private static final int SAMPLE_LOADED = 1;
    private static final String TAG = "SoundPool";
    private EventHandler mEventHandler;
    private final Object mLock;
    private int mNativeContext;
    private OnLoadCompleteListener mOnLoadCompleteListener;

    static
    {
        System.loadLibrary("soundpool");
    }

    public SoundPool(int paramInt1, int paramInt2, int paramInt3)
    {
        if (native_setup(new WeakReference(this), paramInt1, paramInt2, paramInt3) != 0)
            throw new RuntimeException("Native setup failed");
        this.mLock = new Object();
    }

    private final native int _load(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, int paramInt);

    private final native int _load(String paramString, int paramInt);

    private final native int native_setup(Object paramObject, int paramInt1, int paramInt2, int paramInt3);

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        SoundPool localSoundPool = (SoundPool)((WeakReference)paramObject1).get();
        if (localSoundPool == null);
        while (true)
        {
            return;
            if (localSoundPool.mEventHandler != null)
            {
                Message localMessage = localSoundPool.mEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localSoundPool.mEventHandler.sendMessage(localMessage);
            }
        }
    }

    public final native void autoPause();

    public final native void autoResume();

    protected void finalize()
    {
        release();
    }

    public int load(Context paramContext, int paramInt1, int paramInt2)
    {
        AssetFileDescriptor localAssetFileDescriptor = paramContext.getResources().openRawResourceFd(paramInt1);
        int i = 0;
        if (localAssetFileDescriptor != null)
            i = _load(localAssetFileDescriptor.getFileDescriptor(), localAssetFileDescriptor.getStartOffset(), localAssetFileDescriptor.getLength(), paramInt2);
        try
        {
            localAssetFileDescriptor.close();
            label45: return i;
        }
        catch (IOException localIOException)
        {
            break label45;
        }
    }

    public int load(AssetFileDescriptor paramAssetFileDescriptor, int paramInt)
    {
        long l;
        if (paramAssetFileDescriptor != null)
        {
            l = paramAssetFileDescriptor.getLength();
            if (l < 0L)
                throw new AndroidRuntimeException("no length for fd");
        }
        for (int i = _load(paramAssetFileDescriptor.getFileDescriptor(), paramAssetFileDescriptor.getStartOffset(), l, paramInt); ; i = 0)
            return i;
    }

    public int load(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, int paramInt)
    {
        return _load(paramFileDescriptor, paramLong1, paramLong2, paramInt);
    }

    public int load(String paramString, int paramInt)
    {
        int i;
        if (paramString.startsWith("http:"))
            i = _load(paramString, paramInt);
        while (true)
        {
            return i;
            i = 0;
            try
            {
                File localFile = new File(paramString);
                ParcelFileDescriptor localParcelFileDescriptor = ParcelFileDescriptor.open(localFile, 268435456);
                if (localParcelFileDescriptor != null)
                {
                    i = _load(localParcelFileDescriptor.getFileDescriptor(), 0L, localFile.length(), paramInt);
                    localParcelFileDescriptor.close();
                }
            }
            catch (IOException localIOException)
            {
                Log.e("SoundPool", "error loading " + paramString);
            }
        }
    }

    public final native void pause(int paramInt);

    public final native int play(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2, int paramInt3, float paramFloat3);

    public final native void release();

    public final native void resume(int paramInt);

    public final native void setLoop(int paramInt1, int paramInt2);

    // ERROR //
    public void setOnLoadCompleteListener(OnLoadCompleteListener paramOnLoadCompleteListener)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 58	android/media/SoundPool:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_1
        //     8: ifnull +77 -> 85
        //     11: invokestatic 188	android/os/Looper:myLooper	()Landroid/os/Looper;
        //     14: astore 4
        //     16: aload 4
        //     18: ifnull +26 -> 44
        //     21: aload_0
        //     22: new 6	android/media/SoundPool$EventHandler
        //     25: dup
        //     26: aload_0
        //     27: aload_0
        //     28: aload 4
        //     30: invokespecial 191	android/media/SoundPool$EventHandler:<init>	(Landroid/media/SoundPool;Landroid/media/SoundPool;Landroid/os/Looper;)V
        //     33: putfield 75	android/media/SoundPool:mEventHandler	Landroid/media/SoundPool$EventHandler;
        //     36: aload_0
        //     37: aload_1
        //     38: putfield 67	android/media/SoundPool:mOnLoadCompleteListener	Landroid/media/SoundPool$OnLoadCompleteListener;
        //     41: aload_2
        //     42: monitorexit
        //     43: return
        //     44: invokestatic 194	android/os/Looper:getMainLooper	()Landroid/os/Looper;
        //     47: astore 5
        //     49: aload 5
        //     51: ifnull +26 -> 77
        //     54: aload_0
        //     55: new 6	android/media/SoundPool$EventHandler
        //     58: dup
        //     59: aload_0
        //     60: aload_0
        //     61: aload 5
        //     63: invokespecial 191	android/media/SoundPool$EventHandler:<init>	(Landroid/media/SoundPool;Landroid/media/SoundPool;Landroid/os/Looper;)V
        //     66: putfield 75	android/media/SoundPool:mEventHandler	Landroid/media/SoundPool$EventHandler;
        //     69: goto -33 -> 36
        //     72: astore_3
        //     73: aload_2
        //     74: monitorexit
        //     75: aload_3
        //     76: athrow
        //     77: aload_0
        //     78: aconst_null
        //     79: putfield 75	android/media/SoundPool:mEventHandler	Landroid/media/SoundPool$EventHandler;
        //     82: goto -46 -> 36
        //     85: aload_0
        //     86: aconst_null
        //     87: putfield 75	android/media/SoundPool:mEventHandler	Landroid/media/SoundPool$EventHandler;
        //     90: goto -54 -> 36
        //
        // Exception table:
        //     from	to	target	type
        //     11	75	72	finally
        //     77	90	72	finally
    }

    public final native void setPriority(int paramInt1, int paramInt2);

    public final native void setRate(int paramInt, float paramFloat);

    public final native void setVolume(int paramInt, float paramFloat1, float paramFloat2);

    public final native void stop(int paramInt);

    public final native boolean unload(int paramInt);

    private class EventHandler extends Handler
    {
        private SoundPool mSoundPool;

        public EventHandler(SoundPool paramLooper, Looper arg3)
        {
            super();
            this.mSoundPool = paramLooper;
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.e("SoundPool", "Unknown message type " + paramMessage.what);
            case 1:
            }
            while (true)
            {
                return;
                synchronized (SoundPool.this.mLock)
                {
                    if (SoundPool.this.mOnLoadCompleteListener != null)
                        SoundPool.this.mOnLoadCompleteListener.onLoadComplete(this.mSoundPool, paramMessage.arg1, paramMessage.arg2);
                }
            }
        }
    }

    public static abstract interface OnLoadCompleteListener
    {
        public abstract void onLoadComplete(SoundPool paramSoundPool, int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.SoundPool
 * JD-Core Version:        0.6.2
 */