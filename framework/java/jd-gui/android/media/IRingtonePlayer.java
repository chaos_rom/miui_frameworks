package android.media;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IRingtonePlayer extends IInterface
{
    public abstract boolean isPlaying(IBinder paramIBinder)
        throws RemoteException;

    public abstract void play(IBinder paramIBinder, Uri paramUri, int paramInt)
        throws RemoteException;

    public abstract void playAsync(Uri paramUri, boolean paramBoolean, int paramInt)
        throws RemoteException;

    public abstract void stop(IBinder paramIBinder)
        throws RemoteException;

    public abstract void stopAsync()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRingtonePlayer
    {
        private static final String DESCRIPTOR = "android.media.IRingtonePlayer";
        static final int TRANSACTION_isPlaying = 3;
        static final int TRANSACTION_play = 1;
        static final int TRANSACTION_playAsync = 4;
        static final int TRANSACTION_stop = 2;
        static final int TRANSACTION_stopAsync = 5;

        public Stub()
        {
            attachInterface(this, "android.media.IRingtonePlayer");
        }

        public static IRingtonePlayer asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IRingtonePlayer");
                if ((localIInterface != null) && ((localIInterface instanceof IRingtonePlayer)))
                    localObject = (IRingtonePlayer)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.media.IRingtonePlayer");
                continue;
                paramParcel1.enforceInterface("android.media.IRingtonePlayer");
                IBinder localIBinder = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (Uri localUri2 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1); ; localUri2 = null)
                {
                    play(localIBinder, localUri2, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.media.IRingtonePlayer");
                stop(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IRingtonePlayer");
                boolean bool2 = isPlaying(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.media.IRingtonePlayer");
                Uri localUri1;
                if (paramParcel1.readInt() != 0)
                {
                    localUri1 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    label231: if (paramParcel1.readInt() == 0)
                        break label267;
                }
                label267: for (boolean bool1 = j; ; bool1 = false)
                {
                    playAsync(localUri1, bool1, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                    localUri1 = null;
                    break label231;
                }
                paramParcel1.enforceInterface("android.media.IRingtonePlayer");
                stopAsync();
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IRingtonePlayer
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IRingtonePlayer";
            }

            public boolean isPlaying(IBinder paramIBinder)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IRingtonePlayer");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void play(IBinder paramIBinder, Uri paramUri, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IRingtonePlayer");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramUri != null)
                    {
                        localParcel1.writeInt(1);
                        paramUri.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void playAsync(Uri paramUri, boolean paramBoolean, int paramInt)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.media.IRingtonePlayer");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            break label115;
                            localParcel1.writeInt(i);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(4, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label115: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void stop(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IRingtonePlayer");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopAsync()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IRingtonePlayer");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IRingtonePlayer
 * JD-Core Version:        0.6.2
 */