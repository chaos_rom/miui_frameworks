package android.media;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class MiniThumbFile
{
    public static final int BYTES_PER_MINTHUMB = 10000;
    private static final int HEADER_SIZE = 13;
    private static final int MINI_THUMB_DATA_FILE_VERSION = 3;
    private static final String TAG = "MiniThumbFile";
    private static final Hashtable<String, MiniThumbFile> sThumbFiles = new Hashtable();
    private ByteBuffer mBuffer;
    private FileChannel mChannel;
    private RandomAccessFile mMiniThumbFile;
    private Uri mUri;

    public MiniThumbFile(Uri paramUri)
    {
        this.mUri = paramUri;
        this.mBuffer = ByteBuffer.allocateDirect(10000);
    }

    /** @deprecated */
    public static MiniThumbFile instance(Uri paramUri)
    {
        try
        {
            String str = (String)paramUri.getPathSegments().get(1);
            MiniThumbFile localMiniThumbFile = (MiniThumbFile)sThumbFiles.get(str);
            if (localMiniThumbFile == null)
            {
                localMiniThumbFile = new MiniThumbFile(Uri.parse("content://media/external/" + str + "/media"));
                sThumbFiles.put(str, localMiniThumbFile);
            }
            return localMiniThumbFile;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private RandomAccessFile miniThumbDataFile()
    {
        File localFile2;
        if (this.mMiniThumbFile == null)
        {
            removeOldFile();
            String str = randomAccessFilePath(3);
            File localFile1 = new File(str).getParentFile();
            if ((!localFile1.isDirectory()) && (!localFile1.mkdirs()))
                Log.e("MiniThumbFile", "Unable to create .thumbnails directory " + localFile1.toString());
            localFile2 = new File(str);
        }
        try
        {
            this.mMiniThumbFile = new RandomAccessFile(localFile2, "rw");
            if (this.mMiniThumbFile != null)
                this.mChannel = this.mMiniThumbFile.getChannel();
            return this.mMiniThumbFile;
        }
        catch (IOException localIOException1)
        {
            while (true)
                try
                {
                    this.mMiniThumbFile = new RandomAccessFile(localFile2, "r");
                }
                catch (IOException localIOException2)
                {
                }
        }
    }

    private String randomAccessFilePath(int paramInt)
    {
        String str = Environment.getExternalStorageDirectory().toString() + "/DCIM/.thumbnails";
        return str + "/.thumbdata" + paramInt + "-" + this.mUri.hashCode();
    }

    private void removeOldFile()
    {
        File localFile = new File(randomAccessFilePath(2));
        if (localFile.exists());
        try
        {
            localFile.delete();
            label25: return;
        }
        catch (SecurityException localSecurityException)
        {
            break label25;
        }
    }

    /** @deprecated */
    public static void reset()
    {
        try
        {
            Iterator localIterator = sThumbFiles.values().iterator();
            while (localIterator.hasNext())
                ((MiniThumbFile)localIterator.next()).deactivate();
        }
        finally
        {
        }
        sThumbFiles.clear();
    }

    /** @deprecated */
    // ERROR //
    public void deactivate()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 97	android/media/MiniThumbFile:mMiniThumbFile	Ljava/io/RandomAccessFile;
        //     6: astore_2
        //     7: aload_2
        //     8: ifnull +15 -> 23
        //     11: aload_0
        //     12: getfield 97	android/media/MiniThumbFile:mMiniThumbFile	Ljava/io/RandomAccessFile;
        //     15: invokevirtual 199	java/io/RandomAccessFile:close	()V
        //     18: aload_0
        //     19: aconst_null
        //     20: putfield 97	android/media/MiniThumbFile:mMiniThumbFile	Ljava/io/RandomAccessFile;
        //     23: aload_0
        //     24: monitorexit
        //     25: return
        //     26: astore_1
        //     27: aload_0
        //     28: monitorexit
        //     29: aload_1
        //     30: athrow
        //     31: astore_3
        //     32: goto -9 -> 23
        //
        // Exception table:
        //     from	to	target	type
        //     2	7	26	finally
        //     11	23	26	finally
        //     11	23	31	java/io/IOException
    }

    /** @deprecated */
    // ERROR //
    public long getMagic(long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokespecial 205	android/media/MiniThumbFile:miniThumbDataFile	()Ljava/io/RandomAccessFile;
        //     6: astore 4
        //     8: aload 4
        //     10: ifnull +134 -> 144
        //     13: lload_1
        //     14: ldc2_w 206
        //     17: lmul
        //     18: lstore 7
        //     20: aconst_null
        //     21: astore 9
        //     23: aload_0
        //     24: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     27: invokevirtual 210	java/nio/ByteBuffer:clear	()Ljava/nio/Buffer;
        //     30: pop
        //     31: aload_0
        //     32: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     35: bipush 9
        //     37: invokevirtual 214	java/nio/ByteBuffer:limit	(I)Ljava/nio/Buffer;
        //     40: pop
        //     41: aload_0
        //     42: getfield 142	android/media/MiniThumbFile:mChannel	Ljava/nio/channels/FileChannel;
        //     45: lload 7
        //     47: ldc2_w 215
        //     50: iconst_1
        //     51: invokevirtual 222	java/nio/channels/FileChannel:lock	(JJZ)Ljava/nio/channels/FileLock;
        //     54: astore 9
        //     56: aload_0
        //     57: getfield 142	android/media/MiniThumbFile:mChannel	Ljava/nio/channels/FileChannel;
        //     60: aload_0
        //     61: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     64: lload 7
        //     66: invokevirtual 226	java/nio/channels/FileChannel:read	(Ljava/nio/ByteBuffer;J)I
        //     69: bipush 9
        //     71: if_icmpne +167 -> 238
        //     74: aload_0
        //     75: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     78: iconst_0
        //     79: invokevirtual 229	java/nio/ByteBuffer:position	(I)Ljava/nio/Buffer;
        //     82: pop
        //     83: aload_0
        //     84: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     87: invokevirtual 232	java/nio/ByteBuffer:get	()B
        //     90: iconst_1
        //     91: if_icmpne +147 -> 238
        //     94: aload_0
        //     95: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     98: invokevirtual 236	java/nio/ByteBuffer:getLong	()J
        //     101: lstore 20
        //     103: lload 20
        //     105: lstore 5
        //     107: aload 9
        //     109: ifnull +8 -> 117
        //     112: aload 9
        //     114: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     117: aload_0
        //     118: monitorexit
        //     119: lload 5
        //     121: lreturn
        //     122: astore 15
        //     124: ldc 15
        //     126: ldc 243
        //     128: aload 15
        //     130: invokestatic 247	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     133: pop
        //     134: aload 9
        //     136: ifnull +8 -> 144
        //     139: aload 9
        //     141: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     144: lconst_0
        //     145: lstore 5
        //     147: goto -30 -> 117
        //     150: astore 12
        //     152: ldc 15
        //     154: new 68	java/lang/StringBuilder
        //     157: dup
        //     158: invokespecial 69	java/lang/StringBuilder:<init>	()V
        //     161: ldc 249
        //     163: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     166: lload_1
        //     167: invokevirtual 252	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     170: ldc 254
        //     172: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     175: aload 12
        //     177: invokevirtual 258	java/lang/Object:getClass	()Ljava/lang/Class;
        //     180: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     183: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     186: invokestatic 129	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     189: pop
        //     190: aload 9
        //     192: ifnull -48 -> 144
        //     195: aload 9
        //     197: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     200: goto -56 -> 144
        //     203: astore 14
        //     205: goto -61 -> 144
        //     208: astore 10
        //     210: aload 9
        //     212: ifnull +8 -> 220
        //     215: aload 9
        //     217: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     220: aload 10
        //     222: athrow
        //     223: astore_3
        //     224: aload_0
        //     225: monitorexit
        //     226: aload_3
        //     227: athrow
        //     228: astore 11
        //     230: goto -10 -> 220
        //     233: astore 22
        //     235: goto -118 -> 117
        //     238: aload 9
        //     240: ifnull -96 -> 144
        //     243: aload 9
        //     245: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     248: goto -104 -> 144
        //
        // Exception table:
        //     from	to	target	type
        //     23	103	122	java/io/IOException
        //     23	103	150	java/lang/RuntimeException
        //     139	144	203	java/io/IOException
        //     195	200	203	java/io/IOException
        //     243	248	203	java/io/IOException
        //     23	103	208	finally
        //     124	134	208	finally
        //     152	190	208	finally
        //     2	8	223	finally
        //     112	117	223	finally
        //     139	144	223	finally
        //     195	200	223	finally
        //     215	220	223	finally
        //     220	223	223	finally
        //     243	248	223	finally
        //     215	220	228	java/io/IOException
        //     112	117	233	java/io/IOException
    }

    /** @deprecated */
    // ERROR //
    public byte[] getMiniThumbFromFile(long paramLong, byte[] paramArrayOfByte)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokespecial 205	android/media/MiniThumbFile:miniThumbDataFile	()Ljava/io/RandomAccessFile;
        //     6: astore 5
        //     8: aload 5
        //     10: ifnonnull +9 -> 19
        //     13: aconst_null
        //     14: astore_3
        //     15: aload_0
        //     16: monitorexit
        //     17: aload_3
        //     18: areturn
        //     19: lload_1
        //     20: ldc2_w 206
        //     23: lmul
        //     24: lstore 6
        //     26: aconst_null
        //     27: astore 8
        //     29: aload_0
        //     30: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     33: invokevirtual 210	java/nio/ByteBuffer:clear	()Ljava/nio/Buffer;
        //     36: pop
        //     37: aload_0
        //     38: getfield 142	android/media/MiniThumbFile:mChannel	Ljava/nio/channels/FileChannel;
        //     41: lload 6
        //     43: ldc2_w 206
        //     46: iconst_1
        //     47: invokevirtual 222	java/nio/channels/FileChannel:lock	(JJZ)Ljava/nio/channels/FileLock;
        //     50: astore 8
        //     52: aload_0
        //     53: getfield 142	android/media/MiniThumbFile:mChannel	Ljava/nio/channels/FileChannel;
        //     56: aload_0
        //     57: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     60: lload 6
        //     62: invokevirtual 226	java/nio/channels/FileChannel:read	(Ljava/nio/ByteBuffer;J)I
        //     65: istore 17
        //     67: iload 17
        //     69: bipush 13
        //     71: if_icmple +224 -> 295
        //     74: aload_0
        //     75: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     78: iconst_0
        //     79: invokevirtual 229	java/nio/ByteBuffer:position	(I)Ljava/nio/Buffer;
        //     82: pop
        //     83: aload_0
        //     84: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     87: invokevirtual 232	java/nio/ByteBuffer:get	()B
        //     90: pop
        //     91: aload_0
        //     92: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     95: invokevirtual 236	java/nio/ByteBuffer:getLong	()J
        //     98: pop2
        //     99: aload_0
        //     100: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     103: invokevirtual 266	java/nio/ByteBuffer:getInt	()I
        //     106: istore 22
        //     108: iload 17
        //     110: iload 22
        //     112: bipush 13
        //     114: iadd
        //     115: if_icmplt +180 -> 295
        //     118: aload_3
        //     119: arraylength
        //     120: iload 22
        //     122: if_icmplt +173 -> 295
        //     125: aload_0
        //     126: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     129: aload_3
        //     130: iconst_0
        //     131: iload 22
        //     133: invokevirtual 269	java/nio/ByteBuffer:get	([BII)Ljava/nio/ByteBuffer;
        //     136: pop
        //     137: aload 8
        //     139: ifnull -124 -> 15
        //     142: aload 8
        //     144: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     147: goto -132 -> 15
        //     150: astore 24
        //     152: goto -137 -> 15
        //     155: astore 14
        //     157: ldc 15
        //     159: new 68	java/lang/StringBuilder
        //     162: dup
        //     163: invokespecial 69	java/lang/StringBuilder:<init>	()V
        //     166: ldc_w 271
        //     169: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     172: lload_1
        //     173: invokevirtual 252	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     176: ldc_w 273
        //     179: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     182: aload 14
        //     184: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     187: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     190: invokestatic 276	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     193: pop
        //     194: aload 8
        //     196: ifnull +8 -> 204
        //     199: aload 8
        //     201: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     204: aconst_null
        //     205: astore_3
        //     206: goto -191 -> 15
        //     209: astore 11
        //     211: ldc 15
        //     213: new 68	java/lang/StringBuilder
        //     216: dup
        //     217: invokespecial 69	java/lang/StringBuilder:<init>	()V
        //     220: ldc_w 278
        //     223: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: lload_1
        //     227: invokevirtual 252	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     230: ldc 254
        //     232: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     235: aload 11
        //     237: invokevirtual 258	java/lang/Object:getClass	()Ljava/lang/Class;
        //     240: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     243: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     246: invokestatic 129	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     249: pop
        //     250: aload 8
        //     252: ifnull -48 -> 204
        //     255: aload 8
        //     257: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     260: goto -56 -> 204
        //     263: astore 13
        //     265: goto -61 -> 204
        //     268: astore 9
        //     270: aload 8
        //     272: ifnull +8 -> 280
        //     275: aload 8
        //     277: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     280: aload 9
        //     282: athrow
        //     283: astore 4
        //     285: aload_0
        //     286: monitorexit
        //     287: aload 4
        //     289: athrow
        //     290: astore 10
        //     292: goto -12 -> 280
        //     295: aload 8
        //     297: ifnull -93 -> 204
        //     300: aload 8
        //     302: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     305: goto -101 -> 204
        //
        // Exception table:
        //     from	to	target	type
        //     142	147	150	java/io/IOException
        //     29	137	155	java/io/IOException
        //     29	137	209	java/lang/RuntimeException
        //     199	204	263	java/io/IOException
        //     255	260	263	java/io/IOException
        //     300	305	263	java/io/IOException
        //     29	137	268	finally
        //     157	194	268	finally
        //     211	250	268	finally
        //     2	8	283	finally
        //     142	147	283	finally
        //     199	204	283	finally
        //     255	260	283	finally
        //     275	280	283	finally
        //     280	283	283	finally
        //     300	305	283	finally
        //     275	280	290	java/io/IOException
    }

    /** @deprecated */
    // ERROR //
    public void saveMiniThumbToFile(byte[] paramArrayOfByte, long paramLong1, long paramLong2)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokespecial 205	android/media/MiniThumbFile:miniThumbDataFile	()Ljava/io/RandomAccessFile;
        //     6: astore 7
        //     8: aload 7
        //     10: ifnonnull +6 -> 16
        //     13: aload_0
        //     14: monitorexit
        //     15: return
        //     16: lload_2
        //     17: ldc2_w 206
        //     20: lmul
        //     21: lstore 8
        //     23: aconst_null
        //     24: astore 10
        //     26: aload_1
        //     27: ifnull +109 -> 136
        //     30: aload_1
        //     31: arraylength
        //     32: istore 18
        //     34: iload 18
        //     36: sipush 9987
        //     39: if_icmple +14 -> 53
        //     42: iconst_0
        //     43: ifeq -30 -> 13
        //     46: aconst_null
        //     47: athrow
        //     48: astore 11
        //     50: goto -37 -> 13
        //     53: aload_0
        //     54: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     57: invokevirtual 210	java/nio/ByteBuffer:clear	()Ljava/nio/Buffer;
        //     60: pop
        //     61: aload_0
        //     62: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     65: iconst_1
        //     66: invokevirtual 283	java/nio/ByteBuffer:put	(B)Ljava/nio/ByteBuffer;
        //     69: pop
        //     70: aload_0
        //     71: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     74: lload 4
        //     76: invokevirtual 287	java/nio/ByteBuffer:putLong	(J)Ljava/nio/ByteBuffer;
        //     79: pop
        //     80: aload_0
        //     81: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     84: aload_1
        //     85: arraylength
        //     86: invokevirtual 290	java/nio/ByteBuffer:putInt	(I)Ljava/nio/ByteBuffer;
        //     89: pop
        //     90: aload_0
        //     91: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     94: aload_1
        //     95: invokevirtual 293	java/nio/ByteBuffer:put	([B)Ljava/nio/ByteBuffer;
        //     98: pop
        //     99: aload_0
        //     100: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     103: invokevirtual 296	java/nio/ByteBuffer:flip	()Ljava/nio/Buffer;
        //     106: pop
        //     107: aload_0
        //     108: getfield 142	android/media/MiniThumbFile:mChannel	Ljava/nio/channels/FileChannel;
        //     111: lload 8
        //     113: ldc2_w 206
        //     116: iconst_0
        //     117: invokevirtual 222	java/nio/channels/FileChannel:lock	(JJZ)Ljava/nio/channels/FileLock;
        //     120: astore 10
        //     122: aload_0
        //     123: getfield 142	android/media/MiniThumbFile:mChannel	Ljava/nio/channels/FileChannel;
        //     126: aload_0
        //     127: getfield 47	android/media/MiniThumbFile:mBuffer	Ljava/nio/ByteBuffer;
        //     130: lload 8
        //     132: invokevirtual 299	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;J)I
        //     135: pop
        //     136: aload 10
        //     138: ifnull -125 -> 13
        //     141: aload 10
        //     143: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     146: goto -133 -> 13
        //     149: astore 6
        //     151: aload_0
        //     152: monitorexit
        //     153: aload 6
        //     155: athrow
        //     156: astore 16
        //     158: ldc 15
        //     160: new 68	java/lang/StringBuilder
        //     163: dup
        //     164: invokespecial 69	java/lang/StringBuilder:<init>	()V
        //     167: ldc_w 301
        //     170: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: lload_2
        //     174: invokevirtual 252	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     177: ldc_w 303
        //     180: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     183: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     186: aload 16
        //     188: invokestatic 305	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     191: pop
        //     192: aload 16
        //     194: athrow
        //     195: astore 14
        //     197: aload 10
        //     199: ifnull +8 -> 207
        //     202: aload 10
        //     204: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     207: aload 14
        //     209: athrow
        //     210: astore 12
        //     212: ldc 15
        //     214: new 68	java/lang/StringBuilder
        //     217: dup
        //     218: invokespecial 69	java/lang/StringBuilder:<init>	()V
        //     221: ldc_w 301
        //     224: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     227: lload_2
        //     228: invokevirtual 252	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     231: ldc_w 307
        //     234: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     237: aload 12
        //     239: invokevirtual 258	java/lang/Object:getClass	()Ljava/lang/Class;
        //     242: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     245: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     248: invokestatic 129	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     251: pop
        //     252: aload 10
        //     254: ifnull -241 -> 13
        //     257: aload 10
        //     259: invokevirtual 241	java/nio/channels/FileLock:release	()V
        //     262: goto -249 -> 13
        //     265: astore 15
        //     267: goto -60 -> 207
        //
        // Exception table:
        //     from	to	target	type
        //     46	48	48	java/io/IOException
        //     141	146	48	java/io/IOException
        //     257	262	48	java/io/IOException
        //     2	8	149	finally
        //     46	48	149	finally
        //     141	146	149	finally
        //     202	207	149	finally
        //     207	210	149	finally
        //     257	262	149	finally
        //     30	34	156	java/io/IOException
        //     53	136	156	java/io/IOException
        //     30	34	195	finally
        //     53	136	195	finally
        //     158	195	195	finally
        //     212	252	195	finally
        //     30	34	210	java/lang/RuntimeException
        //     53	136	210	java/lang/RuntimeException
        //     202	207	265	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MiniThumbFile
 * JD-Core Version:        0.6.2
 */