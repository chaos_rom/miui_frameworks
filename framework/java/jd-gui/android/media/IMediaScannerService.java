package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IMediaScannerService extends IInterface
{
    public abstract void requestScanFile(String paramString1, String paramString2, IMediaScannerListener paramIMediaScannerListener)
        throws RemoteException;

    public abstract void scanFile(String paramString1, String paramString2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMediaScannerService
    {
        private static final String DESCRIPTOR = "android.media.IMediaScannerService";
        static final int TRANSACTION_requestScanFile = 1;
        static final int TRANSACTION_scanFile = 2;

        public Stub()
        {
            attachInterface(this, "android.media.IMediaScannerService");
        }

        public static IMediaScannerService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IMediaScannerService");
                if ((localIInterface != null) && ((localIInterface instanceof IMediaScannerService)))
                    localObject = (IMediaScannerService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.media.IMediaScannerService");
                continue;
                paramParcel1.enforceInterface("android.media.IMediaScannerService");
                requestScanFile(paramParcel1.readString(), paramParcel1.readString(), IMediaScannerListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.media.IMediaScannerService");
                scanFile(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IMediaScannerService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IMediaScannerService";
            }

            public void requestScanFile(String paramString1, String paramString2, IMediaScannerListener paramIMediaScannerListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IMediaScannerService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    if (paramIMediaScannerListener != null)
                    {
                        localIBinder = paramIMediaScannerListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void scanFile(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.media.IMediaScannerService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IMediaScannerService
 * JD-Core Version:        0.6.2
 */