package android.media;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class ExtraRingtone
{
    private static final String[] DRM_COLUMNS = arrayOfString2;
    private static final String[] MEDIA_COLUMNS;

    static
    {
        String[] arrayOfString1 = new String[3];
        arrayOfString1[0] = "_id";
        arrayOfString1[1] = "_data";
        arrayOfString1[2] = "title";
        MEDIA_COLUMNS = arrayOfString1;
        String[] arrayOfString2 = new String[3];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "_data";
        arrayOfString2[2] = "title";
    }

    public static String getRingtoneTitle(Context paramContext, Uri paramUri, boolean paramBoolean)
    {
        String str = getTitle(paramContext, paramUri, true);
        if ((paramUri != null) && (paramBoolean) && ("settings".equals(paramUri.getAuthority())))
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = str;
            str = paramContext.getString(101450109, arrayOfObject);
        }
        return str;
    }

    private static String getTitle(Context paramContext, Uri paramUri, boolean paramBoolean)
    {
        Cursor localCursor = null;
        ContentResolver localContentResolver = paramContext.getContentResolver();
        Object localObject1 = null;
        String str1;
        Object localObject2;
        int i;
        if (paramUri != null)
        {
            str1 = paramUri.getAuthority();
            if ("settings".equals(str1))
            {
                if (paramBoolean)
                {
                    localObject2 = getTitle(paramContext, RingtoneManager.getActualDefaultRingtoneUri(paramContext, RingtoneManager.getDefaultType(paramUri)), false);
                    return localObject2;
                }
            }
            else
            {
                i = 0;
                if (!"drm".equals(str1))
                    break label151;
                localCursor = localContentResolver.query(paramUri, DRM_COLUMNS, null, null, null);
                i = 1;
                label82: if (localCursor == null)
                    break label180;
            }
        }
        while (true)
        {
            try
            {
                if (localCursor.getCount() == 1)
                {
                    localCursor.moveToFirst();
                    localObject1 = localCursor.getString(2);
                    if (localObject1 == null)
                        localObject1 = "";
                    if (localCursor != null)
                        localCursor.close();
                    if (localObject1 != null)
                        break label220;
                    localObject1 = paramContext.getString(101450133);
                    localObject2 = localObject1;
                    break;
                    label151: if (!"media".equals(str1))
                        break label82;
                    localCursor = localContentResolver.query(paramUri, MEDIA_COLUMNS, null, null, null);
                    i = 1;
                    break label82;
                }
                label180: if (i != 0)
                {
                    localObject1 = "";
                    continue;
                }
                String str2 = paramUri.getLastPathSegment();
                localObject1 = str2;
                continue;
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            label220: if (((String)localObject1).length() == 0)
                localObject1 = paramContext.getString(101450135);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.ExtraRingtone
 * JD-Core Version:        0.6.2
 */