package android.media;

import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.util.Log;
import android.view.KeyEvent;
import java.util.HashMap;

public class AudioManager
{
    public static final String ACTION_AUDIO_BECOMING_NOISY = "android.media.AUDIO_BECOMING_NOISY";

    @Deprecated
    public static final String ACTION_SCO_AUDIO_STATE_CHANGED = "android.media.SCO_AUDIO_STATE_CHANGED";
    public static final String ACTION_SCO_AUDIO_STATE_UPDATED = "android.media.ACTION_SCO_AUDIO_STATE_UPDATED";
    public static final int ADJUST_LOWER = -1;
    public static final int ADJUST_RAISE = 1;
    public static final int ADJUST_SAME = 0;
    public static final int AUDIOFOCUS_GAIN = 1;
    public static final int AUDIOFOCUS_GAIN_TRANSIENT = 2;
    public static final int AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3;
    public static final int AUDIOFOCUS_LOSS = -1;
    public static final int AUDIOFOCUS_LOSS_TRANSIENT = -2;
    public static final int AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK = -3;
    public static final int AUDIOFOCUS_REQUEST_FAILED = 0;
    public static final int AUDIOFOCUS_REQUEST_GRANTED = 1;
    public static final int[] DEFAULT_STREAM_VOLUME = arrayOfInt;
    public static final int DEVICE_OUT_ANLG_DOCK_HEADSET = 2048;
    public static final int DEVICE_OUT_AUX_DIGITAL = 1024;
    public static final int DEVICE_OUT_BLUETOOTH_A2DP = 128;
    public static final int DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES = 256;
    public static final int DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER = 512;
    public static final int DEVICE_OUT_BLUETOOTH_SCO = 16;
    public static final int DEVICE_OUT_BLUETOOTH_SCO_CARKIT = 64;
    public static final int DEVICE_OUT_BLUETOOTH_SCO_HEADSET = 32;
    public static final int DEVICE_OUT_DEFAULT = 32768;
    public static final int DEVICE_OUT_DGTL_DOCK_HEADSET = 4096;
    public static final int DEVICE_OUT_EARPIECE = 1;
    public static final int DEVICE_OUT_SPEAKER = 2;
    public static final int DEVICE_OUT_USB_ACCESSORY = 8192;
    public static final int DEVICE_OUT_USB_DEVICE = 16384;
    public static final int DEVICE_OUT_WIRED_HEADPHONE = 8;
    public static final int DEVICE_OUT_WIRED_HEADSET = 4;
    public static final String EXTRA_MASTER_VOLUME_MUTED = "android.media.EXTRA_MASTER_VOLUME_MUTED";
    public static final String EXTRA_MASTER_VOLUME_VALUE = "android.media.EXTRA_MASTER_VOLUME_VALUE";
    public static final String EXTRA_PREV_MASTER_VOLUME_VALUE = "android.media.EXTRA_PREV_MASTER_VOLUME_VALUE";
    public static final String EXTRA_PREV_VOLUME_STREAM_VALUE = "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE";
    public static final String EXTRA_REMOTE_CONTROL_CLIENT_GENERATION = "android.media.EXTRA_REMOTE_CONTROL_CLIENT_GENERATION";
    public static final String EXTRA_REMOTE_CONTROL_CLIENT_INFO_CHANGED = "android.media.EXTRA_REMOTE_CONTROL_CLIENT_INFO_CHANGED";
    public static final String EXTRA_REMOTE_CONTROL_CLIENT_NAME = "android.media.EXTRA_REMOTE_CONTROL_CLIENT_NAME";
    public static final String EXTRA_REMOTE_CONTROL_EVENT_RECEIVER = "android.media.EXTRA_REMOTE_CONTROL_EVENT_RECEIVER";
    public static final String EXTRA_RINGER_MODE = "android.media.EXTRA_RINGER_MODE";
    public static final String EXTRA_SCO_AUDIO_PREVIOUS_STATE = "android.media.extra.SCO_AUDIO_PREVIOUS_STATE";
    public static final String EXTRA_SCO_AUDIO_STATE = "android.media.extra.SCO_AUDIO_STATE";
    public static final String EXTRA_VIBRATE_SETTING = "android.media.EXTRA_VIBRATE_SETTING";
    public static final String EXTRA_VIBRATE_TYPE = "android.media.EXTRA_VIBRATE_TYPE";
    public static final String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";
    public static final String EXTRA_VOLUME_STREAM_VALUE = "android.media.EXTRA_VOLUME_STREAM_VALUE";
    public static final int FLAG_ALLOW_RINGER_MODES = 2;
    public static final int FLAG_PLAY_SOUND = 4;
    public static final int FLAG_REMOVE_SOUND_AND_VIBRATE = 8;
    public static final int FLAG_SHOW_UI = 1;
    public static final int FLAG_VIBRATE = 16;
    public static final int FX_FOCUS_NAVIGATION_DOWN = 2;
    public static final int FX_FOCUS_NAVIGATION_LEFT = 3;
    public static final int FX_FOCUS_NAVIGATION_RIGHT = 4;
    public static final int FX_FOCUS_NAVIGATION_UP = 1;
    public static final int FX_KEYPRESS_DELETE = 7;
    public static final int FX_KEYPRESS_RETURN = 8;
    public static final int FX_KEYPRESS_SPACEBAR = 6;
    public static final int FX_KEYPRESS_STANDARD = 5;
    public static final int FX_KEY_CLICK = 0;
    public static final String MASTER_MUTE_CHANGED_ACTION = "android.media.MASTER_MUTE_CHANGED_ACTION";
    public static final String MASTER_VOLUME_CHANGED_ACTION = "android.media.MASTER_VOLUME_CHANGED_ACTION";
    public static final int MODE_CURRENT = -1;
    public static final int MODE_INVALID = -2;
    public static final int MODE_IN_CALL = 2;
    public static final int MODE_IN_COMMUNICATION = 3;
    public static final int MODE_NORMAL = 0;
    public static final int MODE_RINGTONE = 1;
    public static final int NUM_SOUND_EFFECTS = 9;

    @Deprecated
    public static final int NUM_STREAMS = 5;
    public static final String REMOTE_CONTROL_CLIENT_CHANGED = "android.media.REMOTE_CONTROL_CLIENT_CHANGED";
    public static final String RINGER_MODE_CHANGED_ACTION = "android.media.RINGER_MODE_CHANGED";
    private static final int RINGER_MODE_MAX = 2;
    public static final int RINGER_MODE_NORMAL = 2;
    public static final int RINGER_MODE_SILENT = 0;
    public static final int RINGER_MODE_VIBRATE = 1;

    @Deprecated
    public static final int ROUTE_ALL = -1;

    @Deprecated
    public static final int ROUTE_BLUETOOTH = 4;

    @Deprecated
    public static final int ROUTE_BLUETOOTH_A2DP = 16;

    @Deprecated
    public static final int ROUTE_BLUETOOTH_SCO = 4;

    @Deprecated
    public static final int ROUTE_EARPIECE = 1;

    @Deprecated
    public static final int ROUTE_HEADSET = 8;

    @Deprecated
    public static final int ROUTE_SPEAKER = 2;
    public static final int SCO_AUDIO_STATE_CONNECTED = 1;
    public static final int SCO_AUDIO_STATE_CONNECTING = 2;
    public static final int SCO_AUDIO_STATE_DISCONNECTED = 0;
    public static final int SCO_AUDIO_STATE_ERROR = -1;
    public static final int STREAM_ALARM = 4;
    public static final int STREAM_BLUETOOTH_SCO = 6;
    public static final int STREAM_DTMF = 8;
    public static final int STREAM_MUSIC = 3;
    public static final int STREAM_NOTIFICATION = 5;
    public static final int STREAM_RING = 2;
    public static final int STREAM_SYSTEM = 1;
    public static final int STREAM_SYSTEM_ENFORCED = 7;
    public static final int STREAM_TTS = 9;
    public static final int STREAM_VOICE_CALL = 0;
    private static String TAG = "AudioManager";
    public static final int USE_DEFAULT_STREAM_TYPE = -2147483648;
    public static final String VIBRATE_SETTING_CHANGED_ACTION = "android.media.VIBRATE_SETTING_CHANGED";
    public static final int VIBRATE_SETTING_OFF = 0;
    public static final int VIBRATE_SETTING_ON = 1;
    public static final int VIBRATE_SETTING_ONLY_SILENT = 2;
    public static final int VIBRATE_TYPE_NOTIFICATION = 1;
    public static final int VIBRATE_TYPE_RINGER = 0;
    public static final String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    private static IAudioService sService;
    private final IAudioFocusDispatcher mAudioFocusDispatcher = new IAudioFocusDispatcher.Stub()
    {
        public void dispatchAudioFocusChange(int paramAnonymousInt, String paramAnonymousString)
        {
            Message localMessage = AudioManager.this.mAudioFocusEventHandlerDelegate.getHandler().obtainMessage(paramAnonymousInt, paramAnonymousString);
            AudioManager.this.mAudioFocusEventHandlerDelegate.getHandler().sendMessage(localMessage);
        }
    };
    private final FocusEventHandlerDelegate mAudioFocusEventHandlerDelegate = new FocusEventHandlerDelegate();
    private final HashMap<String, OnAudioFocusChangeListener> mAudioFocusIdListenerMap = new HashMap();
    private final Context mContext;
    private final Object mFocusListenerLock = new Object();
    private final IBinder mICallBack = new Binder();
    private final boolean mUseMasterVolume;
    private long mVolumeKeyUpTime;

    static
    {
        int[] arrayOfInt = new int[10];
        arrayOfInt[0] = 4;
        arrayOfInt[1] = 7;
        arrayOfInt[2] = 5;
        arrayOfInt[3] = 11;
        arrayOfInt[4] = 6;
        arrayOfInt[5] = 5;
        arrayOfInt[6] = 7;
        arrayOfInt[7] = 7;
        arrayOfInt[8] = 11;
        arrayOfInt[9] = 11;
    }

    public AudioManager(Context paramContext)
    {
        this.mContext = paramContext;
        this.mUseMasterVolume = this.mContext.getResources().getBoolean(17891338);
    }

    private OnAudioFocusChangeListener findFocusListener(String paramString)
    {
        return (OnAudioFocusChangeListener)this.mAudioFocusIdListenerMap.get(paramString);
    }

    private String getIdForAudioFocusListener(OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
    {
        if (paramOnAudioFocusChangeListener == null);
        for (String str = new String(toString()); ; str = new String(toString() + paramOnAudioFocusChangeListener.toString()))
            return str;
    }

    private static IAudioService getService()
    {
        if (sService != null);
        for (IAudioService localIAudioService = sService; ; localIAudioService = sService)
        {
            return localIAudioService;
            sService = IAudioService.Stub.asInterface(ServiceManager.getService("audio"));
        }
    }

    public static boolean isValidRingerMode(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 2));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private boolean querySoundEffectsEnabled()
    {
        boolean bool = false;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "sound_effects_enabled", 0) != 0)
            bool = true;
        return bool;
    }

    public int abandonAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
    {
        int i = 0;
        unregisterAudioFocusListener(paramOnAudioFocusChangeListener);
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.abandonAudioFocus(this.mAudioFocusDispatcher, getIdForAudioFocusListener(paramOnAudioFocusChangeListener));
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Can't call abandonAudioFocus() on AudioService due to " + localRemoteException);
        }
    }

    public void abandonAudioFocusForCall()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.abandonAudioFocus(null, "AudioFocus_For_Phone_Ring_And_Calls");
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Can't call abandonAudioFocusForCall() on AudioService due to " + localRemoteException);
        }
    }

    public void adjustLocalOrRemoteStreamVolume(int paramInt1, int paramInt2)
    {
        if (paramInt1 != 3)
            Log.w(TAG, "adjustLocalOrRemoteStreamVolume() doesn't support stream " + paramInt1);
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.adjustLocalOrRemoteStreamVolume(paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in adjustLocalOrRemoteStreamVolume", localRemoteException);
        }
    }

    public void adjustMasterVolume(int paramInt1, int paramInt2)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.adjustMasterVolume(paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in adjustMasterVolume", localRemoteException);
        }
    }

    public void adjustStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        IAudioService localIAudioService = getService();
        try
        {
            if (this.mUseMasterVolume)
                localIAudioService.adjustMasterVolume(paramInt2, paramInt3);
            else
                localIAudioService.adjustStreamVolume(paramInt1, paramInt2, paramInt3);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in adjustStreamVolume", localRemoteException);
        }
    }

    public void adjustSuggestedStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        IAudioService localIAudioService = getService();
        try
        {
            if (this.mUseMasterVolume)
                localIAudioService.adjustMasterVolume(paramInt1, paramInt3);
            else
                localIAudioService.adjustSuggestedStreamVolume(paramInt1, paramInt2, paramInt3);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in adjustSuggestedStreamVolume", localRemoteException);
        }
    }

    public void adjustVolume(int paramInt1, int paramInt2)
    {
        IAudioService localIAudioService = getService();
        try
        {
            if (this.mUseMasterVolume)
                localIAudioService.adjustMasterVolume(paramInt1, paramInt2);
            else
                localIAudioService.adjustVolume(paramInt1, paramInt2);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in adjustVolume", localRemoteException);
        }
    }

    public void forceVolumeControlStream(int paramInt)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.forceVolumeControlStream(paramInt, this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in forceVolumeControlStream", localRemoteException);
        }
    }

    public int getDevicesForStream(int paramInt)
    {
        switch (paramInt)
        {
        case 6:
        case 7:
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 8:
        }
        for (int i = 0; ; i = AudioSystem.getDevicesForStream(paramInt))
            return i;
    }

    public int getLastAudibleMasterVolume()
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getLastAudibleMasterVolume();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getLastAudibleMasterVolume", localRemoteException);
                int i = 0;
            }
        }
    }

    public int getLastAudibleStreamVolume(int paramInt)
    {
        IAudioService localIAudioService = getService();
        int i;
        try
        {
            if (this.mUseMasterVolume)
            {
                i = localIAudioService.getLastAudibleMasterVolume();
            }
            else
            {
                int j = localIAudioService.getLastAudibleStreamVolume(paramInt);
                i = j;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in getLastAudibleStreamVolume", localRemoteException);
            i = 0;
        }
        return i;
    }

    public int getMasterMaxVolume()
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getMasterMaxVolume();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getMasterMaxVolume", localRemoteException);
                int i = 0;
            }
        }
    }

    public int getMasterStreamType()
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getMasterStreamType();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getMasterStreamType", localRemoteException);
                int i = 2;
            }
        }
    }

    public int getMasterVolume()
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getMasterVolume();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getMasterVolume", localRemoteException);
                int i = 0;
            }
        }
    }

    public int getMode()
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getMode();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getMode", localRemoteException);
                int i = -2;
            }
        }
    }

    public String getParameters(String paramString)
    {
        return AudioSystem.getParameters(paramString);
    }

    public int getRingerMode()
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getRingerMode();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getRingerMode", localRemoteException);
                int i = 2;
            }
        }
    }

    public IRingtonePlayer getRingtonePlayer()
    {
        try
        {
            IRingtonePlayer localIRingtonePlayer2 = getService().getRingtonePlayer();
            localIRingtonePlayer1 = localIRingtonePlayer2;
            return localIRingtonePlayer1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                IRingtonePlayer localIRingtonePlayer1 = null;
        }
    }

    @Deprecated
    public int getRouting(int paramInt)
    {
        return -1;
    }

    public int getStreamMaxVolume(int paramInt)
    {
        IAudioService localIAudioService = getService();
        int i;
        try
        {
            if (this.mUseMasterVolume)
            {
                i = localIAudioService.getMasterMaxVolume();
            }
            else
            {
                int j = localIAudioService.getStreamMaxVolume(paramInt);
                i = j;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in getStreamMaxVolume", localRemoteException);
            i = 0;
        }
        return i;
    }

    public int getStreamVolume(int paramInt)
    {
        IAudioService localIAudioService = getService();
        int i;
        try
        {
            if (this.mUseMasterVolume)
            {
                i = localIAudioService.getMasterVolume();
            }
            else
            {
                int j = localIAudioService.getStreamVolume(paramInt);
                i = j;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in getStreamVolume", localRemoteException);
            i = 0;
        }
        return i;
    }

    public int getVibrateSetting(int paramInt)
    {
        IAudioService localIAudioService = getService();
        try
        {
            int j = localIAudioService.getVibrateSetting(paramInt);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in getVibrateSetting", localRemoteException);
                int i = 0;
            }
        }
    }

    public void handleKeyDown(KeyEvent paramKeyEvent, int paramInt)
    {
        int i = 1;
        int k = paramKeyEvent.getKeyCode();
        switch (k)
        {
        default:
        case 24:
        case 25:
        case 164:
        }
        do
        {
            return;
            if (this.mUseMasterVolume)
            {
                if (k == 24);
                while (true)
                {
                    adjustMasterVolume(i, 17);
                    break;
                    i = -1;
                }
            }
            if (k == 24);
            while (true)
            {
                adjustSuggestedStreamVolume(i, paramInt, 17);
                break;
                i = -1;
            }
        }
        while ((paramKeyEvent.getRepeatCount() != 0) || (!this.mUseMasterVolume));
        if (!isMasterMute());
        while (true)
        {
            setMasterMute(i);
            break;
            int j = 0;
        }
    }

    public void handleKeyUp(KeyEvent paramKeyEvent, int paramInt)
    {
        int i = paramKeyEvent.getKeyCode();
        switch (i)
        {
        default:
            return;
        case 24:
        case 25:
        }
        if (this.mUseMasterVolume)
            if (i == 25)
                adjustMasterVolume(0, 4);
        while (true)
        {
            this.mVolumeKeyUpTime = SystemClock.uptimeMillis();
            break;
            adjustSuggestedStreamVolume(0, paramInt, 4);
        }
    }

    public boolean isBluetoothA2dpOn()
    {
        if (AudioSystem.getDeviceConnectionState(128, "") == 0);
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public boolean isBluetoothScoAvailableOffCall()
    {
        return this.mContext.getResources().getBoolean(17891365);
    }

    public boolean isBluetoothScoOn()
    {
        IAudioService localIAudioService = getService();
        try
        {
            boolean bool2 = localIAudioService.isBluetoothScoOn();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in isBluetoothScoOn", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isMasterMute()
    {
        IAudioService localIAudioService = getService();
        try
        {
            boolean bool2 = localIAudioService.isMasterMute();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in isMasterMute", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isMicrophoneMute()
    {
        return AudioSystem.isMicrophoneMuted();
    }

    public boolean isMusicActive()
    {
        return AudioSystem.isStreamActive(3, 0);
    }

    public boolean isSilentMode()
    {
        int i = 1;
        int j = getRingerMode();
        if ((j == 0) || (j == i));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public boolean isSpeakerphoneOn()
    {
        IAudioService localIAudioService = getService();
        try
        {
            boolean bool2 = localIAudioService.isSpeakerphoneOn();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in isSpeakerphoneOn", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isStreamMute(int paramInt)
    {
        IAudioService localIAudioService = getService();
        try
        {
            boolean bool2 = localIAudioService.isStreamMute(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in isStreamMute", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isWiredHeadsetOn()
    {
        if ((AudioSystem.getDeviceConnectionState(4, "") == 0) && (AudioSystem.getDeviceConnectionState(8, "") == 0));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public void loadSoundEffects()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.loadSoundEffects();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in loadSoundEffects" + localRemoteException);
        }
    }

    public void playSoundEffect(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= 9));
        while (true)
        {
            return;
            if (querySoundEffectsEnabled())
            {
                IAudioService localIAudioService = getService();
                try
                {
                    localIAudioService.playSoundEffect(paramInt);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e(TAG, "Dead object in playSoundEffect" + localRemoteException);
                }
            }
        }
    }

    public void playSoundEffect(int paramInt, float paramFloat)
    {
        if ((paramInt < 0) || (paramInt >= 9));
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.playSoundEffectVolume(paramInt, paramFloat);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in playSoundEffect" + localRemoteException);
            }
        }
    }

    public void preDispatchKeyEvent(KeyEvent paramKeyEvent, int paramInt)
    {
        int i = paramKeyEvent.getKeyCode();
        if ((i != 25) && (i != 24) && (i != 164) && (300L + this.mVolumeKeyUpTime > SystemClock.uptimeMillis()))
        {
            if (!this.mUseMasterVolume)
                break label54;
            adjustMasterVolume(0, 8);
        }
        while (true)
        {
            return;
            label54: adjustSuggestedStreamVolume(0, paramInt, 8);
        }
    }

    public void registerAudioFocusListener(OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
    {
        synchronized (this.mFocusListenerLock)
        {
            if (!this.mAudioFocusIdListenerMap.containsKey(getIdForAudioFocusListener(paramOnAudioFocusChangeListener)))
                this.mAudioFocusIdListenerMap.put(getIdForAudioFocusListener(paramOnAudioFocusChangeListener), paramOnAudioFocusChangeListener);
        }
    }

    public void registerMediaButtonEventReceiver(ComponentName paramComponentName)
    {
        if (paramComponentName == null);
        while (true)
        {
            return;
            if (!paramComponentName.getPackageName().equals(this.mContext.getPackageName()))
            {
                Log.e(TAG, "registerMediaButtonEventReceiver() error: receiver and context package names don't match");
            }
            else
            {
                Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON");
                localIntent.setComponent(paramComponentName);
                registerMediaButtonIntent(PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0), paramComponentName);
            }
        }
    }

    public void registerMediaButtonEventReceiverForCalls(ComponentName paramComponentName)
    {
        if (paramComponentName == null);
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.registerMediaButtonEventReceiverForCalls(paramComponentName);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in registerMediaButtonEventReceiverForCalls", localRemoteException);
            }
        }
    }

    public void registerMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
    {
        if ((paramPendingIntent == null) || (paramComponentName == null))
            Log.e(TAG, "Cannot call registerMediaButtonIntent() with a null parameter");
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.registerMediaButtonIntent(paramPendingIntent, paramComponentName);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in registerMediaButtonIntent" + localRemoteException);
            }
        }
    }

    public void registerRemoteControlClient(RemoteControlClient paramRemoteControlClient)
    {
        if ((paramRemoteControlClient == null) || (paramRemoteControlClient.getRcMediaIntent() == null));
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                paramRemoteControlClient.setRcseId(localIAudioService.registerRemoteControlClient(paramRemoteControlClient.getRcMediaIntent(), paramRemoteControlClient.getIRemoteControlClient(), this.mContext.getPackageName()));
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in registerRemoteControlClient" + localRemoteException);
            }
        }
    }

    public void registerRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
    {
        if (paramIRemoteControlDisplay == null);
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.registerRemoteControlDisplay(paramIRemoteControlDisplay);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in registerRemoteControlDisplay " + localRemoteException);
            }
        }
    }

    public void reloadAudioSettings()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.reloadAudioSettings();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in reloadAudioSettings" + localRemoteException);
        }
    }

    public void remoteControlDisplayUsesBitmapSize(IRemoteControlDisplay paramIRemoteControlDisplay, int paramInt1, int paramInt2)
    {
        if (paramIRemoteControlDisplay == null);
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.remoteControlDisplayUsesBitmapSize(paramIRemoteControlDisplay, paramInt1, paramInt2);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in remoteControlDisplayUsesBitmapSize " + localRemoteException);
            }
        }
    }

    public int requestAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener, int paramInt1, int paramInt2)
    {
        int i = 0;
        int j;
        if ((paramInt2 < 1) || (paramInt2 > 3))
        {
            Log.e(TAG, "Invalid duration hint, audio focus request denied");
            j = 0;
        }
        while (true)
        {
            return j;
            registerAudioFocusListener(paramOnAudioFocusChangeListener);
            IAudioService localIAudioService = getService();
            try
            {
                int k = localIAudioService.requestAudioFocus(paramInt1, paramInt2, this.mICallBack, this.mAudioFocusDispatcher, getIdForAudioFocusListener(paramOnAudioFocusChangeListener), this.mContext.getPackageName());
                i = k;
                j = i;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e(TAG, "Can't call requestAudioFocus() on AudioService due to " + localRemoteException);
            }
        }
    }

    public void requestAudioFocusForCall(int paramInt1, int paramInt2)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.requestAudioFocus(paramInt1, paramInt2, this.mICallBack, null, "AudioFocus_For_Phone_Ring_And_Calls", "system");
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Can't call requestAudioFocusForCall() on AudioService due to " + localRemoteException);
        }
    }

    public int setBluetoothA2dpDeviceConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        IAudioService localIAudioService = getService();
        int i = 0;
        try
        {
            int j = localIAudioService.setBluetoothA2dpDeviceConnectionState(paramBluetoothDevice, paramInt);
            i = j;
            label21: return i;
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in setBluetoothA2dpDeviceConnectionState " + localRemoteException);
        }
        finally
        {
            break label21;
        }
    }

    @Deprecated
    public void setBluetoothA2dpOn(boolean paramBoolean)
    {
    }

    public void setBluetoothScoOn(boolean paramBoolean)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setBluetoothScoOn(paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setBluetoothScoOn", localRemoteException);
        }
    }

    public void setMasterMute(boolean paramBoolean)
    {
        setMasterMute(paramBoolean, 1);
    }

    public void setMasterMute(boolean paramBoolean, int paramInt)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setMasterMute(paramBoolean, paramInt, this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setMasterMute", localRemoteException);
        }
    }

    public void setMasterVolume(int paramInt1, int paramInt2)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setMasterVolume(paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setMasterVolume", localRemoteException);
        }
    }

    public void setMicrophoneMute(boolean paramBoolean)
    {
        AudioSystem.muteMicrophone(paramBoolean);
    }

    public void setMode(int paramInt)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setMode(paramInt, this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setMode", localRemoteException);
        }
    }

    @Deprecated
    public void setParameter(String paramString1, String paramString2)
    {
        setParameters(paramString1 + "=" + paramString2);
    }

    public void setParameters(String paramString)
    {
        AudioSystem.setParameters(paramString);
    }

    public void setRingerMode(int paramInt)
    {
        if (!isValidRingerMode(paramInt));
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.setRingerMode(paramInt);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in setRingerMode", localRemoteException);
            }
        }
    }

    @Deprecated
    public void setRouting(int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public void setSpeakerphoneOn(boolean paramBoolean)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setSpeakerphoneOn(paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setSpeakerphoneOn", localRemoteException);
        }
    }

    public void setStreamMute(int paramInt, boolean paramBoolean)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setStreamMute(paramInt, paramBoolean, this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setStreamMute", localRemoteException);
        }
    }

    public void setStreamSolo(int paramInt, boolean paramBoolean)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setStreamSolo(paramInt, paramBoolean, this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setStreamSolo", localRemoteException);
        }
    }

    public void setStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        IAudioService localIAudioService = getService();
        try
        {
            if (this.mUseMasterVolume)
                localIAudioService.setMasterVolume(paramInt2, paramInt3);
            else
                localIAudioService.setStreamVolume(paramInt1, paramInt2, paramInt3);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e(TAG, "Dead object in setStreamVolume", localRemoteException);
        }
    }

    public void setVibrateSetting(int paramInt1, int paramInt2)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setVibrateSetting(paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setVibrateSetting", localRemoteException);
        }
    }

    public void setWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.setWiredDeviceConnectionState(paramInt1, paramInt2, paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in setWiredDeviceConnectionState " + localRemoteException);
        }
    }

    @Deprecated
    public void setWiredHeadsetOn(boolean paramBoolean)
    {
    }

    public boolean shouldVibrate(int paramInt)
    {
        IAudioService localIAudioService = getService();
        try
        {
            boolean bool2 = localIAudioService.shouldVibrate(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e(TAG, "Dead object in shouldVibrate", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void startBluetoothSco()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.startBluetoothSco(this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in startBluetoothSco", localRemoteException);
        }
    }

    public void stopBluetoothSco()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.stopBluetoothSco(this.mICallBack);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in stopBluetoothSco", localRemoteException);
        }
    }

    public void unloadSoundEffects()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.unloadSoundEffects();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in unloadSoundEffects" + localRemoteException);
        }
    }

    public void unregisterAudioFocusListener(OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
    {
        synchronized (this.mFocusListenerLock)
        {
            this.mAudioFocusIdListenerMap.remove(getIdForAudioFocusListener(paramOnAudioFocusChangeListener));
            return;
        }
    }

    public void unregisterMediaButtonEventReceiver(ComponentName paramComponentName)
    {
        if (paramComponentName == null);
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON");
            localIntent.setComponent(paramComponentName);
            unregisterMediaButtonIntent(PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0), paramComponentName);
        }
    }

    public void unregisterMediaButtonEventReceiverForCalls()
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.unregisterMediaButtonEventReceiverForCalls();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in unregisterMediaButtonEventReceiverForCalls", localRemoteException);
        }
    }

    public void unregisterMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
    {
        IAudioService localIAudioService = getService();
        try
        {
            localIAudioService.unregisterMediaButtonIntent(paramPendingIntent, paramComponentName);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(TAG, "Dead object in unregisterMediaButtonIntent" + localRemoteException);
        }
    }

    public void unregisterRemoteControlClient(RemoteControlClient paramRemoteControlClient)
    {
        if ((paramRemoteControlClient == null) || (paramRemoteControlClient.getRcMediaIntent() == null));
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.unregisterRemoteControlClient(paramRemoteControlClient.getRcMediaIntent(), paramRemoteControlClient.getIRemoteControlClient());
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in unregisterRemoteControlClient" + localRemoteException);
            }
        }
    }

    public void unregisterRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
    {
        if (paramIRemoteControlDisplay == null);
        while (true)
        {
            return;
            IAudioService localIAudioService = getService();
            try
            {
                localIAudioService.unregisterRemoteControlDisplay(paramIRemoteControlDisplay);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "Dead object in unregisterRemoteControlDisplay " + localRemoteException);
            }
        }
    }

    private class FocusEventHandlerDelegate
    {
        private final Handler mHandler;

        FocusEventHandlerDelegate()
        {
            Looper localLooper = Looper.myLooper();
            if (localLooper == null)
                localLooper = Looper.getMainLooper();
            if (localLooper != null);
            for (this.mHandler = new Handler(localLooper)
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    synchronized (AudioManager.this.mFocusListenerLock)
                    {
                        AudioManager.OnAudioFocusChangeListener localOnAudioFocusChangeListener = AudioManager.this.findFocusListener((String)paramAnonymousMessage.obj);
                        if (localOnAudioFocusChangeListener != null)
                            localOnAudioFocusChangeListener.onAudioFocusChange(paramAnonymousMessage.what);
                        return;
                    }
                }
            }
            ; ; this.mHandler = null)
                return;
        }

        Handler getHandler()
        {
            return this.mHandler;
        }
    }

    public static abstract interface OnAudioFocusChangeListener
    {
        public abstract void onAudioFocusChange(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AudioManager
 * JD-Core Version:        0.6.2
 */