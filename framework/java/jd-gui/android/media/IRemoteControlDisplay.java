package android.media;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IRemoteControlDisplay extends IInterface
{
    public abstract void setAllMetadata(int paramInt, Bundle paramBundle, Bitmap paramBitmap)
        throws RemoteException;

    public abstract void setArtwork(int paramInt, Bitmap paramBitmap)
        throws RemoteException;

    public abstract void setCurrentClientId(int paramInt, PendingIntent paramPendingIntent, boolean paramBoolean)
        throws RemoteException;

    public abstract void setMetadata(int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract void setPlaybackState(int paramInt1, int paramInt2, long paramLong)
        throws RemoteException;

    public abstract void setTransportControlFlags(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRemoteControlDisplay
    {
        private static final String DESCRIPTOR = "android.media.IRemoteControlDisplay";
        static final int TRANSACTION_setAllMetadata = 6;
        static final int TRANSACTION_setArtwork = 5;
        static final int TRANSACTION_setCurrentClientId = 1;
        static final int TRANSACTION_setMetadata = 4;
        static final int TRANSACTION_setPlaybackState = 2;
        static final int TRANSACTION_setTransportControlFlags = 3;

        public Stub()
        {
            attachInterface(this, "android.media.IRemoteControlDisplay");
        }

        public static IRemoteControlDisplay asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IRemoteControlDisplay");
                if ((localIInterface != null) && ((localIInterface instanceof IRemoteControlDisplay)))
                    localObject = (IRemoteControlDisplay)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.media.IRemoteControlDisplay");
                    continue;
                    paramParcel1.enforceInterface("android.media.IRemoteControlDisplay");
                    int m = paramParcel1.readInt();
                    PendingIntent localPendingIntent;
                    if (paramParcel1.readInt() != 0)
                    {
                        localPendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label158;
                    }
                    for (boolean bool2 = bool1; ; bool2 = false)
                    {
                        setCurrentClientId(m, localPendingIntent, bool2);
                        break;
                        localPendingIntent = null;
                        break label128;
                    }
                    paramParcel1.enforceInterface("android.media.IRemoteControlDisplay");
                    setPlaybackState(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readLong());
                    continue;
                    paramParcel1.enforceInterface("android.media.IRemoteControlDisplay");
                    setTransportControlFlags(paramParcel1.readInt(), paramParcel1.readInt());
                }
            case 4:
                paramParcel1.enforceInterface("android.media.IRemoteControlDisplay");
                int k = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                {
                    setMetadata(k, localBundle2);
                    break;
                }
            case 5:
                label128: label158: paramParcel1.enforceInterface("android.media.IRemoteControlDisplay");
                int j = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Bitmap localBitmap2 = (Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel1); ; localBitmap2 = null)
                {
                    setArtwork(j, localBitmap2);
                    break;
                }
            case 6:
            }
            paramParcel1.enforceInterface("android.media.IRemoteControlDisplay");
            int i = paramParcel1.readInt();
            Bundle localBundle1;
            if (paramParcel1.readInt() != 0)
            {
                localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                label343: if (paramParcel1.readInt() == 0)
                    break label383;
            }
            label383: for (Bitmap localBitmap1 = (Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel1); ; localBitmap1 = null)
            {
                setAllMetadata(i, localBundle1, localBitmap1);
                break;
                localBundle1 = null;
                break label343;
            }
        }

        private static class Proxy
            implements IRemoteControlDisplay
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IRemoteControlDisplay";
            }

            public void setAllMetadata(int paramInt, Bundle paramBundle, Bitmap paramBitmap)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.media.IRemoteControlDisplay");
                        localParcel.writeInt(paramInt);
                        if (paramBundle != null)
                        {
                            localParcel.writeInt(1);
                            paramBundle.writeToParcel(localParcel, 0);
                            if (paramBitmap != null)
                            {
                                localParcel.writeInt(1);
                                paramBitmap.writeToParcel(localParcel, 0);
                                this.mRemote.transact(6, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public void setArtwork(int paramInt, Bitmap paramBitmap)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlDisplay");
                    localParcel.writeInt(paramInt);
                    if (paramBitmap != null)
                    {
                        localParcel.writeInt(1);
                        paramBitmap.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setCurrentClientId(int paramInt, PendingIntent paramPendingIntent, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.media.IRemoteControlDisplay");
                        localParcel.writeInt(paramInt);
                        if (paramPendingIntent != null)
                        {
                            localParcel.writeInt(1);
                            paramPendingIntent.writeToParcel(localParcel, 0);
                            break label94;
                            localParcel.writeInt(i);
                            this.mRemote.transact(1, localParcel, null, 1);
                        }
                        else
                        {
                            localParcel.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    label94: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void setMetadata(int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlDisplay");
                    localParcel.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setPlaybackState(int paramInt1, int paramInt2, long paramLong)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlDisplay");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeLong(paramLong);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setTransportControlFlags(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlDisplay");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IRemoteControlDisplay
 * JD-Core Version:        0.6.2
 */