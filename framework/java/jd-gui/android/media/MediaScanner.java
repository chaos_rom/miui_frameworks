package android.media;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.IContentProvider;
import android.database.Cursor;
import android.database.SQLException;
import android.drm.DrmManagerClient;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.MediaStore.Audio.Media;
import android.provider.MediaStore.Audio.Playlists;
import android.provider.MediaStore.Files;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Images.Thumbnails;
import android.provider.MediaStore.Video.Media;
import android.provider.Settings.System;
import android.sax.Element;
import android.sax.ElementListener;
import android.sax.RootElement;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;

public class MediaScanner
{
    private static final String ALARMS_DIR = "/alarms/";
    private static final int DATE_MODIFIED_PLAYLISTS_COLUMN_INDEX = 2;
    private static final String DEFAULT_RINGTONE_PROPERTY_PREFIX = "ro.config.";
    private static final boolean ENABLE_BULK_INSERTS = true;
    private static final int FILES_PRESCAN_DATE_MODIFIED_COLUMN_INDEX = 3;
    private static final int FILES_PRESCAN_FORMAT_COLUMN_INDEX = 2;
    private static final int FILES_PRESCAN_ID_COLUMN_INDEX = 0;
    private static final int FILES_PRESCAN_PATH_COLUMN_INDEX = 1;
    private static final String[] FILES_PRESCAN_PROJECTION;
    private static final String[] ID3_GENRES = arrayOfString4;
    private static final int ID_PLAYLISTS_COLUMN_INDEX = 0;
    private static final String[] ID_PROJECTION;
    private static final String MUSIC_DIR = "/music/";
    private static final String NOTIFICATIONS_DIR = "/notifications/";
    private static final int PATH_PLAYLISTS_COLUMN_INDEX = 1;
    private static final String[] PLAYLIST_MEMBERS_PROJECTION;
    private static final String PODCAST_DIR = "/podcasts/";
    private static final String RINGTONES_DIR = "/ringtones/";
    private static final String TAG = "MediaScanner";
    private Uri mAudioUri;
    private final BitmapFactory.Options mBitmapOptions = new BitmapFactory.Options();
    private boolean mCaseInsensitivePaths;
    private final MyMediaScannerClient mClient = new MyMediaScannerClient(null);
    private Context mContext;
    private String mDefaultAlarmAlertFilename;
    private boolean mDefaultAlarmSet;
    private String mDefaultNotificationFilename;
    private boolean mDefaultNotificationSet;
    private String mDefaultRingtoneFilename;
    private boolean mDefaultRingtoneSet;
    private DrmManagerClient mDrmManagerClient = null;
    private final String mExternalStoragePath;
    private Uri mFilesUri;
    private Uri mImagesUri;
    private MediaInserter mMediaInserter;
    private IContentProvider mMediaProvider;
    private int mMtpObjectHandle;
    private int mNativeContext;
    private int mOriginalCount;
    private ArrayList<FileEntry> mPlayLists;
    private ArrayList<PlaylistEntry> mPlaylistEntries = new ArrayList();
    private Uri mPlaylistsUri;
    private boolean mProcessGenres;
    private boolean mProcessPlaylists;
    private Uri mThumbsUri;
    private Uri mVideoUri;
    private boolean mWasEmptyPriorToScan = false;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "_id";
        arrayOfString1[1] = "_data";
        arrayOfString1[2] = "format";
        arrayOfString1[3] = "date_modified";
        FILES_PRESCAN_PROJECTION = arrayOfString1;
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = "_id";
        ID_PROJECTION = arrayOfString2;
        String[] arrayOfString3 = new String[1];
        arrayOfString3[0] = "playlist_id";
        PLAYLIST_MEMBERS_PROJECTION = arrayOfString3;
        String[] arrayOfString4 = new String[''];
        arrayOfString4[0] = "Blues";
        arrayOfString4[1] = "Classic Rock";
        arrayOfString4[2] = "Country";
        arrayOfString4[3] = "Dance";
        arrayOfString4[4] = "Disco";
        arrayOfString4[5] = "Funk";
        arrayOfString4[6] = "Grunge";
        arrayOfString4[7] = "Hip-Hop";
        arrayOfString4[8] = "Jazz";
        arrayOfString4[9] = "Metal";
        arrayOfString4[10] = "New Age";
        arrayOfString4[11] = "Oldies";
        arrayOfString4[12] = "Other";
        arrayOfString4[13] = "Pop";
        arrayOfString4[14] = "R&B";
        arrayOfString4[15] = "Rap";
        arrayOfString4[16] = "Reggae";
        arrayOfString4[17] = "Rock";
        arrayOfString4[18] = "Techno";
        arrayOfString4[19] = "Industrial";
        arrayOfString4[20] = "Alternative";
        arrayOfString4[21] = "Ska";
        arrayOfString4[22] = "Death Metal";
        arrayOfString4[23] = "Pranks";
        arrayOfString4[24] = "Soundtrack";
        arrayOfString4[25] = "Euro-Techno";
        arrayOfString4[26] = "Ambient";
        arrayOfString4[27] = "Trip-Hop";
        arrayOfString4[28] = "Vocal";
        arrayOfString4[29] = "Jazz+Funk";
        arrayOfString4[30] = "Fusion";
        arrayOfString4[31] = "Trance";
        arrayOfString4[32] = "Classical";
        arrayOfString4[33] = "Instrumental";
        arrayOfString4[34] = "Acid";
        arrayOfString4[35] = "House";
        arrayOfString4[36] = "Game";
        arrayOfString4[37] = "Sound Clip";
        arrayOfString4[38] = "Gospel";
        arrayOfString4[39] = "Noise";
        arrayOfString4[40] = "AlternRock";
        arrayOfString4[41] = "Bass";
        arrayOfString4[42] = "Soul";
        arrayOfString4[43] = "Punk";
        arrayOfString4[44] = "Space";
        arrayOfString4[45] = "Meditative";
        arrayOfString4[46] = "Instrumental Pop";
        arrayOfString4[47] = "Instrumental Rock";
        arrayOfString4[48] = "Ethnic";
        arrayOfString4[49] = "Gothic";
        arrayOfString4[50] = "Darkwave";
        arrayOfString4[51] = "Techno-Industrial";
        arrayOfString4[52] = "Electronic";
        arrayOfString4[53] = "Pop-Folk";
        arrayOfString4[54] = "Eurodance";
        arrayOfString4[55] = "Dream";
        arrayOfString4[56] = "Southern Rock";
        arrayOfString4[57] = "Comedy";
        arrayOfString4[58] = "Cult";
        arrayOfString4[59] = "Gangsta";
        arrayOfString4[60] = "Top 40";
        arrayOfString4[61] = "Christian Rap";
        arrayOfString4[62] = "Pop/Funk";
        arrayOfString4[63] = "Jungle";
        arrayOfString4[64] = "Native American";
        arrayOfString4[65] = "Cabaret";
        arrayOfString4[66] = "New Wave";
        arrayOfString4[67] = "Psychadelic";
        arrayOfString4[68] = "Rave";
        arrayOfString4[69] = "Showtunes";
        arrayOfString4[70] = "Trailer";
        arrayOfString4[71] = "Lo-Fi";
        arrayOfString4[72] = "Tribal";
        arrayOfString4[73] = "Acid Punk";
        arrayOfString4[74] = "Acid Jazz";
        arrayOfString4[75] = "Polka";
        arrayOfString4[76] = "Retro";
        arrayOfString4[77] = "Musical";
        arrayOfString4[78] = "Rock & Roll";
        arrayOfString4[79] = "Hard Rock";
        arrayOfString4[80] = "Folk";
        arrayOfString4[81] = "Folk-Rock";
        arrayOfString4[82] = "National Folk";
        arrayOfString4[83] = "Swing";
        arrayOfString4[84] = "Fast Fusion";
        arrayOfString4[85] = "Bebob";
        arrayOfString4[86] = "Latin";
        arrayOfString4[87] = "Revival";
        arrayOfString4[88] = "Celtic";
        arrayOfString4[89] = "Bluegrass";
        arrayOfString4[90] = "Avantgarde";
        arrayOfString4[91] = "Gothic Rock";
        arrayOfString4[92] = "Progressive Rock";
        arrayOfString4[93] = "Psychedelic Rock";
        arrayOfString4[94] = "Symphonic Rock";
        arrayOfString4[95] = "Slow Rock";
        arrayOfString4[96] = "Big Band";
        arrayOfString4[97] = "Chorus";
        arrayOfString4[98] = "Easy Listening";
        arrayOfString4[99] = "Acoustic";
        arrayOfString4[100] = "Humour";
        arrayOfString4[101] = "Speech";
        arrayOfString4[102] = "Chanson";
        arrayOfString4[103] = "Opera";
        arrayOfString4[104] = "Chamber Music";
        arrayOfString4[105] = "Sonata";
        arrayOfString4[106] = "Symphony";
        arrayOfString4[107] = "Booty Bass";
        arrayOfString4[108] = "Primus";
        arrayOfString4[109] = "Porn Groove";
        arrayOfString4[110] = "Satire";
        arrayOfString4[111] = "Slow Jam";
        arrayOfString4[112] = "Club";
        arrayOfString4[113] = "Tango";
        arrayOfString4[114] = "Samba";
        arrayOfString4[115] = "Folklore";
        arrayOfString4[116] = "Ballad";
        arrayOfString4[117] = "Power Ballad";
        arrayOfString4[118] = "Rhythmic Soul";
        arrayOfString4[119] = "Freestyle";
        arrayOfString4[120] = "Duet";
        arrayOfString4[121] = "Punk Rock";
        arrayOfString4[122] = "Drum Solo";
        arrayOfString4[123] = "A capella";
        arrayOfString4[124] = "Euro-House";
        arrayOfString4[125] = "Dance Hall";
        arrayOfString4[126] = "Goa";
        arrayOfString4[127] = "Drum & Bass";
        arrayOfString4[''] = "Club-House";
        arrayOfString4[''] = "Hardcore";
        arrayOfString4[''] = "Terror";
        arrayOfString4[''] = "Indie";
        arrayOfString4[''] = "Britpop";
        arrayOfString4[''] = "Negerpunk";
        arrayOfString4[''] = "Polsk Punk";
        arrayOfString4[''] = "Beat";
        arrayOfString4[''] = "Christian Gangsta";
        arrayOfString4[''] = "Heavy Metal";
        arrayOfString4[''] = "Black Metal";
        arrayOfString4[''] = "Crossover";
        arrayOfString4[''] = "Contemporary Christian";
        arrayOfString4[''] = "Christian Rock";
        arrayOfString4[''] = "Merengue";
        arrayOfString4[''] = "Salsa";
        arrayOfString4[''] = "Thrash Metal";
        arrayOfString4[''] = "Anime";
        arrayOfString4[''] = "JPop";
        arrayOfString4[''] = "Synthpop";
    }

    public MediaScanner(Context paramContext)
    {
        native_setup();
        this.mContext = paramContext;
        this.mBitmapOptions.inSampleSize = 1;
        this.mBitmapOptions.inJustDecodeBounds = true;
        setDefaultRingtoneFileNames();
        this.mExternalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    private void cachePlaylistEntry(String paramString1, String paramString2)
    {
        int i = 0;
        PlaylistEntry localPlaylistEntry = new PlaylistEntry(null);
        for (int j = paramString1.length(); (j > 0) && (Character.isWhitespace(paramString1.charAt(j - 1))); j--);
        if (j < 3);
        while (true)
        {
            return;
            if (j < paramString1.length())
                paramString1 = paramString1.substring(0, j);
            char c = paramString1.charAt(0);
            if ((c == '/') || ((Character.isLetter(c)) && (paramString1.charAt(1) == ':') && (paramString1.charAt(2) == '\\')))
                i = 1;
            if (i == 0)
                paramString1 = paramString2 + paramString1;
            localPlaylistEntry.path = paramString1;
            this.mPlaylistEntries.add(localPlaylistEntry);
        }
    }

    private boolean inScanDirectory(String paramString, String[] paramArrayOfString)
    {
        int i = 0;
        if (i < paramArrayOfString.length)
            if (!paramString.startsWith(paramArrayOfString[i]));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    private void initialize(String paramString)
    {
        this.mMediaProvider = this.mContext.getContentResolver().acquireProvider("media");
        this.mAudioUri = MediaStore.Audio.Media.getContentUri(paramString);
        this.mVideoUri = MediaStore.Video.Media.getContentUri(paramString);
        this.mImagesUri = MediaStore.Images.Media.getContentUri(paramString);
        this.mThumbsUri = MediaStore.Images.Thumbnails.getContentUri(paramString);
        this.mFilesUri = MediaStore.Files.getContentUri(paramString);
        if (!paramString.equals("internal"))
        {
            this.mProcessPlaylists = true;
            this.mProcessGenres = true;
            this.mPlaylistsUri = MediaStore.Audio.Playlists.getContentUri(paramString);
            this.mCaseInsensitivePaths = true;
        }
    }

    private boolean isDrmEnabled()
    {
        String str = SystemProperties.get("drm.service.enabled");
        if ((str != null) && (str.equals("true")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isNoMediaFile(String paramString)
    {
        boolean bool = true;
        if (new File(paramString).isDirectory())
            bool = false;
        while (true)
        {
            return bool;
            int i = paramString.lastIndexOf('/');
            if ((i >= 0) && (i + 2 < paramString.length()))
            {
                if (!paramString.regionMatches(i + 1, "._", 0, 2))
                    if (paramString.regionMatches(bool, -4 + paramString.length(), ".jpg", 0, 4))
                    {
                        if ((paramString.regionMatches(bool, i + 1, "AlbumArt_{", 0, 10)) || (paramString.regionMatches(bool, i + 1, "AlbumArt.", 0, 9)))
                            continue;
                        int j = -1 + (paramString.length() - i);
                        if (((j == 17) && (paramString.regionMatches(bool, i + 1, "AlbumArtSmall", 0, 13))) || ((j == 10) && (paramString.regionMatches(bool, i + 1, "Folder", 0, 6))))
                            continue;
                    }
            }
            else
                bool = false;
        }
    }

    public static boolean isNoMediaPath(String paramString)
    {
        boolean bool = false;
        if (paramString == null);
        while (true)
        {
            return bool;
            if (paramString.indexOf("/.") >= 0)
            {
                bool = true;
            }
            else
            {
                int j;
                for (int i = 1; ; i = j)
                {
                    if (i < 0)
                        break label93;
                    j = paramString.indexOf('/', i);
                    if (j > i)
                    {
                        j++;
                        if (new File(paramString.substring(0, j) + ".nomedia").exists())
                        {
                            bool = true;
                            break;
                        }
                    }
                }
                label93: bool = isNoMediaFile(paramString);
            }
        }
    }

    private boolean matchEntries(long paramLong, String paramString)
    {
        int i = this.mPlaylistEntries.size();
        boolean bool = true;
        int j = 0;
        if (j < i)
        {
            PlaylistEntry localPlaylistEntry = (PlaylistEntry)this.mPlaylistEntries.get(j);
            if (localPlaylistEntry.bestmatchlevel == 2147483647);
            while (true)
            {
                j++;
                break;
                bool = false;
                if (paramString.equalsIgnoreCase(localPlaylistEntry.path))
                {
                    localPlaylistEntry.bestmatchid = paramLong;
                    localPlaylistEntry.bestmatchlevel = 2147483647;
                }
                else
                {
                    int k = matchPaths(paramString, localPlaylistEntry.path);
                    if (k > localPlaylistEntry.bestmatchlevel)
                    {
                        localPlaylistEntry.bestmatchid = paramLong;
                        localPlaylistEntry.bestmatchlevel = k;
                    }
                }
            }
        }
        return bool;
    }

    private int matchPaths(String paramString1, String paramString2)
    {
        int i = 0;
        int j = paramString1.length();
        label94: label102: int i6;
        for (int k = paramString2.length(); ; k = i6 - 1)
        {
            int i1;
            int i2;
            int i3;
            int i4;
            int i5;
            label110: int i7;
            if ((j > 0) && (k > 0))
            {
                int m = paramString1.lastIndexOf('/', j - 1);
                int n = paramString2.lastIndexOf('/', k - 1);
                i1 = paramString1.lastIndexOf('\\', j - 1);
                i2 = paramString2.lastIndexOf('\\', k - 1);
                if (m <= i1)
                    break label129;
                i3 = m;
                if (n <= i2)
                    break label136;
                i4 = n;
                if (i3 >= 0)
                    break label143;
                i5 = 0;
                if (i4 >= 0)
                    break label152;
                i6 = 0;
                i7 = j - i5;
                if (k - i6 == i7)
                    break label161;
            }
            label129: label136: label143: label152: label161: 
            while (!paramString1.regionMatches(true, i5, paramString2, i6, i7))
            {
                return i;
                i3 = i1;
                break;
                i4 = i2;
                break label94;
                i5 = i3 + 1;
                break label102;
                i6 = i4 + 1;
                break label110;
            }
            i++;
            j = i5 - 1;
        }
    }

    private final native void native_finalize();

    private static final native void native_init();

    private final native void native_setup();

    private void postscan(String[] paramArrayOfString)
        throws RemoteException
    {
        if (this.mProcessPlaylists)
            processPlayLists();
        if ((this.mOriginalCount == 0) && (this.mImagesUri.equals(MediaStore.Images.Media.getContentUri("external"))))
            pruneDeadThumbnailFiles();
        this.mPlayLists = null;
        this.mMediaProvider = null;
    }

    // ERROR //
    private void prescan(String paramString, boolean paramBoolean)
        throws RemoteException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: getfield 558	android/media/MediaScanner:mPlayLists	Ljava/util/ArrayList;
        //     6: ifnonnull +248 -> 254
        //     9: aload_0
        //     10: new 445	java/util/ArrayList
        //     13: dup
        //     14: invokespecial 446	java/util/ArrayList:<init>	()V
        //     17: putfield 558	android/media/MediaScanner:mPlayLists	Ljava/util/ArrayList;
        //     20: aload_1
        //     21: ifnull +243 -> 264
        //     24: ldc_w 770
        //     27: astore 4
        //     29: iconst_2
        //     30: anewarray 118	java/lang/String
        //     33: astore 5
        //     35: aload 5
        //     37: iconst_0
        //     38: ldc_w 772
        //     41: aastore
        //     42: aload 5
        //     44: iconst_1
        //     45: aload_1
        //     46: aastore
        //     47: aload_0
        //     48: getfield 492	android/media/MediaScanner:mFilesUri	Landroid/net/Uri;
        //     51: invokevirtual 776	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
        //     54: astore 6
        //     56: aload 6
        //     58: ldc_w 778
        //     61: ldc_w 780
        //     64: invokevirtual 786	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
        //     67: pop
        //     68: new 11	android/media/MediaScanner$MediaBulkDeleter
        //     71: dup
        //     72: aload_0
        //     73: getfield 539	android/media/MediaScanner:mMediaProvider	Landroid/content/IContentProvider;
        //     76: aload 6
        //     78: invokevirtual 790	android/net/Uri$Builder:build	()Landroid/net/Uri;
        //     81: invokespecial 793	android/media/MediaScanner$MediaBulkDeleter:<init>	(Landroid/content/IContentProvider;Landroid/net/Uri;)V
        //     84: astore 8
        //     86: iload_2
        //     87: ifeq +101 -> 188
        //     90: ldc2_w 794
        //     93: lstore 10
        //     95: aload_0
        //     96: getfield 492	android/media/MediaScanner:mFilesUri	Landroid/net/Uri;
        //     99: invokevirtual 776	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
        //     102: ldc_w 797
        //     105: ldc_w 799
        //     108: invokevirtual 786	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
        //     111: invokevirtual 790	android/net/Uri$Builder:build	()Landroid/net/Uri;
        //     114: astore 13
        //     116: aload_0
        //     117: iconst_1
        //     118: putfield 438	android/media/MediaScanner:mWasEmptyPriorToScan	Z
        //     121: aload 5
        //     123: iconst_0
        //     124: new 601	java/lang/StringBuilder
        //     127: dup
        //     128: invokespecial 602	java/lang/StringBuilder:<init>	()V
        //     131: ldc_w 772
        //     134: invokevirtual 606	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     137: lload 10
        //     139: invokevirtual 802	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     142: invokevirtual 609	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     145: aastore
        //     146: aload_3
        //     147: ifnull +11 -> 158
        //     150: aload_3
        //     151: invokeinterface 807 1 0
        //     156: aconst_null
        //     157: astore_3
        //     158: aload_0
        //     159: getfield 539	android/media/MediaScanner:mMediaProvider	Landroid/content/IContentProvider;
        //     162: aload 13
        //     164: getstatic 128	android/media/MediaScanner:FILES_PRESCAN_PROJECTION	[Ljava/lang/String;
        //     167: aload 4
        //     169: aload 5
        //     171: ldc 120
        //     173: aconst_null
        //     174: invokeinterface 813 7 0
        //     179: astore 14
        //     181: aload 14
        //     183: astore_3
        //     184: aload_3
        //     185: ifnonnull +100 -> 285
        //     188: aload_3
        //     189: ifnull +9 -> 198
        //     192: aload_3
        //     193: invokeinterface 807 1 0
        //     198: aload 8
        //     200: invokevirtual 816	android/media/MediaScanner$MediaBulkDeleter:flush	()V
        //     203: aload_0
        //     204: iconst_0
        //     205: putfield 756	android/media/MediaScanner:mOriginalCount	I
        //     208: aload_0
        //     209: getfield 539	android/media/MediaScanner:mMediaProvider	Landroid/content/IContentProvider;
        //     212: aload_0
        //     213: getfield 502	android/media/MediaScanner:mImagesUri	Landroid/net/Uri;
        //     216: getstatic 130	android/media/MediaScanner:ID_PROJECTION	[Ljava/lang/String;
        //     219: aconst_null
        //     220: aconst_null
        //     221: aconst_null
        //     222: aconst_null
        //     223: invokeinterface 813 7 0
        //     228: astore 9
        //     230: aload 9
        //     232: ifnull +21 -> 253
        //     235: aload_0
        //     236: aload 9
        //     238: invokeinterface 819 1 0
        //     243: putfield 756	android/media/MediaScanner:mOriginalCount	I
        //     246: aload 9
        //     248: invokeinterface 807 1 0
        //     253: return
        //     254: aload_0
        //     255: getfield 558	android/media/MediaScanner:mPlayLists	Ljava/util/ArrayList;
        //     258: invokevirtual 822	java/util/ArrayList:clear	()V
        //     261: goto -241 -> 20
        //     264: ldc_w 824
        //     267: astore 4
        //     269: iconst_1
        //     270: anewarray 118	java/lang/String
        //     273: astore 5
        //     275: aload 5
        //     277: iconst_0
        //     278: ldc_w 772
        //     281: aastore
        //     282: goto -235 -> 47
        //     285: aload_3
        //     286: invokeinterface 819 1 0
        //     291: ifeq -103 -> 188
        //     294: aload_0
        //     295: iconst_0
        //     296: putfield 438	android/media/MediaScanner:mWasEmptyPriorToScan	Z
        //     299: aload_3
        //     300: invokeinterface 827 1 0
        //     305: ifeq -184 -> 121
        //     308: aload_3
        //     309: iconst_0
        //     310: invokeinterface 831 2 0
        //     315: lstore 15
        //     317: aload_3
        //     318: iconst_1
        //     319: invokeinterface 835 2 0
        //     324: astore 17
        //     326: aload_3
        //     327: iconst_2
        //     328: invokeinterface 838 2 0
        //     333: istore 18
        //     335: aload_3
        //     336: iconst_3
        //     337: invokeinterface 831 2 0
        //     342: pop2
        //     343: lload 15
        //     345: lstore 10
        //     347: aload 17
        //     349: ifnull -50 -> 299
        //     352: aload 17
        //     354: ldc_w 840
        //     357: invokevirtual 621	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     360: istore 21
        //     362: iload 21
        //     364: ifeq -65 -> 299
        //     367: iconst_0
        //     368: istore 22
        //     370: getstatic 846	libcore/io/Libcore:os	Llibcore/io/Os;
        //     373: aload 17
        //     375: getstatic 851	libcore/io/OsConstants:F_OK	I
        //     378: invokeinterface 857 3 0
        //     383: istore 28
        //     385: iload 28
        //     387: istore 22
        //     389: iload 22
        //     391: ifne -92 -> 299
        //     394: iload 18
        //     396: invokestatic 863	android/mtp/MtpConstants:isAbstractObject	(I)Z
        //     399: ifne -100 -> 299
        //     402: aload 17
        //     404: invokestatic 869	android/media/MediaFile:getFileType	(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;
        //     407: astore 24
        //     409: aload 24
        //     411: ifnonnull +96 -> 507
        //     414: iconst_0
        //     415: istore 25
        //     417: iload 25
        //     419: invokestatic 872	android/media/MediaFile:isPlayListFileType	(I)Z
        //     422: ifne -123 -> 299
        //     425: aload 8
        //     427: lload 15
        //     429: invokevirtual 876	android/media/MediaScanner$MediaBulkDeleter:delete	(J)V
        //     432: aload 17
        //     434: getstatic 882	java/util/Locale:US	Ljava/util/Locale;
        //     437: invokevirtual 886	java/lang/String:toLowerCase	(Ljava/util/Locale;)Ljava/lang/String;
        //     440: ldc_w 888
        //     443: invokevirtual 891	java/lang/String:endsWith	(Ljava/lang/String;)Z
        //     446: ifeq -147 -> 299
        //     449: aload 8
        //     451: invokevirtual 816	android/media/MediaScanner$MediaBulkDeleter:flush	()V
        //     454: new 477	java/io/File
        //     457: dup
        //     458: aload 17
        //     460: invokespecial 680	java/io/File:<init>	(Ljava/lang/String;)V
        //     463: invokevirtual 894	java/io/File:getParent	()Ljava/lang/String;
        //     466: astore 26
        //     468: aload_0
        //     469: getfield 539	android/media/MediaScanner:mMediaProvider	Landroid/content/IContentProvider;
        //     472: ldc_w 896
        //     475: aload 26
        //     477: aconst_null
        //     478: invokeinterface 900 4 0
        //     483: pop
        //     484: goto -185 -> 299
        //     487: astore 12
        //     489: aload_3
        //     490: ifnull +9 -> 499
        //     493: aload_3
        //     494: invokeinterface 807 1 0
        //     499: aload 8
        //     501: invokevirtual 816	android/media/MediaScanner$MediaBulkDeleter:flush	()V
        //     504: aload 12
        //     506: athrow
        //     507: aload 24
        //     509: getfield 905	android/media/MediaFile$MediaFileType:fileType	I
        //     512: istore 25
        //     514: goto -97 -> 417
        //     517: astore 23
        //     519: goto -130 -> 389
        //
        // Exception table:
        //     from	to	target	type
        //     95	181	487	finally
        //     285	362	487	finally
        //     370	385	487	finally
        //     394	484	487	finally
        //     507	514	487	finally
        //     370	385	517	libcore/io/ErrnoException
    }

    private void processCachedPlaylist(Cursor paramCursor, ContentValues paramContentValues, Uri paramUri)
    {
        paramCursor.moveToPosition(-1);
        while ((paramCursor.moveToNext()) && (!matchEntries(paramCursor.getLong(0), paramCursor.getString(1))));
        int i = this.mPlaylistEntries.size();
        int j = 0;
        int k = 0;
        while (true)
            if (k < i)
            {
                PlaylistEntry localPlaylistEntry = (PlaylistEntry)this.mPlaylistEntries.get(k);
                if (localPlaylistEntry.bestmatchlevel > 0);
                try
                {
                    paramContentValues.clear();
                    paramContentValues.put("play_order", Integer.valueOf(j));
                    paramContentValues.put("audio_id", Long.valueOf(localPlaylistEntry.bestmatchid));
                    this.mMediaProvider.insert(paramUri, paramContentValues);
                    j++;
                    k++;
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("MediaScanner", "RemoteException in MediaScanner.processCachedPlaylist()", localRemoteException);
                }
            }
        while (true)
        {
            return;
            this.mPlaylistEntries.clear();
        }
    }

    private native void processDirectory(String paramString, MediaScannerClient paramMediaScannerClient);

    private native void processFile(String paramString1, String paramString2, MediaScannerClient paramMediaScannerClient);

    // ERROR //
    private void processM3uPlayList(String paramString1, String paramString2, Uri paramUri, ContentValues paramContentValues, Cursor paramCursor)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 6
        //     3: new 477	java/io/File
        //     6: dup
        //     7: aload_1
        //     8: invokespecial 680	java/io/File:<init>	(Ljava/lang/String;)V
        //     11: astore 7
        //     13: aload 7
        //     15: invokevirtual 721	java/io/File:exists	()Z
        //     18: ifeq +99 -> 117
        //     21: new 957	java/io/BufferedReader
        //     24: dup
        //     25: new 959	java/io/InputStreamReader
        //     28: dup
        //     29: new 961	java/io/FileInputStream
        //     32: dup
        //     33: aload 7
        //     35: invokespecial 964	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     38: invokespecial 967	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     41: sipush 8192
        //     44: invokespecial 970	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
        //     47: astore 17
        //     49: aload 17
        //     51: invokevirtual 973	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     54: astore 18
        //     56: aload_0
        //     57: getfield 448	android/media/MediaScanner:mPlaylistEntries	Ljava/util/ArrayList;
        //     60: invokevirtual 822	java/util/ArrayList:clear	()V
        //     63: aload 18
        //     65: ifnull +39 -> 104
        //     68: aload 18
        //     70: invokevirtual 582	java/lang/String:length	()I
        //     73: ifle +21 -> 94
        //     76: aload 18
        //     78: iconst_0
        //     79: invokevirtual 586	java/lang/String:charAt	(I)C
        //     82: bipush 35
        //     84: if_icmpeq +10 -> 94
        //     87: aload_0
        //     88: aload 18
        //     90: aload_2
        //     91: invokespecial 551	android/media/MediaScanner:cachePlaylistEntry	(Ljava/lang/String;Ljava/lang/String;)V
        //     94: aload 17
        //     96: invokevirtual 973	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     99: astore 18
        //     101: goto -38 -> 63
        //     104: aload_0
        //     105: aload 5
        //     107: aload 4
        //     109: aload_3
        //     110: invokespecial 975	android/media/MediaScanner:processCachedPlaylist	(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
        //     113: aload 17
        //     115: astore 6
        //     117: aload 6
        //     119: ifnull +8 -> 127
        //     122: aload 6
        //     124: invokevirtual 976	java/io/BufferedReader:close	()V
        //     127: return
        //     128: astore 11
        //     130: ldc 65
        //     132: ldc_w 978
        //     135: aload 11
        //     137: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     140: pop
        //     141: aload 6
        //     143: ifnull -16 -> 127
        //     146: aload 6
        //     148: invokevirtual 976	java/io/BufferedReader:close	()V
        //     151: goto -24 -> 127
        //     154: astore 13
        //     156: ldc 65
        //     158: astore 14
        //     160: ldc_w 978
        //     163: astore 15
        //     165: aload 14
        //     167: aload 15
        //     169: aload 13
        //     171: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     174: pop
        //     175: goto -48 -> 127
        //     178: astore 8
        //     180: aload 6
        //     182: ifnull +8 -> 190
        //     185: aload 6
        //     187: invokevirtual 976	java/io/BufferedReader:close	()V
        //     190: aload 8
        //     192: athrow
        //     193: astore 9
        //     195: ldc 65
        //     197: ldc_w 978
        //     200: aload 9
        //     202: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     205: pop
        //     206: goto -16 -> 190
        //     209: astore 13
        //     211: ldc 65
        //     213: astore 14
        //     215: ldc_w 978
        //     218: astore 15
        //     220: goto -55 -> 165
        //     223: astore 8
        //     225: aload 17
        //     227: astore 6
        //     229: goto -49 -> 180
        //     232: astore 11
        //     234: aload 17
        //     236: astore 6
        //     238: goto -108 -> 130
        //
        // Exception table:
        //     from	to	target	type
        //     3	49	128	java/io/IOException
        //     146	151	154	java/io/IOException
        //     3	49	178	finally
        //     130	141	178	finally
        //     185	190	193	java/io/IOException
        //     122	127	209	java/io/IOException
        //     49	113	223	finally
        //     49	113	232	java/io/IOException
    }

    private void processPlayList(FileEntry paramFileEntry, Cursor paramCursor)
        throws RemoteException
    {
        String str1 = paramFileEntry.mPath;
        ContentValues localContentValues = new ContentValues();
        int i = str1.lastIndexOf('/');
        if (i < 0)
            throw new IllegalArgumentException("bad path " + str1);
        long l = paramFileEntry.mRowId;
        String str2 = localContentValues.getAsString("name");
        int k;
        if (str2 == null)
        {
            str2 = localContentValues.getAsString("title");
            if (str2 == null)
            {
                k = str1.lastIndexOf('.');
                if (k >= 0)
                    break label231;
                str2 = str1.substring(i + 1);
            }
        }
        localContentValues.put("name", str2);
        localContentValues.put("date_modified", Long.valueOf(paramFileEntry.mLastModified));
        Uri localUri2;
        label186: String str3;
        MediaFile.MediaFileType localMediaFileType;
        int j;
        if (l == 0L)
        {
            localContentValues.put("_data", str1);
            Uri localUri3 = this.mMediaProvider.insert(this.mPlaylistsUri, localContentValues);
            ContentUris.parseId(localUri3);
            localUri2 = Uri.withAppendedPath(localUri3, "members");
            str3 = str1.substring(0, i + 1);
            localMediaFileType = MediaFile.getFileType(str1);
            if (localMediaFileType != null)
                break label300;
            j = 0;
            label211: if (j != 41)
                break label310;
            processM3uPlayList(str1, str3, localUri2, localContentValues, paramCursor);
        }
        while (true)
        {
            return;
            label231: str2 = str1.substring(i + 1, k);
            break;
            Uri localUri1 = ContentUris.withAppendedId(this.mPlaylistsUri, l);
            this.mMediaProvider.update(localUri1, localContentValues, null, null);
            localUri2 = Uri.withAppendedPath(localUri1, "members");
            this.mMediaProvider.delete(localUri2, null, null);
            break label186;
            label300: j = localMediaFileType.fileType;
            break label211;
            label310: if (j == 42)
                processPlsPlayList(str1, str3, localUri2, localContentValues, paramCursor);
            else if (j == 43)
                processWplPlayList(str1, str3, localUri2, localContentValues, paramCursor);
        }
    }

    // ERROR //
    private void processPlayLists()
        throws RemoteException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 558	android/media/MediaScanner:mPlayLists	Ljava/util/ArrayList;
        //     4: invokevirtual 1041	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     7: astore_1
        //     8: aconst_null
        //     9: astore_2
        //     10: aload_0
        //     11: getfield 539	android/media/MediaScanner:mMediaProvider	Landroid/content/IContentProvider;
        //     14: aload_0
        //     15: getfield 492	android/media/MediaScanner:mFilesUri	Landroid/net/Uri;
        //     18: getstatic 128	android/media/MediaScanner:FILES_PRESCAN_PROJECTION	[Ljava/lang/String;
        //     21: ldc_w 1043
        //     24: aconst_null
        //     25: aconst_null
        //     26: aconst_null
        //     27: invokeinterface 813 7 0
        //     32: astore_2
        //     33: aload_1
        //     34: invokeinterface 1048 1 0
        //     39: ifeq +58 -> 97
        //     42: aload_1
        //     43: invokeinterface 1052 1 0
        //     48: checkcast 20	android/media/MediaScanner$FileEntry
        //     51: astore 5
        //     53: aload 5
        //     55: getfield 1055	android/media/MediaScanner$FileEntry:mLastModifiedChanged	Z
        //     58: ifeq -25 -> 33
        //     61: aload_0
        //     62: aload 5
        //     64: aload_2
        //     65: invokespecial 1057	android/media/MediaScanner:processPlayList	(Landroid/media/MediaScanner$FileEntry;Landroid/database/Cursor;)V
        //     68: goto -35 -> 33
        //     71: astore 4
        //     73: aload_2
        //     74: ifnull +9 -> 83
        //     77: aload_2
        //     78: invokeinterface 807 1 0
        //     83: return
        //     84: astore_3
        //     85: aload_2
        //     86: ifnull +9 -> 95
        //     89: aload_2
        //     90: invokeinterface 807 1 0
        //     95: aload_3
        //     96: athrow
        //     97: aload_2
        //     98: ifnull -15 -> 83
        //     101: goto -24 -> 77
        //
        // Exception table:
        //     from	to	target	type
        //     10	68	71	android/os/RemoteException
        //     10	68	84	finally
    }

    // ERROR //
    private void processPlsPlayList(String paramString1, String paramString2, Uri paramUri, ContentValues paramContentValues, Cursor paramCursor)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 6
        //     3: new 477	java/io/File
        //     6: dup
        //     7: aload_1
        //     8: invokespecial 680	java/io/File:<init>	(Ljava/lang/String;)V
        //     11: astore 7
        //     13: aload 7
        //     15: invokevirtual 721	java/io/File:exists	()Z
        //     18: ifeq +112 -> 130
        //     21: new 957	java/io/BufferedReader
        //     24: dup
        //     25: new 959	java/io/InputStreamReader
        //     28: dup
        //     29: new 961	java/io/FileInputStream
        //     32: dup
        //     33: aload 7
        //     35: invokespecial 964	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     38: invokespecial 967	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     41: sipush 8192
        //     44: invokespecial 970	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
        //     47: astore 17
        //     49: aload 17
        //     51: invokevirtual 973	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     54: astore 18
        //     56: aload_0
        //     57: getfield 448	android/media/MediaScanner:mPlaylistEntries	Ljava/util/ArrayList;
        //     60: invokevirtual 822	java/util/ArrayList:clear	()V
        //     63: aload 18
        //     65: ifnull +52 -> 117
        //     68: aload 18
        //     70: ldc_w 1059
        //     73: invokevirtual 621	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     76: ifeq +31 -> 107
        //     79: aload 18
        //     81: bipush 61
        //     83: invokevirtual 1061	java/lang/String:indexOf	(I)I
        //     86: istore 19
        //     88: iload 19
        //     90: ifle +17 -> 107
        //     93: aload_0
        //     94: aload 18
        //     96: iload 19
        //     98: iconst_1
        //     99: iadd
        //     100: invokevirtual 1001	java/lang/String:substring	(I)Ljava/lang/String;
        //     103: aload_2
        //     104: invokespecial 551	android/media/MediaScanner:cachePlaylistEntry	(Ljava/lang/String;Ljava/lang/String;)V
        //     107: aload 17
        //     109: invokevirtual 973	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     112: astore 18
        //     114: goto -51 -> 63
        //     117: aload_0
        //     118: aload 5
        //     120: aload 4
        //     122: aload_3
        //     123: invokespecial 975	android/media/MediaScanner:processCachedPlaylist	(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
        //     126: aload 17
        //     128: astore 6
        //     130: aload 6
        //     132: ifnull +8 -> 140
        //     135: aload 6
        //     137: invokevirtual 976	java/io/BufferedReader:close	()V
        //     140: return
        //     141: astore 11
        //     143: ldc 65
        //     145: ldc_w 1063
        //     148: aload 11
        //     150: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     153: pop
        //     154: aload 6
        //     156: ifnull -16 -> 140
        //     159: aload 6
        //     161: invokevirtual 976	java/io/BufferedReader:close	()V
        //     164: goto -24 -> 140
        //     167: astore 13
        //     169: ldc 65
        //     171: astore 14
        //     173: ldc_w 1063
        //     176: astore 15
        //     178: aload 14
        //     180: aload 15
        //     182: aload 13
        //     184: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     187: pop
        //     188: goto -48 -> 140
        //     191: astore 8
        //     193: aload 6
        //     195: ifnull +8 -> 203
        //     198: aload 6
        //     200: invokevirtual 976	java/io/BufferedReader:close	()V
        //     203: aload 8
        //     205: athrow
        //     206: astore 9
        //     208: ldc 65
        //     210: ldc_w 1063
        //     213: aload 9
        //     215: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     218: pop
        //     219: goto -16 -> 203
        //     222: astore 13
        //     224: ldc 65
        //     226: astore 14
        //     228: ldc_w 1063
        //     231: astore 15
        //     233: goto -55 -> 178
        //     236: astore 8
        //     238: aload 17
        //     240: astore 6
        //     242: goto -49 -> 193
        //     245: astore 11
        //     247: aload 17
        //     249: astore 6
        //     251: goto -108 -> 143
        //
        // Exception table:
        //     from	to	target	type
        //     3	49	141	java/io/IOException
        //     159	164	167	java/io/IOException
        //     3	49	191	finally
        //     143	154	191	finally
        //     198	203	206	java/io/IOException
        //     135	140	222	java/io/IOException
        //     49	126	236	finally
        //     49	126	245	java/io/IOException
    }

    // ERROR //
    private void processWplPlayList(String paramString1, String paramString2, Uri paramUri, ContentValues paramContentValues, Cursor paramCursor)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 6
        //     3: new 477	java/io/File
        //     6: dup
        //     7: aload_1
        //     8: invokespecial 680	java/io/File:<init>	(Ljava/lang/String;)V
        //     11: astore 7
        //     13: aload 7
        //     15: invokevirtual 721	java/io/File:exists	()Z
        //     18: ifeq +60 -> 78
        //     21: new 961	java/io/FileInputStream
        //     24: dup
        //     25: aload 7
        //     27: invokespecial 964	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     30: astore 17
        //     32: aload_0
        //     33: getfield 448	android/media/MediaScanner:mPlaylistEntries	Ljava/util/ArrayList;
        //     36: invokevirtual 822	java/util/ArrayList:clear	()V
        //     39: aload 17
        //     41: ldc_w 1067
        //     44: invokestatic 1073	android/util/Xml:findEncodingByName	(Ljava/lang/String;)Landroid/util/Xml$Encoding;
        //     47: new 8	android/media/MediaScanner$WplHandler
        //     50: dup
        //     51: aload_0
        //     52: aload_2
        //     53: aload_3
        //     54: aload 5
        //     56: invokespecial 1076	android/media/MediaScanner$WplHandler:<init>	(Landroid/media/MediaScanner;Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)V
        //     59: invokevirtual 1080	android/media/MediaScanner$WplHandler:getContentHandler	()Lorg/xml/sax/ContentHandler;
        //     62: invokestatic 1084	android/util/Xml:parse	(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
        //     65: aload_0
        //     66: aload 5
        //     68: aload 4
        //     70: aload_3
        //     71: invokespecial 975	android/media/MediaScanner:processCachedPlaylist	(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
        //     74: aload 17
        //     76: astore 6
        //     78: aload 6
        //     80: ifnull +8 -> 88
        //     83: aload 6
        //     85: invokevirtual 1085	java/io/FileInputStream:close	()V
        //     88: return
        //     89: astore 16
        //     91: aload 16
        //     93: invokevirtual 1088	org/xml/sax/SAXException:printStackTrace	()V
        //     96: aload 6
        //     98: ifnull -10 -> 88
        //     101: aload 6
        //     103: invokevirtual 1085	java/io/FileInputStream:close	()V
        //     106: goto -18 -> 88
        //     109: astore 12
        //     111: ldc 65
        //     113: astore 13
        //     115: ldc_w 1090
        //     118: astore 14
        //     120: aload 13
        //     122: aload 14
        //     124: aload 12
        //     126: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     129: pop
        //     130: goto -42 -> 88
        //     133: astore 11
        //     135: aload 11
        //     137: invokevirtual 1091	java/io/IOException:printStackTrace	()V
        //     140: aload 6
        //     142: ifnull -54 -> 88
        //     145: aload 6
        //     147: invokevirtual 1085	java/io/FileInputStream:close	()V
        //     150: goto -62 -> 88
        //     153: astore 12
        //     155: ldc 65
        //     157: astore 13
        //     159: ldc_w 1090
        //     162: astore 14
        //     164: goto -44 -> 120
        //     167: astore 8
        //     169: aload 6
        //     171: ifnull +8 -> 179
        //     174: aload 6
        //     176: invokevirtual 1085	java/io/FileInputStream:close	()V
        //     179: aload 8
        //     181: athrow
        //     182: astore 9
        //     184: ldc 65
        //     186: ldc_w 1090
        //     189: aload 9
        //     191: invokestatic 949	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     194: pop
        //     195: goto -16 -> 179
        //     198: astore 12
        //     200: ldc 65
        //     202: astore 13
        //     204: ldc_w 1090
        //     207: astore 14
        //     209: goto -89 -> 120
        //     212: astore 8
        //     214: aload 17
        //     216: astore 6
        //     218: goto -49 -> 169
        //     221: astore 11
        //     223: aload 17
        //     225: astore 6
        //     227: goto -92 -> 135
        //     230: astore 16
        //     232: aload 17
        //     234: astore 6
        //     236: goto -145 -> 91
        //
        // Exception table:
        //     from	to	target	type
        //     3	32	89	org/xml/sax/SAXException
        //     101	106	109	java/io/IOException
        //     3	32	133	java/io/IOException
        //     145	150	153	java/io/IOException
        //     3	32	167	finally
        //     91	96	167	finally
        //     135	140	167	finally
        //     174	179	182	java/io/IOException
        //     83	88	198	java/io/IOException
        //     32	74	212	finally
        //     32	74	221	java/io/IOException
        //     32	74	230	org/xml/sax/SAXException
    }

    private void pruneDeadThumbnailFiles()
    {
        HashSet localHashSet = new HashSet();
        String[] arrayOfString1 = new File("/sdcard/DCIM/.thumbnails").list();
        if (arrayOfString1 == null)
            arrayOfString1 = new String[0];
        for (int i = 0; i < arrayOfString1.length; i++)
            localHashSet.add("/sdcard/DCIM/.thumbnails" + "/" + arrayOfString1[i]);
        try
        {
            IContentProvider localIContentProvider = this.mMediaProvider;
            Uri localUri = this.mThumbsUri;
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = "_data";
            Cursor localCursor = localIContentProvider.query(localUri, arrayOfString2, null, null, null, null);
            Log.v("MediaScanner", "pruneDeadThumbnailFiles... " + localCursor);
            if ((localCursor != null) && (localCursor.moveToFirst()))
                do
                    localHashSet.remove(localCursor.getString(0));
                while (localCursor.moveToNext());
            Iterator localIterator = localHashSet.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                try
                {
                    new File(str).delete();
                }
                catch (SecurityException localSecurityException)
                {
                }
            }
            Log.v("MediaScanner", "/pruneDeadThumbnailFiles... " + localCursor);
            if (localCursor != null)
                localCursor.close();
            label272: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label272;
        }
    }

    private void setDefaultRingtoneFileNames()
    {
        this.mDefaultRingtoneFilename = SystemProperties.get("ro.config.ringtone");
        this.mDefaultNotificationFilename = SystemProperties.get("ro.config.notification_sound");
        this.mDefaultAlarmAlertFilename = SystemProperties.get("ro.config.alarm_alert");
    }

    public native byte[] extractAlbumArt(FileDescriptor paramFileDescriptor);

    protected void finalize()
    {
        this.mContext.getContentResolver().releaseProvider(this.mMediaProvider);
        native_finalize();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Context getContext()
    {
        return this.mContext;
    }

    // ERROR //
    FileEntry makeEntryFor(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 668	android/media/MediaScanner:mCaseInsensitivePaths	Z
        //     4: ifeq +108 -> 112
        //     7: ldc_w 1146
        //     10: astore_2
        //     11: iconst_1
        //     12: anewarray 118	java/lang/String
        //     15: astore_3
        //     16: aload_3
        //     17: iconst_0
        //     18: aload_1
        //     19: aastore
        //     20: aconst_null
        //     21: astore 4
        //     23: aload_0
        //     24: getfield 539	android/media/MediaScanner:mMediaProvider	Landroid/content/IContentProvider;
        //     27: aload_0
        //     28: getfield 492	android/media/MediaScanner:mFilesUri	Landroid/net/Uri;
        //     31: getstatic 128	android/media/MediaScanner:FILES_PRESCAN_PROJECTION	[Ljava/lang/String;
        //     34: aload_2
        //     35: aload_3
        //     36: aconst_null
        //     37: aconst_null
        //     38: invokeinterface 813 7 0
        //     43: astore 4
        //     45: aload 4
        //     47: invokeinterface 827 1 0
        //     52: ifeq +113 -> 165
        //     55: aload 4
        //     57: iconst_0
        //     58: invokeinterface 831 2 0
        //     63: lstore 8
        //     65: aload 4
        //     67: iconst_2
        //     68: invokeinterface 838 2 0
        //     73: istore 10
        //     75: new 20	android/media/MediaScanner$FileEntry
        //     78: dup
        //     79: lload 8
        //     81: aload_1
        //     82: aload 4
        //     84: iconst_3
        //     85: invokeinterface 831 2 0
        //     90: iload 10
        //     92: invokespecial 1149	android/media/MediaScanner$FileEntry:<init>	(JLjava/lang/String;JI)V
        //     95: astore 6
        //     97: aload 4
        //     99: ifnull +10 -> 109
        //     102: aload 4
        //     104: invokeinterface 807 1 0
        //     109: aload 6
        //     111: areturn
        //     112: ldc_w 1151
        //     115: astore_2
        //     116: iconst_1
        //     117: anewarray 118	java/lang/String
        //     120: astore_3
        //     121: aload_3
        //     122: iconst_0
        //     123: aload_1
        //     124: aastore
        //     125: goto -105 -> 20
        //     128: astore 7
        //     130: aload 4
        //     132: ifnull +10 -> 142
        //     135: aload 4
        //     137: invokeinterface 807 1 0
        //     142: aload 7
        //     144: athrow
        //     145: astore 5
        //     147: aload 4
        //     149: ifnull +10 -> 159
        //     152: aload 4
        //     154: invokeinterface 807 1 0
        //     159: aconst_null
        //     160: astore 6
        //     162: goto -53 -> 109
        //     165: aload 4
        //     167: ifnull -8 -> 159
        //     170: goto -18 -> 152
        //
        // Exception table:
        //     from	to	target	type
        //     23	97	128	finally
        //     23	97	145	android/os/RemoteException
    }

    public void release()
    {
        native_finalize();
    }

    public void scanDirectories(String[] paramArrayOfString, String paramString)
    {
        try
        {
            System.currentTimeMillis();
            initialize(paramString);
            prescan(null, true);
            System.currentTimeMillis();
            this.mMediaInserter = new MediaInserter(this.mMediaProvider, 500);
            for (int i = 0; i < paramArrayOfString.length; i++)
                processDirectory(paramArrayOfString[i], this.mClient);
            this.mMediaInserter.flushAll();
            this.mMediaInserter = null;
            System.currentTimeMillis();
            postscan(paramArrayOfString);
            System.currentTimeMillis();
            return;
        }
        catch (SQLException localSQLException)
        {
            while (true)
                Log.e("MediaScanner", "SQLException in MediaScanner.scan()", localSQLException);
        }
        catch (UnsupportedOperationException localUnsupportedOperationException)
        {
            while (true)
                Log.e("MediaScanner", "UnsupportedOperationException in MediaScanner.scan()", localUnsupportedOperationException);
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("MediaScanner", "RemoteException in MediaScanner.scan()", localRemoteException);
        }
    }

    public void scanMtpFile(String paramString1, String paramString2, int paramInt1, int paramInt2)
    {
        initialize(paramString2);
        MediaFile.MediaFileType localMediaFileType = MediaFile.getFileType(paramString1);
        int i;
        File localFile;
        long l1;
        ContentValues localContentValues;
        if (localMediaFileType == null)
        {
            i = 0;
            localFile = new File(paramString1);
            l1 = localFile.lastModified() / 1000L;
            if ((MediaFile.isAudioFileType(i)) || (MediaFile.isVideoFileType(i)) || (MediaFile.isImageFileType(i)) || (MediaFile.isPlayListFileType(i)))
                break label171;
            localContentValues = new ContentValues();
            localContentValues.put("_size", Long.valueOf(localFile.length()));
            localContentValues.put("date_modified", Long.valueOf(l1));
        }
        while (true)
        {
            try
            {
                String[] arrayOfString = new String[1];
                arrayOfString[0] = Integer.toString(paramInt1);
                this.mMediaProvider.update(MediaStore.Files.getMtpObjectsUri(paramString2), localContentValues, "_id=?", arrayOfString);
                return;
                i = localMediaFileType.fileType;
            }
            catch (RemoteException localRemoteException2)
            {
                Log.e("MediaScanner", "RemoteException in scanMtpFile", localRemoteException2);
                continue;
            }
            label171: this.mMtpObjectHandle = paramInt1;
            Cursor localCursor = null;
            try
            {
                if (MediaFile.isPlayListFileType(i))
                {
                    prescan(null, true);
                    FileEntry localFileEntry = makeEntryFor(paramString1);
                    if (localFileEntry != null)
                    {
                        localCursor = this.mMediaProvider.query(this.mFilesUri, FILES_PRESCAN_PROJECTION, null, null, null, null);
                        processPlayList(localFileEntry, localCursor);
                    }
                }
                while (true)
                {
                    this.mMtpObjectHandle = 0;
                    if (localCursor == null)
                        break;
                    localCursor.close();
                    break;
                    prescan(paramString1, false);
                    MyMediaScannerClient localMyMediaScannerClient = this.mClient;
                    String str = localMediaFileType.mimeType;
                    long l2 = localFile.length();
                    if (paramInt2 != 12289)
                        break label341;
                    bool = true;
                    localMyMediaScannerClient.doScanFile(paramString1, str, l1, l2, bool, true, isNoMediaPath(paramString1));
                }
            }
            catch (RemoteException localRemoteException1)
            {
                while (true)
                {
                    Log.e("MediaScanner", "RemoteException in MediaScanner.scanFile()", localRemoteException1);
                    this.mMtpObjectHandle = 0;
                    if (localCursor == null)
                        break;
                    continue;
                    label341: boolean bool = false;
                }
            }
            finally
            {
                this.mMtpObjectHandle = 0;
                if (localCursor != null)
                    localCursor.close();
            }
        }
    }

    public Uri scanSingleFile(String paramString1, String paramString2, String paramString3)
    {
        try
        {
            initialize(paramString2);
            prescan(paramString1, true);
            File localFile = new File(paramString1);
            long l = localFile.lastModified() / 1000L;
            Uri localUri2 = this.mClient.doScanFile(paramString1, paramString3, l, localFile.length(), false, true, false);
            localUri1 = localUri2;
            return localUri1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("MediaScanner", "RemoteException in MediaScanner.scanFile()", localRemoteException);
                Uri localUri1 = null;
            }
        }
    }

    public native void setLocale(String paramString);

    class WplHandler
        implements ElementListener
    {
        final ContentHandler handler;
        String playListDirectory;

        public WplHandler(String paramUri, Uri paramCursor, Cursor arg4)
        {
            this.playListDirectory = paramUri;
            RootElement localRootElement = new RootElement("smil");
            localRootElement.getChild("body").getChild("seq").getChild("media").setElementListener(this);
            this.handler = localRootElement.getContentHandler();
        }

        public void end()
        {
        }

        ContentHandler getContentHandler()
        {
            return this.handler;
        }

        public void start(Attributes paramAttributes)
        {
            String str = paramAttributes.getValue("", "src");
            if (str != null)
                MediaScanner.this.cachePlaylistEntry(str, this.playListDirectory);
        }
    }

    static class MediaBulkDeleter
    {
        Uri mBaseUri;
        IContentProvider mProvider;
        ArrayList<String> whereArgs = new ArrayList(100);
        StringBuilder whereClause = new StringBuilder();

        public MediaBulkDeleter(IContentProvider paramIContentProvider, Uri paramUri)
        {
            this.mProvider = paramIContentProvider;
            this.mBaseUri = paramUri;
        }

        public void delete(long paramLong)
            throws RemoteException
        {
            if (this.whereClause.length() != 0)
                this.whereClause.append(",");
            this.whereClause.append("?");
            this.whereArgs.add("" + paramLong);
            if (this.whereArgs.size() > 100)
                flush();
        }

        public void flush()
            throws RemoteException
        {
            int i = this.whereArgs.size();
            if (i > 0)
            {
                String[] arrayOfString1 = new String[i];
                String[] arrayOfString2 = (String[])this.whereArgs.toArray(arrayOfString1);
                this.mProvider.delete(this.mBaseUri, "_id IN (" + this.whereClause.toString() + ")", arrayOfString2);
                this.whereClause.setLength(0);
                this.whereArgs.clear();
            }
        }
    }

    private class MyMediaScannerClient
        implements MediaScannerClient
    {
        private String mAlbum;
        private String mAlbumArtist;
        private String mArtist;
        private int mCompilation;
        private String mComposer;
        private int mDuration;
        private long mFileSize;
        private int mFileType;
        private String mGenre;
        private int mHeight;
        private boolean mIsDrm;
        private long mLastModified;
        private String mMimeType;
        private boolean mNoMedia;
        private String mPath;
        private String mTitle;
        private int mTrack;
        private int mWidth;
        private String mWriter;
        private int mYear;

        private MyMediaScannerClient()
        {
        }

        private boolean convertGenreCode(String paramString1, String paramString2)
        {
            String str = getGenreName(paramString1);
            if (str.equals(paramString2));
            for (boolean bool = true; ; bool = false)
            {
                return bool;
                Log.d("MediaScanner", "'" + paramString1 + "' -> '" + str + "', expected '" + paramString2 + "'");
            }
        }

        private boolean doesPathHaveFilename(String paramString1, String paramString2)
        {
            boolean bool = false;
            int i = 1 + paramString1.lastIndexOf(File.separatorChar);
            int j = paramString2.length();
            if ((paramString1.regionMatches(i, paramString2, 0, j)) && (i + j == paramString1.length()))
                bool = true;
            return bool;
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        private Uri endFile(MediaScanner.FileEntry paramFileEntry, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
            throws RemoteException
        {
            if ((this.mArtist == null) || (this.mArtist.length() == 0))
                this.mArtist = this.mAlbumArtist;
            ContentValues localContentValues = toValues();
            String str1 = localContentValues.getAsString("title");
            if ((str1 == null) || (TextUtils.isEmpty(str1.trim())))
                localContentValues.put("title", MediaFile.getFileTitle(localContentValues.getAsString("_data")));
            int i3;
            int i4;
            if ("<unknown>".equals(localContentValues.getAsString("album")))
            {
                String str2 = localContentValues.getAsString("_data");
                int i2 = str2.lastIndexOf('/');
                if (i2 >= 0)
                {
                    i3 = 0;
                    i4 = str2.indexOf('/', i3 + 1);
                    if ((i4 >= 0) && (i4 < i2))
                        break label537;
                    if (i3 != 0)
                        localContentValues.put("album", str2.substring(i3 + 1, i2));
                }
            }
            long l1 = paramFileEntry.mRowId;
            if ((MediaFile.isAudioFileType(this.mFileType)) && ((l1 == 0L) || (MediaScanner.this.mMtpObjectHandle != 0)))
            {
                localContentValues.put("is_ringtone", Boolean.valueOf(paramBoolean1));
                localContentValues.put("is_notification", Boolean.valueOf(paramBoolean2));
                localContentValues.put("is_alarm", Boolean.valueOf(paramBoolean3));
                localContentValues.put("is_music", Boolean.valueOf(paramBoolean4));
                localContentValues.put("is_podcast", Boolean.valueOf(paramBoolean5));
            }
            while (true)
            {
                Uri localUri1 = MediaScanner.this.mFilesUri;
                MediaInserter localMediaInserter = MediaScanner.this.mMediaInserter;
                label297: Uri localUri2;
                int k;
                label447: label475: Object localObject;
                if (!this.mNoMedia)
                {
                    if (MediaFile.isVideoFileType(this.mFileType))
                        localUri1 = MediaScanner.this.mVideoUri;
                }
                else
                {
                    localUri2 = null;
                    k = 0;
                    if (l1 != 0L)
                        break label996;
                    if (MediaScanner.this.mMtpObjectHandle != 0)
                        localContentValues.put("media_scanner_new_object_id", Integer.valueOf(MediaScanner.this.mMtpObjectHandle));
                    Uri localUri3 = MediaScanner.this.mFilesUri;
                    if (localUri1 == localUri3)
                    {
                        int i1 = paramFileEntry.mFormat;
                        if (i1 == 0)
                            i1 = MediaFile.getFormatCode(paramFileEntry.mPath, this.mMimeType);
                        localContentValues.put("format", Integer.valueOf(i1));
                    }
                    if (MediaScanner.this.mWasEmptyPriorToScan)
                    {
                        if ((!paramBoolean2) || (MediaScanner.this.mDefaultNotificationSet))
                            break label859;
                        if ((TextUtils.isEmpty(MediaScanner.this.mDefaultNotificationFilename)) || (doesPathHaveFilename(paramFileEntry.mPath, MediaScanner.this.mDefaultNotificationFilename)))
                            k = 1;
                    }
                    if ((localMediaInserter != null) && (k == 0))
                        break label962;
                    localUri2 = MediaScanner.this.mMediaProvider.insert(localUri1, localContentValues);
                    if (localUri2 != null)
                    {
                        l1 = ContentUris.parseId(localUri2);
                        paramFileEntry.mRowId = l1;
                    }
                    if (k != 0)
                    {
                        MediaScanner.Injector.setAllSettingsIfNotSet(MediaScanner.this, paramFileEntry, paramBoolean1, paramBoolean2, paramBoolean3);
                        if (!paramBoolean2)
                            break label1122;
                        setSettingIfNotSet("notification_sound", localUri1, l1);
                        MediaScanner.access$1602(MediaScanner.this, true);
                    }
                    label534: return localUri2;
                    label537: i3 = i4;
                    break;
                    if ((this.mFileType != 31) || (this.mNoMedia))
                        continue;
                    localObject = null;
                }
                try
                {
                    ExifInterface localExifInterface = new ExifInterface(paramFileEntry.mPath);
                    localObject = localExifInterface;
                    label580: if (localObject == null)
                        continue;
                    float[] arrayOfFloat = new float[2];
                    if (localObject.getLatLong(arrayOfFloat))
                    {
                        localContentValues.put("latitude", Float.valueOf(arrayOfFloat[0]));
                        localContentValues.put("longitude", Float.valueOf(arrayOfFloat[1]));
                    }
                    long l2 = localObject.getGpsDateTime();
                    label659: int j;
                    if (l2 != -1L)
                    {
                        localContentValues.put("datetaken", Long.valueOf(l2));
                        int i = localObject.getAttributeInt("Orientation", -1);
                        if (i == -1)
                            continue;
                        switch (i)
                        {
                        case 4:
                        case 5:
                        case 7:
                        default:
                            j = 0;
                        case 6:
                        case 3:
                        case 8:
                        }
                    }
                    while (true)
                    {
                        localContentValues.put("orientation", Integer.valueOf(j));
                        break;
                        long l3 = localObject.getDateTime();
                        if ((l3 == -1L) || (Math.abs(1000L * this.mLastModified - l3) < 86400000L))
                            break label659;
                        localContentValues.put("datetaken", Long.valueOf(l3));
                        break label659;
                        j = 90;
                        continue;
                        j = 180;
                        continue;
                        j = 270;
                    }
                    if (MediaFile.isImageFileType(this.mFileType))
                    {
                        localUri1 = MediaScanner.this.mImagesUri;
                        break label297;
                    }
                    if (!MediaFile.isAudioFileType(this.mFileType))
                        break label297;
                    localUri1 = MediaScanner.this.mAudioUri;
                    break label297;
                    label859: if ((paramBoolean1) && (!MediaScanner.this.mDefaultRingtoneSet))
                    {
                        if ((!TextUtils.isEmpty(MediaScanner.this.mDefaultRingtoneFilename)) && (!doesPathHaveFilename(paramFileEntry.mPath, MediaScanner.this.mDefaultRingtoneFilename)))
                            break label447;
                        k = 1;
                        break label447;
                    }
                    if ((!paramBoolean3) || (MediaScanner.this.mDefaultAlarmSet) || ((!TextUtils.isEmpty(MediaScanner.this.mDefaultAlarmAlertFilename)) && (!doesPathHaveFilename(paramFileEntry.mPath, MediaScanner.this.mDefaultAlarmAlertFilename))))
                        break label447;
                    k = 1;
                    break label447;
                    label962: if (paramFileEntry.mFormat == 12289)
                    {
                        localMediaInserter.insertwithPriority(localUri1, localContentValues);
                        break label475;
                    }
                    localMediaInserter.insert(localUri1, localContentValues);
                    break label475;
                    label996: localUri2 = ContentUris.withAppendedId(localUri1, l1);
                    localContentValues.remove("_data");
                    int m = 0;
                    int n;
                    if (!MediaScanner.isNoMediaPath(paramFileEntry.mPath))
                    {
                        n = MediaFile.getFileTypeForMimeType(this.mMimeType);
                        if (!MediaFile.isAudioFileType(n))
                            break label1080;
                        m = 2;
                    }
                    while (true)
                    {
                        localContentValues.put("media_type", Integer.valueOf(m));
                        MediaScanner.this.mMediaProvider.update(localUri2, localContentValues, null, null);
                        break;
                        label1080: if (MediaFile.isVideoFileType(n))
                            m = 3;
                        else if (MediaFile.isImageFileType(n))
                            m = 1;
                        else if (MediaFile.isPlayListFileType(n))
                            m = 4;
                    }
                    label1122: if (paramBoolean1)
                    {
                        setSettingIfNotSet("ringtone", localUri1, l1);
                        MediaScanner.access$1802(MediaScanner.this, true);
                        break label534;
                    }
                    if (!paramBoolean3)
                        break label534;
                    setSettingIfNotSet("alarm_alert", localUri1, l1);
                    MediaScanner.access$2002(MediaScanner.this, true);
                }
                catch (IOException localIOException)
                {
                    break label580;
                }
            }
        }

        private int getFileTypeFromDrm(String paramString)
        {
            int i;
            if (!MediaScanner.this.isDrmEnabled())
                i = 0;
            while (true)
            {
                return i;
                i = 0;
                if (MediaScanner.this.mDrmManagerClient == null)
                    MediaScanner.access$2402(MediaScanner.this, new DrmManagerClient(MediaScanner.this.mContext));
                if (MediaScanner.this.mDrmManagerClient.canHandle(paramString, null))
                {
                    String str = MediaScanner.this.mDrmManagerClient.getOriginalMimeType(paramString);
                    if (str != null)
                    {
                        this.mMimeType = str;
                        i = MediaFile.getFileTypeForMimeType(str);
                    }
                }
            }
        }

        private int parseSubstring(String paramString, int paramInt1, int paramInt2)
        {
            int i = paramString.length();
            if (paramInt1 == i);
            while (true)
            {
                return paramInt2;
                int j = paramInt1 + 1;
                int k = paramString.charAt(paramInt1);
                if ((k < 48) || (k <= 57))
                {
                    int m = k - 48;
                    while (true)
                    {
                        if (j >= i)
                            break label114;
                        int n = j + 1;
                        int i1 = paramString.charAt(j);
                        if ((i1 < 48) || (i1 > 57))
                        {
                            paramInt2 = m;
                            break;
                        }
                        m = m * 10 + (i1 - 48);
                        j = n;
                    }
                    label114: paramInt2 = m;
                }
            }
        }

        private void processImageFile(String paramString)
        {
            try
            {
                MediaScanner.this.mBitmapOptions.outWidth = 0;
                MediaScanner.this.mBitmapOptions.outHeight = 0;
                BitmapFactory.decodeFile(paramString, MediaScanner.this.mBitmapOptions);
                this.mWidth = MediaScanner.this.mBitmapOptions.outWidth;
                this.mHeight = MediaScanner.this.mBitmapOptions.outHeight;
                label62: return;
            }
            catch (Throwable localThrowable)
            {
                break label62;
            }
        }

        private void setSettingIfNotSet(String paramString, Uri paramUri, long paramLong)
        {
            if (TextUtils.isEmpty(Settings.System.getString(MediaScanner.this.mContext.getContentResolver(), paramString)))
                Settings.System.putString(MediaScanner.this.mContext.getContentResolver(), paramString, ContentUris.withAppendedId(paramUri, paramLong).toString());
        }

        private void testGenreNameConverter()
        {
            convertGenreCode("2", "Country");
            convertGenreCode("(2)", "Country");
            convertGenreCode("(2", "(2");
            convertGenreCode("2 Foo", "Country");
            convertGenreCode("(2) Foo", "Country");
            convertGenreCode("(2 Foo", "(2 Foo");
            convertGenreCode("2Foo", "2Foo");
            convertGenreCode("(2)Foo", "Country");
            convertGenreCode("200 Foo", "Foo");
            convertGenreCode("(200) Foo", "Foo");
            convertGenreCode("200Foo", "200Foo");
            convertGenreCode("(200)Foo", "Foo");
            convertGenreCode("200)Foo", "200)Foo");
            convertGenreCode("200) Foo", "200) Foo");
        }

        private ContentValues toValues()
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("_data", this.mPath);
            localContentValues.put("title", this.mTitle);
            localContentValues.put("date_modified", Long.valueOf(this.mLastModified));
            localContentValues.put("_size", Long.valueOf(this.mFileSize));
            localContentValues.put("mime_type", this.mMimeType);
            localContentValues.put("is_drm", Boolean.valueOf(this.mIsDrm));
            String str1 = null;
            if ((this.mWidth > 0) && (this.mHeight > 0))
            {
                localContentValues.put("width", Integer.valueOf(this.mWidth));
                localContentValues.put("height", Integer.valueOf(this.mHeight));
                str1 = this.mWidth + "x" + this.mHeight;
            }
            if (!this.mNoMedia)
            {
                if (!MediaFile.isVideoFileType(this.mFileType))
                    break label278;
                if ((this.mArtist == null) || (this.mArtist.length() <= 0))
                    break label264;
                str5 = this.mArtist;
                localContentValues.put("artist", str5);
                if ((this.mAlbum == null) || (this.mAlbum.length() <= 0))
                    break label271;
                str6 = this.mAlbum;
                localContentValues.put("album", str6);
                localContentValues.put("duration", Integer.valueOf(this.mDuration));
                if (str1 != null)
                    localContentValues.put("resolution", str1);
            }
            label264: label271: label278: 
            while ((MediaFile.isImageFileType(this.mFileType)) || (!MediaFile.isAudioFileType(this.mFileType)))
                while (true)
                {
                    return localContentValues;
                    String str5 = "<unknown>";
                    continue;
                    String str6 = "<unknown>";
                }
            String str2;
            label320: String str3;
            if ((this.mArtist != null) && (this.mArtist.length() > 0))
            {
                str2 = this.mArtist;
                localContentValues.put("artist", str2);
                if ((this.mAlbumArtist == null) || (this.mAlbumArtist.length() <= 0))
                    break label485;
                str3 = this.mAlbumArtist;
                label351: localContentValues.put("album_artist", str3);
                if ((this.mAlbum == null) || (this.mAlbum.length() <= 0))
                    break label491;
            }
            label485: label491: for (String str4 = this.mAlbum; ; str4 = "<unknown>")
            {
                localContentValues.put("album", str4);
                localContentValues.put("composer", this.mComposer);
                localContentValues.put("genre", this.mGenre);
                if (this.mYear != 0)
                    localContentValues.put("year", Integer.valueOf(this.mYear));
                localContentValues.put("track", Integer.valueOf(this.mTrack));
                localContentValues.put("duration", Integer.valueOf(this.mDuration));
                localContentValues.put("compilation", Integer.valueOf(this.mCompilation));
                break;
                str2 = "<unknown>";
                break label320;
                str3 = null;
                break label351;
            }
        }

        public MediaScanner.FileEntry beginFile(String paramString1, String paramString2, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2)
        {
            this.mMimeType = paramString2;
            this.mFileType = 0;
            this.mFileSize = paramLong2;
            if (!paramBoolean1)
            {
                if ((!paramBoolean2) && (MediaScanner.isNoMediaFile(paramString1)))
                    paramBoolean2 = true;
                this.mNoMedia = paramBoolean2;
                if (paramString2 != null)
                    this.mFileType = MediaFile.getFileTypeForMimeType(paramString2);
                if (this.mFileType == 0)
                {
                    MediaFile.MediaFileType localMediaFileType = MediaFile.getFileType(paramString1);
                    if (localMediaFileType != null)
                    {
                        this.mFileType = localMediaFileType.fileType;
                        if (this.mMimeType == null)
                            this.mMimeType = localMediaFileType.mimeType;
                    }
                }
                if ((MediaScanner.this.isDrmEnabled()) && (MediaFile.isDrmFileType(this.mFileType)))
                    this.mFileType = getFileTypeFromDrm(paramString1);
            }
            MediaScanner.FileEntry localFileEntry = MediaScanner.this.makeEntryFor(paramString1);
            long l;
            int i;
            if (localFileEntry != null)
            {
                l = paramLong1 - localFileEntry.mLastModified;
                if ((l <= 1L) && (l >= -1L))
                    break label241;
                i = 1;
                label169: if ((localFileEntry == null) || (i != 0))
                {
                    if (i == 0)
                        break label247;
                    localFileEntry.mLastModified = paramLong1;
                    localFileEntry.mLastModifiedChanged = true;
                }
                if ((!MediaScanner.this.mProcessPlaylists) || (!MediaFile.isPlayListFileType(this.mFileType)))
                    break label280;
                MediaScanner.this.mPlayLists.add(localFileEntry);
                localFileEntry = null;
            }
            while (true)
            {
                return localFileEntry;
                l = 0L;
                break;
                label241: i = 0;
                break label169;
                label247: if (paramBoolean1);
                for (int j = 12289; ; j = 0)
                {
                    localFileEntry = new MediaScanner.FileEntry(0L, paramString1, paramLong1, j);
                    break;
                }
                label280: this.mArtist = null;
                this.mAlbumArtist = null;
                this.mAlbum = null;
                this.mTitle = null;
                this.mComposer = null;
                this.mGenre = null;
                this.mTrack = 0;
                this.mYear = 0;
                this.mDuration = 0;
                this.mPath = paramString1;
                this.mLastModified = paramLong1;
                this.mWriter = null;
                this.mCompilation = 0;
                this.mIsDrm = false;
                this.mWidth = 0;
                this.mHeight = 0;
            }
        }

        public Uri doScanFile(String paramString1, String paramString2, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            Object localObject = null;
            while (true)
            {
                try
                {
                    MediaScanner.FileEntry localFileEntry = beginFile(paramString1, paramString2, paramLong1, paramLong2, paramBoolean1, paramBoolean3);
                    if (MediaScanner.this.mMtpObjectHandle != 0)
                        localFileEntry.mRowId = 0L;
                    if ((localFileEntry != null) && ((localFileEntry.mLastModifiedChanged) || (paramBoolean2)))
                        if (paramBoolean3)
                        {
                            localObject = endFile(localFileEntry, false, false, false, false, false);
                        }
                        else
                        {
                            String str = paramString1.toLowerCase();
                            boolean bool1;
                            if (str.indexOf("/ringtones/") > 0)
                            {
                                bool1 = true;
                                if (str.indexOf("/notifications/") > 0)
                                {
                                    bool2 = true;
                                    if (str.indexOf("/alarms/") <= 0)
                                        continue;
                                    bool3 = true;
                                    if (str.indexOf("/podcasts/") <= 0)
                                        continue;
                                    bool4 = true;
                                    if (str.indexOf("/music/") > 0)
                                        break label285;
                                    if ((bool1) || (bool2) || (bool3) || (bool4))
                                        continue;
                                    break label285;
                                    if ((MediaFile.isAudioFileType(this.mFileType)) || (MediaFile.isVideoFileType(this.mFileType)))
                                        MediaScanner.this.processFile(paramString1, paramString2, this);
                                    if (MediaFile.isImageFileType(this.mFileType))
                                        processImageFile(paramString1);
                                    Uri localUri = endFile(localFileEntry, bool1, bool2, bool3, bool5, bool4);
                                    localObject = localUri;
                                    break label282;
                                }
                            }
                            else
                            {
                                bool1 = false;
                                continue;
                            }
                            boolean bool2 = false;
                            continue;
                            boolean bool3 = false;
                            continue;
                            boolean bool4 = false;
                            continue;
                            bool5 = false;
                            continue;
                        }
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("MediaScanner", "RemoteException in MediaScanner.scanFile()", localRemoteException);
                }
                label282: return localObject;
                label285: boolean bool5 = true;
            }
        }

        public String getGenreName(String paramString)
        {
            Object localObject = null;
            if (paramString == null);
            while (true)
            {
                return localObject;
                int i = paramString.length();
                if (i > 0)
                {
                    int j = 0;
                    StringBuffer localStringBuffer = new StringBuffer();
                    int k = 0;
                    if (k < i)
                    {
                        char c2 = paramString.charAt(k);
                        if ((k == 0) && (c2 == '('))
                            j = 1;
                        while (true)
                        {
                            k++;
                            break;
                            if (!Character.isDigit(c2))
                                break label86;
                            localStringBuffer.append(c2);
                        }
                    }
                    label86: char c1;
                    if (k < i)
                    {
                        c1 = paramString.charAt(k);
                        label100: if (((j == 0) || (c1 != ')')) && ((j != 0) || (!Character.isWhitespace(c1))));
                    }
                    else
                    {
                        while (true)
                        {
                            int m;
                            try
                            {
                                m = Short.parseShort(localStringBuffer.toString());
                                if (m < 0)
                                    break label246;
                                if (m >= MediaScanner.ID3_GENRES.length)
                                    break label204;
                                localObject = MediaScanner.ID3_GENRES[m];
                                break;
                                localObject = paramString.substring(k).trim();
                                if (((String)localObject).length() != 0)
                                    break;
                                break label246;
                                String str = localStringBuffer.toString();
                                localObject = str;
                            }
                            catch (NumberFormatException localNumberFormatException)
                            {
                                break label246;
                            }
                            c1 = ' ';
                            break label100;
                            label204: if (m == 255)
                                break;
                            if ((m < 255) && (k + 1 < i))
                                if ((j != 0) && (c1 == ')'))
                                    k++;
                        }
                    }
                }
                label246: localObject = paramString;
            }
        }

        public void handleStringTag(String paramString1, String paramString2)
        {
            int i = 1;
            if ((paramString1.equalsIgnoreCase("title")) || (paramString1.startsWith("title;")))
                this.mTitle = paramString2;
            while (true)
            {
                return;
                if ((paramString1.equalsIgnoreCase("artist")) || (paramString1.startsWith("artist;")))
                {
                    this.mArtist = paramString2.trim();
                }
                else if ((paramString1.equalsIgnoreCase("albumartist")) || (paramString1.startsWith("albumartist;")) || (paramString1.equalsIgnoreCase("band")) || (paramString1.startsWith("band;")))
                {
                    this.mAlbumArtist = paramString2.trim();
                }
                else if ((paramString1.equalsIgnoreCase("album")) || (paramString1.startsWith("album;")))
                {
                    this.mAlbum = paramString2.trim();
                }
                else if ((paramString1.equalsIgnoreCase("composer")) || (paramString1.startsWith("composer;")))
                {
                    this.mComposer = paramString2.trim();
                }
                else if ((MediaScanner.this.mProcessGenres) && ((paramString1.equalsIgnoreCase("genre")) || (paramString1.startsWith("genre;"))))
                {
                    this.mGenre = getGenreName(paramString2);
                }
                else if ((paramString1.equalsIgnoreCase("year")) || (paramString1.startsWith("year;")))
                {
                    this.mYear = parseSubstring(paramString2, 0, 0);
                }
                else if ((paramString1.equalsIgnoreCase("tracknumber")) || (paramString1.startsWith("tracknumber;")))
                {
                    this.mTrack = (parseSubstring(paramString2, 0, 0) + 1000 * (this.mTrack / 1000));
                }
                else if ((paramString1.equalsIgnoreCase("discnumber")) || (paramString1.equals("set")) || (paramString1.startsWith("set;")))
                {
                    this.mTrack = (1000 * parseSubstring(paramString2, 0, 0) + this.mTrack % 1000);
                }
                else if (paramString1.equalsIgnoreCase("duration"))
                {
                    this.mDuration = parseSubstring(paramString2, 0, 0);
                }
                else if ((paramString1.equalsIgnoreCase("writer")) || (paramString1.startsWith("writer;")))
                {
                    this.mWriter = paramString2.trim();
                }
                else if (paramString1.equalsIgnoreCase("compilation"))
                {
                    this.mCompilation = parseSubstring(paramString2, 0, 0);
                }
                else
                {
                    if (paramString1.equalsIgnoreCase("isdrm"))
                    {
                        if (parseSubstring(paramString2, 0, 0) == i);
                        while (true)
                        {
                            this.mIsDrm = i;
                            break;
                            i = 0;
                        }
                    }
                    if (paramString1.equalsIgnoreCase("width"))
                        this.mWidth = parseSubstring(paramString2, 0, 0);
                    else if (paramString1.equalsIgnoreCase("height"))
                        this.mHeight = parseSubstring(paramString2, 0, 0);
                }
            }
        }

        public void scanFile(String paramString, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2)
        {
            doScanFile(paramString, null, paramLong1, paramLong2, paramBoolean1, false, paramBoolean2);
        }

        public void setMimeType(String paramString)
        {
            if (("audio/mp4".equals(this.mMimeType)) && (paramString.startsWith("video")));
            while (true)
            {
                return;
                this.mMimeType = paramString;
                this.mFileType = MediaFile.getFileTypeForMimeType(paramString);
            }
        }
    }

    private static class PlaylistEntry
    {
        long bestmatchid;
        int bestmatchlevel;
        String path;
    }

    private static class FileEntry
    {
        int mFormat;
        long mLastModified;
        boolean mLastModifiedChanged;
        String mPath;
        long mRowId;

        FileEntry(long paramLong1, String paramString, long paramLong2, int paramInt)
        {
            this.mRowId = paramLong1;
            this.mPath = paramString;
            this.mLastModified = paramLong2;
            this.mFormat = paramInt;
            this.mLastModifiedChanged = false;
        }

        public String toString()
        {
            return this.mPath + " mRowId: " + this.mRowId;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void setAllSettingsIfNotSet(MediaScanner paramMediaScanner, MediaScanner.FileEntry paramFileEntry, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            if (paramBoolean2)
            {
                setSettingIfNotSet(paramMediaScanner, "notification_sound", paramFileEntry.mPath);
                setSettingIfNotSet(paramMediaScanner, "sms_delivered_sound", paramFileEntry.mPath);
                setSettingIfNotSet(paramMediaScanner, "sms_received_sound", paramFileEntry.mPath);
            }
            while (true)
            {
                return;
                if (paramBoolean1)
                    setSettingIfNotSet(paramMediaScanner, "ringtone", paramFileEntry.mPath);
                else if (paramBoolean3)
                    setSettingIfNotSet(paramMediaScanner, "alarm_alert", paramFileEntry.mPath);
            }
        }

        static void setSettingIfNotSet(MediaScanner paramMediaScanner, String paramString1, String paramString2)
        {
            Context localContext = paramMediaScanner.getContext();
            if (TextUtils.isEmpty(Settings.System.getString(localContext.getContentResolver(), paramString1)))
                Settings.System.putString(localContext.getContentResolver(), paramString1, Uri.fromFile(new File(paramString2)).toString());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaScanner
 * JD-Core Version:        0.6.2
 */