package android.media;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings.System;
import android.text.TextUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import miui.os.Shell;
import miui.provider.ExtraSettings.System;

public class ExtraRingtoneManager
{
    private static final Uri ACTUAL_DEFAULT_RINGTONE_BASE_URI = Uri.parse("file:///data/system/ringtones");
    private static final String TARGET_USER = "root";
    public static final int TYPE_MUSIC = 32;
    public static final int TYPE_SMS_DELIVERED_SOUND = 8;
    public static final int TYPE_SMS_RECEIVED_SOUND = 16;
    private static ArrayList<RingtoneItem> sRingtoneList = new ArrayList();

    static
    {
        addRingtoneItem(1, "ringtone.mp3", Settings.System.DEFAULT_RINGTONE_URI, "ringtone");
        addRingtoneItem(2, "notification.mp3", Settings.System.DEFAULT_NOTIFICATION_URI, "notification_sound");
        addRingtoneItem(4, "alarm.mp3", Settings.System.DEFAULT_ALARM_ALERT_URI, "alarm_alert");
        addRingtoneItem(8, "sms_delivered_sound.mp3", ExtraSettings.System.DEFAULT_SMS_DELIVERED_RINGTONE_URI, "sms_delivered_sound");
        addRingtoneItem(16, "sms_received_sound.mp3", ExtraSettings.System.DEFAULT_SMS_RECEIVED_RINGTONE_URI, "sms_received_sound");
    }

    private static void addRingtoneItem(int paramInt, String paramString1, Uri paramUri, String paramString2)
    {
        RingtoneItem localRingtoneItem = new RingtoneItem(paramInt, Uri.withAppendedPath(ACTUAL_DEFAULT_RINGTONE_BASE_URI, paramString1), paramUri, paramString2);
        sRingtoneList.add(localRingtoneItem);
    }

    public static void copyRingtone(String paramString, int paramInt)
    {
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return;
            String str = ACTUAL_DEFAULT_RINGTONE_BASE_URI.getPath();
            if (!new File(str).exists())
                Shell.mkdirs(str);
            Shell.copy(paramString, getDefaultRingtoneUri(paramInt).getPath());
        }
    }

    public static Uri getDefaultRingtoneUri(int paramInt)
    {
        Iterator localIterator = sRingtoneList.iterator();
        RingtoneItem localRingtoneItem;
        do
        {
            if (!localIterator.hasNext())
                break;
            localRingtoneItem = (RingtoneItem)localIterator.next();
        }
        while ((paramInt & localRingtoneItem.mRingtoneType) == 0);
        for (Uri localUri = localRingtoneItem.mActualDefaultRingtoneUri; ; localUri = null)
            return localUri;
    }

    public static Uri getRingtoneUri(Context paramContext, int paramInt)
    {
        Uri localUri = null;
        String str1 = getSettingForType(paramInt);
        if (str1 == null);
        while (true)
        {
            return localUri;
            String str2 = Settings.System.getString(paramContext.getContentResolver(), str1);
            if (str2 != null)
                localUri = Uri.parse(str2);
        }
    }

    private static String getSettingForType(int paramInt)
    {
        Iterator localIterator = sRingtoneList.iterator();
        RingtoneItem localRingtoneItem;
        do
        {
            if (!localIterator.hasNext())
                break;
            localRingtoneItem = (RingtoneItem)localIterator.next();
        }
        while ((paramInt & localRingtoneItem.mRingtoneType) == 0);
        for (String str = localRingtoneItem.mSettingType; ; str = null)
            return str;
    }

    public static Uri getUriForExtraCases(Uri paramUri, int paramInt)
    {
        if (isExtraCases(paramUri))
        {
            Uri localUri = getDefaultRingtoneUri(paramInt);
            if (new File(localUri.getPath()).exists())
                paramUri = localUri;
        }
        return paramUri;
    }

    public static boolean isExtraCases(Uri paramUri)
    {
        return "file".equals(paramUri.getScheme());
    }

    public static Uri resolveDefaultRingtoneUri(Context paramContext, int paramInt, Uri paramUri)
    {
        Uri localUri;
        if (paramUri == null)
        {
            localUri = paramUri;
            return localUri;
        }
        String str = "";
        if ("media".equals(paramUri.getAuthority()))
        {
            str = resolveRingtonePath(paramContext, paramUri);
            paramUri = Uri.fromFile(new File(str));
        }
        while (true)
        {
            copyRingtone(str, paramInt);
            localUri = paramUri;
            break;
            if ("file".equals(paramUri.getScheme()))
                str = paramUri.getPath();
        }
    }

    public static String resolveRingtonePath(Context paramContext, Uri paramUri)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "_data";
        Cursor localCursor = localContentResolver.query(paramUri, arrayOfString, null, null, null);
        String str = null;
        if ((localCursor != null) && (localCursor.moveToFirst()))
        {
            str = localCursor.getString(0);
            localCursor.close();
        }
        return str;
    }

    public static void setRingtoneUri(Context paramContext, int paramInt, Uri paramUri)
    {
        String str1 = getSettingForType(paramInt);
        if (str1 == null)
            return;
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramUri != null);
        for (String str2 = paramUri.toString(); ; str2 = null)
        {
            Settings.System.putString(localContentResolver, str1, str2);
            break;
        }
    }

    static class RingtoneItem
    {
        Uri mActualDefaultRingtoneUri;
        Uri mDefaultRingtoneUri;
        int mRingtoneType;
        String mSettingType;

        public RingtoneItem(int paramInt, Uri paramUri1, Uri paramUri2, String paramString)
        {
            this.mRingtoneType = paramInt;
            this.mActualDefaultRingtoneUri = paramUri1;
            this.mDefaultRingtoneUri = paramUri2;
            this.mSettingType = paramString;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.ExtraRingtoneManager
 * JD-Core Version:        0.6.2
 */