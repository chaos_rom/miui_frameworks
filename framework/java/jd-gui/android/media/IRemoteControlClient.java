package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IRemoteControlClient extends IInterface
{
    public abstract void onInformationRequested(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        throws RemoteException;

    public abstract void plugRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
        throws RemoteException;

    public abstract void setCurrentClientGenerationId(int paramInt)
        throws RemoteException;

    public abstract void unplugRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRemoteControlClient
    {
        private static final String DESCRIPTOR = "android.media.IRemoteControlClient";
        static final int TRANSACTION_onInformationRequested = 1;
        static final int TRANSACTION_plugRemoteControlDisplay = 3;
        static final int TRANSACTION_setCurrentClientGenerationId = 2;
        static final int TRANSACTION_unplugRemoteControlDisplay = 4;

        public Stub()
        {
            attachInterface(this, "android.media.IRemoteControlClient");
        }

        public static IRemoteControlClient asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IRemoteControlClient");
                if ((localIInterface != null) && ((localIInterface instanceof IRemoteControlClient)))
                    localObject = (IRemoteControlClient)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.media.IRemoteControlClient");
                continue;
                paramParcel1.enforceInterface("android.media.IRemoteControlClient");
                onInformationRequested(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IRemoteControlClient");
                setCurrentClientGenerationId(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.media.IRemoteControlClient");
                plugRemoteControlDisplay(IRemoteControlDisplay.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("android.media.IRemoteControlClient");
                unplugRemoteControlDisplay(IRemoteControlDisplay.Stub.asInterface(paramParcel1.readStrongBinder()));
            }
        }

        private static class Proxy
            implements IRemoteControlClient
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IRemoteControlClient";
            }

            public void onInformationRequested(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlClient");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void plugRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlClient");
                    if (paramIRemoteControlDisplay != null)
                        localIBinder = paramIRemoteControlDisplay.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setCurrentClientGenerationId(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlClient");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void unplugRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteControlClient");
                    if (paramIRemoteControlDisplay != null)
                        localIBinder = paramIRemoteControlDisplay.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IRemoteControlClient
 * JD-Core Version:        0.6.2
 */