package android.media;

import android.content.res.AssetFileDescriptor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AndroidRuntimeException;
import android.util.Log;
import java.io.FileDescriptor;
import java.lang.ref.WeakReference;

public class JetPlayer
{
    private static final int JET_EVENT = 1;
    private static final int JET_EVENT_CHAN_MASK = 245760;
    private static final int JET_EVENT_CHAN_SHIFT = 14;
    private static final int JET_EVENT_CTRL_MASK = 16256;
    private static final int JET_EVENT_CTRL_SHIFT = 7;
    private static final int JET_EVENT_SEG_MASK = -16777216;
    private static final int JET_EVENT_SEG_SHIFT = 24;
    private static final int JET_EVENT_TRACK_MASK = 16515072;
    private static final int JET_EVENT_TRACK_SHIFT = 18;
    private static final int JET_EVENT_VAL_MASK = 127;
    private static final int JET_NUMQUEUEDSEGMENT_UPDATE = 3;
    private static final int JET_OUTPUT_CHANNEL_CONFIG = 12;
    private static final int JET_OUTPUT_RATE = 22050;
    private static final int JET_PAUSE_UPDATE = 4;
    private static final int JET_USERID_UPDATE = 2;
    private static int MAXTRACKS = 0;
    private static final String TAG = "JetPlayer-J";
    private static JetPlayer singletonRef;
    private NativeEventHandler mEventHandler = null;
    private final Object mEventListenerLock = new Object();
    private Looper mInitializationLooper = null;
    private OnJetEventListener mJetEventListener = null;
    private int mNativePlayerInJavaObj;

    private JetPlayer()
    {
        Looper localLooper = Looper.myLooper();
        this.mInitializationLooper = localLooper;
        if (localLooper == null)
            this.mInitializationLooper = Looper.getMainLooper();
        int i = AudioTrack.getMinBufferSize(22050, 12, 2);
        if ((i != -1) && (i != -2))
            native_setup(new WeakReference(this), getMaxTracks(), Math.max(1200, i / 4));
    }

    public static JetPlayer getJetPlayer()
    {
        if (singletonRef == null)
            singletonRef = new JetPlayer();
        return singletonRef;
    }

    public static int getMaxTracks()
    {
        return MAXTRACKS;
    }

    private static void logd(String paramString)
    {
        Log.d("JetPlayer-J", "[ android.media.JetPlayer ] " + paramString);
    }

    private static void loge(String paramString)
    {
        Log.e("JetPlayer-J", "[ android.media.JetPlayer ] " + paramString);
    }

    private final native boolean native_clearQueue();

    private final native boolean native_closeJetFile();

    private final native void native_finalize();

    private final native boolean native_loadJetFromFile(String paramString);

    private final native boolean native_loadJetFromFileD(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2);

    private final native boolean native_pauseJet();

    private final native boolean native_playJet();

    private final native boolean native_queueJetSegment(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte paramByte);

    private final native boolean native_queueJetSegmentMuteArray(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean[] paramArrayOfBoolean, byte paramByte);

    private final native void native_release();

    private final native boolean native_setMuteArray(boolean[] paramArrayOfBoolean, boolean paramBoolean);

    private final native boolean native_setMuteFlag(int paramInt, boolean paramBoolean1, boolean paramBoolean2);

    private final native boolean native_setMuteFlags(int paramInt, boolean paramBoolean);

    private final native boolean native_setup(Object paramObject, int paramInt1, int paramInt2);

    private final native boolean native_triggerClip(int paramInt);

    private static void postEventFromNative(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        JetPlayer localJetPlayer = (JetPlayer)((WeakReference)paramObject).get();
        if ((localJetPlayer != null) && (localJetPlayer.mEventHandler != null))
        {
            Message localMessage = localJetPlayer.mEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, null);
            localJetPlayer.mEventHandler.sendMessage(localMessage);
        }
    }

    public boolean clearQueue()
    {
        return native_clearQueue();
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    public boolean closeJetFile()
    {
        return native_closeJetFile();
    }

    protected void finalize()
    {
        native_finalize();
    }

    public boolean loadJetFile(AssetFileDescriptor paramAssetFileDescriptor)
    {
        long l = paramAssetFileDescriptor.getLength();
        if (l < 0L)
            throw new AndroidRuntimeException("no length for fd");
        return native_loadJetFromFileD(paramAssetFileDescriptor.getFileDescriptor(), paramAssetFileDescriptor.getStartOffset(), l);
    }

    public boolean loadJetFile(String paramString)
    {
        return native_loadJetFromFile(paramString);
    }

    public boolean pause()
    {
        return native_pauseJet();
    }

    public boolean play()
    {
        return native_playJet();
    }

    public boolean queueJetSegment(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte paramByte)
    {
        return native_queueJetSegment(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramByte);
    }

    public boolean queueJetSegmentMuteArray(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean[] paramArrayOfBoolean, byte paramByte)
    {
        if (paramArrayOfBoolean.length != getMaxTracks());
        for (boolean bool = false; ; bool = native_queueJetSegmentMuteArray(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfBoolean, paramByte))
            return bool;
    }

    public void release()
    {
        native_release();
        singletonRef = null;
    }

    public void setEventListener(OnJetEventListener paramOnJetEventListener)
    {
        setEventListener(paramOnJetEventListener, null);
    }

    // ERROR //
    public void setEventListener(OnJetEventListener paramOnJetEventListener, Handler paramHandler)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 71	android/media/JetPlayer:mEventListenerLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: aload_1
        //     9: putfield 73	android/media/JetPlayer:mJetEventListener	Landroid/media/JetPlayer$OnJetEventListener;
        //     12: aload_1
        //     13: ifnull +54 -> 67
        //     16: aload_2
        //     17: ifnull +23 -> 40
        //     20: aload_0
        //     21: new 9	android/media/JetPlayer$NativeEventHandler
        //     24: dup
        //     25: aload_0
        //     26: aload_0
        //     27: aload_2
        //     28: invokevirtual 244	android/os/Handler:getLooper	()Landroid/os/Looper;
        //     31: invokespecial 247	android/media/JetPlayer$NativeEventHandler:<init>	(Landroid/media/JetPlayer;Landroid/media/JetPlayer;Landroid/os/Looper;)V
        //     34: putfield 67	android/media/JetPlayer:mEventHandler	Landroid/media/JetPlayer$NativeEventHandler;
        //     37: aload_3
        //     38: monitorexit
        //     39: return
        //     40: aload_0
        //     41: new 9	android/media/JetPlayer$NativeEventHandler
        //     44: dup
        //     45: aload_0
        //     46: aload_0
        //     47: aload_0
        //     48: getfield 69	android/media/JetPlayer:mInitializationLooper	Landroid/os/Looper;
        //     51: invokespecial 247	android/media/JetPlayer$NativeEventHandler:<init>	(Landroid/media/JetPlayer;Landroid/media/JetPlayer;Landroid/os/Looper;)V
        //     54: putfield 67	android/media/JetPlayer:mEventHandler	Landroid/media/JetPlayer$NativeEventHandler;
        //     57: goto -20 -> 37
        //     60: astore 4
        //     62: aload_3
        //     63: monitorexit
        //     64: aload 4
        //     66: athrow
        //     67: aload_0
        //     68: aconst_null
        //     69: putfield 67	android/media/JetPlayer:mEventHandler	Landroid/media/JetPlayer$NativeEventHandler;
        //     72: goto -35 -> 37
        //
        // Exception table:
        //     from	to	target	type
        //     7	64	60	finally
        //     67	72	60	finally
    }

    public boolean setMuteArray(boolean[] paramArrayOfBoolean, boolean paramBoolean)
    {
        if (paramArrayOfBoolean.length != getMaxTracks());
        for (boolean bool = false; ; bool = native_setMuteArray(paramArrayOfBoolean, paramBoolean))
            return bool;
    }

    public boolean setMuteFlag(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        return native_setMuteFlag(paramInt, paramBoolean1, paramBoolean2);
    }

    public boolean setMuteFlags(int paramInt, boolean paramBoolean)
    {
        return native_setMuteFlags(paramInt, paramBoolean);
    }

    public boolean triggerClip(int paramInt)
    {
        return native_triggerClip(paramInt);
    }

    public static abstract interface OnJetEventListener
    {
        public abstract void onJetEvent(JetPlayer paramJetPlayer, short paramShort, byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4);

        public abstract void onJetNumQueuedSegmentUpdate(JetPlayer paramJetPlayer, int paramInt);

        public abstract void onJetPauseUpdate(JetPlayer paramJetPlayer, int paramInt);

        public abstract void onJetUserIdUpdate(JetPlayer paramJetPlayer, int paramInt1, int paramInt2);
    }

    private class NativeEventHandler extends Handler
    {
        private JetPlayer mJet;

        public NativeEventHandler(JetPlayer paramLooper, Looper arg3)
        {
            super();
            this.mJet = paramLooper;
        }

        public void handleMessage(Message paramMessage)
        {
            while (true)
            {
                JetPlayer.OnJetEventListener localOnJetEventListener;
                synchronized (JetPlayer.this.mEventListenerLock)
                {
                    localOnJetEventListener = this.mJet.mJetEventListener;
                    switch (paramMessage.what)
                    {
                    default:
                        JetPlayer.loge("Unknown message type " + paramMessage.what);
                        return;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    }
                }
                if (localOnJetEventListener != null)
                {
                    JetPlayer.this.mJetEventListener.onJetEvent(this.mJet, (short)((0xFF000000 & paramMessage.arg1) >> 24), (byte)((0xFC0000 & paramMessage.arg1) >> 18), (byte)(1 + ((0x3C000 & paramMessage.arg1) >> 14)), (byte)((0x3F80 & paramMessage.arg1) >> 7), (byte)(0x7F & paramMessage.arg1));
                    continue;
                    if (localOnJetEventListener != null)
                    {
                        localOnJetEventListener.onJetUserIdUpdate(this.mJet, paramMessage.arg1, paramMessage.arg2);
                        continue;
                        if (localOnJetEventListener != null)
                        {
                            localOnJetEventListener.onJetNumQueuedSegmentUpdate(this.mJet, paramMessage.arg1);
                            continue;
                            if (localOnJetEventListener != null)
                                localOnJetEventListener.onJetPauseUpdate(this.mJet, paramMessage.arg1);
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.JetPlayer
 * JD-Core Version:        0.6.2
 */