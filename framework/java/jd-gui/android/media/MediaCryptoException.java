package android.media;

public final class MediaCryptoException extends Exception
{
    public MediaCryptoException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaCryptoException
 * JD-Core Version:        0.6.2
 */