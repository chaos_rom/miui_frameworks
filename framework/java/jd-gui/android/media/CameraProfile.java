package android.media;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import java.util.Arrays;
import java.util.HashMap;

public class CameraProfile
{
    public static final int QUALITY_HIGH = 2;
    public static final int QUALITY_LOW = 0;
    public static final int QUALITY_MEDIUM = 1;
    private static final HashMap<Integer, int[]> sCache = new HashMap();

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    private static int[] getImageEncodingQualityLevels(int paramInt)
    {
        int i = native_get_num_image_encoding_quality_levels(paramInt);
        if (i != 3)
            throw new RuntimeException("Unexpected Jpeg encoding quality levels " + i);
        int[] arrayOfInt = new int[i];
        for (int j = 0; j < i; j++)
            arrayOfInt[j] = native_get_image_encoding_quality_level(paramInt, j);
        Arrays.sort(arrayOfInt);
        return arrayOfInt;
    }

    public static int getJpegEncodingQualityParameter(int paramInt)
    {
        int i = Camera.getNumberOfCameras();
        Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
        int j = 0;
        if (j < i)
        {
            Camera.getCameraInfo(j, localCameraInfo);
            if (localCameraInfo.facing != 0);
        }
        for (int k = getJpegEncodingQualityParameter(j, paramInt); ; k = 0)
        {
            return k;
            j++;
            break;
        }
    }

    public static int getJpegEncodingQualityParameter(int paramInt1, int paramInt2)
    {
        if ((paramInt2 < 0) || (paramInt2 > 2))
            throw new IllegalArgumentException("Unsupported quality level: " + paramInt2);
        synchronized (sCache)
        {
            int[] arrayOfInt = (int[])sCache.get(Integer.valueOf(paramInt1));
            if (arrayOfInt == null)
            {
                arrayOfInt = getImageEncodingQualityLevels(paramInt1);
                sCache.put(Integer.valueOf(paramInt1), arrayOfInt);
            }
            int i = arrayOfInt[paramInt2];
            return i;
        }
    }

    private static final native int native_get_image_encoding_quality_level(int paramInt1, int paramInt2);

    private static final native int native_get_num_image_encoding_quality_levels(int paramInt);

    private static final native void native_init();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.CameraProfile
 * JD-Core Version:        0.6.2
 */