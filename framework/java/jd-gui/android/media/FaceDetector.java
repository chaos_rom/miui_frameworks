package android.media;

import android.graphics.Bitmap;
import android.graphics.PointF;
import android.util.Log;

public class FaceDetector
{
    private static boolean sInitialized = false;
    private byte[] mBWBuffer;
    private int mDCR;
    private int mFD;
    private int mHeight;
    private int mMaxFaces;
    private int mSDK;
    private int mWidth;

    static
    {
        try
        {
            System.loadLibrary("FFTEm");
            nativeClassInit();
            sInitialized = true;
            return;
        }
        catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
        {
            while (true)
                Log.d("FFTEm", "face detection library not found!");
        }
    }

    public FaceDetector(int paramInt1, int paramInt2, int paramInt3)
    {
        if (!sInitialized);
        while (true)
        {
            return;
            fft_initialize(paramInt1, paramInt2, paramInt3);
            this.mWidth = paramInt1;
            this.mHeight = paramInt2;
            this.mMaxFaces = paramInt3;
            this.mBWBuffer = new byte[paramInt1 * paramInt2];
        }
    }

    private native void fft_destroy();

    private native int fft_detect(Bitmap paramBitmap);

    private native void fft_get_face(Face paramFace, int paramInt);

    private native int fft_initialize(int paramInt1, int paramInt2, int paramInt3);

    private static native void nativeClassInit();

    protected void finalize()
        throws Throwable
    {
        fft_destroy();
    }

    public int findFaces(Bitmap paramBitmap, Face[] paramArrayOfFace)
    {
        int i;
        if (!sInitialized)
            i = 0;
        while (true)
        {
            return i;
            if ((paramBitmap.getWidth() != this.mWidth) || (paramBitmap.getHeight() != this.mHeight))
                throw new IllegalArgumentException("bitmap size doesn't match initialization");
            if (paramArrayOfFace.length < this.mMaxFaces)
                throw new IllegalArgumentException("faces[] smaller than maxFaces");
            i = fft_detect(paramBitmap);
            if (i >= this.mMaxFaces)
                i = this.mMaxFaces;
            for (int j = 0; j < i; j++)
            {
                if (paramArrayOfFace[j] == null)
                    paramArrayOfFace[j] = new Face(null);
                fft_get_face(paramArrayOfFace[j], j);
            }
        }
    }

    public class Face
    {
        public static final float CONFIDENCE_THRESHOLD = 0.4F;
        public static final int EULER_X = 0;
        public static final int EULER_Y = 1;
        public static final int EULER_Z = 2;
        private float mConfidence;
        private float mEyesDist;
        private float mMidPointX;
        private float mMidPointY;
        private float mPoseEulerX;
        private float mPoseEulerY;
        private float mPoseEulerZ;

        private Face()
        {
        }

        public float confidence()
        {
            return this.mConfidence;
        }

        public float eyesDistance()
        {
            return this.mEyesDist;
        }

        public void getMidPoint(PointF paramPointF)
        {
            paramPointF.set(this.mMidPointX, this.mMidPointY);
        }

        public float pose(int paramInt)
        {
            float f;
            if (paramInt == 0)
                f = this.mPoseEulerX;
            while (true)
            {
                return f;
                if (paramInt == 1)
                {
                    f = this.mPoseEulerY;
                }
                else
                {
                    if (paramInt != 2)
                        break;
                    f = this.mPoseEulerZ;
                }
            }
            throw new IllegalArgumentException();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.FaceDetector
 * JD-Core Version:        0.6.2
 */