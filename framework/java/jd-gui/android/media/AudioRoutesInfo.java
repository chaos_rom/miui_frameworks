package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public class AudioRoutesInfo
    implements Parcelable
{
    public static final Parcelable.Creator<AudioRoutesInfo> CREATOR = new Parcelable.Creator()
    {
        public AudioRoutesInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new AudioRoutesInfo(paramAnonymousParcel);
        }

        public AudioRoutesInfo[] newArray(int paramAnonymousInt)
        {
            return new AudioRoutesInfo[paramAnonymousInt];
        }
    };
    static final int MAIN_DOCK_SPEAKERS = 4;
    static final int MAIN_HDMI = 8;
    static final int MAIN_HEADPHONES = 2;
    static final int MAIN_HEADSET = 1;
    static final int MAIN_SPEAKER;
    CharSequence mBluetoothName;
    int mMainType = 0;

    public AudioRoutesInfo()
    {
    }

    public AudioRoutesInfo(AudioRoutesInfo paramAudioRoutesInfo)
    {
        this.mBluetoothName = paramAudioRoutesInfo.mBluetoothName;
        this.mMainType = paramAudioRoutesInfo.mMainType;
    }

    AudioRoutesInfo(Parcel paramParcel)
    {
        this.mBluetoothName = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        this.mMainType = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        TextUtils.writeToParcel(this.mBluetoothName, paramParcel, paramInt);
        paramParcel.writeInt(this.mMainType);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AudioRoutesInfo
 * JD-Core Version:        0.6.2
 */