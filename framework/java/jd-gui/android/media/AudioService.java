package android.media;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.PendingIntent.OnFinished;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.Vibrator;
import android.provider.Settings.System;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import miui.view.VolumePanel;

public class AudioService extends IAudioService.Stub
    implements PendingIntent.OnFinished
{
    private static final int BTA2DP_DOCK_TIMEOUT_MILLIS = 8000;
    private static final int BT_HEADSET_CNCT_TIMEOUT_MS = 3000;
    protected static final boolean DEBUG_RC = false;
    protected static final boolean DEBUG_VOL = false;
    private static final String EXTRA_WAKELOCK_ACQUIRED = "android.media.AudioService.WAKELOCK_ACQUIRED";
    public static final String IN_VOICE_COMM_FOCUS_ID = "AudioFocus_For_Phone_Ring_And_Calls";
    private static final int MAX_BATCH_VOLUME_ADJUST_STEPS = 4;
    private static final int MAX_MASTER_VOLUME = 100;
    private static final int MSG_BTA2DP_DOCK_TIMEOUT = 7;
    private static final int MSG_BT_HEADSET_CNCT_FAILED = 11;
    private static final int MSG_LOAD_SOUND_EFFECTS = 8;
    private static final int MSG_MEDIA_SERVER_DIED = 4;
    private static final int MSG_MEDIA_SERVER_STARTED = 5;
    private static final int MSG_PERSIST_MASTER_VOLUME = 2;
    private static final int MSG_PERSIST_MASTER_VOLUME_MUTE = 15;
    private static final int MSG_PERSIST_MEDIABUTTONRECEIVER = 10;
    private static final int MSG_PERSIST_RINGER_MODE = 3;
    private static final int MSG_PERSIST_VOLUME = 1;
    private static final int MSG_PLAY_SOUND_EFFECT = 6;
    private static final int MSG_RCC_NEW_PLAYBACK_INFO = 18;
    private static final int MSG_RCC_NEW_VOLUME_OBS = 19;
    private static final int MSG_RCDISPLAY_CLEAR = 12;
    private static final int MSG_RCDISPLAY_UPDATE = 13;
    private static final int MSG_REEVALUATE_REMOTE = 17;
    private static final int MSG_REPORT_NEW_ROUTES = 16;
    private static final int MSG_SET_A2DP_CONNECTION_STATE = 21;
    private static final int MSG_SET_ALL_VOLUMES = 14;
    private static final int MSG_SET_DEVICE_VOLUME = 0;
    private static final int MSG_SET_FORCE_USE = 9;
    private static final int MSG_SET_WIRED_DEVICE_CONNECTION_STATE = 20;
    private static final int NOTIFICATION_VOLUME_DELAY_MS = 5000;
    private static final int NUM_SOUNDPOOL_CHANNELS = 4;
    private static final int PERSIST_CURRENT = 1;
    private static final int PERSIST_DELAY = 500;
    private static final int PERSIST_LAST_AUDIBLE = 2;
    private static final int RC_INFO_ALL = 15;
    private static final int RC_INFO_NONE = 0;
    private static final int SCO_STATE_ACTIVATE_REQ = 1;
    private static final int SCO_STATE_ACTIVE_EXTERNAL = 2;
    private static final int SCO_STATE_ACTIVE_INTERNAL = 3;
    private static final int SCO_STATE_DEACTIVATE_EXT_REQ = 4;
    private static final int SCO_STATE_DEACTIVATE_REQ = 5;
    private static final int SCO_STATE_INACTIVE = 0;
    private static final int SENDMSG_NOOP = 1;
    private static final int SENDMSG_QUEUE = 2;
    private static final int SENDMSG_REPLACE = 0;
    private static final String SOUND_EFFECTS_PATH = "/media/audio/ui/";
    private static final String[] SOUND_EFFECT_FILES = arrayOfString;
    private static int SOUND_EFFECT_VOLUME_DB = 0;
    public static final int STREAM_REMOTE_MUSIC = -200;
    private static final String TAG = "AudioService";
    private static final int VOICEBUTTON_ACTION_DISCARD_CURRENT_KEY_PRESS = 1;
    private static final int VOICEBUTTON_ACTION_SIMULATE_KEY_PRESS = 3;
    private static final int VOICEBUTTON_ACTION_START_VOICE_INPUT = 2;
    private static final int WAKELOCK_RELEASE_ON_FINISHED = 1980;
    private static final Object mAudioFocusLock = new Object();
    private static final Object mRingingLock = new Object();
    private static int sLastRccId = 0;
    private final int[] MAX_STREAM_VOLUME;
    private final int[][] SOUND_EFFECT_FILES_MAP;
    private final String[] STREAM_NAMES;
    private final int[] STREAM_VOLUME_ALIAS;
    private final int[] STREAM_VOLUME_ALIAS_NON_VOICE;
    private int mArtworkExpectedHeight;
    private int mArtworkExpectedWidth;
    private AudioHandler mAudioHandler;
    private final AudioSystem.ErrorCallback mAudioSystemCallback;
    private AudioSystemThread mAudioSystemThread;
    int mBecomingNoisyIntentDevices;
    private boolean mBluetoothA2dpEnabled;
    private final Object mBluetoothA2dpEnabledLock;
    private BluetoothHeadset mBluetoothHeadset;
    private BluetoothDevice mBluetoothHeadsetDevice;
    private BluetoothProfile.ServiceListener mBluetoothProfileServiceListener;
    private boolean mBootCompleted;
    private final HashMap<Integer, String> mConnectedDevices;
    private ContentResolver mContentResolver;
    private Context mContext;
    final AudioRoutesInfo mCurAudioRoutes;
    private IRemoteControlClient mCurrentRcClient;
    private int mCurrentRcClientGen;
    private final Object mCurrentRcLock;
    private int mDeviceOrientation;
    private String mDockAddress;
    private final Stack<FocusStackEntry> mFocusStack;
    private ForceControlStreamClient mForceControlStreamClient;
    private final Object mForceControlStreamLock;
    private int mForcedUseForComm;
    private boolean mHasRemotePlayback;
    private final boolean mHasVibrator;
    private boolean mIsRinging;
    BroadcastReceiver mKeyEventDone;
    private KeyguardManager mKeyguardManager;
    private RemotePlaybackState mMainRemote;
    private boolean mMainRemoteIsActive;
    private final int[] mMasterVolumeRamp;
    private PowerManager.WakeLock mMediaEventWakeLock;
    private ComponentName mMediaReceiverForCalls;
    private boolean mMediaServerOk;
    private int mMode;
    private int mMuteAffectedStreams;
    private PhoneStateListener mPhoneStateListener;
    private int mPrevVolDirection;
    private final Stack<RemoteControlStackEntry> mRCStack;
    private IRemoteControlDisplay mRcDisplay;
    private RcDisplayDeathHandler mRcDisplayDeathHandler;
    private final BroadcastReceiver mReceiver;
    private int mRingerMode;
    private int mRingerModeAffectedStreams;
    private int mRingerModeMutedStreams;
    private volatile IRingtonePlayer mRingtonePlayer;
    final RemoteCallbackList<IAudioRoutesObserver> mRoutesObservers;
    private int mScoAudioState;
    private final ArrayList<ScoClient> mScoClients;
    private int mScoConnectionState;
    private final ArrayList<SetModeDeathHandler> mSetModeDeathHandlers;
    private final Object mSettingsLock = new Object();
    private SettingsObserver mSettingsObserver;
    private final Object mSoundEffectsLock = new Object();
    private SoundPool mSoundPool;
    private SoundPoolCallback mSoundPoolCallBack;
    private SoundPoolListenerThread mSoundPoolListenerThread;
    private Looper mSoundPoolLooper;
    private VolumeStreamState[] mStreamStates;
    private int[] mStreamVolumeAlias;
    private final boolean mUseMasterVolume;
    private int mVibrateSetting;
    private boolean mVoiceButtonDown;
    private boolean mVoiceButtonHandled;
    private boolean mVoiceCapable;
    private final Object mVoiceEventLock;
    private int mVolumeControlStream;
    private VolumePanel mVolumePanel;

    static
    {
        String[] arrayOfString = new String[5];
        arrayOfString[0] = "Effect_Tick.ogg";
        arrayOfString[1] = "KeypressStandard.ogg";
        arrayOfString[2] = "KeypressSpacebar.ogg";
        arrayOfString[3] = "KeypressDelete.ogg";
        arrayOfString[4] = "KeypressReturn.ogg";
    }

    public AudioService(Context paramContext)
    {
        int[][] arrayOfInt = new int[9][];
        int[] arrayOfInt1 = new int[2];
        arrayOfInt1[0] = 0;
        arrayOfInt1[1] = -1;
        arrayOfInt[0] = arrayOfInt1;
        int[] arrayOfInt2 = new int[2];
        arrayOfInt2[0] = 0;
        arrayOfInt2[1] = -1;
        arrayOfInt[1] = arrayOfInt2;
        int[] arrayOfInt3 = new int[2];
        arrayOfInt3[0] = 0;
        arrayOfInt3[1] = -1;
        arrayOfInt[2] = arrayOfInt3;
        int[] arrayOfInt4 = new int[2];
        arrayOfInt4[0] = 0;
        arrayOfInt4[1] = -1;
        arrayOfInt[3] = arrayOfInt4;
        int[] arrayOfInt5 = new int[2];
        arrayOfInt5[0] = 0;
        arrayOfInt5[1] = -1;
        arrayOfInt[4] = arrayOfInt5;
        int[] arrayOfInt6 = new int[2];
        arrayOfInt6[0] = 1;
        arrayOfInt6[1] = -1;
        arrayOfInt[5] = arrayOfInt6;
        int[] arrayOfInt7 = new int[2];
        arrayOfInt7[0] = 2;
        arrayOfInt7[1] = -1;
        arrayOfInt[6] = arrayOfInt7;
        int[] arrayOfInt8 = new int[2];
        arrayOfInt8[0] = 3;
        arrayOfInt8[1] = -1;
        arrayOfInt[7] = arrayOfInt8;
        int[] arrayOfInt9 = new int[2];
        arrayOfInt9[0] = 4;
        arrayOfInt9[1] = -1;
        arrayOfInt[8] = arrayOfInt9;
        this.SOUND_EFFECT_FILES_MAP = arrayOfInt;
        int[] arrayOfInt10 = new int[10];
        arrayOfInt10[0] = 5;
        arrayOfInt10[1] = 7;
        arrayOfInt10[2] = 7;
        arrayOfInt10[3] = 15;
        arrayOfInt10[4] = 7;
        arrayOfInt10[5] = 7;
        arrayOfInt10[6] = 15;
        arrayOfInt10[7] = 7;
        arrayOfInt10[8] = 15;
        arrayOfInt10[9] = 15;
        this.MAX_STREAM_VOLUME = arrayOfInt10;
        int[] arrayOfInt11 = new int[10];
        arrayOfInt11[0] = 0;
        arrayOfInt11[1] = 2;
        arrayOfInt11[2] = 2;
        arrayOfInt11[3] = 3;
        arrayOfInt11[4] = 4;
        arrayOfInt11[5] = 2;
        arrayOfInt11[6] = 6;
        arrayOfInt11[7] = 2;
        arrayOfInt11[8] = 2;
        arrayOfInt11[9] = 3;
        this.STREAM_VOLUME_ALIAS = arrayOfInt11;
        int[] arrayOfInt12 = new int[10];
        arrayOfInt12[0] = 0;
        arrayOfInt12[1] = 3;
        arrayOfInt12[2] = 2;
        arrayOfInt12[3] = 3;
        arrayOfInt12[4] = 4;
        arrayOfInt12[5] = 2;
        arrayOfInt12[6] = 6;
        arrayOfInt12[7] = 3;
        arrayOfInt12[8] = 3;
        arrayOfInt12[9] = 3;
        this.STREAM_VOLUME_ALIAS_NON_VOICE = arrayOfInt12;
        String[] arrayOfString = new String[10];
        arrayOfString[0] = "STREAM_VOICE_CALL";
        arrayOfString[1] = "STREAM_SYSTEM";
        arrayOfString[2] = "STREAM_RING";
        arrayOfString[3] = "STREAM_MUSIC";
        arrayOfString[4] = "STREAM_ALARM";
        arrayOfString[5] = "STREAM_NOTIFICATION";
        arrayOfString[6] = "STREAM_BLUETOOTH_SCO";
        arrayOfString[7] = "STREAM_SYSTEM_ENFORCED";
        arrayOfString[8] = "STREAM_DTMF";
        arrayOfString[9] = "STREAM_TTS";
        this.STREAM_NAMES = arrayOfString;
        this.mAudioSystemCallback = new AudioSystem.ErrorCallback()
        {
            public void onError(int paramAnonymousInt)
            {
                switch (paramAnonymousInt)
                {
                default:
                case 100:
                case 0:
                }
                while (true)
                {
                    return;
                    if (AudioService.this.mMediaServerOk)
                    {
                        AudioService.sendMsg(AudioService.this.mAudioHandler, 4, 1, 0, 0, null, 1500);
                        AudioService.access$002(AudioService.this, false);
                        continue;
                        if (!AudioService.this.mMediaServerOk)
                        {
                            AudioService.sendMsg(AudioService.this.mAudioHandler, 5, 1, 0, 0, null, 0);
                            AudioService.access$002(AudioService.this, true);
                        }
                    }
                }
            }
        };
        this.mReceiver = new AudioServiceBroadcastReceiver(null);
        this.mIsRinging = false;
        this.mConnectedDevices = new HashMap();
        this.mSetModeDeathHandlers = new ArrayList();
        this.mScoClients = new ArrayList();
        this.mSoundPoolLooper = null;
        this.mPrevVolDirection = 0;
        this.mVolumeControlStream = -1;
        this.mForceControlStreamLock = new Object();
        this.mForceControlStreamClient = null;
        this.mDeviceOrientation = 0;
        this.mBluetoothA2dpEnabledLock = new Object();
        this.mCurAudioRoutes = new AudioRoutesInfo();
        this.mRoutesObservers = new RemoteCallbackList();
        this.mBluetoothProfileServiceListener = new BluetoothProfile.ServiceListener()
        {
            // ERROR //
            public void onServiceConnected(int paramAnonymousInt, android.bluetooth.BluetoothProfile paramAnonymousBluetoothProfile)
            {
                // Byte code:
                //     0: iload_1
                //     1: tableswitch	default:+23 -> 24, 1:+151->152, 2:+24->25
                //     25: aload_2
                //     26: checkcast 21	android/bluetooth/BluetoothA2dp
                //     29: astore 12
                //     31: aload 12
                //     33: invokevirtual 25	android/bluetooth/BluetoothA2dp:getConnectedDevices	()Ljava/util/List;
                //     36: astore 13
                //     38: aload 13
                //     40: invokeinterface 31 1 0
                //     45: ifle -21 -> 24
                //     48: aload 13
                //     50: iconst_0
                //     51: invokeinterface 35 2 0
                //     56: checkcast 37	android/bluetooth/BluetoothDevice
                //     59: astore 14
                //     61: aload_0
                //     62: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     65: invokestatic 41	android/media/AudioService:access$2800	(Landroid/media/AudioService;)Ljava/util/HashMap;
                //     68: astore 15
                //     70: aload 15
                //     72: monitorenter
                //     73: aload 12
                //     75: aload 14
                //     77: invokevirtual 45	android/bluetooth/BluetoothA2dp:getConnectionState	(Landroid/bluetooth/BluetoothDevice;)I
                //     80: istore 17
                //     82: aload_0
                //     83: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     86: astore 18
                //     88: iload 17
                //     90: iconst_2
                //     91: if_icmpne +55 -> 146
                //     94: iconst_1
                //     95: istore 19
                //     97: aload 18
                //     99: sipush 128
                //     102: iload 19
                //     104: invokestatic 49	android/media/AudioService:access$2900	(Landroid/media/AudioService;II)I
                //     107: istore 20
                //     109: aload_0
                //     110: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     113: aload_0
                //     114: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     117: invokestatic 53	android/media/AudioService:access$100	(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;
                //     120: bipush 21
                //     122: iload 17
                //     124: iconst_0
                //     125: aload 14
                //     127: iload 20
                //     129: invokestatic 57	android/media/AudioService:access$3000	(Landroid/media/AudioService;Landroid/os/Handler;IIILjava/lang/Object;I)V
                //     132: aload 15
                //     134: monitorexit
                //     135: goto -111 -> 24
                //     138: astore 16
                //     140: aload 15
                //     142: monitorexit
                //     143: aload 16
                //     145: athrow
                //     146: iconst_0
                //     147: istore 19
                //     149: goto -52 -> 97
                //     152: aload_0
                //     153: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     156: invokestatic 61	android/media/AudioService:access$2100	(Landroid/media/AudioService;)Ljava/util/ArrayList;
                //     159: astore_3
                //     160: aload_3
                //     161: monitorenter
                //     162: aload_0
                //     163: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     166: invokestatic 53	android/media/AudioService:access$100	(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;
                //     169: bipush 11
                //     171: invokevirtual 67	android/media/AudioService$AudioHandler:removeMessages	(I)V
                //     174: aload_0
                //     175: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     178: aload_2
                //     179: checkcast 69	android/bluetooth/BluetoothHeadset
                //     182: invokestatic 73	android/media/AudioService:access$2502	(Landroid/media/AudioService;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
                //     185: pop
                //     186: aload_0
                //     187: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     190: invokestatic 77	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
                //     193: invokevirtual 78	android/bluetooth/BluetoothHeadset:getConnectedDevices	()Ljava/util/List;
                //     196: astore 6
                //     198: aload 6
                //     200: invokeinterface 31 1 0
                //     205: ifle +149 -> 354
                //     208: aload_0
                //     209: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     212: aload 6
                //     214: iconst_0
                //     215: invokeinterface 35 2 0
                //     220: checkcast 37	android/bluetooth/BluetoothDevice
                //     223: invokestatic 82	android/media/AudioService:access$2602	(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
                //     226: pop
                //     227: aload_0
                //     228: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     231: invokestatic 85	android/media/AudioService:access$2200	(Landroid/media/AudioService;)V
                //     234: aload_0
                //     235: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     238: invokestatic 89	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
                //     241: iconst_1
                //     242: if_icmpeq +25 -> 267
                //     245: aload_0
                //     246: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     249: invokestatic 89	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
                //     252: iconst_5
                //     253: if_icmpeq +14 -> 267
                //     256: aload_0
                //     257: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     260: invokestatic 89	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
                //     263: iconst_4
                //     264: if_icmpne +78 -> 342
                //     267: iconst_0
                //     268: istore 8
                //     270: aload_0
                //     271: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     274: invokestatic 93	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
                //     277: ifnull +43 -> 320
                //     280: aload_0
                //     281: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     284: invokestatic 89	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
                //     287: tableswitch	default:+33 -> 320, 1:+79->366, 2:+33->320, 3:+33->320, 4:+132->419, 5:+110->397
                //     321: iconst_5
                //     322: ifne +20 -> 342
                //     325: aload_0
                //     326: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     329: invokestatic 53	android/media/AudioService:access$100	(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;
                //     332: bipush 11
                //     334: iconst_0
                //     335: iconst_0
                //     336: iconst_0
                //     337: aconst_null
                //     338: iconst_0
                //     339: invokestatic 97	android/media/AudioService:access$200	(Landroid/os/Handler;IIIILjava/lang/Object;I)V
                //     342: aload_3
                //     343: monitorexit
                //     344: goto -320 -> 24
                //     347: astore 4
                //     349: aload_3
                //     350: monitorexit
                //     351: aload 4
                //     353: athrow
                //     354: aload_0
                //     355: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     358: aconst_null
                //     359: invokestatic 82	android/media/AudioService:access$2602	(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
                //     362: pop
                //     363: goto -136 -> 227
                //     366: aload_0
                //     367: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     370: iconst_3
                //     371: invokestatic 101	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
                //     374: pop
                //     375: aload_0
                //     376: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     379: invokestatic 77	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
                //     382: aload_0
                //     383: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     386: invokestatic 93	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
                //     389: invokevirtual 105	android/bluetooth/BluetoothHeadset:startScoUsingVirtualVoiceCall	(Landroid/bluetooth/BluetoothDevice;)Z
                //     392: istore 8
                //     394: goto -74 -> 320
                //     397: aload_0
                //     398: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     401: invokestatic 77	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
                //     404: aload_0
                //     405: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     408: invokestatic 93	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
                //     411: invokevirtual 108	android/bluetooth/BluetoothHeadset:stopScoUsingVirtualVoiceCall	(Landroid/bluetooth/BluetoothDevice;)Z
                //     414: istore 8
                //     416: goto -96 -> 320
                //     419: aload_0
                //     420: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     423: invokestatic 77	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
                //     426: aload_0
                //     427: getfield 14	android/media/AudioService$2:this$0	Landroid/media/AudioService;
                //     430: invokestatic 93	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
                //     433: invokevirtual 111	android/bluetooth/BluetoothHeadset:stopVoiceRecognition	(Landroid/bluetooth/BluetoothDevice;)Z
                //     436: istore 9
                //     438: iload 9
                //     440: istore 8
                //     442: goto -122 -> 320
                //
                // Exception table:
                //     from	to	target	type
                //     73	143	138	finally
                //     162	351	347	finally
                //     354	438	347	finally
            }

            public void onServiceDisconnected(int paramAnonymousInt)
            {
                switch (paramAnonymousInt)
                {
                default:
                case 2:
                case 1:
                }
                while (true)
                {
                    return;
                    synchronized (AudioService.this.mConnectedDevices)
                    {
                        if (AudioService.this.mConnectedDevices.containsKey(Integer.valueOf(128)))
                            AudioService.this.makeA2dpDeviceUnavailableNow((String)AudioService.this.mConnectedDevices.get(Integer.valueOf(128)));
                    }
                    synchronized (AudioService.this.mScoClients)
                    {
                        AudioService.access$2502(AudioService.this, null);
                    }
                }
            }
        };
        this.mBecomingNoisyIntentDevices = 908;
        this.mPhoneStateListener = new PhoneStateListener()
        {
            public void onCallStateChanged(int paramAnonymousInt, String paramAnonymousString)
            {
                if (paramAnonymousInt == 1)
                    synchronized (AudioService.mRingingLock)
                    {
                        AudioService.access$7702(AudioService.this, true);
                    }
                if ((paramAnonymousInt == 2) || (paramAnonymousInt == 0))
                    synchronized (AudioService.mRingingLock)
                    {
                        AudioService.access$7702(AudioService.this, false);
                    }
            }
        };
        this.mFocusStack = new Stack();
        this.mVoiceEventLock = new Object();
        this.mKeyEventDone = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if (paramAnonymousIntent == null);
                while (true)
                {
                    return;
                    Bundle localBundle = paramAnonymousIntent.getExtras();
                    if ((localBundle != null) && (localBundle.containsKey("android.media.AudioService.WAKELOCK_ACQUIRED")))
                        AudioService.this.mMediaEventWakeLock.release();
                }
            }
        };
        this.mCurrentRcLock = new Object();
        this.mCurrentRcClient = null;
        this.mCurrentRcClientGen = 0;
        this.mRCStack = new Stack();
        this.mMediaReceiverForCalls = null;
        this.mArtworkExpectedWidth = -1;
        this.mArtworkExpectedHeight = -1;
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
        this.mVoiceCapable = this.mContext.getResources().getBoolean(17891368);
        this.mMediaEventWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "handleMediaEvent");
        Vibrator localVibrator = (Vibrator)paramContext.getSystemService("vibrator");
        if (localVibrator == null);
        for (boolean bool = false; ; bool = localVibrator.hasVibrator())
        {
            this.mHasVibrator = bool;
            this.MAX_STREAM_VOLUME[0] = SystemProperties.getInt("ro.config.vc_call_vol_steps", this.MAX_STREAM_VOLUME[0]);
            SOUND_EFFECT_VOLUME_DB = paramContext.getResources().getInteger(17694724);
            this.mVolumePanel = new VolumePanel(paramContext, this);
            this.mMode = 0;
            this.mForcedUseForComm = 0;
            createAudioSystemThread();
            readPersistedSettings();
            this.mSettingsObserver = new SettingsObserver();
            updateStreamVolumeAlias(false);
            createStreamStates();
            this.mMediaServerOk = true;
            this.mRingerModeMutedStreams = 0;
            setRingerModeInt(getRingerMode(), false);
            AudioSystem.setErrorCallback(this.mAudioSystemCallback);
            IntentFilter localIntentFilter1 = new IntentFilter("android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED");
            localIntentFilter1.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
            localIntentFilter1.addAction("android.intent.action.DOCK_EVENT");
            localIntentFilter1.addAction("android.intent.action.USB_AUDIO_ACCESSORY_PLUG");
            localIntentFilter1.addAction("android.intent.action.USB_AUDIO_DEVICE_PLUG");
            localIntentFilter1.addAction("android.intent.action.BOOT_COMPLETED");
            localIntentFilter1.addAction("android.intent.action.SCREEN_ON");
            localIntentFilter1.addAction("android.intent.action.SCREEN_OFF");
            if (SystemProperties.getBoolean("ro.audio.monitorOrientation", false))
            {
                Log.v("AudioService", "monitoring device orientation");
                localIntentFilter1.addAction("android.intent.action.CONFIGURATION_CHANGED");
                setOrientationForAudioSystem();
            }
            paramContext.registerReceiver(this.mReceiver, localIntentFilter1);
            IntentFilter localIntentFilter2 = new IntentFilter();
            localIntentFilter2.addAction("android.intent.action.PACKAGE_REMOVED");
            localIntentFilter2.addDataScheme("package");
            paramContext.registerReceiver(this.mReceiver, localIntentFilter2);
            ((TelephonyManager)paramContext.getSystemService("phone")).listen(this.mPhoneStateListener, 32);
            this.mUseMasterVolume = paramContext.getResources().getBoolean(17891338);
            restoreMasterVolume();
            this.mMasterVolumeRamp = paramContext.getResources().getIntArray(17235986);
            this.mMainRemote = new RemotePlaybackState(-1, this.MAX_STREAM_VOLUME[3], this.MAX_STREAM_VOLUME[3], null);
            this.mHasRemotePlayback = false;
            this.mMainRemoteIsActive = false;
            postReevaluateRemote();
            return;
        }
    }

    private void adjustRemoteVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        synchronized (this.mMainRemote)
        {
            if (this.mMainRemoteIsActive)
            {
                int i = this.mMainRemote.mRccId;
                int j;
                if (this.mMainRemote.mVolumeHandling == 0)
                {
                    j = 1;
                    if (j == 0)
                        sendVolumeUpdateToRemote(i, paramInt2);
                    this.mVolumePanel.postRemoteVolumeChanged(paramInt1, paramInt3);
                }
                else
                {
                    j = 0;
                }
            }
        }
    }

    private void broadcastMasterMuteStatus(boolean paramBoolean)
    {
        Intent localIntent = new Intent("android.media.MASTER_MUTE_CHANGED_ACTION");
        localIntent.putExtra("android.media.EXTRA_MASTER_VOLUME_MUTED", paramBoolean);
        localIntent.addFlags(671088640);
        long l = Binder.clearCallingIdentity();
        this.mContext.sendStickyBroadcast(localIntent);
        Binder.restoreCallingIdentity(l);
    }

    private void broadcastRingerMode(int paramInt)
    {
        Intent localIntent = new Intent("android.media.RINGER_MODE_CHANGED");
        localIntent.putExtra("android.media.EXTRA_RINGER_MODE", paramInt);
        localIntent.addFlags(671088640);
        long l = Binder.clearCallingIdentity();
        this.mContext.sendStickyBroadcast(localIntent);
        Binder.restoreCallingIdentity(l);
    }

    private void broadcastScoConnectionState(int paramInt)
    {
        if (paramInt != this.mScoConnectionState)
        {
            Intent localIntent = new Intent("android.media.ACTION_SCO_AUDIO_STATE_UPDATED");
            localIntent.putExtra("android.media.extra.SCO_AUDIO_STATE", paramInt);
            localIntent.putExtra("android.media.extra.SCO_AUDIO_PREVIOUS_STATE", this.mScoConnectionState);
            this.mContext.sendStickyBroadcast(localIntent);
            this.mScoConnectionState = paramInt;
        }
    }

    private void broadcastVibrateSetting(int paramInt)
    {
        if (ActivityManagerNative.isSystemReady())
        {
            Intent localIntent = new Intent("android.media.VIBRATE_SETTING_CHANGED");
            localIntent.putExtra("android.media.EXTRA_VIBRATE_TYPE", paramInt);
            localIntent.putExtra("android.media.EXTRA_VIBRATE_SETTING", getVibrateSetting(paramInt));
            this.mContext.sendBroadcast(localIntent);
        }
    }

    private boolean canReassignAudioFocus()
    {
        if ((!this.mFocusStack.isEmpty()) && ("AudioFocus_For_Phone_Ring_And_Calls".equals(((FocusStackEntry)this.mFocusStack.peek()).mClientId)));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private void cancelA2dpDeviceTimeout()
    {
        this.mAudioHandler.removeMessages(7);
    }

    private void checkAllAliasStreamVolumes()
    {
        int i = AudioSystem.getNumStreamTypes();
        for (int j = 0; j < i; j++)
        {
            if (j != this.mStreamVolumeAlias[j])
            {
                this.mStreamStates[j].setAllIndexes(this.mStreamStates[this.mStreamVolumeAlias[j]], false);
                this.mStreamStates[j].setAllIndexes(this.mStreamStates[this.mStreamVolumeAlias[j]], true);
            }
            if (this.mStreamStates[j].muteCount() == 0)
                this.mStreamStates[j].applyAllVolumes();
        }
    }

    private boolean checkForRingerModeChange(int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool = true;
        int i = getRingerMode();
        switch (i)
        {
        default:
            Log.e("AudioService", "checkForRingerModeChange() wrong ringer mode: " + i);
        case 2:
        case 1:
            while (true)
            {
                setRingerMode(i);
                this.mPrevVolDirection = paramInt2;
                return bool;
                if (paramInt2 == -1)
                    if (this.mHasVibrator)
                    {
                        if ((paramInt3 <= paramInt1) && (paramInt1 < paramInt3 * 2))
                            i = 1;
                    }
                    else if ((paramInt1 < paramInt3) && (this.mPrevVolDirection != -1))
                    {
                        i = 0;
                        continue;
                        if (this.mHasVibrator)
                            break;
                        Log.e("AudioService", "checkForRingerModeChange() current ringer mode is vibratebut no vibrator is present");
                    }
            }
            if (paramInt2 == -1)
                if (this.mPrevVolDirection != -1)
                    i = 0;
            while (true)
            {
                bool = false;
                break;
                if (paramInt2 == 1)
                    i = 2;
            }
        case 0:
        }
        if (paramInt2 == 1)
            if (!this.mHasVibrator)
                break label203;
        label203: for (i = 1; ; i = 2)
        {
            bool = false;
            break;
        }
    }

    private void checkScoAudioState()
    {
        if ((this.mBluetoothHeadset != null) && (this.mBluetoothHeadsetDevice != null) && (this.mScoAudioState == 0) && (this.mBluetoothHeadset.getAudioState(this.mBluetoothHeadsetDevice) != 10))
            this.mScoAudioState = 2;
    }

    private int checkSendBecomingNoisyIntent(int paramInt1, int paramInt2)
    {
        int i = 0;
        if ((paramInt2 == 0) && ((paramInt1 & this.mBecomingNoisyIntentDevices) != 0))
        {
            int j = 0;
            Iterator localIterator = this.mConnectedDevices.keySet().iterator();
            while (localIterator.hasNext())
            {
                int k = ((Integer)localIterator.next()).intValue();
                if ((k & this.mBecomingNoisyIntentDevices) != 0)
                    j |= k;
            }
            if (j == paramInt1)
            {
                i = 1000;
                sendBecomingNoisyIntent();
            }
        }
        if ((this.mAudioHandler.hasMessages(21)) || (this.mAudioHandler.hasMessages(20)))
            i = 1000;
        return i;
    }

    private void checkUpdateRemoteControlDisplay_syncAfRcs(int paramInt)
    {
        if ((this.mRCStack.isEmpty()) || (this.mFocusStack.isEmpty()))
            clearRemoteControlDisplay_syncAfRcs();
        while (true)
        {
            return;
            if ((((RemoteControlStackEntry)this.mRCStack.peek()).mCallingPackageName != null) && (((FocusStackEntry)this.mFocusStack.peek()).mPackageName != null) && (((RemoteControlStackEntry)this.mRCStack.peek()).mCallingPackageName.compareTo(((FocusStackEntry)this.mFocusStack.peek()).mPackageName) != 0))
                clearRemoteControlDisplay_syncAfRcs();
            else if (((RemoteControlStackEntry)this.mRCStack.peek()).mCallingUid != ((FocusStackEntry)this.mFocusStack.peek()).mCallingUid)
                clearRemoteControlDisplay_syncAfRcs();
            else
                updateRemoteControlDisplay_syncAfRcs(paramInt);
        }
    }

    private boolean checkUpdateRemoteStateIfActive(int paramInt)
    {
        int i = 1;
        while (true)
        {
            synchronized (this.mRCStack)
            {
                Iterator localIterator = this.mRCStack.iterator();
                if (localIterator.hasNext())
                {
                    RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                    if ((localRemoteControlStackEntry.mPlaybackType != i) || (!isPlaystateActive(localRemoteControlStackEntry.mPlaybackState)) || (localRemoteControlStackEntry.mPlaybackStream != paramInt))
                        continue;
                    synchronized (this.mMainRemote)
                    {
                        this.mMainRemote.mRccId = localRemoteControlStackEntry.mRccId;
                        this.mMainRemote.mVolume = localRemoteControlStackEntry.mPlaybackVolume;
                        this.mMainRemote.mVolumeMax = localRemoteControlStackEntry.mPlaybackVolumeMax;
                        this.mMainRemote.mVolumeHandling = localRemoteControlStackEntry.mPlaybackVolumeHandling;
                        this.mMainRemoteIsActive = true;
                        return i;
                    }
                }
            }
            synchronized (this.mMainRemote)
            {
                this.mMainRemoteIsActive = false;
                int j = 0;
            }
        }
    }

    private void clearRemoteControlDisplay_syncAfRcs()
    {
        synchronized (this.mCurrentRcLock)
        {
            this.mCurrentRcClient = null;
            this.mAudioHandler.sendMessage(this.mAudioHandler.obtainMessage(12));
            return;
        }
    }

    private void createAudioSystemThread()
    {
        this.mAudioSystemThread = new AudioSystemThread();
        this.mAudioSystemThread.start();
        waitForAudioHandlerCreation();
    }

    private void createStreamStates()
    {
        int i = AudioSystem.getNumStreamTypes();
        VolumeStreamState[] arrayOfVolumeStreamState = new VolumeStreamState[i];
        this.mStreamStates = arrayOfVolumeStreamState;
        for (int j = 0; j < i; j++)
            arrayOfVolumeStreamState[j] = new VolumeStreamState(Settings.System.VOLUME_SETTINGS[this.mStreamVolumeAlias[j]], j, null);
        checkAllAliasStreamVolumes();
    }

    // ERROR //
    private void disconnectBluetoothSco(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 367	android/media/AudioService:mScoClients	Ljava/util/ArrayList;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: invokespecial 663	android/media/AudioService:checkScoAudioState	()V
        //     11: aload_0
        //     12: getfield 671	android/media/AudioService:mScoAudioState	I
        //     15: iconst_2
        //     16: if_icmpeq +11 -> 27
        //     19: aload_0
        //     20: getfield 671	android/media/AudioService:mScoAudioState	I
        //     23: iconst_4
        //     24: if_icmpne +76 -> 100
        //     27: aload_0
        //     28: getfield 682	android/media/AudioService:mBluetoothHeadsetDevice	Landroid/bluetooth/BluetoothDevice;
        //     31: ifnull +38 -> 69
        //     34: aload_0
        //     35: getfield 676	android/media/AudioService:mBluetoothHeadset	Landroid/bluetooth/BluetoothHeadset;
        //     38: ifnull +34 -> 72
        //     41: aload_0
        //     42: getfield 676	android/media/AudioService:mBluetoothHeadset	Landroid/bluetooth/BluetoothHeadset;
        //     45: aload_0
        //     46: getfield 682	android/media/AudioService:mBluetoothHeadsetDevice	Landroid/bluetooth/BluetoothDevice;
        //     49: invokevirtual 1103	android/bluetooth/BluetoothHeadset:stopVoiceRecognition	(Landroid/bluetooth/BluetoothDevice;)Z
        //     52: ifne +17 -> 69
        //     55: aload_0
        //     56: getfield 627	android/media/AudioService:mAudioHandler	Landroid/media/AudioService$AudioHandler;
        //     59: bipush 11
        //     61: iconst_0
        //     62: iconst_0
        //     63: iconst_0
        //     64: aconst_null
        //     65: iconst_0
        //     66: invokestatic 658	android/media/AudioService:sendMsg	(Landroid/os/Handler;IIIILjava/lang/Object;I)V
        //     69: aload_2
        //     70: monitorexit
        //     71: return
        //     72: aload_0
        //     73: getfield 671	android/media/AudioService:mScoAudioState	I
        //     76: iconst_2
        //     77: if_icmpne -8 -> 69
        //     80: aload_0
        //     81: invokespecial 688	android/media/AudioService:getBluetoothHeadset	()Z
        //     84: ifeq -15 -> 69
        //     87: aload_0
        //     88: iconst_4
        //     89: putfield 671	android/media/AudioService:mScoAudioState	I
        //     92: goto -23 -> 69
        //     95: astore_3
        //     96: aload_2
        //     97: monitorexit
        //     98: aload_3
        //     99: athrow
        //     100: aload_0
        //     101: iload_1
        //     102: iconst_1
        //     103: invokevirtual 1106	android/media/AudioService:clearAllScoClients	(IZ)V
        //     106: goto -37 -> 69
        //
        // Exception table:
        //     from	to	target	type
        //     7	98	95	finally
        //     100	106	95	finally
    }

    private void dispatchMediaKeyEvent(KeyEvent paramKeyEvent, boolean paramBoolean)
    {
        int i = 1980;
        if (paramBoolean)
            this.mMediaEventWakeLock.acquire();
        Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON", null);
        localIntent.putExtra("android.intent.extra.KEY_EVENT", paramKeyEvent);
        while (true)
        {
            synchronized (this.mRCStack)
            {
                boolean bool = this.mRCStack.empty();
                if (!bool)
                    try
                    {
                        PendingIntent localPendingIntent = ((RemoteControlStackEntry)this.mRCStack.peek()).mMediaIntent;
                        Context localContext = this.mContext;
                        if (!paramBoolean)
                            break label195;
                        localPendingIntent.send(localContext, i, localIntent, this, this.mAudioHandler);
                        return;
                    }
                    catch (PendingIntent.CanceledException localCanceledException)
                    {
                        Log.e("AudioService", "Error sending pending intent " + this.mRCStack.peek());
                        localCanceledException.printStackTrace();
                        continue;
                    }
            }
            if (paramBoolean)
                localIntent.putExtra("android.media.AudioService.WAKELOCK_ACQUIRED", 1980);
            this.mContext.sendOrderedBroadcast(localIntent, null, this.mKeyEventDone, this.mAudioHandler, -1, null, null);
            continue;
            label195: i = 0;
        }
    }

    private void dispatchMediaKeyEventForCalls(KeyEvent paramKeyEvent, boolean paramBoolean)
    {
        Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON", null);
        localIntent.putExtra("android.intent.extra.KEY_EVENT", paramKeyEvent);
        localIntent.setPackage(this.mMediaReceiverForCalls.getPackageName());
        if (paramBoolean)
        {
            this.mMediaEventWakeLock.acquire();
            localIntent.putExtra("android.media.AudioService.WAKELOCK_ACQUIRED", 1980);
        }
        this.mContext.sendOrderedBroadcast(localIntent, null, this.mKeyEventDone, this.mAudioHandler, -1, null, null);
    }

    private void doSetMasterVolume(float paramFloat, int paramInt)
    {
        if (!AudioSystem.getMasterMute())
        {
            int i = getMasterVolume();
            AudioSystem.setMasterVolume(paramFloat);
            int j = getMasterVolume();
            if (j != i)
                sendMsg(this.mAudioHandler, 2, 0, Math.round(1000.0F * paramFloat), 0, null, 500);
            sendMasterVolumeUpdate(paramInt, i, j);
        }
    }

    // ERROR //
    private void dumpFocusStack(PrintWriter paramPrintWriter)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc_w 1185
        //     4: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     7: getstatic 302	android/media/AudioService:mAudioFocusLock	Ljava/lang/Object;
        //     10: astore_2
        //     11: aload_2
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     17: invokevirtual 1044	java/util/Stack:iterator	()Ljava/util/Iterator;
        //     20: astore 4
        //     22: aload 4
        //     24: invokeinterface 1007 1 0
        //     29: ifeq +93 -> 122
        //     32: aload 4
        //     34: invokeinterface 1010 1 0
        //     39: checkcast 31	android/media/AudioService$FocusStackEntry
        //     42: astore 5
        //     44: aload_1
        //     45: new 964	java/lang/StringBuilder
        //     48: dup
        //     49: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     52: ldc_w 1192
        //     55: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     58: aload 5
        //     60: getfield 1196	android/media/AudioService$FocusStackEntry:mSourceRef	Landroid/os/IBinder;
        //     63: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     66: ldc_w 1198
        //     69: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     72: aload 5
        //     74: getfield 937	android/media/AudioService$FocusStackEntry:mClientId	Ljava/lang/String;
        //     77: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     80: ldc_w 1200
        //     83: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     86: aload 5
        //     88: getfield 1203	android/media/AudioService$FocusStackEntry:mFocusChangeType	I
        //     91: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     94: ldc_w 1205
        //     97: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: aload 5
        //     102: getfield 1039	android/media/AudioService$FocusStackEntry:mCallingUid	I
        //     105: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     108: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     111: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     114: goto -92 -> 22
        //     117: astore_3
        //     118: aload_2
        //     119: monitorexit
        //     120: aload_3
        //     121: athrow
        //     122: aload_2
        //     123: monitorexit
        //     124: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	120	117	finally
        //     122	124	117	finally
    }

    // ERROR //
    private void dumpRCCStack(PrintWriter paramPrintWriter)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc_w 1208
        //     4: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     7: aload_0
        //     8: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     11: astore_2
        //     12: aload_2
        //     13: monitorenter
        //     14: aload_0
        //     15: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     18: invokevirtual 1044	java/util/Stack:iterator	()Ljava/util/Iterator;
        //     21: astore 4
        //     23: aload 4
        //     25: invokeinterface 1007 1 0
        //     30: ifeq +149 -> 179
        //     33: aload 4
        //     35: invokeinterface 1010 1 0
        //     40: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     43: astore 9
        //     45: aload_1
        //     46: new 964	java/lang/StringBuilder
        //     49: dup
        //     50: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     53: ldc_w 1210
        //     56: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     59: aload 9
        //     61: getfield 1038	android/media/AudioService$RemoteControlStackEntry:mCallingUid	I
        //     64: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     67: ldc_w 1212
        //     70: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: aload 9
        //     75: getfield 1057	android/media/AudioService$RemoteControlStackEntry:mRccId	I
        //     78: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     81: ldc_w 1214
        //     84: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     87: aload 9
        //     89: getfield 1047	android/media/AudioService$RemoteControlStackEntry:mPlaybackType	I
        //     92: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     95: ldc_w 1216
        //     98: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     101: aload 9
        //     103: getfield 1050	android/media/AudioService$RemoteControlStackEntry:mPlaybackState	I
        //     106: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     109: ldc_w 1218
        //     112: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: aload 9
        //     117: getfield 1072	android/media/AudioService$RemoteControlStackEntry:mPlaybackVolumeHandling	I
        //     120: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     123: ldc_w 1220
        //     126: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     129: aload 9
        //     131: getfield 1060	android/media/AudioService$RemoteControlStackEntry:mPlaybackVolume	I
        //     134: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     137: ldc_w 1222
        //     140: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     143: aload 9
        //     145: getfield 1066	android/media/AudioService$RemoteControlStackEntry:mPlaybackVolumeMax	I
        //     148: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     151: ldc_w 1224
        //     154: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     157: aload 9
        //     159: getfield 1228	android/media/AudioService$RemoteControlStackEntry:mRemoteVolumeObs	Landroid/media/IRemoteVolumeObserver;
        //     162: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     165: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     168: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     171: goto -148 -> 23
        //     174: astore_3
        //     175: aload_2
        //     176: monitorexit
        //     177: aload_3
        //     178: athrow
        //     179: aload_2
        //     180: monitorexit
        //     181: aload_0
        //     182: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     185: astore 5
        //     187: aload 5
        //     189: monitorenter
        //     190: aload_1
        //     191: ldc_w 1230
        //     194: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     197: aload_1
        //     198: new 964	java/lang/StringBuilder
        //     201: dup
        //     202: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     205: ldc_w 1232
        //     208: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: aload_0
        //     212: getfield 610	android/media/AudioService:mHasRemotePlayback	Z
        //     215: invokevirtual 1235	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     218: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     221: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     224: aload_1
        //     225: new 964	java/lang/StringBuilder
        //     228: dup
        //     229: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     232: ldc_w 1237
        //     235: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     238: aload_0
        //     239: getfield 612	android/media/AudioService:mMainRemoteIsActive	Z
        //     242: invokevirtual 1235	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     245: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     248: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     251: aload_1
        //     252: new 964	java/lang/StringBuilder
        //     255: dup
        //     256: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     259: ldc_w 1239
        //     262: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     265: aload_0
        //     266: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     269: getfield 854	android/media/AudioService$RemotePlaybackState:mRccId	I
        //     272: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     275: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     278: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     281: new 964	java/lang/StringBuilder
        //     284: dup
        //     285: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     288: ldc_w 1241
        //     291: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     294: astore 7
        //     296: aload_0
        //     297: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     300: getfield 857	android/media/AudioService$RemotePlaybackState:mVolumeHandling	I
        //     303: ifne +94 -> 397
        //     306: ldc_w 1243
        //     309: astore 8
        //     311: aload_1
        //     312: aload 7
        //     314: aload 8
        //     316: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     319: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     322: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     325: aload_1
        //     326: new 964	java/lang/StringBuilder
        //     329: dup
        //     330: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     333: ldc_w 1245
        //     336: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     339: aload_0
        //     340: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     343: getfield 1063	android/media/AudioService$RemotePlaybackState:mVolume	I
        //     346: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     349: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     352: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     355: aload_1
        //     356: new 964	java/lang/StringBuilder
        //     359: dup
        //     360: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     363: ldc_w 1247
        //     366: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     369: aload_0
        //     370: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     373: getfield 1069	android/media/AudioService$RemotePlaybackState:mVolumeMax	I
        //     376: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     379: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     382: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     385: aload 5
        //     387: monitorexit
        //     388: return
        //     389: astore 6
        //     391: aload 5
        //     393: monitorexit
        //     394: aload 6
        //     396: athrow
        //     397: ldc_w 1249
        //     400: astore 8
        //     402: goto -91 -> 311
        //
        // Exception table:
        //     from	to	target	type
        //     14	177	174	finally
        //     179	181	174	finally
        //     190	394	389	finally
    }

    // ERROR //
    private void dumpRCStack(PrintWriter paramPrintWriter)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc_w 1252
        //     4: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     7: aload_0
        //     8: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     11: astore_2
        //     12: aload_2
        //     13: monitorenter
        //     14: aload_0
        //     15: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     18: invokevirtual 1044	java/util/Stack:iterator	()Ljava/util/Iterator;
        //     21: astore 4
        //     23: aload 4
        //     25: invokeinterface 1007 1 0
        //     30: ifeq +121 -> 151
        //     33: aload 4
        //     35: invokeinterface 1010 1 0
        //     40: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     43: astore 5
        //     45: aload_1
        //     46: new 964	java/lang/StringBuilder
        //     49: dup
        //     50: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     53: ldc_w 1254
        //     56: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     59: aload 5
        //     61: getfield 1132	android/media/AudioService$RemoteControlStackEntry:mMediaIntent	Landroid/app/PendingIntent;
        //     64: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     67: ldc_w 1256
        //     70: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: aload 5
        //     75: getfield 1259	android/media/AudioService$RemoteControlStackEntry:mReceiverComponent	Landroid/content/ComponentName;
        //     78: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     81: ldc_w 1261
        //     84: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     87: aload 5
        //     89: getfield 1264	android/media/AudioService$RemoteControlStackEntry:mRcClient	Landroid/media/IRemoteControlClient;
        //     92: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     95: ldc_w 1266
        //     98: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     101: aload 5
        //     103: getfield 1038	android/media/AudioService$RemoteControlStackEntry:mCallingUid	I
        //     106: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     109: ldc_w 1214
        //     112: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: aload 5
        //     117: getfield 1047	android/media/AudioService$RemoteControlStackEntry:mPlaybackType	I
        //     120: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     123: ldc_w 1268
        //     126: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     129: aload 5
        //     131: getfield 1050	android/media/AudioService$RemoteControlStackEntry:mPlaybackState	I
        //     134: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     137: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     140: invokevirtual 1190	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     143: goto -120 -> 23
        //     146: astore_3
        //     147: aload_2
        //     148: monitorexit
        //     149: aload_3
        //     150: athrow
        //     151: aload_2
        //     152: monitorexit
        //     153: return
        //
        // Exception table:
        //     from	to	target	type
        //     14	149	146	finally
        //     151	153	146	finally
    }

    private void dumpStreamStates(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\nStream volumes (device: index)");
        int i = AudioSystem.getNumStreamTypes();
        for (int j = 0; j < i; j++)
        {
            paramPrintWriter.println("- " + this.STREAM_NAMES[j] + ":");
            this.mStreamStates[j].dump(paramPrintWriter);
            paramPrintWriter.println("");
        }
    }

    private void ensureValidDirection(int paramInt)
    {
        if ((paramInt < -1) || (paramInt > 1))
            throw new IllegalArgumentException("Bad direction " + paramInt);
    }

    private void ensureValidRingerMode(int paramInt)
    {
        if (!AudioManager.isValidRingerMode(paramInt))
            throw new IllegalArgumentException("Bad ringer mode " + paramInt);
    }

    private void ensureValidSteps(int paramInt)
    {
        if (Math.abs(paramInt) > 4)
            throw new IllegalArgumentException("Bad volume adjust steps " + paramInt);
    }

    private void ensureValidStreamType(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mStreamStates.length))
            throw new IllegalArgumentException("Bad stream type " + paramInt);
    }

    // ERROR //
    private void filterMediaKeyEvent(KeyEvent paramKeyEvent, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: invokestatic 1309	android/media/AudioService:isValidMediaKeyEvent	(Landroid/view/KeyEvent;)Z
        //     4: ifne +30 -> 34
        //     7: ldc 156
        //     9: new 964	java/lang/StringBuilder
        //     12: dup
        //     13: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     16: ldc_w 1311
        //     19: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     22: aload_1
        //     23: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     26: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     29: invokestatic 981	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     32: pop
        //     33: return
        //     34: getstatic 304	android/media/AudioService:mRingingLock	Ljava/lang/Object;
        //     37: astore_3
        //     38: aload_3
        //     39: monitorenter
        //     40: aload_0
        //     41: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     44: astore 5
        //     46: aload 5
        //     48: monitorenter
        //     49: aload_0
        //     50: getfield 419	android/media/AudioService:mMediaReceiverForCalls	Landroid/content/ComponentName;
        //     53: ifnull +39 -> 92
        //     56: aload_0
        //     57: getfield 355	android/media/AudioService:mIsRinging	Z
        //     60: ifne +11 -> 71
        //     63: aload_0
        //     64: invokevirtual 1314	android/media/AudioService:getMode	()I
        //     67: iconst_2
        //     68: if_icmpne +24 -> 92
        //     71: aload_0
        //     72: aload_1
        //     73: iload_2
        //     74: invokespecial 1316	android/media/AudioService:dispatchMediaKeyEventForCalls	(Landroid/view/KeyEvent;Z)V
        //     77: aload 5
        //     79: monitorexit
        //     80: aload_3
        //     81: monitorexit
        //     82: goto -49 -> 33
        //     85: astore 4
        //     87: aload_3
        //     88: monitorexit
        //     89: aload 4
        //     91: athrow
        //     92: aload 5
        //     94: monitorexit
        //     95: aload_3
        //     96: monitorexit
        //     97: aload_1
        //     98: invokevirtual 1321	android/view/KeyEvent:getKeyCode	()I
        //     101: invokestatic 1324	android/media/AudioService:isValidVoiceInputKeyCode	(I)Z
        //     104: ifeq +20 -> 124
        //     107: aload_0
        //     108: aload_1
        //     109: iload_2
        //     110: invokespecial 1327	android/media/AudioService:filterVoiceInputKeyEvent	(Landroid/view/KeyEvent;Z)V
        //     113: goto -80 -> 33
        //     116: astore 6
        //     118: aload 5
        //     120: monitorexit
        //     121: aload 6
        //     123: athrow
        //     124: aload_0
        //     125: aload_1
        //     126: iload_2
        //     127: invokespecial 1329	android/media/AudioService:dispatchMediaKeyEvent	(Landroid/view/KeyEvent;Z)V
        //     130: goto -97 -> 33
        //
        // Exception table:
        //     from	to	target	type
        //     40	49	85	finally
        //     80	89	85	finally
        //     95	97	85	finally
        //     121	124	85	finally
        //     49	80	116	finally
        //     92	95	116	finally
        //     118	121	116	finally
    }

    private void filterVoiceInputKeyEvent(KeyEvent paramKeyEvent, boolean paramBoolean)
    {
        int i = 1;
        int j = paramKeyEvent.getAction();
        Object localObject1 = this.mVoiceEventLock;
        if (j == 0);
        try
        {
            if (paramKeyEvent.getRepeatCount() == 0)
            {
                this.mVoiceButtonDown = true;
                this.mVoiceButtonHandled = false;
            }
            do
            {
                do
                    while (true)
                        switch (i)
                        {
                        case 2:
                            if ((this.mVoiceButtonDown) && (!this.mVoiceButtonHandled) && ((0x80 & paramKeyEvent.getFlags()) != 0))
                            {
                                this.mVoiceButtonHandled = true;
                                i = 2;
                            }
                            break;
                        case 3:
                        case 1:
                        }
                while ((j != 1) || (!this.mVoiceButtonDown));
                this.mVoiceButtonDown = false;
            }
            while ((this.mVoiceButtonHandled) || (paramKeyEvent.isCanceled()));
            i = 3;
        }
        finally
        {
        }
        return;
        sendSimulatedMediaButtonEvent(paramKeyEvent, paramBoolean);
    }

    private int findVolumeDelta(int paramInt1, int paramInt2)
    {
        int i = 0;
        int j = 0;
        int n;
        if (paramInt1 == 1)
        {
            if (paramInt2 == 100)
                return i;
            j = this.mMasterVolumeRamp[1];
            n = -1 + this.mMasterVolumeRamp.length;
            label36: if (n > 1)
            {
                if (paramInt2 < this.mMasterVolumeRamp[(n - 1)])
                    break label70;
                j = this.mMasterVolumeRamp[n];
            }
        }
        label145: 
        while (true)
        {
            i = j;
            break;
            label70: n -= 2;
            break label36;
            if (paramInt1 == -1)
            {
                if (paramInt2 == 0)
                    break;
                int k = this.mMasterVolumeRamp.length;
                j = -this.mMasterVolumeRamp[(k - 1)];
                for (int m = 2; ; m += 2)
                {
                    if (m >= k)
                        break label145;
                    if (paramInt2 <= this.mMasterVolumeRamp[m])
                    {
                        j = -this.mMasterVolumeRamp[(m - 1)];
                        break;
                    }
                }
            }
        }
    }

    private int getActiveStreamType(int paramInt)
    {
        if (this.mVoiceCapable)
            if (isInCommunication())
                if (AudioSystem.getForceUse(0) == 3)
                    paramInt = 6;
        while (true)
        {
            return paramInt;
            paramInt = 0;
            continue;
            if (paramInt == -2147483648)
            {
                if (checkUpdateRemoteStateIfActive(3))
                    paramInt = -200;
                else if (AudioSystem.isStreamActive(3, 0))
                    paramInt = 3;
                else
                    paramInt = 2;
            }
            else if (AudioSystem.isStreamActive(3, 0))
            {
                paramInt = 3;
                continue;
                if (isInCommunication())
                {
                    if (AudioSystem.getForceUse(0) == 3)
                        paramInt = 6;
                    else
                        paramInt = 0;
                }
                else if ((AudioSystem.isStreamActive(5, 5000)) || (AudioSystem.isStreamActive(2, 5000)))
                    paramInt = 5;
                else if (paramInt == -2147483648)
                    if (checkUpdateRemoteStateIfActive(3))
                        paramInt = -200;
                    else
                        paramInt = 3;
            }
        }
    }

    private boolean getBluetoothHeadset()
    {
        boolean bool = false;
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localBluetoothAdapter != null)
            bool = localBluetoothAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 1);
        AudioHandler localAudioHandler = this.mAudioHandler;
        if (bool);
        for (int i = 3000; ; i = 0)
        {
            sendMsg(localAudioHandler, 11, 0, 0, 0, null, i);
            return bool;
        }
    }

    private int getDeviceForStream(int paramInt)
    {
        int i = AudioSystem.getDevicesForStream(paramInt);
        if ((i & i - 1) != 0)
        {
            if ((i & 0x2) == 0)
                break label23;
            i = 2;
        }
        while (true)
        {
            return i;
            label23: i &= 896;
        }
    }

    private ScoClient getScoClient(IBinder paramIBinder, boolean paramBoolean)
    {
        synchronized (this.mScoClients)
        {
            int i = this.mScoClients.size();
            int j = 0;
            Object localObject4 = null;
            while (true)
            {
                if (j < i);
                try
                {
                    ScoClient localScoClient = (ScoClient)this.mScoClients.get(j);
                    if (localScoClient.getBinder() == paramIBinder);
                    for (Object localObject6 = localScoClient; ; localObject6 = localObject5)
                    {
                        return localObject6;
                        j++;
                        localObject4 = localScoClient;
                        break;
                        if (!paramBoolean)
                            break label118;
                        localObject5 = new ScoClient(paramIBinder);
                        this.mScoClients.add(localObject5);
                    }
                    Object localObject1;
                    throw localObject1;
                }
                finally
                {
                    while (true)
                    {
                        continue;
                        label118: Object localObject5 = localObject4;
                    }
                }
            }
        }
    }

    public static int getValueForVibrateSetting(int paramInt1, int paramInt2, int paramInt3)
    {
        return paramInt1 & (0xFFFFFFFF ^ 3 << paramInt2 * 2) | (paramInt3 & 0x3) << paramInt2 * 2;
    }

    private void handleConfigurationChanged(Context paramContext)
    {
        try
        {
            int i = paramContext.getResources().getConfiguration().orientation;
            if (i != this.mDeviceOrientation)
            {
                this.mDeviceOrientation = i;
                setOrientationForAudioSystem();
            }
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Log.e("AudioService", "Error retrieving device orientation: " + localException);
        }
    }

    private boolean handleDeviceConnection(boolean paramBoolean, int paramInt, String paramString)
    {
        while (true)
        {
            boolean bool;
            synchronized (this.mConnectedDevices)
            {
                if (!this.mConnectedDevices.containsKey(Integer.valueOf(paramInt)))
                    break label172;
                if (!paramString.isEmpty())
                {
                    if (!((String)this.mConnectedDevices.get(Integer.valueOf(paramInt))).equals(paramString))
                        break label172;
                    continue;
                    if ((i != 0) && (!paramBoolean))
                    {
                        AudioSystem.setDeviceConnectionState(paramInt, 0, (String)this.mConnectedDevices.get(Integer.valueOf(paramInt)));
                        this.mConnectedDevices.remove(Integer.valueOf(paramInt));
                        bool = true;
                    }
                    else if ((i == 0) && (paramBoolean))
                    {
                        AudioSystem.setDeviceConnectionState(paramInt, 1, paramString);
                        this.mConnectedDevices.put(new Integer(paramInt), paramString);
                        bool = true;
                    }
                    else
                    {
                        bool = false;
                    }
                }
            }
            continue;
            return bool;
            label172: int i = 0;
        }
    }

    private boolean hasScheduledA2dpDockTimeout()
    {
        return this.mAudioHandler.hasMessages(7);
    }

    private boolean isCurrentRcController(PendingIntent paramPendingIntent)
    {
        if ((!this.mRCStack.empty()) && (((RemoteControlStackEntry)this.mRCStack.peek()).mMediaIntent.equals(paramPendingIntent)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isInCommunication()
    {
        int i = 0;
        if (this.mVoiceCapable);
        try
        {
            ITelephony localITelephony = ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
            if (localITelephony != null)
            {
                boolean bool2 = localITelephony.isOffhook();
                i = bool2;
            }
            if ((i != 0) || (getMode() == 3))
            {
                bool1 = true;
                return bool1;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("AudioService", "Couldn't connect to phone service", localRemoteException);
                continue;
                boolean bool1 = false;
            }
        }
    }

    private static boolean isPlaystateActive(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private boolean isStreamMutedByRingerMode(int paramInt)
    {
        int i = 1;
        if ((this.mRingerModeMutedStreams & i << paramInt) != 0);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    private static boolean isValidMediaKeyEvent(KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (paramKeyEvent == null);
        while (true)
        {
            return bool;
            switch (paramKeyEvent.getKeyCode())
            {
            default:
                break;
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
                bool = true;
            }
        }
    }

    private static boolean isValidVoiceInputKeyCode(int paramInt)
    {
        if (paramInt == 79);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void makeA2dpDeviceAvailable(String paramString)
    {
        setBluetoothA2dpOnInt(true);
        AudioSystem.setDeviceConnectionState(128, 1, paramString);
        AudioSystem.setParameters("A2dpSuspended=false");
        this.mConnectedDevices.put(new Integer(128), paramString);
    }

    private void makeA2dpDeviceUnavailableLater(String paramString)
    {
        AudioSystem.setParameters("A2dpSuspended=true");
        this.mConnectedDevices.remove(Integer.valueOf(128));
        Message localMessage = this.mAudioHandler.obtainMessage(7, paramString);
        this.mAudioHandler.sendMessageDelayed(localMessage, 8000L);
    }

    private void makeA2dpDeviceUnavailableNow(String paramString)
    {
        AudioSystem.setDeviceConnectionState(128, 0, paramString);
        this.mConnectedDevices.remove(Integer.valueOf(128));
    }

    private void notifyTopOfAudioFocusStack()
    {
        if ((!this.mFocusStack.empty()) && (((FocusStackEntry)this.mFocusStack.peek()).mFocusDispatcher != null) && (canReassignAudioFocus()));
        try
        {
            ((FocusStackEntry)this.mFocusStack.peek()).mFocusDispatcher.dispatchAudioFocusChange(1, ((FocusStackEntry)this.mFocusStack.peek()).mClientId);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("AudioService", "Failure to signal gain of audio control focus due to " + localRemoteException);
                localRemoteException.printStackTrace();
            }
        }
    }

    // ERROR //
    private void onNewPlaybackInfoForRcc(int paramInt1, int paramInt2, int paramInt3)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     13: invokevirtual 1044	java/util/Stack:iterator	()Ljava/util/Iterator;
        //     16: astore 6
        //     18: aload 6
        //     20: invokeinterface 1007 1 0
        //     25: ifeq +372 -> 397
        //     28: aload 6
        //     30: invokeinterface 1010 1 0
        //     35: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     38: astore 7
        //     40: aload 7
        //     42: getfield 1057	android/media/AudioService$RemoteControlStackEntry:mRccId	I
        //     45: iload_1
        //     46: if_icmpne -28 -> 18
        //     49: iload_2
        //     50: lookupswitch	default:+58->108, 1:+100->150, 2:+121->171, 3:+176->226, 4:+231->281, 5:+286->336, 255:+295->345
        //     109: ifge -17661 -> -17552
        //     112: wide
        //     116: multianewarray 4869 224
        //     120: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: iload_2
        //     124: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     127: ldc_w 1506
        //     130: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: iload_1
        //     134: invokevirtual 974	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     137: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     140: invokestatic 981	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     143: pop
        //     144: aload 4
        //     146: monitorexit
        //     147: goto +253 -> 400
        //     150: aload 7
        //     152: iload_3
        //     153: putfield 1047	android/media/AudioService$RemoteControlStackEntry:mPlaybackType	I
        //     156: aload_0
        //     157: invokespecial 615	android/media/AudioService:postReevaluateRemote	()V
        //     160: goto -16 -> 144
        //     163: astore 5
        //     165: aload 4
        //     167: monitorexit
        //     168: aload 5
        //     170: athrow
        //     171: aload 7
        //     173: iload_3
        //     174: putfield 1060	android/media/AudioService$RemoteControlStackEntry:mPlaybackVolume	I
        //     177: aload_0
        //     178: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     181: astore 14
        //     183: aload 14
        //     185: monitorenter
        //     186: iload_1
        //     187: aload_0
        //     188: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     191: getfield 854	android/media/AudioService$RemotePlaybackState:mRccId	I
        //     194: if_icmpne +18 -> 212
        //     197: aload_0
        //     198: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     201: iload_3
        //     202: putfield 1063	android/media/AudioService$RemotePlaybackState:mVolume	I
        //     205: aload_0
        //     206: getfield 490	android/media/AudioService:mVolumePanel	Lmiui/view/VolumePanel;
        //     209: invokevirtual 1509	miui/view/VolumePanel:postHasNewRemotePlaybackInfo	()V
        //     212: aload 14
        //     214: monitorexit
        //     215: goto -71 -> 144
        //     218: astore 15
        //     220: aload 14
        //     222: monitorexit
        //     223: aload 15
        //     225: athrow
        //     226: aload 7
        //     228: iload_3
        //     229: putfield 1066	android/media/AudioService$RemoteControlStackEntry:mPlaybackVolumeMax	I
        //     232: aload_0
        //     233: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     236: astore 12
        //     238: aload 12
        //     240: monitorenter
        //     241: iload_1
        //     242: aload_0
        //     243: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     246: getfield 854	android/media/AudioService$RemotePlaybackState:mRccId	I
        //     249: if_icmpne +18 -> 267
        //     252: aload_0
        //     253: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     256: iload_3
        //     257: putfield 1069	android/media/AudioService$RemotePlaybackState:mVolumeMax	I
        //     260: aload_0
        //     261: getfield 490	android/media/AudioService:mVolumePanel	Lmiui/view/VolumePanel;
        //     264: invokevirtual 1509	miui/view/VolumePanel:postHasNewRemotePlaybackInfo	()V
        //     267: aload 12
        //     269: monitorexit
        //     270: goto -126 -> 144
        //     273: astore 13
        //     275: aload 12
        //     277: monitorexit
        //     278: aload 13
        //     280: athrow
        //     281: aload 7
        //     283: iload_3
        //     284: putfield 1072	android/media/AudioService$RemoteControlStackEntry:mPlaybackVolumeHandling	I
        //     287: aload_0
        //     288: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     291: astore 10
        //     293: aload 10
        //     295: monitorenter
        //     296: iload_1
        //     297: aload_0
        //     298: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     301: getfield 854	android/media/AudioService$RemotePlaybackState:mRccId	I
        //     304: if_icmpne +18 -> 322
        //     307: aload_0
        //     308: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     311: iload_3
        //     312: putfield 857	android/media/AudioService$RemotePlaybackState:mVolumeHandling	I
        //     315: aload_0
        //     316: getfield 490	android/media/AudioService:mVolumePanel	Lmiui/view/VolumePanel;
        //     319: invokevirtual 1509	miui/view/VolumePanel:postHasNewRemotePlaybackInfo	()V
        //     322: aload 10
        //     324: monitorexit
        //     325: goto -181 -> 144
        //     328: astore 11
        //     330: aload 10
        //     332: monitorexit
        //     333: aload 11
        //     335: athrow
        //     336: aload 7
        //     338: iload_3
        //     339: putfield 1056	android/media/AudioService$RemoteControlStackEntry:mPlaybackStream	I
        //     342: goto -198 -> 144
        //     345: aload 7
        //     347: iload_3
        //     348: putfield 1050	android/media/AudioService$RemoteControlStackEntry:mPlaybackState	I
        //     351: aload_0
        //     352: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     355: astore 8
        //     357: aload 8
        //     359: monitorenter
        //     360: iload_1
        //     361: aload_0
        //     362: getfield 608	android/media/AudioService:mMainRemote	Landroid/media/AudioService$RemotePlaybackState;
        //     365: getfield 854	android/media/AudioService$RemotePlaybackState:mRccId	I
        //     368: if_icmpne +15 -> 383
        //     371: aload_0
        //     372: iload_3
        //     373: invokestatic 1053	android/media/AudioService:isPlaystateActive	(I)Z
        //     376: putfield 612	android/media/AudioService:mMainRemoteIsActive	Z
        //     379: aload_0
        //     380: invokespecial 615	android/media/AudioService:postReevaluateRemote	()V
        //     383: aload 8
        //     385: monitorexit
        //     386: goto -242 -> 144
        //     389: astore 9
        //     391: aload 8
        //     393: monitorexit
        //     394: aload 9
        //     396: athrow
        //     397: aload 4
        //     399: monitorexit
        //     400: return
        //
        // Exception table:
        //     from	to	target	type
        //     9	168	163	finally
        //     171	186	163	finally
        //     223	241	163	finally
        //     278	296	163	finally
        //     333	360	163	finally
        //     394	400	163	finally
        //     186	223	218	finally
        //     241	278	273	finally
        //     296	333	328	finally
        //     360	394	389	finally
    }

    private void onRcDisplayClear()
    {
        synchronized (this.mRCStack)
        {
            synchronized (this.mCurrentRcLock)
            {
                this.mCurrentRcClientGen = (1 + this.mCurrentRcClientGen);
                setNewRcClient_syncRcsCurrc(this.mCurrentRcClientGen, null, true);
                return;
            }
        }
    }

    private void onRcDisplayUpdate(RemoteControlStackEntry paramRemoteControlStackEntry, int paramInt)
    {
        synchronized (this.mRCStack)
        {
            synchronized (this.mCurrentRcLock)
            {
                if ((this.mCurrentRcClient != null) && (this.mCurrentRcClient.equals(paramRemoteControlStackEntry.mRcClient)))
                {
                    this.mCurrentRcClientGen = (1 + this.mCurrentRcClientGen);
                    setNewRcClient_syncRcsCurrc(this.mCurrentRcClientGen, paramRemoteControlStackEntry.mMediaIntent, false);
                }
                try
                {
                    this.mCurrentRcClient.onInformationRequested(this.mCurrentRcClientGen, paramInt, this.mArtworkExpectedWidth, this.mArtworkExpectedHeight);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        Log.e("AudioService", "Current valid remote client is dead: " + localRemoteException);
                        this.mCurrentRcClient = null;
                    }
                }
            }
        }
    }

    private void onReevaluateRemote()
    {
        boolean bool = false;
        synchronized (this.mRCStack)
        {
            Iterator localIterator = this.mRCStack.iterator();
            while (localIterator.hasNext())
                if (((RemoteControlStackEntry)localIterator.next()).mPlaybackType == 1)
                    bool = true;
        }
        synchronized (this.mMainRemote)
        {
            if (this.mHasRemotePlayback != bool)
            {
                this.mHasRemotePlayback = bool;
                this.mVolumePanel.postRemoteSliderVisibility(bool);
            }
            return;
            localObject1 = finally;
            throw localObject1;
        }
    }

    private void onRegisterVolumeObserverForRcc(int paramInt, IRemoteVolumeObserver paramIRemoteVolumeObserver)
    {
        synchronized (this.mRCStack)
        {
            Iterator localIterator = this.mRCStack.iterator();
            while (localIterator.hasNext())
            {
                RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                if (localRemoteControlStackEntry.mRccId == paramInt)
                    localRemoteControlStackEntry.mRemoteVolumeObs = paramIRemoteVolumeObserver;
            }
            return;
        }
    }

    private void onSetA2dpConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        int i = 1;
        if (paramBluetoothDevice == null)
            return;
        String str1 = paramBluetoothDevice.getAddress();
        if (!BluetoothAdapter.checkBluetoothAddress(str1))
            str1 = "";
        synchronized (this.mConnectedDevices)
        {
            if ((this.mConnectedDevices.containsKey(Integer.valueOf(128))) && (((String)this.mConnectedDevices.get(Integer.valueOf(128))).equals(str1)))
            {
                label75: if ((i == 0) || (paramInt == 2))
                    break label181;
                if (!paramBluetoothDevice.isBluetoothDock())
                    break label164;
                if (paramInt == 0)
                    makeA2dpDeviceUnavailableLater(str1);
            }
        }
        label164: label181: 
        do
            synchronized (this.mCurAudioRoutes)
            {
                if (this.mCurAudioRoutes.mBluetoothName != null)
                {
                    this.mCurAudioRoutes.mBluetoothName = null;
                    sendMsg(this.mAudioHandler, 16, 1, 0, 0, null, 0);
                }
                break;
                localObject1 = finally;
                throw localObject1;
                i = 0;
                break label75;
                makeA2dpDeviceUnavailableNow(str1);
            }
        while ((i != 0) || (paramInt != 2));
        if (paramBluetoothDevice.isBluetoothDock())
        {
            cancelA2dpDeviceTimeout();
            this.mDockAddress = str1;
        }
        while (true)
        {
            while (true)
            {
                makeA2dpDeviceAvailable(str1);
                synchronized (this.mCurAudioRoutes)
                {
                    String str2 = paramBluetoothDevice.getAliasName();
                    if (!TextUtils.equals(this.mCurAudioRoutes.mBluetoothName, str2))
                    {
                        this.mCurAudioRoutes.mBluetoothName = str2;
                        sendMsg(this.mAudioHandler, 16, 1, 0, 0, null, 0);
                    }
                }
            }
            if (hasScheduledA2dpDockTimeout())
            {
                cancelA2dpDeviceTimeout();
                makeA2dpDeviceUnavailableNow(this.mDockAddress);
            }
        }
    }

    private void onSetWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString)
    {
        int i = 1;
        HashMap localHashMap = this.mConnectedDevices;
        if ((paramInt2 == 0) && ((paramInt1 == 4) || (paramInt1 == 8)));
        while (true)
        {
            try
            {
                setBluetoothA2dpOnInt(true);
                break label85;
                handleDeviceConnection(i, paramInt1, "");
                if ((paramInt2 != 0) && ((paramInt1 == 4) || (paramInt1 == 8)))
                    setBluetoothA2dpOnInt(false);
                sendDeviceConnectionIntent(paramInt1, paramInt2, paramString);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label85: if (paramInt2 != i)
                i = 0;
        }
    }

    private void postReevaluateRemote()
    {
        sendMsg(this.mAudioHandler, 17, 2, 0, 0, null, 0);
    }

    private void pushMediaButtonReceiver(PendingIntent paramPendingIntent, ComponentName paramComponentName)
    {
        if ((!this.mRCStack.empty()) && (((RemoteControlStackEntry)this.mRCStack.peek()).mMediaIntent.equals(paramPendingIntent)));
        while (true)
        {
            return;
            Iterator localIterator = this.mRCStack.iterator();
            RemoteControlStackEntry localRemoteControlStackEntry = null;
            int i = 0;
            while (localIterator.hasNext())
            {
                localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                if (localRemoteControlStackEntry.mMediaIntent.equals(paramPendingIntent))
                {
                    i = 1;
                    localIterator.remove();
                }
            }
            if (i == 0)
                localRemoteControlStackEntry = new RemoteControlStackEntry(paramPendingIntent, paramComponentName);
            this.mRCStack.push(localRemoteControlStackEntry);
            this.mAudioHandler.sendMessage(this.mAudioHandler.obtainMessage(10, 0, 0, paramComponentName));
        }
    }

    private void queueMsgUnderWakeLock(Handler paramHandler, int paramInt1, int paramInt2, int paramInt3, Object paramObject, int paramInt4)
    {
        this.mMediaEventWakeLock.acquire();
        sendMsg(paramHandler, paramInt1, 2, paramInt2, paramInt3, paramObject, paramInt4);
    }

    private void rcDisplay_startDeathMonitor_syncRcStack()
    {
        IBinder localIBinder;
        if (this.mRcDisplay != null)
        {
            localIBinder = this.mRcDisplay.asBinder();
            this.mRcDisplayDeathHandler = new RcDisplayDeathHandler(localIBinder);
        }
        try
        {
            localIBinder.linkToDeath(this.mRcDisplayDeathHandler, 0);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("AudioService", "registerRemoteControlDisplay() has a dead client " + localIBinder);
                this.mRcDisplay = null;
            }
        }
    }

    private void rcDisplay_stopDeathMonitor_syncRcStack()
    {
        if (this.mRcDisplay != null)
            this.mRcDisplayDeathHandler.unlinkToRcDisplayDeath();
    }

    private void readPersistedSettings()
    {
        int i = 2;
        ContentResolver localContentResolver = this.mContentResolver;
        int j = Settings.System.getInt(localContentResolver, "mode_ringer", i);
        int k = j;
        if (!AudioManager.isValidRingerMode(k))
            k = 2;
        if ((k == 1) && (!this.mHasVibrator))
            k = 0;
        if (k != j)
            Settings.System.putInt(localContentResolver, "mode_ringer", k);
        while (true)
        {
            synchronized (this.mSettingsLock)
            {
                this.mRingerMode = k;
                if (this.mHasVibrator)
                {
                    m = i;
                    this.mVibrateSetting = getValueForVibrateSetting(0, 1, m);
                    int n = this.mVibrateSetting;
                    if (!this.mHasVibrator)
                        break label244;
                    this.mVibrateSetting = getValueForVibrateSetting(n, 0, i);
                    this.mRingerModeAffectedStreams = Settings.System.getInt(localContentResolver, "mode_ringer_streams_affected", 166);
                    if (!this.mVoiceCapable)
                        break label249;
                    this.mRingerModeAffectedStreams = (0xFFFFFFF7 & this.mRingerModeAffectedStreams);
                    Settings.System.putInt(localContentResolver, "mode_ringer_streams_affected", this.mRingerModeAffectedStreams);
                    this.mMuteAffectedStreams = Settings.System.getInt(localContentResolver, "mute_streams_affected", 14);
                    if (Settings.System.getInt(localContentResolver, "volume_master_mute", 0) != 1)
                        break label263;
                    bool = true;
                    AudioSystem.setMasterMute(bool);
                    broadcastMasterMuteStatus(bool);
                    broadcastRingerMode(k);
                    broadcastVibrateSetting(0);
                    broadcastVibrateSetting(1);
                    restoreMediaButtonReceiver();
                    return;
                }
            }
            int m = 0;
            continue;
            label244: i = 0;
            continue;
            label249: this.mRingerModeAffectedStreams = (0x8 | this.mRingerModeAffectedStreams);
            continue;
            label263: boolean bool = false;
        }
    }

    private void removeFocusStackEntry(String paramString, boolean paramBoolean)
    {
        if ((!this.mFocusStack.empty()) && (((FocusStackEntry)this.mFocusStack.peek()).mClientId.equals(paramString)))
        {
            ((FocusStackEntry)this.mFocusStack.pop()).unlinkToDeath();
            if (paramBoolean)
            {
                notifyTopOfAudioFocusStack();
                synchronized (this.mRCStack)
                {
                    checkUpdateRemoteControlDisplay_syncAfRcs(15);
                }
            }
        }
        else
        {
            Iterator localIterator = this.mFocusStack.iterator();
            while (localIterator.hasNext())
            {
                FocusStackEntry localFocusStackEntry = (FocusStackEntry)localIterator.next();
                if (localFocusStackEntry.mClientId.equals(paramString))
                {
                    Log.i("AudioService", " AudioFocus    abandonAudioFocus(): removing entry for " + localFocusStackEntry.mClientId);
                    localIterator.remove();
                    localFocusStackEntry.unlinkToDeath();
                }
            }
        }
    }

    private void removeFocusStackEntryForClient(IBinder paramIBinder)
    {
        if ((!this.mFocusStack.isEmpty()) && (((FocusStackEntry)this.mFocusStack.peek()).mSourceRef.equals(paramIBinder)));
        for (int i = 1; ; i = 0)
        {
            Iterator localIterator = this.mFocusStack.iterator();
            while (localIterator.hasNext())
            {
                FocusStackEntry localFocusStackEntry = (FocusStackEntry)localIterator.next();
                if (localFocusStackEntry.mSourceRef.equals(paramIBinder))
                {
                    Log.i("AudioService", " AudioFocus    abandonAudioFocus(): removing entry for " + localFocusStackEntry.mClientId);
                    localIterator.remove();
                }
            }
        }
        if (i != 0)
        {
            notifyTopOfAudioFocusStack();
            synchronized (this.mRCStack)
            {
                checkUpdateRemoteControlDisplay_syncAfRcs(15);
            }
        }
    }

    private void removeMediaButtonReceiver(PendingIntent paramPendingIntent)
    {
        Iterator localIterator = this.mRCStack.iterator();
        while (localIterator.hasNext())
        {
            RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
            if (localRemoteControlStackEntry.mMediaIntent.equals(paramPendingIntent))
            {
                localIterator.remove();
                localRemoteControlStackEntry.unlinkToRcClientDeath();
            }
        }
    }

    // ERROR //
    private void removeMediaButtonReceiverForPackage(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     11: invokevirtual 1128	java/util/Stack:empty	()Z
        //     14: ifeq +8 -> 22
        //     17: aload_2
        //     18: monitorexit
        //     19: goto +163 -> 182
        //     22: aload_0
        //     23: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     26: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     29: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     32: astore 4
        //     34: aload_0
        //     35: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     38: invokevirtual 1044	java/util/Stack:iterator	()Ljava/util/Iterator;
        //     41: astore 5
        //     43: aload 5
        //     45: invokeinterface 1007 1 0
        //     50: ifeq +50 -> 100
        //     53: aload 5
        //     55: invokeinterface 1010 1 0
        //     60: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     63: astore 8
        //     65: aload_1
        //     66: aload 8
        //     68: getfield 1259	android/media/AudioService$RemoteControlStackEntry:mReceiverComponent	Landroid/content/ComponentName;
        //     71: invokevirtual 1156	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     74: invokevirtual 1656	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     77: ifeq -34 -> 43
        //     80: aload 5
        //     82: invokeinterface 1566 1 0
        //     87: aload 8
        //     89: invokevirtual 1653	android/media/AudioService$RemoteControlStackEntry:unlinkToRcClientDeath	()V
        //     92: goto -49 -> 43
        //     95: astore_3
        //     96: aload_2
        //     97: monitorexit
        //     98: aload_3
        //     99: athrow
        //     100: aload_0
        //     101: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     104: invokevirtual 1128	java/util/Stack:empty	()Z
        //     107: ifeq +28 -> 135
        //     110: aload_0
        //     111: getfield 627	android/media/AudioService:mAudioHandler	Landroid/media/AudioService$AudioHandler;
        //     114: aload_0
        //     115: getfield 627	android/media/AudioService:mAudioHandler	Landroid/media/AudioService$AudioHandler;
        //     118: bipush 10
        //     120: iconst_0
        //     121: iconst_0
        //     122: aconst_null
        //     123: invokevirtual 1574	android/media/AudioService$AudioHandler:obtainMessage	(IIILjava/lang/Object;)Landroid/os/Message;
        //     126: invokevirtual 1080	android/media/AudioService$AudioHandler:sendMessage	(Landroid/os/Message;)Z
        //     129: pop
        //     130: aload_2
        //     131: monitorexit
        //     132: goto +50 -> 182
        //     135: aload 4
        //     137: aload_0
        //     138: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     141: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     144: if_acmpeq -14 -> 130
        //     147: aload_0
        //     148: getfield 627	android/media/AudioService:mAudioHandler	Landroid/media/AudioService$AudioHandler;
        //     151: aload_0
        //     152: getfield 627	android/media/AudioService:mAudioHandler	Landroid/media/AudioService$AudioHandler;
        //     155: bipush 10
        //     157: iconst_0
        //     158: iconst_0
        //     159: aload_0
        //     160: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     163: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     166: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     169: getfield 1259	android/media/AudioService$RemoteControlStackEntry:mReceiverComponent	Landroid/content/ComponentName;
        //     172: invokevirtual 1574	android/media/AudioService$AudioHandler:obtainMessage	(IIILjava/lang/Object;)Landroid/os/Message;
        //     175: invokevirtual 1080	android/media/AudioService$AudioHandler:sendMessage	(Landroid/os/Message;)Z
        //     178: pop
        //     179: goto -49 -> 130
        //     182: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	98	95	finally
        //     100	179	95	finally
    }

    private int rescaleIndex(int paramInt1, int paramInt2, int paramInt3)
    {
        return (paramInt1 * this.mStreamStates[paramInt3].getMaxIndex() + this.mStreamStates[paramInt2].getMaxIndex() / 2) / this.mStreamStates[paramInt2].getMaxIndex();
    }

    private void resetBluetoothSco()
    {
        synchronized (this.mScoClients)
        {
            clearAllScoClients(0, false);
            this.mScoAudioState = 0;
            broadcastScoConnectionState(0);
            return;
        }
    }

    private void restoreMasterVolume()
    {
        if (this.mUseMasterVolume)
        {
            float f = Settings.System.getFloat(this.mContentResolver, "volume_master", -1.0F);
            if (f >= 0.0F)
                AudioSystem.setMasterVolume(f);
        }
    }

    private void restoreMediaButtonReceiver()
    {
        String str = Settings.System.getString(this.mContentResolver, "media_button_receiver");
        if ((str != null) && (!str.isEmpty()))
        {
            ComponentName localComponentName = ComponentName.unflattenFromString(str);
            Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON");
            localIntent.setComponent(localComponentName);
            registerMediaButtonIntent(PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0), localComponentName);
        }
    }

    private void sendBecomingNoisyIntent()
    {
        this.mContext.sendBroadcast(new Intent("android.media.AUDIO_BECOMING_NOISY"));
    }

    private void sendDeviceConnectionIntent(int paramInt1, int paramInt2, String paramString)
    {
        Intent localIntent = new Intent();
        localIntent.putExtra("state", paramInt2);
        localIntent.putExtra("name", paramString);
        localIntent.addFlags(1073741824);
        int i = 0;
        if (paramInt1 == 4)
        {
            i = 1;
            localIntent.setAction("android.intent.action.HEADSET_PLUG");
            localIntent.putExtra("microphone", 1);
        }
        while (true)
        {
            AudioRoutesInfo localAudioRoutesInfo = this.mCurAudioRoutes;
            if (i != 0);
            try
            {
                int j = this.mCurAudioRoutes.mMainType;
                if (paramInt2 != 0);
                for (int k = j | i; ; k = j & (i ^ 0xFFFFFFFF))
                {
                    if (k != this.mCurAudioRoutes.mMainType)
                    {
                        this.mCurAudioRoutes.mMainType = k;
                        sendMsg(this.mAudioHandler, 16, 1, 0, 0, null, 0);
                    }
                    ActivityManagerNative.broadcastStickyIntent(localIntent, null);
                    return;
                    if (paramInt1 == 8)
                    {
                        i = 2;
                        localIntent.setAction("android.intent.action.HEADSET_PLUG");
                        localIntent.putExtra("microphone", 0);
                        break;
                    }
                    if (paramInt1 == 2048)
                    {
                        i = 4;
                        localIntent.setAction("android.intent.action.ANALOG_AUDIO_DOCK_PLUG");
                        break;
                    }
                    if (paramInt1 == 4096)
                    {
                        i = 4;
                        localIntent.setAction("android.intent.action.DIGITAL_AUDIO_DOCK_PLUG");
                        break;
                    }
                    if (paramInt1 != 1024)
                        break;
                    i = 8;
                    localIntent.setAction("android.intent.action.HDMI_AUDIO_PLUG");
                    break;
                }
            }
            finally
            {
            }
        }
    }

    private void sendMasterMuteUpdate(boolean paramBoolean, int paramInt)
    {
        this.mVolumePanel.postMasterMuteChanged(paramInt);
        broadcastMasterMuteStatus(paramBoolean);
    }

    private void sendMasterVolumeUpdate(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mVolumePanel.postMasterVolumeChanged(paramInt1);
        Intent localIntent = new Intent("android.media.MASTER_VOLUME_CHANGED_ACTION");
        localIntent.putExtra("android.media.EXTRA_PREV_MASTER_VOLUME_VALUE", paramInt2);
        localIntent.putExtra("android.media.EXTRA_MASTER_VOLUME_VALUE", paramInt3);
        this.mContext.sendBroadcast(localIntent);
    }

    private static void sendMsg(Handler paramHandler, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Object paramObject, int paramInt5)
    {
        if (paramInt2 == 0)
            paramHandler.removeMessages(paramInt1);
        while (true)
        {
            paramHandler.sendMessageDelayed(paramHandler.obtainMessage(paramInt1, paramInt3, paramInt4, paramObject), paramInt5);
            while (true)
            {
                return;
                if ((paramInt2 != 1) || (!paramHandler.hasMessages(paramInt1)))
                    break;
            }
        }
    }

    private void sendSimulatedMediaButtonEvent(KeyEvent paramKeyEvent, boolean paramBoolean)
    {
        dispatchMediaKeyEvent(KeyEvent.changeAction(paramKeyEvent, 0), paramBoolean);
        dispatchMediaKeyEvent(KeyEvent.changeAction(paramKeyEvent, 1), paramBoolean);
    }

    private void sendVolumeUpdate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((!this.mVoiceCapable) && (paramInt1 == 2))
            paramInt1 = 5;
        this.mVolumePanel.postVolumeChanged(paramInt1, paramInt4);
        int i = (paramInt2 + 5) / 10;
        int j = (paramInt3 + 5) / 10;
        Intent localIntent = new Intent("android.media.VOLUME_CHANGED_ACTION");
        localIntent.putExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", paramInt1);
        localIntent.putExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", j);
        localIntent.putExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", i);
        this.mContext.sendBroadcast(localIntent);
    }

    private void sendVolumeUpdateToRemote(int paramInt1, int paramInt2)
    {
        if (paramInt2 == 0);
        while (true)
        {
            return;
            IRemoteVolumeObserver localIRemoteVolumeObserver = null;
            synchronized (this.mRCStack)
            {
                Iterator localIterator = this.mRCStack.iterator();
                while (localIterator.hasNext())
                {
                    RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                    if (localRemoteControlStackEntry.mRccId == paramInt1)
                        localIRemoteVolumeObserver = localRemoteControlStackEntry.mRemoteVolumeObs;
                }
                if (localIRemoteVolumeObserver == null)
                    continue;
                try
                {
                    localIRemoteVolumeObserver.dispatchRemoteVolumeUpdate(paramInt2, -1);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("AudioService", "Error dispatching relative volume update", localRemoteException);
                }
            }
        }
    }

    private void setNewRcClientGenerationOnClients_syncRcsCurrc(int paramInt)
    {
        Iterator localIterator = this.mRCStack.iterator();
        while (localIterator.hasNext())
        {
            RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
            if ((localRemoteControlStackEntry != null) && (localRemoteControlStackEntry.mRcClient != null))
                try
                {
                    localRemoteControlStackEntry.mRcClient.setCurrentClientGenerationId(paramInt);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("AudioService", "Dead client in setNewRcClientGenerationOnClients_syncRcsCurrc()" + localRemoteException);
                    localIterator.remove();
                    localRemoteControlStackEntry.unlinkToRcClientDeath();
                }
        }
    }

    private void setNewRcClientOnDisplays_syncRcsCurrc(int paramInt, PendingIntent paramPendingIntent, boolean paramBoolean)
    {
        if (this.mRcDisplay != null);
        try
        {
            this.mRcDisplay.setCurrentClientId(paramInt, paramPendingIntent, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("AudioService", "Dead display in setNewRcClientOnDisplays_syncRcsCurrc() " + localRemoteException);
                rcDisplay_stopDeathMonitor_syncRcStack();
                this.mRcDisplay = null;
            }
        }
    }

    private void setNewRcClient_syncRcsCurrc(int paramInt, PendingIntent paramPendingIntent, boolean paramBoolean)
    {
        setNewRcClientOnDisplays_syncRcsCurrc(paramInt, paramPendingIntent, paramBoolean);
        setNewRcClientGenerationOnClients_syncRcsCurrc(paramInt);
    }

    private void setOrientationForAudioSystem()
    {
        switch (this.mDeviceOrientation)
        {
        default:
            Log.e("AudioService", "Unknown orientation");
        case 2:
        case 1:
        case 3:
        case 0:
        }
        while (true)
        {
            return;
            AudioSystem.setParameters("orientation=landscape");
            continue;
            AudioSystem.setParameters("orientation=portrait");
            continue;
            AudioSystem.setParameters("orientation=square");
            continue;
            AudioSystem.setParameters("orientation=undefined");
        }
    }

    // ERROR //
    private void setRingerModeInt(int paramInt, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 310	android/media/AudioService:mSettingsLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: iload_1
        //     9: putfield 718	android/media/AudioService:mRingerMode	I
        //     12: aload_3
        //     13: monitorexit
        //     14: bipush 255
        //     16: invokestatic 949	android/media/AudioSystem:getNumStreamTypes	()I
        //     19: iadd
        //     20: istore 5
        //     22: iload 5
        //     24: iflt +224 -> 248
        //     27: aload_0
        //     28: iload 5
        //     30: invokespecial 1793	android/media/AudioService:isStreamMutedByRingerMode	(I)Z
        //     33: ifeq +173 -> 206
        //     36: aload_0
        //     37: iload 5
        //     39: invokevirtual 1796	android/media/AudioService:isStreamAffectedByRingerMode	(I)Z
        //     42: ifeq +8 -> 50
        //     45: iload_1
        //     46: iconst_2
        //     47: if_icmpne +153 -> 200
        //     50: aload_0
        //     51: getfield 446	android/media/AudioService:mVoiceCapable	Z
        //     54: ifeq +118 -> 172
        //     57: aload_0
        //     58: getfield 715	android/media/AudioService:mStreamVolumeAlias	[I
        //     61: iload 5
        //     63: iaload
        //     64: iconst_2
        //     65: if_icmpne +107 -> 172
        //     68: aload_0
        //     69: getfield 732	android/media/AudioService:mStreamStates	[Landroid/media/AudioService$VolumeStreamState;
        //     72: iload 5
        //     74: aaload
        //     75: astore 6
        //     77: aload 6
        //     79: monitorenter
        //     80: aload_0
        //     81: getfield 732	android/media/AudioService:mStreamStates	[Landroid/media/AudioService$VolumeStreamState;
        //     84: iload 5
        //     86: aaload
        //     87: invokestatic 1800	android/media/AudioService$VolumeStreamState:access$1100	(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/concurrent/ConcurrentHashMap;
        //     90: invokevirtual 1805	java/util/concurrent/ConcurrentHashMap:entrySet	()Ljava/util/Set;
        //     93: invokeinterface 1002 1 0
        //     98: astore 8
        //     100: aload 8
        //     102: invokeinterface 1007 1 0
        //     107: ifeq +62 -> 169
        //     110: aload 8
        //     112: invokeinterface 1010 1 0
        //     117: checkcast 1807	java/util/Map$Entry
        //     120: astore 9
        //     122: aload 9
        //     124: invokeinterface 1810 1 0
        //     129: checkcast 1012	java/lang/Integer
        //     132: invokevirtual 1015	java/lang/Integer:intValue	()I
        //     135: ifne -35 -> 100
        //     138: aload 9
        //     140: bipush 10
        //     142: invokestatic 1415	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     145: invokeinterface 1813 2 0
        //     150: pop
        //     151: goto -51 -> 100
        //     154: astore 7
        //     156: aload 6
        //     158: monitorexit
        //     159: aload 7
        //     161: athrow
        //     162: astore 4
        //     164: aload_3
        //     165: monitorexit
        //     166: aload 4
        //     168: athrow
        //     169: aload 6
        //     171: monitorexit
        //     172: aload_0
        //     173: getfield 732	android/media/AudioService:mStreamStates	[Landroid/media/AudioService$VolumeStreamState;
        //     176: iload 5
        //     178: aaload
        //     179: aconst_null
        //     180: iconst_0
        //     181: invokevirtual 1817	android/media/AudioService$VolumeStreamState:mute	(Landroid/os/IBinder;Z)V
        //     184: aload_0
        //     185: aload_0
        //     186: getfield 514	android/media/AudioService:mRingerModeMutedStreams	I
        //     189: bipush 255
        //     191: iconst_1
        //     192: iload 5
        //     194: ishl
        //     195: ixor
        //     196: iand
        //     197: putfield 514	android/media/AudioService:mRingerModeMutedStreams	I
        //     200: iinc 5 255
        //     203: goto -181 -> 22
        //     206: aload_0
        //     207: iload 5
        //     209: invokevirtual 1796	android/media/AudioService:isStreamAffectedByRingerMode	(I)Z
        //     212: ifeq -12 -> 200
        //     215: iload_1
        //     216: iconst_2
        //     217: if_icmpeq -17 -> 200
        //     220: aload_0
        //     221: getfield 732	android/media/AudioService:mStreamStates	[Landroid/media/AudioService$VolumeStreamState;
        //     224: iload 5
        //     226: aaload
        //     227: aconst_null
        //     228: iconst_1
        //     229: invokevirtual 1817	android/media/AudioService$VolumeStreamState:mute	(Landroid/os/IBinder;Z)V
        //     232: aload_0
        //     233: aload_0
        //     234: getfield 514	android/media/AudioService:mRingerModeMutedStreams	I
        //     237: iconst_1
        //     238: iload 5
        //     240: ishl
        //     241: ior
        //     242: putfield 514	android/media/AudioService:mRingerModeMutedStreams	I
        //     245: goto -45 -> 200
        //     248: iload_2
        //     249: ifeq +18 -> 267
        //     252: aload_0
        //     253: getfield 627	android/media/AudioService:mAudioHandler	Landroid/media/AudioService$AudioHandler;
        //     256: iconst_3
        //     257: iconst_0
        //     258: iconst_0
        //     259: iconst_0
        //     260: aconst_null
        //     261: sipush 500
        //     264: invokestatic 658	android/media/AudioService:sendMsg	(Landroid/os/Handler;IIIILjava/lang/Object;I)V
        //     267: return
        //
        // Exception table:
        //     from	to	target	type
        //     80	159	154	finally
        //     169	172	154	finally
        //     7	14	162	finally
        //     164	166	162	finally
    }

    private void setStreamVolumeInt(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
    {
        VolumeStreamState localVolumeStreamState = this.mStreamStates[paramInt1];
        if (localVolumeStreamState.muteCount() != 0)
            if (paramInt2 != 0)
            {
                localVolumeStreamState.setLastAudibleIndex(paramInt2, paramInt3);
                sendMsg(this.mAudioHandler, 1, 2, 2, paramInt3, localVolumeStreamState, 500);
            }
        while (true)
        {
            return;
            if ((localVolumeStreamState.setIndex(paramInt2, paramInt3, paramBoolean2)) || (paramBoolean1))
                sendMsg(this.mAudioHandler, 0, 2, paramInt3, 0, localVolumeStreamState, 0);
        }
    }

    private void startVoiceBasedInteractions(boolean paramBoolean)
    {
        boolean bool1 = true;
        PowerManager localPowerManager = (PowerManager)this.mContext.getSystemService("power");
        boolean bool2;
        if ((this.mKeyguardManager != null) && (this.mKeyguardManager.isKeyguardLocked()))
            bool2 = bool1;
        while (true)
        {
            Intent localIntent;
            if ((!bool2) && (localPowerManager.isScreenOn()))
            {
                localIntent = new Intent("android.speech.action.WEB_SEARCH");
                if (paramBoolean)
                    this.mMediaEventWakeLock.acquire();
                if (localIntent == null);
            }
            try
            {
                localIntent.setFlags(276824064);
                this.mContext.startActivity(localIntent);
                if (paramBoolean)
                {
                    localWakeLock = this.mMediaEventWakeLock;
                    localWakeLock.release();
                }
                return;
                bool2 = false;
                continue;
                localIntent = new Intent("android.speech.action.VOICE_SEARCH_HANDS_FREE");
                if ((bool2) && (this.mKeyguardManager.isKeyguardSecure()));
                while (true)
                {
                    localIntent.putExtra("android.speech.extras.EXTRA_SECURE", bool1);
                    break;
                    bool1 = false;
                }
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
                while (true)
                {
                    PowerManager.WakeLock localWakeLock;
                    Log.w("AudioService", "No activity for search: " + localActivityNotFoundException);
                    if (paramBoolean)
                        localWakeLock = this.mMediaEventWakeLock;
                }
            }
            finally
            {
                if (paramBoolean)
                    this.mMediaEventWakeLock.release();
            }
        }
    }

    private void updateRemoteControlDisplay_syncAfRcs(int paramInt)
    {
        RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)this.mRCStack.peek();
        int i = paramInt;
        if (localRemoteControlStackEntry.mRcClient == null)
            clearRemoteControlDisplay_syncAfRcs();
        while (true)
        {
            return;
            synchronized (this.mCurrentRcLock)
            {
                if (!localRemoteControlStackEntry.mRcClient.equals(this.mCurrentRcClient))
                    i = 15;
                this.mCurrentRcClient = localRemoteControlStackEntry.mRcClient;
                this.mAudioHandler.sendMessage(this.mAudioHandler.obtainMessage(13, i, 0, localRemoteControlStackEntry));
            }
        }
    }

    private void updateStreamVolumeAlias(boolean paramBoolean)
    {
        if (this.mVoiceCapable)
            this.mStreamVolumeAlias = this.STREAM_VOLUME_ALIAS;
        for (int i = 2; ; i = 3)
        {
            if (isInCommunication())
                i = 0;
            this.mStreamVolumeAlias[8] = i;
            if (paramBoolean)
            {
                this.mStreamStates[8].setAllIndexes(this.mStreamStates[i], false);
                this.mStreamStates[8].setAllIndexes(this.mStreamStates[i], true);
                sendMsg(this.mAudioHandler, 14, 2, 0, 0, this.mStreamStates[8], 0);
            }
            return;
            this.mStreamVolumeAlias = this.STREAM_VOLUME_ALIAS_NON_VOICE;
        }
    }

    private void waitForAudioHandlerCreation()
    {
        try
        {
            while (true)
            {
                AudioHandler localAudioHandler = this.mAudioHandler;
                if (localAudioHandler != null)
                    break;
                try
                {
                    wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                    Log.e("AudioService", "Interrupted while waiting on volume handler.");
                }
            }
        }
        finally
        {
        }
    }

    public int abandonAudioFocus(IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString)
    {
        Log.i("AudioService", " AudioFocus    abandonAudioFocus() from " + paramString);
        try
        {
            synchronized (mAudioFocusLock)
            {
                removeFocusStackEntry(paramString, true);
            }
        }
        catch (ConcurrentModificationException localConcurrentModificationException)
        {
            Log.e("AudioService", "FATAL EXCEPTION AudioFocus    abandonAudioFocus() caused " + localConcurrentModificationException);
            localConcurrentModificationException.printStackTrace();
        }
        return 1;
    }

    public void adjustLocalOrRemoteStreamVolume(int paramInt1, int paramInt2)
    {
        if (checkUpdateRemoteStateIfActive(3))
            adjustRemoteVolume(3, paramInt2, 0);
        while (true)
        {
            return;
            if (AudioSystem.isStreamActive(3, 0))
                adjustStreamVolume(3, paramInt2, 0);
        }
    }

    public void adjustMasterVolume(int paramInt1, int paramInt2)
    {
        ensureValidSteps(paramInt1);
        int i = Math.round(100.0F * AudioSystem.getMasterVolume());
        int j = Math.abs(paramInt1);
        if (paramInt1 > 0);
        for (int k = 1; ; k = -1)
            for (int m = 0; m < j; m++)
                i += findVolumeDelta(k, i);
        setMasterVolume(i, paramInt2);
    }

    public void adjustStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        ensureValidDirection(paramInt2);
        ensureValidStreamType(paramInt1);
        int i = this.mStreamVolumeAlias[paramInt1];
        VolumeStreamState localVolumeStreamState1 = this.mStreamStates[i];
        int j = getDeviceForStream(i);
        boolean bool1;
        boolean bool2;
        int m;
        boolean bool3;
        label157: int n;
        if (localVolumeStreamState1.muteCount() != 0)
        {
            bool1 = true;
            int k = localVolumeStreamState1.getIndex(j, bool1);
            bool2 = true;
            m = rescaleIndex(10, paramInt1, i);
            if (((paramInt3 & 0x2) != 0) || (i == getMasterStreamType()))
            {
                if (getRingerMode() == 1)
                    paramInt3 &= -17;
                bool2 = checkForRingerModeChange(k, paramInt2, m);
                if ((i == getMasterStreamType()) && (this.mRingerMode == 0))
                    localVolumeStreamState1.setLastAudibleIndex(0, j);
            }
            VolumeStreamState localVolumeStreamState2 = this.mStreamStates[paramInt1];
            if (this.mStreamStates[paramInt1].muteCount() == 0)
                break label240;
            bool3 = true;
            n = localVolumeStreamState2.getIndex(j, bool3);
            if (localVolumeStreamState1.muteCount() == 0)
                break label246;
            if (bool2)
            {
                localVolumeStreamState1.adjustLastAudibleIndex(paramInt2 * m, j);
                sendMsg(this.mAudioHandler, 1, 2, 2, j, localVolumeStreamState1, 500);
            }
        }
        for (int i1 = this.mStreamStates[paramInt1].getIndex(j, true); ; i1 = this.mStreamStates[paramInt1].getIndex(j, false))
        {
            sendVolumeUpdate(paramInt1, n, i1, paramInt3);
            return;
            bool1 = false;
            break;
            label240: bool3 = false;
            break label157;
            label246: if ((bool2) && (localVolumeStreamState1.adjustIndex(paramInt2 * m, j)))
                sendMsg(this.mAudioHandler, 0, 2, j, 0, localVolumeStreamState1, 0);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void adjustSuggestedStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        int i;
        int j;
        if (this.mVolumeControlStream != -1)
        {
            i = this.mVolumeControlStream;
            if ((i != -200) && ((paramInt3 & 0x4) != 0) && ((this.mStreamVolumeAlias[i] != 2) || ((this.mKeyguardManager != null) && (this.mKeyguardManager.isKeyguardLocked()))))
                paramInt3 &= -5;
            j = Injector.adjustDirection(this.mVolumePanel, paramInt3, paramInt1);
            if (i != -200)
                break label103;
            adjustRemoteVolume(3, j, paramInt3 & 0xFFFFFFFB);
        }
        while (true)
        {
            return;
            i = getActiveStreamType(paramInt2);
            break;
            label103: adjustStreamVolume(i, j, paramInt3);
        }
    }

    public void adjustVolume(int paramInt1, int paramInt2)
    {
        adjustSuggestedStreamVolume(paramInt1, -2147483648, paramInt2);
    }

    boolean checkAudioSettingsPermission(String paramString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.MODIFY_AUDIO_SETTINGS") == 0);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            Log.w("AudioService", "Audio Settings Permission Denial: " + paramString + " from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        }
    }

    public void clearAllScoClients(int paramInt, boolean paramBoolean)
    {
        ArrayList localArrayList = this.mScoClients;
        Object localObject1 = null;
        int j;
        ScoClient localScoClient;
        try
        {
            int i = this.mScoClients.size();
            j = 0;
            if (j < i)
            {
                localScoClient = (ScoClient)this.mScoClients.get(j);
                if (localScoClient.getPid() == paramInt)
                    break label99;
                localScoClient.clearCount(paramBoolean);
            }
            else
            {
                this.mScoClients.clear();
                if (localObject1 != null)
                    this.mScoClients.add(localObject1);
                return;
            }
        }
        finally
        {
            localObject2 = finally;
            throw localObject2;
        }
        while (true)
        {
            j++;
            break;
            label99: localObject1 = localScoClient;
        }
    }

    public void dispatchMediaKeyEvent(KeyEvent paramKeyEvent)
    {
        filterMediaKeyEvent(paramKeyEvent, false);
    }

    public void dispatchMediaKeyEventUnderWakelock(KeyEvent paramKeyEvent)
    {
        filterMediaKeyEvent(paramKeyEvent, true);
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "AudioService");
        dumpFocusStack(paramPrintWriter);
        dumpRCStack(paramPrintWriter);
        dumpRCCStack(paramPrintWriter);
        dumpStreamStates(paramPrintWriter);
        paramPrintWriter.println("\nAudio routes:");
        paramPrintWriter.print("    mMainType=0x");
        paramPrintWriter.println(Integer.toHexString(this.mCurAudioRoutes.mMainType));
        paramPrintWriter.print("    mBluetoothName=");
        paramPrintWriter.println(this.mCurAudioRoutes.mBluetoothName);
    }

    public void forceVolumeControlStream(int paramInt, IBinder paramIBinder)
    {
        synchronized (this.mForceControlStreamLock)
        {
            this.mVolumeControlStream = paramInt;
            if (this.mVolumeControlStream == -1)
            {
                if (this.mForceControlStreamClient != null)
                {
                    this.mForceControlStreamClient.release();
                    this.mForceControlStreamClient = null;
                }
                return;
            }
            this.mForceControlStreamClient = new ForceControlStreamClient(paramIBinder);
        }
    }

    public int getLastAudibleMasterVolume()
    {
        return Math.round(100.0F * AudioSystem.getMasterVolume());
    }

    public int getLastAudibleStreamVolume(int paramInt)
    {
        ensureValidStreamType(paramInt);
        int i = getDeviceForStream(paramInt);
        return (5 + this.mStreamStates[paramInt].getIndex(i, true)) / 10;
    }

    public int getMasterMaxVolume()
    {
        return 100;
    }

    public int getMasterStreamType()
    {
        if (this.mVoiceCapable);
        for (int i = 2; ; i = 3)
            return i;
    }

    public int getMasterVolume()
    {
        if (isMasterMute());
        for (int i = 0; ; i = getLastAudibleMasterVolume())
            return i;
    }

    public int getMode()
    {
        return this.mMode;
    }

    public int getRemoteStreamMaxVolume()
    {
        int i;
        synchronized (this.mMainRemote)
        {
            if (this.mMainRemote.mRccId == -1)
                i = 0;
            else
                i = this.mMainRemote.mVolumeMax;
        }
        return i;
    }

    public int getRemoteStreamVolume()
    {
        int i;
        synchronized (this.mMainRemote)
        {
            if (this.mMainRemote.mRccId == -1)
                i = 0;
            else
                i = this.mMainRemote.mVolume;
        }
        return i;
    }

    public int getRingerMode()
    {
        synchronized (this.mSettingsLock)
        {
            int i = this.mRingerMode;
            return i;
        }
    }

    public IRingtonePlayer getRingtonePlayer()
    {
        return this.mRingtonePlayer;
    }

    public int getStreamMaxVolume(int paramInt)
    {
        ensureValidStreamType(paramInt);
        return (5 + this.mStreamStates[paramInt].getMaxIndex()) / 10;
    }

    public int getStreamVolume(int paramInt)
    {
        ensureValidStreamType(paramInt);
        int i = getDeviceForStream(paramInt);
        return (5 + this.mStreamStates[paramInt].getIndex(i, false)) / 10;
    }

    public int getVibrateSetting(int paramInt)
    {
        if (!this.mHasVibrator);
        for (int i = 0; ; i = 0x3 & this.mVibrateSetting >> paramInt * 2)
            return i;
    }

    public boolean isBluetoothA2dpOn()
    {
        synchronized (this.mBluetoothA2dpEnabledLock)
        {
            boolean bool = this.mBluetoothA2dpEnabled;
            return bool;
        }
    }

    public boolean isBluetoothScoOn()
    {
        if (this.mForcedUseForComm == 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isMasterMute()
    {
        return AudioSystem.getMasterMute();
    }

    public boolean isSpeakerphoneOn()
    {
        int i = 1;
        if (this.mForcedUseForComm == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean isStreamAffectedByMute(int paramInt)
    {
        int i = 1;
        if ((this.mMuteAffectedStreams & i << paramInt) != 0);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public boolean isStreamAffectedByRingerMode(int paramInt)
    {
        int i = 1;
        if ((this.mRingerModeAffectedStreams & i << paramInt) != 0);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public boolean isStreamMute(int paramInt)
    {
        if (this.mStreamStates[paramInt].muteCount() != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean loadSoundEffects()
    {
        while (true)
        {
            boolean bool;
            int[] arrayOfInt;
            int i;
            String str;
            int i3;
            int i2;
            StringBuilder localStringBuilder;
            int i1;
            synchronized (this.mSoundEffectsLock)
            {
                if (!this.mBootCompleted)
                {
                    Log.w("AudioService", "loadSoundEffects() called before boot complete");
                    bool = false;
                }
                else if (this.mSoundPool != null)
                {
                    bool = true;
                }
            }
            int j = 0;
            int k = 0;
            continue;
            k++;
            continue;
            i1++;
            continue;
            int m = -1;
            continue;
            int n = -1;
        }
    }

    public void onSendFinished(PendingIntent paramPendingIntent, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle)
    {
        if (paramInt == 1980)
            this.mMediaEventWakeLock.release();
    }

    public void playSoundEffect(int paramInt)
    {
        sendMsg(this.mAudioHandler, 6, 1, paramInt, -1, null, 0);
    }

    public void playSoundEffectVolume(int paramInt, float paramFloat)
    {
        loadSoundEffects();
        sendMsg(this.mAudioHandler, 6, 1, paramInt, (int)(1000.0F * paramFloat), null, 0);
    }

    public void registerMediaButtonEventReceiverForCalls(ComponentName paramComponentName)
    {
        if (this.mContext.checkCallingPermission("android.permission.MODIFY_PHONE_STATE") != 0)
            Log.e("AudioService", "Invalid permissions to register media button receiver for calls");
        while (true)
        {
            return;
            synchronized (this.mRCStack)
            {
                this.mMediaReceiverForCalls = paramComponentName;
            }
        }
    }

    public void registerMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
    {
        Log.i("AudioService", "    Remote Control     registerMediaButtonIntent() for " + paramPendingIntent);
        synchronized (mAudioFocusLock)
        {
            synchronized (this.mRCStack)
            {
                pushMediaButtonReceiver(paramPendingIntent, paramComponentName);
                checkUpdateRemoteControlDisplay_syncAfRcs(15);
                return;
            }
        }
    }

    public int registerRemoteControlClient(PendingIntent paramPendingIntent, IRemoteControlClient paramIRemoteControlClient, String paramString)
    {
        int i = -1;
        while (true)
        {
            RemoteControlStackEntry localRemoteControlStackEntry;
            synchronized (mAudioFocusLock)
            {
                synchronized (this.mRCStack)
                {
                    Iterator localIterator = this.mRCStack.iterator();
                    if (localIterator.hasNext())
                    {
                        localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                        if (!localRemoteControlStackEntry.mMediaIntent.equals(paramPendingIntent))
                            continue;
                        if (localRemoteControlStackEntry.mRcClientDeathHandler != null)
                            localRemoteControlStackEntry.unlinkToRcClientDeath();
                        localRemoteControlStackEntry.mRcClient = paramIRemoteControlClient;
                        localRemoteControlStackEntry.mCallingPackageName = paramString;
                        localRemoteControlStackEntry.mCallingUid = Binder.getCallingUid();
                        if (paramIRemoteControlClient != null)
                            continue;
                        localRemoteControlStackEntry.resetPlaybackInfo();
                    }
                    if (isCurrentRcController(paramPendingIntent))
                        checkUpdateRemoteControlDisplay_syncAfRcs(15);
                    return i;
                    i = localRemoteControlStackEntry.mRccId;
                    IRemoteControlDisplay localIRemoteControlDisplay = this.mRcDisplay;
                    if (localIRemoteControlDisplay == null);
                }
            }
            try
            {
                localRemoteControlStackEntry.mRcClient.plugRemoteControlDisplay(this.mRcDisplay);
                localIBinder = localRemoteControlStackEntry.mRcClient.asBinder();
                localRcClientDeathHandler = new RcClientDeathHandler(localIBinder, localRemoteControlStackEntry.mMediaIntent);
            }
            catch (RemoteException localRemoteException2)
            {
                try
                {
                    RcClientDeathHandler localRcClientDeathHandler;
                    localIBinder.linkToDeath(localRcClientDeathHandler, 0);
                    localRemoteControlStackEntry.mRcClientDeathHandler = localRcClientDeathHandler;
                    continue;
                    localObject3 = finally;
                    throw localObject3;
                    localObject2 = finally;
                    throw localObject2;
                    localRemoteException2 = localRemoteException2;
                    Log.e("AudioService", "Error connecting remote control display to client: " + localRemoteException2);
                    localRemoteException2.printStackTrace();
                }
                catch (RemoteException localRemoteException1)
                {
                    while (true)
                    {
                        IBinder localIBinder;
                        Log.w("AudioService", "registerRemoteControlClient() has a dead client " + localIBinder);
                        localRemoteControlStackEntry.mRcClient = null;
                    }
                }
            }
        }
    }

    public void registerRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
    {
        while (true)
        {
            synchronized (mAudioFocusLock)
            {
                synchronized (this.mRCStack)
                {
                    if ((this.mRcDisplay == paramIRemoteControlDisplay) || (paramIRemoteControlDisplay == null))
                        return;
                    rcDisplay_stopDeathMonitor_syncRcStack();
                    this.mRcDisplay = paramIRemoteControlDisplay;
                    rcDisplay_startDeathMonitor_syncRcStack();
                    Iterator localIterator = this.mRCStack.iterator();
                    if (localIterator.hasNext())
                    {
                        RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                        IRemoteControlClient localIRemoteControlClient = localRemoteControlStackEntry.mRcClient;
                        if (localIRemoteControlClient == null)
                            continue;
                        try
                        {
                            localRemoteControlStackEntry.mRcClient.plugRemoteControlDisplay(this.mRcDisplay);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            Log.e("AudioService", "Error connecting remote control display to client: " + localRemoteException);
                            localRemoteException.printStackTrace();
                        }
                    }
                }
            }
            checkUpdateRemoteControlDisplay_syncAfRcs(15);
        }
    }

    public void registerRemoteVolumeObserverForRcc(int paramInt, IRemoteVolumeObserver paramIRemoteVolumeObserver)
    {
        sendMsg(this.mAudioHandler, 19, 2, paramInt, 0, paramIRemoteVolumeObserver, 0);
    }

    public void reloadAudioSettings()
    {
        readPersistedSettings();
        int i = AudioSystem.getNumStreamTypes();
        int j = 0;
        while (true)
        {
            if (j < i);
            synchronized (this.mStreamStates[j])
            {
                ???.readSettings();
                if ((???.muteCount() != 0) && (!isStreamAffectedByMute(j)))
                {
                    int k = ???.mDeathHandlers.size();
                    for (int m = 0; m < k; m++)
                    {
                        AudioService.VolumeStreamState.VolumeDeathHandler.access$2002((AudioService.VolumeStreamState.VolumeDeathHandler)???.mDeathHandlers.get(m), 1);
                        ((AudioService.VolumeStreamState.VolumeDeathHandler)???.mDeathHandlers.get(m)).mute(false);
                    }
                }
                j++;
            }
        }
        setRingerModeInt(getRingerMode(), false);
    }

    public void remoteControlDisplayUsesBitmapSize(IRemoteControlDisplay paramIRemoteControlDisplay, int paramInt1, int paramInt2)
    {
        synchronized (this.mRCStack)
        {
            this.mArtworkExpectedWidth = paramInt1;
            this.mArtworkExpectedHeight = paramInt2;
            return;
        }
    }

    // ERROR //
    public int requestAudioFocus(int paramInt1, int paramInt2, IBinder paramIBinder, IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString1, String paramString2)
    {
        // Byte code:
        //     0: ldc 156
        //     2: new 964	java/lang/StringBuilder
        //     5: dup
        //     6: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     9: ldc_w 2130
        //     12: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     15: aload 5
        //     17: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     20: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     23: invokestatic 1648	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     26: pop
        //     27: aload_3
        //     28: invokeinterface 2133 1 0
        //     33: ifne +18 -> 51
        //     36: ldc 156
        //     38: ldc_w 2135
        //     41: invokestatic 981	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     44: pop
        //     45: iconst_0
        //     46: istore 13
        //     48: iload 13
        //     50: ireturn
        //     51: getstatic 302	android/media/AudioService:mAudioFocusLock	Ljava/lang/Object;
        //     54: astore 8
        //     56: aload 8
        //     58: monitorenter
        //     59: aload_0
        //     60: invokespecial 1493	android/media/AudioService:canReassignAudioFocus	()Z
        //     63: ifne +20 -> 83
        //     66: iconst_0
        //     67: istore 13
        //     69: aload 8
        //     71: monitorexit
        //     72: goto -24 -> 48
        //     75: astore 9
        //     77: aload 8
        //     79: monitorexit
        //     80: aload 9
        //     82: athrow
        //     83: new 28	android/media/AudioService$AudioFocusDeathHandler
        //     86: dup
        //     87: aload_0
        //     88: aload_3
        //     89: invokespecial 2136	android/media/AudioService$AudioFocusDeathHandler:<init>	(Landroid/media/AudioService;Landroid/os/IBinder;)V
        //     92: astore 10
        //     94: aload_3
        //     95: aload 10
        //     97: iconst_0
        //     98: invokeinterface 1589 3 0
        //     103: aload_0
        //     104: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     107: invokevirtual 1128	java/util/Stack:empty	()Z
        //     110: ifne +116 -> 226
        //     113: aload_0
        //     114: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     117: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     120: checkcast 31	android/media/AudioService$FocusStackEntry
        //     123: getfield 937	android/media/AudioService$FocusStackEntry:mClientId	Ljava/lang/String;
        //     126: aload 5
        //     128: invokevirtual 941	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     131: ifeq +95 -> 226
        //     134: aload_0
        //     135: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     138: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     141: checkcast 31	android/media/AudioService$FocusStackEntry
        //     144: getfield 1203	android/media/AudioService$FocusStackEntry:mFocusChangeType	I
        //     147: iload_2
        //     148: if_icmpne +65 -> 213
        //     151: aload_3
        //     152: aload 10
        //     154: iconst_0
        //     155: invokeinterface 2139 3 0
        //     160: pop
        //     161: iconst_1
        //     162: istore 13
        //     164: aload 8
        //     166: monitorexit
        //     167: goto -119 -> 48
        //     170: astore 11
        //     172: ldc 156
        //     174: new 964	java/lang/StringBuilder
        //     177: dup
        //     178: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     181: ldc_w 2141
        //     184: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     187: aload_3
        //     188: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     191: ldc_w 2143
        //     194: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     197: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     200: invokestatic 1593	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     203: pop
        //     204: iconst_0
        //     205: istore 13
        //     207: aload 8
        //     209: monitorexit
        //     210: goto -162 -> 48
        //     213: aload_0
        //     214: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     217: invokevirtual 1636	java/util/Stack:pop	()Ljava/lang/Object;
        //     220: checkcast 31	android/media/AudioService$FocusStackEntry
        //     223: invokevirtual 1639	android/media/AudioService$FocusStackEntry:unlinkToDeath	()V
        //     226: aload_0
        //     227: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     230: invokevirtual 1128	java/util/Stack:empty	()Z
        //     233: ifne +58 -> 291
        //     236: aload_0
        //     237: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     240: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     243: checkcast 31	android/media/AudioService$FocusStackEntry
        //     246: getfield 1491	android/media/AudioService$FocusStackEntry:mFocusDispatcher	Landroid/media/IAudioFocusDispatcher;
        //     249: astore 17
        //     251: aload 17
        //     253: ifnull +38 -> 291
        //     256: aload_0
        //     257: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     260: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     263: checkcast 31	android/media/AudioService$FocusStackEntry
        //     266: getfield 1491	android/media/AudioService$FocusStackEntry:mFocusDispatcher	Landroid/media/IAudioFocusDispatcher;
        //     269: iload_2
        //     270: bipush 255
        //     272: imul
        //     273: aload_0
        //     274: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     277: invokevirtual 934	java/util/Stack:peek	()Ljava/lang/Object;
        //     280: checkcast 31	android/media/AudioService$FocusStackEntry
        //     283: getfield 937	android/media/AudioService$FocusStackEntry:mClientId	Ljava/lang/String;
        //     286: invokeinterface 1499 3 0
        //     291: aload_0
        //     292: aload 5
        //     294: iconst_0
        //     295: invokespecial 1872	android/media/AudioService:removeFocusStackEntry	(Ljava/lang/String;Z)V
        //     298: aload_0
        //     299: getfield 404	android/media/AudioService:mFocusStack	Ljava/util/Stack;
        //     302: new 31	android/media/AudioService$FocusStackEntry
        //     305: dup
        //     306: iload_1
        //     307: iload_2
        //     308: aload 4
        //     310: aload_3
        //     311: aload 5
        //     313: aload 10
        //     315: aload 6
        //     317: invokestatic 1945	android/os/Binder:getCallingUid	()I
        //     320: invokespecial 2146	android/media/AudioService$FocusStackEntry:<init>	(IILandroid/media/IAudioFocusDispatcher;Landroid/os/IBinder;Ljava/lang/String;Landroid/media/AudioService$AudioFocusDeathHandler;Ljava/lang/String;I)V
        //     323: invokevirtual 1571	java/util/Stack:push	(Ljava/lang/Object;)Ljava/lang/Object;
        //     326: pop
        //     327: aload_0
        //     328: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     331: astore 15
        //     333: aload 15
        //     335: monitorenter
        //     336: aload_0
        //     337: bipush 15
        //     339: invokespecial 1643	android/media/AudioService:checkUpdateRemoteControlDisplay_syncAfRcs	(I)V
        //     342: aload 15
        //     344: monitorexit
        //     345: aload 8
        //     347: monitorexit
        //     348: iconst_1
        //     349: istore 13
        //     351: goto -303 -> 48
        //     354: astore 18
        //     356: ldc 156
        //     358: new 964	java/lang/StringBuilder
        //     361: dup
        //     362: invokespecial 965	java/lang/StringBuilder:<init>	()V
        //     365: ldc_w 2148
        //     368: invokevirtual 971	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     371: aload 18
        //     373: invokevirtual 1143	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     376: invokevirtual 978	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     379: invokestatic 981	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     382: pop
        //     383: aload 18
        //     385: invokevirtual 1502	android/os/RemoteException:printStackTrace	()V
        //     388: goto -97 -> 291
        //     391: astore 16
        //     393: aload 15
        //     395: monitorexit
        //     396: aload 16
        //     398: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     59	80	75	finally
        //     83	94	75	finally
        //     94	103	75	finally
        //     103	251	75	finally
        //     256	291	75	finally
        //     291	336	75	finally
        //     345	388	75	finally
        //     396	399	75	finally
        //     94	103	170	android/os/RemoteException
        //     256	291	354	android/os/RemoteException
        //     336	345	391	finally
        //     393	396	391	finally
    }

    public int setBluetoothA2dpDeviceConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        int i = 0;
        HashMap localHashMap = this.mConnectedDevices;
        if (paramInt == 2)
            i = 1;
        try
        {
            int j = checkSendBecomingNoisyIntent(128, i);
            queueMsgUnderWakeLock(this.mAudioHandler, 21, paramInt, 0, paramBluetoothDevice, j);
            return j;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setBluetoothA2dpOn(boolean paramBoolean)
    {
        setBluetoothA2dpOnInt(paramBoolean);
    }

    public void setBluetoothA2dpOnInt(boolean paramBoolean)
    {
        for (int i = 0; ; i = 10)
            synchronized (this.mBluetoothA2dpEnabledLock)
            {
                this.mBluetoothA2dpEnabled = paramBoolean;
                AudioHandler localAudioHandler = this.mAudioHandler;
                if (this.mBluetoothA2dpEnabled)
                {
                    sendMsg(localAudioHandler, 9, 2, 1, i, null, 0);
                    return;
                }
            }
    }

    public void setBluetoothScoOn(boolean paramBoolean)
    {
        if (!checkAudioSettingsPermission("setBluetoothScoOn()"))
            return;
        if (paramBoolean);
        for (int i = 3; ; i = 0)
        {
            this.mForcedUseForComm = i;
            sendMsg(this.mAudioHandler, 9, 2, 0, this.mForcedUseForComm, null, 0);
            sendMsg(this.mAudioHandler, 9, 2, 2, this.mForcedUseForComm, null, 0);
            break;
        }
    }

    public void setMasterMute(boolean paramBoolean, int paramInt, IBinder paramIBinder)
    {
        AudioHandler localAudioHandler;
        if (paramBoolean != AudioSystem.getMasterMute())
        {
            AudioSystem.setMasterMute(paramBoolean);
            localAudioHandler = this.mAudioHandler;
            if (!paramBoolean)
                break label47;
        }
        label47: for (int i = 1; ; i = 0)
        {
            sendMsg(localAudioHandler, 15, 0, i, 0, null, 500);
            sendMasterMuteUpdate(paramBoolean, paramInt);
            return;
        }
    }

    public void setMasterVolume(int paramInt1, int paramInt2)
    {
        if (paramInt1 < 0)
            paramInt1 = 0;
        while (true)
        {
            doSetMasterVolume(paramInt1 / 100.0F, paramInt2);
            return;
            if (paramInt1 > 100)
                paramInt1 = 100;
        }
    }

    public void setMode(int paramInt, IBinder paramIBinder)
    {
        if (!checkAudioSettingsPermission("setMode()"));
        while (true)
        {
            return;
            if ((paramInt < -1) || (paramInt >= 4))
                continue;
            ArrayList localArrayList = this.mSetModeDeathHandlers;
            if (paramInt == -1);
            try
            {
                paramInt = this.mMode;
                int i = setModeInt(paramInt, paramIBinder, Binder.getCallingPid());
                if (i == 0)
                    continue;
                disconnectBluetoothSco(i);
            }
            finally
            {
            }
        }
    }

    int setModeInt(int paramInt1, IBinder paramIBinder, int paramInt2)
    {
        int i = 0;
        int m;
        if (paramIBinder == null)
        {
            Log.e("AudioService", "setModeInt() called with null binder");
            m = 0;
            return m;
        }
        Object localObject = null;
        Iterator localIterator = this.mSetModeDeathHandlers.iterator();
        while (localIterator.hasNext())
        {
            SetModeDeathHandler localSetModeDeathHandler = (SetModeDeathHandler)localIterator.next();
            if (localSetModeDeathHandler.getPid() == paramInt2)
            {
                localObject = localSetModeDeathHandler;
                localIterator.remove();
                ((SetModeDeathHandler)localObject).getBinder().unlinkToDeath((IBinder.DeathRecipient)localObject, 0);
            }
        }
        label90: int k;
        if (paramInt1 == 0)
        {
            if (!this.mSetModeDeathHandlers.isEmpty())
            {
                localObject = (SetModeDeathHandler)this.mSetModeDeathHandlers.get(0);
                paramIBinder = ((SetModeDeathHandler)localObject).getBinder();
                paramInt1 = ((SetModeDeathHandler)localObject).getMode();
            }
            int j = this.mMode;
            if (paramInt1 == j)
                break label389;
            k = AudioSystem.setPhoneState(paramInt1);
            if (k != 0)
                break label359;
            this.mMode = paramInt1;
            label157: if ((k != 0) && (!this.mSetModeDeathHandlers.isEmpty()))
                break label393;
            if (k == 0)
                if (paramInt1 != 0)
                {
                    if (!this.mSetModeDeathHandlers.isEmpty())
                        break label395;
                    Log.e("AudioService", "setMode() different from MODE_NORMAL with empty mode client stack");
                }
        }
        while (true)
        {
            while (true)
            {
                int n = getActiveStreamType(-2147483648);
                if (n == -200)
                    n = 3;
                int i1 = getDeviceForStream(n);
                int i2 = this.mStreamStates[this.mStreamVolumeAlias[n]].getIndex(i1, false);
                setStreamVolumeInt(this.mStreamVolumeAlias[n], i2, i1, true, false);
                updateStreamVolumeAlias(true);
                m = i;
                break;
                if (localObject == null)
                    localObject = new SetModeDeathHandler(paramIBinder, paramInt2);
                try
                {
                    paramIBinder.linkToDeath((IBinder.DeathRecipient)localObject, 0);
                    this.mSetModeDeathHandlers.add(0, localObject);
                    ((SetModeDeathHandler)localObject).setMode(paramInt1);
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.w("AudioService", "setMode() could not link to " + paramIBinder + " binder death");
                }
            }
            label359: if (localObject != null)
            {
                this.mSetModeDeathHandlers.remove(localObject);
                paramIBinder.unlinkToDeath((IBinder.DeathRecipient)localObject, 0);
            }
            paramInt1 = 0;
            break label157;
            label389: k = 0;
            break label157;
            label393: break label90;
            label395: i = ((SetModeDeathHandler)this.mSetModeDeathHandlers.get(0)).getPid();
        }
    }

    public void setPlaybackInfoForRcc(int paramInt1, int paramInt2, int paramInt3)
    {
        sendMsg(this.mAudioHandler, 18, 2, paramInt1, paramInt2, Integer.valueOf(paramInt3), 0);
    }

    public void setRemoteStreamVolume(int paramInt)
    {
        int i;
        IRemoteVolumeObserver localIRemoteVolumeObserver;
        synchronized (this.mMainRemote)
        {
            if (this.mMainRemote.mRccId == -1)
                return;
            i = this.mMainRemote.mRccId;
            localIRemoteVolumeObserver = null;
        }
        synchronized (this.mRCStack)
        {
            Iterator localIterator = this.mRCStack.iterator();
            while (localIterator.hasNext())
            {
                RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                if (localRemoteControlStackEntry.mRccId == i)
                    localIRemoteVolumeObserver = localRemoteControlStackEntry.mRemoteVolumeObs;
            }
            if (localIRemoteVolumeObserver != null)
            {
                try
                {
                    localIRemoteVolumeObserver.dispatchRemoteVolumeUpdate(0, paramInt);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("AudioService", "Error dispatching absolute volume update", localRemoteException);
                }
                localObject1 = finally;
                throw localObject1;
            }
        }
    }

    public void setRingerMode(int paramInt)
    {
        if ((paramInt == 1) && (!this.mHasVibrator))
            paramInt = 0;
        if (paramInt != getRingerMode())
        {
            setRingerModeInt(paramInt, true);
            broadcastRingerMode(paramInt);
        }
    }

    public void setRingtonePlayer(IRingtonePlayer paramIRingtonePlayer)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.REMOTE_AUDIO_PLAYBACK", null);
        this.mRingtonePlayer = paramIRingtonePlayer;
    }

    public void setSpeakerphoneOn(boolean paramBoolean)
    {
        if (!checkAudioSettingsPermission("setSpeakerphoneOn()"))
            return;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            this.mForcedUseForComm = i;
            sendMsg(this.mAudioHandler, 9, 2, 0, this.mForcedUseForComm, null, 0);
            break;
        }
    }

    public void setStreamMute(int paramInt, boolean paramBoolean, IBinder paramIBinder)
    {
        if (isStreamAffectedByMute(paramInt))
            this.mStreamStates[paramInt].mute(paramIBinder, paramBoolean);
    }

    public void setStreamSolo(int paramInt, boolean paramBoolean, IBinder paramIBinder)
    {
        int i = 0;
        if (i < this.mStreamStates.length)
        {
            if ((!isStreamAffectedByMute(i)) || (i == paramInt));
            while (true)
            {
                i++;
                break;
                this.mStreamStates[i].mute(paramIBinder, paramBoolean);
            }
        }
    }

    public void setStreamVolume(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 1;
        ensureValidStreamType(paramInt1);
        VolumeStreamState localVolumeStreamState1 = this.mStreamStates[this.mStreamVolumeAlias[paramInt1]];
        int j = getDeviceForStream(paramInt1);
        boolean bool;
        int k;
        label103: label120: VolumeStreamState localVolumeStreamState2;
        if (localVolumeStreamState1.muteCount() != 0)
        {
            bool = i;
            k = localVolumeStreamState1.getIndex(j, bool);
            int m = rescaleIndex(paramInt2 * 10, paramInt1, this.mStreamVolumeAlias[paramInt1]);
            if (((paramInt3 & 0x2) != 0) || (this.mStreamVolumeAlias[paramInt1] == getMasterStreamType()))
            {
                if (m != 0)
                    break label193;
                if (!this.mHasVibrator)
                    break label187;
                int n = i;
                setStreamVolumeInt(this.mStreamVolumeAlias[paramInt1], m, j, false, i);
                setRingerMode(n);
            }
            setStreamVolumeInt(this.mStreamVolumeAlias[paramInt1], m, j, false, i);
            localVolumeStreamState2 = this.mStreamStates[paramInt1];
            if (this.mStreamStates[paramInt1].muteCount() == 0)
                break label199;
        }
        while (true)
        {
            sendVolumeUpdate(paramInt1, k, localVolumeStreamState2.getIndex(j, i), paramInt3);
            return;
            bool = false;
            break;
            label187: int i1 = 0;
            break label103;
            label193: i1 = 2;
            break label120;
            label199: i = 0;
        }
    }

    public void setVibrateSetting(int paramInt1, int paramInt2)
    {
        if (!this.mHasVibrator);
        while (true)
        {
            return;
            this.mVibrateSetting = getValueForVibrateSetting(this.mVibrateSetting, paramInt1, paramInt2);
            broadcastVibrateSetting(paramInt1);
        }
    }

    public void setWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString)
    {
        synchronized (this.mConnectedDevices)
        {
            int i = checkSendBecomingNoisyIntent(paramInt1, paramInt2);
            queueMsgUnderWakeLock(this.mAudioHandler, 20, paramInt1, paramInt2, paramString, i);
            return;
        }
    }

    public boolean shouldVibrate(int paramInt)
    {
        int i = 1;
        boolean bool = false;
        if (!this.mHasVibrator);
        while (true)
        {
            return bool;
            switch (getVibrateSetting(paramInt))
            {
            case 0:
            default:
            case 1:
            case 2:
            }
        }
        if (getRingerMode() != 0);
        while (true)
        {
            bool = i;
            break;
            i = 0;
        }
        if (getRingerMode() == i);
        while (true)
        {
            bool = i;
            break;
            i = 0;
        }
    }

    public void startBluetoothSco(IBinder paramIBinder)
    {
        if ((!checkAudioSettingsPermission("startBluetoothSco()")) || (!this.mBootCompleted));
        while (true)
        {
            return;
            getScoClient(paramIBinder, true).incCount();
        }
    }

    public AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver paramIAudioRoutesObserver)
    {
        synchronized (this.mCurAudioRoutes)
        {
            AudioRoutesInfo localAudioRoutesInfo2 = new AudioRoutesInfo(this.mCurAudioRoutes);
            this.mRoutesObservers.register(paramIAudioRoutesObserver);
            return localAudioRoutesInfo2;
        }
    }

    public void stopBluetoothSco(IBinder paramIBinder)
    {
        if ((!checkAudioSettingsPermission("stopBluetoothSco()")) || (!this.mBootCompleted));
        while (true)
        {
            return;
            ScoClient localScoClient = getScoClient(paramIBinder, false);
            if (localScoClient != null)
                localScoClient.decCount();
        }
    }

    public void unloadSoundEffects()
    {
        while (true)
        {
            int j;
            synchronized (this.mSoundEffectsLock)
            {
                if (this.mSoundPool != null)
                {
                    this.mAudioHandler.removeMessages(8);
                    this.mAudioHandler.removeMessages(6);
                    int[] arrayOfInt = new int[SOUND_EFFECT_FILES.length];
                    int i = 0;
                    if (i >= SOUND_EFFECT_FILES.length)
                        continue;
                    arrayOfInt[i] = 0;
                    i++;
                    continue;
                    if (j < 9)
                    {
                        if ((this.SOUND_EFFECT_FILES_MAP[j][1] <= 0) || (arrayOfInt[this.SOUND_EFFECT_FILES_MAP[j][0]] != 0))
                            continue;
                        this.mSoundPool.unload(this.SOUND_EFFECT_FILES_MAP[j][1]);
                        this.SOUND_EFFECT_FILES_MAP[j][1] = -1;
                        arrayOfInt[this.SOUND_EFFECT_FILES_MAP[j][0]] = -1;
                    }
                }
            }
        }
    }

    public void unregisterAudioFocusClient(String paramString)
    {
        synchronized (mAudioFocusLock)
        {
            removeFocusStackEntry(paramString, false);
            return;
        }
    }

    public void unregisterMediaButtonEventReceiverForCalls()
    {
        if (this.mContext.checkCallingPermission("android.permission.MODIFY_PHONE_STATE") != 0)
            Log.e("AudioService", "Invalid permissions to unregister media button receiver for calls");
        while (true)
        {
            return;
            synchronized (this.mRCStack)
            {
                this.mMediaReceiverForCalls = null;
            }
        }
    }

    public void unregisterMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName)
    {
        Log.i("AudioService", "    Remote Control     unregisterMediaButtonIntent() for " + paramPendingIntent);
        synchronized (mAudioFocusLock)
        {
            synchronized (this.mRCStack)
            {
                boolean bool = isCurrentRcController(paramPendingIntent);
                removeMediaButtonReceiver(paramPendingIntent);
                if (bool)
                    checkUpdateRemoteControlDisplay_syncAfRcs(15);
                return;
            }
        }
    }

    // ERROR //
    public void unregisterRemoteControlClient(PendingIntent paramPendingIntent, IRemoteControlClient paramIRemoteControlClient)
    {
        // Byte code:
        //     0: getstatic 302	android/media/AudioService:mAudioFocusLock	Ljava/lang/Object;
        //     3: astore_3
        //     4: aload_3
        //     5: monitorenter
        //     6: aload_0
        //     7: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     10: astore 5
        //     12: aload 5
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 417	android/media/AudioService:mRCStack	Ljava/util/Stack;
        //     19: invokevirtual 1044	java/util/Stack:iterator	()Ljava/util/Iterator;
        //     22: astore 7
        //     24: aload 7
        //     26: invokeinterface 1007 1 0
        //     31: ifeq +74 -> 105
        //     34: aload 7
        //     36: invokeinterface 1010 1 0
        //     41: checkcast 19	android/media/AudioService$RemoteControlStackEntry
        //     44: astore 8
        //     46: aload 8
        //     48: getfield 1132	android/media/AudioService$RemoteControlStackEntry:mMediaIntent	Landroid/app/PendingIntent;
        //     51: aload_1
        //     52: invokevirtual 1439	android/app/PendingIntent:equals	(Ljava/lang/Object;)Z
        //     55: ifeq -31 -> 24
        //     58: aload_2
        //     59: aload 8
        //     61: getfield 1264	android/media/AudioService$RemoteControlStackEntry:mRcClient	Landroid/media/IRemoteControlClient;
        //     64: invokevirtual 1514	java/lang/Object:equals	(Ljava/lang/Object;)Z
        //     67: ifeq -43 -> 24
        //     70: aload 8
        //     72: invokevirtual 1653	android/media/AudioService$RemoteControlStackEntry:unlinkToRcClientDeath	()V
        //     75: aload 8
        //     77: aconst_null
        //     78: putfield 1264	android/media/AudioService$RemoteControlStackEntry:mRcClient	Landroid/media/IRemoteControlClient;
        //     81: aload 8
        //     83: aconst_null
        //     84: putfield 1028	android/media/AudioService$RemoteControlStackEntry:mCallingPackageName	Ljava/lang/String;
        //     87: goto -63 -> 24
        //     90: astore 6
        //     92: aload 5
        //     94: monitorexit
        //     95: aload 6
        //     97: athrow
        //     98: astore 4
        //     100: aload_3
        //     101: monitorexit
        //     102: aload 4
        //     104: athrow
        //     105: aload 5
        //     107: monitorexit
        //     108: aload_3
        //     109: monitorexit
        //     110: return
        //
        // Exception table:
        //     from	to	target	type
        //     15	95	90	finally
        //     105	108	90	finally
        //     6	15	98	finally
        //     95	102	98	finally
        //     108	110	98	finally
    }

    public void unregisterRemoteControlDisplay(IRemoteControlDisplay paramIRemoteControlDisplay)
    {
        Stack localStack = this.mRCStack;
        if (paramIRemoteControlDisplay != null);
        try
        {
            if (paramIRemoteControlDisplay != this.mRcDisplay)
                return;
            rcDisplay_stopDeathMonitor_syncRcStack();
            this.mRcDisplay = null;
            Iterator localIterator = this.mRCStack.iterator();
            while (localIterator.hasNext())
            {
                RemoteControlStackEntry localRemoteControlStackEntry = (RemoteControlStackEntry)localIterator.next();
                IRemoteControlClient localIRemoteControlClient = localRemoteControlStackEntry.mRcClient;
                if (localIRemoteControlClient != null)
                    try
                    {
                        localRemoteControlStackEntry.mRcClient.unplugRemoteControlDisplay(paramIRemoteControlDisplay);
                    }
                    catch (RemoteException localRemoteException)
                    {
                        Log.e("AudioService", "Error disconnecting remote control display to client: " + localRemoteException);
                        localRemoteException.printStackTrace();
                    }
            }
        }
        finally
        {
        }
    }

    private class RcDisplayDeathHandler
        implements IBinder.DeathRecipient
    {
        private IBinder mCb;

        public RcDisplayDeathHandler(IBinder arg2)
        {
            Object localObject;
            this.mCb = localObject;
        }

        public void binderDied()
        {
            synchronized (AudioService.this.mRCStack)
            {
                Log.w("AudioService", "RemoteControl: display died");
                AudioService.access$8402(AudioService.this, null);
                return;
            }
        }

        public void unlinkToRcDisplayDeath()
        {
            try
            {
                this.mCb.unlinkToDeath(this, 0);
                return;
            }
            catch (NoSuchElementException localNoSuchElementException)
            {
                while (true)
                {
                    Log.e("AudioService", "Encountered " + localNoSuchElementException + " in unlinkToRcDisplayDeath()");
                    localNoSuchElementException.printStackTrace();
                }
            }
        }
    }

    private static class RemoteControlStackEntry
    {
        public String mCallingPackageName;
        public int mCallingUid;
        public PendingIntent mMediaIntent;
        public int mPlaybackState;
        public int mPlaybackStream;
        public int mPlaybackType;
        public int mPlaybackVolume;
        public int mPlaybackVolumeHandling;
        public int mPlaybackVolumeMax;
        public IRemoteControlClient mRcClient;
        public AudioService.RcClientDeathHandler mRcClientDeathHandler;
        public int mRccId = -1;
        public ComponentName mReceiverComponent;
        public IRemoteVolumeObserver mRemoteVolumeObs;

        public RemoteControlStackEntry(PendingIntent paramPendingIntent, ComponentName paramComponentName)
        {
            this.mMediaIntent = paramPendingIntent;
            this.mReceiverComponent = paramComponentName;
            this.mCallingUid = -1;
            this.mRcClient = null;
            this.mRccId = AudioService.access$8104();
            resetPlaybackInfo();
        }

        protected void finalize()
            throws Throwable
        {
            unlinkToRcClientDeath();
            super.finalize();
        }

        public void resetPlaybackInfo()
        {
            this.mPlaybackType = 0;
            this.mPlaybackVolume = 15;
            this.mPlaybackVolumeMax = 15;
            this.mPlaybackVolumeHandling = 1;
            this.mPlaybackStream = 3;
            this.mPlaybackState = 1;
            this.mRemoteVolumeObs = null;
        }

        public void unlinkToRcClientDeath()
        {
            if ((this.mRcClientDeathHandler != null) && (AudioService.RcClientDeathHandler.access$8200(this.mRcClientDeathHandler) != null));
            try
            {
                AudioService.RcClientDeathHandler.access$8200(this.mRcClientDeathHandler).unlinkToDeath(this.mRcClientDeathHandler, 0);
                this.mRcClientDeathHandler = null;
                return;
            }
            catch (NoSuchElementException localNoSuchElementException)
            {
                while (true)
                {
                    Log.e("AudioService", "Encountered " + localNoSuchElementException + " in unlinkToRcClientDeath()");
                    localNoSuchElementException.printStackTrace();
                }
            }
        }
    }

    private class RemotePlaybackState
    {
        int mRccId;
        int mVolume;
        int mVolumeHandling;
        int mVolumeMax;

        private RemotePlaybackState(int paramInt1, int paramInt2, int arg4)
        {
            this.mRccId = paramInt1;
            this.mVolume = paramInt2;
            int i;
            this.mVolumeMax = i;
            this.mVolumeHandling = 1;
        }
    }

    private class RcClientDeathHandler
        implements IBinder.DeathRecipient
    {
        private IBinder mCb;
        private PendingIntent mMediaIntent;

        RcClientDeathHandler(IBinder paramPendingIntent, PendingIntent arg3)
        {
            this.mCb = paramPendingIntent;
            Object localObject;
            this.mMediaIntent = localObject;
        }

        public void binderDied()
        {
            Log.w("AudioService", "    RemoteControlClient died");
            AudioService.this.registerRemoteControlClient(this.mMediaIntent, null, null);
            AudioService.this.postReevaluateRemote();
        }

        public IBinder getBinder()
        {
            return this.mCb;
        }
    }

    private class AudioFocusDeathHandler
        implements IBinder.DeathRecipient
    {
        private IBinder mCb;

        AudioFocusDeathHandler(IBinder arg2)
        {
            Object localObject;
            this.mCb = localObject;
        }

        public void binderDied()
        {
            synchronized (AudioService.mAudioFocusLock)
            {
                Log.w("AudioService", "    AudioFocus     audio focus client died");
                AudioService.this.removeFocusStackEntryForClient(this.mCb);
                return;
            }
        }

        public IBinder getBinder()
        {
            return this.mCb;
        }
    }

    private static class FocusStackEntry
    {
        public int mCallingUid;
        public String mClientId;
        public int mFocusChangeType;
        public IAudioFocusDispatcher mFocusDispatcher = null;
        public AudioService.AudioFocusDeathHandler mHandler;
        public String mPackageName;
        public IBinder mSourceRef = null;
        public int mStreamType = -1;

        public FocusStackEntry()
        {
        }

        public FocusStackEntry(int paramInt1, int paramInt2, IAudioFocusDispatcher paramIAudioFocusDispatcher, IBinder paramIBinder, String paramString1, AudioService.AudioFocusDeathHandler paramAudioFocusDeathHandler, String paramString2, int paramInt3)
        {
            this.mStreamType = paramInt1;
            this.mFocusDispatcher = paramIAudioFocusDispatcher;
            this.mSourceRef = paramIBinder;
            this.mClientId = paramString1;
            this.mFocusChangeType = paramInt2;
            this.mHandler = paramAudioFocusDeathHandler;
            this.mPackageName = paramString2;
            this.mCallingUid = paramInt3;
        }

        protected void finalize()
            throws Throwable
        {
            unlinkToDeath();
            super.finalize();
        }

        public void unlinkToDeath()
        {
            try
            {
                if ((this.mSourceRef != null) && (this.mHandler != null))
                {
                    this.mSourceRef.unlinkToDeath(this.mHandler, 0);
                    this.mHandler = null;
                }
                return;
            }
            catch (NoSuchElementException localNoSuchElementException)
            {
                while (true)
                    Log.e("AudioService", "Encountered " + localNoSuchElementException + " in FocusStackEntry.unlinkToDeath()");
            }
        }
    }

    private class AudioServiceBroadcastReceiver extends BroadcastReceiver
    {
        private AudioServiceBroadcastReceiver()
        {
        }

        // ERROR //
        public void onReceive(Context paramContext, Intent paramIntent)
        {
            // Byte code:
            //     0: aload_2
            //     1: invokevirtual 27	android/content/Intent:getAction	()Ljava/lang/String;
            //     4: astore_3
            //     5: aload_3
            //     6: ldc 29
            //     8: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     11: ifeq +80 -> 91
            //     14: aload_2
            //     15: ldc 37
            //     17: iconst_0
            //     18: invokevirtual 41	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     21: tableswitch	default:+31 -> 52, 1:+42->63, 2:+49->70, 3:+56->77, 4:+63->84
            //     53: istore 46
            //     55: iconst_3
            //     56: iload 46
            //     58: invokestatic 47	android/media/AudioSystem:setForceUse	(II)I
            //     61: pop
            //     62: return
            //     63: bipush 7
            //     65: istore 46
            //     67: goto -12 -> 55
            //     70: bipush 6
            //     72: istore 46
            //     74: goto -19 -> 55
            //     77: bipush 8
            //     79: istore 46
            //     81: goto -26 -> 55
            //     84: bipush 9
            //     86: istore 46
            //     88: goto -33 -> 55
            //     91: aload_3
            //     92: ldc 49
            //     94: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     97: ifeq +208 -> 305
            //     100: aload_2
            //     101: ldc 51
            //     103: iconst_0
            //     104: invokevirtual 41	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     107: istore 36
            //     109: bipush 16
            //     111: istore 37
            //     113: aload_2
            //     114: ldc 53
            //     116: invokevirtual 57	android/content/Intent:getParcelableExtra	(Ljava/lang/String;)Landroid/os/Parcelable;
            //     119: checkcast 59	android/bluetooth/BluetoothDevice
            //     122: astore 38
            //     124: aload 38
            //     126: ifnull -64 -> 62
            //     129: aload 38
            //     131: invokevirtual 62	android/bluetooth/BluetoothDevice:getAddress	()Ljava/lang/String;
            //     134: astore 39
            //     136: aload 38
            //     138: invokevirtual 66	android/bluetooth/BluetoothDevice:getBluetoothClass	()Landroid/bluetooth/BluetoothClass;
            //     141: astore 40
            //     143: aload 40
            //     145: ifnull +43 -> 188
            //     148: aload 40
            //     150: invokevirtual 72	android/bluetooth/BluetoothClass:getDeviceClass	()I
            //     153: lookupswitch	default:+35->188, 1028:+113->266, 1032:+113->266, 1056:+120->273
            //     189: dload_1
            //     190: invokestatic 78	android/bluetooth/BluetoothAdapter:checkBluetoothAddress	(Ljava/lang/String;)Z
            //     193: ifne +7 -> 200
            //     196: ldc 80
            //     198: astore 39
            //     200: iload 36
            //     202: iconst_2
            //     203: if_icmpne +77 -> 280
            //     206: iconst_1
            //     207: istore 41
            //     209: aload_0
            //     210: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     213: iload 41
            //     215: iload 37
            //     217: aload 39
            //     219: invokestatic 84	android/media/AudioService:access$6800	(Landroid/media/AudioService;ZILjava/lang/String;)Z
            //     222: ifeq -160 -> 62
            //     225: aload_0
            //     226: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     229: invokestatic 88	android/media/AudioService:access$2100	(Landroid/media/AudioService;)Ljava/util/ArrayList;
            //     232: astore 42
            //     234: aload 42
            //     236: monitorenter
            //     237: iload 41
            //     239: ifeq +47 -> 286
            //     242: aload_0
            //     243: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     246: aload 38
            //     248: invokestatic 92	android/media/AudioService:access$2602	(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
            //     251: pop
            //     252: aload 42
            //     254: monitorexit
            //     255: goto -193 -> 62
            //     258: astore 44
            //     260: aload 42
            //     262: monitorexit
            //     263: aload 44
            //     265: athrow
            //     266: bipush 32
            //     268: istore 37
            //     270: goto -82 -> 188
            //     273: bipush 64
            //     275: istore 37
            //     277: goto -89 -> 188
            //     280: iconst_0
            //     281: istore 41
            //     283: goto -74 -> 209
            //     286: aload_0
            //     287: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     290: aconst_null
            //     291: invokestatic 92	android/media/AudioService:access$2602	(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
            //     294: pop
            //     295: aload_0
            //     296: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     299: invokestatic 95	android/media/AudioService:access$5900	(Landroid/media/AudioService;)V
            //     302: goto -50 -> 252
            //     305: aload_3
            //     306: ldc 97
            //     308: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     311: ifne +12 -> 323
            //     314: aload_3
            //     315: ldc 99
            //     317: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     320: ifeq +223 -> 543
            //     323: aload_2
            //     324: ldc 101
            //     326: iconst_0
            //     327: invokevirtual 41	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     330: istore 4
            //     332: aload_2
            //     333: ldc 103
            //     335: bipush 255
            //     337: invokevirtual 41	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     340: istore 5
            //     342: aload_2
            //     343: ldc 105
            //     345: bipush 255
            //     347: invokevirtual 41	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     350: istore 6
            //     352: iload 5
            //     354: bipush 255
            //     356: if_icmpne +131 -> 487
            //     359: iload 6
            //     361: bipush 255
            //     363: if_icmpne +124 -> 487
            //     366: ldc 80
            //     368: astore 7
            //     370: aload_3
            //     371: ldc 97
            //     373: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     376: ifeq +146 -> 522
            //     379: sipush 8192
            //     382: istore 8
            //     384: new 107	java/lang/StringBuilder
            //     387: dup
            //     388: invokespecial 108	java/lang/StringBuilder:<init>	()V
            //     391: ldc 110
            //     393: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     396: astore 9
            //     398: aload_3
            //     399: ldc 97
            //     401: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     404: ifeq +126 -> 530
            //     407: ldc 116
            //     409: astore 10
            //     411: ldc 118
            //     413: aload 9
            //     415: aload 10
            //     417: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     420: ldc 120
            //     422: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     425: iload 4
            //     427: invokevirtual 123	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     430: ldc 125
            //     432: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     435: iload 5
            //     437: invokevirtual 123	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     440: ldc 127
            //     442: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     445: iload 6
            //     447: invokevirtual 123	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     450: invokevirtual 130	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     453: invokestatic 136	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     456: pop
            //     457: aload_0
            //     458: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     461: astore 12
            //     463: iload 4
            //     465: iconst_1
            //     466: if_icmpne +71 -> 537
            //     469: iconst_1
            //     470: istore 13
            //     472: aload 12
            //     474: iload 13
            //     476: iload 8
            //     478: aload 7
            //     480: invokestatic 84	android/media/AudioService:access$6800	(Landroid/media/AudioService;ZILjava/lang/String;)Z
            //     483: pop
            //     484: goto -422 -> 62
            //     487: new 107	java/lang/StringBuilder
            //     490: dup
            //     491: invokespecial 108	java/lang/StringBuilder:<init>	()V
            //     494: ldc 138
            //     496: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     499: iload 5
            //     501: invokevirtual 123	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     504: ldc 140
            //     506: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     509: iload 6
            //     511: invokevirtual 123	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     514: invokevirtual 130	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     517: astore 7
            //     519: goto -149 -> 370
            //     522: sipush 16384
            //     525: istore 8
            //     527: goto -143 -> 384
            //     530: ldc 142
            //     532: astore 10
            //     534: goto -123 -> 411
            //     537: iconst_0
            //     538: istore 13
            //     540: goto -68 -> 472
            //     543: aload_3
            //     544: ldc 144
            //     546: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     549: ifeq +259 -> 808
            //     552: iconst_0
            //     553: istore 26
            //     555: bipush 255
            //     557: istore 27
            //     559: aload_0
            //     560: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     563: invokestatic 88	android/media/AudioService:access$2100	(Landroid/media/AudioService;)Ljava/util/ArrayList;
            //     566: astore 28
            //     568: aload 28
            //     570: monitorenter
            //     571: aload_2
            //     572: ldc 51
            //     574: bipush 255
            //     576: invokevirtual 41	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     579: istore 30
            //     581: aload_0
            //     582: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     585: invokestatic 88	android/media/AudioService:access$2100	(Landroid/media/AudioService;)Ljava/util/ArrayList;
            //     588: invokevirtual 150	java/util/ArrayList:isEmpty	()Z
            //     591: ifne +474 -> 1065
            //     594: aload_0
            //     595: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     598: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     601: iconst_3
            //     602: if_icmpeq +460 -> 1062
            //     605: aload_0
            //     606: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     609: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     612: iconst_1
            //     613: if_icmpeq +449 -> 1062
            //     616: aload_0
            //     617: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     620: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     623: iconst_5
            //     624: if_icmpne +441 -> 1065
            //     627: goto +435 -> 1062
            //     630: aload 28
            //     632: monitorexit
            //     633: iload 26
            //     635: ifeq -573 -> 62
            //     638: aload_0
            //     639: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     642: iload 27
            //     644: invokestatic 158	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     647: new 23	android/content/Intent
            //     650: dup
            //     651: ldc 160
            //     653: invokespecial 163	android/content/Intent:<init>	(Ljava/lang/String;)V
            //     656: astore 32
            //     658: aload 32
            //     660: ldc 165
            //     662: iload 27
            //     664: invokevirtual 169	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
            //     667: pop
            //     668: aload_0
            //     669: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     672: invokestatic 173	android/media/AudioService:access$6900	(Landroid/media/AudioService;)Landroid/content/Context;
            //     675: aload 32
            //     677: invokevirtual 179	android/content/Context:sendStickyBroadcast	(Landroid/content/Intent;)V
            //     680: goto -618 -> 62
            //     683: iconst_1
            //     684: istore 27
            //     686: aload_0
            //     687: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     690: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     693: iconst_3
            //     694: if_icmpeq -64 -> 630
            //     697: aload_0
            //     698: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     701: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     704: iconst_5
            //     705: if_icmpeq -75 -> 630
            //     708: aload_0
            //     709: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     712: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     715: iconst_4
            //     716: if_icmpeq -86 -> 630
            //     719: aload_0
            //     720: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     723: iconst_2
            //     724: invokestatic 183	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     727: pop
            //     728: goto -98 -> 630
            //     731: astore 29
            //     733: aload 28
            //     735: monitorexit
            //     736: aload 29
            //     738: athrow
            //     739: iconst_0
            //     740: istore 27
            //     742: aload_0
            //     743: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     746: iconst_0
            //     747: invokestatic 183	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     750: pop
            //     751: aload_0
            //     752: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     755: iconst_0
            //     756: iconst_0
            //     757: invokevirtual 187	android/media/AudioService:clearAllScoClients	(IZ)V
            //     760: goto -130 -> 630
            //     763: aload_0
            //     764: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     767: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     770: iconst_3
            //     771: if_icmpeq +321 -> 1092
            //     774: aload_0
            //     775: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     778: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     781: iconst_5
            //     782: if_icmpeq +310 -> 1092
            //     785: aload_0
            //     786: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     789: invokestatic 154	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     792: iconst_4
            //     793: if_icmpeq +299 -> 1092
            //     796: aload_0
            //     797: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     800: iconst_2
            //     801: invokestatic 183	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     804: pop
            //     805: goto +287 -> 1092
            //     808: aload_3
            //     809: ldc 189
            //     811: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     814: ifeq +143 -> 957
            //     817: aload_0
            //     818: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     821: iconst_1
            //     822: invokestatic 193	android/media/AudioService:access$7002	(Landroid/media/AudioService;Z)Z
            //     825: pop
            //     826: aload_0
            //     827: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     830: invokestatic 197	android/media/AudioService:access$100	(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;
            //     833: bipush 8
            //     835: iconst_1
            //     836: iconst_0
            //     837: iconst_0
            //     838: aconst_null
            //     839: iconst_0
            //     840: invokestatic 201	android/media/AudioService:access$200	(Landroid/os/Handler;IIIILjava/lang/Object;I)V
            //     843: aload_0
            //     844: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     847: aload_0
            //     848: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     851: invokestatic 173	android/media/AudioService:access$6900	(Landroid/media/AudioService;)Landroid/content/Context;
            //     854: ldc 203
            //     856: invokevirtual 207	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
            //     859: checkcast 209	android/app/KeyguardManager
            //     862: invokestatic 213	android/media/AudioService:access$7102	(Landroid/media/AudioService;Landroid/app/KeyguardManager;)Landroid/app/KeyguardManager;
            //     865: pop
            //     866: aload_0
            //     867: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     870: bipush 255
            //     872: invokestatic 216	android/media/AudioService:access$7202	(Landroid/media/AudioService;I)I
            //     875: pop
            //     876: aload_0
            //     877: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     880: invokestatic 95	android/media/AudioService:access$5900	(Landroid/media/AudioService;)V
            //     883: aload_0
            //     884: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     887: invokestatic 220	android/media/AudioService:access$2700	(Landroid/media/AudioService;)Z
            //     890: pop
            //     891: new 23	android/content/Intent
            //     894: dup
            //     895: ldc 160
            //     897: invokespecial 163	android/content/Intent:<init>	(Ljava/lang/String;)V
            //     900: astore 22
            //     902: aload 22
            //     904: ldc 165
            //     906: iconst_0
            //     907: invokevirtual 169	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
            //     910: pop
            //     911: aload_0
            //     912: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     915: invokestatic 173	android/media/AudioService:access$6900	(Landroid/media/AudioService;)Landroid/content/Context;
            //     918: aload 22
            //     920: invokevirtual 179	android/content/Context:sendStickyBroadcast	(Landroid/content/Intent;)V
            //     923: invokestatic 224	android/bluetooth/BluetoothAdapter:getDefaultAdapter	()Landroid/bluetooth/BluetoothAdapter;
            //     926: astore 24
            //     928: aload 24
            //     930: ifnull -868 -> 62
            //     933: aload 24
            //     935: aload_0
            //     936: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     939: invokestatic 173	android/media/AudioService:access$6900	(Landroid/media/AudioService;)Landroid/content/Context;
            //     942: aload_0
            //     943: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     946: invokestatic 228	android/media/AudioService:access$7300	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
            //     949: iconst_2
            //     950: invokevirtual 232	android/bluetooth/BluetoothAdapter:getProfileProxy	(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
            //     953: pop
            //     954: goto -892 -> 62
            //     957: aload_3
            //     958: ldc 234
            //     960: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     963: ifeq +39 -> 1002
            //     966: aload_2
            //     967: ldc 236
            //     969: iconst_0
            //     970: invokevirtual 240	android/content/Intent:getBooleanExtra	(Ljava/lang/String;Z)Z
            //     973: ifne -911 -> 62
            //     976: aload_2
            //     977: invokevirtual 244	android/content/Intent:getData	()Landroid/net/Uri;
            //     980: invokevirtual 249	android/net/Uri:getSchemeSpecificPart	()Ljava/lang/String;
            //     983: astore 17
            //     985: aload 17
            //     987: ifnull -925 -> 62
            //     990: aload_0
            //     991: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     994: aload 17
            //     996: invokestatic 253	android/media/AudioService:access$7400	(Landroid/media/AudioService;Ljava/lang/String;)V
            //     999: goto -937 -> 62
            //     1002: aload_3
            //     1003: ldc 255
            //     1005: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     1008: ifeq +13 -> 1021
            //     1011: ldc_w 257
            //     1014: invokestatic 261	android/media/AudioSystem:setParameters	(Ljava/lang/String;)I
            //     1017: pop
            //     1018: goto -956 -> 62
            //     1021: aload_3
            //     1022: ldc_w 263
            //     1025: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     1028: ifeq +13 -> 1041
            //     1031: ldc_w 265
            //     1034: invokestatic 261	android/media/AudioSystem:setParameters	(Ljava/lang/String;)I
            //     1037: pop
            //     1038: goto -976 -> 62
            //     1041: aload_3
            //     1042: ldc_w 267
            //     1045: invokevirtual 270	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
            //     1048: ifeq -986 -> 62
            //     1051: aload_0
            //     1052: getfield 13	android/media/AudioService$AudioServiceBroadcastReceiver:this$0	Landroid/media/AudioService;
            //     1055: aload_1
            //     1056: invokestatic 274	android/media/AudioService:access$7500	(Landroid/media/AudioService;Landroid/content/Context;)V
            //     1059: goto -997 -> 62
            //     1062: iconst_1
            //     1063: istore 26
            //     1065: iload 30
            //     1067: tableswitch	default:+25 -> 1092, 10:+-328->739, 11:+-304->763, 12:+-384->683
            //     1093: istore 26
            //     1095: goto -465 -> 630
            //
            // Exception table:
            //     from	to	target	type
            //     242	263	258	finally
            //     286	302	258	finally
            //     571	633	731	finally
            //     686	736	731	finally
            //     742	805	731	finally
        }
    }

    private class SettingsObserver extends ContentObserver
    {
        SettingsObserver()
        {
            super();
            AudioService.this.mContentResolver.registerContentObserver(Settings.System.getUriFor("mode_ringer_streams_affected"), false, this);
        }

        public void onChange(boolean paramBoolean)
        {
            super.onChange(paramBoolean);
            while (true)
            {
                int i;
                synchronized (AudioService.this.mSettingsLock)
                {
                    i = Settings.System.getInt(AudioService.this.mContentResolver, "mode_ringer_streams_affected", 166);
                    if (AudioService.this.mVoiceCapable)
                    {
                        j = i & 0xFFFFFFF7;
                        if (j != AudioService.this.mRingerModeAffectedStreams)
                        {
                            AudioService.access$6702(AudioService.this, j);
                            AudioService.this.setRingerModeInt(AudioService.this.getRingerMode(), false);
                        }
                        return;
                    }
                }
                int j = i | 0x8;
            }
        }
    }

    private class AudioHandler extends Handler
    {
        private AudioHandler()
        {
        }

        private void cleanupPlayer(MediaPlayer paramMediaPlayer)
        {
            if (paramMediaPlayer != null);
            try
            {
                paramMediaPlayer.stop();
                paramMediaPlayer.release();
                return;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                while (true)
                    Log.w("AudioService", "MediaPlayer IllegalStateException: " + localIllegalStateException);
            }
        }

        private void onHandlePersistMediaButtonReceiver(ComponentName paramComponentName)
        {
            ContentResolver localContentResolver = AudioService.this.mContentResolver;
            if (paramComponentName == null);
            for (String str = ""; ; str = paramComponentName.flattenToString())
            {
                Settings.System.putString(localContentResolver, "media_button_receiver", str);
                return;
            }
        }

        private void persistRingerMode(int paramInt)
        {
            Settings.System.putInt(AudioService.this.mContentResolver, "mode_ringer", paramInt);
        }

        private void persistVolume(AudioService.VolumeStreamState paramVolumeStreamState, int paramInt1, int paramInt2)
        {
            if ((paramInt1 & 0x1) != 0)
                Settings.System.putInt(AudioService.this.mContentResolver, paramVolumeStreamState.getSettingNameForDevice(false, paramInt2), (5 + paramVolumeStreamState.getIndex(paramInt2, false)) / 10);
            if ((paramInt1 & 0x2) != 0)
                Settings.System.putInt(AudioService.this.mContentResolver, paramVolumeStreamState.getSettingNameForDevice(true, paramInt2), (5 + paramVolumeStreamState.getIndex(paramInt2, true)) / 10);
        }

        // ERROR //
        private void playSoundEffect(int paramInt1, int paramInt2)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     4: invokestatic 114	android/media/AudioService:access$1500	(Landroid/media/AudioService;)Ljava/lang/Object;
            //     7: astore_3
            //     8: aload_3
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     14: invokestatic 118	android/media/AudioService:access$1600	(Landroid/media/AudioService;)Landroid/media/SoundPool;
            //     17: ifnonnull +8 -> 25
            //     20: aload_3
            //     21: monitorexit
            //     22: goto +288 -> 310
            //     25: iload_2
            //     26: ifge +74 -> 100
            //     29: ldc2_w 119
            //     32: invokestatic 124	android/media/AudioService:access$4400	()I
            //     35: bipush 20
            //     37: idiv
            //     38: i2d
            //     39: invokestatic 130	java/lang/Math:pow	(DD)D
            //     42: d2f
            //     43: fstore 5
            //     45: aload_0
            //     46: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     49: invokestatic 134	android/media/AudioService:access$4500	(Landroid/media/AudioService;)[[I
            //     52: iload_1
            //     53: aaload
            //     54: iconst_1
            //     55: iaload
            //     56: ifle +54 -> 110
            //     59: aload_0
            //     60: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     63: invokestatic 118	android/media/AudioService:access$1600	(Landroid/media/AudioService;)Landroid/media/SoundPool;
            //     66: aload_0
            //     67: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     70: invokestatic 134	android/media/AudioService:access$4500	(Landroid/media/AudioService;)[[I
            //     73: iload_1
            //     74: aaload
            //     75: iconst_1
            //     76: iaload
            //     77: fload 5
            //     79: fload 5
            //     81: iconst_0
            //     82: iconst_0
            //     83: fconst_1
            //     84: invokevirtual 140	android/media/SoundPool:play	(IFFIIF)I
            //     87: pop
            //     88: aload_3
            //     89: monitorexit
            //     90: goto +220 -> 310
            //     93: astore 4
            //     95: aload_3
            //     96: monitorexit
            //     97: aload 4
            //     99: athrow
            //     100: iload_2
            //     101: i2f
            //     102: ldc 141
            //     104: fdiv
            //     105: fstore 5
            //     107: goto -62 -> 45
            //     110: new 33	android/media/MediaPlayer
            //     113: dup
            //     114: invokespecial 142	android/media/MediaPlayer:<init>	()V
            //     117: astore 6
            //     119: aload 6
            //     121: new 43	java/lang/StringBuilder
            //     124: dup
            //     125: invokespecial 44	java/lang/StringBuilder:<init>	()V
            //     128: invokestatic 148	android/os/Environment:getRootDirectory	()Ljava/io/File;
            //     131: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     134: ldc 150
            //     136: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     139: invokestatic 154	android/media/AudioService:access$4600	()[Ljava/lang/String;
            //     142: aload_0
            //     143: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     146: invokestatic 134	android/media/AudioService:access$4500	(Landroid/media/AudioService;)[[I
            //     149: iload_1
            //     150: aaload
            //     151: iconst_0
            //     152: iaload
            //     153: aaload
            //     154: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     157: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     160: invokevirtual 158	android/media/MediaPlayer:setDataSource	(Ljava/lang/String;)V
            //     163: aload 6
            //     165: iconst_1
            //     166: invokevirtual 161	android/media/MediaPlayer:setAudioStreamType	(I)V
            //     169: aload 6
            //     171: invokevirtual 164	android/media/MediaPlayer:prepare	()V
            //     174: aload 6
            //     176: fload 5
            //     178: fload 5
            //     180: invokevirtual 168	android/media/MediaPlayer:setVolume	(FF)V
            //     183: aload 6
            //     185: new 6	android/media/AudioService$AudioHandler$1
            //     188: dup
            //     189: aload_0
            //     190: invokespecial 171	android/media/AudioService$AudioHandler$1:<init>	(Landroid/media/AudioService$AudioHandler;)V
            //     193: invokevirtual 175	android/media/MediaPlayer:setOnCompletionListener	(Landroid/media/MediaPlayer$OnCompletionListener;)V
            //     196: aload 6
            //     198: new 8	android/media/AudioService$AudioHandler$2
            //     201: dup
            //     202: aload_0
            //     203: invokespecial 176	android/media/AudioService$AudioHandler$2:<init>	(Landroid/media/AudioService$AudioHandler;)V
            //     206: invokevirtual 180	android/media/MediaPlayer:setOnErrorListener	(Landroid/media/MediaPlayer$OnErrorListener;)V
            //     209: aload 6
            //     211: invokevirtual 183	android/media/MediaPlayer:start	()V
            //     214: goto -126 -> 88
            //     217: astore 11
            //     219: ldc 41
            //     221: new 43	java/lang/StringBuilder
            //     224: dup
            //     225: invokespecial 44	java/lang/StringBuilder:<init>	()V
            //     228: ldc 185
            //     230: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     233: aload 11
            //     235: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     238: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     241: invokestatic 63	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     244: pop
            //     245: goto -157 -> 88
            //     248: astore 9
            //     250: ldc 41
            //     252: new 43	java/lang/StringBuilder
            //     255: dup
            //     256: invokespecial 44	java/lang/StringBuilder:<init>	()V
            //     259: ldc 187
            //     261: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     264: aload 9
            //     266: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     269: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     272: invokestatic 63	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     275: pop
            //     276: goto -188 -> 88
            //     279: astore 7
            //     281: ldc 41
            //     283: new 43	java/lang/StringBuilder
            //     286: dup
            //     287: invokespecial 44	java/lang/StringBuilder:<init>	()V
            //     290: ldc 46
            //     292: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     295: aload 7
            //     297: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     300: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     303: invokestatic 63	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     306: pop
            //     307: goto -219 -> 88
            //     310: return
            //
            // Exception table:
            //     from	to	target	type
            //     10	97	93	finally
            //     110	119	93	finally
            //     119	214	93	finally
            //     219	307	93	finally
            //     119	214	217	java/io/IOException
            //     119	214	248	java/lang/IllegalArgumentException
            //     119	214	279	java/lang/IllegalStateException
        }

        private void setAllVolumes(AudioService.VolumeStreamState paramVolumeStreamState)
        {
            paramVolumeStreamState.applyAllVolumes();
            for (int i = -1 + AudioSystem.getNumStreamTypes(); i >= 0; i--)
                if ((i != AudioService.VolumeStreamState.access$4100(paramVolumeStreamState)) && (AudioService.this.mStreamVolumeAlias[i] == AudioService.VolumeStreamState.access$4100(paramVolumeStreamState)))
                    AudioService.this.mStreamStates[i].applyAllVolumes();
        }

        private void setDeviceVolume(AudioService.VolumeStreamState paramVolumeStreamState, int paramInt)
        {
            paramVolumeStreamState.applyDeviceVolume(paramInt);
            for (int i = -1 + AudioSystem.getNumStreamTypes(); i >= 0; i--)
                if ((i != AudioService.VolumeStreamState.access$4100(paramVolumeStreamState)) && (AudioService.this.mStreamVolumeAlias[i] == AudioService.VolumeStreamState.access$4100(paramVolumeStreamState)))
                    AudioService.this.mStreamStates[i].applyDeviceVolume(AudioService.this.getDeviceForStream(i));
            AudioService.sendMsg(AudioService.this.mAudioHandler, 1, 2, 3, paramInt, paramVolumeStreamState, 500);
        }

        private void setForceUse(int paramInt1, int paramInt2)
        {
            AudioSystem.setForceUse(paramInt1, paramInt2);
        }

        // ERROR //
        public void handleMessage(Message paramMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 240	android/os/Message:what	I
            //     4: tableswitch	default:+104 -> 108, 0:+105->109, 1:+137->141, 2:+159->163, 3:+205->209, 4:+219->223, 5:+269->273, 6:+580->584, 7:+595->599, 8:+569->573, 9:+635->639, 10:+650->654, 11:+695->699, 12:+664->668, 13:+674->678, 14:+123->127, 15:+184->188, 16:+771->775, 17:+874->878, 18:+884->888, 19:+912->916, 20:+705->709, 21:+740->744
            //     109: aload_0
            //     110: aload_1
            //     111: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     114: checkcast 96	android/media/AudioService$VolumeStreamState
            //     117: aload_1
            //     118: getfield 247	android/os/Message:arg1	I
            //     121: invokespecial 249	android/media/AudioService$AudioHandler:setDeviceVolume	(Landroid/media/AudioService$VolumeStreamState;I)V
            //     124: goto -16 -> 108
            //     127: aload_0
            //     128: aload_1
            //     129: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     132: checkcast 96	android/media/AudioService$VolumeStreamState
            //     135: invokespecial 251	android/media/AudioService$AudioHandler:setAllVolumes	(Landroid/media/AudioService$VolumeStreamState;)V
            //     138: goto -30 -> 108
            //     141: aload_0
            //     142: aload_1
            //     143: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     146: checkcast 96	android/media/AudioService$VolumeStreamState
            //     149: aload_1
            //     150: getfield 247	android/os/Message:arg1	I
            //     153: aload_1
            //     154: getfield 254	android/os/Message:arg2	I
            //     157: invokespecial 256	android/media/AudioService$AudioHandler:persistVolume	(Landroid/media/AudioService$VolumeStreamState;II)V
            //     160: goto -52 -> 108
            //     163: aload_0
            //     164: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     167: invokestatic 69	android/media/AudioService:access$3300	(Landroid/media/AudioService;)Landroid/content/ContentResolver;
            //     170: ldc_w 258
            //     173: aload_1
            //     174: getfield 247	android/os/Message:arg1	I
            //     177: i2f
            //     178: ldc 141
            //     180: fdiv
            //     181: invokestatic 262	android/provider/Settings$System:putFloat	(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
            //     184: pop
            //     185: goto -77 -> 108
            //     188: aload_0
            //     189: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     192: invokestatic 69	android/media/AudioService:access$3300	(Landroid/media/AudioService;)Landroid/content/ContentResolver;
            //     195: ldc_w 264
            //     198: aload_1
            //     199: getfield 247	android/os/Message:arg1	I
            //     202: invokestatic 92	android/provider/Settings$System:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
            //     205: pop
            //     206: goto -98 -> 108
            //     209: aload_0
            //     210: aload_0
            //     211: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     214: invokevirtual 267	android/media/AudioService:getRingerMode	()I
            //     217: invokespecial 269	android/media/AudioService$AudioHandler:persistRingerMode	(I)V
            //     220: goto -112 -> 108
            //     223: aload_0
            //     224: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     227: invokestatic 273	android/media/AudioService:access$000	(Landroid/media/AudioService;)Z
            //     230: ifne -122 -> 108
            //     233: ldc 41
            //     235: ldc_w 275
            //     238: invokestatic 278	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     241: pop
            //     242: aload_0
            //     243: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     246: invokestatic 282	android/media/AudioService:access$4800	(Landroid/media/AudioService;)Landroid/media/AudioSystem$ErrorCallback;
            //     249: invokestatic 286	android/media/AudioSystem:setErrorCallback	(Landroid/media/AudioSystem$ErrorCallback;)V
            //     252: aload_0
            //     253: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     256: invokestatic 222	android/media/AudioService:access$100	(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;
            //     259: iconst_4
            //     260: iconst_1
            //     261: iconst_0
            //     262: iconst_0
            //     263: aconst_null
            //     264: sipush 500
            //     267: invokestatic 226	android/media/AudioService:access$200	(Landroid/os/Handler;IIIILjava/lang/Object;I)V
            //     270: goto -162 -> 108
            //     273: ldc 41
            //     275: ldc_w 288
            //     278: invokestatic 278	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     281: pop
            //     282: ldc_w 290
            //     285: invokestatic 294	android/media/AudioSystem:setParameters	(Ljava/lang/String;)I
            //     288: pop
            //     289: aload_0
            //     290: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     293: invokestatic 298	android/media/AudioService:access$2800	(Landroid/media/AudioService;)Ljava/util/HashMap;
            //     296: astore 13
            //     298: aload 13
            //     300: monitorenter
            //     301: aload_0
            //     302: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     305: invokestatic 298	android/media/AudioService:access$2800	(Landroid/media/AudioService;)Ljava/util/HashMap;
            //     308: invokevirtual 304	java/util/HashMap:entrySet	()Ljava/util/Set;
            //     311: invokeinterface 310 1 0
            //     316: astore 15
            //     318: aload 15
            //     320: invokeinterface 316 1 0
            //     325: ifeq +54 -> 379
            //     328: aload 15
            //     330: invokeinterface 320 1 0
            //     335: checkcast 322	java/util/Map$Entry
            //     338: astore 28
            //     340: aload 28
            //     342: invokeinterface 325 1 0
            //     347: checkcast 327	java/lang/Integer
            //     350: invokevirtual 330	java/lang/Integer:intValue	()I
            //     353: iconst_1
            //     354: aload 28
            //     356: invokeinterface 333 1 0
            //     361: checkcast 335	java/lang/String
            //     364: invokestatic 339	android/media/AudioSystem:setDeviceConnectionState	(IILjava/lang/String;)I
            //     367: pop
            //     368: goto -50 -> 318
            //     371: astore 14
            //     373: aload 13
            //     375: monitorexit
            //     376: aload 14
            //     378: athrow
            //     379: aload 13
            //     381: monitorexit
            //     382: aload_0
            //     383: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     386: invokestatic 343	android/media/AudioService:access$4900	(Landroid/media/AudioService;)I
            //     389: invokestatic 347	android/media/AudioSystem:setPhoneState	(I)I
            //     392: pop
            //     393: iconst_0
            //     394: aload_0
            //     395: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     398: invokestatic 350	android/media/AudioService:access$5000	(Landroid/media/AudioService;)I
            //     401: invokestatic 230	android/media/AudioSystem:setForceUse	(II)I
            //     404: pop
            //     405: iconst_2
            //     406: aload_0
            //     407: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     410: invokestatic 350	android/media/AudioService:access$5000	(Landroid/media/AudioService;)I
            //     413: invokestatic 230	android/media/AudioSystem:setForceUse	(II)I
            //     416: pop
            //     417: bipush 255
            //     419: invokestatic 197	android/media/AudioSystem:getNumStreamTypes	()I
            //     422: iadd
            //     423: istore 19
            //     425: iload 19
            //     427: iflt +47 -> 474
            //     430: aload_0
            //     431: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     434: invokestatic 209	android/media/AudioService:access$3900	(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;
            //     437: iload 19
            //     439: aaload
            //     440: astore 25
            //     442: iconst_5
            //     443: aload 25
            //     445: invokestatic 353	android/media/AudioService$VolumeStreamState:access$5100	(Landroid/media/AudioService$VolumeStreamState;)I
            //     448: iadd
            //     449: bipush 10
            //     451: idiv
            //     452: istore 26
            //     454: iload 19
            //     456: iconst_0
            //     457: iload 26
            //     459: invokestatic 357	android/media/AudioSystem:initStreamVolume	(III)I
            //     462: pop
            //     463: aload 25
            //     465: invokevirtual 192	android/media/AudioService$VolumeStreamState:applyAllVolumes	()V
            //     468: iinc 19 255
            //     471: goto -46 -> 425
            //     474: aload_0
            //     475: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     478: aload_0
            //     479: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     482: invokevirtual 267	android/media/AudioService:getRingerMode	()I
            //     485: iconst_0
            //     486: invokestatic 361	android/media/AudioService:access$5200	(Landroid/media/AudioService;IZ)V
            //     489: aload_0
            //     490: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     493: invokestatic 364	android/media/AudioService:access$5300	(Landroid/media/AudioService;)V
            //     496: ldc_w 366
            //     499: iconst_0
            //     500: invokestatic 372	android/os/SystemProperties:getBoolean	(Ljava/lang/String;Z)Z
            //     503: ifeq +10 -> 513
            //     506: aload_0
            //     507: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     510: invokestatic 375	android/media/AudioService:access$5400	(Landroid/media/AudioService;)V
            //     513: aload_0
            //     514: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     517: invokestatic 378	android/media/AudioService:access$5500	(Landroid/media/AudioService;)Ljava/lang/Object;
            //     520: astore 20
            //     522: aload 20
            //     524: monitorenter
            //     525: aload_0
            //     526: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     529: invokestatic 381	android/media/AudioService:access$5600	(Landroid/media/AudioService;)Z
            //     532: ifeq +26 -> 558
            //     535: iconst_0
            //     536: istore 22
            //     538: iconst_1
            //     539: iload 22
            //     541: invokestatic 230	android/media/AudioSystem:setForceUse	(II)I
            //     544: pop
            //     545: aload 20
            //     547: monitorexit
            //     548: ldc_w 383
            //     551: invokestatic 294	android/media/AudioSystem:setParameters	(Ljava/lang/String;)I
            //     554: pop
            //     555: goto -447 -> 108
            //     558: bipush 10
            //     560: istore 22
            //     562: goto -24 -> 538
            //     565: astore 21
            //     567: aload 20
            //     569: monitorexit
            //     570: aload 21
            //     572: athrow
            //     573: aload_0
            //     574: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     577: invokevirtual 386	android/media/AudioService:loadSoundEffects	()Z
            //     580: pop
            //     581: goto -473 -> 108
            //     584: aload_0
            //     585: aload_1
            //     586: getfield 247	android/os/Message:arg1	I
            //     589: aload_1
            //     590: getfield 254	android/os/Message:arg2	I
            //     593: invokespecial 388	android/media/AudioService$AudioHandler:playSoundEffect	(II)V
            //     596: goto -488 -> 108
            //     599: aload_0
            //     600: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     603: invokestatic 298	android/media/AudioService:access$2800	(Landroid/media/AudioService;)Ljava/util/HashMap;
            //     606: astore 8
            //     608: aload 8
            //     610: monitorenter
            //     611: aload_0
            //     612: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     615: aload_1
            //     616: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     619: checkcast 335	java/lang/String
            //     622: invokestatic 392	android/media/AudioService:access$3100	(Landroid/media/AudioService;Ljava/lang/String;)V
            //     625: aload 8
            //     627: monitorexit
            //     628: goto -520 -> 108
            //     631: astore 9
            //     633: aload 8
            //     635: monitorexit
            //     636: aload 9
            //     638: athrow
            //     639: aload_0
            //     640: aload_1
            //     641: getfield 247	android/os/Message:arg1	I
            //     644: aload_1
            //     645: getfield 254	android/os/Message:arg2	I
            //     648: invokespecial 394	android/media/AudioService$AudioHandler:setForceUse	(II)V
            //     651: goto -543 -> 108
            //     654: aload_0
            //     655: aload_1
            //     656: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     659: checkcast 81	android/content/ComponentName
            //     662: invokespecial 396	android/media/AudioService$AudioHandler:onHandlePersistMediaButtonReceiver	(Landroid/content/ComponentName;)V
            //     665: goto -557 -> 108
            //     668: aload_0
            //     669: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     672: invokestatic 399	android/media/AudioService:access$5700	(Landroid/media/AudioService;)V
            //     675: goto -567 -> 108
            //     678: aload_0
            //     679: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     682: aload_1
            //     683: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     686: checkcast 401	android/media/AudioService$RemoteControlStackEntry
            //     689: aload_1
            //     690: getfield 247	android/os/Message:arg1	I
            //     693: invokestatic 405	android/media/AudioService:access$5800	(Landroid/media/AudioService;Landroid/media/AudioService$RemoteControlStackEntry;I)V
            //     696: goto -588 -> 108
            //     699: aload_0
            //     700: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     703: invokestatic 408	android/media/AudioService:access$5900	(Landroid/media/AudioService;)V
            //     706: goto -598 -> 108
            //     709: aload_0
            //     710: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     713: aload_1
            //     714: getfield 247	android/os/Message:arg1	I
            //     717: aload_1
            //     718: getfield 254	android/os/Message:arg2	I
            //     721: aload_1
            //     722: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     725: checkcast 335	java/lang/String
            //     728: invokestatic 412	android/media/AudioService:access$6000	(Landroid/media/AudioService;IILjava/lang/String;)V
            //     731: aload_0
            //     732: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     735: invokestatic 416	android/media/AudioService:access$6100	(Landroid/media/AudioService;)Landroid/os/PowerManager$WakeLock;
            //     738: invokevirtual 419	android/os/PowerManager$WakeLock:release	()V
            //     741: goto -633 -> 108
            //     744: aload_0
            //     745: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     748: aload_1
            //     749: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     752: checkcast 421	android/bluetooth/BluetoothDevice
            //     755: aload_1
            //     756: getfield 247	android/os/Message:arg1	I
            //     759: invokestatic 425	android/media/AudioService:access$6200	(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;I)V
            //     762: aload_0
            //     763: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     766: invokestatic 416	android/media/AudioService:access$6100	(Landroid/media/AudioService;)Landroid/os/PowerManager$WakeLock;
            //     769: invokevirtual 419	android/os/PowerManager$WakeLock:release	()V
            //     772: goto -664 -> 108
            //     775: aload_0
            //     776: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     779: getfield 429	android/media/AudioService:mRoutesObservers	Landroid/os/RemoteCallbackList;
            //     782: invokevirtual 434	android/os/RemoteCallbackList:beginBroadcast	()I
            //     785: istore_2
            //     786: iload_2
            //     787: ifle +78 -> 865
            //     790: aload_0
            //     791: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     794: getfield 438	android/media/AudioService:mCurAudioRoutes	Landroid/media/AudioRoutesInfo;
            //     797: astore_3
            //     798: aload_3
            //     799: monitorenter
            //     800: new 440	android/media/AudioRoutesInfo
            //     803: dup
            //     804: aload_0
            //     805: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     808: getfield 438	android/media/AudioService:mCurAudioRoutes	Landroid/media/AudioRoutesInfo;
            //     811: invokespecial 443	android/media/AudioRoutesInfo:<init>	(Landroid/media/AudioRoutesInfo;)V
            //     814: astore 4
            //     816: aload_3
            //     817: monitorexit
            //     818: iload_2
            //     819: ifle +46 -> 865
            //     822: iinc 2 255
            //     825: aload_0
            //     826: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     829: getfield 429	android/media/AudioService:mRoutesObservers	Landroid/os/RemoteCallbackList;
            //     832: iload_2
            //     833: invokevirtual 447	android/os/RemoteCallbackList:getBroadcastItem	(I)Landroid/os/IInterface;
            //     836: checkcast 449	android/media/IAudioRoutesObserver
            //     839: astore 6
            //     841: aload 6
            //     843: aload 4
            //     845: invokeinterface 452 2 0
            //     850: goto -32 -> 818
            //     853: astore 7
            //     855: goto -37 -> 818
            //     858: astore 5
            //     860: aload_3
            //     861: monitorexit
            //     862: aload 5
            //     864: athrow
            //     865: aload_0
            //     866: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     869: getfield 429	android/media/AudioService:mRoutesObservers	Landroid/os/RemoteCallbackList;
            //     872: invokevirtual 455	android/os/RemoteCallbackList:finishBroadcast	()V
            //     875: goto -767 -> 108
            //     878: aload_0
            //     879: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     882: invokestatic 458	android/media/AudioService:access$6300	(Landroid/media/AudioService;)V
            //     885: goto -777 -> 108
            //     888: aload_0
            //     889: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     892: aload_1
            //     893: getfield 247	android/os/Message:arg1	I
            //     896: aload_1
            //     897: getfield 254	android/os/Message:arg2	I
            //     900: aload_1
            //     901: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     904: checkcast 327	java/lang/Integer
            //     907: invokevirtual 330	java/lang/Integer:intValue	()I
            //     910: invokestatic 462	android/media/AudioService:access$6400	(Landroid/media/AudioService;III)V
            //     913: goto -805 -> 108
            //     916: aload_0
            //     917: getfield 17	android/media/AudioService$AudioHandler:this$0	Landroid/media/AudioService;
            //     920: aload_1
            //     921: getfield 247	android/os/Message:arg1	I
            //     924: aload_1
            //     925: getfield 244	android/os/Message:obj	Ljava/lang/Object;
            //     928: checkcast 464	android/media/IRemoteVolumeObserver
            //     931: invokestatic 468	android/media/AudioService:access$6500	(Landroid/media/AudioService;ILandroid/media/IRemoteVolumeObserver;)V
            //     934: goto -826 -> 108
            //
            // Exception table:
            //     from	to	target	type
            //     301	376	371	finally
            //     379	382	371	finally
            //     525	548	565	finally
            //     567	570	565	finally
            //     611	636	631	finally
            //     841	850	853	android/os/RemoteException
            //     800	818	858	finally
            //     860	862	858	finally
        }
    }

    private class AudioSystemThread extends Thread
    {
        AudioSystemThread()
        {
            super();
        }

        public void run()
        {
            Looper.prepare();
            synchronized (AudioService.this)
            {
                AudioService.access$102(AudioService.this, new AudioService.AudioHandler(AudioService.this, null));
                AudioService.this.notify();
                Looper.loop();
                return;
            }
        }
    }

    public class VolumeStreamState
    {
        private ArrayList<VolumeDeathHandler> mDeathHandlers;
        private final ConcurrentHashMap<Integer, Integer> mIndex = new ConcurrentHashMap(8, 0.75F, 4);
        private int mIndexMax;
        private final ConcurrentHashMap<Integer, Integer> mLastAudibleIndex = new ConcurrentHashMap(8, 0.75F, 4);
        private String mLastAudibleVolumeIndexSettingName;
        private final int mStreamType;
        private String mVolumeIndexSettingName;

        private VolumeStreamState(String paramInt, int arg3)
        {
            this.mVolumeIndexSettingName = paramInt;
            this.mLastAudibleVolumeIndexSettingName = (paramInt + "_last_audible");
            int i;
            this.mStreamType = i;
            this.mIndexMax = AudioService.this.MAX_STREAM_VOLUME[i];
            AudioSystem.initStreamVolume(i, 0, this.mIndexMax);
            this.mIndexMax = (10 * this.mIndexMax);
            readSettings();
            this.mDeathHandlers = new ArrayList();
        }

        private void dump(PrintWriter paramPrintWriter)
        {
            paramPrintWriter.print("     Current: ");
            Iterator localIterator1 = this.mIndex.entrySet().iterator();
            while (localIterator1.hasNext())
            {
                Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
                paramPrintWriter.print(Integer.toHexString(((Integer)localEntry2.getKey()).intValue()) + ": " + (5 + ((Integer)localEntry2.getValue()).intValue()) / 10 + ", ");
            }
            paramPrintWriter.print("\n     Last audible: ");
            Iterator localIterator2 = this.mLastAudibleIndex.entrySet().iterator();
            while (localIterator2.hasNext())
            {
                Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
                paramPrintWriter.print(Integer.toHexString(((Integer)localEntry1.getKey()).intValue()) + ": " + (5 + ((Integer)localEntry1.getValue()).intValue()) / 10 + ", ");
            }
        }

        private VolumeDeathHandler getDeathHandler(IBinder paramIBinder, boolean paramBoolean)
        {
            int i = this.mDeathHandlers.size();
            Object localObject;
            for (int j = 0; j < i; j++)
            {
                VolumeDeathHandler localVolumeDeathHandler2 = (VolumeDeathHandler)this.mDeathHandlers.get(j);
                if (paramIBinder == localVolumeDeathHandler2.mICallback)
                {
                    localObject = localVolumeDeathHandler2;
                    return localObject;
                }
            }
            if (paramBoolean);
            for (VolumeDeathHandler localVolumeDeathHandler1 = new VolumeDeathHandler(paramIBinder); ; localVolumeDeathHandler1 = null)
            {
                localObject = localVolumeDeathHandler1;
                break;
                Log.w("AudioService", "stream was not muted by this client");
            }
        }

        private int getValidIndex(int paramInt)
        {
            if (paramInt < 0)
                paramInt = 0;
            while (true)
            {
                return paramInt;
                if (paramInt > this.mIndexMax)
                    paramInt = this.mIndexMax;
            }
        }

        /** @deprecated */
        private int muteCount()
        {
            int i = 0;
            try
            {
                int j = this.mDeathHandlers.size();
                for (int k = 0; k < j; k++)
                {
                    int m = ((VolumeDeathHandler)this.mDeathHandlers.get(k)).mMuteCount;
                    i += m;
                }
                return i;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public boolean adjustIndex(int paramInt1, int paramInt2)
        {
            return setIndex(paramInt1 + getIndex(paramInt2, false), paramInt2, true);
        }

        /** @deprecated */
        public void adjustLastAudibleIndex(int paramInt1, int paramInt2)
        {
            try
            {
                setLastAudibleIndex(paramInt1 + getIndex(paramInt2, true), paramInt2);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public void applyAllVolumes()
        {
            try
            {
                AudioSystem.setStreamVolumeIndex(this.mStreamType, (5 + getIndex(32768, false)) / 10, 32768);
                Iterator localIterator = this.mIndex.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    int i = ((Integer)localEntry.getKey()).intValue();
                    if (i != 32768)
                        AudioSystem.setStreamVolumeIndex(this.mStreamType, (5 + ((Integer)localEntry.getValue()).intValue()) / 10, i);
                }
            }
            finally
            {
            }
        }

        public void applyDeviceVolume(int paramInt)
        {
            AudioSystem.setStreamVolumeIndex(this.mStreamType, (5 + getIndex(paramInt, false)) / 10, paramInt);
        }

        public ConcurrentHashMap<Integer, Integer> getAllIndexes(boolean paramBoolean)
        {
            if (paramBoolean);
            for (ConcurrentHashMap localConcurrentHashMap = this.mLastAudibleIndex; ; localConcurrentHashMap = this.mIndex)
                return localConcurrentHashMap;
        }

        /** @deprecated */
        public int getIndex(int paramInt, boolean paramBoolean)
        {
            if (paramBoolean);
            try
            {
                for (ConcurrentHashMap localConcurrentHashMap = this.mLastAudibleIndex; ; localConcurrentHashMap = this.mIndex)
                {
                    Integer localInteger = (Integer)localConcurrentHashMap.get(Integer.valueOf(paramInt));
                    if (localInteger == null)
                        localInteger = (Integer)localConcurrentHashMap.get(Integer.valueOf(32768));
                    int i = localInteger.intValue();
                    return i;
                }
            }
            finally
            {
            }
        }

        public int getMaxIndex()
        {
            return this.mIndexMax;
        }

        public String getSettingNameForDevice(boolean paramBoolean, int paramInt)
        {
            String str1;
            String str2;
            if (paramBoolean)
            {
                str1 = this.mLastAudibleVolumeIndexSettingName;
                str2 = AudioSystem.getDeviceName(paramInt);
                if (!str2.isEmpty())
                    break label33;
            }
            while (true)
            {
                return str1;
                str1 = this.mVolumeIndexSettingName;
                break;
                label33: str1 = str1 + "_" + str2;
            }
        }

        public int getStreamType()
        {
            return this.mStreamType;
        }

        /** @deprecated */
        public void mute(IBinder paramIBinder, boolean paramBoolean)
        {
            try
            {
                VolumeDeathHandler localVolumeDeathHandler = getDeathHandler(paramIBinder, paramBoolean);
                if (localVolumeDeathHandler == null)
                    Log.e("AudioService", "Could not get client death handler for stream: " + this.mStreamType);
                while (true)
                {
                    return;
                    localVolumeDeathHandler.mute(paramBoolean);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public void readSettings()
        {
            int i = 65535;
            int j = 0;
            while (i != 0)
            {
                int k = 1 << j;
                if ((k & i) == 0)
                {
                    j++;
                }
                else
                {
                    i &= (k ^ 0xFFFFFFFF);
                    while (true)
                    {
                        try
                        {
                            String str1 = getSettingNameForDevice(false, k);
                            if (k == 32768)
                            {
                                m = AudioManager.DEFAULT_STREAM_VOLUME[this.mStreamType];
                                int n = Settings.System.getInt(AudioService.this.mContentResolver, str1, m);
                                if (n == -1)
                                    break;
                                String str2 = getSettingNameForDevice(true, k);
                                if (n <= 0)
                                    break label334;
                                i1 = n;
                                int i2 = Settings.System.getInt(AudioService.this.mContentResolver, str2, i1);
                                if ((i2 == 0) && (((AudioService.this.mVoiceCapable) && (AudioService.this.mStreamVolumeAlias[this.mStreamType] == 2)) || (AudioService.this.mStreamVolumeAlias[this.mStreamType] == 1)))
                                {
                                    i2 = AudioManager.DEFAULT_STREAM_VOLUME[this.mStreamType];
                                    AudioService.sendMsg(AudioService.this.mAudioHandler, 1, 2, 2, k, this, 500);
                                }
                                this.mLastAudibleIndex.put(Integer.valueOf(k), Integer.valueOf(getValidIndex(i2 * 10)));
                                if ((n == 0) && (AudioService.this.mRingerMode == 2) && (((AudioService.this.mVoiceCapable) && (AudioService.this.mStreamVolumeAlias[this.mStreamType] == 2)) || (AudioService.this.mStreamVolumeAlias[this.mStreamType] == 1)))
                                {
                                    n = i2;
                                    AudioService.sendMsg(AudioService.this.mAudioHandler, 1, 2, 1, k, this, 500);
                                }
                                this.mIndex.put(Integer.valueOf(k), Integer.valueOf(getValidIndex(n * 10)));
                                break;
                            }
                        }
                        finally
                        {
                        }
                        int m = -1;
                        continue;
                        label334: int i1 = AudioManager.DEFAULT_STREAM_VOLUME[this.mStreamType];
                    }
                }
            }
        }

        /** @deprecated */
        public void setAllIndexes(VolumeStreamState paramVolumeStreamState, boolean paramBoolean)
        {
            try
            {
                Iterator localIterator = paramVolumeStreamState.getAllIndexes(paramBoolean).entrySet().iterator();
                if (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    int i = ((Integer)localEntry.getKey()).intValue();
                    int j = ((Integer)localEntry.getValue()).intValue();
                    setIndex(AudioService.this.rescaleIndex(j, paramVolumeStreamState.getStreamType(), this.mStreamType), i, paramBoolean);
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        public boolean setIndex(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            boolean bool1 = true;
            try
            {
                int i = getIndex(paramInt2, false);
                int j = getValidIndex(paramInt1);
                this.mIndex.put(Integer.valueOf(paramInt2), Integer.valueOf(getValidIndex(j)));
                if (i != j)
                {
                    if (paramBoolean)
                        this.mLastAudibleIndex.put(Integer.valueOf(paramInt2), Integer.valueOf(j));
                    if (paramInt2 == AudioService.this.getDeviceForStream(this.mStreamType));
                    for (boolean bool2 = bool1; ; bool2 = false)
                        for (int k = -1 + AudioSystem.getNumStreamTypes(); k >= 0; k--)
                            if ((k != this.mStreamType) && (AudioService.this.mStreamVolumeAlias[k] == this.mStreamType))
                            {
                                int m = AudioService.this.rescaleIndex(j, this.mStreamType, k);
                                AudioService.this.mStreamStates[k].setIndex(m, paramInt2, paramBoolean);
                                if (bool2)
                                    AudioService.this.mStreamStates[k].setIndex(m, AudioService.this.getDeviceForStream(k), paramBoolean);
                            }
                }
                bool1 = false;
                return bool1;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public void setLastAudibleIndex(int paramInt1, int paramInt2)
        {
            while (true)
            {
                int j;
                try
                {
                    if (paramInt2 != AudioService.this.getDeviceForStream(this.mStreamType))
                        break label157;
                    i = 1;
                    j = -1 + AudioSystem.getNumStreamTypes();
                    if (j >= 0)
                    {
                        if ((j != this.mStreamType) && (AudioService.this.mStreamVolumeAlias[j] == this.mStreamType))
                        {
                            int k = AudioService.this.rescaleIndex(paramInt1, this.mStreamType, j);
                            AudioService.this.mStreamStates[j].setLastAudibleIndex(k, paramInt2);
                            if (i != 0)
                                AudioService.this.mStreamStates[j].setLastAudibleIndex(k, AudioService.this.getDeviceForStream(j));
                        }
                    }
                    else
                    {
                        this.mLastAudibleIndex.put(Integer.valueOf(paramInt2), Integer.valueOf(getValidIndex(paramInt1)));
                        return;
                    }
                }
                finally
                {
                    localObject = finally;
                    throw localObject;
                }
                j--;
                continue;
                label157: int i = 0;
            }
        }

        private class VolumeDeathHandler
            implements IBinder.DeathRecipient
        {
            private IBinder mICallback;
            private int mMuteCount;

            VolumeDeathHandler(IBinder arg2)
            {
                Object localObject;
                this.mICallback = localObject;
            }

            public void binderDied()
            {
                Log.w("AudioService", "Volume service client died for stream: " + AudioService.VolumeStreamState.this.mStreamType);
                if (this.mMuteCount != 0)
                {
                    this.mMuteCount = 1;
                    mute(false);
                }
            }

            public void mute(boolean paramBoolean)
            {
                if (paramBoolean)
                    if (this.mMuteCount == 0)
                        try
                        {
                            if (this.mICallback != null)
                                this.mICallback.linkToDeath(this, 0);
                            AudioService.VolumeStreamState.this.mDeathHandlers.add(this);
                            if (AudioService.VolumeStreamState.this.muteCount() == 0)
                            {
                                Iterator localIterator2 = AudioService.VolumeStreamState.this.mIndex.entrySet().iterator();
                                while (localIterator2.hasNext())
                                {
                                    int j = ((Integer)((Map.Entry)localIterator2.next()).getKey()).intValue();
                                    AudioService.VolumeStreamState.this.setIndex(0, j, false);
                                }
                            }
                        }
                        catch (RemoteException localRemoteException)
                        {
                            binderDied();
                        }
                while (true)
                {
                    return;
                    AudioService.sendMsg(AudioService.this.mAudioHandler, 14, 2, 0, 0, AudioService.VolumeStreamState.this, 0);
                    while (true)
                    {
                        this.mMuteCount = (1 + this.mMuteCount);
                        break;
                        Log.w("AudioService", "stream: " + AudioService.VolumeStreamState.this.mStreamType + " was already muted by this client");
                    }
                    if (this.mMuteCount == 0)
                    {
                        Log.e("AudioService", "unexpected unmute for stream: " + AudioService.VolumeStreamState.this.mStreamType);
                    }
                    else
                    {
                        this.mMuteCount = (-1 + this.mMuteCount);
                        if (this.mMuteCount == 0)
                        {
                            AudioService.VolumeStreamState.this.mDeathHandlers.remove(this);
                            if (this.mICallback != null)
                                this.mICallback.unlinkToDeath(this, 0);
                            if ((AudioService.VolumeStreamState.this.muteCount() == 0) && ((!AudioService.this.isStreamAffectedByRingerMode(AudioService.VolumeStreamState.this.mStreamType)) || (AudioService.this.mRingerMode == 2)))
                            {
                                Iterator localIterator1 = AudioService.VolumeStreamState.this.mIndex.entrySet().iterator();
                                while (localIterator1.hasNext())
                                {
                                    int i = ((Integer)((Map.Entry)localIterator1.next()).getKey()).intValue();
                                    AudioService.VolumeStreamState.this.setIndex(AudioService.VolumeStreamState.this.getIndex(i, true), i, false);
                                }
                                AudioService.sendMsg(AudioService.this.mAudioHandler, 14, 2, 0, 0, AudioService.VolumeStreamState.this, 0);
                            }
                        }
                    }
                }
            }
        }
    }

    private class ScoClient
        implements IBinder.DeathRecipient
    {
        private IBinder mCb;
        private int mCreatorPid;
        private int mStartcount;

        ScoClient(IBinder arg2)
        {
            Object localObject;
            this.mCb = localObject;
            this.mCreatorPid = Binder.getCallingPid();
            this.mStartcount = 0;
        }

        // ERROR //
        private void requestScoState(int paramInt)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     4: invokestatic 41	android/media/AudioService:access$2200	(Landroid/media/AudioService;)V
            //     7: aload_0
            //     8: invokevirtual 44	android/media/AudioService$ScoClient:totalCount	()I
            //     11: ifne +362 -> 373
            //     14: iload_1
            //     15: bipush 12
            //     17: if_icmpne +218 -> 235
            //     20: aload_0
            //     21: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     24: iconst_2
            //     25: invokestatic 48	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     28: aload_0
            //     29: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     32: invokestatic 52	android/media/AudioService:access$1200	(Landroid/media/AudioService;)Ljava/util/ArrayList;
            //     35: astore 5
            //     37: aload 5
            //     39: monitorenter
            //     40: aload_0
            //     41: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     44: invokestatic 52	android/media/AudioService:access$1200	(Landroid/media/AudioService;)Ljava/util/ArrayList;
            //     47: invokevirtual 58	java/util/ArrayList:isEmpty	()Z
            //     50: ifne +27 -> 77
            //     53: aload_0
            //     54: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     57: invokestatic 52	android/media/AudioService:access$1200	(Landroid/media/AudioService;)Ljava/util/ArrayList;
            //     60: iconst_0
            //     61: invokevirtual 62	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     64: checkcast 64	android/media/AudioService$SetModeDeathHandler
            //     67: invokevirtual 67	android/media/AudioService$SetModeDeathHandler:getPid	()I
            //     70: aload_0
            //     71: getfield 33	android/media/AudioService$ScoClient:mCreatorPid	I
            //     74: if_icmpne +150 -> 224
            //     77: aload_0
            //     78: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     81: invokestatic 71	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     84: ifeq +14 -> 98
            //     87: aload_0
            //     88: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     91: invokestatic 71	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     94: iconst_5
            //     95: if_icmpne +129 -> 224
            //     98: aload_0
            //     99: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     102: invokestatic 71	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     105: ifne +99 -> 204
            //     108: aload_0
            //     109: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     112: invokestatic 75	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
            //     115: ifnull +67 -> 182
            //     118: aload_0
            //     119: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     122: invokestatic 79	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
            //     125: ifnull +57 -> 182
            //     128: aload_0
            //     129: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     132: invokestatic 75	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
            //     135: aload_0
            //     136: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     139: invokestatic 79	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
            //     142: invokevirtual 85	android/bluetooth/BluetoothHeadset:startScoUsingVirtualVoiceCall	(Landroid/bluetooth/BluetoothDevice;)Z
            //     145: ifeq +18 -> 163
            //     148: aload_0
            //     149: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     152: iconst_3
            //     153: invokestatic 89	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     156: pop
            //     157: aload 5
            //     159: monitorexit
            //     160: goto +213 -> 373
            //     163: aload_0
            //     164: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     167: iconst_0
            //     168: invokestatic 48	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     171: goto -14 -> 157
            //     174: astore 6
            //     176: aload 5
            //     178: monitorexit
            //     179: aload 6
            //     181: athrow
            //     182: aload_0
            //     183: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     186: invokestatic 93	android/media/AudioService:access$2700	(Landroid/media/AudioService;)Z
            //     189: ifeq -32 -> 157
            //     192: aload_0
            //     193: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     196: iconst_1
            //     197: invokestatic 89	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     200: pop
            //     201: goto -44 -> 157
            //     204: aload_0
            //     205: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     208: iconst_3
            //     209: invokestatic 89	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     212: pop
            //     213: aload_0
            //     214: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     217: iconst_1
            //     218: invokestatic 48	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     221: goto -64 -> 157
            //     224: aload_0
            //     225: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     228: iconst_0
            //     229: invokestatic 48	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     232: goto -75 -> 157
            //     235: iload_1
            //     236: bipush 10
            //     238: if_icmpne +135 -> 373
            //     241: aload_0
            //     242: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     245: invokestatic 71	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     248: iconst_3
            //     249: if_icmpeq +14 -> 263
            //     252: aload_0
            //     253: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     256: invokestatic 71	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     259: iconst_1
            //     260: if_icmpne +113 -> 373
            //     263: aload_0
            //     264: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     267: invokestatic 71	android/media/AudioService:access$2400	(Landroid/media/AudioService;)I
            //     270: iconst_3
            //     271: if_icmpne +85 -> 356
            //     274: aload_0
            //     275: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     278: invokestatic 75	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
            //     281: ifnull +53 -> 334
            //     284: aload_0
            //     285: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     288: invokestatic 79	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
            //     291: ifnull +43 -> 334
            //     294: aload_0
            //     295: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     298: invokestatic 75	android/media/AudioService:access$2500	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
            //     301: aload_0
            //     302: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     305: invokestatic 79	android/media/AudioService:access$2600	(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
            //     308: invokevirtual 96	android/bluetooth/BluetoothHeadset:stopScoUsingVirtualVoiceCall	(Landroid/bluetooth/BluetoothDevice;)Z
            //     311: ifne +62 -> 373
            //     314: aload_0
            //     315: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     318: iconst_0
            //     319: invokestatic 89	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     322: pop
            //     323: aload_0
            //     324: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     327: iconst_0
            //     328: invokestatic 48	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     331: goto +42 -> 373
            //     334: aload_0
            //     335: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     338: invokestatic 93	android/media/AudioService:access$2700	(Landroid/media/AudioService;)Z
            //     341: ifeq +32 -> 373
            //     344: aload_0
            //     345: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     348: iconst_5
            //     349: invokestatic 89	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     352: pop
            //     353: goto +20 -> 373
            //     356: aload_0
            //     357: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     360: iconst_0
            //     361: invokestatic 89	android/media/AudioService:access$2402	(Landroid/media/AudioService;I)I
            //     364: pop
            //     365: aload_0
            //     366: getfield 20	android/media/AudioService$ScoClient:this$0	Landroid/media/AudioService;
            //     369: iconst_0
            //     370: invokestatic 48	android/media/AudioService:access$2300	(Landroid/media/AudioService;I)V
            //     373: return
            //
            // Exception table:
            //     from	to	target	type
            //     40	179	174	finally
            //     182	232	174	finally
        }

        public void binderDied()
        {
            synchronized (AudioService.this.mScoClients)
            {
                Log.w("AudioService", "SCO client died");
                if (AudioService.this.mScoClients.indexOf(this) < 0)
                {
                    Log.w("AudioService", "unregistered SCO client died");
                    return;
                }
                clearCount(true);
                AudioService.this.mScoClients.remove(this);
            }
        }

        public void clearCount(boolean paramBoolean)
        {
            synchronized (AudioService.this.mScoClients)
            {
                int i = this.mStartcount;
                if (i != 0);
                try
                {
                    this.mCb.unlinkToDeath(this, 0);
                    this.mStartcount = 0;
                    if (paramBoolean)
                        requestScoState(10);
                    return;
                }
                catch (NoSuchElementException localNoSuchElementException)
                {
                    while (true)
                        Log.w("AudioService", "clearCount() mStartcount: " + this.mStartcount + " != 0 but not registered to binder");
                }
            }
        }

        public void decCount()
        {
            while (true)
            {
                synchronized (AudioService.this.mScoClients)
                {
                    if (this.mStartcount == 0)
                    {
                        Log.w("AudioService", "ScoClient.decCount() already 0");
                        return;
                    }
                    this.mStartcount = (-1 + this.mStartcount);
                    int i = this.mStartcount;
                    if (i != 0);
                }
                try
                {
                    this.mCb.unlinkToDeath(this, 0);
                    requestScoState(10);
                    continue;
                    localObject = finally;
                    throw localObject;
                }
                catch (NoSuchElementException localNoSuchElementException)
                {
                    while (true)
                        Log.w("AudioService", "decCount() going to 0 but not registered to binder");
                }
            }
        }

        public IBinder getBinder()
        {
            return this.mCb;
        }

        public int getCount()
        {
            return this.mStartcount;
        }

        public int getPid()
        {
            return this.mCreatorPid;
        }

        public void incCount()
        {
            synchronized (AudioService.this.mScoClients)
            {
                requestScoState(12);
                int i = this.mStartcount;
                if (i == 0);
                try
                {
                    this.mCb.linkToDeath(this, 0);
                    this.mStartcount = (1 + this.mStartcount);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.w("AudioService", "ScoClient    incCount() could not link to " + this.mCb + " binder death");
                }
            }
        }

        public int totalCount()
        {
            ArrayList localArrayList = AudioService.this.mScoClients;
            int i = 0;
            try
            {
                int j = AudioService.this.mScoClients.size();
                for (int k = 0; k < j; k++)
                    i += ((ScoClient)AudioService.this.mScoClients.get(k)).getCount();
                return i;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    private final class SoundPoolCallback
        implements SoundPool.OnLoadCompleteListener
    {
        int mLastSample;
        int mStatus;

        private SoundPoolCallback()
        {
        }

        public void onLoadComplete(SoundPool paramSoundPool, int paramInt1, int paramInt2)
        {
            Object localObject1 = AudioService.this.mSoundEffectsLock;
            if (paramInt2 != 0);
            try
            {
                this.mStatus = paramInt2;
                if (paramInt1 == this.mLastSample)
                    AudioService.this.mSoundEffectsLock.notify();
                return;
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
        }

        public void setLastSample(int paramInt)
        {
            this.mLastSample = paramInt;
        }

        public int status()
        {
            return this.mStatus;
        }
    }

    class SoundPoolListenerThread extends Thread
    {
        public SoundPoolListenerThread()
        {
            super();
        }

        public void run()
        {
            Looper.prepare();
            AudioService.access$1402(AudioService.this, Looper.myLooper());
            synchronized (AudioService.this.mSoundEffectsLock)
            {
                if (AudioService.this.mSoundPool != null)
                {
                    AudioService.access$1702(AudioService.this, new AudioService.SoundPoolCallback(AudioService.this, null));
                    AudioService.this.mSoundPool.setOnLoadCompleteListener(AudioService.this.mSoundPoolCallBack);
                }
                AudioService.this.mSoundEffectsLock.notify();
                Looper.loop();
                return;
            }
        }
    }

    private class SetModeDeathHandler
        implements IBinder.DeathRecipient
    {
        private IBinder mCb;
        private int mMode = 0;
        private int mPid;

        SetModeDeathHandler(IBinder paramInt, int arg3)
        {
            this.mCb = paramInt;
            int i;
            this.mPid = i;
        }

        public void binderDied()
        {
            int i = 0;
            synchronized (AudioService.this.mSetModeDeathHandlers)
            {
                Log.w("AudioService", "setMode() client died");
                if (AudioService.this.mSetModeDeathHandlers.indexOf(this) < 0)
                {
                    Log.w("AudioService", "unregistered setMode() client died");
                    if (i != 0)
                        AudioService.this.disconnectBluetoothSco(i);
                    return;
                }
                i = AudioService.this.setModeInt(0, this.mCb, this.mPid);
            }
        }

        public IBinder getBinder()
        {
            return this.mCb;
        }

        public int getMode()
        {
            return this.mMode;
        }

        public int getPid()
        {
            return this.mPid;
        }

        public void setMode(int paramInt)
        {
            this.mMode = paramInt;
        }
    }

    private class ForceControlStreamClient
        implements IBinder.DeathRecipient
    {
        private IBinder mCb;

        ForceControlStreamClient(IBinder arg2)
        {
            Object localObject;
            if (localObject != null);
            try
            {
                localObject.linkToDeath(this, 0);
                this.mCb = localObject;
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Log.w("AudioService", "ForceControlStreamClient() could not link to " + localObject + " binder death");
                    localObject = null;
                }
            }
        }

        public void binderDied()
        {
            synchronized (AudioService.this.mForceControlStreamLock)
            {
                Log.w("AudioService", "SCO client died");
                if (AudioService.this.mForceControlStreamClient != this)
                {
                    Log.w("AudioService", "unregistered control stream client died");
                    return;
                }
                AudioService.access$902(AudioService.this, null);
                AudioService.access$1002(AudioService.this, -1);
            }
        }

        public void release()
        {
            if (this.mCb != null)
            {
                this.mCb.unlinkToDeath(this, 0);
                this.mCb = null;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static int adjustDirection(VolumePanel paramVolumePanel, int paramInt1, int paramInt2)
        {
            if (((paramInt1 & 0x1) != 0) && (!paramVolumePanel.isVisible()))
                paramInt2 = 0;
            return paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AudioService
 * JD-Core Version:        0.6.2
 */