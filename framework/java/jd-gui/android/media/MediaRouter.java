package android.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MediaRouter
{
    public static final int ROUTE_TYPE_LIVE_AUDIO = 1;
    public static final int ROUTE_TYPE_USER = 8388608;
    private static final String TAG = "MediaRouter";
    static final HashMap<Context, MediaRouter> sRouters = new HashMap();
    static Static sStatic;

    public MediaRouter(Context paramContext)
    {
        try
        {
            if (sStatic == null)
            {
                Context localContext = paramContext.getApplicationContext();
                sStatic = new Static(localContext);
                sStatic.startMonitoringRoutes(localContext);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    static void addRoute(RouteInfo paramRouteInfo)
    {
        RouteCategory localRouteCategory = paramRouteInfo.getCategory();
        if (!sStatic.mCategories.contains(localRouteCategory))
            sStatic.mCategories.add(localRouteCategory);
        boolean bool = sStatic.mRoutes.isEmpty();
        if ((localRouteCategory.isGroupable()) && (!(paramRouteInfo instanceof RouteGroup)))
        {
            RouteGroup localRouteGroup = new RouteGroup(paramRouteInfo.getCategory());
            localRouteGroup.mSupportedTypes = paramRouteInfo.mSupportedTypes;
            sStatic.mRoutes.add(localRouteGroup);
            dispatchRouteAdded(localRouteGroup);
            localRouteGroup.addRoute(paramRouteInfo);
            paramRouteInfo = localRouteGroup;
        }
        while (true)
        {
            if (bool)
                selectRouteStatic(paramRouteInfo.getSupportedTypes(), paramRouteInfo);
            return;
            sStatic.mRoutes.add(paramRouteInfo);
            dispatchRouteAdded(paramRouteInfo);
        }
    }

    static void dispatchRouteAdded(RouteInfo paramRouteInfo)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((localCallbackInfo.type & paramRouteInfo.mSupportedTypes) != 0)
                localCallbackInfo.cb.onRouteAdded(localCallbackInfo.router, paramRouteInfo);
        }
    }

    static void dispatchRouteChanged(RouteInfo paramRouteInfo)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((localCallbackInfo.type & paramRouteInfo.mSupportedTypes) != 0)
                localCallbackInfo.cb.onRouteChanged(localCallbackInfo.router, paramRouteInfo);
        }
    }

    static void dispatchRouteGrouped(RouteInfo paramRouteInfo, RouteGroup paramRouteGroup, int paramInt)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((localCallbackInfo.type & paramRouteGroup.mSupportedTypes) != 0)
                localCallbackInfo.cb.onRouteGrouped(localCallbackInfo.router, paramRouteInfo, paramRouteGroup, paramInt);
        }
    }

    static void dispatchRouteRemoved(RouteInfo paramRouteInfo)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((localCallbackInfo.type & paramRouteInfo.mSupportedTypes) != 0)
                localCallbackInfo.cb.onRouteRemoved(localCallbackInfo.router, paramRouteInfo);
        }
    }

    static void dispatchRouteSelected(int paramInt, RouteInfo paramRouteInfo)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((paramInt & localCallbackInfo.type) != 0)
                localCallbackInfo.cb.onRouteSelected(localCallbackInfo.router, paramInt, paramRouteInfo);
        }
    }

    static void dispatchRouteUngrouped(RouteInfo paramRouteInfo, RouteGroup paramRouteGroup)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((localCallbackInfo.type & paramRouteGroup.mSupportedTypes) != 0)
                localCallbackInfo.cb.onRouteUngrouped(localCallbackInfo.router, paramRouteInfo, paramRouteGroup);
        }
    }

    static void dispatchRouteUnselected(int paramInt, RouteInfo paramRouteInfo)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((paramInt & localCallbackInfo.type) != 0)
                localCallbackInfo.cb.onRouteUnselected(localCallbackInfo.router, paramInt, paramRouteInfo);
        }
    }

    static void dispatchRouteVolumeChanged(RouteInfo paramRouteInfo)
    {
        Iterator localIterator = sStatic.mCallbacks.iterator();
        while (localIterator.hasNext())
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)localIterator.next();
            if ((localCallbackInfo.type & paramRouteInfo.mSupportedTypes) != 0)
                localCallbackInfo.cb.onRouteVolumeChanged(localCallbackInfo.router, paramRouteInfo);
        }
    }

    static RouteInfo getRouteAtStatic(int paramInt)
    {
        return (RouteInfo)sStatic.mRoutes.get(paramInt);
    }

    static int getRouteCountStatic()
    {
        return sStatic.mRoutes.size();
    }

    static void removeRoute(RouteInfo paramRouteInfo)
    {
        RouteCategory localRouteCategory;
        int i;
        int j;
        if (sStatic.mRoutes.remove(paramRouteInfo))
        {
            localRouteCategory = paramRouteInfo.getCategory();
            i = sStatic.mRoutes.size();
            j = 0;
        }
        for (int k = 0; ; k++)
            if (k < i)
            {
                if (localRouteCategory == ((RouteInfo)sStatic.mRoutes.get(k)).getCategory())
                    j = 1;
            }
            else
            {
                if (paramRouteInfo == sStatic.mSelectedRoute)
                    selectRouteStatic(8388609, sStatic.mDefaultAudio);
                if (j == 0)
                    sStatic.mCategories.remove(localRouteCategory);
                dispatchRouteRemoved(paramRouteInfo);
                return;
            }
    }

    static void selectRouteStatic(int paramInt, RouteInfo paramRouteInfo)
    {
        if (sStatic.mSelectedRoute == paramRouteInfo);
        while (true)
        {
            return;
            if ((paramInt & paramRouteInfo.getSupportedTypes()) == 0)
            {
                Log.w("MediaRouter", "selectRoute ignored; cannot select route with supported types " + typesToString(paramRouteInfo.getSupportedTypes()) + " into route types " + typesToString(paramInt));
                continue;
            }
            RouteInfo localRouteInfo = sStatic.mBluetoothA2dpRoute;
            if ((localRouteInfo != null) && ((paramInt & 0x1) != 0) && ((paramRouteInfo == localRouteInfo) || (paramRouteInfo == sStatic.mDefaultAudio)));
            try
            {
                IAudioService localIAudioService = sStatic.mAudioService;
                if (paramRouteInfo == localRouteInfo);
                for (boolean bool = true; ; bool = false)
                {
                    localIAudioService.setBluetoothA2dpOn(bool);
                    if (sStatic.mSelectedRoute != null)
                        dispatchRouteUnselected(paramInt & sStatic.mSelectedRoute.getSupportedTypes(), sStatic.mSelectedRoute);
                    sStatic.mSelectedRoute = paramRouteInfo;
                    if (paramRouteInfo == null)
                        break;
                    dispatchRouteSelected(paramInt & paramRouteInfo.getSupportedTypes(), paramRouteInfo);
                    break;
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("MediaRouter", "Error changing Bluetooth A2DP state", localRemoteException);
            }
        }
    }

    static void systemVolumeChanged(int paramInt)
    {
        RouteInfo localRouteInfo1 = sStatic.mSelectedRoute;
        if (localRouteInfo1 == null);
        while (true)
        {
            return;
            if ((localRouteInfo1 == sStatic.mBluetoothA2dpRoute) || (localRouteInfo1 == sStatic.mDefaultAudio))
            {
                dispatchRouteVolumeChanged(localRouteInfo1);
            }
            else
            {
                if (sStatic.mBluetoothA2dpRoute != null)
                    while (true)
                    {
                        try
                        {
                            if (!sStatic.mAudioService.isBluetoothA2dpOn())
                                break label92;
                            localRouteInfo2 = sStatic.mBluetoothA2dpRoute;
                            dispatchRouteVolumeChanged(localRouteInfo2);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            Log.e("MediaRouter", "Error checking Bluetooth A2DP state to report volume change", localRemoteException);
                        }
                        break;
                        label92: RouteInfo localRouteInfo2 = sStatic.mDefaultAudio;
                    }
                dispatchRouteVolumeChanged(sStatic.mDefaultAudio);
            }
        }
    }

    static String typesToString(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if ((paramInt & 0x1) != 0)
            localStringBuilder.append("ROUTE_TYPE_LIVE_AUDIO ");
        if ((0x800000 & paramInt) != 0)
            localStringBuilder.append("ROUTE_TYPE_USER ");
        return localStringBuilder.toString();
    }

    static void updateRoute(RouteInfo paramRouteInfo)
    {
        dispatchRouteChanged(paramRouteInfo);
    }

    public void addCallback(int paramInt, Callback paramCallback)
    {
        int i = sStatic.mCallbacks.size();
        int j = 0;
        if (j < i)
        {
            CallbackInfo localCallbackInfo = (CallbackInfo)sStatic.mCallbacks.get(j);
            if (localCallbackInfo.cb == paramCallback)
                localCallbackInfo.type = (paramInt | localCallbackInfo.type);
        }
        while (true)
        {
            return;
            j++;
            break;
            sStatic.mCallbacks.add(new CallbackInfo(paramCallback, paramInt, this));
        }
    }

    public void addRouteInt(RouteInfo paramRouteInfo)
    {
        addRoute(paramRouteInfo);
    }

    public void addUserRoute(UserRouteInfo paramUserRouteInfo)
    {
        addRoute(paramUserRouteInfo);
    }

    public void clearUserRoutes()
    {
        for (int i = 0; i < sStatic.mRoutes.size(); i++)
        {
            RouteInfo localRouteInfo = (RouteInfo)sStatic.mRoutes.get(i);
            if (((localRouteInfo instanceof UserRouteInfo)) || ((localRouteInfo instanceof RouteGroup)))
            {
                removeRouteAt(i);
                i--;
            }
        }
    }

    public RouteCategory createRouteCategory(int paramInt, boolean paramBoolean)
    {
        return new RouteCategory(paramInt, 8388608, paramBoolean);
    }

    public RouteCategory createRouteCategory(CharSequence paramCharSequence, boolean paramBoolean)
    {
        return new RouteCategory(paramCharSequence, 8388608, paramBoolean);
    }

    public UserRouteInfo createUserRoute(RouteCategory paramRouteCategory)
    {
        return new UserRouteInfo(paramRouteCategory);
    }

    public RouteCategory getCategoryAt(int paramInt)
    {
        return (RouteCategory)sStatic.mCategories.get(paramInt);
    }

    public int getCategoryCount()
    {
        return sStatic.mCategories.size();
    }

    public RouteInfo getRouteAt(int paramInt)
    {
        return (RouteInfo)sStatic.mRoutes.get(paramInt);
    }

    public int getRouteCount()
    {
        return sStatic.mRoutes.size();
    }

    public RouteInfo getSelectedRoute(int paramInt)
    {
        return sStatic.mSelectedRoute;
    }

    public RouteCategory getSystemAudioCategory()
    {
        return sStatic.mSystemCategory;
    }

    public RouteInfo getSystemAudioRoute()
    {
        return sStatic.mDefaultAudio;
    }

    public void removeCallback(Callback paramCallback)
    {
        int i = sStatic.mCallbacks.size();
        int j = 0;
        if (j < i)
            if (((CallbackInfo)sStatic.mCallbacks.get(j)).cb == paramCallback)
                sStatic.mCallbacks.remove(j);
        while (true)
        {
            return;
            j++;
            break;
            Log.w("MediaRouter", "removeCallback(" + paramCallback + "): callback not registered");
        }
    }

    void removeRouteAt(int paramInt)
    {
        RouteInfo localRouteInfo;
        RouteCategory localRouteCategory;
        int i;
        int j;
        if ((paramInt >= 0) && (paramInt < sStatic.mRoutes.size()))
        {
            localRouteInfo = (RouteInfo)sStatic.mRoutes.remove(paramInt);
            localRouteCategory = localRouteInfo.getCategory();
            i = sStatic.mRoutes.size();
            j = 0;
        }
        for (int k = 0; ; k++)
            if (k < i)
            {
                if (localRouteCategory == ((RouteInfo)sStatic.mRoutes.get(k)).getCategory())
                    j = 1;
            }
            else
            {
                if (localRouteInfo == sStatic.mSelectedRoute)
                    selectRouteStatic(8388609, sStatic.mDefaultAudio);
                if (j == 0)
                    sStatic.mCategories.remove(localRouteCategory);
                dispatchRouteRemoved(localRouteInfo);
                return;
            }
    }

    public void removeRouteInt(RouteInfo paramRouteInfo)
    {
        removeRoute(paramRouteInfo);
    }

    public void removeUserRoute(UserRouteInfo paramUserRouteInfo)
    {
        removeRoute(paramUserRouteInfo);
    }

    public void selectRoute(int paramInt, RouteInfo paramRouteInfo)
    {
        selectRouteStatic(paramInt & 0x800000, paramRouteInfo);
    }

    public void selectRouteInt(int paramInt, RouteInfo paramRouteInfo)
    {
        selectRouteStatic(paramInt, paramRouteInfo);
    }

    static class VolumeChangeReceiver extends BroadcastReceiver
    {
        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if ((!paramIntent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) || (paramIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1) != 3));
            while (true)
            {
                return;
                int i = paramIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0);
                if (i != paramIntent.getIntExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", 0))
                    MediaRouter.systemVolumeChanged(i);
            }
        }
    }

    public static abstract class VolumeCallback
    {
        public abstract void onVolumeSetRequest(MediaRouter.RouteInfo paramRouteInfo, int paramInt);

        public abstract void onVolumeUpdateRequest(MediaRouter.RouteInfo paramRouteInfo, int paramInt);
    }

    static class VolumeCallbackInfo
    {
        public final MediaRouter.RouteInfo route;
        public final MediaRouter.VolumeCallback vcb;

        public VolumeCallbackInfo(MediaRouter.VolumeCallback paramVolumeCallback, MediaRouter.RouteInfo paramRouteInfo)
        {
            this.vcb = paramVolumeCallback;
            this.route = paramRouteInfo;
        }
    }

    public static class SimpleCallback extends MediaRouter.Callback
    {
        public void onRouteAdded(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
        }

        public void onRouteChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
        }

        public void onRouteGrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup, int paramInt)
        {
        }

        public void onRouteRemoved(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
        }

        public void onRouteSelected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
        {
        }

        public void onRouteUngrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup)
        {
        }

        public void onRouteUnselected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
        {
        }

        public void onRouteVolumeChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
        }
    }

    public static abstract class Callback
    {
        public abstract void onRouteAdded(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo);

        public abstract void onRouteChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo);

        public abstract void onRouteGrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup, int paramInt);

        public abstract void onRouteRemoved(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo);

        public abstract void onRouteSelected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo);

        public abstract void onRouteUngrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup);

        public abstract void onRouteUnselected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo);

        public abstract void onRouteVolumeChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo);
    }

    static class CallbackInfo
    {
        public final MediaRouter.Callback cb;
        public final MediaRouter router;
        public int type;

        public CallbackInfo(MediaRouter.Callback paramCallback, int paramInt, MediaRouter paramMediaRouter)
        {
            this.cb = paramCallback;
            this.type = paramInt;
            this.router = paramMediaRouter;
        }
    }

    public static class RouteCategory
    {
        final boolean mGroupable;
        CharSequence mName;
        int mNameResId;
        int mTypes;

        RouteCategory(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            this.mNameResId = paramInt1;
            this.mTypes = paramInt2;
            this.mGroupable = paramBoolean;
        }

        RouteCategory(CharSequence paramCharSequence, int paramInt, boolean paramBoolean)
        {
            this.mName = paramCharSequence;
            this.mTypes = paramInt;
            this.mGroupable = paramBoolean;
        }

        public CharSequence getName()
        {
            return getName(MediaRouter.sStatic.mResources);
        }

        public CharSequence getName(Context paramContext)
        {
            return getName(paramContext.getResources());
        }

        CharSequence getName(Resources paramResources)
        {
            if (this.mNameResId != 0);
            for (CharSequence localCharSequence = paramResources.getText(this.mNameResId); ; localCharSequence = this.mName)
                return localCharSequence;
        }

        public List<MediaRouter.RouteInfo> getRoutes(List<MediaRouter.RouteInfo> paramList)
        {
            if (paramList == null)
                paramList = new ArrayList();
            while (true)
            {
                int i = MediaRouter.getRouteCountStatic();
                for (int j = 0; j < i; j++)
                {
                    MediaRouter.RouteInfo localRouteInfo = MediaRouter.getRouteAtStatic(j);
                    if (localRouteInfo.mCategory == this)
                        paramList.add(localRouteInfo);
                }
                paramList.clear();
            }
            return paramList;
        }

        public int getSupportedTypes()
        {
            return this.mTypes;
        }

        public boolean isGroupable()
        {
            return this.mGroupable;
        }

        public String toString()
        {
            return "RouteCategory{ name=" + this.mName + " types=" + MediaRouter.typesToString(this.mTypes) + " groupable=" + this.mGroupable + " }";
        }
    }

    public static class RouteGroup extends MediaRouter.RouteInfo
    {
        final ArrayList<MediaRouter.RouteInfo> mRoutes = new ArrayList();
        private boolean mUpdateName;

        RouteGroup(MediaRouter.RouteCategory paramRouteCategory)
        {
            super();
            this.mGroup = this;
            this.mVolumeHandling = 0;
        }

        public void addRoute(MediaRouter.RouteInfo paramRouteInfo)
        {
            if (paramRouteInfo.getGroup() != null)
                throw new IllegalStateException("Route " + paramRouteInfo + " is already part of a group.");
            if (paramRouteInfo.getCategory() != this.mCategory)
                throw new IllegalArgumentException("Route cannot be added to a group with a different category. (Route category=" + paramRouteInfo.getCategory() + " group category=" + this.mCategory + ")");
            int i = this.mRoutes.size();
            this.mRoutes.add(paramRouteInfo);
            paramRouteInfo.mGroup = this;
            this.mUpdateName = true;
            updateVolume();
            routeUpdated();
            MediaRouter.dispatchRouteGrouped(paramRouteInfo, this, i);
        }

        public void addRoute(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
        {
            if (paramRouteInfo.getGroup() != null)
                throw new IllegalStateException("Route " + paramRouteInfo + " is already part of a group.");
            if (paramRouteInfo.getCategory() != this.mCategory)
                throw new IllegalArgumentException("Route cannot be added to a group with a different category. (Route category=" + paramRouteInfo.getCategory() + " group category=" + this.mCategory + ")");
            this.mRoutes.add(paramInt, paramRouteInfo);
            paramRouteInfo.mGroup = this;
            this.mUpdateName = true;
            updateVolume();
            routeUpdated();
            MediaRouter.dispatchRouteGrouped(paramRouteInfo, this, paramInt);
        }

        CharSequence getName(Resources paramResources)
        {
            if (this.mUpdateName)
                updateName();
            return super.getName(paramResources);
        }

        public MediaRouter.RouteInfo getRouteAt(int paramInt)
        {
            return (MediaRouter.RouteInfo)this.mRoutes.get(paramInt);
        }

        public int getRouteCount()
        {
            return this.mRoutes.size();
        }

        void memberNameChanged(MediaRouter.RouteInfo paramRouteInfo, CharSequence paramCharSequence)
        {
            this.mUpdateName = true;
            routeUpdated();
        }

        void memberStatusChanged(MediaRouter.RouteInfo paramRouteInfo, CharSequence paramCharSequence)
        {
            setStatusInt(paramCharSequence);
        }

        void memberVolumeChanged(MediaRouter.RouteInfo paramRouteInfo)
        {
            updateVolume();
        }

        public void removeRoute(int paramInt)
        {
            MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)this.mRoutes.remove(paramInt);
            localRouteInfo.mGroup = null;
            this.mUpdateName = true;
            updateVolume();
            MediaRouter.dispatchRouteUngrouped(localRouteInfo, this);
            routeUpdated();
        }

        public void removeRoute(MediaRouter.RouteInfo paramRouteInfo)
        {
            if (paramRouteInfo.getGroup() != this)
                throw new IllegalArgumentException("Route " + paramRouteInfo + " is not a member of this group.");
            this.mRoutes.remove(paramRouteInfo);
            paramRouteInfo.mGroup = null;
            this.mUpdateName = true;
            updateVolume();
            MediaRouter.dispatchRouteUngrouped(paramRouteInfo, this);
            routeUpdated();
        }

        public void requestSetVolume(int paramInt)
        {
            int i = getVolumeMax();
            if (i == 0);
            while (true)
            {
                return;
                float f = paramInt / i;
                int j = getRouteCount();
                for (int k = 0; k < j; k++)
                {
                    MediaRouter.RouteInfo localRouteInfo = getRouteAt(k);
                    localRouteInfo.requestSetVolume((int)(f * localRouteInfo.getVolumeMax()));
                }
                if (paramInt != this.mVolume)
                {
                    this.mVolume = paramInt;
                    MediaRouter.dispatchRouteVolumeChanged(this);
                }
            }
        }

        public void requestUpdateVolume(int paramInt)
        {
            if (getVolumeMax() == 0);
            while (true)
            {
                return;
                int i = getRouteCount();
                int j = 0;
                for (int k = 0; k < i; k++)
                {
                    MediaRouter.RouteInfo localRouteInfo = getRouteAt(k);
                    localRouteInfo.requestUpdateVolume(paramInt);
                    int m = localRouteInfo.getVolume();
                    if (m > j)
                        j = m;
                }
                if (j != this.mVolume)
                {
                    this.mVolume = j;
                    MediaRouter.dispatchRouteVolumeChanged(this);
                }
            }
        }

        void routeUpdated()
        {
            int i = 0;
            int j = this.mRoutes.size();
            if (j == 0)
            {
                MediaRouter.removeRoute(this);
                return;
            }
            int k = 0;
            int m = 1;
            int n = 1;
            int i1 = 0;
            if (i1 < j)
            {
                MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)this.mRoutes.get(i1);
                i |= localRouteInfo.mSupportedTypes;
                int i4 = localRouteInfo.getVolumeMax();
                if (i4 > k)
                    k = i4;
                int i5;
                if (localRouteInfo.getPlaybackType() == 0)
                {
                    i5 = 1;
                    label85: m &= i5;
                    if (localRouteInfo.getVolumeHandling() != 0)
                        break label122;
                }
                label122: for (int i6 = 1; ; i6 = 0)
                {
                    n &= i6;
                    i1++;
                    break;
                    i5 = 0;
                    break label85;
                }
            }
            int i2;
            label136: int i3;
            if (m != 0)
            {
                i2 = 0;
                this.mPlaybackType = i2;
                if (n == 0)
                    break label206;
                i3 = 0;
                label150: this.mVolumeHandling = i3;
                this.mSupportedTypes = i;
                this.mVolumeMax = k;
                if (j != 1)
                    break label212;
            }
            label206: label212: for (Drawable localDrawable = ((MediaRouter.RouteInfo)this.mRoutes.get(0)).getIconDrawable(); ; localDrawable = null)
            {
                this.mIcon = localDrawable;
                super.routeUpdated();
                break;
                i2 = 1;
                break label136;
                i3 = 1;
                break label150;
            }
        }

        public void setIconDrawable(Drawable paramDrawable)
        {
            this.mIcon = paramDrawable;
        }

        public void setIconResource(int paramInt)
        {
            setIconDrawable(MediaRouter.sStatic.mResources.getDrawable(paramInt));
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder(super.toString());
            localStringBuilder.append('[');
            int i = this.mRoutes.size();
            for (int j = 0; j < i; j++)
            {
                if (j > 0)
                    localStringBuilder.append(", ");
                localStringBuilder.append(this.mRoutes.get(j));
            }
            localStringBuilder.append(']');
            return localStringBuilder.toString();
        }

        void updateName()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            int i = this.mRoutes.size();
            for (int j = 0; j < i; j++)
            {
                MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)this.mRoutes.get(j);
                if (j > 0)
                    localStringBuilder.append(", ");
                localStringBuilder.append(localRouteInfo.mName);
            }
            this.mName = localStringBuilder.toString();
            this.mUpdateName = false;
        }

        void updateVolume()
        {
            int i = getRouteCount();
            int j = 0;
            for (int k = 0; k < i; k++)
            {
                int m = getRouteAt(k).getVolume();
                if (m > j)
                    j = m;
            }
            if (j != this.mVolume)
            {
                this.mVolume = j;
                MediaRouter.dispatchRouteVolumeChanged(this);
            }
        }
    }

    public static class UserRouteInfo extends MediaRouter.RouteInfo
    {
        RemoteControlClient mRcc;

        UserRouteInfo(MediaRouter.RouteCategory paramRouteCategory)
        {
            super();
            this.mSupportedTypes = 8388608;
            this.mPlaybackType = 1;
            this.mVolumeHandling = 0;
        }

        private void setPlaybackInfoOnRcc(int paramInt1, int paramInt2)
        {
            if (this.mRcc != null)
                this.mRcc.setPlaybackInformation(paramInt1, paramInt2);
        }

        private void updatePlaybackInfoOnRcc()
        {
            if ((this.mRcc != null) && (this.mRcc.getRcseId() != -1))
            {
                this.mRcc.setPlaybackInformation(3, this.mVolumeMax);
                this.mRcc.setPlaybackInformation(2, this.mVolume);
                this.mRcc.setPlaybackInformation(4, this.mVolumeHandling);
                this.mRcc.setPlaybackInformation(5, this.mPlaybackStream);
                this.mRcc.setPlaybackInformation(1, this.mPlaybackType);
            }
            try
            {
                MediaRouter.sStatic.mAudioService.registerRemoteVolumeObserverForRcc(this.mRcc.getRcseId(), this.mRemoteVolObserver);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("MediaRouter", "Error registering remote volume observer", localRemoteException);
            }
        }

        public RemoteControlClient getRemoteControlClient()
        {
            return this.mRcc;
        }

        public void requestSetVolume(int paramInt)
        {
            if (this.mVolumeHandling == 1)
            {
                if (this.mVcb != null)
                    break label24;
                Log.e("MediaRouter", "Cannot requestSetVolume on user route - no volume callback set");
            }
            while (true)
            {
                return;
                label24: this.mVcb.vcb.onVolumeSetRequest(this, paramInt);
            }
        }

        public void requestUpdateVolume(int paramInt)
        {
            if (this.mVolumeHandling == 1)
            {
                if (this.mVcb != null)
                    break label24;
                Log.e("MediaRouter", "Cannot requestChangeVolume on user route - no volumec callback set");
            }
            while (true)
            {
                return;
                label24: this.mVcb.vcb.onVolumeUpdateRequest(this, paramInt);
            }
        }

        public void setIconDrawable(Drawable paramDrawable)
        {
            this.mIcon = paramDrawable;
        }

        public void setIconResource(int paramInt)
        {
            setIconDrawable(MediaRouter.sStatic.mResources.getDrawable(paramInt));
        }

        public void setName(int paramInt)
        {
            this.mNameResId = paramInt;
            this.mName = null;
            routeUpdated();
        }

        public void setName(CharSequence paramCharSequence)
        {
            this.mName = paramCharSequence;
            routeUpdated();
        }

        public void setPlaybackStream(int paramInt)
        {
            if (this.mPlaybackStream != paramInt)
            {
                this.mPlaybackStream = paramInt;
                setPlaybackInfoOnRcc(5, paramInt);
            }
        }

        public void setPlaybackType(int paramInt)
        {
            if (this.mPlaybackType != paramInt)
            {
                this.mPlaybackType = paramInt;
                setPlaybackInfoOnRcc(1, paramInt);
            }
        }

        public void setRemoteControlClient(RemoteControlClient paramRemoteControlClient)
        {
            this.mRcc = paramRemoteControlClient;
            updatePlaybackInfoOnRcc();
        }

        public void setStatus(CharSequence paramCharSequence)
        {
            setStatusInt(paramCharSequence);
        }

        public void setVolume(int paramInt)
        {
            int i = Math.max(0, Math.min(paramInt, getVolumeMax()));
            if (this.mVolume != i)
            {
                this.mVolume = i;
                setPlaybackInfoOnRcc(2, i);
                MediaRouter.dispatchRouteVolumeChanged(this);
                if (this.mGroup != null)
                    this.mGroup.memberVolumeChanged(this);
            }
        }

        public void setVolumeCallback(MediaRouter.VolumeCallback paramVolumeCallback)
        {
            this.mVcb = new MediaRouter.VolumeCallbackInfo(paramVolumeCallback, this);
        }

        public void setVolumeHandling(int paramInt)
        {
            if (this.mVolumeHandling != paramInt)
            {
                this.mVolumeHandling = paramInt;
                setPlaybackInfoOnRcc(4, paramInt);
            }
        }

        public void setVolumeMax(int paramInt)
        {
            if (this.mVolumeMax != paramInt)
            {
                this.mVolumeMax = paramInt;
                setPlaybackInfoOnRcc(3, paramInt);
            }
        }
    }

    public static class RouteInfo
    {
        public static final int PLAYBACK_TYPE_LOCAL = 0;
        public static final int PLAYBACK_TYPE_REMOTE = 1;
        public static final int PLAYBACK_VOLUME_FIXED = 0;
        public static final int PLAYBACK_VOLUME_VARIABLE = 1;
        final MediaRouter.RouteCategory mCategory;
        MediaRouter.RouteGroup mGroup;
        Drawable mIcon;
        CharSequence mName;
        int mNameResId;
        int mPlaybackStream = 3;
        int mPlaybackType = 0;
        final IRemoteVolumeObserver.Stub mRemoteVolObserver = new IRemoteVolumeObserver.Stub()
        {
            public void dispatchRemoteVolumeUpdate(final int paramAnonymousInt1, final int paramAnonymousInt2)
            {
                MediaRouter.sStatic.mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        if (MediaRouter.RouteInfo.this.mVcb != null)
                        {
                            if (paramAnonymousInt1 == 0)
                                break label54;
                            MediaRouter.RouteInfo.this.mVcb.vcb.onVolumeUpdateRequest(MediaRouter.RouteInfo.this.mVcb.route, paramAnonymousInt1);
                        }
                        while (true)
                        {
                            return;
                            label54: MediaRouter.RouteInfo.this.mVcb.vcb.onVolumeSetRequest(MediaRouter.RouteInfo.this.mVcb.route, paramAnonymousInt2);
                        }
                    }
                });
            }
        };
        private CharSequence mStatus;
        int mSupportedTypes;
        private Object mTag;
        MediaRouter.VolumeCallbackInfo mVcb;
        int mVolume = 15;
        int mVolumeHandling = 1;
        int mVolumeMax = 15;

        RouteInfo(MediaRouter.RouteCategory paramRouteCategory)
        {
            this.mCategory = paramRouteCategory;
        }

        public MediaRouter.RouteCategory getCategory()
        {
            return this.mCategory;
        }

        public MediaRouter.RouteGroup getGroup()
        {
            return this.mGroup;
        }

        public Drawable getIconDrawable()
        {
            return this.mIcon;
        }

        public CharSequence getName()
        {
            return getName(MediaRouter.sStatic.mResources);
        }

        public CharSequence getName(Context paramContext)
        {
            return getName(paramContext.getResources());
        }

        CharSequence getName(Resources paramResources)
        {
            CharSequence localCharSequence;
            if (this.mNameResId != 0)
            {
                localCharSequence = paramResources.getText(this.mNameResId);
                this.mName = localCharSequence;
            }
            while (true)
            {
                return localCharSequence;
                localCharSequence = this.mName;
            }
        }

        public int getPlaybackStream()
        {
            return this.mPlaybackStream;
        }

        public int getPlaybackType()
        {
            return this.mPlaybackType;
        }

        public CharSequence getStatus()
        {
            return this.mStatus;
        }

        public int getSupportedTypes()
        {
            return this.mSupportedTypes;
        }

        public Object getTag()
        {
            return this.mTag;
        }

        public int getVolume()
        {
            if (this.mPlaybackType == 0);
            for (int i = 0; ; i = this.mVolume)
                try
                {
                    int j = MediaRouter.sStatic.mAudioService.getStreamVolume(this.mPlaybackStream);
                    i = j;
                    return i;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.e("MediaRouter", "Error getting local stream volume", localRemoteException);
                }
        }

        public int getVolumeHandling()
        {
            return this.mVolumeHandling;
        }

        public int getVolumeMax()
        {
            if (this.mPlaybackType == 0);
            for (int i = 0; ; i = this.mVolumeMax)
                try
                {
                    int j = MediaRouter.sStatic.mAudioService.getStreamMaxVolume(this.mPlaybackStream);
                    i = j;
                    return i;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.e("MediaRouter", "Error getting local stream volume", localRemoteException);
                }
        }

        public void requestSetVolume(int paramInt)
        {
            if (this.mPlaybackType == 0);
            while (true)
            {
                try
                {
                    MediaRouter.sStatic.mAudioService.setStreamVolume(this.mPlaybackStream, paramInt, 0);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("MediaRouter", "Error setting local stream volume", localRemoteException);
                    continue;
                }
                Log.e("MediaRouter", getClass().getSimpleName() + ".requestSetVolume(): " + "Non-local volume playback on system route? " + "Could not request volume change.");
            }
        }

        public void requestUpdateVolume(int paramInt)
        {
            if (this.mPlaybackType == 0);
            while (true)
            {
                try
                {
                    int i = Math.max(0, Math.min(paramInt + getVolume(), getVolumeMax()));
                    MediaRouter.sStatic.mAudioService.setStreamVolume(this.mPlaybackStream, i, 0);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("MediaRouter", "Error setting local stream volume", localRemoteException);
                    continue;
                }
                Log.e("MediaRouter", getClass().getSimpleName() + ".requestChangeVolume(): " + "Non-local volume playback on system route? " + "Could not request volume change.");
            }
        }

        void routeUpdated()
        {
            MediaRouter.updateRoute(this);
        }

        void setStatusInt(CharSequence paramCharSequence)
        {
            if (!paramCharSequence.equals(this.mStatus))
            {
                this.mStatus = paramCharSequence;
                if (this.mGroup != null)
                    this.mGroup.memberStatusChanged(this, paramCharSequence);
                routeUpdated();
            }
        }

        public void setTag(Object paramObject)
        {
            this.mTag = paramObject;
            routeUpdated();
        }

        public String toString()
        {
            String str = MediaRouter.typesToString(getSupportedTypes());
            return getClass().getSimpleName() + "{ name=" + getName() + ", status=" + getStatus() + " category=" + getCategory() + " supportedTypes=" + str + "}";
        }
    }

    static class Static
    {
        final IAudioService mAudioService;
        MediaRouter.RouteInfo mBluetoothA2dpRoute;
        final CopyOnWriteArrayList<MediaRouter.CallbackInfo> mCallbacks = new CopyOnWriteArrayList();
        final ArrayList<MediaRouter.RouteCategory> mCategories = new ArrayList();
        final AudioRoutesInfo mCurRoutesInfo = new AudioRoutesInfo();
        MediaRouter.RouteInfo mDefaultAudio;
        final Handler mHandler;
        final Resources mResources = Resources.getSystem();
        final ArrayList<MediaRouter.RouteInfo> mRoutes = new ArrayList();
        final IAudioRoutesObserver.Stub mRoutesObserver = new IAudioRoutesObserver.Stub()
        {
            public void dispatchAudioRoutesChanged(final AudioRoutesInfo paramAnonymousAudioRoutesInfo)
            {
                MediaRouter.Static.this.mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        MediaRouter.Static.this.updateRoutes(paramAnonymousAudioRoutesInfo);
                    }
                });
            }
        };
        MediaRouter.RouteInfo mSelectedRoute;
        final MediaRouter.RouteCategory mSystemCategory;

        Static(Context paramContext)
        {
            this.mHandler = new Handler(paramContext.getMainLooper());
            this.mAudioService = IAudioService.Stub.asInterface(ServiceManager.getService("audio"));
            this.mSystemCategory = new MediaRouter.RouteCategory(17040641, 1, false);
        }

        void startMonitoringRoutes(Context paramContext)
        {
            this.mDefaultAudio = new MediaRouter.RouteInfo(this.mSystemCategory);
            this.mDefaultAudio.mNameResId = 17040637;
            this.mDefaultAudio.mSupportedTypes = 1;
            MediaRouter.addRoute(this.mDefaultAudio);
            paramContext.registerReceiver(new MediaRouter.VolumeChangeReceiver(), new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            Object localObject = null;
            try
            {
                AudioRoutesInfo localAudioRoutesInfo = this.mAudioService.startWatchingRoutes(this.mRoutesObserver);
                localObject = localAudioRoutesInfo;
                label80: if (localObject != null)
                    updateRoutes(localObject);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label80;
            }
        }

        void updateRoutes(AudioRoutesInfo paramAudioRoutesInfo)
        {
            int i;
            if (paramAudioRoutesInfo.mMainType != this.mCurRoutesInfo.mMainType)
            {
                this.mCurRoutesInfo.mMainType = paramAudioRoutesInfo.mMainType;
                if (((0x2 & paramAudioRoutesInfo.mMainType) == 0) && ((0x1 & paramAudioRoutesInfo.mMainType) == 0))
                    break label216;
                i = 17040638;
            }
            while (true)
            {
                MediaRouter.sStatic.mDefaultAudio.mNameResId = i;
                MediaRouter.dispatchRouteChanged(MediaRouter.sStatic.mDefaultAudio);
                try
                {
                    boolean bool2 = this.mAudioService.isBluetoothA2dpOn();
                    bool1 = bool2;
                    if (!TextUtils.equals(paramAudioRoutesInfo.mBluetoothName, this.mCurRoutesInfo.mBluetoothName))
                    {
                        this.mCurRoutesInfo.mBluetoothName = paramAudioRoutesInfo.mBluetoothName;
                        if (this.mCurRoutesInfo.mBluetoothName == null)
                            break label300;
                        if (MediaRouter.sStatic.mBluetoothA2dpRoute == null)
                        {
                            MediaRouter.RouteInfo localRouteInfo = new MediaRouter.RouteInfo(MediaRouter.sStatic.mSystemCategory);
                            localRouteInfo.mName = this.mCurRoutesInfo.mBluetoothName;
                            localRouteInfo.mSupportedTypes = 1;
                            MediaRouter.sStatic.mBluetoothA2dpRoute = localRouteInfo;
                            MediaRouter.addRoute(MediaRouter.sStatic.mBluetoothA2dpRoute);
                        }
                    }
                    else
                    {
                        if (this.mBluetoothA2dpRoute != null)
                        {
                            if ((this.mCurRoutesInfo.mMainType == 0) || (this.mSelectedRoute != this.mBluetoothA2dpRoute))
                                break label328;
                            MediaRouter.selectRouteStatic(1, this.mDefaultAudio);
                        }
                        return;
                        label216: if ((0x4 & paramAudioRoutesInfo.mMainType) != 0)
                        {
                            i = 17040639;
                            continue;
                        }
                        if ((0x8 & paramAudioRoutesInfo.mMainType) != 0)
                        {
                            i = 17040640;
                            continue;
                        }
                        i = 17040637;
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        Log.e("MediaRouter", "Error querying Bluetooth A2DP state", localRemoteException);
                        boolean bool1 = false;
                        continue;
                        MediaRouter.sStatic.mBluetoothA2dpRoute.mName = this.mCurRoutesInfo.mBluetoothName;
                        MediaRouter.dispatchRouteChanged(MediaRouter.sStatic.mBluetoothA2dpRoute);
                        continue;
                        label300: if (MediaRouter.sStatic.mBluetoothA2dpRoute != null)
                        {
                            MediaRouter.removeRoute(MediaRouter.sStatic.mBluetoothA2dpRoute);
                            MediaRouter.sStatic.mBluetoothA2dpRoute = null;
                            continue;
                            label328: if ((this.mCurRoutesInfo.mMainType == 0) && (this.mSelectedRoute == this.mDefaultAudio) && (bool1))
                                MediaRouter.selectRouteStatic(1, this.mBluetoothA2dpRoute);
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaRouter
 * JD-Core Version:        0.6.2
 */