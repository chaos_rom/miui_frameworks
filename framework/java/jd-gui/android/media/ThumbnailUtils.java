package android.media;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;

public class ThumbnailUtils
{
    private static final int MAX_NUM_PIXELS_MICRO_THUMBNAIL = 16384;
    private static final int MAX_NUM_PIXELS_THUMBNAIL = 196608;
    private static final int OPTIONS_NONE = 0;
    public static final int OPTIONS_RECYCLE_INPUT = 2;
    private static final int OPTIONS_SCALE_UP = 1;
    private static final String TAG = "ThumbnailUtils";
    public static final int TARGET_SIZE_MICRO_THUMBNAIL = 96;
    public static final int TARGET_SIZE_MINI_THUMBNAIL = 320;
    private static final int UNCONSTRAINED = -1;

    private static void closeSilently(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        if (paramParcelFileDescriptor == null);
        while (true)
        {
            return;
            try
            {
                paramParcelFileDescriptor.close();
            }
            catch (Throwable localThrowable)
            {
            }
        }
    }

    private static int computeInitialSampleSize(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
    {
        double d1 = paramOptions.outWidth;
        double d2 = paramOptions.outHeight;
        int i;
        int j;
        if (paramInt2 == -1)
        {
            i = 1;
            if (paramInt1 != -1)
                break label62;
            j = 128;
            label33: if (j >= i)
                break label86;
        }
        while (true)
        {
            return i;
            i = (int)Math.ceil(Math.sqrt(d1 * d2 / paramInt2));
            break;
            label62: j = (int)Math.min(Math.floor(d1 / paramInt1), Math.floor(d2 / paramInt1));
            break label33;
            label86: if ((paramInt2 == -1) && (paramInt1 == -1))
                i = 1;
            else if (paramInt1 != -1)
                i = j;
        }
    }

    private static int computeSampleSize(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
    {
        int i = computeInitialSampleSize(paramOptions, paramInt1, paramInt2);
        if (i <= 8)
        {
            j = 1;
            while (j < i)
                j <<= 1;
        }
        int j = 8 * ((i + 7) / 8);
        return j;
    }

    // ERROR //
    public static Bitmap createImageThumbnail(String paramString, int paramInt)
    {
        // Byte code:
        //     0: iload_1
        //     1: iconst_1
        //     2: if_icmpne +174 -> 176
        //     5: iconst_1
        //     6: istore_2
        //     7: iload_2
        //     8: ifeq +173 -> 181
        //     11: sipush 320
        //     14: istore_3
        //     15: iload_2
        //     16: ifeq +171 -> 187
        //     19: ldc 14
        //     21: istore 4
        //     23: new 8	android/media/ThumbnailUtils$SizedThumbnailBitmap
        //     26: dup
        //     27: aconst_null
        //     28: invokespecial 81	android/media/ThumbnailUtils$SizedThumbnailBitmap:<init>	(Landroid/media/ThumbnailUtils$1;)V
        //     31: astore 5
        //     33: aconst_null
        //     34: astore 6
        //     36: aload_0
        //     37: invokestatic 87	android/media/MediaFile:getFileType	(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;
        //     40: astore 7
        //     42: aload 7
        //     44: ifnull +29 -> 73
        //     47: aload 7
        //     49: getfield 92	android/media/MediaFile$MediaFileType:fileType	I
        //     52: bipush 31
        //     54: if_icmpne +19 -> 73
        //     57: aload_0
        //     58: iload_3
        //     59: iload 4
        //     61: aload 5
        //     63: invokestatic 96	android/media/ThumbnailUtils:createThumbnailFromEXIF	(Ljava/lang/String;IILandroid/media/ThumbnailUtils$SizedThumbnailBitmap;)V
        //     66: aload 5
        //     68: getfield 100	android/media/ThumbnailUtils$SizedThumbnailBitmap:mBitmap	Landroid/graphics/Bitmap;
        //     71: astore 6
        //     73: aload 6
        //     75: ifnonnull +177 -> 252
        //     78: aconst_null
        //     79: astore 9
        //     81: new 102	java/io/FileInputStream
        //     84: dup
        //     85: aload_0
        //     86: invokespecial 105	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     89: astore 10
        //     91: aload 10
        //     93: invokevirtual 109	java/io/FileInputStream:getFD	()Ljava/io/FileDescriptor;
        //     96: astore 22
        //     98: new 47	android/graphics/BitmapFactory$Options
        //     101: dup
        //     102: invokespecial 110	android/graphics/BitmapFactory$Options:<init>	()V
        //     105: astore 23
        //     107: aload 23
        //     109: iconst_1
        //     110: putfield 113	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     113: aload 23
        //     115: iconst_1
        //     116: putfield 117	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     119: aload 22
        //     121: aconst_null
        //     122: aload 23
        //     124: invokestatic 123	android/graphics/BitmapFactory:decodeFileDescriptor	(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     127: pop
        //     128: aload 23
        //     130: getfield 126	android/graphics/BitmapFactory$Options:mCancel	Z
        //     133: ifne +27 -> 160
        //     136: aload 23
        //     138: getfield 50	android/graphics/BitmapFactory$Options:outWidth	I
        //     141: bipush 255
        //     143: if_icmpeq +17 -> 160
        //     146: aload 23
        //     148: getfield 53	android/graphics/BitmapFactory$Options:outHeight	I
        //     151: istore 27
        //     153: iload 27
        //     155: bipush 255
        //     157: if_icmpne +38 -> 195
        //     160: aconst_null
        //     161: astore 8
        //     163: aload 10
        //     165: ifnull +8 -> 173
        //     168: aload 10
        //     170: invokevirtual 127	java/io/FileInputStream:close	()V
        //     173: aload 8
        //     175: areturn
        //     176: iconst_0
        //     177: istore_2
        //     178: goto -171 -> 7
        //     181: bipush 96
        //     183: istore_3
        //     184: goto -169 -> 15
        //     187: sipush 16384
        //     190: istore 4
        //     192: goto -169 -> 23
        //     195: aload 23
        //     197: aload 23
        //     199: iload_3
        //     200: iload 4
        //     202: invokestatic 129	android/media/ThumbnailUtils:computeSampleSize	(Landroid/graphics/BitmapFactory$Options;II)I
        //     205: putfield 113	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     208: aload 23
        //     210: iconst_0
        //     211: putfield 117	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     214: aload 23
        //     216: iconst_0
        //     217: putfield 132	android/graphics/BitmapFactory$Options:inDither	Z
        //     220: aload 23
        //     222: getstatic 138	android/graphics/Bitmap$Config:ARGB_8888	Landroid/graphics/Bitmap$Config;
        //     225: putfield 141	android/graphics/BitmapFactory$Options:inPreferredConfig	Landroid/graphics/Bitmap$Config;
        //     228: aload 22
        //     230: aconst_null
        //     231: aload 23
        //     233: invokestatic 123	android/graphics/BitmapFactory:decodeFileDescriptor	(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     236: astore 28
        //     238: aload 28
        //     240: astore 6
        //     242: aload 10
        //     244: ifnull +8 -> 252
        //     247: aload 10
        //     249: invokevirtual 127	java/io/FileInputStream:close	()V
        //     252: iload_1
        //     253: iconst_3
        //     254: if_icmpne +15 -> 269
        //     257: aload 6
        //     259: bipush 96
        //     261: bipush 96
        //     263: iconst_2
        //     264: invokestatic 145	android/media/ThumbnailUtils:extractThumbnail	(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
        //     267: astore 6
        //     269: aload 6
        //     271: astore 8
        //     273: goto -100 -> 173
        //     276: astore 11
        //     278: ldc 24
        //     280: ldc 147
        //     282: aload 11
        //     284: invokestatic 153	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     287: pop
        //     288: aload 9
        //     290: ifnull -38 -> 252
        //     293: aload 9
        //     295: invokevirtual 127	java/io/FileInputStream:close	()V
        //     298: goto -46 -> 252
        //     301: astore 16
        //     303: ldc 24
        //     305: astore 17
        //     307: ldc 147
        //     309: astore 18
        //     311: aload 17
        //     313: aload 18
        //     315: aload 16
        //     317: invokestatic 153	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     320: pop
        //     321: goto -69 -> 252
        //     324: astore 20
        //     326: ldc 24
        //     328: new 155	java/lang/StringBuilder
        //     331: dup
        //     332: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     335: ldc 158
        //     337: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     340: aload_0
        //     341: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     344: ldc 164
        //     346: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     349: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     352: aload 20
        //     354: invokestatic 153	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     357: pop
        //     358: aload 9
        //     360: ifnull -108 -> 252
        //     363: aload 9
        //     365: invokevirtual 127	java/io/FileInputStream:close	()V
        //     368: goto -116 -> 252
        //     371: astore 16
        //     373: ldc 24
        //     375: astore 17
        //     377: ldc 147
        //     379: astore 18
        //     381: goto -70 -> 311
        //     384: astore 12
        //     386: aload 9
        //     388: ifnull +8 -> 396
        //     391: aload 9
        //     393: invokevirtual 127	java/io/FileInputStream:close	()V
        //     396: aload 12
        //     398: athrow
        //     399: astore 13
        //     401: ldc 24
        //     403: ldc 147
        //     405: aload 13
        //     407: invokestatic 153	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     410: pop
        //     411: goto -15 -> 396
        //     414: astore 25
        //     416: ldc 24
        //     418: ldc 147
        //     420: aload 25
        //     422: invokestatic 153	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     425: pop
        //     426: goto -253 -> 173
        //     429: astore 16
        //     431: ldc 24
        //     433: astore 17
        //     435: ldc 147
        //     437: astore 18
        //     439: goto -128 -> 311
        //     442: astore 12
        //     444: aload 10
        //     446: astore 9
        //     448: goto -62 -> 386
        //     451: astore 20
        //     453: aload 10
        //     455: astore 9
        //     457: goto -131 -> 326
        //     460: astore 11
        //     462: aload 10
        //     464: astore 9
        //     466: goto -188 -> 278
        //
        // Exception table:
        //     from	to	target	type
        //     81	91	276	java/io/IOException
        //     293	298	301	java/io/IOException
        //     81	91	324	java/lang/OutOfMemoryError
        //     363	368	371	java/io/IOException
        //     81	91	384	finally
        //     278	288	384	finally
        //     326	358	384	finally
        //     391	396	399	java/io/IOException
        //     168	173	414	java/io/IOException
        //     247	252	429	java/io/IOException
        //     91	153	442	finally
        //     195	238	442	finally
        //     91	153	451	java/lang/OutOfMemoryError
        //     195	238	451	java/lang/OutOfMemoryError
        //     91	153	460	java/io/IOException
        //     195	238	460	java/io/IOException
    }

    // ERROR //
    private static void createThumbnailFromEXIF(String paramString, int paramInt1, int paramInt2, SizedThumbnailBitmap paramSizedThumbnailBitmap)
    {
        // Byte code:
        //     0: aload_0
        //     1: ifnonnull +4 -> 5
        //     4: return
        //     5: aconst_null
        //     6: astore 4
        //     8: new 170	android/media/ExifInterface
        //     11: dup
        //     12: aload_0
        //     13: invokespecial 171	android/media/ExifInterface:<init>	(Ljava/lang/String;)V
        //     16: astore 5
        //     18: aload 5
        //     20: invokevirtual 175	android/media/ExifInterface:getThumbnail	()[B
        //     23: astore 16
        //     25: aload 16
        //     27: astore 4
        //     29: new 47	android/graphics/BitmapFactory$Options
        //     32: dup
        //     33: invokespecial 110	android/graphics/BitmapFactory$Options:<init>	()V
        //     36: astore 8
        //     38: new 47	android/graphics/BitmapFactory$Options
        //     41: dup
        //     42: invokespecial 110	android/graphics/BitmapFactory$Options:<init>	()V
        //     45: astore 9
        //     47: iconst_0
        //     48: istore 10
        //     50: aload 4
        //     52: ifnull +46 -> 98
        //     55: aload 9
        //     57: iconst_1
        //     58: putfield 117	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     61: aload 4
        //     63: iconst_0
        //     64: aload 4
        //     66: arraylength
        //     67: aload 9
        //     69: invokestatic 179	android/graphics/BitmapFactory:decodeByteArray	([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     72: pop
        //     73: aload 9
        //     75: aload 9
        //     77: iload_1
        //     78: iload_2
        //     79: invokestatic 129	android/media/ThumbnailUtils:computeSampleSize	(Landroid/graphics/BitmapFactory$Options;II)I
        //     82: putfield 113	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     85: aload 9
        //     87: getfield 50	android/graphics/BitmapFactory$Options:outWidth	I
        //     90: aload 9
        //     92: getfield 113	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     95: idiv
        //     96: istore 10
        //     98: aload 8
        //     100: iconst_1
        //     101: putfield 117	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     104: aload_0
        //     105: aload 8
        //     107: invokestatic 183	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     110: pop
        //     111: aload 8
        //     113: aload 8
        //     115: iload_1
        //     116: iload_2
        //     117: invokestatic 129	android/media/ThumbnailUtils:computeSampleSize	(Landroid/graphics/BitmapFactory$Options;II)I
        //     120: putfield 113	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     123: aload 8
        //     125: getfield 50	android/graphics/BitmapFactory$Options:outWidth	I
        //     128: aload 8
        //     130: getfield 113	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     133: idiv
        //     134: istore 12
        //     136: aload 4
        //     138: ifnull +86 -> 224
        //     141: iload 10
        //     143: iload 12
        //     145: if_icmplt +79 -> 224
        //     148: aload 9
        //     150: getfield 50	android/graphics/BitmapFactory$Options:outWidth	I
        //     153: istore 13
        //     155: aload 9
        //     157: getfield 53	android/graphics/BitmapFactory$Options:outHeight	I
        //     160: istore 14
        //     162: aload 9
        //     164: iconst_0
        //     165: putfield 117	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     168: aload_3
        //     169: aload 4
        //     171: iconst_0
        //     172: aload 4
        //     174: arraylength
        //     175: aload 9
        //     177: invokestatic 179	android/graphics/BitmapFactory:decodeByteArray	([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     180: putfield 100	android/media/ThumbnailUtils$SizedThumbnailBitmap:mBitmap	Landroid/graphics/Bitmap;
        //     183: aload_3
        //     184: getfield 100	android/media/ThumbnailUtils$SizedThumbnailBitmap:mBitmap	Landroid/graphics/Bitmap;
        //     187: ifnull -183 -> 4
        //     190: aload_3
        //     191: aload 4
        //     193: putfield 187	android/media/ThumbnailUtils$SizedThumbnailBitmap:mThumbnailData	[B
        //     196: aload_3
        //     197: iload 13
        //     199: putfield 190	android/media/ThumbnailUtils$SizedThumbnailBitmap:mThumbnailWidth	I
        //     202: aload_3
        //     203: iload 14
        //     205: putfield 193	android/media/ThumbnailUtils$SizedThumbnailBitmap:mThumbnailHeight	I
        //     208: goto -204 -> 4
        //     211: astore 6
        //     213: ldc 24
        //     215: aload 6
        //     217: invokestatic 197	android/util/Log:w	(Ljava/lang/String;Ljava/lang/Throwable;)I
        //     220: pop
        //     221: goto -192 -> 29
        //     224: aload 8
        //     226: iconst_0
        //     227: putfield 117	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     230: aload_3
        //     231: aload_0
        //     232: aload 8
        //     234: invokestatic 183	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     237: putfield 100	android/media/ThumbnailUtils$SizedThumbnailBitmap:mBitmap	Landroid/graphics/Bitmap;
        //     240: goto -236 -> 4
        //     243: astore 6
        //     245: goto -32 -> 213
        //
        // Exception table:
        //     from	to	target	type
        //     8	18	211	java/io/IOException
        //     18	25	243	java/io/IOException
    }

    // ERROR //
    public static Bitmap createVideoThumbnail(String paramString, int paramInt)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 204	android/media/MediaMetadataRetriever
        //     5: dup
        //     6: invokespecial 205	android/media/MediaMetadataRetriever:<init>	()V
        //     9: astore_3
        //     10: aload_3
        //     11: aload_0
        //     12: invokevirtual 208	android/media/MediaMetadataRetriever:setDataSource	(Ljava/lang/String;)V
        //     15: aload_3
        //     16: ldc2_w 209
        //     19: invokevirtual 214	android/media/MediaMetadataRetriever:getFrameAtTime	(J)Landroid/graphics/Bitmap;
        //     22: astore 14
        //     24: aload 14
        //     26: astore_2
        //     27: aload_3
        //     28: invokevirtual 217	android/media/MediaMetadataRetriever:release	()V
        //     31: aload_2
        //     32: ifnonnull +18 -> 50
        //     35: aconst_null
        //     36: astore 5
        //     38: aload 5
        //     40: areturn
        //     41: astore 12
        //     43: aload_3
        //     44: invokevirtual 217	android/media/MediaMetadataRetriever:release	()V
        //     47: aload 12
        //     49: athrow
        //     50: iload_1
        //     51: iconst_1
        //     52: if_icmpne +70 -> 122
        //     55: aload_2
        //     56: invokevirtual 223	android/graphics/Bitmap:getWidth	()I
        //     59: istore 6
        //     61: aload_2
        //     62: invokevirtual 226	android/graphics/Bitmap:getHeight	()I
        //     65: istore 7
        //     67: iload 6
        //     69: iload 7
        //     71: invokestatic 230	java/lang/Math:max	(II)I
        //     74: istore 8
        //     76: iload 8
        //     78: sipush 512
        //     81: if_icmple +35 -> 116
        //     84: ldc 231
        //     86: iload 8
        //     88: i2f
        //     89: fdiv
        //     90: fstore 9
        //     92: aload_2
        //     93: fload 9
        //     95: iload 6
        //     97: i2f
        //     98: fmul
        //     99: invokestatic 235	java/lang/Math:round	(F)I
        //     102: fload 9
        //     104: iload 7
        //     106: i2f
        //     107: fmul
        //     108: invokestatic 235	java/lang/Math:round	(F)I
        //     111: iconst_1
        //     112: invokestatic 239	android/graphics/Bitmap:createScaledBitmap	(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
        //     115: astore_2
        //     116: aload_2
        //     117: astore 5
        //     119: goto -81 -> 38
        //     122: iload_1
        //     123: iconst_3
        //     124: if_icmpne -8 -> 116
        //     127: aload_2
        //     128: bipush 96
        //     130: bipush 96
        //     132: iconst_2
        //     133: invokestatic 145	android/media/ThumbnailUtils:extractThumbnail	(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
        //     136: astore_2
        //     137: goto -21 -> 116
        //     140: astore 11
        //     142: aload_3
        //     143: invokevirtual 217	android/media/MediaMetadataRetriever:release	()V
        //     146: goto -115 -> 31
        //     149: astore 4
        //     151: aload_3
        //     152: invokevirtual 217	android/media/MediaMetadataRetriever:release	()V
        //     155: goto -124 -> 31
        //     158: astore 13
        //     160: goto -113 -> 47
        //     163: astore 10
        //     165: goto -134 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     10	24	41	finally
        //     10	24	140	java/lang/IllegalArgumentException
        //     10	24	149	java/lang/RuntimeException
        //     43	47	158	java/lang/RuntimeException
        //     27	31	163	java/lang/RuntimeException
        //     142	155	163	java/lang/RuntimeException
    }

    public static Bitmap extractThumbnail(Bitmap paramBitmap, int paramInt1, int paramInt2)
    {
        return extractThumbnail(paramBitmap, paramInt1, paramInt2, 0);
    }

    public static Bitmap extractThumbnail(Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3)
    {
        Bitmap localBitmap;
        if (paramBitmap == null)
        {
            localBitmap = null;
            return localBitmap;
        }
        if (paramBitmap.getWidth() < paramBitmap.getHeight());
        for (float f = paramInt1 / paramBitmap.getWidth(); ; f = paramInt2 / paramBitmap.getHeight())
        {
            Matrix localMatrix = new Matrix();
            localMatrix.setScale(f, f);
            localBitmap = transform(localMatrix, paramBitmap, paramInt1, paramInt2, paramInt3 | 0x1);
            break;
        }
    }

    private static Bitmap makeBitmap(int paramInt1, int paramInt2, Uri paramUri, ContentResolver paramContentResolver, ParcelFileDescriptor paramParcelFileDescriptor, BitmapFactory.Options paramOptions)
    {
        Object localObject1 = null;
        if (paramParcelFileDescriptor == null);
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor = makeInputStream(paramUri, paramContentResolver);
            paramParcelFileDescriptor = localParcelFileDescriptor;
            if (paramParcelFileDescriptor == null);
            while (true)
            {
                return localObject1;
                if (paramOptions == null)
                    paramOptions = new BitmapFactory.Options();
                FileDescriptor localFileDescriptor = paramParcelFileDescriptor.getFileDescriptor();
                paramOptions.inSampleSize = 1;
                paramOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(localFileDescriptor, null, paramOptions);
                if ((!paramOptions.mCancel) && (paramOptions.outWidth != -1) && (paramOptions.outHeight != -1))
                {
                    paramOptions.inSampleSize = computeSampleSize(paramOptions, paramInt1, paramInt2);
                    paramOptions.inJustDecodeBounds = false;
                    paramOptions.inDither = false;
                    paramOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap localBitmap = BitmapFactory.decodeFileDescriptor(localFileDescriptor, null, paramOptions);
                    closeSilently(paramParcelFileDescriptor);
                    localObject1 = localBitmap;
                }
            }
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
            while (true)
                Log.e("ThumbnailUtils", "Got oom exception ", localOutOfMemoryError);
        }
        finally
        {
            closeSilently(paramParcelFileDescriptor);
        }
    }

    private static ParcelFileDescriptor makeInputStream(Uri paramUri, ContentResolver paramContentResolver)
    {
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor2 = paramContentResolver.openFileDescriptor(paramUri, "r");
            localParcelFileDescriptor1 = localParcelFileDescriptor2;
            return localParcelFileDescriptor1;
        }
        catch (IOException localIOException)
        {
            while (true)
                ParcelFileDescriptor localParcelFileDescriptor1 = null;
        }
    }

    private static Bitmap transform(Matrix paramMatrix, Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3)
    {
        int i;
        if ((paramInt3 & 0x1) != 0)
        {
            i = 1;
            if ((paramInt3 & 0x2) == 0)
                break label205;
        }
        Bitmap localBitmap2;
        label205: for (int j = 1; ; j = 0)
        {
            int k = paramBitmap.getWidth() - paramInt1;
            int m = paramBitmap.getHeight() - paramInt2;
            if ((i != 0) || ((k >= 0) && (m >= 0)))
                break label211;
            localBitmap2 = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
            Canvas localCanvas = new Canvas(localBitmap2);
            int i2 = Math.max(0, k / 2);
            int i3 = Math.max(0, m / 2);
            Rect localRect1 = new Rect(i2, i3, i2 + Math.min(paramInt1, paramBitmap.getWidth()), i3 + Math.min(paramInt2, paramBitmap.getHeight()));
            int i4 = (paramInt1 - localRect1.width()) / 2;
            int i5 = (paramInt2 - localRect1.height()) / 2;
            Rect localRect2 = new Rect(i4, i5, paramInt1 - i4, paramInt2 - i5);
            localCanvas.drawBitmap(paramBitmap, localRect1, localRect2, null);
            if (j != 0)
                paramBitmap.recycle();
            localCanvas.setBitmap(null);
            return localBitmap2;
            i = 0;
            break;
        }
        label211: float f1 = paramBitmap.getWidth();
        float f2 = paramBitmap.getHeight();
        if (f1 / f2 > paramInt1 / paramInt2)
        {
            float f4 = paramInt2 / f2;
            if ((f4 < 0.9F) || (f4 > 1.0F))
            {
                paramMatrix.setScale(f4, f4);
                label270: if (paramMatrix == null)
                    break label420;
            }
        }
        label420: for (Bitmap localBitmap1 = Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), paramMatrix, true); ; localBitmap1 = paramBitmap)
        {
            if ((j != 0) && (localBitmap1 != paramBitmap))
                paramBitmap.recycle();
            int n = Math.max(0, localBitmap1.getWidth() - paramInt1);
            int i1 = Math.max(0, localBitmap1.getHeight() - paramInt2);
            localBitmap2 = Bitmap.createBitmap(localBitmap1, n / 2, i1 / 2, paramInt1, paramInt2);
            if ((localBitmap2 == localBitmap1) || ((j == 0) && (localBitmap1 == paramBitmap)))
                break;
            localBitmap1.recycle();
            break;
            paramMatrix = null;
            break label270;
            float f3 = paramInt1 / f1;
            if ((f3 < 0.9F) || (f3 > 1.0F))
            {
                paramMatrix.setScale(f3, f3);
                break label270;
            }
            paramMatrix = null;
            break label270;
        }
    }

    private static class SizedThumbnailBitmap
    {
        public Bitmap mBitmap;
        public byte[] mThumbnailData;
        public int mThumbnailHeight;
        public int mThumbnailWidth;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.ThumbnailUtils
 * JD-Core Version:        0.6.2
 */