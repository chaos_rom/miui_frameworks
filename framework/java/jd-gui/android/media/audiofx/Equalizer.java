package android.media.audiofx;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

public class Equalizer extends AudioEffect
{
    public static final int PARAM_BAND_FREQ_RANGE = 4;
    public static final int PARAM_BAND_LEVEL = 2;
    public static final int PARAM_CENTER_FREQ = 3;
    public static final int PARAM_CURRENT_PRESET = 6;
    public static final int PARAM_GET_BAND = 5;
    public static final int PARAM_GET_NUM_OF_PRESETS = 7;
    public static final int PARAM_GET_PRESET_NAME = 8;
    public static final int PARAM_LEVEL_RANGE = 1;
    public static final int PARAM_NUM_BANDS = 0;
    private static final int PARAM_PROPERTIES = 9;
    public static final int PARAM_STRING_SIZE_MAX = 32;
    private static final String TAG = "Equalizer";
    private BaseParameterListener mBaseParamListener = null;
    private short mNumBands = 0;
    private int mNumPresets;
    private OnParameterChangeListener mParamListener = null;
    private final Object mParamListenerLock = new Object();
    private String[] mPresetNames;

    public Equalizer(int paramInt1, int paramInt2)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException, RuntimeException
    {
        super(EFFECT_TYPE_EQUALIZER, EFFECT_TYPE_NULL, paramInt1, paramInt2);
        if (paramInt2 == 0)
            Log.w("Equalizer", "WARNING: attaching an Equalizer to global output mix is deprecated!");
        getNumberOfBands();
        this.mNumPresets = getNumberOfPresets();
        if (this.mNumPresets != 0)
        {
            this.mPresetNames = new String[this.mNumPresets];
            byte[] arrayOfByte = new byte[32];
            int[] arrayOfInt = new int[2];
            arrayOfInt[0] = 8;
            int i = 0;
            while (true)
                if (i < this.mNumPresets)
                {
                    arrayOfInt[1] = i;
                    checkStatus(getParameter(arrayOfInt, arrayOfByte));
                    for (int j = 0; arrayOfByte[j] != 0; j++);
                    try
                    {
                        this.mPresetNames[i] = new String(arrayOfByte, 0, j, "ISO-8859-1");
                        i++;
                    }
                    catch (UnsupportedEncodingException localUnsupportedEncodingException)
                    {
                        while (true)
                            Log.e("Equalizer", "preset name decode error");
                    }
                }
        }
    }

    public short getBand(int paramInt)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        int[] arrayOfInt = new int[2];
        short[] arrayOfShort = new short[1];
        arrayOfInt[0] = 5;
        arrayOfInt[1] = paramInt;
        checkStatus(getParameter(arrayOfInt, arrayOfShort));
        return arrayOfShort[0];
    }

    public int[] getBandFreqRange(short paramShort)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        int[] arrayOfInt1 = new int[2];
        int[] arrayOfInt2 = new int[2];
        arrayOfInt1[0] = 4;
        arrayOfInt1[1] = paramShort;
        checkStatus(getParameter(arrayOfInt1, arrayOfInt2));
        return arrayOfInt2;
    }

    public short getBandLevel(short paramShort)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        int[] arrayOfInt = new int[2];
        short[] arrayOfShort = new short[1];
        arrayOfInt[0] = 2;
        arrayOfInt[1] = paramShort;
        checkStatus(getParameter(arrayOfInt, arrayOfShort));
        return arrayOfShort[0];
    }

    public short[] getBandLevelRange()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        short[] arrayOfShort = new short[2];
        checkStatus(getParameter(1, arrayOfShort));
        return arrayOfShort;
    }

    public int getCenterFreq(short paramShort)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        int[] arrayOfInt1 = new int[2];
        int[] arrayOfInt2 = new int[1];
        arrayOfInt1[0] = 3;
        arrayOfInt1[1] = paramShort;
        checkStatus(getParameter(arrayOfInt1, arrayOfInt2));
        return arrayOfInt2[0];
    }

    public short getCurrentPreset()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        short[] arrayOfShort = new short[1];
        checkStatus(getParameter(6, arrayOfShort));
        return arrayOfShort[0];
    }

    public short getNumberOfBands()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        if (this.mNumBands != 0);
        for (short s = this.mNumBands; ; s = this.mNumBands)
        {
            return s;
            int[] arrayOfInt = new int[1];
            arrayOfInt[0] = 0;
            short[] arrayOfShort = new short[1];
            checkStatus(getParameter(arrayOfInt, arrayOfShort));
            this.mNumBands = arrayOfShort[0];
        }
    }

    public short getNumberOfPresets()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        short[] arrayOfShort = new short[1];
        checkStatus(getParameter(7, arrayOfShort));
        return arrayOfShort[0];
    }

    public String getPresetName(short paramShort)
    {
        if ((paramShort >= 0) && (paramShort < this.mNumPresets));
        for (String str = this.mPresetNames[paramShort]; ; str = "")
            return str;
    }

    public Settings getProperties()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        byte[] arrayOfByte = new byte[4 + 2 * this.mNumBands];
        checkStatus(getParameter(9, arrayOfByte));
        Settings localSettings = new Settings();
        localSettings.curPreset = byteArrayToShort(arrayOfByte, 0);
        localSettings.numBands = byteArrayToShort(arrayOfByte, 2);
        localSettings.bandLevels = new short[this.mNumBands];
        int i = 0;
        while (i < this.mNumBands)
        {
            localSettings.bandLevels[i] = byteArrayToShort(arrayOfByte, 4 + i * 2);
            i += 1;
        }
        return localSettings;
    }

    public void setBandLevel(short paramShort1, short paramShort2)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        int[] arrayOfInt = new int[2];
        short[] arrayOfShort = new short[1];
        arrayOfInt[0] = 2;
        arrayOfInt[1] = paramShort1;
        arrayOfShort[0] = paramShort2;
        checkStatus(setParameter(arrayOfInt, arrayOfShort));
    }

    public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener)
    {
        synchronized (this.mParamListenerLock)
        {
            if (this.mParamListener == null)
            {
                this.mParamListener = paramOnParameterChangeListener;
                this.mBaseParamListener = new BaseParameterListener(null);
                super.setParameterListener(this.mBaseParamListener);
            }
            return;
        }
    }

    public void setProperties(Settings paramSettings)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        if ((paramSettings.numBands != paramSettings.bandLevels.length) || (paramSettings.numBands != this.mNumBands))
            throw new IllegalArgumentException("settings invalid band count: " + paramSettings.numBands);
        byte[][] arrayOfByte1 = new byte[2][];
        arrayOfByte1[0] = shortToByteArray(paramSettings.curPreset);
        arrayOfByte1[1] = shortToByteArray(this.mNumBands);
        byte[] arrayOfByte = concatArrays(arrayOfByte1);
        int i = 0;
        while (i < this.mNumBands)
        {
            byte[][] arrayOfByte2 = new byte[2][];
            arrayOfByte2[0] = arrayOfByte;
            arrayOfByte2[1] = shortToByteArray(paramSettings.bandLevels[i]);
            arrayOfByte = concatArrays(arrayOfByte2);
            i += 1;
        }
        checkStatus(setParameter(9, arrayOfByte));
    }

    public void usePreset(short paramShort)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        checkStatus(setParameter(6, paramShort));
    }

    public static class Settings
    {
        public short[] bandLevels = null;
        public short curPreset;
        public short numBands = 0;

        public Settings()
        {
        }

        public Settings(String paramString)
        {
            StringTokenizer localStringTokenizer = new StringTokenizer(paramString, "=;");
            localStringTokenizer.countTokens();
            if (localStringTokenizer.countTokens() < 5)
                throw new IllegalArgumentException("settings: " + paramString);
            String str = localStringTokenizer.nextToken();
            if (!str.equals("Equalizer"))
                throw new IllegalArgumentException("invalid settings for Equalizer: " + str);
            try
            {
                str = localStringTokenizer.nextToken();
                if (!str.equals("curPreset"))
                    throw new IllegalArgumentException("invalid key name: " + str);
            }
            catch (NumberFormatException localNumberFormatException)
            {
                throw new IllegalArgumentException("invalid value for key: " + str);
            }
            this.curPreset = Short.parseShort(localStringTokenizer.nextToken());
            str = localStringTokenizer.nextToken();
            if (!str.equals("numBands"))
                throw new IllegalArgumentException("invalid key name: " + str);
            this.numBands = Short.parseShort(localStringTokenizer.nextToken());
            if (localStringTokenizer.countTokens() != 2 * this.numBands)
                throw new IllegalArgumentException("settings: " + paramString);
            this.bandLevels = new short[this.numBands];
            int i = 0;
            while (i < this.numBands)
            {
                str = localStringTokenizer.nextToken();
                if (!str.equals("band" + (i + 1) + "Level"))
                    throw new IllegalArgumentException("invalid key name: " + str);
                this.bandLevels[i] = Short.parseShort(localStringTokenizer.nextToken());
                i += 1;
            }
        }

        public String toString()
        {
            String str = new String("Equalizer;curPreset=" + Short.toString(this.curPreset) + ";numBands=" + Short.toString(this.numBands));
            int i = 0;
            while (i < this.numBands)
            {
                str = str.concat(";band" + (i + 1) + "Level=" + Short.toString(this.bandLevels[i]));
                i += 1;
            }
            return str;
        }
    }

    private class BaseParameterListener
        implements AudioEffect.OnParameterChangeListener
    {
        private BaseParameterListener()
        {
        }

        public void onParameterChange(AudioEffect paramAudioEffect, int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        {
            Equalizer.OnParameterChangeListener localOnParameterChangeListener = null;
            while (true)
            {
                int k;
                synchronized (Equalizer.this.mParamListenerLock)
                {
                    if (Equalizer.this.mParamListener != null)
                        localOnParameterChangeListener = Equalizer.this.mParamListener;
                    if (localOnParameterChangeListener != null)
                    {
                        int i = -1;
                        int j = -1;
                        k = -1;
                        if (paramArrayOfByte1.length >= 4)
                        {
                            i = Equalizer.this.byteArrayToInt(paramArrayOfByte1, 0);
                            if (paramArrayOfByte1.length >= 8)
                                j = Equalizer.this.byteArrayToInt(paramArrayOfByte1, 4);
                        }
                        if (paramArrayOfByte2.length != 2)
                            break label149;
                        k = Equalizer.this.byteArrayToShort(paramArrayOfByte2, 0);
                        if ((i != -1) && (k != -1))
                            localOnParameterChangeListener.onParameterChange(Equalizer.this, paramInt, i, j, k);
                    }
                    return;
                }
                label149: if (paramArrayOfByte2.length == 4)
                    k = Equalizer.this.byteArrayToInt(paramArrayOfByte2, 0);
            }
        }
    }

    public static abstract interface OnParameterChangeListener
    {
        public abstract void onParameterChange(Equalizer paramEqualizer, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.audiofx.Equalizer
 * JD-Core Version:        0.6.2
 */