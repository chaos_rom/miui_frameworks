package android.media.audiofx;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.lang.ref.WeakReference;

public class Visualizer
{
    public static final int ALREADY_EXISTS = -2;
    public static final int ERROR = -1;
    public static final int ERROR_BAD_VALUE = -4;
    public static final int ERROR_DEAD_OBJECT = -7;
    public static final int ERROR_INVALID_OPERATION = -5;
    public static final int ERROR_NO_INIT = -3;
    public static final int ERROR_NO_MEMORY = -6;
    private static final int NATIVE_EVENT_FFT_CAPTURE = 1;
    private static final int NATIVE_EVENT_PCM_CAPTURE = 0;
    private static final int NATIVE_EVENT_SERVER_DIED = 2;
    public static final int SCALING_MODE_AS_PLAYED = 1;
    public static final int SCALING_MODE_NORMALIZED = 0;
    public static final int STATE_ENABLED = 2;
    public static final int STATE_INITIALIZED = 1;
    public static final int STATE_UNINITIALIZED = 0;
    public static final int SUCCESS = 0;
    private static final String TAG = "Visualizer-JAVA";
    private OnDataCaptureListener mCaptureListener;
    private int mId;
    private int mJniData;
    private final Object mListenerLock;
    private NativeEventHandler mNativeEventHandler;
    private int mNativeVisualizer;
    private OnServerDiedListener mServerDiedListener;
    private int mState;
    private final Object mStateLock;

    static
    {
        System.loadLibrary("audioeffect_jni");
        native_init();
    }

    // ERROR //
    public Visualizer(int paramInt)
        throws java.lang.UnsupportedOperationException, java.lang.RuntimeException
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 78	java/lang/Object:<init>	()V
        //     4: aload_0
        //     5: iconst_0
        //     6: putfield 80	android/media/audiofx/Visualizer:mState	I
        //     9: aload_0
        //     10: new 4	java/lang/Object
        //     13: dup
        //     14: invokespecial 78	java/lang/Object:<init>	()V
        //     17: putfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     20: aload_0
        //     21: new 4	java/lang/Object
        //     24: dup
        //     25: invokespecial 78	java/lang/Object:<init>	()V
        //     28: putfield 84	android/media/audiofx/Visualizer:mListenerLock	Ljava/lang/Object;
        //     31: aload_0
        //     32: aconst_null
        //     33: putfield 86	android/media/audiofx/Visualizer:mNativeEventHandler	Landroid/media/audiofx/Visualizer$NativeEventHandler;
        //     36: aload_0
        //     37: aconst_null
        //     38: putfield 88	android/media/audiofx/Visualizer:mCaptureListener	Landroid/media/audiofx/Visualizer$OnDataCaptureListener;
        //     41: aload_0
        //     42: aconst_null
        //     43: putfield 90	android/media/audiofx/Visualizer:mServerDiedListener	Landroid/media/audiofx/Visualizer$OnServerDiedListener;
        //     46: iconst_1
        //     47: newarray int
        //     49: astore_2
        //     50: aload_0
        //     51: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     54: astore_3
        //     55: aload_3
        //     56: monitorenter
        //     57: aload_0
        //     58: iconst_0
        //     59: putfield 80	android/media/audiofx/Visualizer:mState	I
        //     62: aload_0
        //     63: new 92	java/lang/ref/WeakReference
        //     66: dup
        //     67: aload_0
        //     68: invokespecial 95	java/lang/ref/WeakReference:<init>	(Ljava/lang/Object;)V
        //     71: iload_1
        //     72: aload_2
        //     73: invokespecial 99	android/media/audiofx/Visualizer:native_setup	(Ljava/lang/Object;I[I)I
        //     76: istore 5
        //     78: iload 5
        //     80: ifeq +105 -> 185
        //     83: iload 5
        //     85: bipush 254
        //     87: if_icmpeq +98 -> 185
        //     90: ldc 44
        //     92: new 101	java/lang/StringBuilder
        //     95: dup
        //     96: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     99: ldc 104
        //     101: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     104: iload 5
        //     106: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     109: ldc 113
        //     111: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     117: invokestatic 123	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     120: pop
        //     121: iload 5
        //     123: tableswitch	default:+17 -> 140, -5:+52->175
        //     141: nop
        //     142: astore_1
        //     143: dup
        //     144: new 101	java/lang/StringBuilder
        //     147: dup
        //     148: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     151: ldc 125
        //     153: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: iload 5
        //     158: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     161: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     164: invokespecial 127	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     167: athrow
        //     168: astore 4
        //     170: aload_3
        //     171: monitorexit
        //     172: aload 4
        //     174: athrow
        //     175: new 74	java/lang/UnsupportedOperationException
        //     178: dup
        //     179: ldc 129
        //     181: invokespecial 130	java/lang/UnsupportedOperationException:<init>	(Ljava/lang/String;)V
        //     184: athrow
        //     185: aload_0
        //     186: aload_2
        //     187: iconst_0
        //     188: iaload
        //     189: putfield 132	android/media/audiofx/Visualizer:mId	I
        //     192: aload_0
        //     193: invokespecial 136	android/media/audiofx/Visualizer:native_getEnabled	()Z
        //     196: ifeq +11 -> 207
        //     199: aload_0
        //     200: iconst_2
        //     201: putfield 80	android/media/audiofx/Visualizer:mState	I
        //     204: aload_3
        //     205: monitorexit
        //     206: return
        //     207: aload_0
        //     208: iconst_1
        //     209: putfield 80	android/media/audiofx/Visualizer:mState	I
        //     212: goto -8 -> 204
        //
        // Exception table:
        //     from	to	target	type
        //     57	172	168	finally
        //     175	212	168	finally
    }

    public static native int[] getCaptureSizeRange();

    public static native int getMaxCaptureRate();

    private final native void native_finalize();

    private final native int native_getCaptureSize();

    private final native boolean native_getEnabled();

    private final native int native_getFft(byte[] paramArrayOfByte);

    private final native int native_getSamplingRate();

    private final native int native_getScalingMode();

    private final native int native_getWaveForm(byte[] paramArrayOfByte);

    private static final native void native_init();

    private final native void native_release();

    private final native int native_setCaptureSize(int paramInt);

    private final native int native_setEnabled(boolean paramBoolean);

    private final native int native_setPeriodicCapture(int paramInt, boolean paramBoolean1, boolean paramBoolean2);

    private final native int native_setScalingMode(int paramInt);

    private final native int native_setup(Object paramObject, int paramInt, int[] paramArrayOfInt);

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        Visualizer localVisualizer = (Visualizer)((WeakReference)paramObject1).get();
        if (localVisualizer == null);
        while (true)
        {
            return;
            if (localVisualizer.mNativeEventHandler != null)
            {
                Message localMessage = localVisualizer.mNativeEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localVisualizer.mNativeEventHandler.sendMessage(localMessage);
            }
        }
    }

    protected void finalize()
    {
        native_finalize();
    }

    // ERROR //
    public int getCaptureSize()
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: ifne +38 -> 49
        //     14: new 181	java/lang/IllegalStateException
        //     17: dup
        //     18: new 101	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     25: ldc 183
        //     27: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     30: aload_0
        //     31: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     34: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     37: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     40: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: astore_2
        //     45: aload_1
        //     46: monitorexit
        //     47: aload_2
        //     48: athrow
        //     49: aload_0
        //     50: invokespecial 186	android/media/audiofx/Visualizer:native_getCaptureSize	()I
        //     53: istore_3
        //     54: aload_1
        //     55: monitorexit
        //     56: iload_3
        //     57: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	47	44	finally
        //     49	56	44	finally
    }

    // ERROR //
    public boolean getEnabled()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: ifne +38 -> 49
        //     14: new 181	java/lang/IllegalStateException
        //     17: dup
        //     18: new 101	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     25: ldc 189
        //     27: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     30: aload_0
        //     31: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     34: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     37: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     40: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: astore_2
        //     45: aload_1
        //     46: monitorexit
        //     47: aload_2
        //     48: athrow
        //     49: aload_0
        //     50: invokespecial 136	android/media/audiofx/Visualizer:native_getEnabled	()Z
        //     53: istore_3
        //     54: aload_1
        //     55: monitorexit
        //     56: iload_3
        //     57: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	47	44	finally
        //     49	56	44	finally
    }

    // ERROR //
    public int getFft(byte[] paramArrayOfByte)
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: iconst_2
        //     12: if_icmpeq +38 -> 50
        //     15: new 181	java/lang/IllegalStateException
        //     18: dup
        //     19: new 101	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     26: ldc 192
        //     28: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     31: aload_0
        //     32: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     35: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     38: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     41: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     44: athrow
        //     45: astore_3
        //     46: aload_2
        //     47: monitorexit
        //     48: aload_3
        //     49: athrow
        //     50: aload_0
        //     51: aload_1
        //     52: invokespecial 194	android/media/audiofx/Visualizer:native_getFft	([B)I
        //     55: istore 4
        //     57: aload_2
        //     58: monitorexit
        //     59: iload 4
        //     61: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	48	45	finally
        //     50	59	45	finally
    }

    // ERROR //
    public int getSamplingRate()
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: ifne +38 -> 49
        //     14: new 181	java/lang/IllegalStateException
        //     17: dup
        //     18: new 101	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     25: ldc 197
        //     27: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     30: aload_0
        //     31: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     34: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     37: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     40: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: astore_2
        //     45: aload_1
        //     46: monitorexit
        //     47: aload_2
        //     48: athrow
        //     49: aload_0
        //     50: invokespecial 199	android/media/audiofx/Visualizer:native_getSamplingRate	()I
        //     53: istore_3
        //     54: aload_1
        //     55: monitorexit
        //     56: iload_3
        //     57: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	47	44	finally
        //     49	56	44	finally
    }

    // ERROR //
    public int getScalingMode()
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: ifne +38 -> 49
        //     14: new 181	java/lang/IllegalStateException
        //     17: dup
        //     18: new 101	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     25: ldc 202
        //     27: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     30: aload_0
        //     31: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     34: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     37: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     40: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: astore_2
        //     45: aload_1
        //     46: monitorexit
        //     47: aload_2
        //     48: athrow
        //     49: aload_0
        //     50: invokespecial 204	android/media/audiofx/Visualizer:native_getScalingMode	()I
        //     53: istore_3
        //     54: aload_1
        //     55: monitorexit
        //     56: iload_3
        //     57: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	47	44	finally
        //     49	56	44	finally
    }

    // ERROR //
    public int getWaveForm(byte[] paramArrayOfByte)
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: iconst_2
        //     12: if_icmpeq +38 -> 50
        //     15: new 181	java/lang/IllegalStateException
        //     18: dup
        //     19: new 101	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     26: ldc 207
        //     28: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     31: aload_0
        //     32: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     35: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     38: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     41: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     44: athrow
        //     45: astore_3
        //     46: aload_2
        //     47: monitorexit
        //     48: aload_3
        //     49: athrow
        //     50: aload_0
        //     51: aload_1
        //     52: invokespecial 209	android/media/audiofx/Visualizer:native_getWaveForm	([B)I
        //     55: istore 4
        //     57: aload_2
        //     58: monitorexit
        //     59: iload 4
        //     61: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	48	45	finally
        //     50	59	45	finally
    }

    public void release()
    {
        synchronized (this.mStateLock)
        {
            native_release();
            this.mState = 0;
            return;
        }
    }

    // ERROR //
    public int setCaptureSize(int paramInt)
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: iconst_1
        //     12: if_icmpeq +38 -> 50
        //     15: new 181	java/lang/IllegalStateException
        //     18: dup
        //     19: new 101	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     26: ldc 215
        //     28: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     31: aload_0
        //     32: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     35: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     38: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     41: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     44: athrow
        //     45: astore_3
        //     46: aload_2
        //     47: monitorexit
        //     48: aload_3
        //     49: athrow
        //     50: aload_0
        //     51: iload_1
        //     52: invokespecial 217	android/media/audiofx/Visualizer:native_setCaptureSize	(I)I
        //     55: istore 4
        //     57: aload_2
        //     58: monitorexit
        //     59: iload 4
        //     61: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	48	45	finally
        //     50	59	45	finally
    }

    public int setDataCaptureListener(OnDataCaptureListener paramOnDataCaptureListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        while (true)
        {
            int i;
            synchronized (this.mListenerLock)
            {
                this.mCaptureListener = paramOnDataCaptureListener;
                if (paramOnDataCaptureListener == null)
                {
                    paramBoolean1 = false;
                    paramBoolean2 = false;
                }
                i = native_setPeriodicCapture(paramInt, paramBoolean1, paramBoolean2);
                if ((i == 0) && (paramOnDataCaptureListener != null) && (this.mNativeEventHandler == null))
                {
                    Looper localLooper1 = Looper.myLooper();
                    if (localLooper1 != null)
                        this.mNativeEventHandler = new NativeEventHandler(this, localLooper1);
                }
                else
                {
                    return i;
                }
            }
            Looper localLooper2 = Looper.getMainLooper();
            if (localLooper2 != null)
            {
                this.mNativeEventHandler = new NativeEventHandler(this, localLooper2);
            }
            else
            {
                this.mNativeEventHandler = null;
                i = -3;
            }
        }
    }

    // ERROR //
    public int setEnabled(boolean paramBoolean)
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: iconst_2
        //     1: istore_2
        //     2: aload_0
        //     3: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     6: astore_3
        //     7: aload_3
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     13: ifne +40 -> 53
        //     16: new 181	java/lang/IllegalStateException
        //     19: dup
        //     20: new 101	java/lang/StringBuilder
        //     23: dup
        //     24: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     27: ldc 236
        //     29: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     32: aload_0
        //     33: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     36: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     39: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     42: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     45: athrow
        //     46: astore 4
        //     48: aload_3
        //     49: monitorexit
        //     50: aload 4
        //     52: athrow
        //     53: iconst_0
        //     54: istore 5
        //     56: iload_1
        //     57: ifeq +11 -> 68
        //     60: aload_0
        //     61: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     64: iconst_1
        //     65: if_icmpeq +15 -> 80
        //     68: iload_1
        //     69: ifne +32 -> 101
        //     72: aload_0
        //     73: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     76: iload_2
        //     77: if_icmpne +24 -> 101
        //     80: aload_0
        //     81: iload_1
        //     82: invokespecial 238	android/media/audiofx/Visualizer:native_setEnabled	(Z)I
        //     85: istore 5
        //     87: iload 5
        //     89: ifne +12 -> 101
        //     92: iload_1
        //     93: ifeq +13 -> 106
        //     96: aload_0
        //     97: iload_2
        //     98: putfield 80	android/media/audiofx/Visualizer:mState	I
        //     101: aload_3
        //     102: monitorexit
        //     103: iload 5
        //     105: ireturn
        //     106: iconst_1
        //     107: istore_2
        //     108: goto -12 -> 96
        //
        // Exception table:
        //     from	to	target	type
        //     9	50	46	finally
        //     60	103	46	finally
    }

    // ERROR //
    public int setScalingMode(int paramInt)
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	android/media/audiofx/Visualizer:mStateLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     11: ifne +38 -> 49
        //     14: new 181	java/lang/IllegalStateException
        //     17: dup
        //     18: new 101	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 102	java/lang/StringBuilder:<init>	()V
        //     25: ldc 241
        //     27: invokevirtual 108	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     30: aload_0
        //     31: getfield 80	android/media/audiofx/Visualizer:mState	I
        //     34: invokevirtual 111	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     37: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     40: invokespecial 184	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: astore_3
        //     45: aload_2
        //     46: monitorexit
        //     47: aload_3
        //     48: athrow
        //     49: aload_0
        //     50: iload_1
        //     51: invokespecial 243	android/media/audiofx/Visualizer:native_setScalingMode	(I)I
        //     54: istore 4
        //     56: aload_2
        //     57: monitorexit
        //     58: iload 4
        //     60: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	47	44	finally
        //     49	58	44	finally
    }

    public int setServerDiedListener(OnServerDiedListener paramOnServerDiedListener)
    {
        synchronized (this.mListenerLock)
        {
            this.mServerDiedListener = paramOnServerDiedListener;
            return 0;
        }
    }

    private class NativeEventHandler extends Handler
    {
        private Visualizer mVisualizer;

        public NativeEventHandler(Visualizer paramLooper, Looper arg3)
        {
            super();
            this.mVisualizer = paramLooper;
        }

        private void handleCaptureMessage(Message paramMessage)
        {
            while (true)
            {
                Visualizer.OnDataCaptureListener localOnDataCaptureListener;
                byte[] arrayOfByte;
                int i;
                synchronized (Visualizer.this.mListenerLock)
                {
                    localOnDataCaptureListener = this.mVisualizer.mCaptureListener;
                    if (localOnDataCaptureListener != null)
                    {
                        arrayOfByte = (byte[])paramMessage.obj;
                        i = paramMessage.arg1;
                    }
                    switch (paramMessage.what)
                    {
                    default:
                        Log.e("Visualizer-JAVA", "Unknown native event in handleCaptureMessge: " + paramMessage.what);
                        return;
                    case 0:
                    case 1:
                    }
                }
                localOnDataCaptureListener.onWaveFormDataCapture(this.mVisualizer, arrayOfByte, i);
                continue;
                localOnDataCaptureListener.onFftDataCapture(this.mVisualizer, arrayOfByte, i);
            }
        }

        private void handleServerDiedMessage(Message paramMessage)
        {
            synchronized (Visualizer.this.mListenerLock)
            {
                Visualizer.OnServerDiedListener localOnServerDiedListener = this.mVisualizer.mServerDiedListener;
                if (localOnServerDiedListener != null)
                    localOnServerDiedListener.onServerDied();
                return;
            }
        }

        public void handleMessage(Message paramMessage)
        {
            if (this.mVisualizer == null);
            while (true)
            {
                return;
                switch (paramMessage.what)
                {
                default:
                    Log.e("Visualizer-JAVA", "Unknown native event: " + paramMessage.what);
                    break;
                case 0:
                case 1:
                    handleCaptureMessage(paramMessage);
                    break;
                case 2:
                    handleServerDiedMessage(paramMessage);
                }
            }
        }
    }

    public static abstract interface OnServerDiedListener
    {
        public abstract void onServerDied();
    }

    public static abstract interface OnDataCaptureListener
    {
        public abstract void onFftDataCapture(Visualizer paramVisualizer, byte[] paramArrayOfByte, int paramInt);

        public abstract void onWaveFormDataCapture(Visualizer paramVisualizer, byte[] paramArrayOfByte, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.audiofx.Visualizer
 * JD-Core Version:        0.6.2
 */