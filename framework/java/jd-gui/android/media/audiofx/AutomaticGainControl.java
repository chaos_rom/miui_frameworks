package android.media.audiofx;

public class AutomaticGainControl extends AudioEffect
{
    private static final String TAG = "AutomaticGainControl";

    private AutomaticGainControl(int paramInt)
        throws IllegalArgumentException, UnsupportedOperationException, RuntimeException
    {
        super(EFFECT_TYPE_AGC, EFFECT_TYPE_NULL, 0, paramInt);
    }

    // ERROR //
    public static AutomaticGainControl create(int paramInt)
    {
        // Byte code:
        //     0: new 2	android/media/audiofx/AutomaticGainControl
        //     3: dup
        //     4: iload_0
        //     5: invokespecial 30	android/media/audiofx/AutomaticGainControl:<init>	(I)V
        //     8: astore_1
        //     9: aload_1
        //     10: pop
        //     11: aload_1
        //     12: areturn
        //     13: astore 8
        //     15: ldc 8
        //     17: new 32	java/lang/StringBuilder
        //     20: dup
        //     21: invokespecial 35	java/lang/StringBuilder:<init>	()V
        //     24: ldc 37
        //     26: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: aconst_null
        //     30: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     33: invokevirtual 48	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     36: invokestatic 54	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     39: pop
        //     40: goto +25 -> 65
        //     43: astore 6
        //     45: ldc 8
        //     47: ldc 56
        //     49: invokestatic 54	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     52: pop
        //     53: goto +12 -> 65
        //     56: astore_3
        //     57: ldc 8
        //     59: ldc 58
        //     61: invokestatic 54	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     64: pop
        //     65: aconst_null
        //     66: astore_1
        //     67: goto -56 -> 11
        //     70: astore 5
        //     72: goto -7 -> 65
        //
        // Exception table:
        //     from	to	target	type
        //     0	9	13	java/lang/IllegalArgumentException
        //     0	9	43	java/lang/UnsupportedOperationException
        //     0	9	56	java/lang/RuntimeException
        //     0	9	70	finally
        //     15	65	70	finally
    }

    public static boolean isAvailable()
    {
        return AudioEffect.isEffectTypeAvailable(AudioEffect.EFFECT_TYPE_AGC);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.audiofx.AutomaticGainControl
 * JD-Core Version:        0.6.2
 */