package android.media.audiofx;

import java.util.StringTokenizer;

public class PresetReverb extends AudioEffect
{
    public static final int PARAM_PRESET = 0;
    public static final short PRESET_LARGEHALL = 5;
    public static final short PRESET_LARGEROOM = 3;
    public static final short PRESET_MEDIUMHALL = 4;
    public static final short PRESET_MEDIUMROOM = 2;
    public static final short PRESET_NONE = 0;
    public static final short PRESET_PLATE = 6;
    public static final short PRESET_SMALLROOM = 1;
    private static final String TAG = "PresetReverb";
    private BaseParameterListener mBaseParamListener = null;
    private OnParameterChangeListener mParamListener = null;
    private final Object mParamListenerLock = new Object();

    public PresetReverb(int paramInt1, int paramInt2)
        throws IllegalArgumentException, UnsupportedOperationException, RuntimeException
    {
        super(EFFECT_TYPE_PRESET_REVERB, EFFECT_TYPE_NULL, paramInt1, paramInt2);
    }

    public short getPreset()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        short[] arrayOfShort = new short[1];
        checkStatus(getParameter(0, arrayOfShort));
        return arrayOfShort[0];
    }

    public Settings getProperties()
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        Settings localSettings = new Settings();
        short[] arrayOfShort = new short[1];
        checkStatus(getParameter(0, arrayOfShort));
        localSettings.preset = arrayOfShort[0];
        return localSettings;
    }

    public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener)
    {
        synchronized (this.mParamListenerLock)
        {
            if (this.mParamListener == null)
            {
                this.mParamListener = paramOnParameterChangeListener;
                this.mBaseParamListener = new BaseParameterListener(null);
                super.setParameterListener(this.mBaseParamListener);
            }
            return;
        }
    }

    public void setPreset(short paramShort)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        checkStatus(setParameter(0, paramShort));
    }

    public void setProperties(Settings paramSettings)
        throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException
    {
        checkStatus(setParameter(0, paramSettings.preset));
    }

    public static class Settings
    {
        public short preset;

        public Settings()
        {
        }

        public Settings(String paramString)
        {
            StringTokenizer localStringTokenizer = new StringTokenizer(paramString, "=;");
            localStringTokenizer.countTokens();
            if (localStringTokenizer.countTokens() != 3)
                throw new IllegalArgumentException("settings: " + paramString);
            String str = localStringTokenizer.nextToken();
            if (!str.equals("PresetReverb"))
                throw new IllegalArgumentException("invalid settings for PresetReverb: " + str);
            try
            {
                str = localStringTokenizer.nextToken();
                if (!str.equals("preset"))
                    throw new IllegalArgumentException("invalid key name: " + str);
            }
            catch (NumberFormatException localNumberFormatException)
            {
                throw new IllegalArgumentException("invalid value for key: " + str);
            }
            this.preset = Short.parseShort(localStringTokenizer.nextToken());
        }

        public String toString()
        {
            return new String("PresetReverb;preset=" + Short.toString(this.preset));
        }
    }

    private class BaseParameterListener
        implements AudioEffect.OnParameterChangeListener
    {
        private BaseParameterListener()
        {
        }

        public void onParameterChange(AudioEffect paramAudioEffect, int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        {
            PresetReverb.OnParameterChangeListener localOnParameterChangeListener = null;
            synchronized (PresetReverb.this.mParamListenerLock)
            {
                if (PresetReverb.this.mParamListener != null)
                    localOnParameterChangeListener = PresetReverb.this.mParamListener;
                if (localOnParameterChangeListener != null)
                {
                    int i = -1;
                    short s = -1;
                    if (paramArrayOfByte1.length == 4)
                        i = PresetReverb.this.byteArrayToInt(paramArrayOfByte1, 0);
                    if (paramArrayOfByte2.length == 2)
                        s = PresetReverb.this.byteArrayToShort(paramArrayOfByte2, 0);
                    if ((i != -1) && (s != -1))
                        localOnParameterChangeListener.onParameterChange(PresetReverb.this, paramInt, i, s);
                }
                return;
            }
        }
    }

    public static abstract interface OnParameterChangeListener
    {
        public abstract void onParameterChange(PresetReverb paramPresetReverb, int paramInt1, int paramInt2, short paramShort);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.audiofx.PresetReverb
 * JD-Core Version:        0.6.2
 */